﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CUSTOMER_APPROVAL_DETAIL_v501.aspx.vb" Inherits="CUSTOMER_APPROVAL_DETAIL_v501" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<%@ Register Src="~/goAML/Customer_v501/Component/GridAddress.ascx" TagPrefix="nds" TagName="GridAddress" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridAddressWork.ascx" TagPrefix="nds" TagName="GridAddressWork" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridEmail.ascx" TagPrefix="nds" TagName="GridEmail" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridIdentification.ascx" TagPrefix="nds" TagName="GridIdentification" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridPEP.ascx" TagPrefix="nds" TagName="GridPEP" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridPhone.ascx" TagPrefix="nds" TagName="GridPhone" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridPhoneWork.ascx" TagPrefix="nds" TagName="GridPhoneWork" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridSanction.ascx" TagPrefix="nds" TagName="GridSanction" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridEntityIdentification.ascx" TagPrefix="nds" TagName="GridEntityIdentification" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridURL.ascx" TagPrefix="nds" TagName="GridURL" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridRelatedPerson.ascx" TagPrefix="nds" TagName="GridRelatedPerson" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridRelatedEntity.ascx" TagPrefix="nds" TagName="GridRelatedEntity" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridDirector.ascx" TagPrefix="nds" TagName="GridDirector" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #ContentPlaceHolder1_FormPanelOld-innerCt {
            display: block !important;
            overflow-x: scroll !important;
        }
         #ContentPlaceHolder1_FormPanelNew-innerCt {
            display: block !important;
            overflow-x: scroll !important;
        }
        /*.x-autocontainer-outerCt {
            overflow: unset !important;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%--Pop Up--%>
    <ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Phone Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSetCustomerPhoneAdd" runat="server" Title="Phones" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    >
                    <ext:FormPanel ID="FormPanelTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Hidden runat="server" ID="fld_sender"></ext:Hidden>
                            <%--<ext:TextField ID="txtCet" runat="server" LabelWidth="180" FieldLabel="CET No" AnchorHorizontal="90%"></ext:TextField>--%>
                            <ext:ComboBox ID="cb_phone_Contact_Type" runat="server" FieldLabel="Contact Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Contact_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model_Contact_Type">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:ComboBox ID="cb_phone_Communication_Type" runat="server" FieldLabel="Communication Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Communication_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model_Comminication_Type">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                            </ext:ComboBox>

                            <ext:TextField ID="txt_phone_Country_prefix" runat="server" FieldLabel="Country Prefix" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_phone_number" runat="server" FieldLabel="Phone Number" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_phone_extension" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_phone_comments" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnBackSaveDetail" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>

                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailAddress" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Address Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet2" runat="server" Title="Address" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FP_Address_INDVD" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Hidden runat="server" ID="Hidden1"></ext:Hidden>
                            <%--<ext:TextField ID="txtCet" runat="server" LabelWidth="180" FieldLabel="CET No" AnchorHorizontal="90%"></ext:TextField>--%>
                            <ext:ComboBox ID="cmb_kategoriaddress" runat="server" FieldLabel="Address Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_KategoriAddress">
                                        <Model>
                                            <ext:Model runat="server" ID="ModelKategoriAddress">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:TextField ID="Address_INDV" runat="server" FieldLabel="Address" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Town_INDV" runat="server" FieldLabel="Town" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="City_INDV" runat="server" FieldLabel="City" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Zip_INDV" runat="server" FieldLabel="Zip Code" AnchorHorizontal="90%"></ext:TextField>
                            <ext:ComboBox ID="cmb_kodenegara" runat="server" FieldLabel="Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Kode_Negara">
                                        <Model>
                                            <ext:Model runat="server" ID="ModelKodeNegara">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:TextField ID="State_INDVD" runat="server" FieldLabel="Province" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Comment_Address_INDVD" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Btn_Back_Address" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailIdentification" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet3" runat="server" Title="Identification" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FP_Identification_INDV" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_number_identification" runat="server" FieldLabel="Nomor" AnchorHorizontal="90%"></ext:TextField>
                            <ext:ComboBox ID="cmb_Jenis_Dokumen" runat="server" FieldLabel="Jenis Dokumen Identitas" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_jenis_dokumen">
                                        <Model>
                                            <ext:Model runat="server" ID="Model3">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:ComboBox ID="cmb_issued_country" runat="server" FieldLabel="Issued Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_issued_country">
                                        <Model>
                                            <ext:Model runat="server" ID="Model4">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:TextField ID="txt_issued_by" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%"></ext:TextField>
                            <ext:DateField ID="txt_issued_date" runat="server" Format="dd-MMM-yyyy" FieldLabel="Issued Date" AnchorHorizontal="90%"></ext:DateField>
                            <ext:DateField ID="txt_issued_expired_date" runat="server" Format="dd-MMM-yyyy" FieldLabel="Issued Expired Date" AnchorHorizontal="90%"></ext:DateField>
                            <ext:TextField ID="txt_identification_comment" runat="server" FieldLabel="Comment" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button1" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                    <ext:FormPanel ID="FP_Identification_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_number_identification_Director" runat="server" FieldLabel="Nomor" AnchorHorizontal="90%"></ext:TextField>
                            <ext:ComboBox ID="cmb_Jenis_Dokumen_Director" runat="server" FieldLabel="Jenis Dokumen Identitas" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Storejenisdokumen_Director">
                                        <Model>
                                            <ext:Model runat="server" ID="Model30">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:ComboBox ID="cmb_issued_country_Director" runat="server" FieldLabel="Issued Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreIssueCountry_Director">
                                        <Model>
                                            <ext:Model runat="server" ID="Model31">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:TextField ID="txt_issued_by_Director" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%"></ext:TextField>
                            <ext:DateField ID="txt_issued_date_Director" runat="server" FieldLabel="Issued Date" AnchorHorizontal="90%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:DateField ID="txt_issued_expired_date_Director" runat="server" FieldLabel="Issued Expired Date" AnchorHorizontal="90%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:TextField ID="txt_identification_comment_Director" runat="server" FieldLabel="Comment" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button8" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>


    <%--Pop Up--%>
    <ext:Window ID="WindowDetailDirector" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Director Add" Hidden="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet4" runat="server" Title="Director" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanelDetailDirector" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                            <ext:Hidden runat="server" ID="Hidden2"></ext:Hidden>
                            <%--<ext:TextField ID="txtCet" runat="server" LabelWidth="180" FieldLabel="CET No" AnchorHorizontal="90%"></ext:TextField>--%>
                            <ext:DisplayField ID="Name" runat="server" FieldLabel="Full Name"></ext:DisplayField>
                            <ext:DisplayField ID="Role" runat="server" FieldLabel="Role"></ext:DisplayField>
                            <ext:DisplayField ID="BirthPlace" runat="server" FieldLabel="Place of Birth"></ext:DisplayField>
                            <ext:DisplayField ID="BirthDate" runat="server" FieldLabel="Date of Birth"></ext:DisplayField>
                            <ext:DisplayField ID="MomName" runat="server" FieldLabel="Mohther Maiden Name"></ext:DisplayField>
                            <ext:DisplayField ID="Aliase" runat="server" FieldLabel="Alias Name"></ext:DisplayField>
                            <ext:DisplayField ID="SSN" runat="server" FieldLabel="NIK"></ext:DisplayField>
                            <ext:DisplayField ID="PassportNo" runat="server" FieldLabel="Passport No"></ext:DisplayField>
                            <ext:DisplayField ID="PassportFrom" runat="server" FieldLabel="Passport Issued Country"></ext:DisplayField>
                            <ext:DisplayField ID="No_Identity" runat="server" FieldLabel="Other ID Number"></ext:DisplayField>
                            <ext:DisplayField ID="Nasionality1" runat="server" FieldLabel="Nationality 1"></ext:DisplayField>
                            <ext:DisplayField ID="Nasionality2" runat="server" FieldLabel="Nationality 2"></ext:DisplayField>
                            <ext:DisplayField ID="Nasionality3" runat="server" FieldLabel="Nationality 3"></ext:DisplayField>
                            <ext:DisplayField ID="Residence" runat="server" FieldLabel="Residence"></ext:DisplayField>
                            <ext:DisplayField ID="Email" runat="server" FieldLabel="Email"></ext:DisplayField>
                            <ext:Checkbox runat="server" ID="cb_Director_Deceased" FieldLabel="Has Passed Away ?">
                                <DirectEvents>
                                    <%--<Change OnEvent="checkBoxcb_Director_Deceased_click" />--%>
                                </DirectEvents>
                            </ext:Checkbox>
                            <ext:DisplayField runat="server" Hidden="true" FieldLabel="Date of Passed Away" ID="txt_Director_Deceased_Date" AnchorHorizontal="90%" />
                            <ext:Checkbox runat="server" ID="cb_Director_Tax_Reg_Number" FieldLabel="Is PEP ?">
                            </ext:Checkbox>
                            <ext:DisplayField runat="server" FieldLabel="Tax Number" ID="txt_Director_Tax_Number" AnchorHorizontal="90%" />
                            <ext:DisplayField runat="server" FieldLabel="Type" ID="txtType" AnchorHorizontal="90%" Hidden="true" />
                            <ext:DisplayField runat="server" FieldLabel="Source of Wealth" ID="txt_Director_Source_of_Wealth" AnchorHorizontal="90%" />
                            <ext:DisplayField ID="Pekerjaan" runat="server" FieldLabel="Occupation"></ext:DisplayField>
                            <ext:DisplayField runat="server" FieldLabel="Notes" ID="txt_Director_Comments" AnchorHorizontal="90%" />
                            <ext:DisplayField ID="txt_Director_employer_name" runat="server" FieldLabel="Work Place" AnchorHorizontal="90%"></ext:DisplayField>

                            <%--Director Identificiation--%>
                            <ext:GridPanel ID="GP_DirectorIdentification" runat="server" Title="Identification" AutoScroll="true">
                                <Store>
                                    <ext:Store ID="Store_Director_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model29" runat="server">
                                                <%--to do--%>
                                                <Fields>
                                                    <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Type_Document" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Issued_By" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Issue_Date" Type="Date"></ext:ModelField>
                                                    <ext:ModelField Name="Expiry_Date" Type="Date"></ext:ModelField>
                                                    <%-- 2023-10-09, Nael: Name="Country" -> Name="Issued_Country" --%>
                                                    <ext:ModelField Name="Issued_Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Identification_Comment" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                        <ext:Column ID="Column75" runat="server" DataIndex="Number" Text="Nomor" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column77" runat="server" DataIndex="Type_Document" Text="Jenis Dokumen" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column78" runat="server" DataIndex="Issued_By" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:DateColumn ID="Column79" runat="server" DataIndex="Issue_Date" Text="Issued Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                        <ext:DateColumn ID="Column80" runat="server" DataIndex="Expiry_Date" Text="Expired Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                        <ext:Column ID="Column81" runat="server" DataIndex="Issued_Country" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column82" runat="server" DataIndex="Identification_Comment" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn999" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdDirectorIdentification">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--end of Director Identification--%>

                            <%--Address--%>
                            <ext:GridPanel ID="GP_Director_Address" runat="server" Title="Address" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                                <Store>
                                    <ext:Store ID="StoreDirectorAddress" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="modelDirectorAddress" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Address_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column29" runat="server" DataIndex="Address_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column23" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column24" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column68" runat="server" DataIndex="Country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column25" runat="server" DataIndex="State" Text="Province" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column26" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn56" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdAddressDirector">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--End of Address--%>

                            <%--Phone--%>
                            <ext:GridPanel ID="GP_Director_Phone" runat="server" Title="Phone" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                                <Store>
                                    <ext:Store ID="StoreDirectorPhone" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="modelDirectorPhone" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="column27" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column28" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column30" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column31" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column32" runat="server" DataIndex="tph_extension" Text="Extension" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column33" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn6" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdPhoneDirector">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--End of Phone--%>

                            <%--Address--%>
                            <ext:GridPanel ID="GP_Director_Employer_Address" runat="server" Title="Employer Address" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                                <Store>
                                    <ext:Store ID="StoreDirectorEmployerAddress" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="modelDirectorEmployerAddress" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Address_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column56" runat="server" DataIndex="Address_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column57" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column58" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column67" runat="server" DataIndex="Country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column59" runat="server" DataIndex="State" Text="Province" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column60" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn10" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdAddressDirector">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--End of Address--%>

                            <%--Phone--%>
                            <ext:GridPanel ID="GP_Director_Employer_Phone" runat="server" Title="Employer Phone" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                                <Store>
                                    <ext:Store ID="StoreDirectorEmployerPhone" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="modelDirectorEmployerPhone" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="column61" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column62" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column63" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column64" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column65" runat="server" DataIndex="tph_extension" Text="Nomor Ekstensi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column66" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn11" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdPhoneDirector">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--End of Phone--%>

                            <ext:Panel ID="Panel_Director_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEmail runat="server" ID="GridEmail_Director" UniqueName="Director" FK_REF_DETAIL_OF="6" IsViewMode="true" />
                                </Content>
                            </ext:Panel>

                            <ext:Panel ID="Panel_Director_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridSanction runat="server" ID="GridSanction_Director" UniqueName="Director" FK_REF_DETAIL_OF="6" IsViewMode="true" />
                                </Content>
                            </ext:Panel>

                            <ext:Panel ID="Panel_Director_GridPEP" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridPEP runat="server" ID="GridPEP_Director" UniqueName="Director" FK_REF_DETAIL_OF="6" IsViewMode="true" />
                                </Content>
                            </ext:Panel>

                        </Items>
                        <Buttons>
                            <ext:Button ID="Button3" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirector_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>


    <%--Pop Up--%>
    <ext:Window ID="WindowDetailDirectorPhone" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Director Phone Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSetDirectorPhoneAdd" runat="server" Title="Phones" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanelDirectorPhone" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:ComboBox ID="Director_cb_phone_Contact_Type" runat="server" FieldLabel="Contact Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_Contact_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model21">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <ext:ComboBox ID="Director_cb_phone_Communication_Type" runat="server" FieldLabel="Communication Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_Communication_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model22">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>

                            <ext:TextField ID="Director_txt_phone_Country_prefix" runat="server" FieldLabel="Country Prefix" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_txt_phone_number" runat="server" FieldLabel="Phone Number" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_txt_phone_extension" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_txt_phone_comments" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnBackSaveDirectorPhoneDetail" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>

                    <ext:FormPanel ID="FormPanelEmpDirectorTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:ComboBox ID="Director_Emp_cb_phone_Contact_Type" runat="server" FieldLabel="Contact Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Emp_Store_Contact_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model23">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <ext:ComboBox ID="Director_Emp_cb_phone_Communication_Type" runat="server" FieldLabel="Communication Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Emp_Store_Communication_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model24">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>

                            <ext:TextField ID="Director_Emp_txt_phone_Country_prefix" runat="server" FieldLabel="Country Prefix" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Emp_txt_phone_number" runat="server" FieldLabel="Phone Number" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Emp_txt_phone_extension" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Emp_txt_phone_comments" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button13" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailDirectorAddress" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Director Address Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet1" runat="server" Title="Address" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FP_Address_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:ComboBox ID="Director_cmb_kategoriaddress" runat="server" FieldLabel="Address Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_KategoriAddress">
                                        <Model>
                                            <ext:Model runat="server" ID="Model25">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <ext:TextField ID="Director_Address" runat="server" FieldLabel="Address" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Town" runat="server" FieldLabel="Town" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_City" runat="server" FieldLabel="City" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Zip" runat="server" FieldLabel="Zip Code" AnchorHorizontal="90%"></ext:TextField>
                            <ext:ComboBox ID="Director_cmb_kodenegara" runat="server" FieldLabel="Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_Kode_Negara">
                                        <Model>
                                            <ext:Model runat="server" ID="Model26">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <ext:TextField ID="Director_State" runat="server" FieldLabel="Province" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Comment_Address" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Btn_Back_Director_Address" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                    <ext:FormPanel ID="FP_Address_Emp_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:ComboBox ID="Director_cmb_emp_kategoriaddress" runat="server" FieldLabel="Address Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_emp_KategoriAddress">
                                        <Model>
                                            <ext:Model runat="server" ID="Model27">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <ext:TextField ID="Director_emp_Address" runat="server" FieldLabel="Address" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_Town" runat="server" FieldLabel="Town" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_City" runat="server" FieldLabel="City" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_Zip" runat="server" FieldLabel="Zip Code" AnchorHorizontal="90%"></ext:TextField>
                            <ext:ComboBox ID="Director_cmb_emp_kodenegara" runat="server" FieldLabel="Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_emp_Kode_Negara">
                                        <Model>
                                            <ext:Model runat="server" ID="Model28">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <ext:TextField ID="Director_emp_State" runat="server" FieldLabel="Province" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_Comment_Address" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button15" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up, Add by Septian, goAML v5.0.1--%>
    <ext:Window ID="WindowDetailPreviousName" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Previous Name Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet5" runat="server" Title="Previous Name" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanelPreviousName" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txtCustomer_first_name" runat="server" FieldLabel="First Name" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txtCustomer_last_name" runat="server" FieldLabel="Last Name" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txtCustomer_comments" runat="server" FieldLabel="Comments" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_Customer_BackPreviousName" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_PreviousName_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_Email" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Email Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet6" runat="server" Title="Email" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_Email" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txtCustomer_email_address" runat="server" FieldLabel="Email Address" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_Customer_Back_Email" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_Email_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_Employment_History" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Email Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet7" runat="server" Title="Employment History" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_Employment_History" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_employer_name" runat="server" FieldLabel="Employer Name" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_employer_business" runat="server" FieldLabel="Employer Business" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_employer_identifier" runat="server" FieldLabel="Employer Identifier" AnchorHorizontal="90%"></ext:TextField>
                            <ext:DateField ID="df_valid_from" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Valid From" AnchorHorizontal="70%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_is_approx_from_date" FieldLabel="Is Approx From Date?"></ext:Checkbox>
                            <ext:DateField ID="df_valid_to" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Valid To" AnchorHorizontal="70%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_is_approx_to_date" FieldLabel="Is Approx To Date?"></ext:Checkbox>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_Customer_Back_Employment_History" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_EmploymentHistory_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_Person_PEP" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Person PEP Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet8" runat="server" Title="Person PEP" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_Person_PEP" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_Person_PEP_Country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <%-- 2023-09-27, Nael: si Label Width diilanging biar rata (Defect no 223) --%>
                                    <nds:NDSDropDownField ID="cmb_PersonPEP_Country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_function_name" runat="server" FieldLabel="Function Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:TextField ID="txt_function_description" runat="server" FieldLabel="Function Description" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                            <ext:DateField Hidden="true" ID="df_person_pep_valid_from" LabelWidth="250" runat="server" FieldLabel="Valid From" AnchorHorizontal="70%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Checkbox Hidden="true" runat="server" LabelWidth="250" ID="cbx_pep_is_approx_from_date" FieldLabel="Is Approx From Date?"></ext:Checkbox>
                            <ext:DateField Hidden="true" ID="df_pep_valid_to" LabelWidth="250" runat="server" FieldLabel="Valid To" AnchorHorizontal="70%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Checkbox Hidden="true" runat="server" LabelWidth="250" ID="cbx_pep_is_approx_to_date" FieldLabel="Is Approx To Date?"></ext:Checkbox>
                            <ext:TextField ID="txt_pep_comments" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_Customer_Back_PEP" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_PersonPEP_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_NetworkDevice" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Network Device Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet9" runat="server" Title="Network Device" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_NetworkDevice" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_device_number" runat="server" FieldLabel="Device Number" AnchorHorizontal="90%"></ext:TextField>
                            <ext:Panel ID="Panel_Network_Device_OS" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_network_device_os" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Operating_System" StringFilter="Active = 1" EmptyText="Select One" Label="Operating System" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_service_provider" runat="server" FieldLabel="Service Provider" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_ipv6" runat="server" FieldLabel="IP v6" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_ipv4" runat="server" FieldLabel="IP v4" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_cgn_port" runat="server" FieldLabel="CGN Port" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_ipv4_ipv6" runat="server" FieldLabel="IPV6 with IPV4" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_nat" runat="server" FieldLabel="NAT" AnchorHorizontal="90%"></ext:TextField>
                            <ext:DateField ID="df_first_seen_date" LabelWidth="250" runat="server" FieldLabel="First Seen Date" AnchorHorizontal="70%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:DateField ID="df_last_seen_date" LabelWidth="250" runat="server" FieldLabel="Last Seen Date" AnchorHorizontal="70%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_using_proxy" FieldLabel="Using Proxy?"></ext:Checkbox>
                            <ext:TextField ID="txt_city" runat="server" FieldLabel="City" AnchorHorizontal="90%"></ext:TextField>
                            <ext:Panel ID="Panel_NetworkDevice_Country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_network_device_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_network_device_comments" runat="server" FieldLabel="Comments" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnCustomer_Back_NetworkDevice" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_NetworkDevice_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_SocialMedia" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Social Media Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet10" runat="server" Title="Social Media" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_SocialMedia" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_socmed_platform" runat="server" FieldLabel="Platform" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_socmed_username" runat="server" FieldLabel="Username" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_socmed_comments" runat="server" FieldLabel="Comments" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnBackCustomer_SocialMedia" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_SocialMedia_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_Sanction" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Sanction Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet15" runat="server" Title="Sanction" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_Sanction" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_sanction_provider" runat="server" FieldLabel="Provider" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:TextField ID="txt_sanction_sanction_list_name" runat="server" FieldLabel="Sanction List Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:TextField ID="txt_sanction_match_criteria" runat="server" FieldLabel="Match Criteria" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                            <ext:TextField ID="txt_sanction_link_to_source" runat="server" FieldLabel="Link to Source" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:TextField ID="txt_sanction_sanction_list_attributes" runat="server" FieldLabel="Sanction List Attributes" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                            <ext:DateField Hidden="true" Editable="false" LabelWidth="250" runat="server" FieldLabel="Valid From" ID="df_sanction_valid_from" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox Hidden="true" runat="server" LabelWidth="250" ID="cbx_sanction_is_approx_from_date" FieldLabel="Is Approx From Date?"></ext:Checkbox>
                            <ext:DateField Hidden="true" Editable="false" LabelWidth="250" runat="server" FieldLabel="Valid To" ID="df_sanction_valid_to" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox Hidden="true" runat="server" LabelWidth="250" ID="cbx_sanction_is_approx_to_date" FieldLabel="Is Approx To Date?"></ext:Checkbox>
                            <ext:TextField ID="txt_sanction_comments" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button17" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_Sanction_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_RelatedPerson" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Related Person Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet11" runat="server" Title="Related Person" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_RelatedPerson" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_cmb_rp_person_relation" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <%-- 2023-10-11, Nael: 'Person Person Relation' -> 'Person Relation' --%>
                                    <nds:NDSDropDownField ID="cmb_rp_person_relation" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Person_Person_Relationship" StringFilter="Active = 1" EmptyText="Select One" Label="Person Relation" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <%-- 2023-10-14, Nael: pindahin relation comments ke bawah cmb person relation --%>
                            <ext:TextField ID="txt_rp_relation_comments" runat="server" LabelWidth="250" FieldLabel="Relation Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                            <%--<ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid From" ID="df_relation_date_range_valid_from" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_relation_date_range_is_approx_from_date" FieldLabel="Relation Date Range -  Is Approx From Date?"></ext:Checkbox>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid To" ID="df_relation_date_range_valid_to" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_relation_date_range_is_approx_to_date" FieldLabel="Relation Date Range -  Is Approx To Date?"></ext:Checkbox>--%>
                            <ext:Panel ID="Panel_cmb_rp_gender" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_rp_gender" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Jenis_Kelamin" StringFilter="Active = 1" EmptyText="Select One" Label="Gender" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:TextField LabelWidth="250" ID="txt_rp_title" runat="server" FieldLabel="Title" AnchorHorizontal="90%" MaxLength="30"></ext:TextField>
                            <ext:TextField Hidden="true" LabelWidth="250" ID="txt_rp_first_name" runat="server" FieldLabel="First Name" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                            <ext:TextField Hidden="true" LabelWidth="250" ID="txt_rp_middle_name" runat="server" FieldLabel="Middle Name" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                            <ext:TextField Hidden="true" LabelWidth="250" ID="txt_rp_prefix" runat="server" FieldLabel="Prefix" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                            <ext:TextField LabelWidth="250" ID="txt_rp_last_name" runat="server" FieldLabel="Full Name" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                            <%-- 2023-10-11, Nael: 'Birth Date' -> 'Date of Birth' --%>
                            <ext:DateField LabelWidth="250" Editable="false" runat="server" FieldLabel="Date of Birth" ID="df_birthdate" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%-- 2023-10-11, Nael: 'Birth Place' -> 'Place of Birth' --%>
                            <ext:TextField LabelWidth="250" ID="txt_rp_birth_place" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:Panel ID="Panel_cmb_rp_country_of_birth" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField LabelWidth="250" ID="cmb_rp_country_of_birth" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country Of Birth" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:TextField LabelWidth="250" ID="txt_rp_mothers_name" runat="server" FieldLabel="Mothers Name" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                            <ext:TextField LabelWidth="250" ID="txt_rp_alias" runat="server" FieldLabel="Alias" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                            <%-- <ext:TextField ID="txt_rp_full_name_frn" runat="server" FieldLabel="Full Foreign Name" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>--%>
                            <ext:TextField LabelWidth="250" ID="txt_rp_ssn" runat="server" FieldLabel="SSN" AnchorHorizontal="90%" MaxLength="16"></ext:TextField>
                            <ext:TextField LabelWidth="250" ID="txt_rp_passport_number" runat="server" FieldLabel="Passport Number" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:Panel ID="Panel_cmb_rp_passport_country" runat="server" Hidden="true" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField LabelWidth="250" ID="cmb_rp_passport_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Passport Country" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:TextField LabelWidth="250" ID="txt_rp_id_number" runat="server" FieldLabel="ID Number" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:Panel ID="Panel_cmb_rp_nationality1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_rp_nationality1" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 1" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_cmb_rp_nationality2" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_rp_nationality2" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 2" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_cmb_rp_nationality3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_rp_nationality3" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 3" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_cmb_rp_residence" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_rp_residence" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Domicile Country" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridPhone" Title="Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridPhone runat="server" ID="GridPhone_RelatedPerson" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridAddress" Title="Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridAddress runat="server" ID="GridAddress_RelatedPerson" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEmail runat="server" ID="GridEmail_RelatedPerson" FK_REF_DETAIL_OF="31" />
                                </Content>
                            </ext:Panel>
                            <%--<ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Residence Since" ID="df_rp_residence_since" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />--%>
                            <ext:TextField LabelWidth="250" ID="txt_rp_occupation" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%-- 2023-10-17, Nael: Menambahkan textfield Work Place --%>
                            <ext:TextField ID="txt_rp_employer_name" LabelWidth="250" runat="server" FieldLabel="Work Place" AnchorHorizontal="90%"></ext:TextField>
                            <ext:Panel ID="Panel_RelatedPerson_GridPhoneWork" Title="Work Place Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridPhoneWork runat="server" ID="GridPhoneWork_RelatedPerson" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridAddressWork" Title="Work Place Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridAddress runat="server" ID="GridAddressWork_RelatedPerson" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridIdentification" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridIdentification runat="server" ID="GridIdentification_RelatedPerson" />
                                </Content>
                            </ext:Panel>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_rp_deceased" FieldLabel="Has Passed Away?"></ext:Checkbox>
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Date of Passed Away" ID="df_rp_date_deceased" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:TextField LabelWidth="250" ID="txt_rp_tax_number" runat="server" FieldLabel="Tax Number" AnchorHorizontal="90%" MaxLength="16"></ext:TextField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_rp_tax_reg_number" FieldLabel="Is PEP?"></ext:Checkbox>
                            <ext:TextField LabelWidth="250" ID="txt_rp_source_of_wealth" runat="server" FieldLabel="Source Of Wealth" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_rp_is_protected" FieldLabel="Is Protected?"></ext:Checkbox>

                            <ext:Panel ID="Panel_RelatedPerson_GridPEP" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridPEP runat="server" ID="GridPEP_RelatedPerson" UniqueName="RelatedPerson" FK_REF_DETAIL_OF="31" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridSanction runat="server" ID="GridSanction_RelatedPerson" UniqueName="RelatedPerson" FK_REF_DETAIL_OF="31" />
                                </Content>

                            </ext:Panel>
                            
                            <ext:TextField ID="txt_rp_comments" runat="server" LabelWidth="250" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnBackCustomer_RelatedPerson" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_RelatedPerson_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_AdditionalInfo" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Additional Info Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet16" runat="server" Title="Additional Info" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_AdditionalInfo" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_InfoType" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_info_type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Additional_Info_Type" StringFilter="Active = 1" EmptyText="Select One" Label="Residence" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_info_subject" runat="server" FieldLabel="Info Subject" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_info_text" runat="server" FieldLabel="Info Text" AnchorHorizontal="90%"></ext:TextField>
                            <ext:NumberField runat="server" LabelWidth="200" ID="nfd_info_numeric" FieldLabel="Info Numeric" AnchorHorizontal="100%" MouseWheelEnabled="false" FormatText="#,##0.00" />
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Info Date" ID="df_info_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_info_boolean" FieldLabel="Info Boolean"></ext:Checkbox>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button19" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_AdditionalInfo_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_URL" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity URL/Website Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet12" runat="server" Title="Entity URL/Website" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_URL" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_url" runat="server" FieldLabel="URL" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnBackCustomer_URL" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_URL_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_EntityIdentification" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet13" runat="server" Title="Entity Identification" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_EntityIdentification" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_cmb_ef_type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_ef_type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Identifier_Type" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Identifier Type" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_ef_number" LabelWidth="250" runat="server" FieldLabel="Number" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Issued Date" ID="df_issue_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Expiry Date" ID="df_expiry_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_ef_issued_by" LabelWidth="250" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:Panel ID="pnl_ef_issue_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_ef_issue_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country Of Birth" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_ef_comments" LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="50"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnBackCustomer_EntityIdentification" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_EntityIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_RelatedEntities" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet14" runat="server" Title="Related Entity" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_RelatedEntities" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--5 --%>
                            <ext:Panel ID="Panel_cmb_re_entity_relation" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_re_entity_relation" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Person_Person_Relationship" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Relation" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>

                            <%-- <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid From" ID="df_re_relation_date_range_valid_from" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                          
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_re_relation_date_range_is_approx_from_date" FieldLabel="Relation Date Range -  Is Approx From Date?"></ext:Checkbox>
                         
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid To" ID="df_re_relation_date_range_valid_to" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                           
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_re_relation_date_range_is_approx_to_date" FieldLabel="Relation Date Range -  Is Approx To Date?"></ext:Checkbox>
                           
                            <ext:NumberField runat="server" LabelWidth="250" ID="nfd_re_share_percentage" FieldLabel="Share Percentage" AnchorHorizontal="70%" MouseWheelEnabled="false"  MinValue="0" EmptyNumber="0" Number="0" />--%>
                            <%--11 --%>
                            <ext:TextField ID="txt_re_relation_comments" LabelWidth="250" runat="server" FieldLabel="Relation Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                            <%--12 --%>
                            <ext:TextField ID="txt_re_name" LabelWidth="250" runat="server" FieldLabel="Corporate Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--13 --%>
                            <ext:TextField ID="txt_re_commercial_name" LabelWidth="250" runat="server" FieldLabel="Commercial Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--14 --%>
                            <ext:Panel ID="Panel_cmb_re_incorporation_legal_form" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_re_incorporation_legal_form" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Bentuk_Badan_Usaha" StringFilter="Active = 1" EmptyText="Select One" Label="Legal Form" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <%--15 --%>
                            <ext:TextField ID="txt_re_incorporation_number" LabelWidth="250" runat="server" FieldLabel="Corporate License Number" AnchorHorizontal="90%" MaxLength="50"></ext:TextField>
                            <%--16 --%>
                            <ext:TextField ID="txt_re_business" LabelWidth="250" runat="server" FieldLabel="Line of Business" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>

                            <%-- <ext:Panel ID="Panel_cmb_re_entity_status" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_re_entity_status" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Status" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Status" AnchorHorizontal="70%"  />
	                            </Content>
                            </ext:Panel>
                        
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Entity Status Date" ID="df_re_entity_status_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />--%>
                            <%--19 --%>
                            <ext:TextField ID="txt_re_incorporation_state" LabelWidth="250" runat="server" FieldLabel="Province" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--20 --%>
                            <ext:Panel ID="Panel_cmb_re_incorporation_country_code" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_re_incorporation_country_code" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <%--21 --%>
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Date of Establishment" ID="df_re_incorporation_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%--22 --%>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_re_business_closed" FieldLabel="Businesss Closed?"></ext:Checkbox>
                            <%--23 --%>
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Date Business Closed" ID="df_re_date_business_closed" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%--24 --%>
                            <ext:TextField ID="txt_re_tax_number" LabelWidth="250" runat="server" FieldLabel="Tax Number" AnchorHorizontal="90%" MaxLength="16"></ext:TextField>
                            <%--25 --%>
                            <%-- 2023-10-17, Nael: Hide tag rex number --%>
                            <ext:TextField ID="txt_re_tax_reg_number" LabelWidth="250" Hidden="true" runat="server" FieldLabel="Tax Reg Number" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>

                            <ext:Panel ID="Panel_RelatedEntity_GridPhone" Title="Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridPhone runat="server" ID="GridPhone_RelatedEntity" UniqueName="RelatedEntity"  IsViewMode="true"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridAddress" Title="Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridAddress runat="server" ID="GridAddress_RelatedEntity" UniqueName="RelatedEntity"  IsViewMode="true"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEmail runat="server" ID="GridEmail_RelatedEntity" UniqueName="RelatedEntity" IsViewMode="true" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_EntityIdentification" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEntityIdentification runat="server" ID="GridEntityIdentification_RelatedEntity" UniqueName="RelatedEntity" IsViewMode="true" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridURL" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridURL runat="server" ID="GridURL_RelatedEntity" UniqueName="RelatedEntity" IsViewMode="true" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridSanction runat="server" ID="GridSanction_RelatedEntity" UniqueName="RelatedEntity" IsViewMode="true" />
                                </Content>
                            </ext:Panel>

                            <ext:TextField ID="txt_re_comments" LabelWidth="250" runat="server" FieldLabel="Relation Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button12" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackCustomer_RelatedEntities_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--End Popup--%>

    <ext:Container ID="container" runat="server" Layout="VBoxLayout">
        <LayoutConfig>
            <ext:VBoxLayoutConfig Align="Stretch"></ext:VBoxLayoutConfig>
        </LayoutConfig>
        <Items>
            <ext:Panel ID="PanelInfo" runat="server" Collapsible="true" Title="Module Approval" BodyPadding="5">
                <Items>
                    <ext:DisplayField ID="lblModuleName" runat="server" FieldLabel="Module Name">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblModuleKey" runat="server" FieldLabel="Module Key">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblAction" runat="server" FieldLabel="Action">
                    </ext:DisplayField>
                    <ext:DisplayField ID="LblCreatedBy" runat="server" FieldLabel="Created By">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblCreatedDate" runat="server" FieldLabel="Created Date">
                    </ext:DisplayField>
                </Items>
            </ext:Panel>
            <ext:TabPanel ID="Panel1" runat="server" Layout="HBoxLayout" ButtonAlign="Center" Flex="1" AutoScroll="true" Scrollable="Vertical" Height="1500px" BodyPadding="5">
                <Items>
                    <ext:FormPanel ID="FormPanelOld" runat="server" Title="Old Value" Flex="1">
                        <Items>
                            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="CIF" ID="Old_txt_CIF" />
                            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="GCN" ID="Old_txt_GCN" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="Old_GCNPrimary" FieldLabel="GCN Primary ?" ReadOnly="true">
                            </ext:Checkbox>
                            <ext:DateField ID="Old_TextFieldOpeningDate" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Opening Date" AnchorHorizontal="70%" Format="dd-MMM-yyyy" ReadOnly="true"></ext:DateField>
                            <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Old_cmb_openingBranch" ValueField="FK_AML_BRANCH_CODE" DisplayField="BRANCH_NAME" runat="server" StringField="FK_AML_BRANCH_CODE,BRANCH_NAME" StringTable="AML_BRANCH" Label="Opening Branch Code" AnchorHorizontal="70%" AllowBlank="true" LabelWidth="250" IsReadOnly="true" />
                                    <nds:NDSDropDownField ID="Old_cmb_Status" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Status_Rekening" Label="Status" AnchorHorizontal="70%" AllowBlank="True" LabelWidth="250" IsReadOnly="true" />
                                </Content>
                            </ext:Panel>
                            <ext:DateField ID="Old_TextFieldClosingDate" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Closing Date" AnchorHorizontal="70%" Format="dd-MMM-yyyy" ReadOnly="true"></ext:DateField>

                            <%-- 2023-10-10, Nael: Hidden TextFieldComments, disuruh mba Cici --%>
                            <ext:TextField ID="Old_TextFieldComments" LabelWidth="250" AllowBlank="True" Hidden="true" runat="server" FieldLabel="Comments" AnchorHorizontal="70%"></ext:TextField>
                            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Status Rekening" ID="Old_txt_statusrekening" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="Old_UpdateFromDataSource" FieldLabel="Update From Data Source ?" ReadOnly="true">
                            </ext:Checkbox>
                            <ext:Panel runat="server" Title="Individual Customer" Collapsible="true" AnchorHorizontal="100%" ID="Old_panel_INDV" Border="true" BodyStyle="padding:10px">
                                <Items>
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Title" ID="Old_txt_INDV_Title" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Full Name" ID="Old_txt_INDV_Last_Name" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Gender" ID="Old_txt_INDV_Gender" ReadOnly="true" />
                                    <ext:GridPanel ID="Old_GP_Customer_Previous_Name" runat="server" Title="Previous Name" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="Old_Store_Customer_Previous_Name" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_Model2" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Previous_Name_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FIRST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LAST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_RowNumbererColumn13" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_Column35" runat="server" DataIndex="FIRST_NAME" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column76" runat="server" DataIndex="LAST_NAME" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column83" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_CommandColumn14" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_PagingToolbar13" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Date of Birth" ID="Old_txt_INDV_Birthdate" ReadOnly="true" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Place of Birth" ID="Old_txt_INDV_Birth_place" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Mother Maiden Name" ID="Old_txt_INDV_Mothers_name" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Alias Name" ID="Old_txt_INDV_Alias" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="NIK/SSN" ID="Old_txt_INDV_SSN" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Passport No" ID="Old_txt_INDV_Passport_number" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Passport Issued Country" ID="Old_txt_INDV_Passport_country" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Other ID Number" ID="Old_txt_INDV_ID_Number" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Nationality 1" ID="Old_txt_INDV_Nationality1" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Nationality 2" ID="Old_txt_INDV_Nationality2" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Nationality 3" ID="Old_txt_INDV_Nationality3" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Domicile Country" ID="Old_txt_INDV_Residence" />
                                    <ext:GridPanel ID="Old_gp_INDV_Phones" runat="server" Title="Phone" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_INDV_Phones" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_Model_INDV_Phones" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_RowNumber_INDV_Phones" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_coltph_contact_type" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_coltph_communication_type" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_coltph_country_prefix" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_coltph_number" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Coltph_extension" runat="server" DataIndex="tph_extension" Text="Extension" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_ColComments" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustPhone">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_PagingToolbar14" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_GP_INDVD_Address" runat="server" Hidden="true" Title="Address" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_INDVD_Address" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_Model_INDVD_Address" runat="server" IDProperty="PK_Customer_Address_ID">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Address_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Country_Code" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_Column1" runat="server" DataIndex="Address_Type" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column2" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column3" runat="server" DataIndex="Town" Text="Town" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column4" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column5" runat="server" DataIndex="Zip" Text="Zip Code" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column6" runat="server" DataIndex="Country_Code" Text="Country Code" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column7" runat="server" DataIndex="State" Text="Province" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column8" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustAddress">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_PagingToolbar1" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <ext:GridPanel ID="Old_GP_Email" runat="server" Title="Email" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_Customer_Email" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_model_email" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Email_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EMAIL_ADDRESS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_indv_email" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="Old_cc_indv_email" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_Email">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Email_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="Old_clm_indv_email_address" runat="server" DataIndex="EMAIL_ADDRESS" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
                                                
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_indv_email" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <ext:DisplayField ID="Old_display_INDV_Email" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%"></ext:DisplayField>
                                    <ext:DisplayField ID="Old_display_INDV_Email2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%"></ext:DisplayField>
                                    <ext:DisplayField ID="Old_display_INDV_Email3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%"></ext:DisplayField>
                                    <ext:DisplayField ID="Old_display_INDV_Email4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%"></ext:DisplayField>
                                    <ext:DisplayField ID="Old_display_INDV_Email5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%"></ext:DisplayField>
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Occupation" ID="Old_txt_INDV_Occupation" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Work Place" ID="Old_txt_INDV_Employer_Name" />
                                    <ext:GridPanel ID="Old_GP_Empoloyer_Phone" runat="server" Title="Work Place Phone" AutoScroll="true">

                                        <Store>
                                            <ext:Store ID="Old_Store_Empoloyer_Phone" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_Model5" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_RowNumbererColumn7" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_Column52" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column53" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column42" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column43" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column44" runat="server" DataIndex="tph_extension" Text="Extension" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column45" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_CommandColumn8" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdEmployerPhone">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_PagingToolbar8" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_GP_Employer_Address" runat="server" Title="Work Place Address" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_Employer_Address" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_Model8" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Address_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Country_Code" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_RowNumbererColumn8" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_Column54" runat="server" DataIndex="Address_Type" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column46" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column47" runat="server" DataIndex="Town" Text="Town" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column48" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column49" runat="server" DataIndex="Zip" Text="Zip Code" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column50" runat="server" DataIndex="State" Text="Provice" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column55" runat="server" DataIndex="Country_Code" Text="Country Code" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column51" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_CommandColumn9" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdEmployerAddress">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_PagingToolbar9" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <ext:GridPanel ID="Old_GP_INDVD_Identification" runat="server" Title="Identification" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_INDV_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_Model_INDV_Identification" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Issued_By" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Issue_Date" Type="Date"></ext:ModelField>
                                                            <ext:ModelField Name="Expiry_Date" Type="Date"></ext:ModelField>
                                                            <ext:ModelField Name="Issued_Country" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Identification_Comment" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_Column36" runat="server" DataIndex="Type" Text="Identification Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column34" runat="server" DataIndex="Number" Text="Identification Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:DateColumn ID="Old_Column38" runat="server" DataIndex="Issue_Date" Text="Issued Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                                <ext:DateColumn ID="Old_Column39" runat="server" DataIndex="Expiry_Date" Text="Expired Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                                <ext:Column ID="Old_Column37" runat="server" DataIndex="Issued_By" Text="Issue By" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column40" runat="server" DataIndex="Issued_Country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column41" runat="server" DataIndex="Identification_Comment" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_CommandColumn7" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustIdentification">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_PagingToolbar7" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <ext:Checkbox runat="server" LabelWidth="250" ID="Old_cb_Deceased" FieldLabel="Has Passed Away ?" AnchorHorizontal="90%" ReadOnly="true"></ext:Checkbox>
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Date of Passed Away" ID="Old_txt_Deceased_Date" AnchorHorizontal="90%" />
                                    <ext:Checkbox runat="server" LabelWidth="250" ID="Old_cb_Tax" FieldLabel="Is PEP ?" ReadOnly="true"></ext:Checkbox>
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tax Number" ID="Old_txt_INDV_Tax_Number" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Source of Wealth" ID="Old_txt_INDV_Source_of_Wealth" />
                                    <ext:Checkbox runat="server" LabelWidth="250" ID="Old_cb_INDV_Is_Protected" FieldLabel="Is Protected ?" ReadOnly="true"></ext:Checkbox>
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Notes" ID="Old_txt_INDV_Comments" AnchorHorizontal="90%" />


                                    <ext:GridPanel ID="Old_GP_Customer_Employment_History" runat="server" Title="Employment History" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="Old_Store_Customer_Employment_History" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_employment_history" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Previous_Name_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYER_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYER_BUSINESS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYER_IDENTIFIER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYMENT_PERIOD_VALID_FROM" Type="Date"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE" Type="Boolean"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYMENT_PERIOD_VALID_TO" Type="Date"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE" Type="Boolean"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_employment_history" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_clm_first_name" runat="server" DataIndex="EMPLOYER_NAME" Text="Employer Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_add_info_last_name" runat="server" DataIndex="EMPLOYER_BUSINESS" Text="Employer Business" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_comments" runat="server" DataIndex="EMPLOYER_IDENTIFIER" Text="Employer Identifier" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_valid_from" runat="server" DataIndex="EMPLOYMENT_PERIOD_VALID_FROM" Text="Employment Period - Valid From" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_is_approx_from_date" runat="server" DataIndex="EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE" Text="Employment Period - Is Approx From Date?" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_valid_to" runat="server" DataIndex="EMPLOYMENT_PERIOD_VALID_TO" Text="Employment Period - Valid To" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_xlm_is_approx_to_date" runat="server" DataIndex="EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE" Text="Employment Period - Is Approx To Date?" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_cc_add_info_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_employment_history" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>


                                    <ext:GridPanel ID="Old_GP_Customer_Person_PEP" runat="server" Title="Person PEP" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_Customer_Person_PEP" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_person_pep" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_PEP_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PEP_COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FUNCTION_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FUNCTION_DESCRIPTION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PEP_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PEP_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PEP_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PEP_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_person_pep" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="Old_cmd_person_pep_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_PEP">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_PEP_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="Old_clm_person_pep_first_name" runat="server" DataIndex="PEP_COUNTRY" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_person_pep_function_name" runat="server" DataIndex="FUNCTION_NAME" Text="Function Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_person_pep_description" runat="server" DataIndex="FUNCTION_DESCRIPTION" Text="Description" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_person_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_add_info" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_GP_Network_Device" runat="server" Title="Network Device" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="Old_Store_Customer_Network_Device" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_network_device" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Previous_Name_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DEVICE_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="OPERATING_SYSTEM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SERVICE_PROVIDER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IPV6" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IPV4" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CGN_PORT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IPV4_IPV6" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NAT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FIRST_SEEN_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LAST_SEEN_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="USING_PROXY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_network_device" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_clm_device_number" runat="server" DataIndex="DEVICE_NUMBER" Text="Device Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_operating_system" runat="server" DataIndex="OPERATING_SYSTEM" Text="Operating System" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_service_provider" runat="server" DataIndex="SERVICE_PROVIDER" Text="Service Provider" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_ip_v6" runat="server" DataIndex="IPV6" Text="Ip V6" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_ip_v4" runat="server" DataIndex="IPV4" Text="Ip V4" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_cgn_port" runat="server" DataIndex="CGN_PORT" Text="CGN Port" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_ip_v6_with_ip_v4" runat="server" DataIndex="IPV4_IPV6" Text="Ip V6 with Ip V4" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_nat" runat="server" DataIndex="NAT" Text="NAT" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_first_seen_date" runat="server" DataIndex="FIRST_SEEN_DATE" Text="First Seen Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_last_seen_date" runat="server" DataIndex="LAST_SEEN_DATE" Text="Last Seen Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_using_proxy" runat="server" DataIndex="USING_PROXY" Text="Using Proxy" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_city" runat="server" DataIndex="CITY" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_country" runat="server" DataIndex="COUNTRY" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_net_device_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_cc_net_device_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_net_device" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_GP_Customer_Social_Media" runat="server" Title="Social Media" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="Old_Store_Customer_Social_Media" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_soc_med" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Previous_Name_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PLATFORM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="USER_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_soc_med" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_clm_soc_med_first_name" runat="server" DataIndex="PLATFORM" Text="Platform" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_soc_med_last_name" runat="server" DataIndex="USER_NAME" Text="Username" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_soc_med_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_cc_soc_med_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_soc_med" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_GP_Customer_Sanction" runat="server" Title="Sanction" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_Customer_Sanction" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_sanction" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Sanction_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PROVIDER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MATCH_CRITERIA" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LINK_TO_SOURCE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_ATTRIBUTES" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_sanction" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="Old_cc_customer_sanction_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_Sanction">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="Old_clm_provider" runat="server" DataIndex="PROVIDER" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_sanction_list_name" runat="server" DataIndex="SANCTION_LIST_NAME" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_match_criteria" runat="server" DataIndex="MATCH_CRITERIA" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_link_to_source" runat="server" DataIndex="LINK_TO_SOURCE" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_sanction_attributes" runat="server" DataIndex="SANCTION_LIST_ATTRIBUTES" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_sanction_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_sanction" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_GP_RelatedPerson"
                                        runat="server" Title="Related Person" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_Customer_RelatedPerson" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_related_person" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Related_Person_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PERSON_PERSON_RELATION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="GENDER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TITLE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FIRST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MIDDLE_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PREFIX" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LAST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BIRTH_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BIRTH_PLACE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COUNTRY_OF_BIRTH" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MOTHERS_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ALIAS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PASSPORT_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PASSPORT_COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ID_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NATIONALITY1" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NATIONALITY2" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NATIONALITY3" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RESIDENCE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="OCCUPATION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DECEASED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DATE_DECEASED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SOURCE_OF_WEALTH" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IS_PROTECTED" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_related_person" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="Old_cc_rp_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_RelatedPerson">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <%-- 2023-10-11, Nael: 'Person Person Relation' -> 'Person Relation' --%>
                                                <ext:Column ID="Old_clm_rp_person_relation" runat="server" DataIndex="PERSON_PERSON_RELATION" Text="Person Relation" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="Old_clm_rp_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_gender" runat="server" DataIndex="GENDER" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_title" runat="server" DataIndex="TITLE" Text="Title" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_first_name" runat="server" DataIndex="FIRST_NAME" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_middle_name" runat="server" DataIndex="MIDDLE_NAME" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_prefix" runat="server" DataIndex="PREFIX" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="Old_clm_rp_last_name" runat="server" DataIndex="LAST_NAME" Text="Full Name" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="Old_clm_rp_birth_date" runat="server" DataIndex="BIRTH_DATE" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_birth_place" runat="server" DataIndex="BIRTH_PLACE" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_country_of_birth" runat="server" DataIndex="COUNTRY_OF_BIRTH" Text="Country of Birth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_mother_name" runat="server" DataIndex="MOTHERS_NAME" Text="Mother name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_alias" runat="server" DataIndex="ALIAS" Text="Alias" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_ssn" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_passport_number" runat="server" DataIndex="PASSPORT_NUMBER" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_passport_country" runat="server" DataIndex="PASSPORT_COUNTRY" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_id_number" runat="server" DataIndex="ID_NUMBER" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="Old_clm_rp_nationality1" runat="server" DataIndex="NATIONALITY1" Text="Nationality 1" MinWidth="130" Flex="1"></ext:Column>
                                               <%-- <ext:Column ID="Old_clm_rp_nationality2" runat="server" DataIndex="NATIONALITY2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_nationality3" runat="server" DataIndex="NATIONALITY3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_residence" runat="server" DataIndex="RESIDENCE" Text="Residence" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_occupation" runat="server" DataIndex="OCCUPATION" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_deceased" runat="server" DataIndex="DECEASED" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_date_deceased" runat="server" DataIndex="DATE_DECEASED" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_tax_number" runat="server" DataIndex="TAX_NUMBER" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_is_pep" runat="server" DataIndex="TAX_REG_NUMBER" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_source_of_wealth" runat="server" DataIndex="SOURCE_OF_WEALTH" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_rp_is_protected" runat="server" DataIndex="IS_PROTECTED" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>
                                                --%>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_related_person" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_GP_Customer_Additonal_Info" runat="server" Title="Additional Information" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="Old_Store_Customer_Additional_Info" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_additional_info" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Additional_Information_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_TYPE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_SUBJECT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_TEXT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_NUMERIC" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_BOOLEAN" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_additional_info" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_clm_info_type" runat="server" DataIndex="INFO_TYPE" Text="Info Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_info_subject" runat="server" DataIndex="INFO_SUBJECT" Text="Info Subject" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_info_text" runat="server" DataIndex="INFO_TEXT" Text="Info Text" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_info_numeric" runat="server" DataIndex="INFO_NUMERIC" Text="Info Numeric" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_info_date" runat="server" DataIndex="INFO_DATE" Text="Info Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_info_boolean" runat="server" DataIndex="INFO_BOOLEAN" Text="Info Boolean" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_clm_action_add_info" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_additional_info" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                </Items>
                            </ext:Panel>

                            <ext:Panel runat="server" Title="Corporate Customer" Collapsible="true" AnchorHorizontal="100%" ID="Old_panel_Corp" Border="true" BodyStyle="padding:10px">
                                <Items>
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Corporate Name" ID="Old_txt_Corp_Name" PaddingSpec="0 10 0 0" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Commercial Name" ID="Old_txt_Corp_Commercial_Name" />

                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Legal Form" ID="Old_txt_Corp_Incorporation_Legal_Form" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Corporae License Number" ID="Old_txt_Corp_Incorporation_number" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Line of Business" ID="Old_txt_Corp_Business" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Email Korporasi" ID="Old_txt_Corp_Email" AnchorHorizontal="90%" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Website Korporasi" ID="Old_txt_Corp_url" AnchorHorizontal="90%" />
                                    <ext:Panel ID="Old_Pnl_cmb_Corp_Entity_Status" Hidden="true" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                        <Content>
                                            <nds:NDSDropDownField ID="Old_cmb_Corp_Entity_Status" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Status" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Status" AnchorHorizontal="70%" IsReadOnly="true" />
                                        </Content>
                                    </ext:Panel>
                                    <ext:DisplayField runat="server" LabelWidth="250" Hidden="true" FieldLabel="Entity Status Date" ID="Old_txt_Corp_Entity_Status_Date" />
                                    <ext:GridPanel ID="Old_GP_Phone_Corp" runat="server" Hidden="true" Title="Phone" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_Phone_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_Model_Phone_Corp" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_Column9" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column10" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column11" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column12" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column13" runat="server" DataIndex="tph_extension" Text="Extension" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column14" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustPhone">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_PagingToolbar2" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <ext:GridPanel ID="Old_GP_Address_Corp" runat="server" Hidden="true" Title="Address" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_Address_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_Model_Address_Corp" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Address_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_Column15" runat="server" DataIndex="Address_Type" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column16" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column17" runat="server" DataIndex="Town" Text="Town" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column18" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column19" runat="server" DataIndex="Zip" Text="Zip Code" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column20" runat="server" DataIndex="Country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column21" runat="server" DataIndex="State" Text="Province" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column22" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_CommandColumn4" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustAddress">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_PagingToolbar3" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <ext:GridPanel ID="Old_GP_corp_email" runat="server" Title="Email" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_corp_email" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_corp_email" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Email_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EMAIL_ADDRESS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rcn_cor_email" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="Old_CommandColumn15" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_Email">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Email_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="Old_clm_corp_email_address" runat="server" DataIndex="EMAIL_ADDRESS" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_corp_email" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_GP_Customer_Entity_URL" runat="server" Title="Entity URL" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_Customer_Entity_URL" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_entity_url" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_URL_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="URL" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_entity_url" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="Old_cc_entity_url" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_URL">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_URL_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="Old_clm_url_type" runat="server" DataIndex="URL" Text="URL" MinWidth="130" Flex="1"></ext:Column>

                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_entity_url" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Province" ID="Old_txt_Corp_incorporation_state" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Country" ID="Old_txt_Corp_Incorporation_Country_Code" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Date of Established" ID="Old_txt_Corp_incorporation_date" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Is Closed?" ID="Old_cbTutup" Selectable="false"></ext:DisplayField>
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Closing Date" ID="Old_txt_Corp_date_business_closed" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tax Number" ID="Old_txt_Corp_tax_number" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Notes" ID="Old_txt_Corp_Comments" />

                                    <ext:GridPanel ID="Old_GP_Customer_Entity_Identification" runat="server" Title="Entity Identification" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_Customer_Entity_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_ent_identify" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Entity_Identification_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TYPE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ISSUE_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EXPIRY_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ISSUED_BY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ISSUE_COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_identify" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="Old_cc_identify" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_EntityIdentification">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Identification_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="Old_clm_identify_type" runat="server" DataIndex="TYPE" Text="Entity Identifier Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_identify_number" runat="server" DataIndex="NUMBER" Text="Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_identify_issued_date" runat="server" DataIndex="ISSUE_DATE" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_identify_expiry_date" runat="server" DataIndex="EXPIRY_DATE" Text="Expiry Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_identify_issued_by" runat="server" DataIndex="ISSUED_BY" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_identify_issued_country" runat="server" DataIndex="ISSUE_COUNTRY" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_identify_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_identify" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_gp_Director" runat="server" Title="Director" AutoScroll="true" Border="true" Hidden="true" BodyStyle="padding:10px">
                                        <Store>
                                            <ext:Store ID="Old_store_Director" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_model_Director" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Entity_Director_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Role" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_RowNumber_Director" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_columnDirectorName" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_ColumnDirectorRole" runat="server" DataIndex="Role" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_cmdDirectory" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdDirector">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_PagingToolbar4" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_GPCorp_Network_Device" runat="server" Title="Network Device" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="Old_StoreDirector_Network_Device" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_director_network_device" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Network_Device_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DEVICE_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="OPERATING_SYSTEM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SERVICE_PROVIDER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IPV6" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IPV4" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CGN_PORT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IPV4_IPV6" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NAT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FIRST_SEEN_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LAST_SEEN_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="USING_PROXY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rcn_director_network_device" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_clm_director_device_number" runat="server" DataIndex="DEVICE_NUMBER" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_director_operating_system" runat="server" DataIndex="OPERATING_SYSTEM" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_director_service_provider" runat="server" DataIndex="SERVICE_PROVIDER" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_director_ip_v6" runat="server" DataIndex="IPV6" Text="IP V6" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_director_ip_v4" runat="server" DataIndex="IPV4" Text="IP V4" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_director_cgn_port" runat="server" DataIndex="CGN_PORT" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_director_ip_v6_with_ip_v4" runat="server" DataIndex="IPV4_IPV6" Text="IPV6 WITH IPV4" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_director_nat" runat="server" DataIndex="NAT" Text="NAT" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_director_first_seen_date" runat="server" DataIndex="FIRST_SEEN_DATE" Text="First Seen Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_director_last_seen_date" runat="server" DataIndex="LAST_SEEN_DATE" Text="Last Seen Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_director_using_proxy" runat="server" DataIndex="USING_PROXY" Text="Using Proxy" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_director_city" runat="server" DataIndex="CITY" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_director_country" runat="server" DataIndex="COUNTRY" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_director_comment" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_cc_director_network_device" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_network_device" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_GP_Corp_Sanction" runat="server" Title="Sanction" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_corp_sanction" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_corp_sanction" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Sanction_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PROVIDER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MATCH_CRITERIA" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LINK_TO_SOURCE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_ATTRIBUTES" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_corp_sanction" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="Old_cc_corp_sanction_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_Sanction">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="Old_clm_corp_sanction" runat="server" DataIndex="PROVIDER" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_sanction_list_name" runat="server" DataIndex="SANCTION_LIST_NAME" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_match_criteria" runat="server" DataIndex="MATCH_CRITERIA" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_link_to_source" runat="server" DataIndex="LINK_TO_SOURCE" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_sanction_list_attributes" runat="server" DataIndex="SANCTION_LIST_ATTRIBUTES" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                                                <%-- START: 2023-10-10, Nael hide kolom validFrom validTo dan approx From/To Date --%>
                                                <ext:Column ID="Old_clm_corp_valid_from" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_VALID_FROM" Text="Valid From" Hidden="true" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_is_approx_from_date" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE" Hidden="true" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_is_valid_to" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_VALID_TO" Text="Valid To" Hidden="true" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_is_approx_to_date" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE" Hidden="true" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>
                                                <%-- END: Edit Hide column --%>
                                                <ext:Column ID="Old_clm_corp_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>

                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_corp_sanction" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_GP_CORP_Related_Person" Hidden="true"    
                                        runat="server" Title="Related Person" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_StoreCorp_Related_person" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_corp_related_person" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Related_Person_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PERSON_PERSON_RELATION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RELATION_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RELATION_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RELATION_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RELATION_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="GENDER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TITLE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FIRST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MIDDLE_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PREFIX" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LAST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BIRTHDATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BIRTH_PLACE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COUNTRY_OF_BIRTH" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MOTHER_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ALIAS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FULL_NAME_FRN" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PASSPORT_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PASSPORT_COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ID_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NATIONALITY1" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NATIONALITY2" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NATIONALITY3" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RESIDENCE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RESIDENCE_SINCE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="OCCUPATION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DECEASED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DATE_DECEASED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SOURCE_OF_WEALTH" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IS_PROTECTED" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_corp_related_person" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="Old_CommandColumn16" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_RelatedPerson">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <%-- 2023-10-11, Nael: 'Person Person Relation' -> 'Person Relation' --%>
                                                <ext:Column ID="Old_clm_corp_rp_person_relation" runat="server" DataIndex="PERSON_PERSON_RELATION" Text="Person Relation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_valid_from" runat="server" DataIndex="RELATION_DATE_RANGE_VALID_FROM" Text="Relation Date Range -  Valid From" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_is_approx_from_date" runat="server" DataIndex="RELATION_DATE_RANGE_IS_APPROX_FROM_DATE" Text="Relation Date Range -  Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_valid_to" runat="server" DataIndex="RELATION_DATE_RANGE_VALID_TO" Text="Relation Date Range -  Valid To" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_is_approx_to_date" runat="server" DataIndex="RELATION_DATE_RANGE_IS_APPROX_TO_DATE" Text="Relation Date Range -  Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_gender" runat="server" DataIndex="GENDER" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_title" runat="server" DataIndex="TITLE" Text="Title" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_first_name" runat="server" DataIndex="FIRST_NAME" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_middle_name" runat="server" DataIndex="MIDDLE_NAME" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_prefix" runat="server" DataIndex="PREFIX" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_last_name" runat="server" DataIndex="LAST_NAME" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_birth_date" runat="server" DataIndex="BIRTHDATE" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_birth_place" runat="server" DataIndex="BIRTH_PLACE" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_country_of_birth" runat="server" DataIndex="COUNTRY_OF_BIRTH" Text="Country of Birth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_mother_name" runat="server" DataIndex="MOTHER_NAME" Text="Mother name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_alias" runat="server" DataIndex="ALIAS" Text="Alias" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_foreign_name" runat="server" DataIndex="FULL_NAME_FRN" Text="Full Foreign Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_ssn" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_passport_number" runat="server" DataIndex="PASSPORT_NUMBER" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_passport_country" runat="server" DataIndex="PASSPORT_COUNTRY" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_id_number" runat="server" DataIndex="ID_NUMBER" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_nationality1" runat="server" DataIndex="NATIONALITY1" Text="Nationality 1" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_nationality2" runat="server" DataIndex="NATIONALITY2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_nationality3" runat="server" DataIndex="NATIONALITY3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_residence" runat="server" DataIndex="RESIDENCE" Text="Residence" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_residence_since" runat="server" DataIndex="RESIDENCE_SINCE" Text="Residence Since" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_occupation" runat="server" DataIndex="OCCUPATION" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_deceased" runat="server" DataIndex="DECEASED" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_date_deceased" runat="server" DataIndex="DATE_DECEASED" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_tax_number" runat="server" DataIndex="TAX_NUMBER" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_is_pep" runat="server" DataIndex="TAX_REG_NUMBER" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_source_of_wealth" runat="server" DataIndex="SOURCE_OF_WEALTH" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_rp_is_protected" runat="server" DataIndex="IS_PROTECTED" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>

                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_corp_related_person" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_GP_CORP_Related_Entities" runat="server" Title="Related Entties" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Old_Store_Related_Entities" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_related_entities" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Related_Entities_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ENTITY_ENTITY_RELATION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMERCIAL_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_LEGAL_FORM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BUSINESS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_STATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_COUNTRY_CODE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DATE_BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_related_etities" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="Old_cc_related_entities" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_RelatedEntities">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Entities_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="Old_clm_re_email_address" runat="server" DataIndex="ENTITY_ENTITY_RELATION" Text="Entity Relation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column89" runat="server" Hidden="true" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column90" runat="server" DataIndex="NAME" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column91" runat="server" Hidden="true" DataIndex="COMMERCIAL_NAME" Text="Commercial Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column92" runat="server" DataIndex="INCORPORATION_LEGAL_FORM" Text="Incorporation Legal Form" Hidden="true" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column93" runat="server" Hidden="true" DataIndex="INCORPORATION_NUMBER" Text="Incorporation Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column94" runat="server" Hidden="true" DataIndex="BUSINESS" Text="Business" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column97" runat="server" Hidden="true" DataIndex="INCORPORATION_STATE" Text="Incorporation State" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column98" runat="server" DataIndex="INCORPORATION_COUNTRY_CODE" Text="Incorporation Country Code" Hidden="true" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column99" runat="server" Hidden="true" DataIndex="INCORPORATION_DATE" Text="Incorporation Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column100" runat="server" Hidden="true" DataIndex="BUSINESS_CLOSED" Text="Businesss Closed" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column101" runat="server" Hidden="true" DataIndex="DATE_BUSINESS_CLOSED" Text="Date Business Closed"  MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column102" runat="server" Hidden="true" DataIndex="TAX_NUMBER" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_Column103" runat="server" Hidden="true" DataIndex="TAX_REG_NUMBER" Text="Tax Reg Number" MinWidth="130" Flex="1"></ext:Column>

                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_related_entities" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="Old_GP_Customer_Corp_Additonal_Info" runat="server" Title="Additional Information" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="Old_Store_Customer_Corp_Additional_Info" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Old_mdl_corp_additional_info" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Additional_Information_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_TYPE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_SUBJECT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_TEXT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_NUMERIC" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_BOOLEAN" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="Old_rnc_corp_additional_info" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Old_clm_corp_info_type" runat="server" DataIndex="INFO_TYPE" Text="Info Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_info_subject" runat="server" DataIndex="INFO_SUBJECT" Text="Info Subject" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_info_text" runat="server" DataIndex="INFO_TEXT" Text="Info Text" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_info_numeric" runat="server" DataIndex="INFO_NUMERIC" Text="Info Numeric" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_info_date" runat="server" DataIndex="INFO_DATE" Text="Info Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Old_clm_corp_info_boolean" runat="server" DataIndex="INFO_BOOLEAN" Text="Info Boolean" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="Old_clm_corp_action_add_info" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="old" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="Old_pt_corp_additional_info" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                </Items>
                            </ext:Panel>

                        </Items>
                    </ext:FormPanel>

                    <ext:FormPanel ID="FormPanelNew" runat="server" Title="New Value" Flex="1">
                        <Items>
                            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="CIF" ID="New_txt_CIF" />
                            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="GCN" ID="New_txt_GCN" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="New_GCNPrimary" FieldLabel="GCN Primary ?" ReadOnly="true">
                            </ext:Checkbox>
                            <ext:DateField ID="New_TextFieldOpeningDate" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Opening Date" AnchorHorizontal="70%" Format="dd-MMM-yyyy" ReadOnly="true"></ext:DateField>
                            <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="New_cmb_openingBranch" ValueField="FK_AML_BRANCH_CODE" DisplayField="BRANCH_NAME" runat="server" StringField="FK_AML_BRANCH_CODE,BRANCH_NAME" StringTable="AML_BRANCH" Label="Opening Branch Code" AnchorHorizontal="70%" AllowBlank="true" LabelWidth="250" IsReadOnly="true" />
                                    <nds:NDSDropDownField ID="New_cmb_Status" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Status_Rekening" Label="Status" AnchorHorizontal="70%" AllowBlank="True" LabelWidth="250" IsReadOnly="true" />
                                </Content>
                            </ext:Panel>
                            <ext:DateField ID="New_TextFieldClosingDate" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Closing Date" AnchorHorizontal="70%" Format="dd-MMM-yyyy" ReadOnly="true"></ext:DateField>

                            <ext:TextField ID="New_TextFieldComments" LabelWidth="250" AllowBlank="True" runat="server" FieldLabel="Comments" AnchorHorizontal="70%"></ext:TextField>
                            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Status Rekening" ID="New_txt_statusrekening" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="New_UpdateFromDataSource" FieldLabel="Update From Data Source ?" ReadOnly="true">
                            </ext:Checkbox>

                            <ext:Panel runat="server" Title="Individual Customer" Collapsible="true" AnchorHorizontal="100%" ID="New_panel_INDV" Border="true" BodyStyle="padding:10px">
                                <Items>
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Title" ID="New_txt_INDV_Title" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Full Name" ID="New_txt_INDV_Last_Name" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Gender" ID="New_txt_INDV_Gender" ReadOnly="true" />
                                    <ext:GridPanel ID="New_GP_Customer_Previous_Name" runat="server" Title="Previous Name" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="New_Store_Customer_Previous_Name" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_Model2" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Previous_Name_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FIRST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LAST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_RowNumbererColumn13" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_Column35" runat="server" DataIndex="FIRST_NAME" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column76" runat="server" DataIndex="LAST_NAME" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column83" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_CommandColumn14" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_PagingToolbar13" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Date of Birth" ID="New_txt_INDV_Birthdate" ReadOnly="true" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Place of Birth" ID="New_txt_INDV_Birth_place" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Mother Maiden Name" ID="New_txt_INDV_Mothers_name" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Alias Name" ID="New_txt_INDV_Alias" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="NIK/SSN" ID="New_txt_INDV_SSN" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Passport No" ID="New_txt_INDV_Passport_number" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Passport Issued Country" ID="New_txt_INDV_Passport_country" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Other ID Number" ID="New_txt_INDV_ID_Number" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Nationality 1" ID="New_txt_INDV_Nationality1" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Nationality 2" ID="New_txt_INDV_Nationality2" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Nationality 3" ID="New_txt_INDV_Nationality3" />
                                    <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Domicile Country" ID="New_txt_INDV_Residence" />
                                    <ext:GridPanel ID="New_gp_INDV_Phones" runat="server" Title="Phone" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_INDV_Phones" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_Model_INDV_Phones" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_RowNumber_INDV_Phones" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_coltph_contact_type" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_coltph_communication_type" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_coltph_country_prefix" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_coltph_number" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Coltph_extension" runat="server" DataIndex="tph_extension" Text="Extension" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_ColComments" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustPhone">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_PagingToolbar14" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_GP_INDVD_Address" runat="server" Hidden="true" Title="Address" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_INDVD_Address" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_Model_INDVD_Address" runat="server" IDProperty="PK_Customer_Address_ID">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Address_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Country_Code" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="New_CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustAddress">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="New_Column1" runat="server" DataIndex="Address_Type" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column2" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column3" runat="server" DataIndex="Town" Text="Town" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column4" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column5" runat="server" DataIndex="Zip" Text="Zip Code" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column6" runat="server" DataIndex="Country_Code" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column7" runat="server" DataIndex="State" Text="Province" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column8" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_PagingToolbar1" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <ext:GridPanel ID="New_GP_Email" runat="server" Title="Email" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_Customer_Email" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_model_email" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Email_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EMAIL_ADDRESS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_indv_email" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="New_cc_indv_email" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_Email">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Email_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="New_clm_indv_email_address" runat="server" DataIndex="EMAIL_ADDRESS" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
                                                
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_indv_email" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- 2023-10-11, Nael Membuka kembali txt_INDV_Email untuk goAML 5.2 --%>
                                    <ext:DisplayField ID="New_display_INDV_Email" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%"></ext:DisplayField>
                                    <ext:DisplayField ID="New_display_INDV_Email2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%"></ext:DisplayField>
                                    <ext:DisplayField ID="New_display_INDV_Email3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%"></ext:DisplayField>
                                    <ext:DisplayField ID="New_display_INDV_Email4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%"></ext:DisplayField>
                                    <ext:DisplayField ID="New_display_INDV_Email5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%"></ext:DisplayField>
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Occupation" ID="New_txt_INDV_Occupation" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Work Place" ID="New_txt_INDV_Employer_Name" />
                                    <ext:GridPanel ID="New_GP_Empoloyer_Phone" runat="server" Title="Work Place Phone" AutoScroll="true">

                                        <Store>
                                            <ext:Store ID="New_Store_Empoloyer_Phone" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_Model5" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_RowNumbererColumn7" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_Column52" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column53" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column42" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column43" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column44" runat="server" DataIndex="tph_extension" Text="Extension" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column45" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_CommandColumn8" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdEmployerPhone">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_PagingToolbar8" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_GP_Employer_Address" runat="server" Title="Work Place Address" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_Employer_Address" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_Model8" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Address_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Country_Code" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_RowNumbererColumn8" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_Column54" runat="server" DataIndex="Address_Type" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column46" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column47" runat="server" DataIndex="Town" Text="Town" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column48" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column49" runat="server" DataIndex="Zip" Text="Zip Code" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column50" runat="server" DataIndex="State" Text="Provice" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column55" runat="server" DataIndex="Country_Code" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column51" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_CommandColumn9" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdEmployerAddress">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_PagingToolbar9" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <ext:GridPanel ID="New_GP_INDVD_Identification" runat="server" Title="Identification" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_INDV_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_Model_INDV_Identification" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Issued_By" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Issue_Date" Type="Date"></ext:ModelField>
                                                            <ext:ModelField Name="Expiry_Date" Type="Date"></ext:ModelField>
                                                            <ext:ModelField Name="Issued_Country" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Identification_Comment" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_Column36" runat="server" DataIndex="Type" Text="Identification Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column34" runat="server" DataIndex="Number" Text="Identification Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:DateColumn ID="New_Column38" runat="server" DataIndex="Issue_Date" Text="Issued Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                                <ext:DateColumn ID="New_Column39" runat="server" DataIndex="Expiry_Date" Text="Expired Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                                <ext:Column ID="New_Column37" runat="server" DataIndex="Issued_By" Text="Issue By" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column40" runat="server" DataIndex="Issued_Country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column41" runat="server" DataIndex="Identification_Comment" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_CommandColumn7" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustIdentification">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_PagingToolbar7" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <ext:Checkbox runat="server" LabelWidth="250" ID="New_cb_Deceased" FieldLabel="Has Passed Away ?" AnchorHorizontal="90%" ReadOnly="true"></ext:Checkbox>
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Date of Passed Away" ID="New_txt_Deceased_Date" AnchorHorizontal="90%" />
                                    <ext:Checkbox runat="server" LabelWidth="250" ID="New_cb_Tax" FieldLabel="Is PEP ?" ReadOnly="true"></ext:Checkbox>
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tax Number" ID="New_txt_INDV_Tax_Number" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Source of Wealth" ID="New_txt_INDV_Source_of_Wealth" />
                                    <ext:Checkbox runat="server" LabelWidth="250" ID="New_cb_INDV_Is_Protected" FieldLabel="Is Protected ?" ReadOnly="true"></ext:Checkbox>
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Notes" ID="New_txt_INDV_Comments" AnchorHorizontal="90%" />


                                    <ext:GridPanel ID="New_GP_Customer_Employment_History" runat="server" Title="Employment History" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="New_Store_Customer_Employment_History" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_employment_history" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Previous_Name_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYER_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYER_BUSINESS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYER_IDENTIFIER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYMENT_PERIOD_VALID_FROM" Type="Date"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE" Type="Boolean"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYMENT_PERIOD_VALID_TO" Type="Date"></ext:ModelField>
                                                            <ext:ModelField Name="EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE" Type="Boolean"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_employment_history" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_clm_first_name" runat="server" DataIndex="EMPLOYER_NAME" Text="Employer Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_add_info_last_name" runat="server" DataIndex="EMPLOYER_BUSINESS" Text="Employer Business" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_comments" runat="server" DataIndex="EMPLOYER_IDENTIFIER" Text="Employer Identifier" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_valid_from" runat="server" DataIndex="EMPLOYMENT_PERIOD_VALID_FROM" Text="Employment Period - Valid From" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_is_approx_from_date" runat="server" DataIndex="EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE" Text="Employment Period - Is Approx From Date?" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_valid_to" runat="server" DataIndex="EMPLOYMENT_PERIOD_VALID_TO" Text="Employment Period - Valid To" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_xlm_is_approx_to_date" runat="server" DataIndex="EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE" Text="Employment Period - Is Approx To Date?" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_cc_add_info_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_employment_history" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>


                                    <ext:GridPanel ID="New_GP_Customer_Person_PEP" runat="server" Title="Person PEP" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_Customer_Person_PEP" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_person_pep" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_PEP_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PEP_COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FUNCTION_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FUNCTION_DESCRIPTION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PEP_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PEP_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PEP_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PEP_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_person_pep" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="New_cmd_person_pep_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_PEP">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_PEP_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="New_clm_person_pep_first_name" runat="server" DataIndex="PEP_COUNTRY" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_person_pep_function_name" runat="server" DataIndex="FUNCTION_NAME" Text="Function Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_person_pep_description" runat="server" DataIndex="FUNCTION_DESCRIPTION" Text="Description" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_person_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_add_info" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_GP_Network_Device" runat="server" Title="Network Device" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="New_Store_Customer_Network_Device" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_network_device" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Previous_Name_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DEVICE_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="OPERATING_SYSTEM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SERVICE_PROVIDER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IPV6" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IPV4" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CGN_PORT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IPV4_IPV6" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NAT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FIRST_SEEN_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LAST_SEEN_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="USING_PROXY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_network_device" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_clm_device_number" runat="server" DataIndex="DEVICE_NUMBER" Text="Device Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_operating_system" runat="server" DataIndex="OPERATING_SYSTEM" Text="Operating System" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_service_provider" runat="server" DataIndex="SERVICE_PROVIDER" Text="Service Provider" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_ip_v6" runat="server" DataIndex="IPV6" Text="Ip V6" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_ip_v4" runat="server" DataIndex="IPV4" Text="Ip V4" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_cgn_port" runat="server" DataIndex="CGN_PORT" Text="CGN Port" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_ip_v6_with_ip_v4" runat="server" DataIndex="IPV4_IPV6" Text="Ip V6 with Ip V4" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_nat" runat="server" DataIndex="NAT" Text="NAT" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_first_seen_date" runat="server" DataIndex="FIRST_SEEN_DATE" Text="First Seen Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_last_seen_date" runat="server" DataIndex="LAST_SEEN_DATE" Text="Last Seen Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_using_proxy" runat="server" DataIndex="USING_PROXY" Text="Using Proxy" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_city" runat="server" DataIndex="CITY" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_country" runat="server" DataIndex="COUNTRY" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_net_device_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_cc_net_device_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_net_device" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_GP_Customer_Social_Media" runat="server" Title="Social Media" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="New_Store_Customer_Social_Media" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_soc_med" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Previous_Name_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PLATFORM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="USER_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_soc_med" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_clm_soc_med_first_name" runat="server" DataIndex="PLATFORM" Text="Platform" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_soc_med_last_name" runat="server" DataIndex="USER_NAME" Text="Username" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_soc_med_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_cc_soc_med_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_soc_med" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_GP_Customer_Sanction" runat="server" Title="Sanction" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_Customer_Sanction" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_sanction" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Sanction_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PROVIDER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MATCH_CRITERIA" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LINK_TO_SOURCE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_ATTRIBUTES" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_sanction" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="New_cc_customer_sanction_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_Sanction">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="New_clm_provider" runat="server" DataIndex="PROVIDER" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_sanction_list_name" runat="server" DataIndex="SANCTION_LIST_NAME" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_match_criteria" runat="server" DataIndex="MATCH_CRITERIA" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_link_to_source" runat="server" DataIndex="LINK_TO_SOURCE" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_sanction_attributes" runat="server" DataIndex="SANCTION_LIST_ATTRIBUTES" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_sanction_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_sanction" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_GP_RelatedPerson"
                                        runat="server" Title="Related Person" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_Customer_RelatedPerson" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_related_person" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Related_Person_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PERSON_PERSON_RELATION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="GENDER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TITLE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FIRST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MIDDLE_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PREFIX" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LAST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BIRTH_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BIRTH_PLACE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COUNTRY_OF_BIRTH" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MOTHERS_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ALIAS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PASSPORT_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PASSPORT_COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ID_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NATIONALITY1" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NATIONALITY2" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NATIONALITY3" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RESIDENCE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="OCCUPATION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DECEASED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DATE_DECEASED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SOURCE_OF_WEALTH" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IS_PROTECTED" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_related_person" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                 <ext:CommandColumn ID="New_cc_rp_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_RelatedPerson">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <%-- 2023-10-11, Nael: 'Person Person Relation' -> 'Person Relation' --%>
                                                <ext:Column ID="New_clm_rp_person_relation" runat="server" DataIndex="PERSON_PERSON_RELATION" Text="Person Relation" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="New_clm_rp_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_gender" runat="server" DataIndex="GENDER" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_title" runat="server" DataIndex="TITLE" Text="Title" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_first_name" runat="server" DataIndex="FIRST_NAME" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_middle_name" runat="server" DataIndex="MIDDLE_NAME" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_prefix" runat="server" DataIndex="PREFIX" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="New_clm_rp_last_name" runat="server" DataIndex="LAST_NAME" Text="Full Name" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="New_clm_rp_birth_date" runat="server" DataIndex="BIRTH_DATE" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_birth_place" runat="server" DataIndex="BIRTH_PLACE" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_country_of_birth" runat="server" DataIndex="COUNTRY_OF_BIRTH" Text="Country of Birth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_mother_name" runat="server" DataIndex="MOTHERS_NAME" Text="Mother name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_alias" runat="server" DataIndex="ALIAS" Text="Alias" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_ssn" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_passport_number" runat="server" DataIndex="PASSPORT_NUMBER" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_passport_country" runat="server" DataIndex="PASSPORT_COUNTRY" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_id_number" runat="server" DataIndex="ID_NUMBER" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <%-- 2023-10-11, Nael: HIde kolom nationality1 --%>
                                                <ext:Column ID="New_clm_rp_nationality1" runat="server" DataIndex="NATIONALITY1" Hidden="true" Text="Nationality 1" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="New_clm_rp_nationality2" runat="server" DataIndex="NATIONALITY2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_nationality3" runat="server" DataIndex="NATIONALITY3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_residence" runat="server" DataIndex="RESIDENCE" Text="Residence" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_occupation" runat="server" DataIndex="OCCUPATION" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_deceased" runat="server" DataIndex="DECEASED" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_date_deceased" runat="server" DataIndex="DATE_DECEASED" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_tax_number" runat="server" DataIndex="TAX_NUMBER" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_is_pep" runat="server" DataIndex="TAX_REG_NUMBER" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_source_of_wealth" runat="server" DataIndex="SOURCE_OF_WEALTH" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_rp_is_protected" runat="server" DataIndex="IS_PROTECTED" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>--%>
                                               
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_related_person" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_GP_Customer_Additonal_Info" runat="server" Title="Additional Information" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="New_Store_Customer_Additional_Info" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_additional_info" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Additional_Information_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_TYPE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_SUBJECT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_TEXT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_NUMERIC" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_BOOLEAN" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_additional_info" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_clm_info_type" runat="server" DataIndex="INFO_TYPE" Text="Info Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_info_subject" runat="server" DataIndex="INFO_SUBJECT" Text="Info Subject" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_info_text" runat="server" DataIndex="INFO_TEXT" Text="Info Text" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_info_numeric" runat="server" DataIndex="INFO_NUMERIC" Text="Info Numeric" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_info_date" runat="server" DataIndex="INFO_DATE" Text="Info Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_info_boolean" runat="server" DataIndex="INFO_BOOLEAN" Text="Info Boolean" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_clm_action_add_info" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_additional_info" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                </Items>
                            </ext:Panel>

                            <ext:Panel runat="server" Title="Corporate Customer" Collapsible="true" AnchorHorizontal="100%" ID="New_panel_Corp" Border="true" BodyStyle="padding:10px">
                                <Items>
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Corporate Name" ID="New_txt_Corp_Name" PaddingSpec="0 10 0 0" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Commercial Name" ID="New_txt_Corp_Commercial_Name" />

                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Legal Form" ID="New_txt_Corp_Incorporation_Legal_Form" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Corporae License Number" ID="New_txt_Corp_Incorporation_number" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Line of Business" ID="New_txt_Corp_Business" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Email Korporasi" ID="New_txt_Corp_Email" AnchorHorizontal="90%" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Website Korporasi" ID="New_txt_Corp_url" AnchorHorizontal="90%" />
                                    <ext:Panel ID="New_Pnl_cmb_Corp_Entity_Status" Hidden="true" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                        <Content>
                                            <nds:NDSDropDownField ID="New_cmb_Corp_Entity_Status" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Status" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Status" AnchorHorizontal="70%" IsReadOnly="true" />
                                        </Content>
                                    </ext:Panel>
                                    <ext:DisplayField runat="server" LabelWidth="250" Hidden="true" FieldLabel="Entity Status Date" ID="New_txt_Corp_Entity_Status_Date" />
                                    <ext:GridPanel ID="New_GP_Phone_Corp" runat="server" Hidden="true" Title="Phone" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_Phone_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_Model_Phone_Corp" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_Column9" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column10" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column11" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column12" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column13" runat="server" DataIndex="tph_extension" Text="Extension" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column14" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustPhone">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_PagingToolbar2" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <ext:GridPanel ID="New_GP_Address_Corp" runat="server" Hidden="true" Title="Address" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_Address_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_Model_Address_Corp" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Address_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_Column15" runat="server" DataIndex="Address_Type" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column16" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column17" runat="server" DataIndex="Town" Text="Town" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column18" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column19" runat="server" DataIndex="Zip" Text="Zip Code" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column20" runat="server" DataIndex="Country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column21" runat="server" DataIndex="State" Text="Province" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column22" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_CommandColumn4" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustAddress">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_PagingToolbar3" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <ext:GridPanel ID="New_GP_corp_email" runat="server" Title="Email" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_corp_email" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_corp_email" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Email_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EMAIL_ADDRESS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rcn_cor_email" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="New_CommandColumn15" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_Email">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Email_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="New_clm_corp_email_address" runat="server" DataIndex="EMAIL_ADDRESS" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_corp_email" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_GP_Customer_Entity_URL" runat="server" Title="Entity URL" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_Customer_Entity_URL" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_entity_url" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_URL_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="URL" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_entity_url" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="New_cc_entity_url" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_URL">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_URL_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="New_clm_url_type" runat="server" DataIndex="URL" Text="URL" MinWidth="130" Flex="1"></ext:Column>

                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_entity_url" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Province" ID="New_txt_Corp_incorporation_state" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Country" ID="New_txt_Corp_Incorporation_Country_Code" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Date of Established" ID="New_txt_Corp_incorporation_date" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Is Closed?" ID="New_cbTutup" Selectable="false"></ext:DisplayField>
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Closing Date" ID="New_txt_Corp_date_business_closed" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tax Number" ID="New_txt_Corp_tax_number" />
                                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Notes" ID="New_txt_Corp_Comments" />

                                    <ext:GridPanel ID="New_GP_Customer_Entity_Identification" runat="server" Title="Entity Identification" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_Customer_Entity_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_ent_identify" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Entity_Identification_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TYPE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ISSUE_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EXPIRY_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ISSUED_BY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ISSUE_COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_identify" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="New_cc_identify" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_EntityIdentification">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Identification_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="New_clm_identify_type" runat="server" DataIndex="TYPE" Text="Entity Identifier Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_identify_number" runat="server" DataIndex="NUMBER" Text="Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_identify_issued_date" runat="server" DataIndex="ISSUE_DATE" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_identify_expiry_date" runat="server" DataIndex="EXPIRY_DATE" Text="Expiry Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_identify_issued_by" runat="server" DataIndex="ISSUED_BY" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_identify_issued_country" runat="server" DataIndex="ISSUE_COUNTRY" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_identify_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_identify" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_gp_Director" runat="server" Title="Director" AutoScroll="true" Border="true" Hidden="true" BodyStyle="padding:10px">
                                        <Store>
                                            <ext:Store ID="New_store_Director" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_model_Director" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Entity_Director_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Role" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_RowNumber_Director" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_columnDirectorName" runat="server" DataIndex="Last_Name" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_ColumnDirectorRole" runat="server" DataIndex="Role" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_cmdDirectory" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdDirector">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_PagingToolbar4" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_GPCorp_Network_Device" runat="server" Title="Network Device" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="New_StoreDirector_Network_Device" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_director_network_device" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Network_Device_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DEVICE_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="OPERATING_SYSTEM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SERVICE_PROVIDER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IPV6" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IPV4" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CGN_PORT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IPV4_IPV6" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NAT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FIRST_SEEN_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LAST_SEEN_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="USING_PROXY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CITY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rcn_director_network_device" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_clm_director_device_number" runat="server" DataIndex="DEVICE_NUMBER" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_director_operating_system" runat="server" DataIndex="OPERATING_SYSTEM" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_director_service_provider" runat="server" DataIndex="SERVICE_PROVIDER" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_director_ip_v6" runat="server" DataIndex="IPV6" Text="IP V6" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_director_ip_v4" runat="server" DataIndex="IPV4" Text="IP V4" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_director_cgn_port" runat="server" DataIndex="CGN_PORT" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_director_ip_v6_with_ip_v4" runat="server" DataIndex="IPV4_IPV6" Text="IPV6 WITH IPV4" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_director_nat" runat="server" DataIndex="NAT" Text="NAT" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_director_first_seen_date" runat="server" DataIndex="FIRST_SEEN_DATE" Text="First Seen Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_director_last_seen_date" runat="server" DataIndex="LAST_SEEN_DATE" Text="Last Seen Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_director_using_proxy" runat="server" DataIndex="USING_PROXY" Text="Using Proxy" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_director_city" runat="server" DataIndex="CITY" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_director_country" runat="server" DataIndex="COUNTRY" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_director_comment" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_cc_director_network_device" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_network_device" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_GP_Corp_Sanction" runat="server" Title="Sanction" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_corp_sanction" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_corp_sanction" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Sanction_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PROVIDER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MATCH_CRITERIA" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LINK_TO_SOURCE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_ATTRIBUTES" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_corp_sanction" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="New_cc_corp_sanction_action" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_Sanction">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="New_clm_corp_sanction" runat="server" DataIndex="PROVIDER" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_sanction_list_name" runat="server" DataIndex="SANCTION_LIST_NAME" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_match_criteria" runat="server" DataIndex="MATCH_CRITERIA" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_link_to_source" runat="server" DataIndex="LINK_TO_SOURCE" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_sanction_list_attributes" runat="server" DataIndex="SANCTION_LIST_ATTRIBUTES" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                                                <%-- 2023-10-10, Nael --%>
                                                <ext:Column ID="New_clm_corp_valid_from" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_VALID_FROM" Text="Valid From" Hidden="true" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_is_approx_from_date" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE" Hidden="true" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_is_valid_to" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_VALID_TO" Text="Valid To" Hidden="true" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_is_approx_to_date" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE" Hidden="true" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>
                                                <%-- END --%>
                                                <ext:Column ID="New_clm_corp_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>

                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_corp_sanction" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_GP_CORP_Related_Person" Hidden="true"
                                        runat="server" Title="Related Person" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_StoreCorp_Related_person" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_corp_related_person" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Related_Person_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PERSON_PERSON_RELATION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RELATION_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RELATION_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RELATION_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RELATION_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="GENDER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TITLE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FIRST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MIDDLE_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PREFIX" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LAST_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BIRTHDATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BIRTH_PLACE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COUNTRY_OF_BIRTH" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="MOTHER_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ALIAS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="FULL_NAME_FRN" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PASSPORT_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="PASSPORT_COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ID_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NATIONALITY1" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NATIONALITY2" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NATIONALITY3" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RESIDENCE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="RESIDENCE_SINCE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="OCCUPATION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DECEASED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DATE_DECEASED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SOURCE_OF_WEALTH" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IS_PROTECTED" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_corp_related_person" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="New_CommandColumn16" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_RelatedPerson">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <%-- 2023-10-11, Nael: 'Person Person Relation' -> 'Person Relation'  --%>
                                                <ext:Column ID="New_clm_corp_rp_person_relation" runat="server" DataIndex="PERSON_PERSON_RELATION" Text="Person Relation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_valid_from" runat="server" DataIndex="RELATION_DATE_RANGE_VALID_FROM" Text="Relation Date Range -  Valid From" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_is_approx_from_date" runat="server" DataIndex="RELATION_DATE_RANGE_IS_APPROX_FROM_DATE" Text="Relation Date Range -  Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_valid_to" runat="server" DataIndex="RELATION_DATE_RANGE_VALID_TO" Text="Relation Date Range -  Valid To" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_is_approx_to_date" runat="server" DataIndex="RELATION_DATE_RANGE_IS_APPROX_TO_DATE" Text="Relation Date Range -  Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_gender" runat="server" DataIndex="GENDER" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_title" runat="server" DataIndex="TITLE" Text="Title" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_first_name" runat="server" DataIndex="FIRST_NAME" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_middle_name" runat="server" DataIndex="MIDDLE_NAME" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_prefix" runat="server" DataIndex="PREFIX" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_last_name" runat="server" DataIndex="LAST_NAME" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_birth_date" runat="server" DataIndex="BIRTHDATE" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_birth_place" runat="server" DataIndex="BIRTH_PLACE" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_country_of_birth" runat="server" DataIndex="COUNTRY_OF_BIRTH" Text="Country of Birth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_mother_name" runat="server" DataIndex="MOTHER_NAME" Text="Mother name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_alias" runat="server" DataIndex="ALIAS" Text="Alias" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_foreign_name" runat="server" DataIndex="FULL_NAME_FRN" Text="Full Foreign Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_ssn" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_passport_number" runat="server" DataIndex="PASSPORT_NUMBER" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_passport_country" runat="server" DataIndex="PASSPORT_COUNTRY" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_id_number" runat="server" DataIndex="ID_NUMBER" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_nationality1" runat="server" DataIndex="NATIONALITY1" Text="Nationality 1" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_nationality2" runat="server" DataIndex="NATIONALITY2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_nationality3" runat="server" DataIndex="NATIONALITY3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_residence" runat="server" DataIndex="RESIDENCE" Text="Residence" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_residence_since" runat="server" DataIndex="RESIDENCE_SINCE" Text="Residence Since" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_occupation" runat="server" DataIndex="OCCUPATION" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_deceased" runat="server" DataIndex="DECEASED" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_date_deceased" runat="server" DataIndex="DATE_DECEASED" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_tax_number" runat="server" DataIndex="TAX_NUMBER" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_is_pep" runat="server" DataIndex="TAX_REG_NUMBER" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_source_of_wealth" runat="server" DataIndex="SOURCE_OF_WEALTH" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_rp_is_protected" runat="server" DataIndex="IS_PROTECTED" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>

                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_corp_related_person" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_GP_CORP_Related_Entities" runat="server" Title="Related Entties" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="New_Store_Related_Entities" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_related_entities" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Related_Entities_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ENTITY_ENTITY_RELATION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMERCIAL_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_LEGAL_FORM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BUSINESS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_STATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_COUNTRY_CODE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DATE_BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_related_etities" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="New_cc_related_entities" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomer_RelatedEntities">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Entities_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="New_clm_re_email_address" runat="server" DataIndex="ENTITY_ENTITY_RELATION" Text="Entity Entity Relation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column89" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column90" runat="server" DataIndex="NAME" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column91" runat="server" DataIndex="COMMERCIAL_NAME" Text="Commercial Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column92" runat="server" DataIndex="INCORPORATION_LEGAL_FORM" Text="Incorporation Legal Form" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column93" runat="server" DataIndex="INCORPORATION_NUMBER" Text="Incorporation Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column94" runat="server" DataIndex="BUSINESS" Text="Business" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column97" runat="server" DataIndex="INCORPORATION_STATE" Text="Incorporation State" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column98" runat="server" DataIndex="INCORPORATION_COUNTRY_CODE" Text="Incorporation Country Code" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column99" runat="server" DataIndex="INCORPORATION_DATE" Text="Incorporation Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column100" runat="server" DataIndex="BUSINESS_CLOSED" Text="Businesss Closed" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column101" runat="server" DataIndex="DATE_BUSINESS_CLOSED" Text="Date Business Closed" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column102" runat="server" DataIndex="TAX_NUMBER" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_Column103" runat="server" DataIndex="TAX_REG_NUMBER" Text="Tax Reg Number" MinWidth="130" Flex="1"></ext:Column>

                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_related_entities" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:GridPanel ID="New_GP_Customer_Corp_Additonal_Info" runat="server" Title="Additional Information" AutoScroll="true" Visible="false">
                                        <Store>
                                            <ext:Store ID="New_Store_Customer_Corp_Additional_Info" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="New_mdl_corp_additional_info" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Additional_Information_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_TYPE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_SUBJECT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_TEXT" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_NUMERIC" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INFO_BOOLEAN" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="New_rnc_corp_additional_info" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="New_clm_corp_info_type" runat="server" DataIndex="INFO_TYPE" Text="Info Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_info_subject" runat="server" DataIndex="INFO_SUBJECT" Text="Info Subject" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_info_text" runat="server" DataIndex="INFO_TEXT" Text="Info Text" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_info_numeric" runat="server" DataIndex="INFO_NUMERIC" Text="Info Numeric" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_info_date" runat="server" DataIndex="INFO_DATE" Text="Info Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="New_clm_corp_info_boolean" runat="server" DataIndex="INFO_BOOLEAN" Text="Info Boolean" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="New_clm_corp_action_add_info" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustomerPreviousName">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Previous_Name_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="type" Value="new" Mode="Value"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="New_pt_corp_additional_info" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                </Items>
                            </ext:Panel>

                        </Items>
                    </ext:FormPanel>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnSave" runat="server" Text="Accept" Icon="DiskBlack">
                        <DirectEvents>
                            <Click OnEvent="BtnSave_Click">
                                <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnReject" runat="server" Text="Reject" Icon="Decline">
                        <DirectEvents>
                            <Click OnEvent="BtnReject_Click">
                                <EventMask ShowMask="true" Msg="Saving Reject Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnCancel" runat="server" Text="Cancel" Icon="PageBack">
                        <DirectEvents>
                            <Click OnEvent="BtnCancel_Click">
                                <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:TabPanel>
        </Items>
    </ext:Container>

    <ext:FormPanel ID="Panelconfirmation" runat="server" ClientIDMode="Static" Title="Confirmation" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel" Tex="aa">
            </ext:Label>

        </Items>

        <Buttons>

            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
            </ext:Button>
        </Buttons>
    </ext:FormPanel>



</asp:Content>

