﻿Imports System.Data
Imports Ext.Net
Imports NawaBLL
Imports GoAMLBLL
Imports GoAMLBLL.goAML
Imports GoAMLBLL.goAML.Services
Imports GoAMLDAL

Partial Class CUSTOMER_APPROVAL_DETAIL_v501
    Inherits Parent

    Public Property CustomMessage As String

    Private _IDReq As Long

#Region "Property Session New"

    Public Property New_objListgoAML_Ref_Address() As List(Of GoAMLDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Address))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Address") = value
        End Set
    End Property

    Public Property New_objListgoAML_Ref_Phone() As List(Of GoAMLDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Phone))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Phone") = value
        End Set
    End Property
    Public Property New_objListgoAML_Ref_EmployerPhone() As List(Of GoAMLDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_EmployerPhone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Phone))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_EmployerPhone") = value
        End Set
    End Property
    Public Property Old_objListgoAML_Ref_EmployerPhone() As List(Of GoAMLDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_EmployerPhone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Phone))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_EmployerPhone") = value
        End Set
    End Property
    Public Property New_objListgoAML_Ref_EmployerAddress() As List(Of GoAMLDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_EmployerAddress")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Address))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_EmployerAddress") = value
        End Set
    End Property
    Public Property Old_objListgoAML_Ref_EmployerAddress() As List(Of GoAMLDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_EmployerAddress")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Address))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_EmployerAddress") = value
        End Set
    End Property

    Public Property New_objListgoAML_Ref_Director_Address() As List(Of GoAMLDAL.Vw_Director_Addresses)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Director_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Addresses))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Director_Address") = value
        End Set
    End Property

    Public Property New_objListgoAML_Ref_Director_Phone() As List(Of GoAMLDAL.Vw_Director_Phones)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Director_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Phones))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Director_Phone") = value
        End Set
    End Property

    Public Property New_objListgoAML_Ref_Director_Employer_Address() As List(Of GoAMLDAL.Vw_Director_Employer_Addresses)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Director_Employer_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Employer_Addresses))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Director_Employer_Address") = value
        End Set
    End Property

    Public Property New_objListgoAML_Ref_Director_Employer_Phone() As List(Of GoAMLDAL.Vw_Director_Employer_Phones)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Director_Employer_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Employer_Phones))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Director_Employer_Phone") = value
        End Set
    End Property

    Public Property New_objTempCustomerAddresslEdit() As GoAMLDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomerAddresslEdit")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Address)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomerAddresslEdit") = value
        End Set
    End Property

    Public Property New_objTempCustomerPhoneEdit() As GoAMLDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomerPhoneEdit")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Phone)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomerPhoneEdit") = value
        End Set
    End Property

    Public Property New_obj_Customer As GoAMLDAL.goAML_Ref_Customer
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_obj_Customer")
        End Get
        Set(value As GoAMLDAL.goAML_Ref_Customer)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_obj_Customer") = value
        End Set
    End Property

    Public Property New_objListgoAML_Ref_Identification_Director() As List(Of GoAMLDAL.goAML_Person_Identification)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Identification_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Person_Identification))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Identification_Director") = value
        End Set
    End Property
    Public Property New_objListgoAML_Ref_Identification() As List(Of GoAMLDAL.goAML_Person_Identification)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Identification")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Person_Identification))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Identification") = value
        End Set
    End Property

    Public Property New_objListgoAML_vw_Customer_Identification_Director() As List(Of GoAMLDAL.Vw_Person_Identification)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_Customer_Identification_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Person_Identification))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_Customer_Identification_Director") = value
        End Set
    End Property

    '' Add 04-Jan-2023, Septian. GoAML 5.0.1
    Public Property New_objTempCustomerPreviousNameEdit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomerPreviousName")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomerPreviousName") = value
        End Set
    End Property

    Public Property New_objListgoAML_Ref_Previous_Name() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Previous_Name")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Previous_name") = value
        End Set
    End Property

    Public Property New_objListgoAML_vw_Previous_Name() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_Previous_Name")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_Previous_Name") = value
        End Set
    End Property

    Public Property New_objTempCustomer_Email_Edit() As DataModel.goAML_Ref_Customer_Email
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_Email_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Email)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_Email_Edit") = value
        End Set
    End Property

    Public Property New_objListgoAML_Ref_Email() As List(Of DataModel.goAML_Ref_Customer_Email)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Email")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Email))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Email") = value
        End Set
    End Property

    Public Property New_objListgoAML_vw_Email() As List(Of DataModel.goAML_Ref_Customer_Email)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_Email")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Email))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_Email") = value
        End Set
    End Property

    Public Property New_objTempCustomer_EmploymentHistory_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_EmploymentHistory_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_EmploymentHistory_Edit") = value
        End Set
    End Property

    Public Property New_objListgoAML_Ref_EmploymentHistory() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_EmploymentHistory")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_EmploymentHistory") = value
        End Set
    End Property

    Public Property New_objListgoAML_vw_EmploymentHistory() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_EmploymentHistory")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_EmploymentHistory") = value
        End Set
    End Property

    Public Property New_objTempCustomer_PersonPEP_Edit() As DataModel.goAML_Ref_Customer_PEP
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_PersonPEP_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_PEP)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_PersonPEP_Edit") = value
        End Set
    End Property

    Public Property New_objListgoAML_Ref_PersonPEP() As List(Of DataModel.goAML_Ref_Customer_PEP)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_PersonPEP")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_PEP))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_PersonPEP") = value
        End Set
    End Property

    Public Property New_objListgoAML_vw_PersonPEP() As List(Of DataModel.goAML_Ref_Customer_PEP)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_PersonPEP")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_PEP))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_PersonPEP") = value
        End Set
    End Property
    Public Property New_objTempCustomer_NetworkDevice_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_NetworkDevice_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_NetworkDevice_Edit") = value
        End Set
    End Property

    Public Property New_objListgoAML_Ref_NetworkDevice() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_NetworkDevice")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_NetworkDevice") = value
        End Set
    End Property

    Public Property New_objListgoAML_vw_NetworkDevice() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_NetworkDevice")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_NetworkDevice") = value
        End Set
    End Property

    Public Property New_objTempCustomer_SocialMedia_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media
        Get
            Return Session("CUSTOMER_EDIIT_v501.New_objTempCustomer_SocialMedia_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
            Session("CUSTOMER_EDIIT_v501.New_objTempCustomer_SocialMedia_Edit") = value
        End Set
    End Property

    Public Property New_objListgoAML_Ref_SocialMedia() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_SocialMedia")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_SocialMedia") = value
        End Set
    End Property
    Public Property New_objListgoAML_vw_SocialMedia() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_SocialMedia")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_SocialMedia") = value
        End Set
    End Property

    Public Property New_objTempCustomer_Sanction_Edit() As DataModel.goAML_Ref_Customer_Sanction
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_Sanction_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Sanction)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_Sanction_Edit") = value
        End Set
    End Property
    Public Property New_objListgoAML_Ref_Sanction() As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Sanction")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Sanction))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_Sanction") = value
        End Set
    End Property
    Public Property New_objListgoAML_vw_Sanction() As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_Sanction")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Sanction))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_Sanction") = value
        End Set
    End Property

    Public Property New_objTempCustomer_RelatedPerson_Edit() As DataModel.goAML_Ref_Customer_Related_Person
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_RelatedPerson_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Person)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_RelatedPerson_Edit") = value
        End Set
    End Property
    Public Property New_objListgoAML_Ref_RelatedPerson() As List(Of DataModel.goAML_Ref_Customer_Related_Person)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_RelatedPerson")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Person))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_RelatedPerson") = value
        End Set
    End Property
    Public Property New_objListgoAML_vw_RelatedPerson() As List(Of DataModel.goAML_Ref_Customer_Related_Person)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_RelatedPerson")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Person))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_RelatedPerson") = value
        End Set
    End Property

    Public Property New_objTempCustomer_AdditionalInfo_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_AdditionalInfo_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_AdditionalInfo_Edit") = value
        End Set
    End Property
    Public Property New_objListgoAML_Ref_AdditionalInfo() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_AdditionalInfo")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_AdditionalInfo") = value
        End Set
    End Property
    Public Property New_objListgoAML_vw_AdditionalInfo() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_AdditionalInfo")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_AdditionalInfo") = value
        End Set
    End Property

    Public Property New_objTempCustomer_EntityUrl_Edit() As DataModel.goAML_Ref_Customer_URL
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_EntityUrl_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_URL)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_EntityUrl_Edit") = value
        End Set
    End Property
    Public Property New_objListgoAML_Ref_EntityUrl() As List(Of DataModel.goAML_Ref_Customer_URL)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_EntityUrl")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_URL))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_EntityUrl") = value
        End Set
    End Property
    Public Property New_objListgoAML_vw_EntityUrl() As List(Of DataModel.goAML_Ref_Customer_URL)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_EntityUrl")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_URL))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_EntityUrl") = value
        End Set
    End Property

    Public Property New_objTempCustomer_EntityIdentification_Edit() As DataModel.goAML_Ref_Customer_Entity_Identification
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_EntityIdentification_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Entity_Identification)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_EntityIdentification_Edit") = value
        End Set
    End Property
    Public Property New_objListgoAML_Ref_EntityIdentification() As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_EntityIdentification")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_EntityIdentification") = value
        End Set
    End Property
    Public Property New_objListgoAML_vw_EntityIdentification() As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_EntityIdentification")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_EntityIdentification") = value
        End Set
    End Property

    Public Property New_objTempCustomer_RelatedEntities_Edit() As DataModel.goAML_Ref_Customer_Related_Entities
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_RelatedEntities_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Entities)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTempCustomer_RelatedEntities_Edit") = value
        End Set
    End Property
    Public Property New_objListgoAML_Ref_RelatedEntities() As List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_RelatedEntities")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Entities))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_Ref_RelatedEntities") = value
        End Set
    End Property
    Public Property New_objListgoAML_vw_RelatedEntities() As List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_RelatedEntities")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Entities))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objListgoAML_vw_RelatedEntities") = value
        End Set
    End Property

    Public Property New_obj_Customer2 As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_obj_Customer2")
        End Get
        Set(value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_obj_Customer2") = value
        End Set
    End Property

    Public Property New_objTemp_goAML_Ref_Customer_Email() As DataModel.goAML_Ref_Customer_Email
        Get
            Return Session("CUSTOMER_EDIIT_v501.New_objTemp_goAML_Ref_Customer_Email")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Email)
            Session("CUSTOMER_EDIIT_v501.New_objTemp_goAML_Ref_Customer_Email") = value
        End Set
    End Property

    Public Property New_objTemp_goAML_Ref_Customer_PEP() As DataModel.goAML_Ref_Customer_PEP
        Get
            Return Session("CUSTOMER_EDIIT_v501.New_objTemp_goAML_Ref_Customer_PEP")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_PEP)
            Session("CUSTOMER_EDIIT_v501.New_objTemp_goAML_Ref_Customer_PEP") = value
        End Set
    End Property

    Public Property New_objTemp_goAML_Ref_Customer_Sanction() As DataModel.goAML_Ref_Customer_Sanction
        Get
            Return Session("CUSTOMER_EDIIT_v501.New_objTemp_goAML_Ref_Customer_Sanction")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Sanction)
            Session("CUSTOMER_EDIIT_v501.New_objTemp_goAML_Ref_Customer_Sanction") = value
        End Set
    End Property

    Public Property New_objTemp_goAML_Ref_Customer_Related_Person() As DataModel.goAML_Ref_Customer_Related_Person
        Get
            Return Session("CUSTOMER_EDIIT_v501.New_objTemp_goAML_Ref_Customer_Related_Person")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Person)
            Session("CUSTOMER_EDIIT_v501.New_objTemp_goAML_Ref_Customer_Related_Person") = value
        End Set
    End Property

    Public Property New_objTemp_goAML_Ref_Customer_URL() As DataModel.goAML_Ref_Customer_URL
        Get
            Return Session("CUSTOMER_EDIIT_v501.New_objTemp_goAML_Ref_Customer_URL")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_URL)
            Session("CUSTOMER_EDIIT_v501.New_objTemp_goAML_Ref_Customer_URL") = value
        End Set
    End Property

    Public Property New_objTemp_goAML_Ref_Customer_Entity_Identification() As DataModel.goAML_Ref_Customer_Entity_Identification
        Get
            Return Session("CUSTOMER_EDIIT_v501.New_objTemp_goAML_Ref_Customer_Entity_Identification")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Entity_Identification)
            Session("CUSTOMER_EDIIT_v501.New_objTemp_goAML_Ref_Customer_Entity_Identification") = value
        End Set
    End Property
    Public Property New_objTemp_goAML_Ref_Customer_Related_Entities() As DataModel.goAML_Ref_Customer_Related_Entities
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTemp_goAML_Ref_Customer_Related_Entities")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Entities)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.New_objTemp_goAML_Ref_Customer_Related_Entities") = value
        End Set
    End Property

#End Region

#Region "Property Session Old"

    Public Property Old_objListgoAML_Ref_Address() As List(Of GoAMLDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Address))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Address") = value
        End Set
    End Property

    Public Property Old_objListgoAML_Ref_Phone() As List(Of GoAMLDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Ref_Phone))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Phone") = value
        End Set
    End Property

    Public Property Old_objListgoAML_Ref_Director_Address() As List(Of GoAMLDAL.Vw_Director_Addresses)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Director_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Addresses))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Director_Address") = value
        End Set
    End Property

    Public Property Old_objListgoAML_Ref_Director_Phone() As List(Of GoAMLDAL.Vw_Director_Phones)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Director_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Phones))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Director_Phone") = value
        End Set
    End Property

    Public Property Old_objListgoAML_Ref_Director_Employer_Address() As List(Of GoAMLDAL.Vw_Director_Employer_Addresses)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Director_Employer_Address")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Employer_Addresses))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Director_Employer_Address") = value
        End Set
    End Property

    Public Property Old_objListgoAML_Ref_Director_Employer_Phone() As List(Of GoAMLDAL.Vw_Director_Employer_Phones)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Director_Employer_Phone")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Director_Employer_Phones))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Director_Employer_Phone") = value
        End Set
    End Property

    Public Property Old_objTempCustomerAddresslEdit() As GoAMLDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomerAddresslEdit")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Address)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomerAddresslEdit") = value
        End Set
    End Property

    Public Property Old_objTempCustomerPhoneEdit() As GoAMLDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomerPhoneEdit")
        End Get
        Set(ByVal value As GoAMLDAL.goAML_Ref_Phone)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomerPhoneEdit") = value
        End Set
    End Property

    Public Property Old_obj_Customer As GoAMLDAL.goAML_Ref_Customer
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_obj_Customer")
        End Get
        Set(value As GoAMLDAL.goAML_Ref_Customer)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_obj_Customer") = value
        End Set
    End Property

    Public Property Old_objListgoAML_Ref_Identification_Director() As List(Of GoAMLDAL.goAML_Person_Identification)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Identification_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Person_Identification))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Identification_Director") = value
        End Set
    End Property
    Public Property Old_objListgoAML_Ref_Identification() As List(Of GoAMLDAL.goAML_Person_Identification)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Identification")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.goAML_Person_Identification))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Identification") = value
        End Set
    End Property

    Public Property Old_objListgoAML_vw_Customer_Identification_Director() As List(Of GoAMLDAL.Vw_Person_Identification)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_Customer_Identification_Director")
        End Get
        Set(ByVal value As List(Of GoAMLDAL.Vw_Person_Identification))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_Customer_Identification_Director") = value
        End Set
    End Property

    '' Add 04-Jan-2023, Septian. GoAML 5.0.1
    Public Property Old_objTempCustomerPreviousNameEdit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomerPreviousName")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomerPreviousName") = value
        End Set
    End Property

    Public Property Old_objListgoAML_Ref_Previous_Name() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Previous_Name")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Previous_name") = value
        End Set
    End Property

    Public Property Old_objListgoAML_vw_Previous_Name() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_Previous_Name")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_Previous_Name") = value
        End Set
    End Property

    Public Property Old_objTempCustomer_Email_Edit() As DataModel.goAML_Ref_Customer_Email
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_Email_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Email)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_Email_Edit") = value
        End Set
    End Property

    Public Property Old_objListgoAML_Ref_Email() As List(Of DataModel.goAML_Ref_Customer_Email)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Email")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Email))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Email") = value
        End Set
    End Property

    Public Property Old_objListgoAML_vw_Email() As List(Of DataModel.goAML_Ref_Customer_Email)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_Email")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Email))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_Email") = value
        End Set
    End Property

    Public Property Old_objTempCustomer_EmploymentHistory_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_EmploymentHistory_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_EmploymentHistory_Edit") = value
        End Set
    End Property

    Public Property Old_objListgoAML_Ref_EmploymentHistory() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_EmploymentHistory")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_EmploymentHistory") = value
        End Set
    End Property

    Public Property Old_objListgoAML_vw_EmploymentHistory() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_EmploymentHistory")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_EmploymentHistory") = value
        End Set
    End Property

    Public Property Old_objTempCustomer_PersonPEP_Edit() As DataModel.goAML_Ref_Customer_PEP
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_PersonPEP_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_PEP)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_PersonPEP_Edit") = value
        End Set
    End Property

    Public Property Old_objListgoAML_Ref_PersonPEP() As List(Of DataModel.goAML_Ref_Customer_PEP)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_PersonPEP")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_PEP))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_PersonPEP") = value
        End Set
    End Property

    Public Property Old_objListgoAML_vw_PersonPEP() As List(Of DataModel.goAML_Ref_Customer_PEP)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_PersonPEP")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_PEP))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_PersonPEP") = value
        End Set
    End Property
    Public Property Old_objTempCustomer_NetworkDevice_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_NetworkDevice_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_NetworkDevice_Edit") = value
        End Set
    End Property

    Public Property Old_objListgoAML_Ref_NetworkDevice() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_NetworkDevice")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_NetworkDevice") = value
        End Set
    End Property

    Public Property Old_objListgoAML_vw_NetworkDevice() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_NetworkDevice")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_NetworkDevice") = value
        End Set
    End Property

    Public Property Old_objTempCustomer_SocialMedia_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_SocialMedia_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_SocialMedia_Edit") = value
        End Set
    End Property

    Public Property Old_objListgoAML_Ref_SocialMedia() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_SocialMedia")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_SocialMedia") = value
        End Set
    End Property
    Public Property Old_objListgoAML_vw_SocialMedia() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_SocialMedia")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_SocialMedia") = value
        End Set
    End Property

    Public Property Old_objTempCustomer_Sanction_Edit() As DataModel.goAML_Ref_Customer_Sanction
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_Sanction_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Sanction)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_Sanction_Edit") = value
        End Set
    End Property
    Public Property Old_objListgoAML_Ref_Sanction() As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Sanction")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Sanction))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_Sanction") = value
        End Set
    End Property
    Public Property Old_objListgoAML_vw_Sanction() As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_Sanction")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Sanction))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_Sanction") = value
        End Set
    End Property

    Public Property Old_objTempCustomer_RelatedPerson_Edit() As DataModel.goAML_Ref_Customer_Related_Person
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_RelatedPerson_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Person)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_RelatedPerson_Edit") = value
        End Set
    End Property
    Public Property Old_objListgoAML_Ref_RelatedPerson() As List(Of DataModel.goAML_Ref_Customer_Related_Person)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_RelatedPerson")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Person))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_RelatedPerson") = value
        End Set
    End Property
    Public Property Old_objListgoAML_vw_RelatedPerson() As List(Of DataModel.goAML_Ref_Customer_Related_Person)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_RelatedPerson")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Person))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_RelatedPerson") = value
        End Set
    End Property

    Public Property Old_objTempCustomer_AdditionalInfo_Edit() As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_AdditionalInfo_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_AdditionalInfo_Edit") = value
        End Set
    End Property
    Public Property Old_objListgoAML_Ref_AdditionalInfo() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_AdditionalInfo")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_AdditionalInfo") = value
        End Set
    End Property
    Public Property Old_objListgoAML_vw_AdditionalInfo() As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_AdditionalInfo")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_AdditionalInfo") = value
        End Set
    End Property

    Public Property Old_objTempCustomer_EntityUrl_Edit() As DataModel.goAML_Ref_Customer_URL
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_EntityUrl_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_URL)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_EntityUrl_Edit") = value
        End Set
    End Property
    Public Property Old_objListgoAML_Ref_EntityUrl() As List(Of DataModel.goAML_Ref_Customer_URL)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_EntityUrl")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_URL))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_EntityUrl") = value
        End Set
    End Property
    Public Property Old_objListgoAML_vw_EntityUrl() As List(Of DataModel.goAML_Ref_Customer_URL)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_EntityUrl")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_URL))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_EntityUrl") = value
        End Set
    End Property

    Public Property Old_objTempCustomer_EntityIdentification_Edit() As DataModel.goAML_Ref_Customer_Entity_Identification
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_EntityIdentification_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Entity_Identification)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_EntityIdentification_Edit") = value
        End Set
    End Property
    Public Property Old_objListgoAML_Ref_EntityIdentification() As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_EntityIdentification")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_EntityIdentification") = value
        End Set
    End Property
    Public Property Old_objListgoAML_vw_EntityIdentification() As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_EntityIdentification")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_EntityIdentification") = value
        End Set
    End Property

    Public Property Old_objTempCustomer_RelatedEntities_Edit() As DataModel.goAML_Ref_Customer_Related_Entities
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_RelatedEntities_Edit")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Entities)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTempCustomer_RelatedEntities_Edit") = value
        End Set
    End Property
    Public Property Old_objListgoAML_Ref_RelatedEntities() As List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_RelatedEntities")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Entities))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_Ref_RelatedEntities") = value
        End Set
    End Property
    Public Property Old_objListgoAML_vw_RelatedEntities() As List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_RelatedEntities")
        End Get
        Set(ByVal value As List(Of DataModel.goAML_Ref_Customer_Related_Entities))
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objListgoAML_vw_RelatedEntities") = value
        End Set
    End Property

    Public Property Old_obj_Customer2 As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_obj_Customer2")
        End Get
        Set(value As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_obj_Customer2") = value
        End Set
    End Property

    Public Property Old_objTemp_goAML_Ref_Customer_Email() As DataModel.goAML_Ref_Customer_Email
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_Email")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Email)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_Email") = value
        End Set
    End Property

    Public Property Old_objTemp_goAML_Ref_Customer_PEP() As DataModel.goAML_Ref_Customer_PEP
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_PEP")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_PEP)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_PEP") = value
        End Set
    End Property

    Public Property Old_objTemp_goAML_Ref_Customer_Sanction() As DataModel.goAML_Ref_Customer_Sanction
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_Sanction")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Sanction)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_Sanction") = value
        End Set
    End Property

    Public Property Old_objTemp_goAML_Ref_Customer_Related_Person() As DataModel.goAML_Ref_Customer_Related_Person
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_Related_Person")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Person)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_Related_Person") = value
        End Set
    End Property

    Public Property Old_objTemp_goAML_Ref_Customer_URL() As DataModel.goAML_Ref_Customer_URL
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_URL")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_URL)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_URL") = value
        End Set
    End Property

    Public Property Old_objTemp_goAML_Ref_Customer_Entity_Identification() As DataModel.goAML_Ref_Customer_Entity_Identification
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_Entity_Identification")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Entity_Identification)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_Entity_Identification") = value
        End Set
    End Property

    Public Property Old_objTemp_goAML_Ref_Customer_Related_Entities() As DataModel.goAML_Ref_Customer_Related_Entities
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_Related_Entities")
        End Get
        Set(ByVal value As DataModel.goAML_Ref_Customer_Related_Entities)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.Old_objTemp_goAML_Ref_Customer_Related_Entities") = value
        End Set
    End Property

#End Region

    Public Property IDReq() As Long
        Get
            Return _IDReq
        End Get
        Set(ByVal value As Long)
            _IDReq = value
        End Set
    End Property

    Private _objApproval As NawaDAL.ModuleApproval
    Public Property ObjApproval() As NawaDAL.ModuleApproval
        Get
            Return _objApproval
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            _objApproval = value
        End Set
    End Property



    Public Property objSchemaModule() As NawaDAL.Module
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.objSchemaModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.objSchemaModule") = value
        End Set
    End Property

    Public Property ObjFieldData() As String
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.ObjFieldData")
        End Get
        Set(ByVal value As String)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.ObjFieldData") = value
        End Set
    End Property
    Public Property ObjDataNew() As GoAMLBLL.goAML_CustomerDataBLL
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.ObjDataNew")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.ObjDataNew") = value
        End Set
    End Property
    Public Property ObjDataOld() As GoAMLBLL.goAML_CustomerDataBLL
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL_v501.ObjDataOld")
        End Get
        Set(ByVal value As GoAMLBLL.goAML_CustomerDataBLL)
            Session("CUSTOMER_APPROVAL_DETAIL_v501.ObjDataOld") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim strid As String = Request.Params("ID")
            Dim strModuleid As String = Request.Params("ModuleID")

            Try

                IDReq = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim moduleid As Integer = NawaBLL.Common.DecryptQueryString(strModuleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                objSchemaModule = NawaBLL.ModuleBLL.GetModuleByModuleID(moduleid)
            Catch ex As Exception
                Throw New Exception("Invalid ID Approval.")
            End Try

            If Not Ext.Net.X.IsAjaxRequest Then

                GridAddressWork_RelatedPerson.IsViewMode = True
                GridAddress_RelatedPerson.IsViewMode = True
                GridPhone_RelatedPerson.IsViewMode = True
                GridPhoneWork_RelatedPerson.IsViewMode = True
                GridIdentification_RelatedPerson.IsViewMode = True
                GridEmail_RelatedPerson.IsViewMode = True
                GridSanction_RelatedPerson.IsViewMode = True
                GridPEP_RelatedPerson.IsViewMode = True
                GridEmail_RelatedEntity.IsViewMode = True
                GridEntityIdentification_RelatedEntity.IsViewMode = True
                GridURL_RelatedEntity.IsViewMode = True
                GridSanction_RelatedEntity.IsViewMode = True
                GridEmail_Director.IsViewMode = True
                GridSanction_Director.IsViewMode = True
                GridPEP_Director.IsViewMode = True


                ObjApproval = NawaBLL.ModuleApprovalBLL.GetModuleApprovalByID(IDReq)
                If Not ObjApproval Is Nothing Then
                    PanelInfo.Title = "Daftar Customer Approval"
                    lblModuleName.Text = objSchemaModule.ModuleLabel
                    lblModuleKey.Text = ObjApproval.ModuleKey
                    lblAction.Text = NawaBLL.ModuleBLL.GetModuleActionNamebyID(ObjApproval.PK_ModuleAction_ID)
                    LblCreatedBy.Text = ObjApproval.CreatedBy
                    lblCreatedDate.Text = ObjApproval.CreatedDate.Value.ToString("dd-MMM-yyyy")

                End If

                Select Case ObjApproval.PK_ModuleAction_ID
                    Case NawaBLL.Common.ModuleActionEnum.Insert

                        ObjFieldData = ObjApproval.ModuleField

                        Dim unikkey As String = Guid.NewGuid.ToString

                        Dim objNewEODTask As New GoAMLBLL.goAML_CustomerBLL
                        'LoadData("New", ObjFieldData)
                        'GoAMLBLL.goAML_CustomerBLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkey)

                        LoadDataNew(ObjApproval.ModuleField)
                        GridPanelPopUp()

                        FormPanelOld.Visible = False
                    Case NawaBLL.Common.ModuleActionEnum.Delete
                        Dim unikkeyNew As String = Guid.NewGuid.ToString

                        Dim objNewMGroupAccess As New GoAMLBLL.goAML_CustomerBLL
                        'GoAMLBLL.goAML_CustomerBLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkeyNew)


                        'LoadDataNew(ObjApproval.ModuleField)
                        FormPanelOld.Visible = False
                        FormPanelNew.Visible = False
                    Case NawaBLL.Common.ModuleActionEnum.Update
                        ObjFieldData = ObjApproval.ModuleField
                        Dim unikkeyOld As String = Guid.NewGuid.ToString
                        Dim unikkeyNew As String = Guid.NewGuid.ToString

                        Dim objNewMGroupAccess As New GoAMLBLL.goAML_CustomerBLL
                        LoadDataNew(ObjApproval.ModuleField)
                        LoadDataOld(ObjApproval.ModuleFieldBefore)
                        GridPanelPopUp()

                        'LoadData("New", ObjFieldData)
                        'LoadData("Old", ObjApproval.ModuleFieldBefore)
                        'GoAMLBLL.goAML_CustomerBLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkeyNew)
                        'GoAMLBLL.goAML_CustomerBLL.LoadPanel(FormPanelOld, ObjApproval.ModuleFieldBefore, ObjApproval.ModuleName, unikkeyOld)


                            'GoAMLBLL.goAML_CustomerBLL.SettingColor(FormPanelOld, FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleFieldBefore, unikkeyOld, unikkeyNew)


                            'objNewMGroupAccess.SettingColor(FormPanelOld, FormPanelNew, ObjApproval.ModuleFieldBefore, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkeyOld, unikkeyNew)

                    Case NawaBLL.Common.ModuleActionEnum.Activation
                        Dim unikkeyNew As String = Guid.NewGuid.ToString
                        Dim objNewMGroupAccess As New GoAMLBLL.goAML_CustomerBLL
                        GoAMLBLL.goAML_CustomerBLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkeyNew)

                        FormPanelOld.Visible = False
                        FormPanelNew.Visible = False
                End Select

            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataOld(objData As String)
        Try

            Dim objgoAML_CustomerDataBLL As GoAMLBLL.goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objData, GetType(GoAMLBLL.goAML_CustomerDataBLL))
            ObjDataOld = objgoAML_CustomerDataBLL
            'Dedy added 31082020 aktif is hidden
            Old_txt_statusrekening.Hidden = True
            'Dedy end added 31082020
            ClearSessionOld()
            GridPanelSettingOld()
            Maximizeable()
            'Dim strid As String = GetId()
            Dim strModuleid As String = Request.Params("ModuleID")
            Dim INDV_gender As GoAMLDAL.goAML_Ref_Jenis_Kelamin = Nothing
            Dim INDV_statusCode As GoAMLDAL.goAML_Ref_Status_Rekening = Nothing
            Dim INDV_nationality1 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_nationality2 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_nationality3 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_residence As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim CORP_Incorporation_legal_form As GoAMLDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
            Dim CORP_incorporation_country_code As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim CORP_Role As GoAMLDAL.goAML_Ref_Party_Role = Nothing

            Try
                Old_obj_Customer = objgoAML_CustomerDataBLL.objgoAML_Ref_Customer
                'Old_obj_Customer = goAML_CustomerBLL.GetCustomerbyCIF(strid)

                'Dedy added 01092020
                If Old_obj_Customer IsNot Nothing Then

                    Old_txt_CIF.Text = Old_obj_Customer.CIF
                    Old_txt_GCN.Text = Old_obj_Customer.GCN
                    If Old_obj_Customer.isGCNPrimary = True Then
                        Old_GCNPrimary.Checked = True
                    End If
                    If Old_obj_Customer.Closing_Date IsNot Nothing Then
                        Old_TextFieldClosingDate.Text = Old_obj_Customer.Closing_Date.Value.ToString("dd-MMM-yyyy")
                    End If
                    If Old_obj_Customer.Opening_Date IsNot Nothing Then
                        Old_TextFieldOpeningDate.Text = Old_obj_Customer.Opening_Date.Value.ToString("dd-MMM-yyyy")
                    End If
                    If Old_obj_Customer.Comments IsNot Nothing Then
                        Old_TextFieldComments.Text = Old_obj_Customer.Comments
                    End If


                    Using Old_objdbs As New GoAMLDAL.GoAMLEntities
                        If Old_obj_Customer.Opening_Branch_Code IsNot Nothing And Not String.IsNullOrEmpty(Old_obj_Customer.Opening_Branch_Code) Then
                            Dim Old_objbranch = Old_objdbs.AML_BRANCH.Where(Function(x) x.FK_AML_BRANCH_CODE = Old_obj_Customer.Opening_Branch_Code).FirstOrDefault
                            If Not Old_objbranch Is Nothing Then
                                'Old_cmb_openingBranch.SetTextWithTextValue(Old_objbranch.FK_AML_BRANCH_CODE, Old_objbranch.BRANCH_NAME)
                            End If
                        End If

                        If Old_obj_Customer.Status IsNot Nothing And Not String.IsNullOrEmpty(Old_obj_Customer.Status) Then
                            Dim Old_objstatus = Old_objdbs.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = Old_obj_Customer.Status).FirstOrDefault
                            If Not Old_objstatus Is Nothing Then
                                'Old_cmb_Status.SetTextWithTextValue(Old_objstatus.Kode, Old_objstatus.Keterangan)
                            End If
                        End If

                    End Using


                    If Old_obj_Customer.isUpdateFromDataSource = True Then
                        Old_UpdateFromDataSource.Checked = True
                    End If
                    Dim statusrekening = goAML_CustomerBLL.GetStatusRekeningbyID(Old_obj_Customer.Status_Code)

                    If statusrekening IsNot Nothing Then
                        Old_txt_statusrekening.Text = statusrekening.Keterangan
                    End If

                    If Old_obj_Customer.FK_Customer_Type_ID = 1 Then
                        Old_txt_INDV_Title.Text = Old_obj_Customer.INDV_Title

                        INDV_gender = goAML_CustomerBLL.GetJenisKelamin(Old_obj_Customer.INDV_Gender)
                        If INDV_gender IsNot Nothing Then
                            Old_txt_INDV_Gender.Text = INDV_gender.Keterangan
                        End If
                        Old_txt_INDV_Last_Name.Text = Old_obj_Customer.INDV_Last_Name
                        If Old_obj_Customer.INDV_BirthDate.HasValue Then
                            Old_txt_INDV_Birthdate.Text = Old_obj_Customer.INDV_BirthDate.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            Old_txt_INDV_Birthdate.Text = ""
                        End If
                        Old_txt_INDV_Birth_place.Text = Old_obj_Customer.INDV_Birth_Place
                        Old_txt_INDV_Mothers_name.Text = Old_obj_Customer.INDV_Mothers_Name
                        Old_txt_INDV_Alias.Text = Old_obj_Customer.INDV_Alias
                        Old_txt_INDV_SSN.Text = Old_obj_Customer.INDV_SSN
                        Old_txt_INDV_Passport_number.Text = Old_obj_Customer.INDV_Passport_Number
                        If Not String.IsNullOrEmpty(Old_obj_Customer.INDV_Passport_Country) Then
                            Old_txt_INDV_Passport_country.Text = goAML_CustomerBLL.GetNamaNegaraByID(Old_obj_Customer.INDV_Passport_Country).Keterangan
                        Else
                            Old_txt_INDV_Passport_country.Text = ""
                        End If
                        Old_txt_INDV_ID_Number.Text = Old_obj_Customer.INDV_ID_Number

                        INDV_nationality1 = goAML_CustomerBLL.GetNamaNegaraByID(Old_obj_Customer.INDV_Nationality1)
                        If INDV_nationality1 IsNot Nothing Then
                            Old_txt_INDV_Nationality1.Text = INDV_nationality1.Keterangan
                        End If

                        INDV_nationality2 = goAML_CustomerBLL.GetNamaNegaraByID(Old_obj_Customer.INDV_Nationality2)
                        If INDV_nationality2 IsNot Nothing Then
                            Old_txt_INDV_Nationality2.Text = INDV_nationality2.Keterangan
                        End If

                        INDV_nationality3 = goAML_CustomerBLL.GetNamaNegaraByID(Old_obj_Customer.INDV_Nationality3)
                        If INDV_nationality3 IsNot Nothing Then
                            Old_txt_INDV_Nationality3.Text = INDV_nationality3.Keterangan
                        End If

                        INDV_residence = goAML_CustomerBLL.GetNamaNegaraByID(Old_obj_Customer.INDV_Residence)
                        If INDV_residence IsNot Nothing Then
                            Old_txt_INDV_Residence.Text = INDV_residence.Keterangan
                        End If

                        Old_txt_INDV_Tax_Number.Text = Old_obj_Customer.INDV_Tax_Number
                        Old_txt_INDV_Source_of_Wealth.Text = Old_obj_Customer.INDV_Source_of_Wealth

                        If Old_obj_Customer.INDV_Deceased = True Then
                            Old_cb_Deceased.Checked = True
                            Old_txt_Deceased_Date.Hidden = False
                            Old_txt_Deceased_Date.Text = Old_obj_Customer.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        End If
                        If Old_obj_Customer.INDV_Tax_Reg_Number = True Then
                            Old_cb_Tax.Checked = True
                        End If
                        Old_display_INDV_Email.Text = Old_obj_Customer.INDV_Email
                        Old_display_INDV_Email2.Text = Old_obj_Customer.INDV_Email2
                        Old_display_INDV_Email3.Text = Old_obj_Customer.INDV_Email3
                        Old_display_INDV_Email4.Text = Old_obj_Customer.INDV_Email4
                        Old_display_INDV_Email5.Text = Old_obj_Customer.INDV_Email5
                        Old_txt_INDV_Occupation.Text = Old_obj_Customer.INDV_Occupation
                        Old_txt_INDV_Employer_Name.Text = Old_obj_Customer.INDV_Employer_Name

                        '' Phone
                        'Old_objListgoAML_Ref_Phone = GoAMLBLL.goAML_CustomerBLL.GetVWCustomerPhones(Old_obj_Customer.PK_Customer_ID)
                        'Old_Store_INDV_Phones.DataSource = Old_objListgoAML_Ref_Phone.ToList()
                        Old_objListgoAML_Ref_Phone = objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone
                        Old_Store_INDV_Phones.DataSource = objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone
                        Old_Store_INDV_Phones.DataBind()

                        '' Address
                        'Dim Old_objlist_address = GoAMLBLL.goAML_CustomerBLL.GetVWCustomerAddresses(Old_obj_Customer.PK_Customer_ID)
                        'Old_Store_INDVD_Address.DataSource = Old_objlist_address
                        Old_objListgoAML_Ref_Address = objgoAML_CustomerDataBLL.objlistgoaml_Ref_Address
                        Old_Store_INDVD_Address.DataSource = objgoAML_CustomerDataBLL.objlistgoaml_Ref_Address
                        Old_Store_INDVD_Address.DataBind()

                        '' Identification
                        'Dim Old_objlist_Identification = GoAMLBLL.goAML_CustomerBLL.GetVWCustomerIdentifications(Old_obj_Customer.PK_Customer_ID)
                        'Old_Store_INDV_Identification.DataSource = Old_objlist_Identification
                        Old_objListgoAML_Ref_Identification = objgoAML_CustomerDataBLL.objListgoAML_Ref_Identification
                        Old_Store_INDV_Identification.DataSource = objgoAML_CustomerDataBLL.objListgoAML_Ref_Identification
                        Old_Store_INDV_Identification.DataBind()

                        '' Employer Phone
                        'Old_Store_Empoloyer_Phone.DataSource = GoAMLBLL.goAML_CustomerBLL.GetVWEmployerPhones(Old_obj_Customer.PK_Customer_ID).ToList()
                        Old_objListgoAML_Ref_EmployerPhone = objgoAML_CustomerDataBLL.objListgoAML_Ref_Employer_Phone
                        Old_Store_Empoloyer_Phone.DataSource = objgoAML_CustomerDataBLL.objListgoAML_Ref_Employer_Phone
                        Old_Store_Empoloyer_Phone.DataBind()

                        '' Employer Address
                        'Old_Store_Employer_Address.DataSource = GoAMLBLL.goAML_CustomerBLL.GetVWEmployerAddresses(Old_obj_Customer.PK_Customer_ID).ToList()
                        Old_objListgoAML_Ref_EmployerAddress = objgoAML_CustomerDataBLL.objlistgoaml_Ref_Employer_Address
                        Old_Store_Employer_Address.DataSource = objgoAML_CustomerDataBLL.objlistgoaml_Ref_Employer_Address
                        Old_Store_Employer_Address.DataBind()

                        ''Previous Name
                        'Old_Store_Customer_Previous_Name.DataSource = GoAMLBLL.goAML_CustomerBLL.GetPreviousName(Old_obj_Customer.CIF).ToList()
                        Old_Store_Customer_Previous_Name.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_PreviousName
                        Old_Store_Customer_Previous_Name.DataBind()

                        ''Email
                        'Old_Store_Customer_Email.DataSource = GoAMLBLL.goAML_CustomerBLL.GetEmail(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_Ref_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_vw_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(Old_obj_Customer.CIF).ToList()
                        Old_Store_Customer_Email.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email
                        Old_objListgoAML_Ref_Email = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email
                        Old_objListgoAML_vw_Email = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email
                        Old_Store_Customer_Email.DataBind()

                        ''Employment History
                        'Old_Store_Customer_Employment_History.DataSource = GoAMLBLL.goAML_CustomerBLL.GetEmploymentHistory(Old_obj_Customer.CIF).ToList()
                        Old_Store_Customer_Employment_History.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_EmploymentHistory
                        Old_Store_Customer_Employment_History.DataBind()

                        ''Person PEP
                        'Old_Store_Customer_Person_PEP.DataSource = GoAMLBLL.goAML_CustomerBLL.GetPEP(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_Ref_PersonPEP = GoAMLBLL.goAML_CustomerBLL.GetPEP(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_vw_PersonPEP = GoAMLBLL.goAML_CustomerBLL.GetPEP(Old_obj_Customer.CIF).ToList()
                        Old_Store_Customer_Person_PEP.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_PEP
                        Old_objListgoAML_Ref_PersonPEP = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_PEP
                        Old_objListgoAML_vw_PersonPEP = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_PEP
                        Old_Store_Customer_Person_PEP.DataBind()

                        ''Network Device
                        'Old_Store_Customer_Network_Device.DataSource = GoAMLBLL.goAML_CustomerBLL.GetNetworkDevice(Old_obj_Customer.CIF).ToList()
                        Old_Store_Customer_Network_Device.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_NetworkDevice
                        Old_Store_Customer_Network_Device.DataBind()

                        ''Social Media
                        'Old_Store_Customer_Social_Media.DataSource = GoAMLBLL.goAML_CustomerBLL.GetSocialMedia(Old_obj_Customer.CIF).ToList()
                        Old_Store_Customer_Social_Media.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_SocialMedia
                        Old_Store_Customer_Social_Media.DataBind()

                        ''Sanction
                        'Old_Store_Customer_Sanction.DataSource = GoAMLBLL.goAML_CustomerBLL.GetSanction(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_Ref_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_vw_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(Old_obj_Customer.CIF).ToList()
                        Old_Store_Customer_Sanction.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Sanction
                        Old_objListgoAML_Ref_Sanction = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Sanction
                        Old_objListgoAML_vw_Sanction = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Sanction
                        Old_Store_Customer_Sanction.DataBind()

                        ''Related Person
                        'Old_Store_Customer_RelatedPerson.DataSource = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_Ref_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_vw_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(Old_obj_Customer.CIF).ToList()
                        Old_Store_Customer_RelatedPerson.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson
                        Old_objListgoAML_Ref_RelatedPerson = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson
                        Old_objListgoAML_vw_RelatedPerson = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson
                        Old_Store_Customer_RelatedPerson.DataBind()

                        ''Additonal Information
                        'Old_Store_Customer_Additional_Info.DataSource = GoAMLBLL.goAML_CustomerBLL.GetAdditionalInformation(Old_obj_Customer.CIF).ToList()
                        Old_Store_Customer_Additional_Info.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_AdditionalInformation
                        Old_Store_Customer_Additional_Info.DataBind()

                        Old_panel_Corp.Visible = False
                        Old_GP_INDVD_Address.Hidden = False
                        'Old_panel_INDV.Visible = False
                    ElseIf Old_obj_Customer.FK_Customer_Type_ID = 2 Then
                        Old_txt_Corp_Name.Text = Old_obj_Customer.Corp_Name
                        Old_txt_Corp_Commercial_Name.Text = Old_obj_Customer.Corp_Commercial_Name


                        CORP_Incorporation_legal_form = goAML_CustomerBLL.GetBentukBadanUsahaByID(Old_obj_Customer.Corp_Incorporation_Legal_Form)
                        If CORP_Incorporation_legal_form IsNot Nothing Then
                            Old_txt_Corp_Incorporation_Legal_Form.Text = CORP_Incorporation_legal_form.Keterangan
                        Else
                            Old_txt_Corp_Incorporation_Legal_Form.Text = ""
                        End If


                        Old_txt_Corp_Incorporation_number.Text = Old_obj_Customer.Corp_Incorporation_Number
                        Old_txt_Corp_Business.Text = Old_obj_Customer.Corp_Business
                        Old_txt_Corp_Email.Text = Old_obj_Customer.Corp_Email
                        Old_txt_Corp_url.Text = Old_obj_Customer.Corp_Url
                        Old_txt_Corp_incorporation_state.Text = Old_obj_Customer.Corp_Incorporation_State

                        'Dedy Remarks 27082020 is nothing
                        'CORP_incorporation_country_code = goAML_CustomerBLL.GetNamaNegaraByID(Old_obj_Customer.Corp_Incorporation_Country_Code)
                        'Old_txt_Corp_Incorporation_Country_Code.Text = CORP_incorporation_country_code.Keterangan
                        'Dedy End Remarks 27082020

                        'Dedy Added 27082020 for is nothing
                        CORP_incorporation_country_code = goAML_CustomerBLL.GetNamaNegaraByID(Old_obj_Customer.Corp_Incorporation_Country_Code)
                        If (CORP_incorporation_country_code IsNot Nothing) Then
                            Old_txt_Corp_Incorporation_Country_Code.Text = CORP_incorporation_country_code.Keterangan
                        End If
                        'Dedy End Added 27082020 for is nothing


                        'If Not Old_obj_Customer.Corp_Role Is Nothing And Not Old_obj_Customer.Corp_Role = "" Then
                        '    CORP_Role = goAML_CustomerBLL.GetPartyRolebyID(Old_obj_Customer.Corp_Role)
                        '    Old_txt_Corp_Role.Text = CORP_Role.Keterangan
                        'Else
                        '    Old_txt_Corp_Role.Text = ""
                        'End If

                        If Old_obj_Customer.Corp_Incorporation_Date.HasValue Then
                            Old_txt_Corp_incorporation_date.Text = Old_obj_Customer.Corp_Incorporation_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            Old_txt_Corp_incorporation_date.Text = ""
                        End If
                        If Not Old_obj_Customer.Corp_Business_Closed = "False" Then
                            Old_cbTutup.Text = "Ya"
                        Else
                            Old_cbTutup.Text = "Tidak"
                        End If
                        If Old_obj_Customer.Corp_Date_Business_Closed.HasValue Then
                            Old_txt_Corp_date_business_closed.Text = Old_obj_Customer.Corp_Date_Business_Closed.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            Old_txt_Corp_date_business_closed.Text = ""
                            Old_txt_Corp_date_business_closed.Hidden = True
                        End If
                        Old_txt_Corp_tax_number.Text = Old_obj_Customer.Corp_Tax_Number
                        Old_txt_Corp_Comments.Text = Old_obj_Customer.Corp_Comments

                        Old_GP_Phone_Corp.Hidden = False
                        Old_GP_Address_Corp.Hidden = False

                        '' Phone
                        Old_objListgoAML_Ref_Phone = objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone
                        Old_Store_Phone_Corp.DataSource = objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone
                        Old_Store_Phone_Corp.DataBind()

                        '' Address
                        Dim Old_obj_list_address_Corp = objgoAML_CustomerDataBLL.objlistgoaml_Ref_Address
                        Old_objListgoAML_Ref_Address = Old_obj_list_address_Corp
                        Old_Store_Address_Corp.DataSource = Old_obj_list_address_Corp
                        Old_Store_Address_Corp.DataBind()

                        Old_gp_Director.Hidden = False
                        ''director
                        Old_store_Director.DataSource = goAML_CustomerBLL.GetVWCustomerDirector(Old_obj_Customer.CIF)
                        Old_store_Director.DataBind()

                        For Each item As Vw_Customer_Director In goAML_CustomerBLL.GetVWCustomerDirector(Old_obj_Customer.CIF)
                            Dim fk_entity = item.FK_Entity_ID
                            For Each itemx As Vw_Director_Addresses In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorAddresses(fk_entity)
                                Old_objListgoAML_Ref_Director_Address.Add(itemx)
                            Next

                            For Each itemx As Vw_Director_Phones In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorPhones(fk_entity)
                                Old_objListgoAML_Ref_Director_Phone.Add(itemx)
                            Next

                            For Each itemx As Vw_Director_Employer_Addresses In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorEmployerAddresses(fk_entity)
                                Old_objListgoAML_Ref_Director_Employer_Address.Add(itemx)
                            Next

                            For Each itemx As Vw_Director_Employer_Phones In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorEmployerPhones(fk_entity)
                                Old_objListgoAML_Ref_Director_Employer_Phone.Add(itemx)
                            Next

                            For Each itemx As Vw_Person_Identification In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorIdentifications(fk_entity)
                                Old_objListgoAML_vw_Customer_Identification_Director.Add(itemx)
                            Next
                        Next

                        Old_panel_INDV.Visible = False

                        ''Add By Septian, goAML v5.0.1, 24 January 2023
                        ''Corp Email
                        'Old_Store_corp_email.DataSource = GoAMLBLL.goAML_CustomerBLL.GetEmail(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_Ref_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_vw_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(Old_obj_Customer.CIF).ToList()
                        Old_Store_corp_email.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email
                        Old_objListgoAML_Ref_Email = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email
                        Old_objListgoAML_vw_Email = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email
                        Old_Store_corp_email.DataBind()

                        ''Corp URL
                        'Old_Store_Customer_Entity_URL.DataSource = GoAMLBLL.goAML_CustomerBLL.GetURL(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_Ref_EntityUrl = GoAMLBLL.goAML_CustomerBLL.GetURL(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_vw_EntityUrl = GoAMLBLL.goAML_CustomerBLL.GetURL(Old_obj_Customer.CIF).ToList()
                        Old_Store_Customer_Entity_URL.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_URL
                        Old_objListgoAML_Ref_EntityUrl = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_URL
                        Old_objListgoAML_vw_EntityUrl = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_URL
                        Old_Store_Customer_Entity_URL.DataBind()

                        ''Corp Entity Identification
                        'Old_Store_Customer_Entity_Identification.DataSource = GoAMLBLL.goAML_CustomerBLL.GetEntityIdentification(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_Ref_EntityIdentification = GoAMLBLL.goAML_CustomerBLL.GetEntityIdentification(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_vw_EntityIdentification = GoAMLBLL.goAML_CustomerBLL.GetEntityIdentification(Old_obj_Customer.CIF).ToList()
                        Old_Store_Customer_Entity_Identification.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_EntityIdentification
                        Old_objListgoAML_Ref_EntityIdentification = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_EntityIdentification
                        Old_objListgoAML_vw_EntityIdentification = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_EntityIdentification
                        Old_Store_Customer_Entity_Identification.DataBind()

                        ''Corp Network Device
                        'Old_StoreDirector_Network_Device.DataSource = GoAMLBLL.goAML_CustomerBLL.GetNetworkDevice(Old_obj_Customer.CIF).ToList()
                        Old_StoreDirector_Network_Device.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_NetworkDevice
                        Old_StoreDirector_Network_Device.DataBind()

                        ''Corp Sanction
                        'Old_Store_corp_sanction.DataSource = GoAMLBLL.goAML_CustomerBLL.GetSanction(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_Ref_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_vw_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(Old_obj_Customer.CIF).ToList()
                        Old_Store_corp_sanction.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Sanction
                        Old_objListgoAML_Ref_Sanction = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Sanction
                        Old_objListgoAML_vw_Sanction = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Sanction
                        Old_Store_corp_sanction.DataBind()

                        ''Corp Related Person
                        'Old_StoreCorp_Related_person.DataSource = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_Ref_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_vw_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(Old_obj_Customer.CIF).ToList()
                        Old_StoreCorp_Related_person.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson
                        Old_objListgoAML_Ref_RelatedPerson = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson
                        Old_objListgoAML_vw_RelatedPerson = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson
                        Old_StoreCorp_Related_person.DataBind()

                        ''Corp Related Entities
                        'Old_Store_Related_Entities.DataSource = GoAMLBLL.goAML_CustomerBLL.GetRelatedEntities(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_Ref_RelatedEntities = GoAMLBLL.goAML_CustomerBLL.GetRelatedEntities(Old_obj_Customer.CIF).ToList()
                        'Old_objListgoAML_vw_RelatedEntities = GoAMLBLL.goAML_CustomerBLL.GetRelatedEntities(Old_obj_Customer.CIF).ToList()
                        Old_Store_Related_Entities.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedEntities
                        Old_objListgoAML_Ref_RelatedEntities = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedEntities
                        Old_objListgoAML_vw_RelatedEntities = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedEntities
                        Old_Store_Related_Entities.DataBind()

                        ''Corp Additonal Information
                        'Old_Store_Customer_Corp_Additional_Info.DataSource = GoAMLBLL.goAML_CustomerBLL.GetAdditionalInformation(Old_obj_Customer.CIF).ToList()
                        Old_Store_Customer_Corp_Additional_Info.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_AdditionalInformation
                        Old_Store_Customer_Corp_Additional_Info.DataBind()

                        ''end add by septian
                    End If
                End If

                Dim Old_objCustomer2 = objgoAML_CustomerDataBLL.objgoAML_Ref_Customer
                Dim qNamaNegara = "SELECT TOP 1 * FROM goAML_Ref_Nama_Negara WHERE Kode = '" + Old_objCustomer2.Country_Of_Birth + "'"
                Dim qEntityStatus = "SELECT TOP 1 * FROM goAML_Ref_Entity_Status WHERE Kode = '" + Old_objCustomer2.Entity_Status + "'"
                Dim refNamaNegara As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, qNamaNegara)
                Dim refEntityStatus As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, qEntityStatus)

                'Old_txt_INDV_Foreign_Full_Name.Text = Old_objCustomer2.Full_Name_Frn
                If refNamaNegara IsNot Nothing Then
                    Dim refNamaNegara_Kode = refNamaNegara("Kode").ToString()
                    Dim refNamaNegara_Keterangan = refNamaNegara("Keterangan").ToString()
                    'Old_cmb_INDV_Country_Of_Birth.SetTextWithTextValue(refNamaNegara_Kode, refNamaNegara_Keterangan)
                    'Old_txt_INDV_Residence_Since.Text = Old_objCustomer2.Residence_Since.Value.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    Old_cb_INDV_Is_Protected.Checked = Old_objCustomer2.Is_Protected
                End If
                Old_txt_INDV_Comments.Text = Old_objCustomer2.Comments

                If refEntityStatus IsNot Nothing Then
                    Dim refEntityStatus_Kode = refEntityStatus("Kode").ToString()
                    Dim refEntityStatus_Keterangan = refEntityStatus("Keterangan").ToString()
                    Old_cmb_Corp_Entity_Status.SetTextWithTextValue(refEntityStatus_Kode, refEntityStatus_Keterangan)
                    Old_txt_Corp_Entity_Status_Date.Text = Old_objCustomer2.Entity_Status_Date.Value.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                End If

            Catch ex As Exception
                Throw ex
            End Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataNew(objData As String)
        Try

            Dim objgoAML_CustomerDataBLL As GoAMLBLL.goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objData, GetType(GoAMLBLL.goAML_CustomerDataBLL))
            ObjDataNew = objgoAML_CustomerDataBLL
            'Dedy added 31082020 aktif is hidden
            New_txt_statusrekening.Hidden = True
            'Dedy end added 31082020
            ClearSessionNew()
            GridPanelSettingNew()
            Maximizeable()
            'Dim strid As String = GetId()
            Dim strModuleid As String = Request.Params("ModuleID")
            Dim INDV_gender As GoAMLDAL.goAML_Ref_Jenis_Kelamin = Nothing
            Dim INDV_statusCode As GoAMLDAL.goAML_Ref_Status_Rekening = Nothing
            Dim INDV_nationality1 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_nationality2 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_nationality3 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_residence As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim CORP_Incorporation_legal_form As GoAMLDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
            Dim CORP_incorporation_country_code As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
            Dim CORP_Role As GoAMLDAL.goAML_Ref_Party_Role = Nothing

            Try
                New_obj_Customer = objgoAML_CustomerDataBLL.objgoAML_Ref_Customer
                'New_obj_Customer = goAML_CustomerBLL.GetCustomerbyCIF(strid)

                'Dedy added 01092020
                If New_obj_Customer IsNot Nothing Then

                    New_txt_CIF.Text = New_obj_Customer.CIF
                    New_txt_GCN.Text = New_obj_Customer.GCN
                    If New_obj_Customer.isGCNPrimary = True Then
                        New_GCNPrimary.Checked = True
                    End If
                    If New_obj_Customer.Closing_Date IsNot Nothing Then
                        New_TextFieldClosingDate.Text = New_obj_Customer.Closing_Date.Value.ToString("dd-MMM-yyyy")
                    End If
                    If New_obj_Customer.Opening_Date IsNot Nothing Then
                        New_TextFieldOpeningDate.Text = New_obj_Customer.Opening_Date.Value.ToString("dd-MMM-yyyy")
                    End If
                    If New_obj_Customer.Comments IsNot Nothing Then
                        New_TextFieldComments.Text = New_obj_Customer.Comments
                    End If


                    Using New_objdbs As New GoAMLDAL.GoAMLEntities
                        If New_obj_Customer.Opening_Branch_Code IsNot Nothing And Not String.IsNullOrEmpty(New_obj_Customer.Opening_Branch_Code) Then
                            Dim New_objbranch = New_objdbs.AML_BRANCH.Where(Function(x) x.FK_AML_BRANCH_CODE = New_obj_Customer.Opening_Branch_Code).FirstOrDefault
                            If Not New_objbranch Is Nothing Then
                                New_cmb_openingBranch.SetTextWithTextValue(New_objbranch.FK_AML_BRANCH_CODE, New_objbranch.BRANCH_NAME)
                            End If
                        End If

                        If New_obj_Customer.Status IsNot Nothing And Not String.IsNullOrEmpty(New_obj_Customer.Status) Then
                            Dim New_objstatus = New_objdbs.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = New_obj_Customer.Status).FirstOrDefault
                            If Not New_objstatus Is Nothing Then
                                New_cmb_Status.SetTextWithTextValue(New_objstatus.Kode, New_objstatus.Keterangan)
                            End If
                        End If

                    End Using

                    If New_obj_Customer.isUpdateFromDataSource = True Then
                        New_UpdateFromDataSource.Checked = True
                    End If
                    Dim statusrekening = goAML_CustomerBLL.GetStatusRekeningbyID(New_obj_Customer.Status_Code)

                    If statusrekening IsNot Nothing Then
                        New_txt_statusrekening.Text = statusrekening.Keterangan
                    End If

                    ' TODO: cek kenapa data identification untuk related entity tida diload
                    If New_obj_Customer.FK_Customer_Type_ID = 1 Then
                        New_txt_INDV_Title.Text = New_obj_Customer.INDV_Title

                        INDV_gender = goAML_CustomerBLL.GetJenisKelamin(New_obj_Customer.INDV_Gender)
                        If INDV_gender IsNot Nothing Then
                            New_txt_INDV_Gender.Text = INDV_gender.Keterangan
                        End If
                        New_txt_INDV_Last_Name.Text = New_obj_Customer.INDV_Last_Name
                        If New_obj_Customer.INDV_BirthDate.HasValue Then
                            New_txt_INDV_Birthdate.Text = New_obj_Customer.INDV_BirthDate.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            New_txt_INDV_Birthdate.Text = ""
                        End If
                        New_txt_INDV_Birth_place.Text = New_obj_Customer.INDV_Birth_Place
                        New_txt_INDV_Mothers_name.Text = New_obj_Customer.INDV_Mothers_Name
                        New_txt_INDV_Alias.Text = New_obj_Customer.INDV_Alias
                        New_txt_INDV_SSN.Text = New_obj_Customer.INDV_SSN
                        New_txt_INDV_Passport_number.Text = New_obj_Customer.INDV_Passport_Number
                        If Not String.IsNullOrEmpty(New_obj_Customer.INDV_Passport_Country) Then
                            New_txt_INDV_Passport_country.Text = goAML_CustomerBLL.GetNamaNegaraByID(New_obj_Customer.INDV_Passport_Country).Keterangan
                        Else
                            New_txt_INDV_Passport_country.Text = ""
                        End If
                        New_txt_INDV_ID_Number.Text = New_obj_Customer.INDV_ID_Number

                        INDV_nationality1 = goAML_CustomerBLL.GetNamaNegaraByID(New_obj_Customer.INDV_Nationality1)
                        If INDV_nationality1 IsNot Nothing Then
                            New_txt_INDV_Nationality1.Text = INDV_nationality1.Keterangan
                        End If

                        INDV_nationality2 = goAML_CustomerBLL.GetNamaNegaraByID(New_obj_Customer.INDV_Nationality2)
                        If INDV_nationality2 IsNot Nothing Then
                            New_txt_INDV_Nationality2.Text = INDV_nationality2.Keterangan
                        End If

                        INDV_nationality3 = goAML_CustomerBLL.GetNamaNegaraByID(New_obj_Customer.INDV_Nationality3)
                        If INDV_nationality3 IsNot Nothing Then
                            New_txt_INDV_Nationality3.Text = INDV_nationality3.Keterangan
                        End If

                        INDV_residence = goAML_CustomerBLL.GetNamaNegaraByID(New_obj_Customer.INDV_Residence)
                        If INDV_residence IsNot Nothing Then
                            New_txt_INDV_Residence.Text = INDV_residence.Keterangan
                        End If

                        New_txt_INDV_Tax_Number.Text = New_obj_Customer.INDV_Tax_Number
                        New_txt_INDV_Source_of_Wealth.Text = New_obj_Customer.INDV_Source_of_Wealth

                        If New_obj_Customer.INDV_Deceased = True Then
                            New_cb_Deceased.Checked = True
                            New_txt_Deceased_Date.Hidden = False
                            New_txt_Deceased_Date.Text = IIf(New_obj_Customer.INDV_Deceased_Date Is Nothing, "", New_obj_Customer.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat))
                        End If
                        If New_obj_Customer.INDV_Tax_Reg_Number = True Then
                            New_cb_Tax.Checked = True
                        End If
                        New_display_INDV_Email.Text = New_obj_Customer.INDV_Email
                        New_display_INDV_Email2.Text = New_obj_Customer.INDV_Email2
                        New_display_INDV_Email3.Text = New_obj_Customer.INDV_Email3
                        New_display_INDV_Email4.Text = New_obj_Customer.INDV_Email4
                        New_display_INDV_Email5.Text = New_obj_Customer.INDV_Email5
                        New_txt_INDV_Occupation.Text = New_obj_Customer.INDV_Occupation
                        New_txt_INDV_Employer_Name.Text = New_obj_Customer.INDV_Employer_Name

                        '' Phone
                        New_objListgoAML_Ref_Phone = objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone
                        New_Store_INDV_Phones.DataSource = objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone
                        New_Store_INDV_Phones.DataBind()

                        '' Address
                        New_objListgoAML_Ref_Address = objgoAML_CustomerDataBLL.objlistgoaml_Ref_Address
                        New_Store_INDVD_Address.DataSource = objgoAML_CustomerDataBLL.objlistgoaml_Ref_Address
                        New_Store_INDVD_Address.DataBind()

                        '' Identification
                        New_objListgoAML_Ref_Identification = objgoAML_CustomerDataBLL.objListgoAML_Ref_Identification
                        New_Store_INDV_Identification.DataSource = objgoAML_CustomerDataBLL.objListgoAML_Ref_Identification
                        New_Store_INDV_Identification.DataBind()

                        '' Employer Phone
                        New_objListgoAML_Ref_EmployerPhone = objgoAML_CustomerDataBLL.objListgoAML_Ref_Employer_Phone
                        New_Store_Empoloyer_Phone.DataSource = objgoAML_CustomerDataBLL.objListgoAML_Ref_Employer_Phone
                        New_Store_Empoloyer_Phone.DataBind()

                        '' Employer Address
                        New_objListgoAML_Ref_EmployerAddress = objgoAML_CustomerDataBLL.objlistgoaml_Ref_Employer_Address
                        New_Store_Employer_Address.DataSource = objgoAML_CustomerDataBLL.objlistgoaml_Ref_Employer_Address
                        New_Store_Employer_Address.DataBind()

                        ''Previous Name
                        'New_Store_Customer_Previous_Name.DataSource = GoAMLBLL.goAML_CustomerBLL.GetPreviousName(New_obj_Customer.CIF).ToList()
                        New_Store_Customer_Previous_Name.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_PreviousName
                        New_Store_Customer_Previous_Name.DataBind()

                        ''Email
                        'New_Store_Customer_Email.DataSource = GoAMLBLL.goAML_CustomerBLL.GetEmail(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_Ref_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_vw_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(New_obj_Customer.CIF).ToList()
                        New_Store_Customer_Email.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email
                        New_objListgoAML_Ref_Email = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email
                        New_objListgoAML_vw_Email = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email
                        New_Store_Customer_Email.DataBind()

                        ''Employment History
                        'New_Store_Customer_Employment_History.DataSource = GoAMLBLL.goAML_CustomerBLL.GetEmploymentHistory(New_obj_Customer.CIF).ToList()
                        New_Store_Customer_Employment_History.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_EmploymentHistory
                        New_Store_Customer_Employment_History.DataBind()

                        ''Person PEP
                        'New_Store_Customer_Person_PEP.DataSource = GoAMLBLL.goAML_CustomerBLL.GetPEP(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_Ref_PersonPEP = GoAMLBLL.goAML_CustomerBLL.GetPEP(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_vw_PersonPEP = GoAMLBLL.goAML_CustomerBLL.GetPEP(New_obj_Customer.CIF).ToList()
                        New_Store_Customer_Person_PEP.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_PEP
                        New_objListgoAML_Ref_PersonPEP = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_PEP
                        New_objListgoAML_vw_PersonPEP = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_PEP
                        New_Store_Customer_Person_PEP.DataBind()

                        ''Network Device
                        'New_Store_Customer_Network_Device.DataSource = GoAMLBLL.goAML_CustomerBLL.GetNetworkDevice(New_obj_Customer.CIF).ToList()
                        New_Store_Customer_Network_Device.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_NetworkDevice
                        New_Store_Customer_Network_Device.DataBind()

                        ''Social Media
                        'New_Store_Customer_Social_Media.DataSource = GoAMLBLL.goAML_CustomerBLL.GetSocialMedia(New_obj_Customer.CIF).ToList()
                        New_Store_Customer_Social_Media.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_SocialMedia
                        New_Store_Customer_Social_Media.DataBind()

                        ''Sanction
                        'New_Store_Customer_Sanction.DataSource = GoAMLBLL.goAML_CustomerBLL.GetSanction(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_Ref_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_vw_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(New_obj_Customer.CIF).ToList()
                        New_Store_Customer_Sanction.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Sanction
                        New_objListgoAML_Ref_Sanction = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Sanction
                        New_objListgoAML_vw_Sanction = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Sanction
                        New_Store_Customer_Sanction.DataBind()

                        ''Related Person
                        'New_Store_Customer_RelatedPerson.DataSource = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_Ref_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_vw_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(New_obj_Customer.CIF).ToList()
                        New_Store_Customer_RelatedPerson.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson
                        New_objListgoAML_Ref_RelatedPerson = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson
                        New_objListgoAML_vw_RelatedPerson = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson
                        New_Store_Customer_RelatedPerson.DataBind()

                        ''Additonal Information
                        'New_Store_Customer_Additional_Info.DataSource = GoAMLBLL.goAML_CustomerBLL.GetAdditionalInformation(New_obj_Customer.CIF).ToList()
                        New_Store_Customer_Additional_Info.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_AdditionalInformation
                        New_Store_Customer_Additional_Info.DataBind()

                        New_panel_Corp.Visible = False
                        New_GP_INDVD_Address.Hidden = False
                        'New_panel_INDV.Visible = False
                    ElseIf New_obj_Customer.FK_Customer_Type_ID = 2 Then
                        New_txt_Corp_Name.Text = New_obj_Customer.Corp_Name
                        New_txt_Corp_Commercial_Name.Text = New_obj_Customer.Corp_Commercial_Name


                        CORP_Incorporation_legal_form = goAML_CustomerBLL.GetBentukBadanUsahaByID(New_obj_Customer.Corp_Incorporation_Legal_Form)
                        If CORP_Incorporation_legal_form IsNot Nothing Then
                            New_txt_Corp_Incorporation_Legal_Form.Text = CORP_Incorporation_legal_form.Keterangan
                        Else
                            New_txt_Corp_Incorporation_Legal_Form.Text = ""
                        End If


                        New_txt_Corp_Incorporation_number.Text = New_obj_Customer.Corp_Incorporation_Number
                        New_txt_Corp_Business.Text = New_obj_Customer.Corp_Business
                        ' 2023-10-11, Nael: Uncomment field corp email dan corp url
                        New_txt_Corp_Email.Text = New_obj_Customer.Corp_Email
                        New_txt_Corp_url.Text = New_obj_Customer.Corp_Url
                        ' =========================================================
                        New_txt_Corp_incorporation_state.Text = New_obj_Customer.Corp_Incorporation_State

                        'Dedy Remarks 27082020 is nothing
                        'CORP_incorporation_country_code = goAML_CustomerBLL.GetNamaNegaraByID(New_obj_Customer.Corp_Incorporation_Country_Code)
                        'New_txt_Corp_Incorporation_Country_Code.Text = CORP_incorporation_country_code.Keterangan
                        'Dedy End Remarks 27082020

                        'Dedy Added 27082020 for is nothing
                        CORP_incorporation_country_code = goAML_CustomerBLL.GetNamaNegaraByID(New_obj_Customer.Corp_Incorporation_Country_Code)
                        If (CORP_incorporation_country_code IsNot Nothing) Then
                            New_txt_Corp_Incorporation_Country_Code.Text = CORP_incorporation_country_code.Keterangan
                        End If
                        'Dedy End Added 27082020 for is nothing


                        'If Not New_obj_Customer.Corp_Role Is Nothing And Not New_obj_Customer.Corp_Role = "" Then
                        '    CORP_Role = goAML_CustomerBLL.GetPartyRolebyID(New_obj_Customer.Corp_Role)
                        '    New_txt_Corp_Role.Text = CORP_Role.Keterangan
                        'Else
                        '    New_txt_Corp_Role.Text = ""
                        'End If

                        If New_obj_Customer.Corp_Incorporation_Date.HasValue Then
                            New_txt_Corp_incorporation_date.Text = New_obj_Customer.Corp_Incorporation_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            New_txt_Corp_incorporation_date.Text = ""
                        End If
                        If Not New_obj_Customer.Corp_Business_Closed = "False" Then
                            New_cbTutup.Text = "Ya"
                        Else
                            New_cbTutup.Text = "Tidak"
                        End If
                        If New_obj_Customer.Corp_Date_Business_Closed.HasValue Then
                            New_txt_Corp_date_business_closed.Text = New_obj_Customer.Corp_Date_Business_Closed.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            New_txt_Corp_date_business_closed.Text = ""
                            New_txt_Corp_date_business_closed.Hidden = True
                        End If
                        New_txt_Corp_tax_number.Text = New_obj_Customer.Corp_Tax_Number
                        New_txt_Corp_Comments.Text = New_obj_Customer.Corp_Comments

                        New_GP_Phone_Corp.Hidden = False
                        New_GP_Address_Corp.Hidden = False

                        '' Phone
                        'New_objListgoAML_Ref_Phone = goAML_CustomerBLL.GetVWCustomerPhones(New_obj_Customer.PK_Customer_ID)
                        New_objListgoAML_Ref_Phone = objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone
                        New_Store_Phone_Corp.DataSource = goAML_Customer_Service.PhoneService.GenerateDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone)
                        New_Store_Phone_Corp.DataBind()

                        '' Address
                        'Dim New_obj_list_address_Corp = goAML_CustomerBLL.GetVWCustomerAddresses(New_obj_Customer.PK_Customer_ID)
                        New_objListgoAML_Ref_Address = objgoAML_CustomerDataBLL.objlistgoaml_Ref_Address
                        New_Store_Address_Corp.DataSource = goAML_Customer_Service.AddressService.GenerateDataTable(objgoAML_CustomerDataBLL.objlistgoaml_Ref_Address)
                        New_Store_Address_Corp.DataBind()

                        New_gp_Director.Hidden = False
                        ''director
                        New_store_Director.DataSource = objgoAML_CustomerDataBLL.objListgoAML_Ref_Director
                        New_store_Director.DataBind()

                        'For Each item As Vw_Customer_Director In goAML_CustomerBLL.GetVWCustomerDirector(New_obj_Customer.CIF)
                        '    Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                        '    For Each itemx As Vw_Director_Addresses In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorAddresses(pk_director)
                        '        New_objListgoAML_Ref_Director_Address.Add(itemx)
                        '    Next

                        '    For Each itemx As Vw_Director_Phones In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorPhones(pk_director)
                        '        New_objListgoAML_Ref_Director_Phone.Add(itemx)
                        '    Next

                        '    For Each itemx As Vw_Director_Employer_Addresses In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorEmployerAddresses(pk_director)
                        '        New_objListgoAML_Ref_Director_Employer_Address.Add(itemx)
                        '    Next

                        '    For Each itemx As Vw_Director_Employer_Phones In GoAMLBLL.goAML_CustomerBLL.GetVWDirectorEmployerPhones(pk_director)
                        '        New_objListgoAML_Ref_Director_Employer_Phone.Add(itemx)
                        '    Next

                        '    'For Each itemx As Vw_Person_Identification In ObjDataNew.objListgoAML_Ref_Identification_Director
                        '    '    New_objListgoAML_vw_Customer_Identification_Director.Add(itemx)
                        '    'Next

                        'Next

                        New_objListgoAML_vw_Customer_Identification_Director = ObjDataNew.objListgoAML_Ref_Identification_Director.Select(Function(x) goAML_Person_Identification_Mapper.CreateView(x)).ToList()

                        New_panel_INDV.Visible = False

                        ''Add By Septian, goAML v5.0.1, 24 January 2023
                        ''Corp Email
                        'New_Store_corp_email.DataSource = GoAMLBLL.goAML_CustomerBLL.GetEmail(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_Ref_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_vw_Email = GoAMLBLL.goAML_CustomerBLL.GetEmail(New_obj_Customer.CIF).ToList()
                        New_Store_corp_email.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email
                        New_objListgoAML_Ref_Email = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email
                        New_objListgoAML_vw_Email = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email
                        New_Store_corp_email.DataBind()

                        ''Corp URL
                        'New_Store_Customer_Entity_URL.DataSource = GoAMLBLL.goAML_CustomerBLL.GetURL(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_Ref_EntityUrl = GoAMLBLL.goAML_CustomerBLL.GetURL(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_vw_EntityUrl = GoAMLBLL.goAML_CustomerBLL.GetURL(New_obj_Customer.CIF).ToList()
                        New_Store_Customer_Entity_URL.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_URL
                        New_objListgoAML_Ref_EntityUrl = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_URL
                        New_objListgoAML_vw_EntityUrl = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_URL
                        New_Store_Customer_Entity_URL.DataBind()

                        ''Corp Entity Identification
                        'New_Store_Customer_Entity_Identification.DataSource = GoAMLBLL.goAML_CustomerBLL.GetEntityIdentification(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_Ref_EntityIdentification = GoAMLBLL.goAML_CustomerBLL.GetEntityIdentification(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_vw_EntityIdentification = GoAMLBLL.goAML_CustomerBLL.GetEntityIdentification(New_obj_Customer.CIF).ToList()

                        ' 2023-10-06, Nael: Mengubah objList goAml entity identification jadi data table
                        Dim listEntityId = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_EntityIdentification
                        New_Store_Customer_Entity_Identification.DataSource = goAML_Customer_Service.EntityIdentificationService.GenerateDataTable(listEntityId)
                        New_objListgoAML_Ref_EntityIdentification = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_EntityIdentification
                        New_objListgoAML_vw_EntityIdentification = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_EntityIdentification
                        New_Store_Customer_Entity_Identification.DataBind()

                        ''Corp Network Device
                        'New_StoreDirector_Network_Device.DataSource = GoAMLBLL.goAML_CustomerBLL.GetNetworkDevice(New_obj_Customer.CIF).ToList()
                        New_StoreDirector_Network_Device.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_NetworkDevice
                        New_StoreDirector_Network_Device.DataBind()

                        ''Corp Sanction
                        'New_Store_corp_sanction.DataSource = GoAMLBLL.goAML_CustomerBLL.GetSanction(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_Ref_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_vw_Sanction = GoAMLBLL.goAML_CustomerBLL.GetSanction(New_obj_Customer.CIF).ToList()
                        New_Store_corp_sanction.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Sanction
                        New_objListgoAML_Ref_Sanction = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Sanction
                        New_objListgoAML_vw_Sanction = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Sanction
                        New_Store_corp_sanction.DataBind()

                        ''Corp Related Person
                        'New_StoreCorp_Related_person.DataSource = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_Ref_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_vw_RelatedPerson = GoAMLBLL.goAML_CustomerBLL.GetRelatedPerson(New_obj_Customer.CIF).ToList()
                        New_StoreCorp_Related_person.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson
                        New_objListgoAML_Ref_RelatedPerson = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson
                        New_objListgoAML_vw_RelatedPerson = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson
                        New_StoreCorp_Related_person.DataBind()

                        ''Corp Related Entities
                        'New_Store_Related_Entities.DataSource = GoAMLBLL.goAML_CustomerBLL.GetRelatedEntities(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_Ref_RelatedEntities = GoAMLBLL.goAML_CustomerBLL.GetRelatedEntities(New_obj_Customer.CIF).ToList()
                        'New_objListgoAML_vw_RelatedEntities = GoAMLBLL.goAML_CustomerBLL.GetRelatedEntities(New_obj_Customer.CIF).ToList()
                        New_Store_Related_Entities.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedEntities
                        New_objListgoAML_Ref_RelatedEntities = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedEntities
                        New_objListgoAML_vw_RelatedEntities = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedEntities
                        New_Store_Related_Entities.DataBind()

                        ''Corp Additonal Information
                        'New_Store_Customer_Corp_Additional_Info.DataSource = GoAMLBLL.goAML_CustomerBLL.GetAdditionalInformation(New_obj_Customer.CIF).ToList()
                        New_Store_Customer_Corp_Additional_Info.DataSource = objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_AdditionalInformation
                        New_Store_Customer_Corp_Additional_Info.DataBind()

                        ''end add by septian
                    End If
                End If

                Dim New_objCustomer2 = objgoAML_CustomerDataBLL.objgoAML_Ref_Customer
                Dim qNamaNegara = "SELECT TOP 1 * FROM goAML_Ref_Nama_Negara WHERE Kode = '" + New_objCustomer2.Country_Of_Birth + "'"
                Dim qEntityStatus = "SELECT TOP 1 * FROM goAML_Ref_Entity_Status WHERE Kode = '" + New_objCustomer2.Entity_Status + "'"
                Dim refNamaNegara As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, qNamaNegara)
                Dim refEntityStatus As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, qEntityStatus)

                'New_txt_INDV_Foreign_Full_Name.Text = New_objCustomer2.Full_Name_Frn
                If refNamaNegara IsNot Nothing Then
                    Dim refNamaNegara_Kode = refNamaNegara("Kode").ToString()
                    Dim refNamaNegara_Keterangan = refNamaNegara("Keterangan").ToString()
                    'New_cmb_INDV_Country_Of_Birth.SetTextWithTextValue(refNamaNegara_Kode, refNamaNegara_Keterangan)
                    'New_txt_INDV_Residence_Since.Text = New_objCustomer2.Residence_Since.Value.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    New_cb_INDV_Is_Protected.Checked = New_objCustomer2.Is_Protected
                End If
                New_txt_INDV_Comments.Text = New_objCustomer2.INDV_Comments ' 2023-11-07, Nael

                If refEntityStatus IsNot Nothing Then
                    Dim refEntityStatus_Kode = refEntityStatus("Kode").ToString()
                    Dim refEntityStatus_Keterangan = refEntityStatus("Keterangan").ToString()
                    New_cmb_Corp_Entity_Status.SetTextWithTextValue(refEntityStatus_Kode, refEntityStatus_Keterangan)
                    New_txt_Corp_Entity_Status_Date.Text = New_objCustomer2.Entity_Status_Date.Value.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                End If

            Catch ex As Exception
                Throw ex
            End Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub Maximizeable()
        WindowDetailDirector.Maximizable = True
        WindowDetail.Maximizable = True
        WindowDetailAddress.Maximizable = True
        WindowDetailIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True

        'START: 2023-10-14, Nael
        WindowDetail_RelatedEntities.Maximizable = True
        WindowDetail_EntityIdentification.Maximizable = True
        WindowDetail_RelatedPerson.Maximizable = True
        WindowDetail_Person_PEP.Maximizable = True
        WindowDetail_Sanction.Maximizable = True
        WindowDetail_Email.Maximizable = True
        'END: 2023-10-14, Nael

    End Sub
    Sub ClearSessionNew()
        New_objListgoAML_Ref_Director_Phone = New List(Of Vw_Director_Phones)
        New_objListgoAML_Ref_Director_Address = New List(Of Vw_Director_Addresses)
        New_objListgoAML_Ref_Director_Employer_Phone = New List(Of Vw_Director_Employer_Phones)
        New_objListgoAML_Ref_Director_Employer_Address = New List(Of Vw_Director_Employer_Addresses)
        New_objListgoAML_Ref_Identification_Director = New List(Of goAML_Person_Identification)
        New_objListgoAML_vw_Customer_Identification_Director = New List(Of Vw_Person_Identification)
        New_objListgoAML_Ref_Phone = Nothing
        New_objListgoAML_Ref_Address = Nothing

        'add goAML 5.0.1, Septian, 25 Jan 2023
        New_obj_Customer2 = New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2()
        New_objListgoAML_Ref_Previous_Name = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        New_objListgoAML_vw_Previous_Name = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)

        New_objListgoAML_Ref_Email = New List(Of DataModel.goAML_Ref_Customer_Email)
        New_objListgoAML_vw_Email = New List(Of DataModel.goAML_Ref_Customer_Email)

        New_objListgoAML_Ref_EmploymentHistory = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        New_objListgoAML_vw_EmploymentHistory = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)

        New_objListgoAML_Ref_PersonPEP = New List(Of DataModel.goAML_Ref_Customer_PEP)
        New_objListgoAML_vw_PersonPEP = New List(Of DataModel.goAML_Ref_Customer_PEP)

        New_objListgoAML_Ref_NetworkDevice = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        New_objListgoAML_vw_NetworkDevice = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)

        New_objListgoAML_Ref_SocialMedia = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        New_objListgoAML_vw_SocialMedia = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)

        New_objListgoAML_Ref_Sanction = New List(Of DataModel.goAML_Ref_Customer_Sanction)
        New_objListgoAML_vw_Sanction = New List(Of DataModel.goAML_Ref_Customer_Sanction)

        New_objListgoAML_Ref_RelatedPerson = New List(Of DataModel.goAML_Ref_Customer_Related_Person)
        New_objListgoAML_vw_RelatedPerson = New List(Of DataModel.goAML_Ref_Customer_Related_Person)

        New_objListgoAML_Ref_AdditionalInfo = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        New_objListgoAML_vw_AdditionalInfo = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)

        New_objListgoAML_Ref_EntityUrl = New List(Of DataModel.goAML_Ref_Customer_URL)
        New_objListgoAML_vw_EntityUrl = New List(Of DataModel.goAML_Ref_Customer_URL)

        New_objListgoAML_Ref_EntityIdentification = New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        New_objListgoAML_vw_EntityIdentification = New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)

        New_objListgoAML_Ref_RelatedEntities = New List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        New_objListgoAML_vw_RelatedEntities = New List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        'End Add
    End Sub
    Sub ClearSessionOld()
        Old_objListgoAML_Ref_Director_Phone = New List(Of Vw_Director_Phones)
        Old_objListgoAML_Ref_Director_Address = New List(Of Vw_Director_Addresses)
        Old_objListgoAML_Ref_Director_Employer_Phone = New List(Of Vw_Director_Employer_Phones)
        Old_objListgoAML_Ref_Director_Employer_Address = New List(Of Vw_Director_Employer_Addresses)
        Old_objListgoAML_Ref_Identification_Director = New List(Of goAML_Person_Identification)
        Old_objListgoAML_vw_Customer_Identification_Director = New List(Of Vw_Person_Identification)
        Old_objListgoAML_Ref_Phone = Nothing
        Old_objListgoAML_Ref_Address = Nothing

        'add goAML 5.0.1, Septian, 25 Jan 2023
        Old_obj_Customer2 = New GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2()
        Old_objListgoAML_Ref_Previous_Name = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        Old_objListgoAML_vw_Previous_Name = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)

        Old_objListgoAML_Ref_Email = New List(Of DataModel.goAML_Ref_Customer_Email)
        Old_objListgoAML_vw_Email = New List(Of DataModel.goAML_Ref_Customer_Email)

        Old_objListgoAML_Ref_EmploymentHistory = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        Old_objListgoAML_vw_EmploymentHistory = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)

        Old_objListgoAML_Ref_PersonPEP = New List(Of DataModel.goAML_Ref_Customer_PEP)
        Old_objListgoAML_vw_PersonPEP = New List(Of DataModel.goAML_Ref_Customer_PEP)

        Old_objListgoAML_Ref_NetworkDevice = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        Old_objListgoAML_vw_NetworkDevice = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)

        Old_objListgoAML_Ref_SocialMedia = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        Old_objListgoAML_vw_SocialMedia = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)

        Old_objListgoAML_Ref_Sanction = New List(Of DataModel.goAML_Ref_Customer_Sanction)
        Old_objListgoAML_vw_Sanction = New List(Of DataModel.goAML_Ref_Customer_Sanction)

        Old_objListgoAML_Ref_RelatedPerson = New List(Of DataModel.goAML_Ref_Customer_Related_Person)
        Old_objListgoAML_vw_RelatedPerson = New List(Of DataModel.goAML_Ref_Customer_Related_Person)

        Old_objListgoAML_Ref_AdditionalInfo = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        Old_objListgoAML_vw_AdditionalInfo = New List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)

        Old_objListgoAML_Ref_EntityUrl = New List(Of DataModel.goAML_Ref_Customer_URL)
        Old_objListgoAML_vw_EntityUrl = New List(Of DataModel.goAML_Ref_Customer_URL)

        Old_objListgoAML_Ref_EntityIdentification = New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Old_objListgoAML_vw_EntityIdentification = New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)

        Old_objListgoAML_Ref_RelatedEntities = New List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        Old_objListgoAML_vw_RelatedEntities = New List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        'End Add
    End Sub

    Sub GridPanelSettingNew()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 2 Then
            'GP_DirectorIdentification.ColumnModel.Columns.RemoveAt(GP_DirectorIdentification.ColumnModel.Columns.Count - 1)
            'GP_DirectorIdentification.ColumnModel.Columns.Insert(1, CommandColumn999)
            'GP_Director_Phone.ColumnModel.Columns.RemoveAt(GP_Director_Phone.ColumnModel.Columns.Count - 1)
            'GP_Director_Phone.ColumnModel.Columns.Insert(1, CommandColumn6)
            'GP_Director_Address.ColumnModel.Columns.RemoveAt(GP_Director_Address.ColumnModel.Columns.Count - 1)
            'GP_Director_Address.ColumnModel.Columns.Insert(1, CommandColumn56)
            'GP_Director_Employer_Phone.ColumnModel.Columns.RemoveAt(GP_Director_Employer_Phone.ColumnModel.Columns.Count - 1)
            'GP_Director_Employer_Phone.ColumnModel.Columns.Insert(1, CommandColumn11)
            'GP_Director_Employer_Address.ColumnModel.Columns.RemoveAt(GP_Director_Employer_Address.ColumnModel.Columns.Count - 1)
            'GP_Director_Employer_Address.ColumnModel.Columns.Insert(1, CommandColumn10)
            New_gp_INDV_Phones.ColumnModel.Columns.RemoveAt(New_gp_INDV_Phones.ColumnModel.Columns.Count - 1)
            New_gp_INDV_Phones.ColumnModel.Columns.Insert(1, New_CommandColumn1)
            'New_GP_INDVD_Address.ColumnModel.Columns.RemoveAt(New_GP_INDVD_Address.ColumnModel.Columns.Count - 1)
            'New_GP_INDVD_Address.ColumnModel.Columns.Insert(1, New_CommandColumn2)
            New_GP_INDVD_Identification.ColumnModel.Columns.RemoveAt(New_GP_INDVD_Identification.ColumnModel.Columns.Count - 1)
            New_GP_INDVD_Identification.ColumnModel.Columns.Insert(1, New_CommandColumn7)
            New_GP_Empoloyer_Phone.ColumnModel.Columns.RemoveAt(New_GP_Empoloyer_Phone.ColumnModel.Columns.Count - 1)
            New_GP_Empoloyer_Phone.ColumnModel.Columns.Insert(1, New_CommandColumn8)
            New_GP_Employer_Address.ColumnModel.Columns.RemoveAt(New_GP_Employer_Address.ColumnModel.Columns.Count - 1)
            New_GP_Employer_Address.ColumnModel.Columns.Insert(1, New_CommandColumn9)
            New_GP_Phone_Corp.ColumnModel.Columns.RemoveAt(New_GP_Phone_Corp.ColumnModel.Columns.Count - 1)
            New_GP_Phone_Corp.ColumnModel.Columns.Insert(1, New_CommandColumn3)
            New_GP_Address_Corp.ColumnModel.Columns.RemoveAt(New_GP_Address_Corp.ColumnModel.Columns.Count - 1)
            New_GP_Address_Corp.ColumnModel.Columns.Insert(1, New_CommandColumn4)
            New_gp_Director.ColumnModel.Columns.RemoveAt(New_gp_Director.ColumnModel.Columns.Count - 1)
            New_gp_Director.ColumnModel.Columns.Insert(1, New_cmdDirectory)

        End If

    End Sub
    Sub GridPanelPopUp()
        GP_DirectorIdentification.ColumnModel.Columns.RemoveAt(GP_DirectorIdentification.ColumnModel.Columns.Count - 1)
        GP_DirectorIdentification.ColumnModel.Columns.Insert(1, CommandColumn999)
        GP_Director_Phone.ColumnModel.Columns.RemoveAt(GP_Director_Phone.ColumnModel.Columns.Count - 1)
        GP_Director_Phone.ColumnModel.Columns.Insert(1, CommandColumn6)
        GP_Director_Address.ColumnModel.Columns.RemoveAt(GP_Director_Address.ColumnModel.Columns.Count - 1)
        GP_Director_Address.ColumnModel.Columns.Insert(1, CommandColumn56)
        GP_Director_Employer_Phone.ColumnModel.Columns.RemoveAt(GP_Director_Employer_Phone.ColumnModel.Columns.Count - 1)
        GP_Director_Employer_Phone.ColumnModel.Columns.Insert(1, CommandColumn11)
        GP_Director_Employer_Address.ColumnModel.Columns.RemoveAt(GP_Director_Employer_Address.ColumnModel.Columns.Count - 1)
        GP_Director_Employer_Address.ColumnModel.Columns.Insert(1, CommandColumn10)
    End Sub
    Sub GridPanelSettingOld()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 2 Then
            Old_gp_INDV_Phones.ColumnModel.Columns.RemoveAt(Old_gp_INDV_Phones.ColumnModel.Columns.Count - 1)
            Old_gp_INDV_Phones.ColumnModel.Columns.Insert(1, Old_CommandColumn1)
            Old_GP_INDVD_Address.ColumnModel.Columns.RemoveAt(Old_GP_INDVD_Address.ColumnModel.Columns.Count - 1)
            Old_GP_INDVD_Address.ColumnModel.Columns.Insert(1, Old_CommandColumn2)
            Old_GP_INDVD_Identification.ColumnModel.Columns.RemoveAt(Old_GP_INDVD_Identification.ColumnModel.Columns.Count - 1)
            Old_GP_INDVD_Identification.ColumnModel.Columns.Insert(1, Old_CommandColumn7)
            Old_GP_Empoloyer_Phone.ColumnModel.Columns.RemoveAt(Old_GP_Empoloyer_Phone.ColumnModel.Columns.Count - 1)
            Old_GP_Empoloyer_Phone.ColumnModel.Columns.Insert(1, Old_CommandColumn8)
            Old_GP_Employer_Address.ColumnModel.Columns.RemoveAt(Old_GP_Employer_Address.ColumnModel.Columns.Count - 1)
            Old_GP_Employer_Address.ColumnModel.Columns.Insert(1, Old_CommandColumn9)
            Old_GP_Phone_Corp.ColumnModel.Columns.RemoveAt(Old_GP_Phone_Corp.ColumnModel.Columns.Count - 1)
            Old_GP_Phone_Corp.ColumnModel.Columns.Insert(1, Old_CommandColumn3)
            Old_GP_Address_Corp.ColumnModel.Columns.RemoveAt(Old_GP_Address_Corp.ColumnModel.Columns.Count - 1)
            Old_GP_Address_Corp.ColumnModel.Columns.Insert(1, Old_CommandColumn4)
            Old_gp_Director.ColumnModel.Columns.RemoveAt(Old_gp_Director.ColumnModel.Columns.Count - 1)
            Old_gp_Director.ColumnModel.Columns.Insert(1, Old_cmdDirectory)

        End If

    End Sub

#Region "Event Handler"

    Protected Sub GrdCmdCustomerPreviousName(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            Dim type As String = e.ExtraParams(2).Value
            LoadData_PreviousName(id, False, type)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadData_PreviousName(id As Long, isEdit As Boolean, type As String)
        Dim objEdit

        If type = "new" Then
            objEdit = New_objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)
        Else
            objEdit = Old_objListgoAML_Ref_Previous_Name.Find(Function(x) x.PK_goAML_Ref_Customer_Previous_Name_ID = id)
        End If


        If Not objEdit Is Nothing Then
            FormPanelPreviousName.Hidden = False
            WindowDetailPreviousName.Hidden = False
            txtCustomer_first_name.ReadOnly = Not isEdit
            txtCustomer_last_name.ReadOnly = Not isEdit
            txtCustomer_comments.ReadOnly = Not isEdit
            'btn_Customer_SavePreviousName.Hidden = Not isEdit

            WindowDetailPreviousName.Title = "Previous Name " & If(isEdit, "Edit", "Detail")
            'ClearinputCustomerPreviousName()
            With objEdit
                txtCustomer_first_name.Value = .FIRST_NAME
                txtCustomer_last_name.Value = .LAST_NAME
                txtCustomer_comments.Value = .COMMENTS
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Protected Sub btnBackCustomer_PreviousName_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailPreviousName.Hidden = True
            FormPanelPreviousName.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#Region "Mark"
    Protected Sub GrdCmdCustomer_Email(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            Dim type As String = e.ExtraParams(2).Value
            LoadData_Email(id, False, type)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadData_Email(id As Long, isEdit As Boolean, type As String)
        Try
            Dim objEdit

            If type = "new" Then
                New_objTemp_goAML_Ref_Customer_Email = New_objListgoAML_Ref_Email.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id)
                New_objTempCustomer_Email_Edit = New_objListgoAML_vw_Email.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id)
                objEdit = New_objTempCustomer_Email_Edit
            Else
                Old_objTemp_goAML_Ref_Customer_Email = Old_objListgoAML_Ref_Email.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id)
                Old_objTempCustomer_Email_Edit = Old_objListgoAML_vw_Email.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id)
                objEdit = Old_objTempCustomer_Email_Edit
            End If


            If Not objEdit Is Nothing Then
                FormPanel_Email.Hidden = False
                WindowDetail_Email.Hidden = False
                'btn_Customer_Save_Email.Hidden = Not isEdit

                txtCustomer_email_address.ReadOnly = Not isEdit

                With objEdit
                    txtCustomer_email_address.Value = .EMAIL_ADDRESS
                End With

                WindowDetail_Email.Title = "Email " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GrdCmdCustomer_PEP(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            Dim type As String = e.ExtraParams(2).Value
            LoadData_PEP(id, False, type)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadData_PEP(id As Long, isEdit As Boolean, type As String)
        Try
            Dim objEdit

            If type = "new" Then
                New_objTemp_goAML_Ref_Customer_PEP = New_objListgoAML_Ref_PersonPEP.Find(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id)
                New_objTempCustomer_PersonPEP_Edit = New_objListgoAML_vw_PersonPEP.Find(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id)
                objEdit = New_objTempCustomer_PersonPEP_Edit
            Else
                Old_objTemp_goAML_Ref_Customer_PEP = Old_objListgoAML_Ref_PersonPEP.Find(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id)
                Old_objTempCustomer_PersonPEP_Edit = Old_objListgoAML_vw_PersonPEP.Find(Function(x) x.PK_goAML_Ref_Customer_PEP_ID = id)
                objEdit = Old_objTempCustomer_PersonPEP_Edit
            End If

            If Not objEdit Is Nothing Then
                FormPanel_Person_PEP.Hidden = False
                WindowDetail_Person_PEP.Hidden = False
                'btn_Customer_Save_PEP.Hidden = Not isEdit

                cmb_PersonPEP_Country.IsReadOnly = Not isEdit
                txt_function_name.ReadOnly = Not isEdit
                txt_function_description.ReadOnly = Not isEdit
                df_person_pep_valid_from.ReadOnly = Not isEdit
                cbx_pep_is_approx_from_date.ReadOnly = Not isEdit
                df_pep_valid_to.ReadOnly = Not isEdit
                cbx_pep_is_approx_to_date.ReadOnly = Not isEdit
                txt_pep_comments.ReadOnly = Not isEdit


                With objEdit
                    Dim reference = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .PEP_COUNTRY)

                    If reference IsNot Nothing Then
                        cmb_PersonPEP_Country.SetTextWithTextValue(reference.Kode, reference.Keterangan)
                    End If

                    txt_function_name.Value = .FUNCTION_NAME
                    txt_function_description.Value = .FUNCTION_DESCRIPTION
                    'df_person_pep_valid_from.Text = .PEP_DATE_RANGE_VALID_FROM
                    'cbx_pep_is_approx_from_date.Checked = .PEP_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_pep_valid_to.Text = .PEP_DATE_RANGE_VALID_TO
                    'cbx_pep_is_approx_to_date.Checked = .PEP_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_pep_comments.Text = .COMMENTS
                End With

                WindowDetail_Person_PEP.Title = "Person PEP " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GrdCmdCustomer_Sanction(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            Dim type As String = e.ExtraParams(2).Value
            LoadData_Sanction(id, False, type)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadData_Sanction(id As Long, isEdit As Boolean, type As String)
        Try
            Dim objEdit

            If type = "new" Then
                New_objTemp_goAML_Ref_Customer_Sanction = New_objListgoAML_Ref_Sanction.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id)
                New_objTempCustomer_Sanction_Edit = New_objListgoAML_vw_Sanction.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id)
                objEdit = New_objTempCustomer_Sanction_Edit
            Else
                Old_objTemp_goAML_Ref_Customer_Sanction = Old_objListgoAML_Ref_Sanction.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id)
                Old_objTempCustomer_Sanction_Edit = Old_objListgoAML_vw_Sanction.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id)
                objEdit = Old_objTempCustomer_Sanction_Edit
            End If

            If Not objEdit Is Nothing Then
                FormPanel_Sanction.Hidden = False
                WindowDetail_Sanction.Hidden = False
                'btnSaveCustomer_Sanction.Hidden = Not isEdit

                txt_sanction_provider.ReadOnly = Not isEdit
                txt_sanction_sanction_list_name.ReadOnly = Not isEdit
                txt_sanction_match_criteria.ReadOnly = Not isEdit
                txt_sanction_link_to_source.ReadOnly = Not isEdit
                txt_sanction_sanction_list_attributes.ReadOnly = Not isEdit
                df_sanction_valid_from.ReadOnly = Not isEdit
                cbx_sanction_is_approx_from_date.ReadOnly = Not isEdit
                df_sanction_valid_to.ReadOnly = Not isEdit
                cbx_sanction_is_approx_to_date.ReadOnly = Not isEdit
                txt_sanction_comments.ReadOnly = Not isEdit


                With objEdit
                    txt_sanction_provider.Value = .PROVIDER
                    txt_sanction_sanction_list_name.Value = .SANCTION_LIST_NAME
                    txt_sanction_match_criteria.Value = .MATCH_CRITERIA
                    txt_sanction_link_to_source.Value = .LINK_TO_SOURCE
                    txt_sanction_sanction_list_attributes.Value = .SANCTION_LIST_ATTRIBUTES
                    'df_sanction_valid_from.Text = .SANCTION_LIST_DATE_RANGE_VALID_FROM
                    'cbx_sanction_is_approx_from_date.Checked = .SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_sanction_valid_to.Text = .SANCTION_LIST_DATE_RANGE_VALID_TO
                    'cbx_sanction_is_approx_to_date.Checked = .SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_sanction_comments.Value = .COMMENTS
                End With

                WindowDetail_Sanction.Title = "Sanction " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GrdCmdCustomer_RelatedPerson(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            Dim type As String = e.ExtraParams(2).Value
            LoadData_RelatedPerson(id, False, type)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadData_RelatedPerson(id As Long, isEdit As Boolean, type As String)
        Try
            Dim objEdit

            If type = "new" Then
                New_objTemp_goAML_Ref_Customer_Related_Person = New_objListgoAML_Ref_RelatedPerson.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id)
                New_objTempCustomer_RelatedPerson_Edit = New_objListgoAML_vw_RelatedPerson.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id)
                objEdit = New_objTempCustomer_RelatedPerson_Edit
            Else
                Old_objTemp_goAML_Ref_Customer_Related_Person = Old_objListgoAML_Ref_RelatedPerson.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id)
                Old_objTempCustomer_RelatedPerson_Edit = Old_objListgoAML_vw_RelatedPerson.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id)
                objEdit = Old_objTempCustomer_RelatedPerson_Edit
            End If

            If Not objEdit Is Nothing Then
                FormPanel_RelatedPerson.Hidden = False
                WindowDetail_RelatedPerson.Hidden = False
                'btnSaveCustomer_RelatedPerson.Hidden = Not isEdit

                cmb_rp_person_relation.IsReadOnly = Not isEdit
                'df_relation_date_range_valid_from.ReadOnly = Not isEdit
                'cbx_relation_date_range_is_approx_from_date.ReadOnly = Not isEdit
                'df_relation_date_range_valid_to.ReadOnly = Not isEdit
                'cbx_relation_date_range_is_approx_to_date.ReadOnly = Not isEdit
                txt_rp_comments.ReadOnly = Not isEdit
                txt_rp_relation_comments.ReadOnly = Not isEdit
                cmb_rp_gender.IsReadOnly = Not isEdit
                txt_rp_title.ReadOnly = Not isEdit
                txt_rp_first_name.ReadOnly = Not isEdit
                txt_rp_middle_name.ReadOnly = Not isEdit
                txt_rp_prefix.ReadOnly = Not isEdit
                txt_rp_last_name.ReadOnly = Not isEdit
                df_birthdate.ReadOnly = Not isEdit
                txt_rp_birth_place.ReadOnly = Not isEdit
                cmb_rp_country_of_birth.IsReadOnly = Not isEdit
                txt_rp_mothers_name.ReadOnly = Not isEdit
                txt_rp_alias.ReadOnly = Not isEdit
                'txt_rp_full_name_frn.ReadOnly = Not isEdit
                txt_rp_ssn.ReadOnly = Not isEdit
                txt_rp_passport_number.ReadOnly = Not isEdit
                cmb_rp_passport_country.IsReadOnly = Not isEdit
                txt_rp_id_number.ReadOnly = Not isEdit
                cmb_rp_nationality1.IsReadOnly = Not isEdit
                cmb_rp_nationality2.IsReadOnly = Not isEdit
                cmb_rp_nationality3.IsReadOnly = Not isEdit
                cmb_rp_residence.IsReadOnly = Not isEdit
                'df_rp_residence_since.ReadOnly = Not isEdit
                txt_rp_occupation.ReadOnly = Not isEdit
                txt_rp_employer_name.ReadOnly = Not isEdit
                cbx_rp_deceased.ReadOnly = Not isEdit
                df_rp_date_deceased.ReadOnly = Not isEdit
                txt_rp_tax_number.ReadOnly = Not isEdit
                cbx_rp_tax_reg_number.ReadOnly = Not isEdit
                txt_rp_source_of_wealth.ReadOnly = Not isEdit
                cbx_rp_is_protected.ReadOnly = Not isEdit


                With objEdit
                    Dim refPersonRelation = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Person_Person_Relationship", .PERSON_PERSON_RELATION)
                    Dim refGender = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Jenis_Kelamin", .GENDER)
                    Dim refCountryBirth = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .COUNTRY_OF_BIRTH)
                    Dim refPassportCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .PASSPORT_COUNTRY)
                    Dim refNationality1 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY1)
                    Dim refNationality2 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY2)
                    Dim refNationality3 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY3)
                    Dim refResidence = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .RESIDENCE)

                    cmb_rp_person_relation.SetTextWithTextValue(refPersonRelation.Kode, refPersonRelation.Keterangan)
                    'df_relation_date_range_valid_from.Text = .RELATION_DATE_RANGE_VALID_FROM
                    'cbx_relation_date_range_is_approx_from_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_relation_date_range_valid_to.Text = .RELATION_DATE_RANGE_VALID_TO
                    'cbx_relation_date_range_is_approx_to_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_rp_comments.Value = .COMMENTS
                    txt_rp_relation_comments.Value = .RELATION_COMMENTS
                    If refGender IsNot Nothing Then
                        cmb_rp_gender.SetTextWithTextValue(refGender.Kode, refGender.Keterangan)
                    End If
                    txt_rp_title.Value = .TITLE
                    txt_rp_first_name.Value = .FIRST_NAME
                    txt_rp_middle_name.Value = .MIDDLE_NAME
                    txt_rp_prefix.Value = .PREFIX
                    txt_rp_last_name.Value = .LAST_NAME
                    df_birthdate.Text = .BIRTHDATE
                    txt_rp_birth_place.Value = .BIRTH_PLACE
                    If refCountryBirth IsNot Nothing Then
                        cmb_rp_country_of_birth.SetTextWithTextValue(refCountryBirth.Kode, refCountryBirth.Keterangan)
                    End If
                    txt_rp_mothers_name.Value = .MOTHERS_NAME
                    txt_rp_alias.Value = .ALIAS
                    'txt_rp_full_name_frn.Value = .FULL_NAME_FRN
                    txt_rp_ssn.Value = .SSN
                    txt_rp_passport_number.Value = .PASSPORT_NUMBER
                    If refPassportCountry IsNot Nothing Then
                        cmb_rp_passport_country.SetTextWithTextValue(refPassportCountry.Kode, refPassportCountry.Keterangan)
                    End If
                    txt_rp_id_number.Value = .ID_NUMBER
                    If refNationality1 IsNot Nothing Then
                        cmb_rp_nationality1.SetTextWithTextValue(refNationality1.Kode, refNationality1.Keterangan)
                    End If
                    If refNationality2 IsNot Nothing Then
                        cmb_rp_nationality2.SetTextWithTextValue(refNationality2.Kode, refNationality2.Keterangan)
                    End If
                    If refNationality3 IsNot Nothing Then
                        cmb_rp_nationality3.SetTextWithTextValue(refNationality3.Kode, refNationality3.Keterangan)
                    End If
                    If refResidence IsNot Nothing Then
                        cmb_rp_residence.SetTextWithTextValue(refResidence.Kode, refResidence.Keterangan)
                    End If
                    'df_rp_residence_since.Text = .RESIDENCE_SINCE
                    txt_rp_occupation.Value = .OCCUPATION
                    txt_rp_employer_name.Value = .EMPLOYER_NAME
                    cbx_rp_deceased.Checked = .DECEASED
                    df_rp_date_deceased.Text = IIf(.DATE_DECEASED Is Nothing, "", .DATE_DECEASED)
                    txt_rp_tax_number.Value = .TAX_NUMBER
                    cbx_rp_tax_reg_number.Checked = .TAX_REG_NUMBER
                    txt_rp_source_of_wealth.Text = .SOURCE_OF_WEALTH
                    cbx_rp_is_protected.Checked = .IS_PROTECTED

                    'GridEmail_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Customer_Email)

                    GridAddress_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Address)
                    GridAddressWork_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Address_Work)
                    GridPhone_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Phone)
                    GridPhoneWork_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Phone_Work)
                    GridIdentification_RelatedPerson.LoadData(.ObjList_GoAML_Person_Identification)


                    GridEmail_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Customer_Email)
                    GridSanction_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Customer_Sanction)
                    GridPEP_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Customer_PEP)

                End With

                WindowDetail_RelatedPerson.Title = "Related Person " & If(isEdit, "Edit", "Detail")
                'GridEmail_RelatedPerson.SetViewMode()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GrdCmdCustomer_EntityIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            Dim type As String = e.ExtraParams(2).Value
            LoadData_EntityIdentification(id, False, type)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadData_EntityIdentification(id As Long, isEdit As Boolean, type As String)
        Try
            Dim objEdit

            If type = "new" Then
                New_objTemp_goAML_Ref_Customer_Entity_Identification = New_objListgoAML_Ref_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id)
                New_objTempCustomer_EntityIdentification_Edit = New_objListgoAML_vw_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id)
                objEdit = New_objTempCustomer_EntityIdentification_Edit
            Else
                Old_objTemp_goAML_Ref_Customer_Entity_Identification = Old_objListgoAML_Ref_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id)
                Old_objTempCustomer_EntityIdentification_Edit = Old_objListgoAML_vw_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id)
                objEdit = Old_objTempCustomer_EntityIdentification_Edit
            End If

            If Not objEdit Is Nothing Then
                FormPanel_EntityIdentification.Hidden = False
                WindowDetail_EntityIdentification.Hidden = False
                'btnSaveCustomer_EntityIdentification.Hidden = Not isEdit

                cmb_ef_type.IsReadOnly = Not isEdit
                txt_ef_number.ReadOnly = Not isEdit
                df_issue_date.ReadOnly = Not isEdit
                df_expiry_date.ReadOnly = Not isEdit
                txt_ef_issued_by.ReadOnly = Not isEdit
                cmb_ef_issue_country.IsReadOnly = Not isEdit
                txt_ef_comments.ReadOnly = Not isEdit


                With objEdit
                    Dim refType = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Identifier_Type", .TYPE)
                    Dim refIssue = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .ISSUE_COUNTRY)

                    cmb_ef_type.SetTextWithTextValue(refType.Kode, refType.Keterangan)
                    txt_ef_number.Value = .NUMBER
                    df_issue_date.Text = IIf(.ISSUE_DATE Is Nothing, "", .ISSUE_DATE)
                    df_expiry_date.Text = IIf(.EXPIRY_DATE Is Nothing, "", .EXPIRY_DATE)
                    txt_ef_issued_by.Value = .ISSUED_BY
                    cmb_ef_issue_country.SetTextWithTextValue(refIssue.Kode, refIssue.Keterangan)
                    txt_ef_comments.Value = .COMMENTS

                End With

                WindowDetail_EntityIdentification.Title = "Entity Identification " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GrdCmdCustomer_URL(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            Dim type As String = e.ExtraParams(2).Value
            LoadData_CustomerURL(id, False, type)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadData_CustomerURL(id As Long, isEdit As Boolean, type As String)
        Try
            Dim objEdit

            If type = "new" Then
                New_objTemp_goAML_Ref_Customer_URL = New_objListgoAML_Ref_EntityUrl.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id)
                New_objTempCustomer_EntityUrl_Edit = New_objListgoAML_vw_EntityUrl.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id)
                objEdit = New_objTempCustomer_EntityUrl_Edit
            Else
                Old_objTemp_goAML_Ref_Customer_URL = Old_objListgoAML_Ref_EntityUrl.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id)
                Old_objTempCustomer_EntityUrl_Edit = Old_objListgoAML_vw_EntityUrl.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id)
                objEdit = Old_objTempCustomer_EntityUrl_Edit
            End If

            If Not objEdit Is Nothing Then
                FormPanel_URL.Hidden = False
                WindowDetail_URL.Hidden = False
                'btnSaveCustomer_URL.Hidden = Not isEdit

                txt_url.ReadOnly = Not isEdit

                With objEdit
                    txt_url.Value = .URL
                End With

                WindowDetail_URL.Title = "Entity URL " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GrdCmdCustomer_RelatedEntities(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            Dim type As String = e.ExtraParams(2).Value
            LoadData_RelatedEntities(id, False, type)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadData_RelatedEntities(id As Long, isEdit As Boolean, type As String)
        Try
            Dim objEdit

            If type = "new" Then
                New_objTemp_goAML_Ref_Customer_Related_Entities = New_objListgoAML_Ref_RelatedEntities.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id)
                New_objTempCustomer_RelatedEntities_Edit = New_objListgoAML_vw_RelatedEntities.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id)
                objEdit = New_objTempCustomer_RelatedEntities_Edit
            Else
                Old_objTemp_goAML_Ref_Customer_Related_Entities = Old_objListgoAML_Ref_RelatedEntities.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id)
                Old_objTempCustomer_RelatedEntities_Edit = Old_objListgoAML_vw_RelatedEntities.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id)
                objEdit = Old_objTempCustomer_RelatedEntities_Edit
            End If


            If Not objEdit Is Nothing Then
                FormPanel_RelatedEntities.Hidden = False
                WindowDetail_RelatedEntities.Hidden = False

                cmb_re_entity_relation.IsReadOnly = Not isEdit
                txt_re_comments.ReadOnly = Not isEdit
                txt_re_relation_comments.ReadOnly = Not isEdit
                txt_re_name.ReadOnly = Not isEdit
                txt_re_commercial_name.ReadOnly = Not isEdit
                cmb_re_incorporation_legal_form.IsReadOnly = Not isEdit
                txt_re_incorporation_number.ReadOnly = Not isEdit
                txt_re_business.ReadOnly = Not isEdit
                txt_re_incorporation_state.ReadOnly = Not isEdit
                cmb_re_incorporation_country_code.IsReadOnly = Not isEdit
                df_re_incorporation_date.ReadOnly = Not isEdit
                cbx_re_business_closed.ReadOnly = Not isEdit
                df_re_date_business_closed.ReadOnly = Not isEdit
                txt_re_tax_number.ReadOnly = Not isEdit
                txt_re_tax_reg_number.ReadOnly = Not isEdit


                With objEdit
                    Dim refRelation = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Entity_Relationship", .ENTITY_ENTITY_RELATION)
                    Dim refLegalForm = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Bentuk_Badan_Usaha", .INCORPORATION_LEGAL_FORM)
                    Dim refEntityStatus = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Status", .ENTITY_STATUS)
                    Dim refCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .INCORPORATION_COUNTRY_CODE)

                    If refRelation IsNot Nothing Then
                        cmb_re_entity_relation.SetTextWithTextValue(refRelation.Kode, refRelation.Keterangan)
                    End If
                    txt_re_comments.Value = .COMMENTS
                    txt_re_relation_comments.Value = .RELATION_COMMENTS
                    txt_re_name.Value = .NAME
                    txt_re_commercial_name.Value = .COMMERCIAL_NAME
                    If refLegalForm IsNot Nothing Then
                        cmb_re_incorporation_legal_form.SetTextWithTextValue(refLegalForm.Kode, refLegalForm.Keterangan)
                    End If
                    txt_re_incorporation_number.Value = .INCORPORATION_NUMBER
                    txt_re_business.Value = .BUSINESS
                    txt_re_incorporation_state.Value = .INCORPORATION_STATE
                    If refCountry IsNot Nothing Then
                        cmb_re_incorporation_country_code.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                    End If
                    df_re_incorporation_date.Text = IIf(.INCORPORATION_DATE Is Nothing, "", .INCORPORATION_DATE)
                    cbx_re_business_closed.Checked = .BUSINESS_CLOSED
                    df_re_date_business_closed.Text = IIf(.DATE_BUSINESS_CLOSED Is Nothing, "", .DATE_BUSINESS_CLOSED)
                    txt_re_tax_number.Value = .TAX_NUMBER
                    txt_re_tax_reg_number.Value = .TAX_REG_NUMBER

                    Dim phone_list = goAML_Customer_Service.GetPhoneByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36)
                    Dim address_list = goAML_Customer_Service.GetAddressByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36)
                    Dim email_list = goAML_Customer_Service.GetEmailByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36)
                    Dim entity_identification_list = goAML_Customer_Service.GetEntityIdentificationByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36)
                    Dim url_list = goAML_Customer_Service.GetUrlByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36)
                    Dim sanction_list = goAML_Customer_Service.GetSanctionByPkFk(.PK_goAML_Ref_Customer_Related_Entities_ID, 36)

                    If type = "new" Then
                        Dim data = ObjDataNew.ObjList_GoAML_Ref_Customer_RelatedEntities.Where(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = .PK_goAML_Ref_Customer_Related_Entities_ID).FirstOrDefault()
                        phone_list = data.ObjList_GoAML_Ref_Customer_Phone
                        address_list = data.ObjList_GoAML_Ref_Customer_Address
                        email_list = data.ObjList_GoAML_Ref_Customer_Email
                        entity_identification_list = data.ObjList_GoAML_Ref_Customer_Entity_Identification
                        url_list = data.ObjList_GoAML_Ref_Customer_URL
                        sanction_list = data.ObjList_GoAML_Ref_Customer_Sanction
                    Else
                        Dim data = ObjDataOld.ObjList_GoAML_Ref_Customer_RelatedEntities.Where(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = .PK_goAML_Ref_Customer_Related_Entities_ID).FirstOrDefault()
                        phone_list = data.ObjList_GoAML_Ref_Customer_Phone
                        address_list = data.ObjList_GoAML_Ref_Customer_Address
                        email_list = data.ObjList_GoAML_Ref_Customer_Email
                        entity_identification_list = data.ObjList_GoAML_Ref_Customer_Entity_Identification
                        url_list = data.ObjList_GoAML_Ref_Customer_URL
                        sanction_list = data.ObjList_GoAML_Ref_Customer_Sanction
                    End If

                    GridPhone_RelatedEntity.LoadData(phone_list)
                    GridAddress_RelatedEntity.LoadData(address_list)
                    GridEmail_RelatedEntity.LoadData(email_list)
                    GridEntityIdentification_RelatedEntity.LoadData(entity_identification_list)
                    GridURL_RelatedEntity.LoadData(url_list)
                    GridSanction_RelatedEntity.LoadData(sanction_list)

                End With

                WindowDetail_RelatedEntities.Title = "Related Entities " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GrdCmdCustPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            Dim type As String = e.ExtraParams(2).Value
            LoadDataDetail(id, type)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub ClearinputCustomerPhones()
        txt_phone_Country_prefix.Text = ""
        txt_phone_number.Text = ""
        txt_phone_extension.Text = ""
    End Sub

    Sub LoadDataDetail(id As Long, type As String)
        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        Dim objEdit

        If type = "new" Then
            objEdit = New_objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Else
            objEdit = Old_objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        End If

        If Not objEdit Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            cb_phone_Contact_Type.ReadOnly = True
            cb_phone_Communication_Type.ReadOnly = True
            txt_phone_Country_prefix.ReadOnly = True
            txt_phone_number.ReadOnly = True
            txt_phone_extension.ReadOnly = True
            txt_phone_comments.ReadOnly = True
            'btnSavedetail.Hidden = True
            WindowDetail.Title = "Phone Detail"
            ClearinputCustomerPhones()
            With objEdit
                Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                Store_Contact_Type.DataBind()
                If .Tph_Contact_Type IsNot Nothing Then
                    'Phone_Contact_Type = goAML_CustomerBLL.GetListKategoriKontak().Where(Function(x) x.Kode = .Tph_Contact_Type).FirstOrDefault
                    cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)
                End If
                Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                Store_Communication_Type.DataBind()
                If .Tph_Communication_Type IsNot Nothing Then
                    'Phone_Communication_Type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                    cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                End If
                txt_phone_Country_prefix.Text = .tph_country_prefix
                txt_phone_number.Text = .tph_number
                txt_phone_extension.Text = .tph_extension
                txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Protected Sub GrdCmdCustAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            Dim type As String = e.ExtraParams(2).Value
            LoadDataAddressDetail(id, type)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataAddressDetail(id As Long, type As String)

        'Dim objCustomerAddressDetail = goAML_CustomerBLL.GetVWCustomerAddressesbyPK(id)

        Dim objEdit

        If type = "new" Then
            objEdit = New_objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        Else
            objEdit = Old_objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        End If

        If Not objEdit Is Nothing Then
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            cmb_kategoriaddress.ReadOnly = True
            Address_INDV.ReadOnly = True
            Town_INDV.ReadOnly = True
            City_INDV.ReadOnly = True
            Zip_INDV.ReadOnly = True
            cmb_kodenegara.ReadOnly = True
            State_INDVD.ReadOnly = True
            Comment_Address_INDVD.ReadOnly = True
            'btnSavedetail.Hidden = True
            WindowDetailAddress.Title = "Address Detail"
            ClearinputCustomerAddress()
            With objEdit
                Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                Store_KategoriAddress.DataBind()
                'Dim kategoriaddress = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                If .Address_Type IsNot Nothing Then
                    cmb_kategoriaddress.SetValue(.Address_Type)
                End If

                Address_INDV.Text = .Address
                Town_INDV.Text = .Town
                City_INDV.Text = .City
                Zip_INDV.Text = .Zip
                Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                Store_Kode_Negara.DataBind()
                'Dim namanegara = goAML_CustomerBLL.GetNamaNegaraByKeterangan(.Country)
                If .Country_Code IsNot Nothing Then
                    cmb_kodenegara.SetValue(.Country_Code)
                End If

                State_INDVD.Text = .State
                Comment_Address_INDVD.Text = .Comments
            End With
        End If
    End Sub

    Protected Sub GrdCmdCustIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            Dim type As String = e.ExtraParams(2).Value
            LoadDataIdentificationDetail(id, type)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadDataIdentificationDetail(id As Long, type As String)

        Dim objCustomerIdentificationDetail = Nothing
        If type.ToLower() = "new" Then
            objCustomerIdentificationDetail = New_objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)
        Else
            objCustomerIdentificationDetail = Old_objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)
        End If

        If Not objCustomerIdentificationDetail Is Nothing Then
            FP_Identification_INDV.Hidden = False
            WindowDetailIdentification.Hidden = False
            WindowDetailIdentification.Title = "Identification Detail"
            txt_number_identification.ReadOnly = True
            cmb_Jenis_Dokumen.ReadOnly = True
            cmb_issued_country.ReadOnly = True
            txt_issued_by.ReadOnly = True
            txt_issued_date.ReadOnly = True
            txt_issued_expired_date.ReadOnly = True
            txt_identification_comment.ReadOnly = True
            ClearinputCustomerIdentification()
            txt_number_identification.Text = objCustomerIdentificationDetail.Number
            Store_jenis_dokumen.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            Store_jenis_dokumen.DataBind()
            'Dim jenisdokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(x) x.Keterangan = objCustomerIdentificationDetail.Type).FirstOrDefault()
            'If jenisdokumen IsNot Nothing Then
            '    cmb_Jenis_Dokumen.SetValue(jenisdokumen.Kode)
            'End If
            cmb_Jenis_Dokumen.SetValue(objCustomerIdentificationDetail.Type)

            Store_issued_country.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            Store_issued_country.DataBind()
            'Dim issuedcountry = goAML_CustomerBLL.GetNamaNegaraByKeterangan(objCustomerIdentificationDetail.Country)
            'If issuedcountry IsNot Nothing Then
            '    cmb_issued_country.SetValue(issuedcountry.Kode)
            'End If
            cmb_issued_country.SetValue(objCustomerIdentificationDetail.Issued_Country)

            txt_issued_by.Text = objCustomerIdentificationDetail.Issued_By
            txt_issued_date.Value = objCustomerIdentificationDetail.Issue_Date
            txt_issued_expired_date.Value = objCustomerIdentificationDetail.Expiry_Date
            txt_identification_comment.Text = objCustomerIdentificationDetail.Identification_Comment
        End If
    End Sub

    Protected Sub GrdCmdDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            Dim type As String = e.ExtraParams(2).Value
            LoadDataDetailDirector(id, type)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailDirector(id As Long, type As String)
        Try
            Dim dateFormat As String = "dd-MM-yyyy"

            Dim objSysParamDateFormat As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(6)

            If objSysParamDateFormat IsNot Nothing Then
                dateFormat = objSysParamDateFormat.SettingValue
            End If

            FormPanelDetailDirector.Hidden = False
            cb_Director_Deceased.ReadOnly = True
            txt_Director_Deceased_Date.ReadOnly = True
            txt_Director_Tax_Number.ReadOnly = True
            cb_Director_Tax_Reg_Number.ReadOnly = True
            txt_Director_Source_of_Wealth.ReadOnly = True
            txt_Director_Comments.ReadOnly = True
            'Dim Director_Detail = goAML_CustomerBLL.GetCustomerDirectorByPKDirector(id)
            Dim Director_Detail = Nothing
            If type = "new" Then
                Director_Detail = ObjDataNew.objListgoAML_Ref_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id).FirstOrDefault()
            Else
                Director_Detail = ObjDataOld.objListgoAML_Ref_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id).FirstOrDefault()
            End If

            Name.Text = Director_Detail.Last_Name
            Dim role_detail = goAML_CustomerBLL.GetRoleDirectorByID(Director_Detail.Role)
            If role_detail IsNot Nothing Then
                Role.Text = role_detail.Keterangan
            Else
                Role.Text = ""
            End If
            BirthPlace.Text = Director_Detail.Birth_Place
            ' 2023-10-09, Nael: dicek apakah birthdate nothing?, kalau nothing maka diisi string kosong
            BirthDate.Text = IIf(Director_Detail.BirthDate Is Nothing, "", Convert.ToDateTime(Director_Detail.BirthDate).ToString(dateFormat))
            MomName.Text = Director_Detail.Mothers_Name
            Aliase.Text = Director_Detail.Alias
            SSN.Text = Director_Detail.SSN
            PassportNo.Text = Director_Detail.Passport_Number
            PassportFrom.Text = Director_Detail.Passport_Country
            No_Identity.Text = Director_Detail.ID_Number
            txt_Director_employer_name.Text = Director_Detail.Employer_Name
            If Director_Detail.Nationality1 IsNot Nothing And Director_Detail.Nationality1 IsNot "" Then
                Nasionality1.Text = goAML_CustomerBLL.GetNamaNegaraByID(Director_Detail.Nationality1).Keterangan
            End If
            If Not Director_Detail.Nationality2 = "" And Director_Detail.Nationality2 IsNot Nothing Then
                Nasionality2.Text = goAML_CustomerBLL.GetNamaNegaraByID(Director_Detail.Nationality2).Keterangan
            End If
            If Not Director_Detail.Nationality3 = "" And Director_Detail.Nationality3 IsNot Nothing Then
                Nasionality3.Text = goAML_CustomerBLL.GetNamaNegaraByID(Director_Detail.Nationality3).Keterangan
            End If
            If Not Director_Detail.Residence = "" And Director_Detail.Residence IsNot Nothing Then
                Residence.Text = goAML_CustomerBLL.GetNamaNegaraByID(Director_Detail.Residence).Keterangan
            End If
            Email.Text = Director_Detail.Email
            Pekerjaan.Text = Director_Detail.Occupation

            If Director_Detail.Deceased = True Then
                cb_Director_Deceased.Checked = True
                txt_Director_Deceased_Date.Value = Convert.ToDateTime(Director_Detail.Deceased_Date).ToString(dateFormat)
            End If
            txt_Director_Tax_Number.Text = Director_Detail.Tax_Number
            txtType.Text = type
            If Director_Detail.Tax_Reg_Number = True Then
                cb_Director_Tax_Reg_Number.Checked = True
            End If
            txt_Director_Source_of_Wealth.Text = Director_Detail.Source_of_Wealth
            txt_Director_Comments.Text = Director_Detail.Comments


            ' 2023-10-05, Nael: Mengkonvert list object menjadi data table agar beberapa kolom bisa lebih detail
            If type = "new" Then
                Dim listNewPhone = ObjDataNew.objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                StoreDirectorPhone.DataSource = goAML_Customer_Service.PhoneService.GenerateDataTable(listNewPhone) 'Nael
            End If
            If type = "old" Then
                Dim listOldPhone = ObjDataOld.objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                StoreDirectorPhone.DataSource = goAML_Customer_Service.PhoneService.GenerateDataTable(listOldPhone) 'Nael
            End If
            StoreDirectorPhone.DataBind()

            If type = "new" Then
                Dim listNewAddress = ObjDataNew.objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                StoreDirectorAddress.DataSource = goAML_Customer_Service.AddressService.GenerateDataTable(listNewAddress) 'Nael
            End If
            If type = "old" Then
                Dim listOldAddress = ObjDataOld.objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                StoreDirectorAddress.DataSource = goAML_Customer_Service.AddressService.GenerateDataTable(listOldAddress) 'Nael
            End If
            StoreDirectorAddress.DataBind()

            If type = "new" Then
                StoreDirectorEmployerPhone.DataSource = ObjDataNew.objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
            End If
            If type = "old" Then
                StoreDirectorEmployerPhone.DataSource = ObjDataOld.objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
            End If
            StoreDirectorEmployerPhone.DataBind()

            If type = "new" Then
                ' 2023-10-10, Nael
                Dim newDirectorEmpAddress = ObjDataNew.objlistgoaml_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                StoreDirectorEmployerAddress.DataSource = goAML_Customer_Service.AddressService.GenerateDataTable(newDirectorEmpAddress)
            End If
            If type = "old" Then
                ' 2023-10-10, Nael
                Dim oldDirectorEmpAddress = ObjDataOld.objlistgoaml_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                StoreDirectorEmployerAddress.DataSource = goAML_Customer_Service.AddressService.GenerateDataTable(oldDirectorEmpAddress)
            End If
            StoreDirectorEmployerAddress.DataBind()

            If type = "new" Then
                Dim listNewId = ObjDataNew.objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                Store_Director_Identification.DataSource = listNewId 'Nael
            End If
            If type = "old" Then
                Dim listOldId = ObjDataOld.objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                Store_Director_Identification.DataSource = listOldId
            End If
            Store_Director_Identification.DataBind()

            Dim email_list = goAML_Customer_Service.GetEmailByPkFk(id, 6)
            Dim sanction_list = goAML_Customer_Service.GetSanctionByPkFk(id, 6)
            Dim pep_list = goAML_Customer_Service.GetPEPByPkFk(id, 6)

            If type = "new" Then
                Dim directorNewDetail = ObjDataNew.ObjListgoAML_Ref_Director2.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).FirstOrDefault()
                email_list = directorNewDetail.ObjList_GoAML_Ref_Customer_Email
                sanction_list = directorNewDetail.ObjList_GoAML_Ref_Customer_Sanction
                pep_list = directorNewDetail.ObjList_GoAML_Ref_Customer_PEP
            Else
                Dim directorNewDetail = ObjDataOld.ObjListgoAML_Ref_Director2.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).FirstOrDefault()
                email_list = directorNewDetail.ObjList_GoAML_Ref_Customer_Email
                sanction_list = directorNewDetail.ObjList_GoAML_Ref_Customer_Sanction
                pep_list = directorNewDetail.ObjList_GoAML_Ref_Customer_PEP
            End If

            GridEmail_Director.LoadData(email_list)
            GridSanction_Director.LoadData(sanction_list)
            GridPEP_Director.LoadData(pep_list)

            'Dim objList

            WindowDetailDirector.Hidden = False
            WindowDetailDirector.Title = "Director Detail"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdAddressDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            'Dim type As String = e.ExtraParams(2).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddressDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailAddressDirector(id As Long)
        Try
            'Dim objCustomerAddressDetail = goAML_CustomerBLL.GetAddressByID(id)
            Dim objCustomerAddressDetail As New goAML_Ref_Address()

            objCustomerAddressDetail = ObjDataNew.objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID.ToString().Equals(id.ToString()))
            '2023-10-07, Nael: Menambahakn beberapa logic untuk menghindari Null Pointer exception
            If objCustomerAddressDetail Is Nothing AndAlso ObjDataOld IsNot Nothing Then
                objCustomerAddressDetail = ObjDataOld.objListgoAML_Ref_Director_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
            End If
            If objCustomerAddressDetail Is Nothing Then
                objCustomerAddressDetail = ObjDataNew.objlistgoaml_Ref_Director_Employer_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
            End If
            If objCustomerAddressDetail Is Nothing AndAlso ObjDataOld IsNot Nothing Then
                objCustomerAddressDetail = ObjDataOld.objlistgoaml_Ref_Director_Employer_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
            End If

            If Not objCustomerAddressDetail Is Nothing Then
                FP_Address_Director.Hidden = False
                WindowDetailDirectorAddress.Hidden = False
                WindowDetailDirectorAddress.Title = "Address Detail"
                Director_cmb_kategoriaddress.ReadOnly = True
                Director_Address.ReadOnly = True
                Director_Town.ReadOnly = True
                Director_City.ReadOnly = True
                Director_Zip.ReadOnly = True
                Director_cmb_kodenegara.ReadOnly = True
                Director_State.ReadOnly = True
                Director_Comment_Address.ReadOnly = True
                Director_Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                Director_Store_KategoriAddress.DataBind()
                Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
                If kategorikontak IsNot Nothing Then
                    Director_cmb_kategoriaddress.SetValue(kategorikontak.Kode)
                End If
                Director_Address.Text = objCustomerAddressDetail.Address
                Director_Town.Text = objCustomerAddressDetail.Town
                Director_City.Text = objCustomerAddressDetail.City
                Director_Zip.Text = objCustomerAddressDetail.Zip
                Director_Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                Director_Store_Kode_Negara.DataBind()
                Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerAddressDetail.Country_Code)
                If kodenegara IsNot Nothing Then
                    Director_cmb_kodenegara.SetValue(kodenegara.Kode)
                End If

                Director_State.Text = objCustomerAddressDetail.State
                Director_Comment_Address.Text = objCustomerAddressDetail.Comments
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As Integer
            If Integer.TryParse(e.ExtraParams(0).Value, id) Then
                LoadDataDirectorIdentificationDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadDataDirectorIdentificationDetail(id As Integer)

        Dim objCustomerIdentificationDetail As New Vw_Person_Identification()


        ' 2023-10-05, Nael: ini ganti yg asalnya pake Where jadi FirstOrDefault, biar get 1 ajh
        objCustomerIdentificationDetail = New_objListgoAML_vw_Customer_Identification_Director.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.ToString().Equals(id.ToString()))

        If objCustomerIdentificationDetail Is Nothing Then
            objCustomerIdentificationDetail = Old_objListgoAML_vw_Customer_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = id)
        End If

        If objCustomerIdentificationDetail IsNot Nothing Then
            FP_Identification_Director.Hidden = False
            WindowDetailIdentification.Hidden = False
            WindowDetailIdentification.Title = "Identification Detail"
            txt_number_identification_Director.ReadOnly = True
            cmb_Jenis_Dokumen_Director.ReadOnly = True
            cmb_issued_country_Director.ReadOnly = True
            txt_issued_by_Director.ReadOnly = True
            txt_issued_date_Director.ReadOnly = True
            txt_issued_expired_date_Director.ReadOnly = True
            txt_identification_comment_Director.ReadOnly = True
            ClearinputDirectorIdentification()
            txt_number_identification_Director.Text = objCustomerIdentificationDetail.Number
            Storejenisdokumen_Director.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            Storejenisdokumen_Director.DataBind()
            Dim jenisdokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas().FirstOrDefault(Function(X) X.Keterangan = objCustomerIdentificationDetail.Type_Document)
            If jenisdokumen IsNot Nothing Then
                cmb_Jenis_Dokumen_Director.SetValue(jenisdokumen.Kode)
            End If

            StoreIssueCountry_Director.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            StoreIssueCountry_Director.DataBind()

            cmb_issued_country_Director.SetValue(objCustomerIdentificationDetail.Country)

            txt_issued_by_Director.Text = objCustomerIdentificationDetail.Issued_By
            If objCustomerIdentificationDetail.Issue_Date IsNot Nothing Then
                txt_issued_date_Director.Text = objCustomerIdentificationDetail.Issue_Date
            End If

            If objCustomerIdentificationDetail.Expiry_Date IsNot Nothing Then
                txt_issued_expired_date_Director.Text = objCustomerIdentificationDetail.Expiry_Date
            End If

            If objCustomerIdentificationDetail.Identification_Comment IsNot Nothing Then
                txt_identification_comment_Director.Text = objCustomerIdentificationDetail.Identification_Comment
            End If
        End If
    End Sub

    Protected Sub GrdCmdPhoneDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhoneDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailPhoneDirector(id As Long)
        Try
            'Dim objCustomerPhonesDetail As goAML_Ref_Phone = goAML_CustomerBLL.GetPhonesByID(id)
            Dim objCustomerPhonesDetail

            objCustomerPhonesDetail = ObjDataNew.objListgoAML_Ref_Director_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = id).FirstOrDefault()
            '2023-10-07, Nael: Menambahkan beberapa logic untuk menghindari Null pointer exception
            If objCustomerPhonesDetail Is Nothing AndAlso ObjDataOld IsNot Nothing Then
                objCustomerPhonesDetail = ObjDataOld.objListgoAML_Ref_Director_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = id).FirstOrDefault()
            End If
            If objCustomerPhonesDetail Is Nothing Then
                objCustomerPhonesDetail = ObjDataNew.objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = id).FirstOrDefault()
            End If
            If objCustomerPhonesDetail Is Nothing AndAlso ObjDataOld IsNot Nothing Then
                objCustomerPhonesDetail = ObjDataOld.objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = id).FirstOrDefault()
            End If

            Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

            If Not objCustomerPhonesDetail Is Nothing Then
                FormPanelDirectorPhone.Hidden = False
                WindowDetailDirectorPhone.Hidden = False
                Director_cb_phone_Contact_Type.ReadOnly = True
                Director_cb_phone_Communication_Type.ReadOnly = True
                Director_txt_phone_Country_prefix.ReadOnly = True
                Director_txt_phone_number.ReadOnly = True
                Director_txt_phone_extension.ReadOnly = True
                Director_txt_phone_comments.ReadOnly = True

                WindowDetailDirectorPhone.Title = "Phone Detail"
                With objCustomerPhonesDetail
                    Director_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                    Director_Store_Contact_Type.DataBind()
                    Director_cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)

                    Director_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                    Director_Store_Communication_Type.DataBind()
                    Director_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                    Director_txt_phone_Country_prefix.Text = .tph_country_prefix
                    Director_txt_phone_number.Text = .tph_number
                    Director_txt_phone_extension.Text = .tph_extension
                    Director_txt_phone_comments.Text = .comments
                End With
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadDataDetailEmployerPhone(id As Long)

        'Dim objCustomerPhonesDetail As goAML_Ref_Phone = goAML_CustomerBLL.GetPhonesByID(id)
        Dim objCustomerPhonesDetail
        objCustomerPhonesDetail = ObjDataNew.objListgoAML_Ref_Employer_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = id).FirstOrDefault()
        If objCustomerPhonesDetail Is Nothing Then
            objCustomerPhonesDetail = ObjDataOld.objListgoAML_Ref_Employer_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = id).FirstOrDefault()
        End If

        Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            cb_phone_Contact_Type.ReadOnly = True
            cb_phone_Communication_Type.ReadOnly = True
            txt_phone_Country_prefix.ReadOnly = True
            txt_phone_number.ReadOnly = True
            txt_phone_extension.ReadOnly = True
            txt_phone_comments.ReadOnly = True


            WindowDetail.Title = "Phone Detail"
            ClearinputCustomerPhones()
            With objCustomerPhonesDetail
                Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                Store_Contact_Type.DataBind()
                'Phone_Contact_Type = db.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Keterangan = objCustomerPhonesDetail.Contact_Type).FirstOrDefault
                'cb_phone_Contact_Type.SetValue(Phone_Contact_Type.Kode)
                cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)

                Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                Store_Communication_Type.DataBind()
                'Phone_Communication_Type = db.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Keterangan = objCustomerPhonesDetail.Communcation_Type).FirstOrDefault
                'cb_phone_Communication_Type.SetValue(Phone_Communication_Type.Kode)
                cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                txt_phone_Country_prefix.Text = .tph_country_prefix
                txt_phone_number.Text = .tph_number
                txt_phone_extension.Text = .tph_extension
                txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub ClearinputCustomerIdentification()
        cmb_Jenis_Dokumen.Value = Nothing
        txt_identification_comment.Text = ""
        txt_issued_by.Text = ""
        txt_issued_date.Text = ""
        txt_issued_expired_date.Text = ""
        cmb_issued_country.Value = Nothing
        txt_number_identification.Text = ""
    End Sub

    Sub ClearinputCustomerAddress()
        cmb_kategoriaddress.Value = Nothing
        cmb_kodenegara.Value = Nothing
        Address_INDV.Text = ""
        Town_INDV.Text = ""
        City_INDV.Text = ""
        Zip_INDV.Text = ""
        State_INDVD.Text = ""
        Comment_Address_INDVD.Text = ""
    End Sub
    Sub ClearinputDirectorIdentification()
        cmb_Jenis_Dokumen_Director.Value = Nothing
        txt_identification_comment_Director.Text = ""
        txt_issued_by_Director.Text = ""
        txt_issued_date_Director.Text = ""
        txt_issued_expired_date_Director.Text = ""
        cmb_issued_country_Director.Value = Nothing
        txt_number_identification_Director.Text = ""
    End Sub



    Protected Sub GrdCmdEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailEmployerPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataEmployerAddressDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataEmployerAddressDetail(id As Long)

        'Dim objCustomerAddressDetail = goAML_CustomerBLL.GetAddressByID(id)
        Dim objCustomerAddressDetail
        objCustomerAddressDetail = ObjDataNew.objlistgoaml_Ref_Employer_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        If objCustomerAddressDetail Is Nothing Then
            objCustomerAddressDetail = ObjDataOld.objlistgoaml_Ref_Employer_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        End If
        If Not objCustomerAddressDetail Is Nothing Then
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            WindowDetailAddress.Title = "Address Detail"
            cmb_kategoriaddress.ReadOnly = True
            Address_INDV.ReadOnly = True
            Town_INDV.ReadOnly = True
            City_INDV.ReadOnly = True
            Zip_INDV.ReadOnly = True
            cmb_kodenegara.ReadOnly = True
            State_INDVD.ReadOnly = True
            Comment_Address_INDVD.ReadOnly = True
            ClearinputCustomerAddress()
            Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            Store_KategoriAddress.DataBind()
            Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
            If kategorikontak IsNot Nothing Then
                cmb_kategoriaddress.SetValue(kategorikontak.Kode)
            End If

            Address_INDV.Text = objCustomerAddressDetail.Address
            Town_INDV.Text = objCustomerAddressDetail.Town
            City_INDV.Text = objCustomerAddressDetail.City
            Zip_INDV.Text = objCustomerAddressDetail.Zip
            Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            Store_Kode_Negara.DataBind()
            Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerAddressDetail.Country_Code)
            If kodenegara IsNot Nothing Then
                cmb_kodenegara.SetValue(kodenegara.Kode)
            End If

            State_INDVD.Text = objCustomerAddressDetail.State
            Comment_Address_INDVD.Text = objCustomerAddressDetail.Comments
        End If
    End Sub

    Protected Sub btnBackSaveDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelDetailDirector.Hidden = True
            WindowDetailDirector.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelDirectorPhone.Hidden = True
            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_Director.Hidden = True
            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveCustomerAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_INDVD.Hidden = True
            WindowDetailAddress.Hidden = True
            ClearinputCustomerAddress()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Identification_INDV.Hidden = True
            WindowDetailIdentification.Hidden = True
            ClearinputCustomerIdentification()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Identification_Director.Hidden = True
            WindowDetailIdentification.Hidden = True
            ClearinputDirectorIdentification()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btnBackCustomer_Email_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Email.Hidden = True
            FormPanel_Email.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_EmploymentHistory_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Employment_History.Hidden = True
            FormPanel_Employment_History.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_PersonPEP_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Person_PEP.Hidden = True
            FormPanel_Person_PEP.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_NetworkDevice_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_NetworkDevice.Hidden = True
            FormPanel_NetworkDevice.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackCustomer_SocialMedia_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_SocialMedia.Hidden = True
            FormPanel_SocialMedia.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_Sanction_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Sanction.Hidden = True
            FormPanel_Sanction.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_RelatedPerson_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_RelatedPerson.Hidden = True
            FormPanel_RelatedPerson.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_AdditionalInfo_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_AdditionalInfo.Hidden = True
            FormPanel_AdditionalInfo.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_Url_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_URL.Hidden = True
            FormPanel_URL.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_EntityIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_EntityIdentification.Hidden = True
            FormPanel_EntityIdentification.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackCustomer_RelatedEntities_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_RelatedEntities.Hidden = True
            FormPanel_RelatedEntities.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#End Region

#End Region


    <DirectMethod>
    Public Sub Yes()
        Try
            GoAMLBLL.goAML_CustomerBLL.Accept(Me.IDReq)

            LblConfirmation.Text = "Data Approved. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSave_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If Not String.IsNullOrEmpty(ObjFieldData) Then
                Dim objNew As GoAMLBLL.goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(ObjFieldData, GetType(GoAMLBLL.goAML_CustomerDataBLL))
                Dim txtGCN As String = objNew.objgoAML_Ref_Customer.GCN
                Dim checkGCN As GoAMLDAL.goAML_Ref_Customer = GoAMLBLL.goAML_CustomerBLL.GetCheckGCN(txtGCN)
                If objNew.objgoAML_Ref_Customer.isGCNPrimary = True Then
                    If checkGCN IsNot Nothing Then
                        If checkGCN.CIF <> objNew.objgoAML_Ref_Customer.CIF Then
                            Dim yesBtn = New MessageBoxButtonConfig
                            'Handler = "App.direct.DoThing({ eventMask: { showMask: true, msg: 'Please wait ...' }})",
                            yesBtn.Handler = "NawadataDirect.Yes({ eventMask: { showMask: true, msg: 'Please wait ...' }})"
                            yesBtn.Text = "Ok"

                            Dim listBtn = New MessageBoxButtonsConfig
                            listBtn.Yes = yesBtn

                            Ext.Net.X.MessageBox.Confirm("Information", "Konfirmasi : Untuk GCN " + txtGCN + " sudah memiliki GCNPrimary pada CIF " + checkGCN.CIF + " Jika di save, status GCNPrimary di CIF " + checkGCN.CIF + " akan dilepas dan akan dipasang di CIF ini. Tekan Tombol 'OK' untuk melanjutkan", listBtn).Show()
                        Else
                            GoAMLBLL.goAML_CustomerBLL.Accept(Me.IDReq)

                            LblConfirmation.Text = "Data Approved. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
                            container.Hidden = True
                            Panelconfirmation.Hidden = False
                            container.Render()
                            Panelconfirmation.Render()
                        End If
                    Else
                        GoAMLBLL.goAML_CustomerBLL.Accept(Me.IDReq)

                        LblConfirmation.Text = "Data Approved. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
                        container.Hidden = True
                        Panelconfirmation.Hidden = False
                        container.Render()
                        Panelconfirmation.Render()
                    End If

                Else
                    GoAMLBLL.goAML_CustomerBLL.Accept(Me.IDReq)

                    LblConfirmation.Text = "Data Approved. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
                    container.Hidden = True
                    Panelconfirmation.Hidden = False
                    container.Render()
                    Panelconfirmation.Render()
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub


    Protected Sub BtnReject_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            GoAMLBLL.goAML_CustomerBLL.Reject(Me.IDReq)
            LblConfirmation.Text = "Data Rejected. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objSchemaModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs) Handles BtnConfirmation.DirectClick
        Try


            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objSchemaModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub LoadData(gridPanel As String, objData As String)
        Dim objgoAML_CustomerDataBLL As GoAMLBLL.goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objData, GetType(GoAMLBLL.goAML_CustomerDataBLL))

        'Dim data_director = goAML_Ref_Customer_Entity_Director_Mapper.Serialize(objgoAML_CustomerDataBLL.objListgoAML_Ref_Director)

        'For Each data As GoAMLDAL.goAML_Person_Identification In objgoAML_CustomerDataBLL.objListgoAML_Ref_Identification_Director
        '    Dim item_director = data_director.FirstOrDefault(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = data.FK_Person_ID)

        '    'item_director.ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(data)

        'Next

        'For Each data As GoAMLBLL.goAML_Customer.DataModel.goAML_Ref_Customer_Entity_Director In data_director
        '    data.ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objgoAML_CustomerDataBLL.objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = data.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
        '    data.ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objgoAML_CustomerDataBLL.objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = data.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
        '    data.ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objgoAML_CustomerDataBLL.objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = data.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
        '    data.ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objgoAML_CustomerDataBLL.objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = data.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
        '    data.ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objgoAML_CustomerDataBLL.objlistgoaml_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = data.PK_goAML_Ref_Customer_Entity_Director_ID).ToList())
        '    data.ObjList_GoAML_Ref_Customer_Email = 
        'Next

        Dim data_director = objgoAML_CustomerDataBLL.ObjListgoAML_Ref_Director2

        'Dim unique_name = "ApprovalDetail"

        'If gridPanel = "New" Then
        '    'GridRelatedEntity_New.UniqueName = unique_name & "_RelatedEntity_" & gridPanel
        '    'GridRelatedPerson_New.UniqueName = unique_name & "_RelatedPerson_" & gridPanel
        '    'GridDirector_New.UniqueName = unique_name & "_Director_" & gridPanel
        '    GridEmail_New.LoadData(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email)
        '    GridRelatedEntity_New.LoadData(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedEntities)
        '    GridRelatedPerson_New.LoadData(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson)
        '    GridDirector_New.LoadData(data_director)
        'ElseIf gridPanel = "Old" Then
        '    'GridRelatedEntity_Old.UniqueName = unique_name & "_RelatedEntity_" & gridPanel
        '    'GridRelatedPerson_Old.UniqueName = unique_name & "_RelatedPerson_" & gridPanel
        '    'GridDirector_Old.UniqueName = unique_name & "_Director_" & gridPanel

        '    GridEmail_Old.LoadData(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email)
        '    GridRelatedEntity_Old.LoadData(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedEntities)
        '    GridRelatedPerson_Old.LoadData(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson)
        '    GridDirector_New.LoadData(data_director)
        'End If

    End Sub
End Class