﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GridEmail.ascx.vb" Inherits="goAML_Customer_v501_Component_GridEmail" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<%-- Email Address --%>
<ext:GridPanel ID="GridPanel" runat="server" Title="Email" AutoScroll="true">
    <TopBar>
        <ext:Toolbar ID="tbr_email" runat="server">
            <Items>
                <ext:Button ID="btnAdd" runat="server" Text="Add Email" Icon="Add">
                    <DirectEvents>
                        <Click OnEvent="btnAdd_DirectClick">
                            <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Items>
        </ext:Toolbar>
    </TopBar>
    <Store>
        <ext:Store ID="Store" runat="server" IsPagingStore="true" PageSize="8">
            <Model>
                <ext:Model ID="model_email" runat="server">
                    <%--to do--%>
                    <Fields>
                        <ext:ModelField Name="PK_goAML_Ref_Customer_Email_ID" Type="String"></ext:ModelField>
                        <ext:ModelField Name="EMAIL_ADDRESS" Type="String"></ext:ModelField>
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:RowNumbererColumn ID="rnc_indv_email" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
            <ext:CommandColumn ID="CC_INDV_Email" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                    <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                    <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Email_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Email_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:Column ID="clm_indv_email_address" runat="server" DataIndex="EMAIL_ADDRESS" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
            
        </Columns>
    </ColumnModel>
    <BottomBar>
        <ext:PagingToolbar ID="pt_indv_email" runat="server" HideRefresh="True" />
    </BottomBar>
</ext:GridPanel>
<%-- End of Email Address --%>

<ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Email Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
    <Items>
        <ext:FieldSet ID="FieldSet6" runat="server" Title="Email" Collapsible="true" Layout="FitLayout">
            <Items>
            </Items>
            <Content>
                <ext:FormPanel ID="FormPanel" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                    <Items>
                        <ext:TextField ID="txtCustomer_email_address" runat="server" FieldLabel="Email Address" AnchorHorizontal="90%" AllowBlank="False" MaxLength="255"></ext:TextField>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btn_Customer_Save_Email" runat="server" Text="Save" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnSave_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button ID="btn_Customer_Back_Email" runat="server" Text="Back" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnBack_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>

                    </Buttons>
                </ext:FormPanel>
            </Content>
        </ext:FieldSet>
    </Items>
</ext:Window>