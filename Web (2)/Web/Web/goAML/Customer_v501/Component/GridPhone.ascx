﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GridPhone.ascx.vb" Inherits="goAML_Customer_v501_Component_GridPhone" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<%-- Grid Phone --%>
<ext:GridPanel ID="GridPanel" runat="server" Title="Phone" AutoScroll="true">
    <TopBar>
        <ext:Toolbar ID="toolbar" runat="server">
            <Items>
                <ext:Button ID="btnAdd" runat="server" Text="Add Phones" Icon="Add">
                    <DirectEvents>
                        <Click OnEvent="btnAdd_DirectClick">
                            <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Items>
        </ext:Toolbar>
    </TopBar>
    <Store>
        <ext:Store ID="Store" runat="server" IsPagingStore="true" PageSize="8">
            <Model>
                <ext:Model ID="Model_INDV_Phones" runat="server">
                    <Fields>
                        <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                        <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                        <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                        <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                        <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:RowNumbererColumn ID="RowNumber_INDV_Phones" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                    <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                    <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:Column ID="coltph_contact_type" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="coltph_communication_type" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="coltph_country_prefix" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="coltph_number" runat="server" DataIndex="tph_number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Coltph_extension" runat="server" DataIndex="tph_extension" Text="Extension" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="ColComments" runat="server" DataIndex="comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
        </Columns>
    </ColumnModel>
    <BottomBar>
        <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
    </BottomBar>
</ext:GridPanel>
<%-- End of Grid Phone --%>

<%--Pop Up--%>
<ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Phone Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
    <Items>
        <ext:FieldSet ID="FieldSetCustomerPhoneAdd" runat="server" Title="Phones" Collapsible="true" Layout="FitLayout">
            <Items>
            </Items>
            <Content>
                <ext:FormPanel ID="FormPanelTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                    <Items>
                        <ext:Hidden runat="server" ID="fld_sender"></ext:Hidden>

                        <ext:Panel ID="Pnl_cb_phone_Contact_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:NDSDropDownField ID="ascx_cb_phone_Contact_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringFilter="Active = 1" EmptyText="Select One" StringTable="goAML_Ref_Kategori_Kontak" Label="Contact Type" AnchorHorizontal="70%" AllowBlank="false" />
                            </Content>
                        </ext:Panel>
                        <ext:Panel ID="Pnl_cb_phone_Communication_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:NDSDropDownField ID="ascx_cb_phone_Communication_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringFilter="Active = 1" EmptyText="Select One" StringTable="goAML_Ref_Jenis_Alat_Komunikasi" Label="Communication Type" AnchorHorizontal="70%" AllowBlank="false" />
                            </Content>
                        </ext:Panel>

                        <ext:TextField ID="txt_phone_Country_prefix" runat="server" FieldLabel="Country Prefix" AnchorHorizontal="90%"></ext:TextField>
                        <ext:TextField ID="txt_phone_number" runat="server" AllowBlank="False" FieldLabel="Phone Number" AnchorHorizontal="90%"></ext:TextField>
                        <ext:TextField ID="txt_phone_extension" runat="server" FieldLabel="Extension" AnchorHorizontal="90%"></ext:TextField>
                        <ext:TextField ID="txt_phone_comments" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnSave" runat="server" Text="Save" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnSave_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button ID="btnBack" runat="server" Text="Back" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnBack_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>

                    </Buttons>
                </ext:FormPanel>
            </Content>
        </ext:FieldSet>
    </Items>
</ext:Window>
<%--End Pop Up--%>
