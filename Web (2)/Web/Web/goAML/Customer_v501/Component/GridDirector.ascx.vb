﻿
Imports GoAMLBLL
Imports GoAMLBLL.goAML.DataModel
Imports GoAMLBLL.Helper

Partial Class goAML_Customer_v501_Component_GridDirector
    Inherits System.Web.UI.UserControl
    Implements IGoAML_Grid(Of goAML_Ref_Entity_Director)

    Private Const GRID_TITLE As String = "Director"
    Private Const SESSION_PREFIX As String = "CUSTOMER.GridDirector."

    Private _uniqueName As String
    Public Property UniqueName As String Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).UniqueName
        Get
            'Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME)
            Return _uniqueName
        End Get
        Set(value As String)
            'Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME) = value
            GridIdentification_Director.UniqueName = value
            GridPhone_Director.UniqueName = value
            GridAddress_Director.UniqueName = value
            GridPhoneWork_Director.UniqueName = value
            GridAddressWork_Director.UniqueName = value
            GridEmail_Director.UniqueName = value
            GridSanction_Director.UniqueName = value
            GridPEP_Director.UniqueName = value
            _uniqueName = value
        End Set
    End Property

    Public Property FK_REF_DETAIL_OF As Integer Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).FK_REF_DETAIL_OF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_FK_REF_DETAIL_OF & UniqueName)
        End Get
        Set(value As Integer)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_FK_REF_DETAIL_OF & UniqueName) = value
        End Set
    End Property

    Public Property CIF As String Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).CIF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName)
        End Get
        Set(value As String)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_Edit As goAML_Ref_Entity_Director Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).objTemp_Edit
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName)
        End Get
        Set(value As goAML_Ref_Entity_Director)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref As goAML_Ref_Entity_Director Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).objTemp_goAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName)
        End Get
        Set(value As goAML_Ref_Entity_Director)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_Ref As List(Of goAML_Ref_Entity_Director) Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).objListgoAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Entity_Director))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_vw As List(Of goAML_Ref_Entity_Director) Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).objListgoAML_vw
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Entity_Director))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName) = value
        End Set
    End Property

    Private _title As String
    Public Property Title As String Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).Title
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Public ReadOnly Property SessionPrefix As String Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).SessionPrefix
        Get
            Return SESSION_PREFIX
        End Get
    End Property

    Public Property IsViewMode As Boolean Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).IsViewMode
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName)
        End Get
        Set(value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName) = value
        End Set
    End Property

    Public Property tempflag() As Integer
        Get
            Return Session(SessionPrefix & "tempflag-" & UniqueName)
        End Get
        Set(ByVal value As Integer)
            Session(SessionPrefix & "tempflag-" & UniqueName) = value
        End Set
    End Property

    Public Property IsDataChange As Boolean Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).IsDataChange
        Get
            If Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) Is Nothing Then
                Return False
            End If
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName)
        End Get
        Set(value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) = value
        End Set
    End Property

    Private Shadows Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim uniqueName = "Director"
        'GridIdentification_Director.UniqueName = uniqueName
        'GridPhone_Director.UniqueName = uniqueName
        'GridAddress_Director.UniqueName = uniqueName
        'GridPhoneWork_Director.UniqueName = uniqueName
        'GridAddressWork_Director.UniqueName = uniqueName
        'GridEmail_Director.UniqueName = uniqueName
        'GridSanction_Director.UniqueName = uniqueName
        'GridPEP_Director.UniqueName = uniqueName

        'If IsViewMode Then
        '    Dim cmdColumn As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        '    If cmdColumn IsNot Nothing Then
        '        Dim cmd1 As GridCommand = cmdColumn.Commands(1)
        '        Dim cmd2 As GridCommand = cmdColumn.Commands(2)
        '        cmd1.Hidden = IsViewMode
        '        cmd2.Hidden = IsViewMode
        '    End If

        '    btnAdd.Hidden = IsViewMode

        '    GridIdentification_Director.IsViewMode = IsViewMode
        '    GridPhone_Director.IsViewMode = IsViewMode
        '    GridAddress_Director.IsViewMode = IsViewMode
        '    GridPhoneWork_Director.IsViewMode = IsViewMode
        '    GridAddressWork_Director.IsViewMode = IsViewMode
        '    GridEmail_Director.IsViewMode = IsViewMode
        '    GridSanction_Director.IsViewMode = IsViewMode
        '    GridPEP_Director.IsViewMode = IsViewMode
        'End If
    End Sub
    Private Shadows Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender

        Dim uniqueName = "Director"
        GridIdentification_Director.UniqueName = uniqueName
        GridPhone_Director.UniqueName = uniqueName
        GridAddress_Director.UniqueName = uniqueName
        GridPhoneWork_Director.UniqueName = uniqueName
        GridAddressWork_Director.UniqueName = uniqueName
        GridEmail_Director.UniqueName = uniqueName
        GridSanction_Director.UniqueName = uniqueName
        GridPEP_Director.UniqueName = uniqueName

        Dim cmdColumnEdit As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        Dim cmdColumnDetail As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(2), CommandColumn)
        cmdColumnEdit.Hidden = IsViewMode
        cmdColumnDetail.Hidden = Not IsViewMode

        'Dim store As Store = TryCast(GridPanel.Store(0), Store)
        'store.DataBind()

        btnAdd.Hidden = IsViewMode

        GridIdentification_Director.IsViewMode = IsViewMode
        GridPhone_Director.IsViewMode = IsViewMode
        GridAddress_Director.IsViewMode = IsViewMode
        GridPhoneWork_Director.IsViewMode = IsViewMode
        GridAddressWork_Director.IsViewMode = IsViewMode
        GridEmail_Director.IsViewMode = IsViewMode
        GridSanction_Director.IsViewMode = IsViewMode
        GridPEP_Director.IsViewMode = IsViewMode
    End Sub

    Public Sub btnAdd_DirectClick(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).btnAdd_DirectClick
        Try
            tempflag = 0
            FormPanel.Hidden = False
            WindowDetail.Hidden = False
            cmb_peran.IsReadOnly = False
            txt_Director_Title.ReadOnly = False
            txt_Director_Last_Name.ReadOnly = False
            txt_Director_SSN.ReadOnly = False
            txt_Director_BirthDate.ReadOnly = False
            txt_Director_Birth_Place.ReadOnly = False
            txt_Director_Mothers_Name.ReadOnly = False
            txt_Director_Alias.ReadOnly = False
            txt_Director_Passport_Number.ReadOnly = False

            cmb_Director_Passport_Country.IsReadOnly = False

            txt_Director_ID_Number.ReadOnly = False
            cmb_Director_Nationality1.IsReadOnly = False
            cmb_Director_Nationality2.IsReadOnly = False
            cmb_Director_Nationality3.IsReadOnly = False
            cmb_Director_Gender.IsReadOnly = False
            cmb_Director_Residence.IsReadOnly = False
            txt_Director_Occupation.ReadOnly = False
            txt_Director_employer_name.ReadOnly = False
            cb_Director_Deceased.ReadOnly = False
            txt_Director_Deceased_Date.ReadOnly = False
            txt_Director_Tax_Number.ReadOnly = False
            cb_Director_Tax_Reg_Number.ReadOnly = False
            txt_Director_Source_of_Wealth.ReadOnly = False
            txt_Director_Comments.ReadOnly = False
            btnSave.Hidden = False
            'btnsavedirectorphone.Hidden = False
            'btnsavedirectorAddress.Hidden = False
            'btnsaveEmpdirectorPhone.Hidden = False
            'btnsaveEmpdirectorAddress.Hidden = False
            'btnaddidentificationdirector.Hidden = False
            WindowDetail.Title = Title & " Add"

            Clearinput()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnSave_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).btnSave_DirectEvent
        Try
            If IsData_Valid() Then

                If objTemp_Edit Is Nothing Then
                    Save()
                Else
                    Save(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnBack_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).btnBack_DirectEvent
        Try
            FormPanel.Hidden = True
            WindowDetail.Hidden = True
            Clearinput()
            objTemp_Edit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub GrdCmd(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).GrdCmd
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub checkBoxcb_Director_Deceased_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_Director_Deceased.DirectCheck
        Try
            If cb_Director_Deceased.Checked Then
                txt_Director_Deceased_Date.Hidden = False
            Else
                txt_Director_Deceased_Date.Hidden = True
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub LoadData(id As Long, isEdit As Boolean) Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).LoadData
        Try
            objTemp_goAML_Ref = objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
            objTemp_Edit = objListgoAML_Ref.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id).FirstOrDefault()

            If Not objTemp_Edit Is Nothing Then
                FormPanel.Hidden = False
                WindowDetail.Hidden = False
                btnSave.Hidden = Not isEdit
                WindowDetail.Title = Title & " Detail"

                cmb_peran.IsReadOnly = Not isEdit
                txt_Director_Title.ReadOnly = Not isEdit
                txt_Director_Last_Name.ReadOnly = Not isEdit
                txt_Director_SSN.ReadOnly = Not isEdit
                txt_Director_BirthDate.ReadOnly = Not isEdit
                txt_Director_Birth_Place.ReadOnly = Not isEdit
                txt_Director_Mothers_Name.ReadOnly = Not isEdit
                txt_Director_Alias.ReadOnly = Not isEdit
                txt_Director_Passport_Number.ReadOnly = Not isEdit
                cmb_Director_Passport_Country.IsReadOnly = Not isEdit
                txt_Director_ID_Number.ReadOnly = Not isEdit
                cmb_Director_Nationality1.IsReadOnly = Not isEdit
                cmb_Director_Nationality2.IsReadOnly = Not isEdit
                cmb_Director_Nationality3.IsReadOnly = Not isEdit
                cmb_Director_Gender.IsReadOnly = Not isEdit
                cmb_Director_Residence.IsReadOnly = Not isEdit

                txt_Director_Occupation.ReadOnly = Not isEdit
                txt_Director_employer_name.ReadOnly = Not isEdit
                cb_Director_Deceased.ReadOnly = Not isEdit
                txt_Director_Deceased_Date.ReadOnly = Not isEdit
                txt_Director_Tax_Number.ReadOnly = Not isEdit
                cb_Director_Tax_Reg_Number.ReadOnly = Not isEdit
                txt_Director_Source_of_Wealth.ReadOnly = Not isEdit
                txt_Director_Comments.ReadOnly = Not isEdit

                Dim peran = goAML_CustomerBLL.GetRoleDirectorByID(objTemp_Edit.Role)
                If peran IsNot Nothing Then
                    cmb_peran.SetTextWithTextValue(peran.Kode, peran.Keterangan)
                End If
                txt_Director_Title.Text = objTemp_Edit.Title
                txt_Director_Last_Name.Text = objTemp_Edit.Last_Name
                txt_Director_SSN.Text = objTemp_Edit.SSN
                If objTemp_Edit.BirthDate IsNot Nothing Then
                    txt_Director_BirthDate.Text = objTemp_Edit.BirthDate
                End If
                txt_Director_Birth_Place.Text = objTemp_Edit.Birth_Place
                txt_Director_Mothers_Name.Text = objTemp_Edit.Mothers_Name
                txt_Director_Alias.Text = objTemp_Edit.Alias
                txt_Director_ID_Number.Text = objTemp_Edit.ID_Number
                txt_Director_Passport_Number.Text = objTemp_Edit.Passport_Number
                'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
                Dim director_passport_country = goAML_CustomerBLL.GetNamaNegaraByID(objTemp_Edit.Passport_Country)
                If director_passport_country IsNot Nothing Then
                    cmb_Director_Passport_Country.SetTextWithTextValue(director_passport_country.Kode, director_passport_country.Keterangan)
                End If
                Dim director_Nationality1 = goAML_CustomerBLL.GetNamaNegaraByID(objTemp_Edit.Nationality1)
                If director_Nationality1 IsNot Nothing Then
                    cmb_Director_Nationality1.SetTextWithTextValue(director_Nationality1.Kode, director_Nationality1.Keterangan)
                End If
                Dim director_Nationality2 = goAML_CustomerBLL.GetNamaNegaraByID(objTemp_Edit.Nationality2)
                If director_Nationality2 IsNot Nothing Then
                    cmb_Director_Nationality2.SetTextWithTextValue(director_Nationality2.Kode, director_Nationality2.Keterangan)
                End If
                Dim director_Nationality3 = goAML_CustomerBLL.GetNamaNegaraByID(objTemp_Edit.Nationality3)
                If director_Nationality3 IsNot Nothing Then
                    cmb_Director_Nationality3.SetTextWithTextValue(director_Nationality3.Kode, director_Nationality3.Keterangan)
                End If
                Dim director_Residence = goAML_CustomerBLL.GetNamaNegaraByID(objTemp_Edit.Residence)
                If director_Residence IsNot Nothing Then
                    cmb_Director_Residence.SetTextWithTextValue(director_Residence.Kode, director_Residence.Keterangan)
                End If
                Dim gender = goAML_CustomerBLL.GetJenisKelamin(objTemp_Edit.Gender)
                If gender IsNot Nothing Then
                    cmb_Director_Gender.SetTextWithTextValue(gender.Kode, gender.Keterangan)
                End If
                'End Update

                txt_Director_Occupation.Text = objTemp_Edit.Occupation
                txt_Director_employer_name.Text = objTemp_Edit.Employer_Name
                If objTemp_Edit.Deceased = True Then
                    cb_Director_Deceased.Checked = True
                    txt_Director_Deceased_Date.Value = objTemp_Edit.Deceased_Date
                End If
                txt_Director_Tax_Number.Text = objTemp_Edit.Tax_Number
                If objTemp_Edit.Tax_Reg_Number = True Then
                    cb_Director_Tax_Reg_Number.Checked = True
                End If
                txt_Director_Source_of_Wealth.Text = objTemp_Edit.Source_of_Wealth
                txt_Director_Comments.Text = objTemp_Edit.Comments

                GridIdentification_Director.LoadData(objTemp_Edit.ObjList_GoAML_Person_Identification)
                GridPhone_Director.LoadData(objTemp_Edit.ObjList_GoAML_Ref_Phone)
                GridAddress_Director.LoadData(objTemp_Edit.ObjList_GoAML_Ref_Address)
                GridPhoneWork_Director.LoadData(objTemp_Edit.ObjList_GoAML_Ref_Phone_Work)
                GridAddressWork_Director.LoadData(objTemp_Edit.ObjList_GoAML_Ref_Address_Work)
                GridEmail_Director.LoadData(objTemp_Edit.ObjList_GoAML_Ref_Customer_Email)
                GridSanction_Director.LoadData(objTemp_Edit.ObjList_GoAML_Ref_Customer_Sanction)
                GridPEP_Director.LoadData(objTemp_Edit.ObjList_GoAML_Ref_Customer_PEP)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub LoadData(data As List(Of goAML_Ref_Entity_Director)) Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).LoadData
        objListgoAML_vw = data.Select(Function(x) x).ToList()
        objListgoAML_Ref = data.Select(Function(x) x).ToList()

        If objListgoAML_vw IsNot Nothing Then
            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()
        Else
            Store.DataBind()
        End If
    End Sub

    Public Sub DeleteRecord(id As Long) Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).DeleteRecord
        Dim objDelVWDirector = objListgoAML_vw.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
        'Dim objDelVWAddress As List(Of GoAMLDAL.Vw_Director_Addresses) = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        'Dim objDelVWPhone As List(Of GoAMLDAL.Vw_Director_Phones) = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        'Dim objDelVWEMPAddress As List(Of GoAMLDAL.Vw_Director_Employer_Addresses) = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        'Dim objDelEMPVWPhone As List(Of GoAMLDAL.Vw_Director_Employer_Phones) = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        'Dim objDelVWIdentification As List(Of GoAMLDAL.Vw_Person_Identification) = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()

        'Dim objxDelDirector = objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Entity_Director_ID = id)
        'Dim objxDelAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        'Dim objxDelPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        'Dim objxDelEMPAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        'Dim objxDelEMPPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        'Dim objxDelIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()

        'If Not objxDelDirector Is Nothing Then
        '    objListgoAML_Ref.Remove(objxDelDirector)
        '    For Each item As GoAMLDAL.goAML_Ref_Address In objxDelAddress
        '        objListgoAML_Ref_Director_Address.Remove(item)
        '    Next
        '    For Each item As GoAMLDAL.goAML_Ref_Phone In objxDelPhone
        '        objListgoAML_Ref_Director_Phone.Remove(item)
        '    Next
        '    For Each item As GoAMLDAL.goAML_Ref_Address In objxDelEMPAddress
        '        objListgoAML_Ref_Director_Employer_Address.Remove(item)
        '    Next
        '    For Each item As GoAMLDAL.goAML_Ref_Phone In objxDelEMPPhone
        '        objListgoAML_Ref_Director_Employer_Phone.Remove(item)
        '    Next
        '    For Each item As GoAMLDAL.goAML_Person_Identification In objxDelIdentification
        '        objListgoAML_Ref_Identification_Director.Remove(item)
        '    Next
        'End If

        'If Not objDelVWDirector Is Nothing Then
        '    objListgoAML_vw.Remove(objDelVWDirector)
        '    For Each item As GoAMLDAL.Vw_Director_Addresses In objDelVWAddress
        '        objListgoAML_vw_Director_Addresses.Remove(item)
        '    Next
        '    For Each item As GoAMLDAL.Vw_Director_Phones In objDelVWPhone
        '        objListgoAML_vw_Director_Phones.Remove(item)
        '    Next
        '    For Each item As GoAMLDAL.Vw_Director_Employer_Addresses In objDelVWEMPAddress
        '        objListgoAML_vw_Director_Employer_Addresses.Remove(item)
        '    Next
        '    For Each item As GoAMLDAL.Vw_Director_Employer_Phones In objDelEMPVWPhone
        '        objListgoAML_vw_Director_Employer_Phones.Remove(item)
        '    Next
        '    For Each item As GoAMLDAL.Vw_Person_Identification In objDelVWIdentification
        '        objListgoAML_vw_Customer_Identification_Director.Remove(item)
        '    Next

        '    Store.DataSource = objListgoAML_vw.ToList()
        '    Store.DataBind()
        'End If


        FormPanel.Hidden = True
        WindowDetail.Hidden = True
        Clearinput()
    End Sub

    Public Sub Clearinput() Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).Clearinput
        cmb_peran.SetTextValue("")
        txt_Director_Title.Text = ""
        txt_Director_Last_Name.Text = ""
        txt_Director_SSN.Text = ""
        txt_Director_BirthDate.Text = ""
        txt_Director_Birth_Place.Text = ""
        txt_Director_Mothers_Name.Text = ""
        txt_Director_Alias.Text = ""
        txt_Director_Passport_Number.Text = ""

        cmb_Director_Passport_Country.SetTextValue("")

        txt_Director_ID_Number.Text = ""
        cmb_Director_Nationality1.SetTextValue("")
        cmb_Director_Nationality2.SetTextValue("")
        cmb_Director_Nationality3.SetTextValue("")
        cmb_Director_Gender.SetTextValue("")
        cmb_Director_Residence.SetTextValue("")

        txt_Director_Occupation.Text = ""
        txt_Director_employer_name.Text = ""
        cb_Director_Deceased.Checked = False
        txt_Director_Deceased_Date.Value = ""
        txt_Director_Tax_Number.Text = ""
        cb_Director_Tax_Reg_Number.Checked = False
        txt_Director_Source_of_Wealth.Text = ""
        txt_Director_Comments.Text = ""
        'Store_Director_Employer_Address.DataBind()
        'Store_Director_Employer_Phone.DataBind()
        'Store_DirectorAddress.DataBind()
        'Store_DirectorPhone.DataBind()
        'Store_Director_Identification.DataBind()
    End Sub

    Public Sub Save(Optional isNew As Boolean = True) Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).Save
        Try
            If Not isNew Then
                With objTemp_Edit
                    .Role = cmb_peran.SelectedItemText
                    .Last_Name = txt_Director_Last_Name.Text


                End With

                With objTemp_goAML_Ref
                    .Role = cmb_peran.SelectedItemValue
                    .Title = txt_Director_Title.Text
                    .Last_Name = txt_Director_Last_Name.Text
                    .SSN = txt_Director_SSN.Text
                    If txt_Director_BirthDate.RawValue IsNot Nothing Then
                        .BirthDate = Convert.ToDateTime(txt_Director_BirthDate.RawValue)
                    Else
                        .BirthDate = Nothing
                    End If
                    .Birth_Place = txt_Director_Birth_Place.Text
                    .Mothers_Name = txt_Director_Mothers_Name.Text
                    .Alias = txt_Director_Alias.Text
                    .Passport_Number = txt_Director_Passport_Number.Text
                    .Passport_Country = cmb_Director_Passport_Country.SelectedItemValue
                    .ID_Number = txt_Director_ID_Number.Text
                    .Nationality1 = cmb_Director_Nationality1.SelectedItemValue
                    .Nationality2 = cmb_Director_Nationality2.SelectedItemValue
                    .Nationality3 = cmb_Director_Nationality3.SelectedItemValue
                    .Residence = cmb_Director_Residence.SelectedItemValue
                    .Gender = cmb_Director_Gender.SelectedItemValue

                    .Occupation = txt_Director_Occupation.Text
                    .Employer_Name = txt_Director_employer_name.Text
                    .Deceased = Convert.ToBoolean(IIf(cb_Director_Deceased.Checked = True, 1, 0))
                    If cb_Director_Deceased.Checked Then
                        .Deceased_Date = Convert.ToDateTime(txt_Director_Deceased_Date.RawValue)
                    Else
                        .Deceased_Date = Nothing
                    End If
                    .Tax_Number = txt_Director_Tax_Number.Text
                    .Tax_Reg_Number = Convert.ToBoolean(IIf(cb_Director_Tax_Reg_Number.Checked = True, 1, 0))
                    .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
                    .Comments = txt_Director_Comments.Text

                    'For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                    '    item.FK_To_Table_ID = .PK_goAML_Ref_Entity_Director_ID
                    'Next

                    'For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                    '    item.FK_for_Table_ID = .PK_goAML_Ref_Entity_Director_ID
                    'Next

                    'For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                    '    item.FK_To_Table_ID = .PK_goAML_Ref_Entity_Director_ID
                    'Next

                    'For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                    '    item.FK_for_Table_ID = .PK_goAML_Ref_Entity_Director_ID
                    'Next

                    'For Each item As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
                    '    item.FK_Person_ID = .PK_goAML_Ref_Entity_Director_ID
                    'Next

                    'For Each item As GoAMLDAL.Vw_Person_Identification In objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
                    '    item.FK_Person_ID = .PK_goAML_Ref_Entity_Director_ID
                    'Next

                    'For Each item As GoAMLDAL.Vw_Director_Addresses In objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                    '    item.FK_To_Table_ID = .PK_goAML_Ref_Entity_Director_ID
                    'Next

                    'For Each item As GoAMLDAL.Vw_Director_Phones In objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                    '    item.FK_for_Table_ID = .PK_goAML_Ref_Entity_Director_ID
                    'Next

                    'For Each item As GoAMLDAL.Vw_Director_Employer_Addresses In objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                    '    item.FK_To_Table_ID = .PK_goAML_Ref_Entity_Director_ID
                    'Next

                    'For Each item As GoAMLDAL.Vw_Director_Employer_Phones In objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                    '    item.FK_for_Table_ID = .PK_goAML_Ref_Entity_Director_ID
                    'Next


                End With

            Else
                Dim objNewVWgoAML_Customer As New goAML_Ref_Entity_Director
                Dim objNewgoAML_Customer As New goAML_Ref_Entity_Director
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_Customer
                    .PK_goAML_Ref_Customer_Entity_Director_ID = intpk
                    .Last_Name = txt_Director_Last_Name.Text
                    .Role = cmb_peran.SelectedItemText
                End With
                objListgoAML_vw.Add(objNewVWgoAML_Customer)

                With objNewgoAML_Customer
                    .PK_goAML_Ref_Customer_Entity_Director_ID = intpk
                    .Role = cmb_peran.SelectedItemValue
                    .Title = txt_Director_Title.Text
                    .Gender = cmb_Director_Gender.SelectedItemValue
                    .Last_Name = txt_Director_Last_Name.Text
                    If txt_Director_BirthDate.RawValue IsNot Nothing Then
                        .BirthDate = Convert.ToDateTime(txt_Director_BirthDate.RawValue)
                    Else
                        .BirthDate = Nothing
                    End If

                    .Birth_Place = txt_Director_Birth_Place.Text
                    .Mothers_Name = txt_Director_Mothers_Name.Text
                    .Alias = txt_Director_Alias.Text
                    .SSN = txt_Director_SSN.Text
                    .Passport_Number = txt_Director_Passport_Number.Text
                    .Passport_Country = cmb_Director_Passport_Country.SelectedItemValue
                    .ID_Number = txt_Director_ID_Number.Text
                    .Nationality1 = cmb_Director_Nationality1.SelectedItemValue
                    .Nationality2 = cmb_Director_Nationality2.SelectedItemValue
                    .Nationality3 = cmb_Director_Nationality3.SelectedItemValue
                    .Residence = cmb_Director_Residence.SelectedItemValue

                    .Occupation = txt_Director_Occupation.Text
                    .Employer_Name = txt_Director_employer_name.Text
                    .Deceased = Convert.ToBoolean(IIf(cb_Director_Deceased.Checked = True, 1, 0))
                    If cb_Director_Deceased.Checked Then
                        .Deceased_Date = Convert.ToDateTime(txt_Director_Deceased_Date.RawValue)
                    Else
                        .Deceased_Date = Nothing
                    End If
                    .Tax_Number = txt_Director_Tax_Number.Text
                    .Tax_Reg_Number = Convert.ToBoolean(IIf(cb_Director_Tax_Reg_Number.Checked = True, 1, 0))
                    .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
                    .Comments = txt_Director_Comments.Text
                End With

                objListgoAML_Ref.Add(objNewgoAML_Customer)
                'For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                '    item.FK_To_Table_ID = intpk
                'Next

                'For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                '    item.FK_for_Table_ID = intpk
                'Next

                'For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                '    item.FK_To_Table_ID = intpk
                'Next

                'For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                '    item.FK_for_Table_ID = intpk
                'Next

                'For Each item As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
                '    item.FK_Person_ID = intpk
                'Next

                'For Each item As GoAMLDAL.Vw_Person_Identification In objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
                '    item.FK_Person_ID = intpk
                'Next

                'For Each item As GoAMLDAL.Vw_Director_Addresses In objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                '    item.FK_To_Table_ID = intpk
                'Next

                'For Each item As GoAMLDAL.Vw_Director_Phones In objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                '    item.FK_for_Table_ID = intpk
                'Next

                'For Each item As GoAMLDAL.Vw_Director_Employer_Addresses In objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                '    item.FK_To_Table_ID = intpk
                'Next

                'For Each item As GoAMLDAL.Vw_Director_Employer_Phones In objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                '    item.FK_for_Table_ID = intpk
                'Next

            End If

            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()

            FormPanel.Hidden = True
            WindowDetail.Hidden = True
            Clearinput()
            IsDataChange = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Function IsData_Valid() As Boolean Implements IGoAML_Grid(Of goAML_Ref_Entity_Director).IsData_Valid
        'If cmb_director.RawValue = "Select One" Then
        '    Throw New Exception("Please Select Person")
        'End If
        If cmb_peran.SelectedItemValue = "" Then
            Throw New Exception("Role is required")
        End If
        If txt_Director_Occupation.Text = "" Then
            Throw New Exception("Occupation  is required")
        End If
        If txt_Director_Source_of_Wealth.Text = "" Then
            Throw New Exception("Source of Wealth is required")
        End If
        If txt_Director_Last_Name.Text = "" Then
            Throw New Exception("Full Name is required")
        End If
        If txt_Director_BirthDate.RawValue = "" Then
            Throw New Exception("Birth Date is required")
        End If
        If txt_Director_Birth_Place.Text = "" Then
            Throw New Exception("Birth Place is required")
        End If
        'Update: Zikri_08102020
        If txt_Director_SSN.Text.Trim IsNot "" Then
            If Not CommonHelper.IsNumber(txt_Director_SSN.Text) Then
                Throw New Exception("NIK is Invalid")
            End If
            If txt_Director_SSN.Text.Trim.Length <> 16 Then
                Throw New Exception("NIK must contains 16 digit number")
            End If
        End If
        'End Update
        If cmb_Director_Gender.SelectedItemValue = "" Then
            Throw New Exception("Gender is required")
        End If
        If cmb_Director_Nationality1.SelectedItemValue = "" Then
            Throw New Exception("Nationality 1 is required")
        End If
        If cmb_Director_Residence.SelectedItemValue = "" Then
            Throw New Exception("Domicile Country is required")
        End If
        'If objListgoAML_Ref_Director_Phone.Count = 0 Then
        '    Throw New Exception("Phone Director is required")
        'End If
        'If objListgoAML_Ref_Director_Address.Count = 0 Then
        '    Throw New Exception("Address Director is required")
        'End If
        'If Not String.IsNullOrEmpty(txt_Director_ID_Number.Text) Then
        '    If objListgoAML_Ref_Identification_Director.Count = 0 Then
        '        Throw New Exception("Identification is required")
        '    End If
        'End If

        Return True
    End Function

End Class
