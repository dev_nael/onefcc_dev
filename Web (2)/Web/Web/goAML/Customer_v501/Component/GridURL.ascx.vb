﻿
Imports GoAMLBLL
Imports GoAMLBLL.goAML.DataModel
Imports GoAMLBLL.goAML_CustomerDataBLL

Partial Class goAML_Customer_v501_Component_GridURL
    Inherits System.Web.UI.UserControl
    Implements IGoAML_Grid(Of goAML_Ref_Customer_URL)

    Private Const GRID_TITLE As String = "URL"
    Private Const SESSION_PREFIX As String = "CUSTOMER.GridURL."

    Private _uniqueName As String
    Public Property UniqueName As String Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).UniqueName
        Get
            'Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME)
            Return _uniqueName
        End Get
        Set(value As String)
            'Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME) = value
            _uniqueName = value
        End Set
    End Property

    Public Property FK_REF_DETAIL_OF As Integer Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).FK_REF_DETAIL_OF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_FK_REF_DETAIL_OF & UniqueName)
        End Get
        Set(value As Integer)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_FK_REF_DETAIL_OF & UniqueName) = value
        End Set
    End Property

    Public Property CIF As String Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).CIF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName)
        End Get
        Set(ByVal value As String)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_Edit As goAML_Ref_Customer_URL Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).objTemp_Edit
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName)
        End Get
        Set(value As goAML_Ref_Customer_URL)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref As goAML_Ref_Customer_URL Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).objTemp_goAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName)
        End Get
        Set(value As goAML_Ref_Customer_URL)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_Ref As List(Of goAML_Ref_Customer_URL) Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).objListgoAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Customer_URL))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_vw As List(Of goAML_Ref_Customer_URL) Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).objListgoAML_vw
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Customer_URL))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName) = value
        End Set
    End Property

    Private _title As String
    Public Property Title As String Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).Title
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Public ReadOnly Property SessionPrefix As String Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).SessionPrefix
        Get
            Return SESSION_PREFIX
        End Get
    End Property

    Public Property IsViewMode As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).IsViewMode
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName)
        End Get
        Set(ByVal value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName) = value
        End Set
    End Property

    Public Property IsDataChange As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).IsDataChange
        Get
            If Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) Is Nothing Then
                Return False
            End If
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName)
        End Get
        Set(value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) = value
        End Set
    End Property

    Private Shadows Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If IsViewMode Then
        '    Dim cmdColumn As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        '    If cmdColumn IsNot Nothing Then
        '        Dim cmd1 As GridCommand = cmdColumn.Commands(1)
        '        Dim cmd2 As GridCommand = cmdColumn.Commands(2)
        '        cmd1.Hidden = IsViewMode
        '        cmd2.Hidden = IsViewMode
        '    End If

        '    btnAdd.Hidden = IsViewMode
        'End If
    End Sub
    Private Shadows Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Dim cmdColumnEdit As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        Dim cmdColumnDetail As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(2), CommandColumn)
        cmdColumnEdit.Hidden = IsViewMode
        cmdColumnDetail.Hidden = Not IsViewMode

        'Dim store As Store = TryCast(GridPanel.Store(0), Store)
        'store.DataBind()

        btnAdd.Hidden = IsViewMode
    End Sub
    Public Sub btnAdd_DirectClick(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).btnAdd_DirectClick
        Try
            WindowDetail.Hidden = False
            FormPanel.Hidden = False
            btnSave.Hidden = False

            txt_url.ReadOnly = False

            WindowDetail.Title = Title & " Add"

            objTemp_Edit = Nothing
            Clearinput()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnSave_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).btnSave_DirectEvent
        Try
            If IsData_Valid() Then
                If objTemp_Edit Is Nothing Then
                    Save()
                Else
                    Save(False)
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnBack_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).btnBack_DirectEvent
        Try
            WindowDetail.Hidden = True
            FormPanel.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub GrdCmd(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).GrdCmd
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub LoadData(id As Long, isEdit As Boolean) Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).LoadData
        Try
            objTemp_goAML_Ref = objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id)
            objTemp_Edit = objListgoAML_vw.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id)

            If Not objTemp_Edit Is Nothing Then
                FormPanel.Hidden = False
                WindowDetail.Hidden = False
                btnSave.Hidden = Not isEdit

                txt_url.ReadOnly = Not isEdit

                With objTemp_Edit
                    txt_url.Value = .URL
                End With

                WindowDetail.Title = Title & " " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub DeleteRecord(id As Long) Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).DeleteRecord
        objListgoAML_vw.Remove(objListgoAML_vw.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id))
        objListgoAML_Ref.Remove(objListgoAML_Ref.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_URL_ID = id))

        Store.DataSource = objListgoAML_vw.ToList()
        Store.DataBind()
    End Sub

    Public Sub Clearinput() Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).Clearinput
        txt_url.Text = ""
    End Sub

    Public Sub Save(Optional isNew As Boolean = True) Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).Save
        Try
            If Not isNew Then
                With objTemp_Edit
                    .CIF = CIF
                    .FK_REF_DETAIL_OF = FK_REF_DETAIL_OF
                    .URL = txt_url.Value
                End With

                With objTemp_goAML_Ref
                    .CIF = CIF
                    .FK_REF_DETAIL_OF = FK_REF_DETAIL_OF
                    .URL = txt_url.Value
                End With

                objTemp_Edit = Nothing
                objTemp_goAML_Ref = Nothing
            Else
                Dim objNewVWgoAML_Url As New goAML_Ref_Customer_URL
                Dim objNewgoAML_Url As New goAML_Ref_Customer_URL
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_URL_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_Url
                    .PK_goAML_Ref_Customer_URL_ID = intpk
                    .FK_REF_DETAIL_OF = FK_REF_DETAIL_OF
                    .CIF = CIF
                    .URL = txt_url.Value
                End With

                With objNewgoAML_Url
                    .PK_goAML_Ref_Customer_URL_ID = intpk
                    .FK_REF_DETAIL_OF = FK_REF_DETAIL_OF
                    .CIF = CIF
                    .URL = txt_url.Value
                End With

                objListgoAML_vw.Add(objNewVWgoAML_Url)
                objListgoAML_Ref = objListgoAML_vw
            End If

            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()
            ' 2023-10-04, Nael: Menambahkan isDataChange untuk cek apakah url berubah
            IsDataChange = True
            WindowDetail.Hidden = True
            'ClearinputCustomerPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Function IsData_Valid() As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).IsData_Valid
        'If CIF Is Nothing Then
        '    Throw New Exception("CIF is required")
        'End If

        If txt_url.Text.Trim = "" Then
            Throw New Exception("Url is required")
        End If

        Return True
    End Function

    Public Sub LoadData(data As List(Of goAML_Ref_Customer_URL)) Implements IGoAML_Grid(Of goAML_Ref_Customer_URL).LoadData
        objListgoAML_Ref = data.Select(Function(x) x).ToList()
        objListgoAML_vw = data.Select(Function(x) x).ToList()

        If objListgoAML_vw IsNot Nothing Then
            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()
        Else
            Store.DataBind()
        End If
    End Sub
End Class
