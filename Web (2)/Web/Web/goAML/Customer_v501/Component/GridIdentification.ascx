﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GridIdentification.ascx.vb" Inherits="goAML_Customer_v501_Component_GridIdentification" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<%--Director Identificiation--%>
<ext:GridPanel ID="GridPanel" runat="server" Title="Identification" AutoScroll="true">
    <TopBar>
        <ext:Toolbar ID="toolbar12" runat="server">
            <Items>
                <ext:Button ID="btnAdd" runat="server" Text="Add Identification" Icon="Add">
                    <DirectEvents>
                        <Click OnEvent="btnAdd_DirectClick">
                            <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Items>
        </ext:Toolbar>
    </TopBar>
    <Store>
        <ext:Store ID="Store" runat="server" IsPagingStore="true" PageSize="8">
            <Model>
                <ext:Model ID="Model29" runat="server">
                    <%--to do--%>
                    <Fields>
                        <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Type" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Issued_By" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Issue_Date" Type="Date"></ext:ModelField>
                        <ext:ModelField Name="Expiry_Date" Type="Date"></ext:ModelField>
                        <ext:ModelField Name="Issued_Country" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Identification_Comment" Type="String"></ext:ModelField>
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
            <ext:CommandColumn ID="CommandColumn13" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                    <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                    <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:Column ID="Column75" runat="server" DataIndex="Number" Text="Identification Number" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column77" runat="server" DataIndex="Type" Text="Identification Type" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column78" runat="server" DataIndex="Issued_By" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
            <ext:DateColumn ID="Column79" runat="server" DataIndex="Issue_Date" Text="Issued Date" MinWidth="130" Flex="1"></ext:DateColumn>
            <ext:DateColumn ID="Column80" runat="server" DataIndex="Expiry_Date" Text="Expired Date" MinWidth="130" Flex="1"></ext:DateColumn>
            <%-- 2023-09-27, Nael, mengubah DataIndex yg asalnya 'Country' jadi 'Issued_Country' nyamain dengan field yg ada di objek goAML_Person_Identification --%>
            <ext:Column ID="Column81" runat="server" DataIndex="Issued_Country" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column82" runat="server" DataIndex="Identification_Comment" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
            
        </Columns>
    </ColumnModel>
    <BottomBar>
        <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
    </BottomBar>
</ext:GridPanel>
<%--end of Director Identification--%>

<%--Pop Up--%>
<ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
    <Items>
        <ext:FieldSet ID="FieldSet2" runat="server" Title="Identification" Collapsible="true" Layout="FitLayout">
            <Items>
            </Items>
            <Content>
                <ext:FormPanel ID="FormPanel" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                    <Items>
                        <ext:TextField ID="txt_number_identification" AllowBlank="False" runat="server" FieldLabel="Identification Number" AnchorHorizontal="90%"></ext:TextField>
                        <ext:Panel ID="Pnl_cmb_Jenis_Dokumen" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:NDSDropDownField ID="ascx_cmb_Jenis_Dokumen" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringFilter="Active = 1" StringTable="goaml_ref_jenis_dokumen_identitas" EmptyText="Select One" Label="Identification Type" AnchorHorizontal="70%" AllowBlank="false" />
                            </Content>
                        </ext:Panel>
                        <ext:Panel ID="Pnl_cmb_issued_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:NDSDropDownField ID="ascx_cmb_issued_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Issued Country" AnchorHorizontal="70%" AllowBlank="false" />
                            </Content>
                        </ext:Panel>
                        <ext:TextField ID="txt_issued_by" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%"></ext:TextField>
                        <ext:DateField Editable="false" ID="txt_issued_date" Format="dd-MMM-yyyy" runat="server" FieldLabel="Issued Date" AnchorHorizontal="90%"></ext:DateField>
                        <ext:DateField Editable="false" ID="txt_issued_expired_date" Format="dd-MMM-yyyy" runat="server" FieldLabel="Issued Expired Date" AnchorHorizontal="90%"></ext:DateField>
                        <ext:TextField ID="txt_identification_comment" runat="server" FieldLabel="Comment" AnchorHorizontal="90%"></ext:TextField>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnSave" runat="server" Text="Save" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnSave_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button ID="btnBack" runat="server" Text="Back" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnBack_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Content>
        </ext:FieldSet>
    </Items>
</ext:Window>
<%--Pop Up--%>
