﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GridAddress.ascx.vb" Inherits="goAML_Customer_v501_Component_GridAddress" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<%--Indv Address--%>
<ext:GridPanel ID="GridPanel" runat="server" Title="Address" AutoScroll="true" OnDataBinding="GridPanel_DataBinding">
    <TopBar>
        <ext:Toolbar ID="toolbar1" runat="server">
            <Items>
                <ext:Button ID="btnAdd" runat="server" Text="Add Addresses" Icon="Add">
                    <DirectEvents>
                        <Click OnEvent="btnAdd_DirectClick">
                            <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Items>
        </ext:Toolbar>
    </TopBar>
    <Store>
        <ext:Store ID="Store" runat="server" IsPagingStore="true" PageSize="8">
            <Model>
                <ext:Model ID="Model_INDV_Addresses" runat="server">
                    <%--to do--%>
                    <Fields>
                        <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Address_Type" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                        <ext:ModelField Name="City" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                        <ext:ModelField Name="State" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
            <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                    <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                    <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:Column ID="Column1" runat="server" DataIndex="Address_Type" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column2" runat="server" DataIndex="Address" Text="Address" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column3" runat="server" DataIndex="Town" Text="Town" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column4" runat="server" DataIndex="City" Text="City" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column5" runat="server" DataIndex="Zip" Text="Zip Code" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column7" runat="server" DataIndex="Country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column8" runat="server" DataIndex="State" Text="Province" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column6" runat="server" DataIndex="Comments" Text="Notes" MinWidth="130" Flex="1"></ext:Column>
            
        </Columns>
    </ColumnModel>
    <BottomBar>
        <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
    </BottomBar>
</ext:GridPanel>
<%--end of INDV Address--%>

<%--Pop Up--%>
<ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Address Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
    <Items>
        <ext:FieldSet ID="FieldSet1" runat="server" Title="Address" Collapsible="true" Layout="FitLayout">
            <Items>
            </Items>
            <Content>
                <ext:FormPanel ID="FormPanel" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                    <Items>
                        <ext:Hidden runat="server" ID="Hidden1"></ext:Hidden>
                        <ext:Panel ID="Pnl_cmb_kategoriaddress" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:NDSDropDownField ID="ascx_cmb_kategoriaddress" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" EmptyText="Select One" StringFilter="Active = 1" StringTable="goAML_Ref_Kategori_Kontak" Label="Address Type" AnchorHorizontal="70%" AllowBlank="false" />
                            </Content>
                        </ext:Panel>
                        <ext:TextField ID="Address_INDV" runat="server" AllowBlank="False" FieldLabel="Address" AnchorHorizontal="90%"></ext:TextField>
                        <ext:TextField ID="Town_INDV" runat="server" FieldLabel="Town" AnchorHorizontal="90%"></ext:TextField>
                        <ext:TextField ID="City_INDV" runat="server" AllowBlank="False" FieldLabel="City" AnchorHorizontal="90%"></ext:TextField>
                        <ext:TextField ID="Zip_INDV" runat="server" FieldLabel="Zip COde" AnchorHorizontal="90%"></ext:TextField>
                        <ext:Panel ID="Pnl_cmb_kodenegara" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:NDSDropDownField ID="ascx_cmb_kodenegara" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" EmptyText="Select One" StringFilter="Active = 1" StringTable="goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="70%" AllowBlank="false" />
                            </Content>
                        </ext:Panel>
                        <ext:TextField ID="State_INDVD" runat="server" FieldLabel="Province" AnchorHorizontal="90%"></ext:TextField>
                        <ext:TextField ID="Comment_Address_INDVD" runat="server" FieldLabel="Notes" AnchorHorizontal="90%"></ext:TextField>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnSave" runat="server" Text="Save" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnSave_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button ID="btnBack" runat="server" Text="Back" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnBack_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>

                    </Buttons>
                </ext:FormPanel>
            </Content>
        </ext:FieldSet>
    </Items>
</ext:Window>
<%-- end Pop Up--%>
