﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GridRelatedEntity.ascx.vb" Inherits="goAML_Customer_v501_Component_GridRelatedEntity" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<%@ Register Src="~/goAML/Customer_v501/Component/GridEmail.ascx" TagPrefix="nds" TagName="GridEmail" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridSanction.ascx" TagPrefix="nds" TagName="GridSanction" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridEntityIdentification.ascx" TagPrefix="nds" TagName="GridEntityIdentification" %>
<%@ Register Src="~/goAML/Customer_v501/Component/GridURL.ascx" TagPrefix="nds" TagName="GridURL" %>

<%-- Related Entities --%>
<ext:GridPanel ID="GridPanel" runat="server" Title="Related Entity" AutoScroll="true">
    <TopBar>
        <ext:Toolbar ID="tbr_corp_related_entities" runat="server">
            <Items>
                <ext:Button ID="btnAdd" runat="server" Text="Add Related Entities" Icon="Add">
                    <DirectEvents>
                        <Click OnEvent="btnAdd_DirectClick">
                            <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Items>
        </ext:Toolbar>
    </TopBar>
    <Store>
        <ext:Store ID="Store" runat="server" IsPagingStore="true" PageSize="8">
            <Model>
                <ext:Model ID="mdl_related_entities" runat="server">
                    <%--to do--%>
                    <Fields>
                        <ext:ModelField Name="PK_goAML_Ref_Customer_Related_Entities_ID" Type="String"></ext:ModelField>
                        <ext:ModelField Name="ENTITY_ENTITY_RELATION" Type="String"></ext:ModelField>
                        <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                        <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                        <ext:ModelField Name="COMMERCIAL_NAME" Type="String"></ext:ModelField>
                        <ext:ModelField Name="INCORPORATION_LEGAL_FORM" Type="String"></ext:ModelField>
                        <ext:ModelField Name="INCORPORATION_NUMBER" Type="String"></ext:ModelField>
                        <ext:ModelField Name="BUSINESS" Type="String"></ext:ModelField>
                        <ext:ModelField Name="INCORPORATION_STATE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="INCORPORATION_COUNTRY_CODE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="INCORPORATION_DATE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="BUSINESS_CLOSED" Type="String"></ext:ModelField>
                        <ext:ModelField Name="DATE_BUSINESS_CLOSED" Type="String"></ext:ModelField>
                        <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                        <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:RowNumbererColumn ID="rnc_related_etities" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
            <ext:CommandColumn ID="CC_CORP_RelatedEntities" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                    <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                    <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Entities_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Entities_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:Column ID="clm_re_email_address" runat="server" DataIndex="ENTITY_ENTITY_RELATION" Text="Entity Entity Relation" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column89" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column90" runat="server" DataIndex="NAME" Text="Name" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column91" runat="server" DataIndex="COMMERCIAL_NAME" Text="Commercial Name" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column92" runat="server" DataIndex="INCORPORATION_LEGAL_FORM" Text="Incorporation Legal Form" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column93" runat="server" DataIndex="INCORPORATION_NUMBER" Text="Incorporation Number" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column94" runat="server" DataIndex="BUSINESS" Text="Business" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column97" runat="server" DataIndex="INCORPORATION_STATE" Text="Incorporation State" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column98" runat="server" DataIndex="INCORPORATION_COUNTRY_CODE" Text="Incorporation Country Code" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column99" runat="server" DataIndex="INCORPORATION_DATE" Text="Incorporation Date" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column100" runat="server" DataIndex="BUSINESS_CLOSED" Text="Businesss Closed" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column101" runat="server" DataIndex="DATE_BUSINESS_CLOSED" Text="Date Business Closed" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column102" runat="server" DataIndex="TAX_NUMBER" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column103" runat="server" DataIndex="TAX_REG_NUMBER" Text="Tax Reg Number" MinWidth="130" Flex="1"></ext:Column>
        </Columns>
    </ColumnModel>
    <BottomBar>
        <ext:PagingToolbar ID="pt_related_entities" runat="server" HideRefresh="True" />
    </BottomBar>
</ext:GridPanel>
<%-- End of Related Entities --%>

<ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet14" runat="server" Title="Entity Identification" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--5 --%>
                            <ext:Panel ID="Panel_cmb_re_entity_relation" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_re_entity_relation" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Person_Person_Relationship" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Entity Relation" AnchorHorizontal="70%" AllowBlank="false" />
	                            </Content>
                            </ext:Panel>
                            <%--11 --%>
                            <ext:TextField ID="txt_re_comments" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Relation Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                            <%--12 --%>
                            <ext:TextField ID="txt_re_name" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Corporate Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--13 --%>
                            <ext:TextField ID="txt_re_commercial_name" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Commercial Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--14 --%>
                            <ext:Panel ID="Panel_cmb_re_incorporation_legal_form" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_re_incorporation_legal_form" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Bentuk_Badan_Usaha" StringFilter="Active = 1" EmptyText="Select One" Label="Legal Form" AnchorHorizontal="70%" AllowBlank="false" />
	                            </Content>
                            </ext:Panel>
                            <%--15 --%>
                            <ext:TextField ID="txt_re_incorporation_number" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Corporate License Number" AnchorHorizontal="90%" MaxLength="50"></ext:TextField>
                            <%--16 --%>
                            <ext:TextField ID="txt_re_business" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Line of Business" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                        
                            <%--19 --%>
                            <ext:TextField ID="txt_re_incorporation_state" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Province" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--20 --%>
                            <ext:Panel ID="Panel_cmb_re_incorporation_country_code" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_re_incorporation_country_code" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%" AllowBlank="false" />
	                            </Content>
                            </ext:Panel>
                            <%--21 --%>
                            <ext:DateField Editable="false" LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Date of Established" ID="df_re_incorporation_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%--22 --%>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_re_business_closed" FieldLabel="Businesss Closed?"></ext:Checkbox>
                            <%--23 --%>
                            <ext:DateField Editable="false" LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Date Business Closed" ID="df_re_date_business_closed" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%--24 --%>
                            <ext:TextField ID="txt_re_tax_number" AllowBlank="False" Hidden="true" LabelWidth="250" runat="server" FieldLabel="Tax Number" AnchorHorizontal="90%" MaxLength="16"></ext:TextField>
                            <%--25 --%>
                            <ext:TextField ID="txt_re_tax_reg_number" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Tax Reg Number" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                            <ext:Panel ID="Panel_RelatedEntity_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEmail runat="server" ID="GridEmail_RelatedEntity"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridEntityIdentification" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEntityIdentification runat="server" ID="GridEntityIdentification_RelatedEntity"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridURL" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridURL runat="server" ID="GridURL_RelatedEntity"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridSanction runat="server" ID="GridSanction_RelatedEntity"/>
                                </Content>
                            </ext:Panel>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSave" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBack" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBack_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>