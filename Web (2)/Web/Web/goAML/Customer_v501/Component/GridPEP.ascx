﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GridPEP.ascx.vb" Inherits="goAML_Customer_v501_Component_GridPEP" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<%--Person PEP, Add by Septian--%>
<ext:GridPanel ID="GridPanel" runat="server" Title="Person PEP" AutoScroll="true">
    <TopBar>
        <ext:Toolbar ID="tbr_person_pep" runat="server">
            <Items>
                <ext:Button ID="btnAdd" runat="server" Text="Add Person PEP" Icon="Add">
                    <DirectEvents>
                        <Click OnEvent="btnAdd_DirectClick">
                            <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Items>
        </ext:Toolbar>
    </TopBar>
    <Store>
        <ext:Store ID="Store" runat="server" IsPagingStore="true" PageSize="8">
            <Model>
                <ext:Model ID="mdl_person_pep" runat="server">
                    <%--to do--%>
                    <Fields>
                        <ext:ModelField Name="PK_goAML_Ref_Customer_PEP_ID" Type="String"></ext:ModelField>
                        <ext:ModelField Name="PEP_COUNTRY" Type="String"></ext:ModelField>
                        <ext:ModelField Name="FUNCTION_NAME" Type="String"></ext:ModelField>
                        <ext:ModelField Name="FUNCTION_DESCRIPTION" Type="String"></ext:ModelField>
                        <ext:ModelField Name="PEP_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                        <ext:ModelField Name="PEP_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="PEP_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                        <ext:ModelField Name="PEP_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:RowNumbererColumn ID="rnc_person_pep" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
            <ext:CommandColumn ID="CC_INDV_PEP" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                    <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                    <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_PEP_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_PEP_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:Column ID="clm_person_pep_first_name" runat="server" DataIndex="PEP_COUNTRY" Text="Country" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_person_pep_function_name" runat="server" DataIndex="FUNCTION_NAME" Text="Function Name" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_person_pep_description" runat="server" DataIndex="FUNCTION_DESCRIPTION" Text="Description" MinWidth="130" Flex="1"></ext:Column>
            <%--<ext:Column ID="clm_person_pep_valid_from" runat="server" DataIndex="PEP_DATE_RANGE_VALID_FROM" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_person_is_approx_from_date" runat="server" DataIndex="PEP_DATE_RANGE_IS_APPROX_FROM_DATE" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_person_is_valid_to" runat="server" DataIndex="PEP_DATE_RANGE_VALID_TO" Text="Valid To" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_person_is_approx_to_date" runat="server" DataIndex="PEP_DATE_RANGE_IS_APPROX_TO_DATE" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
            <ext:Column ID="clm_person_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
            
        </Columns>
    </ColumnModel>
    <BottomBar>
        <ext:PagingToolbar ID="pt_add_info" runat="server" HideRefresh="True" />
    </BottomBar>
</ext:GridPanel>
<%--end of Person PEP--%>

<ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Person PEP Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
    <Items>
        <ext:FieldSet ID="FieldSet8" runat="server" Title="Person PEP" Collapsible="true" Layout="FitLayout">
            <Items>
            </Items>
            <Content>
                <ext:FormPanel ID="FormPanel" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                    <Items>
                        <ext:Panel ID="Panel_Person_PEP_Country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                        <Content>
		                        <nds:NDSDropDownField ID="ascx_cmb_PersonPEP_Country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%" AllowBlank="false" />
	                        </Content>
                        </ext:Panel>
                        <ext:TextField ID="txt_function_name" LabelWidth="250" runat="server" FieldLabel="Function Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                        <ext:TextField ID="txt_function_description" LabelWidth="250" runat="server" FieldLabel="Function Description" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                        <ext:DateField Hidden="true" ID="df_person_pep_valid_from" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Valid From" AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>
                        <ext:Checkbox Hidden="true" runat="server" LabelWidth="250" ID="cbx_pep_is_approx_from_date" FieldLabel="Is Approx From Date?"></ext:Checkbox>
                        <ext:DateField Hidden="true" ID="df_pep_valid_to" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Valid To" AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>
                        <ext:Checkbox Hidden="true" runat="server" LabelWidth="250" ID="cbx_pep_is_approx_to_date" FieldLabel="Is Approx To Date?"></ext:Checkbox>
                        <ext:TextField ID="txt_pep_comments" LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%"  MaxLength="8000"></ext:TextField>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnSave" runat="server" Text="Save" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnSave_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button ID="btnBack" runat="server" Text="Back" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnBack_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Content>
        </ext:FieldSet>
    </Items>
</ext:Window>