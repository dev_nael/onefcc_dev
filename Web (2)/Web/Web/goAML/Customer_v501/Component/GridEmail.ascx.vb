﻿Imports System.Reflection.Emit
Imports GoAMLBLL
Imports GoAMLBLL.goAML.DataModel
Imports GoAMLBLL.goAML_CustomerDataBLL

Partial Class goAML_Customer_v501_Component_GridEmail
    Inherits System.Web.UI.UserControl
    Implements IGoAML_Grid(Of goAML_Ref_Customer_Email)

    Private Const GRID_TITLE As String = "Email"
    Private Const SESSION_PREFIX As String = "CUSTOMER.GridEmail."

    Private _uniqueName As String
    Public Property UniqueName As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).UniqueName
        Get
            'Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME)
            Return _uniqueName
        End Get
        Set(value As String)
            'Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME) = value
            _uniqueName = value
        End Set
    End Property

    Private _fk_ref_detail_of As Integer
    Public Property FK_REF_DETAIL_OF As Integer Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).FK_REF_DETAIL_OF
        Get
            Return _fk_ref_detail_of
        End Get
        Set(value As Integer)
            _fk_ref_detail_of = value
        End Set
    End Property

    Public Property CIF As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).CIF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName)
        End Get
        Set(ByVal value As String)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName) = value
        End Set
    End Property
    Public Property objTemp_Edit() As goAML_Ref_Customer_Email Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).objTemp_Edit
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName)
        End Get
        Set(ByVal value As goAML_Ref_Customer_Email)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref() As goAML_Ref_Customer_Email Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).objTemp_goAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName)
        End Get
        Set(ByVal value As goAML_Ref_Customer_Email)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_Ref() As List(Of goAML_Ref_Customer_Email) Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).objListgoAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName)
        End Get
        Set(ByVal value As List(Of goAML_Ref_Customer_Email))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_vw() As List(Of goAML_Ref_Customer_Email) Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).objListgoAML_vw
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName)
        End Get
        Set(ByVal value As List(Of goAML_Ref_Customer_Email))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName) = value
        End Set
    End Property

    Private _title As String
    Public Property Title As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).Title
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Public ReadOnly Property SessionPrefix As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).SessionPrefix
        Get
            Return SESSION_PREFIX
        End Get
    End Property

    Private _isViewMode As Boolean
    Public Property IsViewMode As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).IsViewMode
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName)
        End Get
        Set(ByVal value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName) = value
        End Set
    End Property

    Public Property IsDataChange As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).IsDataChange
        Get
            If Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) Is Nothing Then
                Return False
            End If
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName)
        End Get
        Set(value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) = value
        End Set
    End Property

    Private Shadows Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'objListgoAML_Ref = New List(Of goAML_CustomerDataBLL.goAML_Ref_Customer_Email)
        'objListgoAML_vw = New List(Of goAML_CustomerDataBLL.goAML_Ref_Customer_Email)
        'Dim cmdColumn As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        'If cmdColumn IsNot Nothing Then
        '    Dim cmd1 As GridCommand = cmdColumn.Commands(1)
        '    Dim cmd2 As GridCommand = cmdColumn.Commands(2)
        '    cmd1.Hidden = IsViewMode
        '    cmd2.Hidden = IsViewMode
        'End If

        'btnAdd.Hidden = IsViewMode

    End Sub
    Private Shadows Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Dim cmdColumnEdit As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        Dim cmdColumnDetail As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(2), CommandColumn)
        cmdColumnEdit.Hidden = IsViewMode
        cmdColumnDetail.Hidden = Not IsViewMode

        'Dim store As Store = TryCast(GridPanel.Store(0), Store)
        'store.DataBind()

        btnAdd.Hidden = IsViewMode
    End Sub

    Protected Sub btnAdd_DirectClick(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).btnAdd_DirectClick
        Try
            'If CIF Is Nothing Then
            '    Throw New Exception("CIF is required")
            'End If

            WindowDetail.Hidden = False
            FormPanel.Hidden = False
            btn_Customer_Save_Email.Hidden = False

            txtCustomer_email_address.ReadOnly = False

            WindowDetail.Title = Title & " Add"

            objTemp_Edit = Nothing
            'FormPanel_Email.Reset(False)
            Clearinput()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSave_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).btnSave_DirectEvent
        Try
            If IsData_Valid() Then
                If objTemp_Edit Is Nothing Then
                    Save()
                Else
                    Save(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBack_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).btnBack_DirectEvent
        Try
            WindowDetail.Hidden = True
            FormPanel.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmd(sender As Object, e As Ext.Net.DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).GrdCmd
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadData(id As Long, isEdit As Boolean) Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).LoadData
        Try
            objTemp_goAML_Ref = objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id)
            objTemp_Edit = objListgoAML_vw.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id)

            If Not objTemp_Edit Is Nothing Then
                FormPanel.Hidden = False
                WindowDetail.Hidden = False
                btn_Customer_Save_Email.Hidden = Not isEdit

                txtCustomer_email_address.ReadOnly = Not isEdit

                With objTemp_Edit
                    txtCustomer_email_address.Value = .EMAIL_ADDRESS
                End With

                WindowDetail.Title = Title & " " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub DeleteRecord(id As Long) Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).DeleteRecord
        objListgoAML_vw.Remove(objListgoAML_vw.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id))
        objListgoAML_Ref.Remove(objListgoAML_Ref.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Email_ID = id))

        Store.DataSource = objListgoAML_vw.ToList()
        Store.DataBind()

        'Store_corp_email.DataSource = objListgoAML_vw_Email.ToList()
        'Store_corp_email.DataBind()

    End Sub

    Sub Clearinput() Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).Clearinput
        txtCustomer_email_address.Text = ""

    End Sub

    Function IsData_Valid() As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).IsData_Valid
        'If CIF.Trim = "" Then
        '    Throw New Exception("CIF is required")
        'End If

        If txtCustomer_email_address.Text.Trim = "" Then
            Throw New Exception("Email Address is required")
        End If

        If txtCustomer_email_address.Text.Trim = "" Then
            Throw New Exception("Email Address is required")
        End If

        If Not IsFieldValid(txtCustomer_email_address.Text.Trim, "email_address") Then
            Throw New Exception("Email Address format is not valid")
        End If

        Return True
    End Function

    Sub Save(Optional isNew As Boolean = True) Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).Save
        Try
            If Not isNew Then
                With objTemp_Edit
                    .CIF = CIF
                    .FK_REF_DETAIL_OF = FK_REF_DETAIL_OF
                    .EMAIL_ADDRESS = txtCustomer_email_address.Value
                End With

                With objTemp_goAML_Ref
                    .CIF = CIF
                    .FK_REF_DETAIL_OF = FK_REF_DETAIL_OF
                    .EMAIL_ADDRESS = txtCustomer_email_address.Value
                End With

                objTemp_Edit = Nothing
                objTemp_goAML_Ref = Nothing
            Else
                Dim objNewVWgoAML_Email As New goAML_Ref_Customer_Email
                Dim objNewgoAML_Email As New goAML_Ref_Customer_Email
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_Email_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_Email
                    .PK_goAML_Ref_Customer_Email_ID = intpk
                    .FK_REF_DETAIL_OF = FK_REF_DETAIL_OF
                    .CIF = CIF
                    .EMAIL_ADDRESS = txtCustomer_email_address.Value
                End With

                With objNewgoAML_Email
                    .PK_goAML_Ref_Customer_Email_ID = intpk
                    .FK_REF_DETAIL_OF = FK_REF_DETAIL_OF
                    .CIF = CIF
                    .EMAIL_ADDRESS = txtCustomer_email_address.Value
                End With

                objListgoAML_vw.Add(objNewVWgoAML_Email)
                objListgoAML_Ref = objListgoAML_vw
            End If

            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()

            WindowDetail.Hidden = True
            'ClearinputCustomerPreviousName()
            IsDataChange = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

#Region "Other"
    Private Function IsFieldValid(text As String, fieldName As String) As Boolean
        Try
            Dim regex As New Regex("^[0-9 ]+$")

            Select Case fieldName
                Case "incorporation_state"
                    regex = New Regex("[ a-zA-Z]*")
                Case "tax_number"
                    regex = New Regex("[0-9]{15}")
                Case "last_name"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "birth_place"
                    regex = New Regex("[ a-zA-Z]*")
                Case "mothers_name"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "alias"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "ssn"
                    regex = New Regex("[0-9]{22}")
                Case "city"
                    regex = New Regex("[ a-zA-Z]*")
                Case "state"
                    regex = New Regex("[ a-zA-Z]*")
                Case "email_address"
                    regex = New Regex("[_a-zA-Z0-9-+]+(\.[_a-zA-Z0-9-\+]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})")
                Case "ipv4_address_type"
                    regex = New Regex("((1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])")
                Case "ipv6_address_type"
                    regex = New Regex("([A-Fa-f0-9]{1,4}:){7}[A-Fa-f0-9]{1,4}")
                Case Else
                    Return True
            End Select

            Return regex.IsMatch(text)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Sub LoadData(data As List(Of goAML_Ref_Customer_Email)) Implements IGoAML_Grid(Of goAML_Ref_Customer_Email).LoadData
        objListgoAML_Ref = data.Select(Function(x) x).ToList()
        objListgoAML_vw = data.Select(Function(x) x).ToList()

        If objListgoAML_vw IsNot Nothing Then
            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()
        Else
            Store.DataBind()
        End If


    End Sub

#End Region

End Class
