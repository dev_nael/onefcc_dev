﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GridSanction.ascx.vb" Inherits="goAML_Customer_v501_Component_GridSanction" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<%-- INDV Sanction, Add by Septian--%>
<ext:GridPanel ID="GridPanel" runat="server" Title="Sanction" AutoScroll="true">
    <TopBar>
        <ext:Toolbar ID="tbr_sanction" runat="server">
            <Items>
                <ext:Button ID="btnAdd" runat="server" Text="Add Sanction" Icon="Add">
                    <DirectEvents>
                        <Click OnEvent="btnAdd_DirectClick">
                            <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Items>
        </ext:Toolbar>
    </TopBar>
    <Store>
        <ext:Store ID="Store" runat="server" IsPagingStore="true" PageSize="8">
            <Model>
                <ext:Model ID="mdl_sanction" runat="server">
                    <%--to do--%>
                    <Fields>
                        <ext:ModelField Name="PK_goAML_Ref_Customer_Sanction_ID" Type="String"></ext:ModelField>
                        <ext:ModelField Name="PROVIDER" Type="String"></ext:ModelField>
                        <ext:ModelField Name="SANCTION_LIST_NAME" Type="String"></ext:ModelField>
                        <ext:ModelField Name="MATCH_CRITERIA" Type="String"></ext:ModelField>
                        <ext:ModelField Name="LINK_TO_SOURCE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="SANCTION_LIST_ATTRIBUTES" Type="String"></ext:ModelField>
                        <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                        <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                        <ext:ModelField Name="SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:RowNumbererColumn ID="rnc_sanction" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
            <ext:CommandColumn ID="CC_INDV_Sanction" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                    <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                    <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Sanction_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Sanction_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:Column ID="clm_provider" runat="server" DataIndex="PROVIDER" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_sanction_list_name" runat="server" DataIndex="SANCTION_LIST_NAME" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_match_criteria" runat="server" DataIndex="MATCH_CRITERIA" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_link_to_source" runat="server" DataIndex="LINK_TO_SOURCE" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_sanction_attributes" runat="server" DataIndex="SANCTION_LIST_ATTRIBUTES" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
            <%--<ext:Column ID="clm_saction_valid_from" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_VALID_FROM" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_sanction_is_approx_from_date" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_sanction_valid_to" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_VALID_TO" Text="Valid To" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_sanction_is_approx_to_date" runat="server" DataIndex="SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
            <ext:Column ID="clm_sanction_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
            
        </Columns>
    </ColumnModel>
    <BottomBar>
        <ext:PagingToolbar ID="pt_sanction" runat="server" HideRefresh="True" />
    </BottomBar>
</ext:GridPanel>
<%-- end of INDV Sanction --%>

<ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Sanction Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
    <Items>
        <ext:FieldSet ID="FieldSet15" runat="server" Title="Sanction" Collapsible="true" Layout="FitLayout">
            <Items>
            </Items>
            <Content>
                <ext:FormPanel ID="FormPanel" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                    <Items>
                        <ext:TextField ID="txt_sanction_provider" runat="server" FieldLabel="Provider" AnchorHorizontal="90%" AllowBlank="False" MaxLength="255"></ext:TextField>
                        <ext:TextField ID="txt_sanction_sanction_list_name" runat="server" FieldLabel="Sanction List Name" AllowBlank="False" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                        <ext:TextField ID="txt_sanction_match_criteria" runat="server" FieldLabel="Match Criteria" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                        <ext:TextField ID="txt_sanction_link_to_source" runat="server" FieldLabel="Link to Source" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                        <ext:TextField ID="txt_sanction_sanction_list_attributes" runat="server" FieldLabel="Sanction List Attributes" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                        <ext:DateField Hidden="true" Editable="false" AllowBlank="False" runat="server" FieldLabel="Valid From" ID="df_sanction_valid_from" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                        <ext:Checkbox Hidden="true" runat="server" ID="cbx_sanction_is_approx_from_date" FieldLabel="Is Approx From Date?"></ext:Checkbox>
                        <ext:DateField Hidden="true" Editable="false" AllowBlank="False" runat="server" FieldLabel="Valid To" ID="df_sanction_valid_to" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                        <ext:Checkbox Hidden="true" runat="server" ID="cbx_sanction_is_approx_to_date" FieldLabel="Is Approx To Date?"></ext:Checkbox>
                        <ext:TextField ID="txt_sanction_comments" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnSave" runat="server" Text="Save" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnSave_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button ID="Button17" runat="server" Text="Back" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnBack_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>

                    </Buttons>
                </ext:FormPanel>
            </Content>
        </ext:FieldSet>
    </Items>
</ext:Window>
