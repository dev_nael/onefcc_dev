﻿Imports NawaDevBLL.GlobalReportFunctionBLL
Imports NawaDevDAL
Imports NawaBLL
Imports System.Data.SqlClient
Imports System.Data
Imports System.Globalization
Imports NawaDAL
Imports Elmah
Imports Ext

Partial Class goAML_PersonEdit
    Inherits ParentPage
    Public objFormModuleedit As NawaBLL.FormModuleEdit
    Public goAML_PersonBLL As New NawaDevBLL.ReportingPerson()
#Region "Session"
    Public Property ReportingPersonClass As NawaDevBLL.ReportingPersonData
        Get
            Return Session("goAML_PersonAdd.ReportingPersonClass")
        End Get
        Set(ByVal value As NawaDevBLL.ReportingPersonData)
            Session("goAML_PersonAdd.ReportingPersonClass") = value
        End Set
    End Property
    Public Property ObjIdentification As NawaDevDAL.goAML_Person_Identification
        Get
            Return Session("goAML_PersonEdit.ObjIdentification")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Person_Identification)
            Session("goAML_PersonEdit.ObjIdentification") = value
        End Set
    End Property

    Public Property ObjPhone As NawaDevDAL.goAML_Ref_Phone
        Get
            Return Session("goAML_PersonEdit.ObjPhone")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Phone)
            Session("goAML_PersonEdit.ObjPhone") = value
        End Set
    End Property
    Public Property ObjEmployerPhone As NawaDevDAL.goAML_Ref_Phone
        Get
            Return Session("goAML_PersonEdit.ObjEmployerPhone")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Phone)
            Session("goAML_PersonEdit.ObjEmployerPhone") = value
        End Set
    End Property
    Public Property ObjAddress As NawaDevDAL.goAML_Ref_Address
        Get
            Return Session("goAML_PersonEdit.ObjAddress")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Address)
            Session("goAML_PersonEdit.ObjAddress") = value
        End Set
    End Property
    Public Property ObjEmployerAddress As NawaDevDAL.goAML_Ref_Address
        Get
            Return Session("goAML_PersonEdit.ObjEmployerAddress")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Address)
            Session("goAML_PersonEdit.ObjEmployerAddress") = value
        End Set
    End Property

    Public Property ObjVwIdentification As NawaDevDAL.Vw_Temp_Identification
        Get
            Return Session("goAML_PersonEdit.ObjVwIdentification")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Temp_Identification)
            Session("goAML_PersonEdit.ObjVwIdentification") = value
        End Set
    End Property

    Public Property ObjVwPhone As NawaDevDAL.Vw_Temp_Phone
        Get
            Return Session("goAML_PersonEdit.ObjVwPhone")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Temp_Phone)
            Session("goAML_PersonEdit.ObjVwPhone") = value
        End Set
    End Property

    Public Property ObjVwAddress As NawaDevDAL.Vw_Temp_Address
        Get
            Return Session("goAML_PersonEdit.ObjVwAddress")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Temp_Address)
            Session("goAML_PersonEdit.ObjVwAddress") = value
        End Set
    End Property

    Public Property ListIdentification As List(Of goAML_Person_Identification)
        Get
            Return Session("goAML_PersonEdit.ListIdentification")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_PersonEdit.ListIdentification") = value
        End Set
    End Property

    Public Property ListPhone As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_PersonEdit.ListPhone")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_PersonEdit.ListPhone") = value
        End Set
    End Property
    Public Property ListPhoneEmployer As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_PersonEdit.ListPhoneEmployer")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_PersonEdit.ListPhoneEmployer") = value
        End Set
    End Property

    Public Property ListAddress As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_PersonEdit.ListAddress")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_PersonEdit.ListAddress") = value
        End Set
    End Property
    Public Property ListAddressEmployer As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_PersonEdit.ListAddressEmployer")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_PersonEdit.ListAddressEmployer") = value
        End Set
    End Property
    Public Property VwListIdentification As List(Of Vw_Temp_Identification)
        Get
            Return Session("goAML_PersonEdit.VwListIdentification")
        End Get
        Set(ByVal value As List(Of Vw_Temp_Identification))
            Session("goAML_PersonEdit.VwListIdentification") = value
        End Set
    End Property

    Public Property VwListPhone As List(Of Vw_Temp_Phone)
        Get
            Return Session("goAML_PersonEdit.VwListPhone")
        End Get
        Set(ByVal value As List(Of Vw_Temp_Phone))
            Session("goAML_PersonEdit.VwListPhone") = value
        End Set
    End Property

    Public Property VwListAddress As List(Of Vw_Temp_Address)
        Get
            Return Session("goAML_PersonEdit.VwListAddress")
        End Get
        Set(ByVal value As List(Of Vw_Temp_Address))
            Session("goAML_PersonEdit.VwListAddress") = value
        End Set
    End Property

    Public Property ObjPerson As NawaDevDAL.goAML_ODM_Reporting_Person
        Get
            Return Session("goAML_PersonEdit.ObjPerson")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_ODM_Reporting_Person)
            Session("goAML_PersonEdit.ObjPerson") = value
        End Set
    End Property

#End Region

#Region "Page Load"
    'Public goAML_PersonBLL As New NawaDevBLL.ReportingPerson()
    Public ObjReportingPerson As NawaDevBLL.ReportingPerson

    Protected Sub goAML_PersonEdit_PreLoad() Handles Me.PreLoad
        Me.ActionType = NawaBLL.Common.ModuleActionEnum.Update
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()

                Dim dataStr As String = Request.Params("ID")
                Dim dataID As String = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                ObjPerson = NawaDevBLL.ReportingPerson.GetReportingPersonByID(CInt(dataID))
                ListPhone = NawaDevBLL.ReportingPerson.GetListPhoneByID(CInt(dataID), 2)
                ListPhoneEmployer = NawaDevBLL.ReportingPerson.GetListPhoneByID(CInt(dataID), 11)
                ListAddress = NawaDevBLL.ReportingPerson.GetListAddressByID(CInt(dataID), 2)
                ListAddressEmployer = NawaDevBLL.ReportingPerson.GetListAddressByID(CInt(dataID), 11)
                ListIdentification = NawaDevBLL.ReportingPerson.GetListIdentificationByID(CInt(dataID))
                VwListPhone = NawaDevBLL.ReportingPerson.GetListVwPhoneByID(CInt(dataID), 2)
                VwListAddress = NawaDevBLL.ReportingPerson.GetListVwAddressByID(CInt(dataID), 2)
                VwListIdentification = NawaDevBLL.ReportingPerson.GetListVwIdentificationByID(CInt(dataID))

                Dim moduleStr As String = Request.Params("ModuleID")
                Dim moduleID As Integer = NawaBLL.Common.DecryptQueryString(moduleStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(moduleID)
                LoadDataComboBox()
                LoadData()


                ColumnActionLocation(PhoneField_Grid, CommandColumnPhone)
                ColumnActionLocation(AddressField_Grid, CommandColumnAddress)
                ColumnActionLocation(GridPanel_employerAddress, CommandColumnAddressEmp)
                ColumnActionLocation(GridPanel_EmployerPhone, CommandColumnPhoneEmp)
                ColumnActionLocation(IDField_Grid, CommandColumnIdent)

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If


        If bsettingRight = 1 Then
            'GridPanelReportIndicator.ColumnModel.Columns.Add(CommandColumn87)
        ElseIf bsettingRight = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub
    Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleedit = New NawaBLL.FormModuleEdit(RP_EntireForm, Panelconfirmation, LblConfirmation)
        'ObjReportingPerson = New NawaDevBLL.ReportingPerson(RP_EntireForm)
    End Sub
#End Region


#Region "Method"
    Sub ClearSession()
        Me.ActionType = Nothing
        Me.ObjModule = Nothing
        ListPhone = New List(Of goAML_Ref_Phone)
        VwListPhone = New List(Of Vw_Temp_Phone)
        ListAddress = New List(Of goAML_Ref_Address)
        VwListAddress = New List(Of Vw_Temp_Address)
        ListIdentification = New List(Of goAML_Person_Identification)
        VwListIdentification = New List(Of Vw_Temp_Identification)
        ListPhoneEmployer = New List(Of goAML_Ref_Phone)
        ListAddressEmployer = New List(Of goAML_Ref_Address)
        ReportingPersonClass = New NawaDevBLL.ReportingPersonData
        ObjIdentification = Nothing
        ObjPhone = Nothing
        ObjAddress = Nothing
        ObjVwIdentification = Nothing
        ObjVwPhone = Nothing
        ObjVwAddress = Nothing
        ObjEmployerAddress = Nothing
        ObjEmployerPhone = Nothing
    End Sub

    Protected Sub UserID_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("VwUserID", "kode, keterangan", strfilter, "keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub National_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Nama_Negara", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub JenisIdentitas_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Dokumen_Identitas", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Communication_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Alat_Komunikasi", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Contact_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Kategori_Kontak", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub Gender_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Kelamin", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub JenisLaporan_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub LoadDataComboBox()
        'Negara
        RP_CountryPassport.PageSize = SystemParameterBLL.GetPageSize
        StoreCountryPassport.Reload()
        RP_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignNational1.Reload()
        RP_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignNational2.Reload()
        RP_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignNational3.Reload()
        RP_Residence.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignResisdance.Reload()
        EmployerAddress_Negara.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignCountry_Code.Reload()
        AddrModal_Country.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignCountry_CodeAddress.Reload()
        IDModal_Country.PageSize = SystemParameterBLL.GetPageSize
        StoreIDModal_Country.Reload()

        'Gender
        RP_Gender.PageSize = SystemParameterBLL.GetPageSize
        StoreGender.Reload()

        'User ID
        cmb_UserID.PageSize = SystemParameterBLL.GetPageSize
        StoreUserID.Reload()

        'Jenis Laporan
        RP_KodeLaporan.PageSize = SystemParameterBLL.GetPageSize
        StoreKodeLaporan.Reload()

        'Kategori
        PhoneModal_Type.PageSize = SystemParameterBLL.GetPageSize
        StorePhoneModal_Type.Reload()
        EmployerPhone_Kategori.PageSize = SystemParameterBLL.GetPageSize
        StoreEmployerPhone_Kategori.Reload()
        AddrModal_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreAddrModal_Type.Reload()
        EmployerAddress_Tipe.PageSize = SystemParameterBLL.GetPageSize
        StoreEmployerAddress_Tipe.Reload()

        'Alat Komunikasi
        PhoneModal_Tool.PageSize = SystemParameterBLL.GetPageSize
        StorePhoneModal_Tool.Reload()
        EmployerPhone_Jenis.PageSize = SystemParameterBLL.GetPageSize
        StoreEmployerPhone_Jenis.Reload()

        'Jenis Identitas
        IDModal_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreIDModal_Type.Reload()
    End Sub

    Private Sub BindDetail()
        PhoneField_Store.DataSource = VwListPhone.OrderBy(Function(x) x.PK_goAML_Ref_Phone).ToList
        PhoneField_Store.DataBind()

        AddressField_Store.DataSource = VwListAddress.OrderBy(Function(x) x.PK_Customer_Address_ID).ToList
        AddressField_Store.DataBind()

        IDField_Store.DataSource = VwListIdentification.OrderBy(Function(x) x.PK_Person_Identification_ID).ToList
        IDField_Store.DataBind()

        bindPhone(StorePhoneEmployer, ListPhoneEmployer)

        bindAddress(StoreAddressEmployer, ListAddressEmployer)
    End Sub

    Sub bindPhone(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Ref_Phone))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("PK_goAML_ref_P", GetType(Integer)))
        objtable.Columns.Add(New DataColumn("TipeKontak", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_ref_P") = item("PK_goAML_Ref_Phone")
                item("TipeKontak") = getTypeKontakAlamatbyKode(item("Tph_Contact_Type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub bindAddress(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Ref_Address))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("PK_goAML_Ref_A", GetType(Integer)))
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_Ref_A") = item("PK_Customer_Address_ID")
                item("TypeAlamat") = getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                item("Negara") = getCountryByCode(item("Country_Code"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    'Add new identification
    Sub SaveAddIdentification()
        Dim objNewIdentification As New goAML_Person_Identification
        With objNewIdentification
            .PK_Person_Identification_ID = ListIdentification.Select(Function(x) x.PK_Person_Identification_ID).DefaultIfEmpty(0).Max() + 1
            .FK_Person_ID = PkRP.Text
            .isReportingPerson = True
            .Type = IDModal_Type.Value
            .Number = IDModal_Number.Text
            .Issue_Date = IDModal_IssueDate.Value
            .Expiry_Date = IDModal_ExpiryDate.Value
            .Issued_By = IDModal_IssuedBy.Text
            .Issued_Country = IDModal_Country.Value
            .Identification_Comment = IDModal_Comments.Text
        End With

        If IDModal_IssueDate.RawValue Is Nothing Then
            objNewIdentification.Issue_Date = IDModal_IssueDate.RawValue
        End If

        If IDModal_ExpiryDate.RawValue Is Nothing Then
            objNewIdentification.Expiry_Date = IDModal_ExpiryDate.RawValue
        End If
        ListIdentification.Add(objNewIdentification)
    End Sub
    Sub SaveVwListIdenfitication()
        Dim objVwListIdenfitication As New Vw_Temp_Identification
        With objVwListIdenfitication
            .PK_Person_Identification_ID = VwListIdentification.Select(Function(x) x.PK_Person_Identification_ID).DefaultIfEmpty(0).Max() + 1
            .type = IDModal_Type.RawText
            .Number = IDModal_Number.Text
            .Issue_Date = IDModal_IssueDate.Value
            .Expiry_Date = IDModal_ExpiryDate.Value
            .Issued_By = IDModal_IssuedBy.Text
            .negara = IDModal_Country.RawText
            .Identification_Comment = IDModal_Comments.Text
        End With

        VwListIdentification.Add(objVwListIdenfitication)

        BindDetail()
        IDModal.Hidden = True
    End Sub

    'Add New Phone
    Sub SaveAddPhone()
        Dim objNewPhone As New goAML_Ref_Phone

        With objNewPhone
            .PK_goAML_Ref_Phone = ListPhone.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
            .FK_for_Table_ID = PkRP.Text
            .Tph_Contact_Type = PhoneModal_Type.SelectedItem.Value
            .Tph_Communication_Type = PhoneModal_Tool.SelectedItem.Value
            .FK_Ref_Detail_Of = 2
            .tph_country_prefix = PhoneModal_RegionCode.Text
            .tph_number = PhoneModal_Number.Text
            .tph_extension = PhoneModal_NumberEx.Text
            .comments = PhoneModal_Comment.Text
        End With
        ListPhone.Add(objNewPhone)
    End Sub
    Sub SaveVwListPhone()
        Dim objVwListPhone As New Vw_Temp_Phone

        With objVwListPhone
            .PK_goAML_Ref_Phone = VwListPhone.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
            .kategori_kontak = PhoneModal_Type.RawText
            .ja_komunikasi = PhoneModal_Tool.RawText
            .tph_country_prefix = PhoneModal_RegionCode.Text
            .tph_number = PhoneModal_Number.Text
            .tph_extension = PhoneModal_NumberEx.Text
            .comments = PhoneModal_Comment.Text
        End With

        PhoneModal_Comment.Text = ""

        VwListPhone.Add(objVwListPhone)
        BindDetail()

        PhoneModal.Hidden = True
    End Sub
    Sub SaveAddEmployerPhone()
        Dim objNewEmpPhone As New goAML_Ref_Phone

        With objNewEmpPhone
            .PK_goAML_Ref_Phone = ListPhoneEmployer.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
            .Tph_Contact_Type = EmployerPhone_Kategori.SelectedItem.Value
            .Tph_Communication_Type = EmployerPhone_Jenis.SelectedItem.Value
            .FK_Ref_Detail_Of = 11
            .tph_country_prefix = EmployerPhone_Kode.Text
            .tph_number = EmployerPhone_Nomor.Text
            .tph_extension = EmployerPhone_Extensi.Text
            .comments = EmployerPhone_Note.Text
        End With
        ListPhoneEmployer.Add(objNewEmpPhone)
        BindDetail()

        WindowEmployerPhone.Hide()
    End Sub

    'Add New Address
    Sub SaveAddAddress()
        Dim objNewAddress As New goAML_Ref_Address
        With objNewAddress
            .PK_Customer_Address_ID = ListAddress.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
            .FK_Ref_Detail_Of = 2
            .FK_To_Table_ID = PkRP.Text
            .Address_Type = AddrModal_Type.Value
            .Address = AddrModal_Address.Text
            .Town = AddrModal_Town.Text
            .City = AddrModal_City.Text
            .Zip = AddrModal_PostalCode.Text
            .Country_Code = AddrModal_Country.Value
            .State = AddrModal_Province.Text
            .Comments = AddrModal_Comment.Text
        End With
        ListAddress.Add(objNewAddress)
    End Sub
    Sub SaveVwListAddress()
        Dim objVwListAddress As New Vw_Temp_Address

        With objVwListAddress
            .PK_Customer_Address_ID = VwListAddress.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
            .kontak = AddrModal_Type.RawText
            .address = AddrModal_Address.Text
            .town = AddrModal_Town.Text
            .city = AddrModal_City.Text
            .zip = AddrModal_PostalCode.Text
            .negara = AddrModal_Country.RawText
            .state = AddrModal_Province.Text
            .comments = AddrModal_Comment.Text
        End With
        VwListAddress.Add(objVwListAddress)
        BindDetail()
        AddrModal.Hidden = True
    End Sub
    Sub SaveAddAddressEmployer()

        Dim objNewAddressEmployer As New goAML_Ref_Address
        With objNewAddressEmployer
            .PK_Customer_Address_ID = ListAddressEmployer.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
            .Address_Type = EmployerAddress_Tipe.Value
            .FK_Ref_Detail_Of = 11
            .FK_To_Table_ID = PkRP.Text
            .Address = EmployerAddress_Alamat.Text
            .Town = EmployerAddress_Kec.Text
            .City = EmployerAddress_Kab.Text
            .Zip = EmployerAddress_Pos.Text
            .Country_Code = EmployerAddress_Negara.Value
            .State = EmployerAddress_Prov.Text
            .Comments = EmployerAddress_Note.Text
        End With

        ListAddressEmployer.Add(objNewAddressEmployer)
        BindDetail()
        WindowEmployerAddress.Hide()

    End Sub

    'Load Data
    Private Sub LoadDataEditIdentification(id As Long)
        ObjIdentification = ListIdentification.Find(Function(x) x.PK_Person_Identification_ID = id)
        ObjVwIdentification = VwListIdentification.Find(Function(x) x.PK_Person_Identification_ID = id)
        If Not ObjIdentification Is Nothing Then
            IDModal.Hidden = False
            'ClearInput()
            With ObjIdentification
                IDModal_Type.SetValueAndFireSelect(.Type)
                IDModal_Number.Text = .Number.Trim
                If .Issue_Date IsNot Nothing Then
                    IDModal_IssueDate.Value = .Issue_Date
                End If
                If .Expiry_Date IsNot Nothing Then
                    IDModal_ExpiryDate.Value = .Expiry_Date
                End If
                IDModal_IssuedBy.Text = .Issued_By
                Dim IssuedCountry As String = getNamaNegara(.Issued_Country)
                IDModal_Country.DoQuery(IssuedCountry)
                IDModal_Country.SetValue(ObjIdentification.Issued_Country)
                IDModal_Comments.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub LoadDataEditPhone(id As Long)
        ObjPhone = ListPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        ObjVwPhone = VwListPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjPhone Is Nothing Then
            PhoneModal.Hidden = False
            'ClearInput()
            With ObjPhone
                PhoneModal_Type.SetValueAndFireSelect(.Tph_Contact_Type)
                PhoneModal_Tool.SetValueAndFireSelect(.Tph_Communication_Type)
                PhoneModal_RegionCode.Text = .tph_country_prefix.Trim
                PhoneModal_Number.Text = .tph_number
                PhoneModal_NumberEx.Text = .tph_extension
                PhoneModal_Comment.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditAddress(id As Long)
        ObjAddress = ListAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        ObjVwAddress = VwListAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjAddress Is Nothing Then
            AddrModal.Hidden = False
            'ClearInput()
            With ObjAddress
                AddrModal_Type.SetValueAndFireSelect(.Address_Type)
                AddrModal_Address.Text = .Address.Trim
                AddrModal_Town.Text = .Town
                AddrModal_City.Text = .City
                AddrModal_PostalCode.Text = .Zip
                Dim Country As String = getNamaNegara(.Country_Code)
                AddrModal_Country.DoQuery(Country)
                AddrModal_Country.SetValue(ObjAddress.Country_Code)
                AddrModal_Province.Text = .State
                AddrModal_Comment.Text = .Comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditPhoneEmployer(id As Long)
        ObjEmployerPhone = ListPhoneEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjEmployerPhone Is Nothing Then
            WindowEmployerPhone.Hidden = False
            'ClearInput()
            With ObjEmployerPhone
                EmployerPhone_Kategori.SetValueAndFireSelect(.Tph_Contact_Type)
                EmployerPhone_Jenis.SetValueAndFireSelect(.Tph_Communication_Type)
                EmployerPhone_Kode.Text = .tph_country_prefix.Trim
                EmployerPhone_Nomor.Text = .tph_number
                EmployerPhone_Extensi.Text = .tph_extension
                EmployerPhone_Note.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditAddressEmployer(id As Long)
        ObjEmployerAddress = ListAddressEmployer.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjEmployerAddress Is Nothing Then
            WindowEmployerAddress.Hidden = False
            'ClearInput()
            With ObjEmployerAddress
                EmployerAddress_Tipe.SetValueAndFireSelect(.Address_Type)
                EmployerAddress_Alamat.Text = .Address.Trim
                EmployerAddress_Kec.Text = .Town
                EmployerAddress_Kab.Text = .City
                EmployerAddress_Pos.Text = .Zip
                Dim Country As String = getNamaNegara(.Country_Code)
                AddrModal_Country.DoQuery(Country)
                EmployerAddress_Negara.SetValue(ObjEmployerAddress.Country_Code)
                EmployerAddress_Prov.Text = .State
                EmployerAddress_Note.Text = .Comments
            End With
        End If
    End Sub

    Private Sub LoadDataDetailIdentification(id As Long)
        ObjIdentification = ListIdentification.Find(Function(x) x.PK_Person_Identification_ID = id)
        ObjVwIdentification = VwListIdentification.Find(Function(x) x.PK_Person_Identification_ID = id)
        If Not ObjIdentification Is Nothing Then
            IDModalDetail.Hidden = False
            'ClearInput()
            With ObjVwIdentification
                RP_jenis_identitas_detail.Text = .type
                RP_no_identitas_detail.Text = .Number.Trim
                RP_tgl_terbit_detail.Text = .Issue_Date
                RP_tgl_kadaluarsa_detail.Text = .Expiry_Date
                RP_penerbit_detail.Text = .Issued_By
                RP_negara_penerbit_detail.Text = .negara
                RP_note_identitas_detail.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub LoadDataDetailPhone(id As Long)
        ObjPhone = ListPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        ObjVwPhone = VwListPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjPhone Is Nothing Then
            PhoneModalDetail.Hidden = False
            'ClearInput()
            With ObjVwPhone
                RP_tipe_kontak_detail.Text = .kategori_kontak
                RP_jenis_komunikasi_detail.Text = .ja_komunikasi
                RP_kode_area_detail.Text = .tph_country_prefix.Trim
                RP_no_telepon_detail.Text = .tph_number
                RP_phone_ext_detail.Text = .tph_extension
                RP_note_phone_detail.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataDetailAddress(id As Long)
        ObjAddress = ListAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        ObjVwAddress = VwListAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjAddress Is Nothing Then
            AddrModalDetail.Hidden = False
            'ClearInput()
            With ObjVwAddress
                RP_tipe_alamat_detail.Text = .kontak
                RP_alamat_detail.Text = .address.Trim
                RP_kecamatan_detail.Text = .town
                RP_kabupaten_detail.Text = .city
                RP_kodepos_detail.Text = .zip
                RP_negara_detail.Text = .negara
                RP_propinsi_detail.Text = .state
                RP_note_alamat_detail.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataDetailPhoneEmployer(id As Long)
        ObjEmployerPhone = ListPhoneEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjEmployerPhone Is Nothing Then
            WindowEmployerPhoneDetail.Hidden = False
            'ClearInput()
            With ObjEmployerPhone
                EmployerPhoneDetail_Kategori.Text = getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                EmployerPhoneDetail_jenis.Text = getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                EmployerPhoneDetail_kode.Text = .tph_country_prefix.Trim
                EmployerPhoneDetail_nomor.Text = .tph_number
                EmployerPhoneDetail_extensi.Text = .tph_extension
                EmployerPhoneDetail_note.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataDetailAddressEmployer(id As Long)
        ObjEmployerAddress = ListAddressEmployer.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjEmployerAddress Is Nothing Then
            WindowEmployerAddressDetail.Hidden = False
            'ClearInput()
            With ObjEmployerAddress
                EmployerAddressDetail_tipe.Text = getTypeKontakAlamatbyKode(.Address_Type)
                EmployerAddressDetail_alamat.Text = .Address
                EmployerAddressDetail_kec.Text = .Town
                EmployerAddressDetail_kab.Text = .City
                EmployerAddressDetail_pos.Text = .Zip
                EmployerAddressDetail_negara.Text = getCountryByCode(.Country_Code)
                EmployerAddressDetail_prov.Text = .State
                EmployerAddressDetail_note.Text = .Comments
            End With
        End If
    End Sub

    Sub LoadData()



        If Not ObjPerson Is Nothing Then
            With ObjPerson
                PkRP.Text = .PK_ODM_Reporting_Person_ID
                Dim user As String = getUserID(.UserID)
                cmb_UserID.DoQuery(user)
                cmb_UserID.DoQuery(ObjPerson.UserID)
                cmb_UserID.SetValueAndFireSelect(.UserID)
                RP_KodeLaporan.SetValueAndFireSelect(.Kode_Laporan)
                RP_Title.Text = .Title
                RP_FulltName.Text = .Last_Name
                If .BirthDate IsNot Nothing Then
                    RP_BirthDate.Value = .BirthDate
                End If
                RP_BirthPlace.Text = .Birth_Place
                RP_Gender.SetValueAndFireSelect(.Gender)
                RP_MotherName.Text = .Mothers_Name
                RP_Alias.Text = .Alias
                RP_SSN.Text = .SSN
                RP_Passport.Text = .Passport_Number
                Dim CountPass As String = getNamaNegara(.Passport_Country)
                RP_CountryPassport.DoQuery(CountPass)
                RP_CountryPassport.SetValue(ObjPerson.Passport_Country)

                RP_OtherID.Text = .Id_Number
                Dim Nat1 As String = getNamaNegara(.Nationality1)
                RP_Nationality1.DoQuery(Nat1)
                RP_Nationality1.SetValue(ObjPerson.Nationality1)
                'RP_Email1.Focus()

                Dim Nat2 As String = getNamaNegara(.Nationality2)
                RP_Nationality2.DoQuery(Nat2)
                RP_Nationality2.SetValue(ObjPerson.Nationality2)

                Dim Nat3 As String = getNamaNegara(.Nationality3)
                RP_Nationality3.DoQuery(Nat3)
                RP_Nationality3.SetValue(ObjPerson.Nationality3)

                Dim Residence As String = getNamaNegara(.Residence)
                RP_Residence.DoQuery(Residence)
                RP_Residence.SetValue(ObjPerson.Residence)

                'RP_Nationality1.SetValueAndFireSelect(.Nationality1)
                'RP_Nationality2.SetValueAndFireSelect(.Nationality2)
                'RP_Nationality3.SetValueAndFireSelect(.Nationality3)
                'RP_Residence.SetValueAndFireSelect(.Residence)
                RP_Email1.Text = .Email1
                RP_Email2.Text = .Email2
                RP_Email3.Text = .Email3
                RP_Email4.Text = .Email4
                RP_Email5.Text = .Email5
                RP_Job.Text = .Occupation
                RP_JobPlace.Text = .Employer_Name
                RP_IsDeceased.Value = .Deceased
                RP_DeceasedDate.Disabled = Not RP_IsDeceased.Checked
                RP_DeceasedDate.Value = .Deceased_Date
                RP_TaxNumber.Text = .Tax_Number
                RP_TaxRegNumber.Value = .Tax_Reg_Number
                RP_SourceOfWealth.Text = .Source_Of_Wealth
                RP_Comments.Text = .Comment
            End With

            BindDetail()
        End If
    End Sub

    'Edit Data Sub Table
    Sub SaveEditIdentification()
        'Data Identification
        ObjIdentification.Type = IDModal_Type.SelectedItem.Value
        ObjIdentification.Number = IDModal_Number.Text
        ObjIdentification.Issue_Date = IDModal_IssueDate.Value
        ObjIdentification.Expiry_Date = IDModal_ExpiryDate.Value
        ObjIdentification.Issued_By = IDModal_IssuedBy.Text
        ObjIdentification.Issued_Country = IDModal_Country.Value
        ObjIdentification.Identification_Comment = IDModal_Comments.Text

        If IDModal_IssueDate.RawValue Is Nothing Then
            ObjIdentification.Issue_Date = IDModal_IssueDate.RawValue
        End If

        If IDModal_ExpiryDate.RawValue Is Nothing Then
            ObjIdentification.Expiry_Date = IDModal_ExpiryDate.RawValue
        End If

        'View Identification
        ObjVwIdentification.type = IDModal_Type.RawText
        ObjVwIdentification.Number = IDModal_Number.Text
        ObjVwIdentification.Issue_Date = IDModal_IssueDate.Value
        ObjVwIdentification.Expiry_Date = IDModal_ExpiryDate.Value
        ObjVwIdentification.Issued_By = IDModal_IssuedBy.Text
        ObjVwIdentification.negara = IDModal_Country.RawText
        ObjVwIdentification.Identification_Comment = IDModal_Comments.Text

        ObjIdentification = Nothing
        ObjVwIdentification = Nothing
        BindDetail()

        IDModal.Hidden = True
    End Sub
    Sub SaveEditPhone()
        'Data Identification
        ObjPhone.Tph_Contact_Type = PhoneModal_Type.SelectedItem.Value
        ObjPhone.FK_Ref_Detail_Of = 2
        ObjPhone.Tph_Communication_Type = PhoneModal_Tool.SelectedItem.Value
        ObjPhone.tph_country_prefix = PhoneModal_RegionCode.Text
        ObjPhone.tph_number = PhoneModal_Number.Text
        ObjPhone.tph_extension = PhoneModal_NumberEx.Text
        ObjPhone.comments = PhoneModal_Comment.Text

        'View Identification
        ObjVwPhone.kategori_kontak = PhoneModal_Type.RawText
        ObjVwPhone.ja_komunikasi = PhoneModal_Tool.RawText
        ObjVwPhone.tph_country_prefix = PhoneModal_RegionCode.Text
        ObjVwPhone.tph_number = PhoneModal_Number.Text
        ObjVwPhone.tph_extension = PhoneModal_NumberEx.Text
        ObjVwPhone.comments = PhoneModal_Comment.Text

        ObjPhone = Nothing
        ObjVwPhone = Nothing
        BindDetail()

        PhoneModal.Hidden = True
    End Sub
    Sub SaveEditAddress()
        'Data Address
        ObjAddress.Address_Type = AddrModal_Type.Value
        ObjAddress.FK_Ref_Detail_Of = 2
        ObjAddress.Address = AddrModal_Address.Text
        ObjAddress.Town = AddrModal_Town.Text
        ObjAddress.City = AddrModal_City.Text
        ObjAddress.Zip = AddrModal_PostalCode.Text
        ObjAddress.Country_Code = AddrModal_Country.Value
        ObjAddress.State = AddrModal_Province.Text
        ObjAddress.Comments = AddrModal_Comment.Text

        'View Address
        ObjVwAddress.kontak = AddrModal_Type.RawText
        ObjVwAddress.address = AddrModal_Address.Text
        ObjVwAddress.town = AddrModal_Town.Text
        ObjVwAddress.city = AddrModal_City.Text
        ObjVwAddress.zip = AddrModal_PostalCode.Text
        ObjVwAddress.negara = AddrModal_Country.RawText
        ObjVwAddress.state = AddrModal_Province.Text
        ObjVwAddress.comments = AddrModal_Comment.Text

        ObjAddress = Nothing
        ObjVwAddress = Nothing
        BindDetail()

        AddrModal.Hidden = True
    End Sub
    Sub SaveEditEmployerPhone()
        'Data Phone
        With ObjEmployerPhone
            .Tph_Contact_Type = EmployerPhone_Kategori.SelectedItem.Value
            .Tph_Communication_Type = EmployerPhone_Jenis.SelectedItem.Value
            .FK_Ref_Detail_Of = 11
            .tph_country_prefix = EmployerPhone_Kode.Text
            .tph_number = EmployerPhone_Nomor.Text
            .tph_extension = EmployerPhone_Extensi.Text
            .comments = EmployerPhone_Note.Text
        End With
        BindDetail()
        ObjEmployerPhone = Nothing

        WindowEmployerPhone.Hidden = True
    End Sub
    Sub SaveEditAddressEmployer()
        'Data Address
        With ObjEmployerAddress
            .Address_Type = EmployerAddress_Tipe.Value
            .FK_Ref_Detail_Of = 11
            .Address = EmployerAddress_Alamat.Text
            .Town = EmployerAddress_Kec.Text
            .City = EmployerAddress_Kab.Text
            .Zip = EmployerAddress_Pos.Text
            .Country_Code = EmployerAddress_Negara.Value
            .State = EmployerAddress_Prov.Text
            .Comments = EmployerAddress_Note.Text
        End With
        BindDetail()
        ObjEmployerAddress = Nothing

        WindowEmployerAddress.Hidden = True
    End Sub

    'Delete Data Sub Table
    Private Sub DeleteRecordIdentification(id As Long)
        Dim objVwDeleteIdentification As Vw_Temp_Identification = VwListIdentification.Find(Function(x) x.PK_Person_Identification_ID = id)
        Dim objDeleteIdentification As goAML_Person_Identification = ListIdentification.Find(Function(x) x.PK_Person_Identification_ID = id)
        If Not objVwDeleteIdentification Is Nothing Then
            VwListIdentification.Remove(objVwDeleteIdentification)
            ListIdentification.Remove(objDeleteIdentification)
            BindDetail()
        End If
    End Sub
    Private Sub DeleteRecordPhone(id As Long)
        Dim objVwDeletePhone As Vw_Temp_Phone = VwListPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objDeletePhone As goAML_Ref_Phone = ListPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not objVwDeletePhone Is Nothing Then
            VwListPhone.Remove(objVwDeletePhone)
            ListPhone.Remove(objDeletePhone)
            BindDetail()
        End If
    End Sub
    Private Sub DeleteRecordAddress(id As Long)
        Dim objVwDeleteAddress As Vw_Temp_Address = VwListAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objDeleteAddress As goAML_Ref_Address = ListAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not objVwDeleteAddress Is Nothing Then
            VwListAddress.Remove(objVwDeleteAddress)
            ListAddress.Remove(objDeleteAddress)
            BindDetail()
        End If
    End Sub

#End Region

#Region "Direct Events"

    'Btn save & edit table identification
    Protected Sub IdentificationModal_BtnAdd(sender As Object, e As DirectEventArgs)
        Try
            If ObjIdentification Is Nothing Then
                'Data Store untuk save ke DB
                SaveAddIdentification()

                'Data Store untuk view
                SaveVwListIdenfitication()
            Else
                Dim status As Boolean = True
                Dim ObjIdentificationNew As New goAML_Person_Identification

                ObjIdentificationNew.Type = IDModal_Type.SelectedItem.Value
                ObjIdentificationNew.Number = IDModal_Number.Text
                ObjIdentificationNew.Issue_Date = IDModal_IssueDate.Value
                ObjIdentificationNew.Expiry_Date = IDModal_ExpiryDate.Value
                ObjIdentificationNew.Issued_By = IDModal_IssuedBy.Text
                ObjIdentificationNew.Issued_Country = IDModal_Country.Value
                ObjIdentificationNew.Identification_Comment = IDModal_Comments.Text

                With ObjIdentification
                    If .Type <> ObjIdentificationNew.Type Then
                        status = False
                    End If
                    If .Number <> ObjIdentificationNew.Number Then
                        status = False
                    End If
                    If .Issue_Date <> ObjIdentificationNew.Issue_Date Then
                        status = False
                    End If
                    If .Expiry_Date <> ObjIdentificationNew.Expiry_Date Then
                        status = False
                    End If
                    If .Issued_By <> ObjIdentificationNew.Issued_By Then
                        status = False
                    End If
                    If .Issued_Country <> ObjIdentificationNew.Issued_Country Then
                        status = False
                    End If
                    If .Identification_Comment <> ObjIdentificationNew.Identification_Comment Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditIdentification()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Btn save & edit table Phone
    Protected Sub PhoneModal_BtnAdd(sender As Object, e As DirectEventArgs)
        Try

            If ObjPhone Is Nothing Then
                'Data Store untuk save ke DB
                SaveAddPhone()

                'Data Store untuk view
                SaveVwListPhone()
            Else
                Dim status As Boolean = True
                Dim ObjPhoneNew As New goAML_Ref_Phone

                ObjPhoneNew.Tph_Contact_Type = PhoneModal_Type.SelectedItem.Value
                ObjPhoneNew.Tph_Communication_Type = PhoneModal_Tool.SelectedItem.Value
                ObjPhoneNew.tph_country_prefix = PhoneModal_RegionCode.Text
                ObjPhoneNew.tph_number = PhoneModal_Number.Text
                ObjPhoneNew.tph_extension = PhoneModal_NumberEx.Text
                ObjPhoneNew.comments = PhoneModal_Comment.Text

                With ObjPhone
                    If .Tph_Contact_Type <> ObjPhoneNew.Tph_Contact_Type Then
                        status = False
                    End If
                    If .Tph_Communication_Type <> ObjPhoneNew.Tph_Communication_Type Then
                        status = False
                    End If
                    If .tph_country_prefix <> ObjPhoneNew.tph_country_prefix Then
                        status = False
                    End If
                    If .tph_number <> ObjPhoneNew.tph_number Then
                        status = False
                    End If
                    If .tph_extension <> ObjPhoneNew.tph_extension Then
                        status = False
                    End If
                    If .comments <> ObjPhoneNew.comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditPhone()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    'Btn save & edit table Address
    Protected Sub AddressModal_BtnAdd(sender As Object, e As DirectEventArgs)
        Try
            If ObjAddress Is Nothing Then
                'Data Store untuk save ke DB
                SaveAddAddress()
                'Data Store untuk view
                SaveVwListAddress()
            Else
                Dim status As Boolean = True
                Dim ObjAddressNew As New goAML_Ref_Address

                ObjAddressNew.Address_Type = AddrModal_Type.Value
                ObjAddressNew.Address = AddrModal_Address.Text
                ObjAddressNew.Town = AddrModal_Town.Text
                ObjAddressNew.City = AddrModal_City.Text
                ObjAddressNew.Zip = AddrModal_PostalCode.Text
                ObjAddressNew.Country_Code = AddrModal_Country.Value
                ObjAddressNew.State = AddrModal_Province.Text
                ObjAddressNew.Comments = AddrModal_Comment.Text

                With ObjAddress
                    If .Address_Type <> ObjAddressNew.Address_Type Then
                        status = False
                    End If
                    If .Address <> ObjAddressNew.Address Then
                        status = False
                    End If
                    If .Town <> ObjAddressNew.Town Then
                        status = False
                    End If
                    If .City <> ObjAddressNew.City Then
                        status = False
                    End If
                    If .Zip <> ObjAddressNew.Zip Then
                        status = False
                    End If
                    If .Country_Code <> ObjAddressNew.Country_Code Then
                        status = False
                    End If
                    If .State <> ObjAddressNew.State Then
                        status = False
                    End If
                    If .Comments <> ObjAddressNew.Comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditAddress()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Grind Command
    Protected Sub GridCommandIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordPhone(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordAddress(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployer(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ListPhoneEmployer.Remove(ListPhoneEmployer.Where(Function(x) x.PK_goAML_Ref_Phone = ID).FirstOrDefault)
                BindDetail()
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneEmployer(ID)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhoneEmployer(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEmployer(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ListAddressEmployer.Remove(ListAddressEmployer.Where(Function(x) x.PK_Customer_Address_ID = ID).FirstOrDefault)
                BindDetail()
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressEmployer(ID)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddressEmployer(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Direct Event Phone
    Protected Sub PhoneField_BtnAdd(sender As Object, e As DirectEventArgs)
        PhoneModal.Show()
        AddrModal.Hide()
        IDModal.Hide()

        PhoneModal_Number.Clear()
        PhoneModal_NumberEx.Clear()
        PhoneModal_RegionCode.Clear()
        PhoneModal_Tool.Clear()
        PhoneModal_Type.Clear()
        PhoneModal_Comment.Clear()
    End Sub

    Protected Sub Btn_AddEmployerAddress_click(sender As Object, e As DirectEventArgs)
        WindowEmployerAddress.Show()

        EmployerAddress_Tipe.Clear()
        EmployerAddress_Alamat.Clear()
        EmployerAddress_Kec.Clear()
        EmployerAddress_Kab.Clear()
        EmployerAddress_Pos.Clear()
        EmployerAddress_Negara.Clear()
        EmployerAddress_Prov.Clear()
        EmployerAddress_Note.Clear()
    End Sub
    Protected Sub Btn_AddEmployerPhone_click(sender As Object, e As DirectEventArgs)
        WindowEmployerPhone.Show()

        EmployerPhone_Kategori.Clear()
        EmployerPhone_Jenis.Clear()
        EmployerPhone_Kode.Clear()
        EmployerPhone_Nomor.Clear()
        EmployerPhone_Extensi.Clear()
        EmployerPhone_Nomor.Clear()
        EmployerPhone_Note.Clear()
    End Sub

    'Direct event Address
    Protected Sub AddressField_BtnAdd(sender As Object, e As DirectEventArgs)
        PhoneModal.Hide()
        AddrModal.Show()
        IDModal.Hide()

        AddrModal_Address.Clear()
        AddrModal_City.Clear()
        AddrModal_Comment.Clear()
        AddrModal_Country.Clear()
        AddrModal_PostalCode.Clear()
        AddrModal_Province.Clear()
        AddrModal_Town.Clear()
        AddrModal_Type.Clear()
    End Sub

    Protected Sub AddressModal_BtnCancel(sender As Object, e As DirectEventArgs)
        AddrModal.Hide()
        AddrModalDetail.Hide()
        WindowEmployerAddress.Hide()
        WindowEmployerAddressDetail.Hide()
        ObjAddress = Nothing
        ObjVwAddress = Nothing
        ObjEmployerAddress = Nothing
    End Sub

    Protected Sub IDModal_BtnCancel(sender As Object, e As DirectEventArgs)
        IDModal.Hide()
        IDModalDetail.Hide()
    End Sub

    Protected Sub PhoneModal_BtnCancel(sender As Object, e As DirectEventArgs)
        PhoneModal.Hide()
        PhoneModalDetail.Hide()
        WindowEmployerPhone.Hide()
        WindowEmployerPhoneDetail.Hide()
        ObjPhone = Nothing
        ObjVwPhone = Nothing
        ObjEmployerPhone = Nothing
    End Sub
    Protected Sub PhoneEmployerModal_BtnAdd(sender As Object, e As DirectEventArgs)
        Try
            If ObjEmployerPhone Is Nothing Then
                'Data Store untuk save ke DB
                SaveAddEmployerPhone()
            Else
                Dim status As Boolean = True
                Dim ObjPhoneNew As New goAML_Ref_Phone

                ObjPhoneNew.Tph_Contact_Type = EmployerPhone_Kategori.SelectedItem.Value
                ObjPhoneNew.Tph_Communication_Type = EmployerPhone_Jenis.SelectedItem.Value
                ObjPhoneNew.tph_country_prefix = EmployerPhone_Kode.Text
                ObjPhoneNew.tph_number = EmployerPhone_Nomor.Text
                ObjPhoneNew.tph_extension = EmployerPhone_Extensi.Text
                ObjPhoneNew.comments = EmployerPhone_Note.Text

                With ObjEmployerPhone
                    If .Tph_Contact_Type <> ObjPhoneNew.Tph_Contact_Type Then
                        status = False
                    End If
                    If .Tph_Communication_Type <> ObjPhoneNew.Tph_Communication_Type Then
                        status = False
                    End If
                    If .tph_country_prefix <> ObjPhoneNew.tph_country_prefix Then
                        status = False
                    End If
                    If .tph_number <> ObjPhoneNew.tph_number Then
                        status = False
                    End If
                    If .tph_extension <> ObjPhoneNew.tph_extension Then
                        status = False
                    End If
                    If .comments <> ObjPhoneNew.comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditEmployerPhone()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Direct event Identification
    Protected Sub IDField_BtnAdd(sender As Object, e As DirectEventArgs)
        PhoneModal.Hide()
        AddrModal.Hide()
        IDModal.Show()

        IDModal_Comments.Clear()
        IDModal_Country.Clear()
        IDModal_ExpiryDate.Clear()
        IDModal_IssueDate.Clear()
        IDModal_IssuedBy.Clear()
        IDModal_Number.Clear()
        IDModal_Type.Clear()
    End Sub
    Protected Sub AddressEmployerModal_BtnAdd(sender As Object, e As DirectEventArgs)
        Try
            If ObjEmployerAddress Is Nothing Then
                'Data Store untuk save ke DB
                SaveAddAddressEmployer()
            Else
                Dim status As Boolean = True
                Dim ObjAddressNew As New goAML_Ref_Address

                ObjAddressNew.Address_Type = EmployerAddress_Tipe.Value
                ObjAddressNew.Address = EmployerAddress_Alamat.Text
                ObjAddressNew.Town = EmployerAddress_Kec.Text
                ObjAddressNew.City = EmployerAddress_Kab.Text
                ObjAddressNew.Zip = EmployerAddress_Pos.Text
                ObjAddressNew.Country_Code = EmployerAddress_Negara.Value
                ObjAddressNew.State = EmployerAddress_Prov.Text
                ObjAddressNew.Comments = EmployerAddress_Note.Text

                With ObjEmployerAddress
                    If .Address_Type <> ObjAddressNew.Address_Type Then
                        status = False
                    End If
                    If .Address <> ObjAddressNew.Address Then
                        status = False
                    End If
                    If .Town <> ObjAddressNew.Town Then
                        status = False
                    End If
                    If .City <> ObjAddressNew.City Then
                        status = False
                    End If
                    If .Zip <> ObjAddressNew.Zip Then
                        status = False
                    End If
                    If .Country_Code <> ObjAddressNew.Country_Code Then
                        status = False
                    End If
                    If .State <> ObjAddressNew.State Then
                        status = False
                    End If
                    If .Comments <> ObjAddressNew.Comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditAddressEmployer()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    'Check Box IsDeceased
    Protected Sub RP_IsDeceased_CheckedChanged(sender As Object, e As EventArgs) Handles RP_IsDeceased.DirectCheck

        RP_DeceasedDate.Disabled = Not RP_IsDeceased.Checked

        If RP_IsDeceased.Value = False Then
            RP_DeceasedDate.Clear()
        End If
    End Sub

    Sub AddToReportClass()
        With ReportingPersonClass
            .ObjPhone = ListPhone
            .ObjEmpPhone = ListPhoneEmployer
            .ObjAddress = ListAddress
            .ObjEmpAddress = ListAddressEmployer
            .ObjIdent = ListIdentification
        End With
    End Sub
    Sub isChanges()
        Dim Status As Boolean = True
        Dim objPersonOld As New goAML_ODM_Reporting_Person
        Dim objListPhoneOld As New List(Of goAML_Ref_Phone)
        Dim objListAddressOld As New List(Of goAML_Ref_Address)
        Dim objListPhoneEmployerOld As New List(Of goAML_Ref_Phone)
        Dim objListAddressEmployerOld As New List(Of goAML_Ref_Address)
        Dim objListIdentificationOld As New List(Of goAML_Person_Identification)
        Dim dataStr As String = Request.Params("ID")
        Dim dataID As String = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)

        objPersonOld = NawaDevBLL.ReportingPerson.GetReportingPersonByID(CInt(dataID))
        objListPhoneOld = NawaDevBLL.ReportingPerson.GetListPhoneByID(CInt(dataID), 2)
        objListPhoneEmployerOld = NawaDevBLL.ReportingPerson.GetListPhoneByID(CInt(dataID), 11)
        objListAddressOld = NawaDevBLL.ReportingPerson.GetListAddressByID(CInt(dataID), 2)
        objListAddressEmployerOld = NawaDevBLL.ReportingPerson.GetListAddressByID(CInt(dataID), 11)
        objListIdentificationOld = NawaDevBLL.ReportingPerson.GetListIdentificationByID(CInt(dataID))

        With ReportingPersonClass.objPerson
            If .Kode_Laporan <> objPersonOld.Kode_Laporan Then
                Status = False
            End If
            If .Gender <> objPersonOld.Gender Then
                Status = False
            End If
            If .Title <> objPersonOld.Title Then
                Status = False
            End If
            If .Last_Name <> objPersonOld.Last_Name Then
                Status = False
            End If
            If .BirthDate <> objPersonOld.BirthDate Then
                Status = False
            End If
            If .Birth_Place <> objPersonOld.Birth_Place Then
                Status = False
            End If
            If .Mothers_Name <> objPersonOld.Mothers_Name Then
                Status = False
            End If
            If .Alias <> objPersonOld.Alias Then
                Status = False
            End If
            If .SSN <> objPersonOld.SSN Then
                Status = False
            End If
            If .Passport_Number <> objPersonOld.Passport_Number Then
                Status = False
            End If
            If .Passport_Country <> objPersonOld.Passport_Country Then
                Status = False
            End If
            If .Id_Number <> objPersonOld.Id_Number Then
                Status = False
            End If
            If .Nationality1 <> objPersonOld.Nationality1 Then
                Status = False
            End If
            If .Nationality2 <> objPersonOld.Nationality2 Then
                Status = False
            End If
            If .Nationality3 <> objPersonOld.Nationality3 Then
                Status = False
            End If
            If .Residence <> objPersonOld.Residence Then
                Status = False
            End If
            If .Email1 <> objPersonOld.Email1 Then
                Status = False
            End If
            If .Email2 <> objPersonOld.Email2 Then
                Status = False
            End If
            If .Email3 <> objPersonOld.Email3 Then
                Status = False
            End If
            If .Email4 <> objPersonOld.Email4 Then
                Status = False
            End If
            If .Email5 <> objPersonOld.Email5 Then
                Status = False
            End If
            If .Occupation <> objPersonOld.Occupation Then
                Status = False
            End If
            If .Employer_Name <> objPersonOld.Employer_Name Then
                Status = False
            End If
            If .Deceased <> objPersonOld.Deceased Then
                Status = False
            End If
            If .Deceased_Date <> objPersonOld.Deceased_Date Then
                Status = False
            End If
            If .Tax_Number <> objPersonOld.Tax_Number Then
                Status = False
            End If
            If .Tax_Reg_Number <> objPersonOld.Tax_Reg_Number Then
                Status = False
            End If
            If .Source_Of_Wealth <> objPersonOld.Source_Of_Wealth Then
                Status = False
            End If
            If .Comment <> objPersonOld.Comment Then
                Status = False
            End If
            If .UserID <> objPersonOld.UserID Then
                Status = False
            End If
        End With

        If objListAddressOld.Count = ReportingPersonClass.ObjAddress.Count Then
            Dim Address As New goAML_Ref_Address
            For Each item In ReportingPersonClass.ObjAddress
                Address = objListAddressOld.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                If Address.FK_Ref_Detail_Of IsNot Nothing Then
                    With Address
                        If .FK_Ref_Detail_Of <> item.FK_Ref_Detail_Of Then
                            Status = False
                        End If
                        If .FK_To_Table_ID <> item.FK_To_Table_ID Then
                            Status = False
                        End If
                        If .Address_Type <> item.Address_Type Then
                            Status = False
                        End If
                        If .Address <> item.Address Then
                            Status = False
                        End If
                        If .Town <> item.Town Then
                            Status = False
                        End If
                        If .City <> item.City Then
                            Status = False
                        End If
                        If .Zip <> item.Zip Then
                            Status = False
                        End If
                        If .Country_Code <> item.Country_Code Then
                            Status = False
                        End If
                        If .State <> item.State Then
                            Status = False
                        End If
                        If .Comments <> item.Comments Then
                            Status = False
                        End If
                    End With
                Else
                    Status = False
                End If
                Address = New goAML_Ref_Address
            Next
        Else
            Status = False
        End If
        If objListAddressEmployerOld.Count = ReportingPersonClass.ObjEmpAddress.Count Then
            Dim AddressEmp As New goAML_Ref_Address
            For Each item In ReportingPersonClass.ObjEmpAddress
                AddressEmp = objListAddressEmployerOld.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                If AddressEmp.FK_Ref_Detail_Of IsNot Nothing Then
                    With AddressEmp
                        If .FK_Ref_Detail_Of <> item.FK_Ref_Detail_Of Then
                            Status = False
                        End If
                        If .FK_To_Table_ID <> item.FK_To_Table_ID Then
                            Status = False
                        End If
                        If .Address_Type <> item.Address_Type Then
                            Status = False
                        End If
                        If .Address <> item.Address Then
                            Status = False
                        End If
                        If .Town <> item.Town Then
                            Status = False
                        End If
                        If .City <> item.City Then
                            Status = False
                        End If
                        If .Zip <> item.Zip Then
                            Status = False
                        End If
                        If .Country_Code <> item.Country_Code Then
                            Status = False
                        End If
                        If .State <> item.State Then
                            Status = False
                        End If
                        If .Comments <> item.Comments Then
                            Status = False
                        End If
                    End With
                Else
                    Status = False
                End If
                AddressEmp = New goAML_Ref_Address
            Next
        Else
            Status = False
        End If
        If objListPhoneOld.Count = ReportingPersonClass.ObjPhone.Count Then
            Dim phone As New goAML_Ref_Phone
            For Each item In ReportingPersonClass.ObjPhone
                phone = objListPhoneOld.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                If phone.FK_Ref_Detail_Of IsNot Nothing Then
                    With phone
                        If .FK_Ref_Detail_Of <> item.FK_Ref_Detail_Of Then
                            Status = False
                        End If
                        If .FK_for_Table_ID <> item.FK_for_Table_ID Then
                            Status = False
                        End If
                        If .Tph_Contact_Type <> item.Tph_Contact_Type Then
                            Status = False
                        End If
                        If .Tph_Communication_Type <> item.Tph_Communication_Type Then
                            Status = False
                        End If
                        If .tph_country_prefix <> item.tph_country_prefix Then
                            Status = False
                        End If
                        If .tph_number <> item.tph_number Then
                            Status = False
                        End If
                        If .tph_extension <> item.tph_extension Then
                            Status = False
                        End If
                        If .comments <> item.comments Then
                            Status = False
                        End If
                    End With
                Else
                    Status = False
                End If
                phone = New goAML_Ref_Phone
            Next
        Else
            Status = False
        End If
        If objListPhoneEmployerOld.Count = ReportingPersonClass.ObjEmpPhone.Count Then
            Dim phoneEmp As New goAML_Ref_Phone
            For Each item In ReportingPersonClass.ObjEmpPhone
                phoneEmp = objListPhoneEmployerOld.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                If phoneEmp.FK_Ref_Detail_Of IsNot Nothing Then
                    With phoneEmp
                        If .FK_Ref_Detail_Of <> item.FK_Ref_Detail_Of Then
                            Status = False
                        End If
                        If .FK_for_Table_ID <> item.FK_for_Table_ID Then
                            Status = False
                        End If
                        If .Tph_Contact_Type <> item.Tph_Contact_Type Then
                            Status = False
                        End If
                        If .Tph_Communication_Type <> item.Tph_Communication_Type Then
                            Status = False
                        End If
                        If .tph_country_prefix <> item.tph_country_prefix Then
                            Status = False
                        End If
                        If .tph_number <> item.tph_number Then
                            Status = False
                        End If
                        If .tph_extension <> item.tph_extension Then
                            Status = False
                        End If
                        If .comments <> item.comments Then
                            Status = False
                        End If
                    End With
                Else
                    Status = False
                End If
                phoneEmp = New goAML_Ref_Phone
            Next
        Else
            Status = False
        End If
        If objListIdentificationOld.Count = ReportingPersonClass.ObjIdent.Count Then
            Dim Identification As New goAML_Person_Identification
            For Each item In ReportingPersonClass.ObjIdent
                Identification = objListIdentificationOld.Where(Function(x) x.PK_Person_Identification_ID = item.PK_Person_Identification_ID).FirstOrDefault
                If Identification.FK_Person_ID IsNot Nothing Then
                    With Identification
                        If .isReportingPerson <> item.isReportingPerson Then
                            Status = False
                        End If
                        If .FK_Person_ID <> item.FK_Person_ID Then
                            Status = False
                        End If
                        If .Type <> item.Type Then
                            Status = False
                        End If
                        If .Number <> item.Number Then
                            Status = False
                        End If
                        If .Issue_Date <> item.Issue_Date Then
                            Status = False
                        End If
                        If .Expiry_Date <> item.Expiry_Date Then
                            Status = False
                        End If
                        If .Issued_By <> item.Issued_By Then
                            Status = False
                        End If
                        If .Issued_Country <> item.Issued_Country Then
                            Status = False
                        End If
                        If .Identification_Comment <> item.Identification_Comment Then
                            Status = False
                        End If
                        If .FK_Person_Type <> item.FK_Person_Type Then
                            Status = False
                        End If
                    End With
                Else
                    Status = False
                End If
                Identification = New goAML_Person_Identification
            Next
        Else
            Status = False
        End If

        If Status Then
            Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
        End If
    End Sub

    Protected Sub BtnAddRP(sender As Object, e As DirectEventArgs)
        Try
            Dim objSave As New goAML_ODM_Reporting_Person With
                {
                    .PK_ODM_Reporting_Person_ID = PkRP.Text,
                    .UserID = cmb_UserID.Value,
                    .Kode_Laporan = RP_KodeLaporan.Value,
                    .Title = RP_Title.Text,
                    .Last_Name = RP_FulltName.Text,
                    .BirthDate = RP_BirthDate.Value,
                    .Birth_Place = RP_BirthPlace.Text,
                    .Gender = RP_Gender.Value,
                    .Mothers_Name = RP_MotherName.Text,
                    .Alias = RP_Alias.Text,
                    .SSN = RP_SSN.Text,
                    .Passport_Number = RP_Passport.Text,
                    .Passport_Country = RP_CountryPassport.Text,
                    .Id_Number = RP_OtherID.Text,
                    .Nationality1 = RP_Nationality1.Text,
                    .Nationality2 = RP_Nationality2.Text,
                    .Nationality3 = RP_Nationality3.Text,
                    .Residence = RP_Residence.Text,
                    .Email1 = RP_Email1.Text,
                    .Email2 = RP_Email2.Text,
                    .Email3 = RP_Email3.Text,
                    .Email4 = RP_Email4.Text,
                    .Email5 = RP_Email5.Text,
                    .Occupation = RP_Job.Text,
                    .Employer_Name = RP_JobPlace.Text,
                    .Deceased = RP_IsDeceased.Value,
                    .Deceased_Date = RP_DeceasedDate.Value,
                    .Tax_Number = RP_TaxNumber.Text,
                    .Tax_Reg_Number = RP_TaxRegNumber.Value,
                    .Source_Of_Wealth = RP_SourceOfWealth.Text,
                    .Comment = RP_Comments.Text,
                    .Active = True,
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID,
                    .LastUpdateDate = Date.Now
                }

            If RP_DeceasedDate.RawValue Is Nothing Then
                objSave.Deceased_Date = RP_DeceasedDate.RawValue
            End If

            ReportingPersonClass.objPerson = objSave
            AddToReportClass()

            If ListAddress.Count > 0 Then
                isChanges()
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                    goAML_PersonBLL.SaveEditTanpaApproval(ReportingPersonClass, Me.ObjModule)
                    LblConfirmation.Text = "Data Saved into Database"
                Else
                    goAML_PersonBLL.SaveEditApproval(ReportingPersonClass, Me.ObjModule)
                    LblConfirmation.Text = "Data Saved into Pending Approval"
                End If

                RP_EntireForm.Hidden = True
                Panelconfirmation.Hidden = False
            Else
                Ext.Net.X.Msg.Alert("Message", "Minimal Alamat Harus Ada 1(Satu)").Show()
            End If


        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Button Cancel Modul
    Protected Sub BtnCancelRP()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Moduleid = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#End Region

    Function getNamaNegara(kode As String) As String
        Dim strReturn As String = ""
        Dim str As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select keterangan from goAML_Ref_Nama_Negara where kode='" & kode & "'", Nothing)

        If str IsNot Nothing Then
            strReturn = str
        End If
        Return strReturn
    End Function
    Function getUserID(kode As String) As String
        Dim strReturn As String = ""
        Dim str As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select keterangan from VwUserID where kode='" & kode & "'", Nothing)

        If str IsNot Nothing Then
            strReturn = str
        End If
        Return strReturn
    End Function
End Class