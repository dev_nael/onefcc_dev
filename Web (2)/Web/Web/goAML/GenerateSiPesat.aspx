﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="GenerateSiPesat.aspx.vb" Inherits="goAML_GenerateSiPesat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {
            App.GridpanelView.columns.forEach(function (col) {
                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }
            });
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:FormPanel ID="FormPanelInput" runat="server" Layout="AnchorLayout" ButtonAlign="Center" Title="" BodyStyle="padding:20px" AutoScroll="true" Hidden="false" ClientIDMode="Static">
        <Items>
            <ext:Container runat="server" Layout="ColumnLayout" AnchorHorizontal="100%">
                <Items>
                    <ext:Container runat="server" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField runat="server" ID="lblIDPJK" FieldLabel="IDPJK" Text="A value here 1" />
                            <ext:DisplayField runat="server" ID="lblPeriode" FieldLabel="Periode" Text="A value here 1" />
                            <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" EmptyText="[Select Format]" FieldLabel="Report Type">

                                <Items>
                                    <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                                    <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                                    <%--<ext:ListItem Text="TXT" Value="TXT"></ext:ListItem>--%>
                                    <ext:ListItem Text="XML" Value="XML"></ext:ListItem>
                                </Items>

                            </ext:ComboBox>
                        </Items>
                    </ext:Container>
                    <ext:Container runat="server" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField runat="server" ID="lblTotalValid" FieldLabel="Total Valid" Text="A value here 1" />
                            <ext:DisplayField runat="server" ID="lblTotalInvalid" FieldLabel="Total Invalid" Text="A value here 1" />
                            <ext:DisplayField runat="server" ID="lblStatusUpload" FieldLabel="Status" Text="A value here 1" />
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Container>
        </Items>
        <Items>
            <ext:GridPanel ID="GridpanelView" ClientIDMode="Static" runat="server" Title="List OF Generate goAML" Width="2000" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true">
                    </ext:GridView>
                </View>
                <Store>
                    <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" >
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="PK_Report_ID" Type="Int" />
                                    <ext:ModelField Name="PK_ID" Type="Int" />
                                    <ext:ModelField Name="PK_Customer_Type_ID" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Place_Birth" Type="String" />
                                    <ext:ModelField Name="PK_Date_Birth" Type="String" />
                                    <ext:ModelField Name="Address" Type="String" />
                                    <ext:ModelField Name="No_KTP" Type="String" />
                                    <ext:ModelField Name="PK_Other_ID" Type="String" />
                                    <ext:ModelField Name="No_CIF" Type="String" />
                                    <ext:ModelField Name="No_NPWP" Type="String" />
                                    <ext:ModelField Name="Period" Type="String" />
                                    <ext:ModelField Name="Jenis_Informasi" Type="Int" />
                                    <ext:ModelField Name="IsValid" Type="String" />
                                    <ext:ModelField Name="Status" Type="String" />
                                    <ext:ModelField Name="LastUpdateDate" Type="Date" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column runat="server" DataIndex="PK_Report_ID" Text="ID" ID="No" />
                        <ext:Column runat="server" DataIndex="PK_ID" Text="IDPJK" />
                        <ext:Column runat="server" DataIndex="PK_Customer_Type_ID" Text="Kode Nasabah" />
                        <ext:Column runat="server" DataIndex="Name" Text="Nama Nasabah" />
                        <ext:Column runat="server" DataIndex="Place_Birth" Text="Tempat Lahir" />
                        <ext:DateColumn runat="server" DataIndex="PK_Date_Birth" Text="Tanggal Lahir" />
                        <ext:Column runat="server" DataIndex="Address" Text="Alamat" />
                        <ext:Column runat="server" DataIndex="No_KTP" Text="NIK" />
                        <ext:Column runat="server" DataIndex="PK_Other_ID" Text="Nomor Identitas Lain" />
                        <ext:Column runat="server" DataIndex="No_CIF" Text="Nomor CIF" />
                        <ext:Column runat="server" DataIndex="No_NPWP" Text="Nomor NPWP" />
                        <ext:Column runat="server" DataIndex="Period" Text="Period" />
                        <ext:Column runat="server" DataIndex="Jenis_Informasi" Text="Jenis Informasi" />
                        <ext:Column runat="server" DataIndex="IsValid" Text="Valid" />
                        <ext:Column runat="server" DataIndex="Status" Text="Status" />
                        <ext:DateColumn runat="server" DataIndex="LastUpdateDate" Text="Last Update Date" />
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader ID="grdiheaderfilter" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                </BottomBar>
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                        <Items>
                            <ext:Button ID="BtnAdd" runat="server" Text="Add New Record" Icon="Add" Handler="NawadataDirect.BtnAdd_Click()" Hidden="true">
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Listeners>
                    <ViewReady Handler="columnAutoResize(this);
                    this.getStore().on('load', Ext.bind(columnAutoResize, null, [this]));"
                        Delay="10" />
                </Listeners>
            </ext:GridPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btnGenerateAll" runat="server" Text="Generate All" Icon="Disk" AutoPostBack="true" OnClick="BtnGenerateAll_Click"></ext:Button>
            <ext:Button ID="btnSaveUpload" runat="server" Text="Generate Valid" Icon="Disk" AutoPostBack="true" OnClick="BtnSave_DirectClick"></ext:Button>
            <ext:Button ID="btnGenerateInvalid" runat="server" Text="Generate Invalid" Icon="Disk" AutoPostBack="true" OnClick="btnGenerateInvalid_Click"></ext:Button>
            <ext:Button ID="btnCancelUpload" runat="server" Text="Back" Icon="Cancel">
                <DirectEvents>
                    <Click OnEvent="btnCancel_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
