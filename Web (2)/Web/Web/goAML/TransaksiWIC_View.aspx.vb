﻿
Imports NawaDevDAL
Imports OfficeOpenXml

Partial Class TransaksiWIC_View
    Inherits ParentPage

    Public objFormModuleView As NawaBLL.FormModuleView
    'Private ObjEntities As New NawaDevDAL.NawaDatadevEntities
    'Private ObjSystemParamLvl1 As NawaDevDAL.SystemParameter = ObjEntities.SystemParameters.Where(Function(x) x.PK_SystemParameter_ID = 8000).FirstOrDefault

    Public Property strWhereClause() As String
        Get
            Return Session("TransaksiWIC_View.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("TransaksiWIC_View.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("TransaksiWIC_View.strSort")
        End Get
        Set(ByVal value As String)
            Session("TransaksiWIC_View.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("TransaksiWIC_View.indexStart")
        End Get
        Set(ByVal value As String)
            Session("TransaksiWIC_View.indexStart") = value
        End Set
    End Property

    Public Property ObjTable() As String
        Get
            Return Session("TransaksiWIC_View.ObjTable")
        End Get
        Set(ByVal value As String)
            Session("TransaksiWIC_View.ObjTable") = value
        End Set
    End Property

    Public Property ObjField() As String
        Get
            Return Session("TransaksiWIC_View.ObjField")
        End Get
        Set(ByVal value As String)
            Session("TransaksiWIC_View.ObjField") = value
        End Set
    End Property

    Private Sub WorkingPaperQA_RCSA_TemplateRCSA_view_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(GridpanelView, BtnAdd)
    End Sub

    Private Sub WorkingPaperQA_RCSA_TemplateRCSA_view_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            objFormModuleView.ModuleID = ObjModule.PK_Module_ID
            objFormModuleView.ModuleName = ObjModule.ModuleName

            If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                Dim strIDCode As String = 1
                strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                Exit Sub
            End If

            objFormModuleView.AddField("NO_ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.IDENTITY)
            objFormModuleView.AddField("Date_Transaction", "Date Transaction", 2, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
            objFormModuleView.AddField("CIF_NO", "CIF NO", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("WIC_ID", "WIC ID", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Account_NO", "Account NO", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("isNasabah", "Nasabah", 6, False, True, NawaBLL.Common.MFieldType.BooleanValue)
            objFormModuleView.AddField("Ref_Num", "Ref Num", 7, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Debit_Credit", "Debit/Credit", 8, False, True, NawaBLL.Common.MFieldType.INTValue)
            objFormModuleView.AddField("Original_Amount", "Original Amount", 9, False, True, NawaBLL.Common.MFieldType.MONEYValue)
            objFormModuleView.AddField("Currency", "Currency", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Exchange_Rate", "Exchange Rate", 11, False, True, NawaBLL.Common.MFieldType.MONEYValue)
            objFormModuleView.AddField("IDR_Amount", "IDR Amount", 12, False, True, NawaBLL.Common.MFieldType.MONEYValue)
            objFormModuleView.AddField("Transaction_Code", "Transaction Code", 13, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Source_Data", "Source Data", 14, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Remark", "Remark", 15, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Transaction_Remark", "Transaction Remark", 16, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Transaction_Number", "Transaction Number", 17, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Transaction_Location", "Transaction Location", 18, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Teller", "Teller", 19, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Authorized", "Authorized", 20, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Date_Posting", "Date Posting", 21, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
            objFormModuleView.AddField("Value_Date", "Value Date", 22, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
            objFormModuleView.AddField("Transmode_Code", "Transmode Code", 23, False, False, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Transmode_Comment", "Transmode Comment", 24, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Comments", "Comments", 25, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.SettingFormView()
            objFormModuleView.objGridPanel.Title = "Transaksi WIC"

            If Not Ext.Net.X.IsAjaxRequest Then
                GridpanelView.ColumnModel.Columns(2).Width = 240
                GridpanelView.ColumnModel.Columns(7).Width = 260
            End If


            'remarked 16 Jan 2020
            'If cboExportExcel.SelectedItem.Value Is Nothing Then
            'If Not Ext.Net.X.IsAjaxRequest Then
            '    cboExportExcel.SelectedItem.Value = "Excel"
            'End If
            'End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)
            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter

            'Dim TaskList As String = Request.QueryString("Task")
            'If Not TaskList Is Nothing Then
            '    If strWhereClause = "" Then
            '        strWhereClause += "Kode_StatusDokumen = 'WP_Doc003'"
            '    Else
            '        strWhereClause += " and Kode_StatusDokumen = 'WP_Doc003'"
            '    End If
            'End If


            Me.strOrder = strsort

            If strOrder = "" Then
                strOrder = "LastUpdateDate desc"
            End If

            ObjTable = "goAML_ODM_Transaksi_wic"
            ObjField = "NO_ID,Date_Transaction,CIF_NO,WIC_ID,Account_NO,isNasabah,Ref_Num,Debit_Credit,Original_Amount,Currency,Exchange_Rate,IDR_Amount,Transaction_Code,"
            ObjField &= "Source_Data,Remark,Transaction_Remark,Transaction_Number,Transaction_Location,Teller,Authorized,Date_Posting,Value_Date,Transmode_Code,Transmode_Comment,Comments"

            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(ObjTable, ObjField, strWhereClause, strOrder, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start <0 OrElse limit <0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    'Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(ObjTable, ObjField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    'Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(ObjTable, ObjField, Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    'Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(ObjTable, ObjField, Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)
                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    'Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(ObjTable, ObjField, Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)
                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    <DirectMethod>
    Public Sub BtnAdd_Click()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub WorkingPaperQA_RCSA_TemplateRCSA_view_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.view
    End Sub
End Class
