﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="NotReportedApprovalDetail.aspx.vb" Inherits="goAML_NotReportedApprovalDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .my-style .x-form-display-field-default {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:Container ID="container" runat="server" Layout="VBoxLayout">
        <LayoutConfig>
            <ext:VBoxLayoutConfig Align="Stretch"></ext:VBoxLayoutConfig>
        </LayoutConfig>
        <Items>
            <ext:FormPanel ID="PanelInfo" runat="server" Title="Module Approval" BodyPadding="10">
                <Items>
                    <ext:DisplayField ID="lblModuleName" runat="server" FieldLabel="Module Name">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblModuleKey" runat="server" FieldLabel="Module Key">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblAction" runat="server" FieldLabel="Action">
                    </ext:DisplayField>
                    <ext:DisplayField ID="LblCreatedBy" runat="server" FieldLabel="Created By">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblCreatedDate" runat="server" FieldLabel="Created Date">
                    </ext:DisplayField>
                </Items>
            </ext:FormPanel>

            <%--<ext:Panel runat="server" ID="ReportGeneralInformationPanel" Flex="4" ButtonAlign="Center" Layout="AnchorLayout" ClientIDMode="Static" Title="Generate STR SAR Information" BodyStyle="padding:10px" Margin="5" Height="600" Border="True" AutoScroll="true" Collapsible="true">--%>
            <ext:Panel ID="Panel1" runat="server" Layout="HBoxLayout" ButtonAlign="Center" Flex="1" AutoScroll="true">
                <Items>
                    <ext:FormPanel ID="FormPanelOld" runat="server" Title="Old Value" Flex="1" BodyPadding="10" MarginSpec="0 5 0 0" Hidden="true">
                        <Items>
                            <ext:DisplayField ID="ReportID_Old" runat="server" FieldLabel="Report ID" AnchorHorizontal="100%" />
                            <ext:DisplayField ID="Reason_Old" runat="server" FieldLabel="Alasan" AnchorHorizontal="70%" />
                        </Items>
                    </ext:FormPanel>

                    <ext:FormPanel ID="FormPanelNew" runat="server" Title="New Value" Flex="1" BodyPadding="10">
                        <Items>
                            <ext:DisplayField ID="ReportID_New" runat="server" FieldLabel="Report ID" AnchorHorizontal="100%" />
                            <ext:DisplayField ID="Reason_New" runat="server" FieldLabel="Alasan" AnchorHorizontal="70%" />
                        </Items>
                    </ext:FormPanel>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnSave" runat="server" Text="Accept" Icon="DiskBlack">
                        <DirectEvents>
                            <Click OnEvent="BtnSave_Click">
                                <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnReject" runat="server" Text="Reject" Icon="Decline">
                        <DirectEvents>
                            <Click OnEvent="BtnReject_Click">
                                <EventMask ShowMask="true" Msg="Saving Reject Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnCancel" runat="server" Text="Cancel" Icon="PageBack">
                        <DirectEvents>
                            <Click OnEvent="BtnCancel_Click">
                                <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:Panel>
        </Items>
    </ext:Container>

    <ext:FormPanel ID="Panelconfirmation" runat="server" ClientIDMode="Static" Title="Confirmation" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel" />
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
