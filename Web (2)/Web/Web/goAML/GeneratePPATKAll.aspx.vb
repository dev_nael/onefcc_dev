﻿Imports System.Data
Imports System.IO
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL
Imports System.Data.SqlClient
Imports NawaDAL
Imports System.Xml
Imports Ionic.Zip
Imports Ionic.Zlib

Partial Class goAML_GeneratePPATKAll
    Inherits Parent
    Dim ModuleXmlAll As String = ""
    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("goAML_GeneratePPATKAll.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_GeneratePPATKAll.ObjModule") = value
        End Set
    End Property

    Public Property objEditListOfGeneratexmlEdit() As Data.DataRow
        Get
            Return Session("goAML_GeneratePPATKAll.objEditListOfGeneratexmlEdit")
        End Get
        Set(ByVal value As Data.DataRow)
            Session("goAML_GeneratePPATKAll.objEditListOfGeneratexmlEdit") = value
        End Set
    End Property



    Public Property ListgoamlGeneratexml() As Data.DataTable
        Get

            If Session("goAML_GeneratePPATKAll.ListgoamlGeneratexml") Is Nothing Then
                Dim objtable As DataTable = New DataTable

                Dim objpkid As New Data.DataColumn("IDPk", GetType(Integer))
                objpkid.AutoIncrement = True
                objpkid.AutoIncrementSeed = 1
                objpkid.AutoIncrementStep = 1

                objtable.Columns.Add(objpkid)

                objtable.Columns.Add(New DataColumn("transactiondate", GetType(Date)))
                objtable.Columns.Add(New DataColumn("jenislaporan", GetType(String)))
                objtable.Columns.Add(New DataColumn("lastupdatedate", GetType(Date)))
                Session("goAML_GeneratePPATKAll.ListgoamlGeneratexml") = objtable
            End If


            Return CType(Session("goAML_GeneratePPATKAll.ListgoamlGeneratexml"), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("goAML_GeneratePPATKAll.ListgoamlGeneratexml") = value
        End Set
    End Property

    'Public Property ListReportgoAMLbackup() As List(Of goAML_Report)
    '    Get
    '        Return Session("goAML_GeneratePPATKAll.ListReportgoAMLbackup")
    '    End Get
    '    Set(ByVal value As List(Of goAML_Report))
    '        Session("goAML_GeneratePPATKAll.ListReportgoAMLbackup") = value
    '    End Set
    'End Property


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            ModuleXmlAll = Request.Params("modul")
            If Not Net.X.IsAjaxRequest Then
                ClearSession()

                Try

                    Dim strmodule As String = Request.Params("ModuleID")
                    Dim intmodule As Integer = Common.DecryptQueryString(strmodule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                    Me.ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)

                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.view) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    FormPanelInput.Title = ObjModule.ModuleLabel


                    LoadStore()
                Catch ex As Exception
                    ErrorSignal.FromCurrentContext.Raise(ex)
                    Net.X.Msg.Alert("Error", ex.Message).Show()
                End Try
            End If



        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadStore()
        Cmb_Jenis_Laporan.PageSize = SystemParameterBLL.GetPageSize
        StoreView.PageSize = SystemParameterBLL.GetPageSize
        StoreReportJenis.Reload()
    End Sub

    Private Sub ClearSession()
        ObjModule = New NawaDAL.Module
        ListgoamlGeneratexml = Nothing
        objEditListOfGeneratexmlEdit = Nothing
        '  ListReportgoAML = New List(Of goAML_Report)
    End Sub

    Protected Sub ReportLastUpdateDate_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""

            'If query.Length > 0 Then
            '    strfilter = "  LastUpdateDate Like '%" & query & "%'"
            'End If
            If strfilter.Length > 0 Then
                strfilter += " AND CONVERT(DATE,transaction_date) = '" & txt_Report_Transaction_Date.Text & " ' AND report_code = '" & Cmb_Jenis_Laporan.Text & "' AND isValid = 1 "
            Else
                strfilter += " CONVERT(DATE,transaction_date) = '" & txt_Report_Transaction_Date.Text & " ' AND report_code = '" & Cmb_Jenis_Laporan.Text & "' AND isValid = 1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Report_LastUpdateDate", "report_code, LastUpdateDate", strfilter, "LastUpdateDate", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub JenisLaporan_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then
                strfilter = " Keterangan Like '%" & query & "%'"
            End If
            If ModuleXmlAll = "ifti" Then
                If strfilter.Length > 0 Then
                    strfilter += " and active=1 and kode in ('TKLOB','TKLIB') "
                Else
                    strfilter += "active=1 and kode in ('TKLOB','TKLIB') "
                End If
            End If
            If ModuleXmlAll = "ctr" Then
                If strfilter.Length > 0 Then
                    strfilter += " and active=1 and kode in ('LTKTM','LTKTK') "
                Else
                    strfilter += "active=1 and kode in ('LTKTM','LTKTK') "
                End If
            End If




            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Jenis_Laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnAddLastUpdate_click(sender As Object, e As DirectEventArgs)
        Try
            If CDate(txt_Report_Transaction_Date.Value) = DateTime.MinValue Then
                Throw New Exception("Please Fill Transaction Date")
            End If
            If Cmb_Jenis_Laporan.Text = "" Then
                Throw New Exception("Please Select One Jenis Laporan")
            End If

            WindowLastUpdateDate.Hidden = False
            cmblast_update_date.Text = ""
            loadStorePopUp()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnAddLastUpdateAll_click(sender As Object, e As DirectEventArgs)
        Try
            If CDate(txt_Report_Transaction_Date.Value) = DateTime.MinValue Then
                Throw New Exception("Tanggal Transaksi harus diisi!")
            End If
            If String.IsNullOrEmpty(Cmb_Jenis_Laporan.SelectedItem.Value) Then
                Throw New Exception("Jenis Laporan harus diisi!")
            End If

            Using objdb As New NawaDatadevEntities
                Dim dtTransactionDate As Date = CDate(txt_Report_Transaction_Date.Value)
                Dim strJenisLaporan As String = Cmb_Jenis_Laporan.SelectedItem.Value

                Dim objListLastUpdateDate = objdb.GoAML_Report.Where(Function(x) x.transaction_date = dtTransactionDate And x.report_code = strJenisLaporan).ToList
                If objListLastUpdateDate IsNot Nothing Then

                    For Each item In objListLastUpdateDate

                        If item.status = 4 Or item.status = 5 Then
                            Continue For
                        End If

                        Dim isDataExists As Boolean = False
                        For Each rowx In ListgoamlGeneratexml.Rows
                            If Convert.ToDateTime(rowx("transactiondate")).Date = Convert.ToDateTime(dtTransactionDate).Date And rowx("jenislaporan") = strJenisLaporan And Convert.ToDateTime(rowx("lastupdatedate")).Date = Convert.ToDateTime(item.LastUpdateDate).Date Then
                                isDataExists = True
                            End If
                        Next

                        If Not isDataExists Then
                            Dim objnewrow As Data.DataRow = ListgoamlGeneratexml.NewRow
                            objnewrow("transactiondate") = dtTransactionDate
                            objnewrow("jenislaporan") = strJenisLaporan
                            objnewrow("lastupdatedate") = item.LastUpdateDate
                            ListgoamlGeneratexml.Rows.Add(objnewrow)
                        End If
                    Next
                End If
            End Using

            BindGrid()


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Private Sub loadStorePopUp()
        cmblast_update_date.PageSize = SystemParameterBLL.GetPageSize
        StoreLastUpdateDateReport.Reload()
    End Sub
    Protected Sub StoreValidation_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intTotalRowCount As Long = 0

            Dim intLimit As Int16 = e.Limit
            Dim strfilter As String = Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)
            Dim strsort As String = ""

            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next

            Dim strFiltervalidationschema As String = ""

            For Each item As DataRow In ListgoamlGeneratexml.Rows
                strFiltervalidationschema &= " (TransactionDate='" & Convert.ToDateTime(item("transactiondate")).ToString("yyyy-MM-dd") & "' AND Jenis_Laporan='" & item("jenislaporan") & "' AND LastUpdateDate='" & Convert.ToDateTime(item("lastupdatedate")).ToString("yyyy-MM-dd") & "') or"
            Next
            If strFiltervalidationschema.Length > 0 Then
                strFiltervalidationschema = Left(strFiltervalidationschema, Len(strFiltervalidationschema) - 2)
            End If


            Dim strQuerySchema As String = "SELECT gavsxr.UnikReference,gavsxr.ErrorMessage FROM goAML_ValidateSchemaXSD_Report AS gavsxr   "

            If strFiltervalidationschema.Length > 0 Then
                strQuerySchema &= " where " & strFiltervalidationschema
            Else
                strQuerySchema &= " where 1=2"
            End If
            If strfilter.Length > 0 Then
                strQuerySchema &= " where " & strfilter
            End If
            Dim objListParam(3) As SqlParameter
            objListParam(0) = New SqlParameter
            objListParam(0).ParameterName = "@querydata"
            objListParam(0).SqlDbType = SqlDbType.VarChar
            objListParam(0).Value = strQuerySchema
            objListParam(1) = New SqlParameter
            objListParam(1).ParameterName = "@orderby"
            objListParam(1).SqlDbType = SqlDbType.VarChar
            objListParam(1).Value = strsort
            objListParam(2) = New SqlParameter
            objListParam(2).ParameterName = "@PageNum"
            objListParam(2).SqlDbType = SqlDbType.Int
            objListParam(2).Value = intStart
            objListParam(3) = New SqlParameter
            objListParam(3).ParameterName = "@PageSize"
            objListParam(3).SqlDbType = SqlDbType.Int
            objListParam(3).Value = intLimit
            'query hanya ambil 1 record untuk simpen schemanya saja(pagesize=1) biar enteng
            Dim DataPaging As DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuerySchema, strsort, intStart, intLimit, intTotalRowCount)
            e.Total = intTotalRowCount

            GridPanelValidation.GetStore.DataSource = DataPaging
            GridPanelValidation.GetStore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveLastUpdateDate_Click(sender As Object, e As DirectEventArgs)
        Try


            If String.IsNullOrEmpty(cmblast_update_date.SelectedItem.Value) Then
                Throw New ApplicationException(cmblast_update_date.FieldLabel + " is Required")
            End If


            If objEditListOfGeneratexmlEdit Is Nothing Then
                'add new

                'If ListgoamlGeneratexml.Select("transactiondate='" & Convert.ToDateTime(txt_Report_Transaction_Date.Value).ToString("dd-MMM-yyyy") & "' and jenislaporan='" & Cmb_Jenis_Laporan.Value & "' and lastupdatedate='" & Convert.ToDateTime(cmblast_update_date.SelectedItem.Value).ToString("dd-MMM-yyyy") & "'").Count > 0 Then
                '    Throw New Exception("List for transaction date '" & Convert.ToDateTime(txt_Report_Transaction_Date.Value).ToString("dd-MMM-yyyy") & "' and Jenis Laporan='" & Cmb_Jenis_Laporan.Value & "' and lastupdatedate='" & Convert.ToDateTime(cmblast_update_date.SelectedItem.Value).ToString("dd-MMM-yyyy") & "' already exist")
                'End If

                Dim dtTransactionDate As Date = CDate(txt_Report_Transaction_Date.Value)
                Dim strJenisLaporan As String = Cmb_Jenis_Laporan.SelectedItem.Value
                Dim dtLastUpdate As Date = CDate(cmblast_update_date.SelectedItem.Value)
                Dim isDataExists As Boolean = False

                For Each rowx In ListgoamlGeneratexml.Rows
                    If Convert.ToDateTime(rowx("transactiondate")).Date = Convert.ToDateTime(dtTransactionDate).Date And rowx("jenislaporan") = strJenisLaporan And Convert.ToDateTime(rowx("lastupdatedate")).Date = Convert.ToDateTime(dtLastUpdate).Date Then
                        isDataExists = True
                    End If
                Next

                If isDataExists Then
                    Throw New Exception("Data sudah ada di list!")
                End If

                Dim objnewrow As Data.DataRow = ListgoamlGeneratexml.NewRow
                objnewrow("transactiondate") = txt_Report_Transaction_Date.Value
                objnewrow("jenislaporan") = Cmb_Jenis_Laporan.Value
                objnewrow("lastupdatedate") = cmblast_update_date.SelectedItem.Value
                ListgoamlGeneratexml.Rows.Add(objnewrow)
            Else
                'edit

                If ListgoamlGeneratexml.Select("transactiondate='" & Convert.ToDateTime(txt_Report_Transaction_Date.Value).ToString("dd-MMM-yyyy") & "' and jenislaporan='" & Cmb_Jenis_Laporan.Value & "' and lastupdatedate='" & Convert.ToDateTime(cmblast_update_date.SelectedItem.Value).ToString("dd-MMM-yyyy") & "' and IDPk<> " & objEditListOfGeneratexmlEdit.Item("IDPk")).Count > 0 Then
                    Throw New Exception("List for transaction date '" & Convert.ToDateTime(txt_Report_Transaction_Date.Value).ToString("dd-MMM-yyyy") & "' and Jenis Laporan='" & Cmb_Jenis_Laporan.Value & "' and lastupdatedate='" & Convert.ToDateTime(cmblast_update_date.SelectedItem.Value).ToString("dd-MMM-yyyy") & "' already exist")
                End If


                objEditListOfGeneratexmlEdit("lastupdatedate") = cmblast_update_date.SelectedItem.Value

            End If

            BindGrid()
            WindowLastUpdateDate.Hidden = True
            objEditListOfGeneratexmlEdit = Nothing

            'If ListReportgoAML.Count = 0 Then
            '    ListReportgoAMLbackup = New List(Of goAML_Report)
            'End If

            'If String.IsNullOrEmpty(cmblast_update_date.SelectedItem.Value) Then
            '    Throw New ApplicationException(cmblast_update_date.FieldLabel + " Tidak boleh kosong")
            'End If

            'For Each item In ListReportgoAML
            '    If item.LastUpdateDate = cmblast_update_date.SelectedItem.Value Then
            '        Throw New ApplicationException(cmblast_update_date.SelectedItem.Value + " Sudah Ada")
            '    End If
            'Next

            'Dim report As New goAML_Report
            'If IDPKReport IsNot Nothing Or IDPKReport <> "" Then
            '    ListReportgoAML.Remove(ListReportgoAML.Where(Function(x) x.PK_Report_ID = IDPKReport).FirstOrDefault)
            'End If
            'If ListReportgoAML.Count = 0 Then
            '    report.PK_Report_ID = -1
            'ElseIf ListReportgoAML.Count >= 1 Then
            '    report.PK_Report_ID = ListReportgoAML.Min(Function(x) x.PK_Report_ID) - 1
            'End If

            'report.LastUpdateDate = cmblast_update_date.SelectedItem.Value
            'report.Transaction_Date = txt_Report_Transaction_Date.Text
            'report.Report_Code = Cmb_Jenis_Laporan.Text
            'ListReportgoAML.Add(report)
            'bindReportLastUpdateDate(StoreLastUpdateDateGridReport, ListReportgoAML)
            'WindowLastUpdateDate.Hidden = True
            'IDPKReport = Nothing

            'ListReportgoAMLbackup = ListReportgoAML
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub BtnCancelLastUpdateDate_Click(sender As Object, e As DirectEventArgs)
        WindowLastUpdateDate.Hidden = True
        objEditListOfGeneratexmlEdit = Nothing
    End Sub


    Protected Sub btnValidatingSchema_Click(sender As Object, e As EventArgs)
        Try

            If CDate(txt_Report_Transaction_Date.Value) = DateTime.MinValue Then
                Throw New Exception("Tanggal Transaksi harus diisi!")
            End If
            If String.IsNullOrEmpty(Cmb_Jenis_Laporan.SelectedItem.Value) Then
                Throw New Exception("Jenis Laporan harus diisi!")
            End If
            If ListgoamlGeneratexml.Rows.Count = 0 Then
                Throw New Exception("List of Last Update Date harus diisi!")
            End If

            NawaDevBLL.GeneratePPATKBLL.ValidateSchemaALL(ListgoamlGeneratexml)

            StoreView.Reload()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnSave_DirectClick(sender As Object, e As EventArgs)
        Try

            If CDate(txt_Report_Transaction_Date.Value) = DateTime.MinValue Then
                Throw New Exception("Tanggal Transaksi harus diisi!")
            End If
            If String.IsNullOrEmpty(Cmb_Jenis_Laporan.SelectedItem.Value) Then
                Throw New Exception("Jenis Laporan harus diisi!")
            End If
            If ListgoamlGeneratexml.Rows.Count = 0 Then
                Throw New Exception("List of Last Update Date harus diisi!")
            End If

            Dim FileReturn As String = ""

            '' Added by Felix 27 Aug 2020
            Dim isHarusValid As String = getParameterGlobalByPK(12) '' Download harus Valid XSD
            '' End of Felix
            Dim path As String = Server.MapPath("~\goaml\FolderExport\")
            '  Dim path As String = SystemParameterBLL.GetSystemParameterByPk(9017).SettingValue

            Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
            If Not Directory.Exists(Dirpath) Then
                Directory.CreateDirectory(Dirpath)
            End If

            Dim strquery As String = ""
            For Each item As DataRow In ListgoamlGeneratexml.Rows
                Dim datetransaction As Date = Convert.ToDateTime(item("transactiondate")).ToString("yyyy-MM-dd")
                Dim jenislaporan As String = item("jenislaporan")
                Dim datelastupdate As Date = Convert.ToDateTime(item("lastupdatedate")).ToString("yyyy-MM-dd")
                strquery += " (Transaction_Date='" & datetransaction.ToString("yyyy-MM-dd") & "' AND Jenis_Laporan='" & jenislaporan & "' AND Last_Update_Date='" & datelastupdate.ToString("yyyy-MM-dd") & "' ) OR "
            Next

            If strquery.Length > 0 Then strquery = Left(strquery, Len(strquery) - 4)

            Dim inttotalInvalid As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select count(1) from goAML_Generate_XML where ISNULL(ValidateSchemaResultValid,0)=0 and (" & strquery & ")", Nothing)
            If inttotalInvalid > 0 And isHarusValid = "1" Then
                Throw New Exception("Please Validate Schema or Fix Schema Validation Error first before generate xml.")
            End If

            'FileReturn = GeneratePPATKBLL.GenerateReportXMLALLNew(ListgoamlGeneratexml, Dirpath)
            '6 Jan 2021 - Changed to function local karena NawaDevBLL masih error di laptop Yudis
            Dim dtTransaction As Date = CDate(txt_Report_Transaction_Date.Value)
            Dim strJenisLaporan As String = Cmb_Jenis_Laporan.SelectedItem.Value
            FileReturn = GenerateReportXMLALLNew2(dtTransaction, strJenisLaporan, ListgoamlGeneratexml, Dirpath)

            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(FileReturn))
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "application/zip"
            Response.BinaryWrite(File.ReadAllBytes(FileReturn))
            Response.End()

        Catch ex As Exception
            Net.X.Msg.Alert("Information", ex.Message).Show()
        End Try
    End Sub


    Protected Sub BtnSave_DirectClick_20200106(sender As Object, e As EventArgs)

        If ListgoamlGeneratexml.Rows.Count > 0 Then
            Dim FileReturn As String = ""
            Try

                '' Added by Felix 27 Aug 2020
                Dim isHarusValid As String = getParameterGlobalByPK(12) '' Download harus Valid XSD
                '' End of Felix
                Dim path As String = Server.MapPath("~\goaml\FolderExport\")
                '  Dim path As String = SystemParameterBLL.GetSystemParameterByPk(9017).SettingValue

                Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
                If Not Directory.Exists(Dirpath) Then
                    Directory.CreateDirectory(Dirpath)
                End If
                Dim strquery As String = ""
                For Each item As DataRow In ListgoamlGeneratexml.Rows
                    Dim datetransaction As Date = Convert.ToDateTime(item("transactiondate")).ToString("yyyy-MM-dd")
                    Dim jenislaporan As String = item("jenislaporan")
                    Dim datelastupdate As Date = Convert.ToDateTime(item("lastupdatedate")).ToString("yyyy-MM-dd")
                    strquery += " (Transaction_Date='" & datetransaction.ToString("yyyy-MM-dd") & "' AND Jenis_Laporan='" & jenislaporan & "' AND Last_Update_Date='" & datelastupdate.ToString("yyyy-MM-dd") & "' ) OR "
                Next

                If strquery.Length > 0 Then strquery = Left(strquery, Len(strquery) - 4)

                Dim inttotalInvalid As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select count(1) from goAML_Generate_XML where ISNULL(ValidateSchemaResultValid,0)=0 and (" & strquery & ")", Nothing)
                If inttotalInvalid > 0 And isHarusValid = "1" Then
                    Throw New Exception("Please Validate Schema or Fix Schema Validation Error first before generate xml.")
                End If

                FileReturn = GeneratePPATKBLL.GenerateReportXMLALLNew(ListgoamlGeneratexml, Dirpath)


                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(FileReturn))
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "application/zip"
                Response.BinaryWrite(File.ReadAllBytes(FileReturn))
                Response.End()

                'Dim path As String = SystemParameterBLL.GetSystemParameterByPk(9017).SettingValue
                'Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
                'If Not Directory.Exists(Dirpath) Then
                '    Directory.CreateDirectory(Dirpath)
                'End If

                'Dim connection As String = ConfigurationManager.AppSettings("ConnectionString").ToString
                'Dim filenamexml As String = ConfigurationManager.AppSettings("FileNameXMLPPATK").ToString
                'Dim filenamezip As String = ConfigurationManager.AppSettings("FileNameZipPPATK").ToString
                'Dim formatdate As String = ConfigurationManager.AppSettings("FormatDateFileNamePPATK").ToString

                'If ListReportgoAMLbackup Is Nothing Then
                '    FileReturn = GeneratePPATKBLL.GenerateReportXMLAll(txt_Report_Transaction_Date.Text, Cmb_Jenis_Laporan.Text, Common.SessionCurrentUser.UserID, Dirpath, connection, filenamexml, filenamezip, formatdate, ListReportgoAML)
                'Else
                '    FileReturn = GeneratePPATKBLL.GenerateReportXMLAll(txt_Report_Transaction_Date.Text, Cmb_Jenis_Laporan.Text, Common.SessionCurrentUser.UserID, Dirpath, connection, filenamexml, filenamezip, formatdate, ListReportgoAMLbackup)
                'End If

                'If FileReturn Is Nothing Then
                '    Throw New Exception("Not Exists Report")
                'End If

                'Response.Clear()
                'Response.ClearHeaders()
                'Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(FileReturn))
                'Response.Charset = ""
                'Response.AddHeader("cache-control", "max-age=0")
                'Me.EnableViewState = False
                'Response.ContentType = "application/zip"
                'Response.BinaryWrite(File.ReadAllBytes(FileReturn))
                'Response.End()
            Catch ex As Exception
                ErrorSignal.FromCurrentContext.Raise(ex)
                Net.X.Msg.Alert("Error", ex.Message).Show()
            End Try
        Else
            Net.X.Msg.Alert("Information", "No report Found").Show()
        End If
    End Sub

    'Protected Sub btnCancelUpload_Click(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim Moduleid As String = Request.Params("ModuleID")
    '        Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
    '    Catch ex As Exception
    '        ErrorSignal.FromCurrentContext.Raise(ex)
    '        Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Sub BindGrid()
        StoreLastUpdateDateGridReport.DataSource = ListgoamlGeneratexml
        StoreLastUpdateDateGridReport.DataBind()
    End Sub
    Protected Sub GridcommandReportUpdateDate(sender As Object, e As DirectEventArgs)
        Try


            Dim ID As String = e.ExtraParams(0).Value
            Dim objoperation As String = e.ExtraParams(1).Value


            Select Case objoperation
                Case "Edit"
                    Dim objedit() As DataRow = ListgoamlGeneratexml.Select("IDPk =" & ID)
                    If objedit.Count = 1 Then
                        objEditListOfGeneratexmlEdit = objedit(0)
                        WindowLastUpdateDate.Show()
                        cmblast_update_date.Value = Convert.ToDateTime(objEditListOfGeneratexmlEdit.Item("lastupdatedate")).ToString("yyyy-MM-ddTHH:mm:ss")
                    End If

                Case "Delete"
                    Dim objdelete() As DataRow = ListgoamlGeneratexml.Select("IDPk =" & ID)
                    If objdelete.Count = 1 Then

                        ListgoamlGeneratexml.Rows.Remove(objdelete(0))
                        BindGrid()
                    End If
                Case Else

            End Select

            'If e.ExtraParams(1).Value = "Delete" Then
            '    If ListReportgoAML.Count = 0 Then
            '        ListReportgoAML = ListReportgoAMLbackup
            '    End If
            '    ListReportgoAML.Remove(ListReportgoAML.Where(Function(x) x.LastUpdateDate = ID).FirstOrDefault)
            '    ListReportgoAMLbackup = ListReportgoAML
            '    bindReportLastUpdateDate(StoreLastUpdateDateGridReport, ListReportgoAML)
            'ElseIf e.ExtraParams(1).Value = "Edit" Then
            '    editReportLAstUpdateDate(ID)
            '    IDPKReport = ID
            'End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub editReportLAstUpdateDate(iD As String)
        Try
            'cmblast_update_date.Clear()

            'WindowLastUpdateDate.Hidden = False
            'Dim report As goAML_Report
            'If ListReportgoAML.Count = 0 Then
            '    ListReportgoAML = ListReportgoAMLbackup
            'End If
            'report = ListReportgoAML.Where(Function(x) x.LastUpdateDate = iD).FirstOrDefault
            'ListReportgoAMLbackup = ListReportgoAML
            'cmblast_update_date.SetValue(report.LastUpdateDate)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

    End Sub
    '' added By Felix 27 Aug 2020
    Function getParameterGlobalByPK(id As Integer) As String
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim paramValues As String = ""
            Dim paramGlobal As NawaDevDAL.goAML_Ref_ReportGlobalParameter = objdb.goAML_Ref_ReportGlobalParameter.Where(Function(x) x.PK_GlobalReportParameter_ID = id).FirstOrDefault

            If Not paramGlobal Is Nothing Then
                paramValues = paramGlobal.ParameterValue
            End If
            Return paramValues
        End Using
    End Function
    '' added By Felix 27 Aug 2020

    'added by Adi D 6 Jan 2021 penambahan Last Update Date di nama .zip
    Public Shared Function GenerateReportXMLALLNew2(dtTransaction As Date, strJenisLaporan As String, ListgoamlGeneratexml As DataTable, strdirpath As String) As String

        Dim lstFile As New List(Of String)
        Dim lstFileZip As New List(Of String)

        Dim intjmlFiledalam1zip As Integer = 1000
        intjmlFiledalam1zip = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT garrgp.ParameterValue FROM goAML_Ref_ReportGlobalParameter AS garrgp WHERE garrgp.PK_GlobalReportParameter_ID=11", Nothing)

        For Each item As DataRow In ListgoamlGeneratexml.Rows
            Dim FileReturn As String = ""
            Dim datetransaction As Date = Convert.ToDateTime(item("transactiondate")).ToString("yyyy-MM-dd")
            Dim jenislaporan As String = item("jenislaporan")
            Dim datelastupdate As Date = Convert.ToDateTime(item("lastupdatedate")).ToString("yyyy-MM-dd")
            FileReturn = GenerateReportXMLNew2(datetransaction, jenislaporan, datelastupdate, strdirpath)
            lstFileZip.Add(FileReturn)
        Next

        Dim strTransactionDate As String = Convert.ToDateTime(dtTransaction).ToString("yyyyMMdd")
        Dim strTanggalGenerate As String = Convert.ToDateTime(Now()).ToString("yyyyMMddHHmmtt") '' Added By Felix 28 Dec 2020

        Dim fileNamezip As String = "GenerateXMLAll-" & strJenisLaporan & "-" & strTransactionDate & "-" & strTanggalGenerate & ".zip"
        If lstFileZip.Count > 1 Then
            Using zip As New ZipFile()
                zip.CompressionLevel = CompressionLevel.Level9
                zip.AddFiles(lstFileZip, "")
                zip.Save(fileNamezip)

            End Using
        End If
        If lstFileZip.Count = 1 Then
            Return lstFileZip(0)
        ElseIf lstFileZip.Count > 1 Then
            Return fileNamezip
        End If
    End Function

    Shared Function GenerateReportXMLNew2(dtanggalTransaksi As Date, strJenisLaporan As String, LastUpdateDate As Date, strdirpath As String) As String

        Dim retzipfile As String = ""

        ''update tanggallaporan jadi tanggal hari ini
        NawaDAL.SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "UPDATE GoAML_Report SET submission_date = FORMAT(GETDATE(),'yyyyMMdd'), Status = 2 WHERE DATEDIFF(DAY, transaction_date,'" & dtanggalTransaksi.ToString("yyyy-MM-dd") & "')=0 AND report_code='" & strJenisLaporan & "' AND DATEDIFF(DAY, LastUpdateDate,'" & LastUpdateDate & "')=0 AND isValid=1 and [status] <> 4", Nothing)

        SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "UPDATE goAML_Generate_XML SET Last_Generate_Date = GETDATE(), Status = 'Waiting For Upload Reference No' WHERE Transaction_Date='" & dtanggalTransaksi.ToString("yyyy-MM-dd") & "' AND Jenis_Laporan='" & strJenisLaporan & "' AND Last_Update_Date='" & LastUpdateDate & "'", Nothing)


        ''filter based on tanggaltransaksi,jenislaporan dan isvalid=1
        Dim dtreport As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT gar.PK_Report_ID,gar.submission_date,gar.report_code,gar.unikReference,gar.transaction_date,isnull(b.ReportType,'') ReportType FROM GoAML_Report AS gar LEFT JOIN goAML_Ref_Jenis_Laporan b ON gar.Report_Code=b.Kode WHERE  DATEDIFF(DAY, gar.transaction_date,'" & dtanggalTransaksi.ToString("yyyy-MM-dd") & "')=0 AND gar.isValid=1 and report_code='" & strJenisLaporan & "' AND DATEDIFF(DAY, gar.LastUpdateDate,'" & LastUpdateDate.ToString("yyyy-MM-dd") & "')=0 and [status] not in (4,5)", Nothing)



        Dim lstFile As New List(Of String)
        Dim lstFileZip As New List(Of String)

        Dim intjmlFiledalam1zip As Integer = 1000
        intjmlFiledalam1zip = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT garrgp.ParameterValue FROM goAML_Ref_ReportGlobalParameter AS garrgp WHERE garrgp.PK_GlobalReportParameter_ID=11", Nothing)

        Dim intcounter As Integer = 0

        Dim listAttachment As New List(Of String)
        For Each item As DataRow In dtreport.Rows

            Dim pkreportid As Long = item(0)
            Dim strtanggallaporan As String = Convert.ToDateTime(item(1)).ToString("yyyyMMdd")
            Dim strreportcode As String = item(2)
            Dim strunikreference As String = item(3)
            Dim strtanggaltransaksi As String = Convert.ToDateTime(item(4)).ToString("yyyyMMdd")
            Dim strreporttype As String = item(5)
            Dim strTanggalGenerate As String = Convert.ToDateTime(Now()).ToString("yyyyMMddHHmmtt") '' Added By Felix 28 Dec 2020
            Dim strTanggalLastUpdate As String = Convert.ToDateTime(LastUpdateDate).ToString("yyyyMMdd")

            Dim objParam(1) As SqlParameter
            objParam(0) = New SqlParameter
            objParam(0).ParameterName = "@PK"
            objParam(0).Value = pkreportid

            objParam(1) = New SqlParameter
            objParam(1).ParameterName = "@session"
            objParam(1).Value = NawaBLL.Common.SessionCurrentUser.UserID

            Dim incrementRow As Int32 ''edited by Felix 16 Nov 2020
            incrementRow = 0
            'Dim strresult As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_generate_XMLNew", objParam)
            Dim xmlDataTable As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_generate_XMLNew", objParam) ''edited by Felix 16 Nov 2020

            For Each itemXML As DataRow In xmlDataTable.Rows ''added by Felix 16 Nov 2020
                Dim strresult As String = itemXML(0) ''added by Felix 16 Nov 2020
                Dim filexml As String ''added by Felix 16 Nov 2020

                If xmlDataTable.Rows.Count > 1 Then
                    incrementRow = incrementRow + 1
                    'filexml = strdirpath & strtanggallaporan & "-" & strtanggaltransaksi & "-" & strreportcode & "-" & strunikreference.Replace("/", "_") & "-[" & pkreportid & "]-" & incrementRow & ".xml"
                    filexml = strdirpath & strtanggallaporan & "-" & strtanggaltransaksi & "-" & strTanggalGenerate & "-" & strreportcode & "-" & strunikreference.Replace("/", "_") & "-[" & pkreportid & "]-" & incrementRow & ".xml" '' Edited by FElix 28 Dec 2020, tambah tanggal generate
                Else
                    'filexml = strdirpath & strtanggallaporan & "-" & strtanggaltransaksi & "-" & strreportcode & "-" & strunikreference.Replace("/", "_") & "-[" & pkreportid & "].xml"
                    filexml = strdirpath & strtanggallaporan & "-" & strtanggaltransaksi & "-" & strTanggalGenerate & "-" & strreportcode & "-" & strunikreference.Replace("/", "_") & "-[" & pkreportid & "].xml" '' Edited by FElix 28 Dec 2020, tambah tanggal generate
                End If


                If strresult = "" Then
                    Throw New Exception("Data Report Empty for pk_goaml_report_Id =" & pkreportid)
                End If

                ''delete kalau sudah pernah ada
                If IO.File.Exists(filexml) Then
                    IO.File.Delete(filexml)
                End If


                Dim objdoc As New XmlDocument
                objdoc.LoadXml(strresult)
                objdoc.PreserveWhitespace = False
                objdoc.Save(filexml)

                If strreporttype = "LTKM/STR" Then

                    Dim dtattachment As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT a.[File_Name],a.File_Doc FROM  goAML_ODM_Generate_STR_SAR_Attachment AS a INNER JOIN goAML_ODM_Generate_STR_SAR AS b ON  a.Fk_goAML_ODM_Generate_STR_SAR = b.PK_goAML_ODM_Generate_STR_SAR WHERE b.Fk_Report_ID=" & pkreportid, Nothing)

                    If dtattachment.Rows.Count > 0 Then
                        For Each itemattachment As Data.DataRow In dtattachment.Rows
                            Dim fileattachmentname As String = strdirpath & strtanggallaporan & "-" & strtanggaltransaksi & "-" & strreportcode & "-" & strunikreference & "-[" & pkreportid & "]-" & itemattachment(0)
                            IO.File.WriteAllBytes(fileattachmentname, itemattachment(1))
                            listAttachment.Add(fileattachmentname)

                        Next
                        lstFile.AddRange(listAttachment)
                        lstFile.Add(filexml)
                    Else
                        'gak ada attachment,
                        lstFile.Add(filexml)

                    End If
                Else
                    lstFile.Add(filexml)
                End If

                'lstFile.Add(filexml)
            Next ''added by Felix 16 Nov 2020

            If lstFile.Count = intjmlFiledalam1zip And lstFile.Count > 0 Then
                Using zip As New ZipFile()
                    intcounter += 1
                    Dim fileNamezip As String = strdirpath & strtanggallaporan & "-" & strtanggaltransaksi & "-" & Right("00" & intcounter, 3) & "-" & strreportcode & ".zip"

                    If IO.File.Exists(fileNamezip) Then
                        IO.File.Delete(fileNamezip)
                    End If
                    zip.CompressionLevel = CompressionLevel.Level9
                    zip.AddFiles(lstFile, "")

                    zip.Save(fileNamezip)
                    lstFileZip.Add(fileNamezip)
                End Using

                'buang karena sudah di zip
                For Each Item1 As String In lstFile
                    IO.File.Delete(Item1)
                Next



                lstFile.Clear()
            End If

        Next
        ' ini kalau masih ada sisa
        If lstFile.Count > 0 Then

            Using zip As New ZipFile()
                intcounter += 1
                Dim fileNamezip As String = strdirpath & Now.ToString("yyyyMMdd") & "-" & dtanggalTransaksi.ToString("yyyyMMdd") & "-" & LastUpdateDate.ToString("yyyyMMdd") & "-" & Right("00" & intcounter, 3) & "-" & strJenisLaporan & ".zip"

                If IO.File.Exists(fileNamezip) Then
                    IO.File.Delete(fileNamezip)
                End If
                zip.CompressionLevel = CompressionLevel.Level9
                zip.AddFiles(lstFile, "")

                zip.Save(fileNamezip)
                lstFileZip.Add(fileNamezip)

            End Using

            'buang karena sudah di zip
            For Each Item1 As String In lstFile
                IO.File.Delete(Item1)
            Next
            lstFile.Clear()

        End If

        'Update BSIM-Ryn-11Nov2020
        'ganti kondisi lstFileZip yang count > 1 menjadi > 0 
        '------------------------
        'update by hendra 25 nov 2020
        'diubah balik  jadi >1, tujuan nya kalau list file zipnya ada >1 baru di zip lagi dengan nama ALL
        'jadi harusnya kalau cuma 1 gak akan masuk if bawah ini
        '------------------------
        If lstFileZip.Count > 1 Then
            Using zip As New ZipFile()

                Dim dfiledatename As String
                dfiledatename = Now.ToString("yyyyMMdd")


                Dim fileNamezip As String = strdirpath & dfiledatename & "-" & dtanggalTransaksi.ToString("yyyyMMdd") & "-All-" & strJenisLaporan & ".zip"
                If IO.File.Exists(fileNamezip) Then
                    IO.File.Delete(fileNamezip)
                End If
                zip.CompressionLevel = CompressionLevel.Level9
                zip.AddFiles(lstFileZip, "")
                zip.Save(fileNamezip)

                For Each Item1 As String In lstFileZip
                    IO.File.Delete(Item1)
                Next

                retzipfile = fileNamezip
            End Using
        ElseIf lstFileZip.Count = 1 Then

            retzipfile = lstFileZip.Item(0).ToString

        Else

            'Update BSIM-Ryn-09Nov2020 
            'Remark bagian array karena akan error sql diganti dengan error message dari query dtreport terdapat filter status not in 4 dan 5

            Throw New Exception("Report status Waiting for Approval or Need to Correction")
        End If
        Return retzipfile

    End Function
    'end of added by Adi D 6 Jan 2021 penambahan Last Update Date di nama .zip

End Class