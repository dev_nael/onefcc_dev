﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="ReportSTRApprovalDetail.aspx.vb" Inherits="ReportSTRApprovalDetail" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
               
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <ext:FormPanel ID="FormPanelInput" runat="server" ButtonAlign="Center" Title="" BodyStyle="padding:20px" AutoScroll="true">
        <Items>
                <ext:Window ID="windowDetailTransactionBiparty" runat="server" Hidden="true" Modal="true" Title="Detail Transaction Biparty"  MinWidth="800" Height="500" Layout="AnchorLayout" BodyStyle="padding:20px" AutoScroll="true" Maximizable="true">
                    <Items>
                        <ext:DisplayField runat="server" ID="textCaseID" FieldLabel="Case ID" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtDate_Transaction" FieldLabel="Date Transaction" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtInternal_Ref_Number" FieldLabel="Ref Number" AnchorHorizontal="100%" ></ext:DisplayField>
                       
                     <ext:DisplayField runat="server" ID="txtAmount_Local" FieldLabel="Amount Local" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtFK_Source_Data_ID" FieldLabel="Source Data" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtTransmode_Code" FieldLabel="Transmode Code" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtTransmode_Comment" FieldLabel="Transmode Comment" AnchorHorizontal="100%" ></ext:DisplayField>
                         <ext:DisplayField runat="server" ID="txtTransaction_Remark" FieldLabel="Transaction Remark" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtTransactionNumber" FieldLabel="Transaction Number" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtTransaction_Location" FieldLabel="Transaction Location" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtTeller" FieldLabel="Teller" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtAuthorized" FieldLabel="Authorized" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtLate_Deposit" FieldLabel="Late Deposit" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtDate_Posting" FieldLabel="Date Posting" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtValue_Date" FieldLabel="Value Date" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtisMyClient_FROM" FieldLabel="isMyClient FROM" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtFrom_Funds_Code" FieldLabel="From Funds Code" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtFrom_Funds_Comment" FieldLabel="From Funds Comment" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtFrom_Foreign_Currency_Code" FieldLabel="From Foreign Currency Code" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtFrom_Foreign_Currency_Amount" FieldLabel="From Foreign Currency Amount" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtFrom_Foreign_Currency_Exchange_Rate" FieldLabel="From Foreign Currency Exchange Rate" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtFrom_Country" FieldLabel="From Country" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtisMyClient_TO" FieldLabel="isMyClient TO" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtTo_Funds_Code" FieldLabel="To Funds Code" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtTo_Funds_Comment" FieldLabel="To Funds Comment" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtTo_Foreign_Currency_Code" FieldLabel="To Foreign Currency Code" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtTo_Foreign_Currency_Amount" FieldLabel="To Foreign Currency Amount" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtTo_Foreign_Currency_Exchange_Rate" FieldLabel="To Foreign Currency Exchange Rate" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtTo_Country" FieldLabel="To Country" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtComments" FieldLabel="Comments" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtisSwift" FieldLabel="isSwift" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtFROM_Account_No" FieldLabel="From Account No" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtFROM_CIFNO" FieldLabel="From Cif No" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtFrom_WIC_NO" FieldLabel="From Wic No" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtTO_ACCOUNT_NO" FieldLabel="To Account No" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtTO_CIF_NO" FieldLabel="To Cif No" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtTO_WIC_NO" FieldLabel="To Wic No" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtIsUsedConductor" FieldLabel="Is Used Conductor ?" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtCondutor_ID" FieldLabel="Condutor ID" AnchorHorizontal="100%" ></ext:DisplayField>
                           <%-- HSBC 20201117 Fachmi -- penambahan field swift code lawan --%>
                            <ext:DisplayField runat="server" ID="txtSwiftCodeLawan" FieldLabel="Swift Code Lawan" AnchorHorizontal="100%" ></ext:DisplayField>
                    </Items>
                   <Listeners>
                        <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.6});" />
                        <Resize Handler="#{windowDetailTransactionBiparty}.center()" />
                    </Listeners>
                </ext:Window>
            
                <ext:Window ID="windowDetailTransactionMultiparty" runat="server" Hidden="true" Modal="true" Title="Detail Transaction Multiparty"  MinWidth="800" Height="500" Layout="AnchorLayout" BodyStyle="padding:20px" AutoScroll="true" Maximizable="true">
                    <Items>
                        <ext:DisplayField runat="server" ID="MultitxtCaseID" FieldLabel="Case ID" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiDate_Transaction" FieldLabel="Date Transaction" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiInternal_Ref_Number" FieldLabel="Internal Ref Number" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiAmount_Local" FieldLabel="Amount Local" AnchorHorizontal="100%" ></ext:DisplayField>
					    <ext:DisplayField runat="server" ID="txtMultiFK_Source_Data_ID" FieldLabel="Source Data" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiTransmode_Code" FieldLabel="Transmode Code" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiTransmode_Comment" FieldLabel="Transmode Comment" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiTransaction_Remark" FieldLabel="Transaction Remark" AnchorHorizontal="100%" ></ext:DisplayField>                        
                        <ext:DisplayField runat="server" ID="txtMultiTransactionNumber" FieldLabel="Transaction Number" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiTransaction_Location" FieldLabel="Transaction Location" AnchorHorizontal="100%" ></ext:DisplayField>

                        <ext:DisplayField runat="server" ID="txtMultiTeller" FieldLabel="Teller" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiAuthorized" FieldLabel="Authorized" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiLate_Deposit" FieldLabel="Late Deposit" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiDate_Posting" FieldLabel="Date Posting" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiisMyClient" FieldLabel="isMyClient FROM" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiFunds_Code" FieldLabel="From Funds Code" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiFunds_Comment" FieldLabel="From Funds Comment" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiForeign_Currency_Code" FieldLabel="From Foreign Currency Code" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiForeign_Currency_Amount" FieldLabel="From Foreign Currency Amount" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiForeign_Currency_Exchange_Rate" FieldLabel="From Foreign Currency Exchange Rate" AnchorHorizontal="100%" ></ext:DisplayField>
                        <ext:DisplayField runat="server" ID="txtMultiCountry" FieldLabel="From Country" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtMultiComments" FieldLabel="Comments" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtMultiisSwift" FieldLabel="isSwift" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtMultiAccount_No" FieldLabel="Account No" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtMultiCIFNO" FieldLabel="Cif No" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtMultiWIC_NO" FieldLabel="Wic No" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtMultiIsUsedConductor" FieldLabel="Is Used Conductor?" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="txtMultiCondutor_ID" FieldLabel="Condutor ID" AnchorHorizontal="100%" ></ext:DisplayField>
                    </Items>
                   <Listeners>
                        <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.6});" />
                        <Resize Handler="#{windowDetailTransactionMultiparty}.center()" />
                    </Listeners>
                </ext:Window>

                <ext:FormPanel ID="FormPanel2" runat="server" ButtonAlign="Center" Title="" BodyStyle="padding:20px" AutoScroll="true">
                 <Items>
                        	<ext:DisplayField runat="server" ID="ModuleName" FieldLabel="Module Name" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="ModuleKey" FieldLabel="Module Key" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="Action" FieldLabel="Action" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="CreatedBy" FieldLabel="Created By" AnchorHorizontal="100%" ></ext:DisplayField>
							<ext:DisplayField runat="server" ID="CreatedDate" FieldLabel="Created Date" AnchorHorizontal="100%" ></ext:DisplayField>
						
                 </Items>
                </ext:FormPanel>


                <ext:FormPanel ID="FormPanel3" runat="server" ButtonAlign="Center" Title="" BodyStyle="padding:20px" AutoScroll="true">
                    <items>
                    <ext:FormPanel ID="FormPanel4" runat="server" ButtonAlign="Center" Title="New Value"/>
                        </items>
                </ext:FormPanel>

                    <ext:FormPanel ID="FormPanel1" runat="server" ButtonAlign="Center" Title="" BodyStyle="padding:20px" AutoScroll="true">
                          <Items>    
                            <ext:Panel runat="server" ID="PanelValid" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaction Valid" BodyStyle="padding:10px" Margin="5" Collapsible="true">
                                <Items>
                                    <ext:Panel runat="server" ID="PanelReport" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Report" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelReport" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreReportData" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="Case_ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_ID" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Tanggal_Laporan" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Jenis_laporan" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Alasan" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Tindakan_Pelapor" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="isValid" Type="String"></ext:ModelField>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn01" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column001" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column002" runat="server" DataIndex="Tanggal_Laporan" Text="Tanggal Laporan" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column003" runat="server" DataIndex="Jenis_laporan" Text="Jenis laporan" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column004" runat="server" DataIndex="Ref_Num" Text="No Ref Laporan" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column005" runat="server" DataIndex="Alasan" Text="Alasan" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column006" runat="server" DataIndex="Tindakan_Pelapor" Text="Tindakan Pelapor" Flex="1"></ext:Column>
                                                        <%--<ext:Column ID="Column007" runat="server" DataIndex="Status" Text="Status" Flex="1"></ext:Column>--%>
                                                    </Columns>
                                                </ColumnModel>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                    </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>

                                    <ext:Panel runat="server" ID="PanelIndikatorLaporan" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Indikator Laporan" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelIndikatorLaporan" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreIndikatorLaporanData" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="PK_Indikator_Laporan_STR_ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_Indikator_Laporan_STR_ID" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="FK_Indikator_Laporan" Type="String"></ext:ModelField> 
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column8" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:Column ID="DateColumn1" runat="server" DataIndex="FK_Indikator_Laporan" Text="Indikator Laporan" Flex="1"></ext:Column>
                                                    </Columns>
                                                </ColumnModel>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                    </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>

                                    <ext:Panel runat="server" ID="PanelTransactionBiParty" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaksi BiParty" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelTransactionBiParty" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreTransactionBiParty" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="TransactionNumber">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_Transaction_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="FK_Report_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="FK_Jenis_Laporan_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isValid" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="TransactionNumber" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Internal_Ref_Number" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transaction_Remark" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Teller" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Authorized" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Late_Deposit" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Date_Posting" Type="Date"></ext:ModelField> 
                                                                    <ext:ModelField Name="Value_Date" Type="Date"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transmode_Comment" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Amount_Local" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Transaction_Type" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="From_Funds_Comment" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="From_Foreign_Currency_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="From_Foreign_Currency_Amount" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="From_Foreign_Currency_Exchange_Rate" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Person_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Sender_From_Information" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="From_Country" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isMyClient_TO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="To_Funds_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="To_Funds_Comment" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="To_Foreign_Currency_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="To_Foreign_Currency_Amount" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="To_Foreign_Currency_Exchange_Rate" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Sender_To_Information" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="To_Country" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Generate_Date" Type="Date"></ext:ModelField> 
                                                                    <ext:ModelField Name="isSwift" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Source_Data_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Report_Type_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Value_Report" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Jenis_Transaksi" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FROM_Account_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FROM_CIFNO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="From_WIC_NO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="TO_ACCOUNT_NO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="TO_CIF_NO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="TO_WIC_NO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="IsUsedConductor" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Condutor_ID" Type="String"></ext:ModelField> 

                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column7" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column40" runat="server" DataIndex="Date_Transaction" Text="Date Transaction" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column41" runat="server" DataIndex="Internal_Ref_Number" Text="Ref Num" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column42" runat="server" DataIndex="Amount_Local" Text="Amount Local" Flex="1"></ext:Column>
<%--                                                        <ext:Column ID="Column43" runat="server" DataIndex="FK_Source_Data_ID" Text="Data Source" Flex="1"></ext:Column>--%>
                                                        <ext:Column ID="Column44" runat="server" DataIndex="Transmode_Code" Text="Transmode Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column47" runat="server" DataIndex="TransactionNumber" Text="Transaction Number" Flex="1"></ext:Column>

<%--                                                        <ext:Column ID="Column45" runat="server" DataIndex="Transmode_Comment" Text="Transmode Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column46" runat="server" DataIndex="Transaction_Remark" Text="Transaction Remark" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column48" runat="server" DataIndex="Transaction_Location" Text="Transaction Location" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column49" runat="server" DataIndex="Teller" Text="Teller" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column50" runat="server" DataIndex="Authorized" Text="Authorized" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column51" runat="server" DataIndex="Late_Deposit" Text="Late Deposit Report Party" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column52" runat="server" DataIndex="Date_Posting" Text="Date Posting" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column53" runat="server" DataIndex="Value_Date" Text="Value Date" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column54" runat="server" DataIndex="isMyClient_FROM" Text="IsMyClient_FORM" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column55" runat="server" DataIndex="From_Funds_Code" Text="Form Funds Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column56" runat="server" DataIndex="From_Funds_Comment" Text="From Funds Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column57" runat="server" DataIndex="From_Foreign_Currency_Code" Text="From Foreign Currency Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column58" runat="server" DataIndex="From_Foreign_Currency_Amount" Text="From Foreign Currency Amount" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column59" runat="server" DataIndex="From_Foreign_Currency_Exchange_Rate" Text="From Foreign Currency Exchange Rate" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column60" runat="server" DataIndex="From_Country" Text="From Country" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column61" runat="server" DataIndex="To_Funds_Code" Text="To Funds Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column62" runat="server" DataIndex="To_Funds_Comment" Text="To Funds Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column63" runat="server" DataIndex="To_Foreign_Currency_Code" Text="To Foreign Currency Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column64" runat="server" DataIndex="To_Foreign_Currency_Amount" Text="To Foreign Currency Amount " Flex="1"></ext:Column>
                                                        <ext:Column ID="Column65" runat="server" DataIndex="To_Foreign_Currency_Exchange_Rate" Text="To Foreign Currency Exchange Rate" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column66" runat="server" DataIndex="To_Country" Text="To Country" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column67" runat="server" DataIndex="Comments" Text="Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column68" runat="server" DataIndex="isSwift" Text="isSWIFT" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column69" runat="server" DataIndex="FROM_Account_No" Text="From Account No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column70" runat="server" DataIndex="FROM_CIFNO" Text="From CIF No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column71" runat="server" DataIndex="From_WIC_NO" Text="From WIC No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column72" runat="server" DataIndex="TO_ACCOUNT_NO" Text="To Account No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column73" runat="server" DataIndex="TO_CIF_NO" Text="To CIF No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column74" runat="server" DataIndex="TO_WIC_NO" Text="To WIC No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column75" runat="server" DataIndex="Condutor_ID" Text="Conductor ID" Flex="1"></ext:Column>--%>
                                                        <ext:CommandColumn ID="cmdbipartyvalid" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                            <Commands>
                                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                            </Commands>
                                                            <DirectEvents>
                                                                <Command OnEvent="GridCmdTransactionBiparty">
                                                                    <ExtraParams>
                                                                        <ext:Parameter Name="unikkey" Value="record.data.TransactionNumber" Mode="Raw"></ext:Parameter>
                                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                    </ExtraParams>
                                                                </Command>
                                                            </DirectEvents>
                                                        </ext:CommandColumn>
                                                    </Columns>
                                                </ColumnModel>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                  </BottomBar>
                                            
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>

                                    <ext:Panel runat="server" ID="PanelTransactionMultiParty" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaction MultiParty" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelTransactionMultiParty" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreTransactionMultiParty" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="TransactionNumber">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_Transaction_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="FK_Report_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="FK_Jenis_Laporan_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isValid" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="TransactionNumber" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Internal_Ref_Number" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transaction_Remark" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Teller" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Authorized" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Late_Deposit" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Date_Posting" Type="Date"></ext:ModelField> 
                                                                    <ext:ModelField Name="Value_Date" Type="Date"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transmode_Comment" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Amount_Local" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isMyClient" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Funds_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Funds_Comment" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Foreign_Currency_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Foreign_Currency_Amount" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Foreign_Currency_Exchange_Rate" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Person_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Sender_Information" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Generate_Date" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isSwift" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Source_Data_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Report_Type_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Value_Report" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Jenis_Transaksi" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Peran" Type="Date"></ext:ModelField> 
                                                                    <ext:ModelField Name="Signifikasi" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Account_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="CIFNO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="WIC_NO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="IsUsedConductor" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Condutor_ID" Type="String"></ext:ModelField> 
                                                                    
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column43" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column77" runat="server" DataIndex="Date_Transaction" Text="Date Transaction" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column78" runat="server" DataIndex="Internal_Ref_Number" Text="Ref Num" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column79" runat="server" DataIndex="Amount_Local" Text="Amount Local" Flex="1"></ext:Column>
<%--                                                        <ext:Column ID="Column80" runat="server" DataIndex="FK_Source_Data_ID" Text="Data Source" Flex="1"></ext:Column>--%>
                                                        <ext:Column ID="Column81" runat="server" DataIndex="Transmode_Code" Text="Transmode Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column84" runat="server" DataIndex="TransactionNumber" Text="Transaction Number" Flex="1"></ext:Column>
<%--                                                        <ext:Column ID="Column82" runat="server" DataIndex="Transmode_Comment" Text="Transmode Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column83" runat="server" DataIndex="Transaction_Remark" Text="Transaction Remark" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column85" runat="server" DataIndex="Transaction_Location" Text="Transaction Location" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column86" runat="server" DataIndex="Teller" Text="Teller" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column87" runat="server" DataIndex="Authorized" Text="Authorized" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column88" runat="server" DataIndex="Late_Deposit" Text="Late Deposit Report Party" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column89" runat="server" DataIndex="Date_Posting" Text="Date Posting" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column90" runat="server" DataIndex="Value_Date" Text="Value Date" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column91" runat="server" DataIndex="Peran" Text="Peran" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column113" runat="server" DataIndex="Signifikasi" Text="Signifikasi" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column92" runat="server" DataIndex="Funds_Code" Text="Funds Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column93" runat="server" DataIndex="Funds_Comment" Text="Funds Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column94" runat="server" DataIndex="Foreign_Currency_Code" Text="Foreign Currency Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column95" runat="server" DataIndex="Foreign_Currency_Amount" Text="Foreign Currency Amount" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column96" runat="server" DataIndex="Foreign_Currency_Exchange_Rate" Text="Foreign Currency Exchange Rate" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column97" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column104" runat="server" DataIndex="Comments" Text="Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column105" runat="server" DataIndex="isSwift" Text="isSWIFT" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column106" runat="server" DataIndex="Account_No" Text="Account No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column107" runat="server" DataIndex="CIFNO" Text="CIF No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column108" runat="server" DataIndex="WIC_NO" Text="WIC No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column112" runat="server" DataIndex="Condutor_ID" Text="Conductor ID" Flex="1"></ext:Column>--%>
                                                        <ext:CommandColumn ID="cmdmultipartyvalid" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                            <Commands>
                                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                            </Commands>
                                                            <DirectEvents>
                                                                <Command OnEvent="GridCmdTransactionMultiparty">
                                                                    <ExtraParams>
                                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Transaction_ID" Mode="Raw"></ext:Parameter>
                                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                    </ExtraParams>
                                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                                    </Confirmation>
                                                                </Command>
                                                            </DirectEvents>
                                                        </ext:CommandColumn>
                                                    </Columns>
                                                </ColumnModel>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                  </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>
                                        
                                    <ext:Panel runat="server" ID="PanelActivityPerson" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Activity Person" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelActivityPerson" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreActivityPerson" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="PK_ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_ID" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Significance" Type="Int"></ext:ModelField> 
                                                                    <ext:ModelField Name="Reason" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Account_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="WIC_No" Type="String"></ext:ModelField>  
                                                                    <ext:ModelField Name="NamaPJK" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="KodePJK" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="KodeSwift" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>      
                                                                    <ext:ModelField Name="SSN" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Identity_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Passport_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isMyClient" Type="String"></ext:ModelField>                                                                            
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column10" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column11" runat="server" DataIndex="Significance" Text="Signifikasi" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column12" runat="server" DataIndex="Reason" Text="Alasan" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column13" runat="server" DataIndex="Comments" Text="Catatan Report Party" Flex="1"></ext:Column>
<%--                                                        <ext:Column ID="Column14" runat="server" DataIndex="" Text="Subnode Type" Flex="1"></ext:Column>--%>
                                                        <ext:Column ID="Column15" runat="server" DataIndex="isMyClient" Text="Ismyclient" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column16" runat="server" DataIndex="WIC_No" Text="Wic No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column17" runat="server" DataIndex="Last_Name" Text="Last Name" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column18" runat="server" DataIndex="SSN" Text="SSN" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column19" runat="server" DataIndex="Passport_No" Text="Passport Number" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column20" runat="server" DataIndex="Identity_No" Text="ID Number" Flex="1"></ext:Column>
                                                        
                                                    </Columns>
                                                </ColumnModel>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                  </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>

                                    <ext:Panel runat="server" ID="PanelActivityAccount" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Activity Account" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelActivityAccount" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreActivityAccount" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="PK_ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_ID" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Significance" Type="Int"></ext:ModelField> 
                                                                    <ext:ModelField Name="Reason" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Account_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="WIC_No" Type="String"></ext:ModelField>  
                                                                    <ext:ModelField Name="NamaPJK" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="KodePJK" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="KodeSwift" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>      
                                                                    <ext:ModelField Name="SSN" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Identity_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Passport_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isMyClient" Type="String"></ext:ModelField>                                                                            
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column21" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column22" runat="server" DataIndex="Significance" Text="Signifikasi" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column23" runat="server" DataIndex="Reason" Text="Alasan" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column24" runat="server" DataIndex="Comments" Text="Catatan Report Party" Flex="1"></ext:Column>
<%--                                                        <ext:Column ID="Column25" runat="server" DataIndex="" Text="Subnode Type" Flex="1"></ext:Column>--%>
                                                        <ext:Column ID="Column26" runat="server" DataIndex="isMyClient" Text="Ismyclient" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column27" runat="server" DataIndex="Account_No" Text="Account No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column28" runat="server" DataIndex="NamaPJK" Text="Nama PJK" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column29" runat="server" DataIndex="KodePJK" Text="Kode PJK" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column30" runat="server" DataIndex="KodeSwift" Text="Kode Swift" Flex="1"></ext:Column>
                                                        
                                                    </Columns>
                                                </ColumnModel>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                  </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>

                                    <ext:Panel runat="server" ID="PanelActivityEntity" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Activity Entity" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelActivityEntity" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreActivityEntity" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="PK_ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_ID" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Significance" Type="Int"></ext:ModelField> 
                                                                    <ext:ModelField Name="Reason" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Account_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="WIC_No" Type="String"></ext:ModelField>  
                                                                    <ext:ModelField Name="NamaPJK" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="KodePJK" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="KodeSwift" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>      
                                                                    <ext:ModelField Name="SSN" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Identity_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Passport_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isMyClient" Type="String"></ext:ModelField>                                                                            
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column31" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column32" runat="server" DataIndex="Significance" Text="Signifikasi" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column33" runat="server" DataIndex="Reason" Text="Alasan" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column34" runat="server" DataIndex="Comments" Text="Catatan Report Party" Flex="1"></ext:Column>
<%--                                                        <ext:Column ID="Column35" runat="server" DataIndex="" Text="Subnode Type" Flex="1"></ext:Column>--%>
                                                        <ext:Column ID="Column36" runat="server" DataIndex="isMyClient" Text="Ismyclient" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column37" runat="server" DataIndex="WIC_No" Text="WIC No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column38" runat="server" DataIndex="NamaPJK" Text="Nama Korporasi" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column39" runat="server" DataIndex="KodePJK" Text="Bidang Usaha" Flex="1"></ext:Column>
                                                        
                                                    </Columns>
                                                </ColumnModel>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                  </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>



                                </Items>
                            </ext:Panel>


                            <ext:Panel runat="server" ID="PanelInValid" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaction InValid" BodyStyle="padding:10px" Margin="5" Collapsible="true">
                                <Items>
                                    <ext:Panel runat="server" ID="PanelReportInvalid" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Report" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelReportInvalid" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreReportInvalid" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="PK_ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_ID" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="KeteranganError" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Tanggal_Laporan" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Jenis_laporan" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Ref_Num" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Alasan" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Tindakan_Pelapor" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="isValid" Type="String"></ext:ModelField>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column9" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:column ID="Column76" runat="server" DataIndex="Tanggal_Laporan" Text="Tanggal Laporan" Flex="1"></ext:column>
                                                        <ext:Column ID="Column98" runat="server" DataIndex="Jenis_laporan" Text="Jenis laporan" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column99" runat="server" DataIndex="Ref_Num" Text="No Ref Laporan" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column100" runat="server" DataIndex="Alasan" Text="Alasan" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column101" runat="server" DataIndex="Tindakan_Pelapor" Text="Tindakan_Pelapor" Flex="1"></ext:Column>
                                                        <%--<ext:Column ID="Column102" runat="server" DataIndex="Status" Text="Status" Flex="1"></ext:Column>--%>
                                                    </Columns>
                                                </ColumnModel>
                                                <Plugins>
                                                    <ext:GridFilters ID="GridFiltersReport" runat="server" />

                                                    <ext:RowExpander ID="RowReport" runat="server">

                                                        <Template ID="TemplateInvalidError" runat="server">
                                                            <Html>
                                                                <p><br>Error List:</br> {KeteranganError}</p>
                                                                <br />
                                                            </Html>
                                                        </Template>
                                                    </ext:RowExpander>

                                                </Plugins>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                  </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>

                                    <ext:Panel runat="server" ID="PanelIndikatorLaporanInvalid" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Indikator Laporan" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelIndikatorLaporanInvalid" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreIndikatorLaporanInvalid" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="PK_Indikator_Laporan_STR_ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_Indikator_Laporan_STR_ID" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="KeteranganError" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="FK_Indikator_Laporan" Type="String"></ext:ModelField> 
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column103" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column109" runat="server" DataIndex="FK_Indikator_Laporan" Text="Indikator Laporan" Flex="1"></ext:Column>
                                                    </Columns>
                                                </ColumnModel>
                                                <Plugins>
                                                    <ext:GridFilters ID="GridFiltersIndikatorLaporan" runat="server" />

                                                    <ext:RowExpander ID="RowExpanderIndikatorLaporan" runat="server">

                                                        <Template ID="TemplateIndikatorLaporan" runat="server">
                                                            <Html>
                                                                <p><br>Error List:</br> {KeteranganError}</p>
                                                                <br />
                                                            </Html>
                                                        </Template>
                                                    </ext:RowExpander>

                                                </Plugins>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                  </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>

                                    <ext:Panel runat="server" ID="PanelTransactionBipartyInvalid" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaksi BiParty" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelTransactionBipartyInvalid" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreTransactionBipartyInvalid" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="TransactionNumber">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_Transaction_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="KeteranganError" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="FK_Report_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="FK_Jenis_Laporan_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isValid" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="TransactionNumber" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Internal_Ref_Number" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transaction_Remark" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Teller" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Authorized" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Late_Deposit" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Date_Posting" Type="Date"></ext:ModelField> 
                                                                    <ext:ModelField Name="Value_Date" Type="Date"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transmode_Comment" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Amount_Local" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Transaction_Type" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="From_Funds_Comment" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="From_Foreign_Currency_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="From_Foreign_Currency_Amount" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="From_Foreign_Currency_Exchange_Rate" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Person_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Sender_From_Information" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="From_Country" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isMyClient_TO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="To_Funds_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="To_Funds_Comment" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="To_Foreign_Currency_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="To_Foreign_Currency_Amount" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="To_Foreign_Currency_Exchange_Rate" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Sender_To_Information" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="To_Country" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Generate_Date" Type="Date"></ext:ModelField> 
                                                                    <ext:ModelField Name="isSwift" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Source_Data_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Report_Type_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Value_Report" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Jenis_Transaksi" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FROM_Account_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FROM_CIFNO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="From_WIC_NO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="TO_ACCOUNT_NO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="TO_CIF_NO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="TO_WIC_NO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="IsUsedConductor" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Condutor_ID" Type="String"></ext:ModelField> 

                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column14" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column25" runat="server" DataIndex="Date_Transaction" Text="Date Transaction" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column35" runat="server" DataIndex="Internal_Ref_Number" Text="Ref Num" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column45" runat="server" DataIndex="Amount_Local" Text="Amount Local" Flex="1"></ext:Column>
<%--                                                        <ext:Column ID="Column43" runat="server" DataIndex="FK_Source_Data_ID" Text="Data Source" Flex="1"></ext:Column>--%>
                                                        <ext:Column ID="Column46" runat="server" DataIndex="Transmode_Code" Text="Transmode Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column48" runat="server" DataIndex="TransactionNumber" Text="Transaction Number" Flex="1"></ext:Column>

<%--                                                        <ext:Column ID="Column45" runat="server" DataIndex="Transmode_Comment" Text="Transmode Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column46" runat="server" DataIndex="Transaction_Remark" Text="Transaction Remark" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column48" runat="server" DataIndex="Transaction_Location" Text="Transaction Location" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column49" runat="server" DataIndex="Teller" Text="Teller" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column50" runat="server" DataIndex="Authorized" Text="Authorized" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column51" runat="server" DataIndex="Late_Deposit" Text="Late Deposit Report Party" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column52" runat="server" DataIndex="Date_Posting" Text="Date Posting" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column53" runat="server" DataIndex="Value_Date" Text="Value Date" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column54" runat="server" DataIndex="isMyClient_FROM" Text="IsMyClient_FORM" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column55" runat="server" DataIndex="From_Funds_Code" Text="Form Funds Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column56" runat="server" DataIndex="From_Funds_Comment" Text="From Funds Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column57" runat="server" DataIndex="From_Foreign_Currency_Code" Text="From Foreign Currency Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column58" runat="server" DataIndex="From_Foreign_Currency_Amount" Text="From Foreign Currency Amount" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column59" runat="server" DataIndex="From_Foreign_Currency_Exchange_Rate" Text="From Foreign Currency Exchange Rate" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column60" runat="server" DataIndex="From_Country" Text="From Country" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column61" runat="server" DataIndex="To_Funds_Code" Text="To Funds Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column62" runat="server" DataIndex="To_Funds_Comment" Text="To Funds Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column63" runat="server" DataIndex="To_Foreign_Currency_Code" Text="To Foreign Currency Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column64" runat="server" DataIndex="To_Foreign_Currency_Amount" Text="To Foreign Currency Amount " Flex="1"></ext:Column>
                                                        <ext:Column ID="Column65" runat="server" DataIndex="To_Foreign_Currency_Exchange_Rate" Text="To Foreign Currency Exchange Rate" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column66" runat="server" DataIndex="To_Country" Text="To Country" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column67" runat="server" DataIndex="Comments" Text="Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column68" runat="server" DataIndex="isSwift" Text="isSWIFT" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column69" runat="server" DataIndex="FROM_Account_No" Text="From Account No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column70" runat="server" DataIndex="FROM_CIFNO" Text="From CIF No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column71" runat="server" DataIndex="From_WIC_NO" Text="From WIC No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column72" runat="server" DataIndex="TO_ACCOUNT_NO" Text="To Account No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column73" runat="server" DataIndex="TO_CIF_NO" Text="To CIF No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column74" runat="server" DataIndex="TO_WIC_NO" Text="To WIC No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column75" runat="server" DataIndex="Condutor_ID" Text="Conductor ID" Flex="1"></ext:Column>--%>
                                                        <ext:CommandColumn ID="cmdBipartyInvalid" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                            <Commands>
                                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                            </Commands>
                                                            <DirectEvents>
                                                                <Command OnEvent="GridCmdTransactionBiparty">
                                                                    <ExtraParams>
                                                                        <ext:Parameter Name="unikkey" Value="record.data.TransactionNumber" Mode="Raw"></ext:Parameter> 
                                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                    </ExtraParams>
                                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                                    </Confirmation>
                                                                </Command>
                                                            </DirectEvents>
                                                        </ext:CommandColumn>
                                                    </Columns>
                                                </ColumnModel>
                                                <Plugins>
                                                    <ext:GridFilters ID="GridFiltersTransactionBiparty" runat="server" />

                                                    <ext:RowExpander ID="RowExpanderTransactionBiparty" runat="server">

                                                        <Template ID="TemplateTransactionBiparty" runat="server">
                                                            <Html>
                                                                <p><br>Error List:</br> {KeteranganError}</p>
                                                                <br />
                                                            </Html>
                                                        </Template>
                                                    </ext:RowExpander>

                                                </Plugins>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                  </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>

                                    <ext:Panel runat="server" ID="PanelTransactionMultiPartyInvalid" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaction MultiParty" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelTransactionMultiPartyInvalid" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreTransactionMultiPartyInvalid" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="PK_Transaction_ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_Transaction_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="FK_Report_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="KeteranganError" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="FK_Jenis_Laporan_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isValid" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="TransactionNumber" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Internal_Ref_Number" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transaction_Location" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transaction_Remark" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Teller" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Authorized" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Late_Deposit" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Date_Posting" Type="Date"></ext:ModelField> 
                                                                    <ext:ModelField Name="Value_Date" Type="Date"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Transmode_Comment" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Amount_Local" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isMyClient" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Funds_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Funds_Comment" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Foreign_Currency_Code" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Foreign_Currency_Amount" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Foreign_Currency_Exchange_Rate" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Person_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Sender_Information" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Generate_Date" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isSwift" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Source_Data_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="FK_Report_Type_ID" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Value_Report" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Jenis_Transaksi" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Peran" Type="Date"></ext:ModelField> 
                                                                    <ext:ModelField Name="Signifikasi" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Account_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="CIFNO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="WIC_NO" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="IsUsedConductor" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Condutor_ID" Type="String"></ext:ModelField> 
                                                                    
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column49" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column50" runat="server" DataIndex="Date_Transaction" Text="Date Transaction" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column51" runat="server" DataIndex="Internal_Ref_Number" Text="Ref Num" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column52" runat="server" DataIndex="Amount_Local" Text="Amount Local" Flex="1"></ext:Column>
<%--                                                        <ext:Column ID="Column80" runat="server" DataIndex="FK_Source_Data_ID" Text="Data Source" Flex="1"></ext:Column>--%>
                                                        <ext:Column ID="Column53" runat="server" DataIndex="Transmode_Code" Text="Transmode Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column54" runat="server" DataIndex="TransactionNumber" Text="Transaction Number" Flex="1"></ext:Column>
<%--                                                        <ext:Column ID="Column82" runat="server" DataIndex="Transmode_Comment" Text="Transmode Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column83" runat="server" DataIndex="Transaction_Remark" Text="Transaction Remark" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column85" runat="server" DataIndex="Transaction_Location" Text="Transaction Location" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column86" runat="server" DataIndex="Teller" Text="Teller" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column87" runat="server" DataIndex="Authorized" Text="Authorized" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column88" runat="server" DataIndex="Late_Deposit" Text="Late Deposit Report Party" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column89" runat="server" DataIndex="Date_Posting" Text="Date Posting" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column90" runat="server" DataIndex="Value_Date" Text="Value Date" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column91" runat="server" DataIndex="Peran" Text="Peran" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column113" runat="server" DataIndex="Signifikasi" Text="Signifikasi" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column92" runat="server" DataIndex="Funds_Code" Text="Funds Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column93" runat="server" DataIndex="Funds_Comment" Text="Funds Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column94" runat="server" DataIndex="Foreign_Currency_Code" Text="Foreign Currency Code" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column95" runat="server" DataIndex="Foreign_Currency_Amount" Text="Foreign Currency Amount" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column96" runat="server" DataIndex="Foreign_Currency_Exchange_Rate" Text="Foreign Currency Exchange Rate" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column97" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column104" runat="server" DataIndex="Comments" Text="Comment" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column105" runat="server" DataIndex="isSwift" Text="isSWIFT" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column106" runat="server" DataIndex="Account_No" Text="Account No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column107" runat="server" DataIndex="CIFNO" Text="CIF No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column108" runat="server" DataIndex="WIC_NO" Text="WIC No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column112" runat="server" DataIndex="Condutor_ID" Text="Conductor ID" Flex="1"></ext:Column>--%>
                                                        <ext:CommandColumn ID="cmdMultipartyinValid" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                            <Commands>
                                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                            </Commands>
                                                            <DirectEvents>
                                                                <Command OnEvent="GridCmdTransactionMultiparty">
                                                                    <ExtraParams>
                                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Transaction_ID" Mode="Raw"></ext:Parameter>
                                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                    </ExtraParams>
                                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                                    </Confirmation>
                                                                </Command>
                                                            </DirectEvents>
                                                        </ext:CommandColumn>
                                                    </Columns>
                                                </ColumnModel>
                                                <Plugins>
                                                    <ext:GridFilters ID="GridFiltersTransactionMultiParty" runat="server" />

                                                    <ext:RowExpander ID="RowExpanderTransactionMultiParty" runat="server">

                                                        <Template ID="TemplateTransactionMultiParty" runat="server">
                                                            <Html>
                                                                <p><br>Error List:</br> {KeteranganError}</p>
                                                                <br />
                                                            </Html>
                                                        </Template>
                                                    </ext:RowExpander>

                                                </Plugins>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                  </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>
                                        
                                    <ext:Panel runat="server" ID="PanelActivityPersonInvalid" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Activity Person" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelActivityPersonInvalid" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreActivityPersonInvalid" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="PK_ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_ID" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="KeteranganError" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Significance" Type="Int"></ext:ModelField> 
                                                                    <ext:ModelField Name="Reason" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Account_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="WIC_No" Type="String"></ext:ModelField>  
                                                                    <ext:ModelField Name="NamaPJK" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="KodePJK" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="KodeSwift" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>      
                                                                    <ext:ModelField Name="SSN" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Identity_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Passport_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isMyClient" Type="String"></ext:ModelField>                                                                            
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column176" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column177" runat="server" DataIndex="Significance" Text="Signifikasi" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column178" runat="server" DataIndex="Reason" Text="Alasan" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column179" runat="server" DataIndex="Comments" Text="Catatan Report Party" Flex="1"></ext:Column>
<%--                                                        <ext:Column ID="Column180" runat="server" DataIndex="" Text="Subnode Type" Flex="1"></ext:Column>--%>
                                                        <ext:Column ID="Column181" runat="server" DataIndex="isMyClient" Text="Ismyclient" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column182" runat="server" DataIndex="WIC_No" Text="Wic No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column183" runat="server" DataIndex="Last_Name" Text="Last Name" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column184" runat="server" DataIndex="SSN" Text="SSN" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column185" runat="server" DataIndex="Passport_No" Text="Passport Number" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column186" runat="server" DataIndex="Identity_No" Text="ID Number" Flex="1"></ext:Column>
                                                        
                                                    </Columns>
                                                </ColumnModel>
                                                <Plugins>
                                                    <ext:GridFilters ID="GridFiltersActivityPerson" runat="server" />

                                                    <ext:RowExpander ID="RowExpanderActivityPerson" runat="server">

                                                        <Template ID="TemplateActivityPerson" runat="server">
                                                            <Html>
                                                                <p><br>Error List:</br> {KeteranganError}</p>
                                                                <br />
                                                            </Html>
                                                        </Template>
                                                    </ext:RowExpander>

                                                </Plugins>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                  </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>

                                    <ext:Panel runat="server" ID="PanelActivityAccountInvalid" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Activity Account" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelActivityAccountInvalid" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreActivityAccountInvalid" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="PK_ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_ID" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="KeteranganError" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Significance" Type="Int"></ext:ModelField> 
                                                                    <ext:ModelField Name="Reason" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Account_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="WIC_No" Type="String"></ext:ModelField>  
                                                                    <ext:ModelField Name="NamaPJK" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="KodePJK" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="KodeSwift" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>      
                                                                    <ext:ModelField Name="SSN" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Identity_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Passport_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isMyClient" Type="String"></ext:ModelField>                                                                            
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn13" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column187" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column188" runat="server" DataIndex="Significance" Text="Signifikasi" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column189" runat="server" DataIndex="Reason" Text="Alasan" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column190" runat="server" DataIndex="Comments" Text="Catatan Report Party" Flex="1"></ext:Column>
 <%--                                                       <ext:Column ID="Column191" runat="server" DataIndex="" Text="Subnode Type" Flex="1"></ext:Column>--%>
                                                        <ext:Column ID="Column192" runat="server" DataIndex="isMyClient" Text="Ismyclient" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column193" runat="server" DataIndex="Account_No" Text="Account No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column194" runat="server" DataIndex="NamaPJK" Text="Nama PJK" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column195" runat="server" DataIndex="KodePJK" Text="Kode PJK" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column196" runat="server" DataIndex="KodeSwift" Text="Kode Swift" Flex="1"></ext:Column>
                                                        
                                                    </Columns>
                                                </ColumnModel>
                                                <Plugins>
                                                    <ext:GridFilters ID="GridFiltersActivityAccount" runat="server" />

                                                    <ext:RowExpander ID="RowExpanderActivityAccount" runat="server">

                                                        <Template ID="TemplateActivityAccount" runat="server">
                                                            <Html>
                                                                <p><br>Error List:</br> {KeteranganError}</p>
                                                                <br />
                                                            </Html>
                                                        </Template>
                                                    </ext:RowExpander>

                                                </Plugins>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                  </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>

                                    <ext:Panel runat="server" ID="PanelActivityEntityInvalid" Hidden="false" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Activity Entity" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelActivityEntityInvalid" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <TopBar>
                                                    <ext:Toolbar runat="server">
                                                        <Items>
                                                        </Items>
                                                    </ext:Toolbar>
                                                </TopBar>
                                                <Store>
                                                    <ext:Store ID="StoreActivityEntityInvalid" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" IDProperty="PK_ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_ID" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="KeteranganError" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Case_ID" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Significance" Type="Int"></ext:ModelField> 
                                                                    <ext:ModelField Name="Reason" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Account_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="WIC_No" Type="String"></ext:ModelField>  
                                                                    <ext:ModelField Name="NamaPJK" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="KodePJK" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="KodeSwift" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>      
                                                                    <ext:ModelField Name="SSN" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Identity_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="Passport_No" Type="String"></ext:ModelField> 
                                                                    <ext:ModelField Name="isMyClient" Type="String"></ext:ModelField>                                                                            
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn14" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column197" runat="server" DataIndex="Case_ID" Text="Case ID" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column198" runat="server" DataIndex="Significance" Text="Signifikasi" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column199" runat="server" DataIndex="Reason" Text="Alasan" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column200" runat="server" DataIndex="Comments" Text="Catatan Report Party" Flex="1"></ext:Column>
 <%--                                                       <ext:Column ID="Column201" runat="server" DataIndex="" Text="Subnode Type" Flex="1"></ext:Column>--%>
                                                        <ext:Column ID="Column202" runat="server" DataIndex="isMyClient" Text="Ismyclient" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column203" runat="server" DataIndex="WIC_No" Text="WIC No" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column204" runat="server" DataIndex="NamaPJK" Text="Nama Korporasi" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column205" runat="server" DataIndex="KodePJK" Text="Bidang Usaha" Flex="1"></ext:Column>
                                                        
                                                    </Columns>
                                                </ColumnModel>
                                                <Plugins>
                                                    <ext:GridFilters ID="GridFiltersActivityEntity" runat="server" />

                                                    <ext:RowExpander ID="RowExpanderActivityEntity" runat="server">

                                                        <Template ID="TemplateActivityEntity" runat="server">
                                                            <Html>
                                                                <p><br>Error List:</br> {KeteranganError}</p>
                                                                <br />
                                                            </Html>
                                                        </Template>
                                                    </ext:RowExpander>

                                                </Plugins>
                                                <BottomBar>
                                                         <ext:PagingToolbar runat="server">
                                                            <Items>
                                                                <ext:Label runat="server" Text="Page size:" />
                                                                <ext:ToolbarSpacer runat="server" Width="10" />
                                                            </Items>
                                                        </ext:PagingToolbar>
                                                  </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>



                                </Items>
                            </ext:Panel>

                              <ext:TextArea  ID="RemarkReportSTR" runat="server" fieldlabel="Remark" BodyStyle="padding:10px" Margin="10" AllowBlank="false" AnchorHorizontal="90%"/>

                        </Items>

                    </ext:FormPanel>

        </Items>

        <Buttons>
                    <ext:Button ID="BtnSave" runat="server" Text="Accept" Icon="DiskBlack">
                        <DirectEvents>
                            <Click OnEvent="BtnSave_Click">
                                <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"> </EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="ButtonReject" runat="server" Text="Reject" Icon="Decline">
                        <DirectEvents>
                            <Click OnEvent="BtnReject_Click">
                                <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"> </EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                    <ext:Button ID="BtnCancelDetail" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancel_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

        </Buttons>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>

        </Items>
        <Buttons>

            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
