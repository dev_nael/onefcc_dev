﻿Imports System.Data
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevDAL
Imports NawaDevBLL
Imports NawaBLL
Partial Class goAML_GenerateSAREdit
    Inherits Parent
#Region "Session"
    Private _IDReq As Long
    Public goAML_SarBLL As New NawaDevBLL.GenerateSarBLL()
    Public Property IDReq() As Long
        Get
            Return _IDReq
        End Get
        Set(ByVal value As Long)
            _IDReq = value
        End Set
    End Property

    '' Add 27-Dec-2022 Felix, Thanks To Try. Ubah load ke grid nya pakai StoreReadData
    Public Property strWhereClause() As String
        Get
            Return Session("goAML_GenerateSAREdit.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSAREdit.strWhereClause") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("goAML_GenerateSAREdit.indexStart")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSAREdit.indexStart") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("goAML_GenerateSAREdit.strSort")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSAREdit.strSort") = value
        End Set
    End Property

    Public Property FK_ReportSTRSAR() As String
        Get
            Return Session("goAML_GenerateSAREdit.FK_ReportSTRSAR")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSAREdit.FK_ReportSTRSAR") = value
        End Set
    End Property

    Public Property Date_From() As Date
        Get
            Return Session("goAML_GenerateSAREdit.Date_From")
        End Get
        Set(ByVal value As Date)
            Session("goAML_GenerateSAREdit.Date_From") = value
        End Set
    End Property

    Public Property Date_To() As Date
        Get
            Return Session("goAML_GenerateSAREdit.Date_To")
        End Get
        Set(ByVal value As Date)
            Session("goAML_GenerateSAREdit.Date_To") = value
        End Set
    End Property

    '' End 27-Dec-2022
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("goAML_GenerateSAREdit.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_GenerateSAREdit.ObjModule") = value
        End Set
    End Property

    Public Property ObjGenerateSAR As NawaDevDAL.goAML_ODM_Generate_STR_SAR
        Get
            Return Session("goAML_GenerateSAREdit.ObjGenerateSAR")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_ODM_Generate_STR_SAR)
            Session("goAML_GenerateSAREdit.ObjGenerateSAR") = value
        End Set
    End Property
    Public Property ObjGenerateSAROld As NawaDevDAL.goAML_ODM_Generate_STR_SAR
        Get
            Return Session("goAML_GenerateSAREdit.ObjGenerateSAROld")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_ODM_Generate_STR_SAR)
            Session("goAML_GenerateSAREdit.ObjGenerateSAROld") = value
        End Set
    End Property
    Public Property ListTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi)
        Get
            If Session("goAML_GenerateSAREdit.ListTransaction") Is Nothing Then
                Session("goAML_GenerateSAREdit.ListTransaction") = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
            End If
            Return Session("goAML_GenerateSAREdit.ListTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Transaksi))
            Session("goAML_GenerateSAREdit.ListTransaction") = value
        End Set
    End Property
    Public Property ListIndicator As List(Of NawaDevDAL.goAML_Report_Indicator)
        Get
            Return Session("goAML_GenerateSAREdit.ListIndicator")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Report_Indicator))
            Session("goAML_GenerateSAREdit.ListIndicator") = value
        End Set
    End Property
    'Public Property ObjSARData As NawaDevBLL.GenerateSARData
    '    Get
    '        Return Session("goAML_GenerateSAREdit.ObjSARData")
    '    End Get
    '    Set(ByVal value As NawaDevBLL.GenerateSARData)
    '        Session("goAML_GenerateSAREdit.ObjSARData") = value
    '    End Set
    'End Property
    Public Property ObjSARData As GenerateSARData
        Get
            If Session("goAML_GenerateSARAdd.objGenerateSAR") Is Nothing Then
                Session("goAML_GenerateSARAdd.objGenerateSAR") = New GenerateSARData
            End If
            Return Session("goAML_GenerateSARAdd.objGenerateSAR")
        End Get
        Set(ByVal value As GenerateSARData)
            Session("goAML_GenerateSARAdd.objGenerateSAR") = value
        End Set
    End Property

    Public Property IDIndicator As String
        Get
            Return Session("goAML_GenerateSAREdit.IDIndicator")
        End Get
        Set(value As String)
            Session("goAML_GenerateSAREdit.IDIndicator") = value
        End Set
    End Property
    Public Property ListSelectedTransaction As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction)
        Get
            If Session("goAML_GenerateSAREdit.ListSelectedTransaction") Is Nothing Then
                Session("goAML_GenerateSAREdit.ListSelectedTransaction") = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction)
            End If
            Return Session("goAML_GenerateSAREdit.ListSelectedTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction))
            Session("goAML_GenerateSAREdit.ListSelectedTransaction") = value
        End Set
    End Property
    Public Property ListSelectedOdmTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi)
        Get
            If Session("goAML_GenerateSAREdit.ListSelectedOdmTransaction") Is Nothing Then
                Session("goAML_GenerateSAREdit.ListSelectedOdmTransaction") = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
            End If
            Return Session("goAML_GenerateSAREdit.ListSelectedOdmTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Transaksi))
            Session("goAML_GenerateSAREdit.ListSelectedOdmTransaction") = value
        End Set
    End Property
    Public Property ListobjSarTransaction As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction)
        Get
            If Session("goAML_GenerateSAREdit.ListobjSarTransaction") Is Nothing Then
                Session("goAML_GenerateSAREdit.ListobjSarTransaction") = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction)
            End If
            Return Session("goAML_GenerateSAREdit.ListobjSarTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction))
            Session("goAML_GenerateSAREdit.ListobjSarTransaction") = value
        End Set
    End Property
    Public Property objSAR As goAML_ODM_Generate_STR_SAR
        Get
            If Session("goAML_GenerateSAREdit.objSAR") Is Nothing Then
                Session("goAML_GenerateSAREdit.objSAR") = New goAML_ODM_Generate_STR_SAR
            End If
            Return Session("goAML_GenerateSAREdit.objSAR")
        End Get
        Set(ByVal value As goAML_ODM_Generate_STR_SAR)
            Session("goAML_GenerateSAREdit.objSAR") = value
        End Set
    End Property
    Public Property ListDokumen As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment)
        Get
            Return Session("goAML_GenerateSAREdit.ListDokumen")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment))
            Session("goAML_GenerateSAREdit.ListDokumen") = value
        End Set
    End Property
    Public Property IDDokumen As String
        Get
            Return Session("goAML_GenerateSAREdit.IDDokumen")
        End Get
        Set(value As String)
            Session("goAML_GenerateSAREdit.IDDokumen") = value
        End Set
    End Property
    Public Property IsChange As Boolean
        Get
            Return Session("goAML_GenerateSAREdit.IsChange")
        End Get
        Set(value As Boolean)
            Session("goAML_GenerateSAREdit.IsChange") = value
        End Set
    End Property
#End Region

#Region "Load Data"
    Private Sub goAML_GenerateSAREdit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer

            intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)


            If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Update) Then
                Dim strIDCode As String = 1
                strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                Exit Sub
            End If
            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = ObjModule.ModuleLabel & " Edit"
                clearSession()
                LoadData()
                'LoadStoreComboBox()
                PopupMaximizale()
                ColumnActionLocation()
                FileDoc.FieldStyle = "background-color: #FFE4C4"
                sar_reportIndicator.FieldStyle = "background-color: #FFE4C4"
                'Dim sm As RowSelectionModel = GridPaneldetail.GetSelectionModel
                'For Each DrLob In ObjSARData.listObjGenerateSARTransaction
                '    Dim str As String
                '    str = DrLob.Pk_goAML_ODM_Generate_STR_SAR_Transaction
                '    sm.SelectedRows.Add(New SelectedRow(str))
                'Next

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadApprovalHistory(modulId As String, sarId As String)
        Try
            Dim objApprovalHistory As New List(Of goAML_Module_Note_History)
            objApprovalHistory = NawaDevBLL.GenerateSarBLL.getListApprovalHistory(modulId, sarId)
            If objApprovalHistory.Count > 0 Then
                gpStore_history.DataSource = objApprovalHistory
                gpStore_history.DataBind()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub ColumnActionLocation()
        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 1 Then
            'GridPanelReportIndicator.ColumnModel.Columns.Add(CommandColumn87)
        ElseIf bsettingRight = 2 Then
            'Indicator
            GridPanelReportIndicator.ColumnModel.Columns.RemoveAt(GridPanelReportIndicator.ColumnModel.Columns.Count - 1)
            GridPanelReportIndicator.ColumnModel.Columns.Insert(1, CommandColumn87)

            'Transaction
            'GridPaneldetail.ColumnModel.Columns.RemoveAt(GridPaneldetail.ColumnModel.Columns.Count - 1)
            'GridPaneldetail.ColumnModel.Columns.Insert(1, CommandColumn36)

            'Dokumen
            GridPanelAttachment.ColumnModel.Columns.RemoveAt(GridPanelAttachment.ColumnModel.Columns.Count - 1)
            GridPanelAttachment.ColumnModel.Columns.Insert(1, CommandColumnAttachment)
        End If

    End Sub

    Sub PopupMaximizale()
        WindowReportIndicator.Maximizable = True
        WindowTransaction.Maximizable = True
        WindowAttachment.Maximizable = True
    End Sub
    Sub LoadData()
        Dim IDData As String = Request.Params("ID")
        Dim Arrstr() As String
        Dim num As Integer = 1

        IDReq = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)

        ObjSARData.objGenerateSAR = NawaDevBLL.GenerateSarBLL.getGenerateSARByPk(IDReq)
        ObjGenerateSAROld = NawaDevBLL.GenerateSarBLL.getGenerateSARByPk(IDReq)
        ObjSARData.listObjGenerateSARTransaction = NawaDevBLL.GenerateSarBLL.getListSarTransactionByFk(IDReq)

        ListobjSarTransaction = NawaDevBLL.GenerateSarBLL.getListSarTransactionByFk(IDReq)
        ListDokumen = NawaDevBLL.GenerateSarBLL.getListGenerateSARDokumen(IDReq)
        objSAR = NawaDevBLL.GenerateSarBLL.getGenerateSARByPk(IDReq)

        '' Add 27-Dec-2022
        Date_From = ObjSARData.listObjGenerateSARTransaction.Max(Function(x) x.Date_Transaction)
        Date_To = ObjSARData.listObjGenerateSARTransaction.Min(Function(x) x.Date_Transaction)
        '' End 27-Dec-2022

        'bindTransaction(StoreTransaction, ObjSARData.listObjGenerateSARTransaction)
        bindAttachment(StoreAttachment, ListDokumen)

        With ObjSARData.objGenerateSAR
            FK_ReportSTRSAR = .PK_goAML_ODM_Generate_STR_SAR '' Add 27-Dec-2022
            sar_PK.Text = .PK_goAML_ODM_Generate_STR_SAR
            sar_cif.Text = .CIF_NO + " - " + GenerateSarBLL.getCustomerNameByCif(.CIF_NO)
            sar_jenisLaporan.Text = NawaDevBLL.GlobalReportFunctionBLL.getJenisLaporanByCode(.Transaction_Code)
            If .Date_Report IsNot Nothing Then
                sar_TanggalLaporan.Text = .Date_Report.Value.ToString("dd-MMM-yyyy")
            End If
            NoRefPPATK.Text = .Fiu_Ref_Number
            Alasan.Text = .Reason
            If .indicator IsNot Nothing Then

                Arrstr = .indicator.Split(",")
                For Each item In Arrstr
                    Dim Indikator As New NawaDevDAL.goAML_Report_Indicator
                    Indikator.PK_Report_Indicator = num
                    Indikator.FK_Indicator = item
                    ListIndicator.Add(Indikator)
                    num += 1
                Next
                bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            End If
            'If .total_transaction IsNot Nothing Then
            '    sar_TotalTran.Text = .total_transaction    
            'End If
        End With

        LoadApprovalHistory(ObjModule.PK_Module_ID, IDReq)
    End Sub
    Sub bindOdmTransaction(store As Ext.Net.Store, listTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listTransaction)
        objtable.Columns.Add(New DataColumn("NoTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoRefTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("TipeTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("LokasiTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("KeteranganTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("DateTransaction", GetType(Date)))
        objtable.Columns.Add(New DataColumn("AccountNo", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaTeller", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaPejabat", GetType(String)))
        objtable.Columns.Add(New DataColumn("TglPembukuan", GetType(Date)))
        objtable.Columns.Add(New DataColumn("CaraTranDilakukan", GetType(String)))
        objtable.Columns.Add(New DataColumn("CaraTranLain", GetType(String)))
        objtable.Columns.Add(New DataColumn("DebitCredit", GetType(String)))
        objtable.Columns.Add(New DataColumn("OriginalAmount", GetType(String)))
        objtable.Columns.Add(New DataColumn("IDR", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("NoTran") = item("Transaction_Number")
                item("NoRefTran") = item("Ref_Num")
                Dim SourceData As String = item("Source_Data").ToString
                Dim SourceDataTemp As Integer
                If Integer.TryParse(SourceData, SourceDataTemp) Then
                    If SourceData <> "" Then
                        item("TipeTran") = NawaDevBLL.GenerateSarBLL.getSourceData(SourceData)
                    End If
                Else
                    item("TipeTran") = SourceData
                End If
                item("LokasiTran") = item("Transaction_Location")
                item("KeteranganTran") = item("Transaction_Remark")
                item("DateTransaction") = item("Date_Transaction")
                item("AccountNo") = item("Account_NO")
                item("NamaTeller") = item("Teller")
                item("NamaPejabat") = item("Authorized")
                item("TglPembukuan") = item("Date_Posting")
                item("CaraTranDilakukan") = item("Transmode_Code")
                item("CaraTranLain") = item("Transmode_Comment")
                item("DebitCredit") = item("Debit_Credit")
                If item("Original_Amount").ToString <> "" Then
                    Dim OriginalAmount As Decimal = item("Original_Amount").ToString
                    item("OriginalAmount") = OriginalAmount.ToString("#,###.00")
                End If
                If item("IDR_Amount").ToString <> "" Then
                    Dim IDR As Decimal = item("IDR_Amount").ToString
                    item("IDR") = IDR.ToString("#,###.00")
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAttachment(store As Ext.Net.Store, listAttachment As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAttachment)
        objtable.Columns.Add(New DataColumn("FileName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("FileName") = item("File_Name")
                item("Keterangan") = item("Remarks")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindTransaction(store As Ext.Net.Store, listTransaction As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listTransaction)
        objtable.Columns.Add(New DataColumn("NoTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoRefTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("TipeTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("LokasiTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("KeteranganTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("DateTransaction", GetType(Date)))
        objtable.Columns.Add(New DataColumn("AccountNo", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaTeller", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaPejabat", GetType(String)))
        objtable.Columns.Add(New DataColumn("TglPembukuan", GetType(Date)))
        objtable.Columns.Add(New DataColumn("CaraTranDilakukan", GetType(String)))
        objtable.Columns.Add(New DataColumn("CaraTranLain", GetType(String)))
        objtable.Columns.Add(New DataColumn("DebitCredit", GetType(String)))
        objtable.Columns.Add(New DataColumn("OriginalAmount", GetType(String)))
        objtable.Columns.Add(New DataColumn("IDR", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Pk_goAML_ODM_Generate_STR_SAR_Transaction") = item("Pk_goAML_ODM_Generate_STR_SAR_Transaction")
                item("NoTran") = item("Transaction_Number")
                item("NoRefTran") = item("Ref_Num")
                Dim SourceData As String = item("Source_Data").ToString
                Dim SourceDataTemp As Integer
                If Integer.TryParse(SourceData, SourceDataTemp) Then
                    If SourceData <> "" Then
                        item("TipeTran") = NawaDevBLL.GenerateSarBLL.getSourceData(SourceData)
                    End If
                Else
                    item("TipeTran") = SourceData
                End If
                item("LokasiTran") = item("Transaction_Location")
                item("KeteranganTran") = item("Transaction_Remark")
                item("DateTransaction") = item("Date_Transaction")
                item("AccountNo") = item("Account_NO")
                item("NamaTeller") = item("Teller")
                item("NamaPejabat") = item("Authorized")
                item("TglPembukuan") = item("Date_Posting")
                item("CaraTranDilakukan") = item("Transmode_Code")
                item("CaraTranLain") = item("Transmode_Comment")
                item("DebitCredit") = item("Debit_Credit")
                If item("Original_Amount").ToString <> "" Then
                    Dim OriginalAmount As Decimal = item("Original_Amount").ToString
                    item("OriginalAmount") = OriginalAmount.ToString("#,###.00")
                End If
                If item("IDR_Amount").ToString <> "" Then
                    Dim IDR As Decimal = item("IDR_Amount").ToString
                    item("IDR") = IDR.ToString("#,###.00")
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindReportIndicator(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Report_Indicator))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        '  objtable.Columns.Add(New DataColumn("Kode_Indicator", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                '     item("Kode_Indicator") = item("FK_Indicator")
                item("Keterangan") = NawaDevBLL.GenerateSarBLL.getReportIndicatorByKode(item("FK_Indicator").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub editIndicator(id As String, command As String)
        Try
            sar_reportIndicator.Clear()

            WindowReportIndicator.Hidden = False
            Dim reportIndicator As NawaDevDAL.goAML_Report_Indicator
            reportIndicator = ListIndicator.Where(Function(x) x.PK_Report_Indicator = id).FirstOrDefault


            sar_reportIndicator.SetValue(reportIndicator.FK_Indicator)
            If command = "Detail" Then
                sar_reportIndicator.Selectable = False
                BtnsaveReportIndicator.Hidden = True
            Else
                sar_reportIndicator.Selectable = True
                BtnsaveReportIndicator.Hidden = False
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ReportIndicator_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goaml_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub JenisLaporan_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Jenis_Laporan_STR", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub CIF_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Customer", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

#End Region

#Region "Method"
    Sub clearSession()
        IsChange = True
        ObjGenerateSAR = New NawaDevDAL.goAML_ODM_Generate_STR_SAR
        ObjGenerateSAROld = New NawaDevDAL.goAML_ODM_Generate_STR_SAR
        ListIndicator = New List(Of NawaDevDAL.goAML_Report_Indicator)
        ListTransaction = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
        ListSelectedTransaction = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction)
        ListSelectedOdmTransaction = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
        ObjSARData = New NawaDevBLL.GenerateSARData
        ListobjSarTransaction = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction)
        ListDokumen = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment)
        objSAR = New goAML_ODM_Generate_STR_SAR
    End Sub
    Sub LoadStoreComboBox()
        'sar_jenisLaporan.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        'StoreJenisLaporan.Reload()

        'sar_reportIndicator.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        'StoreReportIndicator.Reload()

    End Sub
#End Region

#Region "Direct Event"
    Protected Sub BtnSave_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'If sar_jenisLaporan.SelectedItem.Value = "LTKMT" Or sar_jenisLaporan.SelectedItem.Value = "LAPT" Or sar_jenisLaporan.SelectedItem.Value = "LTKM" Then
            '    If ListIndicator.Count = 0 Then
            '        Throw New ApplicationException("Indikator tidak boleh kosong")
            '    End If
            'End If
            If ObjSARData.objGenerateSAR.status = 0 Then
                Throw New ApplicationException("Data Tidak Dapat di Edit")
            End If

            If ListIndicator.Count = 0 Then
                Throw New ApplicationException("Indikator tidak boleh kosong")
            End If

            With ObjSARData.objGenerateSAR
                '.Transaction_Code = sar_jenisLaporan.SelectedItem.Value
                '.Date_Report = sar_TanggalLaporan.Value
                '.total_transaction = ObjSARData.listObjGenerateSARTransaction.Count

                If ListIndicator.Count > 0 Then
                    Dim i As Integer
                    Dim temp As String
                    i = 0
                    temp = ""
                    For Each item In ListIndicator
                        i += 1
                        If ListIndicator.Count = 1 Then
                            temp = item.FK_Indicator
                        Else
                            If i = 1 Then
                                temp = item.FK_Indicator
                            Else
                                temp += "," + item.FK_Indicator
                            End If
                        End If
                    Next
                    .indicator = temp
                End If

                If .Date_Report <> ObjGenerateSAROld.Date_Report Then
                    IsChange = False
                End If
                If .Transaction_Code <> ObjGenerateSAROld.Transaction_Code Then
                    IsChange = False
                End If

            End With

            For Each item In ListDokumen
                ObjSARData.listObjDokumen.Add(item)
            Next

            If IsChange Then
                Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
            End If

            Dim listApproval As New List(Of NawaDevDAL.ModuleApproval)
            listApproval = NawaDevBLL.GenerateSarBLL.getListModuleApproval(ObjModule.ModuleName, ObjSARData.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR).ToList

            If listApproval.Count > 0 Then
                Throw New ApplicationException("There is Already Pending Approval Please Confirm Approval First Before Do Another Change For This Data")
            End If

            If Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                'Dim path As String = ConfigurationManager.AppSettings("FilePathGenerateSARAttachment").ToString
                'Dim path As String = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(9018).SettingValue
                'NawaDevBLL.GenerateSarBLL.SaveGenerateSarTanpaApproval(ObjSARData, ObjModule, 2, path)
                'NawaDevBLL.GenerateSarBLL.SaveEditTanpaApproval(objSAR, ListobjSarTransaction, ObjModule)

                Dim DateFrom As New DateTime
                Dim DateTo As New DateTime

                NawaDevBLL.GenerateSarBLL.SaveGenerateSTRSarTanpaApproval(ObjSARData, ObjModule, 2, DateFrom, DateTo)
                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Saved into Database."
            Else
                ObjSARData.objGenerateSAR.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                ObjSARData.objGenerateSAR.LastUpdateDate = DateTime.Now

                'NawaDevBLL.GenerateSarBLL.SaveGenerateSarApproval(ObjSARData, ObjModule, 2)
                'NawaDevBLL.GenerateSarBLL.SaveEditApproval(objSAR, ListobjSarTransaction, ObjModule)
                'goAML_SarBLL.EditApproval(objSAR, ObjModule)

                NawaDevBLL.GenerateSarBLL.SaveGenerateSTRSarApproval(ObjSARData, ObjModule, 2)
                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Saved into Pending Approval"
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub btn_addTransaksi_click(sender As Object, e As DirectEventArgs)
        Try
            WindowTransaction.Hidden = False

            ListTransaction = NawaDevBLL.GenerateSarBLL.getListTransactionByCif(ObjSARData.objGenerateSAR.CIF_NO)

            For Each item In ObjSARData.listObjGenerateSARTransaction
                ListTransaction.Remove(ListTransaction.Where(Function(x) x.Transaction_Number = item.Transaction_Number).FirstOrDefault)
                'ObjSARData.listObjGenerateSARTransaction.Remove(ObjSARData.listObjGenerateSARTransaction.Where(Function(x) x.Transaction_Number = item.Transaction_Number).FirstOrDefault)
            Next

            bindOdmTransaction(StoreOdmTransaction, ListTransaction)

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_addIndicator_click(sender As Object, e As DirectEventArgs)
        Try
            IsChange = False
            sar_reportIndicator.Clear()
            WindowReportIndicator.Hidden = False
            sar_reportIndicator.Selectable = True
            BtnsaveReportIndicator.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAdd_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim str As String
            Dim Arrstr() As String
            Dim i As Integer
            i = 0
            str = ""
            For Each item In ListSelectedOdmTransaction
                i += 1
                If ListSelectedOdmTransaction.Count = 1 Then
                    str = item.NO_ID
                Else
                    If i = 1 Then
                        str = item.NO_ID
                    Else
                        str += "," + item.NO_ID.ToString
                    End If
                End If
            Next
            Arrstr = str.Split(",")
            Dim id As Integer = 0
            Dim AccountFrom As New goAML_Ref_Account
            Dim AccountTo As New goAML_Ref_Account
            Dim Conductor As New goAML_Ref_WIC
            Dim AccountWIC As New goAML_Ref_WIC
            Dim WICFrom As New goAML_Ref_WIC
            Dim WICTo As New goAML_Ref_WIC

            For Each item In Arrstr
                Dim trn As New goAML_ODM_Generate_STR_SAR_Transaction
                Dim odmTrn As New goAML_ODM_Transaksi
                id += 1
                odmTrn = NawaDevBLL.GenerateSarBLL.getTransactionByPk(item)
                If Not odmTrn.NO_ID = Nothing Then
                    With odmTrn
                        trn.Pk_goAML_ODM_Generate_STR_SAR_Transaction = ObjSARData.listObjGenerateSARTransaction.Select(Function(x) x.Pk_goAML_ODM_Generate_STR_SAR_Transaction).DefaultIfEmpty(0).Max() + 1
                        trn.Fk_goAML_Generate_STR_SAR = ObjSARData.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR
                        If .Date_Transaction <> "" Then
                            trn.Date_Transaction = .Date_Transaction
                        End If
                        trn.CIF_NO = .CIF_NO
                        trn.Account_NO = .Account_NO
                        trn.WIC_NO = .WIC_No
                        trn.Conductor_ID = .Conductor_ID
                        trn.Ref_Num = .Ref_Num
                        trn.Debit_Credit = .Debit_Credit
                        trn.Original_Amount = .Original_Amount
                        trn.Currency = .Currency
                        trn.Exchange_Rate = .Exchange_Rate
                        trn.IDR_Amount = .IDR_Amount
                        trn.Transaction_Code = .Transaction_Code
                        trn.Source_Data = .Source_Data
                        trn.Transaction_Remark = .Transaction_Remark
                        trn.Transaction_Number = .Transaction_Number
                        trn.Transaction_Location = .Transaction_Location
                        trn.country_code = .Country_Code
                        trn.country_code_lawan = .Country_Code_Lawan
                        trn.Teller = .Teller
                        trn.Authorized = .Authorized
                        trn.Date_Posting = .Date_Posting
                        trn.Transmode_Code = .Transmode_Code
                        trn.Transmode_Comment = .Transmode_Comment
                        trn.Comments = .Comments
                        trn.CIF_No_Lawan = .CIF_No_Lawan
                        trn.ACCOUNT_No_Lawan = .ACCOUNT_No_Lawan
                        trn.WIC_No_Lawan = .WIC_No_Lawan
                        trn.MsgTypeSwift = .MsgSwiftType
                        trn.From_Funds_Code = .From_Funds_Code
                        trn.To_Funds_Code = .To_Funds_Code
                        trn.Currency_Lawan = .Currency_Lawan
                        trn.BiMultiParty = .BiMultiParty
                        trn.GCN = .GCN
                        trn.Active = .Active

                        If .Transaction_Number Is Nothing Then
                            Throw New ApplicationException("Ada Data Transaksi Yang Tidak Memiliki Transaction Number")
                        End If
                        If .Account_NO Is Nothing And .WIC_No Is Nothing Then
                            Throw New ApplicationException("Nomor Transaksi " + .Transaction_Number + " Harus Memiliki Account Number Pengirim Atau WIC Number Pengirim")
                        End If
                        If .Account_NO IsNot Nothing And .WIC_No IsNot Nothing Then
                            Throw New ApplicationException("Nomor Transaksi " + .Transaction_Number + " Hanya Boleh Ada Salah Satu Account Number Pengirim Atau WIC Number Pengirim")
                        End If
                        If .ACCOUNT_No_Lawan IsNot Nothing And .WIC_No_Lawan IsNot Nothing Then
                            Throw New ApplicationException("Nomor Transaksi " + .Transaction_Number + " Hanya Boleh Ada Salah Satu Account Number Penerima Atau WIC Number Penerima")
                        End If

                        If (.ACCOUNT_No_Lawan IsNot Nothing Or .WIC_No_Lawan IsNot Nothing) And .Country_Code_Lawan Is Nothing Then
                            Throw New ApplicationException("Nomor Transaksi " + .Transaction_Number + " Tidak Memiliki Lokasi Lawan")
                        End If

                        If .Conductor_ID IsNot Nothing Then
                            Conductor = NawaDevBLL.GenerateSarBLL.getWICByNo(.Conductor_ID)

                            If Conductor.WIC_No Is Nothing Then
                                Throw New ApplicationException("Nomor Transaksi " + .Transaction_Number + "Dengan Conductor ID " + .Conductor_ID + " Tidak Memiliki Data Di Master WIC")
                            Else
                                If Conductor.FK_Customer_Type_ID Is Nothing Then
                                    Throw New ApplicationException("Nomor Transaksi " + .Transaction_Number + "Dengan Conductor ID " + .Conductor_ID + " Tidak Memiliki Customer Type")
                                End If
                            End If
                        End If

                        If .Account_NO IsNot Nothing Then
                            AccountFrom = NawaDevBLL.GenerateSarBLL.getAccountByNo(.Account_NO)

                            If AccountFrom.Account_No Is Nothing Then
                                Throw New ApplicationException("Nomor Transaksi " + .Transaction_Number + "Dengan Account Number " + .Account_NO + " Tidak Memiliki Data Di Master Account")
                            End If
                        End If
                        If .WIC_No IsNot Nothing Then
                            WICFrom = NawaDevBLL.GenerateSarBLL.getWICByNo(.WIC_No)

                            If WICFrom.WIC_No Is Nothing Then
                                Throw New ApplicationException("Nomor Transaksi " + .Transaction_Number + "Dengan WIC Number " + .WIC_No + " Tidak Memiliki Data Di Master WIC")
                            Else
                                If WICFrom.FK_Customer_Type_ID Is Nothing Then
                                    Throw New ApplicationException("Nomor Transaksi " + .Transaction_Number + "Dengan Conductor ID " + .WIC_No + " Tidak Memiliki Customer Type")
                                End If
                            End If
                        End If

                        If .ACCOUNT_No_Lawan IsNot Nothing Then
                            If .Country_Code_Lawan = "ID" Then
                                AccountTo = NawaDevBLL.GenerateSarBLL.getAccountByNo(.ACCOUNT_No_Lawan)

                                If AccountTo.Account_No Is Nothing Then
                                    Throw New ApplicationException("Nomor Transaksi " + .Transaction_Number + "Dengan Account Number " + .ACCOUNT_No_Lawan + " Tidak Memiliki Data Di Master Account")
                                End If
                            Else
                                AccountWIC = NawaDevBLL.GenerateSarBLL.getWICByNo(.ACCOUNT_No_Lawan)

                                If AccountWIC.WIC_No Is Nothing Then
                                    Throw New ApplicationException("Nomor Transaksi " + .Transaction_Number + "Dengan Account Number " + .ACCOUNT_No_Lawan + " Tidak Memiliki Data Di Master WIC")
                                End If
                            End If


                        End If
                        If .WIC_No_Lawan IsNot Nothing Then
                            WICTo = NawaDevBLL.GenerateSarBLL.getWICByNo(.WIC_No_Lawan)

                            If WICTo.WIC_No Is Nothing Then
                                Throw New ApplicationException("Nomor Transaksi " + .Transaction_Number + "Dengan WIC Number " + .WIC_No_Lawan + " Tidak Memiliki Data Di Master WIC")
                            Else
                                If WICTo.FK_Customer_Type_ID Is Nothing Then
                                    Throw New ApplicationException("Nomor Transaksi " + .Transaction_Number + "Dengan Conductor ID " + .WIC_No_Lawan + " Tidak Memiliki Customer Type")
                                End If
                            End If
                        End If

                        AccountFrom = New goAML_Ref_Account
                        AccountTo = New goAML_Ref_Account
                        Conductor = New goAML_Ref_WIC
                        AccountWIC = New goAML_Ref_WIC
                        WICFrom = New goAML_Ref_WIC
                        WICTo = New goAML_Ref_WIC
                    End With

                    ObjSARData.listObjGenerateSARTransaction.Add(trn)
                    ListobjSarTransaction.Add(trn)
                End If

            Next

            bindTransaction(StoreTransaction, ObjSARData.listObjGenerateSARTransaction)

            WindowTransaction.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtncancelTransaction_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowTransaction.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                IsChange = False
                ListDokumen.Remove(ListDokumen.Where(Function(x) x.PK_ID = ID).FirstOrDefault)
                bindAttachment(StoreAttachment, ListDokumen)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                IsChange = False
                editDokumen(ID, "Edit")
                IDDokumen = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editDokumen(ID, "Detail")
            ElseIf e.ExtraParams(1).Value = "Download" Then
                DownloadFile(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editDokumen(id As String, command As String)
        Try
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()

            WindowAttachment.Hidden = False
            Dim Dokumen As New NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment
            Dokumen = ListDokumen.Where(Function(x) x.PK_ID = id).FirstOrDefault

            txtFileName.Text = Dokumen.File_Name
            txtKeterangan.Text = Dokumen.Remarks

            If command = "Detail" Then
                FileDoc.Hidden = True
                BtnsaveAttachment.Hidden = True
                txtKeterangan.Editable = False
            Else
                FileDoc.Hidden = False
                BtnsaveAttachment.Hidden = False
                txtKeterangan.Editable = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DownloadFile(id As Long)
        Dim objdownload As NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment = ListDokumen.Find(Function(x) x.PK_ID = id)
        If Not objdownload Is Nothing Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & objdownload.File_Name)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "ContentType"
            Response.BinaryWrite(objdownload.File_Doc)
            Response.End()
        End If
    End Sub
    Protected Sub GridcommandReportIndicator(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                IsChange = False
                ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = ID).FirstOrDefault)
                bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                IsChange = False
                editIndicator(ID, "Edit")
                IDIndicator = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editIndicator(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveReportIndicator_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim KodeIndicator As String = ""
            If String.IsNullOrEmpty(sar_reportIndicator.SelectedItem.Value) Then
                Throw New ApplicationException(sar_reportIndicator.FieldLabel + " Tidak boleh kosong")
            End If

            Dim reportIndicator As New NawaDevDAL.goAML_Report_Indicator
            If IDIndicator IsNot Nothing Or IDIndicator <> "" Then
                Dim IndicatorOld As New goAML_Report_Indicator
                IndicatorOld = ListIndicator.Where(Function(x) x.PK_Report_Indicator = IDIndicator).FirstOrDefault
                KodeIndicator = IndicatorOld.FK_Indicator
            End If

            reportIndicator.FK_Indicator = sar_reportIndicator.SelectedItem.Value

            If IDIndicator IsNot Nothing Or IDIndicator <> "" Then
                If KodeIndicator = reportIndicator.FK_Indicator Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = IDIndicator).FirstOrDefault)
                End If
            End If

            If ListIndicator.Count = 0 Then
                reportIndicator.PK_Report_Indicator = -1
            ElseIf ListIndicator.Count >= 1 Then
                reportIndicator.PK_Report_Indicator = ListIndicator.Min(Function(x) x.PK_Report_Indicator) - 1
            End If

            ListIndicator.Add(reportIndicator)
            bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            WindowReportIndicator.Hidden = True
            IDIndicator = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelreportIndicator_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowReportIndicator.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub OnSelect_Trn_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim data As New goAML_ODM_Transaksi

            data.NO_ID = e.ExtraParams(0).Value
            'data.Date_Posting = Convert.ToDateTime(e.ExtraParams(1).Value)
            'data.Account_NO = Convert.ToInt64(e.ExtraParams(2).Value)
            'data.Transaction_Code = e.ExtraParams(3).Value
            'data.Date_Transaction = Convert.ToDateTime(e.ExtraParams(4).Value)
            'data.Transmode_Code = e.ExtraParams(1).Value
            'data.Transaction_Number = Convert.ToInt64(e.ExtraParams(2).Value)
            'data.Transmode_Code = e.ExtraParams(3).Value
            'data.IDR_Amount = Convert.ToInt64(e.ExtraParams(4).Value)

            ListSelectedOdmTransaction.Add(data)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub OnDeSelect_Trn_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim data As New goAML_ODM_Transaksi

            For Each item In ListSelectedOdmTransaction
                If item.NO_ID = CInt(e.ExtraParams(0).Value) Then
                    data = item
                    Exit For
                End If
            Next

            ListSelectedOdmTransaction.Remove(data)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandTransaksi(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ObjSARData.listObjGenerateSARTransaction.Remove(ObjSARData.listObjGenerateSARTransaction.Where(Function(x) x.Pk_goAML_ODM_Generate_STR_SAR_Transaction = id).FirstOrDefault)
                ListobjSarTransaction.Remove(ListobjSarTransaction.Where(Function(x) x.Pk_goAML_ODM_Generate_STR_SAR_Transaction = id).FirstOrDefault)
                bindTransaction(StoreTransaction, ObjSARData.listObjGenerateSARTransaction)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim status As Boolean = False
            Dim DokumenOld As New NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment
            Dim Dokumen As New NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment
            If IDDokumen IsNot Nothing Or IDDokumen <> "" Then
                status = True
                DokumenOld = ListDokumen.Where(Function(x) x.PK_ID = IDDokumen).FirstOrDefault
            ElseIf Not FileDoc.HasFile Then
                Throw New Exception("Please Upload File")
            End If

            Dim docName As String = IO.Path.GetFileName(FileDoc.FileName)
            For Each item In ListDokumen
                If item.File_Name = docName Then
                    Throw New ApplicationException(docName + " Sudah Ada")
                End If
            Next

            If status = True And Not FileDoc.HasFile Then
                Dokumen.File_Name = DokumenOld.File_Name
                Dokumen.File_Doc = DokumenOld.File_Doc
            Else
                Dokumen.File_Name = IO.Path.GetFileName(FileDoc.FileName)
                Dokumen.File_Doc = FileDoc.FileBytes
            End If
            Dokumen.Remarks = txtKeterangan.Text.Trim

            If IDDokumen IsNot Nothing Or IDDokumen <> "" Then
                If Dokumen.File_Name = DokumenOld.File_Name And Dokumen.Remarks = DokumenOld.Remarks Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    ListDokumen.Remove(ListDokumen.Where(Function(x) x.PK_ID = IDDokumen).FirstOrDefault)
                End If
            End If

            If ListDokumen.Count = 0 Then
                Dokumen.PK_ID = -1
            ElseIf ListIndicator.Count >= 1 Then
                Dokumen.PK_ID = ListDokumen.Min(Function(x) x.PK_ID) - 1
            End If

            Dokumen.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            Dokumen.CreatedDate = DateTime.Now

            ListDokumen.Add(Dokumen)

            bindAttachment(StoreAttachment, ListDokumen)
            WindowAttachment.Hidden = True
            IDDokumen = Nothing

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAttachment.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_addAttachment_click(sender As Object, e As DirectEventArgs)
        Try
            IsChange = False
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()
            WindowAttachment.Hidden = False
            FileDoc.Hidden = False
            BtnsaveAttachment.Hidden = False
            txtKeterangan.Editable = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
#Region "27-Dec-2022 Felix, Thanks To Try. Ubah load ke grid nya pakai StoreReadData"
    Protected Sub Store_ReadDataTrans(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)
            Dim strsort As String = ""
            Dim CustomerID As String = FK_ReportSTRSAR
            Dim datefrom As Date = Date_From
            Dim dateto As Date = Date_To
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter


            If String.IsNullOrEmpty(strWhereClause) Then
                strWhereClause = " Fk_goAML_Generate_STR_SAR = '" & CustomerID & "'"
            Else
                strWhereClause += " AND Fk_goAML_Generate_STR_SAR = '" & CustomerID & "'"
            End If


            Me.strOrder = strsort
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_strsar_approval", "NO_ID,Valid,NoTran,NoRefTran,TipeTran,LokasiTran,KeteranganTran,DateTransaction,AccountNo,NamaTeller,NamaPejabat,TglPembukuan,CaraTranDilakukan,CaraTranLain,DebitCredit,OriginalAmount,IDR", strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            '-- start paging ----------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            '-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridPaneldetail.GetStore.DataSource = DataPaging
            GridPaneldetail.GetStore.DataBind()

            'End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadDataTransODM(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)
            Dim strsort As String = ""
            Dim CustomerID As String = FK_ReportSTRSAR
            Dim datefrom As Date = Date_From
            Dim dateto As Date = Date_To
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter


            If String.IsNullOrEmpty(strWhereClause) Then
                strWhereClause = " Fk_goAML_Generate_STR_SAR = '" & CustomerID & "'"
            Else
                strWhereClause += " AND Fk_goAML_Generate_STR_SAR = '" & CustomerID & "'"
            End If


            Me.strOrder = strsort
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_strsar_approval", "NO_ID,Valid,NoTran,NoRefTran,TipeTran,LokasiTran,KeteranganTran,DateTransaction,AccountNo,NamaTeller,NamaPejabat,TglPembukuan,CaraTranDilakukan,CaraTranLain,DebitCredit,OriginalAmount,IDR", strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            '-- start paging ----------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            '-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridTransaction.GetStore.DataSource = DataPaging
            GridTransaction.GetStore.DataBind()

            'End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class
