﻿Imports System.Data
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevDAL
Imports NawaBLL
Imports Ext
Imports NawaDevBLL
Imports System.Data.SqlClient
Imports NawaDAL
Imports Elmah
Partial Class goAML_GenerateSARDetail
    Inherits Parent
#Region "Session"
    Private _IDReq As Long
    Public Property IDReq() As Long
        Get
            Return _IDReq
        End Get
        Set(ByVal value As Long)
            _IDReq = value
        End Set
    End Property
    '' Add 27-Dec-2022 27-Dec-2022 Felix, Thanks To Try. Ubah load ke grid nya pakai StoreReadData
    Public Property strWhereClause() As String
        Get
            Return Session("goAML_GenerateSARDetail.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSARDetail.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("goAML_GenerateSARDetail.strSort")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSARDetail.strSort") = value
        End Set
    End Property

    Public Property FK_ReportSTRSAR() As String
        Get
            Return Session("goAML_GenerateSARDetail.FK_ReportSTRSAR")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSARDetail.FK_ReportSTRSAR") = value
        End Set
    End Property

    Public Property Date_From() As Date
        Get
            Return Session("goAML_GenerateSARDetail.Date_From")
        End Get
        Set(ByVal value As Date)
            Session("goAML_GenerateSARDetail.Date_From") = value
        End Set
    End Property

    Public Property Date_To() As Date
        Get
            Return Session("goAML_GenerateSARDetail.Date_To")
        End Get
        Set(ByVal value As Date)
            Session("goAML_GenerateSARDetail.Date_To") = value
        End Set
    End Property

    Public Property indexStart() As String
        Get
            Return Session("goAML_GenerateSARDetail.indexStart")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSARDetail.indexStart") = value
        End Set
    End Property

    '' End 27-Dec-2022
    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("goAML_GenerateSARDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_GenerateSARDetail.ObjModule") = value
        End Set
    End Property
    Public Property ObjGenerateSAR As NawaDevDAL.goAML_ODM_Generate_STR_SAR
        Get
            Return Session("goAML_GenerateSARDetail.ObjGenerateSAR")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_ODM_Generate_STR_SAR)
            Session("goAML_GenerateSARDetail.ObjGenerateSAR") = value
        End Set
    End Property
    Public Property ObjSARData As NawaDevBLL.GenerateSARData
        Get
            Return Session("goAML_GenerateSARDetail.ObjSARData")
        End Get
        Set(ByVal value As NawaDevBLL.GenerateSARData)
            Session("goAML_GenerateSARDetail.ObjSARData") = value
        End Set
    End Property
    Public Property ListIndicator As List(Of NawaDevDAL.goAML_Report_Indicator)
        Get
            Return Session("goAML_GenerateSARDetail.ListIndicator")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Report_Indicator))
            Session("goAML_GenerateSARDetail.ListIndicator") = value
        End Set
    End Property
    Public Property ListDokumen As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment)
        Get
            Return Session("goAML_GenerateSARDetail.ListDokumen")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment))
            Session("goAML_GenerateSARDetail.ListDokumen") = value
        End Set
    End Property
    Public Property IDDokumen As String
        Get
            Return Session("goAML_GenerateSARDetail.IDDokumen")
        End Get
        Set(value As String)
            Session("goAML_GenerateSARDetail.IDDokumen") = value
        End Set
    End Property
#End Region

#Region "Load Data"
    Private Sub goAML_GenerateSARDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer

            intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)


            If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Detail) Then
                Dim strIDCode As String = 1
                strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                Exit Sub
            End If
            If Not Ext.Net.X.IsAjaxRequest Then
                FormPanelInput.Title = ObjModule.ModuleLabel & " Detail"
                clearSession()
                LoadData()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadApprovalHistory(modulId As String, sarId As String)
        Try
            Dim objApprovalHistory As New List(Of goAML_Module_Note_History)
            objApprovalHistory = NawaDevBLL.GenerateSarBLL.getListApprovalHistory(modulId, sarId)
            If objApprovalHistory.Count > 0 Then
                gpStore_history.DataSource = objApprovalHistory
                gpStore_history.DataBind()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadData()
        Dim IDData As String = Request.Params("ID")
        Dim Arrstr() As String
        Dim num As Integer = 1

        IDReq = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)

        ObjSARData.objGenerateSAR = NawaDevBLL.GenerateSarBLL.getGenerateSARByPk(IDReq)
        ObjSARData.listObjGenerateSARTransaction = NawaDevBLL.GenerateSarBLL.getListSarTransactionByFk(IDReq)
        '' Add 27-Dec-2022
        Date_From = ObjSARData.listObjGenerateSARTransaction.Max(Function(x) x.Date_Transaction)
        Date_To = ObjSARData.listObjGenerateSARTransaction.Min(Function(x) x.Date_Transaction)
        '' End 27-Dec-2022

        'bindTransaction(StoreTransaction, ObjSARData.listObjGenerateSARTransaction)

        'ObjSARData.listObjDokumen = NawaDevBLL.GenerateSarBLL.getListGenerateSARDokumen(IDReq)
        'bindAttachment(StoreAttachment, ObjSARData.listObjDokumen)

        ListDokumen = NawaDevBLL.GenerateSarBLL.getListGenerateSARDokumen(IDReq)
        bindAttachment(StoreAttachment, ListDokumen)

        With ObjSARData.objGenerateSAR
            FK_ReportSTRSAR = .PK_goAML_ODM_Generate_STR_SAR '' Add 27-Dec-2022
            sar_PK.Text = .PK_goAML_ODM_Generate_STR_SAR
            sar_cif.Text = .CIF_NO + " - " + GenerateSarBLL.getCustomerNameByCif(.CIF_NO)
            sar_laporan.Text = NawaDevBLL.GlobalReportFunctionBLL.getJenisLaporanByCode(.Transaction_Code)
            If .Date_Report IsNot Nothing Then
                sar_TanggalLaporan.Text = .Date_Report.Value.ToString("dd-MMM-yyyy")
            End If
            NoRefPPATK.Text = .Fiu_Ref_Number
            Alasan.Text = .Reason
            If .indicator IsNot Nothing Then

                Arrstr = .indicator.Split(",")
                For Each item In Arrstr
                    Dim Indikator As New NawaDevDAL.goAML_Report_Indicator
                    Indikator.PK_Report_Indicator = num
                    Indikator.FK_Indicator = item
                    ListIndicator.Add(Indikator)
                    num += 1
                Next
                bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            End If
        End With
        LoadApprovalHistory(ObjModule.PK_Module_ID, IDReq)
    End Sub
    Sub bindAttachment(store As Ext.Net.Store, listAttachment As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAttachment)
        objtable.Columns.Add(New DataColumn("FileName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("FileName") = item("File_Name")
                item("Keterangan") = item("Remarks")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindTransaction(store As Ext.Net.Store, listTransaction As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listTransaction)
        objtable.Columns.Add(New DataColumn("NoTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoRefTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("TipeTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("LokasiTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("KeteranganTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("DateTransaction", GetType(Date)))
        objtable.Columns.Add(New DataColumn("AccountNo", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaTeller", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaPejabat", GetType(String)))
        objtable.Columns.Add(New DataColumn("TglPembukuan", GetType(Date)))
        objtable.Columns.Add(New DataColumn("CaraTranDilakukan", GetType(String)))
        objtable.Columns.Add(New DataColumn("CaraTranLain", GetType(String)))
        objtable.Columns.Add(New DataColumn("DebitCredit", GetType(String)))
        objtable.Columns.Add(New DataColumn("OriginalAmount", GetType(String)))
        objtable.Columns.Add(New DataColumn("IDR", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("NoTran") = item("Transaction_Number")
                item("NoRefTran") = item("Ref_Num")
                Dim SourceData As String = item("Source_Data").ToString
                Dim SourceDataTemp As Integer
                If Integer.TryParse(SourceData, SourceDataTemp) Then
                    If SourceData <> "" Then
                        item("TipeTran") = NawaDevBLL.GenerateSarBLL.getSourceData(SourceData)
                    End If
                Else
                    item("TipeTran") = SourceData
                End If
                item("LokasiTran") = item("Transaction_Location")
                item("KeteranganTran") = item("Transaction_Remark")
                item("DateTransaction") = item("Date_Transaction")
                item("AccountNo") = item("Account_NO")
                item("NamaTeller") = item("Teller")
                item("NamaPejabat") = item("Authorized")
                item("TglPembukuan") = item("Date_Posting")
                item("CaraTranDilakukan") = item("Transmode_Code")
                item("CaraTranLain") = item("Transmode_Comment")
                item("DebitCredit") = item("Debit_Credit")
                If item("Original_Amount").ToString <> "" Then
                    Dim OriginalAmount As Decimal = item("Original_Amount").ToString
                    item("OriginalAmount") = OriginalAmount.ToString("#,###.00")
                End If
                If item("IDR_Amount").ToString <> "" Then
                    Dim IDR As Decimal = item("IDR_Amount").ToString
                    item("IDR") = IDR.ToString("#,###.00")
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindReportIndicator(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Report_Indicator))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        '  objtable.Columns.Add(New DataColumn("Kode_Indicator", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                '     item("Kode_Indicator") = item("FK_Indicator")
                item("Keterangan") = NawaDevBLL.GenerateSarBLL.getReportIndicatorByKode(item("FK_Indicator").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

#End Region

#Region "Method"
    Sub clearSession()
        ObjGenerateSAR = New NawaDevDAL.goAML_ODM_Generate_STR_SAR
        ObjSARData = New NawaDevBLL.GenerateSARData
        ListIndicator = New List(Of NawaDevDAL.goAML_Report_Indicator)
        ListDokumen = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment)
    End Sub

    Protected Sub GridcommandAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ListDokumen.Remove(ListDokumen.Where(Function(x) x.PK_ID = ID).FirstOrDefault)
                bindAttachment(StoreAttachment, ListDokumen)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                editDokumen(ID, "Edit")
                IDDokumen = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editDokumen(ID, "Detail")
            ElseIf e.ExtraParams(1).Value = "Download" Then
                DownloadFile(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub editDokumen(id As String, command As String)
        Try
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()

            WindowAttachment.Hidden = False
            Dim Dokumen As New NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment
            Dokumen = ListDokumen.Where(Function(x) x.PK_ID = id).FirstOrDefault

            txtFileName.Text = Dokumen.File_Name
            txtKeterangan.Text = Dokumen.Remarks

            If command = "Detail" Then
                FileDoc.Hidden = True
                BtnsaveAttachment.Hidden = True
                txtKeterangan.Editable = False
            Else
                FileDoc.Hidden = False
                BtnsaveAttachment.Hidden = False
                txtKeterangan.Editable = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DownloadFile(id As Long)
        Dim objdownload As NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment = ListDokumen.Find(Function(x) x.PK_ID = id)
        If Not objdownload Is Nothing Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & objdownload.File_Name)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "ContentType"
            Response.BinaryWrite(objdownload.File_Doc)
            Response.End()
        End If
    End Sub
    Protected Sub btnSaveAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            If Not FileDoc.HasFile Then
                Throw New Exception("Please Upload File")
            End If

            Dim Dokumen As New NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment
            If IDDokumen IsNot Nothing Or IDDokumen <> "" Then
                ListDokumen.Remove(ListDokumen.Where(Function(x) x.PK_ID = IDDokumen).FirstOrDefault)
            End If

            If ListDokumen.Count = 0 Then
                Dokumen.PK_ID = -1
            ElseIf ListIndicator.Count >= 1 Then
                Dokumen.PK_ID = ListDokumen.Min(Function(x) x.PK_ID) - 1
            End If

            Dokumen.File_Name = IO.Path.GetFileName(FileDoc.FileName)
            Dokumen.File_Doc = FileDoc.FileBytes
            Dokumen.Remarks = txtKeterangan.Text.Trim

            Dokumen.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            Dokumen.CreatedDate = DateTime.Now

            ListDokumen.Add(Dokumen)

            bindAttachment(StoreAttachment, ListDokumen)
            WindowAttachment.Hidden = True
            IDDokumen = Nothing

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAttachment.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Direct Event"
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "27-Dec-2022 Felix, Thanks To Try. Ubah load ke grid nya pakai StoreReadData"
    Protected Sub Store_ReadDataTrans(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)
            Dim strsort As String = ""
            Dim CustomerID As String = FK_ReportSTRSAR
            Dim datefrom As Date = Date_From
            Dim dateto As Date = Date_To
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter


            If String.IsNullOrEmpty(strWhereClause) Then
                strWhereClause = " Fk_goAML_Generate_STR_SAR = '" & CustomerID & "'"
            Else
                strWhereClause += " AND Fk_goAML_Generate_STR_SAR = '" & CustomerID & "'"
            End If


            Me.strOrder = strsort
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_strsar_approval", "NO_ID,Valid,NoTran,NoRefTran,TipeTran,LokasiTran,KeteranganTran,DateTransaction,AccountNo,NamaTeller,NamaPejabat,TglPembukuan,CaraTranDilakukan,CaraTranLain,DebitCredit,OriginalAmount,IDR", strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            '-- start paging ----------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            '-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridPaneldetail.GetStore.DataSource = DataPaging
            GridPaneldetail.GetStore.DataBind()

            'End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class
