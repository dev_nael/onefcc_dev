﻿Imports Ext.Net
Imports OfficeOpenXml
Partial Class goAML_ReportingPersonView
    Inherits Parent
    Public objFormModuleView As NawaBLL.FormModuleView
    Public Property strWhereClause() As String
        Get
            Return Session("goAML_ReportingPersonView.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("goAML_ReportingPersonView.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("goAML_ReportingPersonView.strSort")
        End Get
        Set(ByVal value As String)
            Session("goAML_ReportingPersonView.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("goAML_ReportingPersonView.indexStart")
        End Get
        Set(ByVal value As String)
            Session("goAML_ReportingPersonView.indexStart") = value
        End Set
    End Property
    'Private Sub Parameterview_Init(sender As Object, e As EventArgs) Handles Me.Init
    '    objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer
            Try
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                    Dim strIDCode As String = 1
                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If

                objFormModuleView.ModuleID = objmodule.PK_Module_ID
                objFormModuleView.ModuleName = objmodule.ModuleName
                objFormModuleView.AddField("PK_ODM_Reporting_Person_ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.IDENTITY)
                objFormModuleView.AddField("UserID", "User ID", 2, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("laporan", "Jenis Laporan", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("ssn", "NIK", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("last_name", "Nama Lengkap", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("birthdate", "Tanggal Lahir", 6, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                objFormModuleView.AddField("birth_place", "Tempat Lahir", 7, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("gender", "Jenis Kelamin", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Nationality1", "Kewarganegaraan", 9, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Residence", "Negara Domisili", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Occupation", "Pekerjaan", 11, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Tax_Reg_Number", "PEP", 12, False, True, NawaBLL.Common.MFieldType.VARCHARValue)

                objFormModuleView.SettingFormView()
            Catch ex As Exception

            End Try

            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
    '    Try
    '        Dim intStart As Integer = e.Start
    '        'If intStart = 0 Then intStart = 1
    '        Dim intLimit As Int16 = e.Limit
    '        Dim inttotalRecord As Integer
    '        Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e) '.Replace("FormInfo", "(gft.LHBUFormName + ' - ' + fi.Nama)")
    '        Dim strsort As String = ""
    '        For Each item As DataSorter In e.Sort
    '            strsort += item.Property & " " & item.Direction.ToString
    '        Next
    '        Me.indexStart = intStart
    '        Me.strWhereClause = strfilter
    '        Me.strOrder = strsort

    '        ' SELECT fieldString
    '        Dim fieldString As String = "" &
    '            "[PK_ODM_Reporting_Person_ID],[last_name],[birthdate],[birth_place]" &
    '            ",[gender],[Nationality1],[Residence],[Occupation],[Tax_Reg_Number]" &
    '            ",[id_number]"
    '        ' FROM tableString
    '        Dim tableString As String = "goAML_ODM_Reporting_Person"
    '        ' Split it into pages
    '        Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(tableString, fieldString, strfilter, strsort, intStart, intLimit, inttotalRecord)
    '        ''-- start paging ------------------------------------------------------------
    '        Dim limit As Integer = e.Limit
    '        If (e.Start + e.Limit) > inttotalRecord Then
    '            limit = inttotalRecord - e.Start
    '        End If
    '        'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
    '        ''-- end paging ------------------------------------------------------------
    '        e.Total = inttotalRecord
    '        GridpanelView.GetStore.DataSource = DataPaging
    '        GridpanelView.GetStore.DataBind()

    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            Using objdb As New NawaDevDAL.NawaDatadevEntities
                'Begin Penambahan Advanced Filter
                If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                    Toolbar2.Hidden = True
                Else
                    Toolbar2.Hidden = False
                End If
                LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")
                'END Penambahan Advanced Filter

                Dim intStart As Integer = e.Start
                Dim intLimit As Int16 = e.Limit
                Dim inttotalRecord As Integer
                Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

                strfilter = strfilter.Replace("Active", objFormModuleView.ModuleName & ".Active")



                Dim strsort As String = ""
                For Each item As DataSorter In e.Sort
                    strsort += item.Property & " " & item.Direction.ToString
                Next
                Me.indexStart = intStart
                Me.strWhereClause = strfilter
                'Begin Update Penambahan Advance Filter
                If strWhereClause.Length > 0 Then
                    If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                        strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                        intStart = 0
                    End If
                Else
                    If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                        strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                        intStart = 0
                    End If
                End If
                'END Penambahan Advanced Filter
                Me.strOrder = strsort
                If strsort = "" Then
                    strsort = "PK_ODM_Reporting_Person_ID asc"
                End If
                Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
                'Dim DataPaging As Data.DataTable = DLL.SQLHelper.ExecuteTabelPaging("CustomerInformation_WebTempTable", "CIFNo, Name, DateOfBirth, AccountOwnerId, OpeningDate, WorkingUnitName, IsCustomerInList", strfilter, strsort, intStart, intLimit, inttotalRecord)
                ''-- start paging ------------------------------------------------------------
                Dim limit As Integer = e.Limit
                If (e.Start + e.Limit) > inttotalRecord Then
                    limit = inttotalRecord - e.Start
                End If
                'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
                ''-- end paging ------------------------------------------------------------
                e.Total = inttotalRecord
                GridpanelView.GetStore.DataSource = DataPaging
                GridpanelView.GetStore.DataBind()
            End Using
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                objtbl.Columns.Remove(item.Text)

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=ReportingPerson.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                objtbl.Columns.Remove(item.Text)

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write(objtbl.Rows(i).Item(k).ToString + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=ReportingPerson.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                objtbl.Columns.Remove(item.Text)

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=ReportingPerson.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                objtbl.Columns.Remove(item.Text)

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write(objtbl.Rows(i).Item(k).ToString + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=ReportingPerson.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    <DirectMethod>
    Protected Sub BtnAdd_Click(sender As Object, e As DirectEventArgs)
        Try


            Dim Moduleid As String = Request.Params("ModuleID")


            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Parameterview_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub

    'Begin Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update Penambahan Advance Filter

End Class

