﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/Site1.Master" CodeFile="GeneratePPATKView.aspx.vb" Inherits="goAML_GeneratePPATKView" %>
<%--Begin Update Penambahan Advance Filter--%>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>
<%--End Update Penambahan Advance Filter--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordadValue) || "").indexOf(matchValue) > -1;
        };
        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
        var prepareCommandCollection = function (grid, toolbar, rowIndex, record) {
            var GenerateButton = toolbar.items.get(0);
            var EditButton = toolbar.items.get(1);

            //Edited By Felix 27 Nov 2020
            //var wf = record.data.Status
            var wf = record.data.Total_Valid
            
            <%--
                Daniel 5/11/2020
                add condition when status is Waiting For Approval
            --%>
            <%--if (EditButton != undefined) {
                if (wf == "Waiting For Generate" || wf == 'Reference No Uploaded' || wf == "Waiting For Approval") {
                    EditButton.setDisabled(true)
                }
            };
            if (GenerateButton != undefined) {
                if (wf == 'Need To Correction' || wf == 'Waiting For Approval') {
                    GenerateButton.setDisabled(true)
                }
            };--%>

            //Edited By Felix 27 Nov 2020
            if (GenerateButton != undefined) {
                if (wf == 0) {
                    GenerateButton.setDisabled(true)
                }
            };
            //End By Felix 27 Nov 2020
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<ext:GridPanel ID="GridpanelView" ClientIDMode="Static" runat="server" Title="List OF Generate goAML" Width="2000">
        <View>
            <ext:GridView runat="server" EnableTextSelection="true">
            </ext:GridView>
        </View>
        <Store>
            <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadData">
                  <Model>
                  <ext:Model runat="server">
                        <Fields>
                            <ext:ModelField Name="NO_ID" Type="Int" />
                            <ext:ModelField Name="Transaction_Date" Type="Date" />
                            <ext:ModelField Name="Jenis_Laporan" Type="String" />
                            <ext:ModelField Name="Last_Update_Date" Type="Date" />
                            <ext:ModelField Name="Report_Type" Type="String" /> 
                            <ext:ModelField Name="Total_Valid" Type="Int" />
                            <ext:ModelField Name="Total_Invalid" Type="Int" />
                            <ext:ModelField Name="Report_PPATK_Date" Type="Date" />
                            <ext:ModelField Name="Last_Generate_Date" Type="Date" />
                            <ext:ModelField Name="Status" Type="String" />
                            <ext:ModelField Name="StatusGenerateAPI" Type="String" />
                            <ext:ModelField Name="IsAlreadyValidateSchema" Type="String" />
                            <ext:ModelField Name="ValidateSchemaResultValid" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
                <Sorters></Sorters>
                <Proxy>
                    <ext:PageProxy />
                </Proxy>
            </ext:Store>
        </Store>
        <ColumnModel>
            <Columns>
                <ext:CommandColumn runat="server" ID="columncrud" Width="200" Text="Action">
                    <Commands>
                        <ext:GridCommand Icon="ApplicationViewDetail" Text="Generate" CommandName="Generate">
                        </ext:GridCommand>
                        <ext:GridCommand Icon="DiskUpload" Text="Submission" CommandName="Upload" Hidden="true">
                        </ext:GridCommand>
                    </Commands>
                    <DirectEvents>
                        <Command OnEvent="GridcommandListGenerateView">
                            <EventMask ShowMask="true"></EventMask>
                            <ExtraParams>
                                <ext:Parameter Name="unikkey" Value="record.data.NO_ID" Mode="Raw"></ext:Parameter>
                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                            </ExtraParams>
                        </Command>
                    </DirectEvents>
                </ext:CommandColumn>
                <ext:Column runat="server" DataIndex="NO_ID" Text="ID" ID="No" />
                <ext:DateColumn runat="server" DataIndex="Transaction_Date" Text="Transaction Date" Format="dd-MMM-yyyy" />
                <ext:Column runat="server" DataIndex="Jenis_Laporan" Text="Jenis Laporan" />
                <ext:DateColumn runat="server" DataIndex="Last_Update_Date" Text="Last Update Date" Format="dd-MMM-yyyy" />
                 <ext:DateColumn ID="Col_Last_Update_Date" runat="server" DataIndex="Last_Update_Date" Text="Last Update Date" MinWidth="120" Format="dd-MMM-yyyy">
                    <Items>
                    <ext:DateField ID="DateField3" runat="server" Format="dd-MMM-yyyy" FormatText="Expected Date Format dd-MMM-yyyy">
                    <Plugins>
                    <ext:ClearButton ID="Col_Last_Update_Date_ClearButton1">
                    </ext:ClearButton>
                    </Plugins>
                    </ext:DateField>
                    </Items>
                    <Filter>
                    <ext:DateFilter >
                    </ext:DateFilter>
                    </Filter>
                </ext:DateColumn>
                <ext:Column runat="server" DataIndex="Report_Type" Text="Report Type" />
                <ext:Column runat="server" DataIndex="Total_Valid" Text="Total Valid" />
                <ext:Column runat="server" DataIndex="Total_Invalid" Text="Total Invalid" />
                <ext:DateColumn runat="server" DataIndex="Report_PPATK_Date" Text="Report PPATK Date" Format="dd-MMM-yyyy" />
                 <ext:DateColumn ID="Col_Report_PPATK_Date" runat="server" DataIndex="Report_PPATK_Date" Text="Report PPATK Date" MinWidth="120" Format="dd-MMM-yyyy">
                    <Items>
                    <ext:DateField ID="DateField1" runat="server" Format="dd-MMM-yyyy" FormatText="Expected Date Format dd-MMM-yyyy">
                    <Plugins>
                    <ext:ClearButton ID="Col_Report_PPATK_Date_ClearButton1">
                    </ext:ClearButton>
                    </Plugins>
                    </ext:DateField>
                    </Items>
                    <Filter>
                    <ext:DateFilter >
                    </ext:DateFilter>
                    </Filter>
                </ext:DateColumn>
                <ext:DateColumn runat="server" DataIndex="Last_Generate_Date" Text="Last Generated Date" Format="dd-MMM-yyyy" />
                <ext:DateColumn ID="Col_Last_Generate_Date" runat="server" DataIndex="Last_Generate_Date" Text="Last Generated Date" MinWidth="120" Format="dd-MMM-yyyy">
                    <Items>
                    <ext:DateField ID="DateField2" runat="server" Format="dd-MMM-yyyy" FormatText="Expected Date Format dd-MMM-yyyy">
                    <Plugins>
                    <ext:ClearButton ID="Col_Last_Generate_Date_ClearButton1">
                    </ext:ClearButton>
                    </Plugins>
                    </ext:DateField>
                    </Items>
                    <Filter>
                    <ext:DateFilter >
                    </ext:DateFilter>
                    </Filter>
                </ext:DateColumn>
                <ext:Column runat="server" DataIndex="Status" Text="Status" />
                <ext:Column runat="server" DataIndex="StatusGenerateAPI" Text="Status API" />
                <ext:Column runat="server" DataIndex="IsAlreadyValidateSchema" Text="Already Validate Schema" />
                <ext:Column runat="server" DataIndex="ValidateSchemaResultValid" Text="Validate Schema Result" />
            </Columns>
        </ColumnModel>
        <Plugins>
            <ext:FilterHeader ID="grdiheaderfilter" runat="server" Remote="true"></ext:FilterHeader>
        </Plugins>
        <BottomBar>
            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
        </BottomBar>
        <DockedItems>
            <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                <Items>
                    <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export :">
                        <Items>
                            <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                            <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                        </Items>
                    </ext:ComboBox>
                    <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" AutoPostBack="true" OnClick="ExportExcel" ClientIDMode="Static" />
                    <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page" AutoPostBack="true" OnClick="ExportAllExcel" />
                    <ext:Button ID="BtnAdd" runat="server" Text="Add New Record" Icon="Add" Handler="NawadataDirect.BtnAdd_Click()" Hidden="true">
                    </ext:Button>
                    <ext:Button ID="AdvancedFilter" runat="server" Text="Advanced Filter" Icon="Add" Handler="NawadataDirect.BtnAdvancedFilter_Click()">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
            <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true" Dock="Top" Hidden="true">
                <Items>
                    <ext:HyperlinkButton ID="HyperlinkButton1" runat="server" Text="Clear Advanced Filter">
                        <DirectEvents>
                            <Click OnEvent="btnClear_Click"></Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                    <ext:Label ID="LblAdvancedFilter" runat="server" Text="">
                    </ext:Label>
                </Items>
            </ext:Toolbar>
        </DockedItems>
        <Listeners>
            <ViewReady Handler="columnAutoResize(this);
                    this.getStore().on('load', Ext.bind(columnAutoResize, null, [this]));"
                Delay="10" />
        </Listeners>
    </ext:GridPanel>--%>

    <%--Add 11-Mar-2022 Felix--%>
     <ext:GridPanel ID="GridpanelView" ClientIDMode="Static" runat="server" Title="Title" Width="2000">
        <View>
            <ext:GridView runat="server" EnableTextSelection="true" />
        </View>
        <Store>
            <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadData">

                <Sorters>
                    <%--<ext:DataSorter Property="" Direction="ASC" />--%>
                </Sorters>
                <Proxy>
                    <ext:PageProxy />
                </Proxy>
            </ext:Store>
        </Store>
        <SelectionModel>
            <ext:CheckboxSelectionModel runat="server" Mode="Multi" CheckOnly="true">
            </ext:CheckboxSelectionModel>
        </SelectionModel>
        <Plugins>
            <%--<ext:GridFilters ID="GridFilters1" runat="server" />--%>
            <ext:FilterHeader ID="grdiheaderfilter" runat="server" Remote="true"></ext:FilterHeader>

        </Plugins>
        <BottomBar>

            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />

        </BottomBar>

        <%--Begin Update Penambahan Advance Filter--%>
        <DockedItems>
            <%--End Update Penambahan Advance Filter--%>
            <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                <Items>

                    <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export :">
                        <Items>
                            <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                            <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                        </Items>
                    </ext:ComboBox>
                    <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" AutoPostBack="true" OnClick="ExportExcel" ClientIDMode="Static" />
                    <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page" AutoPostBack="true" OnClick="ExportAllExcel" />
                    <%--<ext:Button ID="Button1" runat="server" Text="Print Current Page" Icon="Printer" Handler="this.up('grid').print();" />--%>
                    <ext:Button ID="BtnAdd" runat="server" Text="Add New Record" Icon="Add" Handler="NawadataDirect.BtnAdd_Click()"  />
                    <%--Begin Update Penambahan Advance Filter--%>
                    <ext:Button ID="AdvancedFilter" runat="server" Text="Advanced Filter" Icon="Magnifier" Handler="NawadataDirect.BtnAdvancedFilter_Click()" />
                    <%--End Update Penambahan Advance Filter--%>

                </Items>
            </ext:Toolbar>
            <%--Begin Update Penambahan Advance Filter--%>
            <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true" Dock="Top" Hidden="true">
                <Items>
                    <ext:HyperlinkButton ID="HyperlinkButton1" runat="server" Text="Clear Advanced Filter">
                        <DirectEvents>
                            <Click OnEvent="btnClear_Click"></Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                    <ext:Label ID="LblAdvancedFilter" runat="server" Text="">
                    </ext:Label>
                </Items>
            </ext:Toolbar>
        </DockedItems>
        <%--End Update Penambahan Advance Filter--%>

        <Listeners>
            <ViewReady Handler="columnAutoResize(this);
                    this.getStore().on('load', Ext.bind(columnAutoResize, null, [this]));"
                Delay="10" />
        </Listeners>
        <%--<SelectionModel>
            <ext:CheckboxSelectionModel runat="server" Mode="Single">
            </ext:CheckboxSelectionModel>
        </SelectionModel>--%>
    </ext:GridPanel>
    <%--End 11-Mar-2022--%>

    <%--Begin Update Penambahan Advance Filter--%>
    <ext:Panel ID="Panel1" runat="server" Hidden="true">
        <Content>
            <uc1:AdvancedFilter runat="server" ID="AdvancedFilter1" />
        </Content>
        <Items>
        </Items>
    </ext:Panel>
    <%--End Update Penambahan Advance Filter--%>
</asp:Content>
