﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="WICEdit.aspx.vb" Inherits="goAML_WICEdit" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .x-form-item-label.x-form-item-label-default { width : 175px !important; }
    </style>
</asp:Content>

<%--add Agam 20102020
tambah Maximizable="true"  di semua windows
tambah ButtonAlign="Center" di semua formpanel windows
format DateINDV_Birthdate
combobox untuk negara diganti jadi ndsdropdown
--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <ext:Window ID="WindowDetailDirectorIdentification" runat="server" Icon="ApplicationViewDetail" Title="Director Identitas" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="200" Maximizable="true" BodyPadding="6" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:FormPanel ID="PanelDetailDirectorIdentification" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DsptTypeDirector" runat="server" FieldLabel="Jenis Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspNumberDirector" runat="server" FieldLabel="Nomor Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssueDateDirector" runat="server" FieldLabel="Tanggal Diterbitkan"></ext:DisplayField>
                    <ext:DisplayField ID="DspExpiryDateDirector" runat="server" FieldLabel="Tanggal Kadaluarsa"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedByDirector" runat="server" FieldLabel="Diterbitkan Oleh"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedCountryDirector" runat="server" FieldLabel="Negara Penerbit Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspIdentificationCommentDirector" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button6" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelIdentificationDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FormPanelDirectorIdentification" runat="server" Hidden="true" ButtonAlign="center" AutoScroll="true">
                <Items>
                    <ext:ComboBox ID="CmbTypeIdentificationDirector" runat="server" FieldLabel="Jenis Identitas" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="100%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreTypeIdentificationDirector" OnReadData="Identification_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="TxtNumberDirector" runat="server" FieldLabel="Nomor Identitas" AnchorHorizontal="100%" AllowBlank="false" MaxLength="255"></ext:TextField>
                    <ext:DateField ID="TxtIssueDateDirector" runat="server" Format="dd-MMM-yyyy" FieldLabel="Tanggal Diterbitkan" AnchorHorizontal="100%"></ext:DateField>
                    <ext:DateField ID="TxtExpiryDateDirector" runat="server" Format="dd-MMM-yyyy" FieldLabel="Tanggal Kadaluarsa" AnchorHorizontal="100%"></ext:DateField>
                    <ext:TextField ID="TxtIssuedByDirector" runat="server" FieldLabel="Diterbitkan Oleh" AnchorHorizontal="100%" MaxLength="255"></ext:TextField>
                    <%--<ext:ComboBox ID="CmbCountryIdentificationDirector" runat="server" FieldLabel="Negara Penerbit Identitas" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreCountryCodeIdentificationDirector" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel10" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbCountryIdentificationDirector" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Negara Penerbit Identitas" AnchorHorizontal="100%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <ext:TextArea ID="TxtCommentIdentificationDirector" runat="server" FieldLabel="Catatan" AnchorHorizontal="100%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="Button8" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelDirectorIdentification}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="BtnSaveDetailIdentificationDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button10" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDetailIdentificationDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailDirectorIdentification}.center()" /> 
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailIdentification" runat="server" Icon="ApplicationViewDetail" Title="Identitas" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="200" Maximizable="true" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:FormPanel ID="PanelDetailIdentification" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DsptType" runat="server" FieldLabel="Jenis Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspNumber" runat="server" FieldLabel="Nomor Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssueDate" runat="server" FieldLabel="Tanggal Diterbitkan"></ext:DisplayField>
                    <ext:DisplayField ID="DspExpiryDate" runat="server" FieldLabel="Tanggal Kadaluarsa"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedBy" runat="server" FieldLabel="Diterbitkan Oleh"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedCountry" runat="server" FieldLabel="Negara Penerbit Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspIdentificationComment" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button22" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelIdentification_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FormPanelIdentification" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:ComboBox ID="CmbTypeIdentification" runat="server" FieldLabel="Jenis Identitas" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreTypeIdentification" OnReadData="Identification_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="TxtNumber" runat="server" FieldLabel="Nomor Identitas" AnchorHorizontal="80%" AllowBlank="false" MaxLength="255"></ext:TextField>
                    <ext:DateField ID="TxtIssueDate" runat="server" Format="dd-MMM-yyyy" FieldLabel="Tanggal Diterbitkan" AnchorHorizontal="80%"></ext:DateField>
                    <ext:DateField ID="TxtExpiryDate" runat="server" Format="dd-MMM-yyyy" FieldLabel="Tanggal Kadaluarsa" AnchorHorizontal="80%"></ext:DateField>
                    <ext:TextField ID="TxtIssuedBy" runat="server" FieldLabel="Diterbitkan Oleh" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <%--<ext:ComboBox ID="CmbCountryIdentification" runat="server" FieldLabel="Negara Penerbit Identitas" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreCountryCodeIdentification" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel11" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbCountryIdentification" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Negara Penerbit Identitas" AnchorHorizontal="80%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <ext:TextArea ID="TxtCommentIdentification" runat="server" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="Button23" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelIdentification}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="BtnSaveDetailIdentification_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button24" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDetailIdentification_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorPhone" runat="server" Icon="ApplicationViewDetail" Title="Director Phone" Modal="true" Hidden="true" MinWidth="500" Height="300" BodyPadding="6" Layout="FitLayout" Maximizable="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:FormPanel ID="PanelPhoneDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DspKategoriKontakDirector" runat="server" FieldLabel="Kategori Kontak"></ext:DisplayField>
                    <ext:DisplayField ID="DspJenisAlatKomunikasiDirector" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodeAreaTelpDirector" runat="server" FieldLabel="Kode Area Telp"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorTeleponDirector" runat="server" FieldLabel="Nomor Telepon"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorExtensiDirector" runat="server" FieldLabel="Nomor Extensi"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanDirectorPhoneDirector" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button3" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelPhoneDirectorEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DspKategoriKontakDirectorEmployer" runat="server" FieldLabel="Kategori Kontak"></ext:DisplayField>
                    <ext:DisplayField ID="DspJenisAlatKomunikasiDirectorEmployer" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodeAreaTelpDirectorEmployer" runat="server" FieldLabel="Kode Area Telp"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorTeleponDirectorEmployer" runat="server" FieldLabel="Nomor Telepon"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorExtensiDirectorEmployer" runat="server" FieldLabel="Nomor Extensi"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanDirectorPhoneDirectorEmployer" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button4" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorEmployerPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FormPanelDirectorPhone" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:ComboBox ID="Director_cb_phone_Contact_Type" runat="server" FieldLabel="Kategori Kontak" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="100%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirPContactType" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model21">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:ComboBox ID="Director_cb_phone_Communication_Type" runat="server" FieldLabel="Jenis Alat Komunikasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="100%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirComunicationType" OnReadData="Communication_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model22">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="Director_txt_phone_Country_prefix" runat="server" FieldLabel="Kode Area Telp" AnchorHorizontal="100%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="4"></ext:TextField>
                    <ext:TextField ID="Director_txt_phone_number" runat="server" FieldLabel="Nomor Telepon" AnchorHorizontal="100%" AllowBlank="false" MaxLength="50" Regex="^[0-9]\d*$" RegexText="Must Numeric"></ext:TextField>
                    <ext:TextField ID="Director_txt_phone_extension" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="100%" MaxLength="10" Regex="^[0-9]\d*$" RegexText="Must Numeric"></ext:TextField>
                    <ext:TextField ID="Director_txt_phone_comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="100%" MaxLength="4000"></ext:TextField>
                </Items>
                <Buttons>
                    <ext:Button ID="btnSaveDirectorPhonedetail" runat="server" Text="Save" DefaultAlign="center">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelDirectorPhone}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="btnSaveDirectorPhones_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="btnBackSaveDirectorPhoneDetail" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveDirectorPhones_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FormPanelEmpDirectorTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" BodyPadding="5" AutoScroll="true">
                <Items>
                    <ext:ComboBox ID="Director_Emp_cb_phone_Contact_Type" runat="server" FieldLabel="Kategori Kontak" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="100%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreEmpDirPContactType" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model23">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:ComboBox ID="Director_Emp_cb_phone_Communication_Type" runat="server" FieldLabel="Jenis Alat Komunikasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="100%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreEmpDirComunicationType" OnReadData="Communication_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model24">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="Director_Emp_txt_phone_Country_prefix" runat="server" FieldLabel="Kode Area Telp" AnchorHorizontal="100%" MaxLength="4" Regex="^[0-9]\d*$" RegexText="Must Numeric"></ext:TextField>
                    <ext:TextField ID="Director_Emp_txt_phone_number" runat="server" FieldLabel="Nomor Telepon" AnchorHorizontal="100%" AllowBlank="false" MaxLength="50" Regex="^[0-9]\d*$" RegexText="Must Numeric"></ext:TextField>
                    <ext:TextField ID="Director_Emp_txt_phone_extension" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="100%" MaxLength="10" Regex="^[0-9]\d*$" RegexText="Must Numeric"></ext:TextField>
                    <ext:TextField ID="Director_Emp_txt_phone_comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="100%" MaxLength="4000"></ext:TextField>
                </Items>
                <Buttons>
                    <ext:Button ID="btn_saveDirector_empphone" runat="server" Text="Save" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnSaveDirectorEmpPhones_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button13" runat="server" Text="Back" DefaultAlign="center">
                        <%--<Listeners>
                            <Click Handler="if (!#{FormPanelEmpDirectorTaskDetail}.getForm().isValid()) return false;"></Click>
                        </Listeners>--%>
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveDirectorEmployerPhones_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorAddress" runat="server" Icon="ApplicationViewDetail" Title="Director Address" Hidden="true" Modal="true" MinWidth="600" Height="400" BodyPadding="6" Layout="FitLayout" Maximizable="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:FormPanel ID="PanelAddressDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DspTipeAlamatDirector" runat="server" FieldLabel="Tipe Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspAlamatDirector" runat="server" FieldLabel="Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspKecamatanDirector" runat="server" FieldLabel="Kecamatan"></ext:DisplayField>
                    <ext:DisplayField ID="DspKotaKabupatenDirector" runat="server" FieldLabel="Kota/Kabupaten"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodePosDirector" runat="server" FieldLabel="Kode Pos"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDirector" runat="server" FieldLabel="Negara"></ext:DisplayField>
                    <ext:DisplayField ID="DspProvinsiDirector" runat="server" FieldLabel="Provinsi"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanAddressDirector" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button1" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorDetailAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelAddressDirectorEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DspTipeAlamatDirectorEmployer" runat="server" FieldLabel="Tipe Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspAlamatDirectorEmployer" runat="server" FieldLabel="Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspKecamatanDirectorEmployer" runat="server" FieldLabel="Kecamatan"></ext:DisplayField>
                    <ext:DisplayField ID="DspKotaKabupatenDirectorEmployer" runat="server" FieldLabel="Kota/Kabupaten"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodePosDirectorEmployer" runat="server" FieldLabel="Kode Pos"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDirectorEmployer" runat="server" FieldLabel="Negara"></ext:DisplayField>
                    <ext:DisplayField ID="DspProvinsiDirectorEmployer" runat="server" FieldLabel="Provinsi"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanAddressDirectorEmployer" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button2" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorDetailEmployerAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FP_Address_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:ComboBox ID="Director_cmb_kategoriaddress" runat="server" FieldLabel="Kategori Alamat" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="100%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirKategoryADdressAddress_Type" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model25">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="Director_Address" runat="server" FieldLabel="Alamat" AnchorHorizontal="100%" AllowBlank="false" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="Director_Town" runat="server" FieldLabel="Kecamatan" AnchorHorizontal="100%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="Director_City" runat="server" FieldLabel="Kota" AnchorHorizontal="100%" AllowBlank="false" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="Director_Zip" runat="server" FieldLabel="Kode Pos" AnchorHorizontal="100%" MaxLength="10"></ext:TextField>
                   <%-- <ext:ComboBox ID="Director_cmb_kodenegara" runat="server" FieldLabel="Kode Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="100%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirFromForeignCountry_CodeAddress" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model26">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel12" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="Director_cmb_kodenegara" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Kode Negara" AnchorHorizontal="100%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <ext:TextField ID="Director_State" runat="server" FieldLabel="Ibu Kota" AnchorHorizontal="100%" MaxLength="255"></ext:TextField>
                    <ext:TextArea ID="Director_Comment_Address" runat="server" FieldLabel="Catatan" AnchorHorizontal="100%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="Btn_Save_Director_Address" runat="server" Text="Save" DefaultAlign="center">
                        <Listeners>
                            <Click Handler="if (!#{FP_Address_Director}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="btnSaveDirectorAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Btn_Back_Director_Address" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveDirectorAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FP_Address_Emp_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:ComboBox ID="Director_cmb_emp_kategoriaddress" AllowBlank="false" runat="server" FieldLabel="Kategori Alamat" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreEmpDirKategoryADdressAddress_Type" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model27">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="Director_emp_Address" runat="server" FieldLabel="Alamat" AnchorHorizontal="90%" AllowBlank="false" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="Director_emp_Town" runat="server" FieldLabel="Kecamatan" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="Director_emp_City" runat="server" FieldLabel="Kota" AnchorHorizontal="90%" AllowBlank="false" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="Director_emp_Zip" runat="server" FieldLabel="Kode Pos" AnchorHorizontal="90%" MaxLength="10"></ext:TextField>
                    <%--<ext:ComboBox ID="Director_cmb_emp_kodenegara" runat="server" FieldLabel="Kode Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreEmpDirFromForeignCountry_CodeAddress" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model28">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel13" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="Director_cmb_emp_kodenegara" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Kode Negara" AnchorHorizontal="90%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <ext:TextField ID="Director_emp_State" runat="server" FieldLabel="Ibu Kota" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                    <ext:TextArea ID="Director_emp_Comment_Address" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="btn_Director_saveempaddress" runat="server" Text="Save" DefaultAlign="center">
                        <Listeners>
                            <Click Handler="if (!#{FP_Address_Emp_Director}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="btnSaveDirectorEmpAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button15" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveDirectorEmployerAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailDirector" runat="server" Icon="ApplicationViewDetail" Title="Director" Hidden="true" Modal="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout" Maximizable="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:FormPanel ID="FormPanelDirectorDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:Hidden runat="server" ID="Hidden1"></ext:Hidden>
                    <ext:DisplayField ID="DspPeranDirector" runat="server" FieldLabel="Peran" AnchorHorizontal="70%" LabelWidth="250"></ext:DisplayField>
                    <ext:DisplayField ID="DspGelarDirector" LabelWidth="250" runat="server" FieldLabel="Gelar" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaLengkap" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Nama Lengkap" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250" ID="DspGender" runat="server" FieldLabel="Gender" AnchorHorizontal="70%" AllowBlank="false"></ext:DisplayField>
                    <%--daniel 2020 des 29--%>
                    <ext:DisplayField LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Tanggal Lahir" ID="DspTanggalLahir" AnchorHorizontal="90%"/>
                    <%--end 2020 des 29--%>
                    <ext:DisplayField ID="DspTempatLahir" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaIbuKandung" LabelWidth="250" runat="server" FieldLabel="Nama Ibu Kandung" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaAlias" LabelWidth="250" runat="server" FieldLabel="Nama Alias" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNIK" LabelWidth="250" runat="server" FieldLabel="NIK" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNoPassport" LabelWidth="250" runat="server" FieldLabel="No. Passport" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraPenerbitPassport" LabelWidth="250" runat="server" FieldLabel="Negara Penerbit Passport" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNoIdentitasLain" LabelWidth="250" runat="server" FieldLabel="No. Identitas Lain" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250" AllowBlank="False" ID="DspKewarganegaraan1" runat="server" FieldLabel="Kewarganegaraan 1" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspKewarganegaraan2" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 2" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspKewarganegaraan3" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 3" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDomisili" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Negara Domisili" AnchorHorizontal="70%"></ext:DisplayField>

                    <ext:GridPanel ID="GridPanel1" runat="server" Title="Informasi Telepon" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StorePhoneDirector" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column1" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanel2" runat="server" Title="Informasi Address" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreAddressDirector" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column7" runat="server" DataIndex="Type_Address" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="Addres" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="Cty" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="CountryCode" Text="Kode Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DisplayField ID="DspEmail" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspOccupation" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspTempatBekerja" LabelWidth="250" runat="server" FieldLabel="Tempat Bekerja" AnchorHorizontal="90%"></ext:DisplayField>

                    <ext:GridPanel ID="GridPanel3" runat="server" Title="Informasi Telepon Tempat Kerja" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StorePhoneDirectorEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model8" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column15" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="Communication_Type" Text="Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanel4" runat="server" Title="Informasi Address Tempat Kerja" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreAddressDirectorEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model17" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column21" runat="server" DataIndex="Type_Address" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="Addres" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="Cty" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="CountryCode" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn4" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanel5" runat="server" Title="Informasi Identitas" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreIdentificationDirector" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model18" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                            <%--<ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>--%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column6" runat="server" DataIndex="Tipe" Text="Jenis Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="No" Text="Nomor Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="IssueDate" Text="Tanggal Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="ExpiryDate" Text="Tanggal Kedaluwarsa" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="IssuedCountry" Text="Negara Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column6" runat="server" DataIndex="Tipe" Text="Jenis Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="No" Text="Nomor Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="IssueDate" Text="Tanggal Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="ExpiryDate" Text="Tanggal Kedaluwarsa" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="IssuedCountry" Text="Negara Diterbitkan" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:CommandColumn ID="CommandColumn5" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DisplayField runat="server" LabelWidth="250" ID="DspDeceased" FieldLabel="Deceased ?"></ext:DisplayField>
                    <%--daniel 2020 des 29--%>
                    <ext:DisplayField runat="server" LabelWidth="250" Hidden="true" FieldLabel="Deceased Date " ID="DspDeceasedDate" />
                    <%--end 2020 des 29--%>
                    <ext:DisplayField runat="server" LabelWidth="250" ID="DspPEP" FieldLabel="PEP ?"></ext:DisplayField>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="NPWP" ID="DspNPWP" AnchorHorizontal="90%" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Source of Wealth" ID="DspSourceofWealth" AnchorHorizontal="90%" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Catatan" ID="DspCatatan" AnchorHorizontal="90%" />

                </Items>
                <Buttons>
                    <ext:Button ID="Button5" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveCustomerDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FP_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:Hidden runat="server" ID="Hidden2"></ext:Hidden>
                    <ext:ComboBox ID="cmb_peran" runat="server" FieldLabel="Peran" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" LabelWidth="250" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StorePeran" OnReadData="Role_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model7">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:ComboBox LabelWidth="250" ID="cmb_Director_Gender" runat="server" FieldLabel="Gender" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreGender" OnReadData="Gender_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model9">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>
                    <ext:TextField ID="txt_Director_Title" LabelWidth="250" runat="server" FieldLabel="Gelar" AnchorHorizontal="90%" MaxLength="30"></ext:TextField>
                    <ext:TextField ID="txt_Director_Last_Name" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Nama Lengkap" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                    <ext:DateField LabelWidth="250" runat="server" FieldLabel="Tanggal Lahir" ID="txt_Director_BirthDate" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                    <ext:TextField ID="txt_Director_Birth_Place" LabelWidth="250" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="txt_Director_Mothers_Name" LabelWidth="250" runat="server" FieldLabel="Nama Ibu Kandung" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="txt_Director_Alias" LabelWidth="250" runat="server" FieldLabel="Nama Alias" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="txt_Director_SSN" LabelWidth="250" runat="server" FieldLabel="NIK" AnchorHorizontal="90%" MaxLength="16" MinLength="16" Regex="^[0-9]\d*$" RegexText="Must Numeric"></ext:TextField>
                    <ext:TextField ID="txt_Director_Passport_Number" LabelWidth="250" runat="server" FieldLabel="No. Passport" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                    <%--     <ext:ComboBox ID="txt_Director_Passport_Country" LabelWidth="250" runat="server" FieldLabel="Negara Penerbit Passport" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirectorPassportNumber" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model31">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel5" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="txt_Director_Passport_Country" LabelWidth="250" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Negara Penerbit Passport" AnchorHorizontal="80%" />
                        </Content>
                    </ext:Panel>
                    <ext:TextField ID="txt_Director_ID_Number" LabelWidth="250" runat="server" FieldLabel="No. Identitas Lain" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                    <ext:Panel ID="Panel6" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_Director_Nationality1" LabelWidth="250" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Kewarganegaraan 1" AnchorHorizontal="80%"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel7" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_Director_Nationality2" LabelWidth="250" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Kewarganegaraan 2" AnchorHorizontal="80%" />
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel8" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_Director_Nationality3" LabelWidth="250" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Kewarganegaraan 3" AnchorHorizontal="80%" />
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel9" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_Director_Residence" LabelWidth="250" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Negara Domisili" AnchorHorizontal="80%"/>
                        </Content>
                    </ext:Panel>
                    <%--<ext:ComboBox LabelWidth="250" AllowBlank="False" ID="cmb_Director_Nationality1" runat="server" FieldLabel="Kewarganegaraan 1" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirNational1" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model10">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <%--<ext:ComboBox ID="cmb_Director_Nationality2" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 2" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirNational2" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model11">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <%--<ext:ComboBox ID="cmb_Director_Nationality3" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 3" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirNational3" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model12">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <%-- <ext:ComboBox ID="cmb_Director_Residence" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Negara Domisili" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirReisdence" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model13">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>

                    <ext:GridPanel ID="GP_DirectorPhone" runat="server" Title="Informasi Telepon Director" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar10" runat="server">
                                <Items>
                                    <ext:Button ID="btnsavedirectorphone" runat="server" Text="Add Phones" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddDirectorPhones_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_DirectorPhone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model6" runat="server">
                                        <Fields>
                                <%--daniel 2020 jan 4--%>
                                            <%--<ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                <%--end 2020 jan 4--%>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <%--daniel 2020 jan 4--%>
                                <%--<ext:Column ID="Column61" runat="server" DataIndex="Tph_Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column62" runat="server" DataIndex="Tph_Communication_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column64" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column61" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column62" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column64" runat="server" DataIndex="number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column65" runat="server" DataIndex="tph_extension" Text="Nomor Extensi" MinWidth="130" Flex="1"></ext:Column>
                                <%--end 2020 jan 4--%>
                                <ext:CommandColumn ID="CommandColumn11" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorPhone">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>

                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GP_DirectorAddress" runat="server" Title="Informasi Address Director" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar11" runat="server">
                                <Items>
                                    <ext:Button ID="btnsavedirectorAddress" runat="server" Text="Add Addresses" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddDirectorAddresses_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_DirectorAddress" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model16" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                <%--daniel 2020 jan 4--%>
                                            <%--<ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                <%--end 2020 jan 4--%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <%--daniel 2020 jan 4--%>
                               <%-- <ext:Column ID="Column67" runat="server" DataIndex="Address_Type" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column68" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column70" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column72" runat="server" DataIndex="CountryCode" Text="Kode Negara" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column67" runat="server" DataIndex="Type_Address" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column68" runat="server" DataIndex="Addres" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column70" runat="server" DataIndex="Cty" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column72" runat="server" DataIndex="CountryCode" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                <%--end 2020 jan 4--%>
                                <ext:CommandColumn ID="CommandColumn12" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorAddress">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:TextField ID="txt_Director_Email" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="txt_Director_Email2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="txt_Director_Email3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="txt_Director_Email4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="txt_Director_Email5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="txt_Director_Occupation" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="txt_Director_employer_name" LabelWidth="250" runat="server" FieldLabel="Tempat Bekerja" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>

                    <ext:GridPanel ID="GP_DirectorEmpPhone" runat="server" Title="Informasi Telepon Tempat Kerja Director" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar8" runat="server">
                                <Items>
                                    <ext:Button ID="btnsaveEmpdirectorPhone" runat="server" Text="Add Phones" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddDirectorEmployerPhones_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Director_Employer_Phone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model14" runat="server">
                                        <Fields>
                                <%--daniel 2020 jan 4--%>
                                            <%--<ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                <%--emd 2020 jan 4--%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--daniel 2020 jan 4--%>
                                <%--<ext:Column ID="Column47" runat="server" DataIndex="Tph_Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column48" runat="server" DataIndex="Tph_Communication_Type" Text="Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column50" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column47" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column48" runat="server" DataIndex="Communication_Type" Text="Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column50" runat="server" DataIndex="number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column51" runat="server" DataIndex="tph_extension" Text="Nomor Extensi" MinWidth="130" Flex="1"></ext:Column>
                                <%--end 2020 jan 4--%>
                                <ext:CommandColumn ID="CommandColumn9" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerPhone">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>

                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbarGrdCmdDirectorEmployerPhone" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GP_DirectorEmpAddress" runat="server" Title="Informasi Address Tempat Kerja Director" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar9" runat="server">
                                <Items>
                                    <ext:Button ID="btnsaveEmpdirectorAddress" runat="server" Text="Add Addresses" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddDirectorEmployerAddresses_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Director_Employer_Address" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model15" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                <%--daniel 2020 jan 4--%>
                                            <%--<ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                <%--end 2020 jan 4--%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <%--daniel 2020 jan 4--%>
                                <%--<ext:Column ID="Column53" runat="server" DataIndex="Address_Type" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column54" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column56" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column58" runat="server" DataIndex="CountryCode" Text="Negara" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column53" runat="server" DataIndex="Type_Address" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column54" runat="server" DataIndex="Addres" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column56" runat="server" DataIndex="Cty" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column58" runat="server" DataIndex="CountryCode" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                <%--daniel 2020 jan 4--%>
                                <ext:CommandColumn ID="CommandColumn10" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    
                    <ext:GridPanel ID="GP_DirectorIdentification" runat="server" Title="Informasi Identitas Director" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar4" runat="server">
                                <Items>
                                    <ext:Button ID="Button11" runat="server" Text="Add Identitas" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddDirectorIdentification_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreIdentificationDirectorDetail" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model19" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                <%--daniel 2020 jan 4--%>
                                            <%--<ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Issue_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Expiry_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Issued_Country" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                <%--end 2020 jan 4--%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--daniel 2020 jan 4--%>
                                <%--<ext:Column ID="Column3" runat="server" DataIndex="Type" Text="Jenis Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="Number" Text="Nomor Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="Issue_Date" Text="Tanggal Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column23" runat="server" DataIndex="Expiry_Date" Text="Tanggal Kedaluwarsa" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column25" runat="server" DataIndex="Issued_Country" Text="Negara Diterbitkan" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column3" runat="server" DataIndex="Tipe" Text="Jenis Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="No" Text="Nomor Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="IssueDate" Text="Tanggal Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column23" runat="server" DataIndex="ExpiryDate" Text="Tanggal Kedaluwarsa" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column25" runat="server" DataIndex="IssuedCountry" Text="Negara Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <%--end 2020 jan 4--%>
                                <ext:CommandColumn ID="CommandColumn6" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbarDirectorIdentification" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Director_Deceased" FieldLabel="Deceased ?">
                        <DirectEvents>
                            <Change OnEvent="checkBoxcb_Director_Deceased_click" />
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField runat="server" LabelWidth="250" Hidden="true" FieldLabel="Deceased Date " Format="dd-MMM-yyyy" ID="txt_Director_Deceased_Date" />
                    <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Director_Tax_Reg_Number" FieldLabel="PEP ?"></ext:Checkbox>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="NPWP" ID="txt_Director_Tax_Number" AnchorHorizontal="90%" MaxLength="15" MinLength="15" Regex="^[0-9]\d*$" RegexText="Must Numeric"></ext:TextField>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Source of Wealth" ID="txt_Director_Source_of_Wealth" AnchorHorizontal="90%" MaxLength="255" />
                                <%--daniel 2020 jan 5--%>
                    <%--<ext:TextField runat="server" LabelWidth="250" FieldLabel="Catatan" ID="txt_Director_Comments" AnchorHorizontal="90%" Regex="4000" />--%>
                                <%--end 2020 jan 5--%>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Catatan" ID="txt_Director_Comments" AnchorHorizontal="90%" MaxLength="4000" />

                </Items>
                <Buttons>
                    <ext:Button ID="btnsaveDirector" runat="server" Text="Save" DefaultAlign="center">
                        <%--<Listeners>
                            <Click Handler="if (!#{FP_Director}.getForm().isValid()) return false;"></Click>
                        </Listeners>--%>
                        <DirectEvents>
                            <Click OnEvent="btnSaveCustomerDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button9" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveCustomerDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>



    <ext:Window ID="WindowDetailPhone" runat="server" Icon="ApplicationViewDetail" Title="Phone" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150" Maximizable="true">
        <Items>

            <ext:FormPanel ID="PanelDetailPhone" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="Dsptph_contact_type" runat="server" FieldLabel="Kategori Kontak"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_communication_type" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_country_prefix" runat="server" FieldLabel="Kode Area Telp"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_number" runat="server" FieldLabel="Nomor Telepon"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_extension" runat="server" FieldLabel="Nomor Extensi"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsPhone" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnCancelPhone" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>

            <ext:FormPanel ID="PanelDetailPhoneEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="Dsptph_contact_typeEmployer" runat="server" FieldLabel="Kategori Kontak"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_communication_typeEmployer" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_country_prefixEmployer" runat="server" FieldLabel="Kode Area Telp"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_numberEmployer" runat="server" FieldLabel="Nomor Telepon"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_extensionEmployer" runat="server" FieldLabel="Nomor Extensi"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsPhoneEmployer" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button16" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelPhoneEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>

            <ext:FormPanel ID="FormPanelPhoneDetail" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:ComboBox ID="Cmbtph_contact_type" runat="server" FieldLabel="Kategori Kontak" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignContact_Type" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:ComboBox ID="Cmbtph_communication_type" runat="server" FieldLabel="Jenis Alat Komunikasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignCommunication_Type" OnReadData="Communication_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="Txttph_country_prefix" runat="server" FieldLabel="Kode Area Telp" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="4"></ext:TextField>
                    <ext:TextField ID="Txttph_number" runat="server" AllowBlank="false" FieldLabel="Nomor Telepon" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="50"></ext:TextField>
                    <ext:TextField ID="Txttph_extension" runat="server" FieldLabel="Nomor Extensi" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="10"></ext:TextField>
                    <ext:TextArea ID="TxtcommentsPhone" runat="server" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnSaveDetailPhone" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelPhoneDetail}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="BtnSaveDetailPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnCancelDetailPhone" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDetailPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>

            <ext:FormPanel ID="FormPanelPhoneEmployerDetail" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:ComboBox ID="Cmbtph_contact_typeEmployer" runat="server" FieldLabel="Kategori Kontak" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignContact_TypeEmployer" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:ComboBox ID="Cmbtph_communication_typeEmployer" runat="server" FieldLabel="Jenis Alat Komunikasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignCommunication_TypeEmployer" OnReadData="Communication_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="Txttph_country_prefixEmployer" runat="server" FieldLabel="Kode Area Telp" AnchorHorizontal="80%" MaxLength="4" Regex="^[0-9]\d*$" RegexText="Must Numeric"></ext:TextField>
                    <ext:TextField ID="Txttph_numberEmployer" runat="server" AllowBlank="false" FieldLabel="Nomor Telepon" AnchorHorizontal="80%" MaxLength="50" Regex="^[0-9]\d*$" RegexText="Must Numeric"></ext:TextField>
                    <ext:TextField ID="Txttph_extensionEmployer" runat="server" FieldLabel="Nomor Extensi" AnchorHorizontal="80%" MaxLength="10" Regex="^[0-9]\d*$" RegexText="Must Numeric"></ext:TextField>
                    <ext:TextArea ID="TxtcommentsPhoneEmployer" runat="server" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="Button17" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelPhoneEmployerDetail}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="BtnSaveDetailPhoneEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button18" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDetailPhoneEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                </Buttons>
            </ext:FormPanel>

        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailPhone}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailAddress" runat="server" Icon="ApplicationViewDetail" Title="Address" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150" Maximizable="true">
        <Items>

            <ext:FormPanel ID="PanelDetailAddress" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DspAddress_type" runat="server" FieldLabel="Tipe Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspAddress" runat="server" FieldLabel="Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspTown" runat="server" FieldLabel="Kecamatan"></ext:DisplayField>
                    <ext:DisplayField ID="DspCity" runat="server" FieldLabel="Kota/Kabupaten"></ext:DisplayField>
                    <ext:DisplayField ID="DspZip" runat="server" FieldLabel="Kode Pos"></ext:DisplayField>
                    <ext:DisplayField ID="Dspcountry_code" runat="server" FieldLabel="Negara"></ext:DisplayField>
                    <ext:DisplayField ID="DspState" runat="server" FieldLabel="Provinsi"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsAddress" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnCancelAddress" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>

            <ext:FormPanel ID="PanelDetailAddressEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DspAddress_typeEmployer" runat="server" FieldLabel="Tipe Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspAddressEmployer" runat="server" FieldLabel="Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspTownEmployer" runat="server" FieldLabel="Kecamatan"></ext:DisplayField>
                    <ext:DisplayField ID="DspCityEmployer" runat="server" FieldLabel="Kota/Kabupaten"></ext:DisplayField>
                    <ext:DisplayField ID="DspZipEmployer" runat="server" FieldLabel="Kode Pos"></ext:DisplayField>
                    <ext:DisplayField ID="Dspcountry_codeEmployer" runat="server" FieldLabel="Negara"></ext:DisplayField>
                    <ext:DisplayField ID="DspStateEmployer" runat="server" FieldLabel="Provinsi"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsAddressEmployer" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button19" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelAddressEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>

            <ext:FormPanel ID="FormPanelAddressDetail" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:ComboBox ID="CmbAddress_type" runat="server" FieldLabel="Tipe Alamat" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignAddress_Type" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy></ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents></DirectEvents>
                    </ext:ComboBox>
                    <ext:TextField ID="TxtAddress" runat="server" AllowBlank="false" FieldLabel="Alamat" AnchorHorizontal="80%" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="TxtTown" runat="server" FieldLabel="Kecamatan" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="TxtCity" runat="server" AllowBlank="false" FieldLabel="Kota/Kabupaten" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="TxtZip" runat="server" FieldLabel="Kode Pos" AnchorHorizontal="80%" MaxLength="10"></ext:TextField>
                   <%-- <ext:ComboBox ID="Cmbcountry_code" runat="server" FieldLabel="Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignCountry_CodeAddress" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy></ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents></DirectEvents>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel14" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="Cmbcountry_code" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Negara" AnchorHorizontal="80%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <ext:TextField ID="TxtState" runat="server" FieldLabel="Provinsi" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextArea ID="TxtcommentsAddress" runat="server" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnSaveDetailAddress" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelAddressDetail}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="BtnSaveDetailAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnCancelDetailAddress" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDetailAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>

            <ext:FormPanel ID="FormPanelAddressDetailEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:ComboBox ID="CmbAddress_typeEmployer" runat="server" FieldLabel="Tipe Alamat" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignAddress_TypeEmployer" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy></ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents></DirectEvents>
                    </ext:ComboBox>
                    <ext:TextField ID="TxtAddressEmployer" runat="server" AllowBlank="false" FieldLabel="Alamat" AnchorHorizontal="80%" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="TxtTownEmployer" runat="server" FieldLabel="Kecamatan" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="TxtCityEmployer" runat="server" AllowBlank="false" FieldLabel="Kota/Kabupaten" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="TxtZipEmployer" runat="server" FieldLabel="Kode Pos" AnchorHorizontal="80%" MaxLength="10"></ext:TextField>
                   <%-- <ext:ComboBox ID="Cmbcountry_codeEmployer" runat="server" FieldLabel="Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignCountry_CodeAddressEmployer" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy></ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents></DirectEvents>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel15" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="Cmbcountry_codeEmployer" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Negara" AnchorHorizontal="80%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <ext:TextField ID="TxtStateEmployer" runat="server" FieldLabel="Provinsi" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextArea ID="TxtcommentsAddressEmployer" runat="server" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="Button20" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelAddressDetailEmployer}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="BtnSaveDetailAddressEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button21" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDetailAddressEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>

        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailAddress}.center()" />
        </Listeners>
    </ext:Window>

    <ext:FormPanel runat="server" Title="WIC Edit Form" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="WIC_Panel">
        <Items>
            <ext:DisplayField ID="PKWIC" runat="server" FieldLabel="ID"></ext:DisplayField>
            <ext:DisplayField ID="WICNo" runat="server" FieldLabel="WIC No"></ext:DisplayField>
            <ext:Checkbox ID="CheckboxIsUpdateFromDataSource" runat="server" FieldLabel="Update From Data Source ?" AnchorHorizontal="80%"></ext:Checkbox>
            <ext:DisplayField ID="FKCustomer" runat="server" Hidden="true"></ext:DisplayField>
            <ext:FormPanel runat="server" Title="WIC Add Individu" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="WIC_Individu">
                <Defaults>
                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                </Defaults>
                <Items>
                    <ext:ComboBox ID="CmbINDV_Gender" runat="server" FieldLabel="Jenis Kelamin" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreGenderINDV" OnReadData="Gender_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>
                    <ext:TextField ID="TxtINDV_Title" runat="server" FieldLabel="Gelar" AnchorHorizontal="80%" MaxLength="30"></ext:TextField>
                    <ext:TextField ID="TxtINDV_last_name" runat="server" FieldLabel="Nama Lengkap" AnchorHorizontal="80%" AllowBlank="false" MaxLength="100"></ext:TextField>
                    <ext:DateField ID="DateINDV_Birthdate" runat="server" Flex="1" FieldLabel="Tanggal Lahir" Format="dd-MMM-yyyy">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                        this.clearValue();
                        this.getTrigger(0).hide();}" />
                        </Listeners>
                    </ext:DateField>
                    <ext:TextField ID="TxtINDV_Birth_place" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Mothers_name" runat="server" FieldLabel="Nama Ibu Kandung" AnchorHorizontal="80%" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Alias" runat="server" FieldLabel="Nama Alias" AnchorHorizontal="80%" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="TxtINDV_SSN" runat="server" FieldLabel="NIK" AnchorHorizontal="80%" MaxLength="16" MinLength="16" Regex="^[0-9]\d*$" RegexText="Must Numeric"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Passport_number" runat="server" FieldLabel="No. Passport" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:Panel ID="Pnl_TxtINDV_Passport_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="TxtINDV_Passport_country" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Negara Penerbit Passport" AnchorHorizontal="80%" />
                        </Content>
                    </ext:Panel>
                    <ext:TextField ID="TxtINDV_ID_Number" runat="server" FieldLabel="No. Identitas Lain" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:Panel ID="Panel1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbINDV_Nationality1" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Kewarganegaraan 1" AnchorHorizontal="80%"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel2" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbINDV_Nationality2" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Kewarganegaraan 2" AnchorHorizontal="80%" />
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbINDV_Nationality3" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Kewarganegaraan 3" AnchorHorizontal="80%" />
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel4" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbINDV_residence" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Negara Domisili" AnchorHorizontal="80%"/>
                        </Content>
                    </ext:Panel>
                    <%--<ext:ComboBox ID="CmbINDV_Nationality1" AllowBlank="false" runat="server" FieldLabel="Kewarganegaraan 1" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignNational1" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>--%>
                    <%-- <ext:ComboBox ID="CmbINDV_Nationality2" runat="server" FieldLabel="Kewarganegaraan 2" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignNational2" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>
                    <ext:ComboBox ID="CmbINDV_Nationality3" runat="server" FieldLabel="Kewarganegaraan 3" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignNational3" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>
                    <ext:ComboBox ID="CmbINDV_residence" AllowBlank="false" runat="server" FieldLabel="Negara Domisili" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignResisdance" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>--%>

                    <ext:GridPanel ID="GridPanelPhoneIndividuDetail" runat="server" Title="Informasi Telepon" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar" runat="server">
                                <Items>
                                    <ext:Button
                                        ID="BtnAddNewPhoneIndividu" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddNewPhoneIndividu_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StorePhoneIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="ModelDetail" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumberPhoneDetailIndividu" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="colTph_Contact_TypeIndividu" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Flex="1"></ext:Column>
                                <ext:Column ID="coltph_communication_typeIndividu" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Flex="1"></ext:Column>
                                <ext:Column ID="coltph_numberIndividu" runat="server" DataIndex="number" Text="Nomor Telepon" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhoneIndividuDetail" runat="server" Text="Action" Flex="2">
                                    <Commands>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhoneIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Download' || command=='Edit' || command=='MoveUp' || command=='MoveDown' || command=='Detail') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelAddressIndividuDetail" runat="server" Title="Informasi Alamat" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="BtnAddNewAddressIndividu" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddNewAddressIndividu_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreAddressIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailIndividu" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="ColAddress_typeIndividu" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Flex="1"></ext:Column>
                                <ext:Column ID="ColAddressIndividu" runat="server" DataIndex="Addres" Text="Alamat" Flex="1"></ext:Column>
                                <ext:Column ID="ColCityIndividu" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Flex="1"></ext:Column>
                                <ext:Column ID="Colcountry_codeIndividu" runat="server" DataIndex="CountryCode" Text="Negara" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAddressIndividuDetail" runat="server" Text="Action" Flex="2">
                                    <Commands>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddressIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Download' || command=='Edit' || command=='MoveUp' || command=='MoveDown' || command=='Detail') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar15" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:TextField ID="TxtINDV_Email" runat="server" FieldLabel="Email" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Email2" runat="server" FieldLabel="Email2" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Email3" runat="server" FieldLabel="Email3" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Email4" runat="server" FieldLabel="Email4" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Email5" runat="server" FieldLabel="Email5" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Occupation" runat="server" FieldLabel="Pekerjaan" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="TxtINDV_employer_name" runat="server" FieldLabel="Tempat Bekerja" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>

                    <ext:GridPanel ID="GridPanelPhoneIndividualEmployerDetail" runat="server" Title="Informasi Telepon Tempat Bekerja" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar5" runat="server">
                                <Items>
                                    <ext:Button
                                        ID="Button12" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddNewPhoneEmployerIndividu_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StorePhoneEmployerIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model20" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column27" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Flex="1"></ext:Column>
                                <ext:Column ID="Column28" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Flex="1"></ext:Column>
                                <ext:Column ID="Column29" runat="server" DataIndex="number" Text="Nomor Telepon" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhoneIndividualEmployerDetail" runat="server" Text="Action" Flex="2">
                                    <Commands>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhoneEmployerIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Download' || command=='Edit' || command=='MoveUp' || command=='MoveDown' || command=='Detail') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelAddressIndividualEmployerDetail" runat="server" Title="Informasi Alamat Tempat Bekerja" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar6" runat="server">
                                <Items>
                                    <ext:Button ID="Button14" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddNewAddressEmployerIndividu_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreAddressEmployerIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model29" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn13" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column30" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Flex="1"></ext:Column>
                                <ext:Column ID="Column33" runat="server" DataIndex="Addres" Text="Alamat" Flex="1"></ext:Column>
                                <ext:Column ID="Column34" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Flex="1"></ext:Column>
                                <ext:Column ID="Column35" runat="server" DataIndex="CountryCode" Text="Negara" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAddressIndividualEmployerDetail" runat="server" Text="Action" Flex="2">
                                    <Commands>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddressEmployerIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Download' || command=='Edit' || command=='MoveUp' || command=='MoveDown' || command=='Detail') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelIdentificationIndividualDetail" runat="server" Title="Informasi Identitas" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar12" runat="server">
                                <Items>
                                    <ext:Button ID="Button25" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddIdentification_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreIdentificationDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model30" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn14" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column36" runat="server" DataIndex="Tipe" Text="Jenis Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column37" runat="server" DataIndex="No" Text="Nomor Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:DateColumn ID="Column38" runat="server" DataIndex="IssueDate" Text="Tanggal Diterbitkan" Format="dd-MMM-yyyy" MinWidth="130" Flex="1"></ext:DateColumn>
                                <ext:DateColumn ID="Column39" runat="server" DataIndex="ExpiryDate" Text="Tanggal Kedaluwarsa" Format="dd-MMM-yyyy" MinWidth="130" Flex="1"></ext:DateColumn>
                                <ext:Column ID="Column40" runat="server" DataIndex="IssuedCountry" Text="Negara Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnIdentificationIndividualDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:Checkbox ID="CbINDV_Deceased" runat="server" FieldLabel="Sudah Meninggal" AnchorHorizontal="80%">
                        <DirectEvents>
                            <Change OnEvent="checkBoxcb_INDV_Deceased_click" />
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField ID="DateINDV_Deceased_Date" runat="server" Flex="1" FieldLabel="Tanggal Meninggal" Hidden="true">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                        this.clearValue();
                        this.getTrigger(0).hide();}" />
                        </Listeners>
                    </ext:DateField>
                    <ext:TextField ID="TxtINDV_Tax_Number" runat="server" FieldLabel="NPWP" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="15" MinLength="15"></ext:TextField>
                    <ext:Checkbox ID="CbINDV_Tax_Reg_Number" runat="server" FieldLabel="is PEP ?" AnchorHorizontal="80%"></ext:Checkbox>
                    <ext:TextField ID="TxtINDV_Source_Of_Wealth" runat="server" FieldLabel="Sumber Dana" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextArea ID="TxtINDV_Comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>

                </Items>
            </ext:FormPanel>

            <ext:FormPanel runat="server" Title="WIC Add Corporate" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="WIC_Corporate">
                <Defaults>
                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                </Defaults>
                <Items>
                    <ext:TextField ID="TxtCorp_Name" runat="server" FieldLabel="Nama Korporasi" AnchorHorizontal="80%" AllowBlank="false" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="TxtCorp_Commercial_name" runat="server" FieldLabel="Nama Komersial" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:ComboBox ID="CmbCorp_Incorporation_legal_form" runat="server" FieldLabel="Bentuk Korporasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignIncorporation_legal" OnReadData="Incorporation_legal_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="TxtCorp_Incorporation_number" runat="server" FieldLabel="Nomor Induk Berusaha" AnchorHorizontal="80%" MaxLength="50"></ext:TextField>
                    <%--daniel 11 jan 2021--%>
                    <ext:TextField ID="TxtCorp_Business" runat="server" FieldLabel="Bidang Usaha" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <%--end 11 jan 2021--%>

                    <ext:GridPanel ID="GridPanelPhoneCorporateDetail" runat="server" Title="Informasi Telepon" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar2" runat="server">
                                <Items>
                                    <ext:Button ID="BtnAddNewPhoneCorporate" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddNewPhoneCorporate_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StorePhoneCorporateDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <%--daniel 2020 des 28--%>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                            <%--<ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="FK_Ref_Detail_Of" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_for_Table_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>--%>
                                            <%--end 2020 des 28--%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <%--daniel 2020 des 28--%>
                                <%--<ext:RowNumbererColumn ID="RowNumbererPhoneDetailCorporate" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="colTph_Contact_TypeCorporate" runat="server" DataIndex="Tph_Contact_Type" Text="Kategori Kontak" Flex="1"></ext:Column>
                                <ext:Column ID="coltph_communication_typeCorporate" runat="server" DataIndex="Tph_Communication_Type" Text="Jenis Alat Komunikasi" Flex="1"></ext:Column>
                                <ext:Column ID="coltph_numberCorporate" runat="server" DataIndex="tph_number" Text="Nomor Telepon" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhoneCorporateDetail" runat="server" Text="Action" Flex="2">--%>
                                <%--<ext:RowNumbererColumn ID="RowNumbererPhoneDetailCorporate" runat="server"></ext:RowNumbererColumn>
                                <ext:Column ID="colTph_Contact_TypeCorporate" runat="server" DataIndex="Tph_Contact_Type" Text="Kategori Kontak" Width="170"></ext:Column>
                                <ext:Column ID="coltph_communication_typeCorporate" runat="server" DataIndex="Tph_Communication_Type" Text="Jenis Alat Komunikasi" Width="90"></ext:Column>
                                <ext:Column ID="coltph_numberCorporate" runat="server" DataIndex="tph_number" Text="Nomor Telepon" Width="80"></ext:Column>--%>
                                <ext:RowNumbererColumn ID="RowNumbererColumn15" Text="No" runat="server" Width="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column5" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Flex="1" ></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Flex="1" ></ext:Column>
                                <ext:Column ID="Column41" runat="server" DataIndex="number" Text="Nomor Telepon" Flex="1" ></ext:Column>
                                <%--end 2020 des 28--%>
                                <ext:CommandColumn ID="CommandColumnPhoneCorporateDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhoneCorporate">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Download' || command=='Edit' || command=='MoveUp' || command=='MoveDown' || command=='Detail') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar13" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelAddressCorporateDetail" runat="server" Title="Informasi Alamat" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar3" runat="server">
                                <Items>
                                    <ext:Button ID="BtnAddNewAddressCorporate" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddNewAddressCorporate_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreAddressCorporateDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <%--daniel 2020 des 29--%>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                            <%--<ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="FK_Ref_detail_of" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_to_Table_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Address_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country_Code" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>--%>
                                            <%--end 2020 des 29--%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <%--daniel 2020 des 29--%>
                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailCorporate" Text="No" runat="server" Width="60"></ext:RowNumbererColumn>
                                <ext:Column ID="ColAddress_typeCorporate" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Flex="1"></ext:Column>
                                <ext:Column ID="ColAddressCorporate" runat="server" DataIndex="Addres" Text="Alamat" Flex="1"></ext:Column>
                                <ext:Column ID="ColCityCorporate" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Flex="1"></ext:Column>
                                <ext:Column ID="Colcountry_codeCorporate" runat="server" DataIndex="CountryCode" Text="Negara" Flex="1"></ext:Column>
                                <%--<ext:RowNumbererColumn ID="RowNumbererAddressDetailCorporate" runat="server"  Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="ColAddress_typeCorporate" runat="server" DataIndex="Address_Type" Text="Tipe Alamat" Flex="1"></ext:Column>
                                <ext:Column ID="ColAddressCorporate" runat="server" DataIndex="Address" Text="Alamat" Flex="1"></ext:Column>
                                <ext:Column ID="ColCityCorporate" runat="server" DataIndex="City" Text="Kota/Kabupaten" Flex="1"></ext:Column>
                                <ext:Column ID="Colcountry_codeCorporate" runat="server" DataIndex="Country_Code" Text="Negara" Flex="1"></ext:Column>--%>
                                <%--end 2020 des 29--%>
                                <ext:CommandColumn ID="CommandColumnAddressCorporateDetail" runat="server" Text="Action" Flex="2">
                                    <Commands>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddressCorporate">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Download' || command=='Edit' || command=='MoveUp' || command=='MoveDown' || command=='Detail') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar16" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:TextField ID="TxtCorp_Email" runat="server" FieldLabel="Email Korporasi" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="TxtCorp_url" runat="server" FieldLabel="Website Korporasi" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="TxtCorp_incorporation_state" runat="server" FieldLabel="Provinsi" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:Panel ID="PanelNegara" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbCorp_incorporation_country_code" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Negara" AnchorHorizontal="80%"/>
                        </Content>
                    </ext:Panel>
                    <%--<ext:ComboBox ID="CmbCorp_incorporation_country_code" runat="server" FieldLabel="Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignCountry_Code" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>--%>

                    <ext:GridPanel ID="GP_Corp_Director" runat="server" Title="Director Corp" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar7" runat="server">
                                <Items>
                                    <ext:Button ID="Button7" runat="server" Text="Add Director" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddCustomerDirector_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Director_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_Director_Corp" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <%--daniel 2020 des 29--%>
                                            <ext:ModelField Name="NO_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Roles" Type="String"></ext:ModelField>
                                            <%--<ext:ModelField Name="NO_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Role" Type="String"></ext:ModelField>--%>
                                            <%--end 2020 des 29--%>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server"  Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <%--daniel 2020 des 29--%>
                                <ext:Column ID="Column31" runat="server" DataIndex="LastName" Text="Name" Flex="1"></ext:Column>
                                <ext:Column ID="Column32" runat="server" DataIndex="Roles" Text="Role" Flex="1"></ext:Column>
                                <%--<ext:Column ID="Column31" runat="server" DataIndex="Last_Name" Text="Name" Flex="1"></ext:Column>
                                <ext:Column ID="Column32" runat="server" DataIndex="Role" Text="Role" Flex="1"></ext:Column>--%>
                                <%--end 2020 des 29--%>
                                <ext:CommandColumn ID="CommandColumnDirector" runat="server" Text="Action" Flex="2">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.NO_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DateField ID="DateCorp_incorporation_date" runat="server" Flex="1" FieldLabel="Tanggal Pendirian">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                        this.clearValue();
                        this.getTrigger(0).hide();}" />
                        </Listeners>
                    </ext:DateField>
                    <ext:Checkbox ID="chkCorp_business_closed" runat="server" FieldLabel="Tutup?" BoxLabel="Yes" Checked="false">
                        <DirectEvents>
                            <Change OnEvent="chkCorp_business_closed_Event">
                                <EventMask Msg="Loading..." MinDelay="500" ShowMask="true"></EventMask>
                            </Change>
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField ID="DateCorp_date_business_closed" runat="server" Flex="1" FieldLabel="Tanggal Tutup" Hidden="true">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                        this.clearValue();
                        this.getTrigger(0).hide();}" />
                        </Listeners>
                    </ext:DateField>
                    <ext:TextField ID="TxtCorp_tax_number" runat="server" FieldLabel="NPWP" AnchorHorizontal="80%" MaxLength="15" MinLength="15" Regex="^[0-9]\d*$" RegexText="Must Numeric"></ext:TextField>
                    <ext:TextArea ID="TxtCorp_Comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>
                </Items>
            </ext:FormPanel>


        </Items>
        <Buttons>
            <ext:Button ID="btnSave" runat="server" Icon="Disk" Text="Save Process">
                <Listeners>
                    <%--<Click Handler="if (!#{WIC_Panel}.getForm().isValid()) return false;"></Click>--%>
                    <%--<Click Handler="if (!#{WIC_Individu}.getForm().isValid()) return false;"></Click>
                    <Click Handler="if (!#{WIC_Corporate}.getForm().isValid()) return false;"></Click>--%>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="BtnSave_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>

        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <Listeners>
                    <%--<Click Handler="if (!#{WIC_Panel}.getForm().isValid()) return false;"></Click>--%>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

</asp:Content>


