﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="PersonAdd.aspx.vb" Inherits="goAML_PersonAdd" %>

<script runat="server">


</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- THE ENTIRE PANEL -->
    <ext:FormPanel
        runat="server"
        Title="Reporting Person"
        BodyPadding="30"
        MonitorResize="true"
        Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true"
        ID="RP_EntireForm">
        <Defaults>
            <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
        </Defaults>
        <%-- Form Input goes here --%>
        <Items>
            <%-- PK ID (Display only) --%>
            <ext:DisplayField ID="LblID" runat="server" FieldLabel="ID" AnchorHorizontal="80%" Text="AutoNumber" />
            <%-- User ID --%>
            <ext:ComboBox
                ID="cmb_UserID"
                runat="server"
                FieldLabel="User Id"
                DisplayField="keterangan"
                ValueField="kode"
                Flex="1"
                AllowBlank="false"
                EmptyText="Pilih User ID..."
                AnchorHorizontal="80%"
                ForceSelection="true"
                AutoLoadOnValue="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreUserID" OnReadData="UserID_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Change Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:ComboBox>
            <%-- Kode Laporan --%>
            <ext:ComboBox
                ID="RP_KodeLaporan"
                runat="server"
                FieldLabel="Kode Laporan"
                DisplayField="Keterangan"
                ValueField="Kode"
                Flex="1"
                AllowBlank="false"
                EmptyText="Pilih Kode Laporan..."
                AnchorHorizontal="80%"
                ForceSelection="true"
                AutoLoadOnValue="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreKodeLaporan" OnReadData="JenisLaporan_ReadData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Change Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:ComboBox>
            <%-- Gelar --%>
            <ext:TextField runat="server" ID="RP_Title" Flex="1" FieldLabel="Gelar" AnchorHorizontal="80%" MaxLength="30">
            </ext:TextField>
            <%-- Nama lengkap --%>
            <ext:TextField runat="server" FieldLabel="Nama Lengkap" Flex="1" AnchorHorizontal="80%" ID="RP_FulltName" AllowBlank="false" MaxLength="100" />
            <%-- Tanggal Lahir --%>
            <ext:DateField runat="server" Flex="1" FieldLabel="Tanggal Lahir" ID="RP_BirthDate" AllowBlank="false" Format="dd-MMM-yyyy">
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Change Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:DateField>
            <%-- Tempat lahir --%>
            <ext:TextField runat="server" ID="RP_BirthPlace" Flex="1" FieldLabel="Tempat Lahir" AnchorHorizontal="80%" MaxLength="255">
            </ext:TextField>
            <%-- Jenis kelamin --%>
            <ext:ComboBox
                ID="RP_Gender"
                runat="server"
                FieldLabel="Jenis Kelamin"
                DisplayField="Keterangan"
                ValueField="Kode"
                Flex="1"
                AllowBlank="false"
                EmptyText="Pilih jenis kelamin..."
                AnchorHorizontal="80%"
                ForceSelection="true"
                AutoLoadOnValue="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreGender" OnReadData="Gender_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Change Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:ComboBox>
            <%-- Nama ibu kandung --%>
            <ext:TextField runat="server" Flex="1" ID="RP_MotherName" FieldLabel="Nama Ibu Kandung" AnchorHorizontal="80%" MaxLength="100" />
            <%-- Nama alias --%>
            <ext:TextField runat="server" Flex="1" ID="RP_Alias" FieldLabel="Nama Alias" AnchorHorizontal="80%" MaxLength="100" />
            <%-- NIK --%>
            <ext:TextField runat="server" Flex="1" ID="RP_SSN" FieldLabel="NIK" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="16" MinLength="16" />
            <%-- No passport --%>
            <ext:TextField runat="server" Flex="1" ID="RP_Passport" FieldLabel="No. Paspor" AnchorHorizontal="80%" MaxLength="100" />
            <%-- Negara penerbit passport --%>
            <ext:ComboBox
                ID="RP_CountryPassport"
                runat="server"
                FieldLabel="Negara Penerbit Paspor"
                DisplayField="Keterangan"
                ValueField="Kode"
                Flex="1"
                EmptyText="Pilih Negara Penerbit Paspor..."
                AnchorHorizontal="80%"
                ForceSelection="true"
                AutoLoadOnValue="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreCountryPassport" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Change Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:ComboBox>
            <%-- No identitas lain --%>
            <ext:TextField runat="server" Flex="1" ID="RP_OtherID" FieldLabel="No. Identitas Lainnya" AnchorHorizontal="80%" MaxLength="255" />
            <%-- PHONE FIELD SET --%>
            <ext:FieldSet runat="server" Title="Telepon">
                <Items>
                    <%---- GRID PANEL -->--%>
                    <ext:GridPanel ID="PhoneField_Grid" runat="server" Title="Telepon" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="PhoneField_Store" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" />
                                            <ext:ModelField Name="kategori_kontak" />
                                            <ext:ModelField Name="ja_komunikasi" />
                                            <ext:ModelField Name="tph_country_prefix" />
                                            <ext:ModelField Name="tph_number" />
                                            <ext:ModelField Name="tph_extension" />
                                            <ext:ModelField Name="comments" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererPhone" runat="server"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="Kategori Kontak" DataIndex="kategori_kontak" Flex="1" />
                                <ext:Column runat="server" Text="Jenis Alat Komunikasi" DataIndex="ja_komunikasi" Flex="1" />
                                <ext:Column runat="server" Text="Nomor Telepon" DataIndex="tph_number" Flex="1" />
                                <ext:CommandColumn ID="CommandColumnPhone" runat="server" Width="200" Text="Action" Flex="2">
                                    <Commands>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationViewDetail" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" Text="Add New Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="PhoneField_BtnAdd">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                    </ext:GridPanel>
                    <ext:DisplayField runat="server" Text="&nbsp;" />
                </Items>
            </ext:FieldSet>

            <%-- ADDRESS FIELD SET --%>
            <ext:FieldSet runat="server" Title="Alamat">
                <Items>
                    <%-- THE ADDRESS GRID PANEL --%>
                    <ext:GridPanel runat="server" ID="AddressField_Grid" Title="Alamat" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="AddressField_Store" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" />
                                            <ext:ModelField Name="kontak" />
                                            <ext:ModelField Name="address" />
                                            <ext:ModelField Name="town" />
                                            <ext:ModelField Name="city" />
                                            <ext:ModelField Name="zip" />
                                            <ext:ModelField Name="negara" />
                                            <ext:ModelField Name="state" />
                                            <ext:ModelField Name="comments" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererAddress" runat="server"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="Tipe Alamat" DataIndex="kontak" Flex="1" />
                                <ext:Column runat="server" Text="Alamat" DataIndex="address" Flex="1" />
                                <ext:Column runat="server" Text="Kota/Kabupaten" DataIndex="city" Flex="1" />
                                <ext:Column runat="server" Text="Negara" DataIndex="negara" Flex="1" />
                                <ext:CommandColumn ID="CommandColumnAddress" runat="server" Width="200" Text="Action" Flex="2">
                                    <Commands>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationViewDetail" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" Text="Add New Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="AddressField_BtnAdd"></Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                    </ext:GridPanel>
                    <ext:DisplayField runat="server" Text="&nbsp;" />
                </Items>
            </ext:FieldSet>

            <%-- Kewarganegaraan 1,2,3 --%>
            <ext:ComboBox
                ID="RP_Nationality1"
                runat="server"
                FieldLabel="Kewarganegaraan 1"
                DisplayField="Keterangan"
                ValueField="Kode"
                Flex="1"
                AllowBlank="false"
                EmptyText="Pilih Kewarganegaraan..."
                AnchorHorizontal="80%"
                ForceSelection="true"
                AutoLoadOnValue="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignNational1" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Change Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:ComboBox>
            <ext:ComboBox
                ID="RP_Nationality2"
                runat="server"
                FieldLabel="Kewarganegaraan 2"
                DisplayField="Keterangan"
                ValueField="Kode"
                Flex="1"
                EmptyText="Pilih Kewarganegaraan..."
                AnchorHorizontal="80%"
                ForceSelection="true"
                AutoLoadOnValue="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignNational2" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Change Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:ComboBox>
            <ext:ComboBox
                ID="RP_Nationality3"
                runat="server"
                FieldLabel="Kewarganegaraan 3"
                DisplayField="Keterangan"
                ValueField="Kode"
                Flex="1"
                EmptyText="Pilih Kewarganegaraan..."
                AnchorHorizontal="80%"
                ForceSelection="true"
                AutoLoadOnValue="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignNational3" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Change Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:ComboBox>
            <%-- Negara domisili --%>
            <ext:ComboBox
                ID="RP_Residence"
                runat="server"
                FieldLabel="Negara Domisili"
                DisplayField="Keterangan"
                ValueField="Kode"
                Flex="1"
                AllowBlank="false"
                EmptyText="Pilih negara domisili..."
                AnchorHorizontal="80%"
                ForceSelection="true"
                AutoLoadOnValue="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignResisdance" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Change Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:ComboBox>
            <%-- Email 1--%>
            <ext:TextField runat="server" Flex="1" ID="RP_Email1" FieldLabel="Email 1" AllowBlank="false" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"/>
            <%-- Email 2--%>
            <ext:TextField runat="server" Flex="1" ID="RP_Email2" FieldLabel="Email 2" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"/>
            <%-- Email 3--%>
            <ext:TextField runat="server" Flex="1" ID="RP_Email3" FieldLabel="Email 3" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"/>
            <%-- Email 4--%>
            <ext:TextField runat="server" Flex="1" ID="RP_Email4" FieldLabel="Email 4" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"/>
            <%-- Email 5--%>
            <ext:TextField runat="server" Flex="1" ID="RP_Email5" FieldLabel="Email 5" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"/>
            <%-- Pekerjaan --%>
            <ext:TextField runat="server" Flex="1" ID="RP_Job" AllowBlank="false" FieldLabel="Pekerjaan" AnchorHorizontal="80%" MaxLength="255"/>
            <%-- Nama Tempat bekerja --%>
            <ext:TextField runat="server" Flex="1" ID="RP_JobPlace" FieldLabel="Tempat Bekerja" AnchorHorizontal="80%" MaxLength="255"/>
            <%--EMPLOYER ADDRESS FIELD SET--%>
            <ext:FieldSet runat="server" Title="Alamat">
                <Items>
                    <ext:GridPanel ID="GridPanel_employerAddress" runat="server" Title="Alamat Tempat Bekerja" AutoScroll="true" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>

                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="Btn_AddEmployerAddress" Text="Add New Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="Btn_AddEmployerAddress_click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="500"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>

                        <Store>
                            <ext:Store ID="StoreAddressEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model runat="server" ID="Model148" IDProperty="PK_goAML_Ref_A">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_A" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="TypeAlamat" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Alamat" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Kota" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Negara" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn30" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column101" runat="server" DataIndex="TypeAlamat" Text="Tipe Alamat" Flex="1"></ext:Column>
                                <ext:Column ID="Column102" runat="server" DataIndex="Alamat" Text="Alamat" Flex="1"></ext:Column>
                                <ext:Column ID="Column103" runat="server" DataIndex="Kota" Text="Kota" Flex="1"></ext:Column>
                                <ext:Column ID="Column104" runat="server" DataIndex="Negara" Text="Negara" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAddressEmp" runat="server" Text="Action" Flex="2">

                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="GridcommandAddressEmployer">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_A" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:FieldSet>

            <%--EMPLOYER PHONE FIELD SET--%>
            <ext:FieldSet runat="server" Title="Telepon">
                <Items>
                    <ext:GridPanel ID="GridPanel_EmployerPhone" runat="server" Title="Telepon Tempat Bekerja" AutoScroll="true" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>

                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="Btn_AddEmployerPhone" Text="Add New Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="Btn_AddEmployerPhone_click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="500"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>

                        <Store>
                            <ext:Store ID="StorePhoneEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model runat="server" ID="Model151" IDProperty="PK_goAML_ref_P">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_ref_P" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="TipeKontak" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn31" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column105" runat="server" DataIndex="TipeKontak" Text="Kategori Kontak" Flex="1"></ext:Column>
                                <ext:Column ID="Column106" runat="server" DataIndex="country_prefix" Text="Kode Area Telepon" Flex="1"></ext:Column>
                                <ext:Column ID="Column107" runat="server" DataIndex="number" Text="Nomor Telepon" Flex="1"></ext:Column>

                                <ext:CommandColumn ID="CommandColumnPhoneEmp" runat="server" Text="Action" Flex="2">

                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="GridcommandPhoneEmployer">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_ref_P" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:FieldSet>

            <%-- IDENTITY FIELD SET --%>
            <ext:FieldSet runat="server" Title="Dokumen Identitas">
                <Items>
                    <ext:GridPanel runat="server" ID="IDField_Grid" Title="Dokumen Identitas" AutoScroll="true" EmptyText="No Available Data">
                        <%-- STORES --%>
                        <Store>
                            <ext:Store runat="server" ID="IDField_Store" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" />
                                            <ext:ModelField Name="type" />
                                            <ext:ModelField Name="Number" />
                                            <ext:ModelField Name="Issue_Date" Type="Date" />
                                            <ext:ModelField Name="Expiry_Date" Type="Date" />
                                            <ext:ModelField Name="Issued_By" />
                                            <ext:ModelField Name="negara" />
                                            <ext:ModelField Name="Identification_Comment" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <%-- COLUMNS --%>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumberIdentity" runat="server"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="Jenis identitas" DataIndex="type" Flex="1" />
                                <ext:Column runat="server" Text="Nomor identitas" DataIndex="Number" Flex="1" />
                                <ext:Column runat="server" Text="Negara penerbit identitas" DataIndex="negara" Flex="1" />
                                <ext:CommandColumn ID="CommandColumnIdent" runat="server" Width="200" Text="Action" Flex="2">
                                    <Commands>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationViewDetail" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                        <%-- TOP BAR --%>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" Text="Add New Identity" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="IDField_BtnAdd">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                    </ext:GridPanel>
                    <ext:DisplayField runat="server" Text="&nbsp;" />
                </Items>
            </ext:FieldSet>

            <%-- Sudah meninggal? --%>
            <ext:Checkbox runat="server" ID="RP_IsDeceased" FieldLabel="Sudah Meninggal? " AnchorHorizontal="80%" />
            <%-- Kapan? --%>
            <ext:DateField runat="server" Flex="1" Disabled="true" FieldLabel="Tanggal Meninggal" ID="RP_DeceasedDate" Format="dd-MMM-yyyy">
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                </Triggers>
                <Listeners>
                    <Change Handler="this.getTrigger(0).show();" />
                    <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:DateField>
            <%-- NPWP --%>
            <ext:TextField runat="server" Flex="1" ID="RP_TaxNumber" FieldLabel="NPWP" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="15" MinLength="15"/>
            <%-- Tax Reg Number --%>
            <ext:Checkbox runat="server" ID="RP_TaxRegNumber" FieldLabel="PEPs?" AnchorHorizontal="80%" />
            <%-- Sumber Dana --%>
            <ext:TextField runat="server" Flex="1" ID="RP_SourceOfWealth" FieldLabel="Sumber Dana" AnchorHorizontal="80%" MaxLength="255" />
            <%-- Catatan --%>
            <ext:TextArea runat="server" Flex="1" ID="RP_Comments" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="4000" />
        </Items>

        <%-- Form Button goes here --%>
        <Buttons>
            <ext:Button ID="btnAddAll" runat="server" Icon="Disk" Text="Save">
                <Listeners>
                    <Click Handler="if (!#{RP_EntireForm}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="IDModal_BtnAdd">
                        <EventMask ShowMask="true" Msg="Saveing Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancelRP">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <%-- MODAL: Identity add/edit --%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="IDModal" Width="700" Title="Tambah/Edit Identitas">
        <Items>
            <ext:FormPanel runat="server" BodyPadding="5"
                MonitorResize="true" ID="IDModal_Form">
                <Items>
                    <ext:ComboBox
                        ID="IDModal_Type"
                        runat="server"
                        FieldLabel="Jenis Identitas"
                        DisplayField="Keterangan"
                        ValueField="Kode"
                        Flex="1"
                        AllowBlank="false"
                        EmptyText="Pilih Jenis Identitas..."
                        AnchorHorizontal="80%"
                        ForceSelection="true"
                        AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreIDModal_Type" OnReadData="JenisIdentitas_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                    <ext:TextField runat="server" Flex="1" ID="IDModal_Number" AllowBlank="false" FieldLabel="Nomor Identitas" Regex="^[0-9]\d*$" RegexText="Must Numeric" AnchorHorizontal="80%" MaxLength="255" />
                    <ext:DateField runat="server" Flex="1" ID="IDModal_IssueDate" Format="dd-MMM-yyyy" FieldLabel="Tanggal Diterbitkan" />
                    <ext:DateField runat="server" Flex="1" ID="IDModal_ExpiryDate" Format="dd-MMM-yyyy" FieldLabel="Tanggal Kadaluarsa" />
                    <ext:TextField runat="server" Flex="1" ID="IDModal_IssuedBy" FieldLabel="Diterbitkan Oleh" AnchorHorizontal="80%" MaxLength="255" />
                    <ext:ComboBox
                        ID="IDModal_Country"
                        runat="server"
                        FieldLabel="Negara Penerbit Identitas"
                        DisplayField="Keterangan"
                        ValueField="Kode"
                        Flex="1"
                        AllowBlank="false"
                        EmptyText="Pilih Negara Penerbit..."
                        AnchorHorizontal="80%"
                        ForceSelection="true"
                        AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreIDModal_Country" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                    <ext:TextArea runat="server" ID="IDModal_Comments" Flex="1" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="4000" />
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <%-- Add --%>
            <ext:Button ID="btnSave" runat="server" Icon="Disk" Text="Save">
                <Listeners>
                    <Click Handler="if (!#{IDModal_Form}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="IdentificationModal_BtnAdd">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <%-- Cancel --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="IDModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <%-- MODAL: Phone add/edit --%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="PhoneModal" Width="700" Title="Tambah/Edit Telepon">
        <Items>
            <%--FORM PANEL--%>
            <ext:FormPanel
                runat="server"
                BodyPadding="5"
                MonitorResize="true"
                ID="PhoneModal_Form">
                <Items>
                    <%-- Kategori Kontak --%>
                    <ext:ComboBox
                        ID="PhoneModal_Type"
                        runat="server"
                        FieldLabel="Kategori Kontak"
                        DisplayField="Keterangan"
                        ValueField="Kode"
                        Flex="1"
                        AllowBlank="false"
                        EmptyText="Pilih Kategori Kontak..."
                        AnchorHorizontal="80%"
                        ForceSelection="true"
                        AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StorePhoneModal_Type" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                    <%-- Jenis alat komunikasi --%>
                    <ext:ComboBox
                        ID="PhoneModal_Tool"
                        runat="server"
                        FieldLabel="Jenis Alat Komunikasi"
                        DisplayField="Keterangan"
                        ValueField="Kode"
                        Flex="1"
                        AllowBlank="false"
                        EmptyText="Pilih Jenis Alat Komunikasi..."
                        AnchorHorizontal="80%"
                        ForceSelection="true"
                        AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StorePhoneModal_Tool" OnReadData="Communication_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                    <%-- Kode area telepon --%>
                    <ext:TextField runat="server" Flex="1" ID="PhoneModal_RegionCode" FieldLabel="Kode Area Telepon" Regex="^[0-9]\d*$" RegexText="Must Numeric" AnchorHorizontal="80%" MaxLength="4" />
                    <%-- Nomor Telepon --%>
                    <ext:TextField runat="server" Flex="1" ID="PhoneModal_Number" Regex="^[0-9]\d*$" RegexText="Must Numeric" AllowBlank="false" MaxLength="50" FieldLabel="Nomor Telepon" AnchorHorizontal="80%" />
                    <%-- Nomor Telepon Extensi --%>
                    <ext:TextField runat="server" Flex="1" ID="PhoneModal_NumberEx" FieldLabel="Nomor Extensi" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="10" />
                    <%-- Notes --%>
                    <ext:TextArea runat="server" ID="PhoneModal_Comment" Flex="1" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="4000" />
                </Items>
            </ext:FormPanel>
        </Items>
        <%-- Buttons --%>
        <Buttons>
            <%-- Add --%>
            <ext:Button runat="server" Icon="Disk" Text="Save">
                <Listeners>
                    <Click Handler="if (!#{PhoneModal_Form}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="PhoneModal_BtnAdd">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <%-- Cancel --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="PhoneModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <%-- MODAL: Employer Phone add/edit --%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="WindowEmployerPhone" Width="700" Title="Tambah/Edit Telepon Tempat Bekerja">
        <Items>
            <%--FORM PANEL--%>
            <ext:FormPanel
                runat="server"
                BodyPadding="5"
                MonitorResize="true"
                ID="FormPanelEmployerPhone">
                <Items>
                    <%-- Kategori Kontak --%>
                    <ext:ComboBox
                        ID="EmployerPhone_Kategori"
                        runat="server"
                        FieldLabel="Kategori Kontak"
                        DisplayField="Keterangan"
                        ValueField="Kode"
                        Flex="1"
                        AllowBlank="false"
                        EmptyText="Pilih Kategori Kontak..."
                        AnchorHorizontal="80%"
                        ForceSelection="true"
                        AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreEmployerPhone_Kategori" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                    <%-- Jenis alat komunikasi --%>
                    <ext:ComboBox
                        ID="EmployerPhone_Jenis"
                        runat="server"
                        FieldLabel="Jenis Alat Komunikasi"
                        DisplayField="Keterangan"
                        ValueField="Kode"
                        Flex="1"
                        AllowBlank="false"
                        EmptyText="Pilih Jenis Alat Komunikasi..."
                        AnchorHorizontal="80%"
                        ForceSelection="true"
                        AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreEmployerPhone_Jenis" OnReadData="Communication_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                    <%-- Kode area telepon --%>
                    <ext:TextField runat="server" Flex="1" ID="EmployerPhone_Kode" FieldLabel="Kode Area Telepon" Regex="^[0-9]\d*$" RegexText="Must Numeric" AnchorHorizontal="80%" MaxLength="4" />
                    <%-- Nomor Telepon --%>
                    <ext:TextField runat="server" Flex="1" ID="EmployerPhone_Nomor" Regex="^[0-9]\d*$" RegexText="Must Numeric" AllowBlank="false" MaxLength="50" FieldLabel="Nomor Telepon" AnchorHorizontal="80%" />
                    <%-- Nomor Telepon Extensi --%>
                    <ext:TextField runat="server" Flex="1" ID="EmployerPhone_Extensi" FieldLabel="Nomor Extensi" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="10" />
                    <%-- Notes --%>
                    <ext:TextArea runat="server" ID="EmployerPhone_Note" Flex="1" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="4000" />
                </Items>
            </ext:FormPanel>
        </Items>
        <%-- Buttons --%>
        <Buttons>
            <%-- Add --%>
            <ext:Button runat="server" Icon="Disk" Text="Save">
                <Listeners>
                    <Click Handler="if (!#{FormPanelEmployerPhone}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="PhoneEmployerModal_BtnAdd">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <%-- Cancel --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="PhoneModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <%-- MODAL: Address add/edit --%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="AddrModal" Width="700" Title="Tambah/Edit Alamat">
        <Items>
            <ext:FormPanel
                runat="server"
                BodyPadding="5"
                MonitorResize="true"
                ID="AddrModal_Form">
                <Items>
                    <%-- Tipe alamat --%>
                    <ext:ComboBox
                        ID="AddrModal_Type"
                        runat="server"
                        FieldLabel="Tipe Alamat"
                        DisplayField="Keterangan"
                        ValueField="Kode"
                        Flex="1"
                        AllowBlank="false"
                        EmptyText="Pilih Tipe Alamat..."
                        AnchorHorizontal="80%"
                        ForceSelection="true"
                        AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreAddrModal_Type" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                    <%-- Alamat --%>
                    <ext:TextField runat="server" AllowBlank="false" Flex="1" ID="AddrModal_Address" FieldLabel="Alamat" AnchorHorizontal="80%" MaxLength="100" />
                    <%-- Kecamatan --%>
                    <ext:TextField runat="server" Flex="1" ID="AddrModal_Town" FieldLabel="Kecamatan" AnchorHorizontal="80%" MaxLength="255" />
                    <%-- Kabupaten --%>
                    <ext:TextField runat="server" AllowBlank="false" Flex="1" ID="AddrModal_City" FieldLabel="Kota/Kabupaten" AnchorHorizontal="80%" MaxLength="255" />
                    <%-- Kodepos --%>
                    <ext:TextField runat="server" Flex="1" ID="AddrModal_PostalCode" FieldLabel="Kode Pos" AnchorHorizontal="80%" MaxLength="10" />
                    <%-- Negara --%>
                    <ext:ComboBox
                        ID="AddrModal_Country"
                        runat="server"
                        FieldLabel="Negara"
                        DisplayField="Keterangan"
                        ValueField="Kode"
                        Flex="1"
                        AllowBlank="false"
                        EmptyText="Pilih Negara..."
                        AnchorHorizontal="80%"
                        ForceSelection="true"
                        AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignCountry_CodeAddress" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                    <%-- Province --%>
                    <ext:TextField runat="server" Flex="1" ID="AddrModal_Province" FieldLabel="Provinsi" AnchorHorizontal="80%" MaxLength="255" />
                    <%-- Catatan --%>
                    <ext:TextArea runat="server" Flex="1" ID="AddrModal_Comment" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="4000" />
                </Items>
            </ext:FormPanel>
        </Items>
        <%-- Buttons --%>
        <Buttons>
            <%-- Add --%>
            <ext:Button ID="btnAdd" runat="server" Icon="Disk" Text="Save">
                <Listeners>
                    <Click Handler="if (!#{AddrModal_Form}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="AddressModal_BtnAdd">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <%-- Cancel --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="AddressModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <%-- MODAL: Employer Address add/edit --%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="WindowEmployerAddress" Width="700" Title="Tambah/Edit Alamat Tempat Bekerja">
        <Items>
            <ext:FormPanel
                runat="server"
                BodyPadding="5"
                MonitorResize="true"
                ID="FormPanelEmployerAddress">
                <Items>
                    <%-- Tipe alamat --%>
                    <ext:ComboBox
                        ID="EmployerAddress_Tipe"
                        runat="server"
                        FieldLabel="Tipe Alamat"
                        DisplayField="Keterangan"
                        ValueField="Kode"
                        Flex="1"
                        AllowBlank="false"
                        EmptyText="Pilih Tipe Alamat..."
                        AnchorHorizontal="80%"
                        ForceSelection="true"
                        AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreEmployerAddress_Tipe" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                    <%-- Alamat --%>
                    <ext:TextField runat="server" AllowBlank="false" Flex="1" ID="EmployerAddress_Alamat" FieldLabel="Alamat" AnchorHorizontal="80%" MaxLength="100" />
                    <%-- Kecamatan --%>
                    <ext:TextField runat="server" Flex="1" ID="EmployerAddress_Kec" FieldLabel="Kecamatan" AnchorHorizontal="80%" MaxLength="255" />
                    <%-- Kabupaten --%>
                    <ext:TextField runat="server" AllowBlank="false" Flex="1" ID="EmployerAddress_Kab" FieldLabel="Kota/Kabupaten" AnchorHorizontal="80%" MaxLength="255" />
                    <%-- Kodepos --%>
                    <ext:TextField runat="server" Flex="1" ID="EmployerAddress_Pos" FieldLabel="Kode Pos" AnchorHorizontal="80%" MaxLength="10" />
                    <%-- Negara --%>
                    <ext:ComboBox
                        ID="EmployerAddress_Negara"
                        runat="server"
                        FieldLabel="Negara"
                        DisplayField="Keterangan"
                        ValueField="Kode"
                        Flex="1"
                        AllowBlank="false"
                        EmptyText="Pilih Negara..."
                        AnchorHorizontal="80%"
                        ForceSelection="true"
                        AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignCountry_Code" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                    <%-- Province --%>
                    <ext:TextField runat="server" Flex="1" ID="EmployerAddress_Prov" FieldLabel="Provinsi" AnchorHorizontal="80%" MaxLength="255" />
                    <%-- Catatan --%>
                    <ext:TextArea runat="server" Flex="1" ID="EmployerAddress_Note" FieldLabel="Catatan" AnchorHorizontal="80%" MaxLength="4000" />
                </Items>
            </ext:FormPanel>
        </Items>
        <%-- Buttons --%>
        <Buttons>
            <%-- Add --%>
            <ext:Button ID="btnAddEmp" runat="server" Icon="Disk" Text="Save">
                <Listeners>
                    <Click Handler="if (!#{FormPanelEmployerAddress}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="AddressEmployerModal_BtnAdd">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <%-- Cancel --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="AddressModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <%-- MODAL: Identity Detail--%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="IDModalDetail" Width="700" Title="Detail Identitas">
        <Items>
            <ext:FormPanel runat="server" BodyPadding="5"
                MonitorResize="true" ID="IDModal_Form_Detail">
                <Items>
                    <ext:DisplayField ID="RP_jenis_identitas_detail" runat="server" FieldLabel="Jenis Identitas" />
                    <ext:DisplayField ID="RP_no_identitas_detail" runat="server" FieldLabel="Nomor Identitas" />
                    <ext:DisplayField ID="RP_tgl_terbit_detail" runat="server" FieldLabel="Tanggal Diterbitkan" />
                    <ext:DisplayField ID="RP_tgl_kadaluarsa_detail" runat="server" FieldLabel="Tanggal Kadaluarsa" />
                    <ext:DisplayField ID="RP_penerbit_detail" runat="server" FieldLabel="Diterbitkan Oleh" />
                    <ext:DisplayField ID="RP_negara_penerbit_detail" runat="server" FieldLabel="Negara penerbit identitas" />
                    <ext:DisplayField ID="RP_note_identitas_detail" runat="server" FieldLabel="Catatan" />
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <%-- Close --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="IDModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <%-- MODAL: Phone Detail --%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="PhoneModalDetail" Width="700" Title="Detail Telepon">
        <Items>
            <%--FORM PANEL--%>
            <ext:FormPanel
                runat="server"
                BodyPadding="5"
                MonitorResize="true"
                ID="PhoneModal_Form_Detail">
                <Items>
                    <%-- Kategori Kontak --%>
                    <ext:DisplayField ID="RP_tipe_kontak_detail" runat="server" FieldLabel="Kategori Kontak" />
                    <%-- Jenis alat komunikasi --%>
                    <ext:DisplayField ID="RP_jenis_komunikasi_detail" runat="server" FieldLabel="Jenis Alat Komunikasi" />
                    <%-- Kode area telepon --%>
                    <ext:DisplayField ID="RP_kode_area_detail" runat="server" FieldLabel="Kode area telepon" />
                    <%-- Nomor Telepon --%>
                    <ext:DisplayField ID="RP_no_telepon_detail" runat="server" FieldLabel="Nomor Telepon" />
                    <%-- Nomor Telepon Extensi--%>
                    <ext:DisplayField ID="RP_phone_ext_detail" runat="server" FieldLabel="Nomor Extensi" />
                    <%-- Notes --%>
                    <ext:DisplayField ID="RP_note_phone_detail" runat="server" FieldLabel="Catatan" />
                </Items>
            </ext:FormPanel>
        </Items>
        <%-- Buttons --%>
        <Buttons>
            <%-- Close --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="PhoneModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <%-- MODAL: Employer Phone Detail --%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="WindowEmployerPhoneDetail" Width="700" Title="Detail Telepon Tempat Bekerja">
        <Items>
            <%--FORM PANEL--%>
            <ext:FormPanel
                runat="server"
                BodyPadding="5"
                MonitorResize="true"
                ID="FormPanelEmployerPhoneDetail">
                <Items>
                    <%-- Kategori Kontak --%>
                    <ext:DisplayField ID="EmployerPhoneDetail_Kategori" runat="server" FieldLabel="Kategori Kontak" />
                    <%-- Jenis alat komunikasi --%>
                    <ext:DisplayField ID="EmployerPhoneDetail_jenis" runat="server" FieldLabel="Jenis Alat Komunikasi" />
                    <%-- Kode area telepon --%>
                    <ext:DisplayField ID="EmployerPhoneDetail_kode" runat="server" FieldLabel="Kode area telepon" />
                    <%-- Nomor Telepon --%>
                    <ext:DisplayField ID="EmployerPhoneDetail_nomor" runat="server" FieldLabel="Nomor Telepon" />
                    <%-- Nomor Telepon Extensi--%>
                    <ext:DisplayField ID="EmployerPhoneDetail_extensi" runat="server" FieldLabel="Nomor Extensi" />
                    <%-- Notes --%>
                    <ext:DisplayField ID="EmployerPhoneDetail_note" runat="server" FieldLabel="Catatan" />
                </Items>
            </ext:FormPanel>
        </Items>
        <%-- Buttons --%>
        <Buttons>
            <%-- Close --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="PhoneModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>

    </ext:Window>

    <%-- MODAL: Address Detail--%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="AddrModalDetail" Width="700" Title="Detail Alamat">
        <Items>
            <ext:FormPanel
                runat="server"
                BodyPadding="5"
                MonitorResize="true"
                ID="AddrModal_Form_Detail">
                <Items>
                    <%-- Tipe alamat --%>
                    <ext:DisplayField ID="RP_tipe_alamat_detail" runat="server" FieldLabel="Tipe Alamat" />
                    <%-- Alamat --%>
                    <ext:DisplayField ID="RP_alamat_detail" runat="server" FieldLabel="Alamat" />
                    <%-- Kecamatan --%>
                    <ext:DisplayField ID="RP_kecamatan_detail" runat="server" FieldLabel="Kecamatan" />
                    <%-- Kabupaten --%>
                    <ext:DisplayField ID="RP_kabupaten_detail" runat="server" FieldLabel="Kota/Kabupaten" />
                    <%-- Kodepos --%>
                    <ext:DisplayField ID="RP_kodepos_detail" runat="server" FieldLabel="Kode Pos" />
                    <%-- Negara --%>
                    <ext:DisplayField ID="RP_negara_detail" runat="server" FieldLabel="Negara" />
                    <%-- Province --%>
                    <ext:DisplayField ID="RP_propinsi_detail" runat="server" FieldLabel="Provinsi" />
                    <%-- Note --%>
                    <ext:DisplayField ID="RP_note_alamat_detail" runat="server" FieldLabel="Catatan" />
                </Items>
            </ext:FormPanel>
        </Items>
        <%-- Buttons --%>
        <Buttons>
            <%-- Cancel --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="AddressModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <%-- MODAL: Employer Address Detail--%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="WindowEmployerAddressDetail" Width="700" Title="Detail Alamat Tempat Bekerja">
        <Items>
            <ext:FormPanel
                runat="server"
                BodyPadding="5"
                MonitorResize="true"
                ID="FormPanelEmployerAddressDetail">
                <Items>
                    <%-- Tipe alamat --%>
                    <ext:DisplayField ID="EmployerAddressDetail_tipe" runat="server" FieldLabel="Tipe Alamat" />
                    <%-- Alamat --%>
                    <ext:DisplayField ID="EmployerAddressDetail_alamat" runat="server" FieldLabel="Alamat" />
                    <%-- Kecamatan --%>
                    <ext:DisplayField ID="EmployerAddressDetail_kec" runat="server" FieldLabel="Kecamatan" />
                    <%-- Kabupaten --%>
                    <ext:DisplayField ID="EmployerAddressDetail_kab" runat="server" FieldLabel="Kota/Kabupaten" />
                    <%-- Kodepos --%>
                    <ext:DisplayField ID="EmployerAddressDetail_pos" runat="server" FieldLabel="Kode Pos" />
                    <%-- Negara --%>
                    <ext:DisplayField ID="EmployerAddressDetail_negara" runat="server" FieldLabel="Negara" />
                    <%-- Province --%>
                    <ext:DisplayField ID="EmployerAddressDetail_prov" runat="server" FieldLabel="Provinsi" />
                    <%-- Note --%>
                    <ext:DisplayField ID="EmployerAddressDetail_note" runat="server" FieldLabel="Catatan" />
                </Items>
            </ext:FormPanel>
        </Items>
        <%-- Buttons --%>
        <Buttons>
            <%-- Cancel --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="AddressModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>

        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
