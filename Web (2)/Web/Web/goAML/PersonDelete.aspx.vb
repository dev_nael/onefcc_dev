﻿Imports NawaDevDAL
Imports System.Data.SqlClient
Imports System.Data
Imports System.Globalization

Partial Class goAML_PersonDelete
    Inherits Parent

#Region "Session"
    Private _IDReq As Long
    Public Property IDReq() As Long
        Get
            Return _IDReq
        End Get
        Set(ByVal value As Long)
            _IDReq = value
        End Set
    End Property
    Public Property StrUnikKey() As String
        Get
            Return Session("goAML_PersonDelete.StrUnikKey")
        End Get
        Set(ByVal value As String)
            Session("goAML_PersonDelete.StrUnikKey") = value
        End Set
    End Property
    Public Property objmodule() As NawaDAL.Module
        Get
            Return Session("goAML_PersonDelete.objmodule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_PersonDelete.objmodule") = value
        End Set
    End Property
    Public Property ReportingPersonClass As NawaDevBLL.ReportingPersonData
        Get
            Return Session("goAML_PersonAdd.ReportingPersonClass")
        End Get
        Set(ByVal value As NawaDevBLL.ReportingPersonData)
            Session("goAML_PersonAdd.ReportingPersonClass") = value
        End Set
    End Property
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                Dim Moduleid As String = Request.Params("ModuleID")
                Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                objmodule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

                Dim IDData As String = Request.Params("ID")
                StrUnikKey = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                If Not objmodule Is Nothing Then
                    If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Delete) Then
                        Dim strIDCode As String = 1
                        strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                        Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If

                FormPanelInput.Title = objmodule.ModuleLabel & " Delete"
                Panelconfirmation.Title = objmodule.ModuleLabel & " Delete"

                'Dim objReportingPerson As New NawaDevBLL.ReportingPerson
                'objReportingPerson.LoadPanelDeleteAndDetail(FormPanelInput, objmodule.ModuleName, StrUnikKey)

                clearSession()
                Loaddata()

                ColumnActionLocation(PhoneField_Grid, CommandColumnPhone)
                ColumnActionLocation(AddressField_Grid, CommandColumnAddress)
                ColumnActionLocation(GridPanel_employerAddress, CommandColumnAddressEmp)
                ColumnActionLocation(GridPanel_EmployerPhone, CommandColumnPhoneEmp)
                ColumnActionLocation(IDField_Grid, CommandColumnIdent)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
    Sub clearSession()
        ReportingPersonClass = New NawaDevBLL.ReportingPersonData
    End Sub
    Sub Loaddata()
        Dim IDData As String = Request.Params("ID")

        IDReq = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
        ReportingPersonClass.objPerson = NawaDevBLL.ReportingPerson.GetReportingPersonByID(IDReq)
        ReportingPersonClass.ObjPhone = NawaDevBLL.ReportingPerson.GetListPhoneByID(IDReq, 2)
        ReportingPersonClass.ObjEmpPhone = NawaDevBLL.ReportingPerson.GetListPhoneByID(IDReq, 11)
        ReportingPersonClass.ObjAddress = NawaDevBLL.ReportingPerson.GetListAddressByID(IDReq, 2)
        ReportingPersonClass.ObjEmpAddress = NawaDevBLL.ReportingPerson.GetListAddressByID(IDReq, 11)
        ReportingPersonClass.ObjIdent = NawaDevBLL.ReportingPerson.GetListIdentificationByID(IDReq)

        With ReportingPersonClass.objPerson
            LblID.Text = .PK_ODM_Reporting_Person_ID
            cmb_UserID.Text = .UserID
            RP_KodeLaporan.Text = NawaDevBLL.GlobalReportFunctionBLL.getJenisLaporanByCode(.Kode_Laporan)
            RP_Title.Text = .Title
            RP_FulltName.Text = .Last_Name
            If Not .BirthDate = Nothing Then
                RP_BirthDate.Value = .BirthDate.ToString("dd-MMM-yyyy")
            End If
            RP_BirthPlace.Text = .Birth_Place
            RP_Gender.Text = NawaDevBLL.GlobalReportFunctionBLL.getGenderbyKode(.Gender)
            RP_MotherName.Text = .Mothers_Name
            RP_Alias.Text = .Alias
            RP_SSN.Text = .SSN
            RP_Passport.Text = .Passport_Number
            RP_CountryPassport.Text = .Passport_Country
            RP_OtherID.Text = .Id_Number
            RP_Nationality1.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
            RP_Nationality2.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
            RP_Nationality3.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
            RP_Residence.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Residence)
            RP_Email1.Text = .Email1
            RP_Email2.Text = .Email2
            RP_Email3.Text = .Email3
            RP_Email4.Text = .Email4
            RP_Email5.Text = .Email5
            RP_Job.Text = .Occupation
            RP_JobPlace.Text = .Employer_Name
            If .Deceased = True Then
                RP_IsDeceased.Text = "Ya"
                If .Deceased_Date.HasValue Then
                    RP_DeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yyyy")
                End If
            ElseIf .Deceased = False Then
                RP_IsDeceased.Text = "Tidak"
            End If
            RP_TaxNumber.Text = .Tax_Number
            If .Tax_Reg_Number = True Then
                RP_TaxRegNumber.Value = "Ya"
            ElseIf .Tax_Reg_Number = False Then
                RP_TaxRegNumber.Value = "Tidak"
            End If
            RP_SourceOfWealth.Text = .Source_Of_Wealth
            RP_Comments.Text = .Comment
        End With


        If ReportingPersonClass.ObjAddress.Count > 0 Then
            bindAddress(AddressField_Store, ReportingPersonClass.ObjAddress)
        End If

        If ReportingPersonClass.ObjEmpAddress.Count > 0 Then
            bindAddress(StoreAddressEmployer, ReportingPersonClass.ObjEmpAddress)
        End If

        If ReportingPersonClass.ObjPhone.Count > 0 Then
            bindPhone(PhoneField_Store, ReportingPersonClass.ObjPhone)
        End If

        If ReportingPersonClass.ObjEmpPhone.Count > 0 Then
            bindPhone(StorePhoneEmployer, ReportingPersonClass.ObjEmpPhone)
        End If

        If ReportingPersonClass.ObjIdent.Count > 0 Then
            bindIdentification(IDField_Store, ReportingPersonClass.ObjIdent)
        End If

    End Sub
    Sub bindAddress(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Ref_Address))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        objtable.Columns.Add(New DataColumn("kontak", GetType(String)))
        objtable.Columns.Add(New DataColumn("address", GetType(String)))
        objtable.Columns.Add(New DataColumn("city", GetType(String)))
        objtable.Columns.Add(New DataColumn("negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_Customer_Address_ID") = item("PK_Customer_Address_ID")
                item("kontak") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("address") = item("Address")
                item("city") = item("City")
                Dim Negara As String = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Country_Code").ToString)

                item("negara") = Negara
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub bindPhone(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Ref_Phone))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("kategori_kontak", GetType(String)))
        objtable.Columns.Add(New DataColumn("ja_komunikasi", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_Ref_Phone") = item("PK_goAML_Ref_Phone")
                item("kategori_kontak") = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
                item("ja_komunikasi") = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub bindIdentification(store As Ext.Net.Store, listIdentification As List(Of NawaDevDAL.goAML_Person_Identification))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listIdentification)
        objtable.Columns.Add(New DataColumn("type", GetType(String)))
        objtable.Columns.Add(New DataColumn("no", GetType(String)))
        objtable.Columns.Add(New DataColumn("negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_Person_Identification_ID") = item("PK_Person_Identification_ID")
                item("type") = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(item("Type").ToString)
                item("no") = item("Number")
                item("negara") = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(item("Issued_Country").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Protected Sub GridCommandIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployer(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhoneEmployer(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandAddressEmployer(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddressEmployer(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataDetailIdentification(id As Long)
        Dim Identification As New goAML_Person_Identification
        Identification = ReportingPersonClass.ObjIdent.Find(Function(x) x.PK_Person_Identification_ID = id)
        IDModalDetail.Hidden = False
        With Identification
            RP_jenis_identitas_detail.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
            RP_no_identitas_detail.Text = .Number
            If .Issue_Date.HasValue Then
                RP_tgl_terbit_detail.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
            End If
            If .Expiry_Date.HasValue Then
                RP_tgl_kadaluarsa_detail.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
            End If
            RP_penerbit_detail.Text = .Issued_By
            RP_negara_penerbit_detail.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
            RP_note_identitas_detail.Text = .Identification_Comment
        End With
    End Sub
    Private Sub LoadDataDetailPhone(id As Long)
        Dim Phone As New goAML_Ref_Phone
        Phone = ReportingPersonClass.ObjPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        PhoneModalDetail.Hidden = False
        With Phone
            RP_tipe_kontak_detail.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
            RP_jenis_komunikasi_detail.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
            RP_kode_area_detail.Text = .tph_country_prefix
            RP_no_telepon_detail.Text = .tph_number
            RP_phone_ext_detail.Text = .tph_extension
            RP_note_phone_detail.Text = .comments
        End With
    End Sub
    Private Sub LoadDataDetailAddress(id As Long)
        Dim Address As New goAML_Ref_Address
        Address = ReportingPersonClass.ObjAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        AddrModalDetail.Hidden = False
        'ClearInput()
        With Address
            RP_tipe_alamat_detail.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
            RP_alamat_detail.Text = .Address
            RP_kecamatan_detail.Text = .Town
            RP_kabupaten_detail.Text = .City
            RP_kodepos_detail.Text = .Zip
            RP_negara_detail.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
            RP_propinsi_detail.Text = .State
            RP_note_alamat_detail.Text = .Comments
        End With
    End Sub
    Private Sub LoadDataDetailPhoneEmployer(id As Long)
        Dim PhoneEmp As New goAML_Ref_Phone
        PhoneEmp = ReportingPersonClass.ObjEmpPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        WindowEmployerPhoneDetail.Hidden = False
        'ClearInput()
        With PhoneEmp
            EmployerPhoneDetail_Kategori.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
            EmployerPhoneDetail_jenis.Text = NawaDevBLL.GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
            EmployerPhoneDetail_kode.Text = .tph_country_prefix.Trim
            EmployerPhoneDetail_nomor.Text = .tph_number
            EmployerPhoneDetail_extensi.Text = .tph_extension
            EmployerPhoneDetail_note.Text = .comments
        End With
    End Sub
    Private Sub LoadDataDetailAddressEmployer(id As Long)
        Dim AddressEmp As New goAML_Ref_Address
        AddressEmp = ReportingPersonClass.ObjEmpAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        WindowEmployerAddressDetail.Hidden = False
        'ClearInput()
        With AddressEmp
            EmployerAddressDetail_tipe.Text = NawaDevBLL.GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
            EmployerAddressDetail_alamat.Text = .Address
            EmployerAddressDetail_kec.Text = .Town
            EmployerAddressDetail_kab.Text = .City
            EmployerAddressDetail_pos.Text = .Zip
            EmployerAddressDetail_negara.Text = NawaDevBLL.GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
            EmployerAddressDetail_prov.Text = .State
            EmployerAddressDetail_note.Text = .Comments
        End With
    End Sub
    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If


        If bsettingRight = 1 Then
            'GridPanelReportIndicator.ColumnModel.Columns.Add(CommandColumn87)
        ElseIf bsettingRight = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If



    End Sub
#Region "Direct Events"
    Protected Sub Callback(sender As Object, e As DirectEventArgs)
        Try
            Select Case e.ExtraParams("command")
                Case "Delete"
                    Dim objReportingPerson As New NawaDevBLL.ReportingPerson
                    If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not objmodule.IsUseApproval Then
                        objReportingPerson.DeleteTanpaapproval(StrUnikKey, objmodule)
                        LblConfirmation.Text = "Data  " & objmodule.ModuleLabel & " is deleted."
                    Else
                        objReportingPerson.DeleteDenganapproval(StrUnikKey, objmodule)
                        LblConfirmation.Text = "Data " & objmodule.ModuleLabel & " Saved into Pending Approval"
                    End If

                    Panelconfirmation.Hidden = False
                    FormPanelInput.Hidden = True
            End Select
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
    Private Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs) Handles BtnConfirmation.DirectClick
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objmodule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objmodule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub PhoneModal_BtnCancel(sender As Object, e As DirectEventArgs)
        PhoneModalDetail.Hide()
        WindowEmployerPhoneDetail.Hide()
    End Sub
    Protected Sub AddressModal_BtnCancel(sender As Object, e As DirectEventArgs)
        AddrModalDetail.Hide()
        WindowEmployerAddressDetail.Hide()
    End Sub
    Protected Sub IDModal_BtnCancel(sender As Object, e As DirectEventArgs)
        IDModalDetail.Hide()
    End Sub
#End Region

End Class
