﻿Imports NawaBLL
Imports Ext
Imports NawaDevBLL
Imports System.Data.SqlClient
Imports NawaDAL
Imports NawaDevDAL
Imports Elmah
Imports System.Data

Partial Class goAML_WICAdd
    Inherits Parent
    Public goAML_WICBLL As New WICBLL()
#Region "Session"
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("goAML_WICAdd.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_WICAdd.ObjModule") = value
        End Set
    End Property
    Public Property objWICAddData() As goAML_Ref_WIC
        Get
            If Session("goAML_WICAdd.objWICAddData") Is Nothing Then
                Session("goAML_WICAdd.objWICAddData") = New goAML_Ref_WIC
            End If
            Return Session("goAML_WICAdd.objWICAddData")
        End Get
        Set(ByVal value As goAML_Ref_WIC)
            Session("goAML_WICAdd.objWICAddData") = value
        End Set
    End Property
    Public Property objDirectorAddData() As goAML_Ref_Walk_In_Customer_Director
        Get
            Return Session("goAML_WICAdd.objDirectorAddData")
        End Get
        Set(ByVal value As goAML_Ref_Walk_In_Customer_Director)
            Session("goAML_WICAdd.objDirectorAddData") = value
        End Set
    End Property
    Public Property ListDirectorDetail As List(Of goAML_Ref_Walk_In_Customer_Director)
        Get
            If Session("goAML_WICAdd.ListDirectorDetail") Is Nothing Then
                Session("goAML_WICAdd.ListDirectorDetail") = New List(Of goAML_Ref_Walk_In_Customer_Director)
            End If
            Return Session("goAML_WICAdd.ListDirectorDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Walk_In_Customer_Director))
            Session("goAML_WICAdd.ListDirectorDetail") = value
        End Set
    End Property
    Public Property ListDirectorDetailClass As List(Of WICDirectorDataBLL)
        Get
            Return Session("goAML_WICAdd.ListDirectorDetailClass")
        End Get
        Set(ByVal value As List(Of WICDirectorDataBLL))
            Session("goAML_WICAdd.ListDirectorDetailClass") = value
        End Set
    End Property
    Public Property ListDirectorPhoneDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICAdd.ListDirectorPhoneDetail") Is Nothing Then
                Session("goAML_WICAdd.ListDirectorPhoneDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICAdd.ListDirectorPhoneDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICAdd.ListDirectorPhoneDetail") = value
        End Set
    End Property
    Public Property ListDirectorIdentificationDetail As List(Of goAML_Person_Identification)
        Get
            If Session("goAML_WICAdd.ListDirectorIdentificationDetail") Is Nothing Then
                Session("goAML_WICAdd.ListDirectorIdentificationDetail") = New List(Of goAML_Person_Identification)
            End If
            Return Session("goAML_WICAdd.ListDirectorIdentificationDetail")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_WICAdd.ListDirectorIdentificationDetail") = value
        End Set
    End Property
    Public Property ListDirectorEmployerPhoneDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICAdd.ListDirectorEmployerPhoneDetail") Is Nothing Then
                Session("goAML_WICAdd.ListDirectorEmployerPhoneDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICAdd.ListDirectorEmployerPhoneDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICAdd.ListDirectorEmployerPhoneDetail") = value
        End Set
    End Property
    Public Property ListDirectorAddressDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICAdd.ListDirectorAddressDetail") Is Nothing Then
                Session("goAML_WICAdd.ListDirectorAddressDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICAdd.ListDirectorAddressDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICAdd.ListDirectorAddressDetail") = value
        End Set
    End Property
    Public Property ListDirectorEmployerAddressDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICAdd.ListDirectorEmployerAddressDetail") Is Nothing Then
                Session("goAML_WICAdd.ListDirectorEmployerAddressDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICAdd.ListDirectorEmployerAddressDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICAdd.ListDirectorEmployerAddressDetail") = value
        End Set
    End Property
    Public Property ListPhoneDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICAdd.ListPhoneDetail") Is Nothing Then
                Session("goAML_WICAdd.ListPhoneDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICAdd.ListPhoneDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICAdd.ListPhoneDetail") = value
        End Set
    End Property
    Public Property ListAddressDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICAdd.ListAddressDetail") Is Nothing Then
                Session("goAML_WICAdd.ListAddressDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICAdd.ListAddressDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICAdd.ListAddressDetail") = value
        End Set
    End Property
    Public Property ListPhoneEmployerDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICAdd.ListPhoneEmployerDetail") Is Nothing Then
                Session("goAML_WICAdd.ListPhoneEmployerDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICAdd.ListPhoneEmployerDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICAdd.ListPhoneEmployerDetail") = value
        End Set
    End Property
    Public Property ListAddressEmployerDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICAdd.ListAddressEmployerDetail") Is Nothing Then
                Session("goAML_WICAdd.ListAddressEmployerDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICAdd.ListAddressEmployerDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICAdd.ListAddressEmployerDetail") = value
        End Set
    End Property
    Public Property ListIdentificationDetail As List(Of goAML_Person_Identification)
        Get
            If Session("goAML_WICAdd.ListIdentificationDetail") Is Nothing Then
                Session("goAML_WICAdd.ListIdentificationDetail") = New List(Of goAML_Person_Identification)
            End If
            Return Session("goAML_WICAdd.ListIdentificationDetail")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_WICAdd.ListIdentificationDetail") = value
        End Set
    End Property
    Public Property ObjDetailPhone As goAML_Ref_Phone
        Get
            Return Session("goAML_WICAdd.ObjDetailPhone")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICAdd.ObjDetailPhone") = value
        End Set
    End Property
    Public Property ObjDetailPhoneEmployer As goAML_Ref_Phone
        Get
            Return Session("goAML_WICAdd.ObjDetailPhoneEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICAdd.ObjDetailPhoneEmployer") = value
        End Set
    End Property
    Public Property ObjDetailPhoneDirector As goAML_Ref_Phone
        Get
            Return Session("goAML_WICAdd.ObjDetailPhoneDirector")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICAdd.ObjDetailPhoneDirector") = value
        End Set
    End Property
    Public Property ObjDetailIdentification As goAML_Person_Identification
        Get
            Return Session("goAML_WICAdd.ObjDetailIdentification")
        End Get
        Set(ByVal value As goAML_Person_Identification)
            Session("goAML_WICAdd.ObjDetailIdentification") = value
        End Set
    End Property
    Public Property ObjDetailIdentificationDirector As goAML_Person_Identification
        Get
            Return Session("goAML_WICAdd.ObjDetailIdentificationDirector")
        End Get
        Set(ByVal value As goAML_Person_Identification)
            Session("goAML_WICAdd.ObjDetailIdentificationDirector") = value
        End Set
    End Property
    Public Property ObjDetailPhoneDirectorEmployer As goAML_Ref_Phone
        Get
            Return Session("goAML_WICAdd.ObjDetailPhoneDirectorEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICAdd.ObjDetailPhoneDirectorEmployer") = value
        End Set
    End Property
    Public Property ObjDetailAddress As goAML_Ref_Address
        Get
            Return Session("goAML_WICAdd.ObjDetailAddress")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICAdd.ObjDetailAddress") = value
        End Set
    End Property
    Public Property ObjDetailAddressEmployer As goAML_Ref_Address
        Get
            Return Session("goAML_WICAdd.ObjDetailAddressEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICAdd.ObjDetailAddressEmployer") = value
        End Set
    End Property
    Public Property ObjDetailAddressDirector As goAML_Ref_Address
        Get
            Return Session("goAML_WICAdd.ObjDetailAddressDirector")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICAdd.ObjDetailAddressDirector") = value
        End Set
    End Property
    Public Property ObjDetailAddressDirectorEmployer As goAML_Ref_Address
        Get
            Return Session("goAML_WICAdd.ObjDetailAddressDirectorEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICAdd.ObjDetailAddressDirectorEmployer") = value
        End Set
    End Property
    Public Property DataRowTemp() As DataRow
        Get
            Return Session("goAML_WICEDIT.DataRowTemp")
        End Get
        Set(ByVal value As DataRow)
            Session("goAML_WICEDIT.DataRowTemp") = value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
        goAML_WICBLL = New WICBLL(WIC_Panel)
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                Dim strmodule As String = Request.Params("ModuleID")
                ClearSession()
                Dim intmodule As Integer = Common.DecryptQueryString(strmodule, SystemParameterBLL.GetEncriptionKey)
                ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)

                If Not ObjModule Is Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Insert) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                    WIC_Individu.Visible = True
                    WIC_Corporate.Visible = False

                    Dim objrand As New Random
                    objWICAddData.PK_Customer_ID = objrand.Next

                    goAML_WICBLL = New WICBLL()

                    LoadNegara()
                    LoadBadanUsaha()
                    LoadGender()
                    LoadParty()
                    LoadKategoriKontak()
                    LoadJenisAlatKomunikasi()
                    LoadTypeIdentitas()
                    LoadPopup()
                    LoadColumn()

                    cboFormWIC.SelectedItem.Text = "Individu"
                Else
                    Throw New Exception("Invalid Module ID")
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
#Region "LoadData"
    Private Sub LoadColumn()

        ColumnActionLocation(GridPanel1, CommandColumn1)
        ColumnActionLocation(GridPanel2, CommandColumn2)
        ColumnActionLocation(GridPanel3, CommandColumn3)
        ColumnActionLocation(GridPanel4, CommandColumn4)
        ColumnActionLocation(GridPanel5, CommandColumn5)

        ColumnActionLocation(GP_DirectorPhone, CommandColumn11)
        ColumnActionLocation(GP_DirectorAddress, CommandColumn12)
        ColumnActionLocation(GP_DirectorEmpPhone, CommandColumn9)
        ColumnActionLocation(GP_DirectorEmpAddress, CommandColumn10)
        ColumnActionLocation(GP_DirectorIdentification, CommandColumn6)

        ColumnActionLocation(GridPanelPhoneIndividuDetail, CommandColumnPhoneIndividuDetail)
        ColumnActionLocation(GridPanelAddressIndividuDetail, CommandColumnAddressIndividuDetail)
        ColumnActionLocation(GridPanelPhoneIndividualEmployerDetail, CommandColumnPhoneIndividualEmployerDetail)
        ColumnActionLocation(GridPanelAddressIndividualEmployerDetail, CommandColumnAddressIndividualEmployerDetail)
        ColumnActionLocation(GridPanelIdentificationIndividualDetail, CommandColumnIdentificationIndividualDetail)

        ColumnActionLocation(GridPanelPhoneCorporateDetail, CommandColumnPhoneCorporateDetail)
        ColumnActionLocation(GridPanelAddressCorporateDetail, CommandColumnAddressCorporateDetail)
        ColumnActionLocation(GP_Corp_Director, CommandColumnDirector)

    End Sub
    Private Sub LoadPopup()
        WindowDetailAddress.Maximizable = True
        WindowDetailPhone.Maximizable = True
        WindowDetailDirector.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True
        WindowDetailDirectorIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailIdentification.Maximizable = True
    End Sub
    Protected Sub National_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Nama_Negara", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Communication_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Alat_Komunikasi", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Contact_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Kategori_Kontak", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Identification_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goaml_ref_jenis_dokumen_identitas", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Incorporation_legal_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Bentuk_Badan_Usaha", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Gender_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Kelamin", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Director_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_ODM_Reporting_Person", "PK_ODM_Reporting_Person_ID, Last_Name", strfilter, "Last_Name", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Role_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Peran_orang_dalam_Korporasi", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Private Sub LoadJenisAlatKomunikasi()
        Cmbtph_communication_type.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignCommunication_Type.Reload()
        Director_Emp_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreEmpDirComunicationType.Reload()
        Director_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreDirComunicationType.Reload()
        Cmbtph_communication_typeEmployer.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignCommunication_TypeEmployer.Reload()
    End Sub

    Private Sub LoadKategoriKontak()

        Cmbtph_contact_type.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignContact_Type.Reload()
        Cmbtph_contact_typeEmployer.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignContact_TypeEmployer.Reload()

        CmbAddress_type.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignAddress_Type.Reload()
        CmbAddress_typeEmployer.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignAddress_TypeEmployer.Reload()

        Director_Emp_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreEmpDirPContactType.Reload()
        Director_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreDirPContactType.Reload()

        Director_cmb_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        StoreDirKategoryADdressAddress_Type.Reload()
        Director_cmb_emp_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        StoreEmpDirKategoryADdressAddress_Type.Reload()

    End Sub
    Private Sub LoadTypeIdentitas()
        CmbTypeIdentification.PageSize = SystemParameterBLL.GetPageSize
        StoreTypeIdentification.Reload()

        CmbTypeIdentificationDirector.PageSize = SystemParameterBLL.GetPageSize
        StoreTypeIdentificationDirector.Reload()
    End Sub
    Private Sub LoadParty()
        cmb_peran.PageSize = SystemParameterBLL.GetPageSize
        StorePeran.Reload()
    End Sub
    Private Sub LoadGender()
        cmb_Director_Gender.PageSize = SystemParameterBLL.GetPageSize
        StoreGender.Reload()

        CmbINDV_Gender.PageSize = SystemParameterBLL.GetPageSize
        StoreGenderINDV.Reload()
    End Sub
    Private Sub LoadBadanUsaha()
        CmbCorp_Incorporation_legal_form.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignIncorporation_legal.Reload()
    End Sub
    Private Sub LoadNegara()
        'CmbINDV_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignNational1.Reload()
        'CmbINDV_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignNational2.Reload()
        'CmbINDV_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignNational3.Reload()
        'CmbINDV_residence.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignResisdance.Reload()
        'TxtINDV_Passport_country.PageSize = SystemParameterBLL.GetPageSize
        'StoreIndividuPassportCountry.Reload()
        'txt_Director_Passport_Country.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirectorPassportNumber.Reload()

        'CmbCorp_incorporation_country_code.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignCountry_Code.Reload()

        'Cmbcountry_code.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignCountry_CodeAddress.Reload()
        'Cmbcountry_codeEmployer.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignCountry_CodeAddressEmployer.Reload()

        'cmb_Director_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirNational1.Reload()
        'cmb_Director_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirNational2.Reload()
        'cmb_Director_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirNational3.Reload()
        'cmb_Director_Residence.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirReisdence.Reload()

        'Director_cmb_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirFromForeignCountry_CodeAddress.Reload()
        'Director_cmb_emp_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'StoreEmpDirFromForeignCountry_CodeAddress.Reload()

        'CmbCountryIdentification.PageSize = SystemParameterBLL.GetPageSize
        'StoreCountryCodeIdentification.Reload()
        'CmbCountryIdentificationDirector.PageSize = SystemParameterBLL.GetPageSize
        'StoreCountryCodeIdentificationDirector.Reload()

    End Sub

    Private Sub BindDetailIdentification(store As Store, list As List(Of goAML_Person_Identification))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Tipe", GetType(String)))
        objtable.Columns.Add(New DataColumn("No", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssueDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("ExpiryDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssuedCountry", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Tipe") = GlobalReportFunctionBLL.getjenisDokumenByKode(item("Type"))
                item("No") = item("Number")
                item("IssueDate") = item("Issue_Date")
                item("ExpiryDate") = item("Expiry_Date")
                item("IssuedCountry") = GlobalReportFunctionBLL.getCountryByCode(item("Issued_Country"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailPhone(store As Store, list As List(Of goAML_Ref_Phone))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
                item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailAddress(store As Store, list As List(Of goAML_Ref_Address))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Type_Address", GetType(String)))
        objtable.Columns.Add(New DataColumn("Addres", GetType(String)))
        objtable.Columns.Add(New DataColumn("Cty", GetType(String)))
        objtable.Columns.Add(New DataColumn("CountryCode", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Type_Address") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type"))
                item("Addres") = item("Address")
                item("Cty") = item("City")
                item("CountryCode") = GlobalReportFunctionBLL.getCountryByCode(item("Country_Code"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailDirector(store As Store, list As List(Of goAML_Ref_Walk_In_Customer_Director))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("LastName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Roles", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("LastName") = item("Last_Name")
                item("Roles") = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(item("Role"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
#End Region
#Region "Clear"
    Private Sub ClearSession()
        objWICAddData = Nothing
        'ObjModule = Nothing
        objDirectorAddData = Nothing
        ListDirectorDetailClass = New List(Of WICDirectorDataBLL)
        ListAddressDetail = New List(Of goAML_Ref_Address)
        ListPhoneDetail = New List(Of goAML_Ref_Phone)
        ListAddressEmployerDetail = New List(Of goAML_Ref_Address)
        ListPhoneEmployerDetail = New List(Of goAML_Ref_Phone)
        ListIdentificationDetail = New List(Of goAML_Person_Identification)

        ListDirectorDetail = New List(Of goAML_Ref_Walk_In_Customer_Director)
        ListDirectorAddressDetail = New List(Of goAML_Ref_Address)
        ListDirectorPhoneDetail = New List(Of goAML_Ref_Phone)
        ListDirectorEmployerAddressDetail = New List(Of goAML_Ref_Address)
        ListDirectorEmployerPhoneDetail = New List(Of goAML_Ref_Phone)
        ListDirectorIdentificationDetail = New List(Of goAML_Person_Identification)

        ObjDetailAddress = Nothing
        ObjDetailPhone = Nothing
        ObjDetailAddressEmployer = Nothing
        ObjDetailPhoneEmployer = Nothing
        ObjDetailIdentification = Nothing
        ObjDetailAddressDirector = Nothing
        ObjDetailAddressDirectorEmployer = Nothing
        ObjDetailPhoneDirector = Nothing
        ObjDetailPhoneDirectorEmployer = Nothing
        ObjDetailIdentificationDirector = Nothing

    End Sub
    Private Sub clearinputDirector()
        Director_cb_phone_Contact_Type.ClearValue()
        Director_cb_phone_Communication_Type.ClearValue()
        Director_txt_phone_Country_prefix.Clear()
        Director_txt_phone_number.Clear()
        Director_txt_phone_extension.Clear()
        Director_txt_phone_comments.Clear()

        Director_Emp_cb_phone_Contact_Type.ClearValue()
        Director_Emp_cb_phone_Communication_Type.ClearValue()
        Director_Emp_txt_phone_Country_prefix.Clear()
        Director_Emp_txt_phone_number.Clear()
        Director_Emp_txt_phone_extension.Clear()
        Director_Emp_txt_phone_comments.Clear()

        Director_cmb_kategoriaddress.ClearValue()
        Director_Address.Clear()
        Director_Town.Clear()
        Director_City.Clear()
        Director_Zip.Clear()
        Director_cmb_kodenegara.SetTextValue("")
        Director_State.Clear()
        Director_Comment_Address.Clear()

        Director_cmb_emp_kategoriaddress.ClearValue()
        Director_emp_Address.Clear()
        Director_emp_Town.Clear()
        Director_emp_City.Clear()
        Director_emp_Zip.Clear()
        Director_cmb_emp_kodenegara.SetTextValue("")
        Director_emp_State.Clear()
        Director_emp_Comment_Address.Clear()

        CmbTypeIdentificationDirector.ClearValue()
        TxtNumberDirector.Clear()
        TxtIssueDateDirector.Clear()
        TxtExpiryDateDirector.Clear()
        TxtIssuedByDirector.Clear()
        CmbCountryIdentificationDirector.SetTextValue("")
        TxtCommentIdentificationDirector.Clear()
    End Sub
    Private Sub ClearInput()

        'Phone WIC
        Cmbtph_contact_type.ClearValue()
        Cmbtph_communication_type.ClearValue()
        Txttph_country_prefix.Clear()
        Txttph_number.Clear()
        Txttph_extension.Clear()
        TxtcommentsPhone.Clear()

        'Address WIC
        CmbAddress_type.ClearValue()
        TxtAddress.Clear()
        TxtTown.Clear()
        TxtCity.Clear()
        TxtZip.Clear()
        Cmbcountry_code.SetTextValue("")
        TxtState.Clear()
        TxtcommentsAddress.Clear()

        'Identification WIC
        CmbTypeIdentification.ClearValue()
        TxtNumber.Clear()
        TxtIssueDate.Clear()
        TxtExpiryDate.Clear()
        TxtIssuedBy.Clear()
        CmbCountryIdentification.SetTextValue("")
        TxtCommentIdentification.Clear()

        'Phone Employer WIC
        Cmbtph_contact_typeEmployer.ClearValue()
        Cmbtph_communication_typeEmployer.ClearValue()
        Txttph_country_prefixEmployer.Clear()
        Txttph_numberEmployer.Clear()
        Txttph_extensionEmployer.Clear()
        TxtcommentsPhoneEmployer.Clear()

        'Address Employer WIC
        CmbAddress_typeEmployer.ClearValue()
        TxtAddressEmployer.Clear()
        TxtTownEmployer.Clear()
        TxtCityEmployer.Clear()
        TxtZipEmployer.Clear()
        Cmbcountry_codeEmployer.SetTextValue("")
        TxtStateEmployer.Clear()
        TxtcommentsAddressEmployer.Clear()

        'list
        ListDirectorEmployerAddressDetail.Clear()
        ListDirectorEmployerPhoneDetail.Clear()
        ListDirectorAddressDetail.Clear()
        ListDirectorPhoneDetail.Clear()
        ListDirectorIdentificationDetail.Clear()

        'Director
        cmb_peran.ClearValue()
        txt_Director_Title.Clear()
        txt_Director_Last_Name.Clear()
        cmb_Director_Gender.ClearValue()
        txt_Director_BirthDate.Clear()
        txt_Director_Birth_Place.Clear()
        txt_Director_Mothers_Name.Clear()
        txt_Director_Alias.Clear()
        txt_Director_ID_Number.Clear()
        txt_Director_Passport_Number.Clear()
        'txt_Director_Passport_Country.Clear()
        txt_Director_Passport_Country.SetTextValue("")
        txt_Director_SSN.Clear()
        cmb_Director_Nationality1.SetTextValue("")
        cmb_Director_Nationality2.SetTextValue("")
        cmb_Director_Nationality3.SetTextValue("")
        cmb_Director_Residence.SetTextValue("")
        'cmb_Director_Nationality1.ClearValue()
        'cmb_Director_Nationality2.ClearValue()
        'cmb_Director_Nationality3.ClearValue()
        'cmb_Director_Residence.ClearValue()
        txt_Director_Email.Clear()
        txt_Director_Email2.Clear()
        txt_Director_Email3.Clear()
        txt_Director_Email4.Clear()
        txt_Director_Email5.Clear()
        cb_Director_Deceased.Clear()
        txt_Director_Deceased_Date.Clear()
        cb_Director_Tax_Reg_Number.Clear()
        txt_Director_Tax_Number.Clear()
        txt_Director_Source_of_Wealth.Clear()
        txt_Director_Occupation.Clear()
        txt_Director_Comments.Clear()
        txt_Director_employer_name.Clear()

        'danie 21 jan 2021
        objDirectorAddData = Nothing
        'end 21 jan 2021
        BindDetailPhone(Store_DirectorPhone, ListDirectorPhoneDetail)
        BindDetailPhone(Store_Director_Employer_Phone, ListDirectorEmployerPhoneDetail)
        BindDetailAddress(Store_DirectorAddress, ListDirectorAddressDetail)
        BindDetailAddress(Store_Director_Employer_Address, ListDirectorEmployerAddressDetail)
        BindDetailIdentification(StoreIdentificationDirectorDetail, ListDirectorIdentificationDetail)
    End Sub


#End Region
#Region "Button"
    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            objWICAddData = Nothing
            If TxtWIC_No.Text = "" Then
                Throw New Exception("WIC No Tidak boleh kosong")
            Else
                If cboFormWIC.SelectedItem.Value = "Individu" Then
                    If TxtINDV_last_name.Text = "" Then
                        Throw New Exception("Nama Lengkap Tidak boleh kosong")
                        'If TxtINDV_last_name.Text = "" Then
                        '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
                        'ElseIf DateINDV_Birthdate.Text = "1/1/0001 12:00:00 AM" Then
                        '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
                        'ElseIf TxtINDV_Birth_place.Text = "" Then
                        '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
                        'ElseIf CmbINDV_Nationality1.TextValue = "" Then
                        '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
                        'ElseIf CmbINDV_residence.TextValue = "" Then
                        '    Throw New Exception("Negara Domisili Tidak boleh kosong")
                        'ElseIf ListPhoneDetail.Count <= 0 Then
                        '    Throw New Exception("Informasi Telepon Tidak boleh kosong")
                        'ElseIf ListAddressDetail.Count <= 0 Then
                        '    Throw New Exception("Informasi Alamat Tidak boleh kosong")
                        'ElseIf TxtINDV_Occupation.Text = "" Then
                        '    Throw New Exception("Pekerjaan Tidak boleh kosong")
                        'ElseIf TxtINDV_Source_Of_Wealth.Text = "" Then
                        '    Throw New Exception("Sumber Dana Tidak boleh kosong")
                    Else
                        If TxtINDV_SSN.Text.Length <> 16 And TxtINDV_SSN.Text.Length > 0 Then
                            Throw New Exception("NIK Must 16 Digit")
                        Else
                            With objWICAddData
                                .WIC_No = TxtWIC_No.Text
                                .isUpdateFromDataSource = CheckboxIsUpdateFromDataSource.Checked
                                .INDV_Title = TxtINDV_Title.Text
                                .INDV_Last_Name = TxtINDV_last_name.Text
                                .INDV_Gender = CmbINDV_Gender.Text
                                'agam 03112020
                                If Not DateINDV_Birthdate.SelectedDate = DateTime.MinValue Then
                                    .INDV_BirthDate = DateINDV_Birthdate.SelectedDate
                                End If
                                .INDV_Birth_Place = TxtINDV_Birth_place.Text
                                .INDV_Mothers_Name = TxtINDV_Mothers_name.Text
                                .INDV_Alias = TxtINDV_Alias.Text
                                .INDV_SSN = TxtINDV_SSN.Text
                                .INDV_Passport_Number = TxtINDV_Passport_number.Text
                                .INDV_Passport_Country = TxtINDV_Passport_country.TextValue
                                .INDV_ID_Number = TxtINDV_ID_Number.Text
                                .INDV_Nationality1 = CmbINDV_Nationality1.TextValue
                                .INDV_Nationality2 = CmbINDV_Nationality2.TextValue
                                .INDV_Nationality3 = CmbINDV_Nationality3.TextValue
                                .INDV_Residence = CmbINDV_residence.TextValue
                                .INDV_Email = TxtINDV_Email.Text
                                .INDV_Email2 = TxtINDV_Email2.Text
                                .INDV_Email3 = TxtINDV_Email3.Text
                                .INDV_Email4 = TxtINDV_Email4.Text
                                .INDV_Email5 = TxtINDV_Email5.Text
                                .INDV_Occupation = TxtINDV_Occupation.Text
                                .INDV_Employer_Name = TxtINDV_employer_name.Text
                                .INDV_SumberDana = TxtINDV_Source_Of_Wealth.Text
                                .INDV_Tax_Number = TxtINDV_Tax_Number.Text
                                .INDV_Tax_Reg_Number = CbINDV_Tax_Reg_Number.Checked
                                .INDV_Deceased = CbINDV_Deceased.Checked
                                If CbINDV_Deceased.Checked Then
                                    'agam 03112020
                                    If Not DateINDV_Deceased_Date.SelectedDate = DateTime.MinValue Then
                                        .INDV_Deceased_Date = DateINDV_Deceased_Date.SelectedDate
                                    End If
                                    '.INDV_Deceased_Date = Convert.ToDateTime(DateINDV_Deceased_Date.Text)
                                End If
                                .INDV_Comment = TxtINDV_Comments.Text
                                .FK_Customer_Type_ID = 1
                                'agam 03112020
                                '.Corp_Incorporation_Date = DateCorp_incorporation_date.RawValue
                                '.Corp_Date_Business_Closed = DateCorp_date_business_closed.RawValue

                            End With

                            If Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                                goAML_WICBLL.SaveAddTanpaApproval(objWICAddData, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule)
                                WIC_Panel.Hidden = True
                                Panelconfirmation.Hidden = False
                                LblConfirmation.Text = "Data Saved into Database."
                            Else
                                goAML_WICBLL.SaveAddApproval(objWICAddData, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule)
                                WIC_Panel.Hidden = True
                                Panelconfirmation.Hidden = False
                                LblConfirmation.Text = "Data Saved into Pending Approval."
                            End If
                        End If
                    End If
                Else
                    If TxtCorp_Name.Text = "" Then
                        Throw New Exception("Nama Korporasi Tidak boleh kosong")
                        'ElseIf TxtCorp_Business.Text = "" Then
                        '    Throw New Exception("Bidang Usaha Tidak boleh kosong")
                        'ElseIf ListAddressDetail.Count <= 0 Then
                        '    Throw New Exception("Informasi Alamat Tidak boleh kosong")
                        'ElseIf CmbCorp_incorporation_country_code.TextValue = "" Then
                        '    Throw New Exception("Negara Tidak boleh kosong")
                    Else
                        With objWICAddData
                            .WIC_No = TxtWIC_No.Text
                            .isUpdateFromDataSource = CheckboxIsUpdateFromDataSource.Checked
                            .Corp_Name = TxtCorp_Name.Text
                            .Corp_Commercial_Name = TxtCorp_Commercial_name.Text
                            .Corp_Incorporation_Legal_Form = CmbCorp_Incorporation_legal_form.Text
                            .Corp_Incorporation_Number = TxtCorp_Incorporation_number.Text
                            .Corp_Business = TxtCorp_Business.Text
                            .Corp_Email = TxtCorp_Email.Text
                            .Corp_Url = TxtCorp_url.Text
                            .Corp_Incorporation_State = TxtCorp_incorporation_state.Text
                            .Corp_Incorporation_Country_Code = CmbCorp_incorporation_country_code.TextValue
                            'agam 03112020
                            If Not DateCorp_incorporation_date.SelectedDate = DateTime.MinValue Then
                                .Corp_Incorporation_Date = DateCorp_incorporation_date.SelectedDate
                            End If
                            'Update BSIM 01-10-2020
                            '.Corp_Incorporation_Date = DateTime.Parse(DateCorp_incorporation_date.Value)
                            .Corp_Business_Closed = chkCorp_business_closed.Checked
                            If chkCorp_business_closed.Checked Then
                                'agam 03112020
                                If Not DateCorp_date_business_closed.SelectedDate = DateTime.MinValue Then
                                    .Corp_Date_Business_Closed = DateCorp_date_business_closed.SelectedDate
                                End If
                                '.Corp_Date_Business_Closed = Convert.ToDateTime(DateCorp_date_business_closed.Text)
                            End If
                            .Corp_Tax_Number = TxtCorp_tax_number.Text
                            .Corp_Comments = TxtCorp_Comments.Text
                            .FK_Customer_Type_ID = 2
                            '.INDV_BirthDate = DateINDV_Birthdate.RawValue
                        End With

                        If Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                            goAML_WICBLL.SaveAddTanpaApproval(objWICAddData, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule)
                            WIC_Panel.Hidden = True
                            Panelconfirmation.Hidden = False
                            LblConfirmation.Text = "Data Saved into Database."
                        Else
                            goAML_WICBLL.SaveAddApproval(objWICAddData, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule)
                            WIC_Panel.Hidden = True
                            Panelconfirmation.Hidden = False
                            LblConfirmation.Text = "Data Saved into Pending Approval."
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorIdentification_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()
            WindowDetailDirectorIdentification.Title = "Add Identitas"
            WindowDetailDirectorIdentification.Show()

            FormPanelDirectorIdentification.Show()
            PanelDetailDirectorIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddIdentification_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()

            WindowDetailIdentification.Show()

            FormPanelIdentification.Show()
            PanelDetailIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewPhoneIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            WindowDetailPhone.Title = "Add Phone"
            WindowDetailPhone.Show()

            FormPanelPhoneDetail.Show()
            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewPhoneEmployerIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            'Daniel 2020 Des 28
            If ListPhoneEmployerDetail.Count < 1 Then
                'End 2020 Des 28
                ClearInput()
                WindowDetailPhone.Title = "Add Phone"
                WindowDetailPhone.Show()

                FormPanelPhoneDetail.Hide()
                PanelDetailPhone.Hide()
                PanelDetailPhoneEmployer.Hide()
                FormPanelPhoneEmployerDetail.Show()
                'Daniel 2020 Des 28
            Else
                Throw New Exception("can only enter one Workplace Phone Information")
            End If
            'end 2020 Des 28
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirector_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()

            WindowDetailAddress.Hide()
            WindowDetailPhone.Hide()
            WindowDetailDirector.Show()
            WindowDetailDirectorAddress.Hide()
            WindowDetailDirectorPhone.Hide()
            WindowDetailDirectorIdentification.Hide()

            FormPanelAddressDetail.Hide()
            FormPanelPhoneDetail.Hide()
            FP_Director.Show()


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()

            WindowDetailAddress.Hide()
            WindowDetailPhone.Hide()
            WindowDetailDirector.Show()
            WindowDetailDirectorAddress.Hide()
            WindowDetailDirectorPhone.Title = "Add Director Phone"
            WindowDetailDirectorPhone.Show()

            FormPanelAddressDetail.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelDirectorPhone.Show()


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorEmployerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()

            WindowDetailAddress.Hide()
            WindowDetailPhone.Hide()
            WindowDetailDirector.Show()
            WindowDetailDirectorAddress.Hide()
            WindowDetailDirectorPhone.Title = "Add Director Phone Employer"
            WindowDetailDirectorPhone.Show()

            FormPanelAddressDetail.Hide()
            FormPanelPhoneDetail.Hide()

            FormPanelEmpDirectorTaskDetail.Show()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()

            WindowDetailAddress.Hide()
            WindowDetailPhone.Hide()
            WindowDetailDirector.Show()
            WindowDetailDirectorAddress.Title = "Add Director Address"
            WindowDetailDirectorAddress.Show()
            WindowDetailDirectorPhone.Hide()

            FormPanelAddressDetail.Hide()
            FormPanelPhoneDetail.Hide()

            FP_Address_Director.Show()
            FP_Address_Emp_Director.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorEmployerAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()

            WindowDetailAddress.Hide()
            WindowDetailPhone.Hide()
            WindowDetailDirector.Show()
            WindowDetailDirectorAddress.Title = "Add Director Address Employer"
            WindowDetailDirectorAddress.Show()
            WindowDetailDirectorPhone.Hide()

            FormPanelAddressDetail.Hide()
            FormPanelPhoneDetail.Show()

            FP_Address_Emp_Director.Show()
            FP_Address_Director.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnAddNewAddressIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            FormPanelAddressDetail.Hidden = False
            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
            WindowDetailAddress.Title = "Add Address"
            WindowDetailAddress.Hidden = False
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewAddressEmployerIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            'Daniel 2020 Des 28
            If ListAddressEmployerDetail.Count < 1 Then
                'end 2020 Des 28
                ClearInput()
                FormPanelAddressDetailEmployer.Hidden = False
                FormPanelAddressDetail.Hidden = True
                PanelDetailAddressEmployer.Hidden = True
                PanelDetailAddress.Hidden = True
                WindowDetailAddress.Title = "Add Address Employer"
                WindowDetailAddress.Hidden = False
                'Daniel 2020 Des 28
            Else
                Throw New Exception("can only enter one Workplace Address Information")
            End If
            'end 2020 Des 28
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewPhoneCorporate_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            FormPanelAddressDetail.Hidden = True
            FormPanelPhoneDetail.Hidden = False
            WindowDetailPhone.Title = "Add Phone"
            WindowDetailPhone.Hidden = False
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewAddressCorporate_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            FormPanelAddressDetail.Hidden = False
            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
            WindowDetailAddress.Title = "Add Address"
            WindowDetailAddress.Hidden = False
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailIdentificationDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailIdentificationDirector Is Nothing Then
                SaveAddDetailIdentificationDirector()
            Else
                SaveEditDetailIdentificationDirector()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailIdentification Is Nothing Then
                SaveAddDetailIdentification()
            Else
                SaveEditDetailIdentification()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnSaveDetailPhone_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhone Is Nothing Then
                SaveAddDetailPhone()
            Else
                SaveEditDetailPhone()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailPhoneEmployer_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhoneEmployer Is Nothing Then
                SaveAddDetailPhoneEmployer()
            Else
                SaveEditDetailPhoneEmployer()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If objDirectorAddData Is Nothing Then
                SaveAddDetailDirector()
            Else
                SaveEditDetailDirector()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhoneDirector Is Nothing Then
                SaveAddDetailPhoneDirector()
            Else
                SaveEditDetailPhoneDirector()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorEmpPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhoneDirectorEmployer Is Nothing Then
                SaveAddDetailPhoneDirectorEmployer()
            Else
                SaveEditDetailPhoneDirectorEmployer()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailAddressDirector Is Nothing Then
                SaveAddDetailAddressDirector()
            Else
                SaveEditDetailAddressDirector()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnSaveDirectorEmpAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailAddressDirectorEmployer Is Nothing Then
                SaveAddDetailAddressDirectorEmployer()
            Else
                SaveEditDetailAddressDirectorEmployer()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub BtnSaveDetailAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If ObjDetailAddress Is Nothing Then
                SaveAddDetailAddress()
            Else
                SaveEditDetailAddress()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailAddressEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If ObjDetailAddressEmployer Is Nothing Then
                SaveAddDetailAddressEmployer()
            Else
                SaveEditDetailAddressEmployer()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelIdentification_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailIdentification.Hide()
            PanelDetailIdentification.Hide()
            FormPanelIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelIdentificationDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorIdentification.Hide()
            PanelDetailDirectorIdentification.Hide()
            FormPanelDirectorIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailIdentification_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailIdentification.Hide()
            PanelDetailDirectorIdentification.Hide()
            FormPanelDirectorIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailIdentificationDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorIdentification.Hide()
            PanelDetailIdentification.Hide()
            FormPanelIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()
            FormPanelDirectorPhone.Hide()
            FormPanelEmpDirectorTaskDetail.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorEmployerPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()
            FormPanelDirectorPhone.Hide()
            FormPanelEmpDirectorTaskDetail.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorDetailAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()
            FP_Address_Director.Hide()
            FP_Address_Emp_Director.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorDetailEmployerAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()
            FP_Address_Director.Hide()
            FP_Address_Emp_Director.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()

            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailPhoneEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()

            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()

            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelPhoneEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()

            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveCustomerDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirector.Hide()
            FP_Director.Hide()
            FormPanelDirectorDetail.Hide()
            ClearInput()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorPhones_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()
            FormPanelDirectorPhone.Hide()


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorEmpPhones_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()
            FormPanelEmpDirectorTaskDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            FP_Address_Director.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorEmpAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            FP_Address_Emp_Director.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailAddress.Hide()

            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()
            FormPanelAddressDetail.Hide()
            FormPanelAddressDetailEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddressEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailAddress.Hide()

            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()
            FormPanelAddressDetail.Hide()
            FormPanelAddressDetailEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailAddress.Hide()

            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()
            FormPanelAddressDetail.Hide()
            FormPanelAddressDetailEmployer.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelDetailAddressEmployer_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailAddress.Hide()

            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()
            FormPanelAddressDetail.Hide()
            FormPanelAddressDetailEmployer.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
#Region "Check Box"
    Protected Sub chkCorp_business_closed_Event(sender As Object, e As DirectEventArgs)
        If chkCorp_business_closed.Checked Then
            DateCorp_date_business_closed.Hidden = False
        Else
            DateCorp_date_business_closed.Hidden = True
        End If
    End Sub
    Protected Sub checkBoxcb_Director_Deceased_click(sender As Object, e As DirectEventArgs)
        If cb_Director_Deceased.Checked Then
            txt_Director_Deceased_Date.Hidden = False
        Else
            txt_Director_Deceased_Date.Hidden = True
        End If
    End Sub
    Protected Sub checkBoxcb_INDV_Deceased_click(sender As Object, e As DirectEventArgs)
        If CbINDV_Deceased.Checked Then
            DateINDV_Deceased_Date.Hidden = False
        Else
            DateINDV_Deceased_Date.Hidden = True
        End If
    End Sub
    Protected Sub cboFormWIC_ItemSelected(sender As Object, e As EventArgs)
        If cboFormWIC.Value = "Individu" Then
            WIC_Individu.Visible = True
            WIC_Corporate.Visible = False
            CleanFormCorporateWIC()
        Else
            WIC_Corporate.Visible = True
            WIC_Individu.Visible = False
            CleanFormIndividuWIC()
        End If
    End Sub
#End Region
#Region "Function"
    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailIdentification()
        Try
            Dim objNewDetailIdentification As New goAML_Person_Identification
            With objNewDetailIdentification
                .PK_Person_Identification_ID = ListIdentificationDetail.Select(Function(x) x.PK_Person_Identification_ID).DefaultIfEmpty(0).Max + 1
                .FK_Person_ID = If(objDirectorAddData Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, objDirectorAddData.NO_ID)
                .FK_Person_Type = 8
                .Type = CmbTypeIdentification.Text
                .Number = TxtNumber.Text
                .Issue_Date = If(TxtIssueDate.Text = "1/1/0001 12:00:00 AM", TxtIssueDate.RawValue, Convert.ToDateTime(TxtIssueDate.Text))
                .Expiry_Date = If(TxtExpiryDate.Text = "1/1/0001 12:00:00 AM", TxtExpiryDate.RawValue, Convert.ToDateTime(TxtExpiryDate.Text))
                .Issued_By = TxtIssuedBy.Text
                .Issued_Country = CmbCountryIdentification.TextValue
                .Identification_Comment = TxtCommentIdentification.Text
            End With

            ListIdentificationDetail.Add(objNewDetailIdentification)
            BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
            FormPanelIdentification.Hidden = True
            WindowDetailIdentification.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailIdentificationDirector()
        Try
            Dim objNewDetailIdentification As New goAML_Person_Identification
            With objNewDetailIdentification
                .PK_Person_Identification_ID = ListDirectorIdentificationDetail.Select(Function(x) x.PK_Person_Identification_ID).DefaultIfEmpty(0).Max + 1
                .FK_Person_ID = If(objDirectorAddData Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, objDirectorAddData.NO_ID)
                .FK_Person_Type = 7
                .Type = CmbTypeIdentificationDirector.Text
                .Number = TxtNumberDirector.Text
                .Issue_Date = If(TxtIssueDateDirector.Text = "1/1/0001 12:00:00 AM", TxtIssueDateDirector.RawValue, Convert.ToDateTime(TxtIssueDateDirector.Text))
                .Expiry_Date = If(TxtExpiryDateDirector.Text = "1/1/0001 12:00:00 AM", TxtExpiryDateDirector.RawValue, Convert.ToDateTime(TxtExpiryDateDirector.Text))
                .Issued_By = TxtIssuedByDirector.Text
                .Issued_Country = CmbCountryIdentificationDirector.TextValue
                .Identification_Comment = TxtCommentIdentificationDirector.Text
            End With

            ListDirectorIdentificationDetail.Add(objNewDetailIdentification)
            BindDetailIdentification(StoreIdentificationDirectorDetail, ListDirectorIdentificationDetail)
            FormPanelDirectorIdentification.Hidden = True
            WindowDetailDirectorIdentification.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhone()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                .PK_goAML_Ref_Phone = ListPhoneDetail.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 3
                .FK_for_Table_ID = objWICAddData.PK_Customer_ID
                .Tph_Contact_Type = Cmbtph_contact_type.Text
                .Tph_Communication_Type = Cmbtph_communication_type.Text
                .tph_country_prefix = Txttph_country_prefix.Text
                .tph_number = Txttph_number.Text
                .tph_extension = Txttph_extension.Text
                .comments = TxtcommentsPhone.Text
            End With

            ListPhoneDetail.Add(objNewDetailPhone)
            If cboFormWIC.SelectedItem.Value = "Individu" Then
                BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
            Else
                BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
            End If
            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhoneEmployer()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                .PK_goAML_Ref_Phone = ListPhoneDetail.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 10
                .FK_for_Table_ID = objWICAddData.PK_Customer_ID
                .Tph_Contact_Type = Cmbtph_contact_typeEmployer.Text
                .Tph_Communication_Type = Cmbtph_communication_typeEmployer.Text
                .tph_country_prefix = Txttph_country_prefixEmployer.Text
                .tph_number = Txttph_numberEmployer.Text
                .tph_extension = Txttph_extensionEmployer.Text
                .comments = TxtcommentsPhoneEmployer.Text
            End With

            ListPhoneEmployerDetail.Add(objNewDetailPhone)
            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)
            FormPanelPhoneEmployerDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailDirector()
        Try
            'If cmb_Director_Gender.Value = "" Then
            '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
            'ElseIf cmb_peran.Value = "" Then
            '    Throw New Exception("Role Tidak boleh kosong")
            'ElseIf txt_Director_Last_Name.Text = "" Then
            '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
            'ElseIf txt_Director_BirthDate.Text = "1/1/0001 12:00:00 AM" Or txt_Director_BirthDate.Value = DateTime.MinValue Then
            '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
            'ElseIf txt_Director_Birth_Place.Text = "" Then
            '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
            'ElseIf cmb_Director_Nationality1.TextValue = "" Then
            '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
            'ElseIf cmb_Director_Residence.TextValue = "" Then
            '    Throw New Exception("Negara Domisili Tidak boleh kosong")
            'ElseIf ListDirectorPhoneDetail.Count <= 0 Then
            '    Throw New Exception("Informasi Telepon Director Tidak boleh kosong")
            'ElseIf ListDirectorAddressDetail.Count <= 0 Then
            '    Throw New Exception("Informasi Alamat Director Tidak boleh kosong")
            'ElseIf txt_Director_Occupation.Text = "" Then
            '    Throw New Exception("Pekerjaan Tidak boleh kosong")
            'ElseIf txt_Director_Source_of_Wealth.Text = "" Then
            '    Throw New Exception("Sumber Dana Tidak boleh kosong")
            If cmb_peran.Value = "" Then
                Throw New Exception("Role Tidak boleh kosong")
            ElseIf txt_Director_Last_Name.Text = "" Then
                Throw New Exception("Nama Lengkap Tidak boleh kosong")
            Else
                If txt_Director_SSN.Text.Length <> 16 And txt_Director_SSN.Text.Length > 0 Then
                    Throw New Exception("NIK Must 16 Digit")
                Else
                    Dim DirectorClass As New WICDirectorDataBLL
                    Dim objNewDirector As New goAML_Ref_Walk_In_Customer_Director
                    With objNewDirector
                        .NO_ID = ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1
                        .Role = cmb_peran.Text
                        .Title = txt_Director_Title.Text
                        .Last_Name = txt_Director_Last_Name.Text
                        .Gender = cmb_Director_Gender.Text
                        'Daniel 18 jan 2021
                        If Not txt_Director_BirthDate.SelectedDate = DateTime.MinValue Then
                            .BirthDate = txt_Director_BirthDate.SelectedDate
                        End If
                        '.BirthDate = txt_Director_BirthDate.Text
                        'Daniel 18 jan 2021
                        .Birth_Place = txt_Director_Birth_Place.Text
                        .Mothers_Name = txt_Director_Mothers_Name.Text
                        .Alias = txt_Director_Alias.Text
                        .ID_Number = txt_Director_ID_Number.Text
                        .Passport_Number = txt_Director_Passport_Number.Text
                        .Passport_Country = txt_Director_Passport_Country.TextValue
                        .SSN = txt_Director_SSN.Text
                        .Nationality1 = cmb_Director_Nationality1.TextValue
                        .Nationality2 = cmb_Director_Nationality2.TextValue
                        .Nationality3 = cmb_Director_Nationality3.TextValue
                        .Residence = cmb_Director_Residence.TextValue
                        .Email = txt_Director_Email.Text
                        .Email2 = txt_Director_Email2.Text
                        .Email3 = txt_Director_Email3.Text
                        .Email4 = txt_Director_Email4.Text
                        .Email5 = txt_Director_Email5.Text
                        .Deceased = If(cb_Director_Deceased.Checked, True, False)
                        If cb_Director_Deceased.Checked Then
                            .Deceased_Date = txt_Director_Deceased_Date.Text
                        End If
                        .Tax_Reg_Number = If(cb_Director_Tax_Reg_Number.Checked, True, False)
                        .Tax_Number = txt_Director_Tax_Number.Text
                        .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
                        .Occupation = txt_Director_Occupation.Text
                        .Comments = txt_Director_Comments.Text
                        .Employer_Name = txt_Director_employer_name.Text
                    End With

                    With DirectorClass
                        .ObjDirector = objNewDirector
                        .ListDirectorAddress = ListDirectorAddressDetail
                        .ListDirectorAddressEmployer = ListDirectorEmployerAddressDetail
                        .ListDirectorPhone = ListDirectorPhoneDetail
                        .ListDirectorPhoneEmployer = ListDirectorEmployerPhoneDetail
                        .ListDirectorIdentification = ListDirectorIdentificationDetail
                    End With
                    ListDirectorDetail.Add(DirectorClass.ObjDirector)
                    ListDirectorDetailClass.Add(DirectorClass)
                    BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
                    WindowDetailDirector.Hidden = True
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhoneDirector()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                .PK_goAML_Ref_Phone = ListDirectorPhoneDetail.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 8
                .FK_for_Table_ID = If(objDirectorAddData Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, objDirectorAddData.NO_ID)
                .Tph_Contact_Type = Director_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_txt_phone_Country_prefix.Text
                .tph_number = Director_txt_phone_number.Text
                .tph_extension = Director_txt_phone_extension.Text
                .comments = Director_txt_phone_comments.Text
            End With

            ListDirectorPhoneDetail.Add(objNewDetailPhone)

            BindDetailPhone(Store_DirectorPhone, ListDirectorPhoneDetail)
            FormPanelDirectorPhone.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhoneDirectorEmployer()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                .PK_goAML_Ref_Phone = ListDirectorEmployerPhoneDetail.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 4
                .FK_for_Table_ID = If(objDirectorAddData Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, objDirectorAddData.NO_ID)
                .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text
                .tph_number = Director_Emp_txt_phone_number.Text
                .tph_extension = Director_Emp_txt_phone_extension.Text
                .comments = Director_Emp_txt_phone_comments.Text
            End With

            ListDirectorEmployerPhoneDetail.Add(objNewDetailPhone)
            BindDetailPhone(Store_Director_Employer_Phone, ListDirectorEmployerPhoneDetail)
            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveEditDetailIdentification()
        Try
            With ObjDetailIdentification
                .Type = CmbTypeIdentification.Text
                .Number = TxtNumber.Text
                .Issue_Date = TxtIssueDate.Text
                .Expiry_Date = TxtExpiryDate.Text
                .Issued_By = TxtIssuedBy.Text
                .Issued_Country = CmbCountryIdentification.TextValue
                .Identification_Comment = TxtCommentIdentification.Text
            End With

            ObjDetailIdentification = Nothing

            BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
            FormPanelIdentification.Hidden = True
            WindowDetailIdentification.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailIdentificationDirector()
        Try
            With ObjDetailIdentificationDirector
                .Type = CmbTypeIdentificationDirector.Text
                .Number = TxtNumberDirector.Text
                .Issue_Date = TxtIssueDateDirector.Text
                .Expiry_Date = TxtExpiryDateDirector.Text
                .Issued_By = TxtIssuedByDirector.Text
                .Issued_Country = CmbCountryIdentificationDirector.TextValue
                .Identification_Comment = TxtCommentIdentificationDirector.Text
            End With

            ObjDetailIdentificationDirector = Nothing

            BindDetailIdentification(StoreIdentificationDirectorDetail, ListDirectorIdentificationDetail)
            FormPanelDirectorIdentification.Hidden = True
            WindowDetailDirectorIdentification.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhone()
        Try
            With ObjDetailPhone
                .Tph_Contact_Type = Cmbtph_contact_type.Text
                .Tph_Communication_Type = Cmbtph_communication_type.Text
                .tph_country_prefix = Txttph_country_prefix.Text
                .tph_number = Txttph_number.Text
                .tph_extension = Txttph_extension.Text
                .comments = TxtcommentsPhone.Text
            End With

            ObjDetailPhone = Nothing
            If cboFormWIC.SelectedItem.Value = "Individu" Then
                BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
            Else
                BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
            End If

            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhoneEmployer()
        Try
            With ObjDetailPhoneEmployer
                .Tph_Contact_Type = Cmbtph_contact_typeEmployer.Text
                .Tph_Communication_Type = Cmbtph_communication_typeEmployer.Text
                .tph_country_prefix = Txttph_country_prefixEmployer.Text
                .tph_number = Txttph_numberEmployer.Text
                .tph_extension = Txttph_extensionEmployer.Text
                .comments = TxtcommentsPhoneEmployer.Text
            End With

            ObjDetailPhoneEmployer = Nothing
            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)

            FormPanelPhoneEmployerDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailDirector()
        Try
            'If cmb_Director_Gender.Value = "" Then
            '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
            'ElseIf cmb_peran.Value = "" Then
            '    Throw New Exception("Role Tidak boleh kosong")
            'ElseIf txt_Director_Last_Name.Text = "" Then
            '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
            'ElseIf txt_Director_BirthDate.Text = "1/1/0001 12:00:00 AM" Or txt_Director_BirthDate.Value = DateTime.MinValue Then
            '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
            'ElseIf txt_Director_Birth_Place.Text = "" Then
            '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
            'ElseIf cmb_Director_Nationality1.TextValue = "" Then
            '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
            'ElseIf cmb_Director_Residence.TextValue = "" Then
            '    Throw New Exception("Negara Domisili Tidak boleh kosong")
            'ElseIf txt_Director_Occupation.Text = "" Then
            '    Throw New Exception("Pekerjaan Tidak boleh kosong")
            'ElseIf txt_Director_Source_of_Wealth.Text = "" Then
            '    Throw New Exception("Sumber Dana Tidak boleh kosong")
            'ElseIf txt_Director_SSN.Text.Length <> 16 And txt_Director_SSN.Text <> "" Then
            '    Throw New Exception("NIK Must 16 Digit")
            'End If
            Dim DirectorClass As New WICDirectorDataBLL
            With objDirectorAddData
                .Role = cmb_peran.Text
                .Title = txt_Director_Title.Text
                .Last_Name = txt_Director_Last_Name.Text
                .Gender = cmb_Director_Gender.Text
                'Daniel 18 jan 2021
                If Not txt_Director_BirthDate.SelectedDate = DateTime.MinValue Then
                    .BirthDate = txt_Director_BirthDate.SelectedDate
                End If
                '.BirthDate = txt_Director_BirthDate.Text
                'Daniel 18 jan 2021
                .Birth_Place = txt_Director_Birth_Place.Text
                .Mothers_Name = txt_Director_Mothers_Name.Text
                .Alias = txt_Director_Alias.Text
                .ID_Number = txt_Director_ID_Number.Text
                .Passport_Number = txt_Director_Passport_Number.Text
                .Passport_Country = txt_Director_Passport_Country.TextValue
                .SSN = txt_Director_SSN.Text
                .Nationality1 = cmb_Director_Nationality1.TextValue
                .Nationality2 = cmb_Director_Nationality2.TextValue
                .Nationality3 = cmb_Director_Nationality3.TextValue
                .Residence = cmb_Director_Residence.TextValue
                .Email = txt_Director_Email.Text
                .Email2 = txt_Director_Email2.Text
                .Email3 = txt_Director_Email3.Text
                .Email4 = txt_Director_Email4.Text
                .Email5 = txt_Director_Email5.Text
                .Deceased = If(cb_Director_Deceased.Checked, True, False)
                If cb_Director_Deceased.Checked Then
                    .Deceased_Date = txt_Director_Deceased_Date.Text
                End If
                .Tax_Reg_Number = If(cb_Director_Tax_Reg_Number.Checked, True, False)
                .Tax_Number = txt_Director_Tax_Number.Text
                .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
                .Occupation = txt_Director_Occupation.Text
                .Comments = txt_Director_Comments.Text
                .Employer_Name = txt_Director_employer_name.Text
            End With
            With DirectorClass
                .ObjDirector = objDirectorAddData
                .ListDirectorPhone = ListDirectorPhoneDetail
                .ListDirectorPhoneEmployer = ListDirectorEmployerPhoneDetail
                .ListDirectorAddress = ListDirectorAddressDetail
                .ListDirectorAddressEmployer = ListDirectorEmployerAddressDetail
                .ListDirectorIdentification = ListDirectorIdentificationDetail
            End With
            ListDirectorDetail.Remove(ListDirectorDetail.Where(Function(x) x.NO_ID = DirectorClass.ObjDirector.NO_ID).FirstOrDefault)
            ListDirectorDetailClass.Remove(ListDirectorDetailClass.Where(Function(x) x.ObjDirector.NO_ID = DirectorClass.ObjDirector.NO_ID).FirstOrDefault)
            ListDirectorDetail.Add(DirectorClass.ObjDirector)
            ListDirectorDetailClass.Add(DirectorClass)

            objDirectorAddData = Nothing

            BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
            WindowDetailDirector.Hidden = True
            FP_Director.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhoneDirector()
        Try
            With ObjDetailPhoneDirector
                .Tph_Contact_Type = Director_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_txt_phone_Country_prefix.Text
                .tph_number = Director_txt_phone_number.Text
                .tph_extension = Director_txt_phone_extension.Text
                .comments = Director_txt_phone_comments.Text
            End With

            ObjDetailPhoneDirector = Nothing
            BindDetailPhone(Store_DirectorPhone, ListDirectorPhoneDetail)

            FormPanelDirectorPhone.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhoneDirectorEmployer()
        Try
            With ObjDetailPhoneDirectorEmployer
                .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text
                .tph_number = Director_Emp_txt_phone_number.Text
                .tph_extension = Director_Emp_txt_phone_extension.Text
                .comments = Director_Emp_txt_phone_comments.Text
            End With

            ObjDetailPhoneDirectorEmployer = Nothing
            BindDetailPhone(Store_Director_Employer_Phone, ListDirectorEmployerPhoneDetail)

            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveAddDetailAddress()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                .PK_Customer_Address_ID = ListAddressDetail.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 3
                .FK_To_Table_ID = objWICAddData.PK_Customer_ID
                .Address_Type = CmbAddress_type.Text
                .Address = TxtAddress.Text
                .Town = TxtTown.Text
                .City = TxtCity.Text
                .Zip = TxtZip.Text
                .Country_Code = Cmbcountry_code.TextValue
                .State = TxtState.Text
                .Comments = TxtcommentsAddress.Text
            End With

            ListAddressDetail.Add(objNewDetailAddress)
            If cboFormWIC.SelectedItem.Value = "Individu" Then
                BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
            Else
                BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
            End If

            FormPanelAddressDetail.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailAddressEmployer()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                .PK_Customer_Address_ID = ListAddressEmployerDetail.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 10
                .FK_To_Table_ID = objWICAddData.PK_Customer_ID
                .Address_Type = CmbAddress_typeEmployer.Text
                .Address = TxtAddressEmployer.Text
                .Town = TxtTownEmployer.Text
                .City = TxtCityEmployer.Text
                .Zip = TxtZipEmployer.Text
                .Country_Code = Cmbcountry_codeEmployer.TextValue
                .State = TxtStateEmployer.Text
                .Comments = TxtcommentsAddressEmployer.Text
            End With

            ListAddressEmployerDetail.Add(objNewDetailAddress)
            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)

            FormPanelAddressDetailEmployer.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailAddressDirector()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                .PK_Customer_Address_ID = ListDirectorAddressDetail.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 8
                .FK_To_Table_ID = If(objDirectorAddData Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, objDirectorAddData.NO_ID)
                .Address_Type = Director_cmb_kategoriaddress.Text
                .Address = Director_Address.Text
                .Town = Director_Town.Text
                .City = Director_City.Text
                .Zip = Director_Zip.Text
                .Country_Code = Director_cmb_kodenegara.TextValue
                .State = Director_State.Text
                .Comments = Director_Comment_Address.Text
            End With

            ListDirectorAddressDetail.Add(objNewDetailAddress)
            BindDetailAddress(Store_DirectorAddress, ListDirectorAddressDetail)

            FP_Address_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailAddressDirectorEmployer()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                .PK_Customer_Address_ID = ListDirectorEmployerAddressDetail.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 4
                .FK_To_Table_ID = If(objDirectorAddData Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, objDirectorAddData.NO_ID)
                .Address_Type = Director_cmb_emp_kategoriaddress.Text
                .Address = Director_emp_Address.Text
                .Town = Director_emp_Town.Text
                .City = Director_emp_City.Text
                .Zip = Director_emp_Zip.Text
                .Country_Code = Director_cmb_emp_kodenegara.TextValue
                .State = Director_emp_State.Text
                .Comments = Director_emp_Comment_Address.Text
            End With

            ListDirectorEmployerAddressDetail.Add(objNewDetailAddress)
            BindDetailAddress(Store_Director_Employer_Address, ListDirectorEmployerAddressDetail)

            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveEditDetailAddress()
        Try
            With ObjDetailAddress
                .Address_Type = CmbAddress_type.Text
                .Address = TxtAddress.Text
                .Town = TxtTown.Text
                .City = TxtCity.Text
                .Zip = TxtZip.Text
                .Country_Code = Cmbcountry_code.TextValue
                .State = TxtState.Text
                .Comments = TxtcommentsAddress.Text
            End With

            ObjDetailAddress = Nothing
            If cboFormWIC.SelectedItem.Value = "Individu" Then
                BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
            Else
                BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
            End If

            FormPanelAddressDetail.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailAddressEmployer()
        Try
            With ObjDetailAddressEmployer
                .Address_Type = CmbAddress_typeEmployer.Text
                .Address = TxtAddressEmployer.Text
                .Town = TxtTownEmployer.Text
                .City = TxtCityEmployer.Text
                .Zip = TxtZipEmployer.Text
                .Country_Code = Cmbcountry_codeEmployer.TextValue
                .State = TxtStateEmployer.Text
                .Comments = TxtcommentsAddressEmployer.Text
            End With

            ObjDetailAddressEmployer = Nothing
            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)

            FormPanelAddressDetailEmployer.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailAddressDirector()
        Try
            With ObjDetailAddressDirector
                .Address_Type = Director_cmb_kategoriaddress.Text
                .Address = Director_Address.Text
                .Town = Director_Town.Text
                .City = Director_City.Text
                .Zip = Director_Zip.Text
                .Country_Code = Director_cmb_kodenegara.TextValue
                .State = Director_State.Text
                .Comments = Director_Comment_Address.Text
            End With

            ObjDetailAddressDirector = Nothing
            BindDetailAddress(Store_DirectorAddress, ListDirectorAddressDetail)

            FP_Address_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailAddressDirectorEmployer()
        Try
            With ObjDetailAddressDirectorEmployer
                .Address_Type = Director_cmb_emp_kategoriaddress.Text
                .Address = Director_emp_Address.Text
                .Town = Director_emp_Town.Text
                .City = Director_emp_City.Text
                .Zip = Director_emp_Zip.Text
                .Country_Code = Director_cmb_emp_kodenegara.TextValue
                .State = Director_emp_State.Text
                .Comments = Director_emp_Comment_Address.Text
            End With

            ObjDetailAddressDirectorEmployer = Nothing
            BindDetailAddress(Store_Director_Employer_Address, ListDirectorEmployerAddressDetail)

            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailPhoneIndividu(id As String)
        Try
            BtnAddNewPhoneIndividu_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Phone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not objDelete Is Nothing Then
                ListPhoneDetail.Remove(objDelete)
                BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
                WindowDetailPhone.Hide()
                FormPanelPhoneDetail.Hide()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailPhoneIndividuEmployer(id As String)
        Try
            BtnAddNewPhoneEmployerIndividu_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Phone = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not objDelete Is Nothing Then
                ListPhoneEmployerDetail.Remove(objDelete)
                BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)
                WindowDetailPhone.Hide()
                FormPanelPhoneEmployerDetail.Hide()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailDirector(id As String)
        Try
            btnAddDirector_DirectClick(Nothing, Nothing)
            'daniel 21 jan 2021
            Dim objDirector As WICDirectorDataBLL = ListDirectorDetailClass.Find(Function(x) x.ObjDirector.NO_ID = id)
            Dim objDelete As goAML_Ref_Walk_In_Customer_Director = ListDirectorDetail.Find(Function(x) x.NO_ID = id)
            If Not objDelete Is Nothing Then
                ListDirectorDetail.Remove(objDelete)
            End If
            If objDirector IsNot Nothing Then
                ListDirectorDetailClass.Remove(objDirector)
            End If
            BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
            WindowDetailDirector.Hide()
            FP_Director.Hide()
            'end 21 jan 2021
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailIdentificationDirector(id As String)
        Try
            btnAddDirectorIdentification_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Person_Identification = ListDirectorIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
            If Not objDelete Is Nothing Then
                ListDirectorIdentificationDetail.Remove(objDelete)
                BindDetailIdentification(StoreIdentificationDirectorDetail, ListDirectorIdentificationDetail)
                WindowDetailDirectorIdentification.Hide()
                FormPanelDirectorIdentification.Hide()
                PanelDetailDirectorIdentification.Hide()
                FP_Director.Show()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailIdentification(id As String)
        Try
            btnAddIdentification_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Person_Identification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
            If Not objDelete Is Nothing Then
                ListIdentificationDetail.Remove(objDelete)
                BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
                WindowDetailIdentification.Hide()
                FormPanelIdentification.Hide()
                PanelDetailIdentification.Hide()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailPhoneDirector(id As String)
        Try
            btnAddDirectorPhones_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Phone = ListDirectorPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not objDelete Is Nothing Then
                ListDirectorPhoneDetail.Remove(objDelete)
                BindDetailPhone(Store_DirectorPhone, ListDirectorPhoneDetail)
                WindowDetailDirectorPhone.Hide()
                FormPanelDirectorPhone.Hide()
                WindowDetailPhone.Hide()
                WindowDetailDirector.Show()
                FP_Director.Show()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailPhoneDirectorEmployer(id As String)
        Try
            btnAddDirectorEmployerPhones_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Phone = ListDirectorEmployerPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not objDelete Is Nothing Then
                ListDirectorEmployerPhoneDetail.Remove(objDelete)
                BindDetailPhone(Store_Director_Employer_Phone, ListDirectorEmployerPhoneDetail)
                WindowDetailDirectorPhone.Hide()
                FormPanelEmpDirectorTaskDetail.Hide()
                WindowDetailDirector.Show()
                WindowDetailPhone.Hide()
                FP_Director.Show()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataEditPhoneIndividu(id As String)
        Try
            ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhone Is Nothing Then
                FormPanelPhoneDetail.Show()
                WindowDetailPhone.Title = "Edit Phone"
                WindowDetailPhone.Show()

                With ObjDetailPhone
                    Cmbtph_contact_type.SetValueAndFireSelect(.Tph_Contact_Type)
                    Cmbtph_communication_type.SetValueAndFireSelect(.Tph_Communication_Type)
                    Txttph_country_prefix.Text = .tph_country_prefix
                    Txttph_number.Text = .tph_number
                    Txttph_extension.Text = .tph_extension
                    TxtcommentsPhone.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditPhoneIndividuEmployer(id As String)
        Try
            ObjDetailPhoneEmployer = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhoneEmployer Is Nothing Then
                FormPanelPhoneEmployerDetail.Show()
                WindowDetailPhone.Title = "Edit Phone"
                WindowDetailPhone.Show()

                With ObjDetailPhoneEmployer
                    Cmbtph_contact_typeEmployer.SetValueAndFireSelect(.Tph_Contact_Type)
                    Cmbtph_communication_typeEmployer.SetValueAndFireSelect(.Tph_Communication_Type)
                    Txttph_country_prefixEmployer.Text = .tph_country_prefix
                    Txttph_numberEmployer.Text = .tph_number
                    Txttph_extensionEmployer.Text = .tph_extension
                    TxtcommentsPhoneEmployer.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditDirector(id As String)
        Try
            Dim listDirectorDetails As WICDirectorDataBLL = ListDirectorDetailClass.Where(Function(x) x.ObjDirector.NO_ID = id).FirstOrDefault()
            'objDirectorAddData = listDirectorDetails.ObjDirector
            ListDirectorAddressDetail = listDirectorDetails.ListDirectorAddress.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList()
            ListDirectorEmployerAddressDetail = listDirectorDetails.ListDirectorAddressEmployer.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList()
            ListDirectorPhoneDetail = listDirectorDetails.ListDirectorPhone.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList()
            ListDirectorEmployerPhoneDetail = listDirectorDetails.ListDirectorPhoneEmployer.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList()
            ListDirectorIdentificationDetail = listDirectorDetails.ListDirectorIdentification.Where(Function(x) x.FK_Person_ID = id And x.FK_Person_Type = 7).ToList()

            'daniel 21 jan 2021
            ClearInput()
            objDirectorAddData = listDirectorDetails.ObjDirector
            'end 21 jan 2021
            If Not objDirectorAddData Is Nothing Then
                FP_Director.Show()
                FormPanelDirectorDetail.Hide()
                WindowDetailDirector.Show()
                'daniel 18 jan 2021
                'ClearInput()
                'clearinputDirector()
                'end 18 jan 2021
                With objDirectorAddData
                    cmb_peran.Text = .Role
                    txt_Director_Title.Text = .Title
                    txt_Director_Last_Name.Text = .Last_Name
                    cmb_Director_Gender.Text = .Gender
                    'daniel 18 jan 2021
                    If .BirthDate.HasValue Then
                        txt_Director_BirthDate.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                    End If
                    'end 18 jan 2021
                    txt_Director_Birth_Place.Text = .Birth_Place
                    txt_Director_Mothers_Name.Text = .Mothers_Name
                    txt_Director_Alias.Text = .Alias
                    txt_Director_ID_Number.Text = .ID_Number
                    txt_Director_Passport_Number.Text = .Passport_Number
                    'txt_Director_Passport_Country.Text = .Passport_Country
                    If Not IsNothing(.Passport_Country) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Passport_Country & "'")
                        If DataRowTemp IsNot Nothing Then
                            txt_Director_Passport_Country.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    txt_Director_SSN.Text = .SSN
                    If Not IsNothing(.Nationality1) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Nationality1 & "'")
                        If DataRowTemp IsNot Nothing Then
                            cmb_Director_Nationality1.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    If Not IsNothing(.Nationality2) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Nationality2 & "'")
                        If DataRowTemp IsNot Nothing Then
                            cmb_Director_Nationality2.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    If Not IsNothing(.Nationality3) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Nationality3 & "'")
                        If DataRowTemp IsNot Nothing Then
                            cmb_Director_Nationality3.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    If Not IsNothing(.Residence) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Residence & "'")
                        If DataRowTemp IsNot Nothing Then
                            cmb_Director_Residence.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    'cmb_Director_Nationality1.SetValueAndFireSelect(.Nationality1)
                    'cmb_Director_Nationality2.SetValueAndFireSelect(.Nationality2)
                    'cmb_Director_Nationality3.SetValueAndFireSelect(.Nationality3)
                    'cmb_Director_Residence.SetValueAndFireSelect(.Residence)
                    txt_Director_Email.Text = .Email
                    txt_Director_Email2.Text = .Email2
                    txt_Director_Email3.Text = .Email3
                    txt_Director_Email4.Text = .Email4
                    txt_Director_Email5.Text = .Email5
                    cb_Director_Deceased.Checked = .Deceased
                    If cb_Director_Deceased.Checked Then
                        txt_Director_Deceased_Date.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                    End If
                    cb_Director_Tax_Reg_Number.Checked = .Tax_Reg_Number
                    txt_Director_Tax_Number.Text = .Tax_Number
                    txt_Director_Source_of_Wealth.Text = .Source_of_Wealth
                    txt_Director_Occupation.Text = .Occupation
                    txt_Director_Comments.Text = .Comments
                    txt_Director_employer_name.Text = .Employer_Name
                End With

                BindDetailPhone(Store_DirectorPhone, ListDirectorPhoneDetail)
                BindDetailPhone(Store_Director_Employer_Phone, ListDirectorEmployerPhoneDetail)
                BindDetailAddress(Store_DirectorAddress, ListDirectorAddressDetail)
                BindDetailAddress(Store_Director_Employer_Address, ListDirectorEmployerAddressDetail)
                BindDetailIdentification(StoreIdentificationDirectorDetail, ListDirectorIdentificationDetail)

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataEditIdentificationDirector(id As String)
        Try
            ObjDetailIdentificationDirector = ListDirectorIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
            If Not ObjDetailIdentificationDirector Is Nothing Then
                FormPanelDirectorIdentification.Show()
                PanelDetailDirectorIdentification.Hide()
                WindowDetailDirectorIdentification.Title = "Edit Identitas"
                WindowDetailDirectorIdentification.Show()

                With ObjDetailIdentificationDirector
                    CmbTypeIdentificationDirector.SetValue(.Type)
                    TxtNumberDirector.Text = .Number
                    TxtIssueDateDirector.Text = If(.Issue_Date Is Nothing, "", .Issue_Date.Value.ToString("dd-MMM-yy"))
                    TxtExpiryDateDirector.Text = If(.Expiry_Date Is Nothing, "", .Expiry_Date.Value.ToString("dd-MMM-yy"))
                    TxtIssuedByDirector.Text = .Issued_By
                    If Not IsNothing(.Issued_Country) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Issued_Country & "'")
                        If DataRowTemp IsNot Nothing Then
                            CmbCountryIdentificationDirector.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    'CmbCountryIdentificationDirector.SetValue(.Issued_Country)
                    TxtCommentIdentificationDirector.Text = .Identification_Comment
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditIdentification(id As String)
        Try
            ObjDetailIdentification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
            If Not ObjDetailIdentification Is Nothing Then
                FormPanelIdentification.Show()
                PanelDetailIdentification.Hide()
                WindowDetailIdentification.Show()

                With ObjDetailIdentification
                    CmbTypeIdentification.SetValue(.Type)
                    TxtNumber.Text = .Number
                    TxtIssueDate.Text = If(.Issue_Date Is Nothing, "", .Issue_Date.Value.ToString("dd-MMM-yy"))
                    TxtExpiryDate.Text = If(.Expiry_Date Is Nothing, "", .Expiry_Date.Value.ToString("dd-MMM-yy"))
                    TxtIssuedBy.Text = .Issued_By
                    ' CmbCountryIdentification.SetValue(.Issued_Country)
                    If Not IsNothing(.Issued_Country) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Issued_Country & "'")
                        If DataRowTemp IsNot Nothing Then
                            CmbCountryIdentification.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    TxtCommentIdentification.Text = .Identification_Comment
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataEditPhoneDirector(id As String)
        Try
            ObjDetailPhoneDirector = ListDirectorPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhoneDirector Is Nothing Then
                FormPanelDirectorPhone.Show()
                PanelPhoneDirector.Hide()
                PanelPhoneDirectorEmployer.Hide()
                FormPanelEmpDirectorTaskDetail.Hide()
                WindowDetailDirectorPhone.Title = "Edit Director Phone"
                WindowDetailDirectorPhone.Show()

                With ObjDetailPhoneDirector
                    Director_cb_phone_Contact_Type.SetValueAndFireSelect(.Tph_Contact_Type)
                    Director_cb_phone_Communication_Type.SetValueAndFireSelect(.Tph_Communication_Type)
                    Director_txt_phone_Country_prefix.Text = .tph_country_prefix
                    Director_txt_phone_number.Text = .tph_number
                    Director_txt_phone_extension.Text = .tph_extension
                    Director_txt_phone_comments.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditPhoneDirectorEmployer(id As String)
        Try
            ObjDetailPhoneDirectorEmployer = ListDirectorEmployerPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhoneDirectorEmployer Is Nothing Then
                FormPanelEmpDirectorTaskDetail.Show()
                FormPanelDirectorPhone.Hide()
                PanelPhoneDirector.Hide()
                PanelPhoneDirectorEmployer.Hide()
                WindowDetailDirectorPhone.Title = "Edit Director Phone Employer"
                WindowDetailDirectorPhone.Show()

                With ObjDetailPhoneDirectorEmployer
                    Director_Emp_cb_phone_Contact_Type.SetValueAndFireSelect(.Tph_Contact_Type)
                    Director_Emp_cb_phone_Communication_Type.SetValueAndFireSelect(.Tph_Communication_Type)
                    Director_Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                    Director_Emp_txt_phone_number.Text = .tph_number
                    Director_Emp_txt_phone_extension.Text = .tph_extension
                    Director_Emp_txt_phone_comments.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailAddressIndividu(id As String)
        Try
            BtnAddNewAddressIndividu_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Address = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not objDelete Is Nothing Then
                ListAddressDetail.Remove(objDelete)
                FormPanelAddressDetail.Hide()
                WindowDetailAddress.Hide()
                BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailAddressEmployerIndividu(id As String)
        Try
            BtnAddNewAddressEmployerIndividu_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Address = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not objDelete Is Nothing Then
                ListAddressEmployerDetail.Remove(objDelete)
                PanelDetailAddress.Hide()
                PanelDetailAddressEmployer.Hide()
                FormPanelAddressDetail.Hide()
                FormPanelAddressDetailEmployer.Hide()
                WindowDetailAddress.Hide()
                BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailAddressDirector(id As String)
        Try
            btnAddDirectorAddresses_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Address = ListDirectorAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not objDelete Is Nothing Then
                ListDirectorAddressDetail.Remove(objDelete)
                FP_Address_Director.Hide()
                WindowDetailDirectorAddress.Hide()
                BindDetailAddress(Store_DirectorAddress, ListDirectorAddressDetail)
                WindowDetailDirector.Show()
                WindowDetailAddress.Hide()
                FP_Director.Show()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailAddressDirectorEmployer(id As String)
        Try
            btnAddDirectorEmployerAddresses_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Address = ListDirectorEmployerAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not objDelete Is Nothing Then
                ListDirectorEmployerAddressDetail.Remove(objDelete)
                FP_Address_Director.Hide()
                WindowDetailDirectorAddress.Hide()
                BindDetailAddress(Store_Director_Employer_Address, ListDirectorEmployerAddressDetail)
                WindowDetailDirector.Show()
                FP_Director.Show()
                WindowDetailAddress.Hide()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditAddressIndividu(id As String)
        Try
            ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 3)
            If Not ObjDetailAddress Is Nothing Then
                FormPanelAddressDetail.Show()
                FormPanelAddressDetailEmployer.Hide()
                PanelDetailAddress.Hide()
                PanelDetailAddressEmployer.Hide()
                WindowDetailAddress.Title = "Edit Address"
                WindowDetailAddress.Show()

                With ObjDetailAddress
                    CmbAddress_type.SetValueAndFireSelect(.Address_Type)
                    TxtAddress.Text = .Address
                    TxtTown.Text = .Town
                    TxtCity.Text = .City
                    TxtZip.Text = .Zip
                    If Not IsNothing(.Country_Code) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                        If DataRowTemp IsNot Nothing Then
                            Cmbcountry_code.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    'Cmbcountry_code.SetValueAndFireSelect(.Country_Code)
                    TxtState.Text = .State
                    TxtcommentsAddress.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditAddressEmployerIndividu(id As String)
        Try
            ObjDetailAddressEmployer = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 10)
            If Not ObjDetailAddressEmployer Is Nothing Then
                FormPanelAddressDetailEmployer.Show()
                WindowDetailAddress.Title = "Edit Address Employer"
                WindowDetailAddress.Show()

                With ObjDetailAddressEmployer
                    CmbAddress_typeEmployer.SetValueAndFireSelect(.Address_Type)
                    TxtAddressEmployer.Text = .Address
                    TxtTownEmployer.Text = .Town
                    TxtCityEmployer.Text = .City
                    TxtZipEmployer.Text = .Zip
                    If Not IsNothing(.Country_Code) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                        If DataRowTemp IsNot Nothing Then
                            Cmbcountry_codeEmployer.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    '  Cmbcountry_codeEmployer.SetValueAndFireSelect(.Country_Code)
                    TxtStateEmployer.Text = .State
                    TxtcommentsAddressEmployer.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditAddressDirector(id As String)
        Try
            ObjDetailAddressDirector = ListDirectorAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not ObjDetailAddressDirector Is Nothing Then
                FP_Address_Director.Show()
                FP_Address_Emp_Director.Hide()
                PanelAddressDirector.Hide()
                PanelAddressDirectorEmployer.Hide()
                WindowDetailDirectorAddress.Title = "Edit Director Address"
                WindowDetailDirectorAddress.Show()

                With ObjDetailAddressDirector
                    Director_cmb_kategoriaddress.SetValueAndFireSelect(.Address_Type)
                    Director_Address.Text = .Address
                    Director_Town.Text = .Town
                    Director_City.Text = .City
                    Director_Zip.Text = .Zip
                    If Not IsNothing(.Country_Code) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                        If DataRowTemp IsNot Nothing Then
                            'Daniel 2021 Jan 5
                            'cmb_Director_Nationality1.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            Director_cmb_kodenegara.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            'end 2021 Jan 5
                        End If
                    End If
                    'Director_cmb_kodenegara.SetValueAndFireSelect(.Country_Code)
                    Director_State.Text = .State
                    Director_Comment_Address.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditAddressDirectorEmployer(id As String)
        Try
            ObjDetailAddressDirectorEmployer = ListDirectorEmployerAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not ObjDetailAddressDirectorEmployer Is Nothing Then
                FP_Address_Emp_Director.Show()
                FP_Address_Director.Hide()
                PanelAddressDirector.Hide()
                PanelAddressDirectorEmployer.Hide()
                WindowDetailDirectorAddress.Title = "Edit Director Address Employer"
                WindowDetailDirectorAddress.Show()

                With ObjDetailAddressDirectorEmployer
                    Director_cmb_emp_kategoriaddress.SetValueAndFireSelect(.Address_Type)
                    Director_emp_Address.Text = .Address
                    Director_emp_Town.Text = .Town
                    Director_emp_City.Text = .City
                    Director_emp_Zip.Text = .Zip
                    ' Director_cmb_emp_kodenegara.SetValueAndFireSelect(.Country_Code)
                    If Not IsNothing(.Country_Code) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                        If DataRowTemp IsNot Nothing Then
                            Director_cmb_emp_kodenegara.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    Director_emp_State.Text = .State
                    Director_emp_Comment_Address.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailPhoneCorporate(id As String)
        Try
            BtnAddNewPhoneCorporate_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Phone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not objDelete Is Nothing Then
                ListPhoneDetail.Remove(objDelete)
                WindowDetailPhone.Hide()
                FormPanelPhoneDetail.Hide()
                BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataEditPhoneCorporate(id As String)
        Try
            ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhone Is Nothing Then
                FormPanelPhoneDetail.Show()
                PanelDetailPhone.Hide()
                WindowDetailPhone.Title = "Edit Phone"
                WindowDetailPhone.Show()
                ClearInput()
                With ObjDetailPhone
                    Cmbtph_contact_type.SetValueAndFireSelect(.Tph_Contact_Type)
                    Cmbtph_communication_type.SetValueAndFireSelect(.Tph_Communication_Type)
                    Txttph_country_prefix.Text = .tph_country_prefix
                    Txttph_number.Text = .tph_number
                    Txttph_extension.Text = .tph_extension
                    TxtcommentsPhone.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailAddressCorporate(id As String)
        Try
            BtnAddNewAddressCorporate_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Address = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not objDelete Is Nothing Then
                ListAddressDetail.Remove(objDelete)
                WindowDetailAddress.Hide()
                FormPanelAddressDetail.Hide()
                BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataEditAddressCorporate(id As String)
        Try
            ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not ObjDetailAddress Is Nothing Then
                FormPanelAddressDetail.Show()
                WindowDetailAddress.Title = "Edit Address"
                WindowDetailAddress.Show()
                PanelDetailAddress.Hide()
                ClearInput()
                With ObjDetailAddress
                    CmbAddress_type.SetValueAndFireSelect(.Address_Type)
                    TxtAddress.Text = .Address
                    TxtTown.Text = .Town
                    TxtCity.Text = .City
                    TxtZip.Text = .Zip
                    If Not IsNothing(.Country_Code) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                        If DataRowTemp IsNot Nothing Then
                            Cmbcountry_code.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    ' Cmbcountry_code.SetValueAndFireSelect(.Country_Code)
                    TxtState.Text = .State
                    TxtcommentsAddress.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
#Region "Grid"
    Protected Sub GrdCmdCustDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditIdentificationDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailIdentificationDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordIdentificationDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordIdentification(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhoneDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhoneDirectorEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhone(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneEmployerIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneIndividuEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneIndividuEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhoneEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordPhone(id As String)
        Try
            ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhone Is Nothing Then

                PanelDetailPhone.Show()
                PanelDetailPhoneEmployer.Hide()
                FormPanelPhoneDetail.Hide()
                FormPanelPhoneEmployerDetail.Hide()
                WindowDetailPhone.Title = "Detail Phone"
                WindowDetailPhone.Show()
                ClearInput()
                With ObjDetailPhone
                    Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                    Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                    Dsptph_country_prefix.Text = .tph_country_prefix
                    Dsptph_number.Text = .tph_number
                    Dsptph_extension.Text = .tph_extension
                    DspcommentsPhone.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordPhoneEmployer(id As String)
        Try
            ObjDetailPhoneEmployer = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhoneEmployer Is Nothing Then

                PanelDetailPhone.Hide()
                PanelDetailPhoneEmployer.Show()
                FormPanelPhoneDetail.Hide()
                FormPanelPhoneEmployerDetail.Hide()
                WindowDetailPhone.Title = "Detail Phone"
                WindowDetailPhone.Show()
                ClearInput()
                With ObjDetailPhoneEmployer
                    Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                    Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                    Dsptph_country_prefixEmployer.Text = .tph_country_prefix
                    Dsptph_numberEmployer.Text = .tph_number
                    Dsptph_extensionEmployer.Text = .tph_extension
                    DspcommentsPhoneEmployer.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordDirector(id As String)
        Try
            Dim DirectorClass As WICDirectorDataBLL = ListDirectorDetailClass.Where(Function(x) x.ObjDirector.NO_ID = id).FirstOrDefault
            objDirectorAddData = DirectorClass.ObjDirector
            ListDirectorAddressDetail = DirectorClass.ListDirectorAddress.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList
            ListDirectorEmployerAddressDetail = DirectorClass.ListDirectorAddressEmployer.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList
            ListDirectorPhoneDetail = DirectorClass.ListDirectorPhone.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList
            ListDirectorEmployerPhoneDetail = DirectorClass.ListDirectorPhoneEmployer.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList
            ListDirectorIdentificationDetail = DirectorClass.ListDirectorIdentification.Where(Function(x) x.FK_Person_ID = id And x.FK_Person_Type = 7).ToList

            If Not objDirectorAddData Is Nothing Then
                FP_Director.Hide()
                FormPanelDirectorDetail.Show()
                WindowDetailDirector.Show()
                'ClearInput()
                With objDirectorAddData
                    DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
                    DspGelarDirector.Text = .Title
                    DspNamaLengkap.Text = .Last_Name
                    DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                    'daniel 18 jan 2021
                    If .BirthDate.HasValue Then
                        DspTanggalLahir.Text = formatDate(.BirthDate, "dd-MMM-yy")
                    Else
                        DspTanggalLahir.Text = ""
                    End If
                    'DspTanggalLahir.Text = formatDate(.BirthDate, "dd-MMM-yy")
                    'end 18 jan 2021
                    DspTempatLahir.Text = .Birth_Place
                    DspNamaIbuKandung.Text = .Mothers_Name
                    DspNamaAlias.Text = .Alias
                    DspNIK.Text = .SSN
                    DspNoPassport.Text = .Passport_Number
                    'daniel 19 jan 2021
                    DspNegaraPenerbitPassport.Text = GlobalReportFunctionBLL.getCountryByCode(.Passport_Country)
                    'DspNegaraPenerbitPassport.Text = .Passport_Country
                    'end 19 jan 2021
                    DspNoIdentitasLain.Text = .ID_Number
                    DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                    DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                    DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                    DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                    DspEmail.Text = .Email
                    DspEmail2.Text = .Email2
                    DspEmail3.Text = .Email3
                    DspEmail4.Text = .Email4
                    DspEmail5.Text = .Email5
                    DspDeceased.Text = .Deceased
                    If .Deceased Then
                        'Daniel 2021 Jan 7
                        If .Deceased_Date IsNot Nothing Then
                            DspDeceasedDate.Text = formatDate(.Deceased_Date, "dd-MMM-yy")
                            DspDeceasedDate.Hidden = False
                        End If
                    Else
                        DspDeceasedDate.Hidden = True
                        'end 2021 Jan 7
                    End If
                    DspPEP.Text = .Tax_Reg_Number
                    DspNPWP.Text = .Tax_Number
                    DspSourceofWealth.Text = .Source_of_Wealth
                    DspOccupation.Text = .Occupation
                    DspCatatan.Text = .Comments
                    DspTempatBekerja.Text = .Employer_Name
                End With

                BindDetailPhone(StorePhoneDirector, ListDirectorPhoneDetail)
                BindDetailPhone(StorePhoneDirectorEmployer, ListDirectorEmployerPhoneDetail)
                BindDetailAddress(StoreAddressDirector, ListDirectorAddressDetail)
                BindDetailAddress(StoreAddressDirectorEmployer, ListDirectorEmployerAddressDetail)
                BindDetailIdentification(StoreIdentificationDirector, ListDirectorIdentificationDetail)

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordIdentificationDirector(id As String)
        Try
            ObjDetailIdentificationDirector = ListDirectorIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
            If Not ObjDetailIdentificationDirector Is Nothing Then

                PanelDetailDirectorIdentification.Show()
                FormPanelDirectorIdentification.Hide()
                WindowDetailDirectorIdentification.Title = "Detail Identitas"
                WindowDetailDirectorIdentification.Show()
                With ObjDetailIdentificationDirector
                    DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                    DspNumberDirector.Text = .Number
                    DspIssueDateDirector.Text = If(.Issue_Date Is Nothing, "", .Issue_Date.Value.ToString("dd-MMM-yy"))
                    DspExpiryDateDirector.Text = If(.Expiry_Date Is Nothing, "", .Expiry_Date.Value.ToString("dd-MMM-yy"))
                    DspIssuedByDirector.Text = .Issued_By
                    DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                    DspIdentificationCommentDirector.Text = .Identification_Comment
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordIdentification(id As String)
        Try
            ObjDetailIdentification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
            If Not ObjDetailIdentification Is Nothing Then

                PanelDetailIdentification.Show()
                FormPanelIdentification.Hide()
                WindowDetailIdentification.Show()
                With ObjDetailIdentification
                    DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                    DspNumber.Text = .Number
                    DspIssueDate.Text = If(.Issue_Date Is Nothing, "", .Issue_Date.Value.ToString("dd-MMM-yy"))
                    DspExpiryDate.Text = If(.Expiry_Date Is Nothing, "", .Expiry_Date.Value.ToString("dd-MMM-yy"))
                    DspIssuedBy.Text = .Issued_By
                    DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                    DspIdentificationComment.Text = .Identification_Comment
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordPhoneDirector(id As String)
        Try
            ObjDetailPhone = ListDirectorPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhone Is Nothing Then

                PanelPhoneDirector.Show()
                PanelPhoneDirectorEmployer.Hide()
                WindowDetailDirectorPhone.Title = "Detail Director Phone"
                WindowDetailDirectorPhone.Show()
                FormPanelDirectorPhone.Hide()
                FormPanelEmpDirectorTaskDetail.Hide()

                With ObjDetailPhone
                    DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                    DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                    DspKodeAreaTelpDirector.Text = .tph_country_prefix
                    DspNomorTeleponDirector.Text = .tph_number
                    DspNomorExtensiDirector.Text = .tph_extension
                    DspCatatanDirectorPhoneDirector.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordPhoneDirectorEmployer(id As String)
        Try
            ObjDetailPhone = ListDirectorEmployerPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhone Is Nothing Then

                PanelPhoneDirector.Hide()
                PanelPhoneDirectorEmployer.Show()
                WindowDetailDirectorPhone.Title = "Detail Director Phone Employer"
                WindowDetailDirectorPhone.Show()
                FormPanelDirectorPhone.Hide()
                FormPanelEmpDirectorTaskDetail.Hide()

                With ObjDetailPhone
                    DspKategoriKontakDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                    DspJenisAlatKomunikasiDirectorEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                    DspKodeAreaTelpDirectorEmployer.Text = .tph_country_prefix
                    DspNomorTeleponDirectorEmployer.Text = .tph_number
                    DspNomorExtensiDirectorEmployer.Text = .tph_extension
                    DspCatatanDirectorPhoneDirectorEmployer.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressEmployerIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressEmployerIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressEmployerIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddressEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddress(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddressDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddressDirectorEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordAddress(id As String)
        Try
            ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not ObjDetailAddress Is Nothing Then

                PanelDetailAddress.Show()
                PanelDetailAddressEmployer.Hide()
                FormPanelAddressDetail.Hide()
                FormPanelAddressDetailEmployer.Hide()
                WindowDetailAddress.Title = "Detail Address"
                WindowDetailAddress.Show()
                ClearInput()
                With ObjDetailAddress
                    DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                    DspAddress.Text = .Address
                    DspTown.Text = .Town
                    DspCity.Text = .City
                    DspZip.Text = .Zip
                    Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                    DspState.Text = .State
                    DspcommentsAddress.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordAddressEmployer(id As String)
        Try
            ObjDetailAddressEmployer = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not ObjDetailAddressEmployer Is Nothing Then

                PanelDetailAddressEmployer.Show()
                PanelDetailAddress.Hide()
                FormPanelAddressDetail.Hide()
                FormPanelAddressDetailEmployer.Hide()
                WindowDetailAddress.Title = "Detail Address Employer"
                WindowDetailAddress.Show()

                With ObjDetailAddressEmployer
                    DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                    DspAddressEmployer.Text = .Address
                    DspTownEmployer.Text = .Town
                    DspCityEmployer.Text = .City
                    DspZipEmployer.Text = .Zip
                    Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                    DspStateEmployer.Text = .State
                    DspcommentsAddressEmployer.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordAddressDirector(id As String)
        Try
            ObjDetailAddress = ListDirectorAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not ObjDetailAddress Is Nothing Then

                PanelAddressDirector.Show()
                PanelAddressDirectorEmployer.Hide()
                FP_Address_Director.Hide()
                FP_Address_Emp_Director.Hide()
                WindowDetailDirectorAddress.Title = "Detail Director Address"
                WindowDetailDirectorAddress.Show()

                With ObjDetailAddress
                    DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                    DspAlamatDirector.Text = .Address
                    DspKecamatanDirector.Text = .Town
                    DspKotaKabupatenDirector.Text = .City
                    DspKodePosDirector.Text = .Zip
                    DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                    DspProvinsiDirector.Text = .State
                    DspCatatanAddressDirector.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordAddressDirectorEmployer(id As String)
        Try
            ObjDetailAddress = ListDirectorEmployerAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not ObjDetailAddress Is Nothing Then

                PanelAddressDirector.Hide()
                PanelAddressDirectorEmployer.Show()
                FP_Address_Director.Hide()
                FP_Address_Emp_Director.Hide()
                WindowDetailDirectorAddress.Title = "Detail Director Address Employer"
                WindowDetailDirectorAddress.Show()

                With ObjDetailAddress
                    DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                    DspAlamatDirectorEmployer.Text = .Address
                    DspKecamatanDirectorEmployer.Text = .Town
                    DspKotaKabupatenDirectorEmployer.Text = .City
                    DspKodePosDirectorEmployer.Text = .Zip
                    DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                    DspProvinsiDirectorEmployer.Text = .State
                    DspCatatanAddressDirectorEmployer.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridCommandPhoneCorporate(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhone(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridCommandAddressCorporate(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddress(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region





    Private Sub CleanFormIndividuWIC()
        TxtINDV_Alias.Clear()
        TxtINDV_Birth_place.Clear()
        TxtINDV_Email.Clear()
        TxtINDV_employer_name.Clear()
        TxtINDV_ID_Number.Clear()
        TxtINDV_last_name.Clear()
        TxtINDV_Mothers_name.Clear()
        TxtINDV_Occupation.Clear()
        'TxtINDV_Passport_country.Clear()
        TxtINDV_Passport_number.Clear()
        TxtINDV_SSN.Clear()
        TxtINDV_Title.Clear()
    End Sub

    Private Sub CleanFormCorporateWIC()
        TxtCorp_Business.Clear()
        TxtCorp_Comments.Clear()
        TxtCorp_Commercial_name.Clear()
        TxtCorp_Email.Clear()
        TxtCorp_Incorporation_number.Clear()
        TxtCorp_incorporation_state.Clear()
        TxtCorp_Name.Clear()
        TxtCorp_tax_number.Clear()
        TxtCorp_url.Clear()
    End Sub

    Function getDataRowFromDB(strQuery As String) As DataRow
        Try
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Return drResult
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
            Return Nothing
        End Try
    End Function

    Function formatDate(Dates As DateTime, StringformatDate As String) As String
        Dim strDate As String = ""
        If Dates = Nothing Or Dates <> DateTime.MinValue Then
            strDate = Dates.ToString(StringformatDate)
        End If
        Return strDate
    End Function
End Class
