﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/Site1.Master" CodeFile="GenerateSiPesatView.aspx.vb" Inherits="goAML_GenerateSiPesatView" %>

<%--Begin Update Penambahan Advance Filter--%>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>
<%--End Update Penambahan Advance Filter--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };
        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
        var columnAutoResize = function (grid) {
            App.GridpanelView.columns.forEach(function (col) {
                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }
            });
        };
        var prepareCommandCollection = function (grid, toolbar, rowIndex, record) {
            var GenerateButton = toolbar.items.get(0);
            var EditButton = toolbar.items.get(1);

            //Total valid > 0
            var wr = record.data.Valid
            if (EditButton != undefined) {
                if (wr < 1) {
                    GenerateButton.setDisabled(true)
                    EditButton.setDisabled(true)
                }
            };

            //var wf = record.data.StatusReport
            //if (EditButton != undefined) {
            //    if (wf == "Waiting For Generate" || wf == 'Reference No Uploaded') {
            //        EditButton.setDisabled(true)
            //    }
            //};

            //if (GenerateButton != undefined) {
            //    if (wf == 'Need To Correction' || wf == 'Waiting For Approval') {
            //        GenerateButton.setDisabled(true)
            //    }
            //};


        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:GridPanel ID="GridpanelView" ClientIDMode="Static" runat="server" Title="List OF Generate goAML">
        <View>
            <ext:GridView runat="server" EnableTextSelection="true">
            </ext:GridView>
        </View>
        <Store>
            <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadData">
                <Model>
                    <ext:Model runat="server">
                        <Fields>
                            <ext:ModelField Name="PK_Sipesat_GeneratedFileList_ID" Type="Int" />
                            <ext:ModelField Name="IDPJK" Type="Int" />
                            <ext:ModelField Name="Periode" Type="String" />
                            <ext:ModelField Name="JenisInformasi" Type="String" />
                            <ext:ModelField Name="Valid" Type="Int" />
                            <ext:ModelField Name="inValid" Type="Int" />
                            <ext:ModelField Name="Report_PPATK_Date" Type="Date" />
                            <ext:ModelField Name="No_Referensi" Type="String" />
                            <ext:ModelField Name="LastUpdateDate" Type="Date" />
                            <ext:ModelField Name="StatusReport" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
                <Sorters></Sorters>
                <Proxy>
                    <ext:PageProxy />
                </Proxy>
            </ext:Store>
        </Store>
        <ColumnModel>
            <Columns>
                <ext:CommandColumn runat="server" ID="columncrud" Width="200" Text="Action">
                    <Commands>
                        <ext:GridCommand Icon="ApplicationViewDetail" Text="Generate" CommandName="Generate">
                        </ext:GridCommand>
                        <ext:GridCommand Icon="DiskUpload" Text="Submission" CommandName="Upload">
                        </ext:GridCommand>
                    </Commands>
                    <DirectEvents>
                        <Command OnEvent="GridcommandListGenerateView">
                            <EventMask ShowMask="true"></EventMask>
                            <ExtraParams>
                                <ext:Parameter Name="unikkey" Value="record.data.PK_Sipesat_GeneratedFileList_ID" Mode="Raw"></ext:Parameter>
                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                            </ExtraParams>
                        </Command>
                    </DirectEvents>
                </ext:CommandColumn>
                <ext:Column runat="server" DataIndex="PK_Sipesat_GeneratedFileList_ID" Text="ID" ID="No" />
                <ext:Column runat="server" DataIndex="IDPJK" Text="IDPJK" />
                <ext:Column runat="server" DataIndex="Periode" Text="Periode" />
                <ext:Column runat="server" DataIndex="JenisInformasi" Text="Jenis Informasi" />
                <ext:Column runat="server" DataIndex="Valid" Text="Total Valid" />
                <ext:Column runat="server" DataIndex="inValid" Text="Total Invalid" />
                <ext:DateColumn runat="server" DataIndex="Report_PPATK_Date" Text="Tanggal Referensi PPATK" Format="dd-MMM-yyyy" />
                <ext:Column runat="server" DataIndex="No_Referensi" Text="No. Referensi PPATK" />
                <ext:DateColumn runat="server" DataIndex="LastUpdateDate" Text="Last Generated Date" Format="dd-MMM-yyyy" />
                <ext:Column runat="server" DataIndex="StatusReport" Text="Status" />
            </Columns>
        </ColumnModel>
        <Plugins>
            <ext:FilterHeader ID="grdiheaderfilter" runat="server" Remote="true"></ext:FilterHeader>
        </Plugins>
        <BottomBar>
            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
        </BottomBar>
        <DockedItems>
            <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                <Items>
                    <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export :">
                        <Items>
                            <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                            <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                        </Items>
                    </ext:ComboBox>
                    <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" AutoPostBack="true" OnClick="ExportExcel" ClientIDMode="Static" />
                    <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page" AutoPostBack="true" OnClick="ExportAllExcel" />
                    <ext:Button ID="BtnAdd" runat="server" Text="Add New Record" Icon="Add" Handler="NawadataDirect.BtnAdd_Click()" Hidden="true">
                    </ext:Button>
                    <ext:Button ID="AdvancedFilter" runat="server" Text="Advanced Filter" Icon="Add" Handler="NawadataDirect.BtnAdvancedFilter_Click()">
                    </ext:Button>
                </Items>
            </ext:Toolbar>
            <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true" Dock="Top" Hidden="true">
                <Items>
                    <ext:HyperlinkButton ID="HyperlinkButton1" runat="server" Text="Clear Advanced Filter">
                        <DirectEvents>
                            <Click OnEvent="btnClear_Click"></Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                    <ext:Label ID="LblAdvancedFilter" runat="server" Text="">
                    </ext:Label>
                </Items>
            </ext:Toolbar>
        </DockedItems>
        <Listeners>
            <ViewReady Handler="columnAutoResize(this);
                    this.getStore().on('load', Ext.bind(columnAutoResize, null, [this]));"
                Delay="10" />
        </Listeners>
    </ext:GridPanel>

    <%--Begin Update Penambahan Advance Filter--%>
    <ext:Panel ID="Panel1" runat="server" Hidden="true">
        <Content>
            <uc1:AdvancedFilter runat="server" ID="AdvancedFilter1" />
        </Content>
        <Items>
        </Items>
    </ext:Panel>
    <%--End Update Penambahan Advance Filter--%>

</asp:Content>
