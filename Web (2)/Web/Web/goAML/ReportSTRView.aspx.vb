﻿Imports OfficeOpenXml
Imports Ext.Net
Partial Class ReportSTRView
    Inherits Parent
    Public objFormModuleView As NawaBLL.FormModuleView
    Public Property strWhereClause() As String
        Get
            Return Session("ReportSTRView.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("ReportSTRView.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("ReportSTRView.strSort")
        End Get
        Set(ByVal value As String)
            Session("ReportSTRView.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("ReportSTRView.indexStart")
        End Get
        Set(ByVal value As String)
            Session("ReportSTRView.indexStart") = value
        End Set
    End Property
    Public Property QueryTable() As String
        Get
            Return Session("ReportSTRView.Table")
        End Get
        Set(ByVal value As String)
            Session("ReportSTRView.Table") = value
        End Set
    End Property
    Public Property QueryField() As String
        Get
            Return Session("ReportSTRView.Field")
        End Get
        Set(ByVal value As String)
            Session("ReportSTRView.Field") = value
        End Set
    End Property

    Private Sub ReportSTRView_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer
            Try
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)


                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                    Dim strIDCode As String = 1
                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If

                objFormModuleView.ModuleID = objmodule.PK_Module_ID
                objFormModuleView.ModuleName = objmodule.ModuleName


                objFormModuleView.AddField("PK_ID", "PK", 1, True, False, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
                objFormModuleView.AddField("Case_ID", "Case ID", 2, False, True, NawaBLL.Common.MFieldType.VARCHARValue, , , , , )
                objFormModuleView.AddField("Tanggal_Laporan", "Tanggal Laporan", 3, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )
                objFormModuleView.AddField("Jenis_Laporan", "Jenis Laporan", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("Ref_Num", "No Ref Laporan", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("Fiu_Ref_Number", "No Ref PPATK", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, ) 'Danamon Added 18-Nov-2020
                objFormModuleView.AddField("Alasan", "Alasan", 7, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("Tindakan_Pelapor", "Tindakan Pelapor", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("StatusReport", "Status", 9, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("Remark", "Remark", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("CreatedDate", "Created Date", 11, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )
                objFormModuleView.AddField("LastUpdateDate", "Last Updated Date", 12, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )

                objFormModuleView.SettingFormView()
            Catch ex As Exception

            End Try


            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"
            End If




        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            'Begin Penambahan Advanced Filter
            If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                Toolbar2.Hidden = True
            Else
                Toolbar2.Hidden = False
            End If
            LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")
            'END Penambahan Advanced Filter

            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

            'strfilter = strfilter.Replace("Active", objFormModuleView.ModuleName & ".Active")


            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter

            'Begin Update Penambahan Advance Filter
            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            End If


            Me.strOrder = strsort
            ' By Agam
            'If strsort = "" Then -- 
            '    strsort = ""
            'End If
            ' ==== end Agam ====
            QueryTable = "vw_reportSTR"
            'Danamon Edited 18-Nov-2020
            QueryField = " [PK_ID],[Case_ID],[Tanggal_Laporan],[Jenis_Laporan],[Ref_Num],[Fiu_Ref_Number],[Alasan],[Tindakan_Pelapor],[StatusReport],[Remark],[CreatedDate],[LastUpdateDate]"
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("  dbo.goAML_ReportSTR AS a LEFT JOIN dbo.goAML_Indikator_LaporanSTR AS b ON a.Case_ID = b.Case_ID ", "a.PK_ID ,a.Case_ID, a.Tanggal_Laporan, a.Jenis_laporan, a.Ref_Num, a.Alasan, a.Tindakan_Pelapor, b.FK_Indikator_Laporan AS Indikator_Laporan, CASE WHEN a.Status = 1 THEN 'Uploaded' ELSE 'Submitted' END AS StatusReport, a.CreatedDate,a.LastUpdateDate", Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("ReportSTR")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 0
                            For Each item As System.Data.DataColumn In objtbl.Columns

                                intcolnumber = intcolnumber + 1
                                If item.DataType = GetType(Date) Then

                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename= ReportSTR.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)


                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("  dbo.goAML_ReportSTR AS a LEFT JOIN dbo.goAML_Indikator_LaporanSTR AS b ON a.Case_ID = b.Case_ID ", "a.PK_ID ,a.Case_ID, a.Tanggal_Laporan, a.Jenis_laporan, a.Ref_Num, a.Alasan, a.Tindakan_Pelapor, b.FK_Indikator_Laporan AS Indikator_Laporan, CASE WHEN a.Status = 1 THEN 'Uploaded' ELSE 'Submitted' END AS StatusReport, a.CreatedDate,a.LastUpdateDate", Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename= ReportSTR.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))

                    'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(" ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID", "PK_ModuleApproval_ID, ModuleName, ModuleKey,CreatedDate, moduleaction.ModuleActionName AS ActionName,CreatedBy", Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)
                    '    '                        objFormModuleView.changeheaderName(objtbl)
                    '    Using resource As New ExcelPackage(objfileinfo)
                    '        Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("Approval")
                    '        ws.Cells("A1").LoadFromDataTable(objtbl, True)
                    '        Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                    '        Dim intcolnumber As Integer = 0
                    '        For Each item As System.Data.DataColumn In objtbl.Columns
                    '            If item.DataType = GetType(Date) Then
                    '                intcolnumber = intcolnumber + 1
                    '                ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                    '            End If
                    '        Next
                    '        ws.Cells(ws.Dimension.Address).AutoFitColumns()
                    '        resource.Save()
                    '        Response.Clear()
                    '        Response.ClearHeaders()
                    '        Response.ContentType = "application/vnd.ms-excel"
                    '        Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                    '        Response.Charset = ""
                    '        Response.AddHeader("cache-control", "max-age=0")
                    '        Me.EnableViewState = False
                    '        Response.ContentType = "ContentType"
                    '        Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                    '        Response.End()
                    '    End Using
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("  dbo.goAML_ReportSTR AS a LEFT JOIN dbo.goAML_Indikator_LaporanSTR AS b ON a.Case_ID = b.Case_ID ", "a.PK_ID ,a.Case_ID, a.Tanggal_Laporan, a.Jenis_laporan, a.Ref_Num, a.Alasan, a.Tindakan_Pelapor, b.FK_Indikator_Laporan AS Indikator_Laporan, CASE WHEN a.Status = 1 THEN 'Uploaded' ELSE 'Submitted' END AS StatusReport, a.CreatedDate,a.LastUpdateDate", Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)
                        '                        objFormModuleView.changeheaderName(objtbl)
                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("ReportSTR")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 0
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                intcolnumber = intcolnumber + 1
                                If item.DataType = GetType(Date) Then

                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename= ReportSTR.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)

                    'Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(" ModuleApproval iNNER JOIN ModuleAction ON moduleapproval.PK_ModuleAction_ID=moduleaction.PK_ModuleAction_ID", "PK_ModuleApproval_ID, ModuleName, ModuleKey,CreatedDate, moduleaction.ModuleActionName AS ActionName,CreatedBy", Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)
                    '    For k As Integer = 0 To objtbl.Columns.Count - 1
                    '        'add separator 
                    '        stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                    '    Next
                    '    'append new line 
                    '    stringWriter_Temp.Write(vbCr & vbLf)
                    '    For i As Integer = 0 To objtbl.Rows.Count - 1
                    '        For k As Integer = 0 To objtbl.Columns.Count - 1
                    '            'add separator 
                    '            stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                    '        Next
                    '        'append new line 
                    '        stringWriter_Temp.Write(vbCr & vbLf)
                    '    Next
                    '    stringWriter_Temp.Close()
                    '    Response.Clear()
                    '    Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                    '    Response.Charset = ""
                    '    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    '    Me.EnableViewState = False
                    '    'Response.ContentType = "application/ms-excel"
                    '    Response.ContentType = "text/csv"
                    '    Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                    '    Response.End()
                    'End Using
                    Using objtbl As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("  dbo.goAML_ReportSTR AS a LEFT JOIN dbo.goAML_Indikator_LaporanSTR AS b ON a.Case_ID = b.Case_ID ", "a.PK_ID ,a.Case_ID, a.Tanggal_Laporan, a.Jenis_laporan, a.Ref_Num, a.Alasan, a.Tindakan_Pelapor, b.FK_Indikator_Laporan AS Indikator_Laporan, CASE WHEN a.Status = 1 THEN 'Uploaded' ELSE 'Submitted' END AS StatusReport, a.CreatedDate,a.LastUpdateDate", Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator 
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line 
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator 
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line 
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename= ReportSTR.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub




    <DirectMethod>
    Public Sub BtnAdd_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim sm As RowSelectionModel = TryCast(Me.GridpanelView.GetSelectionModel(), RowSelectionModel)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub ReportSTRView_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub

    'Begin Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update Penambahan Advance Filter


End Class
