﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="GenerateSiPesatUpload.aspx.vb" Inherits="goAML_GenerateSiPesatUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:FormPanel ID="FormPanelInput" runat="server" Layout="AnchorLayout" ButtonAlign="Center" Title="" BodyStyle="padding:20px" AutoScroll="true" Hidden="false" >
        <Items>
            <ext:Container runat="server" Layout="ColumnLayout" AnchorHorizontal="100%">
                <Items>
                    <ext:Container runat="server" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField runat="server" ID="lblIDPJK" FieldLabel="IDPJK" />
                            <ext:DisplayField runat="server" ID="lblPeriod" FieldLabel="Periode" />
                            <ext:DisplayField runat="server" ID="lblStatus" FieldLabel="Status" />
                            <ext:TextField ID="Text_no_ref" runat="server" FieldLabel="No. Referensi PPATK" AnchorHorizontal="40%" AllowBlank="false"/>
                        </Items>
                    </ext:Container>
                    <ext:Container runat="server" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField runat="server" ID="lblTotalValid" FieldLabel="Total Valid" />
                            <ext:DisplayField runat="server" ID="lblTotalInvalid" FieldLabel="Total Invalid" />
                            <ext:FileUploadField ID="FileDoc" runat="server" FieldLabel="Upload File" Width="500" Hidden="true" />
                            <ext:DateField ID="Date_no_ref_date" runat="server" FieldLabel="Tanggal Referensi PPATK" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false"/>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Container>
        </Items>
        <Buttons>
            <ext:Button ID="Button1" runat="server" Icon="Disk" Text="Generate">
                <Listeners>
                    <Click Handler="if (!#{FormPanelInput}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="BtnSave_DirectClick">
                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancelUpload" runat="server" Text="Back" Icon="Cancel">
                <DirectEvents>
                    <Click OnEvent="btnCancel_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
