﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="GeneratePPATKUpload.aspx.vb" Inherits="goAML_GeneratePPATKUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:FormPanel ID="FormPanelInput" runat="server" Layout="AnchorLayout" ButtonAlign="Center" Title="" BodyStyle="padding:20px" AutoScroll="true" Hidden="false">
        <Items>
            <ext:Container runat="server" Layout="ColumnLayout" AnchorHorizontal="100%">
                <Items>
                    <ext:Container runat="server" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField runat="server" LabelWidth="250" ID="lblTransDate" FieldLabel="Transaction Date" Text="A value here 1" AnchorHorizontal="90%" />
                            <ext:DisplayField runat="server" LabelWidth="250" ID="lblTransType" FieldLabel="Transaction Type" Text="A value here 1" AnchorHorizontal="90%" />
                            <ext:DisplayField runat="server" LabelWidth="250" ID="lblReportType" FieldLabel="Report Type" Text="A value here 1" AnchorHorizontal="90%" />
                            <ext:DisplayField runat="server" LabelWidth="250" ID="lblLastUpdateDate" FieldLabel="Last Update Date" Text="A value here 1" AnchorHorizontal="90%" />
                            <ext:DateField LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Tanggal Referensi PPATK" ID="txt_DateReferensi_PPATK" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                        </Items>
                    </ext:Container>
                    <ext:Container runat="server" ColumnWidth="0.5">
                        <Items>
                            <ext:DisplayField runat="server" LabelWidth="250" ID="lblTotalValid" FieldLabel="Total Valid" Text="A value here 1" AnchorHorizontal="90%" />
                            <ext:DisplayField runat="server" LabelWidth="250" ID="lblTotalInvalid" FieldLabel="Total Invalid" Text="A value here 1" AnchorHorizontal="90%" />
                            <ext:DisplayField runat="server" LabelWidth="250" ID="lblStatusUpload" FieldLabel="Status Uploaded to PPATK" Text="A value here 1" AnchorHorizontal="90%" />

                            <ext:TextField ID="txt_NoReferensiPPATK" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="No. Referensi PPATK" AnchorHorizontal="90%" MaxLength="30"></ext:TextField>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Container>
          <%--  <ext:FileUploadField ID="FileUploadField" runat="server" FieldLabel="Input File " AnchorHorizontal="100%" />--%>
        </Items>
        <Buttons>
            <ext:Button ID="btnSaveUpload" runat="server" Text="Submit" Icon="Disk">
                <Listeners>
                    <Click Handler="if (!#{FormPanelInput}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="BtnSave_DirectClick">
                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancelUpload" runat="server" Text="Back" Icon="Cancel">
                <DirectEvents>
                    <Click OnEvent="btnCancelUpload_Click"></Click>

                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
