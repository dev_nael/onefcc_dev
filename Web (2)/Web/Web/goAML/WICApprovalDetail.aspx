﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="WICApprovalDetail.aspx.vb" Inherits="goAML_WICApprovalDetail" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-form-item-label.x-form-item-label-default { width : 175px !important; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <ext:Window ID="WindowDetailDirector" runat="server" Icon="ApplicationViewDetail" Title="Customer Director Detail" Modal="true" Hidden="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="FormPanelDirectorDetail" runat="server" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:Hidden runat="server" ID="Hidden1"></ext:Hidden>
                    <ext:DisplayField ID="DspPeranDirector" runat="server" FieldLabel="Peran" AnchorHorizontal="70%" LabelWidth="250"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250" ID="DspGender" runat="server" FieldLabel="Gender" AnchorHorizontal="70%" AllowBlank="false"></ext:DisplayField>
                    <ext:DisplayField ID="DspGelarDirector" LabelWidth="250" runat="server" FieldLabel="Gelar" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaLengkap" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Nama Lengkap" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Tanggal Lahir" ID="DspTanggalLahir" AnchorHorizontal="90%"/>
                    <ext:DisplayField ID="DspTempatLahir" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaIbuKandung" LabelWidth="250" runat="server" FieldLabel="Nama Ibu Kandung" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaAlias" LabelWidth="250" runat="server" FieldLabel="Nama Alias" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNIK" LabelWidth="250" runat="server" FieldLabel="NIK" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNoPassport" LabelWidth="250" runat="server" FieldLabel="No. Passport" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraPenerbitPassport" LabelWidth="250" runat="server" FieldLabel="Negara Penerbit Passport" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNoIdentitasLain" LabelWidth="250" runat="server" FieldLabel="No. Identitas Lain" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250" AllowBlank="False" ID="DspKewarganegaraan1" runat="server" FieldLabel="Kewarganegaraan 1" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspKewarganegaraan2" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 2" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspKewarganegaraan3" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 3" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDomisili" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Negara Domisili" AnchorHorizontal="70%"></ext:DisplayField>

                    <ext:GridPanel ID="GridPanelDirectorPhoneBefore" runat="server" Title="Informasi Telepon Before" AutoScroll="true" Hidden="true">
                        <Store>
                            <ext:Store ID="StoreDirectorPhoneBefore" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model6" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column3" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn5" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorPhoneBefore">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelDirectorPhone" runat="server" Title="Informasi Telepon" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorPhone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column1" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelDirectorAddressBefore" runat="server" Title="Informasi Address Before" AutoScroll="true" Hidden="true">
                        <Store>
                            <ext:Store ID="StoreDirectorAddressBefore" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model7" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column13" runat="server" DataIndex="Type_Address" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="Addres" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="Cty" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="CountryCode" Text="Kode Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn6" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorAddressBefore">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelDirectorAddress" runat="server" Title="Informasi Address" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorAddress" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column7" runat="server" DataIndex="Type_Address" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="Addres" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="Cty" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="CountryCode" Text="Kode Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DisplayField ID="DspEmail" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspOccupation" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspTempatBekerja" LabelWidth="250" runat="server" FieldLabel="Tempat Bekerja" AnchorHorizontal="90%"></ext:DisplayField>

                    <ext:GridPanel ID="GridPanelDirectorPhoneEmployerBefore" runat="server" Title="Informasi Telepon Tempat Kerja Before" AutoScroll="true" Hidden="true">
                        <Store>
                            <ext:Store ID="StoreDirectorPhoneEmployerBefore" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model9" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column23" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column25" runat="server" DataIndex="Communcation_Type" Text="Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column27" runat="server" DataIndex="number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn7" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerPhoneBefore">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelDirectorPhoneEmployer" runat="server" Title="Informasi Telepon Tempat Kerja" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorPhoneEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model8" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column15" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="Communcation_Type" Text="Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelDirectorAddressEmployerBefore" runat="server" Title="Informasi Address Tempat Kerja Before" AutoScroll="true" Hidden="true">
                        <Store>
                            <ext:Store ID="StoreDirectorAddressEmployerBefore" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model10" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column29" runat="server" DataIndex="Type_Address" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column30" runat="server" DataIndex="Addres" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column33" runat="server" DataIndex="Cty" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column34" runat="server" DataIndex="CountryCode" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn9" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerAddressBefore">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelDirectorAddressEmployer" runat="server" Title="Informasi Address Tempat Kerja" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorAddressEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model17" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column21" runat="server" DataIndex="Type_Address" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="Addres" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="Cty" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="CountryCode" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn4" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelDirectorIdentificationBefore" runat="server" Title="Informasi Identitas Before" Hidden="true" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreIdentificationDirectorBefore" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model16" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn17" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column61" runat="server" DataIndex="Tipe" Text="Jenis Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column62" runat="server" DataIndex="No" Text="Nomor Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column63" runat="server" DataIndex="IssueDate" Text="Tanggal Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column64" runat="server" DataIndex="ExpiryDate" Text="Tanggal Kedaluwarsa" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column65" runat="server" DataIndex="IssuedCountry" Text="Negara Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn13" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorIdentificationBefore">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelDirectordentification" runat="server" Title="Informasi Identitas" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreIdentificationDirector" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model18" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn16" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column56" runat="server" DataIndex="Tipe" Text="Jenis Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column57" runat="server" DataIndex="No" Text="Nomor Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column58" runat="server" DataIndex="IssueDate" Text="Tanggal Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column59" runat="server" DataIndex="ExpiryDate" Text="Tanggal Kedaluwarsa" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column60" runat="server" DataIndex="IssuedCountry" Text="Negara Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn12" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DisplayField runat="server" LabelWidth="250" ID="DspDeceased" FieldLabel="Deceased ?"></ext:DisplayField>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Deceased Date " ID="DspDeceasedDate" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="NPWP" ID="DspNPWP" AnchorHorizontal="90%" />
                    <ext:DisplayField runat="server" LabelWidth="250" ID="DspPEP" FieldLabel="PEP ?"></ext:DisplayField>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Source of Wealth" ID="DspSourceofWealth" AnchorHorizontal="90%" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Catatan" ID="DspCatatan" AnchorHorizontal="90%" />
                </Items>
                <Buttons>
                    <ext:Button ID="Button5" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveCustomerDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorIdentification" runat="server" Icon="ApplicationViewDetail" Title="Identification Detail" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <ext:FormPanel ID="PanelDetailIdentificationDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DsptTypeDirector" runat="server" FieldLabel="Jenis Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspNumberDirector" runat="server" FieldLabel="Nomor Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssueDateDirector" runat="server" FieldLabel="Tanggal Diterbitkan"></ext:DisplayField>
                    <ext:DisplayField ID="DspExpiryDateDirector" runat="server" FieldLabel="Tanggal Kadaluarsa"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedByDirector" runat="server" FieldLabel="Diterbitkan Oleh"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedCountryDirector" runat="server" FieldLabel="Negara Penerbit Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspIdentificationCommentDirector" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button6" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelIdentificationDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailDirectorIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailIdentification" runat="server" Icon="ApplicationViewDetail" Title="Phone Detail" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <ext:FormPanel ID="PanelDetailIdentification" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DsptType" runat="server" FieldLabel="Jenis Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspNumber" runat="server" FieldLabel="Nomor Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssueDate" runat="server" FieldLabel="Tanggal Diterbitkan"></ext:DisplayField>
                    <ext:DisplayField ID="DspExpiryDate" runat="server" FieldLabel="Tanggal Kadaluarsa"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedBy" runat="server" FieldLabel="Diterbitkan Oleh"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedCountry" runat="server" FieldLabel="Negara Penerbit Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspIdentificationComment" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button8" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelIdentification_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorPhone" runat="server" Icon="ApplicationViewDetail" Title="Director Phone Detail" Modal="true" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="PanelPhoneDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspKategoriKontakDirector" runat="server" FieldLabel="Kategori Kontak"></ext:DisplayField>
                    <ext:DisplayField ID="DspJenisAlatKomunikasiDirector" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodeAreaTelpDirector" runat="server" FieldLabel="Kode Area Telp"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorTeleponDirector" runat="server" FieldLabel="Nomor Telepon"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorExtensiDirector" runat="server" FieldLabel="Nomor Extensi"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanDirectorPhoneDirector" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button3" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelPhoneDirectorEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspKategoriKontakDirectorEmployer" runat="server" FieldLabel="Kategori Kontak"></ext:DisplayField>
                    <ext:DisplayField ID="DspJenisAlatKomunikasiDirectorEmployer" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodeAreaTelpDirectorEmployer" runat="server" FieldLabel="Kode Area Telp"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorTeleponDirectorEmployer" runat="server" FieldLabel="Nomor Telepon"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorExtensiDirectorEmployer" runat="server" FieldLabel="Nomor Extensi"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanDirectorPhoneDirectorEmployer" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button4" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorEmployerPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorAddress" runat="server" Icon="ApplicationViewDetail" Title="Director Address Detail" Modal="true" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="PanelAddressDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspTipeAlamatDirector" runat="server" FieldLabel="Tipe Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspAlamatDirector" runat="server" FieldLabel="Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspKecamatanDirector" runat="server" FieldLabel="Kecamatan"></ext:DisplayField>
                    <ext:DisplayField ID="DspKotaKabupatenDirector" runat="server" FieldLabel="Kota/Kabupaten"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodePosDirector" runat="server" FieldLabel="Kode Pos"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDirector" runat="server" FieldLabel="Negara"></ext:DisplayField>
                    <ext:DisplayField ID="DspProvinsiDirector" runat="server" FieldLabel="Provinsi"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanAddressDirector" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button1" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorDetailAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelAddressDirectorEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspTipeAlamatDirectorEmployer" runat="server" FieldLabel="Tipe Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspAlamatDirectorEmployer" runat="server" FieldLabel="Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspKecamatanDirectorEmployer" runat="server" FieldLabel="Kecamatan"></ext:DisplayField>
                    <ext:DisplayField ID="DspKotaKabupatenDirectorEmployer" runat="server" FieldLabel="Kota/Kabupaten"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodePosDirectorEmployer" runat="server" FieldLabel="Kode Pos"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDirectorEmployer" runat="server" FieldLabel="Negara"></ext:DisplayField>
                    <ext:DisplayField ID="DspProvinsiDirectorEmployer" runat="server" FieldLabel="Provinsi"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanAddressDirectorEmployer" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button2" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorDetailEmployerAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailPhone" runat="server" Icon="ApplicationViewDetail" Title="Phone Detail" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <ext:FormPanel ID="PanelDetailPhone" runat="server" AnchorHorizontal="100%" BodyPadding="10" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="Dsptph_contact_type" runat="server" FieldLabel="Kategori Kontak"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_communication_type" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_country_prefix" runat="server" FieldLabel="Kode Area Telp"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_number" runat="server" FieldLabel="Nomor Telepon"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_extension" runat="server" FieldLabel="Nomor Extensi"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsPhone" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnCancelPhone" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelDetailPhoneEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="Dsptph_contact_typeEmployer" runat="server" FieldLabel="Kategori Kontak"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_communication_typeEmployer" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_country_prefixEmployer" runat="server" FieldLabel="Kode Area Telp"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_numberEmployer" runat="server" FieldLabel="Nomor Telepon"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_extensionEmployer" runat="server" FieldLabel="Nomor Extensi"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsPhoneEmployer" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button16" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelPhoneEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailPhone}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailAddress" runat="server" Icon="ApplicationViewDetail" Title="Address Detail" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <ext:FormPanel ID="PanelDetailAddress" runat="server" AnchorHorizontal="100%" BodyPadding="10" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspAddress_type" runat="server" FieldLabel="Tipe Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspAddress" runat="server" FieldLabel="Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspTown" runat="server" FieldLabel="Kecamatan"></ext:DisplayField>
                    <ext:DisplayField ID="DspCity" runat="server" FieldLabel="Kota/Kabupaten"></ext:DisplayField>
                    <ext:DisplayField ID="DspZip" runat="server" FieldLabel="Kode Pos"></ext:DisplayField>
                    <ext:DisplayField ID="Dspcountry_code" runat="server" FieldLabel="Negara"></ext:DisplayField>
                    <ext:DisplayField ID="DspState" runat="server" FieldLabel="Provinsi"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsAddress" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnCancelAddress" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelDetailAddressEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspAddress_typeEmployer" runat="server" FieldLabel="Tipe Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspAddressEmployer" runat="server" FieldLabel="Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspTownEmployer" runat="server" FieldLabel="Kecamatan"></ext:DisplayField>
                    <ext:DisplayField ID="DspCityEmployer" runat="server" FieldLabel="Kota/Kabupaten"></ext:DisplayField>
                    <ext:DisplayField ID="DspZipEmployer" runat="server" FieldLabel="Kode Pos"></ext:DisplayField>
                    <ext:DisplayField ID="Dspcountry_codeEmployer" runat="server" FieldLabel="Negara"></ext:DisplayField>
                    <ext:DisplayField ID="DspStateEmployer" runat="server" FieldLabel="Provinsi"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsAddressEmployer" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button19" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelAddressEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailAddress}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Container ID="container" runat="server" Layout="VBoxLayout">
        <LayoutConfig>
            <ext:VBoxLayoutConfig Align="Stretch"></ext:VBoxLayoutConfig>
        </LayoutConfig>
        <Items>
            <ext:FormPanel ID="PanelInfo" runat="server" Title="Module Approval" BodyPadding="10" Collapsible="true">
                <Items>
                    <ext:DisplayField ID="lblModuleName" runat="server" FieldLabel="Label Name">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblModuleKey" runat="server" FieldLabel="Module Key">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblAction" runat="server" FieldLabel="Action">
                    </ext:DisplayField>
                    <ext:DisplayField ID="LblCreatedBy" runat="server" FieldLabel="Created By">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblCreatedDate" runat="server" FieldLabel="Created Date">
                    </ext:DisplayField>
                </Items>
            </ext:FormPanel>

            <ext:Panel ID="Panel1" runat="server" Layout="HBoxLayout" ButtonAlign="Center" Flex="1" AutoScroll="true">
                <Items>
                    <ext:FormPanel ID="FormPanelOld" runat="server" Title="Old Value" Flex="1" BodyPadding="10" MarginSpec="0 5 0 0">
                        <Items>
                            <ext:DisplayField ID="PKWICOld" runat="server" FieldLabel="ID"></ext:DisplayField>
                            <ext:DisplayField ID="DspWICNoOld" runat="server" FieldLabel="WIC No"></ext:DisplayField>
                            <ext:DisplayField ID="DspIsUpdateFromDataSourceOld" runat="server" FieldLabel="Update From Data Source ?"></ext:DisplayField>
                            <ext:FormPanel runat="server" Title="WIC Add Individu" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="WIC_IndividuOld">
                                <Defaults>
                                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                                </Defaults>
                                <Items>
                                    <ext:DisplayField ID="TxtINDV_GenderOld" runat="server" FieldLabel="Jenis Kelamin" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_TitleOld" runat="server" FieldLabel="Gelar" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_last_nameOld" runat="server" FieldLabel="Nama Lengkap" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="DateINDV_BirthdateOld" runat="server" Flex="1" FieldLabel="Tanggal Lahir"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Birth_placeOld" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Mothers_nameOld" runat="server" FieldLabel="Nama Ibu Kandung" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_AliasOld" runat="server" FieldLabel="Nama Alias" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_SSNOld" runat="server" FieldLabel="NIK" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Passport_numberOld" runat="server" FieldLabel="No. Passport" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Passport_countryOld" runat="server" FieldLabel="Negara Penerbit Passport" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_ID_NumberOld" runat="server" FieldLabel="No. Identitas Lain" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CmbINDV_Nationality1Old" runat="server" FieldLabel="Kewarganegaraan 1" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="CmbINDV_Nationality2Old" runat="server" FieldLabel="Kewarganegaraan 2" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="CmbINDV_Nationality3Old" runat="server" FieldLabel="Kewarganegaraan 3" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="CmbINDV_residenceOld" runat="server" FieldLabel="Negara Domisili" AnchorHorizontal="80%" />

                                    <%--<ext:GridPanel ID="GridPanelPhoneIndividuDetailBefore" runat="server" Title="Phone Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model23" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn23" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column5" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Width="170"></ext:Column>
                                                <ext:Column ID="Column11" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Width="90"></ext:Column>
                                                <ext:Column ID="Column19" runat="server" DataIndex="number" Text="Nomor Telepon" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn8" runat="server" Text="Action" Flex="1">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar27" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelPhoneIndividuDetailOld" runat="server" Title="Phone Detail Data" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneIndividuDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="ModelDetailOld" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumberPhoneDetailIndividuOld" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="colTph_Contact_TypeIndividuOld" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Width="170"></ext:Column>
                                                <ext:Column ID="coltph_communication_typeIndividuOld" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Width="90"></ext:Column>
                                                <ext:Column ID="coltph_numberIndividuOld" runat="server" DataIndex="number" Text="Nomor Telepon" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneIndividuDetailOld" runat="server" Text="Action" Flex="1">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar28" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelAddressIndividuDetailBefore" runat="server" Title="Address Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="Store3" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model25" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn25" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column87" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Width="170"></ext:Column>
                                                <ext:Column ID="Column88" runat="server" DataIndex="Addres" Text="Alamat" Width="90"></ext:Column>
                                                <ext:Column ID="Column89" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Width="80"></ext:Column>
                                                <ext:Column ID="Column90" runat="server" DataIndex="CountryCode" Text="Negara" Width="100"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn11" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar29" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelAddressIndividuDetailOld" runat="server" Title="Address Detail Data" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressIndividuDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model26" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailIndividuOld" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="ColAddress_typeIndividuOld" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Width="170"></ext:Column>
                                                <ext:Column ID="ColAddressIndividuOld" runat="server" DataIndex="Addres" Text="Alamat" Width="90"></ext:Column>
                                                <ext:Column ID="ColCityIndividuOld" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Width="80"></ext:Column>
                                                <ext:Column ID="Colcountry_codeIndividuOld" runat="server" DataIndex="CountryCode" Text="Negara" Width="100"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressIndividuDetailOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar30" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField ID="TxtINDV_EmailOld" runat="server" FieldLabel="Email" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email2Old" runat="server" FieldLabel="Email2" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email3Old" runat="server" FieldLabel="Email3" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email4Old" runat="server" FieldLabel="Email4" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email5Old" runat="server" FieldLabel="Email5" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_OccupationOld" runat="server" FieldLabel="Pekerjaan" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_employer_nameOld" runat="server" FieldLabel="Tempat Bekerja" AnchorHorizontal="80%"></ext:DisplayField>

                                    <%--<ext:GridPanel ID="GridPanelPhoneIndividualEmployerDetailBefore" runat="server" Hidden="true" Title="Phone Detail Data Employer Before" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model27" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn27" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column95" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Width="170"></ext:Column>
                                                <ext:Column ID="Column96" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Width="90"></ext:Column>
                                                <ext:Column ID="Column97" runat="server" DataIndex="number" Text="Nomor Telepon" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn15" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneEmployerIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar31" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelPhoneIndividualEmployerDetailOld" runat="server" Title="Phone Detail Data Employer" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneEmployerIndividuDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model28" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn28" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column98" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Width="170"></ext:Column>
                                                <ext:Column ID="Column99" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Width="90"></ext:Column>
                                                <ext:Column ID="Column100" runat="server" DataIndex="number" Text="Nomor Telepon" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneIndividualEmployerDetailOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneEmployerIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar32" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelAddressIndividualEmployerDetailBefore" runat="server" Hidden="true" Title="Address Detail Data Employer Before" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="Store7" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model31" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn29" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column101" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Width="170"></ext:Column>
                                                <ext:Column ID="Column102" runat="server" DataIndex="Addres" Text="Alamat" Width="90"></ext:Column>
                                                <ext:Column ID="Column103" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Width="80"></ext:Column>
                                                <ext:Column ID="Column104" runat="server" DataIndex="CountryCode" Text="Negara" Width="100"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn17" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressEmployerIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar33" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelAddressIndividualEmployerDetailOld" runat="server" Title="Address Detail Data Employer" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressEmployerIndividuDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model32" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn30" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column105" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Width="170"></ext:Column>
                                                <ext:Column ID="Column106" runat="server" DataIndex="Addres" Text="Alamat" Width="90"></ext:Column>
                                                <ext:Column ID="Column107" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Width="80"></ext:Column>
                                                <ext:Column ID="Column108" runat="server" DataIndex="CountryCode" Text="Negara" Width="100"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressIndividualEmployerDetailOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressEmployerIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar34" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelIdentificationIndividualDetailBefore" runat="server" Title="Identitas Detail Data Before" Hidden="true" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="Store9" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model33" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn31" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column109" runat="server" DataIndex="Tipe" Text="Jenis Identitas" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column110" runat="server" DataIndex="No" Text="Nomor Identitas" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column111" runat="server" DataIndex="IssueDate" Text="Tanggal Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column112" runat="server" DataIndex="ExpiryDate" Text="Tanggal Kedaluwarsa" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column113" runat="server" DataIndex="IssuedCountry" Text="Negara Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn19" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdIdentificationBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar35" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelIdentificationIndividualDetailOld" runat="server" Title="Identitas Detail Data" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreIdentificationDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model34" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn32" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column114" runat="server" DataIndex="Tipe" Text="Jenis Identitas" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column115" runat="server" DataIndex="No" Text="Nomor Identitas" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column116" runat="server" DataIndex="IssueDate" Text="Tanggal Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column117" runat="server" DataIndex="ExpiryDate" Text="Tanggal Kedaluwarsa" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column118" runat="server" DataIndex="IssuedCountry" Text="Negara Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnIdentificationIndividualDetailOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdIdentificationBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar36" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField ID="CbINDV_DeceasedOld" runat="server" FieldLabel="Sudah Meninggal" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="DateINDV_Deceased_DateOld" runat="server" Flex="1" FieldLabel="Tanggal Meninggal" Hidden="true" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Tax_NumberOld" runat="server" FieldLabel="NPWP" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CbINDV_Tax_Reg_NumberOld" runat="server" FieldLabel="is PEP ?" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Source_Of_WealthOld" runat="server" FieldLabel="Sumber Dana" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_CommentsOld" runat="server" FieldLabel="Catatan" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FormPanel>

                            <ext:FormPanel runat="server" Title="WIC Add Corporate" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="WIC_CorporateOld">
                                <Defaults>
                                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                                </Defaults>
                                <Items>
                                    <ext:DisplayField ID="TxtCorp_NameOld" runat="server" FieldLabel="Nama Korporasi" AnchorHorizontal="80%" AllowBlank="false"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_Commercial_nameOld" runat="server" FieldLabel="Nama Komersial" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CmbCorp_Incorporation_legal_formOld" runat="server" FieldLabel="Bentuk Korporasi" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="TxtCorp_Incorporation_numberOld" runat="server" FieldLabel="Nomor Induk Berusaha" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_BusinessOld" runat="server" FieldLabel="Bidang Usaha" AnchorHorizontal="80%"></ext:DisplayField>

                                    <%--<ext:GridPanel ID="GridPanelPhoneCorporateDetailBefore" runat="server" Title="Phone Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="Store11" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model35" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn33" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column119" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Width="170"></ext:Column>
                                                <ext:Column ID="Column120" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Width="90"></ext:Column>
                                                <ext:Column ID="Column121" runat="server" DataIndex="number" Text="Nomor Telepon" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn21" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneCorporateBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar37" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelPhoneCorporateDetailOld" runat="server" Title="Phone Detail Data" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneCorporateDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model36" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn34" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column122" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Width="170"></ext:Column>
                                                <ext:Column ID="Column123" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Width="90"></ext:Column>
                                                <ext:Column ID="Column124" runat="server" DataIndex="number" Text="Nomor Telepon" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneCorporateDetailOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneCorporateBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar38" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelAddressCorporateDetailBefore" runat="server" Title="Address Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="Store13" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model37" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn35" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column125" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Width="170"></ext:Column>
                                                <ext:Column ID="Column126" runat="server" DataIndex="Addres" Text="Alamat" Width="90"></ext:Column>
                                                <ext:Column ID="Column127" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Width="80"></ext:Column>
                                                <ext:Column ID="Column128" runat="server" DataIndex="CountryCode" Text="Negara" Width="100"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn23" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressCorporateBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar39" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelAddressCorporateDetailOld" runat="server" Title="Address Detail Data" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressCorporateDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model38" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn36" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column129" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Width="170"></ext:Column>
                                                <ext:Column ID="Column130" runat="server" DataIndex="Addres" Text="Alamat" Width="90"></ext:Column>
                                                <ext:Column ID="Column131" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Width="80"></ext:Column>
                                                <ext:Column ID="Column132" runat="server" DataIndex="CountryCode" Text="Negara" Width="100"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressCorporateDetailOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressCorporateBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar40" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField ID="TxtCorp_EmailOld" runat="server" FieldLabel="Email Korporasi" AnchorHorizontal="80%" AllowBlank="false"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_urlOld" runat="server" FieldLabel="Website Korporasi" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_incorporation_stateOld" runat="server" FieldLabel="Provinsi" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CmbCorp_incorporation_country_codeOld" runat="server" FieldLabel="Negara" AnchorHorizontal="80%" />

                                    <%--<ext:GridPanel ID="GridPanelDirector_CorpBefore" runat="server" Title="Director Corp Before" AutoScroll="true" Hidden="true">
                                        <Store>
                                            <ext:Store ID="Store15" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Model39" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="NO_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Roles" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn37" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column133" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column134" runat="server" DataIndex="Roles" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn25" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustDirectorBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.NO_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar41" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GP_Corp_DirectorOld" runat="server" Title="Director Corp" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Director_CorpOld" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Model_Director_CorpOld" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="NO_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Roles" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn38" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column135" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column136" runat="server" DataIndex="Roles" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnDirectorOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustDirectorOld">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.NO_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar42" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:DisplayField ID="CmbCorp_RoleOld" runat="server" FieldLabel="Peran" AnchorHorizontal="80%" />--%>
                                    <ext:DisplayField ID="DateCorp_incorporation_dateOld" runat="server" Flex="1" AllowBlank="false" FieldLabel="Tanggal Pendirian"></ext:DisplayField>
                                    <ext:DisplayField ID="chkCorp_business_closedOld" runat="server" FieldLabel="Tutup?"></ext:DisplayField>
                                    <ext:DisplayField ID="DateCorp_date_business_closedOld" runat="server" Flex="1" FieldLabel="Tanggal Tutup" Hidden="true"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_tax_numberOld" runat="server" FieldLabel="NPWP" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_CommentsOld" runat="server" FieldLabel="Catatan" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FormPanel>
                        </Items>
                    </ext:FormPanel>

                    <ext:FormPanel ID="FormPanelNew" runat="server" Title="New Value" Flex="1" BodyPadding="10">
                        <Items>
                            <ext:DisplayField ID="PKWIC" runat="server" FieldLabel="ID"></ext:DisplayField>
                            <ext:DisplayField ID="DspWICNo" runat="server" FieldLabel="WIC No"></ext:DisplayField>
                            <ext:DisplayField ID="DspIsUpdateFromDataSource" runat="server" FieldLabel="Update From Data Source ?"></ext:DisplayField>
                            <ext:FormPanel runat="server" Title="WIC Add Individu" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="WIC_Individu">
                                <Defaults>
                                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                                </Defaults>
                                <Items>
                                    <ext:DisplayField ID="TxtINDV_Gender" runat="server" FieldLabel="Jenis Kelamin" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Title" runat="server" FieldLabel="Gelar" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_last_name" runat="server" FieldLabel="Nama Lengkap" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="DateINDV_Birthdate" runat="server" Flex="1" FieldLabel="Tanggal Lahir"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Birth_place" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Mothers_name" runat="server" FieldLabel="Nama Ibu Kandung" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Alias" runat="server" FieldLabel="Nama Alias" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_SSN" runat="server" FieldLabel="NIK" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Passport_number" runat="server" FieldLabel="No. Passport" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Passport_country" runat="server" FieldLabel="Negara Penerbit Passport" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_ID_Number" runat="server" FieldLabel="No. Identitas Lain" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CmbINDV_Nationality1" runat="server" FieldLabel="Kewarganegaraan 1" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="CmbINDV_Nationality2" runat="server" FieldLabel="Kewarganegaraan 2" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="CmbINDV_Nationality3" runat="server" FieldLabel="Kewarganegaraan 3" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="CmbINDV_residence" runat="server" FieldLabel="Negara Domisili" AnchorHorizontal="80%" />

                                    <%--<ext:GridPanel ID="GridPanelPhoneIndividuDetailBefore" runat="server" Title="Phone Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="StorePhoneIndividuDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model11" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column35" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Width="170"></ext:Column>
                                                <ext:Column ID="Column36" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Width="90"></ext:Column>
                                                <ext:Column ID="Column37" runat="server" DataIndex="number" Text="Nomor Telepon" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneIndividuDetailBefore" runat="server" Text="Action" Flex="1">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar15" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelPhoneIndividuDetail" runat="server" Title="Phone Detail Data" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneIndividuDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="ModelDetail" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumberPhoneDetailIndividu" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="colTph_Contact_TypeIndividu" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Width="170"></ext:Column>
                                                <ext:Column ID="coltph_communication_typeIndividu" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Width="90"></ext:Column>
                                                <ext:Column ID="coltph_numberIndividu" runat="server" DataIndex="number" Text="Nomor Telepon" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneIndividuDetail" runat="server" Text="Action" Flex="1">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneIndividu">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar13" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelAddressIndividuDetailBefore" runat="server" Title="Address Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="StoreAddressIndividuDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model12" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column38" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Width="170"></ext:Column>
                                                <ext:Column ID="Column39" runat="server" DataIndex="Addres" Text="Alamat" Width="90"></ext:Column>
                                                <ext:Column ID="Column40" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Width="80"></ext:Column>
                                                <ext:Column ID="Column41" runat="server" DataIndex="CountryCode" Text="Negara" Width="100"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressIndividuDetailBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelAddressIndividuDetail" runat="server" Title="Address Detail Data" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressIndividuDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model1" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailIndividu" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="ColAddress_typeIndividu" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Width="170"></ext:Column>
                                                <ext:Column ID="ColAddressIndividu" runat="server" DataIndex="Addres" Text="Alamat" Width="90"></ext:Column>
                                                <ext:Column ID="ColCityIndividu" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Width="80"></ext:Column>
                                                <ext:Column ID="Colcountry_codeIndividu" runat="server" DataIndex="CountryCode" Text="Negara" Width="100"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressIndividuDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressIndividu">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar16" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField ID="TxtINDV_Email" runat="server" FieldLabel="Email" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email2" runat="server" FieldLabel="Email2" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email3" runat="server" FieldLabel="Email3" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email4" runat="server" FieldLabel="Email4" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email5" runat="server" FieldLabel="Email5" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Occupation" runat="server" FieldLabel="Pekerjaan" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_employer_name" runat="server" FieldLabel="Tempat Bekerja" AnchorHorizontal="80%"></ext:DisplayField>

                                    <%--<ext:GridPanel ID="GridPanelPhoneIndividualEmployerDetailBefore" runat="server" Hidden="true" Title="Phone Detail Data Employer Before" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneEmployerIndividuDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model19" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn20" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column73" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Width="170"></ext:Column>
                                                <ext:Column ID="Column74" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Width="90"></ext:Column>
                                                <ext:Column ID="Column75" runat="server" DataIndex="number" Text="Nomor Telepon" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneIndividualEmployerDetailBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneEmployerIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar17" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelPhoneIndividualEmployerDetail" runat="server" Title="Phone Detail Data Employer" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneEmployerIndividuDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model20" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn18" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column66" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Width="170"></ext:Column>
                                                <ext:Column ID="Column67" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Width="90"></ext:Column>
                                                <ext:Column ID="Column68" runat="server" DataIndex="number" Text="Nomor Telepon" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneIndividualEmployerDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneEmployerIndividu">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar18" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelAddressIndividualEmployerDetailBefore" runat="server" Hidden="true" Title="Address Detail Data Employer Before" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressEmployerIndividuDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model21" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn21" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column76" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Width="170"></ext:Column>
                                                <ext:Column ID="Column77" runat="server" DataIndex="Addres" Text="Alamat" Width="90"></ext:Column>
                                                <ext:Column ID="Column78" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Width="80"></ext:Column>
                                                <ext:Column ID="Column79" runat="server" DataIndex="CountryCode" Text="Negara" Width="100"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressIndividualEmployerDetailBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressEmployerIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar19" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelAddressIndividualEmployerDetail" runat="server" Title="Address Detail Data Employer" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressEmployerIndividuDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model29" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn19" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column69" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Width="170"></ext:Column>
                                                <ext:Column ID="Column70" runat="server" DataIndex="Addres" Text="Alamat" Width="90"></ext:Column>
                                                <ext:Column ID="Column71" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Width="80"></ext:Column>
                                                <ext:Column ID="Column72" runat="server" DataIndex="CountryCode" Text="Negara" Width="100"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressIndividualEmployerDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressEmployerIndividu">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar20" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelIdentificationIndividualDetailBefore" runat="server" Title="Identitas Detail Data Before" Hidden="true" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreIdentificationDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model22" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn22" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column80" runat="server" DataIndex="Tipe" Text="Jenis Identitas" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column81" runat="server" DataIndex="No" Text="Nomor Identitas" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column82" runat="server" DataIndex="IssueDate" Text="Tanggal Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column83" runat="server" DataIndex="ExpiryDate" Text="Tanggal Kedaluwarsa" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column84" runat="server" DataIndex="IssuedCountry" Text="Negara Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnIdentificationIndividualDetailBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdIdentificationBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar21" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelIdentificationIndividualDetail" runat="server" Title="Identitas Detail Data" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreIdentificationDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model30" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn15" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column51" runat="server" DataIndex="Tipe" Text="Jenis Identitas" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column52" runat="server" DataIndex="No" Text="Nomor Identitas" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column53" runat="server" DataIndex="IssueDate" Text="Tanggal Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column54" runat="server" DataIndex="ExpiryDate" Text="Tanggal Kedaluwarsa" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column55" runat="server" DataIndex="IssuedCountry" Text="Negara Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnIdentificationIndividualDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdIdentification">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar22" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField ID="CbINDV_Deceased" runat="server" FieldLabel="Sudah Meninggal" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="DateINDV_Deceased_Date" runat="server" Flex="1" FieldLabel="Tanggal Meninggal" Hidden="true" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Tax_Number" runat="server" FieldLabel="NPWP" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CbINDV_Tax_Reg_Number" runat="server" FieldLabel="is PEP ?" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Source_Of_Wealth" runat="server" FieldLabel="Sumber Dana" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FormPanel>

                            <ext:FormPanel runat="server" Title="WIC Add Corporate" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="WIC_Corporate">
                                <Defaults>
                                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                                </Defaults>
                                <Items>
                                    <ext:DisplayField ID="TxtCorp_Name" runat="server" FieldLabel="Nama Korporasi" AnchorHorizontal="80%" AllowBlank="false"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_Commercial_name" runat="server" FieldLabel="Nama Komersial" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CmbCorp_Incorporation_legal_form" runat="server" FieldLabel="Bentuk Korporasi" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="TxtCorp_Incorporation_number" runat="server" FieldLabel="Nomor Induk Berusaha" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_Business" runat="server" FieldLabel="Bidang Usaha" AnchorHorizontal="80%"></ext:DisplayField>

                                    <%--<ext:GridPanel ID="GridPanelPhoneCorporateDetailBefore" runat="server" Title="Phone Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="StorePhoneCorporateDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model13" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column42" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Width="170"></ext:Column>
                                                <ext:Column ID="Column43" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Width="90"></ext:Column>
                                                <ext:Column ID="Column44" runat="server" DataIndex="number" Text="Nomor Telepon" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneCorporateDetailBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneCorporateBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar23" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelPhoneCorporateDetail" runat="server" Title="Phone Detail Data" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneCorporateDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model2" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererPhoneDetailCorporate" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="colTph_Contact_TypeCorporate" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" Width="170"></ext:Column>
                                                <ext:Column ID="coltph_communication_typeCorporate" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" Width="90"></ext:Column>
                                                <ext:Column ID="coltph_numberCorporate" runat="server" DataIndex="number" Text="Nomor Telepon" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneCorporateDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneCorporate">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar24" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelAddressCorporateDetailBefore" runat="server" Title="Address Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="StoreAddressCorporateDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model14" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn13" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column45" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Width="170"></ext:Column>
                                                <ext:Column ID="Column46" runat="server" DataIndex="Addres" Text="Alamat" Width="90"></ext:Column>
                                                <ext:Column ID="Column47" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Width="80"></ext:Column>
                                                <ext:Column ID="Column48" runat="server" DataIndex="CountryCode" Text="Negara" Width="100"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressCorporateDetailBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressCorporateBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar25" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelAddressCorporateDetail" runat="server" Title="Address Detail Data" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressCorporateDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model3" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailCorporate" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="ColAddress_typeCorporate" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" Width="170"></ext:Column>
                                                <ext:Column ID="ColAddressCorporate" runat="server" DataIndex="Addres" Text="Alamat" Width="90"></ext:Column>
                                                <ext:Column ID="ColCityCorporate" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" Width="80"></ext:Column>
                                                <ext:Column ID="Colcountry_codeCorporate" runat="server" DataIndex="CountryCode" Text="Negara" Width="100"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressCorporateDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressCorporate">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar26" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField ID="TxtCorp_Email" runat="server" FieldLabel="Email Korporasi" AnchorHorizontal="80%" AllowBlank="false"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_url" runat="server" FieldLabel="Website Korporasi" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_incorporation_state" runat="server" FieldLabel="Provinsi" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CmbCorp_incorporation_country_code" runat="server" FieldLabel="Negara" AnchorHorizontal="80%" />

                                    <%--<ext:GridPanel ID="GridPanelDirector_CorpBefore" runat="server" Title="Director Corp Before" AutoScroll="true" Hidden="true">
                                        <Store>
                                            <ext:Store ID="Store_Director_CorpBefore" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Model15" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="NO_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Roles" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn14" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column49" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column50" runat="server" DataIndex="Roles" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnDirectorBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustDirectorBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.NO_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GP_Corp_Director" runat="server" Title="Director Corp" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Director_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Model_Director_Corp" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="NO_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Roles" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column31" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column32" runat="server" DataIndex="Roles" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnDirector" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustDirector">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.NO_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:DisplayField ID="CmbCorp_Role" runat="server" FieldLabel="Peran" AnchorHorizontal="80%" />--%>
                                    <ext:DisplayField ID="DateCorp_incorporation_date" runat="server" Flex="1" AllowBlank="false" FieldLabel="Tanggal Pendirian"></ext:DisplayField>
                                    <ext:DisplayField ID="chkCorp_business_closed" runat="server" FieldLabel="Tutup?"></ext:DisplayField>
                                    <ext:DisplayField ID="DateCorp_date_business_closed" runat="server" Flex="1" FieldLabel="Tanggal Tutup" Hidden="true"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_tax_number" runat="server" FieldLabel="NPWP" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_Comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="80%"></ext:DisplayField>
                                </Items>
                            </ext:FormPanel>
                        </Items>
                    </ext:FormPanel>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnSave" runat="server" Text="Save" Icon="DiskBlack">
                        <DirectEvents>
                            <Click OnEvent="BtnSave_Click">
                                <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnReject" runat="server" Text="Reject" Icon="Decline">
                        <DirectEvents>
                            <Click OnEvent="BtnReject_Click">
                                <EventMask ShowMask="true" Msg="Saving Reject Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnCancel" runat="server" Text="Cancel" Icon="PageBack">
                        <DirectEvents>
                            <Click OnEvent="BtnCancel_Click">
                                <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:Panel>
        </Items>
    </ext:Container>

    <ext:FormPanel ID="Panelconfirmation" runat="server" ClientIDMode="Static" Title="Confirmation" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
