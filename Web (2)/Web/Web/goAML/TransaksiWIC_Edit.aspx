﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="TransaksiWIC_Edit.aspx.vb" Inherits="TransaksiWIC_Edit" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="DropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:Window ID="WindowDetail" runat="server" Icon="ApplicationViewDetail" Title="Risk Issue Add" Hidden="true" MinWidth="1100" Height="550" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSetRiskIssueDetail" runat="server" Title="Details" Collapsible="true" Layout="FitLayout">
                <Items>                
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanelTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" >
                        <Items>
                                   </Items>
                        <Buttons>
                            <ext:Button ID="btnSavedetail" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSavedetail_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBackSaveDetail" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDetail_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>  
    <ext:FormPanel ID="FormPanelInput" runat="server" ButtonAlign="Center" Title="" BodyStyle="padding:10px 0px 10px 20px" AutoScroll="true">
        <Items>
            <ext:Hidden runat="server" ID="fld_sender"></ext:Hidden>
            <ext:TextField ID="txtNO_ID" runat="server" FieldLabel="ID" AnchorHorizontal="90%"></ext:TextField>       
            <ext:DateField ID="dateTransaction" runat="server" AllowBlank="false" IndicatorCls="required-text" IndicatorText="*" Type="Date" FieldLabel="Date Transaction" AnchorHorizontal="90%" Format="dd MMMM yyyy" Editable="false">
            </ext:DateField>    
                            
            <%--  <ext:TextField ID="txtRiskDescription" runat="server" LabelWidth="180" FieldLabel="Risk Description" AnchorHorizontal="90%" IndicatorText="*"></ext:TextField>
            <ext:TextField ID="txtSource" runat="server" LabelWidth="180" FieldLabel="Source" AnchorHorizontal="90%" IndicatorText="*"></ext:TextField>
            <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="70%">
                <Content>
                    <NDS:DropDownField ID="ddfRegRequirement" runat="server" LabelWidth="180" StringField="Kode_Regulatory_Requirement AS [Kode Regulatory Requirement],Regulatory_Requirement AS [Regulatory Requirement]" 
                        StringTable="dbo.WP_MS_Regulatory_Requirement" 
                        Label="Regulatory Requirement" AnchorHorizontal="100%" AllowBlank="false" IDPropertyUnik="Kode_Regulatory_Requirement" StringFieldStyle="background-color:#ffccb3;" 
                        />
                </Content>
            </ext:Panel>    
            <ext:TextField ID="txtCMRef" runat="server" LabelWidth="180" FieldLabel="CM Ref" AnchorHorizontal="90%"></ext:TextField>
            <ext:TextField ID="txtApplicableRegulation" runat="server" LabelWidth="180" FieldLabel="Applicable Regulation" AnchorHorizontal="90%"></ext:TextField>
            <ext:TextArea ID="txtControlDescription" runat="server" LabelWidth="180" FieldLabel="Control Description" AnchorHorizontal="90%" IndicatorText="*"></ext:TextArea>
            <ext:TextArea ID="txtControlDependency" runat="server" LabelWidth="180" FieldLabel="Control Dependency" AnchorHorizontal="90%" IndicatorText="*"></ext:TextArea>
            <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="70%">
                <Content>
                    <NDS:DropDownField ID="ddfControlDesignAdequacy" runat="server" LabelWidth="180" StringField="PicklistValue AS [Pick List],PicklistDescription AS [PickList Description]" 
                        StringTable="dbo.WP_MS_PicklistYesNo" 
                        Label="Control Design Adequacy" AnchorHorizontal="100%" AllowBlank="false" IDPropertyUnik="PicklistValue" StringFieldStyle="background-color:#ffccb3;" OnOnValueChanged="ddfCDA_Event" 
                        />
                </Content>
            </ext:Panel>    
            <ext:TextField ID="txtJustification" runat="server" LabelWidth="180" FieldLabel="Justification" AnchorHorizontal="90%" ReadOnly="true"></ext:TextField>
            <ext:TextField ID="txtCIM" runat="server" LabelWidth="180" FieldLabel="CIM" AnchorHorizontal="90%" ReadOnly="true"></ext:TextField>
            <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="70%">
                <Content>
                    <NDS:DropDownField ID="ddfResidualRiskRating" runat="server" LabelWidth="180" StringField="Kode_Residual_Risk_Rating AS [Kode Residual Risk Rating],Residual_Risk_Rating AS [Residual Risk Rating]" 
                        StringTable="dbo.WP_MS_Residual_Risk_Rating" 
                        Label="Residual Risk Rating" AnchorHorizontal="100%" AllowBlank="false" IDPropertyUnik="Kode_Residual_Risk_Rating" StringFieldStyle="background-color:#ffccb3;" 
                        />
                </Content>
            </ext:Panel>
            <ext:TextField ID="txtControlTestingDescription" runat="server" LabelWidth="180" FieldLabel="Control Testing Description" AnchorHorizontal="90%" IndicatorText="*"></ext:TextField>
            <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="70%">
                <Content>
                    <NDS:DropDownField ID="ddfControlFrequency" runat="server" LabelWidth="180" StringField="Kode_Control_Frequency AS [Kode Control Frequency],Control_Frequency AS [Control Frequency]" 
                        StringTable="dbo.WP_MS_Control_Frequency" 
                        Label="Control Frequency" AnchorHorizontal="100%" AllowBlank="false" IDPropertyUnik="Kode_Control_Frequency" StringFieldStyle="background-color:#ffccb3;" 
                        />
                </Content>
            </ext:Panel>  
            <ext:TextField ID="txtMinSampleSize" runat="server" LabelWidth="180" FieldLabel="Minimum Sample Size" AnchorHorizontal="90%" IndicatorText="*"></ext:TextField>
            <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="70%">
                <Content>
                    <NDS:DropDownField ID="ddfTestingFrequency" runat="server" LabelWidth="180" StringField="Kode_Testing_Frequency AS [Kode Testing Frequency],Testing_Frequency AS [Testing Frequency]" 
                        StringTable="dbo.WP_MS_Testing_Frequency" 
                        Label="Testing Frequency" AnchorHorizontal="100%" AllowBlank="false" IDPropertyUnik="Kode_Testing_Frequency" StringFieldStyle="background-color:#ffccb3;" 
                        />
                </Content>
            </ext:Panel>
            <ext:TextArea ID="txtRemark" runat="server" LabelWidth="180" FieldLabel="Remark" AnchorHorizontal="90%" IndicatorText="*"></ext:TextArea>
            <ext:TextField ID="txtEffectiveDate" FieldStyle="background-color:#D3D3D3;" runat="server" LabelWidth="180" FieldLabel="Effective Date" AnchorHorizontal="90%" ReadOnly="true"></ext:TextField>
            <ext:TextField ID="txtTipeRCSA" FieldStyle="background-color:#D3D3D3;" runat="server" LabelWidth="180" FieldLabel="Tipe RCSA" AnchorHorizontal="90%" ReadOnly="true"></ext:TextField>--%>
                                         
        </Items>
        <Buttons>
            <ext:Button ID="btnSubmit" runat="server" Icon="Disk" Text="Submit">
                <%-- <Listeners>
                    <Click Handler="if (!#{FormPanelInput}.getForm().isValid()) return false;"></Click>
                </Listeners>--%>
                <DirectEvents>
                    <Click OnEvent="BtnSubmit_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnBack" runat="server" Icon="Cancel" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnBack_Click">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>

        </Items>

        <Buttons>

            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
