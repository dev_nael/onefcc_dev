﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GridEntityIdentification.ascx.vb" Inherits="goAML_WIC_Component_GridEntityIdentification" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<%-- Entity Identification --%>
<ext:GridPanel ID="GridPanel" runat="server" Title="Entity Identification" AutoScroll="true">
    <TopBar>
        <ext:Toolbar ID="tbr_ent_identify" runat="server">
            <Items>
                <ext:Button ID="btnAdd" runat="server" Text="Add Entity Identification" Icon="Add">
                    <DirectEvents>
                        <Click OnEvent="btnAdd_DirectClick">
                            <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Items>
        </ext:Toolbar>
    </TopBar>
    <Store>
        <ext:Store ID="Store" runat="server" IsPagingStore="true" PageSize="8">
            <Model>
                <ext:Model ID="mdl_ent_identify" runat="server">
                    <%--to do--%>
                    <Fields>
                        <ext:ModelField Name="PK_goAML_Ref_Customer_Entity_Identification_ID" Type="String"></ext:ModelField>
                        <ext:ModelField Name="TYPE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="NUMBER" Type="String"></ext:ModelField>
                        <ext:ModelField Name="ISSUE_DATE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="EXPIRY_DATE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="ISSUED_BY" Type="String"></ext:ModelField>
                        <ext:ModelField Name="ISSUE_COUNTRY" Type="String"></ext:ModelField>
                        <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:RowNumbererColumn ID="rnc_identify" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
            <ext:CommandColumn ID="CC_CORP_EntityIdentification" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                    <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                    <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Identification_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Identification_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:Column ID="clm_identify_type" runat="server" DataIndex="TYPE" Text="Entity Identifier Type" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_identify_number" runat="server" DataIndex="NUMBER" Text="Number" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_identify_issued_date" runat="server" DataIndex="ISSUE_DATE" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_identify_expiry_date" runat="server" DataIndex="EXPIRY_DATE" Text="Expiry Date" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_identify_issued_by" runat="server" DataIndex="ISSUED_BY" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_identify_issued_country" runat="server" DataIndex="ISSUE_COUNTRY" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_identify_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
            
        </Columns>
    </ColumnModel>
    <BottomBar>
        <ext:PagingToolbar ID="pt_identify" runat="server" HideRefresh="True" />
    </BottomBar>
</ext:GridPanel>
<%-- End of Entity Identification --%>

<ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
    <Items>
        <ext:FieldSet ID="FieldSet13" runat="server" Title="Entity Identification" Collapsible="true" Layout="FitLayout">
            <Items>
            </Items>
            <Content>
                <ext:FormPanel ID="FormPanel" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                    <Items>
                        <ext:Panel ID="Panel_cmb_ef_type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                        <Content>
		                        <nds:NDSDropDownField ID="ascx_cmb_ef_type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Identifier_Type" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Identifier Type" AnchorHorizontal="70%" AllowBlank="false" />
	                        </Content>
                        </ext:Panel>
                        <ext:TextField ID="txt_ef_number" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Identification Number" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                        <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Issued Date" ID="df_issue_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                        <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Expiry Date" ID="df_expiry_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                        <ext:TextField ID="txt_ef_issued_by" LabelWidth="250" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                        <ext:Panel ID="pnl_ef_issue_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                        <Content>
		                        <nds:NDSDropDownField ID="ascx_cmb_ef_issue_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Issued Country" AnchorHorizontal="70%" AllowBlank="false" />
	                        </Content>
                        </ext:Panel>
                        <ext:TextField ID="txt_ef_comments" LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="50"></ext:TextField>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnSave" runat="server" Text="Save" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnSave_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button ID="btnBack" runat="server" Text="Back" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnBack_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>

                    </Buttons>
                </ext:FormPanel>
            </Content>
        </ext:FieldSet>
    </Items>
</ext:Window>
