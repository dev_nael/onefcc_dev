﻿Imports System.Reflection.Emit
Imports GoAMLBLL
Imports GoAMLBLL.goAML.DataModel
Imports GoAMLBLL.goAML_CustomerDataBLL

Partial Class goAML_WIC_Component_GridSanction
    Inherits System.Web.UI.UserControl
    Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction)

    Private Const GRID_TITLE As String = "Sanction"
    Private Const SESSION_PREFIX As String = "CUSTOMER.GridSanction."

    Private _uniqueName As String
    Public Property UniqueName As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).UniqueName
        Get
            'Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME)
            Return _uniqueName
        End Get
        Set(value As String)
            'Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME) = value
            _uniqueName = value
        End Set
    End Property

    Public Property FK_REF_DETAIL_OF As Integer Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).FK_REF_DETAIL_OF
        Get
            Return Session(SessionPrefix & "FK_REF_DETAIL_OF-" & UniqueName)
        End Get
        Set(value As Integer)
            Session(SessionPrefix & "FK_REF_DETAIL_OF-" & UniqueName) = value
        End Set
    End Property

    Public Property CIF As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).CIF
        Get
            Return Session(SessionPrefix & "CIF-" & UniqueName)
        End Get
        Set(ByVal value As String)
            Session(SessionPrefix & "CIF-" & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_Edit As goAML_Ref_Customer_Sanction Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).objTemp_Edit
        Get
            Return Session(SessionPrefix & "objTempCustomer_Sanction_Edit-" & UniqueName)
        End Get
        Set(ByVal value As goAML_Ref_Customer_Sanction)
            Session(SessionPrefix & "objTempCustomer_Sanction_Edit-" & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref As goAML_Ref_Customer_Sanction Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).objTemp_goAML_Ref
        Get
            Return Session(SessionPrefix & "objTemp_goAML_Ref_Customer_Sanction-" & UniqueName)
        End Get
        Set(ByVal value As goAML_Ref_Customer_Sanction)
            Session(SessionPrefix & "objTemp_goAML_Ref_Customer_Sanction-" & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_Ref As List(Of goAML_Ref_Customer_Sanction) Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).objListgoAML_Ref
        Get
            Return Session(SessionPrefix & "objListgoAML_Ref_Sanction-" & IIf(UniqueName Is Nothing, "", UniqueName))
        End Get
        Set(ByVal value As List(Of goAML_Ref_Customer_Sanction))
            Session(SessionPrefix & "objListgoAML_Ref_Sanction-" & IIf(UniqueName Is Nothing, "", UniqueName)) = value
        End Set
    End Property

    Public Property objListgoAML_vw As List(Of goAML_Ref_Customer_Sanction) Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).objListgoAML_vw
        Get
            Return Session(SessionPrefix & "objListgoAML_vw_Sanction-" & IIf(UniqueName Is Nothing, "", UniqueName))
        End Get
        Set(ByVal value As List(Of goAML_Ref_Customer_Sanction))
            Session(SessionPrefix & "objListgoAML_vw_Sanction-" & IIf(UniqueName Is Nothing, "", UniqueName)) = value
        End Set
    End Property

    Private _title As String
    Public Property Title As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).Title
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Public ReadOnly Property SessionPrefix As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).SessionPrefix
        Get
            Return SESSION_PREFIX
        End Get
    End Property

    Public Property IsViewMode As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).IsViewMode
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName)
        End Get
        Set(ByVal value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName) = value
        End Set
    End Property

    Public Property IsDataChange As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).IsDataChange
        Get
            If Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) Is Nothing Then
                Return False
            End If
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName)
        End Get
        Set(value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) = value
        End Set
    End Property

    Private Shadows Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If IsViewMode Then
        '    Dim cmdColumn As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        '    If cmdColumn IsNot Nothing Then
        '        Dim cmd1 As GridCommand = cmdColumn.Commands(1)
        '        Dim cmd2 As GridCommand = cmdColumn.Commands(2)
        '        cmd1.Hidden = IsViewMode
        '        cmd2.Hidden = IsViewMode
        '    End If

        '    btnAdd.Hidden = IsViewMode
        'End If
    End Sub
    Private Shadows Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Dim cmdColumnEdit As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        Dim cmdColumnDetail As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(2), CommandColumn)
        cmdColumnEdit.Hidden = IsViewMode
        cmdColumnDetail.Hidden = Not IsViewMode

        'Dim store As Store = TryCast(GridPanel.Store(0), Store)
        'store.DataBind()

        btnAdd.Hidden = IsViewMode
    End Sub

    Public Sub btnAdd_DirectClick(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).btnAdd_DirectClick
        Try
            objTemp_Edit = Nothing
            objTemp_goAML_Ref = Nothing

            WindowDetail.Hidden = False
            FormPanel.Hidden = False
            btnSave.Hidden = False

            txt_sanction_provider.ReadOnly = False
            txt_sanction_sanction_list_name.ReadOnly = False
            txt_sanction_match_criteria.ReadOnly = False
            txt_sanction_link_to_source.ReadOnly = False
            txt_sanction_sanction_list_attributes.ReadOnly = False
            'df_sanction_valid_from.ReadOnly = False
            'cbx_sanction_is_approx_from_date.ReadOnly = False
            'df_sanction_valid_to.ReadOnly = False
            'cbx_sanction_is_approx_to_date.ReadOnly = False
            txt_sanction_comments.ReadOnly = False

            WindowDetail.Title = "Sanction Add"

            objTemp_Edit = Nothing
            'FormPanel_Sanction.Reset()
            Clearinput()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnSave_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).btnSave_DirectEvent
        Try
            If IsData_Valid() Then
                If objTemp_Edit Is Nothing Then
                    Save()
                Else
                    Save(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnBack_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).btnBack_DirectEvent
        Try
            WindowDetail.Hidden = True
            FormPanel.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub GrdCmd(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).GrdCmd
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub LoadData(id As Long, isEdit As Boolean) Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).LoadData
        Try
            objTemp_goAML_Ref = objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id)
            objTemp_Edit = objListgoAML_vw.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id)


            If Not objTemp_Edit Is Nothing Then
                FormPanel.Hidden = False
                WindowDetail.Hidden = False
                btnSave.Hidden = Not isEdit

                txt_sanction_provider.ReadOnly = Not isEdit
                txt_sanction_sanction_list_name.ReadOnly = Not isEdit
                txt_sanction_match_criteria.ReadOnly = Not isEdit
                txt_sanction_link_to_source.ReadOnly = Not isEdit
                txt_sanction_sanction_list_attributes.ReadOnly = Not isEdit
                'df_sanction_valid_from.ReadOnly = Not isEdit
                'cbx_sanction_is_approx_from_date.ReadOnly = Not isEdit
                'df_sanction_valid_to.ReadOnly = Not isEdit
                'cbx_sanction_is_approx_to_date.ReadOnly = Not isEdit
                txt_sanction_comments.ReadOnly = Not isEdit


                With objTemp_Edit
                    txt_sanction_provider.Value = .PROVIDER
                    txt_sanction_sanction_list_name.Value = .SANCTION_LIST_NAME
                    txt_sanction_match_criteria.Value = .MATCH_CRITERIA
                    txt_sanction_link_to_source.Value = .LINK_TO_SOURCE
                    txt_sanction_sanction_list_attributes.Value = .SANCTION_LIST_ATTRIBUTES
                    'df_sanction_valid_from.Text = .SANCTION_LIST_DATE_RANGE_VALID_FROM
                    'cbx_sanction_is_approx_from_date.Checked = .SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_sanction_valid_to.Text = .SANCTION_LIST_DATE_RANGE_VALID_TO
                    'cbx_sanction_is_approx_to_date.Checked = .SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_sanction_comments.Value = .COMMENTS
                End With

                WindowDetail.Title = Title & " " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub DeleteRecord(id As Long) Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).DeleteRecord
        objListgoAML_vw.Remove(objListgoAML_vw.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id))
        objListgoAML_Ref.Remove(objListgoAML_Ref.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = id))

        Store.DataSource = objListgoAML_vw.ToList()
        Store.DataBind()

        Store.DataSource = objListgoAML_vw.ToList()
        Store.DataBind()
    End Sub

    Public Sub Clearinput() Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).Clearinput
        txt_sanction_provider.Text = ""
        txt_sanction_sanction_list_name.Text = ""
        txt_sanction_match_criteria.Text = ""
        txt_sanction_link_to_source.Text = ""
        txt_sanction_sanction_list_attributes.Text = ""
        'df_sanction_valid_from.Text = ""
        'cbx_sanction_is_approx_from_date.Checked = False
        'df_sanction_valid_to.Text = ""
        'cbx_sanction_is_approx_to_date.Checked = False
        txt_sanction_comments.Text = ""
    End Sub

    Public Sub Save(Optional isNew As Boolean = True) Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).Save
        Try
            If Not isNew Then
                With objTemp_Edit
                    .FK_REF_DETAIL_OF = FK_REF_DETAIL_OF
                    .CIF = CIF
                    .PROVIDER = txt_sanction_provider.Value
                    .SANCTION_LIST_NAME = txt_sanction_sanction_list_name.Value
                    .MATCH_CRITERIA = txt_sanction_match_criteria.Value
                    .LINK_TO_SOURCE = txt_sanction_link_to_source.Value
                    .SANCTION_LIST_ATTRIBUTES = txt_sanction_sanction_list_attributes.Value
                    '.SANCTION_LIST_DATE_RANGE_VALID_FROM = df_sanction_valid_from.Value
                    '.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_sanction_is_approx_from_date.Value
                    '.SANCTION_LIST_DATE_RANGE_VALID_TO = df_sanction_valid_to.Value
                    '.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = cbx_sanction_is_approx_to_date.Value
                    .COMMENTS = txt_sanction_comments.Value
                End With

                With objTemp_goAML_Ref
                    .FK_REF_DETAIL_OF = FK_REF_DETAIL_OF
                    .CIF = CIF
                    .PROVIDER = txt_sanction_provider.Value
                    .SANCTION_LIST_NAME = txt_sanction_sanction_list_name.Value
                    .MATCH_CRITERIA = txt_sanction_match_criteria.Value
                    .LINK_TO_SOURCE = txt_sanction_link_to_source.Value
                    .SANCTION_LIST_ATTRIBUTES = txt_sanction_sanction_list_attributes.Value
                    '.SANCTION_LIST_DATE_RANGE_VALID_FROM = df_sanction_valid_from.Value
                    '.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_sanction_is_approx_from_date.Value
                    '.SANCTION_LIST_DATE_RANGE_VALID_TO = df_sanction_valid_to.Value
                    '.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = cbx_sanction_is_approx_to_date.Value
                    .COMMENTS = txt_sanction_comments.Value
                End With
            Else

                Dim objNewgoAML_Sanction As goAML_Ref_Customer_Sanction = New goAML_Ref_Customer_Sanction
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_Sanction_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewgoAML_Sanction
                    .PK_goAML_Ref_Customer_Sanction_ID = intpk
                    .FK_REF_DETAIL_OF = FK_REF_DETAIL_OF
                    .CIF = CIF
                    .PROVIDER = txt_sanction_provider.Value
                    .SANCTION_LIST_NAME = txt_sanction_sanction_list_name.Value
                    .MATCH_CRITERIA = txt_sanction_match_criteria.Value
                    .LINK_TO_SOURCE = txt_sanction_link_to_source.Value
                    .SANCTION_LIST_ATTRIBUTES = txt_sanction_sanction_list_attributes.Value
                    '.SANCTION_LIST_DATE_RANGE_VALID_FROM = df_sanction_valid_from.Value
                    '.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_sanction_is_approx_from_date.Value
                    '.SANCTION_LIST_DATE_RANGE_VALID_TO = df_sanction_valid_to.Value
                    '.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = cbx_sanction_is_approx_to_date.Value
                    .COMMENTS = txt_sanction_comments.Value
                End With

                objListgoAML_vw.Add(objNewgoAML_Sanction)
                objListgoAML_Ref = objListgoAML_vw
            End If

            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()

            WindowDetail.Hidden = True
            'ClearinputCustomerPreviousName()
            IsDataChange = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Function IsData_Valid() As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).IsData_Valid
        'If CIF Is Nothing Then
        '    Throw New Exception("CIF is required")
        'End If

        If txt_sanction_provider.Text.Trim = "" Then
            Throw New Exception("Provider is required")
        End If
        If txt_sanction_sanction_list_name.Text.Trim = "" Then
            Throw New Exception("Sanction List Name is required")
        End If
        'If txt_sanction_match_criteria.Text.Trim = "" Then
        '    Throw New Exception("Match Criteria is required")
        'End If
        'If txt_sanction_link_to_source.Text.Trim = "" Then
        '    Throw New Exception("Link To Source is required")
        'End If
        'If txt_sanction_sanction_list_attributes.Text.Trim = "" Then
        '    Throw New Exception("Sanction List Attributes is required")
        'End If
        'If df_sanction_valid_from.RawValue Is Nothing Then
        '    Throw New Exception("Valid From is required")
        'End If
        'If df_sanction_valid_to.RawValue Is Nothing Then
        '    Throw New Exception("Valid To is required")
        'End If
        'If txt_sanction_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If

        Return True
    End Function

    Public Sub LoadData(data As List(Of goAML_Ref_Customer_Sanction)) Implements IGoAML_Grid(Of goAML_Ref_Customer_Sanction).LoadData
        objListgoAML_Ref = data.Select(Function(x) x).ToList()
        objListgoAML_vw = data.Select(Function(x) x).ToList()

        If objListgoAML_vw IsNot Nothing Then
            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()
        Else
            Store.DataBind()
        End If
    End Sub
End Class
