﻿
Imports GoAMLBLL
Imports GoAMLBLL.goAML.DataModel
Imports GoAMLBLL.Helper

Partial Class goAML_WIC_Component_GridRelatedEntity
    Inherits System.Web.UI.UserControl
    Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities)

    Private Const GRID_TITLE As String = "Related Entity"
    Private Const SESSION_PREFIX As String = "CUSTOMER.GridRelatedEntity."

    Private _uniqueName As String
    Public Property UniqueName As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).UniqueName
        Get
            'Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME)
            Return _uniqueName
        End Get
        Set(value As String)
            'Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME) = value
            GridEmail_RelatedEntity.UniqueName = value
            GridEntityIdentification_RelatedEntity.UniqueName = value
            GridURL_RelatedEntity.UniqueName = value
            GridSanction_RelatedEntity.UniqueName = value
            _uniqueName = value
        End Set
    End Property

    Private _fk_ref_detail_of As Integer
    Public Property FK_REF_DETAIL_OF As Integer Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).FK_REF_DETAIL_OF
        Get
            Return _fk_ref_detail_of
        End Get
        Set(value As Integer)
            _fk_ref_detail_of = value
        End Set
    End Property

    Public Property CIF As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).CIF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName)
        End Get
        Set(value As String)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_Edit As goAML_Ref_Customer_Related_Entities Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).objTemp_Edit
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName)
        End Get
        Set(value As goAML_Ref_Customer_Related_Entities)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref As goAML_Ref_Customer_Related_Entities Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).objTemp_goAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName)
        End Get
        Set(value As goAML_Ref_Customer_Related_Entities)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_Ref As List(Of goAML_Ref_Customer_Related_Entities) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).objListgoAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Customer_Related_Entities))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_vw As List(Of goAML_Ref_Customer_Related_Entities) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).objListgoAML_vw
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Customer_Related_Entities))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName) = value
        End Set
    End Property

    Private _title As String
    Public Property Title As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).Title
        Get
            Return _title
        End Get
        Set(value As String)
            _title = value
        End Set
    End Property

    Public ReadOnly Property SessionPrefix As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).SessionPrefix
        Get
            Return SESSION_PREFIX
        End Get
    End Property

    Public Property IsViewMode As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).IsViewMode
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName)
        End Get
        Set(value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName) = value
        End Set
    End Property

    Public Property IsDataChange As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).IsDataChange
        Get
            If Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) Is Nothing Then
                Return False
            End If
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName)
        End Get
        Set(value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) = value
        End Set
    End Property

    Private Shadows Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If IsViewMode Then
        '    Dim cmdColumn As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        '    If cmdColumn IsNot Nothing Then
        '        Dim cmd1 As GridCommand = cmdColumn.Commands(1)
        '        Dim cmd2 As GridCommand = cmdColumn.Commands(2)
        '        cmd1.Hidden = IsViewMode
        '        cmd2.Hidden = IsViewMode
        '    End If

        '    btnAdd.Hidden = IsViewMode

        '    GridEmail_RelatedEntity.IsViewMode = IsViewMode
        '    GridEntityIdentification_RelatedEntity.IsViewMode = IsViewMode
        '    GridURL_RelatedEntity.IsViewMode = IsViewMode
        '    GridSanction_RelatedEntity.IsViewMode = IsViewMode
        'End If
    End Sub
    Private Shadows Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Dim cmdColumnEdit As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        Dim cmdColumnDetail As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(2), CommandColumn)
        cmdColumnEdit.Hidden = IsViewMode
        cmdColumnDetail.Hidden = Not IsViewMode

        'Dim store As Store = TryCast(GridPanel.Store(0), Store)
        'store.DataBind()

        btnAdd.Hidden = IsViewMode

        GridEmail_RelatedEntity.IsViewMode = IsViewMode
        GridEntityIdentification_RelatedEntity.IsViewMode = IsViewMode
        GridURL_RelatedEntity.IsViewMode = IsViewMode
        GridSanction_RelatedEntity.IsViewMode = IsViewMode
    End Sub

    Public Sub btnAdd_DirectClick(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).btnAdd_DirectClick
        Try
            WindowDetail.Hidden = False
            FormPanel.Hidden = False
            btnSave.Hidden = False

            cmb_re_entity_relation.IsReadOnly = False
            txt_re_comments.ReadOnly = False
            txt_re_name.ReadOnly = False
            txt_re_commercial_name.ReadOnly = False
            cmb_re_incorporation_legal_form.IsReadOnly = False
            txt_re_incorporation_number.ReadOnly = False
            txt_re_business.ReadOnly = False
            txt_re_incorporation_state.ReadOnly = False
            cmb_re_incorporation_country_code.IsReadOnly = False
            df_re_incorporation_date.ReadOnly = False
            cbx_re_business_closed.ReadOnly = False
            df_re_date_business_closed.ReadOnly = False
            txt_re_tax_number.ReadOnly = False
            txt_re_tax_reg_number.ReadOnly = False

            WindowDetail.Title = "Related Entity Add"
            objTemp_Edit = Nothing
            Clearinput()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnSave_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).btnSave_DirectEvent
        Try
            If IsData_Valid() Then
                If objTemp_Edit Is Nothing Then
                    Save()
                Else
                    Save(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnBack_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).btnBack_DirectEvent
        Try
            WindowDetail.Hidden = True
            FormPanel.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub GrdCmd(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).GrdCmd
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub LoadData(id As Long, isEdit As Boolean) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).LoadData
        Try
            objTemp_goAML_Ref = objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id)
            objTemp_Edit = objListgoAML_vw.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id)

            If Not objTemp_Edit Is Nothing Then
                FormPanel.Hidden = False
                WindowDetail.Hidden = False
                btnSave.Hidden = Not isEdit

                cmb_re_entity_relation.IsReadOnly = Not isEdit
                txt_re_comments.ReadOnly = Not isEdit
                txt_re_name.ReadOnly = Not isEdit
                txt_re_commercial_name.ReadOnly = Not isEdit
                cmb_re_incorporation_legal_form.IsReadOnly = Not isEdit
                txt_re_incorporation_number.ReadOnly = Not isEdit
                txt_re_business.ReadOnly = Not isEdit
                txt_re_incorporation_state.ReadOnly = Not isEdit
                cmb_re_incorporation_country_code.IsReadOnly = Not isEdit
                df_re_incorporation_date.ReadOnly = Not isEdit
                cbx_re_business_closed.ReadOnly = Not isEdit
                df_re_date_business_closed.ReadOnly = Not isEdit
                txt_re_tax_number.ReadOnly = Not isEdit
                txt_re_tax_reg_number.ReadOnly = Not isEdit


                With objTemp_Edit
                    Dim refRelation = GoAMLBLL.Helper.CommonHelper.GetReferenceByCode("goAML_Ref_Person_Person_Relationship", .ENTITY_ENTITY_RELATION)
                    Dim refLegalForm = GoAMLBLL.Helper.CommonHelper.GetReferenceByCode("goAML_Ref_Bentuk_Badan_Usaha", .INCORPORATION_LEGAL_FORM)
                    Dim refEntityStatus = GoAMLBLL.Helper.CommonHelper.GetReferenceByCode("goAML_Ref_Entity_Status", .ENTITY_STATUS)
                    Dim refCountry = GoAMLBLL.Helper.CommonHelper.GetReferenceByCode("goAML_Ref_Nama_Negara", .INCORPORATION_COUNTRY_CODE)

                    cmb_re_entity_relation.SetTextWithTextValue(refRelation.Kode, refRelation.Keterangan)
                    txt_re_comments.Value = .COMMENTS
                    txt_re_name.Value = .NAME
                    txt_re_commercial_name.Value = .COMMERCIAL_NAME
                    cmb_re_incorporation_legal_form.SetTextWithTextValue(refLegalForm.Kode, refLegalForm.Keterangan)
                    txt_re_incorporation_number.Value = .INCORPORATION_NUMBER
                    txt_re_business.Value = .BUSINESS
                    txt_re_incorporation_state.Value = .INCORPORATION_STATE
                    cmb_re_incorporation_country_code.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                    df_re_incorporation_date.Text = .INCORPORATION_DATE
                    cbx_re_business_closed.Checked = .BUSINESS_CLOSED
                    df_re_date_business_closed.Text = .DATE_BUSINESS_CLOSED
                    txt_re_tax_number.Value = .TAX_NUMBER
                    txt_re_tax_reg_number.Value = .TAX_REG_NUMBER

                    GridEmail_RelatedEntity.LoadData(objTemp_Edit.ObjList_GoAML_Ref_Customer_Email)
                    GridEntityIdentification_RelatedEntity.LoadData(objTemp_Edit.ObjList_GoAML_Ref_Customer_Entity_Identification)
                    GridURL_RelatedEntity.LoadData(objTemp_Edit.ObjList_GoAML_Ref_Customer_URL)
                    GridSanction_RelatedEntity.LoadData(objTemp_Edit.ObjList_GoAML_Ref_Customer_Sanction)
                End With

                WindowDetail.Title = Title & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub LoadData(data As List(Of goAML_Ref_Customer_Related_Entities)) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).LoadData
        objListgoAML_Ref = data.Select(Function(x) x).ToList()
        objListgoAML_vw = data.Select(Function(x) x).ToList()

        If objListgoAML_vw IsNot Nothing Then
            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()
        Else
            Store.DataBind()
        End If
    End Sub

    Public Sub DeleteRecord(id As Long) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).DeleteRecord
        objListgoAML_vw.Remove(objListgoAML_vw.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id))
        objListgoAML_Ref.Remove(objListgoAML_Ref.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = id))

        Store.DataSource = objListgoAML_vw.ToList()
        Store.DataBind()
    End Sub

    Public Sub Clearinput() Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).Clearinput
        cmb_re_entity_relation.SetTextValue("")
        txt_re_comments.Text = ""
        txt_re_name.Text = ""
        txt_re_commercial_name.Text = ""
        cmb_re_incorporation_legal_form.SetTextValue("")
        txt_re_incorporation_number.Text = ""
        txt_re_business.Text = ""
        txt_re_incorporation_state.Text = ""
        cmb_re_incorporation_country_code.SetTextValue("")
        df_re_incorporation_date.Text = ""
        cbx_re_business_closed.Checked = False
        df_re_date_business_closed.Text = ""
        txt_re_tax_number.Text = ""
        txt_re_tax_reg_number.Text = ""
    End Sub

    Public Sub Save(Optional isNew As Boolean = True) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).Save
        Try
            If Not isNew Then
                With objTemp_Edit
                    .CIF = CIF
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    .COMMENTS = txt_re_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    .INCORPORATION_DATE = df_re_incorporation_date.Value
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value
                End With

                With objTemp_goAML_Ref
                    .CIF = CIF
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    .COMMENTS = txt_re_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    .INCORPORATION_DATE = df_re_incorporation_date.Value
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value
                End With

                objTemp_Edit = Nothing
                objTemp_goAML_Ref = Nothing
            Else
                Dim objNewVWgoAML_RelatedEntities As New goAML_Ref_Customer_Related_Entities
                Dim objNewgoAML_RelatedEntities As New goAML_Ref_Customer_Related_Entities
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Entities_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_RelatedEntities
                    .PK_goAML_Ref_Customer_Related_Entities_ID = intpk
                    .CIF = CIF
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    .COMMENTS = txt_re_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    .INCORPORATION_DATE = df_re_incorporation_date.Value
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value
                End With

                With objNewgoAML_RelatedEntities
                    .PK_goAML_Ref_Customer_Related_Entities_ID = intpk
                    .CIF = CIF
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    .COMMENTS = txt_re_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    .INCORPORATION_DATE = df_re_incorporation_date.Value
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value
                End With

                objListgoAML_vw.Add(objNewVWgoAML_RelatedEntities)
                objListgoAML_Ref = objListgoAML_vw
            End If

            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()

            WindowDetail.Hidden = True
            IsDataChange = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Function IsData_Valid() As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Entities).IsData_Valid
        'If CIF.Trim = "" Then
        '    Throw New Exception("CIF is required")
        'End If

        If cmb_re_entity_relation.SelectedItemText Is Nothing Then
            Throw New Exception("Entity Relation is required")
        End If
        If txt_re_comments.Text.Trim = "" Then
            Throw New Exception("Comments is required")
        End If
        If txt_re_name.Text.Trim = "" Then
            Throw New Exception("Name is required")
        End If
        If txt_re_commercial_name.Text.Trim = "" Then
            Throw New Exception("Commercial Name is required")
        End If
        If cmb_re_incorporation_legal_form.SelectedItemText Is Nothing Then
            Throw New Exception("Incorporation Legal is required")
        End If
        If txt_re_incorporation_number.Text.Trim = "" Then
            Throw New Exception("Incorporation Number is required")
        End If
        If txt_re_business.Text.Trim = "" Then
            Throw New Exception("Business is required")
        End If
        If txt_re_incorporation_state.Text.Trim = "" Then
            Throw New Exception("Incorporation State is required")
        End If
        If Not CommonHelper.IsFieldValid(txt_re_incorporation_state.Text.Trim, "incorporation_state") Then
            Throw New Exception("Incorporation State format is not valid")
        End If
        If cmb_re_incorporation_country_code.SelectedItemText Is Nothing Then
            Throw New Exception("Incorporation Country Code is required")
        End If
        If df_re_incorporation_date.RawValue Is Nothing Then
            Throw New Exception("Incorporation Date is required")
        End If
        If df_re_date_business_closed.RawValue Is Nothing Then
            Throw New Exception("Business Closed is required")
        End If
        If txt_re_tax_number.Text.Trim = "" Then
            Throw New Exception("Tax Number is required")
        End If
        If txt_re_tax_reg_number.Text.Trim = "" Then
            Throw New Exception("Tax Reg Number is required")
        End If

        Return True
    End Function
End Class
