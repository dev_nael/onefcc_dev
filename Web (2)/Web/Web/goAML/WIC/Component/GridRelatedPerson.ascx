﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GridRelatedPerson.ascx.vb" Inherits="goAML_WIC_Component_GridRelatedPerson" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<%@ Register Src="~/goAML/WIC/Component/GridAddress.ascx" TagPrefix="goAML" TagName="GridAddress" %>
<%@ Register Src="~/goAML/WIC/Component/GridAddressWork.ascx" TagPrefix="goAML" TagName="GridAddressWork" %>
<%@ Register Src="~/goAML/WIC/Component/GridEmail.ascx" TagPrefix="goAML" TagName="GridEmail" %>
<%@ Register Src="~/goAML/WIC/Component/GridIdentification.ascx" TagPrefix="goAML" TagName="GridIdentification" %>
<%@ Register Src="~/goAML/WIC/Component/GridPEP.ascx" TagPrefix="goAML" TagName="GridPEP" %>
<%@ Register Src="~/goAML/WIC/Component/GridPhone.ascx" TagPrefix="goAML" TagName="GridPhone" %>
<%@ Register Src="~/goAML/WIC/Component/GridSanction.ascx" TagPrefix="goAML" TagName="GridSanction" %>
<%@ Register Src="~/goAML/WIC/Component/GridEntityIdentification.ascx" TagPrefix="goAML" TagName="GridEntityIdentification" %>
<%@ Register Src="~/goAML/WIC/Component/GridURL.ascx" TagPrefix="goAML" TagName="GridURL" %>

<%-- INDV Related Person, Add by Septian--%>
<ext:GridPanel ID="GridPanel"
    runat="server" Title="Related Person" AutoScroll="true">
    <TopBar>
        <ext:Toolbar ID="tbr_related_person" runat="server">
            <Items>
                <ext:Button ID="btnAdd" runat="server" Text="Add Related Person" Icon="Add">
                    <DirectEvents>
                        <Click OnEvent="btnAdd_DirectClick">
                            <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Items>
        </ext:Toolbar>
    </TopBar>
    <Store>
        <ext:Store ID="Store" runat="server" IsPagingStore="true" PageSize="8">
            <Model>
                <ext:Model ID="mdl_related_person" runat="server">
                    <Fields>
                        <ext:ModelField Name="PK_goAML_Ref_Customer_Related_Person_ID" Type="String"></ext:ModelField>
                        <ext:ModelField Name="PERSON_PERSON_RELATION" Type="String"></ext:ModelField>
                        <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                        <ext:ModelField Name="GENDER" Type="String"></ext:ModelField>
                        <ext:ModelField Name="TITLE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="FIRST_NAME" Type="String"></ext:ModelField>
                        <ext:ModelField Name="MIDDLE_NAME" Type="String"></ext:ModelField>
                        <ext:ModelField Name="PREFIX" Type="String"></ext:ModelField>
                        <ext:ModelField Name="LAST_NAME" Type="String"></ext:ModelField>
                        <ext:ModelField Name="BIRTH_DATE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="BIRTH_PLACE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="COUNTRY_OF_BIRTH" Type="String"></ext:ModelField>
                        <ext:ModelField Name="MOTHERS_NAME" Type="String"></ext:ModelField>
                        <ext:ModelField Name="ALIAS" Type="String"></ext:ModelField>
                        <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                        <ext:ModelField Name="PASSPORT_NUMBER" Type="String"></ext:ModelField>
                        <ext:ModelField Name="PASSPORT_COUNTRY" Type="String"></ext:ModelField>
                        <ext:ModelField Name="ID_NUMBER" Type="String"></ext:ModelField>
                        <ext:ModelField Name="NATIONALITY1" Type="String"></ext:ModelField>
                        <ext:ModelField Name="NATIONALITY2" Type="String"></ext:ModelField>
                        <ext:ModelField Name="NATIONALITY3" Type="String"></ext:ModelField>
                        <ext:ModelField Name="RESIDENCE" Type="String"></ext:ModelField>
                        <ext:ModelField Name="OCCUPATION" Type="String"></ext:ModelField>
                        <ext:ModelField Name="DECEASED" Type="String"></ext:ModelField>
                        <ext:ModelField Name="DATE_DECEASED" Type="String"></ext:ModelField>
                        <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                        <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                        <ext:ModelField Name="SOURCE_OF_WEALTH" Type="String"></ext:ModelField>
                        <ext:ModelField Name="IS_PROTECTED" Type="String"></ext:ModelField>
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:RowNumbererColumn ID="rnc_related_person" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
            <ext:CommandColumn ID="CC_INDV_RelatedPerson" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                    <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                    <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Person_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Person_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:Column ID="clm_rp_person_relation" runat="server" DataIndex="PERSON_PERSON_RELATION" Text="Person Person Relation" MinWidth="130" Flex="1"></ext:Column>
            <%--<ext:Column ID="clm_rp_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_gender" runat="server" DataIndex="GENDER" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_title" runat="server" DataIndex="TITLE" Text="Title" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_first_name" runat="server" DataIndex="FIRST_NAME" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_middle_name" runat="server" DataIndex="MIDDLE_NAME" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_prefix" runat="server" DataIndex="PREFIX" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>--%>
            <ext:Column ID="clm_rp_last_name" runat="server" DataIndex="LAST_NAME" Text="Full Name" MinWidth="130" Flex="1"></ext:Column>
           <%-- <ext:Column ID="clm_rp_birth_date" runat="server" DataIndex="BIRTH_DATE" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_birth_place" runat="server" DataIndex="BIRTH_PLACE" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_country_of_birth" runat="server" DataIndex="COUNTRY_OF_BIRTH" Text="Country of Birth" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_mother_name" runat="server" DataIndex="MOTHERS_NAME" Text="Mother name" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_alias" runat="server" DataIndex="ALIAS" Text="Alias" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_ssn" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_passport_number" runat="server" DataIndex="PASSPORT_NUMBER" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_passport_country" runat="server" DataIndex="PASSPORT_COUNTRY" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_id_number" runat="server" DataIndex="ID_NUMBER" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>--%>
            <%-- 2023-10-11, Nael: Hide kolom nationality1 --%>
            <ext:Column ID="clm_rp_nationality1" runat="server" DataIndex="NATIONALITY1" Text="Nationality 1" Hidden="true" MinWidth="130" Flex="1"></ext:Column>
            <%--<ext:Column ID="clm_rp_nationality2" runat="server" DataIndex="NATIONALITY2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_nationality3" runat="server" DataIndex="NATIONALITY3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_residence" runat="server" DataIndex="RESIDENCE" Text="Residence" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_occupation" runat="server" DataIndex="OCCUPATION" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_deceased" runat="server" DataIndex="DECEASED" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_date_deceased" runat="server" DataIndex="DATE_DECEASED" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_tax_number" runat="server" DataIndex="TAX_NUMBER" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_is_pep" runat="server" DataIndex="TAX_REG_NUMBER" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_source_of_wealth" runat="server" DataIndex="SOURCE_OF_WEALTH" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="clm_rp_is_protected" runat="server" DataIndex="IS_PROTECTED" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>--%>
            
        </Columns>
    </ColumnModel>
    <BottomBar>
        <ext:PagingToolbar ID="pt_related_person" runat="server" HideRefresh="True" />
    </BottomBar>
</ext:GridPanel>
<%-- end of INDV Related Person --%>

<ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Related Person Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet11" runat="server" Title="Related Person" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_cmb_rp_person_relation" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_person_relation" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Person_Person_Relationship" StringFilter="Active = 1" EmptyText="Select One" Label="Person Person Relation" AnchorHorizontal="70%" AllowBlank="false" />
	                            </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_cmb_rp_gender" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_gender" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Jenis_Kelamin" StringFilter="Active = 1" EmptyText="Select One" Label="Gender" AnchorHorizontal="70%" AllowBlank="true" />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_rp_title" LabelWidth="250" runat="server" FieldLabel="Title" AnchorHorizontal="90%" AllowBlank="true" MaxLength="30"></ext:TextField>
                            <ext:TextField ID="txt_rp_first_name" Hidden="true" LabelWidth="250" runat="server" FieldLabel="First Name" AnchorHorizontal="90%" AllowBlank="False" MaxLength="100"></ext:TextField>
                            <ext:TextField ID="txt_rp_middle_name" Hidden="true" LabelWidth="250" runat="server" FieldLabel="Middle Name" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                            <ext:TextField ID="txt_rp_prefix" Hidden="true" LabelWidth="250" runat="server" FieldLabel="Prefix" AnchorHorizontal="90%" AllowBlank="False" MaxLength="100"></ext:TextField>
                            <ext:TextField ID="txt_rp_last_name" LabelWidth="250" runat="server" FieldLabel="Last Name" AnchorHorizontal="90%" AllowBlank="False" MaxLength="100"></ext:TextField>
                            <ext:DateField Editable="false" LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Birth Date" ID="df_birthdate" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_rp_birth_place" LabelWidth="250" runat="server" FieldLabel="Birth Place" AnchorHorizontal="90%" AllowBlank="true" MaxLength="255"></ext:TextField>
                            <ext:Panel ID="Panel_cmb_rp_country_of_birth" Hidden="true" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_country_of_birth" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country Of Birth" AnchorHorizontal="70%" AllowBlank="true" />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_rp_mothers_name" LabelWidth="250" runat="server" FieldLabel="Mothers Name" AnchorHorizontal="90%" AllowBlank="true" MaxLength="100"></ext:TextField>
                            <ext:TextField ID="txt_rp_alias" LabelWidth="250" runat="server" FieldLabel="Alias" AnchorHorizontal="90%" AllowBlank="true" MaxLength="100"></ext:TextField>
                            <ext:TextField ID="txt_rp_ssn" LabelWidth="250" runat="server" FieldLabel="SSN" AnchorHorizontal="90%" AllowBlank="true" MaxLength="16"></ext:TextField>
                            <ext:TextField ID="txt_rp_passport_number" LabelWidth="250" runat="server" FieldLabel="Passport Number" AnchorHorizontal="90%" AllowBlank="true" MaxLength="255"></ext:TextField>
                            <ext:Panel ID="Panel_cmb_rp_passport_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_passport_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Passport Country" AnchorHorizontal="70%" />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_rp_id_number" LabelWidth="250" runat="server" FieldLabel="ID Number" AnchorHorizontal="90%" AllowBlank="true" MaxLength="255"></ext:TextField>
                            <ext:Panel ID="Panel_cmb_rp_nationality1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_nationality1" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 1" AnchorHorizontal="70%" AllowBlank="true" />
	                            </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_cmb_rp_nationality2" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_nationality2" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 2" AnchorHorizontal="70%" />
	                            </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_cmb_rp_nationality3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_nationality3" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 3" AnchorHorizontal="70%" />
	                            </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_cmb_rp_residence" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_rp_residence" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Residence" AnchorHorizontal="70%" AllowBlank="true" />
	                            </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridPhone" Title="Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <goAML:GridPhone runat="server" ID="GridPhone_RelatedPerson"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridAddress" Title="Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <goAML:GridAddress runat="server" ID="GridAddress_RelatedPerson"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <goAML:GridEmail runat="server" ID="GridEmail_RelatedPerson" FK_REF_DETAIL_OF="31"/>
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_rp_occupation" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%" AllowBlank="true" MaxLength="255"></ext:TextField>
                            <ext:Panel ID="Panel_RelatedPerson_GridAddressWork" Title="Address Work" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <goAML:GridAddressWork runat="server" ID="GridAddressWork_RelatedPerson"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridPhoneWork" Title="Phone Work" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <goAML:GridPhone runat="server" ID="GridPhoneWork_RelatedPerson"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridIdentification" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <goAML:GridIdentification runat="server" ID="GridIdentification_RelatedPerson"/>
                                </Content>
                            </ext:Panel>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_rp_deceased" FieldLabel="Deceased"></ext:Checkbox>
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Date Deceased" ID="df_rp_date_deceased" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_rp_tax_number" LabelWidth="250" runat="server" FieldLabel="Tax Number" AnchorHorizontal="90%" MaxLength="16"></ext:TextField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_rp_tax_reg_number" FieldLabel="Tax Register Number?"></ext:Checkbox>
                            <ext:TextField ID="txt_rp_source_of_wealth" LabelWidth="250" runat="server" FieldLabel="Source Of Wealth" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_rp_is_protected" FieldLabel="Is Protected?"></ext:Checkbox>
                            <ext:TextField ID="txt_rp_comments" LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSave" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBack" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBack_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>