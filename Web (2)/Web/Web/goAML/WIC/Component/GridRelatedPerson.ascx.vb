﻿
Imports GoAMLBLL
Imports GoAMLBLL.goAML.DataModel
Imports GoAMLBLL.goAML_Customer.DataModel
Imports GoAMLBLL.Helper

Partial Class goAML_WIC_Component_GridRelatedPerson
    Inherits System.Web.UI.UserControl
    Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person)

    Private Const GRID_TITLE As String = "Related Person"
    Private Const SESSION_PREFIX As String = "CUSTOMER.GridRelatedPerson."

    Private _uniqueName As String
    Public Property UniqueName As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).UniqueName
        Get
            'Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME)
            Return _uniqueName
        End Get
        Set(value As String)
            'Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME) = value
            GridAddress_RelatedPerson.UniqueName = value
            GridAddressWork_RelatedPerson.UniqueName = value
            GridPhone_RelatedPerson.UniqueName = value
            GridPhoneWork_RelatedPerson.UniqueName = value
            GridIdentification_RelatedPerson.UniqueName = value
            GridEmail_RelatedPerson.UniqueName = value
            _uniqueName = value
        End Set
    End Property

    Private _fk_ref_detail_of As Integer
    Public Property FK_REF_DETAIL_OF As Integer Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).FK_REF_DETAIL_OF
        Get
            Return _fk_ref_detail_of
        End Get
        Set(value As Integer)
            _fk_ref_detail_of = value
        End Set
    End Property

    Public Property CIF As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).CIF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName)
        End Get
        Set(value As String)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_Edit As goAML_Ref_Customer_Related_Person Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).objTemp_Edit
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName)
        End Get
        Set(value As goAML_Ref_Customer_Related_Person)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref As goAML_Ref_Customer_Related_Person Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).objTemp_goAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName)
        End Get
        Set(value As goAML_Ref_Customer_Related_Person)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_Ref As List(Of goAML_Ref_Customer_Related_Person) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).objListgoAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Customer_Related_Person))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_vw As List(Of goAML_Ref_Customer_Related_Person) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).objListgoAML_vw
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Customer_Related_Person))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName) = value
        End Set
    End Property

    Private _title As String
    Public Property Title As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).Title
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Public ReadOnly Property SessionPrefix As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).SessionPrefix
        Get
            Return SESSION_PREFIX
        End Get
    End Property

    Private _isViewMode As Boolean
    Public Property IsViewMode As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).IsViewMode
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName)
        End Get
        Set(value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName) = value
        End Set
    End Property

    Public Property IsDataChange As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).IsDataChange
        Get
            If Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) Is Nothing Then
                Return False
            End If
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName)
        End Get
        Set(value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) = value
        End Set
    End Property

    Private Shadows Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If IsViewMode Then
        '    Dim cmdColumn As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        '    If cmdColumn IsNot Nothing Then
        '        Dim cmd1 As GridCommand = cmdColumn.Commands(1)
        '        Dim cmd2 As GridCommand = cmdColumn.Commands(2)
        '        cmd1.Hidden = IsViewMode
        '        cmd2.Hidden = IsViewMode
        '    End If

        '    btnAdd.Hidden = IsViewMode

        '    GridAddress_RelatedPerson.IsViewMode = IsViewMode
        '    GridAddressWork_RelatedPerson.IsViewMode = IsViewMode
        '    GridPhone_RelatedPerson.IsViewMode = IsViewMode
        '    GridPhoneWork_RelatedPerson.IsViewMode = IsViewMode
        '    GridIdentification_RelatedPerson.IsViewMode = IsViewMode
        '    GridEmail_RelatedPerson.IsViewMode = IsViewMode
        'End If
    End Sub
    Private Shadows Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Dim cmdColumnEdit As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        Dim cmdColumnDetail As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(2), CommandColumn)
        cmdColumnEdit.Hidden = IsViewMode
        cmdColumnDetail.Hidden = Not IsViewMode

        'Dim store As Store = TryCast(GridPanel.Store(0), Store)
        'store.DataBind()

        btnAdd.Hidden = IsViewMode

        GridAddress_RelatedPerson.IsViewMode = IsViewMode
        GridAddressWork_RelatedPerson.IsViewMode = IsViewMode
        GridPhone_RelatedPerson.IsViewMode = IsViewMode
        GridPhoneWork_RelatedPerson.IsViewMode = IsViewMode
        GridIdentification_RelatedPerson.IsViewMode = IsViewMode
        GridEmail_RelatedPerson.IsViewMode = IsViewMode
    End Sub
    Public Sub btnAdd_DirectClick(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).btnAdd_DirectClick
        Try
            WindowDetail.Hidden = False
            FormPanel.Hidden = False
            btnSave.Hidden = False

            cmb_rp_person_relation.IsReadOnly = False
            txt_rp_comments.ReadOnly = False
            cmb_rp_gender.IsReadOnly = False
            txt_rp_title.ReadOnly = False
            'txt_rp_first_name.ReadOnly = False
            'txt_rp_middle_name.ReadOnly = False
            'txt_rp_prefix.ReadOnly = False
            txt_rp_last_name.ReadOnly = False
            df_birthdate.ReadOnly = False
            txt_rp_birth_place.ReadOnly = False
            'cmb_rp_country_of_birth.IsReadOnly = False
            txt_rp_mothers_name.ReadOnly = False
            txt_rp_alias.ReadOnly = False
            'txt_rp_full_name_frn.ReadOnly = False
            txt_rp_ssn.ReadOnly = False
            txt_rp_passport_number.ReadOnly = False
            cmb_rp_passport_country.IsReadOnly = False
            txt_rp_id_number.ReadOnly = False
            cmb_rp_nationality1.IsReadOnly = False
            cmb_rp_nationality2.IsReadOnly = False
            cmb_rp_nationality3.IsReadOnly = False
            cmb_rp_residence.IsReadOnly = False
            'df_rp_residence_since.ReadOnly = False
            txt_rp_occupation.ReadOnly = False
            cbx_rp_deceased.ReadOnly = False
            df_rp_date_deceased.ReadOnly = False
            txt_rp_tax_number.ReadOnly = False
            cbx_rp_tax_reg_number.ReadOnly = False
            txt_rp_source_of_wealth.ReadOnly = False
            cbx_rp_is_protected.ReadOnly = False

            WindowDetail.Title = "Related Person Add"

            objTemp_Edit = Nothing
            'FormPanel_RelatedPerson.Reset()
            Clearinput()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnSave_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).btnSave_DirectEvent
        Try
            If IsData_Valid() Then
                If objTemp_Edit Is Nothing Then
                    Save()
                Else
                    Save(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnBack_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).btnBack_DirectEvent
        Try
            WindowDetail.Hidden = True
            FormPanel.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub GrdCmd(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).GrdCmd
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub LoadData(id As Long, isEdit As Boolean) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).LoadData
        Try
            objTemp_goAML_Ref = objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id)
            objTemp_Edit = objListgoAML_vw.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id)

            If Not objTemp_Edit Is Nothing Then
                FormPanel.Hidden = False
                WindowDetail.Hidden = False
                btnSave.Hidden = Not isEdit

                cmb_rp_person_relation.IsReadOnly = Not isEdit
                txt_rp_comments.ReadOnly = Not isEdit
                cmb_rp_gender.IsReadOnly = Not isEdit
                txt_rp_title.ReadOnly = Not isEdit
                'txt_rp_first_name.ReadOnly = Not isEdit
                'txt_rp_middle_name.ReadOnly = Not isEdit
                'txt_rp_prefix.ReadOnly = Not isEdit
                txt_rp_last_name.ReadOnly = Not isEdit
                df_birthdate.ReadOnly = Not isEdit
                txt_rp_birth_place.ReadOnly = Not isEdit
                'cmb_rp_country_of_birth.IsReadOnly = Not isEdit
                txt_rp_mothers_name.ReadOnly = Not isEdit
                txt_rp_alias.ReadOnly = Not isEdit
                txt_rp_ssn.ReadOnly = Not isEdit
                txt_rp_passport_number.ReadOnly = Not isEdit
                cmb_rp_passport_country.IsReadOnly = Not isEdit
                txt_rp_id_number.ReadOnly = Not isEdit
                cmb_rp_nationality1.IsReadOnly = Not isEdit
                cmb_rp_nationality2.IsReadOnly = Not isEdit
                cmb_rp_nationality3.IsReadOnly = Not isEdit
                cmb_rp_residence.IsReadOnly = Not isEdit
                txt_rp_occupation.ReadOnly = Not isEdit
                cbx_rp_deceased.ReadOnly = Not isEdit
                df_rp_date_deceased.ReadOnly = Not isEdit
                txt_rp_tax_number.ReadOnly = Not isEdit
                cbx_rp_tax_reg_number.ReadOnly = Not isEdit
                txt_rp_source_of_wealth.ReadOnly = Not isEdit
                cbx_rp_is_protected.ReadOnly = Not isEdit


                With objTemp_Edit
                    Dim refPersonRelation = GoAMLBLL.Helper.CommonHelper.GetReferenceByCode("goAML_Ref_Person_Person_Relationship", .PERSON_PERSON_RELATION)
                    Dim refGender = GoAMLBLL.Helper.CommonHelper.GetReferenceByCode("goAML_Ref_Jenis_Kelamin", .GENDER)
                    Dim refCountryBirth = GoAMLBLL.Helper.CommonHelper.GetReferenceByCode("goAML_Ref_Nama_Negara", .COUNTRY_OF_BIRTH)
                    Dim refPassportCountry = GoAMLBLL.Helper.CommonHelper.GetReferenceByCode("goAML_Ref_Nama_Negara", .PASSPORT_COUNTRY)
                    Dim refNationality1 = GoAMLBLL.Helper.CommonHelper.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY1)
                    Dim refNationality2 = GoAMLBLL.Helper.CommonHelper.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY2)
                    Dim refNationality3 = GoAMLBLL.Helper.CommonHelper.GetReferenceByCode("goAML_Ref_Nama_Negara", .NATIONALITY3)
                    Dim refResidence = GoAMLBLL.Helper.CommonHelper.GetReferenceByCode("goAML_Ref_Nama_Negara", .RESIDENCE)

                    cmb_rp_person_relation.SetTextWithTextValue(refPersonRelation.Kode, refPersonRelation.Keterangan)
                    txt_rp_comments.Value = .COMMENTS
                    cmb_rp_gender.SetTextWithTextValue(refGender.Kode, refGender.Keterangan)
                    txt_rp_title.Value = .TITLE
                    'txt_rp_first_name.Value = .FIRST_NAME
                    'txt_rp_middle_name.Value = .MIDDLE_NAME
                    'txt_rp_prefix.Value = .PREFIX
                    txt_rp_last_name.Value = .LAST_NAME
                    df_birthdate.Text = .BIRTHDATE
                    txt_rp_birth_place.Value = .BIRTH_PLACE
                    'cmb_rp_country_of_birth.SetTextWithTextValue(refCountryBirth.Kode, refCountryBirth.Keterangan)
                    txt_rp_mothers_name.Value = .MOTHERS_NAME
                    txt_rp_alias.Value = .ALIAS
                    txt_rp_ssn.Value = .SSN
                    txt_rp_passport_number.Value = .PASSPORT_NUMBER
                    cmb_rp_passport_country.SetTextWithTextValue(refPassportCountry.Kode, refPassportCountry.Keterangan)
                    txt_rp_id_number.Value = .ID_NUMBER
                    cmb_rp_nationality1.SetTextWithTextValue(refNationality1.Kode, refNationality1.Keterangan)
                    cmb_rp_nationality2.SetTextWithTextValue("", "")
                    If refNationality2 IsNot Nothing Then
                        cmb_rp_nationality2.SetTextWithTextValue(refNationality2.Kode, refNationality2.Keterangan)
                    End If
                    cmb_rp_nationality3.SetTextWithTextValue("", "")
                    If refNationality3 IsNot Nothing Then
                        cmb_rp_nationality3.SetTextWithTextValue(refNationality3.Kode, refNationality3.Keterangan)
                    End If

                    cmb_rp_residence.SetTextWithTextValue(refResidence.Kode, refResidence.Keterangan)
                    txt_rp_occupation.Value = .OCCUPATION
                    cbx_rp_deceased.Checked = .DECEASED
                    df_rp_date_deceased.Text = .DATE_DECEASED
                    txt_rp_tax_number.Value = .TAX_NUMBER
                    cbx_rp_tax_reg_number.Checked = .TAX_REG_NUMBER
                    txt_rp_source_of_wealth.Text = .SOURCE_OF_WEALTH
                    cbx_rp_is_protected.Checked = .IS_PROTECTED

                    GridEmail_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Customer_Email)
                    GridAddress_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Address)
                    GridAddressWork_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Address_Work)
                    GridPhone_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Phone)
                    GridPhoneWork_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Phone_Work)
                    GridIdentification_RelatedPerson.LoadData(.ObjList_GoAML_Person_Identification)
                End With

                WindowDetail.Title = Title & " " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub LoadData(data As List(Of goAML_Ref_Customer_Related_Person)) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).LoadData
        objListgoAML_Ref = data.Select(Function(x) x).ToList()
        objListgoAML_vw = data.Select(Function(x) x).ToList()

        If objListgoAML_vw IsNot Nothing Then
            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()
        Else
            Store.DataBind()
        End If
    End Sub

    Public Sub DeleteRecord(id As Long) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).DeleteRecord
        objListgoAML_vw.Remove(objListgoAML_vw.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id))
        objListgoAML_Ref.Remove(objListgoAML_Ref.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = id))

        Store.DataSource = objListgoAML_vw.ToList()
        Store.DataBind()
    End Sub

    Public Sub Clearinput() Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).Clearinput
        cmb_rp_person_relation.SetTextValue("")
        txt_rp_comments.Text = ""
        cmb_rp_gender.SetTextValue("")
        txt_rp_title.Text = ""
        'txt_rp_first_name.Text = ""
        'txt_rp_middle_name.Text = ""
        'txt_rp_prefix.Text = ""
        txt_rp_last_name.Text = ""
        df_birthdate.Text = ""
        txt_rp_birth_place.Text = ""
        'cmb_rp_country_of_birth.SetTextValue("")
        txt_rp_mothers_name.Text = ""
        txt_rp_alias.Text = ""
        'txt_rp_full_name_frn.Text = ""
        txt_rp_ssn.Text = ""
        txt_rp_passport_number.Text = ""
        cmb_rp_passport_country.SetTextValue("")
        txt_rp_id_number.Text = ""
        cmb_rp_nationality1.SetTextValue("")
        cmb_rp_nationality2.SetTextValue("")
        cmb_rp_nationality3.SetTextValue("")
        cmb_rp_residence.SetTextValue("")
        'df_rp_residence_since.Text = ""
        txt_rp_occupation.Text = ""
        cbx_rp_deceased.Checked = False
        df_rp_date_deceased.Text = ""
        txt_rp_tax_number.Text = ""
        cbx_rp_tax_reg_number.Checked = False
        txt_rp_source_of_wealth.Text = ""
        cbx_rp_is_protected.Checked = False
    End Sub

    Public Sub Save(Optional isNew As Boolean = True) Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).Save
        Try
            If Not isNew Then

                With objTemp_Edit
                    .CIF = CIF
                    .PERSON_PERSON_RELATION = cmb_rp_person_relation.SelectedItemValue
                    .COMMENTS = txt_rp_comments.Value
                    .GENDER = cmb_rp_gender.SelectedItemValue
                    .TITLE = txt_rp_title.Value
                    '.FIRST_NAME = txt_rp_first_name.Value
                    '.MIDDLE_NAME = txt_rp_middle_name.Value
                    '.PREFIX = txt_rp_prefix.Value
                    .LAST_NAME = txt_rp_last_name.Value
                    .BIRTHDATE = df_birthdate.Value
                    .BIRTH_PLACE = txt_rp_birth_place.Value
                    '.COUNTRY_OF_BIRTH = cmb_rp_country_of_birth.SelectedItemValue
                    .MOTHERS_NAME = txt_rp_mothers_name.Value
                    .ALIAS = txt_rp_alias.Value
                    .SSN = txt_rp_ssn.Value
                    .PASSPORT_NUMBER = txt_rp_passport_number.Value
                    .PASSPORT_COUNTRY = cmb_rp_passport_country.SelectedItemValue
                    .ID_NUMBER = txt_rp_id_number.Value
                    .NATIONALITY1 = cmb_rp_nationality1.SelectedItemValue
                    .NATIONALITY2 = cmb_rp_nationality2.SelectedItemValue
                    .NATIONALITY3 = cmb_rp_nationality3.SelectedItemValue
                    .RESIDENCE = cmb_rp_residence.SelectedItemValue
                    .OCCUPATION = txt_rp_occupation.Value
                    .DECEASED = cbx_rp_deceased.Value
                    .DATE_DECEASED = df_rp_date_deceased.Value
                    .TAX_NUMBER = txt_rp_tax_number.Value
                    .TAX_REG_NUMBER = cbx_rp_tax_reg_number.Value
                    .SOURCE_OF_WEALTH = txt_rp_source_of_wealth.Value
                    .IS_PROTECTED = cbx_rp_is_protected.Value

                    '.ObjList_GoAML_Ref_Customer_Email = New List(Of goAML_Ref_Customer_Email)

                    'For Each element In GridEmail_RelatedPerson.objListgoAML_vw
                    '    Dim obj_goAML_Ref_Customer_Email = New goAML_Ref_Customer_Email
                    '    obj_goAML_Ref_Customer_Email.PK_goAML_Ref_Customer_Email_ID = element.PK_goAML_Ref_Customer_Email_ID
                    '    obj_goAML_Ref_Customer_Email.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                    '    obj_goAML_Ref_Customer_Email.CIF = objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID
                    '    obj_goAML_Ref_Customer_Email.EMAIL_ADDRESS = element.EMAIL_ADDRESS

                    '    .ObjList_GoAML_Ref_Customer_Email.Add(obj_goAML_Ref_Customer_Email)
                    'Next
                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridIdentification_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                End With

                With objTemp_goAML_Ref
                    .CIF = CIF
                    .PERSON_PERSON_RELATION = cmb_rp_person_relation.SelectedItemValue
                    .COMMENTS = txt_rp_comments.Value
                    .GENDER = cmb_rp_gender.SelectedItemValue
                    .TITLE = txt_rp_title.Value
                    '.FIRST_NAME = txt_rp_first_name.Value
                    '.MIDDLE_NAME = txt_rp_middle_name.Value
                    '.PREFIX = txt_rp_prefix.Value
                    .LAST_NAME = txt_rp_last_name.Value
                    .BIRTHDATE = df_birthdate.Value
                    .BIRTH_PLACE = txt_rp_birth_place.Value
                    '.COUNTRY_OF_BIRTH = cmb_rp_country_of_birth.SelectedItemValue
                    .MOTHERS_NAME = txt_rp_mothers_name.Value
                    .ALIAS = txt_rp_alias.Value
                    .SSN = txt_rp_ssn.Value
                    .PASSPORT_NUMBER = txt_rp_passport_number.Value
                    .PASSPORT_COUNTRY = cmb_rp_passport_country.SelectedItemValue
                    .ID_NUMBER = txt_rp_id_number.Value
                    .NATIONALITY1 = cmb_rp_nationality1.SelectedItemValue
                    .NATIONALITY2 = cmb_rp_nationality2.SelectedItemValue
                    .NATIONALITY3 = cmb_rp_nationality3.SelectedItemValue
                    .RESIDENCE = cmb_rp_residence.SelectedItemValue
                    .OCCUPATION = txt_rp_occupation.Value
                    .DECEASED = cbx_rp_deceased.Value
                    .DATE_DECEASED = df_rp_date_deceased.Value
                    .TAX_NUMBER = txt_rp_tax_number.Value
                    .TAX_REG_NUMBER = cbx_rp_tax_reg_number.Value
                    .SOURCE_OF_WEALTH = txt_rp_source_of_wealth.Value
                    .IS_PROTECTED = cbx_rp_is_protected.Value

                    '.ObjList_GoAML_Ref_Customer_Email = New List(Of goAML_Ref_Customer_Email)

                    'For Each element In GridEmail_RelatedPerson.objListgoAML_Ref
                    '    Dim obj_goAML_Ref_Customer_Email = New goAML_Ref_Customer_Email
                    '    obj_goAML_Ref_Customer_Email.PK_goAML_Ref_Customer_Email_ID = element.PK_goAML_Ref_Customer_Email_ID
                    '    obj_goAML_Ref_Customer_Email.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                    '    obj_goAML_Ref_Customer_Email.CIF = objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID
                    '    obj_goAML_Ref_Customer_Email.EMAIL_ADDRESS = element.EMAIL_ADDRESS

                    '    .ObjList_GoAML_Ref_Customer_Email.Add(obj_goAML_Ref_Customer_Email)
                    'Next
                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridIdentification_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                End With

                objTemp_Edit = Nothing
                objTemp_goAML_Ref = Nothing
            Else
                Dim objNewVWgoAML_RelatedPerson As New goAML_Ref_Customer_Related_Person
                Dim objNewgoAML_RelatedPerson As New goAML_Ref_Customer_Related_Person
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_RelatedPerson
                    .PK_goAML_Ref_Customer_Related_Person_ID = intpk
                    .CIF = CIF
                    .PERSON_PERSON_RELATION = cmb_rp_person_relation.SelectedItemValue
                    .COMMENTS = txt_rp_comments.Value
                    .GENDER = cmb_rp_gender.SelectedItemValue
                    .TITLE = txt_rp_title.Value
                    '.FIRST_NAME = txt_rp_first_name.Value
                    '.MIDDLE_NAME = txt_rp_middle_name.Value
                    '.PREFIX = txt_rp_prefix.Value
                    .LAST_NAME = txt_rp_last_name.Value
                    .BIRTHDATE = df_birthdate.Value
                    .BIRTH_PLACE = txt_rp_birth_place.Value
                    '.COUNTRY_OF_BIRTH = cmb_rp_country_of_birth.SelectedItemValue
                    .MOTHERS_NAME = txt_rp_mothers_name.Value
                    .ALIAS = txt_rp_alias.Value
                    .SSN = txt_rp_ssn.Value
                    .PASSPORT_NUMBER = txt_rp_passport_number.Value
                    .PASSPORT_COUNTRY = cmb_rp_passport_country.SelectedItemValue
                    .ID_NUMBER = txt_rp_id_number.Value
                    .NATIONALITY1 = cmb_rp_nationality1.SelectedItemValue
                    .NATIONALITY2 = cmb_rp_nationality2.SelectedItemValue
                    .NATIONALITY3 = cmb_rp_nationality3.SelectedItemValue
                    .RESIDENCE = cmb_rp_residence.SelectedItemValue
                    .OCCUPATION = txt_rp_occupation.Value
                    .DECEASED = cbx_rp_deceased.Value
                    .DATE_DECEASED = df_rp_date_deceased.Value
                    .TAX_NUMBER = txt_rp_tax_number.Value
                    .TAX_REG_NUMBER = cbx_rp_tax_reg_number.Value
                    .SOURCE_OF_WEALTH = txt_rp_source_of_wealth.Value
                    .IS_PROTECTED = cbx_rp_is_protected.Value

                    '.ObjList_GoAML_Ref_Customer_Email = New List(Of goAML_Ref_Customer_Email)

                    'For Each element In GridEmail_RelatedPerson.objListgoAML_vw
                    '    Dim obj_goAML_Ref_Customer_Email = New goAML_Ref_Customer_Email
                    '    obj_goAML_Ref_Customer_Email.PK_goAML_Ref_Customer_Email_ID = element.PK_goAML_Ref_Customer_Email_ID
                    '    obj_goAML_Ref_Customer_Email.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                    '    obj_goAML_Ref_Customer_Email.CIF = objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID
                    '    obj_goAML_Ref_Customer_Email.EMAIL_ADDRESS = element.EMAIL_ADDRESS

                    '    .ObjList_GoAML_Ref_Customer_Email.Add(obj_goAML_Ref_Customer_Email)
                    'Next

                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridIdentification_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                End With

                With objNewgoAML_RelatedPerson
                    .PK_goAML_Ref_Customer_Related_Person_ID = intpk
                    .CIF = CIF
                    .PERSON_PERSON_RELATION = cmb_rp_person_relation.SelectedItemValue
                    .COMMENTS = txt_rp_comments.Value
                    .GENDER = cmb_rp_gender.SelectedItemValue
                    .TITLE = txt_rp_title.Value
                    '.FIRST_NAME = txt_rp_first_name.Value
                    '.MIDDLE_NAME = txt_rp_middle_name.Value
                    '.PREFIX = txt_rp_prefix.Value
                    .LAST_NAME = txt_rp_last_name.Value
                    .BIRTHDATE = df_birthdate.Value
                    .BIRTH_PLACE = txt_rp_birth_place.Value
                    '.COUNTRY_OF_BIRTH = cmb_rp_country_of_birth.SelectedItemValue
                    .MOTHERS_NAME = txt_rp_mothers_name.Value
                    .ALIAS = txt_rp_alias.Value
                    .SSN = txt_rp_ssn.Value
                    .PASSPORT_NUMBER = txt_rp_passport_number.Value
                    .PASSPORT_COUNTRY = cmb_rp_passport_country.SelectedItemValue
                    .ID_NUMBER = txt_rp_id_number.Value
                    .NATIONALITY1 = cmb_rp_nationality1.SelectedItemValue
                    .NATIONALITY2 = cmb_rp_nationality2.SelectedItemValue
                    .NATIONALITY3 = cmb_rp_nationality3.SelectedItemValue
                    .RESIDENCE = cmb_rp_residence.SelectedItemValue
                    .OCCUPATION = txt_rp_occupation.Value
                    .DECEASED = cbx_rp_deceased.Value
                    .DATE_DECEASED = df_rp_date_deceased.Value
                    .TAX_NUMBER = txt_rp_tax_number.Value
                    .TAX_REG_NUMBER = cbx_rp_tax_reg_number.Value
                    .SOURCE_OF_WEALTH = txt_rp_source_of_wealth.Value
                    .IS_PROTECTED = cbx_rp_is_protected.Value

                    '.ObjList_GoAML_Ref_Customer_Email = New List(Of goAML_Ref_Customer_Email)

                    'For Each element In GridEmail_RelatedPerson.objListgoAML_Ref
                    '    Dim obj_goAML_Ref_Customer_Email = New goAML_Ref_Customer_Email
                    '    obj_goAML_Ref_Customer_Email.PK_goAML_Ref_Customer_Email_ID = element.PK_goAML_Ref_Customer_Email_ID
                    '    obj_goAML_Ref_Customer_Email.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                    '    obj_goAML_Ref_Customer_Email.CIF = objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID
                    '    obj_goAML_Ref_Customer_Email.EMAIL_ADDRESS = element.EMAIL_ADDRESS

                    '    .ObjList_GoAML_Ref_Customer_Email.Add(obj_goAML_Ref_Customer_Email)
                    'Next
                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridIdentification_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTemp_Edit.PK_goAML_Ref_Customer_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)

                    'For Each
                End With

                objListgoAML_vw.Add(objNewVWgoAML_RelatedPerson)
                objListgoAML_Ref = objListgoAML_vw

            End If

            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()

            WindowDetail.Hidden = True
            IsDataChange = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Function IsData_Valid() As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Related_Person).IsData_Valid
        If CIF.Trim = "" Then
            Throw New Exception("CIF is required")
        End If

        If cmb_rp_person_relation.SelectedItemText Is Nothing Then
            Throw New Exception("Person Relation is required")
        End If
        'If txt_rp_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If
        'If cmb_rp_gender.SelectedItemText Is Nothing Then
        '    Throw New Exception("Gender is required")
        'End If
        'If txt_rp_title.Text.Trim = "" Then
        '    Throw New Exception("Title is required")
        'End If
        'If txt_rp_first_name.Text.Trim = "" Then
        '    Throw New Exception("First Name is required")
        'End If
        'If txt_rp_prefix.Text.Trim = "" Then
        '    Throw New Exception("Prefix is required")
        'End If
        If txt_rp_last_name.Text.Trim = "" Then
            Throw New Exception("Full Name is required")
        End If
        'If df_birthdate.RawValue Is Nothing Then
        '    Throw New Exception("Birth Date is required")
        'End If
        'If txt_rp_birth_place.Text.Trim = "" Then
        '    Throw New Exception("Birth Place is required")
        'End If
        'If Not CommonHelper.IsFieldValid(txt_rp_birth_place.Text.Trim, "birth_place") Then
        '    Throw New Exception("Birth Place format is not valid")
        'End If
        'If cmb_rp_country_of_birth.SelectedItemText Is Nothing Then
        '    Throw New Exception("Country Of Birth is required")
        'End If
        'If txt_rp_mothers_name.Text.Trim = "" Then
        '    Throw New Exception("Mother's Name is required")
        'End If
        'If Not CommonHelper.IsFieldValid(txt_rp_mothers_name.Text.Trim, "mothers_name") Then
        '    Throw New Exception("Mothers Name format is not valid")
        'End If
        'If txt_rp_alias.Text.Trim = "" Then
        '    Throw New Exception("Alias is required")
        'End If
        'If Not CommonHelper.IsFieldValid(txt_rp_alias.Text.Trim, "alias") Then
        '    Throw New Exception("Alias format is not valid")
        'End If
        'If txt_rp_ssn.Text.Trim = "" Then
        '    Throw New Exception("SSN is required")
        'End If
        'If txt_rp_passport_number.Text.Trim = "" Then
        '    Throw New Exception("Passport Number is required")
        'End If
        'If cmb_rp_passport_country.SelectedItemText Is Nothing Then
        '    Throw New Exception("Paassport Country is required")
        'End If
        'If txt_rp_id_number.Text.Trim = "" Then
        '    Throw New Exception("ID Number is required")
        'End If
        'If cmb_rp_nationality1.SelectedItemText Is Nothing Then
        '    Throw New Exception("Nationality 1 is required")
        'End If
        'If cmb_rp_residence.SelectedItemText Is Nothing Then
        '    Throw New Exception("Domicile Country is required")
        'End If

        'If txt_rp_occupation.Text.Trim = "" Then
        '    Throw New Exception("Occupation is required")
        'End If
        'If df_rp_date_deceased.RawValue Is Nothing Then
        '    Throw New Exception("Date Deceased is required")
        'End If
        'If txt_rp_tax_number.Text.Trim = "" Then
        '    Throw New Exception("Tax Number is required")
        'End If
        'If txt_rp_source_of_wealth.Text.Trim = "" Then
        '    Throw New Exception("Source of Wealth is required")
        'End If

        Return True
    End Function
End Class
