﻿
Imports GoAMLBLL
Imports GoAMLBLL.goAML.DataModel
Imports GoAMLBLL.goAML_CustomerDataBLL
Imports System.Data

Partial Class goAML_WIC_Component_GridEntityIdentification
    Inherits System.Web.UI.UserControl
    Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification)

    Private Const GRID_TITLE As String = "Entity Identification"
    Private Const SESSION_PREFIX As String = "CUSTOMER.GridEntityIdentification."

    Private _uniqueName As String
    Public Property UniqueName As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).UniqueName
        Get
            'Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME)
            Return _uniqueName
        End Get
        Set(value As String)
            'Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME) = value
            _uniqueName = value
        End Set
    End Property

    Public Property FK_REF_DETAIL_OF As Integer Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).FK_REF_DETAIL_OF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_FK_REF_DETAIL_OF & UniqueName)
        End Get
        Set(value As Integer)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_FK_REF_DETAIL_OF & UniqueName) = value
        End Set
    End Property

    Public Property CIF As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).CIF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName)
        End Get
        Set(value As String)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_Edit As goAML_Ref_Customer_Entity_Identification Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).objTemp_Edit
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName)
        End Get
        Set(value As goAML_Ref_Customer_Entity_Identification)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref As goAML_Ref_Customer_Entity_Identification Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).objTemp_goAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName)
        End Get
        Set(value As goAML_Ref_Customer_Entity_Identification)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_Ref As List(Of goAML_Ref_Customer_Entity_Identification) Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).objListgoAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Customer_Entity_Identification))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_vw As List(Of goAML_Ref_Customer_Entity_Identification) Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).objListgoAML_vw
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Customer_Entity_Identification))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName) = value
        End Set
    End Property

    Public ReadOnly Property SessionPrefix As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).SessionPrefix
        Get
            Return SESSION_PREFIX
        End Get
    End Property

    Private _title As String
    Public Property Title As String Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).Title
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Public Property IsViewMode As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).IsViewMode
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName)
        End Get
        Set(ByVal value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName) = value
        End Set
    End Property

    Public Property IsDataChange As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).IsDataChange
        Get
            If Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) Is Nothing Then
                Return False
            End If
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName)
        End Get
        Set(value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) = value
        End Set
    End Property

    Private Shadows Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If IsViewMode Then
        '    Dim cmdColumn As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        '    If cmdColumn IsNot Nothing Then
        '        Dim cmd1 As GridCommand = cmdColumn.Commands(1)
        '        Dim cmd2 As GridCommand = cmdColumn.Commands(2)
        '        cmd1.Hidden = IsViewMode
        '        cmd2.Hidden = IsViewMode
        '    End If

        '    btnAdd.Hidden = IsViewMode
        'End If
    End Sub
    Private Shadows Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Dim cmdColumnEdit As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        Dim cmdColumnDetail As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(2), CommandColumn)
        cmdColumnEdit.Hidden = IsViewMode
        cmdColumnDetail.Hidden = Not IsViewMode

        'Dim store As Store = TryCast(GridPanel.Store(0), Store)
        'store.DataBind()

        btnAdd.Hidden = IsViewMode
    End Sub

    Public Sub btnAdd_DirectClick(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).btnAdd_DirectClick
        Try
            WindowDetail.Hidden = False
            FormPanel.Hidden = False
            btnSave.Hidden = False

            ascx_cmb_ef_type.IsReadOnly = False
            txt_ef_number.ReadOnly = False
            df_issue_date.ReadOnly = False
            df_expiry_date.ReadOnly = False
            txt_ef_issued_by.ReadOnly = False
            ascx_cmb_ef_issue_country.IsReadOnly = False
            txt_ef_comments.ReadOnly = False

            WindowDetail.Title = Title & " Add"

            objTemp_Edit = Nothing
            'FormPanel_EntityIdentification.Reset()
            Clearinput()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnSave_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).btnSave_DirectEvent
        Try
            If IsData_Valid() Then
                If objTemp_Edit Is Nothing Then
                    Save()
                Else
                    Save(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnBack_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).btnBack_DirectEvent
        Try
            WindowDetail.Hidden = True
            FormPanel.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub GrdCmd(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).GrdCmd
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub LoadData(id As Long, isEdit As Boolean) Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).LoadData
        Try
            objTemp_goAML_Ref = objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id)
            objTemp_Edit = objListgoAML_vw.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id)

            If Not objTemp_Edit Is Nothing Then
                FormPanel.Hidden = False
                WindowDetail.Hidden = False
                btnSave.Hidden = Not isEdit

                ascx_cmb_ef_type.IsReadOnly = Not isEdit
                txt_ef_number.ReadOnly = Not isEdit
                df_issue_date.ReadOnly = Not isEdit
                df_expiry_date.ReadOnly = Not isEdit
                txt_ef_issued_by.ReadOnly = Not isEdit
                ascx_cmb_ef_issue_country.IsReadOnly = Not isEdit
                txt_ef_comments.ReadOnly = Not isEdit


                With objTemp_Edit
                    Dim refType = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Identifier_Type", .TYPE)
                    Dim refIssue = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .ISSUE_COUNTRY)

                    ascx_cmb_ef_type.SetTextWithTextValue(refType.Kode, refType.Keterangan)
                    txt_ef_number.Value = .NUMBER
                    df_issue_date.Text = IIf(.ISSUE_DATE Is Nothing, "", .ISSUE_DATE)
                    df_expiry_date.Text = IIf(.EXPIRY_DATE Is Nothing, "", .EXPIRY_DATE)
                    txt_ef_issued_by.Value = .ISSUED_BY
                    ascx_cmb_ef_issue_country.SetTextWithTextValue(refIssue.Kode, refIssue.Keterangan)
                    txt_ef_comments.Value = .COMMENTS

                End With

                WindowDetail.Title = Title & " " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub DeleteRecord(id As Long) Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).DeleteRecord
        objListgoAML_vw.Remove(objListgoAML_vw.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id))
        objListgoAML_Ref.Remove(objListgoAML_Ref.SingleOrDefault(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = id))

        Store.DataSource = objListgoAML_vw.ToList()
        Store.DataBind()
    End Sub

    Public Sub Clearinput() Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).Clearinput
        ascx_cmb_ef_type.SetTextValue("")
        txt_ef_number.Text = ""
        df_issue_date.Text = ""
        df_expiry_date.Text = ""
        txt_ef_issued_by.Text = ""
        ascx_cmb_ef_issue_country.SetTextValue("")
        txt_ef_comments.Text = ""
    End Sub

    Public Sub Save(Optional isNew As Boolean = True) Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).Save
        Try
            If Not isNew Then
                With objTemp_Edit
                    .CIF = CIF
                    .TYPE = ascx_cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If df_issue_date.RawValue IsNot Nothing Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If df_expiry_date.RawValue IsNot Nothing Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = ascx_cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateDate = DateTime.Now
                End With

                With objTemp_goAML_Ref
                    .CIF = CIF
                    .TYPE = ascx_cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If df_issue_date.RawValue IsNot Nothing Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If df_expiry_date.RawValue IsNot Nothing Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = ascx_cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateDate = DateTime.Now
                End With

                objTemp_Edit = Nothing
                objTemp_goAML_Ref = Nothing
            Else
                Dim objNewVWgoAML_EntityIdentification As New goAML_Ref_Customer_Entity_Identification
                Dim objNewgoAML_EntityIdentification As New goAML_Ref_Customer_Entity_Identification
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Identification_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_EntityIdentification
                    .PK_goAML_Ref_Customer_Entity_Identification_ID = intpk
                    .CIF = CIF
                    .TYPE = ascx_cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If df_issue_date.RawValue IsNot Nothing Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If df_expiry_date.RawValue IsNot Nothing Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = ascx_cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .CreatedDate = DateTime.Now
                    .Active = True
                End With

                With objNewgoAML_EntityIdentification
                    .PK_goAML_Ref_Customer_Entity_Identification_ID = intpk
                    .CIF = CIF
                    .TYPE = ascx_cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If df_issue_date.RawValue IsNot Nothing Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If df_expiry_date.RawValue IsNot Nothing Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = ascx_cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .CreatedDate = DateTime.Now
                    .Active = True
                End With

                objListgoAML_vw.Add(objNewVWgoAML_EntityIdentification)
                objListgoAML_Ref = objListgoAML_vw
            End If

            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()

            WindowDetail.Hidden = True
            IsDataChange = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Function IsData_Valid() As Boolean Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).IsData_Valid
        'If CIF Is Nothing Then
        '    Throw New Exception("CIF is required")
        'End If

        If ascx_cmb_ef_type.SelectedItemText Is Nothing Then
            Throw New Exception("Type is required")
        End If
        If txt_ef_number.Text.Trim = "" Then
            Throw New Exception("Identification Number is required")
        End If
        'If df_issue_date.RawValue Is Nothing Then
        '    Throw New Exception("Issue Date is required")
        'End If
        'If df_expiry_date.RawValue Is Nothing Then
        '    Throw New Exception("Expiry Date is required")
        'End If
        'If txt_ef_issued_by.Text.Trim = "" Then
        '    Throw New Exception("Issued By is required")
        'End If
        If ascx_cmb_ef_issue_country.SelectedItemText Is Nothing Then
            Throw New Exception("Issue Country is required")
        End If
        If Not IsNumber(txt_ef_number.Text) Then
            Throw New Exception("Identification Number is Invalid")
        End If
        'If txt_ef_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If

        Return True
    End Function
    Private Function IsNumber(text As String) As Boolean
        Try

            Dim number As New Regex("^[0-9 ]+$")
            Return number.IsMatch(text)

        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Sub LoadData(data As List(Of goAML_Ref_Customer_Entity_Identification)) Implements IGoAML_Grid(Of goAML_Ref_Customer_Entity_Identification).LoadData
        objListgoAML_Ref = data.Select(Function(x) x).ToList()
        objListgoAML_vw = data.Select(Function(x) x).ToList()

        If objListgoAML_vw IsNot Nothing Then
            Using objDB As New GoAMLDAL.GoAMLEntities
                Dim dtId As DataTable = NawaBLL.Common.CopyGenericToDataTable(objListgoAML_vw.ToList())
                Dim listCountry = objDB.goAML_Ref_Nama_Negara.ToList()
                For Each row In dtId.Rows
                    If Not IsDBNull(row("ISSUE_COUNTRY")) Then
                        Dim objCountry = listCountry.Find(Function(x) x.Kode.Equals(row("ISSUE_COUNTRY")))
                        If objCountry IsNot Nothing Then
                            row("ISSUE_COUNTRY") = objCountry.Kode & " - " & objCountry.Keterangan
                        End If
                    End If
                Next
                Store.DataSource = dtId
            End Using
            Store.DataBind()
        Else
            Store.DataBind()
        End If
    End Sub
End Class
