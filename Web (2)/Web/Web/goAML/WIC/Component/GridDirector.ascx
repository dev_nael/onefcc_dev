﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GridDirector.ascx.vb" Inherits="goAML_WIC_Component_GridDirector" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<%@ Register Src="~/goAML/WIC/Component/GridEmail.ascx" TagPrefix="nds" TagName="GridEmail" %>
<%@ Register Src="~/goAML/WIC/Component/GridSanction.ascx" TagPrefix="nds" TagName="GridSanction" %>
<%@ Register Src="~/goAML/WIC/Component/GridPEP.ascx" TagPrefix="nds" TagName="GridPEP" %>
<%@ Register Src="~/goAML/WIC/Component/GridIdentification.ascx" TagPrefix="nds" TagName="GridIdentification" %>
<%@ Register Src="~/goAML/WIC/Component/GridPhone.ascx" TagPrefix="nds" TagName="GridPhone" %>
<%@ Register Src="~/goAML/WIC/Component/GridAddress.ascx" TagPrefix="nds" TagName="GridAddress" %>

<%--Corp Director--%>
<ext:GridPanel ID="GridPanel" runat="server" Title="Director" AutoScroll="true">
    <TopBar>
        <ext:Toolbar ID="toolbar7" runat="server">
            <Items>
                <ext:Button ID="btnAdd" runat="server" Text="Add Director" Icon="Add">
                    <DirectEvents>
                        <Click OnEvent="btnAdd_DirectClick">
                            <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Items>
        </ext:Toolbar>
    </TopBar>
    <Store>
        <ext:Store ID="Store" runat="server" IsPagingStore="true" PageSize="8">
            <Model>
                <ext:Model ID="Model_Director_Corp" runat="server">
                    <Fields>
                        <ext:ModelField Name="PK_goAML_Ref_Customer_Entity_Director_ID" Type="String"></ext:ModelField>
                        <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                        <ext:ModelField Name="Role" Type="String"></ext:ModelField>
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
            <ext:CommandColumn ID="CommandColumn8" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                    <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                    <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                <Commands>
                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GrdCmd">
                        <ExtraParams>
                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                        </ExtraParams>
                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                        </Confirmation>
                    </Command>
                </DirectEvents>
            </ext:CommandColumn>
            <ext:Column ID="Column31" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
            <ext:Column ID="Column32" runat="server" DataIndex="Role" Text="Role" MinWidth="130" Flex="1"></ext:Column>
        </Columns>
    </ColumnModel>
    <BottomBar>
        <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
    </BottomBar>
</ext:GridPanel>
<%--end of Corp Director--%>

<ext:Window ID="WindowDetail" runat="server" Modal="true" Icon="ApplicationViewDetail" Title="Customer Director Add" Hidden="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout">
    <Items>
        <ext:FieldSet ID="FieldSet3" runat="server" Title="Director" Collapsible="true" Layout="FitLayout">
            <Items>
            </Items>

            <Content>
                <ext:FormPanel ID="FormPanel" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                    <Items>
                        <ext:Hidden runat="server" ID="Hidden2"></ext:Hidden>
                        <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                        <ext:Panel ID="Pnl_cmb_peran" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:NDSDropDownField ID="cmb_peran" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Peran_orang_dalam_Korporasi" StringFilter="Active = 1" EmptyText="Select One" Label="Role" AnchorHorizontal="70%" AllowBlank="false" />
                            </Content>
                        </ext:Panel>
                        <%--End Update--%>
                        <ext:TextField ID="txt_Director_Title" LabelWidth="250" runat="server" FieldLabel="Title" AnchorHorizontal="90%"></ext:TextField>
                        <ext:TextField ID="txt_Director_Last_Name" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="90%"></ext:TextField>
                        <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                        <ext:Panel ID="Pnl_cmb_Director_Gender" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:NDSDropDownField ID="cmb_Director_Gender" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Jenis_Kelamin" StringFilter="Active = 1" EmptyText="Select One" Label="Gender" AnchorHorizontal="70%" AllowBlank="false" />
                            </Content>
                        </ext:Panel>
                        <ext:DateField Editable="false" LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Date of Birth" ID="txt_Director_BirthDate" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                        <ext:TextField ID="txt_Director_Birth_Place" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="90%"></ext:TextField>
                        <ext:TextField ID="txt_Director_Mothers_Name" LabelWidth="250" runat="server" FieldLabel="Mother Maiden Name" AnchorHorizontal="90%"></ext:TextField>
                        <ext:TextField ID="txt_Director_Alias" LabelWidth="250" runat="server" FieldLabel="Alias" AnchorHorizontal="90%"></ext:TextField>
                        <ext:TextField ID="txt_Director_SSN" LabelWidth="250" runat="server" FieldLabel="NIK/SSN" AnchorHorizontal="90%"></ext:TextField>
                        <ext:TextField ID="txt_Director_Passport_Number" LabelWidth="250" runat="server" FieldLabel="Passport No" AnchorHorizontal="90%"></ext:TextField>
                        <%--Update: Zikri_11092020 Mengubah Passport Issued Country menjadi dropdown--%>
                        <ext:Panel ID="Pnl_cmb_Director_Passport_Country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:NDSDropDownField ID="cmb_Director_Passport_Country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Passport Issued Country" AnchorHorizontal="70%" />
                            </Content>
                        </ext:Panel>
                        <%--end Update--%>
                        <ext:TextField ID="txt_Director_ID_Number" LabelWidth="250" runat="server" FieldLabel="Other ID Number" AnchorHorizontal="90%"></ext:TextField>
                        <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                        <ext:Panel ID="Pnl_cmb_Director_Nationality1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:NDSDropDownField ID="cmb_Director_Nationality1" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 1" AnchorHorizontal="70%" AllowBlank="false" />
                            </Content>
                        </ext:Panel>
                        <ext:Panel ID="Pnl_cmb_Director_Nationality2" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:NDSDropDownField ID="cmb_Director_Nationality2" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 2" AnchorHorizontal="70%" />
                            </Content>
                        </ext:Panel>
                        <ext:Panel ID="Pnl_cmb_Director_Nationality3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:NDSDropDownField ID="cmb_Director_Nationality3" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 3" AnchorHorizontal="70%" />
                            </Content>
                        </ext:Panel>
                        <ext:Panel ID="Pnl_cmb_Director_Residence" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:NDSDropDownField ID="cmb_Director_Residence" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Domicile Country" AnchorHorizontal="70%" AllowBlank="false" />
                            </Content>
                        </ext:Panel>
                        <%--End Update--%>
                        <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Director_Deceased" FieldLabel="Has Passed Away?">
                            <DirectEvents>
                                <Change OnEvent="checkBoxcb_Director_Deceased_click" />
                            </DirectEvents>
                        </ext:Checkbox>
                        <ext:DateField Editable="false" runat="server" LabelWidth="250" Hidden="true" FieldLabel="Date of Passed Away " Format="dd-MMM-yyyy" ID="txt_Director_Deceased_Date" AnchorHorizontal="90%" />
                        <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Director_Tax_Reg_Number" FieldLabel="Is PEP?">
                        </ext:Checkbox>
                        <ext:TextField runat="server" LabelWidth="250" FieldLabel="Tax Number" ID="txt_Director_Tax_Number" AnchorHorizontal="90%" />
                        <ext:TextField runat="server" LabelWidth="250" AllowBlank="false" FieldLabel="Source of Wealth" ID="txt_Director_Source_of_Wealth" AnchorHorizontal="90%" />
                        <ext:TextField ID="txt_Director_Occupation" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%"></ext:TextField>

                        <ext:TextField runat="server" LabelWidth="250" FieldLabel="Notes" ID="txt_Director_Comments" AnchorHorizontal="90%" />
                        <ext:TextField ID="txt_Director_employer_name" LabelWidth="250" runat="server" FieldLabel="Work Place" AnchorHorizontal="90%"></ext:TextField>

                        <ext:Panel ID="Panel_Director_GridIdentification" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:GridIdentification runat="server" ID="GridIdentification_Director" FK_REF_DETAIL_OF="31"/>
                            </Content>
                        </ext:Panel>

                        <ext:Panel ID="Panel_Director_GridPhone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:GridPhone runat="server" ID="GridPhone_Director" FK_REF_DETAIL_OF="31"/>
                            </Content>
                        </ext:Panel>

                        <ext:Panel ID="Panel_Director_GridAddress" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:GridAddress runat="server" ID="GridAddress_Director" FK_REF_DETAIL_OF="31"/>
                            </Content>
                        </ext:Panel>

                        <ext:Panel ID="Panel_Director_GridPhoneWork" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:GridPhone runat="server" ID="GridPhoneWork_Director" FK_REF_DETAIL_OF="31"/>
                            </Content>
                        </ext:Panel>

                        <ext:Panel ID="Panel_Director_GridAddressWork" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:GridAddress runat="server" ID="GridAddressWork_Director" FK_REF_DETAIL_OF="31"/>
                            </Content>
                        </ext:Panel>

                        <ext:Panel ID="Panel_Director_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:GridEmail runat="server" ID="GridEmail_Director" FK_REF_DETAIL_OF="31"/>
                            </Content>
                        </ext:Panel>

                        <ext:Panel ID="Panel_Director_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:GridSanction runat="server" ID="GridSanction_Director"/>
                            </Content>
                        </ext:Panel>

                        <ext:Panel ID="Panel_Director_GridPEP" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                            <Content>
                                <nds:GridPEP runat="server" ID="GridPEP_Director"/>
                            </Content>
                        </ext:Panel>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnSave" runat="server" Text="Save" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnSave_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button ID="btnBack" runat="server" Text="Back" DefaultAlign="center">
                            <DirectEvents>
                                <Click OnEvent="btnBack_DirectEvent">
                                    <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>

                    </Buttons>
                </ext:FormPanel>
            </Content>
        </ext:FieldSet>
    </Items>
</ext:Window>