﻿Imports GoAMLBLL
Imports GoAMLBLL.goAML.DataModel
Imports GoAMLBLL.Helper
Imports System.Data

Partial Class goAML_WIC_Component_GridPhoneWork
    Inherits System.Web.UI.UserControl
    Implements IGoAML_Grid(Of goAML_Ref_Phone)

    Private Const GRID_TITLE As String = "Phone"
    Private Const SESSION_PREFIX As String = "CUSTOMER.GridPhoneWork."
    Private _uniqueName As String
    Public Property UniqueName As String Implements IGoAML_Grid(Of goAML_Ref_Phone).UniqueName
        Get
            'Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME)
            Return _uniqueName
        End Get
        Set(value As String)
            'Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME) = value
            _uniqueName = value
        End Set
    End Property

    Public Property FK_REF_DETAIL_OF As Integer Implements IGoAML_Grid(Of goAML_Ref_Phone).FK_REF_DETAIL_OF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_FK_REF_DETAIL_OF & UniqueName)
        End Get
        Set(value As Integer)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_FK_REF_DETAIL_OF & UniqueName) = value
        End Set
    End Property

    Public Property CIF As String Implements IGoAML_Grid(Of goAML_Ref_Phone).CIF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName)
        End Get
        Set(value As String)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_Edit As goAML_Ref_Phone Implements IGoAML_Grid(Of goAML_Ref_Phone).objTemp_Edit
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName)
        End Get
        Set(value As goAML_Ref_Phone)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref As goAML_Ref_Phone Implements IGoAML_Grid(Of goAML_Ref_Phone).objTemp_goAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName)
        End Get
        Set(value As goAML_Ref_Phone)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_Ref As List(Of goAML_Ref_Phone) Implements IGoAML_Grid(Of goAML_Ref_Phone).objListgoAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_vw As List(Of goAML_Ref_Phone) Implements IGoAML_Grid(Of goAML_Ref_Phone).objListgoAML_vw
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName) = value
        End Set
    End Property

    Private _title As String
    Public Property Title As String Implements IGoAML_Grid(Of goAML_Ref_Phone).Title
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Public ReadOnly Property SessionPrefix As String Implements IGoAML_Grid(Of goAML_Ref_Phone).SessionPrefix
        Get
            Return SESSION_PREFIX
        End Get
    End Property

    Public Property IsViewMode As Boolean Implements IGoAML_Grid(Of goAML_Ref_Phone).IsViewMode
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName)
        End Get
        Set(ByVal value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName) = value
        End Set
    End Property

    Public Property IsCustomer As Boolean
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_CUSTOMER & UniqueName)
        End Get
        Set(ByVal value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_CUSTOMER & UniqueName) = value
        End Set
    End Property

    Public Property IsDataChange As Boolean Implements IGoAML_Grid(Of goAML_Ref_Phone).IsDataChange
        Get
            If Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) Is Nothing Then
                Return False
            End If
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName)
        End Get
        Set(value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) = value
        End Set
    End Property

    Private Shadows Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GridPanel.Title = Title

        'If IsViewMode Then
        '    Dim cmdColumn As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        '    If cmdColumn IsNot Nothing Then
        '        Dim cmd1 As GridCommand = cmdColumn.Commands(1)
        '        Dim cmd2 As GridCommand = cmdColumn.Commands(2)
        '        cmd1.Hidden = IsViewMode
        '        cmd2.Hidden = IsViewMode
        '    End If

        '    btnAdd.Hidden = IsViewMode
        'End If
    End Sub
    Private Shadows Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        GridPanel.Title = Title

        Dim cmdColumnEdit As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        Dim cmdColumnDetail As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(2), CommandColumn)
        cmdColumnEdit.Hidden = IsViewMode
        cmdColumnDetail.Hidden = Not IsViewMode

        'Dim store As Store = TryCast(GridPanel.Store(0), Store)
        'store.DataBind()

        btnAdd.Hidden = IsViewMode
    End Sub

    Public Sub btnAdd_DirectClick(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Phone).btnAdd_DirectClick
        Try
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            btnSave.Hidden = False
            cb_phone_Contact_Type_Work.IsReadOnly = False
            cb_phone_Communication_Type_Work.IsReadOnly = False
            txt_phone_Country_prefix.ReadOnly = False
            txt_phone_number.ReadOnly = False
            txt_phone_extension.ReadOnly = False
            txt_phone_comments.ReadOnly = False
            btnSave.Hidden = False
            WindowDetail.Title = Title & " Phone Add"

            Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

            Clearinput()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnSave_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Phone).btnSave_DirectEvent
        Try

            If IsData_Valid() Then
                If objTemp_Edit Is Nothing Then
                    Save()
                Else
                    Save(False)
                End If
            End If
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            Clearinput()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnBack_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Phone).btnBack_DirectEvent
        Try
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            Clearinput()
            objTemp_Edit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub GrdCmd(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Phone).GrdCmd
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub LoadData(id As Long, isEdit As Boolean) Implements IGoAML_Grid(Of goAML_Ref_Phone).LoadData
        If isEdit Then
            objTemp_goAML_Ref = objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            objTemp_Edit = objListgoAML_vw.Find(Function(x) x.PK_goAML_Ref_Phone = id)


            Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

            If Not objTemp_Edit Is Nothing Then
                FormPanelTaskDetail.Hidden = False
                WindowDetail.Hidden = False
                cb_phone_Contact_Type_Work.IsReadOnly = False
                cb_phone_Communication_Type_Work.IsReadOnly = False
                txt_phone_Country_prefix.ReadOnly = False
                txt_phone_number.ReadOnly = False
                txt_phone_extension.ReadOnly = False
                txt_phone_comments.ReadOnly = False
                btnSave.Hidden = False

                WindowDetail.Title = Title & " Edit"
                Clearinput()
                With objTemp_Edit
                    Dim obj_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                    If obj_contact_type IsNot Nothing Then
                        cb_phone_Contact_Type_Work.SetTextWithTextValue(obj_contact_type.Kode, obj_contact_type.Keterangan)
                    End If

                    Dim obj_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                    If obj_communication_type IsNot Nothing Then
                        cb_phone_Communication_Type_Work.SetTextWithTextValue(obj_communication_type.Kode, obj_communication_type.Keterangan)
                    End If

                    txt_phone_Country_prefix.Text = .tph_country_prefix
                    txt_phone_number.Text = .tph_number
                    txt_phone_extension.Text = .tph_extension
                    txt_phone_comments.Text = .comments
                End With
            End If
        Else
            Dim objCustomerPhonesDetail As goAML_Ref_Phone = objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Phone = id)


            Dim Phone_Contact_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Phone_Communication_Type As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

            If Not objCustomerPhonesDetail Is Nothing Then
                FormPanelTaskDetail.Hidden = False
                WindowDetail.Hidden = False
                cb_phone_Contact_Type_Work.IsReadOnly = True
                cb_phone_Communication_Type_Work.IsReadOnly = True
                txt_phone_Country_prefix.ReadOnly = True
                txt_phone_number.ReadOnly = True
                txt_phone_extension.ReadOnly = True
                txt_phone_comments.ReadOnly = True
                btnSave.Hidden = True

                WindowDetail.Title = Title & " Detail"
                Clearinput()
                With objCustomerPhonesDetail
                    Dim obj_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                    If obj_contact_type IsNot Nothing Then
                        cb_phone_Contact_Type_Work.SetTextWithTextValue(obj_contact_type.Kode, obj_contact_type.Keterangan)
                    End If
                    Dim obj_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                    If obj_communication_type IsNot Nothing Then
                        cb_phone_Communication_Type_Work.SetTextWithTextValue(obj_communication_type.Kode, obj_communication_type.Keterangan)
                    End If

                    txt_phone_Country_prefix.Text = .tph_country_prefix
                    txt_phone_number.Text = .tph_number
                    txt_phone_extension.Text = .tph_extension
                    txt_phone_comments.Text = .comments
                End With

            End If
        End If
    End Sub

    Public Sub DeleteRecord(id As Long) Implements IGoAML_Grid(Of goAML_Ref_Phone).DeleteRecord
        Dim objDelVWPhone = objListgoAML_vw.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objxDelPhone = objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        If Not objxDelPhone Is Nothing Then
            objListgoAML_Ref.Remove(objxDelPhone)
        End If

        If Not objDelVWPhone Is Nothing Then
            objListgoAML_vw.Remove(objDelVWPhone)

            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()
        End If

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        Clearinput()
    End Sub

    Public Sub Clearinput() Implements IGoAML_Grid(Of goAML_Ref_Phone).Clearinput
        cb_phone_Contact_Type_Work.SetTextValue("")
        cb_phone_Communication_Type_Work.SetTextValue("")
        txt_phone_Country_prefix.Text = ""
        txt_phone_number.Text = ""
        txt_phone_extension.Text = ""
        txt_phone_comments.Text = ""
    End Sub

    Public Sub Save(Optional isNew As Boolean = True) Implements IGoAML_Grid(Of goAML_Ref_Phone).Save
        If Not isNew Then
            With objTemp_Edit
                .Tph_Contact_Type = cb_phone_Contact_Type_Work.SelectedItemValue
                '.Contact_Type = cb_phone_Contact_Type_Work.SelectedItemText
                .Tph_Communication_Type = cb_phone_Communication_Type_Work.SelectedItemValue
                '.Communcation_Type = cb_phone_Communication_Type_Work.SelectedItemText
                .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
                .tph_number = txt_phone_number.Text.Trim
                .tph_extension = txt_phone_extension.Text.Trim
                .comments = txt_phone_comments.Text.Trim
            End With

            With objTemp_goAML_Ref
                .Tph_Contact_Type = cb_phone_Contact_Type_Work.SelectedItemValue
                .Tph_Communication_Type = cb_phone_Communication_Type_Work.SelectedItemValue
                .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
                .tph_number = txt_phone_number.Text.Trim
                .tph_extension = txt_phone_extension.Text.Trim
                .comments = txt_phone_comments.Text.Trim
            End With

            objTemp_Edit = Nothing
            objTemp_goAML_Ref = Nothing

            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()

            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            Clearinput()
        Else
            Dim objNewVWgoAML_Customer As New goAML_Ref_Phone
            Dim objNewgoAML_Customer As New goAML_Ref_Phone
            Dim objRand As New Random

            Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
            While Not objListgoAML_Ref.Find(Function(x) x.PK_goAML_Ref_Phone = intpk) Is Nothing
                intpk = objRand.Next
            End While

            With objNewVWgoAML_Customer
                .PK_goAML_Ref_Phone = intpk
                .FK_Ref_Detail_Of = FK_REF_DETAIL_OF
                '.FK_for_Table_ID = obj_customer.PK_Customer_ID
                .Tph_Contact_Type = cb_phone_Contact_Type_Work.SelectedItemValue
                '.Contact_Type = cb_phone_Contact_Type_Work.SelectedItemText
                .Tph_Communication_Type = cb_phone_Communication_Type_Work.SelectedItemValue
                '.Communcation_Type = cb_phone_Communication_Type_Work.SelectedItemText
                .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
                .tph_number = txt_phone_number.Text.Trim
                .tph_extension = txt_phone_extension.Text.Trim
                .comments = txt_phone_comments.Text.Trim
                .IsCustomer = IsCustomer
            End With

            With objNewgoAML_Customer
                .PK_goAML_Ref_Phone = intpk
                .FK_Ref_Detail_Of = FK_REF_DETAIL_OF
                '.FK_for_Table_ID = obj_customer.PK_Customer_ID
                .Tph_Contact_Type = cb_phone_Contact_Type_Work.SelectedItemValue
                .Tph_Communication_Type = cb_phone_Communication_Type_Work.SelectedItemValue
                .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
                .tph_number = txt_phone_number.Text.Trim
                .tph_extension = txt_phone_extension.Text.Trim
                .comments = txt_phone_comments.Text.Trim
                .IsCustomer = IsCustomer
            End With

            objListgoAML_vw.Add(objNewVWgoAML_Customer)
            objListgoAML_Ref = objListgoAML_vw

            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()

            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            Clearinput()
            IsDataChange = True
        End If
    End Sub

    Public Function IsData_Valid() As Boolean Implements IGoAML_Grid(Of goAML_Ref_Phone).IsData_Valid
        If cb_phone_Contact_Type_Work.SelectedItemValue = "" Then
            Throw New Exception("Contact Type is required")
        End If
        If cb_phone_Communication_Type_Work.SelectedItemValue = "" Then
            Throw New Exception("Communication Type is required")
        End If
        If txt_phone_Country_prefix.Text.Length > 3 Then
            Throw New Exception("Max Country Prefix is 3")
        End If
        If txt_phone_extension.Text.Length > 9 Then
            Throw New Exception("Max Phone Extension is 9")
        End If
        If txt_phone_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        If Not CommonHelper.IsNumber(txt_phone_number.Text) Then
            Throw New Exception("Phone Number is Invalid")
        End If
        Return True
    End Function

    Public Sub LoadData(data As List(Of goAML_Ref_Phone)) Implements IGoAML_Grid(Of goAML_Ref_Phone).LoadData
        objListgoAML_Ref = data.Select(Function(x) x).ToList()
        objListgoAML_vw = data.Select(Function(x) x).ToList()

        If objListgoAML_vw IsNot Nothing OrElse objListgoAML_vw.Count > 0 Then
            Using objDB As New GoAMLDAL.GoAMLEntities
                Dim dtPEP As DataTable = NawaBLL.Common.CopyGenericToDataTable(objListgoAML_vw.ToList())
                Dim listTipeKontak = objDB.goAML_Ref_Kategori_Kontak.ToList()
                For Each row In dtPEP.Rows
                    If Not IsDBNull(row("Tph_Contact_Type")) Then
                        Dim objContactType = listTipeKontak.Find(Function(x) x.Kode.Equals(row("Tph_Contact_Type")))
                        If objContactType IsNot Nothing Then
                            row("Tph_Contact_Type") = objContactType.Kode & " - " & objContactType.Keterangan
                        End If
                    End If
                Next
                Store.DataSource = dtPEP
            End Using
            Store.DataBind()
        Else
            Store.DataBind()
        End If
    End Sub
End Class
