﻿Imports System.Reflection.Emit
Imports GoAMLBLL
Imports GoAMLBLL.goAML.DataModel
Imports GoAMLBLL.goAML_CustomerDataBLL
Imports GoAMLBLL.Helper
Imports System.Data

Partial Class goAML_WIC_Component_GridIdentification
    Inherits System.Web.UI.UserControl
    Implements IGoAML_Grid(Of goAML_Person_Identification)

    Private Const GRID_TITLE As String = "Identification"
    Private Const SESSION_PREFIX As String = "CUSTOMER.GridIdentification."

    Private _uniqueName As String
    Public Property UniqueName As String Implements IGoAML_Grid(Of goAML_Person_Identification).UniqueName
        Get
            'Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME)
            Return _uniqueName
        End Get
        Set(value As String)
            'Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME) = value
            _uniqueName = value
        End Set
    End Property

    Public Property FK_REF_DETAIL_OF As Integer Implements IGoAML_Grid(Of goAML_Person_Identification).FK_REF_DETAIL_OF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_FK_REF_DETAIL_OF & UniqueName)
        End Get
        Set(value As Integer)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_FK_REF_DETAIL_OF & UniqueName) = value
        End Set
    End Property

    Public Property CIF As String Implements IGoAML_Grid(Of goAML_Person_Identification).CIF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName)
        End Get
        Set(value As String)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_Edit As goAML_Person_Identification Implements IGoAML_Grid(Of goAML_Person_Identification).objTemp_Edit
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName)
        End Get
        Set(value As goAML_Person_Identification)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref As goAML_Person_Identification Implements IGoAML_Grid(Of goAML_Person_Identification).objTemp_goAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName)
        End Get
        Set(value As goAML_Person_Identification)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_Ref As List(Of goAML_Person_Identification) Implements IGoAML_Grid(Of goAML_Person_Identification).objListgoAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName)
        End Get
        Set(value As List(Of goAML_Person_Identification))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_vw As List(Of goAML_Person_Identification) Implements IGoAML_Grid(Of goAML_Person_Identification).objListgoAML_vw
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName)
        End Get
        Set(value As List(Of goAML_Person_Identification))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName) = value
        End Set
    End Property

    Private _title As String
    Public Property Title As String Implements IGoAML_Grid(Of goAML_Person_Identification).Title
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Public ReadOnly Property SessionPrefix As String Implements IGoAML_Grid(Of goAML_Person_Identification).SessionPrefix
        Get
            Return SESSION_PREFIX
        End Get
    End Property

    Public Property IsViewMode As Boolean Implements IGoAML_Grid(Of goAML_Person_Identification).IsViewMode
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName)
        End Get
        Set(ByVal value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName) = value
        End Set
    End Property

    Public Property IsCustomer As Boolean
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_CUSTOMER & UniqueName)
        End Get
        Set(ByVal value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_CUSTOMER & UniqueName) = value
        End Set
    End Property

    Public Property IsDataChange As Boolean Implements IGoAML_Grid(Of goAML_Person_Identification).IsDataChange
        Get
            If Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) Is Nothing Then
                Return False
            End If
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName)
        End Get
        Set(value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) = value
        End Set
    End Property

    Private Shadows Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If IsViewMode Then
        '    Dim cmdColumn As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        '    If cmdColumn IsNot Nothing Then
        '        Dim cmd1 As GridCommand = cmdColumn.Commands(1)
        '        Dim cmd2 As GridCommand = cmdColumn.Commands(2)
        '        cmd1.Hidden = IsViewMode
        '        cmd2.Hidden = IsViewMode
        '    End If

        '    btnAdd.Hidden = IsViewMode
        'End If
    End Sub
    Private Shadows Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Dim cmdColumnEdit As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        Dim cmdColumnDetail As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(2), CommandColumn)
        cmdColumnEdit.Hidden = IsViewMode
        cmdColumnDetail.Hidden = Not IsViewMode

        'Dim store As Store = TryCast(GridPanel.Store(0), Store)
        'store.DataBind()

        btnAdd.Hidden = IsViewMode
    End Sub

    Public Sub btnAdd_DirectClick(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Person_Identification).btnAdd_DirectClick
        Try
            FormPanel.Hidden = False
            WindowDetail.Hidden = False
            ascx_cmb_Jenis_Dokumen.IsReadOnly = False
            txt_identification_comment.ReadOnly = False
            txt_issued_by.ReadOnly = False
            txt_issued_date.ReadOnly = False
            txt_issued_expired_date.ReadOnly = False
            ascx_cmb_issued_country.IsReadOnly = False
            txt_number_identification.ReadOnly = False
            btnSave.Hidden = False
            WindowDetail.Title = Title & " Add"

            Clearinput()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnSave_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Person_Identification).btnSave_DirectEvent
        Try
            If IsData_Valid() Then
                If objTemp_Edit Is Nothing Then
                    Save()
                Else
                    Save(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnBack_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Person_Identification).btnBack_DirectEvent
        Try
            FormPanel.Hidden = True
            WindowDetail.Hidden = True
            Clearinput()
            objTemp_Edit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub GrdCmd(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Person_Identification).GrdCmd
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub LoadData(id As Long, isEdit As Boolean) Implements IGoAML_Grid(Of goAML_Person_Identification).LoadData
        If isEdit Then
            objTemp_goAML_Ref = objListgoAML_Ref.Find(Function(x) x.PK_Person_Identification_ID = id)
            objTemp_Edit = objListgoAML_vw.Find(Function(x) x.PK_Person_Identification_ID = id)


            If Not objTemp_Edit Is Nothing Then
                FormPanel.Hidden = False
                WindowDetail.Hidden = False
                btnSave.Hidden = False
                ascx_cmb_Jenis_Dokumen.IsReadOnly = False
                txt_issued_by.ReadOnly = False
                txt_issued_date.ReadOnly = False
                txt_issued_expired_date.ReadOnly = False
                txt_identification_comment.ReadOnly = False
                ascx_cmb_issued_country.IsReadOnly = False
                txt_number_identification.ReadOnly = False
                WindowDetail.Title = Title & " Edit"
                Clearinput()
                With objTemp_Edit
                    Dim jenisdokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(x) x.Kode = .Type).FirstOrDefault()
                    If jenisdokumen IsNot Nothing Then
                        ascx_cmb_Jenis_Dokumen.SetTextWithTextValue(jenisdokumen.Kode, jenisdokumen.Keterangan)
                    End If
                    ascx_cmb_Jenis_Dokumen.SelectedItemValue = .Type
                    txt_issued_by.Text = .Issued_By
                    txt_issued_date.Value = IIf(.Issue_Date Is Nothing, "", .Issue_Date)
                    txt_issued_expired_date.Value = IIf(.Expiry_Date Is Nothing, "", .Expiry_Date)
                    txt_identification_comment.Text = .Identification_Comment

                    Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(.Issued_Country)
                    If kodenegara IsNot Nothing Then
                        ascx_cmb_issued_country.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
                    End If

                    txt_number_identification.Text = .Number
                End With
            End If
        Else
            Dim objCustomerIdentificationDetail = objListgoAML_vw.Where(Function(x) x.PK_Person_Identification_ID = id).FirstOrDefault()

            If Not objCustomerIdentificationDetail Is Nothing Then
                FormPanel.Hidden = False
                WindowDetail.Hidden = False
                btnSave.Hidden = True
                WindowDetail.Title = Title & " Detail"
                txt_number_identification.ReadOnly = True
                ascx_cmb_Jenis_Dokumen.IsReadOnly = True
                ascx_cmb_issued_country.IsReadOnly = True
                txt_issued_by.ReadOnly = True
                txt_issued_date.ReadOnly = True
                txt_issued_expired_date.ReadOnly = True
                txt_identification_comment.ReadOnly = True
                Clearinput()
                txt_number_identification.Text = objCustomerIdentificationDetail.Number

                Dim jenisdokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(x) x.Kode = objCustomerIdentificationDetail.Type).FirstOrDefault()
                If jenisdokumen IsNot Nothing Then
                    ascx_cmb_Jenis_Dokumen.SetTextWithTextValue(jenisdokumen.Kode, jenisdokumen.Keterangan)
                End If

                Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerIdentificationDetail.Issued_Country)
                If kodenegara IsNot Nothing Then
                    ascx_cmb_issued_country.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
                End If

                txt_issued_by.Text = objCustomerIdentificationDetail.Issued_By
                txt_issued_date.Value = IIf(objCustomerIdentificationDetail.Issue_Date Is Nothing, "", objCustomerIdentificationDetail.Issue_Date)
                txt_issued_expired_date.Value = IIf(objCustomerIdentificationDetail.Expiry_Date Is Nothing, "", objCustomerIdentificationDetail.Expiry_Date)
                txt_identification_comment.Text = objCustomerIdentificationDetail.Identification_Comment
            End If
        End If
    End Sub

    Public Sub DeleteRecord(id As Long) Implements IGoAML_Grid(Of goAML_Person_Identification).DeleteRecord
        Dim objDelVWIdentification = objListgoAML_vw.Find(Function(x) x.PK_Person_Identification_ID = id)
        Dim objxDelIdentification = objListgoAML_Ref.Find(Function(x) x.PK_Person_Identification_ID = id)

        If Not objxDelIdentification Is Nothing Then
            objListgoAML_Ref.Remove(objxDelIdentification)
        End If

        If Not objDelVWIdentification Is Nothing Then
            objListgoAML_vw.Remove(objDelVWIdentification)


            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()
        End If

        FormPanel.Hidden = True
        WindowDetail.Hidden = True
        Clearinput()
    End Sub

    Public Sub Clearinput() Implements IGoAML_Grid(Of goAML_Person_Identification).Clearinput
        ascx_cmb_Jenis_Dokumen.SetTextValue("")
        txt_identification_comment.Text = ""
        txt_issued_by.Text = ""
        txt_issued_date.Text = ""
        txt_issued_expired_date.Text = ""
        ascx_cmb_issued_country.SetTextValue("")
        txt_number_identification.Text = ""
    End Sub

    Public Sub Save(Optional isNew As Boolean = True) Implements IGoAML_Grid(Of goAML_Person_Identification).Save
        If Not isNew Then
            With objTemp_Edit
                .isReportingPerson = 0
                .FK_Person_Type = 1
                '.FK_Person_ID = obj_customer.PK_Customer_ID
                .Number = txt_number_identification.Value
                .Issued_By = txt_issued_by.Value

                ' 2023-09-29, Nael: Cek juga apakah itu string kosong
                If (txt_issued_date.RawValue IsNot Nothing) OrElse (Not String.IsNullOrWhiteSpace(txt_issued_date.RawValue)) Then
                    .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
                Else
                    .Issue_Date = Nothing
                End If
                If txt_issued_expired_date.RawValue IsNot Nothing Then
                    .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
                Else
                    .Expiry_Date = Nothing
                End If
                .Identification_Comment = txt_identification_comment.Value
                .Issued_Country = ascx_cmb_issued_country.SelectedItemValue
                .Type = ascx_cmb_Jenis_Dokumen.SelectedItemValue
                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                .LastUpdateDate = DateTime.Now
            End With

            With objTemp_goAML_Ref
                .isReportingPerson = 0
                .FK_Person_Type = 1
                '.FK_Person_ID = obj_customer.PK_Customer_ID
                .Number = txt_number_identification.Value
                .Issued_By = txt_issued_by.Value
                If txt_issued_date.RawValue IsNot Nothing Then
                    .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
                Else
                    .Issue_Date = Nothing
                End If
                If txt_issued_expired_date.RawValue IsNot Nothing Then
                    .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
                Else
                    .Expiry_Date = Nothing
                End If
                .Identification_Comment = txt_identification_comment.Value
                .Issued_Country = ascx_cmb_issued_country.SelectedItemValue
                .Type = ascx_cmb_Jenis_Dokumen.SelectedItemValue

                .FK_REF_DETAIL_OF = FK_REF_DETAIL_OF
                .IsCustomer = IsCustomer
                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                .LastUpdateDate = DateTime.Now
            End With

            objTemp_Edit = Nothing
            objTemp_goAML_Ref = Nothing

            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()

            FormPanel.Hidden = True
            WindowDetail.Hidden = True
            Clearinput()
            ' 2023-10-04, Nael: IsDataChange dipindahin ke scope setelah IfElse, mau data diedit atau berubaha, data tetap mengalami perubahan
            'IsDataChange = True
        Else
            Dim objNewVWgoAML_Customer As New goAML_Person_Identification
            Dim objNewgoAML_Customer As New goAML_Person_Identification
            Dim objRand As New Random

            Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
            While Not objListgoAML_Ref.Find(Function(x) x.PK_Person_Identification_ID = intpk) Is Nothing
                intpk = objRand.Next
            End While

            With objNewVWgoAML_Customer
                .PK_Person_Identification_ID = intpk
                .isReportingPerson = 0
                .FK_Person_Type = 1
                '.FK_Person_ID = obj_customer.PK_Customer_ID
                .Number = txt_number_identification.Value
                .Issued_By = txt_issued_by.Value
                If (txt_issued_date.RawValue IsNot Nothing) OrElse (Not String.IsNullOrWhiteSpace(txt_issued_date.RawValue)) Then
                    .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
                Else
                    .Issue_Date = Nothing
                End If
                If txt_issued_expired_date.RawValue IsNot Nothing OrElse (Not String.IsNullOrWhiteSpace(txt_issued_expired_date.RawValue)) Then
                    .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
                Else
                    .Expiry_Date = Nothing
                End If
                .Identification_Comment = txt_identification_comment.Value
                .Issued_Country = ascx_cmb_issued_country.SelectedItemValue
                .Type = ascx_cmb_Jenis_Dokumen.SelectedItemValue
                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                .CreatedDate = DateTime.Now
                .Active = True
            End With

            With objNewgoAML_Customer
                .PK_Person_Identification_ID = intpk
                .isReportingPerson = 0
                .FK_Person_Type = 1
                '.FK_Person_ID = obj_customer.PK_Customer_ID
                .Number = txt_number_identification.Value
                .Issued_Country = ascx_cmb_issued_country.SelectedItemValue
                .Issued_By = txt_issued_by.Value
                If txt_issued_date.RawValue IsNot Nothing Then
                    .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
                Else
                    .Issue_Date = Nothing
                End If
                If txt_issued_expired_date.RawValue IsNot Nothing Then
                    .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
                Else
                    .Expiry_Date = Nothing
                End If
                .Identification_Comment = txt_identification_comment.Value
                .Issued_Country = ascx_cmb_issued_country.SelectedItemValue
                .Type = ascx_cmb_Jenis_Dokumen.SelectedItemValue

                .FK_REF_DETAIL_OF = FK_REF_DETAIL_OF
                .IsCustomer = IsCustomer
                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                .CreatedDate = DateTime.Now
                .Active = True
            End With

            objListgoAML_vw.Add(objNewVWgoAML_Customer)
            objListgoAML_Ref = objListgoAML_vw

            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()

            FormPanel.Hidden = True
            WindowDetail.Hidden = True
            Clearinput()
        End If
        ' 2023-10-04, Nael: IsDataChange dipindahin ke scope setelah IfElse, mau data diedit atau berubaha, data tetap mengalami perubahan
        IsDataChange = True
    End Sub

    Public Function IsData_Valid() As Boolean Implements IGoAML_Grid(Of goAML_Person_Identification).IsData_Valid
        If ascx_cmb_issued_country.SelectedItemValue = "" Then
            Throw New Exception("Country is required")
        End If
        If ascx_cmb_Jenis_Dokumen.SelectedItemValue = "" Then
            Throw New Exception("Identification Type is required")
        End If
        If txt_number_identification.Text.Trim = "" Then
            Throw New Exception("Number Identification is required")
        End If

        If Not CommonHelper.IsNumber(txt_number_identification.Text) Then
            Throw New Exception("Identification Number is Invalid")
        End If
        Return True
    End Function

    Public Sub LoadData(data As List(Of goAML_Person_Identification)) Implements IGoAML_Grid(Of goAML_Person_Identification).LoadData
        objListgoAML_Ref = data.Select(Function(x) x).ToList()
        objListgoAML_vw = data

        If objListgoAML_vw IsNot Nothing Then
            Using objDB As New GoAMLDAL.GoAMLEntities
                Dim dtId As DataTable = NawaBLL.Common.CopyGenericToDataTable(objListgoAML_vw.ToList())
                Dim listCountry = objDB.goAML_Ref_Nama_Negara.ToList()
                For Each row In dtId.Rows
                    If Not IsDBNull(row("Issued_Country")) Then
                        Dim objCountry = listCountry.Find(Function(x) x.Kode.Equals(row("Issued_Country")))
                        If objCountry IsNot Nothing Then
                            row("Issued_Country") = objCountry.Kode & " - " & objCountry.Keterangan
                        End If
                    End If
                Next
                Store.DataSource = dtId
            End Using
            Store.DataBind()
        Else
            Store.DataBind()
        End If
    End Sub
End Class
