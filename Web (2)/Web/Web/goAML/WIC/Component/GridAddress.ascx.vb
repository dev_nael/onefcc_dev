﻿Imports System.Reflection.Emit
Imports GoAMLBLL
Imports GoAMLBLL.goAML.DataModel
Imports System.Data

Partial Class goAML_WIC_Component_GridAddress
    Inherits System.Web.UI.UserControl
    Implements IGoAML_Grid(Of goAML_Ref_Address)

    'Private Const GRID_TITLE As String = "Address"
    Private Const SESSION_PREFIX As String = "CUSTOMER.GridAddress."

    Private _uniqueName As String
    Public Property UniqueName As String Implements IGoAML_Grid(Of goAML_Ref_Address).UniqueName
        Get
            'Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME)
            Return _uniqueName
        End Get
        Set(value As String)
            'Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_UNIQUE_NAME) = value
            _uniqueName = value
        End Set
    End Property

    Public Property FK_REF_DETAIL_OF As Integer Implements IGoAML_Grid(Of goAML_Ref_Address).FK_REF_DETAIL_OF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_FK_REF_DETAIL_OF & UniqueName)
        End Get
        Set(value As Integer)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_FK_REF_DETAIL_OF & UniqueName) = value
        End Set
    End Property

    Public Property CIF As String Implements IGoAML_Grid(Of goAML_Ref_Address).CIF
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName)
        End Get
        Set(ByVal value As String)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_CIF & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_Edit As goAML_Ref_Address Implements IGoAML_Grid(Of goAML_Ref_Address).objTemp_Edit
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName)
        End Get
        Set(value As goAML_Ref_Address)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_EDIT & UniqueName) = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref As goAML_Ref_Address Implements IGoAML_Grid(Of goAML_Ref_Address).objTemp_goAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName)
        End Get
        Set(value As goAML_Ref_Address)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJTEMP_GOAML_REF & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_Ref As List(Of goAML_Ref_Address) Implements IGoAML_Grid(Of goAML_Ref_Address).objListgoAML_Ref
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_Ref & UniqueName) = value
        End Set
    End Property

    Public Property objListgoAML_vw As List(Of goAML_Ref_Address) Implements IGoAML_Grid(Of goAML_Ref_Address).objListgoAML_vw
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName)
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_OBJLIST_GOAML_VW & UniqueName) = value
        End Set
    End Property

    Private _title As String
    Public Property Title As String Implements IGoAML_Grid(Of goAML_Ref_Address).Title
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Public ReadOnly Property SessionPrefix As String Implements IGoAML_Grid(Of goAML_Ref_Address).SessionPrefix
        Get
            Return SESSION_PREFIX
        End Get
    End Property

    Public Property IsViewMode As Boolean Implements IGoAML_Grid(Of goAML_Ref_Address).IsViewMode
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName)
        End Get
        Set(ByVal value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_VIEW_MODE & UniqueName) = value
        End Set
    End Property

    Public Property IsCustomer As Boolean
        Get
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_CUSTOMER & UniqueName)
        End Get
        Set(ByVal value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_CUSTOMER & UniqueName) = value
        End Set
    End Property

    Public Property IsDataChange As Boolean Implements IGoAML_Grid(Of goAML_Ref_Address).IsDataChange
        Get
            If Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) Is Nothing Then
                Return False
            End If
            Return Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName)
        End Get
        Set(value As Boolean)
            Session(SessionPrefix & GoAML_Grid_Session_Constant.SESSION_IS_DATA_CHANGE & UniqueName) = value
        End Set
    End Property

    Private Shadows Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'GridPanel.Title = Title

        'If Not Page.IsPostBack Then
        '    If IsViewMode Then
        '        Dim cmdColumn As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        '        If cmdColumn IsNot Nothing Then
        '            Dim cmd1 As GridCommand = cmdColumn.Commands(1)
        '            Dim cmd2 As GridCommand = cmdColumn.Commands(2)
        '            cmd1.Hidden = IsViewMode
        '            cmd2.Hidden = IsViewMode
        '        End If

        '        btnAdd.Hidden = IsViewMode
        '    End If
        'End If

    End Sub
    Private Shadows Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        GridPanel.Title = Title

        Dim cmdColumnEdit As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        Dim cmdColumnDetail As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(2), CommandColumn)
        cmdColumnEdit.Hidden = IsViewMode
        cmdColumnDetail.Hidden = Not IsViewMode

        'Dim store As Store = TryCast(GridPanel.Store(0), Store)
        'store.DataBind()

        btnAdd.Hidden = IsViewMode
    End Sub

    Public Sub btnAdd_DirectClick(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Address).btnAdd_DirectClick
        Try
            FormPanel.Hidden = False
            WindowDetail.Hidden = False
            ascx_cmb_kategoriaddress.IsReadOnly = False
            Address_INDV.ReadOnly = False
            Town_INDV.ReadOnly = False
            City_INDV.ReadOnly = False
            Zip_INDV.ReadOnly = False
            ascx_cmb_kodenegara.IsReadOnly = False
            State_INDVD.ReadOnly = False
            Comment_Address_INDVD.ReadOnly = False
            btnSave.Hidden = False
            WindowDetail.Title = Title & " Add"


            Clearinput()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnSave_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Address).btnSave_DirectEvent
        Try
            If IsData_Valid() Then
                If objTemp_Edit Is Nothing Then
                    Save()
                Else
                    Save(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub btnBack_DirectEvent(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Address).btnBack_DirectEvent
        Try
            FormPanel.Hidden = True
            WindowDetail.Hidden = True
            Clearinput()
            objTemp_Edit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub GrdCmd(sender As Object, e As DirectEventArgs) Implements IGoAML_Grid(Of goAML_Ref_Address).GrdCmd
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Public Sub LoadData(id As Long, isEdit As Boolean) Implements IGoAML_Grid(Of goAML_Ref_Address).LoadData
        If isEdit Then
            objTemp_goAML_Ref = objListgoAML_Ref.Find(Function(x) x.PK_Customer_Address_ID = id)
            objTemp_Edit = objListgoAML_vw.Find(Function(x) x.PK_Customer_Address_ID = id)

            Dim Address_Type As GoAMLDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Country As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing

            If Not objTemp_Edit Is Nothing Then
                FormPanel.Hidden = False
                WindowDetail.Hidden = False
                btnSave.Hidden = False

                btnSave.Hidden = False
                ascx_cmb_kategoriaddress.IsReadOnly = False
                Address_INDV.ReadOnly = False
                Town_INDV.ReadOnly = False
                City_INDV.ReadOnly = False
                Zip_INDV.ReadOnly = False
                ascx_cmb_kodenegara.IsReadOnly = False
                State_INDVD.ReadOnly = False
                Comment_Address_INDVD.ReadOnly = False
                WindowDetail.Title = Title & " Edit"
                Clearinput()
                With objTemp_Edit

                    Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                    If kategorikontak IsNot Nothing Then
                        ascx_cmb_kategoriaddress.SetTextWithTextValue(kategorikontak.Kode, kategorikontak.Keterangan)
                    End If

                    Address_INDV.Text = .Address
                    Town_INDV.Text = .Town
                    City_INDV.Text = .City
                    Zip_INDV.Text = .Zip

                    Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(.Country_Code)
                    If kodenegara IsNot Nothing Then
                        ascx_cmb_kodenegara.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
                    End If

                    State_INDVD.Text = .State
                    Comment_Address_INDVD.Text = .Comments
                End With

                'EnvironmentForm("Detail")
            End If
        Else
            Dim objCustomerPhonesDetail = objListgoAML_Ref.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
            If Not objCustomerPhonesDetail Is Nothing Then
                FormPanel.Hidden = False
                WindowDetail.Hidden = False
                btnSave.Hidden = True
                WindowDetail.Title = Title & " Detail"
                ascx_cmb_kategoriaddress.IsReadOnly = True
                Address_INDV.ReadOnly = True
                Town_INDV.ReadOnly = True
                City_INDV.ReadOnly = True
                Zip_INDV.ReadOnly = True
                ascx_cmb_kodenegara.IsReadOnly = True
                State_INDVD.ReadOnly = True
                Comment_Address_INDVD.ReadOnly = True
                Clearinput()

                Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(objCustomerPhonesDetail.Address_Type)
                If kategorikontak IsNot Nothing Then
                    ascx_cmb_kategoriaddress.SetTextWithTextValue(kategorikontak.Kode, kategorikontak.Keterangan)
                End If
                Address_INDV.Text = objCustomerPhonesDetail.Address
                Town_INDV.Text = objCustomerPhonesDetail.Town
                City_INDV.Text = objCustomerPhonesDetail.City
                Zip_INDV.Text = objCustomerPhonesDetail.Zip

                Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerPhonesDetail.Country_Code)
                If kodenegara IsNot Nothing Then
                    ascx_cmb_kodenegara.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
                End If
                State_INDVD.Text = objCustomerPhonesDetail.State
                Comment_Address_INDVD.Text = objCustomerPhonesDetail.Comments
            End If
        End If

    End Sub

    Public Sub DeleteRecord(id As Long) Implements IGoAML_Grid(Of goAML_Ref_Address).DeleteRecord
        Dim objDelVWAddress = objListgoAML_vw.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objxDelAddress = objListgoAML_Ref.Find(Function(x) x.PK_Customer_Address_ID = id)

        If Not objxDelAddress Is Nothing Then
            objListgoAML_Ref.Remove(objxDelAddress)
        End If

        If Not objDelVWAddress Is Nothing Then
            objListgoAML_vw.Remove(objDelVWAddress)

            Store.DataSource = objListgoAML_vw.ToList()
            Store.DataBind()
        End If

        FormPanel.Hidden = True
        WindowDetail.Hidden = True
        Clearinput()
    End Sub

    Public Sub Clearinput() Implements IGoAML_Grid(Of goAML_Ref_Address).Clearinput
        ascx_cmb_kategoriaddress.SetTextValue("")
        ascx_cmb_kodenegara.SetTextValue("")
        Address_INDV.Text = ""
        Town_INDV.Text = ""
        City_INDV.Text = ""
        Zip_INDV.Text = ""
        State_INDVD.Text = ""
        Comment_Address_INDVD.Text = ""
    End Sub

    Public Sub Save(Optional isNew As Boolean = True) Implements IGoAML_Grid(Of goAML_Ref_Address).Save
        If Not isNew Then
            With objTemp_Edit
                .Address_Type = ascx_cmb_kategoriaddress.SelectedItemValue
                '.Type_Address_Detail = ascx_cmb_kategoriaddress.SelectedItemText
                .Address = Address_INDV.Value
                .City = City_INDV.Value
                .Town = Town_INDV.Value
                .Country_Code = ascx_cmb_kodenegara.SelectedItemValue
                '.Country = ascx_cmb_kodenegara.SelectedItemText
                .State = State_INDVD.Value
                .Zip = Zip_INDV.Value
                .Comments = Comment_Address_INDVD.Text.Trim
            End With

            With objTemp_goAML_Ref
                .Address_Type = ascx_cmb_kategoriaddress.SelectedItemValue
                .City = City_INDV.Value
                .Address = Address_INDV.Value
                .Town = Town_INDV.Value
                .Country_Code = ascx_cmb_kodenegara.SelectedItemValue
                .State = State_INDVD.Value
                .Zip = Zip_INDV.Value
                .Comments = Comment_Address_INDVD.Text.Trim
            End With

            objTemp_Edit = Nothing
            objTemp_goAML_Ref = Nothing
        Else
            Try
                Dim objNewVWgoAML_Customer As New goAML_Ref_Address
                Dim objNewgoAML_Customer As New goAML_Ref_Address
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_Customer
                    .PK_Customer_Address_ID = intpk
                    .FK_Ref_Detail_Of = FK_REF_DETAIL_OF
                    .IsCustomer = IsCustomer
                    .Address_Type = ascx_cmb_kategoriaddress.SelectedItemValue
                    .Address = Address_INDV.Value
                    .City = City_INDV.Value
                    .Town = Town_INDV.Value
                    .Country_Code = ascx_cmb_kodenegara.SelectedItemValue
                    .Comments = Comment_Address_INDVD.Value
                    .State = State_INDVD.Value
                    .Zip = Zip_INDV.Value
                    '.Type_Address_Detail = ascx_cmb_kategoriaddress.SelectedItemText
                    '.Country = ascx_cmb_kodenegara.SelectedItemText
                    .Comments = Comment_Address_INDVD.Text.Trim
                End With

                With objNewgoAML_Customer
                    .PK_Customer_Address_ID = intpk
                    .FK_Ref_Detail_Of = FK_REF_DETAIL_OF
                    .IsCustomer = IsCustomer
                    '.FK_To_Table_ID = obj_customer.PK_Customer_ID
                    .Address_Type = ascx_cmb_kategoriaddress.SelectedItemValue
                    .City = City_INDV.Value
                    .Address = Address_INDV.Value
                    .Town = Town_INDV.Value
                    .Country_Code = ascx_cmb_kodenegara.SelectedItemValue
                    .Comments = Comment_Address_INDVD.Value
                    .State = State_INDVD.Value
                    .Zip = Zip_INDV.Value
                    .Comments = Comment_Address_INDVD.Text.Trim
                End With

                objListgoAML_vw.Add(objNewVWgoAML_Customer)
                objListgoAML_Ref = objListgoAML_vw
            Catch ex As Exception
                Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
                Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
            End Try
        End If

        Store.DataSource = objListgoAML_vw.ToList()
        Store.DataBind()

        FormPanel.Hidden = True
        WindowDetail.Hidden = True
        Clearinput()
        IsDataChange = True

    End Sub

    Public Function IsData_Valid() As Boolean Implements IGoAML_Grid(Of goAML_Ref_Address).IsData_Valid
        If ascx_cmb_kodenegara.SelectedItemValue = "" Then
            Throw New Exception("Country Code is required")
        End If
        If ascx_cmb_kategoriaddress.SelectedItemValue = "" Then
            Throw New Exception("Address Type is required")
        End If
        If Address_INDV.Text.Trim = "" Then
            Throw New Exception("Address is required")
        End If
        If City_INDV.Text.Trim = "" Then
            Throw New Exception("City is required")
        End If
        Return True
    End Function

    Public Sub LoadData(data As List(Of goAML_Ref_Address)) Implements IGoAML_Grid(Of goAML_Ref_Address).LoadData
        objListgoAML_Ref = data.Select(Function(x) x).ToList()
        objListgoAML_vw = data.Select(Function(x) x).ToList()

        If objListgoAML_vw IsNot Nothing Then
            ' 2023-10-04, Nael: Menambahkan detail untuk kolom Country dan kolom Address Type
            Using objDB As New GoAMLDAL.GoAMLEntities
                Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(objListgoAML_vw.ToList())
                dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
                dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
                Dim listAddressType = objDB.goAML_Ref_Kategori_Kontak.ToList()
                Dim listCountry = objDB.goAML_Ref_Nama_Negara.ToList()
                For Each row In dtAddress.Rows
                    If Not IsDBNull(row("Address_Type")) Then
                        Dim objAddressType = listAddressType.FirstOrDefault(Function(x) x.Kode = row("Address_Type"))
                        If objAddressType IsNot Nothing Then
                            row("Address_Type") = objAddressType.Kode & " - " & objAddressType.Keterangan
                        End If
                    End If

                    If Not IsDBNull(row("Country_Code")) Then
                        Dim objCountry = listCountry.FirstOrDefault(Function(x) x.Kode.Equals(row("Country_Code")))
                        If objCountry IsNot Nothing Then
                            row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                        End If
                    End If
                Next
                Store.DataSource = dtAddress
                '=====================================================================================
            End Using
            Store.DataBind()
        Else
            Store.DataBind()
        End If
    End Sub

    Protected Sub GridPanel_DataBinding(sender As Object, e As EventArgs)
        GridPanel.Title = Title

        Dim cmdColumn As CommandColumn = TryCast(GridPanel.ColumnModel.Columns(1), CommandColumn)
        If cmdColumn IsNot Nothing Then
            If IsViewMode Then
                cmdColumn.Commands.RemoveAt(1)
                cmdColumn.Commands.RemoveAt(1)
            End If
            'Dim cmd1 As GridCommand = cmdColumn.Commands(1)
            'Dim cmd2 As GridCommand = cmdColumn.Commands(2)
            'cmd1.Hidden = IsViewMode
            'cmd2.Hidden = IsViewMode
        End If

        btnAdd.Hidden = IsViewMode
    End Sub
End Class
