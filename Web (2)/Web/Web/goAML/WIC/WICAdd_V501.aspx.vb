﻿Imports NawaBLL
Imports Ext
Imports GoAMLBLL
Imports System.Data.SqlClient
Imports NawaDAL
Imports GoAMLDAL
Imports Elmah
Imports System.Data
Imports System.Data.Entity.ModelConfiguration.Configuration
Imports GoAMLBLL.goAML
Imports GoAMLBLL.goAML.Services

Partial Class goAML_WICAdd_V501
    Inherits Parent
    Public ObjSocialMedia As New WICDataBLL.goAML_Ref_WIC_Social_Media
    Public goAML_WICBLL As New WICBLL()
#Region "Session"
    'START: 2023-10-20, Nael
    'Public Property tempflag() As Integer
    '    Get
    '        Return Session("CUSTOMER_EDIT.tempflag")
    '    End Get
    '    Set(ByVal value As Integer)
    '        Session("CUSTOMER_EDIT.tempflag") = value
    '    End Set
    'End Property
    'END: 2023-10-20, Nael
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("goAML_WICAdd_V501.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_WICAdd_V501.ObjModule") = value
        End Set
    End Property
    Public Property objWICAddData() As goAML_Ref_WIC
        Get
            If Session("goAML_WICAdd_V501.objWICAddData") Is Nothing Then
                Session("goAML_WICAdd_V501.objWICAddData") = New goAML_Ref_WIC
            End If
            Return Session("goAML_WICAdd_V501.objWICAddData")
        End Get
        Set(ByVal value As goAML_Ref_WIC)
            Session("goAML_WICAdd_V501.objWICAddData") = value
        End Set
    End Property
    Public Property objDirectorAddData() As goAML_Ref_Walk_In_Customer_Director
        Get
            Return Session("goAML_WICAdd_V501.objDirectorAddData")
        End Get
        Set(ByVal value As goAML_Ref_Walk_In_Customer_Director)
            Session("goAML_WICAdd_V501.objDirectorAddData") = value
        End Set
    End Property
    Public Property ListDirectorDetail As List(Of goAML_Ref_Walk_In_Customer_Director)
        Get
            If Session("goAML_WICAdd_V501.ListDirectorDetail") Is Nothing Then
                Session("goAML_WICAdd_V501.ListDirectorDetail") = New List(Of goAML_Ref_Walk_In_Customer_Director)
            End If
            Return Session("goAML_WICAdd_V501.ListDirectorDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Walk_In_Customer_Director))
            Session("goAML_WICAdd_V501.ListDirectorDetail") = value
        End Set
    End Property
    Public Property ListDirectorDetailClass As List(Of WICDirectorDataBLL)
        Get
            Return Session("goAML_WICAdd_V501.ListDirectorDetailClass")
        End Get
        Set(ByVal value As List(Of WICDirectorDataBLL))
            Session("goAML_WICAdd_V501.ListDirectorDetailClass") = value
        End Set
    End Property
    Public Property ListDirectorPhoneDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICAdd_V501.ListDirectorPhoneDetail") Is Nothing Then
                Session("goAML_WICAdd_V501.ListDirectorPhoneDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICAdd_V501.ListDirectorPhoneDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICAdd_V501.ListDirectorPhoneDetail") = value
        End Set
    End Property
    Public Property ListDirectorIdentificationDetail As List(Of goAML_Person_Identification)
        Get
            If Session("goAML_WICAdd_V501.ListDirectorIdentificationDetail") Is Nothing Then
                Session("goAML_WICAdd_V501.ListDirectorIdentificationDetail") = New List(Of goAML_Person_Identification)
            End If
            Return Session("goAML_WICAdd_V501.ListDirectorIdentificationDetail")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_WICAdd_V501.ListDirectorIdentificationDetail") = value
        End Set
    End Property
    Public Property ListDirectorEmployerPhoneDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICAdd_V501.ListDirectorEmployerPhoneDetail") Is Nothing Then
                Session("goAML_WICAdd_V501.ListDirectorEmployerPhoneDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICAdd_V501.ListDirectorEmployerPhoneDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICAdd_V501.ListDirectorEmployerPhoneDetail") = value
        End Set
    End Property
    Public Property ListDirectorAddressDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICAdd_V501.ListDirectorAddressDetail") Is Nothing Then
                Session("goAML_WICAdd_V501.ListDirectorAddressDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICAdd_V501.ListDirectorAddressDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICAdd_V501.ListDirectorAddressDetail") = value
        End Set
    End Property
    Public Property ListDirectorEmployerAddressDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICAdd_V501.ListDirectorEmployerAddressDetail") Is Nothing Then
                Session("goAML_WICAdd_V501.ListDirectorEmployerAddressDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICAdd_V501.ListDirectorEmployerAddressDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICAdd_V501.ListDirectorEmployerAddressDetail") = value
        End Set
    End Property
    Public Property ListPhoneDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICAdd_V501.ListPhoneDetail") Is Nothing Then
                Session("goAML_WICAdd_V501.ListPhoneDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICAdd_V501.ListPhoneDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICAdd_V501.ListPhoneDetail") = value
        End Set
    End Property
    Public Property ListAddressDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICAdd_V501.ListAddressDetail") Is Nothing Then
                Session("goAML_WICAdd_V501.ListAddressDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICAdd_V501.ListAddressDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICAdd_V501.ListAddressDetail") = value
        End Set
    End Property
    Public Property ListPhoneEmployerDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICAdd_V501.ListPhoneEmployerDetail") Is Nothing Then
                Session("goAML_WICAdd_V501.ListPhoneEmployerDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICAdd_V501.ListPhoneEmployerDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICAdd_V501.ListPhoneEmployerDetail") = value
        End Set
    End Property
    Public Property ListAddressEmployerDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICAdd_V501.ListAddressEmployerDetail") Is Nothing Then
                Session("goAML_WICAdd_V501.ListAddressEmployerDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICAdd_V501.ListAddressEmployerDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICAdd_V501.ListAddressEmployerDetail") = value
        End Set
    End Property
    Public Property ListIdentificationDetail As List(Of goAML_Person_Identification)
        Get
            If Session("goAML_WICAdd_V501.ListIdentificationDetail") Is Nothing Then
                Session("goAML_WICAdd_V501.ListIdentificationDetail") = New List(Of goAML_Person_Identification)
            End If
            Return Session("goAML_WICAdd_V501.ListIdentificationDetail")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_WICAdd_V501.ListIdentificationDetail") = value
        End Set
    End Property
    Public Property ObjDetailPhone As goAML_Ref_Phone
        Get
            Return Session("goAML_WICAdd_V501.ObjDetailPhone")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICAdd_V501.ObjDetailPhone") = value
        End Set
    End Property
    Public Property ObjDetailPhoneEmployer As goAML_Ref_Phone
        Get
            Return Session("goAML_WICAdd_V501.ObjDetailPhoneEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICAdd_V501.ObjDetailPhoneEmployer") = value
        End Set
    End Property
    Public Property ObjDetailPhoneDirector As goAML_Ref_Phone
        Get
            Return Session("goAML_WICAdd_V501.ObjDetailPhoneDirector")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICAdd_V501.ObjDetailPhoneDirector") = value
        End Set
    End Property
    Public Property ObjDetailIdentification As goAML_Person_Identification
        Get
            Return Session("goAML_WICAdd_V501.ObjDetailIdentification")
        End Get
        Set(ByVal value As goAML_Person_Identification)
            Session("goAML_WICAdd_V501.ObjDetailIdentification") = value
        End Set
    End Property
    Public Property ObjDetailIdentificationDirector As goAML_Person_Identification
        Get
            Return Session("goAML_WICAdd_V501.ObjDetailIdentificationDirector")
        End Get
        Set(ByVal value As goAML_Person_Identification)
            Session("goAML_WICAdd_V501.ObjDetailIdentificationDirector") = value
        End Set
    End Property
    Public Property ObjDetailPhoneDirectorEmployer As goAML_Ref_Phone
        Get
            Return Session("goAML_WICAdd_V501.ObjDetailPhoneDirectorEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICAdd_V501.ObjDetailPhoneDirectorEmployer") = value
        End Set
    End Property
    Public Property ObjDetailAddress As goAML_Ref_Address
        Get
            Return Session("goAML_WICAdd_V501.ObjDetailAddress")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICAdd_V501.ObjDetailAddress") = value
        End Set
    End Property
    Public Property ObjDetailAddressEmployer As goAML_Ref_Address
        Get
            Return Session("goAML_WICAdd_V501.ObjDetailAddressEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICAdd_V501.ObjDetailAddressEmployer") = value
        End Set
    End Property
    Public Property ObjDetailAddressDirector As goAML_Ref_Address
        Get
            Return Session("goAML_WICAdd_V501.ObjDetailAddressDirector")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICAdd_V501.ObjDetailAddressDirector") = value
        End Set
    End Property
    Public Property ObjDetailAddressDirectorEmployer As goAML_Ref_Address
        Get
            Return Session("goAML_WICAdd_V501.ObjDetailAddressDirectorEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICAdd_V501.ObjDetailAddressDirectorEmployer") = value
        End Set
    End Property
    Public Property DataRowTemp() As DataRow
        Get
            Return Session("goAML_WICAdd_v501.DataRowTemp")
        End Get
        Set(ByVal value As DataRow)
            Session("goAML_WICAdd_v501.DataRowTemp") = value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
        goAML_WICBLL = New WICBLL(WIC_Panel)
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                Dim strmodule As String = Request.Params("ModuleID")
                ClearSession()
                Dim intmodule As Integer = Common.DecryptQueryString(strmodule, SystemParameterBLL.GetEncriptionKey)
                ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)

                If Not ObjModule Is Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Insert) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                    WIC_Individu.Visible = True
                    WIC_Corporate.Visible = False

                    Dim objrand As New Random
                    objWICAddData.PK_Customer_ID = objrand.Next

                    goAML_WICBLL = New WICBLL()

                    LoadNegara()
                    LoadBadanUsaha()
                    LoadGender()
                    LoadParty()
                    LoadKategoriKontak()
                    LoadJenisAlatKomunikasi()
                    LoadTypeIdentitas()
                    LoadPopup()
                    LoadColumn()

                    cboFormWIC.SelectedItem.Text = "Individu"
                Else
                    Throw New Exception("Invalid Module ID")
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
#Region "LoadData"
    Private Sub LoadColumn()

        ColumnActionLocation(GridPanel1, CommandColumn1)
        ColumnActionLocation(GridPanel2, CommandColumn2)
        ColumnActionLocation(GridPanel3, CommandColumn3)
        ColumnActionLocation(GridPanel4, CommandColumn4)
        ColumnActionLocation(GridPanel5, CommandColumn5)

        ColumnActionLocation(GP_DirectorPhone, CommandColumn11)
        ColumnActionLocation(GP_DirectorAddress, CommandColumn12)
        ColumnActionLocation(GP_DirectorEmpPhone, CommandColumn9)
        ColumnActionLocation(GP_DirectorEmpAddress, CommandColumn10)
        ColumnActionLocation(GP_DirectorIdentification, CommandColumn6)

        ColumnActionLocation(GridPanelPhoneIndividuDetail, CommandColumnPhoneIndividuDetail)
        ColumnActionLocation(GridPanelAddressIndividuDetail, CommandColumnAddressIndividuDetail)
        ColumnActionLocation(GridPanelPhoneIndividualEmployerDetail, CommandColumnPhoneIndividualEmployerDetail)
        ColumnActionLocation(GridPanelAddressIndividualEmployerDetail, CommandColumnAddressIndividualEmployerDetail)
        ColumnActionLocation(GridPanelIdentificationIndividualDetail, CommandColumnIdentificationIndividualDetail)

        ColumnActionLocation(GridPanelPhoneCorporateDetail, CommandColumnPhoneCorporateDetail)
        ColumnActionLocation(GridPanelAddressCorporateDetail, CommandColumnAddressCorporateDetail)
        ColumnActionLocation(GP_Corp_Director, CommandColumnDirector)
    End Sub

    Private Sub LoadPopup()
        WindowDetailAddress.Maximizable = True
        WindowDetailPhone.Maximizable = True
        WindowDetailDirector.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True
        WindowDetailDirectorIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailIdentification.Maximizable = True

        ' START: 2023-10-24, Nael
        Window_RelatedPerson.Maximizable = True
        Window_Sanction.Maximizable = True
        Window_PersonPEP.Maximizable = True
        Window_Email.Maximizable = True
        WindowDetail_RelatedEntities.Maximizable = True
        ' END: 2023-10-24, Nael

    End Sub
    Protected Sub National_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Nama_Negara", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Communication_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Alat_Komunikasi", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Contact_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Kategori_Kontak", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Identification_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goaml_ref_jenis_dokumen_identitas", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Incorporation_legal_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Bentuk_Badan_Usaha", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Gender_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Kelamin", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Director_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_ODM_Reporting_Person", "PK_ODM_Reporting_Person_ID, Last_Name", strfilter, "Last_Name", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Role_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Peran_orang_dalam_Korporasi", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Private Sub LoadJenisAlatKomunikasi()
        Cmbtph_communication_type.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignCommunication_Type.Reload()
        Director_Emp_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreEmpDirComunicationType.Reload()
        Director_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreDirComunicationType.Reload()
        Cmbtph_communication_typeEmployer.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignCommunication_TypeEmployer.Reload()
    End Sub

    Private Sub LoadKategoriKontak()

        Cmbtph_contact_type.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignContact_Type.Reload()
        Cmbtph_contact_typeEmployer.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignContact_TypeEmployer.Reload()

        CmbAddress_type.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignAddress_Type.Reload()
        CmbAddress_typeEmployer.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignAddress_TypeEmployer.Reload()

        Director_Emp_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreEmpDirPContactType.Reload()
        Director_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreDirPContactType.Reload()

        Director_cmb_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        StoreDirKategoryADdressAddress_Type.Reload()
        Director_cmb_emp_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        StoreEmpDirKategoryADdressAddress_Type.Reload()

    End Sub
    Private Sub LoadTypeIdentitas()
        CmbTypeIdentification.PageSize = SystemParameterBLL.GetPageSize
        StoreTypeIdentification.Reload()

        CmbTypeIdentificationDirector.PageSize = SystemParameterBLL.GetPageSize
        StoreTypeIdentificationDirector.Reload()
    End Sub
    Private Sub LoadParty()
        cmb_peran.PageSize = SystemParameterBLL.GetPageSize
        StorePeran.Reload()
    End Sub
    Private Sub LoadGender()
        cmb_Director_Gender.PageSize = SystemParameterBLL.GetPageSize
        StoreGender.Reload()

        CmbINDV_Gender.PageSize = SystemParameterBLL.GetPageSize
        StoreGenderINDV.Reload()
    End Sub
    Private Sub LoadBadanUsaha()
        CmbCorp_Incorporation_legal_form.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignIncorporation_legal.Reload()
    End Sub
    Private Sub LoadNegara()
        'CmbINDV_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignNational1.Reload()
        'CmbINDV_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignNational2.Reload()
        'CmbINDV_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignNational3.Reload()
        'CmbINDV_residence.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignResisdance.Reload()
        'TxtINDV_Passport_country.PageSize = SystemParameterBLL.GetPageSize
        'StoreIndividuPassportCountry.Reload()
        'txt_Director_Passport_Country.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirectorPassportNumber.Reload()

        'CmbCorp_incorporation_country_code.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignCountry_Code.Reload()

        'Cmbcountry_code.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignCountry_CodeAddress.Reload()
        'Cmbcountry_codeEmployer.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignCountry_CodeAddressEmployer.Reload()

        'cmb_Director_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirNational1.Reload()
        'cmb_Director_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirNational2.Reload()
        'cmb_Director_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirNational3.Reload()
        'cmb_Director_Residence.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirReisdence.Reload()

        'Director_cmb_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirFromForeignCountry_CodeAddress.Reload()
        'Director_cmb_emp_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'StoreEmpDirFromForeignCountry_CodeAddress.Reload()

        'CmbCountryIdentification.PageSize = SystemParameterBLL.GetPageSize
        'StoreCountryCodeIdentification.Reload()
        'CmbCountryIdentificationDirector.PageSize = SystemParameterBLL.GetPageSize
        'StoreCountryCodeIdentificationDirector.Reload()

    End Sub

    Private Sub BindDetailIdentification(store As Store, list As List(Of goAML_Person_Identification))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Tipe", GetType(String)))
        objtable.Columns.Add(New DataColumn("No", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssueDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("ExpiryDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssuedCountry", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Tipe") = GlobalReportFunctionBLL.getjenisDokumenByKode(item("Type"))
                item("No") = item("Number")
                item("IssueDate") = item("Issue_Date")
                item("ExpiryDate") = item("Expiry_Date")
                item("IssuedCountry") = GlobalReportFunctionBLL.getCountryByCode(item("Issued_Country"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailPhone(store As Store, list As List(Of goAML_Ref_Phone))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
                item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailAddress(store As Store, list As List(Of goAML_Ref_Address))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Type_Address", GetType(String)))
        objtable.Columns.Add(New DataColumn("Addres", GetType(String)))
        objtable.Columns.Add(New DataColumn("Cty", GetType(String)))
        objtable.Columns.Add(New DataColumn("CountryCode", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Type_Address") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type"))
                item("Addres") = item("Address")
                item("Cty") = item("City")
                item("CountryCode") = GlobalReportFunctionBLL.getCountryByCode(item("Country_Code"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailDirector(store As Store, list As List(Of goAML_Ref_Walk_In_Customer_Director))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("LastName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Roles", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("LastName") = item("Last_Name")
                item("Roles") = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(item("Role"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
#End Region
#Region "Clear"
    Private Sub ClearSession()
        objWICAddData = Nothing
        'ObjModule = Nothing
        objDirectorAddData = Nothing


        ListDirectorDetailClass = New List(Of WICDirectorDataBLL)
        ListAddressDetail = New List(Of goAML_Ref_Address)
        ListPhoneDetail = New List(Of goAML_Ref_Phone)
        ListAddressEmployerDetail = New List(Of goAML_Ref_Address)
        ListPhoneEmployerDetail = New List(Of goAML_Ref_Phone)
        ListIdentificationDetail = New List(Of goAML_Person_Identification)

        ListDirectorDetail = New List(Of goAML_Ref_Walk_In_Customer_Director)
        ListDirectorAddressDetail = New List(Of goAML_Ref_Address)
        ListDirectorPhoneDetail = New List(Of goAML_Ref_Phone)
        ListDirectorEmployerAddressDetail = New List(Of goAML_Ref_Address)
        ListDirectorEmployerPhoneDetail = New List(Of goAML_Ref_Phone)
        ListDirectorIdentificationDetail = New List(Of goAML_Person_Identification)

        ObjDetailAddress = Nothing
        ObjDetailPhone = Nothing
        ObjDetailAddressEmployer = Nothing
        ObjDetailPhoneEmployer = Nothing
        ObjDetailIdentification = Nothing
        ObjDetailAddressDirector = Nothing
        ObjDetailAddressDirectorEmployer = Nothing
        ObjDetailPhoneDirector = Nothing
        ObjDetailPhoneDirectorEmployer = Nothing
        ObjDetailIdentificationDirector = Nothing

        'Add by Septian, goAML 5.0.1, 2023-02-28
        obj_ListEmail_Edit = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Email)
        obj_ListPersonPEP_Edit = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Person_PEP)
        obj_ListSanction_Edit = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Sanction)
        obj_ListRelatedPerson_Edit = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Person)

        objListgoAML_Ref_EntityUrl = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
        objListgoAML_vw_EntityUrl = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)

        objListgoAML_Ref_EntityIdentification = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        objListgoAML_vw_EntityIdentification = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)

        objListgoAML_Ref_RelatedEntity = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        objListgoAML_vw_RelatedEntity = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        'End Add

    End Sub
    Private Sub clearinputDirector()
        Director_cb_phone_Contact_Type.ClearValue()
        Director_cb_phone_Communication_Type.ClearValue()
        Director_txt_phone_Country_prefix.Clear()
        Director_txt_phone_number.Clear()
        Director_txt_phone_extension.Clear()
        Director_txt_phone_comments.Clear()

        Director_Emp_cb_phone_Contact_Type.ClearValue()
        Director_Emp_cb_phone_Communication_Type.ClearValue()
        Director_Emp_txt_phone_Country_prefix.Clear()
        Director_Emp_txt_phone_number.Clear()
        Director_Emp_txt_phone_extension.Clear()
        Director_Emp_txt_phone_comments.Clear()

        Director_cmb_kategoriaddress.ClearValue()
        Director_Address.Clear()
        Director_Town.Clear()
        Director_City.Clear()
        Director_Zip.Clear()
        Director_cmb_kodenegara.SetTextValue("")
        Director_State.Clear()
        Director_Comment_Address.Clear()

        Director_cmb_emp_kategoriaddress.ClearValue()
        Director_emp_Address.Clear()
        Director_emp_Town.Clear()
        Director_emp_City.Clear()
        Director_emp_Zip.Clear()
        Director_cmb_emp_kodenegara.SetTextValue("")
        Director_emp_State.Clear()
        Director_emp_Comment_Address.Clear()

        CmbTypeIdentificationDirector.ClearValue()
        TxtNumberDirector.Clear()
        TxtIssueDateDirector.Clear()
        TxtExpiryDateDirector.Clear()
        TxtIssuedByDirector.Clear()
        CmbCountryIdentificationDirector.SetTextValue("")
        TxtCommentIdentificationDirector.Clear()
    End Sub
    Private Sub ClearInput()

        'Phone WIC
        Cmbtph_contact_type.ClearValue()
        Cmbtph_communication_type.ClearValue()
        Txttph_country_prefix.Clear()
        Txttph_number.Clear()
        Txttph_extension.Clear()
        TxtcommentsPhone.Clear()

        'Address WIC
        CmbAddress_type.ClearValue()
        TxtAddress.Clear()
        TxtTown.Clear()
        TxtCity.Clear()
        TxtZip.Clear()
        Cmbcountry_code.SetTextValue("")
        TxtState.Clear()
        TxtcommentsAddress.Clear()

        'Identification WIC
        CmbTypeIdentification.ClearValue()
        TxtNumber.Clear()
        TxtIssueDate.Clear()
        TxtExpiryDate.Clear()
        TxtIssuedBy.Clear()
        CmbCountryIdentification.SetTextValue("")
        TxtCommentIdentification.Clear()

        'Phone Employer WIC
        Cmbtph_contact_typeEmployer.ClearValue()
        Cmbtph_communication_typeEmployer.ClearValue()
        Txttph_country_prefixEmployer.Clear()
        Txttph_numberEmployer.Clear()
        Txttph_extensionEmployer.Clear()
        TxtcommentsPhoneEmployer.Clear()

        'Address Employer WIC
        CmbAddress_typeEmployer.ClearValue()
        TxtAddressEmployer.Clear()
        TxtTownEmployer.Clear()
        TxtCityEmployer.Clear()
        TxtZipEmployer.Clear()
        Cmbcountry_codeEmployer.SetTextValue("")
        TxtStateEmployer.Clear()
        TxtcommentsAddressEmployer.Clear()

        'list
        ' 2023-10-20, Nael: Comment, data address, phone, dll jadi kereset
        'ListDirectorEmployerAddressDetail.Clear()
        'ListDirectorEmployerPhoneDetail.Clear()
        'ListDirectorAddressDetail.Clear()
        'ListDirectorPhoneDetail.Clear()
        'ListDirectorIdentificationDetail.Clear()

        'Director
        cmb_peran.ClearValue()
        txt_Director_Title.Clear()
        txt_Director_Last_Name.Clear()
        cmb_Director_Gender.ClearValue()
        txt_Director_BirthDate.Clear()
        txt_Director_Birth_Place.Clear()
        txt_Director_Mothers_Name.Clear()
        txt_Director_Alias.Clear()
        txt_Director_ID_Number.Clear()
        txt_Director_Passport_Number.Clear()
        'txt_Director_Passport_Country.Clear()
        txt_Director_Passport_Country.SetTextValue("")
        txt_Director_SSN.Clear()
        cmb_Director_Nationality1.SetTextValue("")
        cmb_Director_Nationality2.SetTextValue("")
        cmb_Director_Nationality3.SetTextValue("")
        cmb_Director_Residence.SetTextValue("")
        'cmb_Director_Nationality1.ClearValue()
        'cmb_Director_Nationality2.ClearValue()
        'cmb_Director_Nationality3.ClearValue()
        'cmb_Director_Residence.ClearValue()
        'txt_Director_Email.Clear()
        'txt_Director_Email2.Clear()
        'txt_Director_Email3.Clear()
        'txt_Director_Email4.Clear()
        'txt_Director_Email5.Clear()
        cb_Director_Deceased.Clear()
        txt_Director_Deceased_Date.Clear()
        cb_Director_Tax_Reg_Number.Clear()
        txt_Director_Tax_Number.Clear()
        txt_Director_Source_of_Wealth.Clear()
        txt_Director_Occupation.Clear()
        txt_Director_Comments.Clear()
        txt_Director_employer_name.Clear()

        'danie 21 jan 2021

        ' START: 2023-10-20, Nael
        ListDirectorEmployerAddressDetail = ListDirectorEmployerAddressDetail.Where(Function(x) x.FK_To_Table_ID <> objDirectorAddData.NO_ID).ToList()
        ListDirectorEmployerPhoneDetail = ListDirectorEmployerPhoneDetail.Where(Function(x) x.FK_for_Table_ID <> objDirectorAddData.NO_ID).ToList()
        ListDirectorAddressDetail = ListDirectorAddressDetail.Where(Function(x) x.FK_To_Table_ID <> objDirectorAddData.NO_ID).ToList()
        ListDirectorPhoneDetail = ListDirectorPhoneDetail.Where(Function(x) x.FK_for_Table_ID <> objDirectorAddData.NO_ID).ToList()
        ListDirectorIdentificationDetail = ListDirectorIdentificationDetail.Where(Function(x) x.FK_Person_ID <> objDirectorAddData.NO_ID).ToList()

        objDirectorAddData = Nothing
        ' END: 2023-10-20, Nael

        'end 21 jan 2021
        BindDetailPhone(Store_DirectorPhone, New List(Of goAML_Ref_Phone))
        BindDetailPhone(Store_Director_Employer_Phone, New List(Of goAML_Ref_Phone))
        BindDetailAddress(Store_DirectorAddress, New List(Of goAML_Ref_Address))
        BindDetailAddress(Store_Director_Employer_Address, New List(Of goAML_Ref_Address))
        BindDetailIdentification(StoreIdentificationDirectorDetail, New List(Of goAML_Person_Identification))

    End Sub


#End Region
#Region "Button"
    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            objWICAddData = Nothing
            Dim wicData = New WICDataBLL
            Dim gridWIC = New WICDataBLL.goAML_Grid_WIC
            If TxtWIC_No.Text = "" Then
                Throw New Exception("WIC No Is Required")
            Else
                If cboFormWIC.SelectedItem.Value = "Individu" Then
                    If TxtINDV_last_name.Text = "" Then
                        Throw New Exception("Full Name Is Required")
                        'If TxtINDV_last_name.Text = "" Then
                        '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
                        'ElseIf DateINDV_Birthdate.Text = "1/1/0001 12:00:00 AM" Then
                        '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
                        'ElseIf TxtINDV_Birth_place.Text = "" Then
                        '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
                        'ElseIf CmbINDV_Nationality1.TextValue = "" Then
                        '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
                        'ElseIf CmbINDV_residence.TextValue = "" Then
                        '    Throw New Exception("Negara Domisili Tidak boleh kosong")
                        'ElseIf ListPhoneDetail.Count <= 0 Then
                        '    Throw New Exception("Informasi Telepon Tidak boleh kosong")
                        'ElseIf ListAddressDetail.Count <= 0 Then
                        '    Throw New Exception("Informasi Alamat Tidak boleh kosong")
                        'ElseIf TxtINDV_Occupation.Text = "" Then
                        '    Throw New Exception("Pekerjaan Tidak boleh kosong")
                        'ElseIf TxtINDV_Source_Of_Wealth.Text = "" Then
                        '    Throw New Exception("Sumber Dana Tidak boleh kosong")
                    Else
                        If TxtINDV_SSN.Text.Length <> 16 And TxtINDV_SSN.Text.Length > 0 Then
                            Throw New Exception("NIK Must 16 Digit")
                        Else
                            If ListPhoneDetail Is Nothing Then
                                Throw New Exception("Phone Is Required")
                            End If
                            If ListAddressDetail Is Nothing Then
                                Throw New Exception("Address Is Required")
                            End If
                            If ListPhoneDetail.Count = 0 Then
                                Throw New Exception("Phone Is Required")
                            End If
                            If ListAddressDetail.Count = 0 Then
                                Throw New Exception("Address Is Required")
                            End If
                            With objWICAddData
                                .WIC_No = TxtWIC_No.Text
                                .isUpdateFromDataSource = CheckboxIsUpdateFromDataSource.Checked
                                .INDV_Title = TxtINDV_Title.Text
                                .INDV_Last_Name = TxtINDV_last_name.Text
                                .INDV_Gender = CmbINDV_Gender.Text
                                'agam 03112020
                                If Not DateINDV_Birthdate.SelectedDate = DateTime.MinValue Then
                                    .INDV_BirthDate = DateINDV_Birthdate.SelectedDate
                                End If
                                .INDV_Birth_Place = TxtINDV_Birth_place.Text
                                .INDV_Mothers_Name = TxtINDV_Mothers_name.Text
                                .INDV_Alias = TxtINDV_Alias.Text
                                .INDV_SSN = TxtINDV_SSN.Text
                                .INDV_Passport_Number = TxtINDV_Passport_number.Text
                                .INDV_Passport_Country = TxtINDV_Passport_country.TextValue
                                .INDV_ID_Number = TxtINDV_ID_Number.Text
                                .INDV_Nationality1 = CmbINDV_Nationality1.TextValue
                                .INDV_Nationality2 = CmbINDV_Nationality2.TextValue
                                .INDV_Nationality3 = CmbINDV_Nationality3.TextValue
                                .INDV_Residence = CmbINDV_residence.TextValue
                                '.INDV_Email = TxtINDV_Email.Text
                                '.INDV_Email2 = TxtINDV_Email2.Text
                                '.INDV_Email3 = TxtINDV_Email3.Text
                                '.INDV_Email4 = TxtINDV_Email4.Text
                                '.INDV_Email5 = TxtINDV_Email5.Text
                                .INDV_Occupation = TxtINDV_Occupation.Text
                                .INDV_Employer_Name = TxtINDV_employer_name.Text
                                .INDV_SumberDana = TxtINDV_Source_Of_Wealth.Text
                                .INDV_Tax_Number = TxtINDV_Tax_Number.Text
                                .INDV_Tax_Reg_Number = CbINDV_Tax_Reg_Number.Checked
                                .INDV_Deceased = CbINDV_Deceased.Checked
                                If CbINDV_Deceased.Checked Then
                                    'agam 03112020
                                    If Not DateINDV_Deceased_Date.SelectedDate = DateTime.MinValue Then
                                        .INDV_Deceased_Date = DateINDV_Deceased_Date.SelectedDate
                                    End If
                                    '.INDV_Deceased_Date = Convert.ToDateTime(DateINDV_Deceased_Date.Text)
                                End If
                                .INDV_Comment = TxtINDV_Comments.Text
                                .FK_Customer_Type_ID = 1
                                'agam 03112020
                                '.Corp_Incorporation_Date = DateCorp_incorporation_date.RawValue
                                '.Corp_Date_Business_Closed = DateCorp_date_business_closed.RawValue

                            End With

                            ''GO AML 5.0.1, Add by Septian, 2023-03-08
                            wicData = GetObjData()
                            ''End goAML 5.0.1

                            If Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then

                                'gridWIC.ObjList_GoAML_Email = obj_ListEmail_Edit
                                'gridWIC.ObjList_GoAML_PersonPEP = obj_ListPersonPEP_Edit
                                'gridWIC.ObjList_GoAML_Sanction = obj_ListSanction_Edit
                                'gridWIC.ObjList_GoAML_RelatedPerson = obj_ListRelatedPerson_Edit
                                goAML_WICBLL.SaveAddTanpaApproval(objWICAddData, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule, wicData)
                                'goAML_WICBLL.SaveGoAmlWic501(wicData)

                                WIC_Panel.Hidden = True
                                Panelconfirmation.Hidden = False
                                LblConfirmation.Text = "Data Saved into Database."
                            Else

                                Dim objectdata As Object
                                objectdata = ObjSocialMedia


                                Dim objtype As Type = objectdata.GetType
                                Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                                'gridWIC = GetObjGrid()
                                goAML_WICBLL.SaveAddApproval(objWICAddData, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule, wicData)
                                Dim PKAudit As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)

                                WIC_Panel.Hidden = True
                                Panelconfirmation.Hidden = False
                                LblConfirmation.Text = "Data Saved into Pending Approval."
                            End If
                        End If
                    End If
                Else
                    If TxtCorp_Name.Text = "" Then
                        Throw New Exception("Corporate Name is Required")
                        'ElseIf TxtCorp_Business.Text = "" Then
                        '    Throw New Exception("Bidang Usaha Tidak boleh kosong")
                        'ElseIf ListAddressDetail.Count <= 0 Then
                        '    Throw New Exception("Informasi Alamat Tidak boleh kosong")
                        'ElseIf CmbCorp_incorporation_country_code.TextValue = "" Then
                        '    Throw New Exception("Negara Tidak boleh kosong")
                    Else
                        With objWICAddData
                            .WIC_No = TxtWIC_No.Text
                            .isUpdateFromDataSource = CheckboxIsUpdateFromDataSource.Checked
                            .Corp_Name = TxtCorp_Name.Text
                            .Corp_Commercial_Name = TxtCorp_Commercial_name.Text
                            .Corp_Incorporation_Legal_Form = CmbCorp_Incorporation_legal_form.Text
                            .Corp_Incorporation_Number = TxtCorp_Incorporation_number.Text
                            .Corp_Business = TxtCorp_Business.Text
                            '.Corp_Email = TxtCorp_Email.Text
                            '.Corp_Url = TxtCorp_url.Text
                            .Corp_Incorporation_State = TxtCorp_incorporation_state.Text
                            .Corp_Incorporation_Country_Code = CmbCorp_incorporation_country_code.TextValue
                            'agam 03112020
                            If Not DateCorp_incorporation_date.SelectedDate = DateTime.MinValue Then
                                .Corp_Incorporation_Date = DateCorp_incorporation_date.SelectedDate
                            End If
                            'Update BSIM 01-10-2020
                            '.Corp_Incorporation_Date = DateTime.Parse(DateCorp_incorporation_date.Value)
                            .Corp_Business_Closed = chkCorp_business_closed.Checked
                            If chkCorp_business_closed.Checked Then
                                'agam 03112020
                                If Not DateCorp_date_business_closed.SelectedDate = DateTime.MinValue Then
                                    .Corp_Date_Business_Closed = DateCorp_date_business_closed.SelectedDate
                                End If
                                '.Corp_Date_Business_Closed = Convert.ToDateTime(DateCorp_date_business_closed.Text)
                            End If
                            .Corp_Tax_Number = TxtCorp_tax_number.Text
                            .Corp_Comments = TxtCorp_Comments.Text
                            .FK_Customer_Type_ID = 2
                            '.INDV_BirthDate = DateINDV_Birthdate.RawValue
                        End With

                        gridWIC.ObjList_GoAML_Email = obj_ListEmail_Edit
                        gridWIC.ObjList_GoAML_PersonPEP = obj_ListPersonPEP_Edit
                        gridWIC.ObjList_GoAML_Sanction = obj_ListSanction_Edit
                        gridWIC.ObjList_GoAML_RelatedPerson = obj_ListRelatedPerson_Edit
                        gridWIC.ObjList_GoAML_URL = objListgoAML_Ref_EntityUrl
                        gridWIC.ObjList_GoAML_EntityIdentification = objListgoAML_Ref_EntityIdentification
                        gridWIC.ObjList_GoAML_RelatedEntity = objListgoAML_Ref_RelatedEntity

                        If Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                            wicData.ObjGrid = GetObjGrid()
                            goAML_WICBLL.SaveAddTanpaApproval(objWICAddData, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule, wicData)
                            'goAML_WICBLL.SaveGoAmlWic501(wicData)
                            WIC_Panel.Hidden = True
                            Panelconfirmation.Hidden = False
                            LblConfirmation.Text = "Data Saved into Database."
                        Else
                            wicData.ObjGrid = GetObjGrid()
                            goAML_WICBLL.SaveAddApproval(objWICAddData, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule, wicData)
                            WIC_Panel.Hidden = True
                            Panelconfirmation.Hidden = False
                            LblConfirmation.Text = "Data Saved into Pending Approval."
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorIdentification_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()
            WindowDetailDirectorIdentification.Title = "Add Identitas"
            WindowDetailDirectorIdentification.Show()

            FormPanelDirectorIdentification.Show()
            PanelDetailDirectorIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddIdentification_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()

            WindowDetailIdentification.Show()

            FormPanelIdentification.Show()
            PanelDetailIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewPhoneIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            WindowDetailPhone.Title = "Add Phone"
            WindowDetailPhone.Show()

            FormPanelPhoneDetail.Show()
            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewPhoneEmployerIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            'Daniel 2020 Des 28
            If ListPhoneEmployerDetail.Count < 1 Then
                'End 2020 Des 28
                ClearInput()
                WindowDetailPhone.Title = "Add Phone"
                WindowDetailPhone.Show()

                FormPanelPhoneDetail.Hide()
                PanelDetailPhone.Hide()
                PanelDetailPhoneEmployer.Hide()
                FormPanelPhoneEmployerDetail.Show()
                'Daniel 2020 Des 28
            Else
                Throw New Exception("can only enter one Workplace Phone Information")
            End If
            'end 2020 Des 28
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirector_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            GridEmail_Director.IsViewMode = False
            GridSanction_Director.IsViewMode = False
            GridPEP_Director.IsViewMode = False
            GridEmail_Director.LoadData(New List(Of DataModel.goAML_Ref_Customer_Email))
            GridSanction_Director.LoadData(New List(Of DataModel.goAML_Ref_Customer_Sanction))
            GridPEP_Director.LoadData(New List(Of DataModel.goAML_Ref_Customer_PEP))
            ClearInput()

            ' 2023-10-20, Nael: Instance objDirectorAddData sekalian set random PK untuk objDirectorAddData
            'tempflag = 0
            Dim objRand As New Random
            objDirectorAddData = New goAML_Ref_Walk_In_Customer_Director With {
                .NO_ID = -objRand.Next
            }


            WindowDetailAddress.Hide()
            WindowDetailPhone.Hide()
            WindowDetailDirector.Show()
            WindowDetailDirectorAddress.Hide()
            WindowDetailDirectorPhone.Hide()
            WindowDetailDirectorIdentification.Hide()

            FormPanelAddressDetail.Hide()
            FormPanelPhoneDetail.Hide()
            FP_Director.Show()


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()

            WindowDetailAddress.Hide()
            WindowDetailPhone.Hide()
            WindowDetailDirector.Show()
            WindowDetailDirectorAddress.Hide()
            WindowDetailDirectorPhone.Title = "Add Director Phone"
            WindowDetailDirectorPhone.Show()


            FormPanelAddressDetail.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelDirectorPhone.Show()


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorEmployerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()

            WindowDetailAddress.Hide()
            WindowDetailPhone.Hide()
            WindowDetailDirector.Show()
            WindowDetailDirectorAddress.Hide()
            WindowDetailDirectorPhone.Title = "Add Director Phone Employer"
            WindowDetailDirectorPhone.Show()

            FormPanelAddressDetail.Hide()
            FormPanelPhoneDetail.Hide()

            FormPanelEmpDirectorTaskDetail.Show()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()

            WindowDetailAddress.Hide()
            WindowDetailPhone.Hide()
            WindowDetailDirector.Show()
            WindowDetailDirectorAddress.Title = "Add Director Address"
            WindowDetailDirectorAddress.Show()
            WindowDetailDirectorPhone.Hide()

            FormPanelAddressDetail.Hide()
            FormPanelPhoneDetail.Hide()

            FP_Address_Director.Show()
            FP_Address_Emp_Director.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorEmployerAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()

            WindowDetailAddress.Hide()
            WindowDetailPhone.Hide()
            WindowDetailDirector.Show()
            WindowDetailDirectorAddress.Title = "Add Director Address Employer"
            WindowDetailDirectorAddress.Show()
            WindowDetailDirectorPhone.Hide()

            FormPanelAddressDetail.Hide()
            FormPanelPhoneDetail.Show()

            FP_Address_Emp_Director.Show()
            FP_Address_Director.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnAddNewAddressIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            FormPanelAddressDetail.Hidden = False
            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
            WindowDetailAddress.Title = "Add Address"
            WindowDetailAddress.Hidden = False
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewAddressEmployerIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            'Daniel 2020 Des 28
            If ListAddressEmployerDetail.Count < 1 Then
                'end 2020 Des 28
                ClearInput()
                FormPanelAddressDetailEmployer.Hidden = False
                FormPanelAddressDetail.Hidden = True
                PanelDetailAddressEmployer.Hidden = True
                PanelDetailAddress.Hidden = True
                WindowDetailAddress.Title = "Add Address Employer"
                WindowDetailAddress.Hidden = False
                'Daniel 2020 Des 28
            Else
                Throw New Exception("can only enter one Workplace Address Information")
            End If
            'end 2020 Des 28
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewPhoneCorporate_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            FormPanelAddressDetail.Hidden = True
            FormPanelPhoneDetail.Hidden = False
            WindowDetailPhone.Title = "Add Phone"
            WindowDetailPhone.Hidden = False
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewAddressCorporate_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            FormPanelAddressDetail.Hidden = False
            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
            WindowDetailAddress.Title = "Add Address"
            WindowDetailAddress.Hidden = False
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailIdentificationDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailIdentificationDirector Is Nothing Then
                SaveAddDetailIdentificationDirector()
            Else
                SaveEditDetailIdentificationDirector()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailIdentification Is Nothing Then
                SaveAddDetailIdentification()
            Else
                SaveEditDetailIdentification()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnSaveDetailPhone_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhone Is Nothing Then
                SaveAddDetailPhone()
            Else
                SaveEditDetailPhone()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailPhoneEmployer_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhoneEmployer Is Nothing Then
                SaveAddDetailPhoneEmployer()
            Else
                SaveEditDetailPhoneEmployer()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            ' TODO: cek ke list WICBLL nya untuk NO_ID
            Dim alreadySaved = ListDirectorDetail.Any(Function(x) x.NO_ID = objDirectorAddData.NO_ID)
            If Not alreadySaved Then
                SaveAddDetailDirector()
            Else
                SaveEditDetailDirector()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhoneDirector Is Nothing Then
                SaveAddDetailPhoneDirector()
            Else
                SaveEditDetailPhoneDirector()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorEmpPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhoneDirectorEmployer Is Nothing Then
                SaveAddDetailPhoneDirectorEmployer()
            Else
                SaveEditDetailPhoneDirectorEmployer()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailAddressDirector Is Nothing Then
                SaveAddDetailAddressDirector()
            Else
                SaveEditDetailAddressDirector()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnSaveDirectorEmpAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailAddressDirectorEmployer Is Nothing Then
                SaveAddDetailAddressDirectorEmployer()
            Else
                SaveEditDetailAddressDirectorEmployer()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub BtnSaveDetailAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If ObjDetailAddress Is Nothing Then
                SaveAddDetailAddress()
            Else
                SaveEditDetailAddress()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailAddressEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If ObjDetailAddressEmployer Is Nothing Then
                SaveAddDetailAddressEmployer()
            Else
                SaveEditDetailAddressEmployer()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelIdentification_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailIdentification.Hide()
            PanelDetailIdentification.Hide()
            FormPanelIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelIdentificationDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorIdentification.Hide()
            PanelDetailDirectorIdentification.Hide()
            FormPanelDirectorIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailIdentification_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailIdentification.Hide()
            PanelDetailDirectorIdentification.Hide()
            FormPanelDirectorIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailIdentificationDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorIdentification.Hide()
            PanelDetailIdentification.Hide()
            FormPanelIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()
            FormPanelDirectorPhone.Hide()
            FormPanelEmpDirectorTaskDetail.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorEmployerPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()
            FormPanelDirectorPhone.Hide()
            FormPanelEmpDirectorTaskDetail.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorDetailAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()
            FP_Address_Director.Hide()
            FP_Address_Emp_Director.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorDetailEmployerAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()
            FP_Address_Director.Hide()
            FP_Address_Emp_Director.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()

            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailPhoneEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()

            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()

            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelPhoneEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()

            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveCustomerDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirector.Hide()
            FP_Director.Hide()
            FormPanelDirectorDetail.Hide()
            ClearInput()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorPhones_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()
            FormPanelDirectorPhone.Hide()


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorEmpPhones_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()
            FormPanelEmpDirectorTaskDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            FP_Address_Director.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorEmpAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            FP_Address_Emp_Director.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailAddress.Hide()

            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()
            FormPanelAddressDetail.Hide()
            FormPanelAddressDetailEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddressEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailAddress.Hide()

            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()
            FormPanelAddressDetail.Hide()
            FormPanelAddressDetailEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailAddress.Hide()

            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()
            FormPanelAddressDetail.Hide()
            FormPanelAddressDetailEmployer.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelDetailAddressEmployer_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailAddress.Hide()

            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()
            FormPanelAddressDetail.Hide()
            FormPanelAddressDetailEmployer.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
#Region "Check Box"
    Protected Sub chkCorp_business_closed_Event(sender As Object, e As DirectEventArgs)
        If chkCorp_business_closed.Checked Then
            DateCorp_date_business_closed.Hidden = False
        Else
            DateCorp_date_business_closed.Hidden = True
        End If
    End Sub
    Protected Sub chkRE_business_closed_Event(sender As Object, e As DirectEventArgs)
        If cbx_re_business_closed.Checked Then
            df_re_date_business_closed.Hidden = False
        Else
            df_re_date_business_closed.Hidden = True
        End If
    End Sub
    Protected Sub checkBoxcb_Director_Deceased_click(sender As Object, e As DirectEventArgs)
        If cb_Director_Deceased.Checked Then
            txt_Director_Deceased_Date.Hidden = False
        Else
            txt_Director_Deceased_Date.Hidden = True
        End If
    End Sub
    Protected Sub checkBoxcb_RelatedPerson_Deceased_click(sender As Object, e As DirectEventArgs)
        If chk_RelatedPersonDeceased.Checked Then
            dt_TxtRelatedPersonDate_Deceased.Hidden = False
        Else
            dt_TxtRelatedPersonDate_Deceased.Hidden = True
        End If
    End Sub
    Protected Sub checkBoxcb_INDV_Deceased_click(sender As Object, e As DirectEventArgs)
        If CbINDV_Deceased.Checked Then
            DateINDV_Deceased_Date.Hidden = False
        Else
            DateINDV_Deceased_Date.Hidden = True
        End If
    End Sub
    Protected Sub cboFormWIC_ItemSelected(sender As Object, e As EventArgs)
        If cboFormWIC.Value = "Individu" Then
            WIC_Individu.Visible = True
            WIC_Corporate.Visible = False
            CleanFormCorporateWIC()
        Else
            WIC_Corporate.Visible = True
            WIC_Individu.Visible = False
            CleanFormIndividuWIC()
        End If
    End Sub
#End Region
#Region "Function"
    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailIdentification()
        Try
            Dim objNewDetailIdentification As New goAML_Person_Identification
            With objNewDetailIdentification
                .PK_Person_Identification_ID = ListIdentificationDetail.Select(Function(x) x.PK_Person_Identification_ID).DefaultIfEmpty(0).Max + 1
                .FK_Person_ID = If(objDirectorAddData Is Nothing, 0, objDirectorAddData.NO_ID)
                .FK_Person_Type = 8
                .Type = CmbTypeIdentification.Text
                .Number = TxtNumber.Text
                .Issue_Date = If(TxtIssueDate.Text = "1/1/0001 12:00:00 AM", TxtIssueDate.RawValue, Convert.ToDateTime(TxtIssueDate.Text))
                .Expiry_Date = If(TxtExpiryDate.Text = "1/1/0001 12:00:00 AM", TxtExpiryDate.RawValue, Convert.ToDateTime(TxtExpiryDate.Text))
                .Issued_By = TxtIssuedBy.Text
                .Issued_Country = CmbCountryIdentification.TextValue
                .Identification_Comment = TxtCommentIdentification.Text
            End With

            ListIdentificationDetail.Add(objNewDetailIdentification)
            BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
            FormPanelIdentification.Hidden = True
            WindowDetailIdentification.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailIdentificationDirector()
        Try
            Dim objNewDetailIdentification As New goAML_Person_Identification
            With objNewDetailIdentification
                .PK_Person_Identification_ID = ListDirectorIdentificationDetail.Select(Function(x) x.PK_Person_Identification_ID).DefaultIfEmpty(0).Max + 1
                .FK_Person_ID = If(objDirectorAddData Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, objDirectorAddData.NO_ID)
                .FK_Person_Type = 7
                .Type = CmbTypeIdentificationDirector.Text
                .Number = TxtNumberDirector.Text
                If TxtIssueDateDirector.RawValue IsNot Nothing Then
                    .Issue_Date = Convert.ToDateTime(TxtIssueDateDirector.Text)
                End If
                If TxtExpiryDateDirector.RawValue IsNot Nothing Then
                    .Expiry_Date = Convert.ToDateTime(TxtExpiryDateDirector.Text)
                End If
                .Issued_By = TxtIssuedByDirector.Text
                .Issued_Country = CmbCountryIdentificationDirector.TextValue
                .Identification_Comment = TxtCommentIdentificationDirector.Text

            End With

            ListDirectorIdentificationDetail.Add(objNewDetailIdentification)
            ' 2023-10-20, Nael
            If objDirectorAddData IsNot Nothing Then
                Dim currentDirIdentification = ListDirectorIdentificationDetail.Where(Function(x) x.FK_Person_ID = objDirectorAddData.NO_ID).ToList()
                BindDetailIdentification(StoreIdentificationDirectorDetail, currentDirIdentification)
            End If
            FormPanelDirectorIdentification.Hidden = True
            WindowDetailDirectorIdentification.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhone()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                .PK_goAML_Ref_Phone = ListPhoneDetail.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 3
                .FK_for_Table_ID = objWICAddData.PK_Customer_ID
                .Tph_Contact_Type = Cmbtph_contact_type.Text
                .Tph_Communication_Type = Cmbtph_communication_type.Text
                .tph_country_prefix = Txttph_country_prefix.Text
                .tph_number = Txttph_number.Text
                .tph_extension = Txttph_extension.Text
                .comments = TxtcommentsPhone.Text
            End With

            ListPhoneDetail.Add(objNewDetailPhone)
            If cboFormWIC.SelectedItem.Value = "Individu" Then
                BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
            Else
                BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
            End If
            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhoneEmployer()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                .PK_goAML_Ref_Phone = ListPhoneDetail.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 10
                .FK_for_Table_ID = objWICAddData.PK_Customer_ID
                .Tph_Contact_Type = Cmbtph_contact_typeEmployer.Text
                .Tph_Communication_Type = Cmbtph_communication_typeEmployer.Text
                .tph_country_prefix = Txttph_country_prefixEmployer.Text
                .tph_number = Txttph_numberEmployer.Text
                .tph_extension = Txttph_extensionEmployer.Text
                .comments = TxtcommentsPhoneEmployer.Text
            End With

            ListPhoneEmployerDetail.Add(objNewDetailPhone)
            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)
            FormPanelPhoneEmployerDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailDirector()
        Try
            'If cmb_Director_Gender.Value = "" Then
            '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
            'ElseIf cmb_peran.Value = "" Then
            '    Throw New Exception("Role Tidak boleh kosong")
            'ElseIf txt_Director_Last_Name.Text = "" Then
            '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
            'ElseIf txt_Director_BirthDate.Text = "1/1/0001 12:00:00 AM" Or txt_Director_BirthDate.Value = DateTime.MinValue Then
            '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
            'ElseIf txt_Director_Birth_Place.Text = "" Then
            '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
            'ElseIf cmb_Director_Nationality1.TextValue = "" Then
            '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
            'ElseIf cmb_Director_Residence.TextValue = "" Then
            '    Throw New Exception("Negara Domisili Tidak boleh kosong")
            'ElseIf ListDirectorPhoneDetail.Count <= 0 Then
            '    Throw New Exception("Informasi Telepon Director Tidak boleh kosong")
            'ElseIf ListDirectorAddressDetail.Count <= 0 Then
            '    Throw New Exception("Informasi Alamat Director Tidak boleh kosong")
            'ElseIf txt_Director_Occupation.Text = "" Then
            '    Throw New Exception("Pekerjaan Tidak boleh kosong")
            'ElseIf txt_Director_Source_of_Wealth.Text = "" Then
            '    Throw New Exception("Sumber Dana Tidak boleh kosong")
            If cmb_peran.Value = "" Then
                Throw New Exception("Role is Required")
            ElseIf txt_Director_Last_Name.Text = "" Then
                Throw New Exception("Full Name is Required")
            Else
                If txt_Director_SSN.Text.Length <> 16 And txt_Director_SSN.Text.Length > 0 Then
                    Throw New Exception("NIK Must 16 Digit")
                Else
                    If ListDirectorPhoneDetail Is Nothing Then
                        Throw New Exception("Phone is Required")
                    End If
                    If ListDirectorAddressDetail Is Nothing Then
                        Throw New Exception("Address is Required")
                    End If
                    If ListDirectorPhoneDetail.Count = 0 Then
                        Throw New Exception("Phone is Required")
                    End If
                    If ListDirectorAddressDetail.Count = 0 Then
                        Throw New Exception("Address is Required")
                    End If
                    With objDirectorAddData ' 2023-10-21, Nael
                        .Role = cmb_peran.Text
                        .Title = txt_Director_Title.Text
                        .Last_Name = txt_Director_Last_Name.Text
                        .Gender = cmb_Director_Gender.Text
                        'Daniel 18 jan 2021
                        If Not txt_Director_BirthDate.SelectedDate = DateTime.MinValue Then
                            .BirthDate = txt_Director_BirthDate.SelectedDate
                        End If
                        '.BirthDate = txt_Director_BirthDate.Text
                        'Daniel 18 jan 2021
                        .Birth_Place = txt_Director_Birth_Place.Text
                        .Mothers_Name = txt_Director_Mothers_Name.Text
                        .Alias = txt_Director_Alias.Text
                        .ID_Number = txt_Director_ID_Number.Text
                        .Passport_Number = txt_Director_Passport_Number.Text
                        .Passport_Country = txt_Director_Passport_Country.TextValue
                        .SSN = txt_Director_SSN.Text
                        .Nationality1 = cmb_Director_Nationality1.TextValue
                        .Nationality2 = cmb_Director_Nationality2.TextValue
                        .Nationality3 = cmb_Director_Nationality3.TextValue
                        .Residence = cmb_Director_Residence.TextValue
                        '.Email = txt_Director_Email.Text
                        '.Email2 = txt_Director_Email2.Text
                        '.Email3 = txt_Director_Email3.Text
                        '.Email4 = txt_Director_Email4.Text
                        '.Email5 = txt_Director_Email5.Text
                        .Deceased = If(cb_Director_Deceased.Checked, True, False)
                        If cb_Director_Deceased.Checked Then
                            .Deceased_Date = txt_Director_Deceased_Date.Text
                        End If
                        .Tax_Reg_Number = If(cb_Director_Tax_Reg_Number.Checked, True, False)
                        .Tax_Number = txt_Director_Tax_Number.Text
                        .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
                        .Occupation = txt_Director_Occupation.Text
                        .Comments = txt_Director_Comments.Text
                        .Employer_Name = txt_Director_employer_name.Text
                    End With

                    Dim currentDirAddress = ListDirectorAddressDetail.Where(Function(x) x.FK_To_Table_ID = objDirectorAddData.NO_ID).ToList()
                    Dim currentDirEmpAddress = ListDirectorEmployerAddressDetail.Where(Function(x) x.FK_To_Table_ID = objDirectorAddData.NO_ID).ToList()
                    Dim currentDirPhone = ListDirectorPhoneDetail.Where(Function(x) x.FK_for_Table_ID = objDirectorAddData.NO_ID).ToList()
                    Dim currentDirEmpPhone = ListDirectorEmployerPhoneDetail.Where(Function(x) x.FK_for_Table_ID = objDirectorAddData.NO_ID).ToList()
                    Dim currentDirIdentification = ListDirectorIdentificationDetail.Where(Function(x) x.FK_Person_ID = objDirectorAddData.NO_ID).ToList()

                    Dim DirectorClass As New WICDirectorDataBLL With {
                        .ObjDirector = objDirectorAddData,
                        .ListDirectorAddress = currentDirAddress,
                        .ListDirectorAddressEmployer = currentDirEmpAddress,
                        .ListDirectorPhone = currentDirPhone,
                        .ListDirectorPhoneEmployer = currentDirEmpPhone,
                        .ListDirectorIdentification = currentDirIdentification,
                        .ListDirectorEmail = GridEmail_Director.objListgoAML_Ref,
                        .ListDirectorSanction = GridSanction_Director.objListgoAML_Ref,
                        .ListDirectorPEP = GridPEP_Director.objListgoAML_Ref
                    }
                    ListDirectorDetail.Add(DirectorClass.ObjDirector)
                    ListDirectorDetailClass.Add(DirectorClass)
                    BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
                    WindowDetailDirector.Hidden = True
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhoneDirector()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                .PK_goAML_Ref_Phone = ListDirectorPhoneDetail.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 8
                .FK_for_Table_ID = If(objDirectorAddData Is Nothing, 0, objDirectorAddData.NO_ID) ' 2023-10-20, Nael
                .Tph_Contact_Type = Director_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_txt_phone_Country_prefix.Text
                .tph_number = Director_txt_phone_number.Text
                .tph_extension = Director_txt_phone_extension.Text
                .comments = Director_txt_phone_comments.Text
            End With

            ListDirectorPhoneDetail.Add(objNewDetailPhone)
            If objDirectorAddData IsNot Nothing Then
                Dim currentDirPhone = ListDirectorPhoneDetail.Where(Function(x) x.FK_for_Table_ID = objDirectorAddData.NO_ID).ToList() ' 2023-10-20, Nael
                BindDetailPhone(Store_DirectorPhone, currentDirPhone)
            End If
            FormPanelDirectorPhone.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhoneDirectorEmployer()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                .PK_goAML_Ref_Phone = ListDirectorEmployerPhoneDetail.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 4
                .FK_for_Table_ID = If(objDirectorAddData Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, objDirectorAddData.NO_ID)
                .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text
                .tph_number = Director_Emp_txt_phone_number.Text
                .tph_extension = Director_Emp_txt_phone_extension.Text
                .comments = Director_Emp_txt_phone_comments.Text
            End With

            ListDirectorEmployerPhoneDetail.Add(objNewDetailPhone)

            If objDirectorAddData IsNot Nothing Then
                Dim currentDirEmpPhone = ListDirectorEmployerPhoneDetail.Where(Function(x) x.FK_for_Table_ID = objDirectorAddData.NO_ID).ToList()
                BindDetailPhone(Store_Director_Employer_Phone, currentDirEmpPhone)
            End If
            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveEditDetailIdentification()
        Try
            With ObjDetailIdentification
                .Type = CmbTypeIdentification.Text
                .Number = TxtNumber.Text
                If TxtIssueDate.Text <> DateTime.MinValue Then
                    .Issue_Date = TxtIssueDate.Text
                Else
                    .Issue_Date = Nothing
                End If
                If TxtExpiryDate.Text <> DateTime.MinValue Then
                    .Expiry_Date = TxtExpiryDate.Text
                Else
                    .Expiry_Date = Nothing
                End If
                .Issued_By = TxtIssuedBy.Text
                .Issued_Country = CmbCountryIdentification.TextValue
                .Identification_Comment = TxtCommentIdentification.Text
            End With

            ObjDetailIdentification = Nothing

            BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
            FormPanelIdentification.Hidden = True
            WindowDetailIdentification.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailIdentificationDirector()
        Try
            With ObjDetailIdentificationDirector
                .Type = CmbTypeIdentificationDirector.Text
                .Number = TxtNumberDirector.Text
                If TxtIssueDateDirector.RawValue IsNot Nothing Then
                    .Issue_Date = TxtIssueDateDirector.Text
                Else
                    .Issue_Date = Nothing
                End If
                If TxtExpiryDateDirector.RawValue IsNot Nothing Then
                    .Expiry_Date = TxtExpiryDateDirector.Text
                Else
                    .Expiry_Date = Nothing
                End If
                .Issued_By = TxtIssuedByDirector.Text
                .Issued_Country = CmbCountryIdentificationDirector.TextValue
                .Identification_Comment = TxtCommentIdentificationDirector.Text
            End With

            ObjDetailIdentificationDirector = Nothing

            BindDetailIdentification(StoreIdentificationDirectorDetail, ListDirectorIdentificationDetail)
            FormPanelDirectorIdentification.Hidden = True
            WindowDetailDirectorIdentification.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhone()
        Try
            With ObjDetailPhone
                .Tph_Contact_Type = Cmbtph_contact_type.Text
                .Tph_Communication_Type = Cmbtph_communication_type.Text
                .tph_country_prefix = Txttph_country_prefix.Text
                .tph_number = Txttph_number.Text
                .tph_extension = Txttph_extension.Text
                .comments = TxtcommentsPhone.Text
            End With

            ObjDetailPhone = Nothing
            If cboFormWIC.SelectedItem.Value = "Individu" Then
                BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
            Else
                BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
            End If

            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhoneEmployer()
        Try
            With ObjDetailPhoneEmployer
                .Tph_Contact_Type = Cmbtph_contact_typeEmployer.Text
                .Tph_Communication_Type = Cmbtph_communication_typeEmployer.Text
                .tph_country_prefix = Txttph_country_prefixEmployer.Text
                .tph_number = Txttph_numberEmployer.Text
                .tph_extension = Txttph_extensionEmployer.Text
                .comments = TxtcommentsPhoneEmployer.Text
            End With

            ObjDetailPhoneEmployer = Nothing
            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)

            FormPanelPhoneEmployerDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailDirector()
        Try
            'If cmb_Director_Gender.Value = "" Then
            '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
            'ElseIf cmb_peran.Value = "" Then
            '    Throw New Exception("Role Tidak boleh kosong")
            'ElseIf txt_Director_Last_Name.Text = "" Then
            '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
            'ElseIf txt_Director_BirthDate.Text = "1/1/0001 12:00:00 AM" Or txt_Director_BirthDate.Value = DateTime.MinValue Then
            '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
            'ElseIf txt_Director_Birth_Place.Text = "" Then
            '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
            'ElseIf cmb_Director_Nationality1.TextValue = "" Then
            '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
            'ElseIf cmb_Director_Residence.TextValue = "" Then
            '    Throw New Exception("Negara Domisili Tidak boleh kosong")
            'ElseIf txt_Director_Occupation.Text = "" Then
            '    Throw New Exception("Pekerjaan Tidak boleh kosong")
            'ElseIf txt_Director_Source_of_Wealth.Text = "" Then
            '    Throw New Exception("Sumber Dana Tidak boleh kosong")
            'ElseIf txt_Director_SSN.Text.Length <> 16 And txt_Director_SSN.Text <> "" Then
            '    Throw New Exception("NIK Must 16 Digit")
            'End If

            If cmb_peran.Text = "" Then
                Throw New ApplicationException("Role is Required")
            End If
            If txt_Director_Last_Name.Text = "" Then
                Throw New ApplicationException("Full Name is Required")
            End If
            If ListDirectorPhoneDetail Is Nothing Then
                Throw New Exception("Phone is Required")
            End If
            If ListDirectorAddressDetail Is Nothing Then
                Throw New Exception("Address is Required")
            End If
            If ListDirectorPhoneDetail.Count = 0 Then
                Throw New Exception("Phone is Required")
            End If
            If ListDirectorAddressDetail.Count = 0 Then
                Throw New Exception("Address is Required")
            End If

            Dim DirectorClass As New WICDirectorDataBLL
            With objDirectorAddData
                .Role = cmb_peran.Text
                .Title = txt_Director_Title.Text
                .Last_Name = txt_Director_Last_Name.Text
                .Gender = cmb_Director_Gender.Text
                'Daniel 18 jan 2021
                If Not txt_Director_BirthDate.SelectedDate = DateTime.MinValue Then
                    .BirthDate = txt_Director_BirthDate.SelectedDate
                End If
                '.BirthDate = txt_Director_BirthDate.Text
                'Daniel 18 jan 2021
                .Birth_Place = txt_Director_Birth_Place.Text
                .Mothers_Name = txt_Director_Mothers_Name.Text
                .Alias = txt_Director_Alias.Text
                .ID_Number = txt_Director_ID_Number.Text
                .Passport_Number = txt_Director_Passport_Number.Text
                .Passport_Country = txt_Director_Passport_Country.TextValue
                .SSN = txt_Director_SSN.Text
                .Nationality1 = cmb_Director_Nationality1.TextValue
                .Nationality2 = cmb_Director_Nationality2.TextValue
                .Nationality3 = cmb_Director_Nationality3.TextValue
                .Residence = cmb_Director_Residence.TextValue
                '.Email = txt_Director_Email.Text
                '.Email2 = txt_Director_Email2.Text
                '.Email3 = txt_Director_Email3.Text
                '.Email4 = txt_Director_Email4.Text
                '.Email5 = txt_Director_Email5.Text
                .Deceased = If(cb_Director_Deceased.Checked, True, False)
                If cb_Director_Deceased.Checked Then
                    .Deceased_Date = txt_Director_Deceased_Date.Text
                End If
                .Tax_Reg_Number = If(cb_Director_Tax_Reg_Number.Checked, True, False)
                .Tax_Number = txt_Director_Tax_Number.Text
                .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
                .Occupation = txt_Director_Occupation.Text
                .Comments = txt_Director_Comments.Text
                .Employer_Name = txt_Director_employer_name.Text
            End With
            With DirectorClass
                .ObjDirector = objDirectorAddData
                .ListDirectorPhone = ListDirectorPhoneDetail
                .ListDirectorPhoneEmployer = ListDirectorEmployerPhoneDetail
                .ListDirectorAddress = ListDirectorAddressDetail
                .ListDirectorAddressEmployer = ListDirectorEmployerAddressDetail
                .ListDirectorIdentification = ListDirectorIdentificationDetail
                .ListDirectorEmail = GridEmail_Director.objListgoAML_Ref
                .ListDirectorSanction = GridSanction_Director.objListgoAML_Ref
                .ListDirectorPEP = GridPEP_Director.objListgoAML_Ref
            End With
            ListDirectorDetail.Remove(ListDirectorDetail.Where(Function(x) x.NO_ID = DirectorClass.ObjDirector.NO_ID).FirstOrDefault)
            ListDirectorDetailClass.Remove(ListDirectorDetailClass.Where(Function(x) x.ObjDirector.NO_ID = DirectorClass.ObjDirector.NO_ID).FirstOrDefault)
            ListDirectorDetail.Add(DirectorClass.ObjDirector)
            ListDirectorDetailClass.Add(DirectorClass)

            objDirectorAddData = Nothing

            BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
            WindowDetailDirector.Hidden = True
            FP_Director.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhoneDirector()
        Try
            With ObjDetailPhoneDirector
                .Tph_Contact_Type = Director_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_txt_phone_Country_prefix.Text
                .tph_number = Director_txt_phone_number.Text
                .tph_extension = Director_txt_phone_extension.Text
                .comments = Director_txt_phone_comments.Text
            End With



            ObjDetailPhoneDirector = Nothing
            BindDetailPhone(Store_DirectorPhone, ListDirectorPhoneDetail)

            FormPanelDirectorPhone.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhoneDirectorEmployer()
        Try
            With ObjDetailPhoneDirectorEmployer
                .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text
                .tph_number = Director_Emp_txt_phone_number.Text
                .tph_extension = Director_Emp_txt_phone_extension.Text
                .comments = Director_Emp_txt_phone_comments.Text
            End With

            ObjDetailPhoneDirectorEmployer = Nothing
            BindDetailPhone(Store_Director_Employer_Phone, ListDirectorEmployerPhoneDetail)

            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveAddDetailAddress()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                .PK_Customer_Address_ID = ListAddressDetail.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 3
                .FK_To_Table_ID = objWICAddData.PK_Customer_ID
                .Address_Type = CmbAddress_type.Text
                .Address = TxtAddress.Text
                .Town = TxtTown.Text
                .City = TxtCity.Text
                .Zip = TxtZip.Text
                .Country_Code = Cmbcountry_code.TextValue
                .State = TxtState.Text
                .Comments = TxtcommentsAddress.Text
            End With

            ListAddressDetail.Add(objNewDetailAddress)
            If cboFormWIC.SelectedItem.Value = "Individu" Then
                BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
            Else
                BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
            End If

            FormPanelAddressDetail.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailAddressEmployer()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                .PK_Customer_Address_ID = ListAddressEmployerDetail.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 10
                .FK_To_Table_ID = objWICAddData.PK_Customer_ID
                .Address_Type = CmbAddress_typeEmployer.Text
                .Address = TxtAddressEmployer.Text
                .Town = TxtTownEmployer.Text
                .City = TxtCityEmployer.Text
                .Zip = TxtZipEmployer.Text
                .Country_Code = Cmbcountry_codeEmployer.TextValue
                .State = TxtStateEmployer.Text
                .Comments = TxtcommentsAddressEmployer.Text
            End With

            ListAddressEmployerDetail.Add(objNewDetailAddress)
            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)

            FormPanelAddressDetailEmployer.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailAddressDirector()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                .PK_Customer_Address_ID = ListDirectorAddressDetail.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 8
                .FK_To_Table_ID = If(objDirectorAddData Is Nothing, 0, objDirectorAddData.NO_ID)
                .Address_Type = Director_cmb_kategoriaddress.Text
                .Address = Director_Address.Text
                .Town = Director_Town.Text
                .City = Director_City.Text
                .Zip = Director_Zip.Text
                .Country_Code = Director_cmb_kodenegara.TextValue
                .State = Director_State.Text
                .Comments = Director_Comment_Address.Text
            End With

            ListDirectorAddressDetail.Add(objNewDetailAddress)
            If objDirectorAddData IsNot Nothing Then
                Dim currentDirAddress = ListDirectorAddressDetail.Where(Function(x) x.FK_To_Table_ID = objDirectorAddData.NO_ID).ToList()
                BindDetailAddress(Store_DirectorAddress, currentDirAddress)
            End If

            FP_Address_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailAddressDirectorEmployer()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                .PK_Customer_Address_ID = ListDirectorEmployerAddressDetail.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 4
                .FK_To_Table_ID = If(objDirectorAddData Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, objDirectorAddData.NO_ID)
                .Address_Type = Director_cmb_emp_kategoriaddress.Text
                .Address = Director_emp_Address.Text
                .Town = Director_emp_Town.Text
                .City = Director_emp_City.Text
                .Zip = Director_emp_Zip.Text
                .Country_Code = Director_cmb_emp_kodenegara.TextValue
                .State = Director_emp_State.Text
                .Comments = Director_emp_Comment_Address.Text
            End With

            ListDirectorEmployerAddressDetail.Add(objNewDetailAddress)
            If objDirectorAddData IsNot Nothing Then
                Dim listDirEmpAddress = ListDirectorEmployerAddressDetail.Where(Function(x) x.FK_To_Table_ID = objDirectorAddData.NO_ID).ToList()
                BindDetailAddress(Store_Director_Employer_Address, listDirEmpAddress)
            End If

            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveEditDetailAddress()
        Try
            With ObjDetailAddress
                .Address_Type = CmbAddress_type.Text
                .Address = TxtAddress.Text
                .Town = TxtTown.Text
                .City = TxtCity.Text
                .Zip = TxtZip.Text
                .Country_Code = Cmbcountry_code.TextValue
                .State = TxtState.Text
                .Comments = TxtcommentsAddress.Text
            End With

            ObjDetailAddress = Nothing
            If cboFormWIC.SelectedItem.Value = "Individu" Then
                BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
            Else
                BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
            End If

            FormPanelAddressDetail.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailAddressEmployer()
        Try
            With ObjDetailAddressEmployer
                .Address_Type = CmbAddress_typeEmployer.Text
                .Address = TxtAddressEmployer.Text
                .Town = TxtTownEmployer.Text
                .City = TxtCityEmployer.Text
                .Zip = TxtZipEmployer.Text
                .Country_Code = Cmbcountry_codeEmployer.TextValue
                .State = TxtStateEmployer.Text
                .Comments = TxtcommentsAddressEmployer.Text
            End With

            ObjDetailAddressEmployer = Nothing
            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)

            FormPanelAddressDetailEmployer.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailAddressDirector()
        Try
            With ObjDetailAddressDirector
                .Address_Type = Director_cmb_kategoriaddress.Text
                .Address = Director_Address.Text
                .Town = Director_Town.Text
                .City = Director_City.Text
                .Zip = Director_Zip.Text
                .Country_Code = Director_cmb_kodenegara.TextValue
                .State = Director_State.Text
                .Comments = Director_Comment_Address.Text
            End With

            ObjDetailAddressDirector = Nothing
            BindDetailAddress(Store_DirectorAddress, ListDirectorAddressDetail)

            FP_Address_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailAddressDirectorEmployer()
        Try
            With ObjDetailAddressDirectorEmployer
                .Address_Type = Director_cmb_emp_kategoriaddress.Text
                .Address = Director_emp_Address.Text
                .Town = Director_emp_Town.Text
                .City = Director_emp_City.Text
                .Zip = Director_emp_Zip.Text
                .Country_Code = Director_cmb_emp_kodenegara.TextValue
                .State = Director_emp_State.Text
                .Comments = Director_emp_Comment_Address.Text
            End With

            ObjDetailAddressDirectorEmployer = Nothing
            BindDetailAddress(Store_Director_Employer_Address, ListDirectorEmployerAddressDetail)

            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailPhoneIndividu(id As String)
        Try
            BtnAddNewPhoneIndividu_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Phone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If objDelete IsNot Nothing Then
                ListPhoneDetail.Remove(objDelete)
                BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
                WindowDetailPhone.Hide()
                FormPanelPhoneDetail.Hide()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailPhoneIndividuEmployer(id As String)
        Try
            'BtnAddNewPhoneEmployerIndividu_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Phone = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not objDelete Is Nothing Then
                ListPhoneEmployerDetail.Remove(objDelete)
                BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)
                WindowDetailPhone.Hide()
                FormPanelPhoneEmployerDetail.Hide()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailDirector(id As String)
        Try
            btnAddDirector_DirectClick(Nothing, Nothing)
            'daniel 21 jan 2021
            Dim objDirector As WICDirectorDataBLL = ListDirectorDetailClass.Find(Function(x) x.ObjDirector.NO_ID = id)
            Dim objDelete As goAML_Ref_Walk_In_Customer_Director = ListDirectorDetail.Find(Function(x) x.NO_ID = id)
            If Not objDelete Is Nothing Then
                ListDirectorDetail.Remove(objDelete)
            End If
            If objDirector IsNot Nothing Then
                ListDirectorDetailClass.Remove(objDirector)
            End If
            BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
            WindowDetailDirector.Hide()
            FP_Director.Hide()
            'end 21 jan 2021
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailIdentificationDirector(id As String)
        Try
            btnAddDirectorIdentification_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Person_Identification = ListDirectorIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
            If Not objDelete Is Nothing Then
                ListDirectorIdentificationDetail.Remove(objDelete)
                BindDetailIdentification(StoreIdentificationDirectorDetail, ListDirectorIdentificationDetail)
                WindowDetailDirectorIdentification.Hide()
                FormPanelDirectorIdentification.Hide()
                PanelDetailDirectorIdentification.Hide()
                FP_Director.Show()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailIdentification(id As String)
        Try
            btnAddIdentification_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Person_Identification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
            If Not objDelete Is Nothing Then
                ListIdentificationDetail.Remove(objDelete)
                BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
                WindowDetailIdentification.Hide()
                FormPanelIdentification.Hide()
                PanelDetailIdentification.Hide()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailPhoneDirector(id As String)
        Try
            btnAddDirectorPhones_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Phone = ListDirectorPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not objDelete Is Nothing Then
                ListDirectorPhoneDetail.Remove(objDelete)
                BindDetailPhone(Store_DirectorPhone, ListDirectorPhoneDetail)
                WindowDetailDirectorPhone.Hide()
                FormPanelDirectorPhone.Hide()
                WindowDetailPhone.Hide()
                WindowDetailDirector.Show()
                FP_Director.Show()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailPhoneDirectorEmployer(id As String)
        Try
            btnAddDirectorEmployerPhones_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Phone = ListDirectorEmployerPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not objDelete Is Nothing Then
                ListDirectorEmployerPhoneDetail.Remove(objDelete)
                BindDetailPhone(Store_Director_Employer_Phone, ListDirectorEmployerPhoneDetail)
                WindowDetailDirectorPhone.Hide()
                FormPanelEmpDirectorTaskDetail.Hide()
                WindowDetailDirector.Show()
                WindowDetailPhone.Hide()
                FP_Director.Show()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataEditPhoneIndividu(id As String)
        Try
            ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhone Is Nothing Then
                FormPanelPhoneDetail.Show()
                WindowDetailPhone.Title = "Edit Phone"
                WindowDetailPhone.Show()

                With ObjDetailPhone
                    Cmbtph_contact_type.SetValueAndFireSelect(.Tph_Contact_Type)
                    Cmbtph_communication_type.SetValueAndFireSelect(.Tph_Communication_Type)
                    Txttph_country_prefix.Text = .tph_country_prefix
                    Txttph_number.Text = .tph_number
                    Txttph_extension.Text = .tph_extension
                    TxtcommentsPhone.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditPhoneIndividuEmployer(id As String)
        Try
            ObjDetailPhoneEmployer = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhoneEmployer Is Nothing Then
                FormPanelPhoneEmployerDetail.Show()
                WindowDetailPhone.Title = "Edit Phone"
                WindowDetailPhone.Show()

                With ObjDetailPhoneEmployer
                    Cmbtph_contact_typeEmployer.SetValueAndFireSelect(.Tph_Contact_Type)
                    Cmbtph_communication_typeEmployer.SetValueAndFireSelect(.Tph_Communication_Type)
                    Txttph_country_prefixEmployer.Text = .tph_country_prefix
                    Txttph_numberEmployer.Text = .tph_number
                    Txttph_extensionEmployer.Text = .tph_extension
                    TxtcommentsPhoneEmployer.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditDirector(id As String)
        Try
            Dim listDirectorDetails As WICDirectorDataBLL = ListDirectorDetailClass.Where(Function(x) x.ObjDirector.NO_ID = id).FirstOrDefault()
            'objDirectorAddData = listDirectorDetails.ObjDirector
            ListDirectorAddressDetail = listDirectorDetails.ListDirectorAddress.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList()
            ListDirectorEmployerAddressDetail = listDirectorDetails.ListDirectorAddressEmployer.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList()
            ListDirectorPhoneDetail = listDirectorDetails.ListDirectorPhone.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList()
            ListDirectorEmployerPhoneDetail = listDirectorDetails.ListDirectorPhoneEmployer.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList()
            ListDirectorIdentificationDetail = listDirectorDetails.ListDirectorIdentification.Where(Function(x) x.FK_Person_ID = id And x.FK_Person_Type = 7).ToList()

            'daniel 21 jan 2021
            'ClearInput()
            'tempflag = id ' 2023-10-20, Nael
            objDirectorAddData = listDirectorDetails.ObjDirector
            'end 21 jan 2021
            If Not objDirectorAddData Is Nothing Then
                FP_Director.Show()
                FormPanelDirectorDetail.Hide()
                WindowDetailDirector.Show()
                'daniel 18 jan 2021
                'ClearInput()
                'clearinputDirector()
                'end 18 jan 2021
                With objDirectorAddData
                    cmb_peran.Text = .Role
                    txt_Director_Title.Text = .Title
                    txt_Director_Last_Name.Text = .Last_Name
                    cmb_Director_Gender.Text = .Gender
                    'daniel 18 jan 2021
                    If .BirthDate.HasValue Then
                        txt_Director_BirthDate.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                    End If
                    'end 18 jan 2021
                    txt_Director_Birth_Place.Text = .Birth_Place
                    txt_Director_Mothers_Name.Text = .Mothers_Name
                    txt_Director_Alias.Text = .Alias
                    txt_Director_ID_Number.Text = .ID_Number
                    txt_Director_Passport_Number.Text = .Passport_Number
                    'txt_Director_Passport_Country.Text = .Passport_Country
                    If Not IsNothing(.Passport_Country) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Passport_Country & "'")
                        If DataRowTemp IsNot Nothing Then
                            txt_Director_Passport_Country.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    txt_Director_SSN.Text = .SSN
                    If Not IsNothing(.Nationality1) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Nationality1 & "'")
                        If DataRowTemp IsNot Nothing Then
                            cmb_Director_Nationality1.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    If Not IsNothing(.Nationality2) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Nationality2 & "'")
                        If DataRowTemp IsNot Nothing Then
                            cmb_Director_Nationality2.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    If Not IsNothing(.Nationality3) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Nationality3 & "'")
                        If DataRowTemp IsNot Nothing Then
                            cmb_Director_Nationality3.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    If Not IsNothing(.Residence) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Residence & "'")
                        If DataRowTemp IsNot Nothing Then
                            cmb_Director_Residence.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    'cmb_Director_Nationality1.SetValueAndFireSelect(.Nationality1)
                    'cmb_Director_Nationality2.SetValueAndFireSelect(.Nationality2)
                    'cmb_Director_Nationality3.SetValueAndFireSelect(.Nationality3)
                    'cmb_Director_Residence.SetValueAndFireSelect(.Residence)
                    'txt_Director_Email.Text = .Email
                    'txt_Director_Email2.Text = .Email2
                    'txt_Director_Email3.Text = .Email3
                    'txt_Director_Email4.Text = .Email4
                    'txt_Director_Email5.Text = .Email5
                    cb_Director_Deceased.Checked = .Deceased
                    If cb_Director_Deceased.Checked Then
                        txt_Director_Deceased_Date.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                    End If
                    cb_Director_Tax_Reg_Number.Checked = .Tax_Reg_Number
                    txt_Director_Tax_Number.Text = .Tax_Number
                    txt_Director_Source_of_Wealth.Text = .Source_of_Wealth
                    txt_Director_Occupation.Text = .Occupation
                    txt_Director_Comments.Text = .Comments
                    txt_Director_employer_name.Text = .Employer_Name
                End With

                GridEmail_Director.IsViewMode = False
                GridSanction_Director.IsViewMode = False
                GridPEP_Director.IsViewMode = False

                GridEmail_Director.LoadData(listDirectorDetails.ListDirectorEmail)
                GridSanction_Director.LoadData(listDirectorDetails.ListDirectorSanction)
                GridPEP_Director.LoadData(listDirectorDetails.ListDirectorPEP)

                BindDetailPhone(Store_DirectorPhone, ListDirectorPhoneDetail)
                BindDetailPhone(Store_Director_Employer_Phone, ListDirectorEmployerPhoneDetail)
                BindDetailAddress(Store_DirectorAddress, ListDirectorAddressDetail)
                BindDetailAddress(Store_Director_Employer_Address, ListDirectorEmployerAddressDetail)
                BindDetailIdentification(StoreIdentificationDirectorDetail, ListDirectorIdentificationDetail)

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataEditIdentificationDirector(id As String)
        Try
            ObjDetailIdentificationDirector = ListDirectorIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
            If Not ObjDetailIdentificationDirector Is Nothing Then
                FormPanelDirectorIdentification.Show()
                PanelDetailDirectorIdentification.Hide()
                WindowDetailDirectorIdentification.Title = "Edit Identitas"
                WindowDetailDirectorIdentification.Show()

                With ObjDetailIdentificationDirector
                    CmbTypeIdentificationDirector.SetValue(.Type)
                    TxtNumberDirector.Text = .Number
                    TxtIssueDateDirector.Text = If(.Issue_Date Is Nothing, "", .Issue_Date.Value.ToString("dd-MMM-yy"))
                    TxtExpiryDateDirector.Text = If(.Expiry_Date Is Nothing, "", .Expiry_Date.Value.ToString("dd-MMM-yy"))
                    TxtIssuedByDirector.Text = .Issued_By
                    If Not IsNothing(.Issued_Country) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Issued_Country & "'")
                        If DataRowTemp IsNot Nothing Then
                            CmbCountryIdentificationDirector.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    'CmbCountryIdentificationDirector.SetValue(.Issued_Country)
                    TxtCommentIdentificationDirector.Text = .Identification_Comment
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditIdentification(id As String)
        Try
            ObjDetailIdentification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
            If Not ObjDetailIdentification Is Nothing Then
                FormPanelIdentification.Show()
                PanelDetailIdentification.Hide()
                WindowDetailIdentification.Show()

                With ObjDetailIdentification
                    CmbTypeIdentification.SetValue(.Type)
                    TxtNumber.Text = .Number
                    TxtIssueDate.Text = If(.Issue_Date Is Nothing, "", .Issue_Date.Value.ToString("dd-MMM-yy"))
                    TxtExpiryDate.Text = If(.Expiry_Date Is Nothing, "", .Expiry_Date.Value.ToString("dd-MMM-yy"))
                    TxtIssuedBy.Text = .Issued_By
                    ' CmbCountryIdentification.SetValue(.Issued_Country)
                    If Not IsNothing(.Issued_Country) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Issued_Country & "'")
                        If DataRowTemp IsNot Nothing Then
                            CmbCountryIdentification.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    TxtCommentIdentification.Text = .Identification_Comment
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataEditPhoneDirector(id As String)
        Try
            ObjDetailPhoneDirector = ListDirectorPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhoneDirector Is Nothing Then
                FormPanelDirectorPhone.Show()
                PanelPhoneDirector.Hide()
                PanelPhoneDirectorEmployer.Hide()
                FormPanelEmpDirectorTaskDetail.Hide()
                WindowDetailDirectorPhone.Title = "Edit Director Phone"
                WindowDetailDirectorPhone.Show()

                With ObjDetailPhoneDirector
                    Director_cb_phone_Contact_Type.SetValueAndFireSelect(.Tph_Contact_Type)
                    Director_cb_phone_Communication_Type.SetValueAndFireSelect(.Tph_Communication_Type)
                    Director_txt_phone_Country_prefix.Text = .tph_country_prefix
                    Director_txt_phone_number.Text = .tph_number
                    Director_txt_phone_extension.Text = .tph_extension
                    Director_txt_phone_comments.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditPhoneDirectorEmployer(id As String)
        Try
            ObjDetailPhoneDirectorEmployer = ListDirectorEmployerPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhoneDirectorEmployer Is Nothing Then
                FormPanelEmpDirectorTaskDetail.Show()
                FormPanelDirectorPhone.Hide()
                PanelPhoneDirector.Hide()
                PanelPhoneDirectorEmployer.Hide()
                WindowDetailDirectorPhone.Title = "Edit Director Phone Employer"
                WindowDetailDirectorPhone.Show()

                With ObjDetailPhoneDirectorEmployer
                    Director_Emp_cb_phone_Contact_Type.SetValueAndFireSelect(.Tph_Contact_Type)
                    Director_Emp_cb_phone_Communication_Type.SetValueAndFireSelect(.Tph_Communication_Type)
                    Director_Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                    Director_Emp_txt_phone_number.Text = .tph_number
                    Director_Emp_txt_phone_extension.Text = .tph_extension
                    Director_Emp_txt_phone_comments.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailAddressIndividu(id As String)
        Try
            BtnAddNewAddressIndividu_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Address = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not objDelete Is Nothing Then
                ListAddressDetail.Remove(objDelete)
                FormPanelAddressDetail.Hide()
                WindowDetailAddress.Hide()
                BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailAddressEmployerIndividu(id As String)
        Try
            'BtnAddNewAddressEmployerIndividu_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Address = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not objDelete Is Nothing Then
                ListAddressEmployerDetail.Remove(objDelete)
                PanelDetailAddress.Hide()
                PanelDetailAddressEmployer.Hide()
                FormPanelAddressDetail.Hide()
                FormPanelAddressDetailEmployer.Hide()
                WindowDetailAddress.Hide()
                BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailAddressDirector(id As String)
        Try
            btnAddDirectorAddresses_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Address = ListDirectorAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not objDelete Is Nothing Then
                ListDirectorAddressDetail.Remove(objDelete)
                FP_Address_Director.Hide()
                WindowDetailDirectorAddress.Hide()
                BindDetailAddress(Store_DirectorAddress, ListDirectorAddressDetail)
                WindowDetailDirector.Show()
                WindowDetailAddress.Hide()
                FP_Director.Show()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailAddressDirectorEmployer(id As String)
        Try
            btnAddDirectorEmployerAddresses_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Address = ListDirectorEmployerAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not objDelete Is Nothing Then
                ListDirectorEmployerAddressDetail.Remove(objDelete)
                FP_Address_Director.Hide()
                WindowDetailDirectorAddress.Hide()
                BindDetailAddress(Store_Director_Employer_Address, ListDirectorEmployerAddressDetail)
                WindowDetailDirector.Show()
                FP_Director.Show()
                WindowDetailAddress.Hide()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditAddressIndividu(id As String)
        Try
            ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 3)
            If Not ObjDetailAddress Is Nothing Then
                FormPanelAddressDetail.Show()
                FormPanelAddressDetailEmployer.Hide()
                PanelDetailAddress.Hide()
                PanelDetailAddressEmployer.Hide()
                WindowDetailAddress.Title = "Edit Address"
                WindowDetailAddress.Show()

                With ObjDetailAddress
                    CmbAddress_type.SetValueAndFireSelect(.Address_Type)
                    TxtAddress.Text = .Address
                    TxtTown.Text = .Town
                    TxtCity.Text = .City
                    TxtZip.Text = .Zip
                    If Not IsNothing(.Country_Code) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                        If DataRowTemp IsNot Nothing Then
                            Cmbcountry_code.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    'Cmbcountry_code.SetValueAndFireSelect(.Country_Code)
                    TxtState.Text = .State
                    TxtcommentsAddress.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditAddressEmployerIndividu(id As String)
        Try
            ObjDetailAddressEmployer = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 10)
            If Not ObjDetailAddressEmployer Is Nothing Then
                FormPanelAddressDetailEmployer.Show()
                WindowDetailAddress.Title = "Edit Address Employer"
                WindowDetailAddress.Show()

                With ObjDetailAddressEmployer
                    CmbAddress_typeEmployer.SetValueAndFireSelect(.Address_Type)
                    TxtAddressEmployer.Text = .Address
                    TxtTownEmployer.Text = .Town
                    TxtCityEmployer.Text = .City
                    TxtZipEmployer.Text = .Zip
                    If Not IsNothing(.Country_Code) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                        If DataRowTemp IsNot Nothing Then
                            Cmbcountry_codeEmployer.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    '  Cmbcountry_codeEmployer.SetValueAndFireSelect(.Country_Code)
                    TxtStateEmployer.Text = .State
                    TxtcommentsAddressEmployer.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditAddressDirector(id As String)
        Try
            ObjDetailAddressDirector = ListDirectorAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not ObjDetailAddressDirector Is Nothing Then
                FP_Address_Director.Show()
                FP_Address_Emp_Director.Hide()
                PanelAddressDirector.Hide()
                PanelAddressDirectorEmployer.Hide()
                WindowDetailDirectorAddress.Title = "Edit Director Address"
                WindowDetailDirectorAddress.Show()

                With ObjDetailAddressDirector
                    Director_cmb_kategoriaddress.SetValueAndFireSelect(.Address_Type)
                    Director_Address.Text = .Address
                    Director_Town.Text = .Town
                    Director_City.Text = .City
                    Director_Zip.Text = .Zip
                    If Not IsNothing(.Country_Code) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                        If DataRowTemp IsNot Nothing Then
                            'Daniel 2021 Jan 5
                            'cmb_Director_Nationality1.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            Director_cmb_kodenegara.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            'end 2021 Jan 5
                        End If
                    End If
                    'Director_cmb_kodenegara.SetValueAndFireSelect(.Country_Code)
                    Director_State.Text = .State
                    Director_Comment_Address.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadDataEditAddressDirectorEmployer(id As String)
        Try
            ObjDetailAddressDirectorEmployer = ListDirectorEmployerAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not ObjDetailAddressDirectorEmployer Is Nothing Then
                FP_Address_Emp_Director.Show()
                FP_Address_Director.Hide()
                PanelAddressDirector.Hide()
                PanelAddressDirectorEmployer.Hide()
                WindowDetailDirectorAddress.Title = "Edit Director Address Employer"
                WindowDetailDirectorAddress.Show()

                With ObjDetailAddressDirectorEmployer
                    Director_cmb_emp_kategoriaddress.SetValueAndFireSelect(.Address_Type)
                    Director_emp_Address.Text = .Address
                    Director_emp_Town.Text = .Town
                    Director_emp_City.Text = .City
                    Director_emp_Zip.Text = .Zip
                    ' Director_cmb_emp_kodenegara.SetValueAndFireSelect(.Country_Code)
                    If Not IsNothing(.Country_Code) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                        If DataRowTemp IsNot Nothing Then
                            Director_cmb_emp_kodenegara.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    Director_emp_State.Text = .State
                    Director_emp_Comment_Address.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailPhoneCorporate(id As String)
        Try
            BtnAddNewPhoneCorporate_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Phone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not objDelete Is Nothing Then
                ListPhoneDetail.Remove(objDelete)
                WindowDetailPhone.Hide()
                FormPanelPhoneDetail.Hide()
                BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataEditPhoneCorporate(id As String)
        Try
            ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhone Is Nothing Then
                FormPanelPhoneDetail.Show()
                PanelDetailPhone.Hide()
                WindowDetailPhone.Title = "Edit Phone"
                WindowDetailPhone.Show()
                ClearInput()
                With ObjDetailPhone
                    Cmbtph_contact_type.SetValueAndFireSelect(.Tph_Contact_Type)
                    Cmbtph_communication_type.SetValueAndFireSelect(.Tph_Communication_Type)
                    Txttph_country_prefix.Text = .tph_country_prefix
                    Txttph_number.Text = .tph_number
                    Txttph_extension.Text = .tph_extension
                    TxtcommentsPhone.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailAddressCorporate(id As String)
        Try
            BtnAddNewAddressCorporate_DirectClick(Nothing, Nothing)
            Dim objDelete As goAML_Ref_Address = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not objDelete Is Nothing Then
                ListAddressDetail.Remove(objDelete)
                WindowDetailAddress.Hide()
                FormPanelAddressDetail.Hide()
                BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataEditAddressCorporate(id As String)
        Try
            ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not ObjDetailAddress Is Nothing Then
                FormPanelAddressDetail.Show()
                WindowDetailAddress.Title = "Edit Address"
                WindowDetailAddress.Show()
                PanelDetailAddress.Hide()
                ClearInput()
                With ObjDetailAddress
                    CmbAddress_type.SetValueAndFireSelect(.Address_Type)
                    TxtAddress.Text = .Address
                    TxtTown.Text = .Town
                    TxtCity.Text = .City
                    TxtZip.Text = .Zip
                    If Not IsNothing(.Country_Code) Then
                        DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                        If DataRowTemp IsNot Nothing Then
                            Cmbcountry_code.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                        End If
                    End If
                    ' Cmbcountry_code.SetValueAndFireSelect(.Country_Code)
                    TxtState.Text = .State
                    TxtcommentsAddress.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
#Region "Grid"
    Protected Sub GrdCmdCustDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditIdentificationDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailIdentificationDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordIdentificationDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordIdentification(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhoneDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhoneDirectorEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhone(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneEmployerIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneIndividuEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneIndividuEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhoneEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GrdCmdSocialMedia(sender As Object, e As Ext.Net.DirectEventArgs)
    '    Try
    '        Dim id As String = e.ExtraParams(0).Value
    '        '
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Private Sub DetailRecordPhone(id As String)
        Try
            ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhone Is Nothing Then

                PanelDetailPhone.Show()
                PanelDetailPhoneEmployer.Hide()
                FormPanelPhoneDetail.Hide()
                FormPanelPhoneEmployerDetail.Hide()
                WindowDetailPhone.Title = "Detail Phone"
                WindowDetailPhone.Show()
                ClearInput()
                With ObjDetailPhone
                    Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                    Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                    Dsptph_country_prefix.Text = .tph_country_prefix
                    Dsptph_number.Text = .tph_number
                    Dsptph_extension.Text = .tph_extension
                    DspcommentsPhone.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordPhoneEmployer(id As String)
        Try
            ObjDetailPhoneEmployer = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhoneEmployer Is Nothing Then

                PanelDetailPhone.Hide()
                PanelDetailPhoneEmployer.Show()
                FormPanelPhoneDetail.Hide()
                FormPanelPhoneEmployerDetail.Hide()
                WindowDetailPhone.Title = "Detail Phone"
                WindowDetailPhone.Show()
                ClearInput()
                With ObjDetailPhoneEmployer
                    Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                    Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                    Dsptph_country_prefixEmployer.Text = .tph_country_prefix
                    Dsptph_numberEmployer.Text = .tph_number
                    Dsptph_extensionEmployer.Text = .tph_extension
                    DspcommentsPhoneEmployer.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordDirector(id As String)
        Try
            Dim DirectorClass As WICDirectorDataBLL = ListDirectorDetailClass.Where(Function(x) x.ObjDirector.NO_ID = id).FirstOrDefault
            objDirectorAddData = DirectorClass.ObjDirector
            ListDirectorAddressDetail = DirectorClass.ListDirectorAddress.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList
            ListDirectorEmployerAddressDetail = DirectorClass.ListDirectorAddressEmployer.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList
            ListDirectorPhoneDetail = DirectorClass.ListDirectorPhone.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList
            ListDirectorEmployerPhoneDetail = DirectorClass.ListDirectorPhoneEmployer.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList
            ListDirectorIdentificationDetail = DirectorClass.ListDirectorIdentification.Where(Function(x) x.FK_Person_ID = id And x.FK_Person_Type = 7).ToList

            If Not objDirectorAddData Is Nothing Then
                FP_Director.Hide()
                FormPanelDirectorDetail.Show()
                WindowDetailDirector.Show()
                'ClearInput()
                With objDirectorAddData
                    DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
                    DspGelarDirector.Text = .Title
                    DspNamaLengkap.Text = .Last_Name
                    DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                    'daniel 18 jan 2021
                    If .BirthDate.HasValue Then
                        DspTanggalLahir.Text = formatDate(.BirthDate, "dd-MMM-yy")
                    Else
                        DspTanggalLahir.Text = ""
                    End If
                    'DspTanggalLahir.Text = formatDate(.BirthDate, "dd-MMM-yy")
                    'end 18 jan 2021
                    DspTempatLahir.Text = .Birth_Place
                    DspNamaIbuKandung.Text = .Mothers_Name
                    DspNamaAlias.Text = .Alias
                    DspNIK.Text = .SSN
                    DspNoPassport.Text = .Passport_Number
                    'daniel 19 jan 2021
                    DspNegaraPenerbitPassport.Text = GlobalReportFunctionBLL.getCountryByCode(.Passport_Country)
                    'DspNegaraPenerbitPassport.Text = .Passport_Country
                    'end 19 jan 2021
                    DspNoIdentitasLain.Text = .ID_Number
                    DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                    DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                    DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                    DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                    'DspEmail.Text = .Email
                    'DspEmail2.Text = .Email2
                    'DspEmail3.Text = .Email3
                    'DspEmail4.Text = .Email4
                    'DspEmail5.Text = .Email5
                    DspDeceased.Text = .Deceased
                    If .Deceased Then
                        'Daniel 2021 Jan 7
                        If .Deceased_Date IsNot Nothing Then
                            DspDeceasedDate.Text = formatDate(.Deceased_Date, "dd-MMM-yy")
                            DspDeceasedDate.Hidden = False
                        End If
                    Else
                        DspDeceasedDate.Hidden = True
                        'end 2021 Jan 7
                    End If
                    DspPEP.Text = .Tax_Reg_Number
                    DspNPWP.Text = .Tax_Number
                    DspSourceofWealth.Text = .Source_of_Wealth
                    DspOccupation.Text = .Occupation
                    DspCatatan.Text = .Comments
                    DspTempatBekerja.Text = .Employer_Name
                End With

                Dsp_GridEmail_Director.IsViewMode = True
                Dsp_GridSanction_Director.IsViewMode = True
                Dsp_GridPEP_Director.IsViewMode = True

                Dsp_GridEmail_Director.LoadData(DirectorClass.ListDirectorEmail)
                Dsp_GridSanction_Director.LoadData(DirectorClass.ListDirectorSanction)
                Dsp_GridPEP_Director.LoadData(DirectorClass.ListDirectorPEP)

                BindDetailPhone(StorePhoneDirector, ListDirectorPhoneDetail)
                BindDetailPhone(StorePhoneDirectorEmployer, ListDirectorEmployerPhoneDetail)
                BindDetailAddress(StoreAddressDirector, ListDirectorAddressDetail)
                BindDetailAddress(StoreAddressDirectorEmployer, ListDirectorEmployerAddressDetail)
                BindDetailIdentification(StoreIdentificationDirector, ListDirectorIdentificationDetail)

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordIdentificationDirector(id As String)
        Try
            ObjDetailIdentificationDirector = ListDirectorIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
            If Not ObjDetailIdentificationDirector Is Nothing Then

                PanelDetailDirectorIdentification.Show()
                FormPanelDirectorIdentification.Hide()
                WindowDetailDirectorIdentification.Title = "Detail Identitas"
                WindowDetailDirectorIdentification.Show()
                With ObjDetailIdentificationDirector
                    DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                    DspNumberDirector.Text = .Number
                    DspIssueDateDirector.Text = If(.Issue_Date Is Nothing, "", .Issue_Date.Value.ToString("dd-MMM-yy"))
                    DspExpiryDateDirector.Text = If(.Expiry_Date Is Nothing, "", .Expiry_Date.Value.ToString("dd-MMM-yy"))
                    DspIssuedByDirector.Text = .Issued_By
                    DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                    DspIdentificationCommentDirector.Text = .Identification_Comment
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordIdentification(id As String)
        Try
            ObjDetailIdentification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
            If Not ObjDetailIdentification Is Nothing Then

                PanelDetailIdentification.Show()
                FormPanelIdentification.Hide()
                WindowDetailIdentification.Show()
                With ObjDetailIdentification
                    DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                    DspNumber.Text = .Number
                    DspIssueDate.Text = If(.Issue_Date Is Nothing, "", .Issue_Date.Value.ToString("dd-MMM-yy"))
                    DspExpiryDate.Text = If(.Expiry_Date Is Nothing, "", .Expiry_Date.Value.ToString("dd-MMM-yy"))
                    DspIssuedBy.Text = .Issued_By
                    DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                    DspIdentificationComment.Text = .Identification_Comment
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordPhoneDirector(id As String)
        Try
            ObjDetailPhone = ListDirectorPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhone Is Nothing Then

                PanelPhoneDirector.Show()
                PanelPhoneDirectorEmployer.Hide()
                WindowDetailDirectorPhone.Title = "Detail Director Phone"
                WindowDetailDirectorPhone.Show()
                FormPanelDirectorPhone.Hide()
                FormPanelEmpDirectorTaskDetail.Hide()

                With ObjDetailPhone
                    DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                    DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                    DspKodeAreaTelpDirector.Text = .tph_country_prefix
                    DspNomorTeleponDirector.Text = .tph_number
                    DspNomorExtensiDirector.Text = .tph_extension
                    DspCatatanDirectorPhoneDirector.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordPhoneDirectorEmployer(id As String)
        Try
            ObjDetailPhone = ListDirectorEmployerPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
            If Not ObjDetailPhone Is Nothing Then

                PanelPhoneDirector.Hide()
                PanelPhoneDirectorEmployer.Show()
                WindowDetailDirectorPhone.Title = "Detail Director Phone Employer"
                WindowDetailDirectorPhone.Show()
                FormPanelDirectorPhone.Hide()
                FormPanelEmpDirectorTaskDetail.Hide()

                With ObjDetailPhone
                    DspKategoriKontakDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                    DspJenisAlatKomunikasiDirectorEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                    DspKodeAreaTelpDirectorEmployer.Text = .tph_country_prefix
                    DspNomorTeleponDirectorEmployer.Text = .tph_number
                    DspNomorExtensiDirectorEmployer.Text = .tph_extension
                    DspCatatanDirectorPhoneDirectorEmployer.Text = .comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressEmployerIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressEmployerIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressEmployerIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddressEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddress(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddressDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddressDirectorEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordAddress(id As String)
        Try
            ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not ObjDetailAddress Is Nothing Then

                PanelDetailAddress.Show()
                PanelDetailAddressEmployer.Hide()
                FormPanelAddressDetail.Hide()
                FormPanelAddressDetailEmployer.Hide()
                WindowDetailAddress.Title = "Detail Address"
                WindowDetailAddress.Show()
                ClearInput()
                With ObjDetailAddress
                    DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                    DspAddress.Text = .Address
                    DspTown.Text = .Town
                    DspCity.Text = .City
                    DspZip.Text = .Zip
                    Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                    DspState.Text = .State
                    DspcommentsAddress.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordAddressEmployer(id As String)
        Try
            ObjDetailAddressEmployer = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not ObjDetailAddressEmployer Is Nothing Then

                PanelDetailAddressEmployer.Show()
                PanelDetailAddress.Hide()
                FormPanelAddressDetail.Hide()
                FormPanelAddressDetailEmployer.Hide()
                WindowDetailAddress.Title = "Detail Address Employer"
                WindowDetailAddress.Show()

                With ObjDetailAddressEmployer
                    DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                    DspAddressEmployer.Text = .Address
                    DspTownEmployer.Text = .Town
                    DspCityEmployer.Text = .City
                    DspZipEmployer.Text = .Zip
                    Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                    DspStateEmployer.Text = .State
                    DspcommentsAddressEmployer.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordAddressDirector(id As String)
        Try
            ObjDetailAddress = ListDirectorAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If ObjDetailAddress IsNot Nothing Then

                PanelAddressDirector.Show()
                PanelAddressDirectorEmployer.Hide()
                FP_Address_Director.Hide()
                FP_Address_Emp_Director.Hide()
                WindowDetailDirectorAddress.Title = "Detail Director Address"
                WindowDetailDirectorAddress.Show()

                With ObjDetailAddress
                    DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                    DspAlamatDirector.Text = .Address
                    DspKecamatanDirector.Text = .Town
                    DspKotaKabupatenDirector.Text = .City
                    DspKodePosDirector.Text = .Zip
                    DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                    DspProvinsiDirector.Text = .State
                    DspCatatanAddressDirector.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordAddressDirectorEmployer(id As String)
        Try
            ObjDetailAddress = ListDirectorEmployerAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
            If Not ObjDetailAddress Is Nothing Then

                PanelAddressDirector.Hide()
                PanelAddressDirectorEmployer.Show()
                FP_Address_Director.Hide()
                FP_Address_Emp_Director.Hide()
                WindowDetailDirectorAddress.Title = "Detail Director Address Employer"
                WindowDetailDirectorAddress.Show()

                With ObjDetailAddress
                    DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                    DspAlamatDirectorEmployer.Text = .Address
                    DspKecamatanDirectorEmployer.Text = .Town
                    DspKotaKabupatenDirectorEmployer.Text = .City
                    DspKodePosDirectorEmployer.Text = .Zip
                    DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                    DspProvinsiDirectorEmployer.Text = .State
                    DspCatatanAddressDirectorEmployer.Text = .Comments
                End With
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridCommandPhoneCorporate(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhone(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridCommandAddressCorporate(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddress(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region





    Private Sub CleanFormIndividuWIC()
        TxtINDV_Alias.Clear()
        TxtINDV_Birth_place.Clear()
        'TxtINDV_Email.Clear()
        TxtINDV_employer_name.Clear()
        TxtINDV_ID_Number.Clear()
        TxtINDV_last_name.Clear()
        TxtINDV_Mothers_name.Clear()
        TxtINDV_Occupation.Clear()
        'TxtINDV_Passport_country.Clear()
        TxtINDV_Passport_number.Clear()
        TxtINDV_SSN.Clear()
        TxtINDV_Title.Clear()
    End Sub

    Private Sub CleanFormCorporateWIC()
        TxtCorp_Business.Clear()
        TxtCorp_Comments.Clear()
        TxtCorp_Commercial_name.Clear()
        'TxtCorp_Email.Clear()
        TxtCorp_Incorporation_number.Clear()
        TxtCorp_incorporation_state.Clear()
        TxtCorp_Name.Clear()
        TxtCorp_tax_number.Clear()
        'TxtCorp_url.Clear()
    End Sub

    Function getDataRowFromDB(strQuery As String) As DataRow
        Try
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Return drResult
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
            Return Nothing
        End Try
    End Function

    Function formatDate(Dates As DateTime, StringformatDate As String) As String
        Dim strDate As String = ""
        If Dates = Nothing Or Dates <> DateTime.MinValue Then
            strDate = Dates.ToString(StringformatDate)
        End If
        Return strDate
    End Function


#Region "Email"

    Public Property IDEmail() As Long
        Get
            Return Session("GoAMLWICAdd.IDEmail")
        End Get
        Set(ByVal value As Long)
            Session("GoAMLWICAdd.IDEmail") = value
        End Set
    End Property

    Public Property obj_Email_Edit() As WICDataBLL.goAML_Ref_WIC_Email
        Get
            Return Session("GoAMLWICAdd.obj_Email_Edit")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Email)
            Session("GoAMLWICAdd.obj_Email_Edit") = value
        End Set
    End Property

    Public Property obj_ListEmail_Edit() As List(Of WICDataBLL.goAML_Ref_WIC_Email)
        Get
            Return Session("GoAMLWICAdd.obj_ListEmail_Edit")
        End Get
        Set(ByVal value As List(Of WICDataBLL.goAML_Ref_WIC_Email))
            Session("GoAMLWICAdd.obj_ListEmail_Edit") = value
        End Set
    End Property

    Protected Sub btn_Email_Add_Click()
        Try
            If TxtWIC_No.Value IsNot Nothing Then
                If TxtWIC_No.Value = "" Then
                    Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
                Else
                    DisplayWIC_Email.Value = TxtWIC_No.Value
                End If
            Else
                Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            End If
            TxtEmail.ReadOnly = False
            btn_Email_Save.Hidden = False
            obj_Email_Edit = Nothing
            Clean_Window_Email()
            Window_Email.Title = "Email - Add"
            Window_Email.Hidden = False

            'Set IDPayment to 0
            IDEmail = 0
            obj_Email_Edit = Nothing
            ClearFormPanel(FormPanelEmail)
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdEmail(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListEmail_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListEmail_Edit.Remove(objToDelete)
                    End If
                    BindDetailEmail()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Email_Edit = obj_ListEmail_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = strID)
                    Load_Window_Email(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Load_Window_Email(strAction As String)
        'Clean window pop up
        Clean_Window_Email()

        If obj_Email_Edit IsNot Nothing Then
            'Populate fields
            'TxtFirst_Name
            'TxtLast_Name
            'TxtComments
            With obj_Email_Edit
                DisplayWIC_Email.Value = .WIC_No
                DisplayWIC_Email.Text = .WIC_No
                TxtEmail.Value = .email_address
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            TxtEmail.ReadOnly = False
            btn_Email_Save.Hidden = False
        Else
            TxtEmail.ReadOnly = True
            btn_Email_Save.Hidden = True
        End If

        'Bind Indikator
        IDEmail = obj_Email_Edit.PK_goAML_Ref_WIC_Email_ID

        'Show window pop up
        Window_Email.Title = "Email - " & strAction
        Window_Email.Hidden = False
    End Sub

    Protected Sub btn_Email_Cancel_Click()
        Try
            'Hide window pop up
            Window_Email.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Email_Save_Click()
        Try
            If TxtEmail.Text.Trim = "" Then
                Throw New Exception("Email is required")
            End If
            If Not IsFieldValid(TxtEmail.Text.Trim, "email_address") Then
                Throw New Exception("Email Address format is not valid")
            End If
            'Action save here
            Dim intPK As Long = -1

            If obj_Email_Edit Is Nothing Then  'Add
                Dim objAdd As New WICDataBLL.goAML_Ref_WIC_Email
                If obj_ListEmail_Edit IsNot Nothing Then
                    If obj_ListEmail_Edit.Count > 0 Then
                        intPK = obj_ListEmail_Edit.Min(Function(x) x.PK_goAML_Ref_WIC_Email_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDEmail = intPK

                With objAdd
                    .PK_goAML_Ref_WIC_Email_ID = intPK
                    .WIC_No = TxtWIC_No.Value
                    .email_address = TxtEmail.Value

                End With
                If obj_ListEmail_Edit Is Nothing Then
                    obj_ListEmail_Edit = New List(Of WICDataBLL.goAML_Ref_WIC_Email)
                End If
                obj_ListEmail_Edit.Add(objAdd)
            Else    'Edit
                If obj_ListEmail_Edit IsNot Nothing Then
                    Dim objEdit = obj_ListEmail_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = obj_Email_Edit.PK_goAML_Ref_WIC_Email_ID)
                    If objEdit IsNot Nothing Then
                        With objEdit
                            IDEmail = .PK_goAML_Ref_WIC_Email_ID
                            .email_address = TxtEmail.Value

                        End With
                    End If
                End If

            End If

            'Bind to GridPanel
            BindDetailEmail()

            'Hide window popup
            Window_Email.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindDetailEmail()
        Try
            Dim listEmail As New DataTable
            'listEmail = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM [dbo].[goAML_Ref_WIC_Social_Media] where WIC_No = '13123123'")
            listEmail = NawaBLL.Common.CopyGenericToDataTable(obj_ListEmail_Edit)

            GridPanelEmail.GetStore.DataSource = listEmail
            GridPanelEmail.GetStore.DataBind()

            GPWIC_Corp_Email.GetStore.DataSource = listEmail
            GPWIC_Corp_Email.GetStore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try


    End Sub

    Protected Sub Clean_Window_Email()
        'Clean fields
        TxtEmail.Value = Nothing

        'Show Buttons
        btn_Email_Save.Hidden = False
    End Sub
#End Region

#Region "Person PEP"

    Public Property IDPersonPEP() As Long
        Get
            Return Session("GoAMLWICAdd.IDPersonPEP")
        End Get
        Set(ByVal value As Long)
            Session("GoAMLWICAdd.IDPersonPEP") = value
        End Set
    End Property

    Public Property obj_PersonPEP_Edit() As WICDataBLL.goAML_Ref_WIC_Person_PEP
        Get
            Return Session("GoAMLWICAdd.obj_PersonPEP_Edit")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Person_PEP)
            Session("GoAMLWICAdd.obj_PersonPEP_Edit") = value
        End Set
    End Property

    Public Property obj_ListPersonPEP_Edit() As List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP)
        Get
            Return Session("GoAMLWICAdd.obj_ListPersonPEP_Edit")
        End Get
        Set(ByVal value As List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP))
            Session("GoAMLWICAdd.obj_ListPersonPEP_Edit") = value
        End Set
    End Property

    Protected Sub btn_Person_PEP_Add_Click()
        Try
            If TxtWIC_No.Value IsNot Nothing Then
                If TxtWIC_No.Value = "" Then
                    Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
                Else
                    DisplayWIC_PersonPEP.Value = TxtWIC_No.Value
                End If
            Else
                Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            End If

            cmb_PersonPepCountryCode.IsReadOnly = False
            TxtFunctionName.ReadOnly = False
            TxtDescription.ReadOnly = False
            TxtPersonPEPValidFrom.ReadOnly = False
            chk_PersonPEPIsApproxFromDate.ReadOnly = False
            TxtPersonPEPValidTo.ReadOnly = False
            chk_PersonPEPIsApproxToDate.ReadOnly = False
            TxtPersonPEPComments.ReadOnly = False
            btn_PersonPEP_Save.Hidden = False
            Clean_Window_PersonPEP()
            Window_PersonPEP.Title = "Person PEP - Add"
            Window_PersonPEP.Hidden = False

            'Set IDPayment to 0
            IDPersonPEP = 0
            obj_PersonPEP_Edit = Nothing
            ClearFormPanel(FormPanelPersonPEP)
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdPersonPEP(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListPersonPEP_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_PEP_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListPersonPEP_Edit.Remove(objToDelete)
                    End If
                    BindDetailPersonPEP()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_PersonPEP_Edit = obj_ListPersonPEP_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_PEP_ID = strID)
                    Load_Window_PersonPEP(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Load_Window_PersonPEP(strAction As String)
        'Clean window pop up
        Clean_Window_PersonPEP()

        If obj_PersonPEP_Edit IsNot Nothing Then
            'Populate fields
            'TxtFirst_Name
            'TxtLast_Name
            'TxtComments
            With obj_PersonPEP_Edit

                DisplayWIC_PersonPEP.Value = .WIC_No
                DisplayWIC_PersonPEP.Text = .WIC_No
                cmb_PersonPepCountryCode.SelectedItemValue = .pep_country
                TxtFunctionName.Value = .function_name
                TxtDescription.Value = .function_description
                TxtPersonPEPValidFrom.SelectedDate = .pep_date_range_valid_from
                chk_PersonPEPIsApproxFromDate.Checked = .pep_date_range_is_approx_from_date
                TxtPersonPEPValidTo.SelectedDate = .pep_date_range_valid_to
                chk_PersonPEPIsApproxToDate.Checked = .pep_date_range_is_approx_to_date
                TxtPersonPEPComments.Value = .comments
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            cmb_PersonPepCountryCode.IsReadOnly = False
            TxtFunctionName.ReadOnly = False
            TxtDescription.ReadOnly = False
            TxtPersonPEPValidFrom.ReadOnly = False
            chk_PersonPEPIsApproxFromDate.ReadOnly = False
            TxtPersonPEPValidTo.ReadOnly = False
            chk_PersonPEPIsApproxToDate.ReadOnly = False
            TxtPersonPEPComments.ReadOnly = False
            btn_PersonPEP_Save.Hidden = False
        Else
            cmb_PersonPepCountryCode.IsReadOnly = True
            TxtFunctionName.ReadOnly = True
            TxtDescription.ReadOnly = True
            TxtPersonPEPValidFrom.ReadOnly = True
            chk_PersonPEPIsApproxFromDate.ReadOnly = True
            TxtPersonPEPValidTo.ReadOnly = True
            chk_PersonPEPIsApproxToDate.ReadOnly = True
            TxtPersonPEPComments.ReadOnly = True
            btn_PersonPEP_Save.Hidden = True
        End If

        'Bind Indikator
        IDPersonPEP = obj_PersonPEP_Edit.PK_goAML_Ref_WIC_PEP_ID

        'Show window pop up
        Window_PersonPEP.Title = "Person PEP - " & strAction
        Window_PersonPEP.Hidden = False
    End Sub

    Protected Sub btn_Person_PEP_Cancel_Click()
        Try
            'Hide window pop up
            Window_PersonPEP.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Person_PEP_Save_Click()
        Try
            If cmb_PersonPepCountryCode.SelectedItemValue Is Nothing Then
                Throw New Exception("Country is required")
            End If
            'Action save here
            Dim intPK As Long = -1

            If obj_PersonPEP_Edit Is Nothing Then  'Add
                Dim objAdd As New WICDataBLL.goAML_Ref_WIC_Person_PEP
                If obj_ListPersonPEP_Edit IsNot Nothing Then
                    If obj_ListPersonPEP_Edit.Count > 0 Then
                        intPK = obj_ListPersonPEP_Edit.Min(Function(x) x.PK_goAML_Ref_WIC_PEP_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDPersonPEP = intPK
                With objAdd
                    .PK_goAML_Ref_WIC_PEP_ID = intPK
                    .WIC_No = TxtWIC_No.Value
                    .pep_country = cmb_PersonPepCountryCode.SelectedItemValue
                    .function_name = TxtFunctionName.Value
                    .function_description = TxtDescription.Value
                    .pep_date_range_valid_from = TxtPersonPEPValidFrom.SelectedDate
                    .pep_date_range_is_approx_from_date = chk_PersonPEPIsApproxFromDate.Checked
                    .pep_date_range_valid_to = TxtPersonPEPValidTo.SelectedDate
                    .pep_date_range_is_approx_to_date = chk_PersonPEPIsApproxToDate.Checked
                    .comments = TxtPersonPEPComments.Value

                End With
                If obj_ListPersonPEP_Edit Is Nothing Then
                    obj_ListPersonPEP_Edit = New List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP)
                End If
                obj_ListPersonPEP_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListPersonPEP_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_PEP_ID = obj_PersonPEP_Edit.PK_goAML_Ref_WIC_PEP_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDPersonPEP = .PK_goAML_Ref_WIC_PEP_ID
                        .pep_country = cmb_PersonPepCountryCode.SelectedItemValue
                        .function_name = TxtFunctionName.Value
                        .function_description = TxtDescription.Value
                        .pep_date_range_valid_from = TxtPersonPEPValidFrom.SelectedDate
                        .pep_date_range_is_approx_from_date = chk_PersonPEPIsApproxFromDate.Checked
                        .pep_date_range_valid_to = TxtPersonPEPValidTo.SelectedDate
                        .pep_date_range_is_approx_to_date = chk_PersonPEPIsApproxToDate.Checked
                        .comments = TxtPersonPEPComments.Value

                    End With
                End If
            End If

            'Bind to GridPanel
            BindDetailPersonPEP()

            'Hide window popup
            Window_PersonPEP.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindDetailPersonPEP()
        Try
            Dim listPersonPEP As New DataTable
            'listPersonPEP = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM [dbo].[goAML_Ref_WIC_Social_Media] where WIC_No = '13123123'")
            listPersonPEP = NawaBLL.Common.CopyGenericToDataTable(obj_ListPersonPEP_Edit)

            GridPanelPersonPEP.GetStore.DataSource = listPersonPEP
            GridPanelPersonPEP.GetStore.DataBind()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub Clean_Window_PersonPEP()
        'Clean fields
        cmb_PersonPepCountryCode.IsReadOnly = False
        TxtFunctionName.ReadOnly = False
        TxtDescription.ReadOnly = False
        TxtPersonPEPValidFrom.ReadOnly = False
        chk_PersonPEPIsApproxFromDate.ReadOnly = False
        TxtPersonPEPValidTo.ReadOnly = False
        chk_PersonPEPIsApproxToDate.ReadOnly = False
        TxtPersonPEPComments.ReadOnly = False
        'Show Buttons
        btn_PersonPEP_Save.Hidden = False
    End Sub
#End Region

#Region "Sanction"

    Public Property IDSanction() As Long
        Get
            Return Session("goAML_WICAdd_V501.IDSanction")
        End Get
        Set(ByVal value As Long)
            Session("goAML_WICAdd_V501.IDSanction") = value
        End Set
    End Property

    Public Property obj_Sanction_Edit() As WICDataBLL.goAML_Ref_WIC_Sanction
        Get
            Return Session("goAML_WICAdd_V501.obj_Sanction_Edit")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Sanction)
            Session("goAML_WICAdd_V501.obj_Sanction_Edit") = value
        End Set
    End Property

    Public Property obj_ListSanction_Edit() As List(Of WICDataBLL.goAML_Ref_WIC_Sanction)
        Get
            Return Session("goAML_WICAdd_V501.obj_ListSanction_Edit")
        End Get
        Set(ByVal value As List(Of WICDataBLL.goAML_Ref_WIC_Sanction))
            Session("goAML_WICAdd_V501.obj_ListSanction_Edit") = value
        End Set
    End Property

    Protected Sub btn_Sanction_Add_Click()
        Try
            If TxtWIC_No.Value IsNot Nothing Then
                If TxtWIC_No.Value = "" Then
                    Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
                Else
                    DisplayWIC_Sanction.Value = TxtWIC_No.Value
                End If
            Else
                Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            End If

            TxtProvider.ReadOnly = False
            TxtSanctionListName.ReadOnly = False
            TxtMatchCriteria.ReadOnly = False
            TxtLinkToSource.ReadOnly = False
            TxtSanctionListAttributes.ReadOnly = False
            dt_SanctionValidFrom.ReadOnly = False
            chk_SanctionIsApproxFromDate.ReadOnly = False
            dt_SanctionValidTo.ReadOnly = False
            chk_SanctionIsApproxToDate.ReadOnly = False
            TxtSanctionComments.ReadOnly = False
            btn_Sanction_Save.Hidden = False
            Clean_Window_Sanction()
            Window_Sanction.Title = "Sanction - Add"
            Window_Sanction.Hidden = False

            'Set IDPayment to 0
            IDSanction = 0
            obj_Sanction_Edit = Nothing
            ClearFormPanel(FormPanelSanction)
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdSanction(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListSanction_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListSanction_Edit.Remove(objToDelete)
                    End If
                    BindDetailSanction()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Sanction_Edit = obj_ListSanction_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = strID)
                    Load_Window_Sanction(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Load_Window_Sanction(strAction As String)
        'Clean window pop up
        Clean_Window_Sanction()

        If obj_Sanction_Edit IsNot Nothing Then
            With obj_Sanction_Edit
                DisplayWIC_Sanction.Text = .WIC_No
                TxtProvider.Value = .Provider
                TxtSanctionListName.Value = .Sanction_List_Name
                TxtMatchCriteria.Value = .Match_Criteria
                TxtLinkToSource.Value = .Link_To_Source
                TxtSanctionListAttributes.Value = .Sanction_List_Attributes
                dt_SanctionValidFrom.SelectedDate = .Sanction_List_Date_Range_Valid_From
                chk_SanctionIsApproxFromDate.Checked = .Sanction_List_Date_Range_Is_Approx_From_Date
                dt_SanctionValidTo.SelectedDate = .Sanction_List_Date_Range_Valid_To
                chk_SanctionIsApproxToDate.Checked = .Sanction_List_Date_Range_Is_Approx_To_Date
                TxtSanctionComments.Value = .Comments
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            TxtProvider.ReadOnly = False
            TxtSanctionListName.ReadOnly = False
            TxtMatchCriteria.ReadOnly = False
            TxtLinkToSource.ReadOnly = False
            TxtSanctionListAttributes.ReadOnly = False
            dt_SanctionValidFrom.ReadOnly = False
            chk_SanctionIsApproxFromDate.ReadOnly = False
            dt_SanctionValidTo.ReadOnly = False
            chk_SanctionIsApproxToDate.ReadOnly = False
            TxtSanctionComments.ReadOnly = False
            btn_Sanction_Save.Hidden = False
        Else
            TxtProvider.ReadOnly = True
            TxtSanctionListName.ReadOnly = True
            TxtMatchCriteria.ReadOnly = True
            TxtLinkToSource.ReadOnly = True
            TxtSanctionListAttributes.ReadOnly = True
            dt_SanctionValidFrom.ReadOnly = True
            chk_SanctionIsApproxFromDate.ReadOnly = True
            dt_SanctionValidTo.ReadOnly = True
            chk_SanctionIsApproxToDate.ReadOnly = True
            TxtSanctionComments.ReadOnly = True
            btn_Sanction_Save.Hidden = True
        End If

        'Bind Indikator
        IDSanction = obj_Sanction_Edit.PK_goAML_Ref_WIC_Sanction_ID

        'Show window pop up
        Window_Sanction.Title = "Sanction - " & strAction
        Window_Sanction.Hidden = False
    End Sub

    Protected Sub btn_Sanction_Cancel_Click()
        Try
            'Hide window pop up
            Window_Sanction.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Sanction_Save_Click()

        Try
            If TxtProvider.Text.Trim = "" Then
                Throw New Exception("Provider is required")
            End If
            If TxtSanctionListName.Text.Trim = "" Then
                Throw New Exception("Sanction List Name is required")
            End If
            'Action save here
            Dim intPK As Long = -1

            If obj_Sanction_Edit Is Nothing Then  'Add
                Dim objAdd As New WICDataBLL.goAML_Ref_WIC_Sanction
                If obj_ListSanction_Edit IsNot Nothing Then
                    If obj_ListSanction_Edit.Count > 0 Then
                        intPK = obj_ListSanction_Edit.Min(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDSanction = intPK
                With objAdd
                    .PK_goAML_Ref_WIC_Sanction_ID = intPK
                    .WIC_No = TxtWIC_No.Value
                    .Provider = TxtProvider.Value
                    .Sanction_List_Name = TxtSanctionListName.Value
                    .Match_Criteria = TxtMatchCriteria.Value
                    .Link_To_Source = TxtLinkToSource.Value
                    .Sanction_List_Attributes = TxtSanctionListAttributes.Value
                    .Sanction_List_Date_Range_Valid_From = dt_SanctionValidFrom.SelectedDate
                    .Sanction_List_Date_Range_Is_Approx_From_Date = chk_SanctionIsApproxFromDate.Checked
                    .Sanction_List_Date_Range_Valid_To = dt_SanctionValidTo.SelectedDate
                    .Sanction_List_Date_Range_Is_Approx_To_Date = chk_SanctionIsApproxToDate.Checked
                    .Comments = TxtSanctionComments.Value
                    .FK_REF_DETAIL_OF = 3 ' 2023-11-13, Nael
                End With
                If obj_ListSanction_Edit Is Nothing Then
                    obj_ListSanction_Edit = New List(Of WICDataBLL.goAML_Ref_WIC_Sanction)
                End If
                obj_ListSanction_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListSanction_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = obj_Sanction_Edit.PK_goAML_Ref_WIC_Sanction_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDSanction = .PK_goAML_Ref_WIC_Sanction_ID
                        .WIC_No = TxtWIC_No.Value
                        .Provider = TxtProvider.Value
                        .Sanction_List_Name = TxtSanctionListName.Value
                        .Match_Criteria = TxtMatchCriteria.Value
                        .Link_To_Source = TxtLinkToSource.Value
                        .Sanction_List_Attributes = TxtSanctionListAttributes.Value
                        .Sanction_List_Date_Range_Valid_From = dt_SanctionValidFrom.SelectedDate
                        .Sanction_List_Date_Range_Is_Approx_From_Date = chk_SanctionIsApproxFromDate.Checked
                        .Sanction_List_Date_Range_Valid_To = dt_SanctionValidTo.SelectedDate
                        .Sanction_List_Date_Range_Is_Approx_To_Date = chk_SanctionIsApproxToDate.Checked
                        .Comments = TxtSanctionComments.Value
                        .FK_REF_DETAIL_OF = 3 ' 2023-11-13, Nael
                    End With
                End If
            End If

            'Bind to GridPanel
            BindDetailSanction()

            'Hide window popup
            Window_Sanction.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindDetailSanction()
        Try
            Dim listSanction As New DataTable
            'listSanction = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM [dbo].[goAML_Ref_WIC_Social_Media] where WIC_No = '13123123'")
            listSanction = NawaBLL.Common.CopyGenericToDataTable(obj_ListSanction_Edit)

            GridPanelSanction.GetStore.DataSource = listSanction
            GridPanelSanction.GetStore.DataBind()

            GPWIC_CORP_Sanction.GetStore.DataSource = listSanction
            GPWIC_CORP_Sanction.GetStore.DataBind()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub Clean_Window_Sanction()
        'Clean fields
        TxtProvider.ReadOnly = False
        TxtSanctionListName.ReadOnly = False
        TxtMatchCriteria.ReadOnly = False
        TxtLinkToSource.ReadOnly = False
        TxtSanctionListAttributes.ReadOnly = False
        dt_SanctionValidFrom.ReadOnly = False
        chk_SanctionIsApproxFromDate.ReadOnly = False
        dt_SanctionValidTo.ReadOnly = False
        chk_SanctionIsApproxToDate.ReadOnly = False
        TxtSanctionComments.ReadOnly = False
        'Show Buttons
        btn_Sanction_Save.Hidden = False
    End Sub
#End Region

#Region "Related Person"

    Public Property IDRelatedPerson() As Long
        Get
            Return Session("goAML_WICAdd_V501.IDRelatedPerson")
        End Get
        Set(ByVal value As Long)
            Session("goAML_WICAdd_V501.IDRelatedPerson") = value
        End Set
    End Property

    Public Property obj_RelatedPerson_Edit() As WICDataBLL.goAML_Ref_WIC_Related_Person
        Get
            Return Session("goAML_WICAdd_V501.obj_RelatedPerson_Edit")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Related_Person)
            Session("goAML_WICAdd_V501.obj_RelatedPerson_Edit") = value
        End Set
    End Property

    Public Property obj_ListRelatedPerson_Edit() As List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)
        Get
            Return Session("goAML_WICAdd_V501.obj_ListRelatedPerson_Edit")
        End Get
        Set(ByVal value As List(Of WICDataBLL.goAML_Ref_WIC_Related_Person))
            Session("goAML_WICAdd_V501.obj_ListRelatedPerson_Edit") = value
        End Set
    End Property

    Protected Sub btn_Related_Person_Add_Click()
        Try
            If TxtWIC_No.Value IsNot Nothing Then
                If TxtWIC_No.Value = "" Then
                    Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
                Else
                    DisplayWIC_RelatedPerson.Value = TxtWIC_No.Value
                End If
            Else
                Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            End If

            GridAddressWork_RelatedPerson.IsViewMode = False
            GridAddress_RelatedPerson.IsViewMode = False
            GridPhone_RelatedPerson.IsViewMode = False
            GridPhoneWork_RelatedPerson.IsViewMode = False
            GridIdentification_RelatedPerson.IsViewMode = False
            GridEmail_RelatedPerson.IsViewMode = False
            GridSanction_RelatedPerson.IsViewMode = False
            GridPEP_RelatedPerson.IsViewMode = False

            GridAddressWork_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Address))
            GridAddress_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Address))
            GridPhone_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Phone))
            GridPhoneWork_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Phone))
            GridIdentification_RelatedPerson.LoadData(New List(Of DataModel.goAML_Person_Identification))
            GridEmail_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Customer_Email))
            GridSanction_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Customer_Sanction))
            GridPEP_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Customer_PEP))

            dd_Person_Person_Relation.IsReadOnly = False
            'dt_Relation_Date_Range_Valid_From.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_From_Date.ReadOnly = False
            'dt_Relation_Date_Range_Valid_To.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_To_Date.ReadOnly = False
            TxtRelatedPersonComments.ReadOnly = False
            TxtRelatedPersonRelationComments.ReadOnly = False
            'TxtRelatedPersonGender.ReadOnly = False
            cmb_rp_gender.IsReadOnly = False
            TxtRelatedPersonTitle.ReadOnly = False
            TxtRelatedPersonFirst_Name.ReadOnly = False
            TxtRelatedPersonMiddle_Name.ReadOnly = False
            TxtRelatedPersonPrefix.ReadOnly = False
            TxtRelatedPersonLast_Name.ReadOnly = False
            dt_RelatedPersonBirth_Date.ReadOnly = False
            TxtRelatedPersonBirth_Place.ReadOnly = False
            dd_RelatedPersonCountry_Of_Birth.IsReadOnly = False
            TxtRelatedPersonMother_Name.ReadOnly = False
            TxtRelatedPersonAlias.ReadOnly = False
            'TxtRelatedPersonFull_Name_Frn.ReadOnly = False
            TxtRelatedPersonSSN.ReadOnly = False
            TxtPassport_Number.ReadOnly = False
            dd_Passport_Country.IsReadOnly = False
            TxtRelatedPersonID_Number.ReadOnly = False
            dd_RelatedPersonNationality1.IsReadOnly = False
            dd_RelatedPersonNationality2.IsReadOnly = False
            dd_RelatedPersonNationality3.IsReadOnly = False
            dd_RelatedPersonResidence.IsReadOnly = False
            'dt_RelatedPersonResidence_Since.ReadOnly = False
            TxtRelatedPersonOccupation.ReadOnly = False
            TxtRelatedPersonEmployerName.ReadOnly = False
            chk_RelatedPersonDeceased.ReadOnly = False
            dt_TxtRelatedPersonDate_Deceased.ReadOnly = False
            TxtRelatedPersonTax_Number.ReadOnly = False
            chk_RelatedPersonIs_PEP.ReadOnly = False
            TxtRelatedPersonSource_Of_Wealth.ReadOnly = False
            chk_RelatedPersonIs_Protected.ReadOnly = False
            btn_RelatedPerson_Save.Hidden = False
            Clean_Window_RelatedPerson()
            Window_RelatedPerson.Title = "Related Person - Add"
            Window_RelatedPerson.Hidden = False

            'Set IDPayment to 0
            IDRelatedPerson = 0
            obj_RelatedPerson_Edit = Nothing
            ClearFormPanel(FormPanelRelatedPerson)
            dd_Person_Person_Relation.SetTextValue("")
            cmb_rp_gender.SetTextValue("")
            dd_RelatedPersonCountry_Of_Birth.SetTextValue("")
            dd_Passport_Country.SetTextValue("")
            dd_RelatedPersonNationality1.SetTextValue("")
            dd_RelatedPersonNationality2.SetTextValue("")
            dd_RelatedPersonNationality3.SetTextValue("")
            dd_RelatedPersonResidence.SetTextValue("")
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdRelatedPerson(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListRelatedPerson_Edit.Remove(objToDelete)
                    End If
                    BindDetailRelatedPerson()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_RelatedPerson_Edit = obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = strID)
                    Load_Window_RelatedPerson(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Load_Window_RelatedPerson(strAction As String)
        'Clean window pop up
        Clean_Window_RelatedPerson()

        If obj_RelatedPerson_Edit IsNot Nothing Then
            With obj_RelatedPerson_Edit
                DisplayWIC_RelatedPerson.Text = .WIC_No
                dd_Person_Person_Relation.SelectedItemValue = .Person_Person_Relation
                'dt_Relation_Date_Range_Valid_From.SelectedDate = .Relation_Date_Range_Valid_From
                'chk_Relation_Date_Range_Is_Approx_From_Date.Checked = .Relation_Date_Range_Is_Approx_From_Date
                'dt_Relation_Date_Range_Valid_To.SelectedDate = .Relation_Date_Range_Valid_To
                'chk_Relation_Date_Range_Is_Approx_To_Date.Checked = .Relation_Date_Range_Is_Approx_To_Date
                TxtRelatedPersonComments.Value = .Comments
                TxtRelatedPersonRelationComments.Value = .relation_comments ' 2023-10-24, Nael
                'TxtRelatedPersonGender.Value = .Gender
                Dim refGender = WICBLL.GetReferenceByCode("goAML_Ref_Jenis_Kelamin", .Gender)
                If refGender IsNot Nothing Then
                    cmb_rp_gender.SetTextWithTextValue(refGender.Kode, refGender.Keterangan)
                End If

                TxtRelatedPersonTitle.Value = .Title
                TxtRelatedPersonFirst_Name.Value = .First_Name
                TxtRelatedPersonMiddle_Name.Value = .Middle_Name
                TxtRelatedPersonPrefix.Value = .Prefix
                TxtRelatedPersonLast_Name.Value = .Last_Name
                dt_RelatedPersonBirth_Date.Value = IIf(.Birth_Date Is Nothing, "", .Birth_Date)
                TxtRelatedPersonBirth_Place.Value = .Birth_Place
                'dd_RelatedPersonCountry_Of_Birth.SelectedItemValue = .Country_Of_Birth
                TxtRelatedPersonMother_Name.Value = .Mother_Name
                TxtRelatedPersonAlias.Value = ._Alias
                'TxtRelatedPersonFull_Name_Frn.Value = .Full_Name_Frn
                TxtRelatedPersonSSN.Value = .SSN
                TxtPassport_Number.Value = .Passport_Number
                dd_Passport_Country.SelectedItemValue = .Passport_Country
                TxtRelatedPersonID_Number.Value = .ID_Number
                dd_RelatedPersonNationality1.SelectedItemValue = .Nationality1
                dd_RelatedPersonNationality2.SelectedItemValue = .Nationality2
                dd_RelatedPersonNationality3.SelectedItemValue = .Nationality3
                dd_RelatedPersonResidence.SelectedItemValue = .Residence
                'dt_RelatedPersonResidence_Since.SelectedDate = .Residence_Since
                TxtRelatedPersonOccupation.Value = .Occupation
                TxtRelatedPersonEmployerName.Value = .EMPLOYER_NAME
                chk_RelatedPersonDeceased.Checked = .Deceased
                If .Deceased Then
                    dt_TxtRelatedPersonDate_Deceased.Hidden = False
                Else
                    dt_TxtRelatedPersonDate_Deceased.Hidden = True
                End If
                dt_TxtRelatedPersonDate_Deceased.Value = IIf(.Date_Deceased Is Nothing, "", .Date_Deceased)
                TxtRelatedPersonTax_Number.Value = .Tax_Number
                chk_RelatedPersonIs_PEP.Checked = .Is_PEP
                TxtRelatedPersonSource_Of_Wealth.Value = .Source_Of_Wealth
                chk_RelatedPersonIs_Protected.Checked = .Is_Protected


                Dim address_list = .ObjList_GoAML_Ref_Address
                Dim address_work_list = .ObjList_GoAML_Ref_Address_Work
                Dim phone_list = .ObjList_GoAML_Ref_Phone
                Dim phone_work_list = .ObjList_GoAML_Ref_Phone_Work
                Dim identification_list = .ObjList_GoAML_Person_Identification
                Dim email_list = .ObjList_GoAML_Ref_Customer_Email
                Dim sanction_list = .ObjList_GoAML_Ref_Customer_Sanction
                Dim pep_list = .ObjList_GoAML_Ref_Customer_PEP

                GridAddress_RelatedPerson.LoadData(address_list)
                GridAddressWork_RelatedPerson.LoadData(address_work_list)
                GridPhone_RelatedPerson.LoadData(phone_list)
                GridPhoneWork_RelatedPerson.LoadData(phone_work_list)
                GridIdentification_RelatedPerson.LoadData(identification_list)

                GridEmail_RelatedPerson.LoadData(email_list)
                GridSanction_RelatedPerson.LoadData(sanction_list)
                GridPEP_RelatedPerson.LoadData(pep_list)
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            dd_Person_Person_Relation.IsReadOnly = False
            'dt_Relation_Date_Range_Valid_From.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_From_Date.ReadOnly = False
            'dt_Relation_Date_Range_Valid_To.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_To_Date.ReadOnly = False
            TxtRelatedPersonComments.ReadOnly = False
            TxtRelatedPersonRelationComments.ReadOnly = False ' 2023-10-24, Nael
            'TxtRelatedPersonGender.ReadOnly = False
            cmb_rp_gender.IsReadOnly = False
            TxtRelatedPersonTitle.ReadOnly = False
            TxtRelatedPersonFirst_Name.ReadOnly = False
            TxtRelatedPersonMiddle_Name.ReadOnly = False
            TxtRelatedPersonPrefix.ReadOnly = False
            TxtRelatedPersonLast_Name.ReadOnly = False
            dt_RelatedPersonBirth_Date.ReadOnly = False
            TxtRelatedPersonBirth_Place.ReadOnly = False
            dd_RelatedPersonCountry_Of_Birth.IsReadOnly = False
            TxtRelatedPersonMother_Name.ReadOnly = False
            TxtRelatedPersonAlias.ReadOnly = False
            'TxtRelatedPersonFull_Name_Frn.ReadOnly = False
            TxtRelatedPersonSSN.ReadOnly = False
            TxtPassport_Number.ReadOnly = False
            dd_Passport_Country.IsReadOnly = False
            TxtRelatedPersonID_Number.ReadOnly = False
            dd_RelatedPersonNationality1.IsReadOnly = False
            dd_RelatedPersonNationality2.IsReadOnly = False
            dd_RelatedPersonNationality3.IsReadOnly = False
            dd_RelatedPersonResidence.IsReadOnly = False
            'dt_RelatedPersonResidence_Since.ReadOnly = False
            TxtRelatedPersonOccupation.ReadOnly = False
            TxtRelatedPersonEmployerName.ReadOnly = False
            chk_RelatedPersonDeceased.ReadOnly = False
            dt_TxtRelatedPersonDate_Deceased.ReadOnly = False
            TxtRelatedPersonTax_Number.ReadOnly = False
            chk_RelatedPersonIs_PEP.ReadOnly = False
            TxtRelatedPersonSource_Of_Wealth.ReadOnly = False
            chk_RelatedPersonIs_Protected.ReadOnly = False
            btn_RelatedPerson_Save.Hidden = False

            GridAddressWork_RelatedPerson.IsViewMode = False
            GridAddress_RelatedPerson.IsViewMode = False
            GridPhone_RelatedPerson.IsViewMode = False
            GridPhoneWork_RelatedPerson.IsViewMode = False
            GridIdentification_RelatedPerson.IsViewMode = False
            GridEmail_RelatedPerson.IsViewMode = False
            GridSanction_RelatedPerson.IsViewMode = False
            GridPEP_RelatedPerson.IsViewMode = False
        Else
            dd_Person_Person_Relation.IsReadOnly = True
            'dt_Relation_Date_Range_Valid_From.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_From_Date.ReadOnly = False
            'dt_Relation_Date_Range_Valid_To.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_To_Date.ReadOnly = False
            TxtRelatedPersonComments.ReadOnly = True
            TxtRelatedPersonRelationComments.ReadOnly = True ' 2023-10-24, Nael
            'TxtRelatedPersonGender.ReadOnly = False
            cmb_rp_gender.IsReadOnly = True
            TxtRelatedPersonTitle.ReadOnly = True
            TxtRelatedPersonFirst_Name.ReadOnly = True
            TxtRelatedPersonMiddle_Name.ReadOnly = True
            TxtRelatedPersonPrefix.ReadOnly = True
            TxtRelatedPersonLast_Name.ReadOnly = True
            dt_RelatedPersonBirth_Date.ReadOnly = True
            TxtRelatedPersonBirth_Place.ReadOnly = True
            dd_RelatedPersonCountry_Of_Birth.IsReadOnly = True
            TxtRelatedPersonMother_Name.ReadOnly = True
            TxtRelatedPersonAlias.ReadOnly = True
            'TxtRelatedPersonFull_Name_Frn.ReadOnly = False
            TxtRelatedPersonSSN.ReadOnly = True
            TxtPassport_Number.ReadOnly = True
            dd_Passport_Country.IsReadOnly = True
            TxtRelatedPersonID_Number.ReadOnly = True
            dd_RelatedPersonNationality1.IsReadOnly = True
            dd_RelatedPersonNationality2.IsReadOnly = True
            dd_RelatedPersonNationality3.IsReadOnly = True
            dd_RelatedPersonResidence.IsReadOnly = True
            'dt_RelatedPersonResidence_Since.ReadOnly = False
            TxtRelatedPersonOccupation.ReadOnly = True
            TxtRelatedPersonEmployerName.ReadOnly = True
            chk_RelatedPersonDeceased.ReadOnly = True
            dt_TxtRelatedPersonDate_Deceased.ReadOnly = True
            TxtRelatedPersonTax_Number.ReadOnly = True
            chk_RelatedPersonIs_PEP.ReadOnly = True
            TxtRelatedPersonSource_Of_Wealth.ReadOnly = True
            chk_RelatedPersonIs_Protected.ReadOnly = True
            btn_RelatedPerson_Save.Hidden = True

            GridAddressWork_RelatedPerson.IsViewMode = True
            GridAddress_RelatedPerson.IsViewMode = True
            GridPhone_RelatedPerson.IsViewMode = True
            GridPhoneWork_RelatedPerson.IsViewMode = True
            GridIdentification_RelatedPerson.IsViewMode = True
            GridEmail_RelatedPerson.IsViewMode = True
            GridSanction_RelatedPerson.IsViewMode = True
            GridPEP_RelatedPerson.IsViewMode = True
        End If

        'Bind Indikator
        IDRelatedPerson = obj_RelatedPerson_Edit.PK_goAML_Ref_WIC_Related_Person_ID

        'Show window pop up
        Window_RelatedPerson.Title = "Related Person - " & strAction
        Window_RelatedPerson.Hidden = False
    End Sub

    Protected Sub btn_Related_Person_Cancel_Click()
        Try
            'Hide window pop up
            Window_RelatedPerson.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Function IsDataCustomer_RelatedPerson_Valid() As Boolean
        If TxtWIC_No.Text.Trim = "" Then
            Throw New Exception("WIC No is required")
        End If

        If dd_Person_Person_Relation.SelectedItemText Is Nothing Then
            Throw New Exception("Person Relation is required")
        End If
        If TxtRelatedPersonLast_Name.Text.Trim = "" Then
            Throw New Exception("Full Name is required")
        End If
        If GridPhone_RelatedPerson.objListgoAML_Ref Is Nothing Then
            Throw New Exception("Phone is required")
        End If
        If GridPhone_RelatedPerson.objListgoAML_Ref.Count = 0 Then
            Throw New Exception("Phone is required")
        End If
        If GridAddress_RelatedPerson.objListgoAML_Ref Is Nothing Then
            Throw New Exception("Address is required")
        End If
        If GridAddress_RelatedPerson.objListgoAML_Ref.Count = 0 Then
            Throw New Exception("Address is required")
        End If

        Return True
    End Function
    Protected Sub btn_Related_Person_Save_Click()
        Try
            If IsDataCustomer_RelatedPerson_Valid() Then
                If obj_RelatedPerson_Edit Is Nothing Then  'Add
                    Save_RelatedPerson()
                Else    'Edit
                    Save_RelatedPerson(False)
                End If

                'Bind to GridPanel
                BindDetailRelatedPerson()

                'Hide window popup
                Window_RelatedPerson.Hidden = True
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub Save_RelatedPerson(Optional isNew As Boolean = True)
        Try
            'Action save here
            'Dim intPK As Long = -1
            If isNew Then
                Dim objAdd As New WICDataBLL.goAML_Ref_WIC_Related_Person
                Dim objRand As New Random
                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                IDRelatedPerson = intpk

                With objAdd
                    .PK_goAML_Ref_WIC_Related_Person_ID = intpk
                    .WIC_No = TxtWIC_No.Text
                    .Person_Person_Relation = dd_Person_Person_Relation.SelectedItemValue
                    '.Relation_Date_Range_Valid_From = dt_Relation_Date_Range_Valid_From.SelectedDate
                    '.Relation_Date_Range_Is_Approx_From_Date = chk_Relation_Date_Range_Is_Approx_From_Date.Checked
                    '.Relation_Date_Range_Valid_To = dt_Relation_Date_Range_Valid_To.SelectedDate
                    '.Relation_Date_Range_Is_Approx_To_Date = chk_Relation_Date_Range_Is_Approx_To_Date.Checked
                    .Comments = TxtRelatedPersonComments.Value
                    .relation_comments = TxtRelatedPersonRelationComments.Value ' 2023-10-24, Nael
                    '.Gender = TxtRelatedPersonGender.Value
                    .Gender = cmb_rp_gender.SelectedItemValue
                    .Title = TxtRelatedPersonTitle.Value
                    '.First_Name = TxtRelatedPersonFirst_Name.Value
                    '.Middle_Name = TxtRelatedPersonMiddle_Name.Value
                    '.Prefix = TxtRelatedPersonPrefix.Value
                    .Last_Name = TxtRelatedPersonLast_Name.Value
                    If dt_RelatedPersonBirth_Date.RawValue IsNot Nothing Then
                        .Birth_Date = dt_RelatedPersonBirth_Date.Value
                    End If
                    .Birth_Place = TxtRelatedPersonBirth_Place.Value
                    '.Country_Of_Birth = dd_RelatedPersonCountry_Of_Birth.SelectedItemValue
                    .Mother_Name = TxtRelatedPersonMother_Name.Value
                    ._Alias = TxtRelatedPersonAlias.Value
                    '.Full_Name_Frn = TxtRelatedPersonFull_Name_Frn.Value
                    .SSN = TxtRelatedPersonSSN.Value
                    .Passport_Number = TxtPassport_Number.Value
                    .Passport_Country = dd_Passport_Country.SelectedItemValue
                    .ID_Number = TxtRelatedPersonID_Number.Value
                    .Nationality1 = dd_RelatedPersonNationality1.SelectedItemValue
                    .Nationality2 = dd_RelatedPersonNationality2.SelectedItemValue
                    .Nationality3 = dd_RelatedPersonNationality3.SelectedItemValue
                    .Residence = dd_RelatedPersonResidence.SelectedItemValue
                    '.Residence_Since = dt_RelatedPersonResidence_Since.SelectedDate
                    .Occupation = TxtRelatedPersonOccupation.Value
                    .EMPLOYER_NAME = TxtRelatedPersonEmployerName.Value
                    .Deceased = chk_RelatedPersonDeceased.Checked
                    If dt_TxtRelatedPersonDate_Deceased.RawValue IsNot Nothing Then
                        .Date_Deceased = dt_TxtRelatedPersonDate_Deceased.Value
                    End If
                    .Tax_Number = TxtRelatedPersonTax_Number.Value
                    .Is_PEP = chk_RelatedPersonIs_PEP.Checked
                    .Source_Of_Wealth = TxtRelatedPersonSource_Of_Wealth.Value
                    .Is_Protected = chk_RelatedPersonIs_Protected.Checked

                    ' 2023-10-12, Nael: Set FK_Person_Type untuk Identification jadi 31
                    Dim relPersonIdentification = GridIdentification_RelatedPerson.objListgoAML_Ref.Select(Function(x)
                                                                                                               x.FK_Person_Type = 31
                                                                                                               Return x
                                                                                                           End Function).ToList()
                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, relPersonIdentification)

                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridSanction_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridPEP_RelatedPerson.objListgoAML_Ref)
                End With
                If obj_ListRelatedPerson_Edit Is Nothing Then
                    obj_ListRelatedPerson_Edit = New List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)
                End If
                obj_ListRelatedPerson_Edit.Add(objAdd)
            Else
                Dim objEdit = obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = obj_RelatedPerson_Edit.PK_goAML_Ref_WIC_Related_Person_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDRelatedPerson = .PK_goAML_Ref_WIC_Related_Person_ID
                        .WIC_No = TxtWIC_No.Text
                        .Person_Person_Relation = dd_Person_Person_Relation.SelectedItemValue
                        '.Relation_Date_Range_Valid_From = dt_Relation_Date_Range_Valid_From.SelectedDate
                        '.Relation_Date_Range_Is_Approx_From_Date = chk_Relation_Date_Range_Is_Approx_From_Date.Checked
                        '.Relation_Date_Range_Valid_To = dt_Relation_Date_Range_Valid_To.SelectedDate
                        '.Relation_Date_Range_Is_Approx_To_Date = chk_Relation_Date_Range_Is_Approx_To_Date.Checked
                        .Comments = TxtRelatedPersonComments.Value
                        .relation_comments = TxtRelatedPersonRelationComments.Value ' 2023-10-24, Nael
                        '.Gender = TxtRelatedPersonGender.Value
                        .Gender = cmb_rp_gender.SelectedItemValue
                        .Title = TxtRelatedPersonTitle.Value
                        '.First_Name = TxtRelatedPersonFirst_Name.Value
                        '.Middle_Name = TxtRelatedPersonMiddle_Name.Value
                        '.Prefix = TxtRelatedPersonPrefix.Value
                        .Last_Name = TxtRelatedPersonLast_Name.Value
                        If dt_RelatedPersonBirth_Date.RawValue IsNot Nothing Then
                            .Birth_Date = dt_RelatedPersonBirth_Date.Value
                        End If
                        .Birth_Place = TxtRelatedPersonBirth_Place.Value
                        '.Country_Of_Birth = dd_RelatedPersonCountry_Of_Birth.SelectedItemValue
                        .Mother_Name = TxtRelatedPersonMother_Name.Value
                        ._Alias = TxtRelatedPersonAlias.Value
                        '.Full_Name_Frn = TxtRelatedPersonFull_Name_Frn.Value
                        .SSN = TxtRelatedPersonSSN.Value
                        .Passport_Number = TxtPassport_Number.Value
                        .Passport_Country = dd_Passport_Country.SelectedItemValue
                        .ID_Number = TxtRelatedPersonID_Number.Value
                        .Nationality1 = dd_RelatedPersonNationality1.SelectedItemValue
                        .Nationality2 = dd_RelatedPersonNationality2.SelectedItemValue
                        .Nationality3 = dd_RelatedPersonNationality3.SelectedItemValue
                        .Residence = dd_RelatedPersonResidence.SelectedItemValue
                        '.Residence_Since = dt_RelatedPersonResidence_Since.SelectedDate
                        .Occupation = TxtRelatedPersonOccupation.Value
                        .EMPLOYER_NAME = TxtRelatedPersonEmployerName.Value
                        .Deceased = chk_RelatedPersonDeceased.Checked
                        If dt_TxtRelatedPersonDate_Deceased.RawValue IsNot Nothing Then
                            .Date_Deceased = dt_TxtRelatedPersonDate_Deceased.Value
                        End If
                        .Tax_Number = TxtRelatedPersonTax_Number.Value
                        .Is_PEP = chk_RelatedPersonIs_PEP.Checked
                        .Source_Of_Wealth = TxtRelatedPersonSource_Of_Wealth.Value
                        .Is_Protected = chk_RelatedPersonIs_Protected.Checked

                        ' 2023-10-12, Nael: Set FK_Person_Type untuk Identification jadi 31
                        Dim relPersonIdentification = GridIdentification_RelatedPerson.objListgoAML_Ref.Select(Function(x)
                                                                                                                   x.FK_Person_Type = 31
                                                                                                                   Return x
                                                                                                               End Function).ToList()
                        .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, relPersonIdentification)
                        .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                        .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                        .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                        .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                        .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                        .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridSanction_RelatedPerson.objListgoAML_Ref)
                        .ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridPEP_RelatedPerson.objListgoAML_Ref)
                    End With
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindDetailRelatedPerson()
        Try
            Dim listRelatedPerson As New DataTable
            'listRelatedPerson = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM [dbo].[goAML_Ref_WIC_Social_Media] where WIC_No = '13123123'")
            listRelatedPerson = NawaBLL.Common.CopyGenericToDataTable(obj_ListRelatedPerson_Edit)

            GridPanelRelatedPerson.GetStore.DataSource = listRelatedPerson
            GridPanelRelatedPerson.GetStore.DataBind()

            GPWIC_CORP_RelatedPerson.GetStore.DataSource = listRelatedPerson
            GPWIC_CORP_RelatedPerson.GetStore.DataBind()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub Clean_Window_RelatedPerson()
        'Clean fields
        dd_Person_Person_Relation.IsReadOnly = Nothing
        'dt_Relation_Date_Range_Valid_From.ReadOnly = Nothing
        'chk_Relation_Date_Range_Is_Approx_From_Date.ReadOnly = Nothing
        'dt_Relation_Date_Range_Valid_To.ReadOnly = Nothing
        'chk_Relation_Date_Range_Is_Approx_To_Date.ReadOnly = Nothing
        TxtRelatedPersonComments.ReadOnly = Nothing
        TxtRelatedPersonRelationComments.ReadOnly = Nothing
        'TxtRelatedPersonGender.ReadOnly = Nothing
        cmb_rp_gender.IsReadOnly = Nothing
        TxtRelatedPersonTitle.ReadOnly = Nothing
        TxtRelatedPersonFirst_Name.ReadOnly = Nothing
        TxtRelatedPersonMiddle_Name.ReadOnly = Nothing
        TxtRelatedPersonPrefix.ReadOnly = Nothing
        TxtRelatedPersonLast_Name.ReadOnly = Nothing
        dt_RelatedPersonBirth_Date.ReadOnly = Nothing
        TxtRelatedPersonBirth_Place.ReadOnly = Nothing
        dd_RelatedPersonCountry_Of_Birth.IsReadOnly = Nothing
        TxtRelatedPersonMother_Name.ReadOnly = Nothing
        TxtRelatedPersonAlias.ReadOnly = Nothing
        'TxtRelatedPersonFull_Name_Frn.ReadOnly = Nothing
        TxtRelatedPersonSSN.ReadOnly = Nothing
        TxtPassport_Number.ReadOnly = Nothing
        dd_Passport_Country.IsReadOnly = Nothing
        TxtRelatedPersonID_Number.ReadOnly = Nothing
        dd_RelatedPersonNationality1.IsReadOnly = Nothing
        dd_RelatedPersonNationality2.IsReadOnly = Nothing
        dd_RelatedPersonNationality3.IsReadOnly = Nothing
        dd_RelatedPersonResidence.IsReadOnly = Nothing
        'dt_RelatedPersonResidence_Since.ReadOnly = Nothing
        TxtRelatedPersonOccupation.ReadOnly = Nothing
        TxtRelatedPersonEmployerName.ReadOnly = Nothing
        chk_RelatedPersonDeceased.ReadOnly = Nothing
        dt_TxtRelatedPersonDate_Deceased.ReadOnly = Nothing
        TxtRelatedPersonTax_Number.ReadOnly = Nothing
        chk_RelatedPersonIs_PEP.ReadOnly = Nothing
        TxtRelatedPersonSource_Of_Wealth.ReadOnly = Nothing
        chk_RelatedPersonIs_Protected.ReadOnly = Nothing
        'Show Buttons
        btn_RelatedPerson_Save.Hidden = False
    End Sub
#End Region

#Region "goAML 5.0.1, add by Septian, 2023-02-23"
    Public Property objTempWIC_EntityUrl_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL
        Get
            Return Session("WIC_ADD_v501.objTempWIC_EntityUrl_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
            Session("WIC_ADD_v501.objTempWIC_EntityUrl_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityUrl() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
        Get
            Return Session("WIC_ADD_v501.objListgoAML_Ref_EntityUrl")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL))
            Session("WIC_ADD_v501.objListgoAML_Ref_EntityUrl") = value
        End Set
    End Property
    Public Property objListgoAML_vw_EntityUrl() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
        Get
            Return Session("WIC_ADD_v501.objListgoAML_vw_EntityUrl")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL))
            Session("WIC_ADD_v501.objListgoAML_vw_EntityUrl") = value
        End Set
    End Property

    Public Property objTempWIC_EntityIdentification_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification
        Get
            Return Session("WIC_ADD_v501.objTempWIC_EntityIdentification_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
            Session("WIC_ADD_v501.objTempWIC_EntityIdentification_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityIdentification() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        Get
            Return Session("WIC_ADD_v501.objListgoAML_Ref_EntityIdentification")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification))
            Session("WIC_ADD_v501.objListgoAML_Ref_EntityIdentification") = value
        End Set
    End Property
    Public Property objListgoAML_vw_EntityIdentification() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        Get
            Return Session("WIC_ADD_v501.objListgoAML_vw_EntityIdentification")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification))
            Session("WIC_ADD_v501.objListgoAML_vw_EntityIdentification") = value
        End Set
    End Property

    Public Property objTempWIC_RelatedEntities_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity
        Get
            Return Session("WIC_ADD_v501.objTempWIC_RelatedEntity_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
            Session("WIC_ADD_v501.objTempWIC_RelatedEntity_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_RelatedEntity() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        Get
            Return Session("WIC_ADD_v501.objListgoAML_Ref_RelatedEntity")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity))
            Session("WIC_ADD_v501.objListgoAML_Ref_RelatedEntity") = value
        End Set
    End Property
    Public Property objListgoAML_vw_RelatedEntity() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        Get
            Return Session("WIC_ADD_v501.objListgoAML_vw_RelatedEntity")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity))
            Session("WIC_ADD_v501.objListgoAML_vw_RelatedEntity") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_WIC_URL() As WICDataBLL.goAML_Ref_WIC_URL
        Get
            Return Session("WIC_ADD_v501.objTemp_goAML_Ref_WIC_URL")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_URL)
            Session("WIC_ADD_v501.objTemp_goAML_Ref_WIC_URL") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_WIC_Entity_Identification() As WICDataBLL.goAML_Ref_WIC_Entity_Identification
        Get
            Return Session("WIC_ADD_v501.objTemp_goAML_Ref_WIC_Entity_Identification")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Entity_Identification)
            Session("WIC_ADD_v501.objTemp_goAML_Ref_WIC_Entity_Identification") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_WIC_Related_Entity() As WICDataBLL.goAML_Ref_WIC_Related_Entity
        Get
            Return Session("WIC_ADD_v501.objTemp_goAML_Ref_WIC_Related_Entity")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Related_Entity)
            Session("WIC_ADD_v501.objTemp_goAML_Ref_WIC_Related_Entity") = value
        End Set
    End Property

    Protected Sub btnAdd_WIC_Email_DirectClick()
        Try
            If TxtWIC_No.Value IsNot Nothing Then
                If TxtWIC_No.Value = "" Then
                    Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
                Else
                    DisplayWIC_Email.Value = TxtWIC_No.Value
                End If
            Else
                Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            End If
            TxtEmail.ReadOnly = False
            btn_Email_Save.Hidden = False
            obj_Email_Edit = Nothing
            Clean_Window_Email()
            Window_Email.Title = "Email - Add"
            Window_Email.Hidden = False

            'Set IDPayment to 0
            IDEmail = 0
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_WIC_Sanction_DirectClick()
        Try
            If TxtWIC_No.Value IsNot Nothing Then
                If TxtWIC_No.Value = "" Then
                    Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
                Else
                    DisplayWIC_Sanction.Value = TxtWIC_No.Value
                End If
            Else
                Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            End If

            TxtProvider.ReadOnly = False
            TxtSanctionListName.ReadOnly = False
            TxtMatchCriteria.ReadOnly = False
            TxtLinkToSource.ReadOnly = False
            TxtSanctionListAttributes.ReadOnly = False
            dt_SanctionValidFrom.ReadOnly = False
            chk_SanctionIsApproxFromDate.ReadOnly = False
            dt_SanctionValidTo.ReadOnly = False
            chk_SanctionIsApproxToDate.ReadOnly = False
            TxtSanctionComments.ReadOnly = False
            btn_Sanction_Save.Hidden = False
            Clean_Window_Sanction()
            Window_Sanction.Title = "Sanction - Add"
            Window_Sanction.Hidden = False

            'Set IDPayment to 0
            IDSanction = 0
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveWIC_Url_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataWIC_URL_Valid() Then
                If objTempWIC_EntityUrl_Edit Is Nothing Then
                    Save_Url()
                Else
                    Save_Url(False)
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveWIC_EntityIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataWIC_EntityIdentification_Valid() Then
                If objTempWIC_EntityIdentification_Edit Is Nothing Then
                    Save_EntityIdentification()
                Else
                    Save_EntityIdentification(False)
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveWIC_RelatedEntities_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataWIC_RelatedEntity_Valid() Then
                If objTempWIC_RelatedEntities_Edit Is Nothing Then
                    Save_RelatedEntities()
                Else
                    Save_RelatedEntities(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_WIC_Url_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_URL.Hidden = False
            FormPanel_URL.Hidden = False
            btnSaveWIC_URL.Hidden = False

            txt_url.ReadOnly = False

            WindowDetail_URL.Title = "WIC Url Add"

            objTempWIC_EntityUrl_Edit = Nothing
            ClearinputWIC_Url()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_WIC_EntityIdentification_DirectClick(sender As Object, e As DirectEventArgs)
        Try

            WindowDetail_EntityIdentification.Hidden = False
            FormPanel_EntityIdentification.Hidden = False
            btnSaveWIC_EntityIdentification.Hidden = False

            cmb_ef_type.IsReadOnly = False
            txt_ef_number.ReadOnly = False
            df_issue_date.ReadOnly = False
            df_expiry_date.ReadOnly = False
            txt_ef_issued_by.ReadOnly = False
            cmb_ef_issue_country.IsReadOnly = False
            txt_ef_comments.ReadOnly = False

            WindowDetail_EntityIdentification.Title = "Entity Identification Add"

            objTempWIC_EntityIdentification_Edit = Nothing
            'FormPanel_EntityIdentification.Reset()
            ClearinputWIC_EntityIdentification()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_WIC_RelatedEntities_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            If TxtWIC_No.Value IsNot Nothing Then
                If TxtWIC_No.Value = "" Then
                    Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
                Else
                    DisplayWIC_Sanction.Value = TxtWIC_No.Value
                End If
            Else
                Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            End If

            GridAddress_RelatedEntity.IsViewMode = False
            GridPhone_RelatedEntity.IsViewMode = False
            GridEntityIdentification_RelatedEntity.IsViewMode = False
            GridEmail_RelatedEntity.IsViewMode = False
            GridURL_RelatedEntity.IsViewMode = False
            GridSanction_RelatedEntity.IsViewMode = False

            GridAddress_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Address))
            GridPhone_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Phone))
            GridEntityIdentification_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            GridEmail_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_Email))
            GridSanction_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_Sanction))
            GridURL_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_URL))

            WindowDetail_RelatedEntities.Hidden = False
            FormPanel_RelatedEntities.Hidden = False
            btnSaveWIC_RelatedEntities.Hidden = False

            cmb_re_entity_relation.IsReadOnly = False
            'df_re_relation_date_range_valid_from.ReadOnly = False
            'cbx_re_relation_date_range_is_approx_from_date.ReadOnly = False
            'df_re_relation_date_range_valid_to.ReadOnly = False
            'cbx_re_relation_date_range_is_approx_to_date.ReadOnly = False
            'nfd_re_share_percentage.ReadOnly = False
            txt_re_comments.ReadOnly = False
            txt_re_relation_comments.ReadOnly = False
            txt_re_name.ReadOnly = False
            txt_re_commercial_name.ReadOnly = False
            cmb_re_incorporation_legal_form.IsReadOnly = False
            txt_re_incorporation_number.ReadOnly = False
            txt_re_business.ReadOnly = False
            cmb_re_entity_status.IsReadOnly = False
            df_re_entity_status_date.ReadOnly = False
            txt_re_incorporation_state.ReadOnly = False
            cmb_re_incorporation_country_code.IsReadOnly = False
            df_re_incorporation_date.ReadOnly = False
            cbx_re_business_closed.ReadOnly = False
            df_re_date_business_closed.ReadOnly = False
            txt_re_tax_number.ReadOnly = False
            txt_re_tax_reg_number.ReadOnly = False

            WindowDetail_RelatedEntities.Title = "Related Entities Add"
            objTempWIC_RelatedEntities_Edit = Nothing
            'FormPanel_RelatedEntities.Reset(True)
            ClearinputWIC_RelatedEntities()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_WIC_RelatedPerson_DirectClick()
        Try
            If TxtWIC_No.Value IsNot Nothing Then
                If TxtWIC_No.Value = "" Then
                    Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
                Else
                    DisplayWIC_RelatedPerson.Value = TxtWIC_No.Value
                End If
            Else
                Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            End If
            dd_Person_Person_Relation.IsReadOnly = False
            'dt_Relation_Date_Range_Valid_From.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_From_Date.ReadOnly = False
            'dt_Relation_Date_Range_Valid_To.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_To_Date.ReadOnly = False
            TxtRelatedPersonComments.ReadOnly = False
            TxtRelatedPersonRelationComments.ReadOnly = False
            'TxtRelatedPersonGender.ReadOnly = False
            cmb_rp_gender.IsReadOnly = False
            TxtRelatedPersonTitle.ReadOnly = False
            TxtRelatedPersonFirst_Name.ReadOnly = False
            TxtRelatedPersonMiddle_Name.ReadOnly = False
            TxtRelatedPersonPrefix.ReadOnly = False
            TxtRelatedPersonLast_Name.ReadOnly = False
            dt_RelatedPersonBirth_Date.ReadOnly = False
            TxtRelatedPersonBirth_Place.ReadOnly = False
            dd_RelatedPersonCountry_Of_Birth.IsReadOnly = False
            TxtRelatedPersonMother_Name.ReadOnly = False
            TxtRelatedPersonAlias.ReadOnly = False
            'TxtRelatedPersonFull_Name_Frn.ReadOnly = False
            TxtRelatedPersonSSN.ReadOnly = False
            TxtPassport_Number.ReadOnly = False
            dd_Passport_Country.IsReadOnly = False
            TxtRelatedPersonID_Number.ReadOnly = False
            dd_RelatedPersonNationality1.IsReadOnly = False
            dd_RelatedPersonNationality2.IsReadOnly = False
            dd_RelatedPersonNationality3.IsReadOnly = False
            dd_RelatedPersonResidence.IsReadOnly = False
            'dt_RelatedPersonResidence_Since.ReadOnly = False
            TxtRelatedPersonOccupation.ReadOnly = False
            TxtRelatedPersonEmployerName.ReadOnly = False
            chk_RelatedPersonDeceased.ReadOnly = False
            dt_TxtRelatedPersonDate_Deceased.ReadOnly = False
            TxtRelatedPersonTax_Number.ReadOnly = False
            chk_RelatedPersonIs_PEP.ReadOnly = False
            TxtRelatedPersonSource_Of_Wealth.ReadOnly = False
            chk_RelatedPersonIs_Protected.ReadOnly = False
            btn_RelatedPerson_Save.Hidden = False
            Clean_Window_RelatedPerson()
            Window_RelatedPerson.Title = "Related Person - Add"
            Window_RelatedPerson.Hidden = False

            'Set IDPayment to 0
            IDRelatedPerson = 0
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub Save_Url(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempWIC_EntityUrl_Edit
                    .WIC_NO = TxtWIC_No.Value
                    .URL = txt_url.Value
                End With

                With objTemp_goAML_Ref_WIC_URL
                    .WIC_NO = TxtWIC_No.Value
                    .URL = txt_url.Value
                    .FK_FOR_TABLE_ID = objWICAddData.PK_Customer_ID
                    .FK_REF_DETAIL_OF = 3
                End With

                objTempWIC_EntityUrl_Edit = Nothing
                objTemp_goAML_Ref_WIC_URL = Nothing
            Else
                Dim objNewVWgoAML_Url As New GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL
                Dim objNewgoAML_Url As New GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_EntityUrl.Find(Function(x) x.PK_goAML_Ref_WIC_URL_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_Url
                    .PK_goAML_Ref_WIC_URL_ID = intpk
                    .WIC_NO = TxtWIC_No.Value
                    .URL = txt_url.Value
                End With

                With objNewgoAML_Url
                    .PK_goAML_Ref_WIC_URL_ID = intpk
                    .WIC_NO = TxtWIC_No.Value
                    .URL = txt_url.Value
                End With

                objListgoAML_vw_EntityUrl.Add(objNewVWgoAML_Url)
                objListgoAML_Ref_EntityUrl.Add(objNewgoAML_Url)
            End If

            'uncomment this store
            Store_WIC_Entity_URL.DataSource = objListgoAML_vw_EntityUrl.ToList()
            Store_WIC_Entity_URL.DataBind()

            WindowDetail_URL.Hidden = True
            'ClearinputWICPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
    Sub Save_EntityIdentification(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempWIC_EntityIdentification_Edit
                    .WIC_NO = TxtWIC_No.Value
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If df_issue_date.RawValue IsNot Nothing Then
                        .ISSUE_DATE = df_issue_date.Value
                    Else
                        .ISSUE_DATE = Nothing
                    End If
                    If df_expiry_date.RawValue IsNot Nothing Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    Else
                        .EXPIRY_DATE = Nothing
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                With objTemp_goAML_Ref_WIC_Entity_Identification
                    .WIC_NO = TxtWIC_No.Value
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If df_issue_date.RawValue IsNot Nothing Then
                        .ISSUE_DATE = df_issue_date.Value
                    Else
                        .ISSUE_DATE = Nothing
                    End If
                    If df_expiry_date.RawValue IsNot Nothing Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    Else
                        .EXPIRY_DATE = Nothing
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                objTempWIC_EntityIdentification_Edit = Nothing
                objTemp_goAML_Ref_WIC_Entity_Identification = Nothing
            Else
                Dim objNewVWgoAML_EntityIdentification As New GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification
                Dim objNewgoAML_EntityIdentification As New GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_EntityIdentification
                    .PK_goAML_Ref_WIC_Entity_Identifications_ID = intpk
                    .WIC_NO = TxtWIC_No.Value
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If df_issue_date.RawValue IsNot Nothing Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If df_expiry_date.RawValue IsNot Nothing Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                With objNewgoAML_EntityIdentification
                    .PK_goAML_Ref_WIC_Entity_Identifications_ID = intpk
                    .WIC_NO = TxtWIC_No.Value
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If df_issue_date.RawValue IsNot Nothing Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If df_expiry_date.RawValue IsNot Nothing Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                objListgoAML_vw_EntityIdentification.Add(objNewVWgoAML_EntityIdentification)
                objListgoAML_Ref_EntityIdentification.Add(objNewgoAML_EntityIdentification)
            End If

            Store_WIC_Entity_Identification.DataSource = objListgoAML_vw_EntityIdentification.ToList()
            Store_WIC_Entity_Identification.DataBind()

            WindowDetail_EntityIdentification.Hidden = True
            'ClearinputWICPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
    Sub Save_RelatedEntities(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempWIC_RelatedEntities_Edit
                    .WIC_NO = TxtWIC_No.Value
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    .RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    .RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    .RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    .SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    .ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    .ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    Else
                        .INCORPORATION_DATE = Nothing
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    Else
                        .DATE_BUSINESS_CLOSED = Nothing
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objTempWIC_RelatedEntities_Edit.PK_goAML_Ref_WIC_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objTempWIC_RelatedEntities_Edit.PK_goAML_Ref_WIC_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTempWIC_RelatedEntities_Edit.PK_goAML_Ref_WIC_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objTempWIC_RelatedEntities_Edit.PK_goAML_Ref_WIC_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objTempWIC_RelatedEntities_Edit.PK_goAML_Ref_WIC_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objTempWIC_RelatedEntities_Edit.PK_goAML_Ref_WIC_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                With objTemp_goAML_Ref_WIC_Related_Entity
                    .WIC_NO = TxtWIC_No.Value
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    .RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    .RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    .RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    .SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    .ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    .ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    Else
                        .INCORPORATION_DATE = Nothing
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    Else
                        .DATE_BUSINESS_CLOSED = Nothing
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objTemp_goAML_Ref_WIC_Related_Entity.PK_goAML_Ref_WIC_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objTemp_goAML_Ref_WIC_Related_Entity.PK_goAML_Ref_WIC_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTemp_goAML_Ref_WIC_Related_Entity.PK_goAML_Ref_WIC_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objTemp_goAML_Ref_WIC_Related_Entity.PK_goAML_Ref_WIC_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objTemp_goAML_Ref_WIC_Related_Entity.PK_goAML_Ref_WIC_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objTemp_goAML_Ref_WIC_Related_Entity.PK_goAML_Ref_WIC_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                objTempWIC_RelatedEntities_Edit = Nothing
                objTemp_goAML_Ref_WIC_Related_Entity = Nothing
            Else
                Dim objNewVWgoAML_RelatedEntities As New GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity
                Dim objNewgoAML_RelatedEntities As New GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_RelatedEntity.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_RelatedEntities
                    .PK_goAML_Ref_WIC_Related_Entities_ID = intpk
                    .WIC_NO = TxtWIC_No.Value
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    .RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    .RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    .RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    .SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    .ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    .ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                With objNewgoAML_RelatedEntities
                    .PK_goAML_Ref_WIC_Related_Entities_ID = intpk
                    .WIC_NO = TxtWIC_No.Value
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    .RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    .RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    .RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    .SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    .ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    .ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                objListgoAML_vw_RelatedEntity.Add(objNewVWgoAML_RelatedEntities)
                objListgoAML_Ref_RelatedEntity = objListgoAML_vw_RelatedEntity
            End If

            Store_Related_Entities.DataSource = objListgoAML_vw_RelatedEntity.ToList()
            Store_Related_Entities.DataBind()

            WindowDetail_RelatedEntities.Hidden = True
            'ClearinputWICPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub ClearinputWIC_Url()
        txt_url.Text = ""


    End Sub

    Sub ClearinputWIC_RelatedEntities()
        cmb_re_entity_relation.SetTextValue("")
        df_re_relation_date_range_valid_from.Text = ""
        cbx_re_relation_date_range_is_approx_from_date.Checked = False
        df_re_relation_date_range_valid_to.Text = ""
        cbx_re_relation_date_range_is_approx_to_date.Checked = False
        nfd_re_share_percentage.Text = ""
        txt_re_comments.Text = ""
        txt_re_relation_comments.Text = ""
        txt_re_name.Text = ""
        txt_re_commercial_name.Text = ""
        cmb_re_incorporation_legal_form.SetTextValue("")
        txt_re_incorporation_number.Text = ""
        txt_re_business.Text = ""
        cmb_re_entity_status.SetTextValue("")
        df_re_entity_status_date.Text = ""
        txt_re_incorporation_state.Text = ""
        cmb_re_incorporation_country_code.SetTextValue("")
        df_re_incorporation_date.Text = ""
        cbx_re_business_closed.Checked = False
        df_re_date_business_closed.Text = ""
        txt_re_tax_number.Text = ""
        txt_re_tax_reg_number.Text = ""

    End Sub

    Sub ClearinputWIC_EntityIdentification()
        cmb_ef_type.SetTextValue("")
        txt_ef_number.Text = ""
        df_issue_date.Text = ""
        df_expiry_date.Text = ""
        txt_ef_issued_by.Text = ""
        cmb_ef_issue_country.SetTextValue("")
        txt_ef_comments.Text = ""

    End Sub

    Protected Sub btnBackWIC_Url_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_URL.Hidden = True
            FormPanel_URL.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackWIC_EntityIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_EntityIdentification.Hidden = True
            FormPanel_EntityIdentification.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackWIC_RelatedEntities_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_RelatedEntities.Hidden = True
            FormPanel_RelatedEntities.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_Email(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListEmail_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListEmail_Edit.Remove(objToDelete)
                    End If
                    BindDetailEmail()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Email_Edit = obj_ListEmail_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = strID)
                    Load_Window_Email(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_EntityIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_EntityIdentification(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_EntityIdentification(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_EntityIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_URL(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_URL(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_URL(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_URL(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_RelatedEntities(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_RelatedEntities(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_RelatedEntities(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_RelatedEntities(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_Sanction(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListSanction_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListSanction_Edit.Remove(objToDelete)
                    End If
                    BindDetailSanction()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Sanction_Edit = obj_ListSanction_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = strID)
                    Load_Window_Sanction(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_RelatedPerson(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListRelatedPerson_Edit.Remove(objToDelete)
                    End If
                    BindDetailRelatedPerson()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_RelatedPerson_Edit = obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = strID)
                    Load_Window_RelatedPerson(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadData_EntityIdentification(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_WIC_Entity_Identification = objListgoAML_Ref_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id)
            objTempWIC_EntityIdentification_Edit = objListgoAML_vw_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id)

            If Not objTempWIC_EntityIdentification_Edit Is Nothing Then
                FormPanel_EntityIdentification.Hidden = False
                WindowDetail_EntityIdentification.Hidden = False
                btnSaveWIC_EntityIdentification.Hidden = Not isEdit

                cmb_ef_type.IsReadOnly = Not isEdit
                txt_ef_number.ReadOnly = Not isEdit
                df_issue_date.ReadOnly = Not isEdit
                df_expiry_date.ReadOnly = Not isEdit
                txt_ef_issued_by.ReadOnly = Not isEdit
                cmb_ef_issue_country.IsReadOnly = Not isEdit
                txt_ef_comments.ReadOnly = Not isEdit


                With objTempWIC_EntityIdentification_Edit
                    Dim refType = goAML_WICBLL.GetReferenceByCode("goAML_Ref_Entity_Identifier_Type", .TYPE)
                    Dim refIssue = goAML_WICBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .ISSUE_COUNTRY)

                    cmb_ef_type.SetTextWithTextValue(refType.Kode, refType.Keterangan)
                    txt_ef_number.Value = .NUMBER
                    df_issue_date.Value = IIf(.ISSUE_DATE Is Nothing, "", .ISSUE_DATE)
                    df_expiry_date.Value = IIf(.EXPIRY_DATE Is Nothing, "", .EXPIRY_DATE)
                    txt_ef_issued_by.Value = .ISSUED_BY
                    cmb_ef_issue_country.SetTextWithTextValue(refIssue.Kode, refIssue.Keterangan)
                    txt_ef_comments.Value = .COMMENTS

                End With

                WindowDetail_EntityIdentification.Title = "Entity Identification " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_URL(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_WIC_URL = objListgoAML_Ref_EntityUrl.Find(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id)
            objTempWIC_EntityUrl_Edit = objListgoAML_vw_EntityUrl.Find(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id)

            If Not objTempWIC_EntityUrl_Edit Is Nothing Then
                FormPanel_URL.Hidden = False
                WindowDetail_URL.Hidden = False
                btnSaveWIC_URL.Hidden = Not isEdit

                txt_url.ReadOnly = Not isEdit

                With objTempWIC_EntityUrl_Edit
                    txt_url.Value = .URL
                End With

                WindowDetail_URL.Title = "Entity URL " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_RelatedEntities(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_WIC_Related_Entity = objListgoAML_Ref_RelatedEntity.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id)
            objTempWIC_RelatedEntities_Edit = objListgoAML_vw_RelatedEntity.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id)

            If Not objTempWIC_RelatedEntities_Edit Is Nothing Then
                GridPhone_RelatedEntity.IsViewMode = Not isEdit
                GridAddress_RelatedEntity.IsViewMode = Not isEdit
                GridEmail_RelatedEntity.IsViewMode = Not isEdit
                GridEntityIdentification_RelatedEntity.IsViewMode = Not isEdit
                GridURL_RelatedEntity.IsViewMode = Not isEdit
                GridSanction_RelatedEntity.IsViewMode = Not isEdit

                FormPanel_RelatedEntities.Hidden = False
                WindowDetail_RelatedEntities.Hidden = False
                btnSaveWIC_RelatedEntities.Hidden = Not isEdit

                cmb_re_entity_relation.IsReadOnly = Not isEdit
                df_re_relation_date_range_valid_from.ReadOnly = Not isEdit
                cbx_re_relation_date_range_is_approx_from_date.ReadOnly = Not isEdit
                df_re_relation_date_range_valid_to.ReadOnly = Not isEdit
                cbx_re_relation_date_range_is_approx_to_date.ReadOnly = Not isEdit
                nfd_re_share_percentage.ReadOnly = Not isEdit
                txt_re_comments.ReadOnly = Not isEdit
                txt_re_relation_comments.ReadOnly = Not isEdit
                txt_re_name.ReadOnly = Not isEdit
                txt_re_commercial_name.ReadOnly = Not isEdit
                cmb_re_incorporation_legal_form.IsReadOnly = Not isEdit
                txt_re_incorporation_number.ReadOnly = Not isEdit
                txt_re_business.ReadOnly = Not isEdit
                cmb_re_entity_status.IsReadOnly = Not isEdit
                df_re_entity_status_date.ReadOnly = Not isEdit
                txt_re_incorporation_state.ReadOnly = Not isEdit
                cmb_re_incorporation_country_code.IsReadOnly = Not isEdit
                df_re_incorporation_date.ReadOnly = Not isEdit
                cbx_re_business_closed.ReadOnly = Not isEdit
                df_re_date_business_closed.ReadOnly = Not isEdit
                txt_re_tax_number.ReadOnly = Not isEdit
                txt_re_tax_reg_number.ReadOnly = Not isEdit


                With objTempWIC_RelatedEntities_Edit
                    Dim refRelation = goAML_WICBLL.GetReferenceByCode("goAML_Ref_Entity_Entity_Relationship", .ENTITY_ENTITY_RELATION)
                    Dim refLegalForm = goAML_WICBLL.GetReferenceByCode("goAML_Ref_Bentuk_Badan_Usaha", .INCORPORATION_LEGAL_FORM)
                    Dim refEntityStatus = goAML_WICBLL.GetReferenceByCode("goAML_Ref_Entity_Status", .ENTITY_STATUS)
                    Dim refCountry = goAML_WICBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .INCORPORATION_COUNTRY_CODE)

                    cmb_re_entity_relation.SetTextWithTextValue(refRelation.Kode, refRelation.Keterangan)
                    'df_re_relation_date_range_valid_from.Text = .RELATION_DATE_RANGE_VALID_FROM
                    'cbx_re_relation_date_range_is_approx_from_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_re_relation_date_range_valid_to.Text = .RELATION_DATE_RANGE_VALID_TO
                    'cbx_re_relation_date_range_is_approx_to_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    'nfd_re_share_percentage.Value = .SHARE_PERCENTAGE
                    txt_re_comments.Value = .COMMENTS
                    txt_re_relation_comments.Value = .RELATION_COMMENTS ' 2023-10-24, Nael
                    txt_re_name.Value = .NAME
                    txt_re_commercial_name.Value = .COMMERCIAL_NAME
                    If refLegalForm IsNot Nothing Then
                        cmb_re_incorporation_legal_form.SetTextWithTextValue(refLegalForm.Kode, refLegalForm.Keterangan)
                    Else
                        cmb_re_incorporation_legal_form.SetTextValue("")
                    End If
                    txt_re_incorporation_number.Value = .INCORPORATION_NUMBER
                    txt_re_business.Value = .BUSINESS
                    'cmb_re_entity_status.SetTextWithTextValue(refEntityStatus.Kode, refEntityStatus.Keterangan)
                    'df_re_entity_status_date.Text = .ENTITY_STATUS
                    txt_re_incorporation_state.Value = .INCORPORATION_STATE
                    If refCountry IsNot Nothing Then
                        cmb_re_incorporation_country_code.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                    Else
                        cmb_re_incorporation_country_code.SetTextValue("")
                    End If
                    df_re_incorporation_date.Value = IIf(.INCORPORATION_DATE Is Nothing, "", .INCORPORATION_DATE)
                    cbx_re_business_closed.Checked = .BUSINESS_CLOSED
                    If .BUSINESS_CLOSED Then
                        df_re_date_business_closed.Hidden = False
                        df_re_date_business_closed.Value = IIf(.DATE_BUSINESS_CLOSED Is Nothing, "", .DATE_BUSINESS_CLOSED)
                    End If
                    txt_re_tax_number.Value = .TAX_NUMBER
                    txt_re_tax_reg_number.Value = .TAX_REG_NUMBER

                    Dim phone_list = If(.ObjList_GoAML_Ref_Customer_Phone IsNot Nothing, .ObjList_GoAML_Ref_Customer_Phone,
                        goAML_Customer_Service.GetPhoneByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim address_list = If(.ObjList_GoAML_Ref_Customer_Address IsNot Nothing, .ObjList_GoAML_Ref_Customer_Address,
                        goAML_Customer_Service.GetAddressByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim email_list = If(.ObjList_GoAML_Ref_Customer_Email IsNot Nothing, .ObjList_GoAML_Ref_Customer_Email,
                        goAML_Customer_Service.GetEmailByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim entity_identification_list = If(.ObjList_GoAML_Ref_Customer_Entity_Identification IsNot Nothing, .ObjList_GoAML_Ref_Customer_Entity_Identification,
                        goAML_Customer_Service.GetEntityIdentificationByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim url_list = If(.ObjList_GoAML_Ref_Customer_URL IsNot Nothing, .ObjList_GoAML_Ref_Customer_URL,
                        goAML_Customer_Service.GetUrlByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim sanction_list = If(.ObjList_GoAML_Ref_Customer_Sanction IsNot Nothing, .ObjList_GoAML_Ref_Customer_Sanction,
                        goAML_Customer_Service.GetSanctionByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))

                    GridPhone_RelatedEntity.LoadData(phone_list)
                    GridAddress_RelatedEntity.LoadData(address_list)
                    GridEmail_RelatedEntity.LoadData(email_list)
                    GridEntityIdentification_RelatedEntity.LoadData(entity_identification_list)
                    GridURL_RelatedEntity.LoadData(url_list)
                    GridSanction_RelatedEntity.LoadData(sanction_list)

                End With

                WindowDetail_RelatedEntities.Title = "Related Entities " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub DeleteRecord_URL(id As Long)
        objListgoAML_vw_EntityUrl.Remove(objListgoAML_vw_EntityUrl.SingleOrDefault(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id))
        objListgoAML_Ref_EntityUrl.Remove(objListgoAML_Ref_EntityUrl.SingleOrDefault(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id))

        Store_WIC_Entity_URL.DataSource = objListgoAML_vw_EntityUrl.ToList()
        Store_WIC_Entity_URL.DataBind()

    End Sub

    Sub DeleteRecord_EntityIdentification(id As Long)
        objListgoAML_vw_EntityIdentification.Remove(objListgoAML_vw_EntityIdentification.SingleOrDefault(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id))
        objListgoAML_Ref_EntityIdentification.Remove(objListgoAML_Ref_EntityIdentification.SingleOrDefault(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id))

        Store_WIC_Entity_Identification.DataSource = objListgoAML_vw_EntityIdentification.ToList()
        Store_WIC_Entity_Identification.DataBind()

    End Sub

    Sub DeleteRecord_RelatedEntities(id As Long)
        objListgoAML_vw_RelatedEntity.Remove(objListgoAML_vw_RelatedEntity.SingleOrDefault(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id))
        objListgoAML_Ref_RelatedEntity.Remove(objListgoAML_Ref_RelatedEntity.SingleOrDefault(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id))

        Store_Related_Entities.DataSource = objListgoAML_vw_RelatedEntity.ToList()
        Store_Related_Entities.DataBind()

    End Sub

    Function IsDataWIC_Email_Valid() As Boolean
        If TxtEmail.Text.Trim = "" Then
            Throw New Exception("Email Address is required")
        End If

        Return True
    End Function

    Function IsDataWIC_PersonPEP_Valid() As Boolean
        If cmb_PersonPepCountryCode.SelectedItemText Is Nothing Then
            Throw New Exception("Country is required")
        End If

        If TxtFunctionName.Text.Trim = "" Then
            Throw New Exception("Function Name is required")
        End If
        'If TxtDescription.Text.Trim = "" Then
        '    Throw New Exception("Function Description is required")
        'End If
        'If TxtPersonPEPValidFrom.RawValue.Trim = "" Then
        '    Throw New Exception("Valid From is required")
        'End If

        'If TxtPersonPEPValidTo.RawValue.Trim = "" Then
        '    Throw New Exception("Valid To is required")
        'End If

        'If TxtPersonPEPComments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If

        Return True
    End Function

    Function IsDataWIC_EntityIdentification_Valid() As Boolean
        If TxtWIC_No.Text.Trim = "" Then
            Throw New Exception("TxtWIC_No is required")
        End If

        If cmb_ef_type.SelectedItemText Is Nothing Then
            Throw New Exception("Type is required")
        End If
        If txt_ef_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'If df_issue_date.RawValue Is Nothing Then
        '    Throw New Exception("Issue Date is required")
        'End If
        'If df_expiry_date.RawValue Is Nothing Then
        '    Throw New Exception("Expiry Date is required")
        'End If
        'If txt_ef_issued_by.Text.Trim = "" Then
        '    Throw New Exception("Issued By is required")
        'End If
        If cmb_ef_issue_country.SelectedItemText Is Nothing Then
            Throw New Exception("Issue Country is required")
        End If
        'If txt_ef_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If


        Return True
    End Function

    Function IsDataWIC_Sanction_Valid() As Boolean
        If TxtProvider.Text.Trim = "" Then
            Throw New Exception("Provider is required")
        End If
        If TxtSanctionListName.Text.Trim = "" Then
            Throw New Exception("Sanction List Name is required")
        End If
        'If TxtMatchCriteria.Text.Trim = "" Then
        '    Throw New Exception("Match Criteria is required")
        'End If
        'If TxtLinkToSource.Text.Trim = "" Then
        '    Throw New Exception("Link To Source is required")
        'End If
        'If TxtSanctionListAttributes.Text.Trim = "" Then
        '    Throw New Exception("Sanction List Attributes is required")
        'End If
        'If dt_SanctionValidFrom.RawValue.Trim = "" Then
        '    Throw New Exception("Valid From is required")
        'End If
        'If dt_SanctionValidTo.RawValue.Trim = "" Then
        '    Throw New Exception("Valid To is required")
        'End If
        'If TxtSanctionComments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If

        Return True
    End Function

    Function IsDataWIC_RelatedPerson_Valid() As Boolean

        If dd_Person_Person_Relation.SelectedItemText Is Nothing Then
            Throw New Exception("Person Relation is required")
        End If
        'If TxtRelatedPersonComments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If
        'If TxtRelatedPersonGender.SelectedItemText Is Nothing Then
        '    Throw New Exception("Gender is required")
        'End If
        'If TxtRelatedPersonTitle.Text.Trim = "" Then
        '    Throw New Exception("Title is required")
        'End If
        'If TxtRelatedPersonFirst_Name.Text.Trim = "" Then
        '    Throw New Exception("First Name is required")
        'End If
        'If TxtRelatedPersonMiddle_Name.Text.Trim = "" Then
        '    Throw New Exception("Middle Name is required")
        'End If
        'If TxtRelatedPersonPrefix.Text.Trim = "" Then
        '    Throw New Exception("Prefix is required")
        'End If
        If TxtRelatedPersonLast_Name.Text.Trim = "" Then
            Throw New Exception("Full Name is required")
        End If
        'If dt_RelatedPersonBirth_Date.RawValue Is Nothing Then
        '    Throw New Exception("Birth Date is required")
        'End If
        'If TxtRelatedPersonBirth_Place.Text.Trim = "" Then
        '    Throw New Exception("Birth Place is required")
        'End If
        'If Not IsFieldValid(TxtRelatedPersonBirth_Place.Text.Trim, "birth_place") Then
        '    Throw New Exception("Birth Place format is not valid")
        'End If
        'If dd_RelatedPersonCountry_Of_Birth.SelectedItemText Is Nothing Then
        '    Throw New Exception("Country Of Birth is required")
        'End If
        'If TxtRelatedPersonMother_Name.Text.Trim = "" Then
        '    Throw New Exception("Mother's Name is required")
        'End If
        'If Not IsFieldValid(TxtRelatedPersonMother_Name.Text.Trim, "mothers_name") Then
        '    Throw New Exception("Mothers Name format is not valid")
        'End If
        'If TxtRelatedPersonAlias.Text.Trim = "" Then
        '    Throw New Exception("Alias is required")
        'End If
        'If Not IsFieldValid(TxtRelatedPersonAlias.Text.Trim, "alias") Then
        '    Throw New Exception("Alias format is not valid")
        'End If
        'If txt_rp_full_name_frn.Text.Trim = "" Then
        '    Throw New Exception("Full Name is required")
        'End If
        'If TxtRelatedPersonSSN.Text.Trim = "" Then
        '    Throw New Exception("SSN is required")
        'End If
        'If Not IsFieldValid(txt_rp_ssn.Text.Trim, "ssn") Then
        '    Throw New Exception("SSN format is not valid")
        'End If
        'If TxtPassport_Number.Text.Trim = "" Then
        '    Throw New Exception("Passport Number is required")
        'End If
        'If dd_Passport_Country.SelectedItemText Is Nothing Then
        '    Throw New Exception("Paassport Country is required")
        'End If
        'If TxtRelatedPersonID_Number.Text.Trim = "" Then
        '    Throw New Exception("ID Number is required")
        'End If
        'If dd_RelatedPersonNationality1.SelectedItemText Is Nothing Then
        '    Throw New Exception("Nationality 1 is required")
        'End If
        'If dd_RelatedPersonResidence.SelectedItemText Is Nothing Then
        '    Throw New Exception("Residence is required")
        'End If
        'If df_rp_residence_since.RawValue.Trim = "" Then
        '    Throw New Exception("Residence Since is required")
        'End If
        'If TxtRelatedPersonOccupation.Text.Trim = "" Then
        '    Throw New Exception("Occupation is required")
        'End If
        'If dt_TxtRelatedPersonDate_Deceased.RawValue.Trim = "" Then
        '    Throw New Exception("Date Deceased is required")
        'End If
        'If TxtRelatedPersonTax_Number.Text.Trim = "" Then
        '    Throw New Exception("Tax Number is required")
        'End If
        'If TxtRelatedPersonSource_Of_Wealth.Text.Trim = "" Then
        '    Throw New Exception("Source of Wealth is required")
        'End If

        Return True
    End Function

    Function IsDataWIC_URL_Valid() As Boolean

        If txt_url.Text.Trim = "" Then
            Throw New Exception("Url is required")
        End If

        Return True
    End Function

    Function IsDataWIC_RelatedEntity_Valid() As Boolean

        If cmb_re_entity_relation.SelectedItemText Is Nothing Then
            Throw New Exception("Entity Relation is required")
        End If
        'If df_re_relation_date_range_valid_from.RawValue.Trim = "" Then
        '    Throw New Exception("Valid From is required")
        'End If
        'If df_re_relation_date_range_valid_to.RawValue.Trim = "" Then
        '    Throw New Exception("Valid To is required")
        'End If
        'If nfd_re_share_percentage.Text.Trim = "" Then
        '    Throw New Exception("Share Percentage is required")
        'End If
        'If txt_re_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If
        If txt_re_name.Text.Trim = "" Then
            Throw New Exception("Corporate Name is required")
        End If
        'If txt_re_commercial_name.Text.Trim = "" Then
        '    Throw New Exception("Commercial Name is required")
        'End If
        'If cmb_re_incorporation_legal_form.SelectedItemText Is Nothing Then
        '    Throw New Exception("Incorporation Legal is required")
        'End If
        'If txt_re_incorporation_number.Text.Trim = "" Then
        '    Throw New Exception("Incorporation Number is required")
        'End If
        'If txt_re_business.Text.Trim = "" Then
        '    Throw New Exception("Business is required")
        'End If
        'If cmb_re_entity_status.SelectedItemText Is Nothing Then
        '    Throw New Exception("Entity Status is required")
        'End If
        'If df_re_entity_status_date.RawValue.Trim = "" Then
        '    Throw New Exception("Entity Status Date is required")
        'End If
        'If txt_re_incorporation_state.Text.Trim = "" Then
        '    Throw New Exception("Incorporation State is required")
        'End If
        'If Not IsFieldValid(txt_re_incorporation_state.Text.Trim, "incorporation_state") Then
        '    Throw New Exception("Incorporation State format is not valid")
        'End If
        'If cmb_re_incorporation_country_code.SelectedItemText Is Nothing Then
        '    Throw New Exception("Incorporation Country Code is required")
        'End If
        'If df_re_incorporation_date.RawValue.Trim = "" Then
        '    Throw New Exception("Incorporation Date is required")
        'End If
        'If df_re_date_business_closed.RawValue.Trim = "" Then
        '    Throw New Exception("Business Closed is required")
        'End If
        'If txt_re_tax_number.Text.Trim = "" Then
        '    Throw New Exception("Tax Number is required")
        'End If
        'If txt_re_tax_reg_number.Text.Trim = "" Then
        '    Throw New Exception("Tax Reg Number is required")
        'End If

        Return True
    End Function

    Private Function IsFieldValid(text As String, fieldName As String) As Boolean
        Try
            Dim regex As New Regex("^[0-9 ]+$")

            Select Case fieldName
                Case "incorporation_state"
                    regex = New Regex("[ a-zA-Z]*")
                Case "tax_number"
                    regex = New Regex("[0-9]{15}")
                Case "last_name"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "birth_place"
                    regex = New Regex("[ a-zA-Z]*")
                Case "mothers_name"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "alias"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "ssn"
                    regex = New Regex("[0-9]{22}")
                Case "city"
                    regex = New Regex("[ a-zA-Z]*")
                Case "state"
                    regex = New Regex("[ a-zA-Z]*")
                Case "email_address"
                    regex = New Regex("[_a-zA-Z0-9-+]+(\.[_a-zA-Z0-9-\+]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})")
                Case "ipv4_address_type"
                    regex = New Regex("((1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])")
                Case "ipv6_address_type"
                    regex = New Regex("([A-Fa-f0-9]{1,4}:){7}[A-Fa-f0-9]{1,4}")
                Case Else
                    Return True
            End Select

            Return regex.IsMatch(text)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function GetObjData() As WICDataBLL
        Dim wicData = New WICDataBLL
        wicData.ObjWIC2 = GetObjWIC()
        wicData.ObjGrid = GetObjGrid()

        Return wicData
    End Function

    Private Function GetObjWIC() As WICDataBLL.goAML_Ref_WIC2
        Dim objWIC = New WICDataBLL.goAML_Ref_WIC2
        objWIC.WIC_No = TxtWIC_No.Value
        objWIC.IS_REAL_WIC = Cbx_IsRealWIC.Checked
        objWIC.IS_PROTECTED = Cbx_IsProtected.Checked

        Return objWIC
    End Function
    Private Function GetObjGrid() As WICDataBLL.goAML_Grid_WIC
        Dim gridWIC = New WICDataBLL.goAML_Grid_WIC
        gridWIC.ObjList_GoAML_Email = obj_ListEmail_Edit
        gridWIC.ObjList_GoAML_PersonPEP = obj_ListPersonPEP_Edit
        gridWIC.ObjList_GoAML_Sanction = obj_ListSanction_Edit
        gridWIC.ObjList_GoAML_RelatedPerson = obj_ListRelatedPerson_Edit
        gridWIC.ObjList_GoAML_URL = objListgoAML_Ref_EntityUrl
        gridWIC.ObjList_GoAML_EntityIdentification = objListgoAML_Ref_EntityIdentification
        gridWIC.ObjList_GoAML_RelatedEntity = objListgoAML_Ref_RelatedEntity

        Return gridWIC
    End Function

    Private Sub ClearFormPanel(formPanel As Ext.Net.FormPanel)

        If formPanel IsNot Nothing Then
            For Each item As Ext.Net.ComponentBase In formPanel.Items
                'Dim test = item.GetType().Name
                If TypeOf item Is Ext.Net.TextField Then
                    Dim textField As Ext.Net.TextField = CType(item, Ext.Net.TextField)
                    textField.Text = ""
                ElseIf TypeOf item Is Ext.Net.NumberField Then
                    Dim numberField As Ext.Net.NumberField = CType(item, Ext.Net.NumberField)
                    numberField.Value = 0
                ElseIf TypeOf item Is Ext.Net.DateField Then
                    Dim dateField As Ext.Net.DateField = CType(item, Ext.Net.DateField)
                    dateField.Text = ""
                ElseIf TypeOf item Is Ext.Net.Checkbox Then
                    Dim checkBox As Ext.Net.Checkbox = CType(item, Ext.Net.Checkbox)
                    checkBox.Checked = False
                End If
            Next
        End If
    End Sub
#End Region

End Class
