﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="WICApprovalDetail_V501.aspx.vb" Inherits="goAML_WICApprovalDetail_V501" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<%@ Register Src="~/goAML/WIC/Component/GridAddress.ascx" TagPrefix="nds" TagName="GridAddress" %>
<%@ Register Src="~/goAML/WIC/Component/GridAddressWork.ascx" TagPrefix="nds" TagName="GridAddressWork" %>
<%@ Register Src="~/goAML/WIC/Component/GridEmail.ascx" TagPrefix="nds" TagName="GridEmail" %>
<%@ Register Src="~/goAML/WIC/Component/GridIdentification.ascx" TagPrefix="nds" TagName="GridIdentification" %>
<%@ Register Src="~/goAML/WIC/Component/GridPEP.ascx" TagPrefix="nds" TagName="GridPEP" %>
<%@ Register Src="~/goAML/WIC/Component/GridPhone.ascx" TagPrefix="nds" TagName="GridPhone" %>
<%@ Register Src="~/goAML/WIC/Component/GridPhoneWork.ascx" TagPrefix="nds" TagName="GridPhoneWork" %>
<%@ Register Src="~/goAML/WIC/Component/GridSanction.ascx" TagPrefix="nds" TagName="GridSanction" %>
<%@ Register Src="~/goAML/WIC/Component/GridEntityIdentification.ascx" TagPrefix="nds" TagName="GridEntityIdentification" %>
<%@ Register Src="~/goAML/WIC/Component/GridURL.ascx" TagPrefix="nds" TagName="GridURL" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-form-item-label.x-form-item-label-default { width : 175px !important; }
        #ContentPlaceHolder1_FormPanelOld-innerCt {
            display: block !important;
            overflow-x: scroll !important;
        }
         #ContentPlaceHolder1_FormPanelNew-innerCt {
            display: block !important;
            overflow-x: scroll !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <ext:Window ID="WindowDetailDirector" runat="server" Icon="ApplicationViewDetail" Title="Customer Director Detail" Modal="true" Hidden="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="FormPanelDirectorDetail" runat="server" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:Hidden runat="server" ID="Hidden1"></ext:Hidden>
                    <ext:DisplayField ID="DspPeranDirector" runat="server" FieldLabel="Role" AnchorHorizontal="70%" LabelWidth="250"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250" ID="DspGender" runat="server" FieldLabel="Gender" AnchorHorizontal="70%" ></ext:DisplayField>
                    <ext:DisplayField ID="DspGelarDirector" LabelWidth="250" runat="server" FieldLabel="Title" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaLengkap"  LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250"  runat="server" FieldLabel="Birth Date" ID="DspTanggalLahir" AnchorHorizontal="90%"/>
                    <ext:DisplayField ID="DspTempatLahir"  LabelWidth="250" runat="server" FieldLabel="Birth Place" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaIbuKandung" LabelWidth="250" runat="server" FieldLabel="Mother Maiden Name" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaAlias" LabelWidth="250" runat="server" FieldLabel="Alias Name" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNIK" LabelWidth="250" runat="server" FieldLabel="NIK" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNoPassport" LabelWidth="250" runat="server" FieldLabel="No. Passport" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraPenerbitPassport" LabelWidth="250" runat="server" FieldLabel="Passport Issued Country" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNoIdentitasLain" LabelWidth="250" runat="server" FieldLabel="Other ID Number" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250"  ID="DspKewarganegaraan1" runat="server" FieldLabel="Nationality 1" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspKewarganegaraan2" LabelWidth="250" runat="server" FieldLabel="Nationality 2" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspKewarganegaraan3" LabelWidth="250" runat="server" FieldLabel="Nationality 3" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDomisili"  LabelWidth="250" runat="server" FieldLabel="Domicile Country" AnchorHorizontal="70%"></ext:DisplayField>

                    <ext:GridPanel ID="GridPanelDirectorPhoneBefore" runat="server" Title="Phone" AutoScroll="true" Hidden="true">
                        <Store>
                            <ext:Store ID="StoreDirectorPhoneBefore" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model6" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column3" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn5" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorPhoneBefore">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelDirectorPhone" runat="server" Title="Phone" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorPhone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column1" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelDirectorAddressBefore" runat="server" Title="Address" AutoScroll="true" Hidden="true">
                        <Store>
                            <ext:Store ID="StoreDirectorAddressBefore" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model7" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column13" runat="server" DataIndex="Type_Address" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="Addres" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="Cty" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="CountryCode" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn6" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorAddressBefore">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelDirectorAddress" runat="server" Title="Address" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorAddress" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column7" runat="server" DataIndex="Type_Address" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="Addres" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="Cty" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="CountryCode" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                  <%--  <ext:DisplayField ID="DspEmail" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%"></ext:DisplayField>--%>
                    <ext:DisplayField ID="DspOccupation" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspTempatBekerja" LabelWidth="250" runat="server" FieldLabel="Work Place" AnchorHorizontal="90%"></ext:DisplayField>

                    <ext:GridPanel ID="GridPanelDirectorPhoneEmployerBefore" runat="server" Title="Work Place Phone" AutoScroll="true" Hidden="true">
                        <Store>
                            <ext:Store ID="StoreDirectorPhoneEmployerBefore" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model9" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column23" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column25" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column27" runat="server" DataIndex="number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn7" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerPhoneBefore">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelDirectorPhoneEmployer" runat="server" Title="Work Place Phone" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorPhoneEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model8" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column15" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelDirectorAddressEmployerBefore" runat="server" Title="Work Place Address" AutoScroll="true" Hidden="true">
                        <Store>
                            <ext:Store ID="StoreDirectorAddressEmployerBefore" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model10" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column29" runat="server" DataIndex="Type_Address" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column30" runat="server" DataIndex="Addres" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column33" runat="server" DataIndex="Cty" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column34" runat="server" DataIndex="CountryCode" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn9" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerAddressBefore">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelDirectorAddressEmployer" runat="server" Title="Work Place Address" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorAddressEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model17" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column21" runat="server" DataIndex="Type_Address" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="Addres" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="Cty" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="CountryCode" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn4" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelDirectorIdentificationBefore" runat="server" Title="Identification" Hidden="true" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreIdentificationDirectorBefore" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model16" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn17" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column61" runat="server" DataIndex="Tipe" Text="Identity Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column62" runat="server" DataIndex="No" Text="Identity Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column63" runat="server" DataIndex="IssueDate" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column64" runat="server" DataIndex="ExpiryDate" Text="Expired Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column65" runat="server" DataIndex="IssuedCountry" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn13" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorIdentificationBefore">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelDirectordentification" runat="server" Title="Identification" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreIdentificationDirector" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model18" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn16" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column56" runat="server" DataIndex="Tipe" Text="Identity Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column57" runat="server" DataIndex="No" Text="Identity Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column58" runat="server" DataIndex="IssueDate" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column59" runat="server" DataIndex="ExpiryDate" Text="Expired Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column60" runat="server" DataIndex="IssuedCountry" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn12" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DisplayField runat="server" LabelWidth="250" ID="DspDeceased" FieldLabel="Deceased ?"></ext:DisplayField>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Deceased Date " ID="DspDeceasedDate" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tax Number" ID="DspNPWP" AnchorHorizontal="90%" />
                    <ext:DisplayField runat="server" LabelWidth="250" ID="DspPEP" FieldLabel="PEP ?"></ext:DisplayField>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Source of Wealth" ID="DspSourceofWealth" AnchorHorizontal="90%" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Notes" ID="DspCatatan" AnchorHorizontal="90%" />

                    <ext:Panel ID="Panel_Director_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridEmail runat="server" ID="GridEmail_Director" FK_REF_DETAIL_OF="6" UniqueName = "Director"/>
                        </Content>
                    </ext:Panel>

                    <ext:Panel ID="Panel_Director_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridSanction runat="server" ID="GridSanction_Director" UniqueName = "Director" FK_REF_DETAIL_OF="6"/>
                        </Content>
                    </ext:Panel>

                    <ext:Panel ID="Panel_Director_GridPEP" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridPEP runat="server" ID="GridPEP_Director" UniqueName = "Director" FK_REF_DETAIL_OF="6"/>
                        </Content>
                    </ext:Panel>
                </Items>
                <Buttons>
                    <ext:Button ID="Button5" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveCustomerDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorIdentification" runat="server" Icon="ApplicationViewDetail" Title="Identification Detail" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <ext:FormPanel ID="PanelDetailIdentificationDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DsptTypeDirector" runat="server" FieldLabel="Identity Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspNumberDirector" runat="server" FieldLabel="Identity Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssueDateDirector" runat="server" FieldLabel="Issued Date"></ext:DisplayField>
                    <ext:DisplayField ID="DspExpiryDateDirector" runat="server" FieldLabel="Expired date"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedByDirector" runat="server" FieldLabel="Issued By"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedCountryDirector" runat="server" FieldLabel="Issued Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspIdentificationCommentDirector" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button6" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelIdentificationDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailDirectorIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailIdentification" runat="server" Icon="ApplicationViewDetail" Title="Identification Detail" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <%-- 2023-10-14, Nael: Button Align Center --%>
            <ext:FormPanel ID="PanelDetailIdentification" runat="server" AnchorHorizontal="100%" ButtonAlign="Center" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DsptType" runat="server" FieldLabel="Identity Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspNumber" runat="server" FieldLabel="Identity Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssueDate" runat="server" FieldLabel="Issued Date"></ext:DisplayField>
                    <ext:DisplayField ID="DspExpiryDate" runat="server" FieldLabel="Expired date"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedBy" runat="server" FieldLabel="Issued By"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedCountry" runat="server" FieldLabel="Issued Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspIdentificationComment" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button8" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelIdentification_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorPhone" runat="server" Icon="ApplicationViewDetail" Title="Director Phone Detail" Modal="true" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <%-- 2023-10-21, Nael: Button Align Center --%>
            <ext:FormPanel ID="PanelPhoneDirector" runat="server" ButtonAlign="Center" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspKategoriKontakDirector" runat="server" FieldLabel="Contact Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspJenisAlatKomunikasiDirector" runat="server" FieldLabel="Communication Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodeAreaTelpDirector" runat="server" FieldLabel="Prefix"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorTeleponDirector" runat="server" FieldLabel="Phone Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorExtensiDirector" runat="server" FieldLabel="Extention"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanDirectorPhoneDirector" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button3" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelPhoneDirectorEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspKategoriKontakDirectorEmployer" runat="server" FieldLabel="Contact Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspJenisAlatKomunikasiDirectorEmployer" runat="server" FieldLabel="Communication Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodeAreaTelpDirectorEmployer" runat="server" FieldLabel="Prefix"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorTeleponDirectorEmployer" runat="server" FieldLabel="Phone Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorExtensiDirectorEmployer" runat="server" FieldLabel="Extention"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanDirectorPhoneDirectorEmployer" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button4" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorEmployerPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorAddress" runat="server" Icon="ApplicationViewDetail" Title="Director Address Detail" Modal="true" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="PanelAddressDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspTipeAlamatDirector" runat="server" FieldLabel="Address Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspAlamatDirector" runat="server" FieldLabel="Address"></ext:DisplayField>
                    <ext:DisplayField ID="DspKecamatanDirector" runat="server" FieldLabel="Town"></ext:DisplayField>
                    <ext:DisplayField ID="DspKotaKabupatenDirector" runat="server" FieldLabel="City"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodePosDirector" runat="server" FieldLabel="Zip Code"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDirector" runat="server" FieldLabel="Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspProvinsiDirector" runat="server" FieldLabel="Province"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanAddressDirector" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button1" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorDetailAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelAddressDirectorEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspTipeAlamatDirectorEmployer" runat="server" FieldLabel="Address Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspAlamatDirectorEmployer" runat="server" FieldLabel="Address"></ext:DisplayField>
                    <ext:DisplayField ID="DspKecamatanDirectorEmployer" runat="server" FieldLabel="Town"></ext:DisplayField>
                    <ext:DisplayField ID="DspKotaKabupatenDirectorEmployer" runat="server" FieldLabel="City"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodePosDirectorEmployer" runat="server" FieldLabel="Zip Code"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDirectorEmployer" runat="server" FieldLabel="Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspProvinsiDirectorEmployer" runat="server" FieldLabel="Province"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanAddressDirectorEmployer" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button2" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorDetailEmployerAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailPhone" runat="server" Icon="ApplicationViewDetail" Title="Phone Detail" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <%-- 2023-10-14, Nael: ButtonAlign Center --%>
            <ext:FormPanel ID="PanelDetailPhone" runat="server" AnchorHorizontal="100%" ButtonAlign="Center" BodyPadding="10" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="Dsptph_contact_type" runat="server" FieldLabel="Contact Type"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_communication_type" runat="server" FieldLabel="Communication Type"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_country_prefix" runat="server" FieldLabel="Prefix"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_number" runat="server" FieldLabel="Phone Number"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_extension" runat="server" FieldLabel="Extention"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsPhone" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnCancelPhone" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <%-- 2023-10-14, Nael: Button Align Center --%>
            <ext:FormPanel ID="PanelDetailPhoneEmployer" runat="server" ButtonAlign="Center" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="Dsptph_contact_typeEmployer" runat="server" FieldLabel="Contact Type"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_communication_typeEmployer" runat="server" FieldLabel="Communication Type"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_country_prefixEmployer" runat="server" FieldLabel="Prefix"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_numberEmployer" runat="server" FieldLabel="Phone Number"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_extensionEmployer" runat="server" FieldLabel="Extention"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsPhoneEmployer" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button16" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelPhoneEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailPhone}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailAddress" runat="server" Icon="ApplicationViewDetail" Title="Address Detail" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <%-- 2023-10-14, Nael: Button Align Center --%>
            <ext:FormPanel ID="PanelDetailAddress" runat="server" AnchorHorizontal="100%" ButtonAlign="Center" BodyPadding="10" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspAddress_type" runat="server" FieldLabel="Address Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspAddress" runat="server" FieldLabel="Address"></ext:DisplayField>
                    <ext:DisplayField ID="DspTown" runat="server" FieldLabel="Town"></ext:DisplayField>
                    <ext:DisplayField ID="DspCity" runat="server" FieldLabel="City"></ext:DisplayField>
                    <ext:DisplayField ID="DspZip" runat="server" FieldLabel="Zip Code"></ext:DisplayField>
                    <ext:DisplayField ID="Dspcountry_code" runat="server" FieldLabel="Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspState" runat="server" FieldLabel="Province"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsAddress" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnCancelAddress" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <%-- 2023-10-14, Nael: Button Align Center --%>
            <ext:FormPanel ID="PanelDetailAddressEmployer" runat="server" ButtonAlign="Center" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspAddress_typeEmployer" runat="server" FieldLabel="Address Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspAddressEmployer" runat="server" FieldLabel="Address"></ext:DisplayField>
                    <ext:DisplayField ID="DspTownEmployer" runat="server" FieldLabel="Town"></ext:DisplayField>
                    <ext:DisplayField ID="DspCityEmployer" runat="server" FieldLabel="City"></ext:DisplayField>
                    <ext:DisplayField ID="DspZipEmployer" runat="server" FieldLabel="Zip Code"></ext:DisplayField>
                    <ext:DisplayField ID="Dspcountry_codeEmployer" runat="server" FieldLabel="Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspStateEmployer" runat="server" FieldLabel="Province"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsAddressEmployer" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button19" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelAddressEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailAddress}.center()" />
        </Listeners>
    </ext:Window>

    <%-- Add by Septian, goAML v5.0.1, 2023-03-02 --%>
    <ext:Window ID="WindowDetail_Email" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Email Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet6" runat="server" Title="Email" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_Email" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txtWIC_email_address" runat="server" FieldLabel="Email Address" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_Customer_Back_Email" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackWIC_Email_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    
    <ext:Window ID="WindowDetail_Person_PEP" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Person PEP Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet8" runat="server" Title="Person PEP" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_Person_PEP" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_Person_PEP_Country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_PersonPEP_Country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%"  />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_function_name" LabelWidth="250" runat="server" FieldLabel="Function Name" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:TextField ID="txt_function_description" LabelWidth="250" runat="server" FieldLabel="Function Description" AnchorHorizontal="90%"  MaxLength="8000"></ext:TextField>
                            <ext:DateField Hidden="true" ID="df_person_pep_valid_from"  LabelWidth="250" runat="server" FieldLabel="Valid From" AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Checkbox Hidden="true" runat="server" LabelWidth="250" ID="cbx_pep_is_approx_from_date" FieldLabel="Is Approx From Date?"></ext:Checkbox>
                            <ext:DateField Hidden="true" ID="df_pep_valid_to"  LabelWidth="250" runat="server" FieldLabel="Valid To" AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:Checkbox Hidden="true" runat="server" LabelWidth="250" ID="cbx_pep_is_approx_to_date" FieldLabel="Is Approx To Date?"></ext:Checkbox>
                            <ext:TextField ID="txt_pep_comments" LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%"  MaxLength="8000"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_Customer_Back_PEP" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackWIC_PersonPEP_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_Sanction" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Sanction Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet15" runat="server" Title="Sanction" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_Sanction" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_sanction_provider" runat="server" FieldLabel="Provider" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:TextField ID="txt_sanction_sanction_list_name" runat="server" FieldLabel="Sanction List Name" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:TextField ID="txt_sanction_match_criteria" runat="server" FieldLabel="Match Criteria" AnchorHorizontal="90%"  MaxLength="8000"></ext:TextField>
                            <ext:TextField ID="txt_sanction_link_to_source" runat="server" FieldLabel="Link to Source" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:TextField ID="txt_sanction_sanction_list_attributes" runat="server" FieldLabel="Sanction List Attributes" AnchorHorizontal="90%"  MaxLength="8000"></ext:TextField>
                            <ext:DateField Hidden="true" Editable="false"  runat="server" FieldLabel="Valid From" ID="df_sanction_valid_from" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox Hidden="true" runat="server" ID="cbx_sanction_is_approx_from_date" FieldLabel="Is Approx From Date?"></ext:Checkbox>
                            <ext:DateField Hidden="true" Editable="false"  runat="server" FieldLabel="Valid To" ID="df_sanction_valid_to" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox Hidden="true" runat="server" ID="cbx_sanction_is_approx_to_date" FieldLabel="Is Approx To Date?"></ext:Checkbox>
                            <ext:TextField ID="txt_sanction_comments" runat="server" FieldLabel="Comments" AnchorHorizontal="90%"  MaxLength="8000"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button17" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackWIC_Sanction_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_RelatedPerson" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Related Person Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet11" runat="server" Title="Related Person" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_RelatedPerson" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_cmb_rp_person_relation" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_rp_person_relation" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Person_Person_Relationship" StringFilter="Active = 1" EmptyText="Select One" Label="Person Relation" AnchorHorizontal="70%"  />
                                </Content>
                            </ext:Panel>
                            <%--<ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid From" ID="df_relation_date_range_valid_from" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_relation_date_range_is_approx_from_date" FieldLabel="Relation Date Range -  Is Approx From Date?"></ext:Checkbox>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid To" ID="df_relation_date_range_valid_to" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_relation_date_range_is_approx_to_date" FieldLabel="Relation Date Range -  Is Approx To Date?"></ext:Checkbox>--%>
                            <ext:TextField ID="txt_rp_relation_comments" LabelWidth="250" runat="server" FieldLabel="Relation Comments" AnchorHorizontal="90%"  MaxLength="8000"></ext:TextField>
                            <ext:Panel ID="Panel_cmb_rp_gender" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_rp_gender" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Jenis_Kelamin" StringFilter="Active = 1" EmptyText="Select One" Label="Gender" AnchorHorizontal="70%"  />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_rp_title" LabelWidth="250" runat="server" FieldLabel="Title" AnchorHorizontal="90%"  MaxLength="30"></ext:TextField>
                            <ext:TextField Hidden="true" ID="txt_rp_first_name" LabelWidth="250" runat="server" FieldLabel="First Name" AnchorHorizontal="90%"  MaxLength="100"></ext:TextField>
                            <ext:TextField Hidden="true" ID="txt_rp_middle_name" LabelWidth="250" runat="server" FieldLabel="Middle Name" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                            <ext:TextField Hidden="true" ID="txt_rp_prefix" LabelWidth="250" runat="server" FieldLabel="Prefix" AnchorHorizontal="90%"  MaxLength="100"></ext:TextField>
                            <ext:TextField ID="txt_rp_last_name" LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="90%"  MaxLength="100"></ext:TextField>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Date of Birth" ID="df_birthdate" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_rp_birth_place" LabelWidth="250" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:Panel ID="Panel_cmb_rp_country_of_birth" Hidden="true" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_rp_country_of_birth" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country Of Birth" AnchorHorizontal="70%"  />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_rp_mothers_name" LabelWidth="250" runat="server" FieldLabel="Mothers Name" AnchorHorizontal="90%"  MaxLength="100"></ext:TextField>
                            <ext:TextField ID="txt_rp_alias" LabelWidth="250" runat="server" FieldLabel="Alias" AnchorHorizontal="90%"  MaxLength="100"></ext:TextField>
                           <%-- <ext:TextField ID="txt_rp_full_name_frn" runat="server" FieldLabel="Full Foreign Name" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>--%>
                            <ext:TextField ID="txt_rp_ssn" LabelWidth="250" runat="server" FieldLabel="SSN" AnchorHorizontal="90%"  MaxLength="16"></ext:TextField>
                            <ext:TextField ID="txt_rp_passport_number" LabelWidth="250" runat="server" FieldLabel="Passport Number" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:Panel ID="Panel_cmb_rp_passport_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_rp_passport_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Passport Country" AnchorHorizontal="70%"  />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_rp_id_number" LabelWidth="250" runat="server" FieldLabel="ID Number" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:Panel ID="Panel_cmb_rp_nationality1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_rp_nationality1" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 1" AnchorHorizontal="70%"  />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_cmb_rp_nationality2" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_rp_nationality2" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 2" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_cmb_rp_nationality3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_rp_nationality3" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Nationality 3" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_cmb_rp_residence" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_rp_residence" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Residence" AnchorHorizontal="70%"  />
                                </Content>
                            </ext:Panel>
                            <%--<ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Residence Since" ID="df_rp_residence_since" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />--%>
                            <ext:TextField ID="txt_rp_occupation" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:TextField ID="txt_rp_employer_name" LabelWidth="250" runat="server" FieldLabel="Work Place" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_rp_deceased" FieldLabel="Deceased"></ext:Checkbox>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Date Deceased" ID="df_rp_date_deceased" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_rp_tax_number" LabelWidth="250" runat="server" FieldLabel="Tax Number" AnchorHorizontal="90%"  MaxLength="16"></ext:TextField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_rp_tax_reg_number" FieldLabel="Tax Register Number?"></ext:Checkbox>
                            <ext:TextField ID="txt_rp_source_of_wealth" LabelWidth="250" runat="server" FieldLabel="Source Of Wealth" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_rp_is_protected" FieldLabel="Is Protected?"></ext:Checkbox>

                            <ext:Panel ID="Panel_RelatedPerson_GridAddress" Title="Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridAddress runat="server" ID="GridAddress_RelatedPerson" UniqueName="RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="31"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridAddressWork" Title="Work Place Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridAddress runat="server" ID="GridAddressWork_RelatedPerson" UniqueName="Work.RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="32"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridPhone" Title="Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridPhone runat="server" ID="GridPhone_RelatedPerson" UniqueName="RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="31"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridPhoneWork" Title="Work Place Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridPhone runat="server" ID="GridPhoneWork_RelatedPerson" UniqueName="Work.RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="32" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridIdentification" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridIdentification runat="server" ID="GridIdentification_RelatedPerson" UniqueName="RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="31" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEmail runat="server" ID="GridEmail_RelatedPerson" UniqueName="RelatedPerson.ADD" FK_REF_DETAIL_OF="31"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridSanction runat="server" ID="GridSanction_RelatedPerson" UniqueName="RelatedPerson.ADD" FK_REF_DETAIL_OF="31" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedPerson_GridPEP" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridPEP runat="server" ID="GridPEP_RelatedPerson" UniqueName="RelatedPerson.ADD" FK_REF_DETAIL_OF="31" />
                                </Content>
                            </ext:Panel>

                            <ext:TextField ID="txt_rp_comments" LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%"  MaxLength="8000"></ext:TextField>

                        </Items>
                        <Buttons>
                            <ext:Button ID="btnBackWIC_RelatedPerson" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackWIC_RelatedPerson_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_URL" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity URL/Website Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet12" runat="server" Title="Entity URL/Website" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_URL" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_url" runat="server" FieldLabel="URL" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnBackWIC_URL" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackWIC_URL_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_EntityIdentification" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet13" runat="server" Title="Entity Identification" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_EntityIdentification" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_cmb_ef_type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_ef_type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Identifier_Type" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Identifier Type" AnchorHorizontal="70%"  />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_ef_number"  LabelWidth="250" runat="server" FieldLabel="Number" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Issued Date" ID="df_issue_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Expiry Date" ID="df_expiry_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_ef_issued_by"  LabelWidth="250" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:Panel ID="pnl_ef_issue_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_ef_issue_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country Of Birth" AnchorHorizontal="70%"  />
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_ef_comments"  LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="50"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnBackWIC_EntityIdentification" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackWIC_EntityIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_RelatedEntity" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet14" runat="server" Title="Entity Identification" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_RelatedEntities" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--5 --%>
                            <ext:Panel ID="Panel_cmb_re_entity_relation" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_re_entity_relation" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Entity_Relationship" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Relation" AnchorHorizontal="70%"  />
                                </Content>
                            </ext:Panel>
                         
                           <%-- <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid From" ID="df_re_relation_date_range_valid_from" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                          
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_re_relation_date_range_is_approx_from_date" FieldLabel="Relation Date Range -  Is Approx From Date?"></ext:Checkbox>
                         
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid To" ID="df_re_relation_date_range_valid_to" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                           
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_re_relation_date_range_is_approx_to_date" FieldLabel="Relation Date Range -  Is Approx To Date?"></ext:Checkbox>
                           
                            <ext:NumberField runat="server" LabelWidth="250" ID="nfd_re_share_percentage" FieldLabel="Share Percentage" AnchorHorizontal="70%" MouseWheelEnabled="false"  MinValue="0" EmptyNumber="0" Number="0" />--%>
                            <%--11 --%>
                            <ext:TextField ID="txt_re_relation_comments"  LabelWidth="250" runat="server" FieldLabel="Relation Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                            <%--12 --%>
                            <ext:TextField ID="txt_re_name"  LabelWidth="250" runat="server" FieldLabel="Corporate Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--13 --%>
                            <ext:TextField ID="txt_re_commercial_name"  LabelWidth="250" runat="server" FieldLabel="Commercial Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--14 --%>
                            <ext:Panel ID="Panel_cmb_re_incorporation_legal_form" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_re_incorporation_legal_form" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Bentuk_Badan_Usaha" StringFilter="Active = 1" EmptyText="Select One" Label="Legal Form" AnchorHorizontal="70%"  />
                                </Content>
                            </ext:Panel>
                            <%--15 --%>
                            <ext:TextField ID="txt_re_incorporation_number"  LabelWidth="250" runat="server" FieldLabel="License Number" AnchorHorizontal="90%" MaxLength="50"></ext:TextField>
                            <%--16 --%>
                            <ext:TextField ID="txt_re_business"  LabelWidth="250" runat="server" FieldLabel="Line of Business" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                        
                           <%-- <ext:Panel ID="Panel_cmb_re_entity_status" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_re_entity_status" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Status" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Status" AnchorHorizontal="70%"  />
                                </Content>
                            </ext:Panel>
                        
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Entity Status Date" ID="df_re_entity_status_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />--%>
                            <%--19 --%>
                            <ext:TextField ID="txt_re_incorporation_state"  LabelWidth="250" runat="server" FieldLabel="Province" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--20 --%>
                            <ext:Panel ID="Panel_cmb_re_incorporation_country_code" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_re_incorporation_country_code" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country" AnchorHorizontal="70%"  />
                                </Content>
                            </ext:Panel>
                            <%--21 --%>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Date of Established" ID="df_re_incorporation_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%--22 --%>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_re_business_closed" FieldLabel="Businesss Closed?"></ext:Checkbox>
                            <%--23 --%>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Date Business Closed" ID="df_re_date_business_closed" AnchorHorizontal="90%" Format="dd-MMM-yyyy"  Hidden="true"/>
                            <%--24 --%>
                            <ext:TextField ID="txt_re_tax_number"  LabelWidth="250" runat="server" FieldLabel="Tax Number" AnchorHorizontal="90%" MaxLength="16"></ext:TextField>
                            <%--25 --%>
                            <ext:TextField ID="txt_re_tax_reg_number" Hidden="true"  LabelWidth="250" runat="server" FieldLabel="Tax Reg Number" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>

                            <ext:Panel ID="Panel_RelatedEntity_GridPhone" Title="Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <NDS:GridPhone runat="server" ID="GridPhone_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridAddress" Title="Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <NDS:GridAddress runat="server" ID="GridAddress_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEmail runat="server" ID="GridEmail_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_EntityIdentification" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEntityIdentification runat="server" ID="GridEntityIdentification_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridURL" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridURL runat="server" ID="GridURL_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridSanction runat="server" ID="GridSanction_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>

                            <ext:TextField ID="txt_re_comments"  LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button12" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackWIC_RelatedEntities_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--End Popup--%>
        
    <ext:Container ID="container" runat="server" Layout="VBoxLayout">
        <LayoutConfig>
            <ext:VBoxLayoutConfig Align="Stretch"></ext:VBoxLayoutConfig>
        </LayoutConfig>
        <Items>
            <ext:FormPanel ID="PanelInfo" runat="server" Title="Module Approval" BodyPadding="10" Collapsible="true">
                <Items>
                    <ext:DisplayField ID="lblModuleName" runat="server" FieldLabel="Label Name">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblModuleKey" runat="server" FieldLabel="Module Key">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblAction" runat="server" FieldLabel="Action">
                    </ext:DisplayField>
                    <ext:DisplayField ID="LblCreatedBy" runat="server" FieldLabel="Created By">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblCreatedDate" runat="server" FieldLabel="Created Date">
                    </ext:DisplayField>
                </Items>
            </ext:FormPanel>

            <ext:TabPanel ID="Panel1" runat="server" Layout="HBoxLayout" ButtonAlign="Center" Flex="1" AutoScroll="true" Scrollable="Vertical" Height="1500px" BodyPadding="5">
                <Items>
                    <ext:FormPanel ID="FormPanelOld" runat="server" Title="Old Value" Flex="1" BodyPadding="10" MarginSpec="0 5 0 0">
                        <Items>
                            <ext:DisplayField ID="PKWICOld" runat="server" FieldLabel="ID"></ext:DisplayField>
                            <ext:DisplayField ID="DspWICNoOld" runat="server" FieldLabel="WIC No"></ext:DisplayField>
                            <ext:DisplayField ID="DspIsUpdateFromDataSourceOld" runat="server" FieldLabel="Update From Data Source ?"></ext:DisplayField>
                            <ext:DisplayField ID="DspIsRealWICOld" runat="server" FieldLabel="Is Real WIC ?"></ext:DisplayField>
                            <ext:FormPanel runat="server" Title="WIC Add Individu" Collapsible="true" AnchorHorizontal="98%" ID="WIC_IndividuOld" Border="true" BodyStyle="padding:10px">
                                <Defaults>
                                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                                </Defaults>
                                <Items>
                                    <ext:DisplayField ID="TxtINDV_GenderOld" runat="server" FieldLabel="Gender" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_TitleOld" runat="server" FieldLabel="Title" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_last_nameOld" runat="server" FieldLabel="Full Name" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="DateINDV_BirthdateOld" runat="server" Flex="1" FieldLabel="Birth Date"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Birth_placeOld" runat="server" FieldLabel="Birth Place" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Mothers_nameOld" runat="server" FieldLabel="Mother Maiden Name" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_AliasOld" runat="server" FieldLabel="Alias Name" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_SSNOld" runat="server" FieldLabel="NIK" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Passport_numberOld" runat="server" FieldLabel="No. Passport" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Passport_countryOld" runat="server" FieldLabel="Passport Issued Country" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_ID_NumberOld" runat="server" FieldLabel="Other ID Number" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CmbINDV_Nationality1Old" runat="server" FieldLabel="Nationality 1" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="CmbINDV_Nationality2Old" runat="server" FieldLabel="Nationality 2" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="CmbINDV_Nationality3Old" runat="server" FieldLabel="Nationality 3" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="CmbINDV_residenceOld" runat="server" FieldLabel="Domicile Country" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="TxtINDV_Is_Protected_Old" runat="server" FieldLabel="Is Protected" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>

                                    <%--<ext:GridPanel ID="GridPanelPhoneIndividuDetailBefore" runat="server" Title="Phone Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model23" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn23" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column5" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column11" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" Width="90"></ext:Column>
                                                <ext:Column ID="Column19" runat="server" DataIndex="number" Text="Phone Number" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn8" runat="server" Text="Action" Flex="1">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar27" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelPhoneIndividuDetailOld" runat="server" Title="Phone" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneIndividuDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="ModelDetailOld" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumberPhoneDetailIndividuOld" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column Flex="1" ID="colTph_Contact_TypeIndividuOld" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" Width="170"></ext:Column>
                                                <ext:Column Flex="1" ID="coltph_communication_typeIndividuOld" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" Width="90"></ext:Column>
                                                <ext:Column Flex="1" ID="coltph_numberIndividuOld" runat="server" DataIndex="number" Text="Phone Number" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneIndividuDetailOld" runat="server" Text="Action" Flex="1">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar28" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelAddressIndividuDetailBefore" runat="server" Title="Address Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="Store3" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model25" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn25" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column87" runat="server" DataIndex="Type_Address" Text="Address Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column88" runat="server" DataIndex="Addres" Text="Address"></ext:Column>
                                                <ext:Column ID="Column89" runat="server" DataIndex="Cty" Text="City"></ext:Column>
                                                <ext:Column ID="Column90" runat="server" DataIndex="CountryCode" Text="Country"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn11" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar29" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelAddressIndividuDetailOld" runat="server" Title="Address" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressIndividuDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model26" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailIndividuOld" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column Flex="1" ID="ColAddress_typeIndividuOld" runat="server" DataIndex="Type_Address" Text="Address Type" Width="170"></ext:Column>
                                                <ext:Column Flex="1" ID="ColAddressIndividuOld" runat="server" DataIndex="Addres" Text="Address"></ext:Column>
                                                <ext:Column Flex="1" ID="ColCityIndividuOld" runat="server" DataIndex="Cty" Text="City"></ext:Column>
                                                <ext:Column Flex="1" ID="Colcountry_codeIndividuOld" runat="server" DataIndex="CountryCode" Text="Country"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressIndividuDetailOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar30" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:DisplayField ID="TxtINDV_EmailOld" runat="server" FieldLabel="Email" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email2Old" runat="server" FieldLabel="Email2" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email3Old" runat="server" FieldLabel="Email3" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email4Old" runat="server" FieldLabel="Email4" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email5Old" runat="server" FieldLabel="Email5" AnchorHorizontal="80%"></ext:DisplayField>--%>
                                    <ext:DisplayField ID="TxtINDV_OccupationOld" runat="server" FieldLabel="Occupation" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_employer_nameOld" runat="server" FieldLabel="Work Place" AnchorHorizontal="80%"></ext:DisplayField>

                                    <%--<ext:GridPanel ID="GridPanelPhoneIndividualEmployerDetailBefore" runat="server" Hidden="true" Title="Phone Detail Data Employer Before" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model27" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn27" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column95" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column96" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" Width="90"></ext:Column>
                                                <ext:Column ID="Column97" runat="server" DataIndex="number" Text="Phone Number" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn15" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneEmployerIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar31" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelPhoneIndividualEmployerDetailOld" runat="server" Title="Work Place Phone" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneEmployerIndividuDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model28" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn28" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column Flex="1" ID="Column98" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" Width="170"></ext:Column>
                                                <ext:Column Flex="1" ID="Column99" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" Width="90"></ext:Column>
                                                <ext:Column Flex="1" ID="Column100" runat="server" DataIndex="number" Text="Phone Number" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneIndividualEmployerDetailOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneEmployerIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar32" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelAddressIndividualEmployerDetailBefore" runat="server" Hidden="true" Title="Address Detail Data Employer Before" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="Store7" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model31" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn29" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column101" runat="server" DataIndex="Type_Address" Text="Address Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column102" runat="server" DataIndex="Addres" Text="Address"></ext:Column>
                                                <ext:Column ID="Column103" runat="server" DataIndex="Cty" Text="City"></ext:Column>
                                                <ext:Column ID="Column104" runat="server" DataIndex="CountryCode" Text="Country"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn17" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressEmployerIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar33" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelAddressIndividualEmployerDetailOld" runat="server" Title="Work Place Address" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressEmployerIndividuDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model32" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn30" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column Flex="1" ID="Column105" runat="server" DataIndex="Type_Address" Text="Address Type" Width="170"></ext:Column>
                                                <ext:Column Flex="1" ID="Column106" runat="server" DataIndex="Addres" Text="Address"></ext:Column>
                                                <ext:Column Flex="1" ID="Column107" runat="server" DataIndex="Cty" Text="City"></ext:Column>
                                                <ext:Column Flex="1" ID="Column108" runat="server" DataIndex="CountryCode" Text="Country"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressIndividualEmployerDetailOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressEmployerIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar34" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelIdentificationIndividualDetailBefore" runat="server" Title="Identitas Detail Data Before" Hidden="true" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="Store9" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model33" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn31" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column109" runat="server" DataIndex="Tipe" Text="Identity Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column110" runat="server" DataIndex="No" Text="Identity Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column111" runat="server" DataIndex="IssueDate" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column112" runat="server" DataIndex="ExpiryDate" Text="Expired Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column113" runat="server" DataIndex="IssuedCountry" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn19" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdIdentificationBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar35" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelIdentificationIndividualDetailOld" runat="server" Title="Identification" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreIdentificationDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model34" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn32" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column114" runat="server" DataIndex="Tipe" Text="Identity Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column115" runat="server" DataIndex="No" Text="Identity Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column116" runat="server" DataIndex="IssueDate" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column117" runat="server" DataIndex="ExpiryDate" Text="Expired Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column118" runat="server" DataIndex="IssuedCountry" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnIdentificationIndividualDetailOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdIdentificationBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar36" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField ID="CbINDV_DeceasedOld" runat="server" FieldLabel="is Deceased ?" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="DateINDV_Deceased_DateOld" runat="server" Flex="1" FieldLabel="Tanggal Meninggal" Hidden="true" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Tax_NumberOld" runat="server" FieldLabel="Tax Number" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CbINDV_Tax_Reg_NumberOld" runat="server" FieldLabel="is PEP ?" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Source_Of_WealthOld" runat="server" FieldLabel="Source Of Wealth" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_CommentsOld" runat="server" FieldLabel="Notes" AnchorHorizontal="80%"></ext:DisplayField>
                                    
                                    <%-- Add By Septian, Grid Panel Individu Old, 2023-03-02 %-->
                                    <%-- INDIVIDU Email --%>
                                    <ext:GridPanel ID="GP_WIC_INDV_Email_Old" runat="server" Title="Email" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Customer_Email_Old" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="model_email_old" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Email_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="email_address" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_indv_email_old" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_WIC_Previous_Name_ID" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                                <ext:CommandColumn ID="CC_INDV_Email_Old" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                        <%--<ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>--%>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_Email">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Email_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_indv_email_address_old" runat="server" DataIndex="email_address" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_indv_email_old" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Individu Email --%>
                                    
                                    <%-- Individu Person PEP --%>
                                    <ext:GridPanel ID="GP_WIC_INDV_PEP_Old" runat="server" Title="Person PEP" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_WIC_Person_PEP" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_person_pep" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_PEP_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="pep_country" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="function_name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="function_description" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="pep_date_range_valid_from" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="pep_date_range_is_approx_from_date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="pep_date_range_valid_to" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="pep_date_range_is_approx_to_date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_person_pep" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                                <ext:CommandColumn ID="CC_INDV_PEP_Old" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_PEP">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_PEP_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_person_pep_first_name" runat="server" DataIndex="pep_country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_person_pep_function_name" runat="server" DataIndex="function_name" Text="Function Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_person_pep_description" runat="server" DataIndex="function_description" Text="Description" MinWidth="130" Flex="1"></ext:Column>
                                               <%-- <ext:Column ID="clm_person_pep_valid_from" runat="server" DataIndex="pep_date_range_valid_from" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_person_is_approx_from_date" runat="server" DataIndex="pep_date_range_is_approx_from_date" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_person_is_valid_to" runat="server" DataIndex="pep_date_range_valid_to" Text="Valid To" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_person_is_approx_to_date" runat="server" DataIndex="pep_date_range_is_approx_to_date" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_person_comments" runat="server" DataIndex="comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_add_info" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Individu Person PEP -->
                                    
                                    <%-- Individu Sanction --%>
                                    <ext:GridPanel ID="GP_WIC_INDV_Sanction_Old" runat="server" Title="Sanction" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Customer_Sanction" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_sanction" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Sanction_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Provider" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Match_Criteria" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Link_To_Source" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Attributes" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_From" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_From_Date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_To" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_To_Date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_sanction" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                                <ext:CommandColumn ID="CC_INDV_Sanction_Old" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_Sanction">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_provider" runat="server" DataIndex="Provider" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_list_name" runat="server" DataIndex="Sanction_List_Name" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_match_criteria" runat="server" DataIndex="Match_Criteria" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_link_to_source" runat="server" DataIndex="Link_To_Source" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_attributes" runat="server" DataIndex="Sanction_List_Attributes" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                                               <%-- <ext:Column ID="clm_saction_valid_from" runat="server" DataIndex="Sanction_List_Date_Range_Valid_From" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_is_approx_from_date" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_From_Date" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_valid_to" runat="server" DataIndex="Sanction_List_Date_Range_Valid_To" Text="Valid To" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_is_approx_to_date" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_To_Date" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_sanction_comments" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_sanction" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Individu Sanction --%>
                                    
                                    <%-- Individu Related Person --%>
                                    <ext:GridPanel ID="GP_WIC_INDV_RelatedPerson_Old" runat="server" Title="Related Person" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_WIC_RelatedPerson_Old" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_related_person_old" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Related_Person_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Person_Person_Relation" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Gender" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Title" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="First_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Middle_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Prefix" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Birth_Date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Country_Of_Birth" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Mother_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="_Alias" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Passport_Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Passport_Country" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ID_Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Nationality1" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Nationality2" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Nationality3" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Residence" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Occupation" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Deceased" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Date_Deceased" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tax_Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Is_PEP" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Source_Of_Wealth" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Is_Protected" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_related_person" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="CC_INDV_RelatedPerson_Old" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_RelatedPerson">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_rp_person_relation" runat="server" DataIndex="Person_Person_Relation" Text="Person Person Relation" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="clm_rp_comments" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_gender" runat="server" DataIndex="Gender" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_title" runat="server" DataIndex="Title" Text="Title" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_first_name" runat="server" DataIndex="First_Name" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_middle_name" runat="server" DataIndex="Middle_Name" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_prefix" runat="server" DataIndex="Prefix" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_rp_last_name" runat="server" DataIndex="Last_Name" Text="Full Name" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="clm_rp_birth_date" runat="server" DataIndex="Birth_Date" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_birth_place" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_country_of_birth" runat="server" DataIndex="Country_Of_Birth" Text="Country of Birth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_mother_name" runat="server" DataIndex="Mother_Name" Text="Mother name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_alias" runat="server" DataIndex="_Alias" Text="Alias" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <%--<ext:Column ID="clm_rp_foreign_name" runat="server" DataIndex="FULL_NAME_FRN" Text="Full Foreign Name" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <%--<ext:Column ID="clm_rp_ssn" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_passport_number" runat="server" DataIndex="Passport_Number" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_passport_country" runat="server" DataIndex="Passport_Country" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_id_number" runat="server" DataIndex="ID_Number" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_rp_nationality1" runat="server" Hidden="true" DataIndex="Nationality1" Text="Nationality 1" MinWidth="130" Flex="1"></ext:Column>
                                               <%-- <ext:Column ID="clm_rp_nationality2" runat="server" DataIndex="Nationality2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_nationality3" runat="server" DataIndex="Nationality3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_residence" runat="server" DataIndex="Residence" Text="Residence" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <%--<ext:Column ID="clm_rp_residence_since" runat="server" DataIndex="RESIDENCE_SINCE" Text="Residence Since" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <%--<ext:Column ID="clm_rp_occupation" runat="server" DataIndex="Occupation" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_deceased" runat="server" DataIndex="Deceased" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_date_deceased" runat="server" DataIndex="Date_Deceased" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_tax_number" runat="server" DataIndex="Tax_Number" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_is_pep" runat="server" DataIndex="Is_PEP" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_source_of_wealth" runat="server" DataIndex="Source_Of_Wealth" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_is_protected" runat="server" DataIndex="Is_Protected" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>--%>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_related_person" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Individu Related Person --%>
                                    <%-- End Add, Grid Panel GoAML 5.0.1 --%>
                                </Items>
                            </ext:FormPanel>

                            <ext:FormPanel runat="server" Title="WIC Add Corporate" Collapsible="true" AnchorHorizontal="98%" ID="WIC_CorporateOld" Border="true" BodyStyle="padding:10px">
                                <Defaults>
                                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                                </Defaults>
                                <Items>
                                    <ext:DisplayField ID="TxtCorp_NameOld" runat="server" FieldLabel="Corporate Name" AnchorHorizontal="80%" ></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_Commercial_nameOld" runat="server" FieldLabel="Commercial Name" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CmbCorp_Incorporation_legal_formOld" runat="server" FieldLabel="Legal Form" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="TxtCorp_Incorporation_numberOld" runat="server" FieldLabel="Corporate License Number" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_BusinessOld" runat="server" FieldLabel="Line Of Bussiness" AnchorHorizontal="80%"></ext:DisplayField>

                                    <%--<ext:GridPanel ID="GridPanelPhoneCorporateDetailBefore" runat="server" Title="Phone Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="Store11" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model35" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn33" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column119" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column120" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" Width="90"></ext:Column>
                                                <ext:Column ID="Column121" runat="server" DataIndex="number" Text="Phone Number" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn21" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneCorporateBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar37" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelPhoneCorporateDetailOld" runat="server" Title="Phone" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneCorporateDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model36" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn34" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column122" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column123" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" Width="90"></ext:Column>
                                                <ext:Column ID="Column124" runat="server" DataIndex="number" Text="Phone Number" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneCorporateDetailOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneCorporateBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar38" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelAddressCorporateDetailBefore" runat="server" Title="Address Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="Store13" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model37" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn35" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column125" runat="server" DataIndex="Type_Address" Text="Address Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column126" runat="server" DataIndex="Addres" Text="Address"></ext:Column>
                                                <ext:Column ID="Column127" runat="server" DataIndex="Cty" Text="City"></ext:Column>
                                                <ext:Column ID="Column128" runat="server" DataIndex="CountryCode" Text="Country"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn23" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressCorporateBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar39" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelAddressCorporateDetailOld" runat="server" Title="Address" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressCorporateDetailOld" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model38" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn36" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column129" runat="server" DataIndex="Type_Address" Text="Address Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column130" runat="server" DataIndex="Addres" Text="Address"></ext:Column>
                                                <ext:Column ID="Column131" runat="server" DataIndex="Cty" Text="City"></ext:Column>
                                                <ext:Column ID="Column132" runat="server" DataIndex="CountryCode" Text="Country"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressCorporateDetailOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressCorporateBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar40" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField ID="TxtCorp_EmailOld" runat="server" FieldLabel="Email Korporasi" AnchorHorizontal="80%" Hidden="true"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_urlOld" runat="server" FieldLabel="Website Korporasi" AnchorHorizontal="80%" Hidden="true"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_incorporation_stateOld" runat="server" FieldLabel="Province" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CmbCorp_incorporation_country_codeOld" runat="server" FieldLabel="Country" AnchorHorizontal="80%" />

                                    <%--<ext:GridPanel ID="GridPanelDirector_CorpBefore" runat="server" Title="Director Corp Before" AutoScroll="true" Hidden="true">
                                        <Store>
                                            <ext:Store ID="Store15" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Model39" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="NO_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Roles" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn37" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column133" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column134" runat="server" DataIndex="Roles" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumn25" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustDirectorBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.NO_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar41" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GP_Corp_DirectorOld" runat="server" Title="Director Corp" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Director_CorpOld" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Model_Director_CorpOld" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="NO_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Roles" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn38" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column135" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column136" runat="server" DataIndex="Roles" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnDirectorOld" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustDirectorOld">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.NO_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar42" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:DisplayField ID="CmbCorp_RoleOld" runat="server" FieldLabel="Role" AnchorHorizontal="80%" />--%>
                                    <ext:DisplayField ID="DateCorp_incorporation_dateOld" runat="server" Flex="1"  FieldLabel="Date Of Stablished"></ext:DisplayField>
                                    <ext:DisplayField ID="chkCorp_business_closedOld" runat="server" FieldLabel="Is Closed?"></ext:DisplayField>
                                    <ext:DisplayField ID="DateCorp_date_business_closedOld" runat="server" Flex="1" FieldLabel="Closed Date" Hidden="true"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_tax_numberOld" runat="server" FieldLabel="Tax Number" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_CommentsOld" runat="server" FieldLabel="Notes" AnchorHorizontal="80%"></ext:DisplayField>
                                
                                    <%-- Add by Septian, Grid Panel Corporate Old, goAML 5.0.1, 2023-03-02 %-->
                                    <%-- Corporate Email --%>
                                    <ext:GridPanel ID="GP_WIC_CORP_Email_Old" runat="server" Title="Email" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_corp_email" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_corp_email" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Email_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="email_address" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rcn_cor_email" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_WIC_Previous_Name_ID" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                                <ext:CommandColumn ID="CC_CORP_Email_Old" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_Email">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Email_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_corp_email_address" runat="server" DataIndex="email_address" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_corp_email" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Corporate Email --%>
                                    
                                    <%-- Corporate URL --%>
                                    <ext:GridPanel ID="GP_WIC_CORP_URL_Old" runat="server" Title="Entity URL" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Customer_Entity_URL" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_entity_url" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_URL_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="URL" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_entity_url" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                                <ext:CommandColumn ID="CC_CORP_URL_Old" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    <%--    <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>--%>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_URL">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_URL_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_url_type" runat="server" DataIndex="URL" Text="URL" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_entity_url" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Corporate URL --%>
                                    
                                    <%-- Corporate Entity Identification --%>
                                    <ext:GridPanel ID="GP_WIC_CORP_EntityIdentification_Old" runat="server" Title="Entity Identification" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Customer_Entity_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_ent_identify" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Entity_Identifications_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TYPE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ISSUE_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EXPIRY_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ISSUED_BY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ISSUE_COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_identify" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                                <ext:CommandColumn ID="CC_CORP_EntityIdentification_Old" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                        <%--<ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>--%>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_EntityIdentification">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Entity_Identifications_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_identify_type" runat="server" DataIndex="TYPE" Text="Entity Identifier Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_identify_number" runat="server" DataIndex="NUMBER" Text="Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_identify_issued_date" runat="server" DataIndex="ISSUE_DATE" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_identify_expiry_date" runat="server" DataIndex="EXPIRY_DATE" Text="Expiry Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_identify_issued_by" runat="server" DataIndex="ISSUED_BY" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_identify_issued_country" runat="server" DataIndex="ISSUE_COUNTRY" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_identify_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_identify" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Corporate Entity Identication --%>
                                    
                                    <%-- Corporate Sanction --%>
                                    <ext:GridPanel ID="GP_WIC_CORP_Sanction_Old" runat="server" Title="Sanction" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_corp_sanction" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_corp_sanction" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Sanction_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Provider" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Match_Criteria" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Link_To_Source" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Attributes" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_From" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_From_Date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_To" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_To_Date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_corp_sanction" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                                <ext:CommandColumn ID="CC_CORP_Sanction_Old" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_Sanction">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_corp_sanction" runat="server" DataIndex="Provider" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_sanction_list_name" runat="server" DataIndex="Sanction_List_Name" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_match_criteria" runat="server" DataIndex="Match_Criteria" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_link_to_source" runat="server" DataIndex="Link_To_Source" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_sanction_list_attributes" runat="server" DataIndex="Sanction_List_Attributes" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                                             <%--   <ext:Column ID="clm_corp_valid_from" runat="server" DataIndex="Sanction_List_Date_Range_Valid_From" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_is_approx_from_date" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_From_Date" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_is_valid_to" runat="server" DataIndex="Sanction_List_Date_Range_Valid_To" Text="Valid To" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_is_approx_to_date" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_To_Date" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_corp_comments" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_corp_sanction" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Corporate Sanction --%>
                                    
                                    <%-- Corporate Related Person --%>
                                    <ext:GridPanel ID="GP_WIC_CORP_RelatedPerson_Old" Hidden="true" runat="server" Title="Related Person" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="StoreCorp_Related_person" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_corp_related_person" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Related_Person_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Person_Person_Relation" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Gender" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Title" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="First_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Middle_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Prefix" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Birth_Date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Country_Of_Birth" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Mother_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="_Alias" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Passport_Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Passport_Country" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ID_Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Nationality1" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Nationality2" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Nationality3" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Residence" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Occupation" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Deceased" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Date_Deceased" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tax_Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Is_PEP" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Source_Of_Wealth" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Is_Protected" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_corp_related_person" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="CC_CORP_RelatedPerson_Old" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_RelatedPerson">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_corp_rp_person_relation" runat="server" DataIndex="Person_Person_Relation" Text="Person Person Relation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_comments" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_gender" runat="server" DataIndex="Gender" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_title" runat="server" DataIndex="Title" Text="Title" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_first_name" runat="server" DataIndex="First_Name" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_middle_name" runat="server" DataIndex="Middle_Name" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_prefix" runat="server" DataIndex="Prefix" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_last_name" runat="server" DataIndex="Last_Name" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_birth_date" runat="server" DataIndex="Birth_Date" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_birth_place" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_country_of_birth" runat="server" DataIndex="Country_Of_Birth" Text="Country of Birth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_mother_name" runat="server" DataIndex="Mother_Name" Text="Mother name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_alias" runat="server" DataIndex="_Alias" Text="Alias" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="clm_corp_rp_foreign_name" runat="server" DataIndex="FULL_NAME_FRN" Text="Full Foreign Name" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_corp_rp_ssn" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_passport_number" runat="server" DataIndex="Passport_Number" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_passport_country" runat="server" DataIndex="Passport_Country" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_id_number" runat="server" DataIndex="ID_Number" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_nationality1" runat="server" DataIndex="Nationality1" Text="Nationality 1" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_nationality2" runat="server" DataIndex="Nationality2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_nationality3" runat="server" DataIndex="Nationality3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_residence" runat="server" DataIndex="Residence" Text="Residence" MinWidth="130" Flex="1"></ext:Column>
                                              <%--  <ext:Column ID="clm_corp_rp_residence_since" runat="server" DataIndex="RESIDENCE_SINCE" Text="Residence Since" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_corp_rp_occupation" runat="server" DataIndex="Occupation" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_deceased" runat="server" DataIndex="Deceased" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_date_deceased" runat="server" DataIndex="Date_Deceased" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_tax_number" runat="server" DataIndex="Tax_Number" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_is_pep" runat="server" DataIndex="Is_PEP" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_source_of_wealth" runat="server" DataIndex="Source_Of_Wealth" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_corp_rp_is_protected" runat="server" DataIndex="Is_Protected" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_corp_related_person" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Corporate Related Person--%>
                                    
                                    <%-- Corporate Related Entity --%>
                                    <ext:GridPanel ID="GP_WIC_CORP_RelatedEntity_Old" runat="server" Title="Related Entities" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Related_Entities" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_related_entities" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Related_Entities_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ENTITY_ENTITY_RELATION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMERCIAL_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_LEGAL_FORM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BUSINESS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_STATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_COUNTRY_CODE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DATE_BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_related_etities" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="CC_CORP_RelatedEntities_Old" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_RelatedEntities">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Related_Entities_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_re_entity_relation" runat="server" DataIndex="ENTITY_ENTITY_RELATION" Text="Entity Relation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_comments" runat="server" DataIndex="COMMENTS" Hidden="true" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_name" runat="server" DataIndex="NAME" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_commercial_name" runat="server" DataIndex="COMMERCIAL_NAME" Hidden="true" Text="Commercial Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_incorp_legal_form" runat="server" DataIndex="INCORPORATION_LEGAL_FORM" Hidden="true" Text="Incorporation Legal Form" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="cml_re_incorp_number" runat="server" DataIndex="INCORPORATION_NUMBER" Hidden="true" Text="Incorporation Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_business" runat="server" DataIndex="BUSINESS" Hidden="true" Text="Business" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_incorp_state" runat="server" DataIndex="INCORPORATION_STATE" Hidden="true" Text="Incorporation State" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="cml_re_incorp_country_code" runat="server" DataIndex="INCORPORATION_COUNTRY_CODE" Hidden="true" Text="Incorporation Country Code" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_incorp_date" runat="server" DataIndex="INCORPORATION_DATE" Hidden="true" Text="Incorporation Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_business_closed" runat="server" DataIndex="BUSINESS_CLOSED" Hidden="true" Text="Businesss Closed" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_date_business_closed" runat="server" DataIndex="DATE_BUSINESS_CLOSED" Hidden="true" Text="Date Business Closed" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_tax_number" runat="server" DataIndex="TAX_NUMBER" Hidden="true" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_tax_reg_number" runat="server" DataIndex="TAX_REG_NUMBER" Hidden="true" Text="Tax Reg Number" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_related_entities" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Corporate Related Entity --%>
                                    <%-- End Add --%>
                                </Items>
                            </ext:FormPanel>
                        </Items>
                    </ext:FormPanel>

                    <ext:FormPanel ID="FormPanelNew" runat="server" Title="New Value" Flex="1" BodyPadding="10">
                        <Items>
                            <ext:DisplayField ID="PKWIC" runat="server" FieldLabel="ID"></ext:DisplayField>
                            <ext:DisplayField ID="DspWICNo" runat="server" FieldLabel="WIC No"></ext:DisplayField>
                            <ext:DisplayField ID="DspIsUpdateFromDataSource" runat="server" FieldLabel="Update From Data Source ?"></ext:DisplayField>
                            <ext:DisplayField ID="DspIsRealWICNew" runat="server" FieldLabel="Is Real WIC ?"></ext:DisplayField>
                            <ext:FormPanel runat="server" Title="WIC Add Individu" Collapsible="true" AnchorHorizontal="98%" ID="WIC_Individu" Border="true" BodyStyle="padding:10px">
                                <%--<Defaults>
                                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                                </Defaults>--%>
                                <Items>
                                    <ext:DisplayField ID="TxtINDV_Gender" runat="server" FieldLabel="Gender" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Title" runat="server" FieldLabel="Title" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_last_name" runat="server" FieldLabel="Full Name" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="DateINDV_Birthdate" runat="server" Flex="1" FieldLabel="Birth Date"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Birth_place" runat="server" FieldLabel="Birth Place" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Mothers_name" runat="server" FieldLabel="Mother Maiden Name" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Alias" runat="server" FieldLabel="Alias Name" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_SSN" runat="server" FieldLabel="NIK" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Passport_number" runat="server" FieldLabel="No. Passport" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Passport_country" runat="server" FieldLabel="Passport Issued Country" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_ID_Number" runat="server" FieldLabel="Other ID Number" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CmbINDV_Nationality1" runat="server" FieldLabel="Nationality 1" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="CmbINDV_Nationality2" runat="server" FieldLabel="Nationality 2" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="CmbINDV_Nationality3" runat="server" FieldLabel="Nationality 3" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="CmbINDV_residence" runat="server" FieldLabel="Domicile Country" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="TxtINDV_Is_Protected_New" runat="server" FieldLabel="Is Protected" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>

                                    <%--<ext:GridPanel ID="GridPanelPhoneIndividuDetailBefore" runat="server" Title="Phone Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="StorePhoneIndividuDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model11" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column35" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column36" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" Width="90"></ext:Column>
                                                <ext:Column ID="Column37" runat="server" DataIndex="number" Text="Phone Number" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneIndividuDetailBefore" runat="server" Text="Action" Flex="1">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar15" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelPhoneIndividuDetail" runat="server" Title="Phone" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneIndividuDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="ModelDetail" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumberPhoneDetailIndividu" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="colTph_Contact_TypeIndividu" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" Flex="1"></ext:Column>
                                                <ext:Column ID="coltph_communication_typeIndividu" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" Flex="1"></ext:Column>
                                                <ext:Column ID="coltph_numberIndividu" runat="server" DataIndex="number" Text="Phone Number" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneIndividuDetail" runat="server" Text="Action" Flex="1">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneIndividu">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar13" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelAddressIndividuDetailBefore" runat="server" Title="Address Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="StoreAddressIndividuDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model12" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column38" runat="server" DataIndex="Type_Address" Text="Address Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column39" runat="server" DataIndex="Addres" Text="Address"></ext:Column>
                                                <ext:Column ID="Column40" runat="server" DataIndex="Cty" Text="City"></ext:Column>
                                                <ext:Column ID="Column41" runat="server" DataIndex="CountryCode" Text="Country"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressIndividuDetailBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelAddressIndividuDetail" runat="server" Title="Address" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressIndividuDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model1" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailIndividu" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="ColAddress_typeIndividu" runat="server" DataIndex="Type_Address" Text="Address Type" Flex="1"></ext:Column>
                                                <ext:Column ID="ColAddressIndividu" runat="server" DataIndex="Addres" Text="Address" Flex="1"></ext:Column>
                                                <ext:Column ID="ColCityIndividu" runat="server" DataIndex="Cty" Text="City" Flex="1"></ext:Column>
                                                <ext:Column ID="Colcountry_codeIndividu" runat="server" DataIndex="CountryCode" Text="Country" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressIndividuDetail" runat="server" Text="Action" Flex="1">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressIndividu">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar16" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

<%--                                    <ext:DisplayField ID="TxtINDV_Email" runat="server" FieldLabel="Email" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email2" runat="server" FieldLabel="Email2" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email3" runat="server" FieldLabel="Email3" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email4" runat="server" FieldLabel="Email4" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Email5" runat="server" FieldLabel="Email5" AnchorHorizontal="80%"></ext:DisplayField>--%>
                                    <ext:DisplayField ID="TxtINDV_Occupation" runat="server" FieldLabel="Occupation" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_employer_name" runat="server" FieldLabel="Work Place" AnchorHorizontal="80%"></ext:DisplayField>

                                    <%--<ext:GridPanel ID="GridPanelPhoneIndividualEmployerDetailBefore" runat="server" Hidden="true" Title="Phone Detail Data Employer Before" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneEmployerIndividuDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model19" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn20" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column73" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column74" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" Width="90"></ext:Column>
                                                <ext:Column ID="Column75" runat="server" DataIndex="number" Text="Phone Number" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneIndividualEmployerDetailBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneEmployerIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar17" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelPhoneIndividualEmployerDetail" runat="server" Title="Work Place Phone" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneEmployerIndividuDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model20" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn18" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column66" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" Flex="1"></ext:Column>
                                                <ext:Column ID="Column67" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" Flex="1"></ext:Column>
                                                <ext:Column ID="Column68" runat="server" DataIndex="number" Text="Phone Number" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneIndividualEmployerDetail" runat="server" Text="Action" Flex="1">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneEmployerIndividu">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar18" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelAddressIndividualEmployerDetailBefore" runat="server" Hidden="true" Title="Address Detail Data Employer Before" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressEmployerIndividuDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model21" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn21" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column76" runat="server" DataIndex="Type_Address" Text="Address Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column77" runat="server" DataIndex="Addres" Text="Address"></ext:Column>
                                                <ext:Column ID="Column78" runat="server" DataIndex="Cty" Text="City"></ext:Column>
                                                <ext:Column ID="Column79" runat="server" DataIndex="CountryCode" Text="Country"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressIndividualEmployerDetailBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressEmployerIndividuBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar19" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelAddressIndividualEmployerDetail" runat="server" Title="Work Place Address" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressEmployerIndividuDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model29" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn19" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column69" runat="server" DataIndex="Type_Address" Text="Address Type" Flex="1"></ext:Column>
                                                <ext:Column ID="Column70" runat="server" DataIndex="Addres" Text="Address" Flex="1"></ext:Column>
                                                <ext:Column ID="Column71" runat="server" DataIndex="Cty" Text="City" Flex="1"></ext:Column>
                                                <ext:Column ID="Column72" runat="server" DataIndex="CountryCode" Text="Country" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressIndividualEmployerDetail" runat="server" Text="Action" Flex="1">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressEmployerIndividu">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar20" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelIdentificationIndividualDetailBefore" runat="server" Title="Identitas Detail Data Before" Hidden="true" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreIdentificationDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model22" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn22" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column80" runat="server" DataIndex="Tipe" Text="Identity Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column81" runat="server" DataIndex="No" Text="Identity Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column82" runat="server" DataIndex="IssueDate" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column83" runat="server" DataIndex="ExpiryDate" Text="Expired Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column84" runat="server" DataIndex="IssuedCountry" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnIdentificationIndividualDetailBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdIdentificationBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar21" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelIdentificationIndividualDetail" runat="server" Title="Identification" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreIdentificationDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model30" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn15" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column51" runat="server" DataIndex="Tipe" Text="Identity Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column52" runat="server" DataIndex="No" Text="Identity Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column53" runat="server" DataIndex="IssueDate" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column54" runat="server" DataIndex="ExpiryDate" Text="Expired Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column55" runat="server" DataIndex="IssuedCountry" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnIdentificationIndividualDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdIdentification">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar22" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField ID="CbINDV_Deceased" runat="server" FieldLabel="is Deceased ?" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="DateINDV_Deceased_Date" runat="server" Flex="1" FieldLabel="Tanggal Meninggal" Hidden="true" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Tax_Number" runat="server" FieldLabel="Tax Number" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CbINDV_Tax_Reg_Number" runat="server" FieldLabel="is PEP ?" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Source_Of_Wealth" runat="server" FieldLabel="Source Of Wealth" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtINDV_Comments" runat="server" FieldLabel="Notes" AnchorHorizontal="80%"></ext:DisplayField>
                                
                                    <%-- Add By Septian, Grid Panel Individu New, 2023-03-02 %-->
                                    <%-- INDIVIDU Email --%>
                                    <ext:GridPanel ID="GP_WIC_INDV_Email_New" runat="server" Title="Email" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_WIC_Email_New" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="model_email_new" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Email_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="email_address" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_indv_email_new" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_WIC_Previous_Name_ID" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                                <ext:CommandColumn ID="CC_INDV_Email_New" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_Email">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Email_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_indv_email_address_new" runat="server" DataIndex="email_address" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_indv_email" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Individu Email --%>
                                    
                                    <%-- Individu Person PEP --%>
                                    <ext:GridPanel ID="GP_WIC_INDV_PEP_New" runat="server" Title="Person PEP" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Customer_Person_PEP_New" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_person_pep_new" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_PEP_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="pep_country" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="function_name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="function_description" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="pep_date_range_valid_from" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="pep_date_range_is_approx_from_date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="pep_date_range_valid_to" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="pep_date_range_is_approx_to_date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_person_pep_new" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="CC_INDV_PEP_New" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_PEP">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_PEP_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_person_pep_first_name_new" runat="server" DataIndex="pep_country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_person_pep_function_name_new" runat="server" DataIndex="function_name" Text="Function Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_person_pep_description_new" runat="server" DataIndex="function_description" Text="Description" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="clm_person_pep_valid_from_new" runat="server" DataIndex="pep_date_range_valid_from" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_person_is_approx_from_date_new" runat="server" DataIndex="pep_date_range_is_approx_from_date" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_person_is_valid_to_new" runat="server" DataIndex="pep_date_range_valid_to" Text="Valid To" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_person_is_approx_to_date_new" runat="server" DataIndex="pep_date_range_is_approx_to_date" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_person_comments_new" runat="server" DataIndex="comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_add_info_new" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Individu Person PEP -->
                                    
                                    <%-- Individu Sanction --%>
                                    <ext:GridPanel ID="GP_WIC_INDV_Sanction_New" runat="server" Title="Sanction" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Customer_Sanction_New" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_sanction_new" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Sanction_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Provider" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Match_Criteria" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Link_To_Source" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Attributes" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_From" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_From_Date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_To" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_To_Date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_sanction_new" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="CC_INDV_Sanction_New" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_Sanction">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_provider_new" runat="server" DataIndex="Provider" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_list_name_new" runat="server" DataIndex="Sanction_List_Name" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_match_criteria_new" runat="server" DataIndex="Match_Criteria" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_link_to_source_new" runat="server" DataIndex="Link_To_Source" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_attributes_new" runat="server" DataIndex="Sanction_List_Attributes" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="clm_saction_valid_from_new" runat="server" DataIndex="Sanction_List_Date_Range_Valid_From" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_is_approx_from_date_new" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_From_Date" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_valid_to_new" runat="server" DataIndex="Sanction_List_Date_Range_Valid_To" Text="Valid To" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_is_approx_to_date_new" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_To_Date" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_sanction_comments_new" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_sanction_new" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Individu Sanction --%>
                                    
                                    <%-- Individu Related Person --%>
                                    <ext:GridPanel ID="GP_WIC_INDV_RelatedPerson_New" runat="server" Title="Related Person" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Customer_RelatedPerson_New" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_related_person_new" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Related_Person_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Person_Person_Relation" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Gender" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Title" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="First_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Middle_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Prefix" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Birth_Date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Country_Of_Birth" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Mother_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="_Alias" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Passport_Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Passport_Country" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ID_Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Nationality1" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Nationality2" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Nationality3" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Residence" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Occupation" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Deceased" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Date_Deceased" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tax_Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Is_PEP" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Source_Of_Wealth" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Is_Protected" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_related_person_new" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="CC_INDV_RelatedPerson_New" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_RelatedPerson">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_rp_person_relation_new" runat="server" DataIndex="Person_Person_Relation" Text="Person Person Relation" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="clm_rp_comments_new" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_gender_new" runat="server" DataIndex="Gender" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_title_new" runat="server" DataIndex="Title" Text="Title" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_first_name_new" runat="server" DataIndex="First_Name" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_middle_name_new" runat="server" DataIndex="Middle_Name" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_prefix_new" runat="server" DataIndex="Prefix" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_rp_last_name_new" runat="server" DataIndex="Last_Name" Text="Full Name" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="clm_rp_birth_date_new" runat="server" DataIndex="Birth_Date" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_birth_place_new" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_country_of_birth_new" runat="server" DataIndex="Country_Of_Birth" Text="Country of Birth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_mother_name_new" runat="server" DataIndex="Mother_Name" Text="Mother name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_alias_new" runat="server" DataIndex="_Alias" Text="Alias" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <%--<ext:Column ID="clm_rp_foreign_name" runat="server" DataIndex="FULL_NAME_FRN" Text="Full Foreign Name" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <%--<ext:Column ID="clm_rp_ssn_new" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_passport_number_new" runat="server" DataIndex="Passport_Number" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_passport_country_new" runat="server" DataIndex="Passport_Country" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_id_number_new" runat="server" DataIndex="ID_Number" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_rp_nationality1_new" runat="server" DataIndex="Nationality1" Hidden="true" Text="Nationality 1" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="clm_rp_nationality2_new" runat="server" DataIndex="Nationality2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_nationality3_new" runat="server" DataIndex="Nationality3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_residence_new" runat="server" DataIndex="Residence" Text="Residence" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <%--<ext:Column ID="clm_rp_residence_since" runat="server" DataIndex="RESIDENCE_SINCE" Text="Residence Since" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <%--<ext:Column ID="clm_rp_occupation_new" runat="server" DataIndex="Occupation" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_deceased_new" runat="server" DataIndex="Deceased" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_date_deceased_new" runat="server" DataIndex="Date_Deceased" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_tax_number_new" runat="server" DataIndex="Tax_Number" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_is_pep_new" runat="server" DataIndex="Is_PEP" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_source_of_wealth_new" runat="server" DataIndex="Source_Of_Wealth" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_is_protected_new" runat="server" DataIndex="Is_Protected" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>--%>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_related_person_new" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Individu Related Person --%>
                                    <%-- End Add, Grid Panel GoAML 5.0.1 --%>
                                </Items>
                            </ext:FormPanel>

                            <ext:FormPanel runat="server" Title="WIC Add Corporate" Collapsible="true" AnchorHorizontal="98%" ID="WIC_Corporate" Border="true" BodyStyle="padding:10px">
                                <%--<Defaults>
                                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                                </Defaults>--%>
                                <Items>
                                    <ext:DisplayField ID="TxtCorp_Name" runat="server" FieldLabel="Corporate Name" AnchorHorizontal="80%" ></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_Commercial_name" runat="server" FieldLabel="Commercial Name" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CmbCorp_Incorporation_legal_form" runat="server" FieldLabel="Legal Form" AnchorHorizontal="80%" />
                                    <ext:DisplayField ID="TxtCorp_Incorporation_number" runat="server" FieldLabel="Corporate License Number" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_Business" runat="server" FieldLabel="Line Of Bussiness" AnchorHorizontal="80%"></ext:DisplayField>

                                    <%--<ext:GridPanel ID="GridPanelPhoneCorporateDetailBefore" runat="server" Title="Phone Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="StorePhoneCorporateDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model13" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column42" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column43" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" Width="90"></ext:Column>
                                                <ext:Column ID="Column44" runat="server" DataIndex="number" Text="Phone Number" Width="80"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneCorporateDetailBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneCorporateBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar23" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelPhoneCorporateDetail" runat="server" Title="Phone" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StorePhoneCorporateDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model2" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Contact_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tph_Communication_Type" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererPhoneDetailCorporate" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="colTph_Contact_TypeCorporate" runat="server" DataIndex="Tph_Contact_Type" Text="Contact Type" Width="170" Flex="1"></ext:Column>
                                                <ext:Column ID="coltph_communication_typeCorporate" runat="server" DataIndex="Tph_Communication_Type" Text="Communication Type" Width="90" Flex="1"></ext:Column>
                                                <ext:Column ID="coltph_numberCorporate" runat="server" DataIndex="number" Text="Phone Number" Width="80" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnPhoneCorporateDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandPhoneCorporate">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar24" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:GridPanel ID="GridPanelAddressCorporateDetailBefore" runat="server" Title="Address Detail Data Before" AutoScroll="true" EmptyText="No Available Data" Hidden="true">
                                        <Store>
                                            <ext:Store ID="StoreAddressCorporateDetailBefore" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model14" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn13" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column45" runat="server" DataIndex="Type_Address" Text="Address Type" Width="170"></ext:Column>
                                                <ext:Column ID="Column46" runat="server" DataIndex="Addres" Text="Address"></ext:Column>
                                                <ext:Column ID="Column47" runat="server" DataIndex="Cty" Text="City"></ext:Column>
                                                <ext:Column ID="Column48" runat="server" DataIndex="CountryCode" Text="Country"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressCorporateDetailBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressCorporateBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar25" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GridPanelAddressCorporateDetail" runat="server" Title="Address" AutoScroll="true" EmptyText="No Available Data">
                                        <Store>
                                            <ext:Store ID="StoreAddressCorporateDetail" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model3" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailCorporate" runat="server"></ext:RowNumbererColumn>
                                                <ext:Column ID="ColAddress_typeCorporate" runat="server" DataIndex="Type_Address" Text="Address Type" Width="170" Flex="1"></ext:Column>
                                                <ext:Column ID="ColAddressCorporate" runat="server" DataIndex="Addres" Text="Address" Flex="1"></ext:Column>
                                                <ext:Column ID="ColCityCorporate" runat="server" DataIndex="Cty" Text="City" Flex="1"></ext:Column>
                                                <ext:Column ID="Colcountry_codeCorporate" runat="server" DataIndex="CountryCode" Text="Country" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnAddressCorporateDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GridCommandAddressCorporate">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar26" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <ext:DisplayField ID="TxtCorp_Email" runat="server" FieldLabel="Email Korporasi" AnchorHorizontal="80%" Hidden="true"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_url" runat="server" FieldLabel="Website Korporasi" AnchorHorizontal="80%" Hidden="true"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_incorporation_state" runat="server" FieldLabel="Province" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="CmbCorp_incorporation_country_code" runat="server" FieldLabel="Country" AnchorHorizontal="80%" />

                                    <%--<ext:GridPanel ID="GridPanelDirector_CorpBefore" runat="server" Title="Director Corp Before" AutoScroll="true" Hidden="true">
                                        <Store>
                                            <ext:Store ID="Store_Director_CorpBefore" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Model15" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="NO_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Roles" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn14" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column49" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column50" runat="server" DataIndex="Roles" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnDirectorBefore" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustDirectorBefore">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.NO_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>--%>
                                    <ext:GridPanel ID="GP_Corp_Director" runat="server" Title="Director Corp" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Director_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="Model_Director_Corp" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="NO_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Roles" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:Column ID="Column31" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="Column32" runat="server" DataIndex="Roles" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:CommandColumn ID="CommandColumnDirector" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdCustDirector">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.NO_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>

                                    <%--<ext:DisplayField ID="CmbCorp_Role" runat="server" FieldLabel="Role" AnchorHorizontal="80%" />--%>
                                    <ext:DisplayField ID="DateCorp_incorporation_date" runat="server" Flex="1"  FieldLabel="Date Of Stablished"></ext:DisplayField>
                                    <ext:DisplayField ID="chkCorp_business_closed" runat="server" FieldLabel="Is Closed?"></ext:DisplayField>
                                    <ext:DisplayField ID="DateCorp_date_business_closed" runat="server" Flex="1" FieldLabel="Closed Date" Hidden="true"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_tax_number" runat="server" FieldLabel="Tax Number" AnchorHorizontal="80%"></ext:DisplayField>
                                    <ext:DisplayField ID="TxtCorp_Comments" runat="server" FieldLabel="Notes" AnchorHorizontal="80%"></ext:DisplayField>
                                
                                    <%-- Add by Septian, Grid Panel Corporate New, goAML 5.0.1, 2023-03-02 %-->
                                    <%-- Corporate Email --%>
                                    <ext:GridPanel ID="GP_WIC_CORP_Email_New" runat="server" Title="Email" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_corp_email_new" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_corp_email_new" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Email_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="email_address" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rcn_cor_email_new" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_WIC_Previous_Name_ID" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                                <ext:CommandColumn ID="CC_CORP_Email_New" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_Email">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Email_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_corp_email_address_new" runat="server" DataIndex="email_address" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_corp_email_new" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Corporate Email --%>
                                    
                                    <%-- Corporate URL --%>
                                    <ext:GridPanel ID="GP_WIC_CORP_URL_New" runat="server" Title="Entity URL" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Customer_Entity_URL_New" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_entity_url_new" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_URL_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="URL" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_entity_url_corp_new" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                                <ext:CommandColumn ID="CC_CORP_URL_New" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_URL">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_URL_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_url_type_corp_new" runat="server" DataIndex="URL" Text="URL" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_entity_url_new" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Corporate URL --%>
                                    
                                    <%-- Corporate Entity Identification --%>
                                    <ext:GridPanel ID="GP_WIC_CORP_EntityIdentification_New" runat="server" Title="Entity Identification" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Customer_Entity_Identification_New" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_ent_identify_new" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Entity_Identifications_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TYPE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ISSUE_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="EXPIRY_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ISSUED_BY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ISSUE_COUNTRY" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_identify_new" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="CC_CORP_EntityIdentification_New" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_EntityIdentification">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Entity_Identifications_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_identify_type_new" runat="server" DataIndex="TYPE" Text="Entity Identifier Type" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_identify_number_new" runat="server" DataIndex="NUMBER" Text="Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_identify_issued_date_new" runat="server" DataIndex="ISSUE_DATE" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_identify_expiry_date_new" runat="server" DataIndex="EXPIRY_DATE" Text="Expiry Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_identify_issued_by_new" runat="server" DataIndex="ISSUED_BY" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_identify_issued_country_new" runat="server" DataIndex="ISSUE_COUNTRY" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_identify_comments_new" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_identify_new" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Corporate Entity Identication --%>
                                    
                                    <%-- Corporate Sanction --%>
                                    <ext:GridPanel ID="GP_WIC_CORP_Sanction_New" runat="server" Title="Sanction" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_corp_sanction_new" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_corp_sanction_new" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Sanction_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Provider" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Match_Criteria" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Link_To_Source" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Attributes" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_From" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_From_Date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_To" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_To_Date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_sanction_corp_new" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="CC_CORP_Sanction_New" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_Sanction">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_provider_corp_new" runat="server" DataIndex="Provider" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_list_name_corp_new" runat="server" DataIndex="Sanction_List_Name" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_match_criteria_corp_new" runat="server" DataIndex="Match_Criteria" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_link_to_source_corp_new" runat="server" DataIndex="Link_To_Source" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_attributes_corp_new" runat="server" DataIndex="Sanction_List_Attributes" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                                          <%--      <ext:Column ID="clm_saction_valid_from_corp_new" runat="server" DataIndex="Sanction_List_Date_Range_Valid_From" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_is_approx_from_date_corp_new" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_From_Date" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_valid_to_corp_new" runat="server" DataIndex="Sanction_List_Date_Range_Valid_To" Text="Valid To" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_sanction_is_approx_to_date_corp_new" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_To_Date" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_sanction_comments_corp_new" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_corp_sanction_new" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Corporate Sanction --%>
                                    
                                    <%-- Corporate Related Person --%>
                                    <ext:GridPanel ID="GP_WIC_CORP_RelatedPerson_New" Hidden="true" runat="server" Title="Related Person" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="StoreCorp_Related_person_new" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_corp_related_person_new" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Related_Person_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Person_Person_Relation" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Gender" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Title" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="First_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Middle_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Prefix" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Birth_Date" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Country_Of_Birth" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Mother_Name" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="_Alias" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Passport_Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Passport_Country" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ID_Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Nationality1" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Nationality2" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Nationality3" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Residence" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Occupation" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Deceased" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Date_Deceased" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Tax_Number" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Is_PEP" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Source_Of_Wealth" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="Is_Protected" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_related_person_corp_new" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="CC_CORP_RelatedPerson_New" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_RelatedPerson">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_rp_person_relation_corp_new" runat="server" DataIndex="Person_Person_Relation" Text="Person Person Relation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_comments_corp_new" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_gender_corp_new" runat="server" DataIndex="Gender" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_title_corp_new" runat="server" DataIndex="Title" Text="Title" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_first_name_corp_new" runat="server" DataIndex="First_Name" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_middle_name_corp_new" runat="server" DataIndex="Middle_Name" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_prefix_corp_new" runat="server" DataIndex="Prefix" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_last_name_corp_new" runat="server" DataIndex="Last_Name" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_birth_date_corp_new" runat="server" DataIndex="Birth_Date" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_birth_place_corp_new" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_country_of_birth_corp_new" runat="server" DataIndex="Country_Of_Birth" Text="Country of Birth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_mother_name_corp_new" runat="server" DataIndex="Mother_Name" Text="Mother name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_alias_corp_new" runat="server" DataIndex="_Alias" Text="Alias" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="clm_rp_foreign_name" runat="server" DataIndex="FULL_NAME_FRN" Text="Full Foreign Name" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_rp_ssn_corp_new" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_passport_number_corp_new" runat="server" DataIndex="Passport_Number" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_passport_country_corp_new" runat="server" DataIndex="Passport_Country" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_id_number_corp_new" runat="server" DataIndex="ID_Number" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_nationality1_corp_new" runat="server" DataIndex="Nationality1" Text="Nationality 1" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_nationality2_corp_new" runat="server" DataIndex="Nationality2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_nationality3_corp_new" runat="server" DataIndex="Nationality3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_residence_corp_new" runat="server" DataIndex="Residence" Text="Residence" MinWidth="130" Flex="1"></ext:Column>
                                                <%--<ext:Column ID="clm_rp_residence_since" runat="server" DataIndex="RESIDENCE_SINCE" Text="Residence Since" MinWidth="130" Flex="1"></ext:Column>--%>
                                                <ext:Column ID="clm_rp_occupation_corp_new" runat="server" DataIndex="Occupation" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_deceased_corp_new" runat="server" DataIndex="Deceased" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_date_deceased_corp_new" runat="server" DataIndex="Date_Deceased" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_tax_number_corp_new" runat="server" DataIndex="Tax_Number" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_is_pep_corp_new" runat="server" DataIndex="Is_PEP" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_source_of_wealth_corp_new" runat="server" DataIndex="Source_Of_Wealth" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_rp_is_protected_corp_new" runat="server" DataIndex="Is_Protected" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_corp_related_person_new" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Corporate Related Person--%>
                                    
                                    <%-- Corporate Related Entity --%>
                                    <ext:GridPanel ID="GP_WIC_CORP_RelatedEntity_New" runat="server" Title="Related Entities" AutoScroll="true">
                                        <Store>
                                            <ext:Store ID="Store_Related_Entities_New" runat="server" IsPagingStore="true" PageSize="8">
                                                <Model>
                                                    <ext:Model ID="mdl_related_entities_new" runat="server">
                                                        <%--to do--%>
                                                        <Fields>
                                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Related_Entities_ID" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="ENTITY_ENTITY_RELATION" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="COMMERCIAL_NAME" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_LEGAL_FORM" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BUSINESS" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_STATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_COUNTRY_CODE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="INCORPORATION_DATE" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="DATE_BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                                                            <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:RowNumbererColumn ID="rnc_related_entities_corp_new" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                                <ext:CommandColumn ID="CC_CORP_RelatedEntities_New" runat="server" Text="Action" Flex="1" MinWidth="200">
                                                    <Commands>
                                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="GrdCmdWIC_RelatedEntities">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Related_Entities_ID" Mode="Raw"></ext:Parameter>
                                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                            </ExtraParams>
                                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                            </Confirmation>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>
                                                <ext:Column ID="clm_re_entity_relation_corp_new" runat="server" DataIndex="ENTITY_ENTITY_RELATION" Text="Entity Entity Relation" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_comments_corp_new" runat="server" DataIndex="COMMENTS" Hidden="true" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_name_corp_new" runat="server" DataIndex="NAME" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_commercial_name_corp_new" runat="server" DataIndex="COMMERCIAL_NAME" Hidden="true" Text="Commercial Name" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_incorp_legal_form_corp_new" runat="server" DataIndex="INCORPORATION_LEGAL_FORM" Hidden="true" Text="Incorporation Legal Form" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="cml_re_incorp_number_corp_new" runat="server" DataIndex="INCORPORATION_NUMBER" Hidden="true" Text="Incorporation Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_business_corp_new" runat="server" DataIndex="BUSINESS" Hidden="true" Text="Business" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_incorp_state_corp_new" runat="server" DataIndex="INCORPORATION_STATE"  Hidden="true" Text="Incorporation State" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="cml_re_incorp_country_code_corp_new" runat="server" DataIndex="INCORPORATION_COUNTRY_CODE" Hidden="true" Text="Incorporation Country Code" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_incorp_date_corp_new" runat="server" DataIndex="INCORPORATION_DATE" Hidden="true" Text="Incorporation Date" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_business_closed_corp_new" runat="server" DataIndex="BUSINESS_CLOSED" Hidden="true" Text="Businesss Closed" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_date_business_closed_corp_new" runat="server" DataIndex="DATE_BUSINESS_CLOSED" Hidden="true" Text="Date Business Closed" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_tax_number_corp_new" runat="server" DataIndex="TAX_NUMBER" Hidden="true" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                                <ext:Column ID="clm_re_tax_reg_number_corp_new" runat="server" DataIndex="TAX_REG_NUMBER" Hidden="true" Text="Tax Reg Number" MinWidth="130" Flex="1"></ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <BottomBar>
                                            <ext:PagingToolbar ID="pt_related_entities_new" runat="server" HideRefresh="True" />
                                        </BottomBar>
                                    </ext:GridPanel>
                                    <%-- End of Corporate Related Entity --%>
                                    <%-- End Add --%>
                                </Items>
                            </ext:FormPanel>
                        </Items>
                    </ext:FormPanel>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnSave" runat="server" Text="Approve" Icon="DiskBlack">
                        <DirectEvents>
                            <Click OnEvent="BtnSave_Click">
                                <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnReject" runat="server" Text="Reject" Icon="Decline">
                        <DirectEvents>
                            <Click OnEvent="BtnReject_Click">
                                <EventMask ShowMask="true" Msg="Saving Reject Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnCancel" runat="server" Text="Cancel" Icon="PageBack">
                        <DirectEvents>
                            <Click OnEvent="BtnCancel_Click">
                                <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:TabPanel>
        </Items>
    </ext:Container>

    <ext:FormPanel ID="Panelconfirmation" runat="server" ClientIDMode="Static" Title="Confirmation" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
