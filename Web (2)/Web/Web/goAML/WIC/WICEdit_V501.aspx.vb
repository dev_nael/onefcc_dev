﻿Imports System.Data
Imports System.Data.SqlClient
Imports Elmah
Imports Ext
Imports Ext.Net.Utilities
Imports NawaBLL
Imports NawaDAL
Imports GoAMLBLL
Imports GoAMLBLL.Base.Constant
Imports GoAMLBLL.goAML
Imports GoAMLBLL.goAML.Services
Imports GoAMLDAL
Imports NPOI.HSSF.Record.Formula.Functions
Imports Panel = Ext.Net.Panel

Partial Class goAML_WICEdit_V501
    Inherits Parent
#Region "Session"
    Public objgoAML_WIC As WICBLL
    Dim con As New SqlConnection(SQLHelper.strConnectionString)
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("goAML_WICEdit_V501.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_WICEdit_V501.ObjModule") = value
        End Set
    End Property
    Public Property objModuleApproval As GoAMLDAL.ModuleApproval
        Get
            'daniel 13 jan 2021
            Return Session("goAML_WICEdit_V501.objModuleApproval")
            'end 13 jan 2021
        End Get
        Set(value As GoAMLDAL.ModuleApproval)
            'daniel 13 jan 2021
            Session("goAML_WICEdit_V501.objModuleApproval") = value
            'end 13 jan 2021
        End Set
    End Property
    Public Property ObjWIC As goAML_Ref_WIC
        Get
            Return Session("goAML_WICEdit_V501.ObjWIC")
        End Get
        Set(ByVal value As goAML_Ref_WIC)
            Session("goAML_WICEdit_V501.ObjWIC") = value
        End Set
    End Property
    Public Property ObjDirector As goAML_Ref_Walk_In_Customer_Director
        Get
            Return Session("goAML_WICEdit_V501.ObjDirector")
        End Get
        Set(ByVal value As goAML_Ref_Walk_In_Customer_Director)
            Session("goAML_WICEdit_V501.ObjDirector") = value
        End Set
    End Property
    Public Property ListDirectorDetail As List(Of goAML_Ref_Walk_In_Customer_Director)
        Get
            If Session("goAML_WICEdit_V501.ListDirectorDetail") Is Nothing Then
                Session("goAML_WICEdit_V501.ListDirectorDetail") = New List(Of goAML_Ref_Walk_In_Customer_Director)
            End If
            Return Session("goAML_WICEdit_V501.ListDirectorDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Walk_In_Customer_Director))
            Session("goAML_WICEdit_V501.ListDirectorDetail") = value
        End Set
    End Property
    Public Property ListDirectorDetailClass As List(Of WICDirectorDataBLL)
        Get
            If Session("goAML_WICEdit_V501.ListDirectorDetailClass") Is Nothing Then
                Session("goAML_WICEdit_V501.ListDirectorDetailClass") = New List(Of WICDirectorDataBLL)
            End If
            Return Session("goAML_WICEdit_V501.ListDirectorDetailClass")
        End Get
        Set(ByVal value As List(Of WICDirectorDataBLL))
            Session("goAML_WICEdit_V501.ListDirectorDetailClass") = value
        End Set
    End Property
    Public Property ListPhoneDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICEdit_V501.ListPhoneDetail") Is Nothing Then
                Session("goAML_WICEdit_V501.ListPhoneDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICEdit_V501.ListPhoneDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICEdit_V501.ListPhoneDetail") = value
        End Set
    End Property
    Public Property ListPhoneEmployerDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICEdit.ListPhoneEmployerDetail") Is Nothing Then
                Session("goAML_WICEdit.ListPhoneEmployerDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICEdit.ListPhoneEmployerDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICEdit.ListPhoneEmployerDetail") = value
        End Set
    End Property
    Public Property ListAddressEmployerDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICEdit.ListAddressEmployerDetail") Is Nothing Then
                Session("goAML_WICEdit.ListAddressEmployerDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICEdit.ListAddressEmployerDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICEdit.ListAddressEmployerDetail") = value
        End Set
    End Property
    Public Property ListIdentificationDetail As List(Of goAML_Person_Identification)
        Get
            If Session("goAML_WICEdit.ListIdentificationDetail") Is Nothing Then
                Session("goAML_WICEdit.ListIdentificationDetail") = New List(Of goAML_Person_Identification)
            End If
            Return Session("goAML_WICEdit.ListIdentificationDetail")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_WICEdit.ListIdentificationDetail") = value
        End Set
    End Property
    Public Property ObjDetailPhoneEmployer As goAML_Ref_Phone
        Get
            Return Session("goAML_WICEdit.ObjDetailPhoneEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICEdit.ObjDetailPhoneEmployer") = value
        End Set
    End Property
    Public Property ObjDetailIdentification As goAML_Person_Identification
        Get
            Return Session("goAML_WICEdit.ObjDetailIdentification")
        End Get
        Set(ByVal value As goAML_Person_Identification)
            Session("goAML_WICEdit.ObjDetailIdentification") = value
        End Set
    End Property
    Public Property ObjDetailAddressEmployer As goAML_Ref_Address
        Get
            Return Session("goAML_WICEdit.ObjDetailAddressEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICEdit.ObjDetailAddressEmployer") = value
        End Set
    End Property
    Public Property ListPhoneDetailDirector As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICEdit_V501.ListPhoneDetailDirector") Is Nothing Then
                Session("goAML_WICEdit_V501.ListPhoneDetailDirector") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICEdit_V501.ListPhoneDetailDirector")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICEdit_V501.ListPhoneDetailDirector") = value
        End Set
    End Property
    Public Property ListPhoneDetailDirectorEmployer As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICEdit_V501.ListPhoneDetailDirectorEmployer") Is Nothing Then
                Session("goAML_WICEdit_V501.ListPhoneDetailDirectorEmployer") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICEdit_V501.ListPhoneDetailDirectorEmployer")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICEdit_V501.ListPhoneDetailDirectorEmployer") = value
        End Set
    End Property
    Public Property ListAddressDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICEdit_V501.ListAddressDetail") Is Nothing Then
                Session("goAML_WICEdit_V501.ListAddressDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICEdit_V501.ListAddressDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICEdit_V501.ListAddressDetail") = value
        End Set
    End Property
    Public Property ListAddressDetailDirector As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICEdit_V501.ListAddressDetailDirector") Is Nothing Then
                Session("goAML_WICEdit_V501.ListAddressDetailDirector") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICEdit_V501.ListAddressDetailDirector")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICEdit_V501.ListAddressDetailDirector") = value
        End Set
    End Property
    Public Property ListAddressDetailDirectorEmployer As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICEdit_V501.ListAddressDetailDirectorEmployer") Is Nothing Then
                Session("goAML_WICEdit_V501.ListAddressDetailDirectorEmployer") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICEdit_V501.ListAddressDetailDirectorEmployer")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICEdit_V501.ListAddressDetailDirectorEmployer") = value
        End Set
    End Property
    Public Property ListIdentificationDirector As List(Of goAML_Person_Identification)
        Get
            If Session("goAML_WICEdit.ListDirectorIdentification") Is Nothing Then
                Session("goAML_WICEdit.ListDirectorIdentification") = New List(Of goAML_Person_Identification)
            End If
            Return Session("goAML_WICEdit.ListDirectorIdentification")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_WICEdit.ListDirectorIdentification") = value
        End Set
    End Property
    Public Property ObjDetailPhone As goAML_Ref_Phone
        Get
            Return Session("goAML_WICEdit_V501.ObjDetailPhone")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICEdit_V501.ObjDetailPhone") = value
        End Set
    End Property
    Public Property ObjDetailPhoneDirector As goAML_Ref_Phone
        Get
            Return Session("goAML_WICEdit_V501.ObjDetailPhoneDirector")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICEdit_V501.ObjDetailPhoneDirector") = value
        End Set
    End Property
    Public Property ObjDetailPhoneDirectorEmployer As goAML_Ref_Phone
        Get
            Return Session("goAML_WICEdit_V501.ObjDetailPhoneDirectorEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICEdit_V501.ObjDetailPhoneDirectorEmployer") = value
        End Set
    End Property
    Public Property ObjDetailAddress As goAML_Ref_Address
        Get
            Return Session("goAML_WICEdit_V501.ObjDetailAddress")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICEdit_V501.ObjDetailAddress") = value
        End Set
    End Property
    Public Property ObjDetailAddressDirector As goAML_Ref_Address
        Get
            Return Session("goAML_WICEdit_V501.ObjDetailAddressDirector")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICEdit_V501.ObjDetailAddressDirector") = value
        End Set
    End Property
    Public Property ObjDetailAddressDirectorEmployer As goAML_Ref_Address
        Get
            Return Session("goAML_WICEdit_V501.ObjDetailAddressDirectorEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICEdit_V501.ObjDetailAddressDirectorEmployer") = value
        End Set
    End Property
    Public Property ObjDetailIdentificationDirector As goAML_Person_Identification
        Get
            Return Session("goAML_WICEdit.ObjDetailIdentificationDirector")
        End Get
        Set(ByVal value As goAML_Person_Identification)
            Session("goAML_WICEdit.ObjDetailIdentificationDirector") = value
        End Set
    End Property
    Public Property DataRowTemp() As DataRow
        Get
            Return Session("goAML_WICEdit_V501.DataRowTemp")
        End Get
        Set(ByVal value As DataRow)
            Session("goAML_WICEdit_V501.DataRowTemp") = value
        End Set
    End Property
#End Region

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()

                Dim strmodule As String = Request.Params("ModuleID")
                Dim intmodule As Integer = Common.DecryptQueryString(strmodule, SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)

                If Not ObjModule Is Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Update) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If

                WIC_Panel.Title = "Non Customer" & " Edit"
                Panelconfirmation.Title = ObjModule.ModuleLabel & " Edit"

                Dim dataStr As String = Request.Params("ID")
                Dim dataID As String = Common.DecryptQueryString(dataStr, SystemParameterBLL.GetEncriptionKey)
                ObjWIC = objgoAML_WIC.GetWICByID(dataID)
                If ObjWIC Is Nothing Then
                    ObjWIC = objgoAML_WIC.GetWICByIDD(dataID)
                End If
                ListAddressDetail = objgoAML_WIC.GetWICByPKIDDetailAddress(dataID)
                ListPhoneDetail = objgoAML_WIC.GetWICByPKIDDetailPhone(dataID)
                ListAddressEmployerDetail = objgoAML_WIC.GetWICByPKIDDetailEmployerAddress(dataID)
                ListPhoneEmployerDetail = objgoAML_WIC.GetWICByPKIDDetailEmployerPhone(dataID)
                ListIdentificationDetail = objgoAML_WIC.GetWICByPKIDDetailIdentification(dataID)
                obj_ListSanction_Edit = GetWICByPKIDDetailSanction(dataID)
                obj_ListEmail_Edit = GetWICByPKIDDetailEmail(dataID)
                obj_ListPersonPEP_Edit = GetWICByPKIDDetailPersonPEP(dataID)
                obj_ListRelatedPerson_Edit = GetWICByPKIDDetailRelatedPerson(dataID)

                If ObjWIC.FK_Customer_Type_ID = 2 Then
                    'Daniel 2020 Des 30
                    'Dim Director As New WICDirectorDataBLL
                    'end 2020 Des 30
                    ListDirectorDetail = objgoAML_WIC.GetWICByPKIDDetailDirector(dataID)
                    For Each item In ListDirectorDetail
                        'Daniel 2020 Des 30
                        Dim Director As New WICDirectorDataBLL
                        'end 2020 Des 30
                        With Director
                            .ObjDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorDetail(item.NO_ID)
                            .ListDirectorAddress = objgoAML_WIC.GetWICByPKIDDetailDirectorAddress(item.NO_ID)
                            .ListDirectorAddressEmployer = objgoAML_WIC.GetWICByPKIDDetailDirectorAddressEmployer(item.NO_ID)
                            .ListDirectorPhone = objgoAML_WIC.GetWICByPKIDDetailDirectorPhone(item.NO_ID)
                            .ListDirectorPhoneEmployer = objgoAML_WIC.GetWICByPKIDDetailDirectorPhoneEmployer(item.NO_ID)
                            .ListDirectorIdentification = objgoAML_WIC.GetWICByPKIDDetailDirectorIdentification(item.NO_ID)
                            .ListDirectorEmail = goAML_Customer_Service.WICEmailService.GetByPkFk(item.NO_ID, RefDetail.WIC_DIRECTOR)
                            .ListDirectorSanction = goAML_Customer_Service.WICSanctionService.GetByPkFk(item.NO_ID, RefDetail.WIC_DIRECTOR)
                            .ListDirectorPEP = goAML_Customer_Service.WICPersonPEPService.GetByPkFk(item.NO_ID, RefDetail.WIC_DIRECTOR)
                        End With
                        ListDirectorDetailClass.Add(Director)
                    Next

                End If

                'LoadNegara()
                LoadBadanUsaha()
                LoadGender()
                LoadParty()
                LoadKategoriKontak()
                LoadJenisAlatKomunikasi()
                LoadTypeIdentitas()
                LoadData()
                LoadPopup()
                LoadColumn()
                BindData()

                Dim strUnikKey As String = NawaBLL.Common.DecryptQueryString(dataStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                objModuleApproval = GoAMLBLL.WICBLL.getDataApproval(strUnikKey, ObjModule.ModuleName)
                If objModuleApproval IsNot Nothing Then
                    btnSave.Hidden = True
                    Throw New ApplicationException("This data is already in approval.")
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadColumn()

        ColumnActionLocation(GridPanel1, CommandColumn1)
        ColumnActionLocation(GridPanel2, CommandColumn2)
        ColumnActionLocation(GridPanel3, CommandColumn3)
        ColumnActionLocation(GridPanel4, CommandColumn4)
        ColumnActionLocation(GridPanel5, CommandColumn5)

        ColumnActionLocation(GP_DirectorPhone, CommandColumn11)
        ColumnActionLocation(GP_DirectorAddress, CommandColumn12)
        ColumnActionLocation(GP_DirectorEmpPhone, CommandColumn9)
        ColumnActionLocation(GP_DirectorEmpAddress, CommandColumn10)
        ColumnActionLocation(GP_DirectorIdentification, CommandColumn6)

        ColumnActionLocation(GridPanelPhoneIndividuDetail, CommandColumnPhoneIndividuDetail)
        ColumnActionLocation(GridPanelAddressIndividuDetail, CommandColumnAddressIndividuDetail)
        ColumnActionLocation(GridPanelPhoneIndividualEmployerDetail, CommandColumnPhoneIndividualEmployerDetail)
        ColumnActionLocation(GridPanelAddressIndividualEmployerDetail, CommandColumnAddressIndividualEmployerDetail)
        ColumnActionLocation(GridPanelIdentificationIndividualDetail, CommandColumnIdentificationIndividualDetail)

        ColumnActionLocation(GridPanelPhoneCorporateDetail, CommandColumnPhoneCorporateDetail)
        ColumnActionLocation(GridPanelAddressCorporateDetail, CommandColumnAddressCorporateDetail)
        ColumnActionLocation(GP_Corp_Director, CommandColumnDirector)

        ColumnActionLocation(GridPanelSanction, CommandColumnSanction)
        ColumnActionLocation(GridPanelEmail, CommandColumnActionEmail)
        ColumnActionLocation(GridPanelPersonPEP, CommandColumnPersonPEP)
        ColumnActionLocation(GridPanelRelatedPerson, CommandColumnRelatedPersonAction)

    End Sub

    Private Sub LoadPopup()
        WindowDetailAddress.Maximizable = True
        WindowDetailPhone.Maximizable = True
        WindowDetailDirector.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True
        WindowDetailDirectorIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailIdentification.Maximizable = True

        ' START: 2023-10-24, Nael
        WindowDetail_EntityIdentification.Maximizable = True
        Window_Email.Maximizable = True
        WindowDetail_URL.Maximizable = True
        Window_Sanction.Maximizable = True
        WindowDetail_RelatedEntity.Maximizable = True
        ' END: 2023-10-24, Nael

    End Sub
    Protected Sub National_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Nama_Negara", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Communication_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Alat_Komunikasi", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Contact_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and Active=1"
            Else
                strfilter += "Active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Kategori_Kontak", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Incorporation_legal_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Bentuk_Badan_Usaha", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Identification_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goaml_ref_jenis_dokumen_identitas", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Gender_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Kelamin", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Director_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_ODM_Reporting_Person", "PK_ODM_Reporting_Person_ID, Last_Name", strfilter, "Last_Name", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Role_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Peran_orang_dalam_Korporasi", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Private Sub LoadGender()
        cmb_Director_Gender.PageSize = SystemParameterBLL.GetPageSize
        StoreGender.Reload()
    End Sub
    Private Sub LoadKategoriKontak()
        Cmbtph_contact_type.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignContact_Type.Reload()
        'Agam 20102020 
        Cmbtph_contact_typeEmployer.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignContact_TypeEmployer.Reload()
        CmbAddress_typeEmployer.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignAddress_TypeEmployer.Reload()
        'Cmbcountry_codeEmployer.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignCountry_CodeAddressEmployer.Reload()

        ' end agam
        CmbAddress_type.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignAddress_Type.Reload()
        Director_Emp_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreEmpDirPContactType.Reload()
        Director_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreDirPContactType.Reload()
        Director_cmb_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        StoreDirKategoryADdressAddress_Type.Reload()
        Director_cmb_emp_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        StoreEmpDirKategoryADdressAddress_Type.Reload()
        'CmbCountryIdentification.PageSize = SystemParameterBLL.GetPageSize
        'StoreCountryCodeIdentification.Reload()
    End Sub
    Private Sub LoadTypeIdentitas()
        CmbTypeIdentification.PageSize = SystemParameterBLL.GetPageSize
        StoreTypeIdentification.Reload()
        CmbTypeIdentificationDirector.PageSize = SystemParameterBLL.GetPageSize
        StoreTypeIdentificationDirector.Reload()
    End Sub
    Private Sub LoadJenisAlatKomunikasi()
        Cmbtph_communication_type.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignCommunication_Type.Reload()
        Director_Emp_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreEmpDirComunicationType.Reload()
        Director_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreDirComunicationType.Reload()
        Cmbtph_communication_typeEmployer.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignCommunication_TypeEmployer.Reload()
    End Sub

    Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
        objgoAML_WIC = New WICBLL(WIC_Panel)
    End Sub

#End Region

#Region "Method"
    Private Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
                gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadParty()
        cmb_peran.PageSize = SystemParameterBLL.GetPageSize
        StorePeran.Reload()
    End Sub
    Private Sub LoadBadanUsaha()
        CmbCorp_Incorporation_legal_form.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignIncorporation_legal.Reload()
    End Sub

    Private Sub LoadNegara()
        'agam 22102020
        'TxtINDV_Passport_country.PageSize = SystemParameterBLL.GetPageSize
        'StoreIndividuPassportCountry.Reload()
        'CmbINDV_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignNational1.Reload()
        'CmbINDV_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignNational2.Reload()
        'CmbINDV_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignNational3.Reload()
        'CmbINDV_residence.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignResisdance.Reload()
        'cmb_Director_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirNational1.Reload()
        'cmb_Director_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirNational2.Reload()
        'cmb_Director_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirNational3.Reload()
        'cmb_Director_Residence.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirReisdence.Reload()

        'CmbCountryIdentificationDirector.PageSize = SystemParameterBLL.GetPageSize
        'StoreCountryCodeIdentificationDirector.Reload()
        'CmbCorp_incorporation_country_code.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignCountry_Code.Reload()
        'Cmbcountry_code.PageSize = SystemParameterBLL.GetPageSize
        'StoreFromForeignCountry_CodeAddress.Reload()

        'Director_cmb_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'StoreDirFromForeignCountry_CodeAddress.Reload()
        'Director_cmb_emp_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'StoreEmpDirFromForeignCountry_CodeAddress.Reload()


    End Sub
    Private Sub LoadData()
        Try
            Dim DirectorClass As New WICDirectorDataBLL
            If Not ObjWIC Is Nothing Then
                With ObjWIC
                    PKWIC.Text = .PK_Customer_ID
                    WICNo.Text = .WIC_No
                    If .isUpdateFromDataSource IsNot Nothing Then
                        CheckboxIsUpdateFromDataSource.Checked = .isUpdateFromDataSource
                    Else
                        CheckboxIsUpdateFromDataSource.Checked = False
                    End If
                    'If .Is_RealWIC IsNot Nothing Then
                    '    CheckboxIsRealWIC.Checked = .Is_RealWIC
                    'Else
                    '    CheckboxIsRealWIC.Checked = False
                    'End If
                    If .FK_Customer_Type_ID = 1 Then
                        TxtINDV_Title.Text = .INDV_Title
                        TxtINDV_last_name.Text = .INDV_Last_Name
                        If .INDV_Gender IsNot Nothing Then
                            CmbINDV_Gender.SetValueAndFireSelect(.INDV_Gender)
                        End If
                        If .INDV_BirthDate IsNot Nothing Then
                            DateINDV_Birthdate.Value = .INDV_BirthDate
                        End If
                        TxtINDV_Birth_place.Text = .INDV_Birth_Place
                        TxtINDV_Mothers_name.Text = .INDV_Mothers_Name
                        TxtINDV_Alias.Text = .INDV_Alias
                        TxtINDV_SSN.Text = .INDV_SSN
                        TxtINDV_Passport_number.Text = .INDV_Passport_Number
                        'agam 20102020
                        ' TxtINDV_Passport_country.Text = .INDV_Passport_Country
                        If Not IsNothing(.INDV_Passport_Country) Then
                            DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .INDV_Passport_Country & "'")
                            If DataRowTemp IsNot Nothing Then
                                TxtINDV_Passport_country.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            End If
                        End If
                        TxtINDV_ID_Number.Text = .INDV_ID_Number
                        If Not IsNothing(.INDV_Nationality1) Then
                            DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .INDV_Nationality1 & "'")
                            If DataRowTemp IsNot Nothing Then
                                CmbINDV_Nationality1.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Nationality2) Then
                            DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .INDV_Nationality2 & "'")
                            If DataRowTemp IsNot Nothing Then
                                CmbINDV_Nationality2.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Nationality3) Then
                            DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .INDV_Nationality3 & "'")
                            If DataRowTemp IsNot Nothing Then
                                CmbINDV_Nationality3.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Residence) Then
                            DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .INDV_Residence & "'")
                            If DataRowTemp IsNot Nothing Then
                                CmbINDV_residence.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            End If
                        End If
                        'CmbINDV_Nationality1.SetValueAndFireSelect(.INDV_Nationality1)
                        'CmbINDV_Nationality2.SetValue(.INDV_Nationality2)
                        'CmbINDV_Nationality3.SetValue(.INDV_Nationality3)
                        'CmbINDV_residence.SetValue(.INDV_Residence)

                        'TxtINDV_Email.Focus()
                        'TxtINDV_Email.Text = .INDV_Email
                        'TxtINDV_Email2.Text = .INDV_Email2
                        'TxtINDV_Email3.Text = .INDV_Email3
                        'TxtINDV_Email4.Text = .INDV_Email4
                        'TxtINDV_Email5.Text = .INDV_Email5
                        TxtINDV_Occupation.Text = .INDV_Occupation
                        TxtINDV_employer_name.Text = .INDV_Employer_Name
                        TxtINDV_Source_Of_Wealth.Text = .INDV_SumberDana
                        TxtINDV_Tax_Number.Text = .INDV_Tax_Number
                        CbINDV_Tax_Reg_Number.Checked = .INDV_Tax_Reg_Number
                        If .INDV_Deceased IsNot Nothing Then
                            CbINDV_Deceased.Checked = .INDV_Deceased
                        Else
                            CbINDV_Deceased.Checked = False
                        End If

                        If CbINDV_Deceased.Checked Then
                            DateINDV_Deceased_Date.Hidden = False
                            If .INDV_Deceased_Date IsNot Nothing Then
                                DateINDV_Deceased_Date.Text = .INDV_Deceased_Date
                            End If
                        End If
                        TxtINDV_Comments.Text = .INDV_Comment
                        FKCustomer.Text = .FK_Customer_Type_ID
                    Else
                        TxtCorp_Name.Text = .Corp_Name
                        TxtCorp_Commercial_name.Text = .Corp_Commercial_Name
                        CmbCorp_Incorporation_legal_form.SetValueAndFireSelect(.Corp_Incorporation_Legal_Form)
                        TxtCorp_Incorporation_number.Text = .Corp_Incorporation_Number
                        TxtCorp_Business.Text = .Corp_Business
                        'TxtCorp_Email.Text = .Corp_Email
                        'TxtCorp_url.Text = .Corp_Url
                        TxtCorp_incorporation_state.Text = .Corp_Incorporation_State
                        'CmbCorp_incorporation_country_code.SetValueAndFireSelect(.Corp_Incorporation_Country_Code)
                        If Not IsNothing(.Corp_Incorporation_Country_Code) Then
                            DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Corp_Incorporation_Country_Code & "'")
                            If DataRowTemp IsNot Nothing Then
                                CmbCorp_incorporation_country_code.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                            End If
                        End If
                        If .Corp_Incorporation_Date IsNot Nothing Then
                            DateCorp_incorporation_date.Text = .Corp_Incorporation_Date
                        End If
                        If .Corp_Business_Closed IsNot Nothing Then
                            chkCorp_business_closed.Checked = .Corp_Business_Closed
                        End If
                        If chkCorp_business_closed.Checked Then
                            If .Corp_Date_Business_Closed IsNot Nothing Then
                                DateCorp_date_business_closed.Text = .Corp_Date_Business_Closed
                            End If
                            DateCorp_date_business_closed.Hidden = False
                        End If
                        TxtCorp_tax_number.Text = .Corp_Tax_Number
                        TxtCorp_Comments.Text = .Corp_Comments
                        FKCustomer.Text = .FK_Customer_Type_ID
                    End If
                End With
                If ObjWIC.FK_Customer_Type_ID = 1 Then
                    BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
                    BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
                    BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)
                    BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)
                    BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
                    WIC_Individu.Show()
                    WIC_Corporate.Hide()
                Else
                    BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
                    BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
                    BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
                    WIC_Individu.Hide()
                    WIC_Corporate.Show()
                End If

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub ClearSession()
        ObjWIC = Nothing
        ObjDirector = Nothing
        'ObjModule = Nothing
        ListDirectorDetailClass = New List(Of WICDirectorDataBLL)
        ListAddressDetail = New List(Of goAML_Ref_Address)
        ListAddressEmployerDetail = New List(Of goAML_Ref_Address)
        ListAddressDetailDirector = New List(Of goAML_Ref_Address)
        ListAddressDetailDirectorEmployer = New List(Of goAML_Ref_Address)
        ListPhoneDetail = New List(Of goAML_Ref_Phone)
        ListPhoneEmployerDetail = New List(Of goAML_Ref_Phone)
        ListPhoneDetailDirector = New List(Of goAML_Ref_Phone)
        ListPhoneDetailDirectorEmployer = New List(Of goAML_Ref_Phone)
        ListIdentificationDetail = New List(Of goAML_Person_Identification)
        ListIdentificationDirector = New List(Of goAML_Person_Identification)

        ObjDetailAddress = Nothing
        ObjDetailAddressEmployer = Nothing
        ObjDetailAddressDirector = Nothing
        ObjDetailAddressDirectorEmployer = Nothing
        ObjDetailPhone = Nothing
        ObjDetailPhoneEmployer = Nothing
        ObjDetailPhoneDirector = Nothing
        ObjDetailPhoneDirectorEmployer = Nothing
        ObjDetailIdentification = Nothing
        ObjDetailIdentificationDirector = Nothing

        obj_Sanction_Edit = Nothing
        obj_ListSanction_Edit = New List(Of WICDataBLL.goAML_Ref_WIC_Sanction)
        obj_Email_Edit = Nothing
        obj_ListEmail_Edit = New List(Of WICDataBLL.goAML_Ref_WIC_Email)
        obj_PersonPEP_Edit = Nothing
        obj_ListPersonPEP_Edit = New List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP)
        obj_RelatedPerson_Edit = Nothing
        obj_ListRelatedPerson_Edit = New List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)

        objListgoAML_Ref_EntityUrl = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
        objListgoAML_vw_EntityUrl = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
        objListgoAML_Ref_EntityIdentification = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        objListgoAML_vw_EntityIdentification = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        objListgoAML_Ref_RelatedEntity = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        objListgoAML_vw_RelatedEntity = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
    End Sub

    Private Sub clearinputDirector()
        Director_cb_phone_Contact_Type.ClearValue()
        Director_cb_phone_Communication_Type.ClearValue()
        Director_txt_phone_Country_prefix.Clear()
        Director_txt_phone_number.Clear()
        Director_txt_phone_extension.Clear()
        Director_txt_phone_comments.Clear()

        Director_Emp_cb_phone_Contact_Type.ClearValue()
        Director_Emp_cb_phone_Communication_Type.ClearValue()
        Director_Emp_txt_phone_Country_prefix.Clear()
        Director_Emp_txt_phone_number.Clear()
        Director_Emp_txt_phone_extension.Clear()
        Director_Emp_txt_phone_comments.Clear()

        Director_cmb_kategoriaddress.ClearValue()
        Director_Address.Clear()
        Director_Town.Clear()
        Director_City.Clear()
        Director_Zip.Clear()
        Director_cmb_kodenegara.SetTextValue("")
        Director_State.Clear()
        Director_Comment_Address.Clear()

        Director_cmb_emp_kategoriaddress.ClearValue()
        Director_emp_Address.Clear()
        Director_emp_Town.Clear()
        Director_emp_City.Clear()
        Director_emp_Zip.Clear()
        Director_cmb_emp_kodenegara.SetTextValue("")
        Director_emp_State.Clear()
        Director_emp_Comment_Address.Clear()

        CmbTypeIdentificationDirector.ClearValue()
        TxtNumberDirector.Clear()
        TxtIssueDateDirector.Clear()
        TxtExpiryDateDirector.Clear()
        TxtIssuedByDirector.Clear()
        CmbCountryIdentificationDirector.SetTextValue("")
        TxtCommentIdentificationDirector.Clear()

    End Sub
    Private Sub ClearInput()
        Cmbtph_contact_type.ClearValue()
        Cmbtph_communication_type.ClearValue()
        Txttph_country_prefix.Clear()
        Txttph_number.Clear()
        Txttph_extension.Clear()
        TxtcommentsPhone.Clear()

        CmbAddress_type.ClearValue()
        TxtAddress.Clear()
        TxtTown.Clear()
        TxtCity.Clear()
        TxtZip.Clear()
        Cmbcountry_code.SetTextValue("")
        TxtState.Clear()
        TxtcommentsAddress.Clear()

        'ListAddressDetailDirector.Clear()
        'ListAddressDetailDirectorEmployer.Clear()
        'ListPhoneDetailDirector.Clear()
        'ListPhoneDetailDirectorEmployer.Clear()
        'ListIdentificationDirector.Clear()

        Cmbtph_contact_typeEmployer.ClearValue()
        Cmbtph_communication_typeEmployer.ClearValue()
        Txttph_country_prefixEmployer.Clear()
        Txttph_numberEmployer.Clear()
        Txttph_extensionEmployer.Clear()
        TxtcommentsPhoneEmployer.Clear()

        CmbAddress_typeEmployer.ClearValue()
        TxtAddressEmployer.Clear()
        TxtTownEmployer.Clear()
        TxtCityEmployer.Clear()
        TxtZipEmployer.Clear()
        Cmbcountry_codeEmployer.SetTextValue("")
        TxtStateEmployer.Clear()

        CmbTypeIdentification.ClearValue()
        TxtNumber.Clear()
        TxtIssueDate.Clear()
        TxtExpiryDate.Clear()
        TxtIssuedBy.Clear()
        CmbCountryIdentification.SetTextValue("")
        TxtCommentIdentification.Clear()

        Hidden2.Value = ""
        cmb_peran.ClearValue()
        txt_Director_Title.Clear()
        txt_Director_Last_Name.Clear()
        cmb_Director_Gender.ClearValue()
        txt_Director_BirthDate.Clear()
        txt_Director_Birth_Place.Clear()
        txt_Director_Mothers_Name.Clear()
        txt_Director_Alias.Clear()
        txt_Director_ID_Number.Clear()
        txt_Director_Passport_Number.Clear()
        'txt_Director_Passport_Country.Clear()
        txt_Director_Passport_Country.SetTextValue("")
        txt_Director_SSN.Clear()
        cmb_Director_Nationality1.SetTextValue("")
        cmb_Director_Nationality2.SetTextValue("")
        cmb_Director_Nationality3.SetTextValue("")
        cmb_Director_Residence.SetTextValue("")
        'cmb_Director_Nationality1.ClearValue()
        'cmb_Director_Nationality2.ClearValue()
        'cmb_Director_Nationality3.ClearValue()
        'cmb_Director_Residence.ClearValue()
        'txt_Director_Email.Clear()
        'txt_Director_Email2.Clear()
        'txt_Director_Email3.Clear()
        'txt_Director_Email4.Clear()
        'txt_Director_Email5.Clear()
        cb_Director_Deceased.Clear()
        txt_Director_Deceased_Date.Clear()
        cb_Director_Tax_Reg_Number.Clear()
        txt_Director_Tax_Number.Clear()
        txt_Director_Source_of_Wealth.Clear()
        txt_Director_Occupation.Clear()
        txt_Director_Comments.Clear()
        txt_Director_employer_name.Clear()

        ' START: 2023-10-20, Nael
        'If ObjDirector Is Nothing Then
        ListAddressDetailDirector.Clear()
        ListAddressDetailDirectorEmployer.Clear()
        ListPhoneDetailDirector.Clear()
        ListPhoneDetailDirectorEmployer.Clear()
        ListIdentificationDirector.Clear()
        'Else
        '    ListAddressDetailDirector = ListAddressDetailDirector.Where(Function(x) x.FK_To_Table_ID = ObjDirector.NO_ID).ToList()
        '    ListAddressDetailDirectorEmployer = ListAddressDetailDirectorEmployer.Where(Function(x) x.FK_To_Table_ID = ObjDirector.NO_ID).ToList()
        '    ListPhoneDetailDirector = ListPhoneDetailDirector.Where(Function(x) x.FK_for_Table_ID = ObjDirector.NO_ID).ToList()
        '    ListPhoneDetailDirectorEmployer = ListPhoneDetailDirectorEmployer.Where(Function(x) x.FK_for_Table_ID = ObjDirector.NO_ID).ToList()
        '    ListIdentificationDirector = ListIdentificationDirector.Where(Function(x) x.FK_REF_DETAIL_OF = ObjDirector.NO_ID).ToList()
        'End If

        ObjDirector = Nothing
        ' END: 2023-10-20, Nael

        'daniel 21 jan 2021
        BindDetailIdentification(StoreIdentificationDirectorDetail, New List(Of goAML_Person_Identification))
        'end 21 jan 2021
        BindDetailAddress(Store_DirectorAddress, New List(Of goAML_Ref_Address))
        BindDetailAddress(Store_Director_Employer_Address, New List(Of goAML_Ref_Address))

        BindDetailPhone(Store_DirectorPhone, New List(Of goAML_Ref_Phone))
        BindDetailPhone(Store_Director_Employer_Phone, New List(Of goAML_Ref_Phone))



    End Sub

#End Region

#Region "Direct Event"
    Protected Sub checkBoxcb_Director_Deceased_click(sender As Object, e As DirectEventArgs)
        If cb_Director_Deceased.Checked Then
            txt_Director_Deceased_Date.Hidden = False
        Else
            txt_Director_Deceased_Date.Hidden = True
        End If
    End Sub
    Protected Sub checkBoxcb_RelatedPerson_Deceased_click(sender As Object, e As DirectEventArgs)
        If chk_RelatedPersonDeceased.Checked Then
            dt_TxtRelatedPersonDate_Deceased.Hidden = False
        Else
            dt_TxtRelatedPersonDate_Deceased.Hidden = True
        End If
    End Sub
    Protected Sub checkBoxcb_INDV_Deceased_click(sender As Object, e As DirectEventArgs)
        If CbINDV_Deceased.Checked Then
            DateINDV_Deceased_Date.Hidden = False
        Else
            DateINDV_Deceased_Date.Hidden = True
        End If
    End Sub
    Protected Sub chkCorp_business_closed_Event(sender As Object, e As DirectEventArgs)
        If chkCorp_business_closed.Checked Then
            DateCorp_date_business_closed.Hidden = False
        Else
            DateCorp_date_business_closed.Hidden = True
        End If
    End Sub
    Protected Sub chkRE_business_closed_Event(sender As Object, e As DirectEventArgs)
        If cbx_re_business_closed.Checked Then
            df_re_date_business_closed.Hidden = False
        Else
            df_re_date_business_closed.Hidden = True
        End If
    End Sub
    Protected Sub btnAddCustomerDirector_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            GridEmail_Director.IsViewMode = False
            GridSanction_Director.IsViewMode = False
            GridPEP_Director.IsViewMode = False
            GridEmail_Director.LoadData(New List(Of DataModel.goAML_Ref_Customer_Email))
            GridSanction_Director.LoadData(New List(Of DataModel.goAML_Ref_Customer_Sanction))
            GridPEP_Director.LoadData(New List(Of DataModel.goAML_Ref_Customer_PEP))

            ClearInput()
            ' 2023-10-21, Nael
            Dim objRandom As New Random
            ObjDirector = New goAML_Ref_Walk_In_Customer_Director With {
                .NO_ID = -objRandom.Next
            }

            WindowDetailDirector.Show()
            FP_Director.Show()
            FormPanelDirectorDetail.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewPhoneIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()

            WindowDetailAddress.Hide()
            WindowDetailPhone.Title = "Add Phone"
            WindowDetailPhone.Show()

            FormPanelAddressDetail.Hide()
            FormPanelPhoneDetail.Show()
            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewPhoneEmployerIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            'Daniel 2020 Des 28
            If ListPhoneEmployerDetail.Count < 1 Then
                'End 2020 Des 28
                ClearInput()

                WindowDetailPhone.Title = "Add Phone Employer"
                WindowDetailPhone.Show()

                FormPanelPhoneDetail.Hide()
                PanelDetailPhone.Hide()
                PanelDetailPhoneEmployer.Hide()
                FormPanelPhoneEmployerDetail.Show()
                'Daniel 2020 Des 28
            Else
                Throw New Exception("can only enter one Workplace Phone Information")
            End If
            'end 2020 Des 28
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()

            Dim objRandom As New Random
            ' 2023-10-23, Nael
            If ObjDirector IsNot Nothing Then
                ObjDetailPhoneDirector = New goAML_Ref_Phone With {
                    .PK_goAML_Ref_Phone = -objRandom.Next,
                    .FK_for_Table_ID = ObjDirector.NO_ID
                }
            End If

            WindowDetailDirectorPhone.Title = "Add Director Phone"
            WindowDetailDirectorPhone.Show()
            FormPanelDirectorPhone.Show()
            FormPanelEmpDirectorTaskDetail.Hide()
            PanelPhoneDirector.Hide()
            FP_Director.Show()
            PanelPhoneDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorEmployerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()

            Dim objRandom As New Random
            ObjDetailPhoneDirectorEmployer = New goAML_Ref_Phone With {
                .PK_goAML_Ref_Phone = -objRandom.Next
            }

            WindowDetailDirectorPhone.Title = "Add Director Phone Employer"
            WindowDetailDirectorPhone.Show()
            FormPanelPhoneDetail.Hide()
            FormPanelEmpDirectorTaskDetail.Show()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()
            FP_Director.Show()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()

            Dim objRandom As New Random
            ObjDetailAddressDirector = New goAML_Ref_Address With {
                .PK_Customer_Address_ID = -objRandom.Next
            }

            WindowDetailDirectorAddress.Title = "Add Director Address"
            WindowDetailDirectorAddress.Show()
            FP_Address_Director.Show()
            FP_Address_Emp_Director.Hide()
            PanelAddressDirector.Hide()
            FP_Director.Show()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorIdentification_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()

            Dim objRandom As New Random
            ObjDetailIdentificationDirector = New goAML_Person_Identification With {
                .PK_Person_Identification_ID = -objRandom.Next
            }

            WindowDetailDirectorIdentification.Title = "Add Identitas"
            WindowDetailDirectorIdentification.Show()
            FormPanelDirectorIdentification.Show()
            PanelDetailDirectorIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddIdentification_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            WindowDetailIdentification.Title = "Add Identitas"
            WindowDetailIdentification.Show()
            FormPanelIdentification.Show()
            PanelDetailIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnAddDirectorEmployerAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            clearinputDirector()

            Dim objRandom As New Random
            ObjDetailAddressDirectorEmployer = New goAML_Ref_Address With {
                .PK_Customer_Address_ID = -objRandom.Next
            }

            WindowDetailDirectorAddress.Title = "Add Director Address Emplpoyer"
            WindowDetailDirectorAddress.Show()
            FP_Address_Director.Hide()
            FP_Address_Emp_Director.Show()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub BtnAddNewAddressIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            FormPanelAddressDetail.Hidden = False
            FormPanelPhoneDetail.Hidden = True
            WindowDetailAddress.Title = "Add Address"
            WindowDetailPhone.Hidden = True
            WindowDetailAddress.Hidden = False
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewAddressEmployerIndividu_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            'Daniel 2020 Des 28
            If ListAddressEmployerDetail.Count < 1 Then
                'end 2020 Des 28
                ClearInput()
                FormPanelAddressDetail.Hidden = True
                PanelDetailAddress.Hidden = True
                PanelDetailAddressEmployer.Hidden = True
                FormPanelAddressDetailEmployer.Hidden = False
                WindowDetailAddress.Title = "Add Address Employer"
                WindowDetailAddress.Hidden = False
                'Daniel 2020 Des 28
            Else
                Throw New Exception("can only enter one Workplace Address Information")
            End If
            'end 2020 Des 28
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewPhoneCorporate_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            FormPanelAddressDetail.Hidden = True
            FormPanelPhoneDetail.Hidden = False
            WindowDetailPhone.Title = "Add Phone Corporate"
            WindowDetailPhone.Hidden = False
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnAddNewAddressCorporate_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            ClearInput()
            FormPanelAddressDetail.Hidden = False
            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
            WindowDetailAddress.Title = "Add Address Corporate"
            WindowDetailAddress.Hidden = False
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindDetailIdentification(store As Store, list As List(Of goAML_Person_Identification))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Tipe", GetType(String)))
        objtable.Columns.Add(New DataColumn("No", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssueDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("ExpiryDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssuedCountry", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Tipe") = GlobalReportFunctionBLL.getjenisDokumenByKode(item("Type"))
                item("No") = item("Number")
                item("IssueDate") = item("Issue_Date")
                item("ExpiryDate") = item("Expiry_Date")
                item("IssuedCountry") = GlobalReportFunctionBLL.getCountryByCode(item("Issued_Country"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Private Sub BindDetailPhone(store As Store, list As List(Of goAML_Ref_Phone))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
                item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailAddress(store As Store, list As List(Of goAML_Ref_Address))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Type_Address", GetType(String)))
        objtable.Columns.Add(New DataColumn("Addres", GetType(String)))
        objtable.Columns.Add(New DataColumn("Cty", GetType(String)))
        objtable.Columns.Add(New DataColumn("CountryCode", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Type_Address") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type"))
                item("Addres") = item("Address")
                item("Cty") = item("City")
                item("CountryCode") = GlobalReportFunctionBLL.getCountryByCode(item("Country_Code"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailDirector(store As Store, list As List(Of goAML_Ref_Walk_In_Customer_Director))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("LastName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Roles", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("LastName") = item("Last_Name")
                item("Roles") = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(item("Role"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Protected Sub BtnSaveDetailIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailIdentification Is Nothing Then
                SaveAddDetailIdentification()
            Else

                Dim status As Boolean = True
                Dim ObjDetailIdentificationNew As New goAML_Person_Identification

                With ObjDetailIdentificationNew
                    .Type = CmbTypeIdentification.Text
                    .Number = TxtNumber.Text
                    If TxtIssueDate.Text <> DateTime.MinValue Then
                        .Issue_Date = TxtIssueDate.Text
                    Else
                        .Issue_Date = Nothing
                    End If
                    If TxtExpiryDate.Text <> DateTime.MinValue Then
                        .Expiry_Date = TxtExpiryDate.Text
                    Else
                        .Expiry_Date = Nothing
                    End If
                    .Issued_By = TxtIssuedBy.Text
                    .Issued_Country = CmbCountryIdentification.TextValue
                    .Identification_Comment = TxtCommentIdentification.Text
                End With

                With ObjDetailIdentification
                    If .Type <> ObjDetailIdentificationNew.Type Then
                        status = False
                    End If
                    If .Number <> ObjDetailIdentificationNew.Number Then
                        status = False
                    End If
                    If .Issue_Date <> ObjDetailIdentificationNew.Issue_Date Then
                        status = False
                    End If
                    If .Expiry_Date <> ObjDetailIdentificationNew.Expiry_Date Then
                        status = False
                    End If
                    If .Issued_By <> ObjDetailIdentificationNew.Issued_By Then
                        status = False
                    End If
                    If .Issued_Country <> ObjDetailIdentificationNew.Issued_Country Then
                        status = False
                    End If
                    If .Identification_Comment <> ObjDetailIdentificationNew.Identification_Comment Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailIdentification()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailIdentificationDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'Daniel 2021 Jan 6
            'If ObjDetailIdentification Is Nothing Then
            If ObjDetailIdentificationDirector IsNot Nothing Then
                'end 2021 Jan 6
                Dim alreadySaved = ListIdentificationDirector.Exists(Function(x) x.PK_Person_Identification_ID = ObjDetailIdentificationDirector.PK_Person_Identification_ID)
                If Not alreadySaved Then
                    SaveAddDetailIdentificationDirector()
                Else
                    Dim status As Boolean = True
                    Dim ObjDetailIdentificationNew As New goAML_Person_Identification

                    With ObjDetailIdentificationNew
                        .Type = CmbTypeIdentificationDirector.Text
                        .Number = TxtNumberDirector.Text
                        If TxtIssueDateDirector.RawValue IsNot Nothing Then
                            .Issue_Date = TxtIssueDateDirector.Value
                        End If
                        If TxtExpiryDateDirector.RawValue IsNot Nothing Then
                            .Expiry_Date = TxtExpiryDateDirector.Value
                        End If
                        .Issued_By = TxtIssuedByDirector.Text
                        .Issued_Country = CmbCountryIdentificationDirector.TextValue
                        .Identification_Comment = TxtCommentIdentificationDirector.Text
                    End With

                    'Daniel 2021 Jan 6
                    'With ObjDetailIdentification
                    With ObjDetailIdentificationDirector
                        'Daniel 2021 Jan 6
                        If .Type <> ObjDetailIdentificationNew.Type Then
                            status = False
                        End If
                        If .Number <> ObjDetailIdentificationNew.Number Then
                            status = False
                        End If
                        If .Issue_Date <> ObjDetailIdentificationNew.Issue_Date Then
                            status = False
                        End If
                        If .Expiry_Date <> ObjDetailIdentificationNew.Expiry_Date Then
                            status = False
                        End If
                        If .Issued_By <> ObjDetailIdentificationNew.Issued_By Then
                            status = False
                        End If
                        If .Issued_Country <> ObjDetailIdentificationNew.Issued_Country Then
                            status = False
                        End If
                        If .Identification_Comment <> ObjDetailIdentificationNew.Identification_Comment Then
                            status = False
                        End If
                    End With

                    If status Then
                        Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                    Else
                        SaveEditDetailIdentificationDirector()
                    End If
                End If

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailPhone_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhone Is Nothing Then
                SaveAddDetailPhone()
            Else
                Dim status As Boolean = True
                Dim ObjDetailPhoneNew As New goAML_Ref_Phone

                With ObjDetailPhoneNew
                    .Tph_Contact_Type = Cmbtph_contact_type.Text
                    .Tph_Communication_Type = Cmbtph_communication_type.Text
                    .tph_country_prefix = Txttph_country_prefix.Text
                    .tph_number = Txttph_number.Text
                    .tph_extension = Txttph_extension.Text
                    .comments = TxtcommentsPhone.Text
                End With

                With ObjDetailPhone
                    If .Tph_Contact_Type <> ObjDetailPhoneNew.Tph_Contact_Type Then
                        status = False
                    End If
                    If .Tph_Communication_Type <> ObjDetailPhoneNew.Tph_Communication_Type Then
                        status = False
                    End If
                    If .tph_country_prefix <> ObjDetailPhoneNew.tph_country_prefix Then
                        status = False
                    End If
                    If .tph_number <> ObjDetailPhoneNew.tph_number Then
                        status = False
                    End If
                    If .tph_extension <> ObjDetailPhoneNew.tph_extension Then
                        status = False
                    End If
                    If .comments <> ObjDetailPhoneNew.comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailPhone()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailPhoneEmployer_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhoneEmployer Is Nothing Then
                SaveAddDetailPhoneEmployer()
            Else
                Dim status As Boolean = True
                Dim ObjDetailPhoneEmployerNew As New goAML_Ref_Phone

                With ObjDetailPhoneEmployerNew
                    .Tph_Contact_Type = Cmbtph_contact_typeEmployer.Text
                    .Tph_Communication_Type = Cmbtph_communication_typeEmployer.Text
                    .tph_country_prefix = Txttph_country_prefixEmployer.Text
                    .tph_number = Txttph_numberEmployer.Text
                    .tph_extension = Txttph_extensionEmployer.Text
                    .comments = TxtcommentsPhoneEmployer.Text
                End With

                With ObjDetailPhoneEmployer
                    If .Tph_Contact_Type <> ObjDetailPhoneEmployerNew.Tph_Contact_Type Then
                        status = False
                    End If
                    If .Tph_Communication_Type <> ObjDetailPhoneEmployerNew.Tph_Communication_Type Then
                        status = False
                    End If
                    If .tph_country_prefix <> ObjDetailPhoneEmployerNew.tph_country_prefix Then
                        status = False
                    End If
                    If .tph_number <> ObjDetailPhoneEmployerNew.tph_number Then
                        status = False
                    End If
                    If .tph_extension <> ObjDetailPhoneEmployerNew.tph_extension Then
                        status = False
                    End If
                    If .comments <> ObjDetailPhoneEmployerNew.comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailPhoneEmployer()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailIdentification()
        Try
            Dim objNewDetailIdentification As New goAML_Person_Identification
            With objNewDetailIdentification
                ' update agam 21102020
                If ListAddressDetailDirectorEmployer.Count = 0 Then
                    .PK_Person_Identification_ID = -1
                ElseIf ListIdentificationDetail.Count > 0 And ListIdentificationDetail.Min(Function(x) x.PK_Person_Identification_ID) > 0 Then
                    .PK_Person_Identification_ID = -1
                ElseIf ListAddressDetailDirectorEmployer.Count > 0 And ListIdentificationDetail.Min(Function(x) x.PK_Person_Identification_ID) < 0 Then
                    .PK_Person_Identification_ID = ListIdentificationDetail.Min(Function(x) x.PK_Person_Identification_ID) - 1
                End If
                '.PK_Person_Identification_ID = ListIdentificationDetail.Select(Function(x) x.PK_Person_Identification_ID).DefaultIfEmpty(0).Max + 1
                .FK_Person_ID = ObjWIC.PK_Customer_ID
                .FK_Person_Type = 8
                .Type = CmbTypeIdentification.Text
                .Number = TxtNumber.Text
                If TxtIssueDate.Text <> DateTime.MinValue Then
                    .Issue_Date = TxtIssueDate.Text
                End If
                If TxtExpiryDate.Text <> DateTime.MinValue Then
                    .Expiry_Date = TxtExpiryDate.Text
                End If
                .Issued_By = TxtIssuedBy.Text
                .Issued_Country = CmbCountryIdentification.TextValue
                .Identification_Comment = TxtCommentIdentification.Text
            End With

            ListIdentificationDetail.Add(objNewDetailIdentification)
            BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
            FormPanelIdentification.Hidden = True
            WindowDetailIdentification.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailIdentificationDirector()
        Try
            Dim objNewDetailIdentification As New goAML_Person_Identification
            With objNewDetailIdentification
                .PK_Person_Identification_ID = ObjDetailIdentificationDirector.PK_Person_Identification_ID
                .FK_Person_ID = If(ObjDirector Is Nothing, 0, ObjDirector.NO_ID)
                .FK_Person_Type = 7
                .Type = CmbTypeIdentificationDirector.Text
                .Number = TxtNumberDirector.Text
                If TxtIssueDateDirector.RawValue IsNot Nothing Then
                    .Issue_Date = TxtIssueDateDirector.Text
                End If
                If TxtExpiryDateDirector.RawValue IsNot Nothing Then
                    .Expiry_Date = TxtExpiryDateDirector.Text
                End If
                .Issued_By = TxtIssuedByDirector.Text
                .Issued_Country = CmbCountryIdentificationDirector.TextValue
                .Identification_Comment = TxtCommentIdentificationDirector.Text
            End With

            ListIdentificationDirector.Add(objNewDetailIdentification)

            If ObjDirector IsNot Nothing Then
                Dim currentDirIdentification = ListIdentificationDirector.Where(Function(x) x.FK_Person_ID = ObjDirector.NO_ID).ToList()
                BindDetailIdentification(StoreIdentificationDirectorDetail, currentDirIdentification)
            End If

            FormPanelDirectorIdentification.Hidden = True
            WindowDetailDirectorIdentification.Hidden = True
            'StoreIdentificationDirector.Reload()
            'FormPanelDirectorIdentification.Hidden = True
            'WindowDetailDirectorIdentification.Hidden = True
            'GP_DirectorIdentification.Reload()
            'WindowDetailDirectorIdentification.Render()
            'WIC_Panel.Reload()
            WindowDetailDirector.Reload()
            'PanelDetailDirectorIdentification.Render()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhone()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                ' update agam 21102020
                If ListPhoneDetail.Count = 0 Then
                    .PK_goAML_Ref_Phone = -1
                ElseIf ListPhoneDetail.Count > 0 And ListPhoneDetail.Min(Function(x) x.PK_goAML_Ref_Phone) > 0 Then
                    .PK_goAML_Ref_Phone = -1
                ElseIf ListPhoneDetail.Count > 0 And ListPhoneDetail.Min(Function(x) x.PK_goAML_Ref_Phone) < 0 Then
                    .PK_goAML_Ref_Phone = ListPhoneDetail.Min(Function(x) x.PK_goAML_Ref_Phone) - 1
                End If
                '.PK_goAML_Ref_Phone = ListPhoneDetail.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 3
                .FK_for_Table_ID = ObjWIC.PK_Customer_ID
                .Tph_Contact_Type = Cmbtph_contact_type.Text
                .Tph_Communication_Type = Cmbtph_communication_type.Text
                .tph_country_prefix = Txttph_country_prefix.Text
                .tph_number = Txttph_number.Text
                .tph_extension = Txttph_extension.Text
                .comments = TxtcommentsPhone.Text
            End With

            ListPhoneDetail.Add(objNewDetailPhone)
            If ObjWIC.FK_Customer_Type_ID = 1 Then
                BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
            Else
                BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
            End If
            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhoneEmployer()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                ' update agam 21102020
                If ListPhoneEmployerDetail.Count = 0 Then
                    .PK_goAML_Ref_Phone = -1
                ElseIf ListPhoneEmployerDetail.Count > 0 And ListPhoneEmployerDetail.Min(Function(x) x.PK_goAML_Ref_Phone) > 0 Then
                    .PK_goAML_Ref_Phone = -1
                ElseIf ListPhoneEmployerDetail.Count > 0 And ListPhoneEmployerDetail.Min(Function(x) x.PK_goAML_Ref_Phone) < 0 Then
                    .PK_goAML_Ref_Phone = ListPhoneEmployerDetail.Min(Function(x) x.PK_goAML_Ref_Phone) - 1
                End If
                '.PK_goAML_Ref_Phone = ListPhoneEmployerDetail.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 10
                .FK_for_Table_ID = ObjWIC.PK_Customer_ID
                .Tph_Contact_Type = Cmbtph_contact_typeEmployer.Text
                .Tph_Communication_Type = Cmbtph_communication_typeEmployer.Text
                .tph_country_prefix = Txttph_country_prefixEmployer.Text
                .tph_number = Txttph_numberEmployer.Text
                .tph_extension = Txttph_extensionEmployer.Text
                .comments = TxtcommentsPhoneEmployer.Text
            End With

            ListPhoneEmployerDetail.Add(objNewDetailPhone)
            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)
            FormPanelPhoneEmployerDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailPhoneDirector()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                .PK_goAML_Ref_Phone = ObjDetailPhoneDirector.PK_goAML_Ref_Phone ' 2023-10-23, Nael
                .FK_Ref_Detail_Of = 8
                .FK_for_Table_ID = If(ObjDirector Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, ObjDirector.NO_ID)
                .Tph_Contact_Type = Director_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_txt_phone_Country_prefix.Text
                .tph_number = Director_txt_phone_number.Text
                .tph_extension = Director_txt_phone_extension.Text
                .comments = Director_txt_phone_comments.Text
            End With

            ListPhoneDetailDirector.Add(objNewDetailPhone)
            If ObjDirector IsNot Nothing Then
                Dim currentDirPhone = ListPhoneDetailDirector.Where(Function(x) x.FK_for_Table_ID = ObjDirector.NO_ID).ToList()
                BindDetailPhone(Store_DirectorPhone, currentDirPhone)
            End If
            FormPanelDirectorPhone.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try


    End Sub
    Private Sub SaveAddDetailPhoneDirectorEmployer()
        Try
            Dim objNewDetailPhone As New goAML_Ref_Phone
            With objNewDetailPhone
                .PK_goAML_Ref_Phone = ObjDetailPhoneDirectorEmployer.PK_goAML_Ref_Phone ' 2023-10-23, Nael
                .FK_Ref_Detail_Of = 4
                .FK_for_Table_ID = If(ObjDirector Is Nothing, ListDirectorDetail.Select(Function(x) x.NO_ID).DefaultIfEmpty(0).Max() + 1, ObjDirector.NO_ID)
                .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text
                .tph_number = Director_Emp_txt_phone_number.Text
                .tph_extension = Director_Emp_txt_phone_extension.Text
                .comments = Director_Emp_txt_phone_comments.Text
            End With

            ListPhoneDetailDirectorEmployer.Add(objNewDetailPhone)
            BindDetailPhone(Store_Director_Employer_Phone, ListPhoneDetailDirectorEmployer)
            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailIdentification()
        Try
            With ObjDetailIdentification
                .Type = CmbTypeIdentification.Text
                .Number = TxtNumber.Text
                If TxtIssueDate.Text <> DateTime.MinValue Then
                    .Issue_Date = TxtIssueDate.Text
                Else
                    .Issue_Date = Nothing
                End If
                If TxtExpiryDate.Text <> DateTime.MinValue Then
                    .Expiry_Date = TxtExpiryDate.Text
                Else
                    .Expiry_Date = Nothing
                End If
                .Issued_By = TxtIssuedBy.Text
                .Issued_Country = CmbCountryIdentification.TextValue
                .Identification_Comment = TxtCommentIdentification.Text
            End With

            ObjDetailIdentification = Nothing

            BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
            FormPanelIdentification.Hidden = True
            WindowDetailIdentification.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailIdentificationDirector()
        Try
            With ObjDetailIdentificationDirector
                .Type = CmbTypeIdentificationDirector.Text
                .Number = TxtNumberDirector.Text
                If TxtIssueDateDirector.RawValue IsNot Nothing Then
                    .Issue_Date = TxtIssueDateDirector.Text
                Else
                    .Issue_Date = Nothing
                End If
                If TxtExpiryDateDirector.RawValue IsNot Nothing Then
                    .Expiry_Date = TxtExpiryDateDirector.Text
                Else
                    .Expiry_Date = Nothing
                End If
                .Issued_By = TxtIssuedByDirector.Text
                .Issued_Country = CmbCountryIdentificationDirector.TextValue
                .Identification_Comment = TxtCommentIdentificationDirector.Text
            End With

            ObjDetailIdentificationDirector = Nothing

            'Daniel 2021 Jan 6
            'BindDetailIdentification(StoreIdentificationDirector, ListIdentificationDirector)
            BindDetailIdentification(StoreIdentificationDirectorDetail, ListIdentificationDirector)
            'end 2021 Jan 6
            FormPanelIdentification.Hidden = True
            'Daniel 2021 Jan 7
            'WindowDetailIdentification.Hidden = True
            WindowDetailDirectorIdentification.Hidden = True
            'end 2021 Jan 7
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhone()
        Try
            With ObjDetailPhone
                .Tph_Contact_Type = Cmbtph_contact_type.Text
                .Tph_Communication_Type = Cmbtph_communication_type.Text
                .tph_country_prefix = Txttph_country_prefix.Text
                .tph_number = Txttph_number.Text
                .tph_extension = Txttph_extension.Text
                .comments = TxtcommentsPhone.Text
            End With

            ObjDetailPhone = Nothing
            If ObjWIC.FK_Customer_Type_ID = 1 Then
                BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
            Else
                BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
            End If

            FormPanelPhoneDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhoneEmployer()
        Try
            With ObjDetailPhoneEmployer
                .Tph_Contact_Type = Cmbtph_contact_typeEmployer.Text
                .Tph_Communication_Type = Cmbtph_communication_typeEmployer.Text
                .tph_country_prefix = Txttph_country_prefixEmployer.Text
                .tph_number = Txttph_numberEmployer.Text
                .tph_extension = Txttph_extensionEmployer.Text
                .comments = TxtcommentsPhoneEmployer.Text
            End With

            ObjDetailPhoneEmployer = Nothing
            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)

            FormPanelPhoneEmployerDetail.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhoneDirector()
        Try
            With ObjDetailPhoneDirector
                .Tph_Contact_Type = Director_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_txt_phone_Country_prefix.Text
                .tph_number = Director_txt_phone_number.Text
                .tph_extension = Director_txt_phone_extension.Text
                .comments = Director_txt_phone_comments.Text
            End With

            ObjDetailPhoneDirector = Nothing
            BindDetailPhone(Store_DirectorPhone, ListPhoneDetailDirector)
            FormPanelDirectorPhone.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailPhoneDirectorEmployer()
        Try
            With ObjDetailPhoneDirectorEmployer
                .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.Text
                .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.Text
                .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text
                .tph_number = Director_Emp_txt_phone_number.Text
                .tph_extension = Director_Emp_txt_phone_extension.Text
                .comments = Director_Emp_txt_phone_comments.Text
            End With

            ObjDetailPhoneDirectorEmployer = Nothing
            BindDetailPhone(Store_Director_Employer_Phone, ListPhoneDetailDirectorEmployer)
            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If ObjDetailAddress Is Nothing Then
                SaveAddDetailAddress()
            Else
                Dim status As Boolean = True
                Dim ObjDetailAddressNew As New goAML_Ref_Address

                With ObjDetailAddressNew
                    .Address_Type = CmbAddress_type.Text
                    .Address = TxtAddress.Text
                    .Town = TxtTown.Text
                    .City = TxtCity.Text
                    .Zip = TxtZip.Text
                    .Country_Code = Cmbcountry_code.TextValue
                    .State = TxtState.Text
                    .Comments = TxtcommentsAddress.Text
                End With

                With ObjDetailAddress
                    If .Address_Type <> ObjDetailAddressNew.Address_Type Then
                        status = False
                    End If
                    If .Address <> ObjDetailAddressNew.Address Then
                        status = False
                    End If
                    If .Town <> ObjDetailAddressNew.Town Then
                        status = False
                    End If
                    If .City <> ObjDetailAddressNew.City Then
                        status = False
                    End If
                    If .Zip <> ObjDetailAddressNew.Zip Then
                        status = False
                    End If
                    If .Country_Code <> ObjDetailAddressNew.Country_Code Then
                        status = False
                    End If
                    If .State <> ObjDetailAddressNew.State Then
                        status = False
                    End If
                    If .Comments <> ObjDetailAddressNew.Comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailAddress()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSaveDetailAddressEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If ObjDetailAddressEmployer Is Nothing Then
                SaveAddDetailAddressEmployer()
            Else
                Dim status As Boolean = True
                Dim ObjDetailAddressEmployerNew As New goAML_Ref_Address

                With ObjDetailAddressEmployerNew
                    .Address_Type = CmbAddress_typeEmployer.Text
                    .Address = TxtAddressEmployer.Text
                    .Town = TxtTownEmployer.Text
                    .City = TxtCityEmployer.Text
                    .Zip = TxtZipEmployer.Text
                    .Country_Code = Cmbcountry_codeEmployer.TextValue
                    .State = TxtStateEmployer.Text
                    .Comments = TxtcommentsAddressEmployer.Text
                End With

                With ObjDetailAddressEmployer
                    If .Address_Type <> ObjDetailAddressEmployerNew.Address_Type Then
                        status = False
                    End If
                    If .Address <> ObjDetailAddressEmployerNew.Address Then
                        status = False
                    End If
                    If .Town <> ObjDetailAddressEmployerNew.Town Then
                        status = False
                    End If
                    If .City <> ObjDetailAddressEmployerNew.City Then
                        status = False
                    End If
                    If .Zip <> ObjDetailAddressEmployerNew.Zip Then
                        status = False
                    End If
                    If .Country_Code <> ObjDetailAddressEmployerNew.Country_Code Then
                        status = False
                    End If
                    If .State <> ObjDetailAddressEmployerNew.State Then
                        status = False
                    End If
                    If .Comments <> ObjDetailAddressEmployerNew.Comments Then
                        status = False
                    End If
                End With

                If status Then
                    Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                Else
                    SaveEditDetailAddressEmployer()
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorPhones_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If ObjDetailPhoneDirector IsNot Nothing Then
                Dim alreadySaved = ListPhoneDetailDirector.Exists(Function(x) x.PK_goAML_Ref_Phone = ObjDetailPhoneDirector.PK_goAML_Ref_Phone)
                If Not alreadySaved Then ' 2023-10-23, Nael
                    SaveAddDetailPhoneDirector()
                Else
                    Dim status As Boolean = True
                    Dim ObjDetailPhoneDirectorNew As New goAML_Ref_Phone

                    With ObjDetailPhoneDirectorNew
                        .Tph_Contact_Type = Director_cb_phone_Contact_Type.Text
                        .Tph_Communication_Type = Director_cb_phone_Communication_Type.Text
                        .tph_country_prefix = Director_txt_phone_Country_prefix.Text
                        .tph_number = Director_txt_phone_number.Text
                        .tph_extension = Director_txt_phone_extension.Text
                        .comments = Director_txt_phone_comments.Text
                    End With

                    With ObjDetailPhoneDirector
                        If .Tph_Contact_Type <> ObjDetailPhoneDirectorNew.Tph_Contact_Type Then
                            status = False
                        End If
                        If .Tph_Communication_Type <> ObjDetailPhoneDirectorNew.Tph_Communication_Type Then
                            status = False
                        End If
                        If .tph_country_prefix <> ObjDetailPhoneDirectorNew.tph_country_prefix Then
                            status = False
                        End If
                        If .tph_number <> ObjDetailPhoneDirectorNew.tph_number Then
                            status = False
                        End If
                        If .tph_extension <> ObjDetailPhoneDirectorNew.tph_extension Then
                            status = False
                        End If
                        If .comments <> ObjDetailPhoneDirectorNew.comments Then
                            status = False
                        End If
                    End With

                    If status Then
                        Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                    Else
                        SaveEditDetailPhoneDirector()
                    End If
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveAddDetailAddress()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                ' update agam 21102020
                If ListAddressDetail.Count = 0 Then
                    .PK_Customer_Address_ID = -1
                ElseIf ListAddressDetail.Count > 0 And ListAddressDetail.Min(Function(x) x.PK_Customer_Address_ID) > 0 Then
                    .PK_Customer_Address_ID = -1
                ElseIf ListAddressDetail.Count > 0 And ListAddressDetail.Min(Function(x) x.PK_Customer_Address_ID) < 0 Then
                    .PK_Customer_Address_ID = ListAddressDetail.Min(Function(x) x.PK_Customer_Address_ID) - 1
                End If
                '.PK_Customer_Address_ID = ListAddressDetail.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 3
                .FK_To_Table_ID = ObjWIC.PK_Customer_ID
                .Address_Type = CmbAddress_type.Text
                .Address = TxtAddress.Text
                .Town = TxtTown.Text
                .City = TxtCity.Text
                .Zip = TxtZip.Text
                .Country_Code = Cmbcountry_code.TextValue
                .State = TxtState.Text
                .Comments = TxtcommentsAddress.Text
            End With

            ListAddressDetail.Add(objNewDetailAddress)
            If ObjWIC.FK_Customer_Type_ID = 1 Then
                BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
            Else
                BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
            End If

            FormPanelAddressDetail.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailAddressEmployer()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                ' update agam 21102020
                If ListAddressEmployerDetail.Count = 0 Then
                    .PK_Customer_Address_ID = -1
                ElseIf ListAddressEmployerDetail.Count > 0 And ListAddressEmployerDetail.Min(Function(x) x.PK_Customer_Address_ID) > 0 Then
                    .PK_Customer_Address_ID = -1
                ElseIf ListAddressEmployerDetail.Count > 0 And ListAddressEmployerDetail.Min(Function(x) x.PK_Customer_Address_ID) < 0 Then
                    .PK_Customer_Address_ID = ListAddressEmployerDetail.Min(Function(x) x.PK_Customer_Address_ID) - 1
                End If
                '.PK_Customer_Address_ID = ListAddressEmployerDetail.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
                .FK_Ref_Detail_Of = 10
                .FK_To_Table_ID = ObjWIC.PK_Customer_ID
                .Address_Type = CmbAddress_typeEmployer.Text
                .Address = TxtAddressEmployer.Text
                .Town = TxtTownEmployer.Text
                .City = TxtCityEmployer.Text
                .Zip = TxtZipEmployer.Text
                .Country_Code = Cmbcountry_codeEmployer.TextValue
                .State = TxtStateEmployer.Text
                .Comments = TxtcommentsAddressEmployer.Text
            End With

            ListAddressEmployerDetail.Add(objNewDetailAddress)
            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)

            FormPanelAddressDetailEmployer.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailAddressDirector()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                .PK_Customer_Address_ID = ObjDetailAddressDirector.PK_Customer_Address_ID
                .FK_Ref_Detail_Of = 8
                .FK_To_Table_ID = If(ObjDirector Is Nothing, 0, ObjDirector.NO_ID)
                .Address_Type = Director_cmb_kategoriaddress.Text
                .Address = Director_Address.Text
                .Town = Director_Town.Text
                .City = Director_City.Text
                .Zip = Director_Zip.Text
                .Country_Code = Director_cmb_kodenegara.TextValue
                .State = Director_State.Text
                .Comments = Director_Comment_Address.Text
            End With

            ListAddressDetailDirector.Add(objNewDetailAddress)
            If ObjDirector IsNot Nothing Then
                Dim currentDirAddress = ListAddressDetailDirector.Where(Function(x) x.FK_To_Table_ID = ObjDirector.NO_ID).ToList()
                BindDetailAddress(Store_DirectorAddress, currentDirAddress)
            End If
            FP_Address_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveAddDetailAddressDirectorEmployer()
        Try
            Dim objNewDetailAddress As New goAML_Ref_Address
            With objNewDetailAddress
                .PK_Customer_Address_ID = ObjDetailAddressDirectorEmployer.PK_Customer_Address_ID
                .FK_Ref_Detail_Of = 4
                .FK_To_Table_ID = If(ObjDirector Is Nothing, 0, ObjDirector.NO_ID)
                .Address_Type = Director_cmb_emp_kategoriaddress.Text
                .Address = Director_emp_Address.Text
                .Town = Director_emp_Town.Text
                .City = Director_emp_City.Text
                .Zip = Director_emp_Zip.Text
                .Country_Code = Director_cmb_emp_kodenegara.TextValue
                .State = Director_emp_State.Text
                .Comments = Director_emp_Comment_Address.Text
            End With

            ListAddressDetailDirectorEmployer.Add(objNewDetailAddress)
            If ObjDirector IsNot Nothing Then
                Dim currentDirAddressEmp = ListAddressDetailDirectorEmployer.Where(Function(x) x.FK_To_Table_ID = ObjDirector.NO_ID).ToList()
                BindDetailAddress(Store_Director_Employer_Address, currentDirAddressEmp)
            End If
            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveEditDetailAddress()
        Try
            With ObjDetailAddress
                .Address_Type = CmbAddress_type.Text
                .Address = TxtAddress.Text
                .Town = TxtTown.Text
                .City = TxtCity.Text
                .Zip = TxtZip.Text
                .Country_Code = Cmbcountry_code.TextValue
                .State = TxtState.Text
                .Comments = TxtcommentsAddress.Text
            End With

            ObjDetailAddress = Nothing
            If ObjWIC.FK_Customer_Type_ID = 1 Then
                BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
            Else
                BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
            End If

            FormPanelAddressDetail.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailAddressEmployer()
        Try
            With ObjDetailAddressEmployer
                .Address_Type = CmbAddress_typeEmployer.Text
                .Address = TxtAddressEmployer.Text
                .Town = TxtTownEmployer.Text
                .City = TxtCityEmployer.Text
                .Zip = TxtZipEmployer.Text
                .Country_Code = Cmbcountry_codeEmployer.TextValue
                .State = TxtStateEmployer.Text
                .Comments = TxtcommentsAddressEmployer.Text
            End With

            ObjDetailAddressEmployer = Nothing
            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)

            FormPanelAddressDetailEmployer.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailAddressDirector()
        Try
            With ObjDetailAddressDirector
                .Address_Type = Director_cmb_kategoriaddress.Text
                .Address = Director_Address.Text
                .Town = Director_Town.Text
                .City = Director_City.Text
                .Zip = Director_Zip.Text
                .Country_Code = Director_cmb_kodenegara.TextValue
                .State = Director_State.Text
                .Comments = Director_Comment_Address.Text
            End With

            ObjDetailAddressDirector = Nothing
            BindDetailAddress(Store_DirectorAddress, ListAddressDetailDirector)
            FP_Address_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub SaveEditDetailAddressDirectorEmployer()
        Try
            With ObjDetailAddressDirectorEmployer
                .Address_Type = Director_cmb_emp_kategoriaddress.Text
                .Address = Director_emp_Address.Text
                .Town = Director_emp_Town.Text
                .City = Director_emp_City.Text
                .Zip = Director_emp_Zip.Text
                .Country_Code = Director_cmb_emp_kodenegara.TextValue
                .State = Director_emp_State.Text
                .Comments = Director_emp_Comment_Address.Text
            End With

            ObjDetailAddressDirectorEmployer = Nothing
            BindDetailAddress(Store_Director_Employer_Address, ListAddressDetailDirectorEmployer)
            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveCustomerDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim alreadySaved = ListDirectorDetail.Any(Function(x) x.NO_ID = ObjDirector.NO_ID)
            If Not alreadySaved Then
                SaveAddDetailDirector()
            Else
                SaveEditDetailDirector()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveEditDetailDirector()
        Try
            'If cmb_Director_Gender.Value = "" Then
            '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
            'ElseIf cmb_peran.Value = "" Then
            '    Throw New Exception("Role Tidak boleh kosong")
            'ElseIf txt_Director_Last_Name.Text = "" Then
            '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
            'ElseIf txt_Director_BirthDate.Text = "1/1/0001 12:00:00 AM" Or txt_Director_BirthDate.Value = DateTime.MinValue Then
            '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
            'ElseIf txt_Director_Birth_Place.Text = "" Then
            '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
            'ElseIf cmb_Director_Nationality1.TextValue = "" Then
            '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
            'ElseIf cmb_Director_Residence.TextValue = "" Then
            '    Throw New Exception("Negara Domisili Tidak boleh kosong")
            'ElseIf txt_Director_Occupation.Text = "" Then
            '    Throw New Exception("Pekerjaan Tidak boleh kosong")
            'ElseIf txt_Director_Source_of_Wealth.Text = "" Then
            '    Throw New Exception("Sumber Dana Tidak boleh kosong")
            'ElseIf txt_Director_SSN.Text.Length <> 16 And txt_Director_SSN.Text <> "" Then
            '    Throw New Exception("NIK Must 16 Digit")
            'End If
            If cmb_peran.Text = "" Then
                Throw New ApplicationException("Role is Required")
            End If
            If txt_Director_Last_Name.Text = "" Then
                Throw New ApplicationException("Full Name is Required")
            End If
            If ListPhoneDetailDirector Is Nothing Then
                Throw New Exception("Phone is Required")
            End If
            If ListAddressDetailDirector Is Nothing Then
                Throw New Exception("Address is Required")
            End If
            If ListPhoneDetailDirector.Count = 0 Then
                Throw New Exception("Phone is Required")
            End If
            If ListAddressDetailDirector.Count = 0 Then
                Throw New Exception("Address is Required")
            End If

            Dim status As Boolean = True
            Dim DirectorClass As New WICDirectorDataBLL
            Dim ListDirectorDetailOld As New List(Of goAML_Ref_Walk_In_Customer_Director)
            Dim DirectorOld As New goAML_Ref_Walk_In_Customer_Director
            Dim ListDirectorAddressOld As New List(Of goAML_Ref_Address)
            Dim ListDirectorAddressEmployerOld As New List(Of goAML_Ref_Address)
            Dim ListDirectorPhoneOld As New List(Of goAML_Ref_Phone)
            Dim ListDirectorPhoneEmployerOld As New List(Of goAML_Ref_Phone)
            Dim ListDirectorIdentificationOld As New List(Of goAML_Person_Identification)

            ListDirectorDetailOld = objgoAML_WIC.GetWICByPKIDDetailDirector(ObjWIC.PK_Customer_ID)

            With ObjDirector
                .Role = cmb_peran.Text
                .Title = txt_Director_Title.Text
                .Last_Name = txt_Director_Last_Name.Text
                .Gender = cmb_Director_Gender.Text
                If txt_Director_BirthDate.Text <> DateTime.MinValue Then
                    .BirthDate = txt_Director_BirthDate.Text
                End If
                .Birth_Place = txt_Director_Birth_Place.Text
                .Mothers_Name = txt_Director_Mothers_Name.Text
                .Alias = txt_Director_Alias.Text
                .ID_Number = txt_Director_ID_Number.Text
                .Passport_Number = txt_Director_Passport_Number.Text
                .Passport_Country = txt_Director_Passport_Country.TextValue
                .SSN = txt_Director_SSN.Text
                .Nationality1 = cmb_Director_Nationality1.TextValue
                .Nationality2 = cmb_Director_Nationality2.TextValue
                .Nationality3 = cmb_Director_Nationality3.TextValue
                .Residence = cmb_Director_Residence.TextValue
                '.Email = txt_Director_Email.Text
                '.Email2 = txt_Director_Email2.Text
                '.Email3 = txt_Director_Email3.Text
                '.Email4 = txt_Director_Email4.Text
                '.Email5 = txt_Director_Email5.Text
                .Deceased = cb_Director_Deceased.Checked
                If cb_Director_Deceased.Checked Then
                    If txt_Director_Deceased_Date.Text <> DateTime.MinValue Then
                        .Deceased_Date = txt_Director_Deceased_Date.Text
                    End If
                End If
                .Tax_Reg_Number = cb_Director_Tax_Reg_Number.Checked
                .Tax_Number = txt_Director_Tax_Number.Text
                .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
                .Occupation = txt_Director_Occupation.Text
                .Comments = txt_Director_Comments.Text
                'Daniel 2021 Jan 5
                .Employer_Name = txt_Director_employer_name.Text
                'end 2021 Jan 5
            End With

            With ObjDirector
                DirectorOld = ListDirectorDetailOld.Where(Function(x) x.NO_ID = .NO_ID).FirstOrDefault

                If DirectorOld IsNot Nothing Then
                    If .Role <> DirectorOld.Role Then
                        status = False
                    End If
                    If .Gender <> DirectorOld.Gender Then
                        status = False
                    End If
                    If .Title <> DirectorOld.Title Then
                        status = False
                    End If
                    If .Last_Name <> DirectorOld.Last_Name Then
                        status = False
                    End If
                    If .BirthDate <> DirectorOld.BirthDate Then
                        status = False
                    End If
                    If .Birth_Place <> DirectorOld.Birth_Place Then
                        status = False
                    End If
                    If .Mothers_Name <> DirectorOld.Mothers_Name Then
                        status = False
                    End If
                    If .Alias <> DirectorOld.Alias Then
                        status = False
                    End If
                    If .SSN <> DirectorOld.SSN Then
                        status = False
                    End If
                    If .Passport_Number <> DirectorOld.Passport_Number Then
                        status = False
                    End If
                    If .Passport_Country <> DirectorOld.Passport_Country Then
                        status = False
                    End If
                    If .ID_Number <> DirectorOld.ID_Number Then
                        status = False
                    End If
                    If .Nationality1 <> DirectorOld.Nationality1 Then
                        status = False
                    End If
                    If .Nationality2 <> DirectorOld.Nationality2 Then
                        status = False
                    End If
                    If .Nationality3 <> DirectorOld.Nationality3 Then
                        status = False
                    End If
                    If .Residence <> DirectorOld.Residence Then
                        status = False
                    End If
                    If .Email <> DirectorOld.Email Then
                        status = False
                    End If
                    If .Email2 <> DirectorOld.Email2 Then
                        status = False
                    End If
                    If .Email3 <> DirectorOld.Email3 Then
                        status = False
                    End If
                    If .Email4 <> DirectorOld.Email4 Then
                        status = False
                    End If
                    If .Email5 <> DirectorOld.Email5 Then
                        status = False
                    End If
                    If .Occupation <> DirectorOld.Occupation Then
                        status = False
                    End If
                    If .Employer_Name <> DirectorOld.Employer_Name Then
                        status = False
                    End If
                    If .Deceased <> DirectorOld.Deceased Then
                        status = False
                    End If
                    If .Deceased_Date <> DirectorOld.Deceased_Date Then
                        status = False
                    End If
                    If .Tax_Number <> DirectorOld.Tax_Number Then
                        status = False
                    End If
                    If .Tax_Reg_Number <> DirectorOld.Tax_Reg_Number Then
                        status = False
                    End If
                    If .Source_of_Wealth <> DirectorOld.Source_of_Wealth Then
                        status = False
                    End If
                    If .Comments <> DirectorOld.Comments Then
                        status = False
                    End If
                Else
                    status = False
                End If

                If DirectorOld IsNot Nothing Then
                    ListDirectorAddressOld = objgoAML_WIC.GetWICByPKIDDetailDirectorAddress(DirectorOld.NO_ID)
                    ListDirectorAddressEmployerOld = objgoAML_WIC.GetWICByPKIDDetailDirectorAddressEmployer(DirectorOld.NO_ID)
                    ListDirectorPhoneOld = objgoAML_WIC.GetWICByPKIDDetailDirectorPhone(DirectorOld.NO_ID)
                    ListDirectorPhoneEmployerOld = objgoAML_WIC.GetWICByPKIDDetailDirectorPhoneEmployer(DirectorOld.NO_ID)
                    ListDirectorIdentificationOld = objgoAML_WIC.GetWICByPKIDDetailDirectorIdentification(DirectorOld.NO_ID)
                End If

                If ListAddressDetailDirector.Count = ListDirectorAddressOld.Count Then
                    Dim Address As New goAML_Ref_Address
                    For Each item In ListAddressDetailDirector
                        Address = ListDirectorAddressOld.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                        If Address IsNot Nothing AndAlso Address.FK_Ref_Detail_Of IsNot Nothing Then
                            With Address
                                If .Address_Type <> item.Address_Type Then
                                    status = False
                                End If
                                If .Address <> item.Address Then
                                    status = False
                                End If
                                If .Town <> item.Town Then
                                    status = False
                                End If
                                If .City <> item.City Then
                                    status = False
                                End If
                                If .Zip <> item.Zip Then
                                    status = False
                                End If
                                If .Country_Code <> item.Country_Code Then
                                    status = False
                                End If
                                If .State <> item.State Then
                                    status = False
                                End If
                                If .Comments <> item.Comments Then
                                    status = False
                                End If
                            End With
                        Else
                            status = False
                        End If
                        Address = New goAML_Ref_Address
                    Next
                Else
                    status = False
                End If

                If ListAddressDetailDirectorEmployer.Count = ListDirectorAddressEmployerOld.Count Then
                    Dim AddressEmp As New goAML_Ref_Address
                    For Each item In ListAddressDetailDirectorEmployer
                        AddressEmp = ListDirectorAddressEmployerOld.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                        If AddressEmp IsNot Nothing AndAlso AddressEmp.FK_Ref_Detail_Of IsNot Nothing Then
                            With AddressEmp
                                If .Address_Type <> item.Address_Type Then
                                    status = False
                                End If
                                If .Address <> item.Address Then
                                    status = False
                                End If
                                If .Town <> item.Town Then
                                    status = False
                                End If
                                If .City <> item.City Then
                                    status = False
                                End If
                                If .Zip <> item.Zip Then
                                    status = False
                                End If
                                If .Country_Code <> item.Country_Code Then
                                    status = False
                                End If
                                If .State <> item.State Then
                                    status = False
                                End If
                                If .Comments <> item.Comments Then
                                    status = False
                                End If
                            End With
                        Else
                            status = False
                        End If
                        AddressEmp = New goAML_Ref_Address
                    Next
                Else
                    status = False
                End If

                If ListPhoneDetailDirector.Count = ListDirectorPhoneOld.Count Then
                    Dim phone As New goAML_Ref_Phone
                    For Each item In ListPhoneDetailDirector
                        phone = ListDirectorPhoneOld.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                        If phone IsNot Nothing AndAlso phone.FK_Ref_Detail_Of IsNot Nothing Then ' 2023-10-23, Nael: Null checker ke object phone
                            With phone
                                If .Tph_Contact_Type <> item.Tph_Contact_Type Then
                                    status = False
                                End If
                                If .Tph_Communication_Type <> item.Tph_Communication_Type Then
                                    status = False
                                End If
                                If .tph_country_prefix <> item.tph_country_prefix Then
                                    status = False
                                End If
                                If .tph_number <> item.tph_number Then
                                    status = False
                                End If
                                If .tph_extension <> item.tph_extension Then
                                    status = False
                                End If
                                If .comments <> item.comments Then
                                    status = False
                                End If
                            End With
                        Else
                            status = False
                        End If
                        phone = New goAML_Ref_Phone
                    Next
                Else
                    status = False
                End If

                If ListPhoneDetailDirectorEmployer.Count = ListDirectorPhoneEmployerOld.Count Then
                    Dim phoneEmp As New goAML_Ref_Phone
                    For Each item In ListPhoneDetailDirectorEmployer
                        phoneEmp = ListDirectorPhoneEmployerOld.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                        If phoneEmp IsNot Nothing AndAlso phoneEmp.FK_Ref_Detail_Of IsNot Nothing Then
                            With phoneEmp
                                If .Tph_Contact_Type <> item.Tph_Contact_Type Then
                                    status = False
                                End If
                                If .Tph_Communication_Type <> item.Tph_Communication_Type Then
                                    status = False
                                End If
                                If .tph_country_prefix <> item.tph_country_prefix Then
                                    status = False
                                End If
                                If .tph_number <> item.tph_number Then
                                    status = False
                                End If
                                If .tph_extension <> item.tph_extension Then
                                    status = False
                                End If
                                If .comments <> item.comments Then
                                    status = False
                                End If
                            End With
                        Else
                            status = False
                        End If
                        phoneEmp = New goAML_Ref_Phone
                    Next
                Else
                    status = False
                End If

                If ListIdentificationDirector.Count = ListDirectorIdentificationOld.Count Then
                    Dim Identification As New goAML_Person_Identification
                    For Each item In ListIdentificationDirector
                        Identification = ListDirectorIdentificationOld.Where(Function(x) x.PK_Person_Identification_ID = item.PK_Person_Identification_ID).FirstOrDefault
                        If Identification IsNot Nothing AndAlso Identification.FK_Person_ID IsNot Nothing Then
                            With Identification
                                If .Type <> item.Type Then
                                    status = False
                                End If
                                If .Number <> item.Number Then
                                    status = False
                                End If
                                If .Issue_Date <> item.Issue_Date Then
                                    status = False
                                End If
                                If .Expiry_Date <> item.Expiry_Date Then
                                    status = False
                                End If
                                If .Issued_By <> item.Issued_By Then
                                    status = False
                                End If
                                If .Issued_Country <> item.Issued_Country Then
                                    status = False
                                End If
                                If .Identification_Comment <> item.Identification_Comment Then
                                    status = False
                                End If
                            End With
                        Else
                            status = False
                        End If
                        Identification = New goAML_Person_Identification
                    Next
                Else
                    status = False
                End If

                If GridEmail_Director.IsDataChange Then
                    status = False
                End If
                If GridSanction_Director.IsDataChange Then
                    status = False
                End If
                If GridPEP_Director.IsDataChange Then
                    status = False
                End If
            End With

            If status Then
                Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
            End If

            Dim currentDirAddress = ListAddressDetailDirector.Where(Function(x) x.FK_To_Table_ID = ObjDirector.NO_ID).ToList()
            Dim currentDirAddressEmp = ListAddressDetailDirectorEmployer.Where(Function(x) x.FK_To_Table_ID = ObjDirector.NO_ID).ToList()
            Dim currentDirPhone = ListPhoneDetailDirector.Where(Function(x) x.FK_for_Table_ID = ObjDirector.NO_ID).ToList()
            Dim currentDirPhoneEmp = ListPhoneDetailDirectorEmployer.Where(Function(x) x.FK_for_Table_ID = ObjDirector.NO_ID).ToList()
            Dim currentDirIdentification = ListIdentificationDirector.Where(Function(x) x.FK_Person_ID = ObjDirector.NO_ID).ToList()

            With DirectorClass
                .ObjDirector = ObjDirector
                .ListDirectorAddress = currentDirAddress
                .ListDirectorAddressEmployer = currentDirAddressEmp
                .ListDirectorPhone = currentDirPhone
                .ListDirectorPhoneEmployer = currentDirPhoneEmp
                .ListDirectorIdentification = currentDirIdentification
                .ListDirectorEmail = GridEmail_Director.objListgoAML_Ref
                .ListDirectorSanction = GridSanction_Director.objListgoAML_Ref
                .ListDirectorPEP = GridPEP_Director.objListgoAML_Ref
            End With
            ListDirectorDetail.Remove(ListDirectorDetail.Where(Function(x) x.NO_ID = DirectorClass.ObjDirector.NO_ID).FirstOrDefault)
            ListDirectorDetailClass.Remove(ListDirectorDetailClass.Where(Function(x) x.ObjDirector.NO_ID = DirectorClass.ObjDirector.NO_ID).FirstOrDefault)
            ListDirectorDetail.Add(DirectorClass.ObjDirector)
            ListDirectorDetailClass.Add(DirectorClass)

            ObjDirector = Nothing
            BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
            FP_Director.Hidden = True
            WindowDetailDirector.Hidden = True
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub SaveAddDetailDirector()
        Try
            'If cmb_Director_Gender.Value = "" Then
            '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
            'ElseIf cmb_peran.Value = "" Then
            '    Throw New Exception("Role Tidak boleh kosong")
            'ElseIf txt_Director_Last_Name.Text = "" Then
            '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
            'ElseIf txt_Director_BirthDate.Text = "1/1/0001 12:00:00 AM" Or txt_Director_BirthDate.Value = DateTime.MinValue Then
            '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
            'ElseIf txt_Director_Birth_Place.Text = "" Then
            '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
            'ElseIf cmb_Director_Nationality1.TextValue = "" Then
            '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
            'ElseIf cmb_Director_Residence.TextValue = "" Then
            '    Throw New Exception("Negara Domisili Tidak boleh kosong")
            'ElseIf txt_Director_Occupation.Text = "" Then
            '    Throw New Exception("Pekerjaan Tidak boleh kosong")
            'ElseIf txt_Director_Source_of_Wealth.Text = "" Then
            '    Throw New Exception("Sumber Dana Tidak boleh kosong")
            'ElseIf txt_Director_SSN.Text.Length <> 16 And txt_Director_SSN.Text <> "" Then
            '    Throw New Exception("NIK Must 16 Digit")
            'End If
            'If cmb_Director_Gender.Value = "" Then
            '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
            'ElseIf cmb_peran.Value = "" Then
            '    Throw New Exception("Role Tidak boleh kosong")
            'ElseIf txt_Director_Last_Name.Text = "" Then
            '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
            'ElseIf txt_Director_BirthDate.Text = "1/1/0001 12:00:00 AM" Or txt_Director_BirthDate.Value = DateTime.MinValue Then
            '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
            'ElseIf txt_Director_Birth_Place.Text = "" Then
            '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
            'ElseIf cmb_Director_Nationality1.TextValue = "" Then
            '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
            'ElseIf cmb_Director_Residence.TextValue = "" Then
            '    Throw New Exception("Negara Domisili Tidak boleh kosong")
            'ElseIf ListPhoneDetailDirector.Count <= 0 Then
            '    Throw New Exception("Informasi Telepon Director Tidak boleh kosong")
            'ElseIf ListAddressDetailDirector.Count <= 0 Then
            '    Throw New Exception("Informasi Alamat Director Tidak boleh kosong")
            'ElseIf txt_Director_Occupation.Text = "" Then
            '    Throw New Exception("Pekerjaan Tidak boleh kosong")
            'ElseIf txt_Director_Source_of_Wealth.Text = "" Then
            '    Throw New Exception("Sumber Dana Tidak boleh kosong")
            If cmb_peran.Value = "" Then
                Throw New Exception("Role is Required")
            ElseIf txt_Director_Last_Name.Text = "" Then
                Throw New Exception("Full Name is Required")
            Else
                If txt_Director_SSN.Text.Length <> 16 And txt_Director_SSN.Text.Length > 0 Then
                    Throw New Exception("NIK Must 16 Digit")
                Else
                    If ListPhoneDetailDirector Is Nothing Then
                        Throw New Exception("Phone is Required")
                    End If
                    If ListAddressDetailDirector Is Nothing Then
                        Throw New Exception("Address is Required")
                    End If
                    If ListPhoneDetailDirector.Count = 0 Then
                        Throw New Exception("Phone is Required")
                    End If
                    If ListAddressDetailDirector.Count = 0 Then
                        Throw New Exception("Address is Required")
                    End If
                    Dim DirectorClass As New WICDirectorDataBLL
                    'Dim objSaveDirector As New goAML_Ref_Walk_In_Customer_Director
                    With ObjDirector ' 2023-10-21, Nael
                        .Role = cmb_peran.Text
                        .Title = txt_Director_Title.Text
                        .Last_Name = txt_Director_Last_Name.Text
                        .Gender = cmb_Director_Gender.Text
                        If txt_Director_BirthDate.Text <> DateTime.MinValue Then
                            .BirthDate = txt_Director_BirthDate.Text
                        End If
                        .Birth_Place = txt_Director_Birth_Place.Text
                        .Mothers_Name = txt_Director_Mothers_Name.Text
                        .Alias = txt_Director_Alias.Text
                        .ID_Number = txt_Director_ID_Number.Text
                        .Passport_Number = txt_Director_Passport_Number.Text
                        .Passport_Country = txt_Director_Passport_Country.TextValue
                        .SSN = txt_Director_SSN.Text
                        .Nationality1 = cmb_Director_Nationality1.TextValue
                        .Nationality2 = cmb_Director_Nationality2.TextValue
                        .Nationality3 = cmb_Director_Nationality3.TextValue
                        .Residence = cmb_Director_Residence.TextValue
                        '.Email = txt_Director_Email.Text
                        '.Email2 = txt_Director_Email2.Text
                        '.Email3 = txt_Director_Email3.Text
                        '.Email4 = txt_Director_Email4.Text
                        '.Email5 = txt_Director_Email5.Text
                        .Deceased = cb_Director_Deceased.Checked
                        If cb_Director_Deceased.Checked Then
                            .Deceased_Date = txt_Director_Deceased_Date.Text
                        End If
                        .Tax_Reg_Number = cb_Director_Tax_Reg_Number.Checked
                        .Tax_Number = txt_Director_Tax_Number.Text
                        .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
                        .Occupation = txt_Director_Occupation.Text
                        .Comments = txt_Director_Comments.Text
                        'Daniel 2021 Jan 5
                        .Employer_Name = txt_Director_employer_name.Text
                        'end 2021 Jan 5
                    End With

                    Dim currentDirAddress = ListAddressDetailDirector.Where(Function(x) x.FK_To_Table_ID = ObjDirector.NO_ID).ToList()
                    Dim currentDirAddressEmp = ListAddressDetailDirectorEmployer.Where(Function(x) x.FK_To_Table_ID = ObjDirector.NO_ID).ToList()
                    Dim currentDirPhone = ListPhoneDetailDirector.Where(Function(x) x.FK_for_Table_ID = ObjDirector.NO_ID).ToList()
                    Dim currentDirPhoneEmp = ListPhoneDetailDirectorEmployer.Where(Function(x) x.FK_for_Table_ID = ObjDirector.NO_ID).ToList()
                    Dim currentDirIdentification = ListIdentificationDirector.Where(Function(x) x.FK_Person_ID = ObjDirector.NO_ID).ToList()

                    With DirectorClass
                        .ObjDirector = ObjDirector
                        .ListDirectorAddress = currentDirAddress
                        .ListDirectorAddressEmployer = currentDirAddressEmp
                        .ListDirectorPhone = currentDirPhone
                        .ListDirectorPhoneEmployer = currentDirPhoneEmp
                        .ListDirectorIdentification = currentDirIdentification
                        .ListDirectorEmail = GridEmail_Director.objListgoAML_Ref
                        .ListDirectorSanction = GridSanction_Director.objListgoAML_Ref
                        .ListDirectorPEP = GridPEP_Director.objListgoAML_Ref
                    End With

                    ListDirectorDetail.Add(DirectorClass.ObjDirector)
                    ListDirectorDetailClass.Add(DirectorClass)

                    BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
                    FP_Director.Hidden = True
                    WindowDetailDirector.Hidden = True
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Status As Boolean = True
            Dim wicData = New WICDataBLL
            Dim gridWIC = New WICDataBLL.goAML_Grid_WIC

            wicData = GetObjData()

            If FKCustomer.Text = 1 Then
                'If CmbINDV_Gender.Value = "" Then
                '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
                'If TxtINDV_last_name.Text = "" Then
                '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
                'ElseIf DateINDV_Birthdate.Text = "1/1/0001 12:00:00 AM" Or DateINDV_Birthdate.Value = DateTime.MinValue Then
                '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
                'ElseIf TxtINDV_Birth_place.Text = "" Then
                '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
                'ElseIf CmbINDV_Nationality1.TextValue = "" Then
                '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
                'ElseIf CmbINDV_residence.TextValue = "" Then
                '    Throw New Exception("Negara Domisili Tidak boleh kosong")
                'ElseIf TxtINDV_Occupation.Text = "" Then
                '    Throw New Exception("Pekerjaan Tidak boleh kosong")
                'ElseIf TxtINDV_Source_Of_Wealth.Text = "" Then
                '    Throw New Exception("Sumber Dana Tidak boleh kosong")
                'daniel 14 jan 2021
                'If CmbINDV_Gender.Value = "" Or TxtINDV_last_name.Text = "" Or TxtINDV_SSN.Text = "" Or DateINDV_Birthdate.Value = DateTime.MinValue Or TxtINDV_Occupation.Text = "" Or TxtINDV_Source_Of_Wealth.Text = "" Then
                '    Throw New Exception("Jenis Kelamin, Nama, NIK, Tanggal Lahir, pekerjaan, Sumber Penghasilan Tidak boleh kosong")
                'If CmbINDV_Gender.Value = "" Or TxtINDV_last_name.Text = "" Or DateINDV_Birthdate.Value = DateTime.MinValue Or TxtINDV_Occupation.Text = "" Or TxtINDV_Source_Of_Wealth.Text = "" Then
                '    Throw New Exception("Jenis Kelamin, Nama, NIK, Tanggal Lahir, pekerjaan, Sumber Penghasilan Tidak boleh kosong")
                'end 14 jan 2021
                'If CmbINDV_Gender.Value = "" Then
                '    Throw New Exception("Jenis Kelamin Tidak boleh kosong")
                'ElseIf TxtINDV_last_name.Text = "" Then
                '    Throw New Exception("Nama Lengkap Tidak boleh kosong")
                'ElseIf DateINDV_Birthdate.Text = "1/1/0001 12:00:00 AM" Then
                '    Throw New Exception("Tanggal Lahir Tidak boleh kosong")
                'ElseIf TxtINDV_Birth_place.Text = "" Then
                '    Throw New Exception("Tempat Lahir Tidak boleh kosong")
                'ElseIf CmbINDV_Nationality1.TextValue = "" Then
                '    Throw New Exception("Kewarganegaraan 1 Tidak boleh kosong")
                'ElseIf CmbINDV_residence.TextValue = "" Then
                '    Throw New Exception("Negara Domisili Tidak boleh kosong")
                'ElseIf ListPhoneDetail.Count <= 0 Then
                '    Throw New Exception("Informasi Telepon Tidak boleh kosong")
                'ElseIf ListAddressDetail.Count <= 0 Then
                '    Throw New Exception("Informasi Alamat Tidak boleh kosong")
                'ElseIf TxtINDV_Occupation.Text = "" Then
                '    Throw New Exception("Pekerjaan Tidak boleh kosong")
                'ElseIf TxtINDV_Source_Of_Wealth.Text = "" Then
                '    Throw New Exception("Sumber Dana Tidak boleh kosong")
                If TxtINDV_last_name.Text = "" Then
                    Throw New Exception("Full Name is Required")
                Else
                    If TxtINDV_SSN.Text.Length <> 16 And TxtINDV_SSN.Text.Length > 0 Then
                        Throw New Exception("NIK Must 16 Digit")
                    Else
                        If ListPhoneDetail Is Nothing Then
                            Throw New Exception("Phone Is Required")
                        End If
                        If ListAddressDetail Is Nothing Then
                            Throw New Exception("Address Is Required")
                        End If
                        If ListPhoneDetail.Count = 0 Then
                            Throw New Exception("Phone Is Required")
                        End If
                        If ListAddressDetail.Count = 0 Then
                            Throw New Exception("Address Is Required")
                        End If
                        Dim objSaveOld As New goAML_Ref_WIC
                        Dim ListAddressDetailOld As New List(Of goAML_Ref_Address)
                        Dim ListPhoneDetailOld As New List(Of goAML_Ref_Phone)
                        Dim ListAddressEmployerDetailOld As New List(Of goAML_Ref_Address)
                        Dim ListPhoneEmployerDetailOld As New List(Of goAML_Ref_Phone)
                        Dim ListIdentificationDetailOld As New List(Of goAML_Person_Identification)
                        Dim ListSanctionOld As New List(Of WICDataBLL.goAML_Ref_WIC_Sanction)
                        Dim ListEmailOld As New List(Of WICDataBLL.goAML_Ref_WIC_Email)
                        Dim ListPersonPEPOld As New List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP)
                        Dim ListRelatedPersonOld As New List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)

                        objSaveOld = ObjWIC
                        ListAddressDetailOld = objgoAML_WIC.GetWICByPKIDDetailAddress(objSaveOld.PK_Customer_ID)
                        ListPhoneDetailOld = objgoAML_WIC.GetWICByPKIDDetailPhone(objSaveOld.PK_Customer_ID)
                        ListAddressEmployerDetailOld = objgoAML_WIC.GetWICByPKIDDetailEmployerAddress(objSaveOld.PK_Customer_ID)
                        ListPhoneEmployerDetailOld = objgoAML_WIC.GetWICByPKIDDetailEmployerPhone(objSaveOld.PK_Customer_ID)
                        ListIdentificationDetailOld = objgoAML_WIC.GetWICByPKIDDetailIdentification(objSaveOld.PK_Customer_ID)
                        ListSanctionOld = GetWICByPKIDDetailSanction(objSaveOld.PK_Customer_ID)
                        ListEmailOld = GetWICByPKIDDetailEmail(objSaveOld.PK_Customer_ID)
                        ListPersonPEPOld = GetWICByPKIDDetailPersonPEP(objSaveOld.PK_Customer_ID)
                        ListRelatedPersonOld = GetWICByPKIDDetailRelatedPerson(objSaveOld.PK_Customer_ID)
                        Dim objSave As New goAML_Ref_WIC

                        With objSave
                            .PK_Customer_ID = PKWIC.Text
                            .isUpdateFromDataSource = CheckboxIsUpdateFromDataSource.Checked
                            '.Is_RealWIC = CheckboxIsRealWIC.Checked
                            .WIC_No = WICNo.Text
                            .INDV_Title = TxtINDV_Title.Text
                            .INDV_Last_Name = TxtINDV_last_name.Text
                            .INDV_Gender = CmbINDV_Gender.Text
                            'agam 02112020
                            If Not DateINDV_Birthdate.SelectedDate = DateTime.MinValue Then
                                .INDV_BirthDate = DateINDV_Birthdate.SelectedDate
                            End If
                            .INDV_Birth_Place = TxtINDV_Birth_place.Text
                            .INDV_Mothers_Name = TxtINDV_Mothers_name.Text
                            .INDV_Alias = TxtINDV_Alias.Text
                            .INDV_SSN = TxtINDV_SSN.Text
                            .INDV_Passport_Number = TxtINDV_Passport_number.Text
                            'Agam 20102020
                            '.INDV_Passport_Country = TxtINDV_Passport_country.Text
                            .INDV_Passport_Country = TxtINDV_Passport_country.TextValue
                            .INDV_ID_Number = TxtINDV_ID_Number.Value
                            .INDV_Nationality1 = CmbINDV_Nationality1.TextValue
                            .INDV_Nationality2 = CmbINDV_Nationality2.TextValue
                            .INDV_Nationality3 = CmbINDV_Nationality3.TextValue
                            .INDV_Residence = CmbINDV_residence.TextValue
                            '.INDV_Email = TxtINDV_Email.Text
                            '.INDV_Email2 = TxtINDV_Email2.Text
                            '.INDV_Email3 = TxtINDV_Email3.Text
                            '.INDV_Email4 = TxtINDV_Email4.Text
                            '.INDV_Email5 = TxtINDV_Email5.Text
                            .INDV_Occupation = TxtINDV_Occupation.Text
                            .INDV_Employer_Name = TxtINDV_employer_name.Text
                            .INDV_SumberDana = TxtINDV_Source_Of_Wealth.Text
                            .INDV_Tax_Number = TxtINDV_Tax_Number.Text
                            .INDV_Tax_Reg_Number = CbINDV_Tax_Reg_Number.Checked
                            .INDV_Deceased = CbINDV_Deceased.Checked
                            If CbINDV_Deceased.Checked Then
                                If DateINDV_Deceased_Date.Text <> DateTime.MinValue Then
                                    .INDV_Deceased_Date = DateINDV_Deceased_Date.Text
                                End If
                            End If
                            .INDV_Comment = TxtINDV_Comments.Text
                            If DateCorp_incorporation_date.RawValue <> DateTime.MinValue Then
                                .Corp_Incorporation_Date = DateCorp_incorporation_date.RawValue
                            End If
                            If DateCorp_date_business_closed.RawValue <> DateTime.MinValue Then
                                .Corp_Date_Business_Closed = DateCorp_date_business_closed.RawValue
                            End If
                            .FK_Customer_Type_ID = 1
                            .Active = True
                            .LastUpdateBy = Common.SessionCurrentUser.UserID
                            .LastUpdateDate = DateTime.Now
                        End With

                        'Cek Perubahan Person
                        With objSave
                            If .isUpdateFromDataSource <> objSaveOld.isUpdateFromDataSource Then
                                Status = False
                            End If
                            'If .Is_RealWIC <> objSaveOld.Is_RealWIC Then
                            '    Status = False
                            'End If
                            If .INDV_Title <> objSaveOld.INDV_Title Then
                                Status = False
                            End If
                            If .INDV_Last_Name <> objSaveOld.INDV_Last_Name Then
                                Status = False
                            End If
                            If .INDV_Gender <> objSaveOld.INDV_Gender Then
                                Status = False
                            End If
                            If .INDV_BirthDate <> objSaveOld.INDV_BirthDate Then
                                Status = False
                            End If
                            If .INDV_Birth_Place <> objSaveOld.INDV_Birth_Place Then
                                Status = False
                            End If
                            If .INDV_Mothers_Name <> objSaveOld.INDV_Mothers_Name Then
                                Status = False
                            End If
                            If .INDV_Alias <> objSaveOld.INDV_Alias Then
                                Status = False
                            End If
                            If .INDV_SSN <> objSaveOld.INDV_SSN Then
                                Status = False
                            End If
                            If .INDV_Passport_Number <> objSaveOld.INDV_Passport_Number Then
                                Status = False
                            End If
                            If .INDV_Passport_Country <> objSaveOld.INDV_Passport_Country Then
                                Status = False
                            End If
                            If .INDV_ID_Number <> objSaveOld.INDV_ID_Number Then
                                Status = False
                            End If
                            If .INDV_Nationality1 <> objSaveOld.INDV_Nationality1 Then
                                Status = False
                            End If
                            If .INDV_Nationality2 <> objSaveOld.INDV_Nationality2 Then
                                Status = False
                            End If
                            If .INDV_Nationality3 <> objSaveOld.INDV_Nationality3 Then
                                Status = False
                            End If
                            If .INDV_Residence <> objSaveOld.INDV_Residence Then
                                Status = False
                            End If
                            If .INDV_Email <> objSaveOld.INDV_Email Then
                                Status = False
                            End If
                            If .INDV_Email2 <> objSaveOld.INDV_Email2 Then
                                Status = False
                            End If
                            If .INDV_Email3 <> objSaveOld.INDV_Email3 Then
                                Status = False
                            End If
                            If .INDV_Email4 <> objSaveOld.INDV_Email4 Then
                                Status = False
                            End If
                            If .INDV_Email5 <> objSaveOld.INDV_Email5 Then
                                Status = False
                            End If
                            If .INDV_Occupation <> objSaveOld.INDV_Occupation Then
                                Status = False
                            End If
                            If .INDV_Employer_Name <> objSaveOld.INDV_Employer_Name Then
                                Status = False
                            End If
                            If .INDV_SumberDana <> objSaveOld.INDV_SumberDana Then
                                Status = False
                            End If
                            If .INDV_Tax_Number <> objSaveOld.Corp_Tax_Number Then
                                Status = False
                            End If
                            If .INDV_Tax_Reg_Number <> objSaveOld.INDV_Tax_Reg_Number Then
                                Status = False
                            End If
                            If .INDV_Deceased <> objSaveOld.INDV_Deceased Then
                                Status = False
                            End If
                            If .INDV_Deceased_Date <> objSaveOld.INDV_Deceased_Date Then
                                Status = False
                            End If
                            If .INDV_Comment <> objSaveOld.INDV_Comment Then
                                Status = False
                            End If
                        End With

                        If ListPhoneDetailOld.Count = ListPhoneDetail.Count Then
                            Dim phone As New goAML_Ref_Phone
                            For Each item In ListPhoneDetail
                                phone = ListPhoneDetailOld.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                                'agam 22102020
                                If phone Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                If phone.FK_Ref_Detail_Of IsNot Nothing Then
                                    With phone
                                        If .Tph_Contact_Type <> item.Tph_Contact_Type Then
                                            Status = False
                                        End If
                                        If .Tph_Communication_Type <> item.Tph_Communication_Type Then
                                            Status = False
                                        End If
                                        If .tph_country_prefix <> item.tph_country_prefix Then
                                            Status = False
                                        End If
                                        If .tph_number <> item.tph_number Then
                                            Status = False
                                        End If
                                        If .tph_extension <> item.tph_extension Then
                                            Status = False
                                        End If
                                        If .comments <> item.comments Then
                                            Status = False
                                        End If
                                    End With
                                Else
                                    Status = False
                                End If
                                phone = New goAML_Ref_Phone
                            Next
                        Else
                            Status = False
                        End If

                        If ListPhoneEmployerDetailOld.Count = ListPhoneEmployerDetail.Count Then
                            Dim phoneEmp As New goAML_Ref_Phone
                            For Each item In ListPhoneEmployerDetail
                                phoneEmp = ListPhoneEmployerDetailOld.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                                'agam 22102020
                                If phoneEmp Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                If phoneEmp.FK_Ref_Detail_Of IsNot Nothing Then
                                    With phoneEmp
                                        If .Tph_Contact_Type <> item.Tph_Contact_Type Then
                                            Status = False
                                        End If
                                        If .Tph_Communication_Type <> item.Tph_Communication_Type Then
                                            Status = False
                                        End If
                                        If .tph_country_prefix <> item.tph_country_prefix Then
                                            Status = False
                                        End If
                                        If .tph_number <> item.tph_number Then
                                            Status = False
                                        End If
                                        If .tph_extension <> item.tph_extension Then
                                            Status = False
                                        End If
                                        If .comments <> item.comments Then
                                            Status = False
                                        End If
                                    End With
                                Else
                                    Status = False
                                End If
                                phoneEmp = New goAML_Ref_Phone
                            Next
                        Else
                            Status = False
                        End If

                        If ListAddressDetailOld.Count = ListAddressDetail.Count Then
                            Dim Address As New goAML_Ref_Address
                            For Each item In ListAddressDetail
                                Address = ListAddressDetailOld.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                                'agam 21102020
                                If Address Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                If Address IsNot Nothing Then
                                    If Address.FK_Ref_Detail_Of IsNot Nothing Then
                                        With Address
                                            If .Address_Type <> item.Address_Type Then
                                                Status = False
                                            End If
                                            If .Address <> item.Address Then
                                                Status = False
                                            End If
                                            If .Town <> item.Town Then
                                                Status = False
                                            End If
                                            If .City <> item.City Then
                                                Status = False
                                            End If
                                            If .Zip <> item.Zip Then
                                                Status = False
                                            End If
                                            If .Country_Code <> item.Country_Code Then
                                                Status = False
                                            End If
                                            If .State <> item.State Then
                                                Status = False
                                            End If
                                            If .Comments <> item.Comments Then
                                                Status = False
                                            End If
                                        End With
                                    Else
                                        Status = False
                                    End If
                                End If
                                Address = New goAML_Ref_Address
                            Next
                        Else
                            Status = False
                        End If

                        If ListAddressEmployerDetailOld.Count = ListAddressEmployerDetail.Count Then
                            Dim AddressEmp As New goAML_Ref_Address
                            For Each item In ListAddressEmployerDetail
                                AddressEmp = ListAddressEmployerDetailOld.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                                'agam 22102020
                                If AddressEmp Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                If AddressEmp.FK_Ref_Detail_Of IsNot Nothing Then
                                    With AddressEmp
                                        If .Address_Type <> item.Address_Type Then
                                            Status = False
                                        End If
                                        If .Address <> item.Address Then
                                            Status = False
                                        End If
                                        If .Town <> item.Town Then
                                            Status = False
                                        End If
                                        If .City <> item.City Then
                                            Status = False
                                        End If
                                        If .Zip <> item.Zip Then
                                            Status = False
                                        End If
                                        If .Country_Code <> item.Country_Code Then
                                            Status = False
                                        End If
                                        If .State <> item.State Then
                                            Status = False
                                        End If
                                        If .Comments <> item.Comments Then
                                            Status = False
                                        End If
                                    End With
                                Else
                                    Status = False
                                End If
                                AddressEmp = New goAML_Ref_Address
                            Next
                        Else
                            Status = False
                        End If

                        If ListIdentificationDetailOld.Count = ListIdentificationDetail.Count Then
                            Dim Identification As New goAML_Person_Identification
                            For Each item In ListIdentificationDetail
                                Identification = ListIdentificationDetailOld.Where(Function(x) x.PK_Person_Identification_ID = item.PK_Person_Identification_ID).FirstOrDefault
                                'agam 22102020
                                If Identification Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                If Identification.FK_Person_ID <> Nothing Then
                                    With Identification
                                        If .Type <> item.Type Then
                                            Status = False
                                        End If
                                        If .Number <> item.Number Then
                                            Status = False
                                        End If
                                        If .Issue_Date <> item.Issue_Date Then
                                            Status = False
                                        End If
                                        If .Expiry_Date <> item.Expiry_Date Then
                                            Status = False
                                        End If
                                        If .Issued_By <> item.Issued_By Then
                                            Status = False
                                        End If
                                        If .Issued_Country <> item.Issued_Country Then
                                            Status = False
                                        End If
                                        If .Identification_Comment <> item.Identification_Comment Then
                                            Status = False
                                        End If
                                    End With
                                Else
                                    Status = False
                                End If
                                Identification = New goAML_Person_Identification
                            Next
                        Else
                            Status = False
                        End If

                        'New add by Asep Maulana 150223
                        If ListSanctionOld.Count = obj_ListSanction_Edit.Count Then
                            Dim Sanction As New WICDataBLL.goAML_Ref_WIC_Sanction
                            For Each item In obj_ListSanction_Edit
                                Sanction = ListSanctionOld.Where(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = item.PK_goAML_Ref_WIC_Sanction_ID).FirstOrDefault
                                If Sanction Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                With Sanction
                                    If .Provider <> item.Provider Then
                                        Status = False
                                    End If
                                    If .Sanction_List_Name <> item.Sanction_List_Name Then
                                        Status = False
                                    End If
                                    If .Match_Criteria <> item.Match_Criteria Then
                                        Status = False
                                    End If
                                    If .Link_To_Source <> item.Link_To_Source Then
                                        Status = False
                                    End If
                                    If .Sanction_List_Attributes <> item.Sanction_List_Attributes Then
                                        Status = False
                                    End If
                                    If .Sanction_List_Date_Range_Valid_From <> item.Sanction_List_Date_Range_Valid_From Then
                                        Status = False
                                    End If
                                    If .Sanction_List_Date_Range_Is_Approx_From_Date <> item.Sanction_List_Date_Range_Is_Approx_From_Date Then
                                        Status = False
                                    End If
                                    If .Sanction_List_Date_Range_Valid_To <> item.Sanction_List_Date_Range_Valid_To Then
                                        Status = False
                                    End If
                                    If .Sanction_List_Date_Range_Is_Approx_To_Date <> item.Sanction_List_Date_Range_Is_Approx_To_Date Then
                                        Status = False
                                    End If
                                    If .Comments <> item.Comments Then
                                        Status = False
                                    End If
                                End With
                                Sanction = New WICDataBLL.goAML_Ref_WIC_Sanction
                            Next
                        Else
                            Status = False
                        End If

                        If ListEmailOld.Count = obj_ListEmail_Edit.Count Then
                            Dim Email As New WICDataBLL.goAML_Ref_WIC_Email
                            For Each item In obj_ListEmail_Edit
                                Email = ListEmailOld.Where(Function(x) x.PK_goAML_Ref_WIC_Email_ID = item.PK_goAML_Ref_WIC_Email_ID).FirstOrDefault
                                If Email Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                With Email
                                    If .email_address <> item.email_address Then
                                        Status = False
                                    End If
                                End With
                                Email = New WICDataBLL.goAML_Ref_WIC_Email
                            Next
                        Else
                            Status = False
                        End If

                        If ListPersonPEPOld.Count = obj_ListPersonPEP_Edit.Count Then
                            Dim PersonPEP As New WICDataBLL.goAML_Ref_WIC_Person_PEP
                            For Each item In obj_ListPersonPEP_Edit
                                PersonPEP = ListPersonPEPOld.Where(Function(x) x.PK_goAML_Ref_WIC_PEP_ID = item.PK_goAML_Ref_WIC_PEP_ID).FirstOrDefault
                                If PersonPEP Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                With PersonPEP
                                    If .pep_country <> item.pep_country Then
                                        Status = False
                                    End If
                                    If .function_name <> item.function_name Then
                                        Status = False
                                    End If
                                    If .function_description <> item.function_description Then
                                        Status = False
                                    End If
                                    If .pep_date_range_valid_from <> item.pep_date_range_valid_from Then
                                        Status = False
                                    End If
                                    If .pep_date_range_is_approx_from_date <> item.pep_date_range_is_approx_from_date Then
                                        Status = False
                                    End If
                                    If .pep_date_range_valid_to <> item.pep_date_range_valid_to Then
                                        Status = False
                                    End If
                                    If .pep_date_range_is_approx_to_date <> item.pep_date_range_is_approx_to_date Then
                                        Status = False
                                    End If
                                    If .comments <> item.comments Then
                                        Status = False
                                    End If
                                End With
                                PersonPEP = New WICDataBLL.goAML_Ref_WIC_Person_PEP
                            Next
                        Else
                            Status = False
                        End If

                        If ListRelatedPersonOld.Count = obj_ListRelatedPerson_Edit.Count Then
                            Dim RelatedPerson As New WICDataBLL.goAML_Ref_WIC_Related_Person
                            For Each item In obj_ListRelatedPerson_Edit
                                RelatedPerson = ListRelatedPersonOld.Where(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = item.PK_goAML_Ref_WIC_Related_Person_ID).FirstOrDefault
                                If RelatedPerson Is Nothing Then
                                    Status = False
                                    Exit For
                                End If
                                With RelatedPerson
                                    If .Person_Person_Relation <> item.Person_Person_Relation Then
                                        Status = False
                                    End If
                                    If .Relation_Date_Range_Valid_From <> item.Relation_Date_Range_Valid_From Then
                                        Status = False
                                    End If
                                    If .Relation_Date_Range_Is_Approx_From_Date <> item.Relation_Date_Range_Is_Approx_From_Date Then
                                        Status = False
                                    End If
                                    If .Relation_Date_Range_Valid_To <> item.Relation_Date_Range_Valid_To Then
                                        Status = False
                                    End If
                                    If .Relation_Date_Range_Is_Approx_To_Date <> item.Relation_Date_Range_Is_Approx_To_Date Then
                                        Status = False
                                    End If
                                    If .Comments <> item.Comments Then
                                        Status = False
                                    End If
                                    If .Gender <> item.Gender Then
                                        Status = False
                                    End If
                                    If .Title <> item.Title Then
                                        Status = False
                                    End If
                                    If .First_Name <> item.First_Name Then
                                        Status = False
                                    End If
                                    If .Middle_Name <> item.Middle_Name Then
                                        Status = False
                                    End If
                                    If .Prefix <> item.Prefix Then
                                        Status = False
                                    End If
                                    If .Last_Name <> item.Last_Name Then
                                        Status = False
                                    End If
                                    If .Birth_Date <> item.Birth_Date Then
                                        Status = False
                                    End If
                                    If .Birth_Place <> item.Birth_Place Then
                                        Status = False
                                    End If
                                    If .Country_Of_Birth <> item.Country_Of_Birth Then
                                        Status = False
                                    End If
                                    If .Mother_Name <> item.Mother_Name Then
                                        Status = False
                                    End If
                                    If ._Alias <> item._Alias Then
                                        Status = False
                                    End If
                                    If .Full_Name_Frn <> item.Full_Name_Frn Then
                                        Status = False
                                    End If
                                    If .SSN <> item.SSN Then
                                        Status = False
                                    End If
                                    If .Passport_Number <> item.Passport_Number Then
                                        Status = False
                                    End If
                                    If .Passport_Country <> item.Passport_Country Then
                                        Status = False
                                    End If
                                    If .ID_Number <> item.ID_Number Then
                                        Status = False
                                    End If
                                    If .Nationality1 <> item.Nationality1 Then
                                        Status = False
                                    End If
                                    If .Nationality2 <> item.Nationality2 Then
                                        Status = False
                                    End If
                                    If .Nationality3 <> item.Nationality3 Then
                                        Status = False
                                    End If
                                    If .Residence <> item.Residence Then
                                        Status = False
                                    End If
                                    If .Residence_Since <> item.Residence_Since Then
                                        Status = False
                                    End If
                                    If .Occupation <> item.Occupation Then
                                        Status = False
                                    End If
                                    If .Deceased <> item.Deceased Then
                                        Status = False
                                    End If
                                    If .Date_Deceased <> item.Date_Deceased Then
                                        Status = False
                                    End If
                                    If .Tax_Number <> item.Tax_Number Then
                                        Status = False
                                    End If
                                    If .Is_PEP <> item.Is_PEP Then
                                        Status = False
                                    End If
                                    If .Source_Of_Wealth <> item.Source_Of_Wealth Then
                                        Status = False
                                    End If
                                    If .Is_Protected <> item.Is_Protected Then
                                        Status = False
                                    End If
                                End With
                                RelatedPerson = New WICDataBLL.goAML_Ref_WIC_Related_Person
                            Next
                        Else
                            Status = False
                        End If

                        Dim NewSanctionData As Long = 0
                        For Each item In obj_ListSanction_Edit
                            If item.PK_goAML_Ref_WIC_Sanction_ID < 1 Then
                                NewSanctionData = NewSanctionData + 1
                            End If
                        Next
                        If NewSanctionData > 0 Then
                            Status = False
                        End If

                        Dim NewEmailData As Long = 0
                        For Each itemEmail In obj_ListEmail_Edit
                            If itemEmail.PK_goAML_Ref_WIC_Email_ID < 1 Then
                                NewEmailData = NewEmailData + 1
                            End If
                        Next
                        If NewEmailData > 0 Then
                            Status = False
                        End If

                        Dim NewPersonPEPData As Long = 0
                        For Each itemPersonPEP In obj_ListPersonPEP_Edit
                            If itemPersonPEP.PK_goAML_Ref_WIC_PEP_ID < 1 Then
                                NewPersonPEPData = NewPersonPEPData + 1
                            End If
                        Next
                        If NewPersonPEPData > 0 Then
                            Status = False
                        End If

                        Dim NewRelatedPersonData As Long = 0
                        For Each itemRelatedPerson In obj_ListRelatedPerson_Edit
                            If itemRelatedPerson.PK_goAML_Ref_WIC_Related_Person_ID < 1 Then
                                NewRelatedPersonData = NewRelatedPersonData + 1
                            End If
                        Next
                        If NewRelatedPersonData > 0 Then
                            Status = False
                        End If
                        'New add 100223 by Asep Maulana

                        'goAML 5.0.1, Add by Septian, 2023-03-10
                        If Status Then
                            Status = Not IsFormChanged(wicData)
                        End If
                        'end Add

                        If Status Then
                            Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                        End If
                        If Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                            objgoAML_WIC.SaveEditTanpaApproval(objSave, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule, wicData)
                            Dim strQuery = ""
                            Dim strQuery2 = ""
                            Dim strQuery3 = ""
                            Dim strQuery4 = ""
                            Dim strQuery5 = ""

                            'gridWIC = GetObjGrid()

                            WIC_Panel.Hide()
                            Panelconfirmation.Show()
                            LblConfirmation.Text = "Data Saved into Database"
                        Else
                            objgoAML_WIC.SaveEditApproval(objSave, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule, wicData)
                            WIC_Panel.Hide()
                            Panelconfirmation.Show()
                            LblConfirmation.Text = "Data Saved into Pending Approval"
                        End If

                    End If
                End If
            Else
                'daniel 11 jan 2021
                'If TxtCorp_Name.Text = "" Then
                '    Throw New Exception("Nama Korporasi Tidak boleh kosong")
                'end 11 jan 2021
                'If TxtCorp_Name.Text = "" Then
                '    Throw New Exception("Nama Korporasi Tidak boleh kosong")
                'ElseIf TxtCorp_Business.Text = "" Then
                '    Throw New Exception("Bidang Usaha Tidak boleh kosong")
                If TxtCorp_Name.Text = "" Then
                    Throw New Exception("Nama Korporasi Tidak boleh kosong")
                    'ElseIf TxtCorp_Business.Text = "" Then
                    '    Throw New Exception("Bidang Usaha Tidak boleh kosong")
                    'ElseIf ListAddressDetail.Count <= 0 Then
                    '    Throw New Exception("Informasi Alamat Tidak boleh kosong")
                    'ElseIf CmbCorp_incorporation_country_code.TextValue = "" Then
                    '    Throw New Exception("Negara Tidak boleh kosong")
                Else
                    Dim objSaveOld As New goAML_Ref_WIC
                    Dim objSave As New goAML_Ref_WIC
                    Dim ListAddressDetailOld As New List(Of goAML_Ref_Address)
                    Dim ListPhoneDetailOld As New List(Of goAML_Ref_Phone)

                    objSaveOld = ObjWIC
                    ListAddressDetailOld = objgoAML_WIC.GetWICByPKIDDetailAddress(objSaveOld.PK_Customer_ID)
                    ListPhoneDetailOld = objgoAML_WIC.GetWICByPKIDDetailPhone(objSaveOld.PK_Customer_ID)

                    With objSave
                        .PK_Customer_ID = PKWIC.Text
                        .isUpdateFromDataSource = CheckboxIsUpdateFromDataSource.Checked
                        '.Is_RealWIC = CheckboxIsRealWIC.Checked
                        'daniel 13 jan 2021
                        .WIC_No = WICNo.Text
                        'end 13 jan 2021
                        .Corp_Name = TxtCorp_Name.Text
                        .Corp_Commercial_Name = TxtCorp_Commercial_name.Text
                        .Corp_Incorporation_Legal_Form = CmbCorp_Incorporation_legal_form.Value
                        .Corp_Incorporation_Number = TxtCorp_Incorporation_number.Text
                        .Corp_Business = TxtCorp_Business.Text
                        '.Corp_Email = TxtCorp_Email.Text
                        '.Corp_Url = TxtCorp_url.Text
                        .Corp_Incorporation_State = TxtCorp_incorporation_state.Text
                        .Corp_Incorporation_Country_Code = CmbCorp_incorporation_country_code.TextValue
                        If DateCorp_incorporation_date.Value <> DateTime.MinValue Then
                            .Corp_Incorporation_Date = DateCorp_incorporation_date.Value
                        End If
                        .Corp_Business_Closed = chkCorp_business_closed.Value
                        If chkCorp_business_closed.Checked Then
                            If DateCorp_date_business_closed.Value <> DateTime.MinValue Then
                                .Corp_Date_Business_Closed = DateCorp_date_business_closed.Value
                            End If
                        End If
                        .Corp_Tax_Number = TxtCorp_tax_number.Text
                        .Corp_Comments = TxtCorp_Comments.Text
                        .FK_Customer_Type_ID = 2
                        If DateINDV_Birthdate.RawValue <> DateTime.MinValue Then
                            .INDV_BirthDate = DateINDV_Birthdate.RawValue
                        End If
                    End With

                    'Cek Perubahan Korporasi
                    With objSave
                        If .isUpdateFromDataSource <> objSaveOld.isUpdateFromDataSource Then
                            Status = False
                        End If
                        If .Corp_Name <> objSaveOld.Corp_Name Then
                            Status = False
                        End If
                        If .Corp_Commercial_Name <> objSaveOld.Corp_Commercial_Name Then
                            Status = False
                        End If
                        If .Corp_Incorporation_Legal_Form <> objSaveOld.Corp_Incorporation_Legal_Form Then
                            Status = False
                        End If
                        If .Corp_Incorporation_Number <> objSaveOld.Corp_Incorporation_Number Then
                            Status = False
                        End If
                        If .Corp_Business <> objSaveOld.Corp_Business Then
                            Status = False
                        End If
                        If .Corp_Email <> objSaveOld.Corp_Email Then
                            Status = False
                        End If
                        If .Corp_Url <> objSaveOld.Corp_Url Then
                            Status = False
                        End If
                        If .Corp_Incorporation_State <> objSaveOld.Corp_Incorporation_State Then
                            Status = False
                        End If
                        If .Corp_Incorporation_Country_Code <> objSaveOld.Corp_Incorporation_Country_Code Then
                            Status = False
                        End If
                        If .Corp_Incorporation_Date <> objSaveOld.Corp_Incorporation_Date Then
                            Status = False
                        End If
                        If .Corp_Business_Closed <> objSaveOld.Corp_Business_Closed Then
                            Status = False
                        End If
                        If .Corp_Date_Business_Closed <> objSaveOld.Corp_Date_Business_Closed Then
                            Status = False
                        End If
                        If .Corp_Tax_Number <> objSaveOld.Corp_Tax_Number Then
                            Status = False
                        End If
                        If .Corp_Comments <> objSaveOld.Corp_Comments Then
                            Status = False
                        End If
                    End With

                    If ListPhoneDetailOld.Count = ListPhoneDetail.Count Then
                        Dim phone As New goAML_Ref_Phone
                        For Each item In ListPhoneDetail
                            phone = ListPhoneDetailOld.Where(Function(x) x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone).FirstOrDefault
                            'agam 22102020
                            If phone Is Nothing Then
                                Status = False
                                Exit For
                            End If
                            If phone.FK_Ref_Detail_Of IsNot Nothing Then
                                With phone
                                    If .Tph_Contact_Type <> item.Tph_Contact_Type Then
                                        Status = False
                                    End If
                                    If .Tph_Communication_Type <> item.Tph_Communication_Type Then
                                        Status = False
                                    End If
                                    If .tph_country_prefix <> item.tph_country_prefix Then
                                        Status = False
                                    End If
                                    If .tph_number <> item.tph_number Then
                                        Status = False
                                    End If
                                    If .tph_extension <> item.tph_extension Then
                                        Status = False
                                    End If
                                    If .comments <> item.comments Then
                                        Status = False
                                    End If
                                End With
                            Else
                                Status = False
                            End If
                            phone = New goAML_Ref_Phone
                        Next
                    Else
                        Status = False
                    End If

                    If ListAddressDetailOld.Count = ListAddressDetail.Count Then
                        Dim Address As New goAML_Ref_Address
                        For Each item In ListAddressDetail
                            Address = ListAddressDetailOld.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                            If Address Is Nothing Then
                                Status = False
                                Exit For
                            End If
                            If Address.FK_Ref_Detail_Of IsNot Nothing Then
                                With Address
                                    If .Address_Type <> item.Address_Type Then
                                        Status = False
                                    End If
                                    If .Address <> item.Address Then
                                        Status = False
                                    End If
                                    If .Town <> item.Town Then
                                        Status = False
                                    End If
                                    If .City <> item.City Then
                                        Status = False
                                    End If
                                    If .Zip <> item.Zip Then
                                        Status = False
                                    End If
                                    If .Country_Code <> item.Country_Code Then
                                        Status = False
                                    End If
                                    If .State <> item.State Then
                                        Status = False
                                    End If
                                    If .Comments <> item.Comments Then
                                        Status = False
                                    End If
                                End With
                            Else
                                Status = False
                            End If
                            Address = New goAML_Ref_Address
                        Next
                    Else
                        Status = False
                    End If

                    'Cek Perubahan Director
                    Dim ListDirectorDetailOld As New List(Of goAML_Ref_Walk_In_Customer_Director)
                    Dim ListDirectorAddressOld As New List(Of goAML_Ref_Address)
                    Dim ListDirectorAddressEmployerOld As New List(Of goAML_Ref_Address)
                    Dim ListDirectorPhoneOld As New List(Of goAML_Ref_Phone)
                    Dim ListDirectorPhoneEmployerOld As New List(Of goAML_Ref_Phone)
                    Dim ListDirectorIdentificationOld As New List(Of goAML_Person_Identification)
                    Dim Director As New goAML_Ref_Walk_In_Customer_Director
                    Dim AddressDir As New goAML_Ref_Address
                    Dim AddressDirEmp As New goAML_Ref_Address
                    Dim PhoneDir As New goAML_Ref_Phone
                    Dim PhoneDirEmp As New goAML_Ref_Phone
                    Dim IdentDir As New goAML_Person_Identification

                    ListDirectorDetailOld = objgoAML_WIC.GetWICByPKIDDetailDirector(objSaveOld.PK_Customer_ID)

                    For Each item In ListDirectorDetailClass
                        Director = ListDirectorDetailOld.Where(Function(x) x.NO_ID = item.ObjDirector.NO_ID).FirstOrDefault
                        'agam 22102020
                        If Director Is Nothing Then
                            Status = False
                            Exit For
                        End If
                        If Director IsNot Nothing Then
                            ListDirectorAddressOld = objgoAML_WIC.GetWICByPKIDDetailDirectorAddress(item.ObjDirector.NO_ID)
                            ListDirectorAddressEmployerOld = objgoAML_WIC.GetWICByPKIDDetailDirectorAddressEmployer(item.ObjDirector.NO_ID)
                            ListDirectorPhoneOld = objgoAML_WIC.GetWICByPKIDDetailDirectorPhone(item.ObjDirector.NO_ID)
                            ListDirectorPhoneEmployerOld = objgoAML_WIC.GetWICByPKIDDetailDirectorPhoneEmployer(item.ObjDirector.NO_ID)
                            ListDirectorIdentificationOld = objgoAML_WIC.GetWICByPKIDDetailDirectorIdentification(item.ObjDirector.NO_ID)

                            'Director
                            With item.ObjDirector
                                If .Role <> Director.Role Then
                                    Status = False
                                End If
                                If .Gender <> Director.Gender Then
                                    Status = False
                                End If
                                If .Title <> Director.Title Then
                                    Status = False
                                End If
                                If .Last_Name <> Director.Last_Name Then
                                    Status = False
                                End If
                                If .BirthDate <> Director.BirthDate Then
                                    Status = False
                                End If
                                If .Birth_Place <> Director.Birth_Place Then
                                    Status = False
                                End If
                                If .Mothers_Name <> Director.Mothers_Name Then
                                    Status = False
                                End If
                                If .Alias <> Director.Alias Then
                                    Status = False
                                End If
                                If .SSN <> Director.SSN Then
                                    Status = False
                                End If
                                If .Passport_Number <> Director.Passport_Number Then
                                    Status = False
                                End If
                                If .Passport_Country <> Director.Passport_Country Then
                                    Status = False
                                End If
                                If .ID_Number <> Director.ID_Number Then
                                    Status = False
                                End If
                                If .Nationality1 <> Director.Nationality1 Then
                                    Status = False
                                End If
                                If .Nationality2 <> Director.Nationality2 Then
                                    Status = False
                                End If
                                If .Nationality3 <> Director.Nationality3 Then
                                    Status = False
                                End If
                                If .Residence <> Director.Residence Then
                                    Status = False
                                End If
                                If .Email <> Director.Email Then
                                    Status = False
                                End If
                                If .Email2 <> Director.Email2 Then
                                    Status = False
                                End If
                                If .Email3 <> Director.Email3 Then
                                    Status = False
                                End If
                                If .Email4 <> Director.Email4 Then
                                    Status = False
                                End If
                                If .Email5 <> Director.Email5 Then
                                    Status = False
                                End If
                                If .Occupation <> Director.Occupation Then
                                    Status = False
                                End If
                                If .Employer_Name <> Director.Employer_Name Then
                                    Status = False
                                End If
                                If .Deceased <> Director.Deceased Then
                                    Status = False
                                End If
                                If .Deceased_Date <> Director.Deceased_Date Then
                                    Status = False
                                End If
                                If .Tax_Number <> Director.Tax_Number Then
                                    Status = False
                                End If
                                If .Tax_Reg_Number <> Director.Tax_Reg_Number Then
                                    Status = False
                                End If
                                If .Source_of_Wealth <> Director.Source_of_Wealth Then
                                    Status = False
                                End If
                                If .Comments <> Director.Comments Then
                                    Status = False
                                End If
                            End With

                            'Director Adress
                            If item.ListDirectorAddress.Count = ListDirectorAddressOld.Count Then

                                For Each itemx In item.ListDirectorAddress
                                    AddressDir = ListDirectorAddressOld.Where(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID).FirstOrDefault
                                    'agam 22102020
                                    If AddressDir Is Nothing Then
                                        Status = False
                                        Exit For
                                    End If
                                    If AddressDir.FK_Ref_Detail_Of IsNot Nothing Then
                                        With AddressDir
                                            If .Address_Type <> itemx.Address_Type Then
                                                Status = False
                                            End If
                                            If .Address <> itemx.Address Then
                                                Status = False
                                            End If
                                            If .Town <> itemx.Town Then
                                                Status = False
                                            End If
                                            If .City <> itemx.City Then
                                                Status = False
                                            End If
                                            If .Zip <> itemx.Zip Then
                                                Status = False
                                            End If
                                            If .Country_Code <> itemx.Country_Code Then
                                                Status = False
                                            End If
                                            If .State <> itemx.State Then
                                                Status = False
                                            End If
                                            If .Comments <> itemx.Comments Then
                                                Status = False
                                            End If
                                        End With
                                    Else
                                        Status = False
                                    End If
                                    AddressDir = New goAML_Ref_Address
                                Next
                            Else
                                Status = False
                            End If

                            'Director Adress Emp
                            If item.ListDirectorAddressEmployer.Count = ListDirectorAddressEmployerOld.Count Then

                                For Each itemx In item.ListDirectorAddressEmployer
                                    AddressDirEmp = ListDirectorAddressEmployerOld.Where(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID).FirstOrDefault
                                    'agam 22102020
                                    If AddressDirEmp Is Nothing Then
                                        Status = False
                                        Exit For
                                    End If
                                    If AddressDirEmp.FK_Ref_Detail_Of IsNot Nothing Then
                                        With AddressDirEmp
                                            If .Address_Type <> itemx.Address_Type Then
                                                Status = False
                                            End If
                                            If .Address <> itemx.Address Then
                                                Status = False
                                            End If
                                            If .Town <> itemx.Town Then
                                                Status = False
                                            End If
                                            If .City <> itemx.City Then
                                                Status = False
                                            End If
                                            If .Zip <> itemx.Zip Then
                                                Status = False
                                            End If
                                            If .Country_Code <> itemx.Country_Code Then
                                                Status = False
                                            End If
                                            If .State <> itemx.State Then
                                                Status = False
                                            End If
                                            If .Comments <> itemx.Comments Then
                                                Status = False
                                            End If
                                        End With
                                    Else
                                        Status = False
                                    End If

                                    AddressDirEmp = New goAML_Ref_Address
                                Next
                            Else
                                Status = False
                            End If

                            'Director Phone
                            If item.ListDirectorPhone.Count = ListDirectorPhoneOld.Count Then
                                For Each itemx In item.ListDirectorPhone
                                    PhoneDir = ListDirectorPhoneOld.Where(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone).FirstOrDefault
                                    'agam 22102020
                                    If PhoneDir Is Nothing Then
                                        Status = False
                                        Exit For
                                    End If
                                    If PhoneDir.FK_Ref_Detail_Of IsNot Nothing Then
                                        With PhoneDir
                                            If .Tph_Contact_Type <> itemx.Tph_Contact_Type Then
                                                Status = False
                                            End If
                                            If .Tph_Communication_Type <> itemx.Tph_Communication_Type Then
                                                Status = False
                                            End If
                                            If .tph_country_prefix <> itemx.tph_country_prefix Then
                                                Status = False
                                            End If
                                            If .tph_number <> itemx.tph_number Then
                                                Status = False
                                            End If
                                            If .tph_extension <> itemx.tph_extension Then
                                                Status = False
                                            End If
                                            If .comments <> itemx.comments Then
                                                Status = False
                                            End If
                                        End With
                                    Else
                                        Status = False
                                    End If
                                    PhoneDir = New goAML_Ref_Phone
                                Next
                            Else
                                Status = False
                            End If

                            'Director Phone Emp
                            If item.ListDirectorPhoneEmployer.Count = ListDirectorPhoneEmployerOld.Count Then
                                For Each itemx In item.ListDirectorPhoneEmployer
                                    PhoneDirEmp = ListDirectorPhoneEmployerOld.Where(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone).FirstOrDefault
                                    'agam 22102020
                                    If PhoneDirEmp Is Nothing Then
                                        Status = False
                                        Exit For
                                    End If
                                    If PhoneDirEmp.FK_Ref_Detail_Of IsNot Nothing Then
                                        With PhoneDirEmp
                                            If .Tph_Contact_Type <> itemx.Tph_Contact_Type Then
                                                Status = False
                                            End If
                                            If .Tph_Communication_Type <> itemx.Tph_Communication_Type Then
                                                Status = False
                                            End If
                                            If .tph_country_prefix <> itemx.tph_country_prefix Then
                                                Status = False
                                            End If
                                            If .tph_number <> itemx.tph_number Then
                                                Status = False
                                            End If
                                            If .tph_extension <> itemx.tph_extension Then
                                                Status = False
                                            End If
                                            If .comments <> itemx.comments Then
                                                Status = False
                                            End If
                                        End With
                                    Else
                                        Status = False
                                    End If
                                    PhoneDirEmp = New goAML_Ref_Phone
                                Next
                            Else
                                Status = False
                            End If

                            'Director Identification
                            If item.ListDirectorIdentification.Count = ListDirectorIdentificationOld.Count Then
                                For Each itemx In item.ListDirectorIdentification
                                    IdentDir = ListDirectorIdentificationOld.Where(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID).FirstOrDefault
                                    'agam 22102020
                                    If IdentDir Is Nothing Then
                                        Status = False
                                        Exit For
                                    End If
                                    If IdentDir.FK_Person_ID <> Nothing Then
                                        With IdentDir
                                            If .Type <> itemx.Type Then
                                                Status = False
                                            End If
                                            If .Number <> itemx.Number Then
                                                Status = False
                                            End If
                                            If .Issue_Date <> itemx.Issue_Date Then
                                                Status = False
                                            End If
                                            If .Expiry_Date <> itemx.Expiry_Date Then
                                                Status = False
                                            End If
                                            If .Issued_By <> itemx.Issued_By Then
                                                Status = False
                                            End If
                                            If .Issued_Country <> itemx.Issued_Country Then
                                                Status = False
                                            End If
                                            If .Identification_Comment <> itemx.Identification_Comment Then
                                                Status = False
                                            End If
                                        End With
                                    Else
                                        Status = False
                                    End If
                                    IdentDir = New goAML_Person_Identification
                                Next
                            Else
                                Status = False
                            End If

                            Director = New goAML_Ref_Walk_In_Customer_Director
                            ListDirectorAddressOld = New List(Of goAML_Ref_Address)
                            ListDirectorAddressEmployerOld = New List(Of goAML_Ref_Address)
                            ListDirectorPhoneOld = New List(Of goAML_Ref_Phone)
                            ListDirectorPhoneEmployerOld = New List(Of goAML_Ref_Phone)
                            ListDirectorIdentificationOld = New List(Of goAML_Person_Identification)

                        Else
                            Status = False
                        End If
                    Next

                    'goAML 5.0.1, Add by Septian, 2023-03-10
                    If Status Then
                        Status = Not IsFormChanged(wicData)
                    End If
                    'end Add

                    If Status Then
                        Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                    End If

                    If Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                        objgoAML_WIC.SaveEditTanpaApproval(objSave, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule, wicData)
                        WIC_Panel.Hide()
                        Panelconfirmation.Show()
                        LblConfirmation.Text = "Data Saved into Database"
                    Else
                        objgoAML_WIC.SaveEditApproval(objSave, ListDirectorDetailClass, ListAddressDetail, ListPhoneDetail, ListAddressEmployerDetail, ListPhoneEmployerDetail, ListIdentificationDetail, ObjModule, wicData)
                        WIC_Panel.Hide()
                        Panelconfirmation.Show()
                        LblConfirmation.Text = "Data Saved into Pending Approval"
                    End If


                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()
            PanelDetailPhone.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()
            FormPanelDirectorPhone.Hide()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()
            FormPanelEmpDirectorTaskDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorEmployerPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()
            FormPanelDirectorPhone.Hide()
            FormPanelEmpDirectorTaskDetail.Hide()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelPhoneEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            PanelDetailPhone.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailPhoneEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            PanelDetailPhone.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelIdentification_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailIdentification.Hide()
            PanelDetailIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailIdentification_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailIdentification.Hide()
            FormPanelIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelIdentificationDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorIdentification.Hide()
            PanelDetailDirectorIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailIdentificationDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorIdentification.Hide()
            FormPanelDirectorIdentification.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailAddress.Hide()
            PanelDetailAddress.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddressEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorDetailAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hidden = True
            FP_Address_Director.Hide()
            FP_Address_Emp_Director.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorDetailEmployerAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            FP_Address_Director.Hide()
            FP_Address_Emp_Director.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'agam 221-2020
            WindowDetailDirectorPhone.Hidden = True
            'FormPanelEmpDirectorTaskDetail.Hidden = True
            'FormPanelDirectorPhone.Hidden = True
            'PanelPhoneDirectorEmployer.Hidden = True
            'PanelPhoneDirector.Hidden = True
            'If ObjDetailPhoneDirector Is Nothing Then
            '    SaveAddDetailPhoneDirector()
            'Else
            '    SaveEditDetailPhoneDirector()
            'End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorEmpPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailPhoneDirectorEmployer IsNot Nothing Then
                Dim alreadySaved = ListPhoneDetailDirectorEmployer.Exists(Function(x) x.PK_goAML_Ref_Phone = ObjDetailPhoneDirectorEmployer.PK_goAML_Ref_Phone)
                If Not alreadySaved Then
                    SaveAddDetailPhoneDirectorEmployer()
                Else
                    Dim status As Boolean = True
                    Dim ObjDetailPhoneDirectorEmployerNew As New goAML_Ref_Phone

                    With ObjDetailPhoneDirectorEmployerNew
                        .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.Text
                        .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.Text
                        .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text
                        .tph_number = Director_Emp_txt_phone_number.Text
                        .tph_extension = Director_Emp_txt_phone_extension.Text
                        .comments = Director_Emp_txt_phone_comments.Text
                    End With

                    With ObjDetailPhoneDirectorEmployer
                        If .Tph_Contact_Type <> ObjDetailPhoneDirectorEmployerNew.Tph_Contact_Type Then
                            status = False
                        End If
                        If .Tph_Communication_Type <> ObjDetailPhoneDirectorEmployerNew.Tph_Communication_Type Then
                            status = False
                        End If
                        If .tph_country_prefix <> ObjDetailPhoneDirectorEmployerNew.tph_country_prefix Then
                            status = False
                        End If
                        If .tph_number <> ObjDetailPhoneDirectorEmployerNew.tph_number Then
                            status = False
                        End If
                        If .tph_extension <> ObjDetailPhoneDirectorEmployerNew.tph_extension Then
                            status = False
                        End If
                        If .comments <> ObjDetailPhoneDirectorEmployerNew.comments Then
                            status = False
                        End If
                    End With

                    If status Then
                        Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                    Else
                        SaveEditDetailPhoneDirectorEmployer()
                    End If
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorEmployerPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'agam 22102020
            WindowDetailDirectorPhone.Hidden = True
            'FormPanelEmpDirectorTaskDetail.Hidden = True
            'FormPanelDirectorPhone.Hidden = True
            'PanelPhoneDirectorEmployer.Hidden = True
            'PanelPhoneDirector.Hidden = True


            '    If ObjDetailPhoneDirectorEmployer Is Nothing Then
            '        SaveAddDetailPhoneDirectorEmployer()
            '    Else
            '        SaveEditDetailPhoneDirectorEmployer()
            '    End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailAddress.Hide()
            PanelDetailAddress.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailAddressEmployer_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailAddressDirector IsNot Nothing Then
                Dim alreadySaved = ListAddressDetailDirector.Exists(Function(x) x.PK_Customer_Address_ID = ObjDetailAddressDirector.PK_Customer_Address_ID)
                If Not alreadySaved Then
                    SaveAddDetailAddressDirector()
                Else
                    Dim status As Boolean = True
                    Dim ObjDetailAddressDirectorNew As New goAML_Ref_Address

                    With ObjDetailAddressDirectorNew
                        .Address_Type = Director_cmb_kategoriaddress.Text
                        .Address = Director_Address.Text
                        .Town = Director_Town.Text
                        .City = Director_City.Text
                        .Zip = Director_Zip.Text
                        .Country_Code = Director_cmb_kodenegara.TextValue
                        .State = Director_State.Text
                        .Comments = Director_Comment_Address.Text
                    End With

                    With ObjDetailAddressDirector
                        If .Address_Type <> ObjDetailAddressDirectorNew.Address_Type Then
                            status = False
                        End If
                        If .Address <> ObjDetailAddressDirectorNew.Address Then
                            status = False
                        End If
                        If .Town <> ObjDetailAddressDirectorNew.Town Then
                            status = False
                        End If
                        If .City <> ObjDetailAddressDirectorNew.City Then
                            status = False
                        End If
                        If .Zip <> ObjDetailAddressDirectorNew.Zip Then
                            status = False
                        End If
                        If .Country_Code <> ObjDetailAddressDirectorNew.Country_Code Then
                            status = False
                        End If
                        If .State <> ObjDetailAddressDirectorNew.State Then
                            status = False
                        End If
                        If .Comments <> ObjDetailAddressDirectorNew.Comments Then
                            status = False
                        End If
                    End With
                    Dim NewSanctionData As Long = 0
                    For Each item In obj_ListSanction_Edit
                        If item.PK_goAML_Ref_WIC_Sanction_ID < 1 Then
                            NewSanctionData = NewSanctionData + 1
                        End If
                    Next
                    If NewSanctionData > 1 Then
                        status = False
                    End If
                    If status Then
                        Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                    Else
                        SaveEditDetailAddressDirector()
                    End If
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorEmployerAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'agam 22102020
            WindowDetailDirectorAddress.Hidden = True
            'FP_Address_Emp_Director.Hidden = True
            'If ObjDirector Is Nothing Then
            '    SaveAddDetailAddressDirectorEmployer()
            'Else
            '    SaveEditDetailAddressDirectorEmployer()
            'End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveCustomerDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetailDirector.Hide()
            FP_Director.Hide()
            FormPanelDirectorDetail.Hide()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'agam 22102020
            WindowDetailDirectorAddress.Hidden = True
            'FP_Address_Director.Hidden = True
            'If ObjDetailAddressDirector Is Nothing Then
            '    SaveAddDetailAddressDirector()
            'Else
            '    SaveEditDetailAddressDirector()
            'End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveDirectorEmpAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If ObjDetailAddressDirectorEmployer IsNot Nothing Then
                Dim alreadSaved = ListAddressDetailDirectorEmployer.Exists(Function(x) x.PK_Customer_Address_ID = ObjDetailAddressDirectorEmployer.PK_Customer_Address_ID)
                If Not alreadSaved Then
                    SaveAddDetailAddressDirectorEmployer()
                Else
                    Dim status As Boolean = True
                    Dim ObjDetailAddressDirectorEmployerNew As New goAML_Ref_Address

                    With ObjDetailAddressDirectorEmployerNew
                        .Address_Type = Director_cmb_emp_kategoriaddress.Text
                        .Address = Director_emp_Address.Text
                        .Town = Director_emp_Town.Text
                        .City = Director_emp_City.Text
                        .Zip = Director_emp_Zip.Text
                        .Country_Code = Director_cmb_emp_kodenegara.TextValue
                        .State = Director_emp_State.Text
                        .Comments = Director_emp_Comment_Address.Text
                    End With

                    With ObjDetailAddressDirectorEmployer
                        If .Address_Type <> ObjDetailAddressDirectorEmployerNew.Address_Type Then
                            status = False
                        End If
                        If .Address <> ObjDetailAddressDirectorEmployerNew.Address Then
                            status = False
                        End If
                        If .Town <> ObjDetailAddressDirectorEmployerNew.Town Then
                            status = False
                        End If
                        If .City <> ObjDetailAddressDirectorEmployerNew.City Then
                            status = False
                        End If
                        If .Zip <> ObjDetailAddressDirectorEmployerNew.Zip Then
                            status = False
                        End If
                        If .Country_Code <> ObjDetailAddressDirectorEmployerNew.Country_Code Then
                            status = False
                        End If
                        If .State <> ObjDetailAddressDirectorEmployerNew.State Then
                            status = False
                        End If
                        If .Comments <> ObjDetailAddressDirectorEmployerNew.Comments Then
                            status = False
                        End If
                    End With

                    If status Then
                        Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
                    Else
                        SaveEditDetailAddressDirectorEmployer()
                    End If
                End If
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDetailPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            PanelDetailPhone.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdCustDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditIdentificationDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailIdentificationDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordIdentificationDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordIdentification(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhone(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneEmployerIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneIndividuEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneIndividuEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhoneEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhoneDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhoneDirectorEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DeleteRecordDetailIdentificationDirector(id As String)
        btnAddDirectorIdentification_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Person_Identification = ListIdentificationDirector.Find(Function(x) x.PK_Person_Identification_ID = id)
        If Not objDelete Is Nothing Then
            ListIdentificationDirector.Remove(objDelete)
            'Daniel 2021 Jan 5
            'BindDetailIdentification(StoreIdentificationDirector, ListIdentificationDirector)
            BindDetailIdentification(StoreIdentificationDirectorDetail, ListIdentificationDirector)
            'end 2021 Jan 5
            WindowDetailDirectorIdentification.Hide()
            FormPanelIdentification.Hide()
            PanelDetailIdentification.Hide()
            FP_Director.Show()
        End If
    End Sub
    Private Sub DeleteRecordDetailIdentification(id As String)
        btnAddIdentification_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Person_Identification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id)
        If Not objDelete Is Nothing Then
            ListIdentificationDetail.Remove(objDelete)
            BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)
            WindowDetailIdentification.Hide()
            FormPanelIdentification.Hide()
            PanelDetailIdentification.Hide()
        End If
    End Sub
    Private Sub DeleteRecordDetailPhoneIndividu(id As String)
        BtnAddNewPhoneIndividu_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Phone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not objDelete Is Nothing Then
            ListPhoneDetail.Remove(objDelete)
            BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
            WindowDetailPhone.Hide()
            FormPanelPhoneDetail.Hide()
        End If

    End Sub
    Private Sub DeleteRecordDetailPhoneIndividuEmployer(id As String)
        'BtnAddNewPhoneEmployerIndividu_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Phone = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not objDelete Is Nothing Then
            ListPhoneEmployerDetail.Remove(objDelete)
            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)
            WindowDetailPhone.Hide()
            FormPanelPhoneEmployerDetail.Hide()
        End If

    End Sub
    Private Sub DeleteRecordDetailPhoneDirector(id As String)
        btnAddDirectorPhones_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Phone = ListPhoneDetailDirector.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not objDelete Is Nothing Then
            ListPhoneDetailDirector.Remove(objDelete)
            BindDetailPhone(Store_DirectorPhone, ListPhoneDetailDirector)
            WindowDetailDirectorPhone.Hide()
            FormPanelPhoneDetail.Hide()
        End If

    End Sub
    Private Sub DeleteRecordDetailPhoneDirectorEmployer(id As String)
        btnAddDirectorEmployerPhones_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Phone = ListPhoneDetailDirectorEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not objDelete Is Nothing Then
            ListPhoneDetailDirectorEmployer.Remove(objDelete)
            BindDetailPhone(Store_Director_Employer_Phone, ListPhoneDetailDirectorEmployer)
            WindowDetailDirectorPhone.Hide()
            FormPanelEmpDirectorTaskDetail.Hide()
        End If

    End Sub
    Private Sub DetailRecordIdentificationDirector(id As String)
        ObjDetailIdentificationDirector = ListIdentificationDirector.Find(Function(x) x.PK_Person_Identification_ID = id And x.FK_Person_Type = 7)
        If Not ObjDetailIdentificationDirector Is Nothing Then

            PanelDetailDirectorIdentification.Show()
            FormPanelDirectorIdentification.Hide()
            WindowDetailDirectorIdentification.Title = "Detail Identitas"
            WindowDetailDirectorIdentification.Show()
            With ObjDetailIdentificationDirector
                DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                DspNumberDirector.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDateDirector.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDateDirector.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedByDirector.Text = .Issued_By
                DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                DspIdentificationCommentDirector.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub DetailRecordIdentification(id As String)
        ObjDetailIdentification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id And x.FK_Person_Type = 8)
        If Not ObjDetailIdentification Is Nothing Then

            PanelDetailIdentification.Show()
            FormPanelIdentification.Hide()
            WindowDetailIdentification.Title = "Detail Identitas"
            WindowDetailIdentification.Show()
            With ObjDetailIdentification
                DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                DspNumber.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDate.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDate.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedBy.Text = .Issued_By
                DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                DspIdentificationComment.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub DetailRecordPhone(id As String)
        ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhone Is Nothing Then

            PanelDetailPhoneEmployer.Hide()
            PanelDetailPhone.Show()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()
            WindowDetailPhone.Title = "Detail Phone"
            WindowDetailPhone.Show()

            With ObjDetailPhone
                Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
    End Sub
    Private Sub DetailRecordPhoneEmployer(id As String)
        ObjDetailPhoneEmployer = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneEmployer Is Nothing Then

            PanelDetailPhoneEmployer.Show()
            PanelDetailPhone.Hide()
            FormPanelPhoneDetail.Hide()
            FormPanelPhoneEmployerDetail.Hide()
            WindowDetailPhone.Title = "Detail Phone Employer"
            WindowDetailPhone.Show()

            With ObjDetailPhoneEmployer
                Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefixEmployer.Text = .tph_country_prefix
                Dsptph_numberEmployer.Text = .tph_number
                Dsptph_extensionEmployer.Text = .tph_extension
                DspcommentsPhoneEmployer.Text = .comments
            End With
        End If
    End Sub

    'Daniel 2021 Jan 5
    'Private Sub DetailRecordDirector(id As String)
    '    Dim ListObjDirector As WICDirectorDataBLL = ListDirectorDetailClass.Find(Function(x) x.ObjDirector.NO_ID = id)
    '    Dim ObjDirector As goAML_Ref_Walk_In_Customer_Director = ListObjDirector.ObjDirector
    '    ListAddressDetailDirector = ListObjDirector.ListDirectorAddress.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList
    '    ListAddressDetailDirectorEmployer = ListObjDirector.ListDirectorAddressEmployer.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList
    '    ListPhoneDetailDirector = ListObjDirector.ListDirectorPhone.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList
    '    ListPhoneDetailDirectorEmployer = ListObjDirector.ListDirectorPhoneEmployer.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList
    '    ListIdentificationDirector = ListObjDirector.ListDirectorIdentification.Where(Function(x) x.FK_Person_ID = id And x.FK_Person_Type = 7).ToList

    '    If Not ObjDirector Is Nothing Then

    '        FormPanelDirectorDetail.Show()
    '        FP_Director.Hide()
    '        WindowDetailDirector.Show()

    '        With ObjDirector
    '            DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleTypebyCode(.Role)
    '            DspGelarDirector.Text = .Title
    '            DspNamaLengkap.Text = .Last_Name
    '            DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
    '            If .BirthDate IsNot Nothing Then
    '                DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yy")
    '            End If
    '            DspTempatLahir.Text = .Birth_Place
    '            DspNamaIbuKandung.Text = .Mothers_Name
    '            DspNamaAlias.Text = .Alias
    '            DspNIK.Text = .SSN
    '            DspNoPassport.Text = .Passport_Number
    '            DspNegaraPenerbitPassport.Text = .Passport_Country
    '            DspNoIdentitasLain.Text = .ID_Number
    '            DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
    '            DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
    '            DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
    '            DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
    '            DspEmail.Text = .Email
    '            DspEmail2.Text = .Email2
    '            DspEmail3.Text = .Email3
    '            DspEmail4.Text = .Email4
    '            DspEmail5.Text = .Email5
    '            DspDeceased.Text = .Deceased
    '            If DspDeceased.Text = "True" Then
    '                If .Deceased_Date IsNot Nothing Then
    '                    DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
    '                End If
    '            End If
    '            DspPEP.Text = .Tax_Reg_Number
    '            DspNPWP.Text = .Tax_Number
    '            DspSourceofWealth.Text = .Source_of_Wealth
    '            DspOccupation.Text = .Occupation
    '            DspCatatan.Text = .Comments
    '        End With

    '        'BindDetailAddress(StoreAddressDirector, ListAddressDetailDirector)
    '        'BindDetailAddress(StoreAddressDirectorEmployer, ListAddressDetailDirectorEmployer)
    '        'BindDetailPhone(StorePhoneDirector, ListPhoneDetailDirector)
    '        'BindDetailPhone(StorePhoneDirectorEmployer, ListPhoneDetailDirectorEmployer)
    '        'BindDetailIdentification(StoreIdentificationDirector, ListIdentificationDirector)
    '        BindDetailAddress(Store_DirectorAddress, ListAddressDetailDirector)
    '        BindDetailAddress(Store_Director_Employer_Address, ListAddressDetailDirectorEmployer)
    '        BindDetailPhone(Store_DirectorPhone, ListPhoneDetailDirector)
    '        BindDetailPhone(Store_Director_Employer_Phone, ListPhoneDetailDirectorEmployer)
    '        BindDetailIdentification(StoreIdentificationDirectorDetail, ListIdentificationDirector)

    '    End If
    'End Sub

    Private Sub DetailRecordDirector(id As String)
        Dim listDirectorDetails As WICDirectorDataBLL = ListDirectorDetailClass.Where(Function(x) x.ObjDirector.NO_ID = id).FirstOrDefault
        ObjDirector = ListDirectorDetail.Find(Function(x) x.NO_ID = id)
        If Not ObjDirector Is Nothing Then

            FormPanelDirectorDetail.Show()
            WindowDetailDirector.Show()
            'Daniel 2021 Jan 8
            FP_Director.Hide()
            'end 2021 Jan 8
            With ObjDirector
                If GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role) IsNot Nothing Then
                    DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
                Else
                    DspPeranDirector.Text = ""
                End If

                DspGelarDirector.Text = .Title
                DspNamaLengkap.Text = .Last_Name
                If GlobalReportFunctionBLL.getGenderbyKode(.Gender) IsNot Nothing Then
                    DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                Else
                    DspGender.Text = ""
                End If


                If .BirthDate IsNot Nothing Then
                    DspTanggalLahir.Text = ObjDirector.BirthDate.Value.ToString("dd-MMM-yy")
                Else
                    DspTanggalLahir.Text = ""
                End If
                DspTempatLahir.Text = .Birth_Place
                DspNamaIbuKandung.Text = .Mothers_Name
                DspNamaAlias.Text = .Alias
                DspNIK.Text = .SSN
                DspNoPassport.Text = .Passport_Number
                'daniel 19 jan 2021
                DspNegaraPenerbitPassport.Text = GlobalReportFunctionBLL.getCountryByCode(.Passport_Country)
                DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                'If GlobalReportFunctionBLL.getCountryByCode(.Passport_Country) IsNot Nothing Then
                'End If
                'DspNegaraPenerbitPassport.Text = .Passport_Country
                DspNoIdentitasLain.Text = .ID_Number
                'If GlobalReportFunctionBLL.getCountryByCode(.Nationality1) IsNot Nothing Then
                '    DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.Nationality2) IsNot Nothing Then
                '    DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                'Else
                '    DspKewarganegaraan2.Text = ""
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.Nationality3) IsNot Nothing Then
                '    DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                'Else
                '    DspKewarganegaraan3.Text = ""
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.Residence) IsNot Nothing Then
                '    DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                'Else
                '    DspNegaraDomisili.Text = ""
                'End If
                'end 19 jan 2021

                'DspEmail.Text = .Email
                'DspEmail2.Text = .Email2
                'DspEmail3.Text = .Email3
                'DspEmail4.Text = .Email4
                'DspEmail5.Text = .Email5
                DspDeceased.Text = .Deceased
                If DspDeceased.Text = "True" Then
                    If .Deceased_Date IsNot Nothing Then
                        DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                        'Daniel 2021 Jan 7
                        DspDeceasedDate.Hidden = False
                    End If
                Else
                    DspDeceasedDate.Hidden = True
                    'end 2021 Jan 7
                End If
                DspPEP.Text = .Tax_Reg_Number
                DspNPWP.Text = .Tax_Number
                DspSourceofWealth.Text = .Source_of_Wealth
                DspOccupation.Text = .Occupation
                DspCatatan.Text = .Comments
                DspTempatBekerja.Text = .Employer_Name
            End With

            Dsp_GridEmail_Director.IsViewMode = True
            Dsp_GridSanction_Director.IsViewMode = True
            Dsp_GridPEP_Director.IsViewMode = True

            ListPhoneDetailDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorPhone(ObjDirector.NO_ID)
            ListPhoneDetailDirectorEmployer = objgoAML_WIC.GetWICByPKIDDetailDirectorPhoneEmployer(ObjDirector.NO_ID)
            ListAddressDetailDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorAddress(ObjDirector.NO_ID)
            ListAddressDetailDirectorEmployer = objgoAML_WIC.GetWICByPKIDDetailDirectorAddressEmployer(ObjDirector.NO_ID)
            ListIdentificationDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorIdentification(ObjDirector.NO_ID)

            BindDetailPhone(StorePhoneDirector, ListPhoneDetailDirector)
            BindDetailPhone(StorePhoneDirectorEmployer, ListPhoneDetailDirectorEmployer)
            BindDetailAddress(StoreAddressDirector, ListAddressDetailDirector)
            BindDetailAddress(StoreAddressDirectorEmployer, ListAddressDetailDirectorEmployer)
            BindDetailIdentification(StoreIdentificationDirector, ListIdentificationDirector)

            Dsp_GridEmail_Director.LoadData(listDirectorDetails.ListDirectorEmail)
            Dsp_GridSanction_Director.LoadData(listDirectorDetails.ListDirectorSanction)
            Dsp_GridPEP_Director.LoadData(listDirectorDetails.ListDirectorPEP)

        End If
    End Sub
    'end 2021 Jan 5

    Private Sub DetailRecordPhoneDirector(id As String)
        ObjDetailPhoneDirector = ListPhoneDetailDirector.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneDirector Is Nothing Then
            WindowDetailDirectorPhone.Title = "Detail Director Phone"
            WindowDetailDirectorPhone.Show()
            PanelPhoneDirector.Show()
            FormPanelEmpDirectorTaskDetail.Hide()
            'agam 22102020
            PanelPhoneDirectorEmployer.Hidden = True
            FormPanelDirectorPhone.Hidden = True

            'ClearInput()
            With ObjDetailPhoneDirector
                DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                DspKodeAreaTelpDirector.Text = .tph_country_prefix
                DspNomorTeleponDirector.Text = .tph_number
                DspNomorExtensiDirector.Text = .tph_extension
                DspCatatanDirectorPhoneDirector.Text = .comments
            End With
        End If
    End Sub
    Private Sub DetailRecordPhoneDirectorEmployer(id As String)
        ObjDetailPhoneDirectorEmployer = ListPhoneDetailDirectorEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneDirectorEmployer Is Nothing Then

            PanelPhoneDirectorEmployer.Show()
            PanelPhoneDirector.Hide()
            WindowDetailDirectorPhone.Title = "Detail Director Phone Employer"
            WindowDetailDirectorPhone.Show()
            'agam 22102020
            FormPanelDirectorPhone.Hidden = True
            FormPanelEmpDirectorTaskDetail.Hidden = True
            clearinputDirector()
            'ClearInput()
            With ObjDetailPhoneDirectorEmployer
                DspKategoriKontakDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                DspJenisAlatKomunikasiDirectorEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                DspKodeAreaTelpDirectorEmployer.Text = .tph_country_prefix
                DspNomorTeleponDirectorEmployer.Text = .tph_number
                DspNomorExtensiDirectorEmployer.Text = .tph_extension
                DspCatatanDirectorPhoneDirectorEmployer.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditIdentificationDirector(id As String)
        ObjDetailIdentificationDirector = ListIdentificationDirector.Find(Function(x) x.PK_Person_Identification_ID = id And x.FK_Person_Type = 7)
        If Not ObjDetailIdentificationDirector Is Nothing Then

            FormPanelDirectorIdentification.Show()
            PanelDetailDirectorIdentification.Hide()
            WindowDetailDirectorIdentification.Title = "Edit identitas"
            WindowDetailDirectorIdentification.Show()

            With ObjDetailIdentificationDirector
                CmbTypeIdentificationDirector.SetValue(.Type)
                TxtNumberDirector.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    TxtIssueDateDirector.Text = .Issue_Date
                End If
                If .Expiry_Date IsNot Nothing Then
                    TxtExpiryDateDirector.Text = .Expiry_Date
                End If
                TxtIssuedByDirector.Text = .Issued_By
                If Not IsNothing(.Issued_Country) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Issued_Country & "'")
                    If DataRowTemp IsNot Nothing Then
                        CmbCountryIdentificationDirector.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                ' CmbCountryIdentificationDirector.SetValueAndFireSelect(.Issued_Country)
                TxtCommentIdentificationDirector.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub LoadDataEditIdentification(id As String)
        ObjDetailIdentification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id And x.FK_Person_Type = 8)
        If Not ObjDetailIdentification Is Nothing Then

            FormPanelIdentification.Show()
            PanelDetailIdentification.Hide()
            WindowDetailIdentification.Title = "Edit Identitas"
            WindowDetailIdentification.Show()

            With ObjDetailIdentification
                CmbTypeIdentification.SetValue(.Type)
                TxtNumber.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    TxtIssueDate.Text = .Issue_Date
                End If
                If .Expiry_Date IsNot Nothing Then
                    TxtExpiryDate.Text = .Expiry_Date
                End If
                TxtIssuedBy.Text = .Issued_By
                If Not IsNothing(.Issued_Country) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Issued_Country & "'")
                    If DataRowTemp IsNot Nothing Then
                        CmbCountryIdentification.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                'CmbCountryIdentification.SetValueAndFireSelect(.Issued_Country)
                TxtCommentIdentification.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub LoadDataEditPhoneIndividu(id As String)
        ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhone Is Nothing Then
            FormPanelPhoneDetail.Show()
            WindowDetailPhone.Title = "Edit Phone"
            WindowDetailPhone.Show()
            ClearInput()
            With ObjDetailPhone
                Cmbtph_contact_type.SetValueAndFireSelect(.Tph_Contact_Type)
                Cmbtph_communication_type.SetValueAndFireSelect(.Tph_Communication_Type)
                Txttph_country_prefix.Text = .tph_country_prefix
                Txttph_number.Text = .tph_number
                Txttph_extension.Text = .tph_extension
                TxtcommentsPhone.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditPhoneIndividuEmployer(id As String)
        ObjDetailPhoneEmployer = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneEmployer Is Nothing Then
            FormPanelPhoneEmployerDetail.Show()
            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            FormPanelPhoneDetail.Hide()
            WindowDetailPhone.Title = "Edit Phone Employer"
            WindowDetailPhone.Show()

            With ObjDetailPhoneEmployer
                Cmbtph_contact_typeEmployer.SetValueAndFireSelect(.Tph_Contact_Type)
                Cmbtph_communication_typeEmployer.SetValueAndFireSelect(.Tph_Communication_Type)
                Txttph_country_prefixEmployer.Text = .tph_country_prefix
                Txttph_numberEmployer.Text = .tph_number
                Txttph_extensionEmployer.Text = .tph_extension
                TxtcommentsPhoneEmployer.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditDirector(id As String)
        'ClearInput() ' 2023-10-23, Nael

        Dim listDirectorDetails As WICDirectorDataBLL = ListDirectorDetailClass.Where(Function(x) x.ObjDirector.NO_ID = id).FirstOrDefault
        'ObjDirector = listDirectorDetails.ObjDirector
        ListAddressDetailDirector = listDirectorDetails.ListDirectorAddress.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList
        ListAddressDetailDirectorEmployer = listDirectorDetails.ListDirectorAddressEmployer.Where(Function(x) x.FK_To_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList
        ListPhoneDetailDirector = listDirectorDetails.ListDirectorPhone.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 8).ToList
        ListPhoneDetailDirectorEmployer = listDirectorDetails.ListDirectorPhoneEmployer.Where(Function(x) x.FK_for_Table_ID = id And x.FK_Ref_Detail_Of = 4).ToList
        ListIdentificationDirector = listDirectorDetails.ListDirectorIdentification.Where(Function(x) x.FK_Person_ID = id And x.FK_Person_Type = 7).ToList()
        'daniel 21 jan 2021
        ObjDirector = listDirectorDetails.ObjDirector
        'end 21 jan 2021
        If Not ObjDirector Is Nothing Then
            FP_Director.Show()
            WindowDetailDirector.Show()
            FormPanelDirectorDetail.Hide()
            'ClearInput()
            With ObjDirector
                Hidden2.Value = .NO_ID
                cmb_peran.SetValueAndFireSelect(.Role)
                txt_Director_Title.Text = .Title
                txt_Director_Last_Name.Text = .Last_Name
                cmb_Director_Gender.SetValueAndFireSelect(.Gender)
                If .BirthDate IsNot Nothing Then
                    txt_Director_BirthDate.Text = .BirthDate.Value.ToString("dd-MMM-yy")
                End If
                txt_Director_Birth_Place.Text = .Birth_Place
                txt_Director_Mothers_Name.Text = .Mothers_Name
                txt_Director_Alias.Text = .Alias
                txt_Director_ID_Number.Text = .ID_Number
                txt_Director_Passport_Number.Text = .Passport_Number
                If Not IsNothing(.Passport_Country) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Passport_Country & "'")
                    If DataRowTemp IsNot Nothing Then
                        txt_Director_Passport_Country.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                ' txt_Director_Passport_Country.Text = .Passport_Country
                txt_Director_SSN.Text = .SSN
                If Not IsNothing(.Nationality1) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Nationality1 & "'")
                    If DataRowTemp IsNot Nothing Then
                        cmb_Director_Nationality1.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                If Not IsNothing(.Nationality2) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Nationality2 & "'")
                    If DataRowTemp IsNot Nothing Then
                        cmb_Director_Nationality2.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                If Not IsNothing(.Nationality3) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Nationality3 & "'")
                    If DataRowTemp IsNot Nothing Then
                        cmb_Director_Nationality3.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                If Not IsNothing(.Residence) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Residence & "'")
                    If DataRowTemp IsNot Nothing Then
                        cmb_Director_Residence.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                'cmb_Director_Nationality1.SetValueAndFireSelect(.Nationality1)
                'cmb_Director_Nationality2.SetValueAndFireSelect(.Nationality2)
                'cmb_Director_Nationality3.SetValueAndFireSelect(.Nationality3)
                'cmb_Director_Residence.SetValueAndFireSelect(.Residence)
                'txt_Director_Email.Text = .Email
                'txt_Director_Email2.Text = .Email2
                'txt_Director_Email3.Text = .Email3
                'txt_Director_Email4.Text = .Email4
                'txt_Director_Email5.Text = .Email5
                cb_Director_Deceased.Checked = .Deceased
                If cb_Director_Deceased.Checked Then
                    If .Deceased_Date IsNot Nothing Then
                        txt_Director_Deceased_Date.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                    End If
                End If
                cb_Director_Tax_Reg_Number.Checked = .Tax_Reg_Number
                txt_Director_Tax_Number.Text = .Tax_Number
                txt_Director_Source_of_Wealth.Text = .Source_of_Wealth
                txt_Director_Occupation.Text = .Occupation
                txt_Director_Comments.Text = .Comments
                txt_Director_employer_name.Text = .Employer_Name

            End With

            GridEmail_Director.IsViewMode = False
            GridSanction_Director.IsViewMode = False
            GridPEP_Director.IsViewMode = False

            GridEmail_Director.LoadData(listDirectorDetails.ListDirectorEmail)
            GridSanction_Director.LoadData(listDirectorDetails.ListDirectorSanction)
            GridPEP_Director.LoadData(listDirectorDetails.ListDirectorPEP)

            BindDetailAddress(Store_DirectorAddress, ListAddressDetailDirector)
            BindDetailAddress(Store_Director_Employer_Address, ListAddressDetailDirectorEmployer)
            BindDetailPhone(Store_DirectorPhone, ListPhoneDetailDirector)
            BindDetailPhone(Store_Director_Employer_Phone, ListPhoneDetailDirectorEmployer)
            BindDetailIdentification(StoreIdentificationDirectorDetail, ListIdentificationDirector)

        End If
    End Sub
    Private Sub LoadDataEditPhoneDirector(id As String)
        ObjDetailPhoneDirector = ListPhoneDetailDirector.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneDirector Is Nothing Then
            FormPanelDirectorPhone.Show()
            FormPanelEmpDirectorTaskDetail.Hide()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()
            WindowDetailDirectorPhone.Title = "Edit Director Phone"
            WindowDetailDirectorPhone.Show()
            'ClearInput()
            With ObjDetailPhoneDirector
                Director_cb_phone_Contact_Type.SetValueAndFireSelect(.Tph_Contact_Type)
                Director_cb_phone_Communication_Type.SetValueAndFireSelect(.Tph_Communication_Type)
                Director_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_txt_phone_number.Text = .tph_number
                Director_txt_phone_extension.Text = .tph_extension
                Director_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditPhoneDirectorEmployer(id As String)
        ObjDetailPhoneDirectorEmployer = ListPhoneDetailDirectorEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneDirectorEmployer Is Nothing Then
            FormPanelEmpDirectorTaskDetail.Show()
            FormPanelDirectorPhone.Hide()
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Hide()
            WindowDetailDirectorPhone.Title = "Detail Director Phone Employer"
            WindowDetailDirectorPhone.Show()
            'ClearInput()
            With ObjDetailPhoneDirectorEmployer
                Director_Emp_cb_phone_Contact_Type.SetValueAndFireSelect(.Tph_Contact_Type)
                Director_Emp_cb_phone_Communication_Type.SetValueAndFireSelect(.Tph_Communication_Type)
                Director_Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_Emp_txt_phone_number.Text = .tph_number
                Director_Emp_txt_phone_extension.Text = .tph_extension
                Director_Emp_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub

    Protected Sub GridCommandAddressIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressIndividu(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddress(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressEmployerIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressIndividuEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressIndividuEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddressEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressDirector(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddressDirector(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressDirectorEmployer(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddressDirectorEmployer(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DeleteRecordDetailAddressIndividu(id As String)
        'BtnAddNewAddressIndividu_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Address = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not objDelete Is Nothing Then
            ListAddressDetail.Remove(objDelete)
            FormPanelAddressDetail.Hide()
            WindowDetailAddress.Hide()
            BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
        End If
    End Sub
    Private Sub DeleteRecordDetailAddressIndividuEmployer(id As String)
        'BtnAddNewAddressEmployerIndividu_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Address = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not objDelete Is Nothing Then
            ListAddressEmployerDetail.Remove(objDelete)
            FormPanelAddressDetailEmployer.Hide()
            WindowDetailAddress.Hide()
            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)
        End If
    End Sub
    Private Sub DeleteRecordDetailDirector(id As String)
        'btnAddCustomerDirector_DirectClick(Nothing, Nothing)
        'daniel 21 jan 2021
        Dim objDirector As WICDirectorDataBLL = ListDirectorDetailClass.Find(Function(x) x.ObjDirector.NO_ID = id)
        Dim objDelete As goAML_Ref_Walk_In_Customer_Director = ListDirectorDetail.Find(Function(x) x.NO_ID = id)
        If Not objDelete Is Nothing Then
            ListDirectorDetail.Remove(objDelete)
        End If
        If objDirector IsNot Nothing Then
            ListDirectorDetailClass.Remove(objDirector)
        End If
        FP_Director.Hide()
        WindowDetailDirector.Hide()
        BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
        'end 21 jan 2021
    End Sub
    Private Sub DeleteRecordDetailAddressDirector(id As String)
        'btnAddDirectorAddresses_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Address = ListAddressDetailDirector.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 8)
        If Not objDelete Is Nothing Then
            ListAddressDetailDirector.Remove(objDelete)
            FP_Address_Director.Hide()
            WindowDetailDirectorAddress.Hide()
            BindDetailAddress(Store_DirectorAddress, ListAddressDetailDirector)
        End If
    End Sub
    Private Sub DeleteRecordDetailAddressDirectorEmployer(id As String)
        'btnAddDirectorAddresses_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Address = ListAddressDetailDirectorEmployer.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 4)
        If Not objDelete Is Nothing Then
            ListAddressDetailDirectorEmployer.Remove(objDelete)
            FP_Address_Emp_Director.Hide()
            WindowDetailDirectorAddress.Hide()
            BindDetailAddress(Store_Director_Employer_Address, ListAddressDetailDirectorEmployer)
        End If
    End Sub

    Private Sub LoadDataEditAddressIndividu(id As String)
        ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 3)
        If Not ObjDetailAddress Is Nothing Then
            FormPanelAddressDetail.Show()
            WindowDetailAddress.Title = "Edit Address"
            WindowDetailAddress.Show()
            FormPanelAddressDetailEmployer.Hide()
            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()

            With ObjDetailAddress
                CmbAddress_type.SetValueAndFireSelect(.Address_Type)
                TxtAddress.Text = .Address
                TxtTown.Text = .Town
                TxtCity.Text = .City
                TxtZip.Text = .Zip
                If Not IsNothing(.Country_Code) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                    If DataRowTemp IsNot Nothing Then
                        Cmbcountry_code.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                'Cmbcountry_code.SetValueAndFireSelect(.Country_Code)
                TxtState.Text = .State
                TxtcommentsAddress.Text = .Comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditAddressIndividuEmployer(id As String)
        ObjDetailAddressEmployer = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 10)

        If Not ObjDetailAddressEmployer Is Nothing Then
            FormPanelAddressDetailEmployer.Show()
            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()
            FormPanelAddressDetail.Hide()
            WindowDetailAddress.Title = "Edit Address Employer"
            WindowDetailAddress.Hidden = False

            With ObjDetailAddressEmployer
                CmbAddress_typeEmployer.SetValueAndFireSelect(.Address_Type)
                TxtAddressEmployer.Text = .Address
                TxtTownEmployer.Text = .Town
                TxtCityEmployer.Text = .City
                TxtZipEmployer.Text = .Zip
                If Not IsNothing(.Country_Code) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                    If DataRowTemp IsNot Nothing Then
                        Cmbcountry_codeEmployer.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                'Cmbcountry_codeEmployer.SetValueAndFireSelect(.Country_Code)
                TxtStateEmployer.Text = .State
                TxtcommentsAddressEmployer.Text = .Comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditAddressDirector(id As String)
        ObjDetailAddressDirector = ListAddressDetailDirector.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 8)
        If Not ObjDetailAddressDirector Is Nothing Then
            FP_Address_Director.Show()
            'Daniel 2021 Jan 4
            'FP_Address_Emp_Director.Hide()
            PanelAddressDirector.Hide()
            'end 2021 Jan 4
            WindowDetailDirectorAddress.Title = "Edit Director Address"
            WindowDetailDirectorAddress.Show()
            'ClearInput()
            With ObjDetailAddressDirector
                Director_cmb_kategoriaddress.SetValueAndFireSelect(.Address_Type)
                Director_Address.Text = .Address
                Director_Town.Text = .Town
                Director_City.Text = .City
                Director_Zip.Text = .Zip
                If Not IsNothing(.Country_Code) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                    If DataRowTemp IsNot Nothing Then
                        Director_cmb_kodenegara.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                ' Director_cmb_kodenegara.SetValueAndFireSelect(.Country_Code)
                Director_State.Text = .State
                Director_Comment_Address.Text = .Comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditAddressDirectorEmployer(id As String)
        ObjDetailAddressDirectorEmployer = ListAddressDetailDirectorEmployer.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 4)
        If Not ObjDetailAddressDirectorEmployer Is Nothing Then
            'Daniel 2021 Jan 4
            PanelAddressDirectorEmployer.Hide()
            'FP_Address_Director.Hide()
            'end 2021 Jan 4
            FP_Address_Emp_Director.Show()
            WindowDetailDirectorAddress.Title = "Edit Director Address Employer"
            WindowDetailDirectorAddress.Show()
            'ClearInput()
            With ObjDetailAddressDirectorEmployer
                Director_cmb_emp_kategoriaddress.SetValueAndFireSelect(.Address_Type)
                Director_emp_Address.Text = .Address
                Director_emp_Town.Text = .Town
                Director_emp_City.Text = .City
                Director_emp_Zip.Text = .Zip
                If Not IsNothing(.Country_Code) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                    If DataRowTemp IsNot Nothing Then
                        Director_cmb_emp_kodenegara.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                ' Director_cmb_emp_kodenegara.SetValueAndFireSelect(.Country_Code)
                Director_emp_State.Text = .State
                Director_emp_Comment_Address.Text = .Comments
            End With
        End If
    End Sub

    Protected Sub GridCommandPhoneCorporate(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailPhoneCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordPhone(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordAddress(id As String)
        ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddress Is Nothing Then

            PanelDetailAddress.Show()
            WindowDetailAddress.Title = "Detail Address"
            WindowDetailAddress.Show()

            With ObjDetailAddress
                DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
    End Sub
    Private Sub DetailRecordAddressEmployer(id As String)
        ObjDetailAddressEmployer = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddressEmployer Is Nothing Then

            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Show()
            FormPanelAddressDetail.Hide()
            FormPanelAddressDetailEmployer.Hide()
            WindowDetailAddress.Title = "Detail Address Employer"
            WindowDetailAddress.Show()

            With ObjDetailAddressEmployer
                DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddressEmployer.Text = .Address
                DspTownEmployer.Text = .Town
                DspCityEmployer.Text = .City
                DspZipEmployer.Text = .Zip
                Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspStateEmployer.Text = .State
                DspcommentsAddressEmployer.Text = .Comments
            End With
        End If
    End Sub
    Private Sub DetailRecordAddressDirector(id As String)
        ObjDetailAddressDirector = ListAddressDetailDirector.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddressDirector Is Nothing Then

            PanelAddressDirector.Show()
            PanelAddressDirectorEmployer.Hide()
            'agam 22102020
            WindowDetailDirectorAddress.Title = "Detail Director Address"
            WindowDetailDirectorAddress.Hidden = False
            FP_Address_Director.Hidden = True
            FP_Address_Emp_Director.Hidden = True
            'WindowDetailAddress.Show()
            ' ClearInput()
            With ObjDetailAddressDirector
                DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAlamatDirector.Text = .Address
                DspKecamatanDirector.Text = .Town
                DspKotaKabupatenDirector.Text = .City
                DspKodePosDirector.Text = .Zip
                DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspProvinsiDirector.Text = .State
                DspCatatanAddressDirector.Text = .Comments
            End With
        End If
    End Sub
    Private Sub DetailRecordAddressDirectorEmployer(id As String)
        ObjDetailAddressDirectorEmployer = ListAddressDetailDirectorEmployer.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddressDirectorEmployer Is Nothing Then

            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Show()
            'Daniel 2021 Jan 4
            FP_Address_Director.Hide()
            'WindowDetailAddress.Title = "Detail Director Address Employer"
            'WindowDetailAddress.Show()
            WindowDetailDirectorAddress.Title = "Detail Director Address Employer"
            WindowDetailDirectorAddress.Show()
            'end 2021 Jan 4
            'ClearInput()
            'agam 22102020
            'Daniel 2021 Jan 12
            FP_Address_Emp_Director.Hide()
            'FormPanelAddressDetail.Hidden = True
            'FormPanelAddressDetailEmployer.Hidden = True
            'end 2021 Jan 12


            With ObjDetailAddressDirectorEmployer
                DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAlamatDirectorEmployer.Text = .Address
                DspKecamatanDirectorEmployer.Text = .Town
                DspKotaKabupatenDirectorEmployer.Text = .City
                DspKodePosDirectorEmployer.Text = .Zip
                DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspProvinsiDirectorEmployer.Text = .State
                DspCatatanAddressDirectorEmployer.Text = .Comments
            End With
        End If
    End Sub

    Private Sub DeleteRecordDetailPhoneCorporate(id As String)
        BtnAddNewPhoneCorporate_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Phone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not objDelete Is Nothing Then
            ListPhoneDetail.Remove(objDelete)
            WindowDetailPhone.Hide()
            FormPanelPhoneDetail.Hide()
            BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)
        End If
    End Sub

    Private Sub LoadDataEditPhoneCorporate(id As String)
        ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhone Is Nothing Then
            FormPanelPhoneDetail.Show()
            WindowDetailPhone.Title = "Edit Phone Corporate"
            WindowDetailPhone.Show()
            'ClearInput()
            With ObjDetailPhone
                Cmbtph_contact_type.SetValueAndFireSelect(.Tph_Contact_Type)
                Cmbtph_communication_type.SetValueAndFireSelect(.Tph_Communication_Type)
                Txttph_country_prefix.Text = .tph_country_prefix
                Txttph_number.Text = .tph_number
                Txttph_extension.Text = .tph_extension
                TxtcommentsPhone.Text = .comments
            End With
        End If
    End Sub

    Protected Sub GridCommandAddressCorporate(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordDetailAddressCorporate(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                DetailRecordAddress(id)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DeleteRecordDetailAddressCorporate(id As String)
        BtnAddNewAddressCorporate_DirectClick(Nothing, Nothing)
        Dim objDelete As goAML_Ref_Address = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not objDelete Is Nothing Then
            ListAddressDetail.Remove(objDelete)
            WindowDetailAddress.Hide()
            FormPanelAddressDetail.Hide()
            BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
        End If
    End Sub

    Private Sub LoadDataEditAddressCorporate(id As String)
        ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddress Is Nothing Then
            FormPanelAddressDetail.Show()
            WindowDetailAddress.Title = "Edit Address Corporate"
            WindowDetailAddress.Show()
            'ClearInput()
            With ObjDetailAddress
                CmbAddress_type.SetValueAndFireSelect(.Address_Type)
                TxtAddress.Text = .Address
                TxtTown.Text = .Town
                TxtCity.Text = .City
                TxtZip.Text = .Zip
                If Not IsNothing(.Country_Code) Then
                    DataRowTemp = getDataRowFromDB("SELECT TOP 1 Kode, Keterangan FROM goAML_Ref_Nama_Negara WHERE kode='" & .Country_Code & "'")
                    If DataRowTemp IsNot Nothing Then
                        Cmbcountry_code.SetTextWithTextValue(DataRowTemp("kode"), DataRowTemp("keterangan"))
                    End If
                End If
                ' Cmbcountry_code.SetValueAndFireSelect(.Country_Code)
                TxtState.Text = .State
                TxtcommentsAddress.Text = .Comments
            End With
        End If
    End Sub
#End Region

    Function getDataRowFromDB(strQuery As String) As DataRow
        Try
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Return drResult
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
            Return Nothing
        End Try
    End Function

#Region "Email"
    Function GetWICByPKIDDetailEmail(id As Long) As List(Of WICDataBLL.goAML_Ref_WIC_Email)
        'Email
        Dim WIC As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT WIC_NO FROM goAML_Ref_WIC WHERE PK_Customer_ID = " & id, Nothing)

        Dim ListEmail = New List(Of WICDataBLL.goAML_Ref_WIC_Email)
        Dim objEmailDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM goAML_Ref_WIC_Email WHERE WIC_NO = '" & WIC & "'", Nothing)
        Dim ListNewEmail As New List(Of WICDataBLL.goAML_Ref_WIC_Email)
        For Each itemEmail As DataRow In objEmailDataTable.Rows
            Dim NewEmail As New WICDataBLL.goAML_Ref_WIC_Email
            If Not IsDBNull(itemEmail("PK_goAML_Ref_WIC_Email_ID")) Then
                NewEmail.PK_goAML_Ref_WIC_Email_ID = itemEmail("PK_goAML_Ref_WIC_Email_ID")
            End If
            If Not IsDBNull(itemEmail("WIC_No")) Then
                NewEmail.WIC_No = itemEmail("WIC_No")
            End If
            If Not IsDBNull(itemEmail("email_address")) Then
                NewEmail.email_address = itemEmail("email_address")
            End If
            If Not IsDBNull(itemEmail("Active")) Then
                NewEmail.Active = itemEmail("Active")
            End If
            If Not IsDBNull(itemEmail("CreatedBy")) Then
                NewEmail.CreatedBy = itemEmail("CreatedBy")
            End If
            If Not IsDBNull(itemEmail("LastUpdateBy")) Then
                NewEmail.LastUpdateBy = itemEmail("LastUpdateBy")
            End If
            If Not IsDBNull(itemEmail("ApprovedBy")) Then
                NewEmail.ApprovedBy = itemEmail("ApprovedBy")
            End If
            If Not IsDBNull(itemEmail("CreatedDate")) Then
                NewEmail.CreatedDate = itemEmail("CreatedDate")
            End If
            If Not IsDBNull(itemEmail("LastUpdateDate")) Then
                NewEmail.LastUpdateDate = itemEmail("LastUpdateDate")
            End If
            If Not IsDBNull(itemEmail("ApprovedDate")) Then
                NewEmail.ApprovedDate = itemEmail("LastUpdateDate")
            End If
            If Not IsDBNull(itemEmail("Alternateby")) Then
                NewEmail.Alternateby = itemEmail("Alternateby")
            End If
            ListNewEmail.Add(NewEmail)
        Next
        ListEmail = ListNewEmail
        Return ListEmail
    End Function

    Public Property IDEmail() As Long
        Get
            Return Session("GoAMLWICAdd.IDEmail")
        End Get
        Set(ByVal value As Long)
            Session("GoAMLWICAdd.IDEmail") = value
        End Set
    End Property

    Public Property obj_Email_Edit() As WICDataBLL.goAML_Ref_WIC_Email
        Get
            Return Session("GoAMLWICAdd.obj_Email_Edit")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Email)
            Session("GoAMLWICAdd.obj_Email_Edit") = value
        End Set
    End Property

    Public Property obj_ListEmail_Edit() As List(Of WICDataBLL.goAML_Ref_WIC_Email)
        Get
            Return Session("GoAMLWICAdd.obj_ListEmail_Edit")
        End Get
        Set(ByVal value As List(Of WICDataBLL.goAML_Ref_WIC_Email))
            Session("GoAMLWICAdd.obj_ListEmail_Edit") = value
        End Set
    End Property

    Protected Sub btn_Email_Add_Click()
        Try
            'If TxtWIC_No.Value IsNot Nothing Then
            '    If TxtWIC_No.Value = "" Then
            '        Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            '    Else
            '        DisplayWIC_Email.Value = TxtWIC_No.Value
            '    End If
            'Else
            '    Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            'End If
            TxtEmail.ReadOnly = False
            btn_Email_Save.Hidden = False
            'obj_SocialMedia_Edit = Nothing
            Clean_Window_Email()
            Window_Email.Title = "Email - Add"
            Window_Email.Hidden = False

            ClearFormPanel(FormPanelEmail)

            'Dim myFormPanel = CType(FindControl("FormPanelEmail"), FormPanel)

            'For Each control As Ext.Net.AbstractComponent In FormPanelEmail.Items
            '    Dim test = control.GetType()
            '    If TypeOf control Is TextField Then
            '        CType(control, Ext.Net.TextField).Text = ""
            '    End If

            '    ' Add other control types as needed
            'Next

            'Set IDPayment to 0
            IDEmail = 0
            obj_Email_Edit = Nothing
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdEmail(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListEmail_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListEmail_Edit.Remove(objToDelete)
                    End If
                    BindDetailEmail()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Email_Edit = obj_ListEmail_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = strID)
                    Load_Window_Email(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Load_Window_Email(strAction As String)
        'Clean window pop up
        Clean_Window_Email()

        If obj_Email_Edit IsNot Nothing Then
            With obj_Email_Edit
                DisplayWIC_Email.Value = .WIC_No
                DisplayWIC_Email.Text = .WIC_No
                TxtEmail.Value = .email_address
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            TxtEmail.ReadOnly = False
            btn_Email_Save.Hidden = False
        Else
            TxtEmail.ReadOnly = True
            btn_Email_Save.Hidden = True
        End If

        'Bind Indikator
        IDEmail = obj_Email_Edit.PK_goAML_Ref_WIC_Email_ID

        'Show window pop up
        Window_Email.Title = "Email - " & strAction
        Window_Email.Hidden = False
    End Sub

    Protected Sub btn_Email_Cancel_Click()
        Try
            'Hide window pop up
            Window_Email.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Email_Save_Click()
        Try
            If TxtEmail.Text.Trim = "" Then
                Throw New Exception("Email is required")
            End If
            If Not IsFieldValid(TxtEmail.Text.Trim, "email_address") Then
                Throw New Exception("Email Address format is not valid")
            End If
            'Action save here
            Dim intPK As Long = -1

            If obj_Email_Edit Is Nothing Then  'Add
                Dim objAdd As New WICDataBLL.goAML_Ref_WIC_Email
                If obj_ListEmail_Edit IsNot Nothing Then
                    If obj_ListEmail_Edit.Count > 0 Then
                        intPK = obj_ListEmail_Edit.Min(Function(x) x.PK_goAML_Ref_WIC_Email_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDEmail = intPK

                With objAdd
                    .PK_goAML_Ref_WIC_Email_ID = intPK
                    .WIC_No = WICNo.Text
                    .email_address = TxtEmail.Value

                End With
                If obj_ListEmail_Edit Is Nothing Then
                    obj_ListEmail_Edit = New List(Of WICDataBLL.goAML_Ref_WIC_Email)
                End If
                obj_ListEmail_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListEmail_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = obj_Email_Edit.PK_goAML_Ref_WIC_Email_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDEmail = .PK_goAML_Ref_WIC_Email_ID
                        .email_address = TxtEmail.Value

                    End With
                End If
            End If

            'Bind to GridPanel
            BindDetailEmail()

            'Hide window popup
            Window_Email.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindDetailEmail()
        Try
            'Dim listEmail As New DataTable
            'listEmail = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM [dbo].[goAML_Ref_WIC_Social_Media] where WIC_No = '13123123'")

            StoreEmail.DataSource = NawaBLL.Common.CopyGenericToDataTable(obj_ListEmail_Edit)
            StoreEmail.DataBind()

            GPWIC_Corp_Email.GetStore.DataSource = obj_ListEmail_Edit
            GPWIC_Corp_Email.GetStore.DataBind()
        Catch ex As Exception

        End Try


    End Sub

    Protected Sub Clean_Window_Email()
        'Clean fields
        TxtEmail.Value = Nothing

        'Show Buttons
        btn_Email_Save.Hidden = False
    End Sub
#End Region

#Region "Person PEP"
    Function GetWICByPKIDDetailPersonPEP(id As Long) As List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP)
        Dim WIC As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select wic_no from goAML_Ref_WIC where PK_Customer_ID = " & id, Nothing)
        Dim ListPersonPEP = New List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP)
        Dim objPersonPEPDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_WIC_Person_PEP where wic_no = '" & WIC & "'", Nothing)
        Dim ListNewPersonPEP As New List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP)
        For Each itemPersonPEP As DataRow In objPersonPEPDataTable.Rows
            Dim NewPersonPEP As New WICDataBLL.goAML_Ref_WIC_Person_PEP
            If Not IsDBNull(itemPersonPEP("PK_goAML_Ref_WIC_PEP_ID")) Then
                NewPersonPEP.PK_goAML_Ref_WIC_PEP_ID = itemPersonPEP("PK_goAML_Ref_WIC_PEP_ID")
            End If
            If Not IsDBNull(itemPersonPEP("WIC_No")) Then
                NewPersonPEP.WIC_No = itemPersonPEP("WIC_No")
            End If
            If Not IsDBNull(itemPersonPEP("pep_country")) Then
                NewPersonPEP.pep_country = itemPersonPEP("pep_country")
            End If
            If Not IsDBNull(itemPersonPEP("function_name")) Then
                NewPersonPEP.function_name = itemPersonPEP("function_name")
            End If
            If Not IsDBNull(itemPersonPEP("function_description")) Then
                NewPersonPEP.function_description = itemPersonPEP("function_description")
            End If
            If Not IsDBNull(itemPersonPEP("pep_date_range_valid_from")) Then
                NewPersonPEP.pep_date_range_valid_from = itemPersonPEP("pep_date_range_valid_from")
            End If
            If Not IsDBNull(itemPersonPEP("pep_date_range_is_approx_from_date")) Then
                NewPersonPEP.pep_date_range_is_approx_from_date = itemPersonPEP("pep_date_range_is_approx_from_date")
            End If
            If Not IsDBNull(itemPersonPEP("pep_date_range_valid_to")) Then
                NewPersonPEP.pep_date_range_valid_to = itemPersonPEP("pep_date_range_valid_to")
            End If
            If Not IsDBNull(itemPersonPEP("pep_date_range_is_approx_to_date")) Then
                NewPersonPEP.pep_date_range_is_approx_to_date = itemPersonPEP("pep_date_range_is_approx_to_date")
            End If
            If Not IsDBNull(itemPersonPEP("comments")) Then
                NewPersonPEP.comments = itemPersonPEP("comments")
            End If
            If Not IsDBNull(itemPersonPEP("Active")) Then
                NewPersonPEP.Active = itemPersonPEP("Active")
            End If
            If Not IsDBNull(itemPersonPEP("CreatedBy")) Then
                NewPersonPEP.CreatedBy = itemPersonPEP("CreatedBy")
            End If
            If Not IsDBNull(itemPersonPEP("LastUpdateBy")) Then
                NewPersonPEP.LastUpdateBy = itemPersonPEP("LastUpdateBy")
            End If
            If Not IsDBNull(itemPersonPEP("ApprovedBy")) Then
                NewPersonPEP.ApprovedBy = itemPersonPEP("ApprovedBy")
            End If
            If Not IsDBNull(itemPersonPEP("CreatedDate")) Then
                NewPersonPEP.CreatedDate = itemPersonPEP("CreatedDate")
            End If
            If Not IsDBNull(itemPersonPEP("LastUpdateDate")) Then
                NewPersonPEP.LastUpdateDate = itemPersonPEP("LastUpdateDate")
            End If
            If Not IsDBNull(itemPersonPEP("ApprovedDate")) Then
                NewPersonPEP.ApprovedDate = itemPersonPEP("LastUpdateDate")
            End If
            If Not IsDBNull(itemPersonPEP("Alternateby")) Then
                NewPersonPEP.Alternateby = itemPersonPEP("Alternateby")
            End If
            ListNewPersonPEP.Add(NewPersonPEP)
        Next
        ListPersonPEP = ListNewPersonPEP
        Return ListPersonPEP
    End Function

    Public Property IDPersonPEP() As Long
        Get
            Return Session("GoAMLWICAdd.IDPersonPEP")
        End Get
        Set(ByVal value As Long)
            Session("GoAMLWICAdd.IDPersonPEP") = value
        End Set
    End Property

    Public Property obj_PersonPEP_Edit() As WICDataBLL.goAML_Ref_WIC_Person_PEP
        Get
            Return Session("GoAMLWICAdd.obj_PersonPEP_Edit")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Person_PEP)
            Session("GoAMLWICAdd.obj_PersonPEP_Edit") = value
        End Set
    End Property

    Public Property obj_ListPersonPEP_Edit() As List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP)
        Get
            Return Session("GoAMLWICAdd.obj_ListPersonPEP_Edit")
        End Get
        Set(ByVal value As List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP))
            Session("GoAMLWICAdd.obj_ListPersonPEP_Edit") = value
        End Set
    End Property

    Protected Sub btn_Person_PEP_Add_Click()
        Try
            'If TxtWIC_No.Value IsNot Nothing Then
            '    If TxtWIC_No.Value = "" Then
            '        Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            '    Else
            '        DisplayWIC_PersonPEP.Value = TxtWIC_No.Value
            '    End If
            'Else
            '    Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            'End If
            DisplayWIC_PersonPEP.Value = WICNo.Text
            cmb_PersonPepCountryCode.SetTextValue("")
            cmb_PersonPepCountryCode.IsReadOnly = False
            TxtFunctionName.ReadOnly = False
            TxtDescription.ReadOnly = False
            TxtPersonPEPValidFrom.ReadOnly = False
            chk_PersonPEPIsApproxFromDate.ReadOnly = False
            TxtPersonPEPValidTo.ReadOnly = False
            chk_PersonPEPIsApproxToDate.ReadOnly = False
            TxtPersonPEPComments.ReadOnly = False
            btn_PersonPEP_Save.Hidden = False
            'obj_SocialMedia_Edit = Nothing
            Clean_Window_PersonPEP()
            Window_PersonPEP.Title = "Person PEP - Add"
            Window_PersonPEP.Hidden = False

            'Set IDPayment to 0
            IDPersonPEP = 0
            obj_PersonPEP_Edit = Nothing
            ClearFormPanel(FormPanelPersonPEP)
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdPersonPEP(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListPersonPEP_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_PEP_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListPersonPEP_Edit.Remove(objToDelete)
                    End If
                    BindDetailPersonPEP()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_PersonPEP_Edit = obj_ListPersonPEP_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_PEP_ID = strID)
                    Load_Window_PersonPEP(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Load_Window_PersonPEP(strAction As String)
        'Clean window pop up
        Clean_Window_PersonPEP()

        If obj_PersonPEP_Edit IsNot Nothing Then
            With obj_PersonPEP_Edit
                'vw_goAML_Ref_Nama_Negara
                Dim refCountry = goAML_CustomerBLL.GetReferenceByCode("vw_goAML_Ref_Nama_Negara", .pep_country)

                DisplayWIC_PersonPEP.Value = .WIC_No
                DisplayWIC_PersonPEP.Text = .WIC_No
                cmb_PersonPepCountryCode.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                TxtFunctionName.Value = .function_name
                TxtDescription.Value = .function_description
                TxtPersonPEPValidFrom.SelectedDate = .pep_date_range_valid_from
                chk_PersonPEPIsApproxFromDate.Checked = .pep_date_range_is_approx_from_date
                TxtPersonPEPValidTo.SelectedDate = .pep_date_range_valid_to
                chk_PersonPEPIsApproxToDate.Checked = .pep_date_range_is_approx_to_date
                TxtPersonPEPComments.Value = .comments
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            cmb_PersonPepCountryCode.IsReadOnly = False
            TxtFunctionName.ReadOnly = False
            TxtDescription.ReadOnly = False
            TxtPersonPEPValidFrom.ReadOnly = False
            chk_PersonPEPIsApproxFromDate.ReadOnly = False
            TxtPersonPEPValidTo.ReadOnly = False
            chk_PersonPEPIsApproxToDate.ReadOnly = False
            TxtPersonPEPComments.ReadOnly = False
            btn_PersonPEP_Save.Hidden = False
        Else
            cmb_PersonPepCountryCode.IsReadOnly = False
            TxtFunctionName.ReadOnly = False
            TxtDescription.ReadOnly = False
            TxtPersonPEPValidFrom.ReadOnly = False
            chk_PersonPEPIsApproxFromDate.ReadOnly = False
            TxtPersonPEPValidTo.ReadOnly = False
            chk_PersonPEPIsApproxToDate.ReadOnly = False
            TxtPersonPEPComments.ReadOnly = False
            btn_PersonPEP_Save.Hidden = True
        End If

        'Bind Indikator
        IDPersonPEP = obj_PersonPEP_Edit.PK_goAML_Ref_WIC_PEP_ID

        'Show window pop up
        Window_PersonPEP.Title = "Person PEP - " & strAction
        Window_PersonPEP.Hidden = False
    End Sub

    Protected Sub btn_Person_PEP_Cancel_Click()
        Try
            'Hide window pop up
            Window_PersonPEP.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Person_PEP_Save_Click()
        Try
            'Action save here
            If cmb_PersonPepCountryCode.SelectedItemValue Is Nothing Then
                Throw New Exception("Country is required")
            End If

            Dim intPK As Long = -1

            If obj_PersonPEP_Edit Is Nothing Then  'Add
                Dim objAdd As New WICDataBLL.goAML_Ref_WIC_Person_PEP
                If obj_ListPersonPEP_Edit IsNot Nothing Then
                    If obj_ListPersonPEP_Edit.Count > 0 Then
                        intPK = obj_ListPersonPEP_Edit.Min(Function(x) x.PK_goAML_Ref_WIC_PEP_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDPersonPEP = intPK
                With objAdd
                    .PK_goAML_Ref_WIC_PEP_ID = intPK
                    .WIC_No = WICNo.Text
                    .pep_country = cmb_PersonPepCountryCode.SelectedItemValue
                    .function_name = TxtFunctionName.Value
                    .function_description = TxtDescription.Value
                    .pep_date_range_valid_from = TxtPersonPEPValidFrom.SelectedDate
                    .pep_date_range_is_approx_from_date = chk_PersonPEPIsApproxFromDate.Checked
                    .pep_date_range_valid_to = TxtPersonPEPValidTo.SelectedDate
                    .pep_date_range_is_approx_to_date = chk_PersonPEPIsApproxToDate.Checked
                    .comments = TxtPersonPEPComments.Value

                End With
                If obj_ListPersonPEP_Edit Is Nothing Then
                    obj_ListPersonPEP_Edit = New List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP)
                End If
                obj_ListPersonPEP_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListPersonPEP_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_PEP_ID = obj_PersonPEP_Edit.PK_goAML_Ref_WIC_PEP_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDPersonPEP = .PK_goAML_Ref_WIC_PEP_ID
                        .pep_country = cmb_PersonPepCountryCode.SelectedItemValue
                        .function_name = TxtFunctionName.Value
                        .function_description = TxtDescription.Value
                        .pep_date_range_valid_from = TxtPersonPEPValidFrom.SelectedDate
                        .pep_date_range_is_approx_from_date = chk_PersonPEPIsApproxFromDate.Checked
                        .pep_date_range_valid_to = TxtPersonPEPValidTo.SelectedDate
                        .pep_date_range_is_approx_to_date = chk_PersonPEPIsApproxToDate.Checked
                        .comments = TxtPersonPEPComments.Value

                    End With
                End If
            End If

            'Bind to GridPanel
            BindDetailPersonPEP()

            'Hide window popup
            Window_PersonPEP.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindDetailPersonPEP()
        Try
            'Dim listPersonPEP = objgoAML_WIC.GetPEP(ObjWIC.WIC_No, 3)

            GridPanelPersonPEP.GetStore.DataSource = obj_ListPersonPEP_Edit
            GridPanelPersonPEP.GetStore.DataBind()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub Clean_Window_PersonPEP()
        'Clean fields
        cmb_PersonPepCountryCode.IsReadOnly = False
        TxtFunctionName.ReadOnly = False
        TxtDescription.ReadOnly = False
        TxtPersonPEPValidFrom.ReadOnly = False
        chk_PersonPEPIsApproxFromDate.ReadOnly = False
        TxtPersonPEPValidTo.ReadOnly = False
        chk_PersonPEPIsApproxToDate.ReadOnly = False
        TxtPersonPEPComments.ReadOnly = False
        'Show Buttons
        btn_PersonPEP_Save.Hidden = False
    End Sub
#End Region

#Region "Sanction"

    Function GetWICByPKIDDetailSanction(id As Long) As List(Of WICDataBLL.goAML_Ref_WIC_Sanction)
        'Sanction
        Dim WIC As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select wic_no from goAML_Ref_WIC where PK_Customer_ID = " & id, Nothing)

        Dim ListSanction = New List(Of WICDataBLL.goAML_Ref_WIC_Sanction)
        Dim objSanctionDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_WIC_Sanction where wic_no = '" & WIC & "'", Nothing)
        Dim ListNewSanction As New List(Of WICDataBLL.goAML_Ref_WIC_Sanction)
        For Each itemAddress As DataRow In objSanctionDataTable.Rows
            Dim NewSanction As New WICDataBLL.goAML_Ref_WIC_Sanction
            If Not IsDBNull(itemAddress("PK_goAML_Ref_WIC_Sanction_ID")) Then
                NewSanction.PK_goAML_Ref_WIC_Sanction_ID = itemAddress("PK_goAML_Ref_WIC_Sanction_ID")
            End If
            If Not IsDBNull(itemAddress("WIC_No")) Then
                NewSanction.WIC_No = itemAddress("WIC_No")
            End If
            If Not IsDBNull(itemAddress("Provider")) Then
                NewSanction.Provider = itemAddress("Provider")
            End If
            If Not IsDBNull(itemAddress("Sanction_List_Name")) Then
                NewSanction.Sanction_List_Name = itemAddress("Sanction_List_Name")
            End If
            If Not IsDBNull(itemAddress("Match_Criteria")) Then
                NewSanction.Match_Criteria = itemAddress("Match_Criteria")
            End If
            If Not IsDBNull(itemAddress("Link_To_Source")) Then
                NewSanction.Link_To_Source = itemAddress("Link_To_Source")
            End If
            If Not IsDBNull(itemAddress("Sanction_List_Attributes")) Then
                NewSanction.Sanction_List_Attributes = itemAddress("Sanction_List_Attributes")
            End If
            If Not IsDBNull(itemAddress("Sanction_List_Date_Range_Valid_From")) Then
                NewSanction.Sanction_List_Date_Range_Valid_From = itemAddress("Sanction_List_Date_Range_Valid_From")
            End If
            If Not IsDBNull(itemAddress("Sanction_List_Date_Range_Is_Approx_From_Date")) Then
                NewSanction.Sanction_List_Date_Range_Is_Approx_From_Date = itemAddress("Sanction_List_Date_Range_Is_Approx_From_Date")
            End If
            If Not IsDBNull(itemAddress("Sanction_List_Date_Range_Valid_To")) Then
                NewSanction.Sanction_List_Date_Range_Valid_To = itemAddress("Sanction_List_Date_Range_Valid_To")
            End If
            If Not IsDBNull(itemAddress("Sanction_List_Date_Range_Is_Approx_To_Date")) Then
                NewSanction.Sanction_List_Date_Range_Is_Approx_To_Date = itemAddress("Sanction_List_Date_Range_Is_Approx_To_Date")
            End If
            If Not IsDBNull(itemAddress("Comments")) Then
                NewSanction.Comments = itemAddress("Comments")
            End If
            If Not IsDBNull(itemAddress("Active")) Then
                NewSanction.Active = itemAddress("Active")
            End If
            If Not IsDBNull(itemAddress("CreatedBy")) Then
                NewSanction.CreatedBy = itemAddress("CreatedBy")
            End If
            If Not IsDBNull(itemAddress("LastUpdateBy")) Then
                NewSanction.LastUpdateBy = itemAddress("LastUpdateBy")
            End If
            If Not IsDBNull(itemAddress("ApprovedBy")) Then
                NewSanction.ApprovedBy = itemAddress("ApprovedBy")
            End If
            If Not IsDBNull(itemAddress("CreatedDate")) Then
                NewSanction.CreatedDate = itemAddress("CreatedDate")
            End If
            If Not IsDBNull(itemAddress("LastUpdateDate")) Then
                NewSanction.LastUpdateDate = itemAddress("LastUpdateDate")
            End If
            If Not IsDBNull(itemAddress("ApprovedDate")) Then
                NewSanction.ApprovedDate = itemAddress("LastUpdateDate")
            End If
            If Not IsDBNull(itemAddress("Alternateby")) Then
                NewSanction.Alternateby = itemAddress("Alternateby")
            End If
            ListNewSanction.Add(NewSanction)
        Next
        ListSanction = ListNewSanction
        Return ListSanction
    End Function

    Public Property IDSanction() As Long
        Get
            Return Session("goAML_WICEdit_V501.IDSanction")
        End Get
        Set(ByVal value As Long)
            Session("goAML_WICEdit_V501.IDSanction") = value
        End Set
    End Property

    Public Property obj_Sanction_Edit() As WICDataBLL.goAML_Ref_WIC_Sanction
        Get
            Return Session("goAML_WICEdit_V501.obj_Sanction_Edit")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Sanction)
            Session("goAML_WICEdit_V501.obj_Sanction_Edit") = value
        End Set
    End Property

    Public Property obj_ListSanction_Edit() As List(Of WICDataBLL.goAML_Ref_WIC_Sanction)
        Get
            Return Session("goAML_WICEdit_V501.obj_ListSanction_Edit")
        End Get
        Set(ByVal value As List(Of WICDataBLL.goAML_Ref_WIC_Sanction))
            Session("goAML_WICEdit_V501.obj_ListSanction_Edit") = value
        End Set
    End Property

    Protected Sub btn_Sanction_Add_Click()
        Try
            'If TxtWIC_No.Value IsNot Nothing Then
            '    If TxtWIC_No.Value = "" Then
            '        Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            '    Else
            '        DisplayWIC_Sanction.Value = TxtWIC_No.Value
            '    End If
            'Else
            '    Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            'End If
            'Clean window pop up
            Clean_Window_Sanction()
            DisplayWIC_Sanction.Value = WICNo.Text
            TxtProvider.ReadOnly = False
            TxtSanctionListName.ReadOnly = False
            TxtMatchCriteria.ReadOnly = False
            TxtLinkToSource.ReadOnly = False
            TxtSanctionListAttributes.ReadOnly = False
            dt_SanctionValidFrom.ReadOnly = False
            chk_SanctionIsApproxFromDate.ReadOnly = False
            dt_SanctionValidTo.ReadOnly = False
            chk_SanctionIsApproxToDate.ReadOnly = False
            TxtSanctionComments.ReadOnly = False
            btn_Sanction_Save.Hidden = False
            'obj_SocialMedia_Edit = Nothing
            Clean_Window_Sanction()
            Window_Sanction.Title = "Sanction - Add"
            Window_Sanction.Hidden = False

            'Set IDPayment to 0
            IDSanction = 0
            obj_Sanction_Edit = Nothing
            ClearFormPanel(FormPanelSanction)
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdSanction(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListSanction_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListSanction_Edit.Remove(objToDelete)
                    End If
                    BindDetailSanction()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Sanction_Edit = obj_ListSanction_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = strID)
                    Load_Window_Sanction(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Load_Window_Sanction(strAction As String)
        'Clean window pop up
        Clean_Window_Sanction()

        If obj_Sanction_Edit IsNot Nothing Then
            With obj_Sanction_Edit
                DisplayWIC_Sanction.Text = .WIC_No
                TxtProvider.Value = .Provider
                TxtSanctionListName.Value = .Sanction_List_Name
                TxtMatchCriteria.Value = .Match_Criteria
                TxtLinkToSource.Value = .Link_To_Source
                TxtSanctionListAttributes.Value = .Sanction_List_Attributes
                'dt_SanctionValidFrom.SelectedDate = .Sanction_List_Date_Range_Valid_From
                'chk_SanctionIsApproxFromDate.Checked = .Sanction_List_Date_Range_Is_Approx_From_Date
                'dt_SanctionValidTo.SelectedDate = .Sanction_List_Date_Range_Valid_To
                'chk_SanctionIsApproxToDate.Checked = .Sanction_List_Date_Range_Is_Approx_To_Date
                TxtSanctionComments.Value = .Comments
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            TxtProvider.ReadOnly = False
            TxtSanctionListName.ReadOnly = False
            TxtMatchCriteria.ReadOnly = False
            TxtLinkToSource.ReadOnly = False
            TxtSanctionListAttributes.ReadOnly = False
            dt_SanctionValidFrom.ReadOnly = False
            chk_SanctionIsApproxFromDate.ReadOnly = False
            dt_SanctionValidTo.ReadOnly = False
            chk_SanctionIsApproxToDate.ReadOnly = False
            TxtSanctionComments.ReadOnly = False
            btn_Sanction_Save.Hidden = False
        Else
            TxtProvider.ReadOnly = False
            TxtSanctionListName.ReadOnly = False
            TxtMatchCriteria.ReadOnly = False
            TxtLinkToSource.ReadOnly = False
            TxtSanctionListAttributes.ReadOnly = False
            dt_SanctionValidFrom.ReadOnly = False
            chk_SanctionIsApproxFromDate.ReadOnly = False
            dt_SanctionValidTo.ReadOnly = False
            chk_SanctionIsApproxToDate.ReadOnly = False
            TxtSanctionComments.ReadOnly = False
            btn_Sanction_Save.Hidden = True
        End If

        'Bind Indikator
        IDSanction = obj_Sanction_Edit.PK_goAML_Ref_WIC_Sanction_ID

        'Show window pop up
        Window_Sanction.Title = "Sanction - " & strAction
        Window_Sanction.Hidden = False
    End Sub

    Protected Sub btn_Sanction_Cancel_Click()
        Try
            'Hide window pop up
            Window_Sanction.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Sanction_Save_Click()
        Try
            If TxtProvider.Text.Trim = "" Then
                Throw New Exception("Provider No is required")
            End If

            If TxtSanctionListName.Text.Trim = "" Then
                Throw New Exception("Sanction List Name is required")
            End If
            'Action save here
            Dim intPK As Long = -1

            If obj_Sanction_Edit Is Nothing Then  'Add
                Dim objAdd As New WICDataBLL.goAML_Ref_WIC_Sanction
                If obj_ListSanction_Edit IsNot Nothing Then
                    If obj_ListSanction_Edit.Count > 0 Then
                        intPK = obj_ListSanction_Edit.Min(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDSanction = intPK
                With objAdd
                    .PK_goAML_Ref_WIC_Sanction_ID = intPK
                    .WIC_No = WICNo.Text
                    .Provider = TxtProvider.Value
                    .Sanction_List_Name = TxtSanctionListName.Value
                    .Match_Criteria = TxtMatchCriteria.Value
                    .Link_To_Source = TxtLinkToSource.Value
                    .Sanction_List_Attributes = TxtSanctionListAttributes.Value
                    .Sanction_List_Date_Range_Valid_From = dt_SanctionValidFrom.SelectedDate
                    .Sanction_List_Date_Range_Is_Approx_From_Date = chk_SanctionIsApproxFromDate.Checked
                    .Sanction_List_Date_Range_Valid_To = dt_SanctionValidTo.SelectedDate
                    .Sanction_List_Date_Range_Is_Approx_To_Date = chk_SanctionIsApproxToDate.Checked
                    .Comments = TxtSanctionComments.Value
                    .FK_REF_DETAIL_OF = 3
                End With
                If obj_ListSanction_Edit Is Nothing Then
                    obj_ListSanction_Edit = New List(Of WICDataBLL.goAML_Ref_WIC_Sanction)
                End If
                obj_ListSanction_Edit.Add(objAdd)
            Else    'Edit
                Dim objEdit = obj_ListSanction_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = obj_Sanction_Edit.PK_goAML_Ref_WIC_Sanction_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDSanction = .PK_goAML_Ref_WIC_Sanction_ID
                        .WIC_No = WICNo.Text
                        .Provider = TxtProvider.Value
                        .Sanction_List_Name = TxtSanctionListName.Value
                        .Match_Criteria = TxtMatchCriteria.Value
                        .Link_To_Source = TxtLinkToSource.Value
                        .Sanction_List_Attributes = TxtSanctionListAttributes.Value
                        .Sanction_List_Date_Range_Valid_From = dt_SanctionValidFrom.SelectedDate
                        .Sanction_List_Date_Range_Is_Approx_From_Date = chk_SanctionIsApproxFromDate.Checked
                        .Sanction_List_Date_Range_Valid_To = dt_SanctionValidTo.SelectedDate
                        .Sanction_List_Date_Range_Is_Approx_To_Date = chk_SanctionIsApproxToDate.Checked
                        .Comments = TxtSanctionComments.Value
                        .FK_REF_DETAIL_OF = 3
                    End With
                End If
            End If

            'Bind to GridPanel
            BindDetailSanction()

            'Hide window popup
            Window_Sanction.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindDetailSanction()
        Try
            'Dim listSanction = objgoAML_WIC.GetSanction(ObjWIC.WIC_No, 3)

            StoreSanction.DataSource = NawaBLL.Common.CopyGenericToDataTable(obj_ListSanction_Edit)
            StoreSanction.DataBind()

            GPWIC_CORP_Sanction.GetStore.DataSource = obj_ListSanction_Edit
            GPWIC_CORP_Sanction.GetStore.DataBind()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub Clean_Window_Sanction()
        'Clean fields
        TxtProvider.Value = Nothing
        TxtSanctionListName.Value = Nothing
        TxtMatchCriteria.Value = Nothing
        TxtLinkToSource.Value = Nothing
        TxtSanctionListAttributes.Value = Nothing
        dt_SanctionValidFrom.Value = Nothing
        chk_SanctionIsApproxFromDate.Checked = False
        dt_SanctionValidTo.Value = Nothing
        chk_SanctionIsApproxToDate.Checked = False
        TxtSanctionComments.Value = Nothing
        'Show Buttons
        btn_Sanction_Save.Hidden = False
    End Sub
#End Region

#Region "Related Person"

    Function GetWICByPKIDDetailRelatedPerson(id As Long) As List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)
        Dim WIC As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT WIC_NO FROM goAML_Ref_WIC WHERE PK_Customer_ID = " & id, Nothing)
        Dim ListRelatedPerson = New List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)
        Dim objRelatedPersonDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM goAML_Ref_WIC_Related_Person WHERE WIC_NO = '" & WIC & "'", Nothing)
        Dim ListNewRelatedPerson As New List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)
        For Each itemRelatedPerson As DataRow In objRelatedPersonDataTable.Rows
            Dim NewRelatedPerson As New WICDataBLL.goAML_Ref_WIC_Related_Person
            If Not IsDBNull(itemRelatedPerson("PK_goAML_Ref_WIC_Related_Person_ID")) Then
                NewRelatedPerson.PK_goAML_Ref_WIC_Related_Person_ID = itemRelatedPerson("PK_goAML_Ref_WIC_Related_Person_ID")
            End If
            If Not IsDBNull(itemRelatedPerson("WIC_No")) Then
                NewRelatedPerson.WIC_No = itemRelatedPerson("WIC_No")
            End If
            If Not IsDBNull(itemRelatedPerson("Person_Person_Relation")) Then
                NewRelatedPerson.Person_Person_Relation = itemRelatedPerson("Person_Person_Relation")
            End If
            If Not IsDBNull(itemRelatedPerson("Relation_Date_Range_Valid_From")) Then
                NewRelatedPerson.Relation_Date_Range_Valid_From = itemRelatedPerson("Relation_Date_Range_Valid_From")
            End If
            If Not IsDBNull(itemRelatedPerson("Relation_Date_Range_Is_Approx_From_Date")) Then
                NewRelatedPerson.Relation_Date_Range_Is_Approx_From_Date = itemRelatedPerson("Relation_Date_Range_Is_Approx_From_Date")
            End If
            If Not IsDBNull(itemRelatedPerson("Relation_Date_Range_Valid_To")) Then
                NewRelatedPerson.Relation_Date_Range_Valid_To = itemRelatedPerson("Relation_Date_Range_Valid_To")
            End If
            If Not IsDBNull(itemRelatedPerson("Relation_Date_Range_Is_Approx_To_Date")) Then
                NewRelatedPerson.Relation_Date_Range_Is_Approx_To_Date = itemRelatedPerson("Relation_Date_Range_Is_Approx_To_Date")
            End If
            If Not IsDBNull(itemRelatedPerson("Comments")) Then
                NewRelatedPerson.Comments = itemRelatedPerson("Comments")
            End If
            If Not IsDBNull(itemRelatedPerson("Gender")) Then
                NewRelatedPerson.Gender = itemRelatedPerson("Gender")
            End If
            If Not IsDBNull(itemRelatedPerson("Title")) Then
                NewRelatedPerson.Title = itemRelatedPerson("Title")
            End If
            If Not IsDBNull(itemRelatedPerson("First_Name")) Then
                NewRelatedPerson.First_Name = itemRelatedPerson("First_Name")
            End If
            If Not IsDBNull(itemRelatedPerson("Middle_Name")) Then
                NewRelatedPerson.Middle_Name = itemRelatedPerson("Middle_Name")
            End If
            If Not IsDBNull(itemRelatedPerson("Prefix")) Then
                NewRelatedPerson.Prefix = itemRelatedPerson("Prefix")
            End If
            If Not IsDBNull(itemRelatedPerson("Last_Name")) Then
                NewRelatedPerson.Last_Name = itemRelatedPerson("Last_Name")
            End If
            If Not IsDBNull(itemRelatedPerson("Birth_Date")) Then
                NewRelatedPerson.Birth_Date = itemRelatedPerson("Birth_Date")
            End If
            If Not IsDBNull(itemRelatedPerson("Birth_Place")) Then
                NewRelatedPerson.Birth_Place = itemRelatedPerson("Birth_Place")
            End If
            If Not IsDBNull(itemRelatedPerson("Country_Of_Birth")) Then
                NewRelatedPerson.Country_Of_Birth = itemRelatedPerson("Country_Of_Birth")
            End If
            If Not IsDBNull(itemRelatedPerson("Mother_Name")) Then
                NewRelatedPerson.Mother_Name = itemRelatedPerson("Mother_Name")
            End If
            If Not IsDBNull(itemRelatedPerson("Alias")) Then
                NewRelatedPerson._Alias = itemRelatedPerson("Alias")
            End If
            If Not IsDBNull(itemRelatedPerson("Full_Name_Frn")) Then
                NewRelatedPerson.Full_Name_Frn = itemRelatedPerson("Full_Name_Frn")
            End If
            If Not IsDBNull(itemRelatedPerson("SSN")) Then
                NewRelatedPerson.SSN = itemRelatedPerson("SSN")
            End If
            If Not IsDBNull(itemRelatedPerson("Passport_Number")) Then
                NewRelatedPerson.Passport_Number = itemRelatedPerson("Passport_Number")
            End If
            If Not IsDBNull(itemRelatedPerson("Passport_Country")) Then
                NewRelatedPerson.Passport_Country = itemRelatedPerson("Passport_Country")
            End If
            If Not IsDBNull(itemRelatedPerson("ID_Number")) Then
                NewRelatedPerson.ID_Number = itemRelatedPerson("ID_Number")
            End If
            If Not IsDBNull(itemRelatedPerson("Nationality1")) Then
                NewRelatedPerson.Nationality1 = itemRelatedPerson("Nationality1")
            End If
            If Not IsDBNull(itemRelatedPerson("Nationality2")) Then
                NewRelatedPerson.Nationality2 = itemRelatedPerson("Nationality2")
            End If
            If Not IsDBNull(itemRelatedPerson("Nationality3")) Then
                NewRelatedPerson.Nationality3 = itemRelatedPerson("Nationality3")
            End If
            If Not IsDBNull(itemRelatedPerson("Residence")) Then
                NewRelatedPerson.Residence = itemRelatedPerson("Residence")
            End If
            If Not IsDBNull(itemRelatedPerson("Residence_Since")) Then
                NewRelatedPerson.Residence_Since = itemRelatedPerson("Residence_Since")
            End If
            If Not IsDBNull(itemRelatedPerson("Occupation")) Then
                NewRelatedPerson.Occupation = itemRelatedPerson("Occupation")
            End If
            If Not IsDBNull(itemRelatedPerson("Deceased")) Then
                NewRelatedPerson.Deceased = itemRelatedPerson("Deceased")
            End If
            If Not IsDBNull(itemRelatedPerson("Date_Deceased")) Then
                NewRelatedPerson.Date_Deceased = itemRelatedPerson("Date_Deceased")
            End If
            If Not IsDBNull(itemRelatedPerson("Tax_Number")) Then
                NewRelatedPerson.Tax_Number = itemRelatedPerson("Tax_Number")
            End If
            If Not IsDBNull(itemRelatedPerson("Is_PEP")) Then
                NewRelatedPerson.Is_PEP = IIf(itemRelatedPerson("Is_PEP") = "1", True, False)
            End If
            If Not IsDBNull(itemRelatedPerson("Source_Of_Wealth")) Then
                NewRelatedPerson.Source_Of_Wealth = itemRelatedPerson("Source_Of_Wealth")
            End If
            If Not IsDBNull(itemRelatedPerson("Is_Protected")) Then
                NewRelatedPerson.Is_Protected = itemRelatedPerson("Is_Protected")
            End If
            If Not IsDBNull(itemRelatedPerson("Active")) Then
                NewRelatedPerson.Active = itemRelatedPerson("Active")
            End If
            If Not IsDBNull(itemRelatedPerson("CreatedBy")) Then
                NewRelatedPerson.CreatedBy = itemRelatedPerson("CreatedBy")
            End If
            If Not IsDBNull(itemRelatedPerson("LastUpdateBy")) Then
                NewRelatedPerson.LastUpdateBy = itemRelatedPerson("LastUpdateBy")
            End If
            If Not IsDBNull(itemRelatedPerson("ApprovedBy")) Then
                NewRelatedPerson.ApprovedBy = itemRelatedPerson("ApprovedBy")
            End If
            If Not IsDBNull(itemRelatedPerson("CreatedDate")) Then
                NewRelatedPerson.CreatedDate = itemRelatedPerson("CreatedDate")
            End If
            If Not IsDBNull(itemRelatedPerson("LastUpdateDate")) Then
                NewRelatedPerson.LastUpdateDate = itemRelatedPerson("LastUpdateDate")
            End If
            If Not IsDBNull(itemRelatedPerson("ApprovedDate")) Then
                NewRelatedPerson.ApprovedDate = itemRelatedPerson("LastUpdateDate")
            End If
            If Not IsDBNull(itemRelatedPerson("Alternateby")) Then
                NewRelatedPerson.Alternateby = itemRelatedPerson("Alternateby")
            End If
            ' 2023-11-10, Nael
            If Not IsDBNull(itemRelatedPerson("EMPLOYER_NAME")) Then
                NewRelatedPerson.EMPLOYER_NAME = itemRelatedPerson("EMPLOYER_NAME")
            End If
            If Not IsDBNull(itemRelatedPerson("relation_comments")) Then ' 2023-11-13, Nael
                NewRelatedPerson.relation_comments = itemRelatedPerson("relation_comments")
            End If

            Dim pk_id = itemRelatedPerson("PK_goAML_Ref_WIC_Related_Person_ID")

            NewRelatedPerson.ObjList_GoAML_Ref_Address = goAML_Customer_Service.GetAddressByPkFk(pk_id, 31)
            NewRelatedPerson.ObjList_GoAML_Ref_Address_Work = goAML_Customer_Service.GetAddressByPkFk(pk_id, 32)
            NewRelatedPerson.ObjList_GoAML_Ref_Phone = goAML_Customer_Service.GetPhoneByPkFk(pk_id, 31)
            NewRelatedPerson.ObjList_GoAML_Ref_Phone_Work = goAML_Customer_Service.GetPhoneByPkFk(pk_id, 32)
            NewRelatedPerson.ObjList_GoAML_Person_Identification = goAML_Customer_Service.GetIdentificationPkFk(pk_id, 31)
            NewRelatedPerson.ObjList_GoAML_Ref_Customer_Email = goAML_Customer_Service.WICEmailService.GetByPkFk(pk_id, 31)
            NewRelatedPerson.ObjList_GoAML_Ref_Customer_Sanction = goAML_Customer_Service.WICSanctionService.GetByPkFk(pk_id, 31)
            NewRelatedPerson.ObjList_GoAML_Ref_Customer_PEP = goAML_Customer_Service.WICPersonPEPService.GetByPkFk(pk_id, 31)

            ListNewRelatedPerson.Add(NewRelatedPerson)
        Next
        ListRelatedPerson = ListNewRelatedPerson
        Return ListRelatedPerson
    End Function

    Public Property IDRelatedPerson() As Long
        Get
            Return Session("goAML_WICEdit_V501.IDRelatedPerson")
        End Get
        Set(ByVal value As Long)
            Session("goAML_WICEdit_V501.IDRelatedPerson") = value
        End Set
    End Property

    Public Property obj_RelatedPerson_Edit() As WICDataBLL.goAML_Ref_WIC_Related_Person
        Get
            Return Session("goAML_WICEdit_V501.obj_RelatedPerson_Edit")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Related_Person)
            Session("goAML_WICEdit_V501.obj_RelatedPerson_Edit") = value
        End Set
    End Property

    Public Property obj_ListRelatedPerson_Edit() As List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)
        Get
            Return Session("goAML_WICEdit_V501.obj_ListRelatedPerson_Edit")
        End Get
        Set(ByVal value As List(Of WICDataBLL.goAML_Ref_WIC_Related_Person))
            Session("goAML_WICEdit_V501.obj_ListRelatedPerson_Edit") = value
        End Set
    End Property

    Protected Sub btn_Related_Person_Add_Click()
        Try
            'If TxtWIC_No.Value IsNot Nothing Then
            '    If TxtWIC_No.Value = "" Then
            '        Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            '    Else
            '        DisplayWIC_RelatedPerson.Value = TxtWIC_No.Value
            '    End If
            'Else
            '    Throw New ApplicationException(TxtWIC_No.FieldLabel & " harus diisi.")
            'End If

            GridAddressWork_RelatedPerson.IsViewMode = False
            GridAddress_RelatedPerson.IsViewMode = False
            GridPhone_RelatedPerson.IsViewMode = False
            GridPhoneWork_RelatedPerson.IsViewMode = False
            GridIdentification_RelatedPerson.IsViewMode = False
            GridEmail_RelatedPerson.IsViewMode = False
            GridSanction_RelatedPerson.IsViewMode = False
            GridPEP_RelatedPerson.IsViewMode = False

            GridAddressWork_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Address))
            GridAddress_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Address))
            GridPhone_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Phone))
            GridPhoneWork_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Phone))
            GridIdentification_RelatedPerson.LoadData(New List(Of DataModel.goAML_Person_Identification))
            GridEmail_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Customer_Email))
            GridSanction_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Customer_Sanction))
            GridPEP_RelatedPerson.LoadData(New List(Of DataModel.goAML_Ref_Customer_PEP))
            DisplayWIC_RelatedPerson.Text = WICNo.Text
            dd_Person_Person_Relation.IsReadOnly = False
            'dt_Relation_Date_Range_Valid_From.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_From_Date.ReadOnly = False
            'dt_Relation_Date_Range_Valid_To.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_To_Date.ReadOnly = False
            TxtRelatedPersonComments.ReadOnly = False
            TxtRelatedPersonRelationComments.ReadOnly = False
            'TxtRelatedPersonGender.ReadOnly = False
            cmb_rp_gender.IsReadOnly = False
            TxtRelatedPersonTitle.ReadOnly = False
            TxtRelatedPersonFirst_Name.ReadOnly = False
            TxtRelatedPersonMiddle_Name.ReadOnly = False
            TxtRelatedPersonPrefix.ReadOnly = False
            TxtRelatedPersonLast_Name.ReadOnly = False
            dt_RelatedPersonBirth_Date.ReadOnly = False
            TxtRelatedPersonBirth_Place.ReadOnly = False
            dd_RelatedPersonCountry_Of_Birth.IsReadOnly = False
            TxtRelatedPersonMother_Name.ReadOnly = False
            TxtRelatedPersonAlias.ReadOnly = False
            'TxtRelatedPersonFull_Name_Frn.ReadOnly = False
            TxtRelatedPersonSSN.ReadOnly = False
            TxtPassport_Number.ReadOnly = False
            dd_Passport_Country.IsReadOnly = False
            TxtRelatedPersonID_Number.ReadOnly = False
            dd_RelatedPersonNationality1.IsReadOnly = False
            dd_RelatedPersonNationality2.IsReadOnly = False
            dd_RelatedPersonNationality3.IsReadOnly = False
            dd_RelatedPersonResidence.IsReadOnly = False
            'dt_RelatedPersonResidence_Since.ReadOnly = False
            TxtRelatedPersonOccupation.ReadOnly = False
            TxtRelatedPersonEmployerName.ReadOnly = False
            chk_RelatedPersonDeceased.ReadOnly = False
            dt_TxtRelatedPersonDate_Deceased.ReadOnly = False
            TxtRelatedPersonTax_Number.ReadOnly = False
            chk_RelatedPersonIs_PEP.ReadOnly = False
            TxtRelatedPersonSource_Of_Wealth.ReadOnly = False
            chk_RelatedPersonIs_Protected.ReadOnly = False
            btn_RelatedPerson_Save.Hidden = False
            'obj_SocialMedia_Edit = Nothing
            Clean_Window_RelatedPerson()
            Window_RelatedPerson.Title = "Related Person - Add"
            Window_RelatedPerson.Hidden = False

            'Set IDPayment to 0
            IDRelatedPerson = 0
            obj_RelatedPerson_Edit = Nothing
            ClearFormPanel(FormPanelRelatedPerson)
            dd_Person_Person_Relation.SetTextValue("")
            cmb_rp_gender.SetTextValue("")
            dd_RelatedPersonCountry_Of_Birth.SetTextValue("")
            dd_Passport_Country.SetTextValue("")
            dd_RelatedPersonNationality1.SetTextValue("")
            dd_RelatedPersonNationality2.SetTextValue("")
            dd_RelatedPersonNationality3.SetTextValue("")
            dd_RelatedPersonResidence.SetTextValue("")
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdRelatedPerson(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListRelatedPerson_Edit.Remove(objToDelete)
                    End If
                    BindDetailRelatedPerson()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_RelatedPerson_Edit = obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = strID)
                    Load_Window_RelatedPerson(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Load_Window_RelatedPerson(strAction As String)
        'Clean window pop up
        Clean_Window_RelatedPerson()


        If obj_RelatedPerson_Edit IsNot Nothing Then
            With obj_RelatedPerson_Edit
                Dim refPersonRelation = WICBLL.GetReferenceByCode("goAML_Ref_Person_Person_Relationship", .Person_Person_Relation)
                Dim refCountryBirth = WICBLL.GetReferenceByCode("vw_goAML_Ref_Nama_Negara", .Country_Of_Birth)
                Dim refPassportCountry = WICBLL.GetReferenceByCode("vw_goAML_Ref_Nama_Negara", .Passport_Country)
                Dim refNationality1 = WICBLL.GetReferenceByCode("vw_goAML_Ref_Nama_Negara", .Nationality1)
                Dim refNationality2 = WICBLL.GetReferenceByCode("vw_goAML_Ref_Nama_Negara", .Nationality2)
                Dim refNationality3 = WICBLL.GetReferenceByCode("vw_goAML_Ref_Nama_Negara", .Nationality3)
                Dim refResidence = WICBLL.GetReferenceByCode("vw_goAML_Ref_Nama_Negara", .Residence)

                DisplayWIC_RelatedPerson.Text = .WIC_No
                If refPersonRelation IsNot Nothing Then
                    dd_Person_Person_Relation.SetTextWithTextValue(refPersonRelation.Kode, refPersonRelation.Keterangan)
                End If

                'dt_Relation_Date_Range_Valid_From.SelectedDate = .Relation_Date_Range_Valid_From
                'chk_Relation_Date_Range_Is_Approx_From_Date.Checked = .Relation_Date_Range_Is_Approx_From_Date
                'dt_Relation_Date_Range_Valid_To.SelectedDate = .Relation_Date_Range_Valid_To
                'chk_Relation_Date_Range_Is_Approx_To_Date.Checked = .Relation_Date_Range_Is_Approx_To_Date
                TxtRelatedPersonComments.Value = .Comments
                TxtRelatedPersonRelationComments.Value = .relation_comments
                'TxtRelatedPersonGender.Value = .Gender
                Dim refGender = WICBLL.GetReferenceByCode("goAML_Ref_Jenis_Kelamin", .Gender)
                If refGender IsNot Nothing Then
                    cmb_rp_gender.SetTextWithTextValue(refGender.Kode, refGender.Keterangan)
                End If
                TxtRelatedPersonTitle.Value = .Title
                TxtRelatedPersonFirst_Name.Value = .First_Name
                TxtRelatedPersonMiddle_Name.Value = .Middle_Name
                TxtRelatedPersonPrefix.Value = .Prefix
                TxtRelatedPersonLast_Name.Value = .Last_Name
                dt_RelatedPersonBirth_Date.Value = IIf(.Birth_Date Is Nothing, "", .Birth_Date)
                TxtRelatedPersonBirth_Place.Value = .Birth_Place
                If refCountryBirth IsNot Nothing Then
                    dd_RelatedPersonCountry_Of_Birth.SetTextWithTextValue(refCountryBirth.Kode, refCountryBirth.Keterangan)
                End If

                TxtRelatedPersonMother_Name.Value = .Mother_Name
                TxtRelatedPersonAlias.Value = ._Alias
                'TxtRelatedPersonFull_Name_Frn.Value = .Full_Name_Frn
                TxtRelatedPersonSSN.Value = .SSN
                TxtPassport_Number.Value = .Passport_Number
                If refPassportCountry IsNot Nothing Then
                    dd_Passport_Country.SetTextWithTextValue(refPassportCountry.Kode, refPassportCountry.Keterangan)
                End If

                TxtRelatedPersonID_Number.Value = .ID_Number
                If refNationality1 IsNot Nothing Then
                    dd_RelatedPersonNationality1.SetTextWithTextValue(refNationality1.Kode, refNationality1.Keterangan)
                End If

                If refNationality2 IsNot Nothing Then
                    dd_RelatedPersonNationality2.SetTextWithTextValue(refNationality2.Kode, refNationality2.Keterangan)
                End If

                If refNationality3 IsNot Nothing Then
                    dd_RelatedPersonNationality3.SetTextWithTextValue(refNationality3.Kode, refNationality3.Keterangan)
                End If

                If refResidence IsNot Nothing Then
                    dd_RelatedPersonResidence.SetTextWithTextValue(refResidence.Kode, refResidence.Keterangan)
                End If

                'dt_RelatedPersonResidence_Since.SelectedDate = .Residence_Since
                TxtRelatedPersonOccupation.Value = .Occupation
                TxtRelatedPersonEmployerName.Value = .EMPLOYER_NAME
                chk_RelatedPersonDeceased.Checked = .Deceased
                If .Deceased Then
                    dt_TxtRelatedPersonDate_Deceased.Hidden = False
                Else
                    dt_TxtRelatedPersonDate_Deceased.Hidden = True
                End If
                dt_TxtRelatedPersonDate_Deceased.Value = IIf(.Date_Deceased Is Nothing, "", .Date_Deceased)
                TxtRelatedPersonTax_Number.Value = .Tax_Number
                chk_RelatedPersonIs_PEP.Checked = .Is_PEP
                TxtRelatedPersonSource_Of_Wealth.Value = .Source_Of_Wealth
                chk_RelatedPersonIs_Protected.Checked = .Is_Protected

                Dim pk_id = obj_RelatedPerson_Edit.PK_goAML_Ref_WIC_Related_Person_ID

                Dim address_list = If(.ObjList_GoAML_Ref_Address IsNot Nothing, .ObjList_GoAML_Ref_Address,
                    goAML_Customer_Service.GetAddressByPkFk(pk_id, 31))
                Dim address_work_list = If(.ObjList_GoAML_Ref_Address_Work IsNot Nothing, .ObjList_GoAML_Ref_Address_Work,
                    goAML_Customer_Service.GetAddressByPkFk(pk_id, 32))
                Dim phone_list = If(.ObjList_GoAML_Ref_Phone IsNot Nothing, .ObjList_GoAML_Ref_Phone,
                    goAML_Customer_Service.GetPhoneByPkFk(pk_id, 31))
                Dim phone_work_list = If(.ObjList_GoAML_Ref_Phone_Work IsNot Nothing, .ObjList_GoAML_Ref_Phone_Work,
                    goAML_Customer_Service.GetPhoneByPkFk(pk_id, 32))
                Dim identification_list = If(.ObjList_GoAML_Person_Identification IsNot Nothing, .ObjList_GoAML_Person_Identification,
                    goAML_Customer_Service.GetIdentificationPkFk(pk_id, 31))
                Dim email_list = If(.ObjList_GoAML_Ref_Customer_Email IsNot Nothing, .ObjList_GoAML_Ref_Customer_Email,
                    goAML_Customer_Service.GetEmailByPkFk(pk_id, 31))
                Dim sanction_list = If(.ObjList_GoAML_Ref_Customer_Sanction IsNot Nothing, .ObjList_GoAML_Ref_Customer_Sanction,
                    goAML_Customer_Service.GetSanctionByPkFk(pk_id, 31))
                Dim pep_list = If(.ObjList_GoAML_Ref_Customer_PEP IsNot Nothing, .ObjList_GoAML_Ref_Customer_PEP,
                    goAML_Customer_Service.GetPEPByPkFk(pk_id, 31))

                GridAddress_RelatedPerson.LoadData(address_list)
                GridAddressWork_RelatedPerson.LoadData(address_work_list)
                GridPhone_RelatedPerson.LoadData(phone_list)
                GridPhoneWork_RelatedPerson.LoadData(phone_work_list)
                GridIdentification_RelatedPerson.LoadData(identification_list)

                GridEmail_RelatedPerson.LoadData(email_list)
                GridSanction_RelatedPerson.LoadData(sanction_list)
                GridPEP_RelatedPerson.LoadData(pep_list)
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            dd_Person_Person_Relation.IsReadOnly = False
            'dt_Relation_Date_Range_Valid_From.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_From_Date.ReadOnly = False
            'dt_Relation_Date_Range_Valid_To.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_To_Date.ReadOnly = False
            TxtRelatedPersonComments.ReadOnly = False
            TxtRelatedPersonRelationComments.ReadOnly = False
            'TxtRelatedPersonGender.ReadOnly = False
            cmb_rp_gender.IsReadOnly = False
            TxtRelatedPersonTitle.ReadOnly = False
            TxtRelatedPersonFirst_Name.ReadOnly = False
            TxtRelatedPersonMiddle_Name.ReadOnly = False
            TxtRelatedPersonPrefix.ReadOnly = False
            TxtRelatedPersonLast_Name.ReadOnly = False
            dt_RelatedPersonBirth_Date.ReadOnly = False
            TxtRelatedPersonBirth_Place.ReadOnly = False
            dd_RelatedPersonCountry_Of_Birth.IsReadOnly = False
            TxtRelatedPersonMother_Name.ReadOnly = False
            TxtRelatedPersonAlias.ReadOnly = False
            'TxtRelatedPersonFull_Name_Frn.ReadOnly = False
            TxtRelatedPersonSSN.ReadOnly = False
            TxtPassport_Number.ReadOnly = False
            dd_Passport_Country.IsReadOnly = False
            TxtRelatedPersonID_Number.ReadOnly = False
            dd_RelatedPersonNationality1.IsReadOnly = False
            dd_RelatedPersonNationality2.IsReadOnly = False
            dd_RelatedPersonNationality3.IsReadOnly = False
            dd_RelatedPersonResidence.IsReadOnly = False
            'dt_RelatedPersonResidence_Since.ReadOnly = False
            TxtRelatedPersonOccupation.ReadOnly = False
            TxtRelatedPersonEmployerName.ReadOnly = False
            chk_RelatedPersonDeceased.ReadOnly = False
            dt_TxtRelatedPersonDate_Deceased.ReadOnly = False
            TxtRelatedPersonTax_Number.ReadOnly = False
            chk_RelatedPersonIs_PEP.ReadOnly = False
            TxtRelatedPersonSource_Of_Wealth.ReadOnly = False
            chk_RelatedPersonIs_Protected.ReadOnly = False
            btn_RelatedPerson_Save.Hidden = False

            GridAddressWork_RelatedPerson.IsViewMode = False
            GridAddress_RelatedPerson.IsViewMode = False
            GridPhone_RelatedPerson.IsViewMode = False
            GridPhoneWork_RelatedPerson.IsViewMode = False
            GridIdentification_RelatedPerson.IsViewMode = False
            GridEmail_RelatedPerson.IsViewMode = False
            GridSanction_RelatedPerson.IsViewMode = False
            GridPEP_RelatedPerson.IsViewMode = False
        Else
            dd_Person_Person_Relation.IsReadOnly = True
            'dt_Relation_Date_Range_Valid_From.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_From_Date.ReadOnly = False
            'dt_Relation_Date_Range_Valid_To.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_To_Date.ReadOnly = False
            TxtRelatedPersonComments.ReadOnly = True
            TxtRelatedPersonRelationComments.ReadOnly = True
            'TxtRelatedPersonGender.ReadOnly = False
            cmb_rp_gender.IsReadOnly = True
            TxtRelatedPersonTitle.ReadOnly = True
            TxtRelatedPersonFirst_Name.ReadOnly = True
            TxtRelatedPersonMiddle_Name.ReadOnly = True
            TxtRelatedPersonPrefix.ReadOnly = True
            TxtRelatedPersonLast_Name.ReadOnly = True
            dt_RelatedPersonBirth_Date.ReadOnly = True
            TxtRelatedPersonBirth_Place.ReadOnly = True
            dd_RelatedPersonCountry_Of_Birth.IsReadOnly = True
            TxtRelatedPersonMother_Name.ReadOnly = True
            TxtRelatedPersonAlias.ReadOnly = True
            'TxtRelatedPersonFull_Name_Frn.ReadOnly = False
            TxtRelatedPersonSSN.ReadOnly = True
            TxtPassport_Number.ReadOnly = True
            dd_Passport_Country.IsReadOnly = True
            TxtRelatedPersonID_Number.ReadOnly = True
            dd_RelatedPersonNationality1.IsReadOnly = True
            dd_RelatedPersonNationality2.IsReadOnly = True
            dd_RelatedPersonNationality3.IsReadOnly = True
            dd_RelatedPersonResidence.IsReadOnly = True
            'dt_RelatedPersonResidence_Since.ReadOnly = False
            TxtRelatedPersonOccupation.ReadOnly = True
            TxtRelatedPersonEmployerName.ReadOnly = True
            chk_RelatedPersonDeceased.ReadOnly = True
            dt_TxtRelatedPersonDate_Deceased.ReadOnly = True
            TxtRelatedPersonTax_Number.ReadOnly = True
            chk_RelatedPersonIs_PEP.ReadOnly = True
            TxtRelatedPersonSource_Of_Wealth.ReadOnly = True
            chk_RelatedPersonIs_Protected.ReadOnly = True
            btn_RelatedPerson_Save.Hidden = True

            GridAddressWork_RelatedPerson.IsViewMode = True
            GridAddress_RelatedPerson.IsViewMode = True
            GridPhone_RelatedPerson.IsViewMode = True
            GridPhoneWork_RelatedPerson.IsViewMode = True
            GridIdentification_RelatedPerson.IsViewMode = True
            GridEmail_RelatedPerson.IsViewMode = True
            GridSanction_RelatedPerson.IsViewMode = True
            GridPEP_RelatedPerson.IsViewMode = True
        End If

        'Bind Indikator
        IDRelatedPerson = obj_RelatedPerson_Edit.PK_goAML_Ref_WIC_Related_Person_ID

        'Show window pop up
        Window_RelatedPerson.Title = "Related Person - " & strAction
        Window_RelatedPerson.Hidden = False
    End Sub

    Protected Sub btn_Related_Person_Cancel_Click()
        Try
            'Hide window pop up
            Window_RelatedPerson.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Function IsDataCustomer_RelatedPerson_Valid() As Boolean
        If WICNo.Text.Trim = "" Then
            Throw New Exception("WIC No is required")
        End If

        If dd_Person_Person_Relation.SelectedItemText Is Nothing Then
            Throw New Exception("Person Relation is required")
        End If
        If TxtRelatedPersonLast_Name.Text.Trim = "" Then
            Throw New Exception("Full Name is required")
        End If
        If GridPhone_RelatedPerson.objListgoAML_Ref Is Nothing Then
            Throw New Exception("Phone is required")
        End If
        If GridPhone_RelatedPerson.objListgoAML_Ref.Count = 0 Then
            Throw New Exception("Phone is required")
        End If
        If GridAddress_RelatedPerson.objListgoAML_Ref Is Nothing Then
            Throw New Exception("Address is required")
        End If
        If GridAddress_RelatedPerson.objListgoAML_Ref.Count = 0 Then
            Throw New Exception("Address is required")
        End If

        Return True
    End Function
    Protected Sub btn_Related_Person_Save_Click()
        Try
            If IsDataCustomer_RelatedPerson_Valid() Then
                If obj_RelatedPerson_Edit Is Nothing Then  'Add
                    Save_RelatedPerson()
                Else    'Edit
                    Save_RelatedPerson(False)
                End If

                'Bind to GridPanel
                BindDetailRelatedPerson()

                'Hide window popup
                Window_RelatedPerson.Hidden = True
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub Save_RelatedPerson(Optional isNew As Boolean = True)
        Try
            'Action save here
            'Dim intPK As Long = -1
            If isNew Then
                Dim objAdd As New WICDataBLL.goAML_Ref_WIC_Related_Person
                Dim objRand As New Random
                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                IDRelatedPerson = intpk

                With objAdd
                    .PK_goAML_Ref_WIC_Related_Person_ID = intpk
                    .WIC_No = WICNo.Text
                    .Person_Person_Relation = dd_Person_Person_Relation.SelectedItemValue
                    '.Relation_Date_Range_Valid_From = dt_Relation_Date_Range_Valid_From.SelectedDate
                    '.Relation_Date_Range_Is_Approx_From_Date = chk_Relation_Date_Range_Is_Approx_From_Date.Checked
                    '.Relation_Date_Range_Valid_To = dt_Relation_Date_Range_Valid_To.SelectedDate
                    '.Relation_Date_Range_Is_Approx_To_Date = chk_Relation_Date_Range_Is_Approx_To_Date.Checked
                    .Comments = TxtRelatedPersonComments.Value
                    .relation_comments = TxtRelatedPersonRelationComments.Value
                    '.Gender = TxtRelatedPersonGender.Value
                    .Gender = cmb_rp_gender.SelectedItemValue
                    .Title = TxtRelatedPersonTitle.Value
                    '.First_Name = TxtRelatedPersonFirst_Name.Value
                    '.Middle_Name = TxtRelatedPersonMiddle_Name.Value
                    '.Prefix = TxtRelatedPersonPrefix.Value
                    .Last_Name = TxtRelatedPersonLast_Name.Value
                    If dt_RelatedPersonBirth_Date.RawValue IsNot Nothing Then
                        .Birth_Date = dt_RelatedPersonBirth_Date.Value
                    End If
                    .Birth_Place = TxtRelatedPersonBirth_Place.Value
                    '.Country_Of_Birth = dd_RelatedPersonCountry_Of_Birth.SelectedItemValue
                    .Mother_Name = TxtRelatedPersonMother_Name.Value
                    ._Alias = TxtRelatedPersonAlias.Value
                    '.Full_Name_Frn = TxtRelatedPersonFull_Name_Frn.Value
                    .SSN = TxtRelatedPersonSSN.Value
                    .Passport_Number = TxtPassport_Number.Value
                    .Passport_Country = dd_Passport_Country.SelectedItemValue
                    .ID_Number = TxtRelatedPersonID_Number.Value
                    .Nationality1 = dd_RelatedPersonNationality1.SelectedItemValue
                    .Nationality2 = dd_RelatedPersonNationality2.SelectedItemValue
                    .Nationality3 = dd_RelatedPersonNationality3.SelectedItemValue
                    .Residence = dd_RelatedPersonResidence.SelectedItemValue
                    '.Residence_Since = dt_RelatedPersonResidence_Since.SelectedDate
                    .Occupation = TxtRelatedPersonOccupation.Value
                    .EMPLOYER_NAME = TxtRelatedPersonEmployerName.Value
                    .Deceased = chk_RelatedPersonDeceased.Checked
                    If dt_TxtRelatedPersonDate_Deceased.RawValue IsNot Nothing Then
                        .Date_Deceased = dt_TxtRelatedPersonDate_Deceased.Value
                    End If
                    .Tax_Number = TxtRelatedPersonTax_Number.Value
                    .Is_PEP = chk_RelatedPersonIs_PEP.Checked
                    .Source_Of_Wealth = TxtRelatedPersonSource_Of_Wealth.Value
                    .Is_Protected = chk_RelatedPersonIs_Protected.Checked

                    .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridIdentification_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridSanction_RelatedPerson.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(objAdd.PK_goAML_Ref_WIC_Related_Person_ID, GridPEP_RelatedPerson.objListgoAML_Ref)
                End With
                If obj_ListRelatedPerson_Edit Is Nothing Then
                    obj_ListRelatedPerson_Edit = New List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)
                End If
                obj_ListRelatedPerson_Edit.Add(objAdd)
            Else
                Dim objEdit = obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = obj_RelatedPerson_Edit.PK_goAML_Ref_WIC_Related_Person_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDRelatedPerson = .PK_goAML_Ref_WIC_Related_Person_ID
                        .WIC_No = WICNo.Text
                        .Person_Person_Relation = dd_Person_Person_Relation.SelectedItemValue
                        '.Relation_Date_Range_Valid_From = dt_Relation_Date_Range_Valid_From.SelectedDate
                        '.Relation_Date_Range_Is_Approx_From_Date = chk_Relation_Date_Range_Is_Approx_From_Date.Checked
                        '.Relation_Date_Range_Valid_To = dt_Relation_Date_Range_Valid_To.SelectedDate
                        '.Relation_Date_Range_Is_Approx_To_Date = chk_Relation_Date_Range_Is_Approx_To_Date.Checked
                        .Comments = TxtRelatedPersonComments.Value
                        .relation_comments = TxtRelatedPersonRelationComments.Value
                        '.Gender = TxtRelatedPersonGender.Value
                        .Gender = cmb_rp_gender.SelectedItemValue
                        .Title = TxtRelatedPersonTitle.Value
                        '.First_Name = TxtRelatedPersonFirst_Name.Value
                        '.Middle_Name = TxtRelatedPersonMiddle_Name.Value
                        '.Prefix = TxtRelatedPersonPrefix.Value
                        .Last_Name = TxtRelatedPersonLast_Name.Value
                        If dt_RelatedPersonBirth_Date.RawValue IsNot Nothing Then
                            .Birth_Date = dt_RelatedPersonBirth_Date.Value
                        Else
                            .Birth_Date = Nothing
                        End If
                        .Birth_Place = TxtRelatedPersonBirth_Place.Value
                        '.Country_Of_Birth = dd_RelatedPersonCountry_Of_Birth.SelectedItemValue
                        .Mother_Name = TxtRelatedPersonMother_Name.Value
                        ._Alias = TxtRelatedPersonAlias.Value
                        '.Full_Name_Frn = TxtRelatedPersonFull_Name_Frn.Value
                        .SSN = TxtRelatedPersonSSN.Value
                        .Passport_Number = TxtPassport_Number.Value
                        .Passport_Country = dd_Passport_Country.SelectedItemValue
                        .ID_Number = TxtRelatedPersonID_Number.Value
                        .Nationality1 = dd_RelatedPersonNationality1.SelectedItemValue
                        .Nationality2 = dd_RelatedPersonNationality2.SelectedItemValue
                        .Nationality3 = dd_RelatedPersonNationality3.SelectedItemValue
                        .Residence = dd_RelatedPersonResidence.SelectedItemValue
                        '.Residence_Since = dt_RelatedPersonResidence_Since.SelectedDate
                        .Occupation = TxtRelatedPersonOccupation.Value
                        .EMPLOYER_NAME = TxtRelatedPersonEmployerName.Value
                        .Deceased = chk_RelatedPersonDeceased.Checked
                        If dt_TxtRelatedPersonDate_Deceased.RawValue IsNot Nothing Then
                            .Date_Deceased = dt_TxtRelatedPersonDate_Deceased.Value
                        Else
                            .Date_Deceased = Nothing
                        End If
                        .Tax_Number = TxtRelatedPersonTax_Number.Value
                        .Is_PEP = chk_RelatedPersonIs_PEP.Checked
                        .Source_Of_Wealth = TxtRelatedPersonSource_Of_Wealth.Value
                        .Is_Protected = chk_RelatedPersonIs_Protected.Checked

                        .ObjList_GoAML_Person_Identification = goAML_Person_Identification_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridIdentification_RelatedPerson.objListgoAML_Ref)
                        .ObjList_GoAML_Ref_Address = goAML_Ref_Address_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridAddress_RelatedPerson.objListgoAML_Ref)
                        .ObjList_GoAML_Ref_Address_Work = goAML_Ref_Address_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridAddressWork_RelatedPerson.objListgoAML_Ref)
                        .ObjList_GoAML_Ref_Phone = goAML_Ref_Phone_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridPhone_RelatedPerson.objListgoAML_Ref)
                        .ObjList_GoAML_Ref_Phone_Work = goAML_Ref_Phone_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridPhoneWork_RelatedPerson.objListgoAML_Ref)
                        .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridEmail_RelatedPerson.objListgoAML_Ref)
                        .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridSanction_RelatedPerson.objListgoAML_Ref)
                        .ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(objEdit.PK_goAML_Ref_WIC_Related_Person_ID, GridPEP_RelatedPerson.objListgoAML_Ref)
                    End With
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub BindDetailRelatedPerson()
        Try
            Dim listRelatedPerson As New DataTable
            'listRelatedPerson = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM [dbo].[goAML_Ref_WIC_Social_Media] where WIC_No = '13123123'")
            listRelatedPerson = NawaBLL.Common.CopyGenericToDataTable(obj_ListRelatedPerson_Edit)

            GridPanelRelatedPerson.GetStore.DataSource = listRelatedPerson
            GridPanelRelatedPerson.GetStore.DataBind()

            GPWIC_CORP_RelatedPerson.GetStore.DataSource = listRelatedPerson
            GPWIC_CORP_RelatedPerson.GetStore.DataBind()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Protected Sub Clean_Window_RelatedPerson()
        'Clean fields
        dd_Person_Person_Relation.IsReadOnly = Nothing
        'dt_Relation_Date_Range_Valid_From.ReadOnly = Nothing
        'chk_Relation_Date_Range_Is_Approx_From_Date.ReadOnly = Nothing
        'dt_Relation_Date_Range_Valid_To.ReadOnly = Nothing
        'chk_Relation_Date_Range_Is_Approx_To_Date.ReadOnly = Nothing
        TxtRelatedPersonComments.ReadOnly = Nothing
        TxtRelatedPersonRelationComments.ReadOnly = Nothing
        'TxtRelatedPersonGender.ReadOnly = Nothing
        cmb_rp_gender.IsReadOnly = Nothing
        TxtRelatedPersonTitle.ReadOnly = Nothing
        TxtRelatedPersonFirst_Name.ReadOnly = Nothing
        TxtRelatedPersonMiddle_Name.ReadOnly = Nothing
        TxtRelatedPersonPrefix.ReadOnly = Nothing
        TxtRelatedPersonLast_Name.ReadOnly = Nothing
        dt_RelatedPersonBirth_Date.ReadOnly = Nothing
        TxtRelatedPersonBirth_Place.ReadOnly = Nothing
        dd_RelatedPersonCountry_Of_Birth.IsReadOnly = Nothing
        TxtRelatedPersonMother_Name.ReadOnly = Nothing
        TxtRelatedPersonAlias.ReadOnly = Nothing
        'TxtRelatedPersonFull_Name_Frn.ReadOnly = Nothing
        TxtRelatedPersonSSN.ReadOnly = Nothing
        TxtPassport_Number.ReadOnly = Nothing
        dd_Passport_Country.IsReadOnly = Nothing
        TxtRelatedPersonID_Number.ReadOnly = Nothing
        dd_RelatedPersonNationality1.IsReadOnly = Nothing
        dd_RelatedPersonNationality2.IsReadOnly = Nothing
        dd_RelatedPersonNationality3.IsReadOnly = Nothing
        dd_RelatedPersonResidence.IsReadOnly = Nothing
        'dt_RelatedPersonResidence_Since.ReadOnly = Nothing
        TxtRelatedPersonOccupation.ReadOnly = Nothing
        TxtRelatedPersonEmployerName.ReadOnly = Nothing
        chk_RelatedPersonDeceased.ReadOnly = Nothing
        dt_TxtRelatedPersonDate_Deceased.ReadOnly = Nothing
        TxtRelatedPersonTax_Number.ReadOnly = Nothing
        chk_RelatedPersonIs_PEP.ReadOnly = Nothing
        TxtRelatedPersonSource_Of_Wealth.ReadOnly = Nothing
        chk_RelatedPersonIs_Protected.ReadOnly = Nothing
        'Show Buttons
        btn_RelatedPerson_Save.Hidden = False
    End Sub
#End Region

#Region "goAML 5.0.1, add by Septian, 2023-02-23"
    Public Property obj_ListSocialMedia_Edit() As List(Of WICDataBLL.goAML_Ref_WIC_Social_Media)
        Get
            Return Session("GoAMLWICAdd.obj_ListSocialMedia_Edit")
        End Get
        Set(ByVal value As List(Of WICDataBLL.goAML_Ref_WIC_Social_Media))
            Session("GoAMLWICAdd.obj_ListSocialMedia_Edit") = value
        End Set
    End Property

    Public Property obj_SocialMedia_Edit() As WICDataBLL.goAML_Ref_WIC_Social_Media
        Get
            Return Session("GoAMLWICAdd.obj_Reporting_SocialMedia_Edit")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Social_Media)
            Session("GoAMLWICAdd.obj_Reporting_SocialMedia_Edit") = value
        End Set
    End Property

    Public Property objTempWIC_EntityUrl_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL
        Get
            Return Session("WIC_EDIT_v501.objTempWIC_EntityUrl_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
            Session("WIC_EDIT_v501.objTempWIC_EntityUrl_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityUrl() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
        Get
            Return Session("WIC_EDIT_v501.objListgoAML_Ref_EntityUrl")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL))
            Session("WIC_EDIT_v501.objListgoAML_Ref_EntityUrl") = value
        End Set
    End Property
    Public Property objListgoAML_vw_EntityUrl() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
        Get
            Return Session("WIC_EDIT_v501.objListgoAML_vw_EntityUrl")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL))
            Session("WIC_EDIT_v501.objListgoAML_vw_EntityUrl") = value
        End Set
    End Property

    Public Property objTempWIC_EntityIdentification_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification
        Get
            Return Session("WIC_EDIT_v501.objTempWIC_EntityIdentification_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
            Session("WIC_EDIT_v501.objTempWIC_EntityIdentification_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityIdentification() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        Get
            Return Session("WIC_EDIT_v501.objListgoAML_Ref_EntityIdentification")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification))
            Session("WIC_EDIT_v501.objListgoAML_Ref_EntityIdentification") = value
        End Set
    End Property
    Public Property objListgoAML_vw_EntityIdentification() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        Get
            Return Session("WIC_EDIT_v501.objListgoAML_vw_EntityIdentification")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification))
            Session("WIC_EDIT_v501.objListgoAML_vw_EntityIdentification") = value
        End Set
    End Property

    Public Property objTempWIC_RelatedEntities_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity
        Get
            Return Session("WIC_EDIT_v501.objTempWIC_RelatedEntity_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
            Session("WIC_EDIT_v501.objTempWIC_RelatedEntity_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_RelatedEntity() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        Get
            Return Session("WIC_EDIT_v501.objListgoAML_Ref_RelatedEntity")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity))
            Session("WIC_EDIT_v501.objListgoAML_Ref_RelatedEntity") = value
        End Set
    End Property
    Public Property objListgoAML_vw_RelatedEntity() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        Get
            Return Session("WIC_EDIT_v501.objListgoAML_vw_RelatedEntity")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity))
            Session("WIC_EDIT_v501.objListgoAML_vw_RelatedEntity") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_WIC_URL() As WICDataBLL.goAML_Ref_WIC_URL
        Get
            Return Session("WIC_EDIT_v501.objTemp_goAML_Ref_WIC_URL")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_URL)
            Session("WIC_EDIT_v501.objTemp_goAML_Ref_WIC_URL") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_WIC_Entity_Identification() As WICDataBLL.goAML_Ref_WIC_Entity_Identification
        Get
            Return Session("WIC_EDIT_v501.objTemp_goAML_Ref_WIC_Entity_Identification")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Entity_Identification)
            Session("WIC_EDIT_v501.objTemp_goAML_Ref_WIC_Entity_Identification") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_WIC_Related_Entity() As WICDataBLL.goAML_Ref_WIC_Related_Entity
        Get
            Return Session("WIC_EDIT_v501.objTemp_goAML_Ref_WIC_Related_Entity")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Related_Entity)
            Session("WIC_EDIT_v501.objTemp_goAML_Ref_WIC_Related_Entity") = value
        End Set
    End Property

    Protected Sub btnAdd_WIC_Email_DirectClick()
        Try
            DisplayWIC_Email.Text = WICNo.Text
            TxtEmail.ReadOnly = False
            btn_Email_Save.Hidden = False
            obj_Email_Edit = Nothing
            Clean_Window_Email()
            Window_Email.Title = "Email - Add"
            Window_Email.Hidden = False

            'Dim myFormPanel = CType(FindControl("FormPanelEmail"), FormPanel)

            'For Each item As Ext.Net.Component In FormPanelEmail.Items
            '    Dim tipe = item.GetType()
            '    'If TypeOf control Is TextField Then
            '    '    CType(control, TextField).Text = ""
            '    'End If

            '    'If TypeOf control Is TextBox Then
            '    '    Dim value As String = CType(control, TextBox).Text
            '    '    ' Do something with the value
            '    'ElseIf TypeOf control Is DropDownList Then
            '    '    Dim value As String = CType(control, DropDownList).SelectedValue
            '    '    ' Do something with the value
            '    'End If
            '    ' Add other control types as needed
            'Next

            'Set IDPayment to 0
            IDEmail = 0
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_WIC_Sanction_DirectClick()
        Try
            TxtProvider.ReadOnly = False
            TxtSanctionListName.ReadOnly = False
            TxtMatchCriteria.ReadOnly = False
            TxtLinkToSource.ReadOnly = False
            TxtSanctionListAttributes.ReadOnly = False
            dt_SanctionValidFrom.ReadOnly = False
            chk_SanctionIsApproxFromDate.ReadOnly = False
            dt_SanctionValidTo.ReadOnly = False
            chk_SanctionIsApproxToDate.ReadOnly = False
            TxtSanctionComments.ReadOnly = False
            btn_Sanction_Save.Hidden = False
            obj_SocialMedia_Edit = Nothing
            Clean_Window_Sanction()
            Window_Sanction.Title = "Sanction - Add"
            Window_Sanction.Hidden = False

            'Set IDPayment to 0
            IDSanction = 0
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveWIC_Url_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataWIC_URL_Valid() Then
                If objTempWIC_EntityUrl_Edit Is Nothing Then
                    Save_Url()
                Else
                    Save_Url(False)
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveWIC_EntityIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataWIC_EntityIdentification_Valid() Then
                If objTempWIC_EntityIdentification_Edit Is Nothing Then
                    Save_EntityIdentification()
                Else
                    Save_EntityIdentification(False)
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveWIC_RelatedEntities_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataWIC_RelatedEntity_Valid() Then
                If objTempWIC_RelatedEntities_Edit Is Nothing Then
                    Save_RelatedEntities()
                Else
                    Save_RelatedEntities(False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_WIC_Url_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_URL.Hidden = False
            FormPanel_URL.Hidden = False
            btnSaveWIC_URL.Hidden = False ' 2023-10-18, Nael: Uncomment

            txt_url.ReadOnly = False

            WindowDetail_URL.Title = "WIC Url Add"

            objTempWIC_EntityUrl_Edit = Nothing
            ClearinputWIC_Url()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_WIC_EntityIdentification_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            WindowDetail_EntityIdentification.Hidden = False
            FormPanel_EntityIdentification.Hidden = False
            btnSaveWIC_EntityIdentification.Hidden = False ' 2023-10-18, Nael: Uncomment

            cmb_ef_type.IsReadOnly = False
            txt_ef_number.ReadOnly = False
            df_issue_date.ReadOnly = False
            df_expiry_date.ReadOnly = False
            txt_ef_issued_by.ReadOnly = False
            cmb_ef_issue_country.IsReadOnly = False
            txt_ef_comments.ReadOnly = False

            WindowDetail_EntityIdentification.Title = "Entity Identification Add"

            objTempWIC_EntityIdentification_Edit = Nothing
            'FormPanel_EntityIdentification.Reset()
            ClearinputWIC_EntityIdentification()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_WIC_RelatedEntities_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            GridAddress_RelatedEntity.IsViewMode = False
            GridPhone_RelatedEntity.IsViewMode = False
            GridEntityIdentification_RelatedEntity.IsViewMode = False
            GridEmail_RelatedEntity.IsViewMode = False
            GridURL_RelatedEntity.IsViewMode = False
            GridSanction_RelatedEntity.IsViewMode = False

            GridAddress_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Address))
            GridPhone_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Phone))
            GridEntityIdentification_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_Entity_Identification))
            GridEmail_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_Email))
            GridSanction_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_Sanction))
            GridURL_RelatedEntity.LoadData(New List(Of DataModel.goAML_Ref_Customer_URL))

            WindowDetail_RelatedEntity.Hidden = False
            FormPanel_RelatedEntities.Hidden = False
            btnSaveWIC_RelatedEntity.Hidden = False ' 2023-10-18, Nael

            cmb_re_entity_relation.IsReadOnly = False
            'df_re_relation_date_range_valid_from.ReadOnly = False
            'cbx_re_relation_date_range_is_approx_from_date.ReadOnly = False
            'df_re_relation_date_range_valid_to.ReadOnly = False
            'cbx_re_relation_date_range_is_approx_to_date.ReadOnly = False
            nfd_re_share_percentage.ReadOnly = False
            txt_re_comments.ReadOnly = False
            txt_re_relation_comments.ReadOnly = False
            txt_re_name.ReadOnly = False
            txt_re_commercial_name.ReadOnly = False
            cmb_re_incorporation_legal_form.IsReadOnly = False
            txt_re_incorporation_number.ReadOnly = False
            txt_re_business.ReadOnly = False
            'cmb_re_entity_status.IsReadOnly = False
            'df_re_entity_status_date.ReadOnly = False
            txt_re_incorporation_state.ReadOnly = False
            cmb_re_incorporation_country_code.IsReadOnly = False
            df_re_incorporation_date.ReadOnly = False
            cbx_re_business_closed.ReadOnly = False
            df_re_date_business_closed.ReadOnly = False
            txt_re_tax_number.ReadOnly = False
            txt_re_tax_reg_number.ReadOnly = False

            WindowDetail_RelatedEntity.Title = "Related Entities Add"
            objTempWIC_RelatedEntities_Edit = Nothing
            'FormPanel_RelatedEntities.Reset(True)
            ClearinputWIC_RelatedEntities()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAdd_WIC_RelatedPerson_DirectClick()
        Try
            dd_Person_Person_Relation.IsReadOnly = False
            'dt_Relation_Date_Range_Valid_From.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_From_Date.ReadOnly = False
            'dt_Relation_Date_Range_Valid_To.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_To_Date.ReadOnly = False
            TxtRelatedPersonComments.ReadOnly = False
            TxtRelatedPersonRelationComments.ReadOnly = False
            'TxtRelatedPersonGender.ReadOnly = False
            cmb_rp_gender.IsReadOnly = False
            TxtRelatedPersonTitle.ReadOnly = False
            TxtRelatedPersonFirst_Name.ReadOnly = False
            TxtRelatedPersonMiddle_Name.ReadOnly = False
            TxtRelatedPersonPrefix.ReadOnly = False
            TxtRelatedPersonLast_Name.ReadOnly = False
            dt_RelatedPersonBirth_Date.ReadOnly = False
            TxtRelatedPersonBirth_Place.ReadOnly = False
            dd_RelatedPersonCountry_Of_Birth.IsReadOnly = False
            TxtRelatedPersonMother_Name.ReadOnly = False
            TxtRelatedPersonAlias.ReadOnly = False
            'TxtRelatedPersonFull_Name_Frn.ReadOnly = False
            TxtRelatedPersonSSN.ReadOnly = False
            TxtPassport_Number.ReadOnly = False
            dd_Passport_Country.IsReadOnly = False
            TxtRelatedPersonID_Number.ReadOnly = False
            dd_RelatedPersonNationality1.IsReadOnly = False
            dd_RelatedPersonNationality2.IsReadOnly = False
            dd_RelatedPersonNationality3.IsReadOnly = False
            dd_RelatedPersonResidence.IsReadOnly = False
            'dt_RelatedPersonResidence_Since.ReadOnly = False
            TxtRelatedPersonOccupation.ReadOnly = False
            TxtRelatedPersonEmployerName.ReadOnly = False
            chk_RelatedPersonDeceased.ReadOnly = False
            dt_TxtRelatedPersonDate_Deceased.ReadOnly = False
            TxtRelatedPersonTax_Number.ReadOnly = False
            chk_RelatedPersonIs_PEP.ReadOnly = False
            TxtRelatedPersonSource_Of_Wealth.ReadOnly = False
            chk_RelatedPersonIs_Protected.ReadOnly = False
            btn_RelatedPerson_Save.Hidden = False
            obj_SocialMedia_Edit = Nothing
            Clean_Window_RelatedPerson()
            Window_RelatedPerson.Title = "Related Person - Add"
            Window_RelatedPerson.Hidden = False

            'Set IDPayment to 0
            IDRelatedPerson = 0
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub Save_Url(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempWIC_EntityUrl_Edit
                    .WIC_NO = ObjWIC.WIC_No
                    .URL = txt_url.Value
                End With

                With objTemp_goAML_Ref_WIC_URL
                    .WIC_NO = ObjWIC.WIC_No
                    .URL = txt_url.Value
                End With

                objTempWIC_EntityUrl_Edit = Nothing
                objTemp_goAML_Ref_WIC_URL = Nothing
            Else
                Dim objNewVWgoAML_Url As New GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL
                Dim objNewgoAML_Url As New GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_EntityUrl.Find(Function(x) x.PK_goAML_Ref_WIC_URL_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_Url
                    .PK_goAML_Ref_WIC_URL_ID = intpk
                    .WIC_NO = ObjWIC.WIC_No
                    .URL = txt_url.Value
                End With

                With objNewgoAML_Url
                    .PK_goAML_Ref_WIC_URL_ID = intpk
                    .WIC_NO = ObjWIC.WIC_No
                    .URL = txt_url.Value
                End With

                'objListgoAML_vw_EntityUrl.Add(objNewVWgoAML_Url)
                objListgoAML_Ref_EntityUrl.Add(objNewgoAML_Url)
                objListgoAML_vw_EntityUrl = objListgoAML_Ref_EntityUrl
            End If

            GPWIC_CORP_URL.GetStore.DataSource = objListgoAML_vw_EntityUrl.ToList()
            GPWIC_CORP_URL.GetStore.DataBind()

            WindowDetail_URL.Hidden = True
            'ClearinputWICPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
    Sub Save_EntityIdentification(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempWIC_EntityIdentification_Edit
                    .WIC_NO = ObjWIC.WIC_No
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If df_issue_date.RawValue IsNot Nothing Then
                        .ISSUE_DATE = df_issue_date.Value
                    Else
                        .ISSUE_DATE = Nothing
                    End If
                    If df_expiry_date.RawValue IsNot Nothing Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    Else
                        .EXPIRY_DATE = Nothing
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                With objTemp_goAML_Ref_WIC_Entity_Identification
                    .WIC_NO = ObjWIC.WIC_No
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If df_issue_date.RawValue IsNot Nothing Then
                        .ISSUE_DATE = df_issue_date.Value
                    Else
                        .ISSUE_DATE = Nothing
                    End If
                    If df_expiry_date.RawValue IsNot Nothing Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    Else
                        .EXPIRY_DATE = Nothing
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                objTempWIC_EntityIdentification_Edit = Nothing
                objTemp_goAML_Ref_WIC_Entity_Identification = Nothing
            Else
                Dim objNewVWgoAML_EntityIdentification As New GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification
                Dim objNewgoAML_EntityIdentification As New GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_EntityIdentification
                    .PK_goAML_Ref_WIC_Entity_Identifications_ID = intpk
                    .WIC_NO = ObjWIC.WIC_No
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If df_issue_date.RawValue IsNot Nothing Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If df_expiry_date.RawValue IsNot Nothing Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                With objNewgoAML_EntityIdentification
                    .PK_goAML_Ref_WIC_Entity_Identifications_ID = intpk
                    .WIC_NO = ObjWIC.WIC_No
                    .TYPE = cmb_ef_type.SelectedItemValue
                    .NUMBER = txt_ef_number.Value
                    If df_issue_date.RawValue IsNot Nothing Then
                        .ISSUE_DATE = df_issue_date.Value
                    End If
                    If df_expiry_date.RawValue IsNot Nothing Then
                        .EXPIRY_DATE = df_expiry_date.Value
                    End If
                    .ISSUED_BY = txt_ef_issued_by.Value
                    .ISSUE_COUNTRY = cmb_ef_issue_country.SelectedItemValue
                    .COMMENTS = txt_ef_comments.Value
                End With

                'objListgoAML_vw_EntityIdentification.Add(objNewVWgoAML_EntityIdentification)
                objListgoAML_Ref_EntityIdentification.Add(objNewgoAML_EntityIdentification)
                objListgoAML_vw_EntityIdentification = objListgoAML_Ref_EntityIdentification
            End If

            GPWIC_CORP_EntityIdentification.GetStore.DataSource = objListgoAML_vw_EntityIdentification.ToList()
            GPWIC_CORP_EntityIdentification.GetStore.DataBind()

            WindowDetail_EntityIdentification.Hidden = True
            'ClearinputWICPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub
    Sub Save_RelatedEntities(Optional isNew As Boolean = True)
        Try
            If Not isNew Then
                With objTempWIC_RelatedEntities_Edit
                    .WIC_NO = ObjWIC.WIC_No
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    .SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    '.ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    '.ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    Else
                        .INCORPORATION_DATE = Nothing
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    Else
                        .DATE_BUSINESS_CLOSED = Nothing
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objTempWIC_RelatedEntities_Edit.PK_goAML_Ref_WIC_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objTempWIC_RelatedEntities_Edit.PK_goAML_Ref_WIC_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTempWIC_RelatedEntities_Edit.PK_goAML_Ref_WIC_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objTempWIC_RelatedEntities_Edit.PK_goAML_Ref_WIC_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objTempWIC_RelatedEntities_Edit.PK_goAML_Ref_WIC_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objTempWIC_RelatedEntities_Edit.PK_goAML_Ref_WIC_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                With objTemp_goAML_Ref_WIC_Related_Entity
                    .WIC_NO = ObjWIC.WIC_No
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    .SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    '.ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    '.ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    Else
                        .INCORPORATION_DATE = Nothing
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    Else
                        .DATE_BUSINESS_CLOSED = Nothing
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objTemp_goAML_Ref_WIC_Related_Entity.PK_goAML_Ref_WIC_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objTemp_goAML_Ref_WIC_Related_Entity.PK_goAML_Ref_WIC_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objTemp_goAML_Ref_WIC_Related_Entity.PK_goAML_Ref_WIC_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objTemp_goAML_Ref_WIC_Related_Entity.PK_goAML_Ref_WIC_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objTemp_goAML_Ref_WIC_Related_Entity.PK_goAML_Ref_WIC_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objTemp_goAML_Ref_WIC_Related_Entity.PK_goAML_Ref_WIC_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                objTempWIC_RelatedEntities_Edit = Nothing
                objTemp_goAML_Ref_WIC_Related_Entity = Nothing
            Else
                Dim objNewVWgoAML_RelatedEntities As New GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity
                Dim objNewgoAML_RelatedEntities As New GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity
                Dim objRand As New Random

                Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
                While Not objListgoAML_Ref_RelatedEntity.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = intpk) Is Nothing
                    intpk = objRand.Next
                End While

                With objNewVWgoAML_RelatedEntities
                    .PK_goAML_Ref_WIC_Related_Entities_ID = intpk
                    .WIC_NO = ObjWIC.WIC_No
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    .SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    '.ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    '.ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    Else
                        .INCORPORATION_DATE = Nothing
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    Else
                        .DATE_BUSINESS_CLOSED = Nothing
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objNewVWgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                With objNewgoAML_RelatedEntities
                    .PK_goAML_Ref_WIC_Related_Entities_ID = intpk
                    .WIC_NO = ObjWIC.WIC_No
                    .ENTITY_ENTITY_RELATION = cmb_re_entity_relation.SelectedItemValue
                    '.RELATION_DATE_RANGE_VALID_FROM = df_re_relation_date_range_valid_from.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = cbx_re_relation_date_range_is_approx_from_date.Value
                    '.RELATION_DATE_RANGE_VALID_TO = df_re_relation_date_range_valid_to.Value
                    '.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = cbx_re_relation_date_range_is_approx_to_date.Value
                    .SHARE_PERCENTAGE = nfd_re_share_percentage.Value
                    .COMMENTS = txt_re_comments.Value
                    .RELATION_COMMENTS = txt_re_relation_comments.Value
                    .NAME = txt_re_name.Value
                    .COMMERCIAL_NAME = txt_re_commercial_name.Value
                    .INCORPORATION_LEGAL_FORM = cmb_re_incorporation_legal_form.SelectedItemValue
                    .INCORPORATION_NUMBER = txt_re_incorporation_number.Value
                    .BUSINESS = txt_re_business.Value
                    '.ENTITY_STATUS = cmb_re_entity_status.SelectedItemValue
                    '.ENTITY_STATUS_DATE = df_re_entity_status_date.Value
                    .INCORPORATION_STATE = txt_re_incorporation_state.Value
                    .INCORPORATION_COUNTRY_CODE = cmb_re_incorporation_country_code.SelectedItemValue
                    If df_re_incorporation_date.RawValue IsNot Nothing Then
                        .INCORPORATION_DATE = df_re_incorporation_date.Value
                    Else
                        .INCORPORATION_DATE = Nothing
                    End If
                    .BUSINESS_CLOSED = cbx_re_business_closed.Value
                    If df_re_date_business_closed.RawValue IsNot Nothing Then
                        .DATE_BUSINESS_CLOSED = df_re_date_business_closed.Value
                    Else
                        .DATE_BUSINESS_CLOSED = Nothing
                    End If
                    .TAX_NUMBER = txt_re_tax_number.Value
                    .TAX_REG_NUMBER = txt_re_tax_reg_number.Value

                    .ObjList_GoAML_Ref_Customer_Phone = goAML_Ref_Phone_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridPhone_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Address = goAML_Ref_Address_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridAddress_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridEmail_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Ref_Customer_Entity_Identification_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridEntityIdentification_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_URL = goAML_Ref_Customer_URL_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridURL_RelatedEntity.objListgoAML_Ref)
                    .ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(objNewgoAML_RelatedEntities.PK_goAML_Ref_WIC_Related_Entities_ID, GridSanction_RelatedEntity.objListgoAML_Ref)
                End With

                'objListgoAML_vw_RelatedEntity.Add(objNewVWgoAML_RelatedEntities)
                objListgoAML_Ref_RelatedEntity.Add(objNewgoAML_RelatedEntities)
                objListgoAML_vw_RelatedEntity = objListgoAML_Ref_RelatedEntity
            End If

            GPWIC_CORP_RelatedEntities.GetStore.DataSource = objListgoAML_vw_RelatedEntity.ToList()
            GPWIC_CORP_RelatedEntities.GetStore.DataBind()

            WindowDetail_RelatedEntity.Hidden = True
            'ClearinputWICPreviousName()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub ClearinputWIC_Url()
        txt_url.Text = ""


    End Sub

    Sub ClearinputWIC_RelatedEntities()
        cmb_re_entity_relation.SetTextValue("")
        'df_re_relation_date_range_valid_from.Text = ""
        'cbx_re_relation_date_range_is_approx_from_date.Checked = False
        'df_re_relation_date_range_valid_to.Text = ""
        'cbx_re_relation_date_range_is_approx_to_date.Checked = False
        nfd_re_share_percentage.Text = ""
        txt_re_comments.Text = ""
        txt_re_relation_comments.Text = ""
        txt_re_name.Text = ""
        txt_re_commercial_name.Text = ""
        cmb_re_incorporation_legal_form.SetTextValue("")
        txt_re_incorporation_number.Text = ""
        txt_re_business.Text = ""
        'cmb_re_entity_status.SetTextValue("")
        'df_re_entity_status_date.Text = ""
        txt_re_incorporation_state.Text = ""
        cmb_re_incorporation_country_code.SetTextValue("")
        df_re_incorporation_date.Text = ""
        cbx_re_business_closed.Checked = False
        df_re_date_business_closed.Text = ""
        txt_re_tax_number.Text = ""
        txt_re_tax_reg_number.Text = ""

    End Sub

    Sub ClearinputWIC_EntityIdentification()
        cmb_ef_type.SetTextValue("")
        txt_ef_number.Text = ""
        df_issue_date.Text = ""
        df_expiry_date.Text = ""
        txt_ef_issued_by.Text = ""
        cmb_ef_issue_country.SetTextValue("")
        txt_ef_comments.Text = ""

    End Sub

    Protected Sub btnBackWIC_Url_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_URL.Hidden = True
            FormPanel_URL.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackWIC_EntityIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_EntityIdentification.Hidden = True
            FormPanel_EntityIdentification.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackWIC_RelatedEntities_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_RelatedEntity.Hidden = True
            FormPanel_RelatedEntities.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_Email(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListEmail_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListEmail_Edit.Remove(objToDelete)
                    End If
                    BindDetailEmail()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Email_Edit = obj_ListEmail_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = strID)
                    Load_Window_Email(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_EntityIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_EntityIdentification(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_EntityIdentification(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_EntityIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_URL(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_WICURL(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_WICURL(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_URL(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_RelatedEntities(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_RelatedEntities(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_RelatedEntities(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecord_RelatedEntities(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_Sanction(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListSanction_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListSanction_Edit.Remove(objToDelete)
                    End If
                    BindDetailSanction()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Sanction_Edit = obj_ListSanction_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = strID)
                    Load_Window_Sanction(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_RelatedPerson(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = strID)
                    If objToDelete IsNot Nothing Then
                        obj_ListRelatedPerson_Edit.Remove(objToDelete)
                    End If
                    BindDetailRelatedPerson()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_RelatedPerson_Edit = obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = strID)
                    Load_Window_RelatedPerson(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadData_EntityIdentification(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_WIC_Entity_Identification = objListgoAML_Ref_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id)
            objTempWIC_EntityIdentification_Edit = objListgoAML_vw_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id)

            If Not objTempWIC_EntityIdentification_Edit Is Nothing Then
                FormPanel_EntityIdentification.Hidden = False
                WindowDetail_EntityIdentification.Hidden = False
                btnSaveWIC_EntityIdentification.Hidden = Not isEdit

                cmb_ef_type.IsReadOnly = Not isEdit
                txt_ef_number.ReadOnly = Not isEdit
                df_issue_date.ReadOnly = Not isEdit
                df_expiry_date.ReadOnly = Not isEdit
                txt_ef_issued_by.ReadOnly = Not isEdit
                cmb_ef_issue_country.IsReadOnly = Not isEdit
                txt_ef_comments.ReadOnly = Not isEdit


                With objTempWIC_EntityIdentification_Edit
                    Dim refType = objgoAML_WIC.GetReferenceByCode("goAML_Ref_Entity_Identifier_Type", .TYPE)
                    Dim refIssue = objgoAML_WIC.GetReferenceByCode("goAML_Ref_Nama_Negara", .ISSUE_COUNTRY)

                    cmb_ef_type.SetTextWithTextValue(refType.Kode, refType.Keterangan)
                    txt_ef_number.Value = .NUMBER
                    df_issue_date.Value = IIf(.ISSUE_DATE Is Nothing, "", .ISSUE_DATE)
                    df_expiry_date.Value = IIf(.EXPIRY_DATE Is Nothing, "", .EXPIRY_DATE)
                    txt_ef_issued_by.Value = .ISSUED_BY
                    cmb_ef_issue_country.SetTextWithTextValue(refIssue.Kode, refIssue.Keterangan)
                    txt_ef_comments.Value = .COMMENTS

                End With

                WindowDetail_EntityIdentification.Title = "Entity Identification " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_WICURL(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_WIC_URL = objListgoAML_Ref_EntityUrl.Find(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id)
            objTempWIC_EntityUrl_Edit = objListgoAML_vw_EntityUrl.Find(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id)

            If Not objTempWIC_EntityUrl_Edit Is Nothing Then
                FormPanel_URL.Hidden = False
                WindowDetail_URL.Hidden = False
                btnSaveWIC_URL.Hidden = Not isEdit

                txt_url.ReadOnly = Not isEdit

                With objTempWIC_EntityUrl_Edit
                    txt_url.Value = .URL
                End With

                WindowDetail_URL.Title = "Entity URL " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_RelatedEntities(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_WIC_Related_Entity = objListgoAML_Ref_RelatedEntity.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id)
            objTempWIC_RelatedEntities_Edit = objListgoAML_vw_RelatedEntity.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id)

            If Not objTempWIC_RelatedEntities_Edit Is Nothing Then
                GridPhone_RelatedEntity.IsViewMode = Not isEdit
                GridAddress_RelatedEntity.IsViewMode = Not isEdit
                GridEmail_RelatedEntity.IsViewMode = Not isEdit
                GridEntityIdentification_RelatedEntity.IsViewMode = Not isEdit
                GridURL_RelatedEntity.IsViewMode = Not isEdit
                GridSanction_RelatedEntity.IsViewMode = Not isEdit

                FormPanel_RelatedEntities.Hidden = False
                WindowDetail_RelatedEntity.Hidden = False
                btnSaveWIC_RelatedEntity.Hidden = Not isEdit

                cmb_re_entity_relation.IsReadOnly = Not isEdit
                'df_re_relation_date_range_valid_from.ReadOnly = Not isEdit
                'cbx_re_relation_date_range_is_approx_from_date.ReadOnly = Not isEdit
                'df_re_relation_date_range_valid_to.ReadOnly = Not isEdit
                'cbx_re_relation_date_range_is_approx_to_date.ReadOnly = Not isEdit
                nfd_re_share_percentage.ReadOnly = Not isEdit
                txt_re_comments.ReadOnly = Not isEdit
                txt_re_relation_comments.ReadOnly = Not isEdit
                txt_re_name.ReadOnly = Not isEdit
                txt_re_commercial_name.ReadOnly = Not isEdit
                cmb_re_incorporation_legal_form.IsReadOnly = Not isEdit
                txt_re_incorporation_number.ReadOnly = Not isEdit
                txt_re_business.ReadOnly = Not isEdit
                'cmb_re_entity_status.IsReadOnly = Not isEdit
                'df_re_entity_status_date.ReadOnly = Not isEdit
                txt_re_incorporation_state.ReadOnly = Not isEdit
                cmb_re_incorporation_country_code.IsReadOnly = Not isEdit
                df_re_incorporation_date.ReadOnly = Not isEdit
                cbx_re_business_closed.ReadOnly = Not isEdit
                df_re_date_business_closed.ReadOnly = Not isEdit
                txt_re_tax_number.ReadOnly = Not isEdit
                txt_re_tax_reg_number.ReadOnly = Not isEdit


                With objTempWIC_RelatedEntities_Edit
                    Dim refRelation = objgoAML_WIC.GetReferenceByCode("goAML_Ref_Entity_Entity_Relationship", .ENTITY_ENTITY_RELATION)
                    Dim refLegalForm = objgoAML_WIC.GetReferenceByCode("goAML_Ref_Bentuk_Badan_Usaha", .INCORPORATION_LEGAL_FORM)
                    Dim refEntityStatus = objgoAML_WIC.GetReferenceByCode("goAML_Ref_Entity_Status", .ENTITY_STATUS)
                    Dim refCountry = objgoAML_WIC.GetReferenceByCode("goAML_Ref_Nama_Negara", .INCORPORATION_COUNTRY_CODE)

                    cmb_re_entity_relation.SetTextWithTextValue(refRelation.Kode, refRelation.Keterangan)
                    'If .RELATION_DATE_RANGE_VALID_FROM IsNot Nothing Then
                    '    df_re_relation_date_range_valid_from.Text = .RELATION_DATE_RANGE_VALID_FROM
                    'End If
                    'cbx_re_relation_date_range_is_approx_from_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'If .RELATION_DATE_RANGE_VALID_TO IsNot Nothing Then
                    '    df_re_relation_date_range_valid_to.Text = .RELATION_DATE_RANGE_VALID_TO
                    'End If
                    'cbx_re_relation_date_range_is_approx_to_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    nfd_re_share_percentage.Value = .SHARE_PERCENTAGE
                    txt_re_comments.Value = .COMMENTS
                    txt_re_relation_comments.Value = .RELATION_COMMENTS
                    txt_re_name.Value = .NAME
                    txt_re_commercial_name.Value = .COMMERCIAL_NAME
                    If refLegalForm IsNot Nothing Then
                        cmb_re_incorporation_legal_form.SetTextWithTextValue(refLegalForm.Kode, refLegalForm.Keterangan)
                    Else
                        cmb_re_incorporation_legal_form.SetTextValue("")
                    End If
                    txt_re_incorporation_number.Value = .INCORPORATION_NUMBER
                    txt_re_business.Value = .BUSINESS
                    'If refEntityStatus IsNot Nothing Then
                    '    cmb_re_entity_status.SetTextWithTextValue(refEntityStatus.Kode, refEntityStatus.Keterangan)
                    'End If

                    'df_re_entity_status_date.Text = .ENTITY_STATUS
                    txt_re_incorporation_state.Value = .INCORPORATION_STATE
                    If refCountry IsNot Nothing Then
                        cmb_re_incorporation_country_code.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                    Else
                        cmb_re_incorporation_country_code.SetTextValue("")
                    End If

                    df_re_incorporation_date.Value = IIf(.INCORPORATION_DATE Is Nothing, "", .INCORPORATION_DATE)
                    cbx_re_business_closed.Checked = .BUSINESS_CLOSED
                    If .BUSINESS_CLOSED Then
                        df_re_date_business_closed.Hidden = False
                        df_re_date_business_closed.Value = IIf(.DATE_BUSINESS_CLOSED Is Nothing, "", .DATE_BUSINESS_CLOSED)
                    End If
                    txt_re_tax_number.Value = .TAX_NUMBER
                    txt_re_tax_reg_number.Value = .TAX_REG_NUMBER

                    Dim phone_list = If(.ObjList_GoAML_Ref_Customer_Phone IsNot Nothing, .ObjList_GoAML_Ref_Customer_Phone,
                        goAML_Customer_Service.GetPhoneByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim address_list = If(.ObjList_GoAML_Ref_Customer_Address IsNot Nothing, .ObjList_GoAML_Ref_Customer_Address,
                        goAML_Customer_Service.GetAddressByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim email_list = If(.ObjList_GoAML_Ref_Customer_Email IsNot Nothing, .ObjList_GoAML_Ref_Customer_Email,
                        goAML_Customer_Service.WICEmailService.GetByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim entity_identification_list = If(.ObjList_GoAML_Ref_Customer_Entity_Identification IsNot Nothing, .ObjList_GoAML_Ref_Customer_Entity_Identification,
                        goAML_Customer_Service.WICEntityIdentificationService.GetByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim url_list = If(.ObjList_GoAML_Ref_Customer_URL IsNot Nothing, .ObjList_GoAML_Ref_Customer_URL,
                        goAML_Customer_Service.WICSanctionService.GetByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim sanction_list = If(.ObjList_GoAML_Ref_Customer_Sanction IsNot Nothing, .ObjList_GoAML_Ref_Customer_Sanction,
                        goAML_Customer_Service.WICSanctionService.GetByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))

                    GridPhone_RelatedEntity.LoadData(phone_list)
                    GridAddress_RelatedEntity.LoadData(address_list)
                    GridEmail_RelatedEntity.LoadData(email_list)
                    GridEntityIdentification_RelatedEntity.LoadData(entity_identification_list)
                    GridURL_RelatedEntity.LoadData(url_list)
                    GridSanction_RelatedEntity.LoadData(sanction_list)
                End With

                WindowDetail_RelatedEntity.Title = "Related Entity " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub DeleteRecord_URL(id As Long)
        objListgoAML_vw_EntityUrl.Remove(objListgoAML_vw_EntityUrl.SingleOrDefault(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id))
        objListgoAML_Ref_EntityUrl.Remove(objListgoAML_Ref_EntityUrl.SingleOrDefault(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id))

        Store_WIC_Entity_URL.DataSource = objListgoAML_vw_EntityUrl.ToList()
        Store_WIC_Entity_URL.DataBind()

    End Sub

    Sub DeleteRecord_EntityIdentification(id As Long)
        objListgoAML_vw_EntityIdentification.Remove(objListgoAML_vw_EntityIdentification.SingleOrDefault(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id))
        objListgoAML_Ref_EntityIdentification.Remove(objListgoAML_Ref_EntityIdentification.SingleOrDefault(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id))

        Store_WIC_Entity_Identification.DataSource = objListgoAML_vw_EntityIdentification.ToList()
        Store_WIC_Entity_Identification.DataBind()

    End Sub

    Sub DeleteRecord_RelatedEntities(id As Long)
        objListgoAML_vw_RelatedEntity.Remove(objListgoAML_vw_RelatedEntity.SingleOrDefault(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id))
        objListgoAML_Ref_RelatedEntity.Remove(objListgoAML_Ref_RelatedEntity.SingleOrDefault(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id))

        Store_Related_Entities.DataSource = objListgoAML_vw_RelatedEntity.ToList()
        Store_Related_Entities.DataBind()

    End Sub

    Function IsDataWIC_Email_Valid() As Boolean
        If TxtEmail.Text.Trim = "" Then
            Throw New Exception("Email Address is required")
        End If

        Return True
    End Function

    Function IsDataWIC_PersonPEP_Valid() As Boolean
        If cmb_PersonPepCountryCode.SelectedItemText Is Nothing Then
            Throw New Exception("Country is required")
        End If

        If TxtFunctionName.Text.Trim = "" Then
            Throw New Exception("Function Name is required")
        End If
        'If TxtDescription.Text.Trim = "" Then
        '    Throw New Exception("Function Description is required")
        'End If
        'If TxtPersonPEPValidFrom.RawValue.Trim Is Nothing Then
        '    Throw New Exception("Valid From is required")
        'End If

        'If TxtPersonPEPValidTo.RawValue.Trim Is Nothing Then
        '    Throw New Exception("Valid To is required")
        'End If

        'If TxtPersonPEPComments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If

        Return True
    End Function

    Function IsDataWIC_EntityIdentification_Valid() As Boolean
        If ObjWIC.WIC_No.Trim = "" Then
            Throw New Exception("TxtWIC_No is required")
        End If

        If cmb_ef_type.SelectedItemText Is Nothing Then
            Throw New Exception("Type is required")
        End If
        If txt_ef_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'If df_issue_date.RawValue Is Nothing Then
        '    Throw New Exception("Issue Date is required")
        'End If
        'If df_expiry_date.RawValue Is Nothing Then
        '    Throw New Exception("Expiry Date is required")
        'End If
        'If txt_ef_issued_by.Text.Trim = "" Then
        '    Throw New Exception("Issued By is required")
        'End If
        If cmb_ef_issue_country.SelectedItemText Is Nothing Then
            Throw New Exception("Issue Country is required")
        End If
        'If txt_ef_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If


        Return True
    End Function

    Function IsDataWIC_Sanction_Valid() As Boolean
        If TxtProvider.Text.Trim = "" Then
            Throw New Exception("Provider is required")
        End If
        If TxtSanctionListName.Text.Trim = "" Then
            Throw New Exception("Sanction List Name is required")
        End If
        'If TxtMatchCriteria.Text.Trim = "" Then
        '    Throw New Exception("Match Criteria is required")
        'End If
        'If TxtLinkToSource.Text.Trim = "" Then
        '    Throw New Exception("Link To Source is required")
        'End If
        'If TxtSanctionListAttributes.Text.Trim = "" Then
        '    Throw New Exception("Sanction List Attributes is required")
        'End If
        'If dt_SanctionValidFrom.RawValue.Trim Is Nothing Then
        '    Throw New Exception("Valid From is required")
        'End If
        'If dt_SanctionValidTo.RawValue.Trim Is Nothing Then
        '    Throw New Exception("Valid To is required")
        'End If
        'If TxtSanctionComments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If

        Return True
    End Function

    Function IsDataWIC_RelatedPerson_Valid() As Boolean

        If dd_Person_Person_Relation.SelectedItemText Is Nothing Then
            Throw New Exception("Person Relation is required")
        End If
        'If TxtRelatedPersonComments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If
        'If TxtRelatedPersonGender.SelectedItemText Is Nothing Then
        '    Throw New Exception("Gender is required")
        'End If
        'If TxtRelatedPersonTitle.Text.Trim = "" Then
        '    Throw New Exception("Title is required")
        'End If
        'If TxtRelatedPersonFirst_Name.Text.Trim = "" Then
        '    Throw New Exception("First Name is required")
        'End If
        'If TxtRelatedPersonMiddle_Name.Text.Trim = "" Then
        '    Throw New Exception("Middle Name is required")
        'End If
        'If TxtRelatedPersonPrefix.Text.Trim = "" Then
        '    Throw New Exception("Prefix is required")
        'End If
        If TxtRelatedPersonLast_Name.Text.Trim = "" Then
            Throw New Exception("Fulll Name is required")
        End If
        'If dt_RelatedPersonBirth_Date.RawValue Is Nothing Then
        '    Throw New Exception("Birth Date is required")
        'End If
        'If TxtRelatedPersonBirth_Place.Text.Trim = "" Then
        '    Throw New Exception("Birth Place is required")
        'End If
        If Not IsFieldValid(TxtRelatedPersonBirth_Place.Text.Trim, "birth_place") Then
            Throw New Exception("Birth Place format is not valid")
        End If
        'If dd_RelatedPersonCountry_Of_Birth.SelectedItemText Is Nothing Then
        '    Throw New Exception("Country Of Birth is required")
        'End If
        'If TxtRelatedPersonMother_Name.Text.Trim = "" Then
        '    Throw New Exception("Mother's Name is required")
        'End If
        If Not IsFieldValid(TxtRelatedPersonMother_Name.Text.Trim, "mothers_name") Then
            Throw New Exception("Mothers Name format is not valid")
        End If
        'If TxtRelatedPersonAlias.Text.Trim = "" Then
        '    Throw New Exception("Alias is required")
        'End If
        If Not IsFieldValid(TxtRelatedPersonAlias.Text.Trim, "alias") Then
            Throw New Exception("Alias format is not valid")
        End If
        'If txt_rp_full_name_frn.Text.Trim = "" Then
        '    Throw New Exception("Full Name is required")
        'End If
        'If TxtRelatedPersonSSN.Text.Trim = "" Then
        '    Throw New Exception("SSN is required")
        'End If
        'If Not IsFieldValid(txt_rp_ssn.Text.Trim, "ssn") Then
        '    Throw New Exception("SSN format is not valid")
        'End If
        'If TxtPassport_Number.Text.Trim = "" Then
        '    Throw New Exception("Passport Number is required")
        'End If
        'If dd_Passport_Country.SelectedItemText Is Nothing Then
        '    Throw New Exception("Paassport Country is required")
        'End If
        'If TxtRelatedPersonID_Number.Text.Trim = "" Then
        '    Throw New Exception("ID Number is required")
        'End If
        'If dd_RelatedPersonNationality1.SelectedItemText Is Nothing Then
        '    Throw New Exception("Nationality 1 is required")
        'End If
        'If dd_RelatedPersonResidence.SelectedItemText Is Nothing Then
        '    Throw New Exception("Residence is required")
        'End If
        'If df_rp_residence_since.RawValue.Trim Is Nothing Then
        '    Throw New Exception("Residence Since is required")
        'End If
        'If TxtRelatedPersonOccupation.Text.Trim = "" Then
        '    Throw New Exception("Occupation is required")
        'End If
        'If dt_TxtRelatedPersonDate_Deceased.RawValue.Trim Is Nothing Then
        '    Throw New Exception("Date Deceased is required")
        'End If
        'If TxtRelatedPersonTax_Number.Text.Trim = "" Then
        '    Throw New Exception("Tax Number is required")
        'End If
        'If TxtRelatedPersonSource_Of_Wealth.Text.Trim = "" Then
        '    Throw New Exception("Source of Wealth is required")
        'End If

        Return True
    End Function

    Function IsDataWIC_URL_Valid() As Boolean

        If txt_url.Text.Trim = "" Then
            Throw New Exception("Url is required")
        End If

        Return True
    End Function

    Function IsDataWIC_RelatedEntity_Valid() As Boolean

        If cmb_re_entity_relation.SelectedItemText Is Nothing Then
            Throw New Exception("Entity Relation is required")
        End If
        'If df_re_relation_date_range_valid_from.RawValue Is Nothing Then
        '    Throw New Exception("Valid From is required")
        'End If
        'If df_re_relation_date_range_valid_to.RawValue Is Nothing Then
        '    Throw New Exception("Valid To is required")
        'End If
        'If nfd_re_share_percentage.Text.Trim = "" Then
        '    Throw New Exception("Share Percentage is required")
        'End If
        'If txt_re_comments.Text.Trim = "" Then
        '    Throw New Exception("Comments is required")
        'End If
        If txt_re_name.Text.Trim = "" Then
            Throw New Exception("Corporate Name is required")
        End If
        'If txt_re_commercial_name.Text.Trim = "" Then
        '    Throw New Exception("Commercial Name is required")
        'End If
        'If cmb_re_incorporation_legal_form.SelectedItemText Is Nothing Then
        '    Throw New Exception("Incorporation Legal is required")
        'End If
        'If txt_re_incorporation_number.Text.Trim = "" Then
        '    Throw New Exception("Incorporation Number is required")
        'End If
        'If txt_re_business.Text.Trim = "" Then
        '    Throw New Exception("Business is required")
        'End If
        'If cmb_re_entity_status.SelectedItemText Is Nothing Then
        '    Throw New Exception("Entity Status is required")
        'End If
        'If df_re_entity_status_date.RawValue Is Nothing Then
        '    Throw New Exception("Entity Status Date is required")
        'End If
        'If txt_re_incorporation_state.Text.Trim = "" Then
        '    Throw New Exception("Incorporation State is required")
        'End If
        If Not IsFieldValid(txt_re_incorporation_state.Text.Trim, "incorporation_state") Then
            Throw New Exception("Incorporation State format is not valid")
        End If
        'If cmb_re_incorporation_country_code.SelectedItemText Is Nothing Then
        '    Throw New Exception("Incorporation Country Code is required")
        'End If
        'If df_re_incorporation_date.RawValue Is Nothing Then
        '    Throw New Exception("Incorporation Date is required")
        'End If
        'If df_re_date_business_closed.RawValue Is Nothing Then
        '    Throw New Exception("Business Closed is required")
        'End If
        'If txt_re_tax_number.Text.Trim = "" Then
        '    Throw New Exception("Tax Number is required")
        'End If
        'If txt_re_tax_reg_number.Text.Trim = "" Then
        '    Throw New Exception("Tax Reg Number is required")
        'End If

        Return True
    End Function

    Private Function IsFieldValid(text As String, fieldName As String) As Boolean
        Try
            Dim regex As New Regex("^[0-9 ]+$")

            Select Case fieldName
                Case "incorporation_state"
                    regex = New Regex("[ a-zA-Z]*")
                Case "tax_number"
                    regex = New Regex("[0-9]{15}")
                Case "last_name"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "birth_place"
                    regex = New Regex("[ a-zA-Z]*")
                Case "mothers_name"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "alias"
                    regex = New Regex("[\-'. a-zA-Z]*")
                Case "ssn"
                    regex = New Regex("[0-9]{22}")
                Case "city"
                    regex = New Regex("[ a-zA-Z]*")
                Case "state"
                    regex = New Regex("[ a-zA-Z]*")
                Case "email_address"
                    regex = New Regex("[_a-zA-Z0-9-+]+(\.[_a-zA-Z0-9-\+]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})")
                Case "ipv4_address_type"
                    regex = New Regex("((1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])")
                Case "ipv6_address_type"
                    regex = New Regex("([A-Fa-f0-9]{1,4}:){7}[A-Fa-f0-9]{1,4}")
                Case Else
                    Return True
            End Select

            Return regex.IsMatch(text)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Sub BindData()
        Dim wicData = objgoAML_WIC.GetWICData(ObjWIC.WIC_No)
        BindForm()
        LoadObject(wicData)
        BindDetailSanction()
        BindDetailEmail()
        BindDetailPersonPEP()
        BindDetailRelatedPerson()
        BindDetail_Url(GPWIC_CORP_URL.GetStore, wicData.ObjGrid.ObjList_GoAML_URL)
        BindDetail_EntityIdentification(GPWIC_CORP_EntityIdentification.GetStore, wicData.ObjGrid.ObjList_GoAML_EntityIdentification)
        BindDetail_RelatedEntity(GPWIC_CORP_RelatedEntities.GetStore, wicData.ObjGrid.ObjList_GoAML_RelatedEntity)
    End Sub
    Private Sub LoadObject(objWicNew As GoAMLBLL.WICDataBLL)
        If objWicNew.ObjGrid IsNot Nothing Then
            'objListgoAML_Ref_Email = objWicNew.ObjGrid.ObjList_GoAML_Email
            'objListgoAML_Ref_PersonPEP_New = objWicNew.ObjGrid.ObjList_GoAML_PersonPEP
            'objListgoAML_Ref_Sanction_New = objWicNew.ObjGrid.ObjList_GoAML_Sanction
            'objListgoAML_Ref_RelatedPerson_New = objWicNew.ObjGrid.ObjList_GoAML_RelatedPerson
            objListgoAML_Ref_EntityUrl = objWicNew.ObjGrid.ObjList_GoAML_URL
            objListgoAML_Ref_EntityIdentification = objWicNew.ObjGrid.ObjList_GoAML_EntityIdentification
            objListgoAML_Ref_RelatedEntity = objWicNew.ObjGrid.ObjList_GoAML_RelatedEntity

            objListgoAML_vw_EntityUrl = objWicNew.ObjGrid.ObjList_GoAML_URL
            objListgoAML_vw_EntityIdentification = objWicNew.ObjGrid.ObjList_GoAML_EntityIdentification
            objListgoAML_vw_RelatedEntity = objWicNew.ObjGrid.ObjList_GoAML_RelatedEntity
        End If
    End Sub
    Sub BindForm()
        Dim wicData = objgoAML_WIC.GetWIC(ObjWIC.WIC_No)

        Cbx_IsRealWIC.Checked = wicData.IS_REAL_WIC
        Cbx_IsProtected.Checked = wicData.IS_PROTECTED
    End Sub
    Private Sub BindDetail_Url(store As Store, list As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL))
        If list IsNot Nothing Then
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            store.DataSource = objtable
            store.DataBind()
        End If
    End Sub

    Private Sub BindDetail_EntityIdentification(store As Store, list As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification))
        If list IsNot Nothing Then
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            store.DataSource = objtable
            store.DataBind()
        End If
    End Sub

    Private Sub BindDetail_RelatedEntity(store As Store, list As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity))
        If list IsNot Nothing Then
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            store.DataSource = objtable
            store.DataBind()
        End If
    End Sub
    Private Function GetObjData() As WICDataBLL
        Dim wicData = New WICDataBLL
        wicData.ObjWIC2 = GetObjWIC()
        wicData.ObjGrid = GetObjGrid()

        Return wicData
    End Function

    Private Function GetObjWIC() As WICDataBLL.goAML_Ref_WIC2
        Dim objWIC2 = New WICDataBLL.goAML_Ref_WIC2
        objWIC2.WIC_No = ObjWIC.WIC_No
        objWIC2.IS_REAL_WIC = Cbx_IsRealWIC.Checked
        objWIC2.IS_PROTECTED = Cbx_IsProtected.Checked

        Return objWIC2
    End Function
    Private Function GetObjGrid() As WICDataBLL.goAML_Grid_WIC
        Dim gridWIC = New WICDataBLL.goAML_Grid_WIC
        gridWIC.ObjList_GoAML_Email = obj_ListEmail_Edit
        gridWIC.ObjList_GoAML_PersonPEP = obj_ListPersonPEP_Edit
        gridWIC.ObjList_GoAML_Sanction = obj_ListSanction_Edit
        gridWIC.ObjList_GoAML_RelatedPerson = obj_ListRelatedPerson_Edit
        gridWIC.ObjList_GoAML_URL = objListgoAML_Ref_EntityUrl
        gridWIC.ObjList_GoAML_EntityIdentification = objListgoAML_Ref_EntityIdentification
        gridWIC.ObjList_GoAML_RelatedEntity = objListgoAML_Ref_RelatedEntity

        Return gridWIC
    End Function

    Private Function IsFormChanged(wicData As WICDataBLL) As Boolean
        If IsWIC_Changed(wicData.ObjWIC2) Then
            Return True
        End If

        If IsEmail_Changed(wicData.ObjWIC2.WIC_No, wicData.ObjGrid.ObjList_GoAML_Email) Then
            Return True
        End If

        If IsPersonPEP_Changed(wicData.ObjWIC2.WIC_No, wicData.ObjGrid.ObjList_GoAML_PersonPEP) Then
            Return True
        End If

        If IsSanction_Changed(wicData.ObjWIC2.WIC_No, wicData.ObjGrid.ObjList_GoAML_Sanction) Then
            Return True
        End If

        If IsRelatedPerson_Changed(wicData.ObjWIC2.WIC_No, wicData.ObjGrid.ObjList_GoAML_RelatedPerson) Then
            Return True
        End If

        If IsURL_Changed(wicData.ObjWIC2.WIC_No, wicData.ObjGrid.ObjList_GoAML_URL) Then
            Return True
        End If

        If IsEntityIdentification_Changed(wicData.ObjWIC2.WIC_No, wicData.ObjGrid.ObjList_GoAML_EntityIdentification) Then
            Return True
        End If

        If IsRelatedEntity_Changed(wicData.ObjWIC2.WIC_No, wicData.ObjGrid.ObjList_GoAML_RelatedEntity) Then
            Return True
        End If

        Return False
    End Function

    Private Function IsWIC_Changed(wic As WICDataBLL.goAML_Ref_WIC2) As Boolean

        Dim wicOld = objgoAML_WIC.GetWIC(ObjWIC.WIC_No)

        If wicOld.IS_REAL_WIC <> wic.IS_REAL_WIC Then
            Return True
        End If

        If wicOld.IS_PROTECTED <> wic.IS_PROTECTED Then
            Return True
        End If

        Return False
    End Function

    Private Function IsEmail_Changed(wicNo As String, listNew As List(Of WICDataBLL.goAML_Ref_WIC_Email)) As Boolean
        Dim listOld = objgoAML_WIC.GetEmail(wicNo)
        If listOld.Count = listNew.Count Then
            For Each itemNew In listNew
                Dim itemOld = listOld.Where(Function(x) x.PK_goAML_Ref_WIC_Email_ID = itemNew.PK_goAML_Ref_WIC_Email_ID).FirstOrDefault()
                If IsItem_Changed(itemOld, itemNew) Then
                    Return True
                End If
            Next

        Else
            Return True
        End If

        Return False
    End Function

    Private Function IsPersonPEP_Changed(wicNo As String, listNew As List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP)) As Boolean
        Dim listOld = objgoAML_WIC.GetPEP(wicNo)
        If listOld.Count = listNew.Count Then
            For Each itemNew In listNew
                Dim itemOld = listOld.Where(Function(x) x.PK_goAML_Ref_WIC_PEP_ID = itemNew.PK_goAML_Ref_WIC_PEP_ID).FirstOrDefault()
                If IsItem_Changed(itemOld, itemNew) Then
                    Return True
                End If
            Next

        Else
            Return True
        End If

        Return False
    End Function

    Private Function IsSanction_Changed(wicNo As String, listNew As List(Of WICDataBLL.goAML_Ref_WIC_Sanction)) As Boolean
        Dim listOld = objgoAML_WIC.GetSanction(wicNo, 3)
        If listOld.Count = listNew.Count Then
            For Each itemNew In listNew
                Dim itemOld = listOld.Where(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = itemNew.PK_goAML_Ref_WIC_Sanction_ID).FirstOrDefault()
                If IsItem_Changed(itemOld, itemNew) Then
                    Return True
                End If
            Next
        Else
            Return True
        End If

        Return False
    End Function

    Private Function IsRelatedPerson_Changed(wicNo As String, listNew As List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)) As Boolean
        Dim listOld = objgoAML_WIC.GetRelatedPerson(wicNo)
        If listOld.Count = listNew.Count Then
            For Each itemNew In listNew
                Dim itemOld = listOld.Where(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = itemNew.PK_goAML_Ref_WIC_Related_Person_ID).FirstOrDefault()
                If IsItem_Changed(itemOld, itemNew) Then
                    Return True
                End If
            Next
        Else
            Return True
        End If

        If GridAddressWork_RelatedPerson.IsDataChange Then
            Return True
        End If
        If GridAddress_RelatedPerson.IsDataChange Then
            Return True
        End If
        If GridPhone_RelatedPerson.IsDataChange Then
            Return True
        End If
        If GridPhoneWork_RelatedPerson.IsDataChange Then
            Return True
        End If
        If GridIdentification_RelatedPerson.IsDataChange Then
            Return True
        End If
        If GridEmail_RelatedPerson.IsDataChange Then
            Return True
        End If
        If GridSanction_RelatedPerson.IsDataChange Then
            Return True
        End If
        If GridPEP_RelatedPerson.IsDataChange Then
            Return True
        End If

        Return False
    End Function

    Private Function IsURL_Changed(wicNo As String, listNew As List(Of WICDataBLL.goAML_Ref_WIC_URL)) As Boolean
        Dim listOld = objgoAML_WIC.GetURL(wicNo)
        If listOld.Count = listNew.Count Then
            For Each itemNew In listNew
                Dim itemOld = listOld.Where(Function(x) x.PK_goAML_Ref_WIC_URL_ID = itemNew.PK_goAML_Ref_WIC_URL_ID).FirstOrDefault()
                If IsItem_Changed(itemOld, itemNew) Then
                    Return True
                End If
            Next
        Else
            Return True
        End If

        Return False
    End Function

    Private Function IsEntityIdentification_Changed(wicNo As String, listNew As List(Of WICDataBLL.goAML_Ref_WIC_Entity_Identification)) As Boolean
        Dim listOld = objgoAML_WIC.GetEntityIdentification(wicNo, 3)
        If listOld.Count = listNew.Count Then
            For Each itemNew In listNew
                Dim itemOld = listOld.Where(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = itemNew.PK_goAML_Ref_WIC_Entity_Identifications_ID).FirstOrDefault()
                If IsItem_Changed(itemOld, itemNew) Then
                    Return True
                End If
            Next
        Else
            Return True
        End If

        Return False
    End Function

    Private Function IsRelatedEntity_Changed(wicNo As String, listNew As List(Of WICDataBLL.goAML_Ref_WIC_Related_Entity)) As Boolean
        Dim listOld = objgoAML_WIC.GetRelatedEntity(wicNo)
        If listOld.Count = listNew.Count Then
            For Each itemNew In listNew
                Dim itemOld = listOld.Where(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = itemNew.PK_goAML_Ref_WIC_Related_Entities_ID).FirstOrDefault()
                If IsItem_Changed(itemOld, itemNew) Then
                    Return True
                End If
            Next
        Else
            Return True
        End If

        If GridPhone_RelatedEntity.IsDataChange Then
            Return True
        End If
        If GridAddress_RelatedEntity.IsDataChange Then
            Return True
        End If
        If GridEmail_RelatedEntity.IsDataChange Then
            Return True
        End If
        If GridEntityIdentification_RelatedEntity.IsDataChange Then
            Return True
        End If
        If GridURL_RelatedEntity.IsDataChange Then
            Return True
        End If
        If GridSanction_RelatedEntity.IsDataChange Then
            Return True
        End If

        Return False
    End Function

    Private Function IsItem_Changed(itemOld As Object, itemNew As Object) As Boolean
        If itemOld Is Nothing Then
            Return True
        End If

        Dim listPNew = itemNew.GetType().GetProperties()

        For Each pOld As System.Reflection.PropertyInfo In itemOld.GetType().GetProperties()
            If pOld.CanRead Then
                'Console.WriteLine("{0}: {1}", p.Name, p.GetValue(obj, Nothing))
                If {"Active", "CreatedBy", "LastUpdateBy", "ApprovedBy", "CreatedDate", "LastUpdateDate"}.Contains(pOld.Name) Then
                    Continue For
                End If
                Dim pNew = listPNew.FirstOrDefault(Function(x) x.Name = pOld.Name)
                If Not pNew.PropertyType().Name.ToLower().Contains("list") Then
                    If pNew.GetValue(itemNew) <> pOld.GetValue(itemOld) Then
                        Return True
                    End If
                End If
            End If
        Next

        Return False
    End Function

    Private Sub ClearFormPanel(formPanel As Ext.Net.FormPanel)

        If formPanel IsNot Nothing Then
            For Each item As Ext.Net.ComponentBase In formPanel.Items
                'Dim test = item.GetType().Name
                If TypeOf item Is Ext.Net.TextField Then
                    Dim textField As Ext.Net.TextField = CType(item, Ext.Net.TextField)
                    textField.Text = ""
                ElseIf TypeOf item Is Ext.Net.NumberField Then
                    Dim numberField As Ext.Net.NumberField = CType(item, Ext.Net.NumberField)
                    numberField.Value = 0
                ElseIf TypeOf item Is Ext.Net.DateField Then
                    Dim dateField As Ext.Net.DateField = CType(item, Ext.Net.DateField)
                    dateField.Text = ""
                ElseIf TypeOf item Is Ext.Net.Checkbox Then
                    Dim checkBox As Ext.Net.Checkbox = CType(item, Ext.Net.Checkbox)
                    checkBox.Checked = False
                End If
            Next
        End If
    End Sub
#End Region

End Class
