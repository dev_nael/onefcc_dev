﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="WICAdd_V501.aspx.vb" Inherits="goAML_WICAdd_V501" %>


<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<%@ Register Src="~/goAML/WIC/Component/GridAddress.ascx" TagPrefix="nds" TagName="GridAddress" %>
<%@ Register Src="~/goAML/WIC/Component/GridAddressWork.ascx" TagPrefix="nds" TagName="GridAddressWork" %>
<%@ Register Src="~/goAML/WIC/Component/GridEmail.ascx" TagPrefix="nds" TagName="GridEmail" %>
<%@ Register Src="~/goAML/WIC/Component/GridIdentification.ascx" TagPrefix="nds" TagName="GridIdentification" %>
<%@ Register Src="~/goAML/WIC/Component/GridPEP.ascx" TagPrefix="nds" TagName="GridPEP" %>
<%@ Register Src="~/goAML/WIC/Component/GridPhone.ascx" TagPrefix="nds" TagName="GridPhone" %>
<%@ Register Src="~/goAML/WIC/Component/GridPhoneWork.ascx" TagPrefix="nds" TagName="GridPhoneWork" %>
<%@ Register Src="~/goAML/WIC/Component/GridSanction.ascx" TagPrefix="nds" TagName="GridSanction" %>
<%@ Register Src="~/goAML/WIC/Component/GridEntityIdentification.ascx" TagPrefix="nds" TagName="GridEntityIdentification" %>
<%@ Register Src="~/goAML/WIC/Component/GridURL.ascx" TagPrefix="nds" TagName="GridURL" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .x-form-item-label.x-form-item-label-default { width : 175px !important; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <ext:Window ID="WindowDetailDirector" runat="server" Icon="ApplicationViewDetail" Title="Director" Modal="true" Hidden="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout">
        <Content>
            <ext:FormPanel ID="FormPanelDirectorDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:Hidden runat="server" ID="Hidden1"></ext:Hidden>
                    <ext:DisplayField ID="DspPeranDirector" runat="server" FieldLabel="Role" AnchorHorizontal="70%" LabelWidth="250"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250" ID="DspGender" runat="server" FieldLabel="Gender" AnchorHorizontal="70%" AllowBlank="false"></ext:DisplayField>
                    <ext:DisplayField ID="DspGelarDirector" LabelWidth="250" runat="server" FieldLabel="Title" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaLengkap" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="90%"></ext:DisplayField>
                    <%--daniel 2020 des 29--%>
                    <ext:DisplayField LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Birth of Date" ID="DspTanggalLahir" AnchorHorizontal="90%"/>
                    <%--end 2020 des 29--%>
                    <ext:DisplayField ID="DspTempatLahir" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Birth of Place" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaIbuKandung" LabelWidth="250" runat="server" FieldLabel="Mother Maiden Name" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaAlias" LabelWidth="250" runat="server" FieldLabel="Alias Name" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNIK" LabelWidth="250" runat="server" FieldLabel="NIK/SSN" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNoPassport" LabelWidth="250" runat="server" FieldLabel="Passport No" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraPenerbitPassport" LabelWidth="250" runat="server" FieldLabel="Passport Issued Country" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNoIdentitasLain" LabelWidth="250" runat="server" FieldLabel="Other ID Number" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250" AllowBlank="False" ID="DspKewarganegaraan1" runat="server" FieldLabel="Nationality 1" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspKewarganegaraan2" LabelWidth="250" runat="server" FieldLabel="Nationality 2" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspKewarganegaraan3" LabelWidth="250" runat="server" FieldLabel="Nationality 3" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDomisili" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Domicile Country" AnchorHorizontal="70%"></ext:DisplayField>
                    



                    <%-- Grid Panel --%>
                    <ext:GridPanel ID="GridPanel1" runat="server" Title="Phone" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StorePhoneDirector" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column1" runat="server" DataIndex="Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanel2" runat="server" Title="Informasi Address" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreAddressDirector" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column7" runat="server" DataIndex="Type_Address" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="Addres" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="Cty" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="CountryCode" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <%--<ext:DisplayField ID="DspEmail" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%"></ext:DisplayField>--%>
                    <ext:DisplayField ID="DspOccupation" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspTempatBekerja" LabelWidth="250" runat="server" FieldLabel="Work Place" AnchorHorizontal="90%"></ext:DisplayField>
                   
                    <ext:GridPanel ID="GridPanel3" runat="server" Title="Work Place Phone" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StorePhoneDirectorEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model8" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column15" runat="server" DataIndex="Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanel4" runat="server" Title="Work Place Address" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreAddressDirectorEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model17" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column21" runat="server" DataIndex="Type_Address" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="Addres" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="Cty" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="CountryCode" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn4" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanel5" runat="server" Title="Identification" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreIdentificationDirector" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model18" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column6" runat="server" DataIndex="Tipe" Text="Identity Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="No" Text="Identity Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="IssueDate" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="ExpiryDate" Text="Expired Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="IssuedCountry" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn5" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DisplayField runat="server" LabelWidth="250" ID="DspDeceased" FieldLabel="Deceased ?"></ext:DisplayField>
                    <%--daniel 2020 des 29--%>
                    <ext:DisplayField runat="server" LabelWidth="250" Hidden="true" FieldLabel="Deceased Date " ID="DspDeceasedDate" />
                    <%--end 2020 des 29--%>
                    <ext:DisplayField runat="server" LabelWidth="250" ID="DspPEP" FieldLabel="PEP ?"></ext:DisplayField>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tax Number" ID="DspNPWP" AnchorHorizontal="90%" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Source of Wealth" ID="DspSourceofWealth" AnchorHorizontal="90%" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Number" ID="DspCatatan" AnchorHorizontal="90%" />

                    <ext:Panel ID="Panel_Director_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridEmail runat="server" ID="Dsp_GridEmail_Director" FK_REF_DETAIL_OF="8" UniqueName = "Director"/>
                        </Content>
                    </ext:Panel>

                    <ext:Panel ID="Panel_Director_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridSanction runat="server" ID="Dsp_GridSanction_Director" UniqueName = "Director" FK_REF_DETAIL_OF="8"/>
                        </Content>
                    </ext:Panel>

                    <ext:Panel ID="Panel_Director_GridPEP" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%" Title="Person PEP">
                        <Content>
                            <nds:GridPEP runat="server" ID="Dsp_GridPEP_Director" UniqueName = "Director" FK_REF_DETAIL_OF="8"/>
                        </Content>
                    </ext:Panel>
                </Items>
                <Buttons>
                    <ext:Button ID="Button5" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveCustomerDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FP_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:Hidden runat="server" ID="Hidden2"></ext:Hidden>
                    <ext:ComboBox ID="cmb_peran" runat="server" FieldLabel="Role" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" LabelWidth="250" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StorePeran" OnReadData="Role_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model7">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:ComboBox LabelWidth="250" ID="cmb_Director_Gender" runat="server" FieldLabel="Gender" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreGender" OnReadData="Gender_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model9">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>
                    <ext:TextField ID="txt_Director_Title" LabelWidth="250" runat="server" FieldLabel="Title" AnchorHorizontal="90%" MaxLength="30"></ext:TextField>
                    <ext:TextField ID="txt_Director_Last_Name" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                    <ext:DateField LabelWidth="250" runat="server" FieldLabel="Birth of Date" ID="txt_Director_BirthDate" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                    <ext:TextField ID="txt_Director_Birth_Place" LabelWidth="250" runat="server" FieldLabel="Birth of Place" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="txt_Director_Mothers_Name" LabelWidth="250" runat="server" FieldLabel="MOther Maiden Name" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="txt_Director_Alias" LabelWidth="250" runat="server" FieldLabel="Alias Name" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="txt_Director_SSN" LabelWidth="250" runat="server" FieldLabel="NIK/SSN" AnchorHorizontal="90%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="16" MinLength="16"></ext:TextField>
                    <ext:TextField ID="txt_Director_Passport_Number" LabelWidth="250" runat="server" FieldLabel="Passport No" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>
                    <%--<ext:ComboBox ID="txt_Director_Passport_Country" LabelWidth="250" runat="server" FieldLabel="Negara Penerbit Passport" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirectorPassportNumber" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model31">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel5" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="txt_Director_Passport_Country" LabelWidth="250" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Passport Issued Country" AnchorHorizontal="80%" />
                        </Content>
                    </ext:Panel>
                    <ext:TextField ID="txt_Director_ID_Number" LabelWidth="250" runat="server" FieldLabel="Other ID Number" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                    <ext:Panel ID="Panel6" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_Director_Nationality1" LabelWidth="250" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Nationality 1" AnchorHorizontal="80%"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel7" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_Director_Nationality2" LabelWidth="250" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Nationality 2" AnchorHorizontal="80%" />
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel8" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_Director_Nationality3" LabelWidth="250" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Nationality 3" AnchorHorizontal="80%" />
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel9" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_Director_Residence" LabelWidth="250" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Domicile Country" AnchorHorizontal="80%"/>
                        </Content>
                    </ext:Panel>
                    <%--<ext:ComboBox LabelWidth="250" AllowBlank="False" ID="cmb_Director_Nationality1" runat="server" FieldLabel="Kewarganegaraan 1" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirNational1" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model10">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:ComboBox ID="cmb_Director_Nationality2" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 2" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirNational2" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model11">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:ComboBox ID="cmb_Director_Nationality3" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 3" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirNational3" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model12">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:ComboBox ID="cmb_Director_Residence" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Negara Domisili" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirReisdence" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model13">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>

                    <ext:GridPanel ID="GP_DirectorPhone" runat="server" Title="Phone" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar10" runat="server">
                                <Items>
                                    <ext:Button ID="btnsavedirectorphone" runat="server" Text="Add Phones" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddDirectorPhones_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_DirectorPhone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model6" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column61" runat="server" DataIndex="Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column62" runat="server" DataIndex="Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column64" runat="server" DataIndex="number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn11" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GP_DirectorAddress" runat="server" Title="Address" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar11" runat="server">
                                <Items>
                                    <ext:Button ID="btnsavedirectorAddress" runat="server" Text="Add Addresses" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddDirectorAddresses_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_DirectorAddress" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model16" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column67" runat="server" DataIndex="Type_Address" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column68" runat="server" DataIndex="Addres" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column70" runat="server" DataIndex="Cty" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column72" runat="server" DataIndex="CountryCode" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn12" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <%--<ext:TextField ID="txt_Director_Email" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="txt_Director_Email2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="txt_Director_Email3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="txt_Director_Email4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>
                    <ext:TextField ID="txt_Director_Email5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email"></ext:TextField>--%>
                    <ext:TextField ID="txt_Director_Occupation" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="txt_Director_employer_name" LabelWidth="250" runat="server" FieldLabel="Work Place" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>

                    <ext:GridPanel ID="GP_DirectorEmpPhone" runat="server" Title="Work Place Phone" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar8" runat="server">
                                <Items>
                                    <ext:Button ID="btnsaveEmpdirectorPhone" runat="server" Text="Add Phones" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddDirectorEmployerPhones_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Director_Employer_Phone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model14" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column47" runat="server" DataIndex="Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column48" runat="server" DataIndex="Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column50" runat="server" DataIndex="number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn9" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GP_DirectorEmpAddress" runat="server" Title="Work Place Address" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar9" runat="server">
                                <Items>
                                    <ext:Button ID="btnsaveEmpdirectorAddress" runat="server" Text="Add Addresses" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddDirectorEmployerAddresses_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Director_Employer_Address" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model15" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column53" runat="server" DataIndex="TypeAddress" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column54" runat="server" DataIndex="Addres" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column56" runat="server" DataIndex="Cty" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column58" runat="server" DataIndex="CountryCode" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn10" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GP_DirectorIdentification" runat="server" Title="Identification" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar4" runat="server">
                                <Items>
                                    <ext:Button ID="Button11" runat="server" Text="Add Identitas" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddDirectorIdentification_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreIdentificationDirectorDetail" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model19" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column3" runat="server" DataIndex="Tipe" Text="Identity Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="No" Text="Identity Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="IssueDate" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column23" runat="server" DataIndex="ExpiryDate" Text="Expired Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column25" runat="server" DataIndex="IssuedCountry" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn6" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Director_Deceased" FieldLabel="Deceased ?">
                        <DirectEvents>
                            <Change OnEvent="checkBoxcb_Director_Deceased_click" />
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField runat="server" LabelWidth="250" Hidden="true" FieldLabel="Deceased Date " Format="dd-MMM-yyyy" ID="txt_Director_Deceased_Date" />
                    <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Director_Tax_Reg_Number" FieldLabel="PEP ?"></ext:Checkbox>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Tax Number" ID="txt_Director_Tax_Number" AnchorHorizontal="90%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="15" MinLength="15"></ext:TextField>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Source of Wealth" ID="txt_Director_Source_of_Wealth" AnchorHorizontal="90%" MaxLength="255" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Notes" ID="txt_Director_Comments" AnchorHorizontal="90%" MaxLength="4000" />
                
                    <ext:Panel ID="Panel16" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridEmail runat="server" ID="GridEmail_Director" FK_REF_DETAIL_OF="8" UniqueName = "Director"/>
                        </Content>
                    </ext:Panel>

                    <ext:Panel ID="Panel17" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridSanction runat="server" ID="GridSanction_Director" UniqueName = "Director" FK_REF_DETAIL_OF="8"/>
                        </Content>
                    </ext:Panel>

                    <ext:Panel ID="Panel18" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridPEP runat="server" ID="GridPEP_Director" />
                        </Content>
                    </ext:Panel>
                </Items>
                <Buttons>
                    <ext:Button ID="btnsaveDirector" runat="server" Text="Save" DefaultAlign="center">
                        <%--<Listeners>
                            <Click Handler="if (!#{FP_Director}.getForm().isValid()) return false;"></Click>
                        </Listeners>--%>
                        <DirectEvents>
                            <Click OnEvent="btnSaveDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button9" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveCustomerDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                </Buttons>

            </ext:FormPanel>
        </Content>

    </ext:Window>

    <ext:Window ID="WindowDetailDirectorIdentification" runat="server" Icon="ApplicationViewDetail" Title="Director Identitas" Hidden="true" Layout="FitLayout" Modal="true" MinWidth="500" Height="300" >
        <Items>
            <ext:FormPanel ID="PanelDetailDirectorIdentification" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DsptTypeDirector" runat="server" FieldLabel="Identity Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspNumberDirector" runat="server" FieldLabel="Identity Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssueDateDirector" runat="server" FieldLabel="Issued Date"></ext:DisplayField>
                    <ext:DisplayField ID="DspExpiryDateDirector" runat="server" FieldLabel="Expired Datee"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedByDirector" runat="server" FieldLabel="Issued By"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedCountryDirector" runat="server" FieldLabel="Issued Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspIdentificationCommentDirector" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button6" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelIdentificationDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FormPanelDirectorIdentification" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:ComboBox ID="CmbTypeIdentificationDirector" runat="server" FieldLabel="Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreTypeIdentificationDirector" OnReadData="Identification_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="TxtNumberDirector" runat="server" FieldLabel="Identification Number" AnchorHorizontal="80%" AllowBlank="false" MaxLength="255"></ext:TextField>
                    <ext:DateField ID="TxtIssueDateDirector" runat="server" Format="dd-MMM-yyyy" FieldLabel="Issued Date" AnchorHorizontal="80%"></ext:DateField>
                    <ext:DateField ID="TxtExpiryDateDirector" runat="server" Format="dd-MMM-yyyy" FieldLabel="Expired Date" AnchorHorizontal="80%"></ext:DateField>
                    <ext:TextField ID="TxtIssuedByDirector" runat="server" FieldLabel="Issued By" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                   <%-- <ext:ComboBox ID="CmbCountryIdentificationDirector" runat="server" FieldLabel="Negara Penerbit Identitas" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreCountryCodeIdentificationDirector" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel10" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbCountryIdentificationDirector"  ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Issued Country" AnchorHorizontal="80%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <ext:TextArea ID="TxtCommentIdentificationDirector" runat="server" FieldLabel="Notes" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="Button8" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelDirectorIdentification}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="BtnSaveDetailIdentificationDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button10" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelIdentificationDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailDirectorIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailIdentification" runat="server" Icon="ApplicationViewDetail" Title="Identification" Hidden="true" Layout="FitLayout" Modal="true" MinWidth="800" Height="300" >
        <Items>
            <ext:FormPanel ID="PanelDetailIdentification" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DsptType" runat="server" FieldLabel="Identity Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspNumber" runat="server" FieldLabel="Identity Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssueDate" runat="server" FieldLabel="Issued Date"></ext:DisplayField>
                    <ext:DisplayField ID="DspExpiryDate" runat="server" FieldLabel="Expired Date"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedBy" runat="server" FieldLabel="Issued By"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedCountry" runat="server" FieldLabel="Issued Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspIdentificationComment" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button22" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelIdentification_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FormPanelIdentification" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:ComboBox ID="CmbTypeIdentification" runat="server" FieldLabel="Identity Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreTypeIdentification" OnReadData="Identification_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="TxtNumber" runat="server" FieldLabel="Identity Number" AnchorHorizontal="80%" AllowBlank="false" MaxLength="255"></ext:TextField>
                    <ext:DateField ID="TxtIssueDate" runat="server" Format="dd-MMM-yyyy" FieldLabel="Issued Date" AnchorHorizontal="80%"></ext:DateField>
                    <ext:DateField ID="TxtExpiryDate" runat="server" Format="dd-MMM-yyyy" FieldLabel="Expired Date" AnchorHorizontal="80%"></ext:DateField>
                    <ext:TextField ID="TxtIssuedBy" runat="server" FieldLabel="Issued By" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <%--<ext:ComboBox ID="CmbCountryIdentification" runat="server" FieldLabel="Negara Penerbit Identitas" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreCountryCodeIdentification" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel11" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbCountryIdentification"  ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Issued Country" AnchorHorizontal="80%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <ext:TextArea ID="TxtCommentIdentification" runat="server" FieldLabel="Notes" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="Button23" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelIdentification}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="BtnSaveDetailIdentification_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button24" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDetailIdentification_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorPhone" runat="server" Icon="ApplicationViewDetail" Title="Director Phone" Modal="true" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="PanelPhoneDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DspKategoriKontakDirector" runat="server" FieldLabel="Contact Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspJenisAlatKomunikasiDirector" runat="server" FieldLabel="Communication Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodeAreaTelpDirector" runat="server" FieldLabel="Prefix"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorTeleponDirector" runat="server" FieldLabel="Phone Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorExtensiDirector" runat="server" FieldLabel="Extention"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanDirectorPhoneDirector" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button3" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelPhoneDirectorEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DspKategoriKontakDirectorEmployer" runat="server" FieldLabel="Contact Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspJenisAlatKomunikasiDirectorEmployer" runat="server" FieldLabel="Communication Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodeAreaTelpDirectorEmployer" runat="server" FieldLabel="Prefix"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorTeleponDirectorEmployer" runat="server" FieldLabel="Phone Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorExtensiDirectorEmployer" runat="server" FieldLabel="Extention"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanDirectorPhoneDirectorEmployer" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button4" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorEmployerPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FormPanelDirectorPhone" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:ComboBox ID="Director_cb_phone_Contact_Type" runat="server" FieldLabel="Contact Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirPContactType" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model21">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:ComboBox ID="Director_cb_phone_Communication_Type" runat="server" FieldLabel="Communication Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirComunicationType" OnReadData="Communication_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model22">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="Director_txt_phone_Country_prefix" runat="server" FieldLabel="Prefix" AnchorHorizontal="90%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="4"></ext:TextField>
                    <ext:TextField ID="Director_txt_phone_number" runat="server" FieldLabel="Phone Number" AnchorHorizontal="90%" AllowBlank="false" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="50"></ext:TextField>
                    <ext:TextField ID="Director_txt_phone_extension" runat="server" FieldLabel="Extention" AnchorHorizontal="90%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="10"></ext:TextField>
                    <ext:TextField ID="Director_txt_phone_comments" runat="server" FieldLabel="Notes" AnchorHorizontal="90%" MaxLength="4000"></ext:TextField>
                </Items>
                <Buttons>
                    <ext:Button ID="btnSaveDirectorPhonedetail" runat="server" Text="Save" DefaultAlign="center">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelDirectorPhone}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="btnSaveDirectorPhones_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="btnBackSaveDirectorPhoneDetail" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveDirectorPhones_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FormPanelEmpDirectorTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:ComboBox ID="Director_Emp_cb_phone_Contact_Type" runat="server" FieldLabel="Contact Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreEmpDirPContactType" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model23">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:ComboBox ID="Director_Emp_cb_phone_Communication_Type" runat="server" FieldLabel="Communication Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreEmpDirComunicationType" OnReadData="Communication_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model24">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="Director_Emp_txt_phone_Country_prefix" runat="server" FieldLabel="Prefix" Regex="^[0-9]\d*$" RegexText="Must Numeric" AnchorHorizontal="90%" MaxLength="4"></ext:TextField>
                    <ext:TextField ID="Director_Emp_txt_phone_number" runat="server" FieldLabel="Phone Number" AnchorHorizontal="90%" Regex="^[0-9]\d*$" RegexText="Must Numeric" AllowBlank="false" MaxLength="50"></ext:TextField>
                    <ext:TextField ID="Director_Emp_txt_phone_extension" runat="server" FieldLabel="Extention" AnchorHorizontal="90%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="10"></ext:TextField>
                    <ext:TextField ID="Director_Emp_txt_phone_comments" runat="server" FieldLabel="Notes" AnchorHorizontal="90%" MaxLength="4000"></ext:TextField>
                </Items>
                <Buttons>
                    <ext:Button ID="btn_saveDirector_empphone" runat="server" Text="Save" DefaultAlign="center">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelEmpDirectorTaskDetail}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="btnSaveDirectorEmpPhones_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button13" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveDirectorEmpPhones_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorAddress" runat="server" Icon="ApplicationViewDetail" Title="Director Address" Modal="true" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="PanelAddressDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DspTipeAlamatDirector" runat="server" FieldLabel="Address Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspAlamatDirector" runat="server" FieldLabel="Address"></ext:DisplayField>
                    <ext:DisplayField ID="DspKecamatanDirector" runat="server" FieldLabel="Town"></ext:DisplayField>
                    <ext:DisplayField ID="DspKotaKabupatenDirector" runat="server" FieldLabel="City"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodePosDirector" runat="server" FieldLabel="Zip Code"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDirector" runat="server" FieldLabel="Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspProvinsiDirector" runat="server" FieldLabel="Province"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanAddressDirector" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button1" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorDetailAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelAddressDirectorEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DspTipeAlamatDirectorEmployer" runat="server" FieldLabel="Address Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspAlamatDirectorEmployer" runat="server" FieldLabel="Address"></ext:DisplayField>
                    <ext:DisplayField ID="DspKecamatanDirectorEmployer" runat="server" FieldLabel="Town"></ext:DisplayField>
                    <ext:DisplayField ID="DspKotaKabupatenDirectorEmployer" runat="server" FieldLabel="City"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodePosDirectorEmployer" runat="server" FieldLabel="Zip Code"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDirectorEmployer" runat="server" FieldLabel="Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspProvinsiDirectorEmployer" runat="server" FieldLabel="Province"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanAddressDirectorEmployer" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button2" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorDetailEmployerAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FP_Address_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:ComboBox ID="Director_cmb_kategoriaddress" runat="server" FieldLabel="Address Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirKategoryADdressAddress_Type" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model25">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="Director_Address" runat="server" FieldLabel="Address" AnchorHorizontal="90%" AllowBlank="false" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="Director_Town" runat="server" FieldLabel="Town" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="Director_City" runat="server" FieldLabel="Kota" AnchorHorizontal="90%" AllowBlank="false" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="Director_Zip" runat="server" FieldLabel="Zip Code" AnchorHorizontal="90%" MaxLength="10"></ext:TextField>
                    <%--<ext:ComboBox ID="Director_cmb_kodenegara" runat="server" FieldLabel="Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreDirFromForeignCountry_CodeAddress" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model26">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel12" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="Director_cmb_kodenegara" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="90%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <ext:TextField ID="Director_State" runat="server" FieldLabel="Province" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="Director_Comment_Address" runat="server" FieldLabel="Notes" AnchorHorizontal="90%" MaxLength="4000"></ext:TextField>
                </Items>
                <Buttons>
                    <ext:Button ID="Btn_Save_Director_Address" runat="server" Text="Save" DefaultAlign="center">
                        <Listeners>
                            <Click Handler="if (!#{FP_Address_Director}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="btnSaveDirectorAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Btn_Back_Director_Address" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveDirectorAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="FP_Address_Emp_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:ComboBox ID="Director_cmb_emp_kategoriaddress" AllowBlank="false" runat="server" FieldLabel="Address Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreEmpDirKategoryADdressAddress_Type" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model27">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="Director_emp_Address" runat="server" FieldLabel="Address" AnchorHorizontal="90%" AllowBlank="false" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="Director_emp_Town" runat="server" FieldLabel="Town" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="Director_emp_City" runat="server" FieldLabel="Kota" AnchorHorizontal="90%" AllowBlank="false" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="Director_emp_Zip" runat="server" FieldLabel="Zip Code" AnchorHorizontal="90%" MaxLength="10"></ext:TextField>
                    <%--<ext:ComboBox ID="Director_cmb_emp_kodenegara" runat="server" FieldLabel="Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreEmpDirFromForeignCountry_CodeAddress" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server" ID="Model28">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>    
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel15" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="Director_cmb_emp_kodenegara"  ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="90%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <ext:TextField ID="Director_emp_State" runat="server" FieldLabel="Province" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="Director_emp_Comment_Address" runat="server" FieldLabel="Notes" AnchorHorizontal="90%" MaxLength="4000"></ext:TextField>
                </Items>
                <Buttons>
                    <ext:Button ID="btn_Director_saveempaddress" runat="server" Text="Save" DefaultAlign="center">
                        <Listeners>
                            <Click Handler="if (!#{FP_Address_Emp_Director}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="btnSaveDirectorEmpAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button15" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveDirectorEmpAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailPhone" runat="server" Title="Phone" Hidden="true" Modal="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>

            <ext:FormPanel ID="PanelDetailPhone" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="Dsptph_contact_type" runat="server" FieldLabel="Contact Type"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_communication_type" runat="server" FieldLabel="Communication Type"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_country_prefix" runat="server" FieldLabel="Prefix"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_number" runat="server" FieldLabel="Phone Number"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_extension" runat="server" FieldLabel="Extention"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsPhone" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnCancelPhone" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>

            <ext:FormPanel ID="PanelDetailPhoneEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="Dsptph_contact_typeEmployer" runat="server" FieldLabel="Contact Type"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_communication_typeEmployer" runat="server" FieldLabel="Communication Type"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_country_prefixEmployer" runat="server" FieldLabel="Prefix"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_numberEmployer" runat="server" FieldLabel="Phone Number"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_extensionEmployer" runat="server" FieldLabel="Extention"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsPhoneEmployer" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button16" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelPhoneEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>

            <ext:FormPanel ID="FormPanelPhoneDetail" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:ComboBox ID="Cmbtph_contact_type" runat="server" FieldLabel="Contact Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignContact_Type" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:ComboBox ID="Cmbtph_communication_type" runat="server" FieldLabel="Communication Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignCommunication_Type" OnReadData="Communication_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="Txttph_country_prefix" runat="server" FieldLabel="Prefix" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="4"></ext:TextField>
                    <ext:TextField ID="Txttph_number" runat="server" AllowBlank="false" FieldLabel="Phone Number" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="50"></ext:TextField>
                    <ext:TextField ID="Txttph_extension" runat="server" FieldLabel="Extention" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="10"></ext:TextField>
                    <ext:TextArea ID="TxtcommentsPhone" runat="server" FieldLabel="Notes" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnSaveDetailPhone" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelPhoneDetail}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="BtnSaveDetailPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnCancelDetailPhone" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDetailPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                </Buttons>
            </ext:FormPanel>

            <ext:FormPanel ID="FormPanelPhoneEmployerDetail" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:ComboBox ID="Cmbtph_contact_typeEmployer" runat="server" FieldLabel="Contact Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignContact_TypeEmployer" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:ComboBox ID="Cmbtph_communication_typeEmployer" runat="server" FieldLabel="Communication Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignCommunication_TypeEmployer" OnReadData="Communication_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="Txttph_country_prefixEmployer" runat="server" FieldLabel="Prefix" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="4"></ext:TextField>
                    <ext:TextField ID="Txttph_numberEmployer" runat="server" AllowBlank="false" FieldLabel="Phone Number" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="50"></ext:TextField>
                    <ext:TextField ID="Txttph_extensionEmployer" runat="server" FieldLabel="Extention" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="10"></ext:TextField>
                    <ext:TextArea ID="TxtcommentsPhoneEmployer" runat="server" FieldLabel="Notes" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="Button17" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelPhoneEmployerDetail}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="BtnSaveDetailPhoneEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button18" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDetailPhoneEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>

                </Buttons>
            </ext:FormPanel>

        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailPhone}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailAddress" runat="server" Icon="ApplicationViewDetail" Title="Address" Hidden="true" Layout="FitLayout" Modal="true"  MinWidth="800" Height="300" >
        <Items>

            <ext:FormPanel ID="PanelDetailAddress" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DspAddress_type" runat="server" FieldLabel="Address Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspAddress" runat="server" FieldLabel="Address"></ext:DisplayField>
                    <ext:DisplayField ID="DspTown" runat="server" FieldLabel="Town"></ext:DisplayField>
                    <ext:DisplayField ID="DspCity" runat="server" FieldLabel="City"></ext:DisplayField>
                    <ext:DisplayField ID="DspZip" runat="server" FieldLabel="Zip Code"></ext:DisplayField>
                    <ext:DisplayField ID="Dspcountry_code" runat="server" FieldLabel="Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspState" runat="server" FieldLabel="Province"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsAddress" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnCancelAddress" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>

            <ext:FormPanel ID="PanelDetailAddressEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:DisplayField ID="DspAddress_typeEmployer" runat="server" FieldLabel="Address Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspAddressEmployer" runat="server" FieldLabel="Address"></ext:DisplayField>
                    <ext:DisplayField ID="DspTownEmployer" runat="server" FieldLabel="Town"></ext:DisplayField>
                    <ext:DisplayField ID="DspCityEmployer" runat="server" FieldLabel="City"></ext:DisplayField>
                    <ext:DisplayField ID="DspZipEmployer" runat="server" FieldLabel="Zip Code"></ext:DisplayField>
                    <ext:DisplayField ID="Dspcountry_codeEmployer" runat="server" FieldLabel="Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspStateEmployer" runat="server" FieldLabel="Province"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsAddressEmployer" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button19" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelAddressEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>

            <ext:FormPanel ID="FormPanelAddressDetail" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:ComboBox ID="CmbAddress_type" runat="server" FieldLabel="Address Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignAddress_Type" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy></ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents></DirectEvents>
                    </ext:ComboBox>
                    <ext:TextField ID="TxtAddress" runat="server" AllowBlank="false" FieldLabel="Address" AnchorHorizontal="80%" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="TxtTown" runat="server" FieldLabel="Town" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="TxtCity" runat="server" AllowBlank="false" FieldLabel="City" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="TxtZip" runat="server" FieldLabel="Zip Code" AnchorHorizontal="80%" MaxLength="10"></ext:TextField>
                   <%-- <ext:ComboBox ID="Cmbcountry_code" runat="server" FieldLabel="Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignCountry_CodeAddress" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy></ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents></DirectEvents>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel13" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="Cmbcountry_code" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="80%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <ext:TextField ID="TxtState" runat="server" FieldLabel="Province" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextArea ID="TxtcommentsAddress" runat="server" FieldLabel="Notes" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnSaveDetailAddress" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelAddressDetail}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="BtnSaveDetailAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="BtnCancelDetailAddress" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDetailAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>

            <ext:FormPanel ID="FormPanelAddressDetailEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true" ButtonAlign="Center">
                <Items>
                    <ext:ComboBox ID="CmbAddress_typeEmployer" runat="server" FieldLabel="Address Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignAddress_TypeEmployer" OnReadData="Contact_Type_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy></ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents></DirectEvents>
                    </ext:ComboBox>
                    <ext:TextField ID="TxtAddressEmployer" runat="server" AllowBlank="false" FieldLabel="Address" AnchorHorizontal="80%" MaxLength="100"></ext:TextField>
                    <ext:TextField ID="TxtTownEmployer" runat="server" FieldLabel="Town" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="TxtCityEmployer" runat="server" AllowBlank="false" FieldLabel="City" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextField ID="TxtZipEmployer" runat="server" FieldLabel="Zip Code" AnchorHorizontal="80%" MaxLength="10"></ext:TextField>
                   <%-- <ext:ComboBox ID="Cmbcountry_codeEmployer" runat="server" FieldLabel="Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignCountry_CodeAddressEmployer" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy></ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents></DirectEvents>
                    </ext:ComboBox>--%>
                    <ext:Panel ID="Panel14" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="Cmbcountry_codeEmployer"  ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="80%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <ext:TextField ID="TxtStateEmployer" runat="server" FieldLabel="Province" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextArea ID="TxtcommentsAddressEmployer" runat="server" FieldLabel="Notes" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button ID="Button20" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                            <Click Handler="if (!#{FormPanelAddressDetailEmployer}.getForm().isValid()) return false;"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="BtnSaveDetailAddressEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="Button21" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDetailAddressEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>

        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailAddress}.center()" />
        </Listeners>
    </ext:Window>

    <ext:FormPanel runat="server" Title="WIC Add Form" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="WIC_Panel">
        <Items>
            <ext:DisplayField ID="PKWIC" runat="server" Text="AutoNumber" FieldLabel="ID"></ext:DisplayField>
            <ext:ComboBox runat="server" ID="cboFormWIC" EmptyText="[Select Form]" FieldLabel="WIC Type" AutoPostBack="true" OnItemSelected="cboFormWIC_ItemSelected">
                <Items>
                    <ext:ListItem Text="Individu" Value="Individu"></ext:ListItem>
                    <ext:ListItem Text="Corporate" Value="Corporate"></ext:ListItem>
                </Items>
            </ext:ComboBox>
            <ext:TextField ID="TxtWIC_No" runat="server" FieldLabel="WIC No" AnchorHorizontal="80%" AllowBlank="false"></ext:TextField>
            <ext:Checkbox ID="CheckboxIsUpdateFromDataSource" runat="server" FieldLabel="Update From Data Source ?" AnchorHorizontal="80%"></ext:Checkbox>
            <ext:Checkbox ID="Cbx_IsRealWIC" runat="server" FieldLabel="Is Real WIC ?" AnchorHorizontal="80%"></ext:Checkbox>
            <ext:FormPanel runat="server" Title="WIC Add Individu" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" ID="WIC_Individu" Border="true" BodyStyle="padding:10px">
                <Defaults>
                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                </Defaults>
                <Items>
                    <ext:ComboBox ID="CmbINDV_Gender" runat="server" FieldLabel="Gender" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" LabelWidth="175">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreGenderINDV" OnReadData="Gender_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>
                    <ext:TextField ID="TxtINDV_Title" runat="server" FieldLabel="Title" AnchorHorizontal="80%" MaxLength="30" LabelWidth="175"></ext:TextField>
                    <ext:TextField ID="TxtINDV_last_name" runat="server" FieldLabel="Full Name" AnchorHorizontal="80%" AllowBlank="false" MaxLength="100" LabelWidth="175"></ext:TextField>
                    <ext:DateField ID="DateINDV_Birthdate" runat="server" Flex="1" FieldLabel="Birth Of Date" LabelWidth="175">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                        this.clearValue();
                        this.getTrigger(0).hide();}" />
                        </Listeners>
                    </ext:DateField>
                    <ext:TextField ID="TxtINDV_Birth_place" runat="server" FieldLabel="Birth Of Place" AnchorHorizontal="80%" MaxLength="255" LabelWidth="175"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Mothers_name" runat="server" FieldLabel="Mother Maiden Name" AnchorHorizontal="80%" MaxLength="100" LabelWidth="175"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Alias" runat="server" FieldLabel="Alias Name" AnchorHorizontal="80%" MaxLength="100" LabelWidth="175"></ext:TextField>
                    <ext:TextField ID="TxtINDV_SSN" runat="server" FieldLabel="NIK/SSN" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="16" MinLength="16" LabelWidth="175">
                    </ext:TextField>
                    <ext:TextField ID="TxtINDV_Passport_number" runat="server" FieldLabel="Passport No" AnchorHorizontal="80%" MaxLength="255" LabelWidth="175"></ext:TextField>
                    <ext:Panel ID="Pnl_TxtINDV_Passport_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="TxtINDV_Passport_country" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Passport Issued Country" AnchorHorizontal="80%" LabelWidth="175"/>
                        </Content>
                    </ext:Panel>
                    <ext:TextField ID="TxtINDV_ID_Number" runat="server" FieldLabel="Other ID Number" AnchorHorizontal="80%" MaxLength="255" LabelWidth="175"></ext:TextField>
                    <ext:Panel ID="Panel1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbINDV_Nationality1" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Nationality 1" AnchorHorizontal="80%" LabelWidth="175"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel2" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbINDV_Nationality2" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Nationality 2" AnchorHorizontal="80%" LabelWidth="175"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbINDV_Nationality3" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Nationality 3" AnchorHorizontal="80%" LabelWidth="175"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel4" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbINDV_residence" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Domicile Country" AnchorHorizontal="80%" LabelWidth="175"/>
                        </Content>
                    </ext:Panel>
                    <ext:Checkbox ID="Cbx_IsProtected" runat="server" FieldLabel="Is Protected ?" AnchorHorizontal="80%"></ext:Checkbox>
                    <%--<ext:ComboBox ID="CmbINDV_Nationality1" AllowBlank="false" runat="server" FieldLabel="Kewarganegaraan 1" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignNational1" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>--%>
                    <%-- <ext:ComboBox ID="CmbINDV_Nationality2" runat="server" FieldLabel="Kewarganegaraan 2" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignNational2" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>
                    <ext:ComboBox ID="CmbINDV_Nationality3" runat="server" FieldLabel="Kewarganegaraan 3" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignNational3" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>
                    <ext:ComboBox ID="CmbINDV_residence" AllowBlank="false" runat="server" FieldLabel="Negara Domisili" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignResisdance" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>--%>

                    <ext:GridPanel ID="GridPanelEmail" runat="server" Title="Email" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="ButtonEmail" Text="Add New Record" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Email_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreEmail" runat="server">
                                <Model>
                                    <ext:Model ID="ModelEmail" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Email_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="email_address" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumnEmail" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumnActionEmail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                            <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdEmail">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Email_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="ColumnEmail_Address" runat="server" DataIndex="email_address" Text="Email" MinWidth="130" Flex="1"></ext:Column>
                                
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar21" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelPersonPEP" runat="server" Title="Person PEP" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="ButtonPersonPEP" Text="Add New Record" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Person_PEP_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StorePersonPEP" runat="server">
                                <Model>
                                    <ext:Model ID="ModelPersonPEP" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_PEP_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="pep_country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="function_name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="function_description" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="pep_date_range_valid_from" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="pep_date_range_is_approx_from_date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="pep_date_range_valid_to" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="pep_date_range_is_approx_to_date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumnPersonPEP" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumnPersonPEP" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                            <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdPersonPEP">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_PEP_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="ColumnPep_country" runat="server" DataIndex="pep_country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnFunction_name" runat="server" DataIndex="function_name" Text="Function Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnFunction_description" runat="server" DataIndex="function_description" Text="Description" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="ColumnPep_date_range_valid_from" runat="server" DataIndex="pep_date_range_valid_from" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnPep_date_range_is_approx_from_date" runat="server" DataIndex="pep_date_range_is_approx_from_date" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnPep_date_range_valid_to" runat="server" DataIndex="pep_date_range_valid_to" Text="Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnPep_date_range_is_approx_to_date" runat="server" DataIndex="pep_date_range_is_approx_to_date" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="ColumnPersonPEPComments" runat="server" DataIndex="comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                          
                                
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar23" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelSanction" runat="server" Title="Sanction" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="ButtonSanction" Text="Add New Record" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Sanction_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreSanction" runat="server">
                                <Model>
                                    <ext:Model ID="ModelSanction" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Sanction_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Provider" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Match_Criteria" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Link_To_Source" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Attributes" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_From" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_From_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_To" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_To_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Last_Seen_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumnSanction" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumnSanction" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                            <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdSanction"> 
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="ColumnProvider" runat="server" DataIndex="Provider" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSanction_List_Name" runat="server" DataIndex="Sanction_List_Name" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnMatch_Criteria" runat="server" DataIndex="Match_Criteria" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnLink_To_Source" runat="server" DataIndex="Link_To_Source" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSanction_List_Attributes" runat="server" DataIndex="Sanction_List_Attributes" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="ColumnSanction_List_Date_Range_Valid_From" runat="server" DataIndex="Sanction_List_Date_Range_Valid_From" Text="Sanction List Date Range Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSanction_List_Date_Range_Is_Approx_From_Date" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_From_Date" Text="Sanction List Date Range Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSanction_List_Date_Range_Valid_To" runat="server" DataIndex="Sanction_List_Date_Range_Valid_To" Text="Sanction List Date Range Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSanction_List_Date_Range_Is_Approx_To_Date" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_To_Date" Text="Sanction List Date Range Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="ColumnCommentsSanction" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
         
                                
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar25" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelRelatedPerson" runat="server" Title="Related Person" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="ButtonRelatedPerson" Text="Add New Record" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Related_Person_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreRelatedPerson" runat="server">
                                <Model>
                                    <ext:Model ID="ModelRelatedPerson" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Related_Person_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Person_Person_Relation" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Valid_From" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Is_Approx_From_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Valid_To" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Is_Approx_To_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Gender" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Title" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="First_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Middle_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Birth_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country_Of_Birth" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Mother_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Alias" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Full_Name_Frn" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Passport_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Passport_Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nationality1" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nationality2" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nationality3" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Residence" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Residence_Since" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Occupation" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Deceased" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Date_Deceased" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tax_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Is_PEP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Source_Of_Wealth" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Is_Protected" Type="String"></ext:ModelField>
                                            
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumnRelatedPerson" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumnRelatedPersonAction" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdRelatedPerson">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <%-- 2023-10-24, Nael: "Person Person Relation" to "Person Relation" --%>
                                <ext:Column ID="ColumnPerson_Person_Relation" runat="server" DataIndex="Person_Person_Relation" Text="Person Relation" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="ColumnCommentsRelatedPerson" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnGender" runat="server" DataIndex="Gender" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnTitle" runat="server" DataIndex="Title" Text="Title" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnFirst_NameRelatedPerson" runat="server" DataIndex="First_Name" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnMiddle_Name" runat="server" DataIndex="Middle_Name" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnPrefix" runat="server" DataIndex="Prefix" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="ColumnLast_NameRelatedPerson" runat="server" DataIndex="Last_Name" Text="Full Name" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="ColumnBirth_Date" runat="server" DataIndex="Birth_Date" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnBirth_Place" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnCountry_Of_Birth" runat="server" DataIndex="Country_Of_Birth" Text="Country Of Birth" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnMother_Name" runat="server" DataIndex="Mother_Name" Text="Mother Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnAlias" runat="server" DataIndex="Alias" Text="Alias" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSSN" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnPassport_Number" runat="server" DataIndex="Passport_Number" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnPassport_Country" runat="server" DataIndex="Passport_Country" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnID_Number" runat="server" DataIndex="ID_Number" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="ColumnNationality1" Hidden="true" runat="server" DataIndex="Nationality1" Text="Nationality 1" MinWidth="130" Flex="1"></ext:Column>
                               <%-- <ext:Column ID="ColumnNationality2" runat="server" DataIndex="Nationality2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnNationality3" runat="server" DataIndex="Nationality3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnResidence" runat="server" DataIndex="Residence" Text="Residence" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnOccupation" runat="server" DataIndex="Occupation" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnDeceased" runat="server" DataIndex="Deceased" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnDate_Deceased" runat="server" DataIndex="Date_Deceased" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnTax_Number" runat="server" DataIndex="Tax_Number" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnIs_PEP" runat="server" DataIndex="Is_PEP" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSource_Of_Wealth" runat="server" DataIndex="Source_Of_Wealth" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnIs_Protected" runat="server" DataIndex="Is_Protected" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>--%>
                                
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar26" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    
                    <ext:GridPanel ID="GridPanelPhoneIndividuDetail" runat="server" Title="Phone" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar" runat="server">
                                <Items>
                                    <ext:Button
                                        ID="BtnAddNewPhoneIndividu" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddNewPhoneIndividu_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StorePhoneIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="ModelDetail" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumberPhoneDetailIndividu" Text="No" runat="server" Width="60"></ext:RowNumbererColumn>
                                <ext:Column ID="colTph_Contact_TypeIndividu" runat="server" DataIndex="Contact_Type" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="coltph_communication_typeIndividu" runat="server" DataIndex="Communication_Type" Text="Communication Type" Flex="1"></ext:Column>
                                <ext:Column ID="coltph_numberIndividu" runat="server" DataIndex="number" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhoneIndividuDetail" runat="server" Text="Action" Flex="1">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhoneIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Download' || command=='Edit' || command=='MoveUp' || command=='MoveDown' || command=='Detail') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar15" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelAddressIndividuDetail" runat="server" Title="Address" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="BtnAddNewAddressIndividu" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddNewAddressIndividu_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreAddressIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailIndividu" Text="No" runat="server" Width="60"></ext:RowNumbererColumn>
                                <ext:Column ID="ColAddress_typeIndividu" runat="server" DataIndex="Type_Address" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="ColAddressIndividu" runat="server" DataIndex="Addres" Text="Address" Flex="1"></ext:Column>
                                <ext:Column ID="ColCityIndividu" runat="server" DataIndex="Cty" Text="City" Flex="1"></ext:Column>
                                <ext:Column ID="Colcountry_codeIndividu" runat="server" DataIndex="CountryCode" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAddressIndividuDetail" runat="server" Text="Action" Flex="1">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddressIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Download' || command=='Edit' || command=='MoveUp' || command=='MoveDown' || command=='Detail') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <%--<ext:TextField ID="TxtINDV_Email" runat="server" FieldLabel="Email" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email" LabelWidth="175"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Email2" runat="server" FieldLabel="Email2" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email2" LabelWidth="175"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Email3" runat="server" FieldLabel="Email3" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email" LabelWidth="175"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Email4" runat="server" FieldLabel="Email4" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email" LabelWidth="175"></ext:TextField>
                    <ext:TextField ID="TxtINDV_Email5" runat="server" FieldLabel="Email5" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email" LabelWidth="175"></ext:TextField>--%>
                    <ext:TextField ID="TxtINDV_Occupation" runat="server" FieldLabel="Occupation" AnchorHorizontal="80%" MaxLength="255" LabelWidth="175"></ext:TextField>
                    <ext:TextField ID="TxtINDV_employer_name" runat="server" FieldLabel="Work Place" AnchorHorizontal="80%" MaxLength="255" LabelWidth="175"></ext:TextField>

                    <ext:GridPanel ID="GridPanelPhoneIndividualEmployerDetail" runat="server" Title="Work Place Phone" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar5" runat="server">
                                <Items>
                                    <ext:Button
                                        ID="Button12" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddNewPhoneEmployerIndividu_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StorePhoneEmployerIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model20" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn12" Text="No" runat="server" Width="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column27" runat="server" DataIndex="Contact_Type" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column28" runat="server" DataIndex="Communication_Type" Text="Communication Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column29" runat="server" DataIndex="number" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhoneIndividualEmployerDetail" runat="server" Text="Action" Flex="1">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhoneEmployerIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Download' || command=='Edit' || command=='MoveUp' || command=='MoveDown' || command=='Detail') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar13" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelAddressIndividualEmployerDetail" runat="server" Title="Work Place Address" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar6" runat="server">
                                <Items>
                                    <ext:Button ID="Button14" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddNewAddressEmployerIndividu_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreAddressEmployerIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model29" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn13" Text="No" runat="server" Width="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column30" runat="server" DataIndex="Type_Address" Text="Address Type" Flex="1" ></ext:Column>
                                <ext:Column ID="Column33" runat="server" DataIndex="Addres" Text="Address" Flex="1" ></ext:Column>
                                <ext:Column ID="Column34" runat="server" DataIndex="Cty" Text="City" Flex="1" ></ext:Column>
                                <ext:Column ID="Column35" runat="server" DataIndex="CountryCode" Text="Country" Flex="1" ></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAddressIndividualEmployerDetail" runat="server" Text="Action" Flex="1">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddressEmployerIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Download' || command=='Edit' || command=='MoveUp' || command=='MoveDown' || command=='Detail') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelIdentificationIndividualDetail" runat="server" Title="Identification" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar12" runat="server">
                                <Items>
                                    <ext:Button ID="Button25" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddIdentification_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreIdentificationDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model30" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn14" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column36" runat="server" DataIndex="Tipe" Text="Identity Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column37" runat="server" DataIndex="No" Text="Identity Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column38" runat="server" DataIndex="IssueDate" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column39" runat="server" DataIndex="ExpiryDate" Text="Expired Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column40" runat="server" DataIndex="IssuedCountry" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnIdentificationIndividualDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Download' || command=='Edit' || command=='MoveUp' || command=='MoveDown' || command=='Detail') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:Checkbox ID="CbINDV_Deceased" runat="server" FieldLabel="Has Passed Awaly?" AnchorHorizontal="80%" LabelWidth="175">
                        <DirectEvents>
                            <Change OnEvent="checkBoxcb_INDV_Deceased_click" />
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField ID="DateINDV_Deceased_Date" runat="server" Flex="1" FieldLabel="Date of Passed Away" Hidden="true" LabelWidth="175">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                        this.clearValue();
                        this.getTrigger(0).hide();}" />
                        </Listeners>
                    </ext:DateField>
                    <ext:TextField ID="TxtINDV_Tax_Number" runat="server" FieldLabel="Tax Number" AnchorHorizontal="80%" EnableKeyEvents="true" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="15" MinLength="15" LabelWidth="175"></ext:TextField>
                    <ext:Checkbox ID="CbINDV_Tax_Reg_Number" runat="server" FieldLabel="is PEP ?" AnchorHorizontal="80%" LabelWidth="175"></ext:Checkbox>
                    <ext:TextField ID="TxtINDV_Source_Of_Wealth" runat="server" FieldLabel="Source of Wealth" AnchorHorizontal="80%" MaxLength="255" LabelWidth="175"></ext:TextField>
                    <ext:TextArea ID="TxtINDV_Comments" runat="server" FieldLabel="Notes" AnchorHorizontal="80%" MaxLength="4000" LabelWidth="175"></ext:TextArea>

                </Items>
            </ext:FormPanel>

            <ext:FormPanel runat="server" Title="WIC Add Corporate" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" ID="WIC_Corporate" Border="true" BodyStyle="padding:10px">
                <Defaults>
                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                </Defaults>
                <Items>
                    <ext:TextField ID="TxtCorp_Name" runat="server" FieldLabel="Corporate Name" AnchorHorizontal="80%" AllowBlank="false" MaxLength="255" LabelWidth="175"></ext:TextField>
                    <ext:TextField ID="TxtCorp_Commercial_name" runat="server" FieldLabel="Commercial Name" AnchorHorizontal="80%" MaxLength="255" LabelWidth="175"></ext:TextField>
                    <ext:ComboBox ID="CmbCorp_Incorporation_legal_form" runat="server" FieldLabel="Legal Form" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" LabelWidth="175">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignIncorporation_legal" OnReadData="Incorporation_legal_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <ext:TextField ID="TxtCorp_Incorporation_number" runat="server" FieldLabel="Corporate License Number" AnchorHorizontal="80%" MaxLength="50" LabelWidth="175"></ext:TextField>
                    <ext:TextField ID="TxtCorp_Business" runat="server" FieldLabel="Line Of Bussiness" AnchorHorizontal="80%" MaxLength="255" LabelWidth="175"></ext:TextField>

                    <ext:GridPanel ID="GridPanelPhoneCorporateDetail" runat="server" Title="Phone" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar2" runat="server">
                                <Items>
                                    <ext:Button ID="BtnAddNewPhoneCorporate" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddNewPhoneCorporate_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StorePhoneCorporateDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererPhoneDetailCorporate" Text="No" runat="server" Width="60"></ext:RowNumbererColumn>
                                <ext:Column ID="colTph_Contact_TypeCorporate" runat="server" DataIndex="Contact_Type" Text="Contact Type" Flex="1" ></ext:Column>
                                <ext:Column ID="coltph_communication_typeCorporate" runat="server" DataIndex="Communication_Type" Text="Communication Type" Flex="1" ></ext:Column>
                                <ext:Column ID="coltph_numberCorporate" runat="server" DataIndex="number" Text="Phone Number" Flex="1" ></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhoneCorporateDetail" runat="server" Text="Action" Flex="1" >
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhoneCorporate">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Download' || command=='Edit' || command=='MoveUp' || command=='MoveDown' || command=='Detail') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar16" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelAddressCorporateDetail" runat="server" Title="Address" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar3" runat="server">
                                <Items>
                                    <ext:Button ID="BtnAddNewAddressCorporate" runat="server" Text="Add New Detail" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddNewAddressCorporate_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreAddressCorporateDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailCorporate" Text="No" runat="server" Width="60"></ext:RowNumbererColumn>
                                <ext:Column ID="ColAddress_typeCorporate" runat="server" DataIndex="Type_Address" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="ColAddressCorporate" runat="server" DataIndex="Addres" Text="Address" Flex="1"></ext:Column>
                                <ext:Column ID="ColCityCorporate" runat="server" DataIndex="Cty" Text="City" Flex="1"></ext:Column>
                                <ext:Column ID="Colcountry_codeCorporate" runat="server" DataIndex="CountryCode" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAddressCorporateDetail" runat="server" Text="Action" Flex="1">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Table" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddressCorporate">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Download' || command=='Edit' || command=='MoveUp' || command=='MoveDown' || command =='Detail') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar17" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                   <%-- <ext:TextField ID="TxtCorp_Email" runat="server" FieldLabel="Email Korporasi" AnchorHorizontal="80%" MaxLength="255" Regex="^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$" RegexText="Invalid Email" LabelWidth="175"></ext:TextField>
                    <ext:TextField ID="TxtCorp_url" runat="server" FieldLabel="Website Korporasi" AnchorHorizontal="80%" MaxLength="255" LabelWidth="175"></ext:TextField>--%>
                    <ext:TextField ID="TxtCorp_incorporation_state" runat="server" FieldLabel="Province" AnchorHorizontal="80%" MaxLength="255" LabelWidth="175"></ext:TextField>
                    <ext:Panel ID="PanelNegara" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField ID="CmbCorp_incorporation_country_code" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="80%" LabelWidth="175"/>
                        </Content>
                    </ext:Panel>
                    <%-- <ext:ComboBox ID="CmbCorp_incorporation_country_code" runat="server" FieldLabel="Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreFromForeignCountry_Code" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                        </DirectEvents>
                    </ext:ComboBox>--%>

                    <%-- Add by Septian, goAML 5.0.1, 28 Februari 2023 --%>
                    <ext:GridPanel ID="GPWIC_CORP_EntityIdentification" runat="server" Title="Entity Identification" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="tbr_ent_identify" runat="server">
                                <Items>
                                    <ext:Button ID="btn_add_ent_identify" runat="server" Text="Add Entity Identification" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_WIC_EntityIdentification_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_WIC_Entity_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_ent_identify" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Entity_Identifications_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TYPE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUE_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EXPIRY_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUED_BY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUE_COUNTRY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_identify" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:CommandColumn ID="CC_CORP_EntityIdentification" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdWIC_EntityIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Entity_Identifications_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="clm_identify_type" runat="server" DataIndex="TYPE" Text="Entity Identifier Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_number" runat="server" DataIndex="NUMBER" Text="Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_issued_date" runat="server" DataIndex="ISSUE_DATE" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_expiry_date" runat="server" DataIndex="EXPIRY_DATE" Text="Expiry Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_issued_by" runat="server" DataIndex="ISSUED_BY" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_issued_country" runat="server" DataIndex="ISSUE_COUNTRY" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_identify" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GPWIC_Corp_Email" runat="server" Title="Email" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="tbr_email" runat="server">
                                <Items>
                                    <ext:Button ID="btn_add_email" runat="server" Text="Add Email" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_WIC_Email_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_WIC_Email" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="model_email" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Email_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="email_address" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_indv_email" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_WIC_Previous_Name_ID" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:CommandColumn ID="CC_INDV_Email" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdWIC_Email">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Email_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="clm_indv_email_address" runat="server" DataIndex="email_address" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_indv_email" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GPWIC_CORP_URL" runat="server" Title="Entity URL" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="tbr_entity_url" runat="server">
                                <Items>
                                    <ext:Button ID="btn_add_entity_url" runat="server" Text="Add Entity URL" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_WIC_Url_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_WIC_Entity_URL" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_entity_url" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_URL_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="URL" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_entity_url" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:CommandColumn ID="CC_CORP_URL" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdWIC_URL">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_URL_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="clm_url_type" runat="server" DataIndex="URL" Text="URL" MinWidth="130" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_entity_url" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End Add --%>

                    <ext:GridPanel ID="GP_Corp_Director" runat="server" Title="Director Corp" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar ID="toolbar7" runat="server">
                                <Items>
                                    <ext:Button ID="Button7" runat="server" Text="Add Director" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddDirector_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Director_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_Director_Corp" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="NO_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Roles" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column31" runat="server" DataIndex="LastName" Text="Full Name" Flex="1"></ext:Column>
                                <ext:Column ID="Column32" runat="server" DataIndex="Roles" Text="Role" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnDirector" runat="server" Text="Action" Flex="1" >
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.NO_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar18" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <%-- Add BY Septian, goAML 5.0.1, 2023-02-28 %>
                     <%-- Corp Sanction --%>
                    <ext:GridPanel ID="GPWIC_CORP_Sanction" runat="server" Title="Sanction" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="Button26" Text="Add New Record" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Sanction_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model11" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Sanction_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Provider" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Match_Criteria" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Link_To_Source" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Attributes" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_From" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_From_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_To" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_To_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Last_Seen_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn15" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumn7" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                            <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdSanction"> 
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column5" runat="server" DataIndex="Provider" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="Sanction_List_Name" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column41" runat="server" DataIndex="Match_Criteria" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column42" runat="server" DataIndex="Link_To_Source" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column43" runat="server" DataIndex="Sanction_List_Attributes" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="Column44" runat="server" DataIndex="Sanction_List_Date_Range_Valid_From" Text="Sanction List Date Range Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column45" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_From_Date" Text="Sanction List Date Range Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column46" runat="server" DataIndex="Sanction_List_Date_Range_Valid_To" Text="Sanction List Date Range Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column49" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_To_Date" Text="Sanction List Date Range Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column51" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
         
                                
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar28" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- end of INDV Sanction --%>

                    <%-- INDV Related Person --%>
                    <ext:GridPanel ID="GPWIC_CORP_RelatedPerson" Hidden="true" runat="server" Title="Related Person" AutoScroll="true" EmptyText="No Available Data">
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="Button27" Text="Add New Record" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Related_Person_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model12" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Related_Person_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Person_Person_Relation" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Valid_From" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Is_Approx_From_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Valid_To" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Is_Approx_To_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Gender" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Title" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="First_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Middle_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Birth_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country_Of_Birth" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Mother_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Alias" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Full_Name_Frn" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Passport_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Passport_Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nationality1" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nationality2" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nationality3" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Residence" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Residence_Since" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Occupation" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Deceased" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Date_Deceased" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tax_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Is_PEP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Source_Of_Wealth" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Is_Protected" Type="String"></ext:ModelField>
                                            
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn16" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column52" runat="server" DataIndex="Person_Person_Relation" Text="Person Person Relation" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column63" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column65" runat="server" DataIndex="Gender" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column66" runat="server" DataIndex="Title" Text="Title" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column69" runat="server" DataIndex="First_Name" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column71" runat="server" DataIndex="Middle_Name" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column73" runat="server" DataIndex="Prefix" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column74" runat="server" DataIndex="Last_Name" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column75" runat="server" DataIndex="Birth_Date" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column76" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column77" runat="server" DataIndex="Country_Of_Birth" Text="Country Of Birth" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column78" runat="server" DataIndex="Mother_Name" Text="Mother Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column79" runat="server" DataIndex="Alias" Text="Alias" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column81" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column82" runat="server" DataIndex="Passport_Number" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column83" runat="server" DataIndex="Passport_Country" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column84" runat="server" DataIndex="ID_Number" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column85" runat="server" DataIndex="Nationality1" Text="Nationality 1" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column86" runat="server" DataIndex="Nationality2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column87" runat="server" DataIndex="Nationality3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column88" runat="server" DataIndex="Residence" Text="Residence" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column96" runat="server" DataIndex="Occupation" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column104" runat="server" DataIndex="Deceased" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column105" runat="server" DataIndex="Date_Deceased" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column106" runat="server" DataIndex="Tax_Number" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column107" runat="server" DataIndex="Is_PEP" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column108" runat="server" DataIndex="Source_Of_Wealth" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column109" runat="server" DataIndex="Is_Protected" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn8" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                            <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdRelatedPerson">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar29" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- end of INDV Related Person --%>

                    <%-- Related Entities --%>
                    <ext:GridPanel ID="GPWIC_CORP_RelatedEntities" runat="server" Title="Related Entities" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="tbr_corp_related_entities" runat="server">
                                <Items>
                                    <ext:Button ID="btn_corp_re_add_email" runat="server" Text="Add Related Entities" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_WIC_RelatedEntities_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Related_Entities" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_related_entities" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Related_Entities_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ENTITY_ENTITY_RELATION" Type="String"></ext:ModelField>
                                       <%--     <ext:ModelField Name="RELATION_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RELATION_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RELATION_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RELATION_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SHARE_PERCENTAGE" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMERCIAL_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INCORPORATION_LEGAL_FORM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INCORPORATION_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="BUSINESS" Type="String"></ext:ModelField>
                                      <%--      <ext:ModelField Name="ENTITY_STATUS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ENTITY_STATUS_DATE" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="INCORPORATION_STATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INCORPORATION_COUNTRY_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INCORPORATION_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DATE_BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_related_etities" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CC_CORP_RelatedEntities" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdWIC_RelatedEntities">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Related_Entities_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_WIC_Previous_Name_ID" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <%-- 2023-10-24, Nael: "Entity Entity Relation" to "Entity Relation" --%>
                                <ext:Column ID="clm_re_email_address" runat="server" DataIndex="ENTITY_ENTITY_RELATION" Text="Entity Relation" MinWidth="130" Flex="1"></ext:Column>
                             <%--   <ext:Column ID="Column84" runat="server" DataIndex="RELATION_DATE_RANGE_VALID_FROM" Text="Relation Date Range - Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column85" runat="server" DataIndex="RELATION_DATE_RANGE_IS_APPROX_FROM_DATE" Text="Relation Date Range - Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column86" runat="server" DataIndex="RELATION_DATE_RANGE_VALID_TO" Text="Relation Date Range - Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column87" runat="server" DataIndex="RELATION_DATE_RANGE_IS_APPROX_TO_DATE" Text="Relation Date Range - Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column88" runat="server" DataIndex="SHARE_PERCENTAGE" Text="Share Percentage" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column89" runat="server" DataIndex="COMMENTS" Hidden="true" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column90" runat="server" DataIndex="NAME" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column91" runat="server" DataIndex="COMMERCIAL_NAME" Hidden="true" Text="Commercial Name" MinWidth="130" Flex="1"></ext:Column>
                                <%-- 2023-10-24, Nael --%>
                                <ext:Column ID="Column92" runat="server" DataIndex="INCORPORATION_LEGAL_FORM" Hidden="true" Text="Legal Form" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column93" runat="server" DataIndex="INCORPORATION_NUMBER" Hidden="true" Text="License Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column94" runat="server" DataIndex="BUSINESS" Hidden="true" Text="Line of Business" MinWidth="130" Flex="1"></ext:Column>
                              <%--  <ext:Column ID="Column95" runat="server" DataIndex="ENTITY_STATUS" Text="Entity Status" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column96" runat="server" DataIndex="ENTITY_STATUS_DATE" Text="Entity Status Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column97" runat="server" DataIndex="INCORPORATION_STATE" Hidden="true" Text="Incorporation State" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column98" runat="server" DataIndex="INCORPORATION_COUNTRY_CODE" Hidden="true" Text="Incorporation Country Code" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column99" runat="server" DataIndex="INCORPORATION_DATE" Hidden="true" Text="Incorporation Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column100" runat="server" DataIndex="BUSINESS_CLOSED" Hidden="true" Text="Businesss Closed" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column101" runat="server" DataIndex="DATE_BUSINESS_CLOSED" Hidden="true" Text="Date Business Closed" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column102" runat="server" DataIndex="TAX_NUMBER" Hidden="true" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column103" runat="server" DataIndex="TAX_REG_NUMBER" Hidden="true" Text="Tax Reg Number" MinWidth="130" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_related_entities" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Related Entities --%>

                    <ext:DateField ID="DateCorp_incorporation_date" runat="server" Flex="1" FieldLabel="Date Of Estabilished" LabelWidth="175">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                        this.clearValue();
                        this.getTrigger(0).hide();}" />
                        </Listeners>
                    </ext:DateField>
                    <ext:Checkbox ID="chkCorp_business_closed" runat="server" FieldLabel="Is Closed?" BoxLabel="Yes" Checked="false" LabelWidth="175">
                        <DirectEvents>
                            <Change OnEvent="chkCorp_business_closed_Event">
                                <EventMask Msg="Loading..." MinDelay="500" ShowMask="true"></EventMask>
                            </Change>
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField ID="DateCorp_date_business_closed" runat="server" Flex="1" FieldLabel="Closed Date" Hidden="true" LabelWidth="175">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" Hidden="true" Weight="-1" />
                        </Triggers>
                        <Listeners>
                            <Change Handler="this.getTrigger(0).show();" />
                            <TriggerClick Handler="if (index == 0) {
                        this.clearValue();
                        this.getTrigger(0).hide();}" />
                        </Listeners>
                    </ext:DateField>
                    <ext:TextField ID="TxtCorp_tax_number" runat="server" FieldLabel="Tax Number" AnchorHorizontal="80%" Regex="^[0-9]\d*$" RegexText="Must Numeric" MaxLength="15" MinLength="15" LabelWidth="175"></ext:TextField>
                    <ext:TextArea ID="TxtCorp_Comments" runat="server" FieldLabel="Notes" AnchorHorizontal="80%" MaxLength="8000" LabelWidth="175"></ext:TextArea>
                </Items>
            </ext:FormPanel>

        </Items>
        <Buttons>
            <ext:Button ID="btnSave" runat="server" Icon="Disk" Text="Save">
                <Listeners>
                    <%--<Click Handler="if (!#{WIC_Panel}.getForm().isValid()) return false;"></Click>--%>
                    <%--<Click Handler="if (!#{WIC_Individu}.getForm().isValid()) return false;"></Click>
                    <Click Handler="if (!#{WIC_Corporate}.getForm().isValid()) return false;"></Click>--%>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="BtnSave_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <%-- Pop Up Window Email --%>
    <ext:Window ID="Window_Email" runat="server"  Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="FormPanelEmail" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DisplayWIC_Email" runat="server" FieldLabel="WIC" AnchorHorizontal="80%"  MaxLength="500" ></ext:DisplayField>
                    <ext:TextField ID="TxtEmail" runat="server" FieldLabel="Email" AnchorHorizontal="80%"  MaxLength="500" AllowBlank="false" ></ext:TextField>
               </Items>

            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Email_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Email_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="ButtonCancelEmail" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Email_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.7});" />

            <Resize Handler="#{Window_SocialMedia}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Email --%>

    <%-- Pop Up Window Person PEP --%>
    <ext:Window ID="Window_PersonPEP" runat="server"   Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="FormPanelPersonPEP" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DisplayWIC_PersonPEP" runat="server" FieldLabel="WIC" AnchorHorizontal="80%"  MaxLength="500" ></ext:DisplayField>
                    <ext:Panel ID="Pnl_PersonPepCountryCode" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" AllowBlank="false" LabelWidth="200" ID="cmb_PersonPepCountryCode" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="70%"  />
                        </Content>
                    </ext:Panel>
                    <ext:TextField ID="TxtFunctionName" runat="server" FieldLabel="Function Name" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField ID="TxtDescription" runat="server" FieldLabel="Description" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:DateField Hidden="true" ID="TxtPersonPEPValidFrom" runat="server" FieldLabel="Valid From" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" />
                    <ext:Checkbox Hidden="true" runat="server" LabelWidth="200" ID="chk_PersonPEPIsApproxFromDate" FieldLabel="Is Approx From Date"></ext:Checkbox>
                    <ext:DateField Hidden="true" ID="TxtPersonPEPValidTo" runat="server" FieldLabel="Valid To" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" />
                    <ext:Checkbox Hidden="true" runat="server" LabelWidth="200" ID="chk_PersonPEPIsApproxToDate" FieldLabel="Is Approx To Date"></ext:Checkbox>
                    <ext:TextField ID="TxtPersonPEPComments" runat="server" FieldLabel="Comments" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                </Items>

            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_PersonPEP_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Person_PEP_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="ButtonCancelPersonPEP" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Person_PEP_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.7});" />

            <Resize Handler="#{Window_SocialMedia}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Person PEP --%>

    <%-- Pop Up Window Sanction --%>
    <ext:Window ID="Window_Sanction" runat="server"  Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="FormPanelSanction" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DisplayWIC_Sanction" runat="server" FieldLabel="WIC" AnchorHorizontal="80%"  MaxLength="500" ></ext:DisplayField>
                    <ext:TextField ID="TxtProvider" runat="server" FieldLabel="Provider" AnchorHorizontal="80%"  MaxLength="500" AllowBlank="false" ></ext:TextField>
                    <ext:TextField ID="TxtSanctionListName" runat="server" FieldLabel="Sanction List Name" AnchorHorizontal="80%"  MaxLength="500" AllowBlank="false" ></ext:TextField>
                    <ext:TextField ID="TxtMatchCriteria" runat="server" FieldLabel="Match Criteria" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField ID="TxtLinkToSource" runat="server" FieldLabel="Link to Source" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField ID="TxtSanctionListAttributes" runat="server" FieldLabel="Sanction List Attributes" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:DateField Hidden="true" ID="dt_SanctionValidFrom" runat="server" FieldLabel="Valid From" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" />
                    <ext:Checkbox Hidden="true" runat="server" LabelWidth="200" ID="chk_SanctionIsApproxFromDate" FieldLabel="Is Approx From Date"></ext:Checkbox>
                    <ext:DateField Hidden="true" ID="dt_SanctionValidTo" runat="server" FieldLabel="Valid To" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" />
                    <ext:Checkbox Hidden="true" runat="server" LabelWidth="200" ID="chk_SanctionIsApproxToDate" FieldLabel="Is Approx To Date"></ext:Checkbox>
                    <ext:TextField ID="TxtSanctionComments" runat="server" FieldLabel="Comments" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Sanction_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Sanction_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="ButtonCancelSanction" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Sanction_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.7});" />

            <Resize Handler="#{Window_SocialMedia}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Sanction --%>

    <%-- Pop Up Window Related Person --%>
    <ext:Window ID="Window_RelatedPerson" runat="server"   Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="FormPanelRelatedPerson" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DisplayWIC_RelatedPerson" runat="server" FieldLabel="WIC" AnchorHorizontal="80%"  MaxLength="500" ></ext:DisplayField>
                    <ext:Panel ID="Pnl_Person_Person_Relation" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_Person_Person_Relation" Label="Person Relation" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="goAML_Ref_Person_Person_Relationship" AnchorHorizontal="70%" AllowBlank="false"/>
                        </Content>
                    </ext:Panel>
                    
         <%--           <ext:DateField ID="dt_Relation_Date_Range_Valid_From" runat="server" FieldLabel="Relation Date Range -  Valid From" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Relation_Date_Range_Is_Approx_From_Date" FieldLabel="Is Approx From Date"></ext:Checkbox>
                    <ext:DateField ID="dt_Relation_Date_Range_Valid_To" runat="server" FieldLabel="Relation Date Range -  Valid To" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Relation_Date_Range_Is_Approx_To_Date" FieldLabel="Is Approx From Date"></ext:Checkbox>--%>
                    <ext:TextField ID="TxtRelatedPersonRelationComments" runat="server" FieldLabel="Relation Comments" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <%--<ext:TextField ID="TxtRelatedPersonGender" runat="server" FieldLabel="Gender" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>--%>
                    <ext:Panel ID="Panel_cmb_rp_gender" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                    <Content>
		                    <nds:NDSDropDownField ID="cmb_rp_gender" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Jenis_Kelamin" StringFilter="Active = 1" EmptyText="Select One" Label="Gender" AnchorHorizontal="70%"  />
	                    </Content>
                    </ext:Panel>
                    <ext:TextField ID="TxtRelatedPersonTitle" runat="server" FieldLabel="Title" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField Hidden="true" ID="TxtRelatedPersonFirst_Name" runat="server" FieldLabel="First Name" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField Hidden="true" ID="TxtRelatedPersonMiddle_Name" runat="server" FieldLabel="Middle Name" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField Hidden="true" ID="TxtRelatedPersonPrefix" runat="server" FieldLabel="Prefix" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField ID="TxtRelatedPersonLast_Name" runat="server" FieldLabel="Full Name" AnchorHorizontal="80%"  MaxLength="500" AllowBlank="false"></ext:TextField>
                    <ext:DateField ID="dt_RelatedPersonBirth_Date" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="50%" Format="dd-MMM-yyyy" />
                    <ext:TextField ID="TxtRelatedPersonBirth_Place" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:Panel ID="Pnl_RelatedPersonCountry_Of_Birth" Hidden="true" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_RelatedPersonCountry_Of_Birth" Label="Country of Birth" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                        </Content>
                    </ext:Panel>
                    
                    <ext:TextField ID="TxtRelatedPersonMother_Name" runat="server" FieldLabel="Mother Name" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField ID="TxtRelatedPersonAlias" runat="server" FieldLabel="Alias" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                  <%--  <ext:TextField ID="TxtRelatedPersonFull_Name_Frn" runat="server" FieldLabel="Full Name Frn" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>--%>
                    <ext:TextField ID="TxtRelatedPersonSSN" runat="server" FieldLabel="SSN" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField ID="TxtPassport_Number" runat="server" FieldLabel="Passport Number" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:Panel ID="Pnl_Passport_Country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_Passport_Country" Label="Passport Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                        </Content>
                    </ext:Panel>
                    
                    <ext:TextField ID="TxtRelatedPersonID_Number" runat="server" FieldLabel="ID Number" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:Panel ID="Pnl_RelatedPersonNationality1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_RelatedPersonNationality1" Label="Nationality 1" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                        </Content>
                    </ext:Panel>
                    
                    <ext:Panel ID="Pnl_RelatedPersonNationality2" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_RelatedPersonNationality2" Label="Nationality 2" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                        </Content>
                    </ext:Panel>
                    
                    <ext:Panel ID="Pnl_RelatedPersonNationality3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_RelatedPersonNationality3" Label="Nationality 3" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                        </Content>
                    </ext:Panel>
                    
                    <ext:Panel ID="Pnl_RelatedPersonResidence" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_RelatedPersonResidence" Label="Residence" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                        </Content>
                    </ext:Panel>
                    
                    <%--<ext:DateField ID="dt_RelatedPersonResidence_Since" runat="server" FieldLabel="Residence Since" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" />--%>
                    <ext:TextField ID="TxtRelatedPersonOccupation" runat="server" FieldLabel="Occupation" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <%-- 2023-10-24, Nael: Menambahkan field Work Place untuk wic related person --%>
                    <ext:TextField ID="TxtRelatedPersonEmployerName" runat="server" FieldLabel="Work Place" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>

                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_RelatedPersonDeceased" FieldLabel="Deceased">
                        <DirectEvents>
                            <Change OnEvent="checkBoxcb_RelatedPerson_Deceased_click" />
                        </DirectEvents>
                    </ext:Checkbox>
                    <%-- 2023-10-24, Nael: Hiden="true" date deceased tidak terhide pada saat pertamakali dibuka --%>
                    <ext:DateField ID="dt_TxtRelatedPersonDate_Deceased" runat="server" FieldLabel="Date Deceased" Hidden="true" AnchorHorizontal="40%" Format="dd-MMM-yyyy" />
                    <ext:TextField ID="TxtRelatedPersonTax_Number" runat="server" FieldLabel="Tax Number" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_RelatedPersonIs_PEP" FieldLabel="Is PEP"></ext:Checkbox>
                    <ext:TextField ID="TxtRelatedPersonSource_Of_Wealth" runat="server" FieldLabel="Source of Wealth" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_RelatedPersonIs_Protected" FieldLabel="Is Protected"></ext:Checkbox>    
                    
                    <ext:Panel ID="Panel_RelatedPerson_GridAddress" Title="Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridAddress runat="server" ID="GridAddress_RelatedPerson" UniqueName="RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="31"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridAddressWork" Title="Work Place Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:GridAddressWork runat="server" ID="GridAddressWork_RelatedPerson" UniqueName="Work.RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="32"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridPhone" Title="Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridPhone runat="server" ID="GridPhone_RelatedPerson" UniqueName="RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="31"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridPhoneWork" Title="Work Place Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:GridPhoneWork runat="server" ID="GridPhoneWork_RelatedPerson" UniqueName="Work.RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="32" />
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridIdentification" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridIdentification runat="server" ID="GridIdentification_RelatedPerson" UniqueName="RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="31" />
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridEmail runat="server" ID="GridEmail_RelatedPerson" UniqueName="RelatedPerson.ADD" FK_REF_DETAIL_OF="31"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridSanction runat="server" ID="GridSanction_RelatedPerson" UniqueName="RelatedPerson.ADD" FK_REF_DETAIL_OF="31" />
                        </Content>
                                
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridPEP" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridPEP runat="server" ID="GridPEP_RelatedPerson" UniqueName="RelatedPerson.ADD" FK_REF_DETAIL_OF="31" />
                        </Content>
                    </ext:Panel>

                    <ext:TextField ID="TxtRelatedPersonComments" runat="server" FieldLabel="Comments" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>

                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_RelatedPerson_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Related_Person_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="ButtonCancelRelatedPerson" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Related_Person_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.7});" />

            <Resize Handler="#{Window_SocialMedia}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Related Person --%>

    <%-- Add By Septian, goAML 5.0.1, 23 Februari 2022 --%>
    <ext:Window ID="WindowDetail_URL" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity URL/Website Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet12" runat="server" Title="Entity URL/Website" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_URL" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_url" runat="server" FieldLabel="URL" AnchorHorizontal="90%" AllowBlank="False" MaxLength="255"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSaveWIC_URL" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveWIC_URL_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBacKWIC_URL" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBacKWIC_URL_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_EntityIdentification" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet13" runat="server" Title="Entity Identification" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_EntityIdentification" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_cmb_ef_type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_ef_type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Identifier_Type" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Identifier Type" AnchorHorizontal="70%" AllowBlank="false" />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_ef_number" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Number" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Issued Date" ID="df_issue_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:DateField Editable="false" LabelWidth="250" runat="server" FieldLabel="Expiry Date" ID="df_expiry_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_ef_issued_by" LabelWidth="250" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:Panel ID="pnl_ef_issue_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField AllowBlank="False" ID="cmb_ef_issue_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Country Of Birth" AnchorHorizontal="70%" />
	                            </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_ef_comments" LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="50"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSaveWIC_EntityIdentification" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveWIC_EntityIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBacKWIC_EntityIdentification" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBacKWIC_EntityIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_RelatedEntities" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet14" runat="server" Title="Related Entities" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_RelatedEntities" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_cmb_re_entity_relation" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField AllowBlank="false" ID="cmb_re_entity_relation" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Entity_Relationship" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Relation" AnchorHorizontal="70%"  />
	                            </Content>
                            </ext:Panel>
                            <ext:DateField Hidden="true" Editable="false" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid From" ID="df_re_relation_date_range_valid_from" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            
                            <ext:Checkbox Hidden="true" runat="server" LabelWidth="250" ID="cbx_re_relation_date_range_is_approx_from_date" FieldLabel="Relation Date Range -  Is Approx From Date?"></ext:Checkbox>
                            
                            <ext:DateField Hidden="true" Editable="false" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid To" ID="df_re_relation_date_range_valid_to" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            
                            <ext:Checkbox Hidden="true" runat="server" LabelWidth="250" ID="cbx_re_relation_date_range_is_approx_to_date" FieldLabel="Relation Date Range -  Is Approx To Date?"></ext:Checkbox>
                           
                            <ext:NumberField Hidden="true" runat="server" LabelWidth="250" ID="nfd_re_share_percentage" FieldLabel="Share Percentage" AnchorHorizontal="90%" MouseWheelEnabled="false" FormatText="#,##0.00" />
                            
                            <ext:TextField ID="txt_re_relation_comments"  LabelWidth="250" runat="server" FieldLabel="Relation Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                            <%-- 2023-11-22, Nael: txt_re_name mandatory --%>
                            <ext:TextField ID="txt_re_name"  LabelWidth="250" runat="server" FieldLabel="Corporate Name" AnchorHorizontal="90%" MaxLength="255" AllowBlank="false"></ext:TextField>
                            <%--13 --%>
                            <ext:TextField ID="txt_re_commercial_name"  LabelWidth="250" runat="server" FieldLabel="Commercial Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--14 --%>
                            <ext:Panel ID="Panel_cmb_re_incorporation_legal_form" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_re_incorporation_legal_form" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Bentuk_Badan_Usaha" StringFilter="Active = 1" EmptyText="Select One" Label="Legal Form" AnchorHorizontal="70%"  />
	                            </Content>
                            </ext:Panel>
                            <%--15 --%>
                            <ext:TextField ID="txt_re_incorporation_number"  LabelWidth="250" runat="server" FieldLabel="License Number" AnchorHorizontal="90%" MaxLength="50"></ext:TextField>
                            <%--16 --%>
                            <ext:TextField ID="txt_re_business"  LabelWidth="250" runat="server" FieldLabel="Line of Business" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--17 --%>
                            <ext:Panel Hidden="true" ID="Panel_cmb_re_entity_status" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_re_entity_status" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Status" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Status" AnchorHorizontal="70%"  />
	                            </Content>
                            </ext:Panel>
                            <%--18 --%>
                            <ext:DateField Hidden="true" Editable="false" LabelWidth="250"  runat="server" FieldLabel="Entity Status Date" ID="df_re_entity_status_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%--19 --%>
                            <ext:TextField ID="txt_re_incorporation_state"  LabelWidth="250" runat="server" FieldLabel="Province" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--20 --%>
                            <ext:Panel ID="Panel_cmb_re_incorporation_country_code" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                            <Content>
		                            <nds:NDSDropDownField ID="cmb_re_incorporation_country_code" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Incorproation Country Code" AnchorHorizontal="70%"  />
	                            </Content>
                            </ext:Panel>
                            <%--21 --%>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Date of Established" ID="df_re_incorporation_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%--22 --%>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_re_business_closed" FieldLabel="Businesss Closed?">
                                <DirectEvents>
                                    <Change OnEvent="chkRE_business_closed_Event">
                                        <EventMask Msg="Loading..." MinDelay="500" ShowMask="true"></EventMask>
                                    </Change>
                                </DirectEvents>
                            </ext:Checkbox>
                            <%--23 --%>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Date Business Closed" ID="df_re_date_business_closed" AnchorHorizontal="90%" Format="dd-MMM-yyyy" Hidden="true" />
                            <%--24 --%>
                            <ext:TextField ID="txt_re_tax_number"  LabelWidth="250" runat="server" FieldLabel="Tax Number" AnchorHorizontal="90%" MaxLength="16"></ext:TextField>
                            <%--25 --%>
                            <ext:TextField ID="txt_re_tax_reg_number" Hidden="true"  LabelWidth="250" runat="server" FieldLabel="Tax Reg Number" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>

                            <ext:Panel ID="Panel_RelatedEntity_GridPhone" Title="Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <NDS:GridPhone runat="server" ID="GridPhone_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridAddress" Title="Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <NDS:GridAddress runat="server" ID="GridAddress_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEmail runat="server" ID="GridEmail_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_EntityIdentification" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEntityIdentification runat="server" ID="GridEntityIdentification_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridURL" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridURL runat="server" ID="GridURL_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridSanction runat="server" ID="GridSanction_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_re_comments"  LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSaveWIC_RelatedEntities" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveWIC_RelatedEntities_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBacKWIC_RelatedEntities" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBacKWIC_RelatedEntities_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    %-- End Add --%>

    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>

        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <Listeners>
                    <Click Handler="if (!#{WIC_Panel}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick">
                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
