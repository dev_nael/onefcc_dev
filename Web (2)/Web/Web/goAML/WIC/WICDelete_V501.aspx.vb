﻿Imports Ext
Imports NawaBLL
Imports GoAMLBLL
Imports GoAMLDAL
Imports Elmah
Imports System.Data
Imports GoAMLBLL.goAML.Services
Imports NawaDAL
Imports GoAMLBLL.Base.Constant

Partial Class goAML_WICDelete_V501
    Inherits Parent
    'Dim objWIC As New goAML_Ref_WIC
    Public objgoAML_WIC As WICBLL
    Public Property StrUnikKey() As String
        Get
            Return Session("goAML_WICDetail_V501.StrUnikKey")
        End Get
        Set(ByVal value As String)
            Session("goAML_WICDetail_V501.StrUnikKey") = value
        End Set
    End Property

    Public Property objmodule() As NawaDAL.Module
        Get
            Return Session("goAML_WICDetail_V501.objmodule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_WICDetail_V501.objmodule") = value
        End Set
    End Property

    Public Property ObjWIC As goAML_Ref_WIC
        Get
            Return Session("goAML_WICDetail_V501.ObjWIC")
        End Get
        Set(ByVal value As goAML_Ref_WIC)
            Session("goAML_WICDetail_V501.ObjWIC") = value
        End Set
    End Property
    Public Property ObjDirector As goAML_Ref_Walk_In_Customer_Director
        Get
            Return Session("goAML_WICDetail_V501.ObjDirector")
        End Get
        Set(ByVal value As goAML_Ref_Walk_In_Customer_Director)
            Session("goAML_WICDetail_V501.ObjDirector") = value
        End Set
    End Property
    Public Property ListDirectorDetail As List(Of goAML_Ref_Walk_In_Customer_Director)
        Get
            If Session("goAML_WICDetail_V501.ListDirectorDetail") Is Nothing Then
                Session("goAML_WICDetail_V501.ListDirectorDetail") = New List(Of goAML_Ref_Walk_In_Customer_Director)
            End If
            Return Session("goAML_WICDetail_V501.ListDirectorDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Walk_In_Customer_Director))
            Session("goAML_WICDetail_V501.ListDirectorDetail") = value
        End Set
    End Property
    Public Property ListPhoneDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICDetail_V501.ListPhoneDetail") Is Nothing Then
                Session("goAML_WICDetail_V501.ListPhoneDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICDetail_V501.ListPhoneDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICDetail_V501.ListPhoneDetail") = value
        End Set
    End Property
    Public Property ListPhoneEmployerDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICDetail_V501.ListPhoneEmployerDetail") Is Nothing Then
                Session("goAML_WICDetail_V501.ListPhoneEmployerDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICDetail_V501.ListPhoneEmployerDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICDetail_V501.ListPhoneEmployerDetail") = value
        End Set
    End Property
    Public Property ListPhoneDetailDirector As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICDetail_V501.ListPhoneDetailDirector") Is Nothing Then
                Session("goAML_WICDetail_V501.ListPhoneDetailDirector") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICDetail_V501.ListPhoneDetailDirector")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICDetail_V501.ListPhoneDetailDirector") = value
        End Set
    End Property
    Public Property ListPhoneDetailDirectorEmployer As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICDetail_V501.ListPhoneDetailDirectorEmployer") Is Nothing Then
                Session("goAML_WICDetail_V501.ListPhoneDetailDirectorEmployer") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICDetail_V501.ListPhoneDetailDirectorEmployer")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICDetail_V501.ListPhoneDetailDirectorEmployer") = value
        End Set
    End Property
    Public Property ListAddressDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICDetail_V501.ListAddressDetail") Is Nothing Then
                Session("goAML_WICDetail_V501.ListAddressDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICDetail_V501.ListAddressDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICDetail_V501.ListAddressDetail") = value
        End Set
    End Property
    Public Property ListAddressEmployerDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICDetail_V501.ListAddressEmployerDetail") Is Nothing Then
                Session("goAML_WICDetail_V501.ListAddressEmployerDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICDetail_V501.ListAddressEmployerDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICDetail_V501.ListAddressEmployerDetail") = value
        End Set
    End Property
    Public Property ListAddressDetailDirector As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICDetail_V501.ListAddressDetailDirector") Is Nothing Then
                Session("goAML_WICDetail_V501.ListAddressDetailDirector") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICDetail_V501.ListAddressDetailDirector")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICDetail_V501.ListAddressDetailDirector") = value
        End Set
    End Property
    Public Property ListIdentificationDetailDirector As List(Of goAML_Person_Identification)
        Get
            If Session("goAML_WICDetail_V501.ListIdentificationDetailDirector") Is Nothing Then
                Session("goAML_WICDetail_V501.ListIdentificationDetailDirector") = New List(Of goAML_Person_Identification)
            End If
            Return Session("goAML_WICDetail_V501.ListIdentificationDetailDirector")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_WICDetail_V501.ListIdentificationDetailDirector") = value
        End Set
    End Property
    Public Property ListIdentificationDetail As List(Of goAML_Person_Identification)
        Get
            If Session("goAML_WICDetail_V501.ListIdentificationDetail") Is Nothing Then
                Session("goAML_WICDetail_V501.ListIdentificationDetail") = New List(Of goAML_Person_Identification)
            End If
            Return Session("goAML_WICDetail_V501.ListIdentificationDetail")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_WICDetail_V501.ListIdentificationDetail") = value
        End Set
    End Property
    Public Property ListAddressDetailDirectorEmployer As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICDetail_V501.ListAddressDetailDirectorEmployer") Is Nothing Then
                Session("goAML_WICDetail_V501.ListAddressDetailDirectorEmployer") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICDetail_V501.ListAddressDetailDirectorEmployer")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICDetail_V501.ListAddressDetailDirectorEmployer") = value
        End Set
    End Property
    Public Property ObjDetailPhone As goAML_Ref_Phone
        Get
            Return Session("goAML_WICDetail_V501.ObjDetailPhone")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICDetail_V501.ObjDetailPhone") = value
        End Set
    End Property
    Public Property ObjDetailPhoneEmployer As goAML_Ref_Phone
        Get
            Return Session("goAML_WICDetail_V501.ObjDetailPhoneEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICDetail_V501.ObjDetailPhoneEmployer") = value
        End Set
    End Property
    Public Property ObjDetailPhoneDirector As goAML_Ref_Phone
        Get
            Return Session("goAML_WICDetail_V501.ObjDetailPhoneDirector")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICDetail_V501.ObjDetailPhoneDirector") = value
        End Set
    End Property
    Public Property ObjDetailPhoneDirectorEmployer As goAML_Ref_Phone
        Get
            Return Session("goAML_WICDetail_V501.ObjDetailPhoneDirectorEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICDetail_V501.ObjDetailPhoneDirectorEmployer") = value
        End Set
    End Property
    Public Property ObjDetailAddress As goAML_Ref_Address
        Get
            Return Session("goAML_WICDetail_V501.ObjDetailAddress")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICDetail_V501.ObjDetailAddress") = value
        End Set
    End Property
    Public Property ObjDetailAddressEmployer As goAML_Ref_Address
        Get
            Return Session("goAML_WICDetail_V501.ObjDetailAddressEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICDetail_V501.ObjDetailAddressEmployer") = value
        End Set
    End Property
    Public Property ObjDetailAddressDirector As goAML_Ref_Address
        Get
            Return Session("goAML_WICDetail_V501.ObjDetailAddressDirector")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICDetail_V501.ObjDetailAddressDirector") = value
        End Set
    End Property
    Public Property ObjDetailIdentificationDirector As goAML_Person_Identification
        Get
            Return Session("goAML_WICDetail_V501.ObjDetailIdentificationDirector")
        End Get
        Set(ByVal value As goAML_Person_Identification)
            Session("goAML_WICDetail_V501.ObjDetailIdentificationDirector") = value
        End Set
    End Property
    Public Property ObjDetailIdentification As goAML_Person_Identification
        Get
            Return Session("goAML_WICDetail_V501.ObjDetailIdentification")
        End Get
        Set(ByVal value As goAML_Person_Identification)
            Session("goAML_WICDetail_V501.ObjDetailIdentification") = value
        End Set
    End Property
    Public Property ObjDetailAddressDirectorEmployer As goAML_Ref_Address
        Get
            Return Session("goAML_WICDetail_V501.ObjDetailAddressDirectorEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICDetail_V501.ObjDetailAddressDirectorEmployer") = value
        End Set
    End Property
    Public Property obj_ListRelatedPerson_Edit() As List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)
        Get
            Return Session("goAML_WICEdit_V501.obj_ListRelatedPerson_Edit")
        End Get
        Set(ByVal value As List(Of WICDataBLL.goAML_Ref_WIC_Related_Person))
            Session("goAML_WICEdit_V501.obj_ListRelatedPerson_Edit") = value
        End Set
    End Property

    'Add 040123 by Asep
    'GoAML 501
    Public Property WICNo() As String
        Get
            Return Session("goAML_WICDetail_V501.WICNo")
        End Get
        Set(ByVal value As String)
            Session("goAML_WICDetail_V501.WICNo") = value
        End Set
    End Property
    'End 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                GridAddressWork_RelatedPerson.IsViewMode = True
                GridAddress_RelatedPerson.IsViewMode = True
                GridPhone_RelatedPerson.IsViewMode = True
                GridPhoneWork_RelatedPerson.IsViewMode = True
                GridIdentification_RelatedPerson.IsViewMode = True
                GridEmail_RelatedPerson.IsViewMode = True
                GridSanction_RelatedPerson.IsViewMode = True
                GridPEP_RelatedPerson.IsViewMode = True
                GridEmail_RelatedEntity.IsViewMode = True
                GridEntityIdentification_RelatedEntity.IsViewMode = True
                GridURL_RelatedEntity.IsViewMode = True
                GridSanction_RelatedEntity.IsViewMode = True
                GridEmail_Director.IsViewMode = True
                GridSanction_Director.IsViewMode = True
                GridPEP_Director.IsViewMode = True

                Dim Moduleid As String = Request.Params("ModuleID")
                Dim intModuleid As Integer = Common.DecryptQueryString(Moduleid, SystemParameterBLL.GetEncriptionKey)
                objmodule = ModuleBLL.GetModuleByModuleID(intModuleid)

                Dim IDData As String = Request.Params("ID")
                StrUnikKey = Common.DecryptQueryString(IDData, SystemParameterBLL.GetEncriptionKey)

                If Not objmodule Is Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If

                'FormPanelWICDelete.Title = objmodule.ModuleLabel & " Detail"
                'Dim objWICBLL As New WICBLL
                'objWICBLL.LoadPanelDelete(FormPanelWICDelete, objmodule.ModuleName, StrUnikKey)

                ObjWIC = WICBLL.GetWICByID(StrUnikKey)
                ListPhoneDetail = objgoAML_WIC.GetWICByPKIDDetailPhone(StrUnikKey)
                ListAddressDetail = objgoAML_WIC.GetWICByPKIDDetailAddress(StrUnikKey)
                ListPhoneEmployerDetail = objgoAML_WIC.GetWICByPKIDDetailEmployerPhone(StrUnikKey)
                ListAddressEmployerDetail = objgoAML_WIC.GetWICByPKIDDetailEmployerAddress(StrUnikKey)
                ListIdentificationDetail = objgoAML_WIC.GetWICByPKIDDetailIdentification(StrUnikKey)
                obj_ListRelatedPerson_Edit = GetWICByPKIDDetailRelatedPerson(StrUnikKey)

                If ObjWIC.FK_Customer_Type_ID = 2 Then
                    ListDirectorDetail = objgoAML_WIC.GetWICByPKIDDetailDirector(StrUnikKey)
                End If

                LoadDataWIC()
                LoadColumn()
                LoadPopup()
                'Dim strresult As Object = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquery, Nothing)

                WICNo = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select WIC_No from goAML_Ref_WIC where  PK_Customer_ID = " & StrUnikKey, Nothing)

                BindData()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Private Sub LoadPopup()
        WindowDetailAddress.Maximizable = True
        WindowDetailPhone.Maximizable = True
        WindowDetailDirector.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True
        WindowDetailDirectorIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailIdentification.Maximizable = True
        WindowEmail.Maximizable = True
        WindowPersonPEP.Maximizable = True
        WindowSanction.Maximizable = True
        WindowRelatedPerson.Maximizable = True

        ' 2023-10-24, Nael
        WindowDetail_RelatedEntities.Maximizable = True
        WindowDetail_URL.Maximizable = True
        WindowDetail_EntityIdentification.Maximizable = True

    End Sub

    Private Sub LoadColumn()

        ColumnActionLocation(GridPanel1, CommandColumn1)
        ColumnActionLocation(GridPanel2, CommandColumn2)
        ColumnActionLocation(GridPanel3, CommandColumn3)
        ColumnActionLocation(GridPanel4, CommandColumn4)
        ColumnActionLocation(GridPanel5, CommandColumn5)

        ColumnActionLocation(GridPanelPhoneIndividuDetail, CommandColumnPhoneIndividuDetail)
        ColumnActionLocation(GridPanelAddressIndividuDetail, CommandColumnAddressIndividuDetail)
        ColumnActionLocation(GridPanelPhoneIndividualEmployerDetail, CommandColumnPhoneIndividualEmployerDetail)
        ColumnActionLocation(GridPanelAddressIndividualEmployerDetail, CommandColumnAddressIndividualEmployerDetail)
        ColumnActionLocation(GridPanelIdentificationIndividualDetail, CommandColumnIdentificationIndividualDetail)

        ColumnActionLocation(GridPanelPhoneCorporateDetail, CommandColumnPhoneCorporateDetail)
        ColumnActionLocation(GridPanelAddressCorporateDetail, CommandColumnAddressCorporateDetail)
        ColumnActionLocation(GP_Corp_Director, CommandColumnDirector)
        ColumnActionLocation(GridPanelEmail, CommandColumnEmailAction)
        ColumnActionLocation(GridPanelPersonPEP, CommandColumnPersonPEP)
        ColumnActionLocation(GridPanelSanction, CommandColumnActionSanction)
        ColumnActionLocation(GridPanelRelatedPerson, CommandColumnRelatedPersonAction)

    End Sub

    Private Sub ColumnActionLocation(gridPanel As GridPanel, commandColumn As CommandColumn)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridPanel.ColumnModel.Columns.RemoveAt(gridPanel.ColumnModel.Columns.Count - 1)
                gridPanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
        objgoAML_WIC = New WICBLL(WIC_Panel)
    End Sub
    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordIdentificationDirector(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordIdentification(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordAddressDirector(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordPhoneDirectorEmployer(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordAddressDirectorEmployer(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordAddress(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressEmployerIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordAddressEmployer(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneCorporate(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordPhone(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressCorporate(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordAddress(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdCustDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordDirector(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdEmail(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            'id = 1
            DetailRecordEmail(id)
            WindowEmail.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdSanction(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordSanction(id)
            WindowSanction.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdPersonPEP(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordPersonPEP(id)
            WindowPersonPEP.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdRelatedPerson(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    'Dim objToDelete = obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = strID)
                    'If objToDelete IsNot Nothing Then
                    '    obj_ListRelatedPerson_Edit.Remove(objToDelete)
                    'End If
                    'BindDetailRelatedPerson()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    Load_Window_RelatedPerson(strID, strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If

            'DetailRecordRelatedPerson(id)
            WindowRelatedPerson.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub Load_Window_RelatedPerson(strID As Integer, strAction As String)
        'Clean window pop up
        'Clean_Window_RelatedPerson()

        Dim obj_RelatedPerson_Edit = obj_ListRelatedPerson_Edit.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = strID)


        If obj_RelatedPerson_Edit IsNot Nothing Then
            With obj_RelatedPerson_Edit
                Dim refPersonRelation = WICBLL.GetReferenceByCode("goAML_Ref_Person_Person_Relationship", .Person_Person_Relation)
                Dim refCountryBirth = WICBLL.GetReferenceByCode("vw_goAML_Ref_Nama_Negara", .Country_Of_Birth)
                Dim refPassportCountry = WICBLL.GetReferenceByCode("vw_goAML_Ref_Nama_Negara", .Passport_Country)
                Dim refNationality1 = WICBLL.GetReferenceByCode("vw_goAML_Ref_Nama_Negara", .Nationality1)
                Dim refNationality2 = WICBLL.GetReferenceByCode("vw_goAML_Ref_Nama_Negara", .Nationality2)
                Dim refNationality3 = WICBLL.GetReferenceByCode("vw_goAML_Ref_Nama_Negara", .Nationality3)
                Dim refResidence = WICBLL.GetReferenceByCode("vw_goAML_Ref_Nama_Negara", .Residence)

                DisplayWIC_RelatedPerson.Text = .WIC_No
                If refPersonRelation IsNot Nothing Then
                    dd_Person_Person_Relation.SetTextWithTextValue(refPersonRelation.Kode, refPersonRelation.Keterangan)
                End If

                'dt_Relation_Date_Range_Valid_From.SelectedDate = .Relation_Date_Range_Valid_From
                'chk_Relation_Date_Range_Is_Approx_From_Date.Checked = .Relation_Date_Range_Is_Approx_From_Date
                'dt_Relation_Date_Range_Valid_To.SelectedDate = .Relation_Date_Range_Valid_To
                'chk_Relation_Date_Range_Is_Approx_To_Date.Checked = .Relation_Date_Range_Is_Approx_To_Date
                TxtRelatedPersonComments.Value = .Comments
                TxtRelatedPersonRelationComments.Value = .relation_comments
                'TxtRelatedPersonGender.Value = .Gender
                Dim refGender = WICBLL.GetReferenceByCode("goAML_Ref_Jenis_Kelamin", .Gender)
                If refGender IsNot Nothing Then
                    cmb_rp_gender.SetTextWithTextValue(refGender.Kode, refGender.Keterangan)
                End If
                TxtRelatedPersonTitle.Value = .Title
                TxtRelatedPersonFirst_Name.Value = .First_Name
                TxtRelatedPersonMiddle_Name.Value = .Middle_Name
                TxtRelatedPersonPrefix.Value = .Prefix
                TxtRelatedPersonLast_Name.Value = .Last_Name
                dt_RelatedPersonBirth_Date.Value = IIf(.Birth_Date Is Nothing, "", .Birth_Date)
                TxtRelatedPersonBirth_Place.Value = .Birth_Place
                If refCountryBirth IsNot Nothing Then
                    dd_RelatedPersonCountry_Of_Birth.SetTextWithTextValue(refCountryBirth.Kode, refCountryBirth.Keterangan)
                End If

                TxtRelatedPersonMother_Name.Value = .Mother_Name
                TxtRelatedPersonAlias.Value = ._Alias
                'TxtRelatedPersonFull_Name_Frn.Value = .Full_Name_Frn
                TxtRelatedPersonSSN.Value = .SSN
                TxtPassport_Number.Value = .Passport_Number
                If refPassportCountry IsNot Nothing Then
                    dd_Passport_Country.SetTextWithTextValue(refPassportCountry.Kode, refPassportCountry.Keterangan)
                End If

                TxtRelatedPersonID_Number.Value = .ID_Number
                If refNationality1 IsNot Nothing Then
                    dd_RelatedPersonNationality1.SetTextWithTextValue(refNationality1.Kode, refNationality1.Keterangan)
                End If

                If refNationality2 IsNot Nothing Then
                    dd_RelatedPersonNationality2.SetTextWithTextValue(refNationality2.Kode, refNationality2.Keterangan)
                End If

                If refNationality3 IsNot Nothing Then
                    dd_RelatedPersonNationality3.SetTextWithTextValue(refNationality3.Kode, refNationality3.Keterangan)
                End If

                If refResidence IsNot Nothing Then
                    dd_RelatedPersonResidence.SetTextWithTextValue(refResidence.Kode, refResidence.Keterangan)
                End If

                'dt_RelatedPersonResidence_Since.SelectedDate = .Residence_Since
                TxtRelatedPersonOccupation.Value = .Occupation
                TxtRelatedPersonEmployerName.Value = .EMPLOYER_NAME
                chk_RelatedPersonDeceased.Checked = .Deceased
                If .Deceased Then
                    dt_TxtRelatedPersonDate_Deceased.Hidden = False
                Else
                    dt_TxtRelatedPersonDate_Deceased.Hidden = True
                End If
                dt_TxtRelatedPersonDate_Deceased.Value = IIf(.Date_Deceased Is Nothing, "", .Date_Deceased)
                TxtRelatedPersonTax_Number.Value = .Tax_Number
                chk_RelatedPersonIs_PEP.Checked = .Is_PEP
                TxtRelatedPersonSource_Of_Wealth.Value = .Source_Of_Wealth
                chk_RelatedPersonIs_Protected.Checked = .Is_Protected

                Dim pk_id = obj_RelatedPerson_Edit.PK_goAML_Ref_WIC_Related_Person_ID

                Dim address_list = If(.ObjList_GoAML_Ref_Address IsNot Nothing, .ObjList_GoAML_Ref_Address,
                    goAML_Customer_Service.GetAddressByPkFk(pk_id, 31))
                Dim address_work_list = If(.ObjList_GoAML_Ref_Address_Work IsNot Nothing, .ObjList_GoAML_Ref_Address_Work,
                    goAML_Customer_Service.GetAddressByPkFk(pk_id, 32))
                Dim phone_list = If(.ObjList_GoAML_Ref_Phone IsNot Nothing, .ObjList_GoAML_Ref_Phone,
                    goAML_Customer_Service.GetPhoneByPkFk(pk_id, 31))
                Dim phone_work_list = If(.ObjList_GoAML_Ref_Phone_Work IsNot Nothing, .ObjList_GoAML_Ref_Phone_Work,
                    goAML_Customer_Service.GetPhoneByPkFk(pk_id, 32))
                Dim identification_list = If(.ObjList_GoAML_Person_Identification IsNot Nothing, .ObjList_GoAML_Person_Identification,
                    goAML_Customer_Service.GetIdentificationPkFk(pk_id, 31))
                Dim email_list = If(.ObjList_GoAML_Ref_Customer_Email IsNot Nothing, .ObjList_GoAML_Ref_Customer_Email,
                    goAML_Customer_Service.GetEmailByPkFk(pk_id, 31))
                Dim sanction_list = If(.ObjList_GoAML_Ref_Customer_Sanction IsNot Nothing, .ObjList_GoAML_Ref_Customer_Sanction,
                    goAML_Customer_Service.GetSanctionByPkFk(pk_id, 31))
                Dim pep_list = If(.ObjList_GoAML_Ref_Customer_PEP IsNot Nothing, .ObjList_GoAML_Ref_Customer_PEP,
                    goAML_Customer_Service.GetPEPByPkFk(pk_id, 31))

                GridAddress_RelatedPerson.LoadData(address_list)
                GridAddressWork_RelatedPerson.LoadData(address_work_list)
                GridPhone_RelatedPerson.LoadData(phone_list)
                GridPhoneWork_RelatedPerson.LoadData(phone_work_list)
                GridIdentification_RelatedPerson.LoadData(identification_list)

                GridEmail_RelatedPerson.LoadData(email_list)
                GridSanction_RelatedPerson.LoadData(sanction_list)
                GridPEP_RelatedPerson.LoadData(pep_list)
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            dd_Person_Person_Relation.IsReadOnly = False
            'dt_Relation_Date_Range_Valid_From.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_From_Date.ReadOnly = False
            'dt_Relation_Date_Range_Valid_To.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_To_Date.ReadOnly = False
            TxtRelatedPersonComments.ReadOnly = False
            TxtRelatedPersonRelationComments.ReadOnly = False
            'TxtRelatedPersonGender.ReadOnly = False
            cmb_rp_gender.IsReadOnly = False
            TxtRelatedPersonTitle.ReadOnly = False
            TxtRelatedPersonFirst_Name.ReadOnly = False
            TxtRelatedPersonMiddle_Name.ReadOnly = False
            TxtRelatedPersonPrefix.ReadOnly = False
            TxtRelatedPersonLast_Name.ReadOnly = False
            dt_RelatedPersonBirth_Date.ReadOnly = False
            TxtRelatedPersonBirth_Place.ReadOnly = False
            dd_RelatedPersonCountry_Of_Birth.IsReadOnly = False
            TxtRelatedPersonMother_Name.ReadOnly = False
            TxtRelatedPersonAlias.ReadOnly = False
            'TxtRelatedPersonFull_Name_Frn.ReadOnly = False
            TxtRelatedPersonSSN.ReadOnly = False
            TxtPassport_Number.ReadOnly = False
            dd_Passport_Country.IsReadOnly = False
            TxtRelatedPersonID_Number.ReadOnly = False
            dd_RelatedPersonNationality1.IsReadOnly = False
            dd_RelatedPersonNationality2.IsReadOnly = False
            dd_RelatedPersonNationality3.IsReadOnly = False
            dd_RelatedPersonResidence.IsReadOnly = False
            'dt_RelatedPersonResidence_Since.ReadOnly = False
            TxtRelatedPersonOccupation.ReadOnly = False
            TxtRelatedPersonEmployerName.ReadOnly = False
            chk_RelatedPersonDeceased.ReadOnly = False
            dt_TxtRelatedPersonDate_Deceased.ReadOnly = False
            TxtRelatedPersonTax_Number.ReadOnly = False
            chk_RelatedPersonIs_PEP.ReadOnly = False
            TxtRelatedPersonSource_Of_Wealth.ReadOnly = False
            chk_RelatedPersonIs_Protected.ReadOnly = False

            GridAddressWork_RelatedPerson.IsViewMode = False
            GridAddress_RelatedPerson.IsViewMode = False
            GridPhone_RelatedPerson.IsViewMode = False
            GridPhoneWork_RelatedPerson.IsViewMode = False
            GridIdentification_RelatedPerson.IsViewMode = False
            GridEmail_RelatedPerson.IsViewMode = False
            GridSanction_RelatedPerson.IsViewMode = False
            GridPEP_RelatedPerson.IsViewMode = False
        Else
            dd_Person_Person_Relation.IsReadOnly = True
            'dt_Relation_Date_Range_Valid_From.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_From_Date.ReadOnly = False
            'dt_Relation_Date_Range_Valid_To.ReadOnly = False
            'chk_Relation_Date_Range_Is_Approx_To_Date.ReadOnly = False
            TxtRelatedPersonComments.ReadOnly = True
            TxtRelatedPersonRelationComments.ReadOnly = True
            'TxtRelatedPersonGender.ReadOnly = False
            cmb_rp_gender.IsReadOnly = True
            TxtRelatedPersonTitle.ReadOnly = True
            TxtRelatedPersonFirst_Name.ReadOnly = True
            TxtRelatedPersonMiddle_Name.ReadOnly = True
            TxtRelatedPersonPrefix.ReadOnly = True
            TxtRelatedPersonLast_Name.ReadOnly = True
            dt_RelatedPersonBirth_Date.ReadOnly = True
            TxtRelatedPersonBirth_Place.ReadOnly = True
            dd_RelatedPersonCountry_Of_Birth.IsReadOnly = True
            TxtRelatedPersonMother_Name.ReadOnly = True
            TxtRelatedPersonAlias.ReadOnly = True
            'TxtRelatedPersonFull_Name_Frn.ReadOnly = False
            TxtRelatedPersonSSN.ReadOnly = True
            TxtPassport_Number.ReadOnly = True
            dd_Passport_Country.IsReadOnly = True
            TxtRelatedPersonID_Number.ReadOnly = True
            dd_RelatedPersonNationality1.IsReadOnly = True
            dd_RelatedPersonNationality2.IsReadOnly = True
            dd_RelatedPersonNationality3.IsReadOnly = True
            dd_RelatedPersonResidence.IsReadOnly = True
            'dt_RelatedPersonResidence_Since.ReadOnly = False
            TxtRelatedPersonOccupation.ReadOnly = True
            TxtRelatedPersonEmployerName.ReadOnly = True
            chk_RelatedPersonDeceased.ReadOnly = True
            dt_TxtRelatedPersonDate_Deceased.ReadOnly = True
            TxtRelatedPersonTax_Number.ReadOnly = True
            chk_RelatedPersonIs_PEP.ReadOnly = True
            TxtRelatedPersonSource_Of_Wealth.ReadOnly = True
            chk_RelatedPersonIs_Protected.ReadOnly = True

            GridAddressWork_RelatedPerson.IsViewMode = True
            GridAddress_RelatedPerson.IsViewMode = True
            GridPhone_RelatedPerson.IsViewMode = True
            GridPhoneWork_RelatedPerson.IsViewMode = True
            GridIdentification_RelatedPerson.IsViewMode = True
            GridEmail_RelatedPerson.IsViewMode = True
            GridSanction_RelatedPerson.IsViewMode = True
            GridPEP_RelatedPerson.IsViewMode = True
        End If

        'Bind Indikator

        'Show window pop up
        WindowRelatedPerson.Title = "Related Person - " & strAction
        WindowRelatedPerson.Hidden = False
    End Sub
    Private Sub DetailRecordDirector(id As String)
        ObjDirector = ListDirectorDetail.Find(Function(x) x.NO_ID = id)
        If Not ObjDirector Is Nothing Then

            FormPanelDirectorDetail.Show()
            WindowDetailDirector.Show()
            With ObjDirector
                If GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role) IsNot Nothing Then
                    DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
                Else
                    DspPeranDirector.Text = ""
                End If

                DspGelarDirector.Text = .Title
                DspNamaLengkap.Text = .Last_Name
                If GlobalReportFunctionBLL.getGenderbyKode(.Gender) IsNot Nothing Then
                    DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                Else
                    DspGender.Text = ""
                End If


                If .BirthDate IsNot Nothing Then
                    DspTanggalLahir.Text = ObjDirector.BirthDate.Value.ToString("dd-MMM-yy")
                End If
                DspTempatLahir.Text = .Birth_Place
                DspNamaIbuKandung.Text = .Mothers_Name
                DspNamaAlias.Text = .Alias
                DspNIK.Text = .SSN
                DspNoPassport.Text = .Passport_Number
                'daniel 19 jan 2021
                'DspNegaraPenerbitPassport.Text = .Passport_Country
                DspNegaraPenerbitPassport.Text = GlobalReportFunctionBLL.getCountryByCode(.Passport_Country)
                DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                DspNoIdentitasLain.Text = .ID_Number
                'If GlobalReportFunctionBLL.getCountryByCode(.Nationality1) IsNot Nothing Then
                '    DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.Nationality2) IsNot Nothing Then
                '    DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                'Else
                '    DspKewarganegaraan2.Text = ""
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.Nationality3) IsNot Nothing Then
                '    DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                'Else
                '    DspKewarganegaraan3.Text = ""
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.Residence) IsNot Nothing Then
                '    DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                'Else
                '    DspNegaraDomisili.Text = ""
                'End If
                'end 19 jan 2021

                DspEmail.Text = .Email
                DspEmail2.Text = .Email2
                DspEmail3.Text = .Email3
                DspEmail4.Text = .Email4
                DspEmail5.Text = .Email5
                DspDeceased.Text = .Deceased
                If DspDeceased.Text = "True" Then
                    If .Deceased_Date IsNot Nothing Then
                        DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                        'Daniel 2021 Jan 7
                        DspDeceasedDate.Hidden = False
                    End If
                Else
                    DspDeceasedDate.Hidden = True
                    'end 2021 Jan 7
                End If
                DspPEP.Text = .Tax_Reg_Number
                DspNPWP.Text = .Tax_Number
                DspSourceofWealth.Text = .Source_of_Wealth
                DspOccupation.Text = .Occupation
                DspCatatan.Text = .Comments
                DspTempatBekerja.Text = .Employer_Name
            End With

            ListPhoneDetailDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorPhone(ObjDirector.NO_ID)
            ListPhoneDetailDirectorEmployer = objgoAML_WIC.GetWICByPKIDDetailDirectorPhoneEmployer(ObjDirector.NO_ID)
            ListAddressDetailDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorAddress(ObjDirector.NO_ID)
            ListAddressDetailDirectorEmployer = objgoAML_WIC.GetWICByPKIDDetailDirectorAddressEmployer(ObjDirector.NO_ID)
            ListIdentificationDetailDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorIdentification(ObjDirector.NO_ID)

            BindDetailPhone(StoreDirectorPhone, ListPhoneDetailDirector)
            BindDetailPhone(StoreDirectorPhoneEmployer, ListPhoneDetailDirectorEmployer)
            BindDetailAddress(StoreDirectorAddress, ListAddressDetailDirector)
            BindDetailAddress(StoreDirectorAddressEmployer, ListAddressDetailDirectorEmployer)
            BindDetailIdentification(StoreIdentificationDirector, ListIdentificationDetailDirector)

            GridEmail_Director.IsViewMode = True
            GridSanction_Director.IsViewMode = True
            GridPEP_Director.IsViewMode = True
            ' 2023-11-16, Nael
            GridEmail_Director.LoadData(goAML_Customer_Service.WICEmailService.GetByPkFk(ObjDirector.NO_ID, RefDetail.WIC_DIRECTOR))
            GridSanction_Director.LoadData(goAML_Customer_Service.WICSanctionService.GetByPkFk(ObjDirector.NO_ID, RefDetail.WIC_DIRECTOR))
            GridPEP_Director.LoadData(goAML_Customer_Service.WICPersonPEPService.GetByPkFk(ObjDirector.NO_ID, RefDetail.WIC_DIRECTOR))
        End If
    End Sub

    Private Sub DetailRecordAddress(id As String)
        ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddress Is Nothing Then

            PanelDetailAddress.Show()
            WindowDetailAddress.Show()
            With ObjDetailAddress
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                Else
                    DspAddress_type.Text = ""
                End If

                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                Else
                    Dspcountry_code.Text = ""
                End If
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
    End Sub
    Private Sub DetailRecordAddressEmployer(id As String)
        ObjDetailAddressEmployer = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 10)
        If Not ObjDetailAddressEmployer Is Nothing Then

            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Show()
            WindowDetailAddress.Show()
            With ObjDetailAddressEmployer
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                Else
                    DspAddress_typeEmployer.Text = ""
                End If
                DspAddressEmployer.Text = .Address
                DspTownEmployer.Text = .Town
                DspCityEmployer.Text = .City
                DspZipEmployer.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                Else
                    Dspcountry_codeEmployer.Text = ""
                End If

                DspStateEmployer.Text = .State
                DspcommentsAddressEmployer.Text = .Comments
            End With
        End If
    End Sub
    Protected Sub GridCommandPhoneIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordPhone(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneEmployerIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordPhoneEmployer(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordPhone(id As String)
        ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhone Is Nothing Then

            PanelDetailPhone.Show()
            WindowDetailPhone.Show()
            With ObjDetailPhone
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Else
                    Dsptph_contact_type.Text = ""
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Else
                    Dsptph_communication_type.Text = ""
                End If
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
    End Sub
    Private Sub DetailRecordPhoneEmployer(id As String)
        ObjDetailPhoneEmployer = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id And x.FK_Ref_Detail_Of = 10)
        If Not ObjDetailPhoneEmployer Is Nothing Then

            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Show()
            WindowDetailPhone.Show()
            With ObjDetailPhoneEmployer
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Else
                    Dsptph_contact_typeEmployer.Text = ""
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Else
                    Dsptph_communication_typeEmployer.Text = ""
                End If
                Dsptph_country_prefixEmployer.Text = .tph_country_prefix
                Dsptph_numberEmployer.Text = .tph_number
                Dsptph_extensionEmployer.Text = .tph_extension
                DspcommentsPhoneEmployer.Text = .comments
            End With
        End If
    End Sub
    Protected Sub btnBackSaveCustomerDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirector.Hide()
            FormPanelDirectorDetail.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()
            PanelDetailPhone.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailAddress.Hide()
            PanelDetailAddress.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorDetailAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorDetailEmployerAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()

            PanelPhoneDirector.Show()
            PanelPhoneDirectorEmployer.Hide()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorEmployerPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()

            PanelPhoneDirector.Show()
            PanelPhoneDirectorEmployer.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelIdentificationDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorIdentification.Hide()
            PanelDetailIdentificationDirector.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelIdentification_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailIdentification.Hide()
            PanelDetailIdentification.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelPhoneEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            PanelDetailPhone.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddressEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailAddress.Hide()
            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Private Sub DetailRecordAddressDirectorEmployer(id As String)
        ObjDetailAddressDirectorEmployer = ListAddressDetailDirectorEmployer.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddressDirectorEmployer Is Nothing Then

            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Show()
            WindowDetailDirectorAddress.Show()
            With ObjDetailAddressDirectorEmployer
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                Else
                    DspTipeAlamatDirectorEmployer.Text = ""
                End If
                DspAlamatDirectorEmployer.Text = .Address
                DspKecamatanDirectorEmployer.Text = .Town
                DspKotaKabupatenDirectorEmployer.Text = .City
                DspKodePosDirectorEmployer.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                Else
                    DspNegaraDirectorEmployer.Text = ""
                End If

                DspProvinsiDirectorEmployer.Text = .State
                DspCatatanAddressDirectorEmployer.Text = .Comments
            End With
        End If
    End Sub

    Private Sub DetailRecordPhoneDirectorEmployer(id As String)
        ObjDetailPhoneDirectorEmployer = ListPhoneDetailDirectorEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneDirectorEmployer Is Nothing Then

            PanelPhoneDirector.Hide()
            WindowDetailDirectorPhone.Show()
            PanelPhoneDirectorEmployer.Show()
            'Daniel 2021 Jan 5
            'With ObjDetailPhoneDirector
            With ObjDetailPhoneDirectorEmployer
                'end 2021 Jan 5
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    DspKategoriKontakDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Else
                    DspKategoriKontakDirectorEmployer.Text = ""
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    DspJenisAlatKomunikasiDirectorEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Else
                    DspJenisAlatKomunikasiDirectorEmployer.Text = ""
                End If
                DspKodeAreaTelpDirectorEmployer.Text = .tph_country_prefix
                DspNomorTeleponDirectorEmployer.Text = .tph_number
                DspNomorExtensiDirectorEmployer.Text = .tph_extension
                DspCatatanDirectorPhoneDirectorEmployer.Text = .comments
            End With
        End If
    End Sub
    Private Sub DetailRecordIdentificationDirector(id As String)
        ObjDetailIdentificationDirector = ListIdentificationDetailDirector.Find(Function(x) x.PK_Person_Identification_ID = id And x.FK_Person_Type = 7)
        If Not ObjDetailIdentificationDirector Is Nothing Then

            PanelDetailIdentificationDirector.Show()
            WindowDetailDirectorIdentification.Show()
            With ObjDetailIdentificationDirector
                If GlobalReportFunctionBLL.getjenisDokumenByKode(.Type) IsNot Nothing Then
                    DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                Else
                    DsptTypeDirector.Text = ""
                End If
                DspNumberDirector.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDateDirector.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDateDirector.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedByDirector.Text = .Issued_By
                If GlobalReportFunctionBLL.getCountryByCode(.Issued_Country) IsNot Nothing Then
                    DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                Else
                    DspIssuedCountryDirector.Text = ""
                End If
                DspIdentificationCommentDirector.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub DetailRecordIdentification(id As String)
        ObjDetailIdentification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id And x.FK_Person_Type = 8)
        If Not ObjDetailIdentification Is Nothing Then

            PanelDetailIdentification.Show()
            WindowDetailIdentification.Show()
            With ObjDetailIdentification
                If GlobalReportFunctionBLL.getjenisDokumenByKode(.Type) IsNot Nothing Then
                    DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                Else
                    DsptType.Text = ""
                End If

                DspNumber.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDate.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDate.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedBy.Text = .Issued_By
                If GlobalReportFunctionBLL.getCountryByCode(.Issued_Country) IsNot Nothing Then
                    DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                Else
                    DspIssuedCountry.Text = ""
                End If

                DspIdentificationComment.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub DetailRecordAddressDirector(id As String)
        ObjDetailAddressDirector = ListAddressDetailDirector.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddressDirector Is Nothing Then

            PanelAddressDirector.Show()
            PanelAddressDirectorEmployer.Hide()
            WindowDetailDirectorAddress.Show()
            With ObjDetailAddressDirector
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                Else
                    DspTipeAlamatDirector.Text = ""
                End If
                DspAlamatDirector.Text = .Address
                DspKecamatanDirector.Text = .Town
                DspKotaKabupatenDirector.Text = .City
                DspKodePosDirector.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                End If
                DspProvinsiDirector.Text = .State
                DspCatatanAddressDirector.Text = .Comments
            End With
        End If
    End Sub

    Protected Sub GrdCmdDirectorPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordPhoneDirector(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordPhoneDirector(id As String)
        ObjDetailPhoneDirector = ListPhoneDetailDirector.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneDirector Is Nothing Then

            PanelPhoneDirector.Show()
            WindowDetailDirectorPhone.Show()
            PanelPhoneDirectorEmployer.Hide()
            With ObjDetailPhoneDirector
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                End If
                DspKodeAreaTelpDirector.Text = .tph_country_prefix
                DspNomorTeleponDirector.Text = .tph_number
                DspNomorExtensiDirector.Text = .tph_extension
                DspCatatanDirectorPhoneDirector.Text = .comments
            End With
        End If
    End Sub

    Private Sub LoadDataPhone()
        ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = ID And x.FK_Ref_Detail_Of = 3)
        If Not ObjDetailPhone Is Nothing Then
            PanelDetailPhone.Show()
            WindowDetailPhone.Show()
            With ObjDetailPhone
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                End If
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataPhoneDirector()
        ObjDetailPhoneDirector = ListPhoneDetailDirector.Find(Function(x) x.PK_goAML_Ref_Phone = ID And x.FK_Ref_Detail_Of = 8)
        If Not ObjDetailPhone Is Nothing Then
            PanelPhoneDirector.Show()
            PanelPhoneDirectorEmployer.Hide()
            WindowDetailDirectorPhone.Show()
            With ObjDetailPhone
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                End If
                DspKodeAreaTelpDirector.Text = .tph_country_prefix
                DspNomorTeleponDirector.Text = .tph_number
                DspNomorExtensiDirector.Text = .tph_extension
                DspCatatanDirectorPhoneDirector.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataPhoneDirectorEmployer()
        ObjDetailPhoneDirectorEmployer = ListPhoneDetailDirectorEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = ID And x.FK_Ref_Detail_Of = 4)
        If Not ObjDetailPhone Is Nothing Then
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Show()
            WindowDetailDirectorPhone.Show()
            With ObjDetailPhone
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    DspKategoriKontakDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    DspJenisAlatKomunikasiDirectorEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                End If
                DspKodeAreaTelpDirectorEmployer.Text = .tph_country_prefix
                DspNomorTeleponDirectorEmployer.Text = .tph_number
                DspNomorExtensiDirectorEmployer.Text = .tph_extension
                DspCatatanDirectorPhoneDirectorEmployer.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataAddress()
        ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = ID And x.FK_Ref_Detail_Of = 3)
        If Not ObjDetailAddress Is Nothing Then
            PanelDetailAddress.Show()
            WindowDetailAddress.Show()
            With ObjDetailAddress
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                End If
                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                End If
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
    End Sub
    Private Sub LoadDataAddressDirector()
        ObjDetailAddressDirector = ListAddressDetailDirector.Find(Function(x) x.PK_Customer_Address_ID = ID And x.FK_Ref_Detail_Of = 8)
        If Not ObjDetailAddress Is Nothing Then
            PanelAddressDirector.Show()
            PanelAddressDirectorEmployer.Hide()
            WindowDetailAddress.Show()
            With ObjDetailAddress
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                End If
                DspAlamatDirector.Text = .Address
                DspKecamatanDirector.Text = .Town
                DspKotaKabupatenDirector.Text = .City
                DspKodePosDirector.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                End If
                DspProvinsiDirector.Text = .State
                DspCatatanAddressDirector.Text = .Comments
            End With
        End If
    End Sub
    Private Sub LoadDataAddressDirectorEmployer()
        ObjDetailAddressDirectorEmployer = ListAddressDetailDirectorEmployer.Find(Function(x) x.PK_Customer_Address_ID = ID And x.FK_Ref_Detail_Of = 4)
        If Not ObjDetailAddress Is Nothing Then
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Show()
            WindowDetailAddress.Show()
            With ObjDetailAddress
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                End If
                DspAlamatDirectorEmployer.Text = .Address
                DspKecamatanDirectorEmployer.Text = .Town
                DspKotaKabupatenDirectorEmployer.Text = .City
                DspKodePosDirectorEmployer.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                End If
                DspProvinsiDirectorEmployer.Text = .State
                DspCatatanAddressDirectorEmployer.Text = .Comments
            End With
        End If
    End Sub
    Private Sub LoadDataWIC()
        PKWIC.Text = ObjWIC.PK_Customer_ID
        DSpWICNo.Text = ObjWIC.WIC_No
        If ObjWIC.isUpdateFromDataSource = True Then
            DspIsUpdateFromDataSource.Text = "True"
        ElseIf ObjWIC.isUpdateFromDataSource = False Then
            DspIsUpdateFromDataSource.Text = "False"
        Else
            DspIsUpdateFromDataSource.Text = ""
        End If

        'IsRealWIC
        'If ObjWIC.Is_RealWIC = True Then
        '    DspIsRealWIC.Text = "True"
        'Else
        If ObjWIC.isUpdateFromDataSource = False Then
            DspIsRealWIC.Text = "False"
        Else
            DspIsRealWIC.Text = ""
        End If

        If ObjWIC.FK_Customer_Type_ID = 1 Then
            With ObjWIC
                TxtINDV_Title.Text = .INDV_Title
                TxtINDV_last_name.Text = .INDV_Last_Name
                'TxtINDV_Frgn_Lng_Full_Name.Value = .Full_Name_Frn
                'TxtINDV_Residence_Since.Value = .Residence_Since
                'TxtINDV_Is_Protected.Value = .Is_Protected
                If GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender) IsNot Nothing Then
                    TxtINDV_Gender.Text = GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender)
                End If
                'agam 02112020
                If .INDV_BirthDate IsNot Nothing Then
                    DateINDV_Birthdate.Text = .INDV_BirthDate.Value.ToString("dd-MMM-yy")
                End If
                ' DateINDV_Birthdate.Text = .INDV_BirthDate
                TxtINDV_Birth_place.Text = .INDV_Birth_Place
                TxtINDV_Mothers_name.Text = .INDV_Mothers_Name
                TxtINDV_Alias.Text = .INDV_Alias
                TxtINDV_SSN.Text = .INDV_SSN
                TxtINDV_Passport_number.Text = .INDV_Passport_Number
                'daniel 19 jan 2021
                'TxtINDV_Passport_country.Text = .INDV_Passport_Country
                TxtINDV_Passport_country.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Passport_Country)
                CmbINDV_Nationality1.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1)
                CmbINDV_Nationality2.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2)
                CmbINDV_Nationality3.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3)
                CmbINDV_residence.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence)
                TxtINDV_ID_Number.Text = .INDV_ID_Number
                'If GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1) IsNot Nothing Then
                '    CmbINDV_Nationality1.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1)
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2) IsNot Nothing Then
                '    CmbINDV_Nationality2.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2)
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3) IsNot Nothing Then
                '    CmbINDV_Nationality3.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3)
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence) IsNot Nothing Then
                '    CmbINDV_residence.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence)
                'End If
                'daniel 19 jan 2021
                'comment on 3/1/23
                'TxtINDV_Email.Text = .INDV_Email
                'TxtINDV_Email2.Text = .INDV_Email2
                'TxtINDV_Email3.Text = .INDV_Email3
                'TxtINDV_Email4.Text = .INDV_Email4
                'TxtINDV_Email5.Text = .INDV_Email5
                TxtINDV_Occupation.Text = .INDV_Occupation
                'daniel 11 jan 2021
                TxtINDV_employer_name.Text = .INDV_Employer_Name
                'end 11 jan 2021
                'comment on 3/1/23
                TxtINDV_Source_Of_Wealth.Text = .INDV_SumberDana ' 2023-11-08, Nael: Uncomment
                TxtINDV_Tax_Number.Text = .INDV_Tax_Number
                CbINDV_Tax_Reg_Number.Text = .INDV_Tax_Reg_Number
                If .INDV_Deceased = True Then
                    CbINDV_Deceased.Text = "True"
                ElseIf .INDV_Deceased = True Then
                    CbINDV_Deceased.Text = "False"
                Else
                    CbINDV_Deceased.Text = ""
                End If

                If .INDV_Deceased Then
                    If .INDV_Deceased_Date IsNot Nothing Then
                        DateINDV_Deceased_Date.Text = .INDV_Deceased_Date.ToString
                        DateINDV_Deceased_Date.Hidden = False
                    End If
                End If
                'comment on 010523 by Asep
                TxtINDV_Comments.Text = .INDV_Comment ' 2023-11-08, Nael: Uncomment
            End With

            BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
            BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)
            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)
            BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)



            WIC_Individu.Show()
            WIC_Corporate.Hide()
        Else
            With ObjWIC
                TxtCorp_Name.Text = .Corp_Name
                TxtCorp_Commercial_name.Text = .Corp_Commercial_Name
                If GlobalReportFunctionBLL.getBentukKorporasiByKode(.Corp_Incorporation_Legal_Form) IsNot Nothing Then
                    CmbCorp_Incorporation_legal_form.Text = GlobalReportFunctionBLL.getBentukKorporasiByKode(.Corp_Incorporation_Legal_Form)
                End If
                TxtCorp_Incorporation_number.Text = .Corp_Incorporation_Number
                TxtCorp_Business.Text = .Corp_Business
                'TxtCorp_Email.Text = .Corp_Email
                'TxtCorp_url.Text = .Corp_Url
                TxtCorp_incorporation_state.Text = .Corp_Incorporation_State
                If GlobalReportFunctionBLL.getCountryByCode(.Corp_Incorporation_Country_Code) IsNot Nothing Then
                    CmbCorp_incorporation_country_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Corp_Incorporation_Country_Code)
                End If
                'daniel 12 jam 2021
                'If GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Corp_Role) IsNot Nothing Then
                '    CmbCorp_Role.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Corp_Role)
                'End If
                'end 12 jam 2021
                If .Corp_Incorporation_Date IsNot Nothing Then
                    DateCorp_incorporation_date.Text = .Corp_Incorporation_Date.Value.ToString("dd-MMM-yy")
                End If
                chkCorp_business_closed.Text = If(.Corp_Business_Closed, True, False)
                If chkCorp_business_closed.Text = "True" Then
                    DateCorp_date_business_closed.Hidden = False
                    If .Corp_Date_Business_Closed IsNot Nothing Then
                        DateCorp_date_business_closed.Text = .Corp_Date_Business_Closed.Value.ToString("dd-MMM-yy")
                    End If
                End If
                TxtCorp_tax_number.Text = .Corp_Tax_Number
                TxtCorp_Comments.Text = .Corp_Comments
            End With

            BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
            BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
            BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)

            WIC_Individu.Hide()
            WIC_Corporate.Show()
        End If
    End Sub
    Private Sub BindDetailIdentification(store As Store, list As List(Of goAML_Person_Identification))
        Dim objtable As DataTable = NawaBLL.Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Tipe", GetType(String)))
        objtable.Columns.Add(New DataColumn("No", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssueDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("ExpiryDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssuedCountry", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Tipe") = GlobalReportFunctionBLL.getjenisDokumenByKode(item("Type"))
                item("No") = item("Number")
                item("IssueDate") = item("Issue_Date")
                item("ExpiryDate") = item("Expiry_Date")
                item("IssuedCountry") = GlobalReportFunctionBLL.getCountryByCode(item("Issued_Country"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailPhone(store As Store, list As List(Of goAML_Ref_Phone))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
                item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailAddress(store As Store, list As List(Of goAML_Ref_Address))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Type_Address", GetType(String)))
        objtable.Columns.Add(New DataColumn("Addres", GetType(String)))
        objtable.Columns.Add(New DataColumn("Cty", GetType(String)))
        objtable.Columns.Add(New DataColumn("CountryCode", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Type_Address") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type"))
                item("Addres") = item("Address")
                item("Cty") = item("City")
                item("CountryCode") = GlobalReportFunctionBLL.getCountryByCode(item("Country_Code"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailDirector(store As Store, list As List(Of goAML_Ref_Walk_In_Customer_Director))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("LastName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Roles", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("LastName") = item("Last_Name")
                item("Roles") = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(item("Role"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Protected Sub BtnDelete_Click(sender As Object, e As DirectEventArgs)
        Try
            Select Case e.ExtraParams("command")
                Case "Delete"
                    Dim objWICBLL As New WICBLL
                    'If WICBLL.IsDataValidDelete(objWIC) Then
                    If Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not objmodule.IsUseApproval Then
                        objWICBLL.DeleteTanpaapproval(StrUnikKey, objmodule)
                        LblConfirmation.Text = "Data  " & objmodule.ModuleLabel & " is deleted."
                    Else
                        objWICBLL.DeleteDenganapproval(StrUnikKey, objmodule)
                        LblConfirmation.Text = "Data " & objmodule.ModuleLabel & " Saved into Pending Approval"
                    End If

                    Panelconfirmation.Hidden = False
                    WIC_Panel.Hidden = True
                    'End If
            End Select
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Net.X.Redirect(Common.GetApplicationPath & objmodule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

#Region "Email"
    Private Sub BindDetailEmail()
        Try
            'Dim listEmail As New DataTable
            'listEmail = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM [dbo].[goAML_Ref_WIC_Email] WHERE WIC_No = '" & WICNo & "'")
            Dim listEmail = objgoAML_WIC.GetEmail(ObjWIC.WIC_No, 3)

            GridPanelEmail.GetStore.DataSource = listEmail
            GridPanelEmail.GetStore.DataBind()

            GPWIC_Corp_Email.GetStore.DataSource = listEmail
            GPWIC_Corp_Email.GetStore.DataBind()
        Catch ex As Exception

        End Try


    End Sub
#End Region

#Region "Person PEP"
    Private Sub BindDetailPersonPEP()
        Try

            Dim listPersonPEP = objgoAML_WIC.GetPEP(WICNo, 3)

            GridPanelPersonPEP.GetStore.DataSource = listPersonPEP
            GridPanelPersonPEP.GetStore.DataBind()
        Catch ex As Exception

        End Try


    End Sub
#End Region


#Region "Sanction"
    Private Sub BindDetailSanction()
        Try

            Dim listSanction = objgoAML_WIC.GetSanction(WICNo, 3)

            GridPanelSanction.GetStore.DataSource = listSanction
            GridPanelSanction.GetStore.DataBind()

            GP_Corp_Sanction.GetStore.DataSource = listSanction
            GP_Corp_Sanction.GetStore.DataBind()
        Catch ex As Exception
            Throw ex
        End Try


    End Sub
#End Region

#Region "Related Person"
    Private Sub BindDetailRelatedPerson()
        Try
            Dim listRelatedPerson As New DataTable
            listRelatedPerson = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM [dbo].[goAML_Ref_WIC_Related_Person] WHERE WIC_No = '" & WICNo & "'")

            GridPanelRelatedPerson.GetStore.DataSource = listRelatedPerson
            GridPanelRelatedPerson.GetStore.DataBind()

            GP_Corp_RelatedPerson.GetStore.DataSource = listRelatedPerson
            GP_Corp_RelatedPerson.GetStore.DataBind()
        Catch ex As Exception

        End Try
    End Sub
#End Region


    Protected Sub BtnCancelEmailDirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowEmail.Hide()
            'FormPanelEmail.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordEmail(id As String)
        Try

            Dim listEmail As New DataTable
            listEmail = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM [dbo].[goAML_Ref_WIC_Email] WHERE PK_goAML_Ref_WIC_Email_ID = '" & id & "'")

            If listEmail IsNot Nothing Then
                If listEmail.Rows.Count > 0 Then
                    For Each item As DataRow In listEmail.Rows
                        DspWindowEmail.Value = item("email_address")
                    Next

                End If
            End If

            'Set Fields' ReadOnly
            DspWindowEmail.ReadOnly = True

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub BtnCancelPersonPEPDirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowPersonPEP.Hide()
            'FormPanelPersonPEP.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordPersonPEP(id As String)
        Try

            Dim listPersonPEP As New DataTable
            listPersonPEP = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM [dbo].[goAML_Ref_WIC_Person_PEP] WHERE PK_goAML_Ref_WIC_PEP_ID = '" & id & "'")
            If listPersonPEP IsNot Nothing Then
                If listPersonPEP.Rows.Count > 0 Then
                    For Each item As DataRow In listPersonPEP.Rows
                        DspCountry.Value = item("pep_country")
                        DspFunctionName.Value = item("function_name")
                        DspWindowDescription.Value = item("function_description")
                        DspWindowValidFrom.Value = item("pep_date_range_valid_from")
                        DspWindowIsApproxFromDate.Value = item("pep_date_range_is_approx_from_date")
                        DspWindowValidTo.Value = item("pep_date_range_valid_to")
                        DspWindowIsApproxToDate.Value = item("pep_date_range_is_approx_to_date")
                        DspWindowPersonPEPComments.Value = item("comments")
                    Next
                End If
            End If

            'Set Fields' ReadOnly
            DspCountry.ReadOnly = True
            DspFunctionName.ReadOnly = True
            DspWindowDescription.ReadOnly = True
            DspWindowValidFrom.ReadOnly = True
            DspWindowIsApproxFromDate.ReadOnly = True
            DspWindowValidTo.ReadOnly = True
            DspWindowIsApproxToDate.ReadOnly = True
            DspWindowPersonPEPComments.ReadOnly = True

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub BtnCancelSanctionDirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowSanction.Hide()
            'FormPanelSanction.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordSanction(id As String)
        Try

            Dim listSanction As New DataTable
            listSanction = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM [dbo].[goAML_Ref_WIC_Sanction] WHERE PK_goAML_Ref_WIC_Sanction_ID = '" & id & "'")
            If listSanction IsNot Nothing Then
                If listSanction.Rows.Count > 0 Then
                    For Each item As DataRow In listSanction.Rows
                        DspProvider.Value = item("Provider")
                        DspSanctionListName.Value = item("Sanction_List_Name")
                        DspMatchCriteria.Value = item("Match_Criteria")
                        DspLinktoSource.Value = item("Link_To_Source")
                        DspSanctionListAttributes.Value = item("Sanction_List_Attributes")
                        DspWindowSanctionValidFrom.Value = item("Sanction_List_Date_Range_Valid_From")
                        DspWindowSanctionIsApproxFromDate.Value = item("Sanction_List_Date_Range_Is_Approx_From_Date")
                        DspWindowSanctionValidTo.Value = item("Sanction_List_Date_Range_Valid_To")
                        DspWindowSanctionIsApproxToDate.Value = item("Sanction_List_Date_Range_Is_Approx_To_Date")
                        DisplayWindowSanctionComments.Value = item("Comments")
                    Next
                End If
            End If

            'Set fields' ReadOnly
            DspProvider.ReadOnly = True
            DspSanctionListName.ReadOnly = True
            DspMatchCriteria.ReadOnly = True
            DspLinktoSource.ReadOnly = True
            DspSanctionListAttributes.ReadOnly = True
            DspWindowSanctionValidFrom.ReadOnly = True
            DspWindowSanctionIsApproxFromDate.ReadOnly = True
            DspWindowSanctionValidTo.ReadOnly = True
            DspWindowSanctionIsApproxToDate.ReadOnly = True
            DisplayWindowSanctionComments.ReadOnly = True

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub BtnCancelRelatedPersonDirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowRelatedPerson.Hide()
            'FormPanelRelatedPerson.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Function GetWICByPKIDDetailRelatedPerson(id As Long) As List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)
        Dim WIC As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT WIC_NO FROM goAML_Ref_WIC WHERE PK_Customer_ID = " & id, Nothing)
        Dim ListRelatedPerson = New List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)
        Dim objRelatedPersonDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM goAML_Ref_WIC_Related_Person WHERE WIC_NO = '" & WIC & "'", Nothing)
        Dim ListNewRelatedPerson As New List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)
        For Each itemRelatedPerson As DataRow In objRelatedPersonDataTable.Rows
            Dim NewRelatedPerson As New WICDataBLL.goAML_Ref_WIC_Related_Person
            If Not IsDBNull(itemRelatedPerson("PK_goAML_Ref_WIC_Related_Person_ID")) Then
                NewRelatedPerson.PK_goAML_Ref_WIC_Related_Person_ID = itemRelatedPerson("PK_goAML_Ref_WIC_Related_Person_ID")
            End If
            If Not IsDBNull(itemRelatedPerson("WIC_No")) Then
                NewRelatedPerson.WIC_No = itemRelatedPerson("WIC_No")
            End If
            If Not IsDBNull(itemRelatedPerson("Person_Person_Relation")) Then
                NewRelatedPerson.Person_Person_Relation = itemRelatedPerson("Person_Person_Relation")
            End If
            If Not IsDBNull(itemRelatedPerson("Relation_Date_Range_Valid_From")) Then
                NewRelatedPerson.Relation_Date_Range_Valid_From = itemRelatedPerson("Relation_Date_Range_Valid_From")
            End If
            If Not IsDBNull(itemRelatedPerson("Relation_Date_Range_Is_Approx_From_Date")) Then
                NewRelatedPerson.Relation_Date_Range_Is_Approx_From_Date = itemRelatedPerson("Relation_Date_Range_Is_Approx_From_Date")
            End If
            If Not IsDBNull(itemRelatedPerson("Relation_Date_Range_Valid_To")) Then
                NewRelatedPerson.Relation_Date_Range_Valid_To = itemRelatedPerson("Relation_Date_Range_Valid_To")
            End If
            If Not IsDBNull(itemRelatedPerson("Relation_Date_Range_Is_Approx_To_Date")) Then
                NewRelatedPerson.Relation_Date_Range_Is_Approx_To_Date = itemRelatedPerson("Relation_Date_Range_Is_Approx_To_Date")
            End If
            If Not IsDBNull(itemRelatedPerson("Comments")) Then
                NewRelatedPerson.Comments = itemRelatedPerson("Comments")
            End If
            If Not IsDBNull(itemRelatedPerson("Gender")) Then
                NewRelatedPerson.Gender = itemRelatedPerson("Gender")
            End If
            If Not IsDBNull(itemRelatedPerson("Title")) Then
                NewRelatedPerson.Title = itemRelatedPerson("Title")
            End If
            If Not IsDBNull(itemRelatedPerson("First_Name")) Then
                NewRelatedPerson.First_Name = itemRelatedPerson("First_Name")
            End If
            If Not IsDBNull(itemRelatedPerson("Middle_Name")) Then
                NewRelatedPerson.Middle_Name = itemRelatedPerson("Middle_Name")
            End If
            If Not IsDBNull(itemRelatedPerson("Prefix")) Then
                NewRelatedPerson.Prefix = itemRelatedPerson("Prefix")
            End If
            If Not IsDBNull(itemRelatedPerson("Last_Name")) Then
                NewRelatedPerson.Last_Name = itemRelatedPerson("Last_Name")
            End If
            If Not IsDBNull(itemRelatedPerson("Birth_Date")) Then
                NewRelatedPerson.Birth_Date = itemRelatedPerson("Birth_Date")
            End If
            If Not IsDBNull(itemRelatedPerson("Birth_Place")) Then
                NewRelatedPerson.Birth_Place = itemRelatedPerson("Birth_Place")
            End If
            If Not IsDBNull(itemRelatedPerson("Country_Of_Birth")) Then
                NewRelatedPerson.Country_Of_Birth = itemRelatedPerson("Country_Of_Birth")
            End If
            If Not IsDBNull(itemRelatedPerson("Mother_Name")) Then
                NewRelatedPerson.Mother_Name = itemRelatedPerson("Mother_Name")
            End If
            If Not IsDBNull(itemRelatedPerson("Alias")) Then
                NewRelatedPerson._Alias = itemRelatedPerson("Alias")
            End If
            If Not IsDBNull(itemRelatedPerson("Full_Name_Frn")) Then
                NewRelatedPerson.Full_Name_Frn = itemRelatedPerson("Full_Name_Frn")
            End If
            If Not IsDBNull(itemRelatedPerson("SSN")) Then
                NewRelatedPerson.SSN = itemRelatedPerson("SSN")
            End If
            If Not IsDBNull(itemRelatedPerson("Passport_Number")) Then
                NewRelatedPerson.Passport_Number = itemRelatedPerson("Passport_Number")
            End If
            If Not IsDBNull(itemRelatedPerson("Passport_Country")) Then
                NewRelatedPerson.Passport_Country = itemRelatedPerson("Passport_Country")
            End If
            If Not IsDBNull(itemRelatedPerson("ID_Number")) Then
                NewRelatedPerson.ID_Number = itemRelatedPerson("ID_Number")
            End If
            If Not IsDBNull(itemRelatedPerson("Nationality1")) Then
                NewRelatedPerson.Nationality1 = itemRelatedPerson("Nationality1")
            End If
            If Not IsDBNull(itemRelatedPerson("Nationality2")) Then
                NewRelatedPerson.Nationality2 = itemRelatedPerson("Nationality2")
            End If
            If Not IsDBNull(itemRelatedPerson("Nationality3")) Then
                NewRelatedPerson.Nationality3 = itemRelatedPerson("Nationality3")
            End If
            If Not IsDBNull(itemRelatedPerson("Residence")) Then
                NewRelatedPerson.Residence = itemRelatedPerson("Residence")
            End If
            If Not IsDBNull(itemRelatedPerson("Residence_Since")) Then
                NewRelatedPerson.Residence_Since = itemRelatedPerson("Residence_Since")
            End If
            If Not IsDBNull(itemRelatedPerson("Occupation")) Then
                NewRelatedPerson.Occupation = itemRelatedPerson("Occupation")
            End If
            If Not IsDBNull(itemRelatedPerson("Deceased")) Then
                NewRelatedPerson.Deceased = itemRelatedPerson("Deceased")
            End If
            If Not IsDBNull(itemRelatedPerson("Date_Deceased")) Then
                NewRelatedPerson.Date_Deceased = itemRelatedPerson("Date_Deceased")
            End If
            If Not IsDBNull(itemRelatedPerson("Tax_Number")) Then
                NewRelatedPerson.Tax_Number = itemRelatedPerson("Tax_Number")
            End If
            If Not IsDBNull(itemRelatedPerson("Is_PEP")) Then
                NewRelatedPerson.Is_PEP = IIf(itemRelatedPerson("Is_PEP") = "1", True, False)
            End If
            If Not IsDBNull(itemRelatedPerson("Source_Of_Wealth")) Then
                NewRelatedPerson.Source_Of_Wealth = itemRelatedPerson("Source_Of_Wealth")
            End If
            If Not IsDBNull(itemRelatedPerson("Is_Protected")) Then
                NewRelatedPerson.Is_Protected = itemRelatedPerson("Is_Protected")
            End If
            If Not IsDBNull(itemRelatedPerson("Active")) Then
                NewRelatedPerson.Active = itemRelatedPerson("Active")
            End If
            If Not IsDBNull(itemRelatedPerson("CreatedBy")) Then
                NewRelatedPerson.CreatedBy = itemRelatedPerson("CreatedBy")
            End If
            If Not IsDBNull(itemRelatedPerson("LastUpdateBy")) Then
                NewRelatedPerson.LastUpdateBy = itemRelatedPerson("LastUpdateBy")
            End If
            If Not IsDBNull(itemRelatedPerson("ApprovedBy")) Then
                NewRelatedPerson.ApprovedBy = itemRelatedPerson("ApprovedBy")
            End If
            If Not IsDBNull(itemRelatedPerson("CreatedDate")) Then
                NewRelatedPerson.CreatedDate = itemRelatedPerson("CreatedDate")
            End If
            If Not IsDBNull(itemRelatedPerson("LastUpdateDate")) Then
                NewRelatedPerson.LastUpdateDate = itemRelatedPerson("LastUpdateDate")
            End If
            If Not IsDBNull(itemRelatedPerson("ApprovedDate")) Then
                NewRelatedPerson.ApprovedDate = itemRelatedPerson("LastUpdateDate")
            End If
            If Not IsDBNull(itemRelatedPerson("Alternateby")) Then
                NewRelatedPerson.Alternateby = itemRelatedPerson("Alternateby")
            End If

            ' 2023-11-10, Nael
            If Not IsDBNull(itemRelatedPerson("EMPLOYER_NAME")) Then
                NewRelatedPerson.EMPLOYER_NAME = itemRelatedPerson("EMPLOYER_NAME")
            End If
            If Not IsDBNull(itemRelatedPerson("relation_comments")) Then
                NewRelatedPerson.relation_comments = itemRelatedPerson("relation_comments")
            End If

            Dim pk_id = itemRelatedPerson("PK_goAML_Ref_WIC_Related_Person_ID")

            NewRelatedPerson.ObjList_GoAML_Ref_Address = goAML_Customer_Service.GetAddressByPkFk(pk_id, 31)
            NewRelatedPerson.ObjList_GoAML_Ref_Address_Work = goAML_Customer_Service.GetAddressByPkFk(pk_id, 32)
            NewRelatedPerson.ObjList_GoAML_Ref_Phone = goAML_Customer_Service.GetPhoneByPkFk(pk_id, 31)
            NewRelatedPerson.ObjList_GoAML_Ref_Phone_Work = goAML_Customer_Service.GetPhoneByPkFk(pk_id, 32)
            NewRelatedPerson.ObjList_GoAML_Person_Identification = goAML_Customer_Service.GetIdentificationPkFk(pk_id, 31)
            NewRelatedPerson.ObjList_GoAML_Ref_Customer_Email = goAML_Customer_Service.WICEmailService.GetByPkFk(pk_id, 31)
            NewRelatedPerson.ObjList_GoAML_Ref_Customer_Sanction = goAML_Customer_Service.WICSanctionService.GetByPkFk(pk_id, 31)
            NewRelatedPerson.ObjList_GoAML_Ref_Customer_PEP = goAML_Customer_Service.WICPersonPEPService.GetByPkFk(pk_id, 31)

            ListNewRelatedPerson.Add(NewRelatedPerson)
        Next
        ListRelatedPerson = ListNewRelatedPerson
        Return ListRelatedPerson
    End Function
    Private Sub DetailRecordRelatedPerson(id As String)
        Try

            Dim listRelatedPerson As New DataTable
            listRelatedPerson = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM [dbo].[goAML_Ref_WIC_Related_Person] WHERE PK_goAML_Ref_WIC_Related_Person_ID = '" & id & "'")

            If listRelatedPerson IsNot Nothing Then
                If listRelatedPerson.Rows.Count > 0 Then
                    For Each item As DataRow In listRelatedPerson.Rows
                        'DspPersonPersonRelation.Value = item("Person_Person_Relation")
                        'DspRelationDateRangeValidFrom.Value = item("Relation_Date_Range_Valid_From")
                        'DspRelationDateRangeIsApproxFromDate.Value = item("Relation_Date_Range_Is_Approx_From_Date")
                        'DspRelationDateRangeValidTo.Value = item("Relation_Date_Range_Valid_To")
                        'DspRelationDateRangeIsApproxToDate.Value = item("Relation_Date_Range_Is_Approx_To_Date")
                        'DspWindowRelatedPersonComments.Value = item("Comments")
                        'DspWindowGender.Value = item("Gender")
                        'DspTitle.Value = item("Title")
                        'DspFirstName.Value = item("First_Name")
                        'DspMiddleName.Value = item("Middle_Name")
                        'DspPrefix.Value = item("Prefix")
                        'DspLastName.Value = item("Last_Name")
                        'DspBirthDate.Value = item("Birth_Date")
                        'DspBirthPlace.Value = item("Birth_Place")
                        'DspCountryofBirth.Value = item("Country_Of_Birth")
                        'DspMotherName.Value = item("Mother_Name")
                        'DspAlias.Value = item("Alias")
                        'DspFullForeignName.Value = item("Full_Name_Frn")
                        'DspSSN.Value = item("SSN")
                        'DspPassportNumber.Value = item("Passport_Number")
                        'DspPassportCountry.Value = item("Passport_Country")
                        'DspIDNumber.Value = item("ID_Number")
                        'DspNationality1.Value = item("Nationality1")
                        'DspNationality2.Value = item("Nationality2")
                        'DspNationality3.Value = item("Nationality3")
                        'DspResidence.Value = item("Residence")
                        'DspResidenceSince.Value = item("Residence_Since")
                        'DspWindowOccupation.Value = item("Occupation")
                        'DspWindowDeceased.Value = item("Deceased")
                        'DspDateDeceased.Value = item("Date_Deceased")
                        'DspTaxNumber.Value = item("Tax_Number")
                        'DspIsPEP.Value = item("Is_PEP")
                        'DspWindowSourceofWealth.Value = item("Source_Of_Wealth")
                        'DspIsProtected.Value = item("Is_Protected")

                        Dim pk_id = item("PK_goAML_Ref_WIC_Related_Person_ID")

                        Dim address_list = goAML_Customer_Service.GetAddressByPkFk(pk_id, 31)
                        Dim address_work_list = goAML_Customer_Service.GetAddressByPkFk(pk_id, 32)
                        Dim phone_list = goAML_Customer_Service.GetPhoneByPkFk(pk_id, 31)
                        Dim phone_work_list = goAML_Customer_Service.GetPhoneByPkFk(pk_id, 32)
                        Dim identification_list = goAML_Customer_Service.GetIdentificationPkFk(pk_id, 31)
                        ' 2023-11-15, Nael
                        Dim email_list = goAML_Customer_Service.WICEmailService.GetByPkFk(pk_id, 31)
                        Dim sanction_list = goAML_Customer_Service.WICSanctionService.GetByPkFk(pk_id, 31)
                        Dim pep_list = goAML_Customer_Service.WICPersonPEPService.GetByPkFk(pk_id, 31)

                        GridAddress_RelatedPerson.LoadData(address_list)
                        GridAddressWork_RelatedPerson.LoadData(address_work_list)
                        GridPhone_RelatedPerson.LoadData(phone_list)
                        GridPhoneWork_RelatedPerson.LoadData(phone_work_list)
                        GridIdentification_RelatedPerson.LoadData(identification_list)

                        GridEmail_RelatedPerson.LoadData(email_list)
                        GridSanction_RelatedPerson.LoadData(sanction_list)
                        GridPEP_RelatedPerson.LoadData(pep_list)
                    Next
                End If
            End If

            'Set fields' ReadOnly
            'DspPersonPersonRelation.ReadOnly = True
            'DspRelationDateRangeValidFrom.ReadOnly = True
            'DspRelationDateRangeIsApproxFromDate.ReadOnly = True
            'DspRelationDateRangeValidTo.ReadOnly = True
            'DspRelationDateRangeIsApproxToDate.ReadOnly = True
            'DspWindowRelatedPersonComments.ReadOnly = True
            'DspWindowGender.ReadOnly = True
            'DspTitle.ReadOnly = True
            'DspFirstName.ReadOnly = True
            'DspMiddleName.ReadOnly = True
            'DspPrefix.ReadOnly = True
            'DspLastName.ReadOnly = True
            'DspBirthDate.ReadOnly = True
            'DspBirthPlace.ReadOnly = True
            'DspCountryofBirth.ReadOnly = True
            'DspMotherName.ReadOnly = True
            'DspAlias.ReadOnly = True
            'DspFullForeignName.ReadOnly = True
            'DspSSN.ReadOnly = True
            'DspPassportNumber.ReadOnly = True
            'DspPassportCountry.ReadOnly = True
            'DspIDNumber.ReadOnly = True
            'DspNationality1.ReadOnly = True
            'DspNationality2.ReadOnly = True
            'DspNationality3.ReadOnly = True
            'DspResidence.ReadOnly = True
            'DspResidenceSince.ReadOnly = True
            'DspWindowOccupation.ReadOnly = True
            'DspWindowDeceased.ReadOnly = True
            'DspDateDeceased.ReadOnly = True
            'DspTaxNumber.ReadOnly = True
            'DspIsPEP.ReadOnly = True
            'DspWindowSourceofWealth.ReadOnly = True
            'DspIsProtected.ReadOnly = True

        Catch ex As Exception

        End Try
    End Sub

#Region "goAML 5.0.1, add by Septian, 2023-03-02"
    Public Property objTempWIC_EntityUrl_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL
        Get
            Return Session("WIC_DETAIL_v501.objTempWIC_EntityUrl_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
            Session("WIC_DETAIL_v501.objTempWIC_EntityUrl_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityUrl() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
        Get
            Return Session("WIC_DETAIL_v501.objListgoAML_Ref_EntityUrl")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL))
            Session("WIC_DETAIL_v501.objListgoAML_Ref_EntityUrl") = value
        End Set
    End Property
    Public Property objListgoAML_vw_EntityUrl() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
        Get
            Return Session("WIC_DETAIL_v501.objListgoAML_vw_EntityUrl")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL))
            Session("WIC_DETAIL_v501.objListgoAML_vw_EntityUrl") = value
        End Set
    End Property

    Public Property objTempWIC_EntityIdentification_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification
        Get
            Return Session("WIC_DETAIL_v501.objTempWIC_EntityIdentification_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
            Session("WIC_DETAIL_v501.objTempWIC_EntityIdentification_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityIdentification() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        Get
            Return Session("WIC_DETAIL_v501.objListgoAML_Ref_EntityIdentification")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification))
            Session("WIC_DETAIL_v501.objListgoAML_Ref_EntityIdentification") = value
        End Set
    End Property
    Public Property objListgoAML_vw_EntityIdentification() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        Get
            Return Session("WIC_DETAIL_v501.objListgoAML_vw_EntityIdentification")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification))
            Session("WIC_DETAIL_v501.objListgoAML_vw_EntityIdentification") = value
        End Set
    End Property

    Public Property objTempWIC_RelatedEntities_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity
        Get
            Return Session("WIC_DETAIL_v501.objTempWIC_RelatedEntity_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
            Session("WIC_DETAIL_v501.objTempWIC_RelatedEntity_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_RelatedEntity() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        Get
            Return Session("WIC_DETAIL_v501.objListgoAML_Ref_RelatedEntity")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity))
            Session("WIC_DETAIL_v501.objListgoAML_Ref_RelatedEntity") = value
        End Set
    End Property
    Public Property objListgoAML_vw_RelatedEntity() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        Get
            Return Session("WIC_DETAIL_v501.objListgoAML_vw_RelatedEntity")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity))
            Session("WIC_DETAIL_v501.objListgoAML_vw_RelatedEntity") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_WIC_URL() As WICDataBLL.goAML_Ref_WIC_URL
        Get
            Return Session("WIC_DETAIL_v501.objTemp_goAML_Ref_WIC_URL")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_URL)
            Session("WIC_DETAIL_v501.objTemp_goAML_Ref_WIC_URL") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_WIC_Entity_Identification() As WICDataBLL.goAML_Ref_WIC_Entity_Identification
        Get
            Return Session("WIC_DETAIL_v501.objTemp_goAML_Ref_WIC_Entity_Identification")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Entity_Identification)
            Session("WIC_DETAIL_v501.objTemp_goAML_Ref_WIC_Entity_Identification") = value
        End Set
    End Property

    Public Property objTemp_goAML_Ref_WIC_Related_Entity() As WICDataBLL.goAML_Ref_WIC_Related_Entity
        Get
            Return Session("WIC_DETAIL_v501.objTemp_goAML_Ref_WIC_Related_Entity")
        End Get
        Set(ByVal value As WICDataBLL.goAML_Ref_WIC_Related_Entity)
            Session("WIC_DETAIL_v501.objTemp_goAML_Ref_WIC_Related_Entity") = value
        End Set
    End Property

    Protected Sub btnBackWIC_Url_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_URL.Hidden = True
            FormPanel_URL.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackWIC_EntityIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_EntityIdentification.Hidden = True
            FormPanel_EntityIdentification.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackWIC_RelatedEntities_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_RelatedEntities.Hidden = True
            FormPanel_RelatedEntities.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdWIC_EntityIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_EntityIdentification(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_EntityIdentification(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecord_EntityIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdWIC_URL(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_URL(id, False)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadData_URL(id, True)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                'DeleteRecord_URL(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_RelatedEntities(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadData_RelatedEntities(id, False)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Sub LoadData_EntityIdentification(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_WIC_Entity_Identification = objListgoAML_Ref_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id)
            objTempWIC_EntityIdentification_Edit = objListgoAML_vw_EntityIdentification.Find(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id)

            If Not objTempWIC_EntityIdentification_Edit Is Nothing Then
                FormPanel_EntityIdentification.Hidden = False
                WindowDetail_EntityIdentification.Hidden = False
                'btnSaveWIC_EntityIdentification.Hidden = Not isEdit

                cmb_ef_type.IsReadOnly = Not isEdit
                txt_ef_number.ReadOnly = Not isEdit
                df_issue_date.ReadOnly = Not isEdit
                df_expiry_date.ReadOnly = Not isEdit
                txt_ef_issued_by.ReadOnly = Not isEdit
                cmb_ef_issue_country.IsReadOnly = Not isEdit
                txt_ef_comments.ReadOnly = Not isEdit


                With objTempWIC_EntityIdentification_Edit
                    Dim refType = objgoAML_WIC.GetReferenceByCode("goAML_Ref_Entity_Identifier_Type", .TYPE)
                    Dim refIssue = objgoAML_WIC.GetReferenceByCode("goAML_Ref_Nama_Negara", .ISSUE_COUNTRY)

                    If refType IsNot Nothing Then
                        cmb_ef_type.SetTextWithTextValue(refType.Kode, refType.Keterangan)
                    End If

                    txt_ef_number.Value = .NUMBER
                    df_issue_date.Text = IIf(.ISSUE_DATE Is Nothing, "", .ISSUE_DATE)
                    df_expiry_date.Text = IIf(.EXPIRY_DATE Is Nothing, "", .EXPIRY_DATE)
                    txt_ef_issued_by.Value = .ISSUED_BY

                    If refIssue IsNot Nothing Then
                        cmb_ef_issue_country.SetTextWithTextValue(refIssue.Kode, refIssue.Keterangan)
                    End If

                    txt_ef_comments.Value = .COMMENTS

                End With

                WindowDetail_EntityIdentification.Title = "Entity Identification " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_URL(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_WIC_URL = objListgoAML_Ref_EntityUrl.Find(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id)
            objTempWIC_EntityUrl_Edit = objListgoAML_vw_EntityUrl.Find(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id)

            If Not objTempWIC_EntityUrl_Edit Is Nothing Then
                FormPanel_URL.Hidden = False
                WindowDetail_URL.Hidden = False
                'btnSaveWIC_URL.Hidden = Not isEdit

                txt_url.ReadOnly = Not isEdit

                With objTempWIC_EntityUrl_Edit
                    txt_url.Value = .URL
                End With

                WindowDetail_URL.Title = "Entity URL " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadData_RelatedEntities(id As Long, isEdit As Boolean)
        Try
            objTemp_goAML_Ref_WIC_Related_Entity = objListgoAML_Ref_RelatedEntity.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id)
            objTempWIC_RelatedEntities_Edit = objListgoAML_vw_RelatedEntity.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id)

            If Not objTempWIC_RelatedEntities_Edit Is Nothing Then
                GridPhone_RelatedEntity.IsViewMode = Not isEdit
                GridAddress_RelatedEntity.IsViewMode = Not isEdit
                GridEmail_RelatedEntity.IsViewMode = Not isEdit
                GridEntityIdentification_RelatedEntity.IsViewMode = Not isEdit
                GridURL_RelatedEntity.IsViewMode = Not isEdit
                GridSanction_RelatedEntity.IsViewMode = Not isEdit

                FormPanel_RelatedEntities.Hidden = False
                WindowDetail_RelatedEntities.Hidden = False
                'btnSaveWIC_RelatedEntities.Hidden = Not isEdit

                cmb_re_entity_relation.IsReadOnly = Not isEdit
                df_re_relation_date_range_valid_from.ReadOnly = Not isEdit
                cbx_re_relation_date_range_is_approx_from_date.ReadOnly = Not isEdit
                df_re_relation_date_range_valid_to.ReadOnly = Not isEdit
                cbx_re_relation_date_range_is_approx_to_date.ReadOnly = Not isEdit
                nfd_re_share_percentage.ReadOnly = Not isEdit
                txt_re_comments.ReadOnly = Not isEdit
                txt_re_relation_comments.ReadOnly = Not isEdit
                txt_re_name.ReadOnly = Not isEdit
                txt_re_commercial_name.ReadOnly = Not isEdit
                cmb_re_incorporation_legal_form.IsReadOnly = Not isEdit
                txt_re_incorporation_number.ReadOnly = Not isEdit
                txt_re_business.ReadOnly = Not isEdit
                cmb_re_entity_status.IsReadOnly = Not isEdit
                df_re_entity_status_date.ReadOnly = Not isEdit
                txt_re_incorporation_state.ReadOnly = Not isEdit
                cmb_re_incorporation_country_code.IsReadOnly = Not isEdit
                df_re_incorporation_date.ReadOnly = Not isEdit
                cbx_re_business_closed.ReadOnly = Not isEdit
                df_re_date_business_closed.ReadOnly = Not isEdit
                txt_re_tax_number.ReadOnly = Not isEdit
                txt_re_tax_reg_number.ReadOnly = Not isEdit


                With objTempWIC_RelatedEntities_Edit
                    Dim refRelation = objgoAML_WIC.GetReferenceByCode("goAML_Ref_Entity_Entity_Relationship", .ENTITY_ENTITY_RELATION)
                    Dim refLegalForm = objgoAML_WIC.GetReferenceByCode("goAML_Ref_Bentuk_Badan_Usaha", .INCORPORATION_LEGAL_FORM)
                    Dim refEntityStatus = objgoAML_WIC.GetReferenceByCode("goAML_Ref_Entity_Status", .ENTITY_STATUS)
                    Dim refCountry = objgoAML_WIC.GetReferenceByCode("goAML_Ref_Nama_Negara", .INCORPORATION_COUNTRY_CODE)

                    If refRelation IsNot Nothing Then
                        cmb_re_entity_relation.SetTextWithTextValue(refRelation.Kode, refRelation.Keterangan)
                    End If
                    'df_re_relation_date_range_valid_from.Text = .RELATION_DATE_RANGE_VALID_FROM
                    'cbx_re_relation_date_range_is_approx_from_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_re_relation_date_range_valid_to.Text = .RELATION_DATE_RANGE_VALID_TO
                    'cbx_re_relation_date_range_is_approx_to_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    nfd_re_share_percentage.Value = .SHARE_PERCENTAGE
                    txt_re_comments.Value = .COMMENTS
                    txt_re_relation_comments.Value = .RELATION_COMMENTS
                    txt_re_name.Value = .NAME
                    txt_re_commercial_name.Value = .COMMERCIAL_NAME
                    If refLegalForm IsNot Nothing Then
                        cmb_re_incorporation_legal_form.SetTextWithTextValue(refLegalForm.Kode, refLegalForm.Keterangan)
                    End If

                    txt_re_incorporation_number.Value = .INCORPORATION_NUMBER
                    txt_re_business.Value = .BUSINESS
                    If refEntityStatus IsNot Nothing Then
                        cmb_re_entity_status.SetTextWithTextValue(refEntityStatus.Kode, refEntityStatus.Keterangan)
                    End If

                    df_re_entity_status_date.Text = .ENTITY_STATUS
                    txt_re_incorporation_state.Value = .INCORPORATION_STATE
                    If refCountry IsNot Nothing Then
                        cmb_re_incorporation_country_code.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                    End If

                    df_re_incorporation_date.Text = IIf(.INCORPORATION_DATE Is Nothing, "", .INCORPORATION_DATE)
                    cbx_re_business_closed.Checked = .BUSINESS_CLOSED
                    If .BUSINESS_CLOSED Then
                        df_re_date_business_closed.Hidden = False
                        df_re_date_business_closed.Text = IIf(.DATE_BUSINESS_CLOSED Is Nothing, "", .DATE_BUSINESS_CLOSED)
                    End If
                    txt_re_tax_number.Value = .TAX_NUMBER
                    txt_re_tax_reg_number.Value = .TAX_REG_NUMBER

                    Dim phone_list = If(.ObjList_GoAML_Ref_Customer_Phone IsNot Nothing, .ObjList_GoAML_Ref_Customer_Phone,
                        goAML_Customer_Service.GetPhoneByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim address_list = If(.ObjList_GoAML_Ref_Customer_Address IsNot Nothing, .ObjList_GoAML_Ref_Customer_Address,
                        goAML_Customer_Service.GetAddressByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim email_list = If(.ObjList_GoAML_Ref_Customer_Email IsNot Nothing, .ObjList_GoAML_Ref_Customer_Email,
                        goAML_Customer_Service.WICEmailService.GetByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim entity_identification_list = If(.ObjList_GoAML_Ref_Customer_Entity_Identification IsNot Nothing, .ObjList_GoAML_Ref_Customer_Entity_Identification,
                        goAML_Customer_Service.WICEntityIdentificationService.GetByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim url_list = If(.ObjList_GoAML_Ref_Customer_URL IsNot Nothing, .ObjList_GoAML_Ref_Customer_URL,
                        goAML_Customer_Service.WICUrlService.GetByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim sanction_list = If(.ObjList_GoAML_Ref_Customer_Sanction IsNot Nothing, .ObjList_GoAML_Ref_Customer_Sanction,
                        goAML_Customer_Service.WICSanctionService.GetByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))

                    GridPhone_RelatedEntity.LoadData(phone_list)
                    GridAddress_RelatedEntity.LoadData(address_list)
                    GridEmail_RelatedEntity.LoadData(email_list)
                    GridEntityIdentification_RelatedEntity.LoadData(entity_identification_list)
                    GridURL_RelatedEntity.LoadData(url_list)
                    GridSanction_RelatedEntity.LoadData(sanction_list)
                End With

                WindowDetail_RelatedEntities.Title = "Related Entity " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub BindData()
        Dim wicData = objgoAML_WIC.GetWICData(ObjWIC.WIC_No)
        BindForm()
        LoadObject(wicData)
        BindDetailEmail()
        BindDetailPersonPEP()
        BindDetailSanction()
        BindDetailRelatedPerson()
        BindDetail_Url(GPWIC_CORP_URL.GetStore, wicData.ObjGrid.ObjList_GoAML_URL)
        BindDetail_EntityIdentification(GPWIC_CORP_EntityIdentification.GetStore, wicData.ObjGrid.ObjList_GoAML_EntityIdentification)
        BindDetail_RelatedEntity(GPWIC_CORP_RelatedEntities.GetStore, wicData.ObjGrid.ObjList_GoAML_RelatedEntity)
    End Sub
    Private Sub LoadObject(objWicNew As GoAMLBLL.WICDataBLL)
        If objWicNew.ObjGrid IsNot Nothing Then
            'objListgoAML_Ref_Email = objWicNew.ObjGrid.ObjList_GoAML_Email
            'objListgoAML_Ref_PersonPEP_New = objWicNew.ObjGrid.ObjList_GoAML_PersonPEP
            'objListgoAML_Ref_Sanction_New = objWicNew.ObjGrid.ObjList_GoAML_Sanction
            'objListgoAML_Ref_RelatedPerson_New = objWicNew.ObjGrid.ObjList_GoAML_RelatedPerson
            objListgoAML_Ref_EntityUrl = objWicNew.ObjGrid.ObjList_GoAML_URL
            objListgoAML_Ref_EntityIdentification = objWicNew.ObjGrid.ObjList_GoAML_EntityIdentification
            objListgoAML_Ref_RelatedEntity = objWicNew.ObjGrid.ObjList_GoAML_RelatedEntity

            objListgoAML_vw_EntityUrl = objWicNew.ObjGrid.ObjList_GoAML_URL
            objListgoAML_vw_EntityIdentification = objWicNew.ObjGrid.ObjList_GoAML_EntityIdentification
            objListgoAML_vw_RelatedEntity = objWicNew.ObjGrid.ObjList_GoAML_RelatedEntity
        End If
    End Sub

    Sub BindForm()
        Dim wicData = objgoAML_WIC.GetWIC(ObjWIC.WIC_No)

        DspIsRealWIC.Text = If(wicData.IS_REAL_WIC, "True", "False")
        TxtINDV_Is_Protected.Text = If(wicData.IS_PROTECTED, "True", "False")
    End Sub

    Private Sub BindDetail_Url(store As Store, list As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL))
        If list IsNot Nothing Then
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            store.DataSource = objtable
            store.DataBind()
        End If
    End Sub

    Private Sub BindDetail_EntityIdentification(store As Store, list As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification))
        If list IsNot Nothing Then
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            store.DataSource = objtable
            store.DataBind()
        End If
    End Sub

    Private Sub BindDetail_RelatedEntity(store As Store, list As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity))
        If list IsNot Nothing Then
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            store.DataSource = objtable
            store.DataBind()
        End If
    End Sub
#End Region

    Private Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs) Handles BtnConfirmation.DirectClick
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & objmodule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

End Class
