﻿Imports System.ComponentModel
Imports Ext
Imports NawaBLL
Imports GoAMLBLL
Imports GoAMLDAL
Imports Elmah
Imports System.Data
Imports NPOI.HSSF.Record.Formula.Functions
Imports NPOI.Util.Collections
Imports OfficeOpenXml.FormulaParsing.Excel.Functions.Information
Imports System.Activities.Expressions
Imports GoAMLBLL.goAML.Services

Partial Class goAML_WICApprovalDetail_V501
    Inherits Parent

#Region "Session"
    Public Property ObjApproval As NawaDAL.ModuleApproval
        Get
            Return Session("goAML_WICApprovalDetail_V501.ObjApproval")
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            Session("goAML_WICApprovalDetail_V501.ObjApproval") = value
        End Set
    End Property
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("goAML_WICApprovalDetail_V501.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_WICApprovalDetail_V501.ObjModule") = value
        End Set
    End Property
    Public Property objWICNew() As GoAMLBLL.WICDataBLL
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICNew")
        End Get
        Set(value As GoAMLBLL.WICDataBLL)
            Session("goAML_WICApprovalDetail_V501.objWICNew") = value
        End Set
    End Property
    Public Property objWICOld() As GoAMLBLL.WICDataBLL
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICOld")
        End Get
        Set(value As GoAMLBLL.WICDataBLL)
            Session("goAML_WICApprovalDetail_V501.objWICOld") = value
        End Set
    End Property
    Public Property objWICDirectorNew() As List(Of GoAMLBLL.WICDirectorDataBLL)
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICDirectorNew")
        End Get
        Set(value As List(Of GoAMLBLL.WICDirectorDataBLL))
            Session("goAML_WICApprovalDetail_V501.objWICDirectorNew") = value
        End Set
    End Property
    Public Property objWICDirectorOld() As List(Of GoAMLBLL.WICDirectorDataBLL)
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICDirectorOld")
        End Get
        Set(value As List(Of GoAMLBLL.WICDirectorDataBLL))
            Session("goAML_WICApprovalDetail_V501.objWICDirectorOld") = value
        End Set
    End Property
    Public Property objWIC() As goAML_Ref_WIC
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWIC")
        End Get
        Set(value As goAML_Ref_WIC)
            Session("goAML_WICApprovalDetail_V501.objWIC") = value
        End Set
    End Property
    Public Property objWICBefore() As goAML_Ref_WIC
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICBefore")
        End Get
        Set(value As goAML_Ref_WIC)
            Session("goAML_WICApprovalDetail_V501.objWICBefore") = value
        End Set
    End Property
    Public Property objWICDirector() As goAML_Ref_Walk_In_Customer_Director
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICDirector")
        End Get
        Set(value As goAML_Ref_Walk_In_Customer_Director)
            Session("goAML_WICApprovalDetail_V501.objWICDirector") = value
        End Set
    End Property
    Public Property objWICDirectorBefore() As goAML_Ref_Walk_In_Customer_Director
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICDirectorBefore")
        End Get
        Set(value As goAML_Ref_Walk_In_Customer_Director)
            Session("goAML_WICApprovalDetail_V501.objWICDirectorBefore") = value
        End Set
    End Property
    Public Property objWICPhone() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICPhone")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail_V501.objWICPhone") = value
        End Set
    End Property
    Public Property objWICPhoneBefore() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICPhoneBefore")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail_V501.objWICPhoneBefore") = value
        End Set
    End Property
    Public Property objWICPhoneEmployer() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICPhoneEmployer")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail_V501.objWICPhoneEmployer") = value
        End Set
    End Property
    Public Property objWICPhoneEmployerBefore() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICPhoneEmployerBefore")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail_V501.objWICPhoneEmployerBefore") = value
        End Set
    End Property
    Public Property objWICPhoneDirector() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICPhoneDirector")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail_V501.objWICPhoneDirector") = value
        End Set
    End Property
    Public Property objWICPhoneDirectorBefore() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICPhoneDirectorBefore")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail_V501.objWICPhoneDirectorBefore") = value
        End Set
    End Property
    Public Property objWICPhoneDirectorEmployer() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICPhoneDirectorEmployer")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail_V501.objWICPhoneDirectorEmployer") = value
        End Set
    End Property
    Public Property objWICPhoneDirectorEmployerBefore() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICPhoneDirectorEmployerBefore")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail_V501.objWICPhoneDirectorEmployerBefore") = value
        End Set
    End Property
    Public Property objWICAddress() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICAddress")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail_V501.objWICAddress") = value
        End Set
    End Property
    Public Property objWICAddressBefore() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICAddressBefore")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail_V501.objWICAddressBefore") = value
        End Set
    End Property
    Public Property objWICAddressEmployer() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICAddressEmployer")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail_V501.objWICAddressEmployer") = value
        End Set
    End Property
    Public Property objWICAddressEmployerBefore() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICAddressEmployerBefore")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail_V501.objWICAddressEmployerBefore") = value
        End Set
    End Property
    Public Property objWICAddressDirector() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICAddressDirector")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail_V501.objWICAddressDirector") = value
        End Set
    End Property
    Public Property objWICAddressDirectorBefore() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICAddressDirectorBefore")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail_V501.objWICAddressDirectorBefore") = value
        End Set
    End Property
    Public Property objWICAddressDirectorEmployer() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICAddressDirectorEmployer")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail_V501.objWICAddressDirectorEmployer") = value
        End Set
    End Property
    Public Property objWICAddressDirectorEmployerBefore() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICAddressDirectorEmployerBefore")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail_V501.objWICAddressDirectorEmployerBefore") = value
        End Set
    End Property
    Public Property objWICIdentification() As goAML_Person_Identification
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICIdentification")
        End Get
        Set(value As goAML_Person_Identification)
            Session("goAML_WICApprovalDetail_V501.objWICIdentification") = value
        End Set
    End Property
    Public Property objWICIdentificationBefore() As goAML_Person_Identification
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICIdentificationBefore")
        End Get
        Set(value As goAML_Person_Identification)
            Session("goAML_WICApprovalDetail_V501.objWICIdentificationBefore") = value
        End Set
    End Property
    Public Property objWICIdentificationDirector() As goAML_Person_Identification
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICIdentificationDirector")
        End Get
        Set(value As goAML_Person_Identification)
            Session("goAML_WICApprovalDetail_V501.objWICIdentificationDirector") = value
        End Set
    End Property
    Public Property objWICIdentificationDirectorBefore() As goAML_Person_Identification
        Get
            Return Session("goAML_WICApprovalDetail_V501.objWICIdentificationDirectorBefore")
        End Get
        Set(value As goAML_Person_Identification)
            Session("goAML_WICApprovalDetail_V501.objWICIdentificationDirectorBefore") = value
        End Set
    End Property
    Public Property ListObjWICAddress() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICAddress")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail_V501.ListObjWICAddress") = value
        End Set
    End Property
    Public Property ListObjWICAddressBefore() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICAddressBefore")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail_V501.ListObjWICAddressBefore") = value
        End Set
    End Property
    Public Property ListObjWICAddressEmployer() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICAddressEmployer")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail_V501.ListObjWICAddressEmployer") = value
        End Set
    End Property
    Public Property ListObjWICAddressEmployerBefore() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICAddressEmployerBefore")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail_V501.ListObjWICAddressEmployerBefore") = value
        End Set
    End Property
    Public Property ListObjWICAddressDirector() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICAddressDirector")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail_V501.ListObjWICAddressDirector") = value
        End Set
    End Property
    Public Property ListObjWICAddressDirectorBefore() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICAddressDirectorBefore")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail_V501.ListObjWICAddressDirectorBefore") = value
        End Set
    End Property
    Public Property ListObjWICAddressDirectorEmployer() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICAddressDirectorEmployer")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail_V501.ListObjWICAddressDirectorEmployer") = value
        End Set
    End Property
    Public Property ListObjWICAddressDirectorEmployerBefore() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICAddressDirectorEmployerBefore")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail_V501.ListObjWICAddressDirectorEmployerBefore") = value
        End Set
    End Property
    Public Property ListObjWICIdentification() As List(Of goAML_Person_Identification)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICIdentification")
        End Get
        Set(value As List(Of goAML_Person_Identification))
            Session("goAML_WICApprovalDetail_V501.ListObjWICIdentification") = value
        End Set
    End Property
    Public Property ListObjWICIdentificationBefore() As List(Of goAML_Person_Identification)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICIdentificationBefore")
        End Get
        Set(value As List(Of goAML_Person_Identification))
            Session("goAML_WICApprovalDetail_V501.ListObjWICIdentificationBefore") = value
        End Set
    End Property
    Public Property ListObjWICIdentificationDirector() As List(Of goAML_Person_Identification)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICIdentificationDirector")
        End Get
        Set(value As List(Of goAML_Person_Identification))
            Session("goAML_WICApprovalDetail_V501.ListObjWICIdentificationDirector") = value
        End Set
    End Property
    Public Property ListObjWICIdentificationDirectorBefore() As List(Of goAML_Person_Identification)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICIdentificationDirectorBefore")
        End Get
        Set(value As List(Of goAML_Person_Identification))
            Session("goAML_WICApprovalDetail_V501.ListObjWICIdentificationDirectorBefore") = value
        End Set
    End Property
    Public Property ListObjWICPhone() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICPhone")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail_V501.ListObjWICPhone") = value
        End Set
    End Property
    Public Property ListObjWICPhoneBefore() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICPhoneBefore")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail_V501.ListObjWICPhoneBefore") = value
        End Set
    End Property
    Public Property ListObjWICPhoneEmployer() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICPhoneEmployer")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail_V501.ListObjWICPhoneEmployer") = value
        End Set
    End Property
    Public Property ListObjWICPhoneEmployerBefore() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICPhonEmployereBefore")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail_V501.ListObjWICPhonEmployereBefore") = value
        End Set
    End Property
    Public Property ListObjWICPhoneDirector() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICPhoneDirector")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail_V501.ListObjWICPhoneDirector") = value
        End Set
    End Property
    Public Property ListObjWICPhoneDirectorBefore() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICPhoneDirectorBefore")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail_V501.ListObjWICPhoneDirectorBefore") = value
        End Set
    End Property
    Public Property ListObjWICPhoneDirectorEmployer() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICPhoneDirectorEmployer")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail_V501.ListObjWICPhoneDirectorEmployer") = value
        End Set
    End Property
    Public Property ListObjWICPhoneDirectorEmployerBefore() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICPhoneDirectorEmployerBefore")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail_V501.ListObjWICPhoneDirectorEmployerBefore") = value
        End Set
    End Property
    Public Property ListObjWICDirector() As List(Of goAML_Ref_Walk_In_Customer_Director)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICDirector")
        End Get
        Set(value As List(Of goAML_Ref_Walk_In_Customer_Director))
            Session("goAML_WICApprovalDetail_V501.ListObjWICDirector") = value
        End Set
    End Property
    Public Property ListObjWICDirectorBefore() As List(Of goAML_Ref_Walk_In_Customer_Director)
        Get
            Return Session("goAML_WICApprovalDetail_V501.ListObjWICDirectorBefore")
        End Get
        Set(value As List(Of goAML_Ref_Walk_In_Customer_Director))
            Session("goAML_WICApprovalDetail_V501.ListObjWICDirectorBefore") = value
        End Set
    End Property

    '' Obj List Ref, add By Septian, 2023-03-03
    Public Property objTempWIC_Email_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Email
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_Email_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Email)
            Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_Email_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Email_Old() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Email)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_Email_Old")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Email))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_Email_Old") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Email_New() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Email)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_Email_New")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Email))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_Email_New") = value
        End Set
    End Property

    Public Property objTempWIC_PersonPEP_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Person_PEP
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_PersonPEP_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Person_PEP)
            Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_PersonPEP_Edit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_PersonPEP_Old() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Person_PEP)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_PersonPEP_Old")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Person_PEP))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_PersonPEP_Old") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_PersonPEP_New() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Person_PEP)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_PersonPEP_New")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Person_PEP))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_PersonPEP_New") = value
        End Set
    End Property

    Public Property objTempWIC_Sanction_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Sanction
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_Sanction_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Sanction)
            Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_Sanction_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_Sanction_Old() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Sanction)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_Sanction_Old")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Sanction))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_Sanction_Old") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_Sanction_New() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Sanction)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_Sanction_New")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Sanction))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_Sanction_New") = value
        End Set
    End Property
    Public Property objTempWIC_RelatedPerson_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Person
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_RelatedPerson_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Person)
            Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_RelatedPerson_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_RelatedPerson_Old() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Person)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_RelatedPerson_Old")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Person))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_RelatedPerson_Old") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_RelatedPerson_New() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Person)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_RelatedPerson_New")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Person))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_RelatedPerson_New") = value
        End Set
    End Property

    Public Property objTempWIC_EntityUrl_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_EntityUrl_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
            Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_EntityUrl_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityUrl_Old() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_EntityUrl_Old")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_EntityUrl_Old") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityUrl_New() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_EntityUrl_New")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_EntityUrl_New") = value
        End Set
    End Property
    Public Property objTempWIC_EntityIdentification_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_EntityIdentification_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
            Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_EntityIdentification_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityIdentification_Old() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_EntityIdentification_Old")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_EntityIdentification_Old") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_EntityIdentification_New() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_EntityIdentification_New")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_EntityIdentification_New") = value
        End Set
    End Property
    Public Property objTempWIC_RelatedEntity_Edit() As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_RelatedEntities_Edit")
        End Get
        Set(ByVal value As GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
            Session("WIC_APPROVAL_DETAIL_v501.objTempWIC_RelatedEntities_Edit") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_RelatedEntity_Old() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_RelatedEntity_Old")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_RelatedEntity_Old") = value
        End Set
    End Property
    Public Property objListgoAML_Ref_RelatedEntity_New() As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        Get
            Return Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_RelatedEntity_New")
        End Get
        Set(ByVal value As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity))
            Session("WIC_APPROVAL_DETAIL_v501.objListgoAML_Ref_RelatedEntity_New") = value
        End Set
    End Property
    '' End of ObjList Ref
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try

            If Not Net.X.IsAjaxRequest Then
                GridAddressWork_RelatedPerson.IsViewMode = True
                GridAddress_RelatedPerson.IsViewMode = True
                GridPhone_RelatedPerson.IsViewMode = True
                GridPhoneWork_RelatedPerson.IsViewMode = True
                GridIdentification_RelatedPerson.IsViewMode = True
                GridEmail_RelatedPerson.IsViewMode = True
                GridSanction_RelatedPerson.IsViewMode = True
                GridPEP_RelatedPerson.IsViewMode = True
                GridEmail_RelatedEntity.IsViewMode = True
                GridEntityIdentification_RelatedEntity.IsViewMode = True
                GridURL_RelatedEntity.IsViewMode = True
                GridSanction_RelatedEntity.IsViewMode = True
                GridEmail_Director.IsViewMode = True
                GridSanction_Director.IsViewMode = True
                GridPEP_Director.IsViewMode = True

                ClearSession()
                Dim moduleStr As String = Request.Params("ModuleID")
                Dim moduleID As Integer = Common.DecryptQueryString(moduleStr, SystemParameterBLL.GetEncriptionKey)
                ObjModule = ModuleBLL.GetModuleByModuleID(moduleID)

                Dim dataStr As String = Request.Params("ID")
                Dim dataID As Long = Common.DecryptQueryString(dataStr, SystemParameterBLL.GetEncriptionKey)
                ObjApproval = ModuleApprovalBLL.GetModuleApprovalByID(dataID)

                If ObjModule Is Nothing Then
                    Throw New Exception("Module not found")
                End If

                If Not ObjApproval Is Nothing Then
                    'Validasi hak akses & created by
                    If ObjApproval.CreatedBy <> Common.SessionCurrentUser.UserID Then
                        If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Approval) Then
                            Dim strIDCode As String = 1
                            strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                            Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                            Exit Sub
                        End If
                    End If

                    PanelInfo.Title = ObjModule.ModuleLabel & " Approval"
                    lblModuleName.Text = ObjModule.ModuleLabel
                    lblModuleKey.Text = ObjApproval.ModuleKey
                    lblAction.Text = ModuleBLL.GetModuleActionNamebyID(ObjApproval.PK_ModuleAction_ID)
                    LblCreatedBy.Text = ObjApproval.CreatedBy
                    lblCreatedDate.Text = ObjApproval.CreatedDate.Value.ToString("dd-MMM-yyyy")

                    LoadPopup()
                    LoadColumn()


                Else
                    Throw New Exception("Invalid ID Approval")
                End If

                'FormPanelOld.Visible = (ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update)
                BtnSave.Visible = ObjApproval.CreatedBy <> Common.SessionCurrentUser.UserID
                BtnReject.Visible = ObjApproval.CreatedBy <> Common.SessionCurrentUser.UserID

                Select Case ObjApproval.PK_ModuleAction_ID
                    Case Common.ModuleActionEnum.Insert
                        FormPanelOld.Visible = False
                        'FormPanelNew.Hidden = False
                        'Dim unikID As String = Guid.NewGuid.ToString
                        'Dim objWIC As New WICBLL
                        'objWIC.LoadPanel(FormPanelNew, ObjApproval.ModuleField, unikID)

                        objWICNew = Common.Deserialize(ObjApproval.ModuleField, GetType(WICDataBLL))

                        ListObjWICPhone = objWICNew.ObjWICPhone
                        ListObjWICAddress = objWICNew.ObjWICAddress

                        PKWIC.Text = objWICNew.ObjWIC.PK_Customer_ID
                        DspWICNo.Text = objWICNew.ObjWIC.WIC_No
                        If objWICNew.ObjWIC.isUpdateFromDataSource = True Then
                            DspIsUpdateFromDataSource.Text = "True"
                        Else
                            DspIsUpdateFromDataSource.Text = "False"
                        End If

                        'goAML 5.0.1, Add by Septian, 2023-03-03'
                        LoadData(objWICNew)
                        'end goAML 5.0.1
                        If objWICNew.ObjWIC.FK_Customer_Type_ID = 1 Then

                            ListObjWICPhoneEmployer = objWICNew.OBjWICPhoneEmployer
                            ListObjWICAddressEmployer = objWICNew.ObjWICAddressEmployer
                            ListObjWICIdentification = objWICNew.ObjWICIdentification





                            With objWICNew.ObjWIC
                                TxtINDV_Title.Text = .INDV_Title
                                TxtINDV_last_name.Text = .INDV_Last_Name
                                If GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender) IsNot Nothing Then
                                    TxtINDV_Gender.Text = GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender)
                                End If
                                If .INDV_BirthDate IsNot Nothing Then
                                    DateINDV_Birthdate.Text = .INDV_BirthDate
                                Else
                                    DateINDV_Birthdate.Text = ""
                                End If
                                TxtINDV_Birth_place.Text = .INDV_Birth_Place
                                TxtINDV_Mothers_name.Text = .INDV_Mothers_Name
                                TxtINDV_Alias.Text = .INDV_Alias
                                TxtINDV_SSN.Text = .INDV_SSN
                                TxtINDV_Passport_number.Text = .INDV_Passport_Number
                                'daniel 19 jan 2021
                                'TxtINDV_Passport_country.Text = .INDV_Passport_Country
                                TxtINDV_Passport_country.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Passport_Country)
                                'end 19 jan 2021
                                TxtINDV_ID_Number.Text = .INDV_ID_Number
                                CmbINDV_Nationality1.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1)
                                CmbINDV_Nationality2.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2)
                                CmbINDV_Nationality3.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3)
                                CmbINDV_residence.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence)
                                'TxtINDV_Email.Text = .INDV_Email
                                'TxtINDV_Email2.Text = .INDV_Email2
                                'TxtINDV_Email3.Text = .INDV_Email3
                                'TxtINDV_Email4.Text = .INDV_Email4
                                'TxtINDV_Email5.Text = .INDV_Email5
                                TxtINDV_Occupation.Text = .INDV_Occupation
                                TxtINDV_employer_name.Text = .INDV_Employer_Name
                                TxtINDV_Source_Of_Wealth.Text = .INDV_SumberDana
                                TxtINDV_Tax_Number.Text = .INDV_Tax_Number
                                CbINDV_Tax_Reg_Number.Text = .INDV_Tax_Reg_Number
                                If .INDV_Deceased = True Then
                                    CbINDV_Deceased.Text = "True"
                                Else
                                    CbINDV_Deceased.Text = "False"
                                End If
                                If .INDV_Deceased Then
                                    DateINDV_Deceased_Date.Hidden = False
                                    If .INDV_Deceased_Date IsNot Nothing Then
                                        DateINDV_Deceased_Date.Text = .INDV_Deceased_Date
                                    End If
                                End If
                                TxtINDV_Comments.Text = .INDV_Comment
                            End With

                            BindDetailPhone(StorePhoneIndividuDetail, ListObjWICPhone)
                            BindDetailAddress(StoreAddressIndividuDetail, ListObjWICAddress)
                            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListObjWICPhoneEmployer)
                            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListObjWICAddressEmployer)
                            BindDetailIdentification(StoreIdentificationDetail, ListObjWICIdentification)

                            WIC_Individu.Show()
                            WIC_Corporate.Hide()
                            WIC_IndividuOld.Show()
                            WIC_CorporateOld.Hide()

                        Else
                            objWICDirectorNew = objWICNew.ObjDirector

                            If objWICDirectorNew.Count > 0 Then
                                For Each Director In objWICDirectorNew
                                    ListObjWICDirector.Add(Director.ObjDirector)
                                    If Director.ListDirectorAddress.Count > 0 Then
                                        For Each item In Director.ListDirectorAddress
                                            ListObjWICAddressDirector.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each item In Director.ListDirectorAddressEmployer
                                            ListObjWICAddressDirectorEmployer.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorPhone.Count > 0 Then
                                        For Each item In Director.ListDirectorPhone
                                            ListObjWICPhoneDirector.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each item In Director.ListDirectorPhoneEmployer
                                            ListObjWICPhoneDirectorEmployer.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorIdentification.Count > 0 Then
                                        For Each item In Director.ListDirectorIdentification
                                            ListObjWICIdentificationDirector.Add(item)
                                        Next
                                    End If
                                Next
                            End If

                            With objWICNew.ObjWIC
                                TxtCorp_Name.Text = .Corp_Name
                                TxtCorp_Commercial_name.Text = .Corp_Commercial_Name
                                CmbCorp_Incorporation_legal_form.Text = GlobalReportFunctionBLL.getBentukKorporasiByKode(.Corp_Incorporation_Legal_Form)
                                TxtCorp_Incorporation_number.Text = .Corp_Incorporation_Number
                                TxtCorp_Business.Text = .Corp_Business
                                TxtCorp_Email.Text = .Corp_Email
                                TxtCorp_url.Text = .Corp_Url
                                TxtCorp_incorporation_state.Text = .Corp_Incorporation_State
                                'daniel 19 jan 2021
                                'CmbCorp_incorporation_country_code.Text = .Corp_Incorporation_Country_Code
                                CmbCorp_incorporation_country_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Corp_Incorporation_Country_Code)
                                'daniel 19 jan 2021
                                'CmbCorp_Role.Text = .Corp_Role
                                'agam 03112020
                                'DateCorp_incorporation_date.Text = .Corp_Incorporation_Date
                                If .Corp_Incorporation_Date IsNot Nothing Then
                                    DateCorp_incorporation_date.Text = .Corp_Incorporation_Date
                                End If
                                chkCorp_business_closed.Text = .Corp_Business_Closed
                                'agam 03112020
                                If .Corp_Business_Closed Then
                                    DateCorp_date_business_closed.Hidden = False
                                    If .Corp_Date_Business_Closed IsNot Nothing Then
                                        DateCorp_date_business_closed.Text = .Corp_Date_Business_Closed
                                    End If
                                End If
                                TxtCorp_tax_number.Text = .Corp_Tax_Number
                                TxtCorp_Comments.Text = .Corp_Comments
                            End With

                            BindDetailPhone(StorePhoneCorporateDetail, ListObjWICPhone)
                            BindDetailAddress(StoreAddressCorporateDetail, ListObjWICAddress)
                            BindDetailDirector(Store_Director_Corp, ListObjWICDirector)

                            WIC_Individu.Hide()
                            WIC_Corporate.Show()
                            WIC_IndividuOld.Hide()
                            WIC_CorporateOld.Show()

                        End If

                    Case Common.ModuleActionEnum.Update
                        'FormPanelOld.Hidden = False
                        'FormPanelNew.Hidden = False
                        'Dim unikID As String = Guid.NewGuid.ToString
                        'Dim objWIC As New WICBLL
                        'objWIC.LoadPanel(FormPanelNew, ObjApproval.ModuleField, unikID)

                        objWICNew = Common.Deserialize(ObjApproval.ModuleField, GetType(WICDataBLL))

                        ListObjWICPhone = objWICNew.ObjWICPhone
                        ListObjWICAddress = objWICNew.ObjWICAddress


                        objWICOld = Common.Deserialize(ObjApproval.ModuleFieldBefore, GetType(WICDataBLL))

                        ListObjWICPhoneBefore = objWICOld.ObjWICPhone
                        ListObjWICAddressBefore = objWICOld.ObjWICAddress

                        PKWIC.Text = objWICNew.ObjWIC.PK_Customer_ID
                        DspWICNo.Text = objWICNew.ObjWIC.WIC_No
                        DspIsUpdateFromDataSource.Text = objWICNew.ObjWIC.isUpdateFromDataSource

                        PKWICOld.Text = objWICOld.ObjWIC.PK_Customer_ID
                        DspWICNoOld.Text = objWICOld.ObjWIC.WIC_No
                        DspIsUpdateFromDataSourceOld.Text = objWICOld.ObjWIC.isUpdateFromDataSource

                        LoadData(objWICNew, objWICOld)
                        If objWICNew.ObjWIC.FK_Customer_Type_ID = 1 Then

                            ListObjWICPhoneEmployer = objWICNew.OBjWICPhoneEmployer
                            ListObjWICAddressEmployer = objWICNew.ObjWICAddressEmployer
                            ListObjWICIdentification = objWICNew.ObjWICIdentification

                            ListObjWICPhoneEmployerBefore = objWICOld.OBjWICPhoneEmployer
                            ListObjWICAddressEmployerBefore = objWICOld.ObjWICAddressEmployer
                            ListObjWICIdentificationBefore = objWICOld.ObjWICIdentification

                            With objWICNew.ObjWIC
                                TxtINDV_Title.Text = .INDV_Title
                                TxtINDV_last_name.Text = .INDV_Last_Name
                                TxtINDV_Gender.Text = GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender)
                                If .INDV_BirthDate IsNot Nothing Then
                                    DateINDV_Birthdate.Text = .INDV_BirthDate
                                End If
                                TxtINDV_Birth_place.Text = .INDV_Birth_Place
                                TxtINDV_Mothers_name.Text = .INDV_Mothers_Name
                                TxtINDV_Alias.Text = .INDV_Alias
                                TxtINDV_SSN.Text = .INDV_SSN
                                TxtINDV_Passport_number.Text = .INDV_Passport_Number
                                'daniel 19 jan 2021
                                'TxtINDV_Passport_country.Text = .INDV_Passport_Country
                                TxtINDV_Passport_country.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Passport_Country)
                                'end 19 jan 2021
                                TxtINDV_ID_Number.Text = .INDV_ID_Number
                                CmbINDV_Nationality1.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1)
                                CmbINDV_Nationality2.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2)
                                CmbINDV_Nationality3.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3)
                                CmbINDV_residence.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence)
                                'TxtINDV_Email.Text = .INDV_Email
                                'TxtINDV_Email2.Text = .INDV_Email2
                                'TxtINDV_Email3.Text = .INDV_Email3
                                'TxtINDV_Email4.Text = .INDV_Email4
                                'TxtINDV_Email5.Text = .INDV_Email5
                                TxtINDV_Occupation.Text = .INDV_Occupation
                                TxtINDV_employer_name.Text = .INDV_Employer_Name
                                TxtINDV_Source_Of_Wealth.Text = .INDV_SumberDana
                                TxtINDV_Tax_Number.Text = .INDV_Tax_Number
                                CbINDV_Tax_Reg_Number.Text = Convert.ToString(.INDV_Tax_Reg_Number)
                                CbINDV_Deceased.Text = Convert.ToString(.INDV_Deceased)
                                If .INDV_Deceased Then
                                    DateINDV_Deceased_Date.Show()
                                    If .INDV_Deceased_Date IsNot Nothing Then
                                        DateINDV_Deceased_Date.Text = .INDV_Deceased_Date
                                    End If
                                End If
                                TxtINDV_Comments.Text = .INDV_Comment
                            End With

                            With objWICOld.ObjWIC
                                TxtINDV_TitleOld.Text = .INDV_Title
                                TxtINDV_last_nameOld.Text = .INDV_Last_Name
                                TxtINDV_GenderOld.Text = GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender)
                                If .INDV_BirthDate IsNot Nothing Then
                                    DateINDV_BirthdateOld.Text = .INDV_BirthDate
                                End If
                                TxtINDV_Birth_placeOld.Text = .INDV_Birth_Place
                                TxtINDV_Mothers_nameOld.Text = .INDV_Mothers_Name
                                TxtINDV_AliasOld.Text = .INDV_Alias
                                TxtINDV_SSNOld.Text = .INDV_SSN
                                TxtINDV_Passport_numberOld.Text = .INDV_Passport_Number
                                'daniel 19 jan 2021
                                'TxtINDV_Passport_countryOld.Text = .INDV_Passport_Country
                                TxtINDV_Passport_countryOld.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Passport_Country)
                                'daniel 19 jan 2021
                                TxtINDV_ID_NumberOld.Text = .INDV_ID_Number
                                CmbINDV_Nationality1Old.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1)
                                CmbINDV_Nationality2Old.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2)
                                CmbINDV_Nationality3Old.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3)
                                CmbINDV_residenceOld.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence)
                                'TxtINDV_EmailOld.Text = .INDV_Email
                                'TxtINDV_Email2Old.Text = .INDV_Email2
                                'TxtINDV_Email3Old.Text = .INDV_Email3
                                'TxtINDV_Email4Old.Text = .INDV_Email4
                                'TxtINDV_Email5Old.Text = .INDV_Email5
                                TxtINDV_OccupationOld.Text = .INDV_Occupation
                                TxtINDV_employer_nameOld.Text = .INDV_Employer_Name
                                TxtINDV_Source_Of_WealthOld.Text = .INDV_SumberDana
                                TxtINDV_Tax_NumberOld.Text = .INDV_Tax_Number
                                CbINDV_Tax_Reg_NumberOld.Text = Convert.ToString(.INDV_Tax_Reg_Number)
                                CbINDV_DeceasedOld.Text = Convert.ToString(.INDV_Deceased)
                                If .INDV_Deceased Then
                                    DateINDV_Deceased_DateOld.Hidden = False
                                    If .INDV_Deceased_Date IsNot Nothing Then
                                        DateINDV_Deceased_DateOld.Text = .INDV_Deceased_Date
                                    End If
                                End If
                                TxtINDV_CommentsOld.Text = .INDV_Comment
                            End With
                            'GridPanelPhoneIndividuDetailBefore.Show()
                            'GridPanelAddressIndividuDetailBefore.Show()

                            'GridPanelIdentificationIndividualDetailBefore.Show()
                            'GridPanelDirectorIdentificationBefore.Show()

                            'GridPanelPhoneCorporateDetailBefore.Show()
                            'GridPanelAddressCorporateDetailBefore.Show()

                            'GridPanelPhoneIndividualEmployerDetailBefore.Show()
                            'GridPanelAddressIndividualEmployerDetailBefore.Show()

                            'BindDetailPhone(StorePhoneIndividuDetailBefore, ListObjWICPhoneBefore)

                            'BindDetailAddress(StoreAddressIndividuDetailBefore, ListObjWICAddressBefore)


                            'BindDetailPhone(StorePhoneEmployerIndividuDetailBefore, ListObjWICPhoneEmployerBefore)

                            'BindDetailAddress(StoreAddressEmployerIndividuDetailBefore, ListObjWICAddressEmployerBefore)

                            'BindDetailIdentification(StoreIdentificationDetailBefore, ListObjWICIdentificationBefore)

                            BindDetailPhone(StorePhoneIndividuDetail, ListObjWICPhone)
                            BindDetailAddress(StoreAddressIndividuDetail, ListObjWICAddress)
                            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListObjWICPhoneEmployer)
                            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListObjWICAddressEmployer)
                            BindDetailIdentification(StoreIdentificationDetail, ListObjWICIdentification)

                            BindDetailPhone(StorePhoneIndividuDetailOld, ListObjWICPhoneBefore)
                            BindDetailAddress(StoreAddressIndividuDetailOld, ListObjWICAddressBefore)
                            BindDetailPhone(StorePhoneEmployerIndividuDetailOld, ListObjWICPhoneEmployerBefore)
                            BindDetailAddress(StoreAddressEmployerIndividuDetailOld, ListObjWICAddressEmployerBefore)
                            BindDetailIdentification(StoreIdentificationDetailOld, ListObjWICIdentificationBefore)

                            WIC_Individu.Show()
                            WIC_Corporate.Hide()
                            WIC_IndividuOld.Show()
                            WIC_CorporateOld.Hide()
                        Else
                            objWICDirectorNew = objWICNew.ObjDirector

                            If objWICDirectorNew.Count > 0 Then
                                For Each Director In objWICDirectorNew
                                    ListObjWICDirector.Add(Director.ObjDirector)
                                    If Director.ListDirectorAddress.Count > 0 Then
                                        For Each item In Director.ListDirectorAddress
                                            ListObjWICAddressDirector.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each item In Director.ListDirectorAddressEmployer
                                            ListObjWICAddressDirectorEmployer.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorPhone.Count > 0 Then
                                        For Each item In Director.ListDirectorPhone
                                            ListObjWICPhoneDirector.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each item In Director.ListDirectorPhoneEmployer
                                            ListObjWICPhoneDirectorEmployer.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorIdentification.Count > 0 Then
                                        For Each item In Director.ListDirectorIdentification
                                            ListObjWICIdentificationDirector.Add(item)
                                        Next
                                    End If
                                Next
                            End If

                            objWICDirectorOld = objWICOld.ObjDirector

                            If objWICDirectorOld.Count > 0 Then
                                For Each Director In objWICDirectorOld
                                    'agam 22102020
                                    If Director.ObjDirector IsNot Nothing Then
                                        ListObjWICDirectorBefore.Add(Director.ObjDirector)
                                        If Director.ListDirectorAddress.Count > 0 Then
                                            For Each item In Director.ListDirectorAddress
                                                If item.FK_To_Table_ID = Director.ObjDirector.NO_ID Then '2023-10-21, Nael
                                                    ListObjWICAddressDirectorBefore.Add(item)
                                                End If
                                            Next
                                        End If
                                        If Director.ListDirectorAddressEmployer.Count > 0 Then
                                            For Each item In Director.ListDirectorAddressEmployer
                                                If item.FK_To_Table_ID = Director.ObjDirector.NO_ID Then '2023-10-21, Nael
                                                    ListObjWICAddressDirectorEmployerBefore.Add(item)
                                                End If
                                            Next
                                        End If
                                        If Director.ListDirectorPhone.Count > 0 Then
                                            For Each item In Director.ListDirectorPhone
                                                If item.FK_for_Table_ID = Director.ObjDirector.NO_ID Then '2023-10-21, Nael
                                                    ListObjWICPhoneDirectorBefore.Add(item)
                                                End If
                                            Next
                                        End If
                                        If Director.ListDirectorPhoneEmployer.Count > 0 Then
                                            For Each item In Director.ListDirectorPhoneEmployer
                                                If item.FK_for_Table_ID = Director.ObjDirector.NO_ID Then '2023-10-21, Nael
                                                    ListObjWICPhoneDirectorEmployerBefore.Add(item)
                                                End If
                                            Next
                                        End If
                                        If Director.ListDirectorIdentification.Count > 0 Then
                                            For Each item In Director.ListDirectorIdentification
                                                If item.FK_Person_ID = Director.ObjDirector.NO_ID Then '2023-10-21, Nael
                                                    ListObjWICIdentificationDirectorBefore.Add(item)
                                                End If
                                            Next
                                        End If
                                    End If
                                Next
                            End If

                            With objWICNew.ObjWIC
                                TxtCorp_Name.Text = .Corp_Name
                                TxtCorp_Commercial_name.Text = .Corp_Commercial_Name
                                CmbCorp_Incorporation_legal_form.Text = GlobalReportFunctionBLL.getBentukKorporasiByKode(.Corp_Incorporation_Legal_Form)
                                TxtCorp_Incorporation_number.Text = .Corp_Incorporation_Number
                                TxtCorp_Business.Text = .Corp_Business
                                TxtCorp_Email.Text = .Corp_Email
                                TxtCorp_url.Text = .Corp_Url
                                TxtCorp_incorporation_state.Text = .Corp_Incorporation_State
                                'daniel 19 jan 2021
                                'CmbCorp_incorporation_country_code.Text = .Corp_Incorporation_Country_Code
                                CmbCorp_incorporation_country_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Corp_Incorporation_Country_Code)
                                'daniel 19 jan 2021
                                'CmbCorp_Role.Text = .Corp_Role
                                'agam 22102020
                                If .Corp_Incorporation_Date <> Date.MinValue Then
                                    DateCorp_incorporation_date.Text = .Corp_Incorporation_Date
                                End If

                                chkCorp_business_closed.Text = .Corp_Business_Closed
                                If .Corp_Business_Closed Then

                                    DateCorp_date_business_closed.Text = .Corp_Date_Business_Closed
                                End If
                                TxtCorp_tax_number.Text = .Corp_Tax_Number
                                TxtCorp_Comments.Text = .Corp_Comments
                            End With

                            With objWICOld.ObjWIC
                                TxtCorp_NameOld.Text = .Corp_Name
                                TxtCorp_Commercial_nameOld.Text = .Corp_Commercial_Name
                                CmbCorp_Incorporation_legal_formOld.Text = GlobalReportFunctionBLL.getBentukKorporasiByKode(.Corp_Incorporation_Legal_Form)
                                TxtCorp_Incorporation_numberOld.Text = .Corp_Incorporation_Number
                                TxtCorp_BusinessOld.Text = .Corp_Business
                                TxtCorp_EmailOld.Text = .Corp_Email
                                TxtCorp_urlOld.Text = .Corp_Url
                                TxtCorp_incorporation_stateOld.Text = .Corp_Incorporation_State
                                'daniel 19 jan 2021
                                'CmbCorp_incorporation_country_codeOld.Text = .Corp_Incorporation_Country_Code
                                CmbCorp_incorporation_country_codeOld.Text = GlobalReportFunctionBLL.getCountryByCode(.Corp_Incorporation_Country_Code)
                                'daniel 19 jan 2021
                                'CmbCorp_RoleOld.Text = .Corp_Role
                                'agam 22102020
                                If .Corp_Incorporation_Date <> Date.MinValue Then
                                    DateCorp_incorporation_dateOld.Text = .Corp_Incorporation_Date
                                End If

                                chkCorp_business_closedOld.Text = .Corp_Business_Closed
                                If .Corp_Business_Closed Then
                                    DateCorp_date_business_closedOld.Hidden = False
                                    If .Corp_Date_Business_Closed IsNot Nothing Then
                                        DateCorp_date_business_closedOld.Text = .Corp_Date_Business_Closed
                                    End If
                                End If
                                TxtCorp_tax_numberOld.Text = .Corp_Tax_Number
                                TxtCorp_CommentsOld.Text = .Corp_Comments
                            End With
                            'GridPanelPhoneCorporateDetailBefore.Show()
                            'GridPanelAddressCorporateDetailBefore.Show()
                            'GridPanelDirector_CorpBefore.Show()

                            'BindDetailPhone(StorePhoneCorporateDetailBefore, ListObjWICPhoneBefore)

                            'BindDetailAddress(StoreAddressCorporateDetailBefore, ListObjWICAddressBefore)

                            'BindDetailDirector(Store_Director_CorpBefore, ListObjWICDirectorBefore)
                            BindDetailPhone(StorePhoneCorporateDetail, ListObjWICPhone)
                            BindDetailAddress(StoreAddressCorporateDetail, ListObjWICAddress)
                            BindDetailDirector(Store_Director_Corp, ListObjWICDirector)

                            BindDetailPhone(StorePhoneCorporateDetailOld, ListObjWICPhoneBefore)
                            BindDetailAddress(StoreAddressCorporateDetailOld, ListObjWICAddressBefore)
                            BindDetailDirector(Store_Director_CorpOld, ListObjWICDirectorBefore)

                            WIC_Individu.Hide()
                            WIC_Corporate.Show()
                            WIC_IndividuOld.Hide()
                            WIC_CorporateOld.Show()

                        End If

                    Case Common.ModuleActionEnum.Delete
                        'FormPanelOld.Hidden = False
                        FormPanelNew.Visible = False
                        'Dim unikOldID As String = Guid.NewGuid.ToString
                        'Dim unikNewID As String = Guid.NewGuid.ToString

                        'Dim objWIC As New WICBLL
                        'objWIC.LoadPanel(FormPanelOld, ObjApproval.ModuleFieldBefore, unikOldID)
                        'objWIC.LoadPanel(FormPanelNew, ObjApproval.ModuleField, unikNewID)

                        'Case Common.ModuleActionEnum.Activation
                        '    Dim unikID As String = Guid.NewGuid.ToString
                        '    Dim objWIC As New WICBLL
                        '    objWIC.LoadPanel(FormPanelNew, ObjApproval.ModuleField, unikID)

                        objWICNew = Common.Deserialize(ObjApproval.ModuleField, GetType(WICDataBLL))

                        ListObjWICPhoneBefore = objWICNew.ObjWICPhone
                        ListObjWICAddressBefore = objWICNew.ObjWICAddress

                        PKWICOld.Text = objWICNew.ObjWIC.PK_Customer_ID
                        DspWICNoOld.Text = objWICNew.ObjWIC.WIC_No
                        DspIsUpdateFromDataSourceOld.Text = objWICNew.ObjWIC.isUpdateFromDataSource
                        If objWICNew.ObjWIC.FK_Customer_Type_ID = 1 Then

                            ListObjWICPhoneEmployerBefore = objWICNew.OBjWICPhoneEmployer
                            ListObjWICAddressEmployerBefore = objWICNew.ObjWICAddressEmployer
                            ListObjWICIdentificationBefore = objWICNew.ObjWICIdentification

                            With objWICNew.ObjWIC
                                TxtINDV_TitleOld.Text = .INDV_Title
                                TxtINDV_last_nameOld.Text = .INDV_Last_Name
                                TxtINDV_GenderOld.Text = GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender)
                                If .INDV_BirthDate IsNot Nothing Then
                                    DateINDV_BirthdateOld.Text = .INDV_BirthDate
                                End If
                                TxtINDV_Birth_placeOld.Text = .INDV_Birth_Place
                                TxtINDV_Mothers_nameOld.Text = .INDV_Mothers_Name
                                TxtINDV_AliasOld.Text = .INDV_Alias
                                TxtINDV_SSNOld.Text = .INDV_SSN
                                TxtINDV_Passport_numberOld.Text = .INDV_Passport_Number
                                'daniel 19 jan 2021
                                'TxtINDV_Passport_countryOld.Text = .INDV_Passport_Country
                                TxtINDV_Passport_countryOld.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Passport_Country)
                                'end 19 jan 2021
                                TxtINDV_ID_NumberOld.Text = .INDV_ID_Number
                                CmbINDV_Nationality1Old.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1)
                                CmbINDV_Nationality2Old.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2)
                                CmbINDV_Nationality3Old.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3)
                                CmbINDV_residenceOld.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence)
                                'TxtINDV_EmailOld.Text = .INDV_Email
                                'TxtINDV_Email2Old.Text = .INDV_Email2
                                'TxtINDV_Email3Old.Text = .INDV_Email3
                                'TxtINDV_Email4Old.Text = .INDV_Email4
                                'TxtINDV_Email5Old.Text = .INDV_Email5
                                TxtINDV_OccupationOld.Text = .INDV_Occupation
                                TxtINDV_employer_nameOld.Text = .INDV_Employer_Name
                                TxtINDV_Source_Of_WealthOld.Text = .INDV_SumberDana
                                TxtINDV_Tax_NumberOld.Text = .INDV_Tax_Number
                                CbINDV_Tax_Reg_NumberOld.Text = .INDV_Tax_Reg_Number
                                CbINDV_DeceasedOld.Text = .INDV_Deceased
                                If .INDV_Deceased Then
                                    DateINDV_Deceased_DateOld.Show()
                                    If .INDV_Deceased_Date IsNot Nothing Then
                                        DateINDV_Deceased_DateOld.Text = .INDV_Deceased_Date
                                    End If
                                End If
                                TxtINDV_CommentsOld.Text = .INDV_Comment

                                BindDetailPhone(StorePhoneIndividuDetailOld, ListObjWICPhoneBefore)
                                BindDetailAddress(StoreAddressIndividuDetailOld, ListObjWICAddressBefore)
                                BindDetailPhone(StorePhoneEmployerIndividuDetailOld, ListObjWICPhoneEmployerBefore)
                                BindDetailAddress(StoreAddressEmployerIndividuDetailOld, ListObjWICAddressEmployerBefore)
                                BindDetailIdentification(StoreIdentificationDetailOld, ListObjWICIdentificationBefore)
                            End With

                            WIC_Individu.Hide()
                            WIC_Corporate.Hide()
                            WIC_IndividuOld.Show()
                            WIC_CorporateOld.Hide()
                        Else
                            objWICDirectorNew = objWICNew.ObjDirector

                            If objWICDirectorNew.Count > 0 Then
                                For Each Director In objWICDirectorNew
                                    ListObjWICDirectorBefore.Add(Director.ObjDirector)
                                    If Director.ListDirectorAddress.Count > 0 Then
                                        For Each item In Director.ListDirectorAddress
                                            ListObjWICAddressDirectorBefore.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each item In Director.ListDirectorAddressEmployer
                                            ListObjWICAddressDirectorEmployerBefore.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorPhone.Count > 0 Then
                                        For Each item In Director.ListDirectorPhone
                                            ListObjWICPhoneDirectorBefore.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each item In Director.ListDirectorPhoneEmployer
                                            ListObjWICPhoneDirectorEmployerBefore.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorIdentification.Count > 0 Then
                                        For Each item In Director.ListDirectorIdentification
                                            ListObjWICIdentificationDirectorBefore.Add(item)
                                        Next
                                    End If
                                Next
                            End If

                            With objWICNew.ObjWIC
                                TxtCorp_NameOld.Text = .Corp_Name
                                TxtCorp_Commercial_nameOld.Text = .Corp_Commercial_Name
                                CmbCorp_Incorporation_legal_formOld.Text = GlobalReportFunctionBLL.getBentukKorporasiByKode(.Corp_Incorporation_Legal_Form)
                                TxtCorp_Incorporation_numberOld.Text = .Corp_Incorporation_Number
                                TxtCorp_BusinessOld.Text = .Corp_Business
                                TxtCorp_EmailOld.Text = .Corp_Email
                                TxtCorp_urlOld.Text = .Corp_Url
                                TxtCorp_incorporation_stateOld.Text = .Corp_Incorporation_State
                                'daniel 19 jan 2021
                                'CmbCorp_incorporation_country_codeOld.Text = .Corp_Incorporation_Country_Code
                                CmbCorp_incorporation_country_codeOld.Text = GlobalReportFunctionBLL.getCountryByCode(.Corp_Incorporation_Country_Code)
                                'daniel 19 jan 2021
                                'CmbCorp_RoleOld.Text = .Corp_Role
                                If .Corp_Incorporation_Date IsNot Nothing Then
                                    DateCorp_incorporation_dateOld.Text = .Corp_Incorporation_Date
                                End If

                                chkCorp_business_closedOld.Text = .Corp_Business_Closed
                                If .Corp_Business_Closed Then
                                    DateCorp_date_business_closedOld.Hidden = False
                                    If .Corp_Date_Business_Closed IsNot Nothing Then
                                        DateCorp_date_business_closedOld.Text = .Corp_Date_Business_Closed
                                    End If
                                End If
                                TxtCorp_tax_numberOld.Text = .Corp_Tax_Number
                                TxtCorp_CommentsOld.Text = .Corp_Comments
                            End With

                            BindDetailPhone(StorePhoneCorporateDetailOld, ListObjWICPhoneBefore)
                            BindDetailAddress(StoreAddressCorporateDetailOld, ListObjWICAddressBefore)
                            BindDetailDirector(Store_Director_CorpOld, ListObjWICDirectorBefore)

                            WIC_Individu.Hide()
                            WIC_Corporate.Hide()
                            WIC_IndividuOld.Hide()
                            WIC_CorporateOld.Show()

                        End If

                End Select
            End If
        Catch ex As Exception
            BtnSave.Visible = False
            BtnReject.Visible = False
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadDataNew()

    End Sub
    Private Sub LoadPopup()
        WindowDetailAddress.Maximizable = True
        WindowDetailPhone.Maximizable = True
        WindowDetailDirector.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True
        WindowDetailDirectorIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailIdentification.Maximizable = True

        ' 2023-10-24, Nael
        WindowDetail_RelatedPerson.Maximizable = True
        WindowDetail_Email.Maximizable = True
        WindowDetail_EntityIdentification.Maximizable = True
        WindowDetail_Person_PEP.Maximizable = True
        WindowDetail_RelatedEntity.Maximizable = True
        WindowDetail_Sanction.Maximizable = True
        WindowDetail_URL.Maximizable = True

    End Sub

    Private Sub LoadColumn()

        ColumnActionLocation(GridPanelDirectorPhoneBefore, CommandColumn5)
        ColumnActionLocation(GridPanelDirectorPhone, CommandColumn1)
        ColumnActionLocation(GridPanelDirectorAddressBefore, CommandColumn6)
        ColumnActionLocation(GridPanelDirectorAddress, CommandColumn2)
        ColumnActionLocation(GridPanelDirectorPhoneEmployerBefore, CommandColumn7)
        ColumnActionLocation(GridPanelDirectorPhoneEmployer, CommandColumn3)
        ColumnActionLocation(GridPanelDirectorAddressEmployerBefore, CommandColumn9)
        ColumnActionLocation(GridPanelDirectorAddressEmployer, CommandColumn4)
        ColumnActionLocation(GridPanelDirectorIdentificationBefore, CommandColumn13)
        ColumnActionLocation(GridPanelDirectordentification, CommandColumn12)

        'ColumnActionLocation(GridPanelPhoneIndividuDetailBefore, CommandColumnPhoneIndividuDetailBefore)
        ColumnActionLocation(GridPanelPhoneIndividuDetail, CommandColumnPhoneIndividuDetail)
        ColumnActionLocation(GridPanelPhoneIndividuDetailOld, CommandColumnPhoneIndividuDetailOld)
        'ColumnActionLocation(GridPanelAddressIndividuDetailBefore, CommandColumnAddressIndividuDetailBefore)
        ColumnActionLocation(GridPanelAddressIndividuDetail, CommandColumnAddressIndividuDetail)
        ColumnActionLocation(GridPanelAddressIndividuDetailOld, CommandColumnAddressIndividuDetailOld)
        'ColumnActionLocation(GridPanelPhoneIndividualEmployerDetailBefore, CommandColumnPhoneIndividualEmployerDetailBefore)
        ColumnActionLocation(GridPanelPhoneIndividualEmployerDetail, CommandColumnPhoneIndividualEmployerDetail)
        ColumnActionLocation(GridPanelPhoneIndividualEmployerDetailOld, CommandColumnPhoneIndividualEmployerDetailOld)
        'ColumnActionLocation(GridPanelAddressIndividualEmployerDetailBefore, CommandColumnAddressIndividualEmployerDetailBefore)
        ColumnActionLocation(GridPanelAddressIndividualEmployerDetail, CommandColumnAddressIndividualEmployerDetail)
        ColumnActionLocation(GridPanelAddressIndividualEmployerDetailOld, CommandColumnAddressIndividualEmployerDetailOld)
        'ColumnActionLocation(GridPanelIdentificationIndividualDetailBefore, CommandColumnIdentificationIndividualDetailBefore)
        ColumnActionLocation(GridPanelIdentificationIndividualDetail, CommandColumnIdentificationIndividualDetail)
        ColumnActionLocation(GridPanelIdentificationIndividualDetailOld, CommandColumnIdentificationIndividualDetailOld)

        'ColumnActionLocation(GridPanelPhoneCorporateDetailBefore, CommandColumnPhoneCorporateDetailBefore)
        ColumnActionLocation(GridPanelPhoneCorporateDetail, CommandColumnPhoneCorporateDetail)
        ColumnActionLocation(GridPanelPhoneCorporateDetailOld, CommandColumnPhoneCorporateDetailOld)
        'ColumnActionLocation(GridPanelAddressCorporateDetailBefore, CommandColumnAddressCorporateDetailBefore)
        ColumnActionLocation(GridPanelAddressCorporateDetail, CommandColumnAddressCorporateDetail)
        ColumnActionLocation(GridPanelAddressCorporateDetailOld, CommandColumnAddressCorporateDetailOld)
        'ColumnActionLocation(GridPanelDirector_CorpBefore, CommandColumnDirectorBefore)
        ColumnActionLocation(GP_Corp_Director, CommandColumnDirector)
        ColumnActionLocation(GP_Corp_DirectorOld, CommandColumnDirectorOld)

    End Sub
    Private Sub ColumnActionLocation(gridPanel As GridPanel, commandColumn As CommandColumn)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridPanel.ColumnModel.Columns.RemoveAt(gridPanel.ColumnModel.Columns.Count - 1)
                gridPanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub ClearSession()

        objWIC = New goAML_Ref_WIC
        objWICAddress = New goAML_Ref_Address
        objWICAddressBefore = New goAML_Ref_Address
        objWICAddressDirector = New goAML_Ref_Address
        objWICAddressDirectorBefore = New goAML_Ref_Address
        objWICAddressDirectorEmployer = New goAML_Ref_Address
        objWICAddressDirectorEmployerBefore = New goAML_Ref_Address
        objWICAddressEmployer = New goAML_Ref_Address
        objWICAddressEmployerBefore = New goAML_Ref_Address
        objWICBefore = New goAML_Ref_WIC
        objWICDirector = New goAML_Ref_Walk_In_Customer_Director
        objWICDirectorBefore = New goAML_Ref_Walk_In_Customer_Director
        objWICDirectorNew = New List(Of WICDirectorDataBLL)
        objWICDirectorOld = New List(Of WICDirectorDataBLL)
        objWICIdentification = New goAML_Person_Identification
        objWICIdentificationBefore = New goAML_Person_Identification
        objWICIdentificationDirector = New goAML_Person_Identification
        objWICIdentificationDirectorBefore = New goAML_Person_Identification
        objWICNew = New WICDataBLL
        objWICOld = New WICDataBLL
        objWICPhone = New goAML_Ref_Phone
        objWICPhoneBefore = New goAML_Ref_Phone
        objWICPhoneDirector = New goAML_Ref_Phone
        objWICPhoneDirectorBefore = New goAML_Ref_Phone
        objWICPhoneDirectorEmployer = New goAML_Ref_Phone
        objWICPhoneDirectorEmployerBefore = New goAML_Ref_Phone
        objWICPhoneEmployer = New goAML_Ref_Phone
        objWICPhoneEmployerBefore = New goAML_Ref_Phone

        ListObjWICAddress = New List(Of goAML_Ref_Address)
        ListObjWICAddressBefore = New List(Of goAML_Ref_Address)
        ListObjWICAddressDirector = New List(Of goAML_Ref_Address)
        ListObjWICAddressDirectorBefore = New List(Of goAML_Ref_Address)
        ListObjWICAddressDirectorEmployer = New List(Of goAML_Ref_Address)
        ListObjWICAddressDirectorEmployerBefore = New List(Of goAML_Ref_Address)
        ListObjWICAddressEmployer = New List(Of goAML_Ref_Address)
        ListObjWICAddressEmployerBefore = New List(Of goAML_Ref_Address)
        ListObjWICDirector = New List(Of goAML_Ref_Walk_In_Customer_Director)
        ListObjWICDirectorBefore = New List(Of goAML_Ref_Walk_In_Customer_Director)
        ListObjWICIdentification = New List(Of goAML_Person_Identification)
        ListObjWICIdentificationBefore = New List(Of goAML_Person_Identification)
        ListObjWICIdentificationDirector = New List(Of goAML_Person_Identification)
        ListObjWICIdentificationDirectorBefore = New List(Of goAML_Person_Identification)
        ListObjWICPhone = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneBefore = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneDirector = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneDirectorBefore = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneDirectorEmployer = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneDirectorEmployerBefore = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneEmployer = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneEmployerBefore = New List(Of goAML_Ref_Phone)

        ''goAML 5.0.1, Add by Septian, 2023-03-15
        objListgoAML_Ref_Email_Old = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Email)
        objListgoAML_Ref_Email_New = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Email)

        objListgoAML_Ref_PersonPEP_Old = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Person_PEP)
        objListgoAML_Ref_PersonPEP_New = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Person_PEP)

        objListgoAML_Ref_Sanction_Old = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Sanction)
        objListgoAML_Ref_Sanction_New = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Sanction)

        objListgoAML_Ref_RelatedPerson_Old = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Person)
        objListgoAML_Ref_RelatedPerson_New = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Person)

        objListgoAML_Ref_EntityUrl_Old = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
        objListgoAML_Ref_EntityUrl_New = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)

        objListgoAML_Ref_EntityIdentification_Old = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        objListgoAML_Ref_EntityIdentification_New = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)

        objListgoAML_Ref_RelatedEntity_Old = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        objListgoAML_Ref_RelatedEntity_New = New List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        ''end goAML 5.0.1

    End Sub
#End Region

#Region "Bind"
    Private Sub BindDetailIdentification(store As Store, list As List(Of goAML_Person_Identification))
        Dim objtable As DataTable = NawaBLL.Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Tipe", GetType(String)))
        objtable.Columns.Add(New DataColumn("No", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssueDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("ExpiryDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssuedCountry", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                If Not item.IsNull("Tipe") Then
                    item("Tipe") = GlobalReportFunctionBLL.getjenisDokumenByKode(item("Type"))
                End If

                If Not item.IsNull("Number") Then
                    item("No") = item("Number")
                End If

                If Not item.IsNull("Issue_Date") Then
                    item("IssueDate") = item("Issue_Date")
                End If

                If Not item.IsNull("Expiry_Date") Then
                    item("ExpiryDate") = item("Expiry_Date")
                End If

                If Not item.IsNull("Issued_Country") Then
                    item("IssuedCountry") = GlobalReportFunctionBLL.getCountryByCode(item("Issued_Country"))
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailPhone(store As Store, list As List(Of goAML_Ref_Phone))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                If Not item.IsNull("Tph_Contact_Type") Then
                    item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
                End If
                If Not item.IsNull("Tph_Communication_Type") Then
                    item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
                End If
                If Not item.IsNull("tph_number") Then
                    item("number") = item("tph_number")
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailAddress(store As Store, list As List(Of goAML_Ref_Address))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Type_Address", GetType(String)))
        objtable.Columns.Add(New DataColumn("Addres", GetType(String)))
        objtable.Columns.Add(New DataColumn("Cty", GetType(String)))
        objtable.Columns.Add(New DataColumn("CountryCode", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                If Not item.IsNull("Address_Type") Then
                    item("Type_Address") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type"))
                End If
                If Not item.IsNull("Address") Then
                    item("Addres") = item("Address")
                End If
                If Not item.IsNull("City") Then
                    item("Cty") = item("City")
                End If
                If Not item.IsNull("Country_Code") Then
                    item("CountryCode") = GlobalReportFunctionBLL.getCountryByCode(item("Country_Code"))
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailDirector(store As Store, list As List(Of goAML_Ref_Walk_In_Customer_Director))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("LastName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Roles", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                If Not item.IsNull("Last_Name") Then
                    item("LastName") = item("Last_Name")
                End If
                If Not item.IsNull("Roles") Then
                    item("Roles") = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(item("Role").ToString)
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
#End Region

#Region "Grid"
    Protected Sub GrdCmdDirectorPhoneBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneDirectorBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorPhone(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneDirector(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorAddressBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressDirectorBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorAddress(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressDirector(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerPhoneBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneEmployerDirectorBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerPhone(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneEmployerDirector(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerAddressBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressEmployerDirectorBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerAddress(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressEmployerDirector(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorIdentificationBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentidicationDirectorBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentidicationDirector(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdIdentification(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentidication(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GrdCmdIdentificationOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailIdentidication(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GrdCmdIdentificationBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentidicationBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneIndividuBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneINDVBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneIndividu(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneINDV(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GridCommandPhoneIndividuOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailPhoneINDV(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GridCommandPhoneEmployerIndividuBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneINDVEmployerBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneEmployerIndividu(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneINDVEmployer(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GridCommandPhoneEmployerIndividuOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailPhoneINDVEmployer(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GridCommandAddressIndividuBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressINDVBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressIndividu(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressINDV(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GridCommandAddressIndividuOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailAddressINDV(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GridCommandAddressEmployerIndividuBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressINDVEmployerBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressEmployerIndividu(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressINDVEmployer(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GridCommandAddressEmployerIndividuOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailAddressINDVEmployer(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GridCommandPhoneCorporateBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneCorpBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneCorporate(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneCorp(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GridCommandPhoneCorporateOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailPhoneCorp(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GridCommandAddressCorporateBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressCorpBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressCorporate(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressCorp(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GridCommandAddressCorporateOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailAddressCorp(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    'Protected Sub GrdCmdCustDirectorBefore(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailDirectorCorpBefore(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GrdCmdCustDirector(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailDirectorCorp(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdCustDirectorOld(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailDirectorCorpBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Detail"
    Private Sub ClearDetailDirector()
        DspPeranDirector.Text = ""
        DspGelarDirector.Text = ""
        DspNamaLengkap.Text = ""
        DspGender.Text = ""
        DspTanggalLahir.Text = ""
        DspTempatLahir.Text = ""
        DspNamaIbuKandung.Text = ""
        DspNamaAlias.Text = ""
        DspNIK.Text = ""
        DspNoPassport.Text = ""
        DspNegaraPenerbitPassport.Text = ""
        DspNoIdentitasLain.Text = ""
        DspKewarganegaraan1.Text = ""
        DspKewarganegaraan2.Text = ""
        DspKewarganegaraan3.Text = ""
        DspNegaraDomisili.Text = ""
        'DspEmail.Text = ""
        'DspEmail2.Text = ""
        'DspEmail3.Text = ""
        'DspEmail4.Text = ""
        'DspEmail5.Text = ""
        DspDeceased.Text = ""
        DspDeceasedDate.Text = ""
        DspPEP.Text = ""
        DspNPWP.Text = ""
        DspSourceofWealth.Text = ""
        DspOccupation.Text = ""
        DspCatatan.Text = ""
        DspTempatBekerja.Text = ""
    End Sub
    Private Sub ClearDetailDirectorEdit()
        DspPeranDirector.Text = " => "
        DspGelarDirector.Text = " => "
        DspNamaLengkap.Text = " => "
        DspGender.Text = " => "
        DspTanggalLahir.Text = " => "
        DspTempatLahir.Text = " => "
        DspNamaIbuKandung.Text = " => "
        DspNamaAlias.Text = " => "
        DspNIK.Text = " => "
        DspNoPassport.Text = " => "
        DspNegaraPenerbitPassport.Text = " => "
        DspNoIdentitasLain.Text = " => "
        DspKewarganegaraan1.Text = " => "
        DspKewarganegaraan2.Text = " => "
        DspKewarganegaraan3.Text = " => "
        DspNegaraDomisili.Text = " => "
        'DspEmail.Text = " => "
        'DspEmail2.Text = " => "
        'DspEmail3.Text = " => "
        'DspEmail4.Text = " => "
        'DspEmail5.Text = " => "
        DspDeceased.Text = " => "
        DspDeceasedDate.Text = " => "
        DspPEP.Text = " => "
        DspNPWP.Text = " => "
        DspSourceofWealth.Text = " => "
        DspOccupation.Text = " => "
        DspCatatan.Text = " => "
        DspTempatBekerja.Text = " => "
    End Sub
    Private Sub DetailDirectorCorp(iD As String)
        WindowDetailDirector.Show()

        GridPanelDirectorPhoneBefore.Hide()
        GridPanelDirectorAddressBefore.Hide()
        GridPanelDirectorPhoneEmployerBefore.Hide()
        GridPanelDirectorAddressEmployerBefore.Hide()
        GridPanelDirectorIdentificationBefore.Hide()

        GridPanelDirectorPhone.Show()
        GridPanelDirectorAddress.Show()
        GridPanelDirectorPhoneEmployer.Show()
        GridPanelDirectorAddressEmployer.Show()
        GridPanelDirectordentification.Show()

        Dim objDirector As goAML_Ref_Walk_In_Customer_Director = ListObjWICDirector.Where(Function(x) x.NO_ID = iD).FirstOrDefault
        If objDirector IsNot Nothing Then
            ClearDetailDirector()
            With objDirector
                DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
                DspGelarDirector.Text = .Title
                DspNamaLengkap.Text = .Last_Name
                DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                If .BirthDate IsNot Nothing Then
                    DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yyy")
                End If
                DspTempatLahir.Text = .Birth_Place
                DspNamaIbuKandung.Text = .Mothers_Name
                DspNamaAlias.Text = .Alias
                DspNIK.Text = .SSN
                DspNoPassport.Text = .Passport_Number
                'daniel 19 jan 2021
                'DspNegaraPenerbitPassport.Text = .Passport_Country
                DspNegaraPenerbitPassport.Text = GlobalReportFunctionBLL.getCountryByCode(.Passport_Country)
                'end 19 jan 2021
                DspNoIdentitasLain.Text = .ID_Number
                DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                'DspEmail.Text = .Email
                'DspEmail2.Text = .Email2
                'DspEmail3.Text = .Email3
                'DspEmail4.Text = .Email4
                'DspEmail5.Text = .Email5
                DspDeceased.Text = .Deceased
                If .Deceased_Date IsNot Nothing Then
                    DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                End If
                DspPEP.Text = .Tax_Reg_Number
                DspNPWP.Text = .Tax_Number
                DspSourceofWealth.Text = .Source_of_Wealth
                DspOccupation.Text = .Occupation
                DspCatatan.Text = .Comments
                DspTempatBekerja.Text = .Employer_Name

                ListObjWICAddressDirector = objWICDirectorNew.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorAddress
                ListObjWICAddressDirectorEmployer = objWICDirectorNew.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorAddressEmployer
                ListObjWICPhoneDirector = objWICDirectorNew.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorPhone
                ListObjWICPhoneDirectorEmployer = objWICDirectorNew.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorPhoneEmployer
                ListObjWICIdentificationDirector = objWICDirectorNew.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorIdentification

                GridEmail_Director.IsViewMode = True
                GridSanction_Director.IsViewMode = True
                GridPEP_Director.IsViewMode = True

                GridEmail_Director.LoadData(objWICDirectorNew.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorEmail)
                GridSanction_Director.LoadData(objWICDirectorNew.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorSanction)
                GridPEP_Director.LoadData(objWICDirectorNew.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorPEP)

                BindDetailAddress(StoreDirectorAddress, ListObjWICAddressDirector)
                BindDetailAddress(StoreDirectorAddressEmployer, ListObjWICAddressDirectorEmployer)
                BindDetailPhone(StoreDirectorPhone, ListObjWICPhoneDirector)
                BindDetailPhone(StoreDirectorPhoneEmployer, ListObjWICPhoneDirectorEmployer)
                BindDetailIdentification(StoreIdentificationDirector, ListObjWICIdentificationDirector)
            End With
        End If

        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then

        '    'agam 22102020
        '    Dim objDirectorOld As goAML_Ref_Walk_In_Customer_Director
        '    If ListObjWICDirectorBefore.Count > 0 Then
        '        objDirectorOld = ListObjWICDirectorBefore.Where(Function(x) x.NO_ID = iD).FirstOrDefault
        '    End If

        '    If objDirectorOld Is Nothing Then
        '        ClearDetailDirector()
        '        With objDirector
        '            DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
        '            DspGelarDirector.Text = .Title
        '            DspNamaLengkap.Text = .Last_Name
        '            DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
        '            If .BirthDate IsNot Nothing Then
        '                DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yyy")
        '            End If
        '            DspTempatLahir.Text = .Birth_Place
        '            DspNamaIbuKandung.Text = .Mothers_Name
        '            DspNamaAlias.Text = .Alias
        '            DspNIK.Text = .SSN
        '            DspNoPassport.Text = .Passport_Number
        '            DspNegaraPenerbitPassport.Text = .Passport_Country
        '            DspNoIdentitasLain.Text = .ID_Number
        '            DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
        '            DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
        '            DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
        '            DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
        '            DspEmail.Text = .Email
        '            DspEmail2.Text = .Email2
        '            DspEmail3.Text = .Email3
        '            DspEmail4.Text = .Email4
        '            DspEmail5.Text = .Email5
        '            DspDeceased.Text = .Deceased
        '            If .Deceased_Date IsNot Nothing Then
        '                DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            DspPEP.Text = .Tax_Reg_Number
        '            DspNPWP.Text = .Tax_Number
        '            DspSourceofWealth.Text = .Source_of_Wealth
        '            DspOccupation.Text = .Occupation
        '            DspCatatan.Text = .Comments
        '            DspTempatBekerja.Text = .Employer_Name

        '            BindDetailAddress(StoreDirectorAddress, ListObjWICAddressDirector)
        '            BindDetailAddress(StoreDirectorAddressEmployer, ListObjWICAddressDirectorEmployer)

        '            BindDetailPhone(StoreDirectorPhoneBefore, ListObjWICPhoneDirector)
        '            BindDetailPhone(StoreDirectorPhoneEmployer, ListObjWICPhoneDirectorEmployer)

        '            BindDetailIdentification(StoreIdentificationDirector, ListObjWICIdentificationDirector)

        '        End With
        '    Else
        '        ClearDetailDirectorEdit()
        '        With objDirector
        '            DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(objDirectorOld.Role) + " => " + GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
        '            DspGelarDirector.Text = objDirectorOld.Title + " => " + .Title
        '            DspNamaLengkap.Text = objDirectorOld.Last_Name + " => " + .Last_Name
        '            DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(objDirectorOld.Gender) + " => " + GlobalReportFunctionBLL.getGenderbyKode(.Gender)
        '            If .BirthDate IsNot Nothing And objDirectorOld.BirthDate IsNot Nothing Then
        '                DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yyy") + " => " + objDirectorOld.BirthDate.Value.ToString("dd-MMM-yy")
        '            ElseIf .BirthDate Is Nothing And objDirectorOld.BirthDate Is Nothing Then
        '                DspTanggalLahir.Text = " => "
        '            ElseIf .BirthDate IsNot Nothing Then
        '                DspTanggalLahir.Text = .BirthDate + " => "
        '            ElseIf objWICOld.ObjWIC.INDV_BirthDate IsNot Nothing Then
        '                DspTanggalLahir.Text = " => " + objDirectorOld.BirthDate
        '            End If
        '            DspTempatLahir.Text = objDirectorOld.Birth_Place + " => " + .Birth_Place
        '            DspNamaIbuKandung.Text = objDirectorOld.Mothers_Name + " => " + .Mothers_Name
        '            DspNamaAlias.Text = objDirectorOld.Alias + " => " + .Alias
        '            DspNIK.Text = objDirectorOld.SSN + " => " + .SSN
        '            DspNoPassport.Text = objDirectorOld.Passport_Number + " => " + .Passport_Number
        '            DspNegaraPenerbitPassport.Text = objDirectorOld.Passport_Country + " => " + .Passport_Country
        '            DspNoIdentitasLain.Text = objDirectorOld.ID_Number + " => " + .ID_Number
        '            DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(objDirectorOld.Nationality1) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
        '            DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(objDirectorOld.Nationality2) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
        '            DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(objDirectorOld.Nationality3) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
        '            DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(objDirectorOld.Residence) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Residence)
        '            DspEmail.Text = objDirectorOld.Email + " => " + .Email
        '            DspEmail2.Text = objDirectorOld.Email2 + " => " + .Email2
        '            DspEmail3.Text = objDirectorOld.Email3 + " => " + .Email3
        '            DspEmail4.Text = objDirectorOld.Email4 + " => " + .Email4
        '            DspEmail5.Text = objDirectorOld.Email5 + " => " + .Email5
        '            DspDeceased.Text = Convert.ToString(objDirectorOld.Deceased) + " => " + Convert.ToString(.Deceased)
        '            If objDirectorOld.Deceased_Date IsNot Nothing And .Deceased_Date IsNot Nothing Then
        '                DspDeceasedDate.Text = objDirectorOld.Deceased_Date + " => " + .Deceased_Date
        '            ElseIf objDirectorOld.Deceased_Date Is Nothing And .Deceased_Date Is Nothing Then
        '                DspDeceasedDate.Text = " => "
        '            ElseIf objDirectorOld.Deceased_Date IsNot Nothing Then
        '                DspDeceasedDate.Text = objDirectorOld.Deceased_Date + " => "
        '            ElseIf .Deceased_Date IsNot Nothing Then
        '                DspDeceasedDate.Text = " => " + .Deceased_Date
        '            End If
        '            DspPEP.Text = objDirectorOld.Tax_Reg_Number + " => " + .Tax_Reg_Number
        '            DspNPWP.Text = objDirectorOld.Tax_Number + " => " + .Tax_Number
        '            DspSourceofWealth.Text = objDirectorOld.Source_of_Wealth + " => " + .Source_of_Wealth
        '            DspOccupation.Text = objDirectorOld.Occupation + " => " + .Occupation
        '            DspCatatan.Text = objDirectorOld.Comments + " => " + .Comments
        '            DspTempatBekerja.Text = objDirectorOld.Employer_Name + " => " + .Employer_Name

        '            GridPanelDirectorAddressBefore.Show()
        '            GridPanelDirectorAddressEmployerBefore.Show()
        '            GridPanelDirectorPhoneBefore.Show()
        '            GridPanelDirectorPhoneEmployerBefore.Show()
        '            GridPanelDirectorIdentificationBefore.Show()

        '            GridPanelDirectorPhone.Show()
        '            GridPanelDirectorAddress.Show()
        '            GridPanelDirectorPhoneEmployer.Show()
        '            GridPanelDirectorAddressEmployer.Show()
        '            GridPanelDirectordentification.Show()

        '            BindDetailAddress(StoreDirectorAddressBefore, ListObjWICAddressDirectorBefore)
        '            BindDetailAddress(StoreDirectorAddressEmployerBefore, ListObjWICAddressDirectorEmployerBefore)
        '            BindDetailAddress(StoreDirectorAddress, ListObjWICAddressDirector)
        '            BindDetailAddress(StoreDirectorAddressEmployer, ListObjWICAddressDirectorEmployer)

        '            BindDetailPhone(StoreDirectorPhoneBefore, ListObjWICPhoneDirectorBefore)
        '            BindDetailPhone(StoreDirectorPhone, ListObjWICPhoneDirector)
        '            BindDetailPhone(StoreDirectorPhoneEmployerBefore, ListObjWICPhoneDirectorEmployerBefore)
        '            BindDetailPhone(StoreDirectorPhoneEmployer, ListObjWICPhoneDirectorEmployer)

        '            BindDetailIdentification(StoreIdentificationDirector, ListObjWICIdentificationDirector)
        '            BindDetailIdentification(StoreIdentificationDirectorBefore, ListObjWICIdentificationDirectorBefore)

        '        End With
        '    End If

        'Else
        '    Dim objDirector As goAML_Ref_Walk_In_Customer_Director = ListObjWICDirector.Where(Function(x) x.NO_ID = iD).FirstOrDefault
        '    ClearDetailDirector()
        '    With objDirector
        '        DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
        '        DspGelarDirector.Text = .Title
        '        DspNamaLengkap.Text = .Last_Name
        '        DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
        '        If .BirthDate IsNot Nothing Then
        '            DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yyy")
        '        End If
        '        DspTempatLahir.Text = .Birth_Place
        '        DspNamaIbuKandung.Text = .Mothers_Name
        '        DspNamaAlias.Text = .Alias
        '        DspNIK.Text = .SSN
        '        DspNoPassport.Text = .Passport_Number
        '        DspNegaraPenerbitPassport.Text = .Passport_Country
        '        DspNoIdentitasLain.Text = .ID_Number
        '        DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
        '        DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
        '        DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
        '        DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
        '        DspEmail.Text = .Email
        '        DspEmail2.Text = .Email2
        '        DspEmail3.Text = .Email3
        '        DspEmail4.Text = .Email4
        '        DspEmail5.Text = .Email5
        '        DspDeceased.Text = .Deceased
        '        If .Deceased_Date IsNot Nothing Then
        '            DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
        '        End If
        '        DspPEP.Text = .Tax_Reg_Number
        '        DspNPWP.Text = .Tax_Number
        '        DspSourceofWealth.Text = .Source_of_Wealth
        '        DspOccupation.Text = .Occupation
        '        DspCatatan.Text = .Comments
        '        DspTempatBekerja.Text = .Employer_Name

        '        BindDetailAddress(StoreDirectorAddress, ListObjWICAddressDirector)
        '        BindDetailAddress(StoreDirectorAddressEmployer, ListObjWICAddressDirectorEmployer)

        '        BindDetailPhone(StoreDirectorPhone, ListObjWICPhoneDirector)
        '        BindDetailPhone(StoreDirectorPhoneEmployer, ListObjWICPhoneDirectorEmployer)

        '        BindDetailIdentification(StoreIdentificationDirector, ListObjWICIdentificationDirector)

        '    End With
        'End If
    End Sub
    Private Sub DetailDirectorCorpBefore(iD As String)
        WindowDetailDirector.Show()

        GridPanelDirectorPhoneBefore.Show()
        GridPanelDirectorAddressBefore.Show()
        GridPanelDirectorPhoneEmployerBefore.Show()
        GridPanelDirectorAddressEmployerBefore.Show()
        GridPanelDirectorIdentificationBefore.Show()

        GridPanelDirectorPhone.Hide()
        GridPanelDirectorAddress.Hide()
        GridPanelDirectorPhoneEmployer.Hide()
        GridPanelDirectorAddressEmployer.Hide()
        GridPanelDirectordentification.Hide()

        Dim objDirector As goAML_Ref_Walk_In_Customer_Director = ListObjWICDirectorBefore.Where(Function(x) x.NO_ID = iD).FirstOrDefault
        If objDirector IsNot Nothing Then
            ClearDetailDirector()
            With objDirector
                DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
                DspGelarDirector.Text = .Title
                DspNamaLengkap.Text = .Last_Name
                DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                If .BirthDate IsNot Nothing Then
                    DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yyy")
                End If
                DspTempatLahir.Text = .Birth_Place
                DspNamaIbuKandung.Text = .Mothers_Name
                DspNamaAlias.Text = .Alias
                DspNIK.Text = .SSN
                DspNoPassport.Text = .Passport_Number
                DspNegaraPenerbitPassport.Text = .Passport_Country
                DspNoIdentitasLain.Text = .ID_Number
                DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                'DspEmail.Text = .Email
                'DspEmail2.Text = .Email2
                'DspEmail3.Text = .Email3
                'DspEmail4.Text = .Email4
                'DspEmail5.Text = .Email5
                DspDeceased.Text = .Deceased
                If .Deceased_Date IsNot Nothing Then
                    DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                End If
                DspPEP.Text = .Tax_Reg_Number
                DspNPWP.Text = .Tax_Number
                DspSourceofWealth.Text = .Source_of_Wealth
                DspOccupation.Text = .Occupation
                DspCatatan.Text = .Comments
                DspTempatBekerja.Text = .Employer_Name

                ListObjWICAddressDirector = objWICDirectorOld.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorAddress
                ListObjWICAddressDirectorEmployer = objWICDirectorOld.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorAddressEmployer
                ListObjWICPhoneDirector = objWICDirectorOld.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorPhone
                ListObjWICPhoneDirectorEmployer = objWICDirectorOld.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorPhoneEmployer
                ListObjWICIdentificationDirector = objWICDirectorOld.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorIdentification

                GridEmail_Director.IsViewMode = True
                GridSanction_Director.IsViewMode = True
                GridPEP_Director.IsViewMode = True

                'START: 2023-10-21, Nael
                Dim listDirAddressBefore = ListObjWICAddressDirector.Where(Function(x) x.FK_To_Table_ID = iD).ToList()
                Dim listDirAddressEmpBefore = ListObjWICAddressDirectorEmployer.Where(Function(x) x.FK_To_Table_ID = iD).ToList()
                Dim listDirPhoneBefore = ListObjWICPhoneDirector.Where(Function(x) x.FK_for_Table_ID = iD).ToList()
                Dim listDirPhoneEmpBefore = ListObjWICPhoneDirectorEmployer.Where(Function(x) x.FK_for_Table_ID = iD).ToList()
                Dim listDirIdenBefore = ListObjWICIdentificationDirector.Where(Function(x) x.FK_Person_ID = iD).ToList()
                'END: 2023-10-21, Nael
                GridEmail_Director.LoadData(objWICDirectorOld.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorEmail)
                GridSanction_Director.LoadData(objWICDirectorOld.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorSanction)
                GridPEP_Director.LoadData(objWICDirectorOld.Where(Function(x) x.ObjDirector.NO_ID = iD).FirstOrDefault().ListDirectorPEP)

                BindDetailAddress(StoreDirectorAddressBefore, listDirAddressBefore)
                BindDetailAddress(StoreDirectorAddressEmployerBefore, listDirAddressEmpBefore)
                BindDetailPhone(StoreDirectorPhoneBefore, listDirPhoneBefore)
                BindDetailPhone(StoreDirectorPhoneEmployerBefore, listDirPhoneEmpBefore)
                BindDetailIdentification(StoreIdentificationDirectorBefore, listDirIdenBefore)
            End With
        End If
        'WindowDetailDirector.Show()
        'ClearDetailDirector()
        'Dim objDirector As goAML_Ref_Walk_In_Customer_Director = ListObjWICDirectorBefore.Where(Function(x) x.NO_ID = iD).FirstOrDefault

        'With objDirector
        '    DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
        '    DspGelarDirector.Text = .Title
        '    DspNamaLengkap.Text = .Last_Name
        '    DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
        '    If .BirthDate IsNot Nothing Then
        '        DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yyy")
        '    End If
        '    DspTempatLahir.Text = .Birth_Place
        '    DspNamaIbuKandung.Text = .Mothers_Name
        '    DspNamaAlias.Text = .Alias
        '    DspNIK.Text = .SSN
        '    DspNoPassport.Text = .Passport_Number
        '    DspNegaraPenerbitPassport.Text = .Passport_Country
        '    DspNoIdentitasLain.Text = .ID_Number
        '    DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
        '    DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
        '    DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
        '    DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
        '    DspEmail.Text = .Email
        '    DspEmail2.Text = .Email2
        '    DspEmail3.Text = .Email3
        '    DspEmail4.Text = .Email4
        '    DspEmail5.Text = .Email5
        '    DspDeceased.Text = .Deceased
        '    If .Deceased_Date IsNot Nothing Then
        '        DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
        '    End If
        '    DspPEP.Text = .Tax_Reg_Number
        '    DspNPWP.Text = .Tax_Number
        '    DspSourceofWealth.Text = .Source_of_Wealth
        '    DspOccupation.Text = .Occupation
        '    DspCatatan.Text = .Comments
        '    DspTempatBekerja.Text = .Employer_Name

        '    GridPanelDirectorAddress.Hide()
        '    GridPanelDirectorAddressBefore.Show()

        '    GridPanelDirectorAddressEmployer.Hide()
        '    GridPanelDirectorAddressEmployerBefore.Show()

        '    GridPanelDirectorPhone.Hide()
        '    GridPanelDirectorPhoneBefore.Show()

        '    GridPanelDirectorPhoneEmployer.Hide()
        '    GridPanelDirectorPhoneEmployerBefore.Show()

        '    GridPanelDirectordentification.Hide()
        '    GridPanelDirectorIdentificationBefore.Show()

        '    BindDetailAddress(StoreDirectorAddressBefore, ListObjWICAddressDirectorBefore)
        '    BindDetailAddress(StoreDirectorAddressEmployerBefore, ListObjWICAddressDirectorEmployerBefore)

        '    BindDetailPhone(StoreDirectorPhoneBefore, ListObjWICPhoneDirectorBefore)
        '    BindDetailPhone(StoreDirectorPhoneEmployerBefore, ListObjWICPhoneDirectorEmployerBefore)
        'End With
    End Sub

    'Address
    Private Sub ClearDetailAddress()
        DspAddress_type.Text = ""
        DspAddress.Text = ""
        DspTown.Text = ""
        DspCity.Text = ""
        DspZip.Text = ""
        Dspcountry_code.Text = ""
        DspState.Text = ""
        DspcommentsAddress.Text = ""
    End Sub
    Private Sub ClearDetailAddressEmployer()
        DspAddress_typeEmployer.Text = ""
        DspAddressEmployer.Text = ""
        DspTownEmployer.Text = ""
        DspCityEmployer.Text = ""
        DspZipEmployer.Text = ""
        Dspcountry_codeEmployer.Text = ""
        DspStateEmployer.Text = ""
        DspcommentsAddressEmployer.Text = ""
    End Sub
    Private Sub ClearDetailAddressDirector()
        DspTipeAlamatDirector.Text = ""
        DspAlamatDirector.Text = ""
        DspKecamatanDirector.Text = ""
        DspKotaKabupatenDirector.Text = ""
        DspKodePosDirector.Text = ""
        DspNegaraDirector.Text = ""
        DspProvinsiDirector.Text = ""
        DspCatatanAddressDirector.Text = ""
    End Sub
    Private Sub ClearDetailAddressDirectorEmployer()
        DspTipeAlamatDirectorEmployer.Text = ""
        DspAlamatDirectorEmployer.Text = ""
        DspKecamatanDirectorEmployer.Text = ""
        DspKotaKabupatenDirectorEmployer.Text = ""
        DspKodePosDirectorEmployer.Text = ""
        DspNegaraDirectorEmployer.Text = ""
        DspProvinsiDirectorEmployer.Text = ""
        DspCatatanAddressDirectorEmployer.Text = ""
    End Sub
    Private Sub DetailAddressCorp(iD As String)
        WindowDetailAddress.Show()
        PanelDetailAddress.Show()
        PanelDetailAddressEmployer.Hide()
        ClearDetailAddress()
        Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    'Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    'Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        '    Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault

        '    If objAddressOld Is Nothing Then
        '        With objAddress
        '            DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAddress.Text = .Address
        '            DspTown.Text = .Town
        '            DspCity.Text = .City
        '            DspZip.Text = .Zip
        '            Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspState.Text = .State
        '            DspcommentsAddress.Text = .Comments
        '        End With
        '    Else
        '        With objAddress
        '            DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objAddressOld.Address_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAddress.Text = objAddressOld.Address + " => " + .Address
        '            DspTown.Text = objAddressOld.Town + " => " + .Town
        '            DspCity.Text = objAddressOld.City + " => " + .City
        '            DspZip.Text = objAddressOld.Zip + " => " + .Zip
        '            Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(objAddressOld.Country_Code) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspState.Text = objAddressOld.State + " => " + .State
        '            DspcommentsAddress.Text = objAddressOld.Comments + " => " + .Comments
        '        End With
        '    End If

        'Else
        '    'Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault

        '    With objAddress
        '        DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '        DspAddress.Text = .Address
        '        DspTown.Text = .Town
        '        DspCity.Text = .City
        '        DspZip.Text = .Zip
        '        Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '        DspState.Text = .State
        '        DspcommentsAddress.Text = .Comments
        '    End With
        'End If
    End Sub

    Private Sub DetailAddressCorpBefore(iD As String)
        WindowDetailAddress.Show()
        PanelDetailAddress.Show()
        PanelDetailAddressEmployer.Hide()
        ClearDetailAddress()
        'Dim objAddress As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
    End Sub

    Private Sub DetailAddressINDV(iD As String)
        WindowDetailAddress.Show()
        PanelDetailAddress.Show()
        PanelDetailAddressEmployer.Hide()
        ClearDetailAddress()
        Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    'Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    'Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        '    Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault

        '    If objAddressOld Is Nothing Then
        '        With objAddress
        '            DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAddress.Text = .Address
        '            DspTown.Text = .Town
        '            DspCity.Text = .City
        '            DspZip.Text = .Zip
        '            Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspState.Text = .State
        '            DspcommentsAddress.Text = .Comments
        '        End With
        '    Else
        '        With objAddress
        '            DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objAddressOld.Address_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAddress.Text = objAddressOld.Address + " => " + .Address
        '            DspTown.Text = objAddressOld.Town + " => " + .Town
        '            DspCity.Text = objAddressOld.City + " => " + .City
        '            DspZip.Text = objAddressOld.Zip + " => " + .Zip
        '            Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(objAddressOld.Country_Code) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspState.Text = objAddressOld.State + " => " + .State
        '            DspcommentsAddress.Text = objAddressOld.Comments + " => " + .Comments
        '        End With
        '    End If

        'Else
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault

        '    With objAddress
        '        DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '        DspAddress.Text = .Address
        '        DspTown.Text = .Town
        '        DspCity.Text = .City
        '        DspZip.Text = .Zip
        '        Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '        DspState.Text = .State
        '        DspcommentsAddress.Text = .Comments
        '    End With
        'End If
    End Sub

    Private Sub DetailAddressINDVBefore(iD As String)
        WindowDetailAddress.Show()
        PanelDetailAddress.Show()
        PanelDetailAddressEmployer.Hide()
        ClearDetailAddress()
        'Dim objAddress As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
    End Sub

    Private Sub DetailAddressINDVEmployer(iD As String)
        WindowDetailAddress.Show()
        PanelDetailAddress.Hide()
        PanelDetailAddressEmployer.Show()
        ClearDetailAddressEmployer()
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressEmployer.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddressEmployer.Text = .Address
                DspTownEmployer.Text = .Town
                DspCityEmployer.Text = .City
                DspZipEmployer.Text = .Zip
                Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspStateEmployer.Text = .State
                DspcommentsAddressEmployer.Text = .Comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddressEmployer.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault
        '    Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressEmployerBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault

        '    If objAddressOld Is Nothing Then
        '        With objAddress
        '            DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAddressEmployer.Text = .Address
        '            DspTownEmployer.Text = .Town
        '            DspCityEmployer.Text = .City
        '            DspZipEmployer.Text = .Zip
        '            Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspStateEmployer.Text = .State
        '            DspcommentsAddressEmployer.Text = .Comments
        '        End With
        '    Else
        '        With objAddress
        '            DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objAddressOld.Address_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAddressEmployer.Text = objAddressOld.Address + " => " + .Address
        '            DspTownEmployer.Text = objAddressOld.Town + " => " + .Town
        '            DspCityEmployer.Text = objAddressOld.City + " => " + .City
        '            DspZipEmployer.Text = objAddressOld.Zip + " => " + .Zip
        '            Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(objAddressOld.Country_Code) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspStateEmployer.Text = objAddressOld.State + " => " + .State
        '            DspcommentsAddressEmployer.Text = objAddressOld.Comments + " => " + .Comments
        '        End With
        '    End If

        'Else
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddressEmployer.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault

        '    With objAddress
        '        DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '        DspAddressEmployer.Text = .Address
        '        DspTownEmployer.Text = .Town
        '        DspCityEmployer.Text = .City
        '        DspZipEmployer.Text = .Zip
        '        Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '        DspStateEmployer.Text = .State
        '        DspcommentsAddressEmployer.Text = .Comments
        '    End With
        'End If
    End Sub

    Private Sub DetailAddressINDVEmployerBefore(iD As String)
        WindowDetailAddress.Show()
        PanelDetailAddress.Hide()
        PanelDetailAddressEmployer.Show()
        ClearDetailAddressEmployer()
        'Dim objAddress As goAML_Ref_Address = ListObjWICAddressEmployerBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressEmployerBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddressEmployer.Text = .Address
                DspTownEmployer.Text = .Town
                DspCityEmployer.Text = .City
                DspZipEmployer.Text = .Zip
                Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspStateEmployer.Text = .State
                DspcommentsAddressEmployer.Text = .Comments
            End With
        End If
    End Sub

    Private Sub DetailAddressEmployerDirector(iD As String)
        WindowDetailDirectorAddress.Show()
        PanelAddressDirector.Hide()
        PanelAddressDirectorEmployer.Show()
        ClearDetailAddressDirectorEmployer()
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorEmployer.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAlamatDirectorEmployer.Text = .Address
                DspKecamatanDirectorEmployer.Text = .Town
                DspKotaKabupatenDirectorEmployer.Text = .City
                DspKodePosDirectorEmployer.Text = .Zip
                DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspProvinsiDirectorEmployer.Text = .State
                DspCatatanAddressDirectorEmployer.Text = .Comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorEmployer.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 9).FirstOrDefault
        '    Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressDirectorEmployerBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 9).FirstOrDefault

        '    If objAddressOld Is Nothing Then
        '        With objAddress
        '            DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAlamatDirectorEmployer.Text = .Address
        '            DspKecamatanDirectorEmployer.Text = .Town
        '            DspKotaKabupatenDirectorEmployer.Text = .City
        '            DspKodePosDirectorEmployer.Text = .Zip
        '            DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspProvinsiDirectorEmployer.Text = .State
        '            DspCatatanAddressDirectorEmployer.Text = .Comments
        '        End With
        '    Else
        '        With objAddress
        '            DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objAddressOld.Address_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAlamatDirectorEmployer.Text = objAddressOld.Address + " => " + .Address
        '            DspKecamatanDirectorEmployer.Text = objAddressOld.Town + " => " + .Town
        '            DspKotaKabupatenDirectorEmployer.Text = objAddressOld.City + " => " + .City
        '            DspKodePosDirectorEmployer.Text = objAddressOld.Zip + " => " + .Zip
        '            DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(objAddressOld.Country_Code) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspProvinsiDirectorEmployer.Text = objAddressOld.State + " => " + .State
        '            DspCatatanAddressDirectorEmployer.Text = objAddressOld.Comments + " => " + .Comments
        '        End With
        '    End If

        'Else
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorEmployer.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 9).FirstOrDefault

        '    With objAddress
        '        DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '        DspAlamatDirectorEmployer.Text = .Address
        '        DspKecamatanDirectorEmployer.Text = .Town
        '        DspKotaKabupatenDirectorEmployer.Text = .City
        '        DspKodePosDirectorEmployer.Text = .Zip
        '        DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '        DspProvinsiDirectorEmployer.Text = .State
        '        DspCatatanAddressDirectorEmployer.Text = .Comments
        '    End With
        'End If
    End Sub

    Private Sub DetailAddressEmployerDirectorBefore(iD As String)
        WindowDetailDirectorAddress.Show()
        PanelAddressDirector.Hide()
        PanelAddressDirectorEmployer.Show()
        ClearDetailAddressDirectorEmployer()
        'Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorEmployerBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 9).FirstOrDefault
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorEmployerBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAlamatDirectorEmployer.Text = .Address
                DspKecamatanDirectorEmployer.Text = .Town
                DspKotaKabupatenDirectorEmployer.Text = .City
                DspKodePosDirectorEmployer.Text = .Zip
                DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspProvinsiDirectorEmployer.Text = .State
                DspCatatanAddressDirectorEmployer.Text = .Comments
            End With
        End If
    End Sub

    Private Sub DetailAddressDirector(iD As String)
        WindowDetailDirectorAddress.Show()
        PanelAddressDirector.Show()
        PanelAddressDirectorEmployer.Hide()
        ClearDetailAddressDirector()
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirector.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAlamatDirector.Text = .Address
                DspKecamatanDirector.Text = .Town
                DspKotaKabupatenDirector.Text = .City
                DspKodePosDirector.Text = .Zip
                DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspProvinsiDirector.Text = .State
                DspCatatanAddressDirector.Text = .Comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirector.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault
        '    Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressDirectorBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault

        '    If objAddressOld Is Nothing Then
        '        With objAddress
        '            DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAlamatDirector.Text = .Address
        '            DspKecamatanDirector.Text = .Town
        '            DspKotaKabupatenDirector.Text = .City
        '            DspKodePosDirector.Text = .Zip
        '            DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspProvinsiDirector.Text = .State
        '            DspCatatanAddressDirector.Text = .Comments
        '        End With
        '    Else
        '        With objAddress
        '            DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objAddressOld.Address_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAlamatDirector.Text = objAddressOld.Address + " => " + .Address
        '            DspKecamatanDirector.Text = objAddressOld.Town + " => " + .Town
        '            DspKotaKabupatenDirector.Text = objAddressOld.City + " => " + .City
        '            DspKodePosDirector.Text = objAddressOld.Zip + " => " + .Zip
        '            DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(objAddressOld.Country_Code) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspProvinsiDirector.Text = objAddressOld.State + " => " + .State
        '            DspCatatanAddressDirector.Text = objAddressOld.Comments + " => " + .Comments
        '        End With
        '    End If

        'Else
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirector.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault

        '    With objAddress
        '        DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '        DspAlamatDirector.Text = .Address
        '        DspKecamatanDirector.Text = .Town
        '        DspKotaKabupatenDirector.Text = .City
        '        DspKodePosDirector.Text = .Zip
        '        DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '        DspProvinsiDirector.Text = .State
        '        DspCatatanAddressDirector.Text = .Comments
        '    End With
        'End If
    End Sub

    Private Sub DetailAddressDirectorBefore(iD As String)
        WindowDetailDirectorAddress.Show()
        PanelAddressDirector.Show()
        PanelAddressDirectorEmployer.Hide()
        ClearDetailAddressDirector()
        'Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAlamatDirector.Text = .Address
                DspKecamatanDirector.Text = .Town
                DspKotaKabupatenDirector.Text = .City
                DspKodePosDirector.Text = .Zip
                DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspProvinsiDirector.Text = .State
                DspCatatanAddressDirector.Text = .Comments
            End With
        End If
    End Sub


    'Phone
    Private Sub ClearDetailPhone()
        Dsptph_contact_type.Text = ""
        Dsptph_communication_type.Text = ""
        Dsptph_country_prefix.Text = ""
        Dsptph_number.Text = ""
        Dsptph_extension.Text = ""
        DspcommentsPhone.Text = ""
    End Sub

    Private Sub ClearDetailPhoneEmployer()
        Dsptph_contact_typeEmployer.Text = ""
        Dsptph_communication_typeEmployer.Text = ""
        Dsptph_country_prefixEmployer.Text = ""
        Dsptph_numberEmployer.Text = ""
        Dsptph_extensionEmployer.Text = ""
        DspcommentsPhoneEmployer.Text = ""
    End Sub

    Private Sub ClearDetailPhoneDirector()
        DspKategoriKontakDirector.Text = ""
        DspJenisAlatKomunikasiDirector.Text = ""
        DspKodeAreaTelpDirector.Text = ""
        DspNomorTeleponDirector.Text = ""
        DspNomorExtensiDirector.Text = ""
        DspCatatanDirectorPhoneDirector.Text = ""
    End Sub

    Private Sub ClearDetailPhoneDirectorEmployer()
        DspKategoriKontakDirectorEmployer.Text = ""
        DspJenisAlatKomunikasiDirectorEmployer.Text = ""
        DspKodeAreaTelpDirectorEmployer.Text = ""
        DspNomorTeleponDirectorEmployer.Text = ""
        DspNomorExtensiDirectorEmployer.Text = ""
        DspCatatanDirectorPhoneDirectorEmployer.Text = ""
    End Sub
    Private Sub DetailPhoneCorp(iD As String)
        WindowDetailPhone.Show()
        PanelDetailPhone.Show()
        PanelDetailPhoneEmployer.Hide()
        ClearDetailPhone()
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhone.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhone.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    Dim objPhoneOld As goAML_Ref_Phone = ListObjWICPhoneBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault

        '    If objPhoneOld Is Nothing Then
        '        With objPhone
        '            Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            Dsptph_country_prefix.Text = .tph_country_prefix
        '            Dsptph_number.Text = .tph_number
        '            Dsptph_extension.Text = .tph_extension
        '            DspcommentsPhone.Text = .comments
        '        End With
        '    Else
        '        With objPhone
        '            Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objPhone.Tph_Contact_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(objPhoneOld.Tph_Communication_Type) + " => " + GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            Dsptph_country_prefix.Text = objPhoneOld.tph_country_prefix + " => " + .tph_country_prefix
        '            Dsptph_number.Text = objPhoneOld.tph_number + " => " + .tph_number
        '            Dsptph_extension.Text = objPhoneOld.tph_extension + " => " + .tph_extension
        '            DspcommentsPhone.Text = objPhoneOld.comments + " => " + .comments
        '        End With
        '    End If

        'Else
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhone.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault

        '    With objPhone
        '        Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '        Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '        Dsptph_country_prefix.Text = .tph_country_prefix
        '        Dsptph_number.Text = .tph_number
        '        Dsptph_extension.Text = .tph_extension
        '        DspcommentsPhone.Text = .comments
        '    End With
        'End If
    End Sub

    Private Sub DetailPhoneCorpBefore(iD As String)
        WindowDetailPhone.Show()
        PanelDetailPhone.Show()
        PanelDetailPhoneEmployer.Hide()
        ClearDetailPhone()
        'Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
    End Sub

    Private Sub DetailPhoneINDV(iD As String)
        WindowDetailPhone.Show()
        PanelDetailPhone.Show()
        PanelDetailPhoneEmployer.Hide()
        ClearDetailPhone()
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhone.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhone.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    Dim objPhoneOld As goAML_Ref_Phone = ListObjWICPhoneBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault

        '    If objPhoneOld Is Nothing Then
        '        With objPhone
        '            Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            Dsptph_country_prefix.Text = .tph_country_prefix
        '            Dsptph_number.Text = .tph_number
        '            Dsptph_extension.Text = .tph_extension
        '            DspcommentsPhone.Text = .comments
        '        End With
        '    Else
        '        With objPhone
        '            Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objPhone.Tph_Contact_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(objPhoneOld.Tph_Communication_Type) + " => " + GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            Dsptph_country_prefix.Text = objPhoneOld.tph_country_prefix + " => " + .tph_country_prefix
        '            Dsptph_number.Text = objPhoneOld.tph_number + " => " + .tph_number
        '            Dsptph_extension.Text = objPhoneOld.tph_extension + " => " + .tph_extension
        '            DspcommentsPhone.Text = objPhoneOld.comments + " => " + .comments
        '        End With
        '    End If

        'Else
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhone.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault

        '    With objPhone
        '        Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '        Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '        Dsptph_country_prefix.Text = .tph_country_prefix
        '        Dsptph_number.Text = .tph_number
        '        Dsptph_extension.Text = .tph_extension
        '        DspcommentsPhone.Text = .comments
        '    End With
        'End If
    End Sub

    Private Sub DetailPhoneINDVBefore(iD As String)
        WindowDetailPhone.Show()
        PanelDetailPhone.Show()
        PanelDetailPhoneEmployer.Hide()
        ClearDetailPhone()
        'Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
    End Sub

    Private Sub DetailPhoneINDVEmployer(iD As String)
        WindowDetailPhone.Show()
        PanelDetailPhone.Hide()
        PanelDetailPhoneEmployer.Show()
        ClearDetailPhoneEmployer()
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneEmployer.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefixEmployer.Text = .tph_country_prefix
                Dsptph_numberEmployer.Text = .tph_number
                Dsptph_extensionEmployer.Text = .tph_extension
                DspcommentsPhoneEmployer.Text = .comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneEmployer.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault
        '    Dim objPhoneOld As goAML_Ref_Phone = ListObjWICPhoneEmployerBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault

        '    If objPhoneOld Is Nothing Then
        '        With objPhone
        '            Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            Dsptph_country_prefixEmployer.Text = .tph_country_prefix
        '            Dsptph_numberEmployer.Text = .tph_number
        '            Dsptph_extensionEmployer.Text = .tph_extension
        '            DspcommentsPhoneEmployer.Text = .comments
        '        End With
        '    Else
        '        With objPhone
        '            Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objPhone.Tph_Contact_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(objPhoneOld.Tph_Communication_Type) + " => " + GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            Dsptph_country_prefixEmployer.Text = objPhoneOld.tph_country_prefix + " => " + .tph_country_prefix
        '            Dsptph_numberEmployer.Text = objPhoneOld.tph_number + " => " + .tph_number
        '            Dsptph_extensionEmployer.Text = objPhoneOld.tph_extension + " => " + .tph_extension
        '            DspcommentsPhoneEmployer.Text = objPhoneOld.comments + " => " + .comments
        '        End With
        '    End If

        'Else
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneEmployer.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault

        '    With objPhone
        '        Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '        Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '        Dsptph_country_prefixEmployer.Text = .tph_country_prefix
        '        Dsptph_numberEmployer.Text = .tph_number
        '        Dsptph_extensionEmployer.Text = .tph_extension
        '        DspcommentsPhoneEmployer.Text = .comments
        '    End With
        'End If
    End Sub

    Private Sub DetailPhoneINDVEmployerBefore(iD As String)
        WindowDetailPhone.Show()
        PanelDetailPhone.Hide()
        PanelDetailPhoneEmployer.Show()
        ClearDetailPhoneEmployer()
        'Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneEmployerBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneEmployerBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefixEmployer.Text = .tph_country_prefix
                Dsptph_numberEmployer.Text = .tph_number
                Dsptph_extensionEmployer.Text = .tph_extension
                DspcommentsPhoneEmployer.Text = .comments
            End With
        End If
    End Sub

    Private Sub DetailPhoneEmployerDirector(iD As String)
        WindowDetailDirectorPhone.Show()
        PanelPhoneDirector.Hide()
        PanelPhoneDirectorEmployer.Show()
        ClearDetailPhoneDirectorEmployer()
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirectorEmployer.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        With objPhone
            DspKategoriKontakDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
            DspJenisAlatKomunikasiDirectorEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
            DspKodeAreaTelpDirectorEmployer.Text = .tph_country_prefix
            DspNomorTeleponDirectorEmployer.Text = .tph_number
            DspNomorExtensiDirectorEmployer.Text = .tph_extension
            DspCatatanDirectorPhoneDirectorEmployer.Text = .comments
        End With
    End Sub

    Private Sub DetailPhoneEmployerDirectorBefore(iD As String)
        WindowDetailDirectorPhone.Show()
        PanelPhoneDirector.Hide()
        PanelPhoneDirectorEmployer.Show()
        ClearDetailPhoneDirectorEmployer()
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirectorEmployerBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault

        With objPhone
            DspKategoriKontakDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
            DspJenisAlatKomunikasiDirectorEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
            DspKodeAreaTelpDirectorEmployer.Text = .tph_country_prefix
            DspNomorTeleponDirectorEmployer.Text = .tph_number
            DspNomorExtensiDirectorEmployer.Text = .tph_extension
            DspCatatanDirectorPhoneDirectorEmployer.Text = .comments
        End With
    End Sub

    Private Sub DetailPhoneDirector(iD As String)
        WindowDetailDirectorPhone.Show()
        PanelPhoneDirector.Show()
        PanelPhoneDirectorEmployer.Hide()
        ClearDetailPhoneDirector()
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirector.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                DspKodeAreaTelpDirector.Text = .tph_country_prefix
                DspNomorTeleponDirector.Text = .tph_number
                DspNomorExtensiDirector.Text = .tph_extension
                DspCatatanDirectorPhoneDirector.Text = .comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirector.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault
        '    Dim objPhoneOld As goAML_Ref_Phone = ListObjWICPhoneDirectorBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault

        '    If objPhoneOld Is Nothing Then
        '        With objPhone
        '            DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            DspKodeAreaTelpDirector.Text = .tph_country_prefix
        '            DspNomorTeleponDirector.Text = .tph_number
        '            DspNomorExtensiDirector.Text = .tph_extension
        '            DspCatatanDirectorPhoneDirector.Text = .comments
        '        End With
        '    Else
        '        With objPhone
        '            DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objPhone.Tph_Contact_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(objPhoneOld.Tph_Communication_Type) + " => " + GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            DspKodeAreaTelpDirector.Text = objPhoneOld.tph_country_prefix + " => " + .tph_country_prefix
        '            DspNomorTeleponDirector.Text = objPhoneOld.tph_number + " => " + .tph_number
        '            DspNomorExtensiDirector.Text = objPhoneOld.tph_extension + " => " + .tph_extension
        '            DspCatatanDirectorPhoneDirector.Text = objPhoneOld.comments + " => " + .comments
        '        End With
        '    End If

        'Else
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirector.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault

        '    With objPhone
        '        DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '        DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '        DspKodeAreaTelpDirector.Text = .tph_country_prefix
        '        DspNomorTeleponDirector.Text = .tph_number
        '        DspNomorExtensiDirector.Text = .tph_extension
        '        DspCatatanDirectorPhoneDirector.Text = .comments
        '    End With
        'End If
    End Sub

    Private Sub DetailPhoneDirectorBefore(iD As String)
        WindowDetailDirectorPhone.Show()
        PanelPhoneDirector.Show()
        PanelPhoneDirectorEmployer.Hide()
        ClearDetailPhoneDirector()
        'Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirectorBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirectorBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                DspKodeAreaTelpDirector.Text = .tph_country_prefix
                DspNomorTeleponDirector.Text = .tph_number
                DspNomorExtensiDirector.Text = .tph_extension
                DspCatatanDirectorPhoneDirector.Text = .comments
            End With
        End If
    End Sub


    'Identification
    Private Sub ClearDetailIdentification()
        DsptType.Text = ""
        DspNumber.Text = ""
        DspIssueDate.Text = ""
        DspExpiryDate.Text = ""
        DspIssuedBy.Text = ""
        DspIssuedCountry.Text = ""
        DspIdentificationComment.Text = ""
    End Sub
    Private Sub ClearDetailIdentificationDirector()
        DsptTypeDirector.Text = ""
        DspNumberDirector.Text = ""
        DspIssueDateDirector.Text = ""
        DspExpiryDateDirector.Text = ""
        DspIssuedByDirector.Text = ""
        DspIssuedCountryDirector.Text = ""
        DspIdentificationCommentDirector.Text = ""
    End Sub
    Private Sub DetailIdentidicationDirector(iD As String)
        WindowDetailDirectorIdentification.Show()
        PanelDetailIdentificationDirector.Show()
        ClearDetailIdentificationDirector()
        Dim objIdentification As goAML_Person_Identification = ListObjWICIdentificationDirector.Where(Function(x) x.PK_Person_Identification_ID = iD).FirstOrDefault
        If objIdentification IsNot Nothing Then
            With objIdentification
                DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                DspNumberDirector.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDateDirector.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDateDirector.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedByDirector.Text = .Issued_By
                DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                DspIdentificationCommentDirector.Text = .Identification_Comment
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objIdentification As goAML_Person_Identification = ListObjWICIdentificationDirector.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 7).FirstOrDefault
        '    Dim objIdentificationOld As goAML_Person_Identification = ListObjWICIdentificationDirectorBefore.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 7).FirstOrDefault

        '    If objIdentificationOld Is Nothing Then
        '        With objIdentificationOld
        '            If .Type IsNot Nothing Then
        '                DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
        '            End If
        '            DspNumberDirector.Text = .Number
        '            If .Issue_Date IsNot Nothing Then
        '                DspIssueDateDirector.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            If .Expiry_Date IsNot Nothing Then
        '                DspExpiryDateDirector.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            DspIssuedByDirector.Text = .Issued_By
        '            DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
        '            DspIdentificationCommentDirector.Text = .Identification_Comment
        '        End With
        '    Else
        '        With objIdentificationOld
        '            DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(objIdentificationOld.Type) + " => " + GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
        '            DspNumberDirector.Text = objIdentificationOld.Number + " => " + .Number
        '            If objIdentificationOld.Issue_Date IsNot Nothing And .Issue_Date IsNot Nothing Then
        '                DspIssueDateDirector.Text = objIdentificationOld.Issue_Date.Value.ToString("dd-MMM-yy") + " => " + .Issue_Date.Value.ToString("dd-MMM-yy")
        '            ElseIf objIdentificationOld.Issue_Date Is Nothing And .Issue_Date Is Nothing Then
        '                DspIssueDateDirector.Text = " => "
        '            ElseIf objIdentificationOld.Issue_Date IsNot Nothing Then
        '                DspIssueDateDirector.Text = objIdentificationOld.Issue_Date.Value.ToString("dd-MMM-yy") + " => "
        '            ElseIf .Issue_Date IsNot Nothing Then
        '                DspIssueDateDirector.Text = " => " + .Issue_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            If objIdentificationOld.Expiry_Date IsNot Nothing And .Expiry_Date IsNot Nothing Then
        '                DspExpiryDateDirector.Text = objIdentificationOld.Expiry_Date.Value.ToString("dd-MMM-yy") + " => " + .Expiry_Date.Value.ToString("dd-MMM-yy")
        '            ElseIf objIdentificationOld.Expiry_Date Is Nothing And .Expiry_Date Is Nothing Then
        '                DspExpiryDateDirector.Text = " => "
        '            ElseIf objIdentificationOld.Expiry_Date IsNot Nothing Then
        '                DspExpiryDateDirector.Text = objIdentificationOld.Expiry_Date.Value.ToString("dd-MMM-yy") + " => "
        '            ElseIf .Expiry_Date IsNot Nothing Then
        '                DspExpiryDateDirector.Text = " => " + .Expiry_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            DspIssuedByDirector.Text = objIdentificationOld.Issued_By + " => " + .Issued_By
        '            DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(objIdentificationOld.Issued_Country) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
        '            DspIdentificationCommentDirector.Text = objIdentificationOld.Identification_Comment + " => " + .Identification_Comment
        '        End With
        '    End If

        'Else
        '    Dim objIdentification As goAML_Person_Identification = ListObjWICIdentificationDirector.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 7).FirstOrDefault

        '    With objIdentification
        '        DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
        '        DspNumberDirector.Text = .Number
        '        If .Issue_Date IsNot Nothing Then
        '            DspIssueDateDirector.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
        '        End If
        '        If .Expiry_Date IsNot Nothing Then
        '            DspExpiryDateDirector.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
        '        End If
        '        DspIssuedByDirector.Text = .Issued_By
        '        DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
        '        DspIdentificationCommentDirector.Text = .Identification_Comment
        '    End With
        'End If
    End Sub

    Private Sub DetailIdentidicationDirectorBefore(iD As String)
        WindowDetailDirectorIdentification.Show()
        PanelDetailIdentificationDirector.Show()
        ClearDetailIdentificationDirector()
        'Dim objIdentification As goAML_Person_Identification = ListObjWICIdentificationDirectorBefore.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 7).FirstOrDefault
        Dim objIdentification As goAML_Person_Identification = ListObjWICIdentificationDirectorBefore.Where(Function(x) x.PK_Person_Identification_ID = iD).FirstOrDefault
        If objIdentification IsNot Nothing Then
            With objIdentification
                DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                DspNumberDirector.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDateDirector.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDateDirector.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedByDirector.Text = .Issued_By
                DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                DspIdentificationCommentDirector.Text = .Identification_Comment
            End With
        End If
    End Sub

    Private Sub DetailIdentidication(iD As String)
        WindowDetailIdentification.Show()
        PanelDetailIdentification.Show()
        ClearDetailIdentification()
        Dim objIdentification As goAML_Person_Identification = ListObjWICIdentification.Where(Function(x) x.PK_Person_Identification_ID = iD).FirstOrDefault
        If objIdentification IsNot Nothing Then
            With objIdentification
                If .Type IsNot Nothing Then
                    DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                End If
                DspNumber.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDate.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDate.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedBy.Text = .Issued_By
                DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                DspIdentificationComment.Text = .Identification_Comment
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objIdentification As goAML_Person_Identification = ListObjWICIdentification.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 8).FirstOrDefault
        '    Dim objIdentificationOld As goAML_Person_Identification = ListObjWICIdentificationBefore.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 8).FirstOrDefault

        '    If objIdentificationOld Is Nothing Then
        '        With objIdentificationOld
        '            DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
        '            DspNumber.Text = .Number
        '            If .Issue_Date IsNot Nothing Then
        '                DspIssueDate.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            If .Expiry_Date IsNot Nothing Then
        '                DspExpiryDate.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            DspIssuedBy.Text = .Issued_By
        '            DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
        '            DspIdentificationComment.Text = .Identification_Comment
        '        End With
        '    Else
        '        With objIdentificationOld
        '            DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(objIdentificationOld.Type) + " => " + GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
        '            DspNumber.Text = objIdentificationOld.Number + " => " + .Number

        '            If objIdentificationOld.Issue_Date IsNot Nothing And .Issue_Date IsNot Nothing Then
        '                DspIssueDate.Text = objIdentificationOld.Issue_Date.Value.ToString("dd-MMM-yy") + " => " + .Issue_Date.Value.ToString("dd-MMM-yy")
        '            ElseIf objIdentificationOld.Issue_Date Is Nothing And .Issue_Date Is Nothing Then
        '                DspIssueDate.Text = " => "
        '            ElseIf objIdentificationOld.Issue_Date IsNot Nothing Then
        '                DspIssueDate.Text = objIdentificationOld.Issue_Date.Value.ToString("dd-MMM-yy") + " => "
        '            ElseIf .Issue_Date IsNot Nothing Then
        '                DspIssueDate.Text = " => " + .Issue_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            If objIdentificationOld.Expiry_Date IsNot Nothing And .Expiry_Date IsNot Nothing Then
        '                DspExpiryDate.Text = objIdentificationOld.Expiry_Date.Value.ToString("dd-MMM-yy") + " => " + .Expiry_Date.Value.ToString("dd-MMM-yy")
        '            ElseIf objIdentificationOld.Expiry_Date Is Nothing And .Expiry_Date Is Nothing Then
        '                DspExpiryDate.Text = " => "
        '            ElseIf objIdentificationOld.Expiry_Date IsNot Nothing Then
        '                DspExpiryDate.Text = objIdentificationOld.Expiry_Date.Value.ToString("dd-MMM-yy") + " => "
        '            ElseIf .Expiry_Date IsNot Nothing Then
        '                DspExpiryDate.Text = " => " + .Expiry_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            DspIssuedBy.Text = objIdentificationOld.Issued_By + " => " + .Issued_By
        '            DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(objIdentificationOld.Issued_Country) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
        '            DspIdentificationComment.Text = objIdentificationOld.Identification_Comment + " => " + .Identification_Comment
        '        End With
        '    End If

        'Else
        '    Dim objIdentification As goAML_Person_Identification = ListObjWICIdentification.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 8).FirstOrDefault

        '    With objIdentification
        '        DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
        '        DspNumber.Text = .Number
        '        If .Issue_Date IsNot Nothing Then
        '            DspIssueDate.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
        '        End If
        '        If .Expiry_Date IsNot Nothing Then
        '            DspExpiryDate.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
        '        End If
        '        DspIssuedBy.Text = .Issued_By
        '        DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
        '        DspIdentificationComment.Text = .Identification_Comment
        '    End With
        'End If
    End Sub

    Private Sub DetailIdentidicationBefore(iD As String)
        WindowDetailIdentification.Show()
        PanelDetailIdentification.Show()
        ClearDetailIdentification()
        Dim objIdentification As goAML_Person_Identification = ListObjWICIdentificationBefore.Where(Function(x) x.PK_Person_Identification_ID = iD).FirstOrDefault
        If objIdentification IsNot Nothing Then
            With objIdentification
                If .Type IsNot Nothing Then
                    DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                End If
                DspNumber.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDate.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDate.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedBy.Text = .Issued_By
                DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                DspIdentificationComment.Text = .Identification_Comment
            End With
        End If
    End Sub


#End Region


#Region "Direct Events"
    Protected Sub btnBackSaveCustomerDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailDirector.Hide()
        'daniel 19 jan 2021
        'FormPanelDirectorDetail.Hide()
        'end 19 jan 2021
    End Sub
    Protected Sub BtnCancelIdentificationDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailDirectorIdentification.Hide()
        PanelDetailIdentificationDirector.Hide()
        FormPanelDirectorDetail.Show()
    End Sub
    Protected Sub BtnCancelIdentification_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailIdentification.Hide()
        PanelDetailIdentification.Hide()
    End Sub
    Protected Sub BtnCancelDirectorPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailDirectorPhone.Hide()
        PanelPhoneDirector.Hide()
        FormPanelDirectorDetail.Show()
    End Sub
    Protected Sub BtnCancelDirectorEmployerPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailDirectorPhone.Hide()
        PanelPhoneDirectorEmployer.Hide()
        FormPanelDirectorDetail.Show()
    End Sub
    Protected Sub BtnCancelDirectorDetailAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailDirectorAddress.Hide()
        PanelAddressDirector.Hide()
        FormPanelDirectorDetail.Show()
    End Sub
    Protected Sub BtnCancelDirectorDetailEmployerAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailDirectorAddress.Hide()
        PanelAddressDirectorEmployer.Hide()
        FormPanelDirectorDetail.Show()
    End Sub
    Protected Sub BtnCancelPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailPhone.Hide()
        PanelDetailPhone.Hide()
    End Sub
    Protected Sub BtnCancelPhoneEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailPhone.Hide()
        PanelDetailPhoneEmployer.Hide()
    End Sub
    Protected Sub BtnCancelAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailAddress.Hide()
        PanelDetailAddress.Hide()
    End Sub
    Protected Sub BtnCancelAddressEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailAddress.Hide()
        PanelDetailAddressEmployer.Hide()
    End Sub
    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim objWIC As New WICBLL
            objWIC.Accept(ObjApproval.PK_ModuleApproval_ID)

            LblConfirmation.Text = "Data Approved. Click Ok to Back To " & ObjModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnReject_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim objWIC As New WICBLL
            objWIC.Reject(ObjApproval.PK_ModuleApproval_ID)

            LblConfirmation.Text = "Data Rejected. Click Ok to Back To " & ObjModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim strURL As String
            strURL = String.Format(Common.GetApplicationPath & "/Parameter/WaitingApproval.aspx?ModuleID={0}", Request.Params("ModuleID"))

            If Not ObjApproval Is Nothing Then
                If ObjApproval.CreatedBy <> Common.SessionCurrentUser.UserID Then
                    strURL = String.Format(Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID"))
                End If
            End If

            Net.X.Redirect(strURL)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs) Handles BtnConfirmation.DirectClick
        Try
            Net.X.Redirect(String.Format(Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "goAML 5.0.1, Add By Septian, 2022-03-03"
    '    Private _is_load_detail_new As Boolean
    '    Public Property IsLoadDetailNew() As String
    '        Get
    '            Return _is_load_detail_new
    '        End Get
    '        Set(ByVal value As String)
    '            _is_load_detail_new = value
    '        End Set
    '    End Property
    Private Sub LoadData(objWICNew As GoAMLBLL.WICDataBLL, Optional objWICOld As GoAMLBLL.WICDataBLL = Nothing)
        LoadObject(objWICNew, objWICOld)
        BindForm(objWICNew, objWICOld)
        BindDetail()
    End Sub
    Private Sub BindForm(objWICNew As GoAMLBLL.WICDataBLL, Optional objWICOld As GoAMLBLL.WICDataBLL = Nothing)
        If objWICNew IsNot Nothing Then
            If objWICNew.ObjWIC2 IsNot Nothing Then
                DspIsRealWICNew.Text = If(objWICNew.ObjWIC2.IS_REAL_WIC, "True", "False")
                TxtINDV_Is_Protected_New.Text = If(objWICNew.ObjWIC2.IS_PROTECTED, "True", "False")
            End If

        End If
        If objWICOld IsNot Nothing Then
            If objWICOld.ObjWIC2 IsNot Nothing Then
                DspIsRealWICOld.Text = If(objWICOld.ObjWIC2.IS_REAL_WIC, "True", "False")
                TxtINDV_Is_Protected_Old.Text = If(objWICOld.ObjWIC2.IS_PROTECTED, "True", "False")
            End If
        End If
    End Sub
    Private Sub BindDetail()
        BindDetailIndvOld()
        BindDetailCorpOld()
        BindDetailIndvNew()
        BindDetailCorpNew()
    End Sub

    Private Sub BindDetailIndvOld()
        BindDetail_Email(GP_WIC_INDV_Email_Old.GetStore, objListgoAML_Ref_Email_Old)
        BindDetail_PEP(GP_WIC_INDV_PEP_Old.GetStore, objListgoAML_Ref_PersonPEP_Old)
        BindDetail_Sanction(GP_WIC_INDV_Sanction_Old.GetStore, objListgoAML_Ref_Sanction_Old)
        BindDetail_RelatedPerson(GP_WIC_INDV_RelatedPerson_Old.GetStore, objListgoAML_Ref_RelatedPerson_Old)
    End Sub

    Private Sub BindDetailCorpOld()
        BindDetail_Email(GP_WIC_CORP_Email_Old.GetStore, objListgoAML_Ref_Email_Old)
        BindDetail_Url(GP_WIC_CORP_URL_Old.GetStore, objListgoAML_Ref_EntityUrl_Old)
        BindDetail_EntityIdentification(GP_WIC_CORP_EntityIdentification_Old.GetStore, objListgoAML_Ref_EntityIdentification_Old)
        BindDetail_Sanction(GP_WIC_CORP_Sanction_Old.GetStore, objListgoAML_Ref_Sanction_Old)
        BindDetail_RelatedPerson(GP_WIC_CORP_RelatedPerson_Old.GetStore, objListgoAML_Ref_RelatedPerson_Old)
        BindDetail_RelatedEntity(GP_WIC_CORP_RelatedEntity_Old.GetStore, objListgoAML_Ref_RelatedEntity_Old)
    End Sub

    Private Sub BindDetailIndvNew()
        BindDetail_Email(GP_WIC_INDV_Email_New.GetStore, objListgoAML_Ref_Email_New)
        BindDetail_PEP(GP_WIC_INDV_PEP_New.GetStore, objListgoAML_Ref_PersonPEP_New)
        BindDetail_Sanction(GP_WIC_INDV_Sanction_New.GetStore, objListgoAML_Ref_Sanction_New)
        BindDetail_RelatedPerson(GP_WIC_INDV_RelatedPerson_New.GetStore, objListgoAML_Ref_RelatedPerson_New)
    End Sub

    Private Sub BindDetailCorpNew()
        BindDetail_Email(GP_WIC_CORP_Email_New.GetStore, objListgoAML_Ref_Email_New)
        BindDetail_Url(GP_WIC_CORP_URL_New.GetStore, objListgoAML_Ref_EntityUrl_New)
        BindDetail_EntityIdentification(GP_WIC_CORP_EntityIdentification_New.GetStore, objListgoAML_Ref_EntityIdentification_New)
        BindDetail_Sanction(GP_WIC_CORP_Sanction_New.GetStore, objListgoAML_Ref_Sanction_New)
        BindDetail_RelatedPerson(GP_WIC_CORP_RelatedPerson_New.GetStore, objListgoAML_Ref_RelatedPerson_New)
        BindDetail_RelatedEntity(GP_WIC_CORP_RelatedEntity_New.GetStore, objListgoAML_Ref_RelatedEntity_New)
    End Sub

    Private Sub BindDetail_Email(store As Store, list As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Email))
        If list IsNot Nothing Then
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            'objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("number", GetType(String)))
            'If objtable.Rows.Count > 0 Then
            '    For Each item As Data.DataRow In objtable.Rows
            '        item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
            '        item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
            '        item("number") = item("tph_number")
            '    Next
            'End If
            store.DataSource = objtable
            store.DataBind()
        End If

    End Sub

    Private Sub BindDetail_PEP(store As Store, list As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Person_PEP))
        If list IsNot Nothing Then
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            'objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("number", GetType(String)))
            'If objtable.Rows.Count > 0 Then
            '    For Each item As Data.DataRow In objtable.Rows
            '        item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
            '        item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
            '        item("number") = item("tph_number")
            '    Next
            'End If
            store.DataSource = objtable
            store.DataBind()
        End If
    End Sub

    Private Sub BindDetail_Sanction(store As Store, list As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Sanction))
        If list IsNot Nothing Then
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            'objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("number", GetType(String)))
            'If objtable.Rows.Count > 0 Then
            '    For Each item As Data.DataRow In objtable.Rows
            '        item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
            '        item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
            '        item("number") = item("tph_number")
            '    Next
            'End If
            store.DataSource = objtable
            store.DataBind()
        End If
    End Sub

    Private Sub BindDetail_RelatedPerson(store As Store, list As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Person))
        If list IsNot Nothing Then
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            'objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("number", GetType(String)))
            'If objtable.Rows.Count > 0 Then
            '    For Each item As Data.DataRow In objtable.Rows
            '        item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
            '        item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
            '        item("number") = item("tph_number")
            '    Next
            'End If
            store.DataSource = objtable
            store.DataBind()
        End If
    End Sub

    Private Sub BindDetail_Url(store As Store, list As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL))
        If list IsNot Nothing Then
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            'objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("number", GetType(String)))
            'If objtable.Rows.Count > 0 Then
            '    For Each item As Data.DataRow In objtable.Rows
            '        item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
            '        item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
            '        item("number") = item("tph_number")
            '    Next
            'End If
            store.DataSource = objtable
            store.DataBind()
        End If
    End Sub

    Private Sub BindDetail_EntityIdentification(store As Store, list As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification))
        If list IsNot Nothing Then
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            'objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("number", GetType(String)))
            'If objtable.Rows.Count > 0 Then
            '    For Each item As Data.DataRow In objtable.Rows
            '        item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
            '        item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
            '        item("number") = item("tph_number")
            '    Next
            'End If
            store.DataSource = objtable
            store.DataBind()
        End If
    End Sub

    Private Sub BindDetail_RelatedEntity(store As Store, list As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity))
        If list IsNot Nothing Then
            Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
            'objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
            'objtable.Columns.Add(New DataColumn("number", GetType(String)))
            'If objtable.Rows.Count > 0 Then
            '    For Each item As Data.DataRow In objtable.Rows
            '        item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
            '        item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
            '        item("number") = item("tph_number")
            '    Next
            'End If
            store.DataSource = objtable
            store.DataBind()
        End If
    End Sub

    Private Sub LoadObject(objWicNew As GoAMLBLL.WICDataBLL, Optional objWICOld As GoAMLBLL.WICDataBLL = Nothing)
        If objWicNew.ObjGrid IsNot Nothing Then
            objListgoAML_Ref_Email_New = objWicNew.ObjGrid.ObjList_GoAML_Email
            objListgoAML_Ref_PersonPEP_New = objWicNew.ObjGrid.ObjList_GoAML_PersonPEP
            objListgoAML_Ref_Sanction_New = objWicNew.ObjGrid.ObjList_GoAML_Sanction
            objListgoAML_Ref_RelatedPerson_New = objWicNew.ObjGrid.ObjList_GoAML_RelatedPerson
            objListgoAML_Ref_EntityUrl_New = objWicNew.ObjGrid.ObjList_GoAML_URL
            objListgoAML_Ref_EntityIdentification_New = objWicNew.ObjGrid.ObjList_GoAML_EntityIdentification
            objListgoAML_Ref_RelatedEntity_New = objWicNew.ObjGrid.ObjList_GoAML_RelatedEntity
        End If


        If objWICOld IsNot Nothing Then
            If objWICOld.ObjGrid IsNot Nothing Then
                objListgoAML_Ref_Email_Old = objWICOld.ObjGrid.ObjList_GoAML_Email
                objListgoAML_Ref_PersonPEP_Old = objWICOld.ObjGrid.ObjList_GoAML_PersonPEP
                objListgoAML_Ref_Sanction_Old = objWICOld.ObjGrid.ObjList_GoAML_Sanction
                objListgoAML_Ref_RelatedPerson_Old = objWICOld.ObjGrid.ObjList_GoAML_RelatedPerson
                objListgoAML_Ref_EntityUrl_Old = objWICOld.ObjGrid.ObjList_GoAML_URL
                objListgoAML_Ref_EntityIdentification_Old = objWICOld.ObjGrid.ObjList_GoAML_EntityIdentification
                objListgoAML_Ref_RelatedEntity_Old = objWICOld.ObjGrid.ObjList_GoAML_RelatedEntity
            End If

        End If
    End Sub

    Sub LoadDetail_Email(id As Long, Optional isDataNew As Boolean = True)
        Try
            If isDataNew Then
                objTempWIC_Email_Edit = objListgoAML_Ref_Email_New.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = id)
            Else
                objTempWIC_Email_Edit = objListgoAML_Ref_Email_Old.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = id)
            End If

            If Not objTempWIC_Email_Edit Is Nothing Then
                FormPanel_Email.Hidden = False
                WindowDetail_Email.Hidden = False

                txtWIC_email_address.ReadOnly = False

                With objTempWIC_Email_Edit
                    txtWIC_email_address.Value = .email_address
                End With

                WindowDetail_Email.Title = "Email Detail"

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDetail_PEP(id As Long, Optional isDataNew As Boolean = True)
        Try
            If isDataNew Then
                objTempWIC_PersonPEP_Edit = objListgoAML_Ref_PersonPEP_New.Find(Function(x) x.PK_goAML_Ref_WIC_PEP_ID = id)
            Else
                objTempWIC_PersonPEP_Edit = objListgoAML_Ref_PersonPEP_Old.Find(Function(x) x.PK_goAML_Ref_WIC_PEP_ID = id)
            End If

            If Not objTempWIC_PersonPEP_Edit Is Nothing Then
                FormPanel_Person_PEP.Hidden = False
                WindowDetail_Person_PEP.Hidden = False

                cmb_PersonPEP_Country.IsReadOnly = False
                txt_function_name.ReadOnly = False
                txt_function_description.ReadOnly = False
                df_person_pep_valid_from.ReadOnly = False
                cbx_pep_is_approx_from_date.ReadOnly = False
                df_pep_valid_to.ReadOnly = False
                cbx_pep_is_approx_to_date.ReadOnly = False
                txt_pep_comments.ReadOnly = False
                With objTempWIC_PersonPEP_Edit
                    Dim reference = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .pep_country)

                    If reference IsNot Nothing Then
                        cmb_PersonPEP_Country.SetTextWithTextValue(reference.Kode, reference.Keterangan)
                    End If
                    txt_function_name.Value = .function_name
                    txt_function_description.Value = .function_description
                    df_person_pep_valid_from.Text = .pep_date_range_valid_from
                    cbx_pep_is_approx_from_date.Checked = .pep_date_range_is_approx_from_date
                    df_pep_valid_to.Text = .pep_date_range_valid_to
                    cbx_pep_is_approx_to_date.Checked = .pep_date_range_is_approx_to_date
                    txt_pep_comments.Text = .comments
                End With

                WindowDetail_Person_PEP.Title = "Person PEP Detail"

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDetail_Sanction(id As Long, Optional isDataNew As Boolean = True)
        Try
            If isDataNew Then
                objTempWIC_Sanction_Edit = objListgoAML_Ref_Sanction_New.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = id)
            Else
                objTempWIC_Sanction_Edit = objListgoAML_Ref_Sanction_Old.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = id)
            End If


            If Not objTempWIC_Sanction_Edit Is Nothing Then
                FormPanel_Sanction.Hidden = False
                WindowDetail_Sanction.Hidden = False

                txt_sanction_provider.ReadOnly = True
                txt_sanction_sanction_list_name.ReadOnly = True
                txt_sanction_match_criteria.ReadOnly = True
                txt_sanction_link_to_source.ReadOnly = True
                txt_sanction_sanction_list_attributes.ReadOnly = True
                df_sanction_valid_from.ReadOnly = True
                cbx_sanction_is_approx_from_date.ReadOnly = True
                df_sanction_valid_to.ReadOnly = True
                cbx_sanction_is_approx_to_date.ReadOnly = True
                txt_sanction_comments.ReadOnly = True


                With objTempWIC_Sanction_Edit
                    txt_sanction_provider.Value = .Provider
                    txt_sanction_sanction_list_name.Value = .Sanction_List_Name
                    txt_sanction_match_criteria.Value = .Match_Criteria
                    txt_sanction_link_to_source.Value = .Link_To_Source
                    txt_sanction_sanction_list_attributes.Value = .Sanction_List_Attributes
                    df_sanction_valid_from.Text = .Sanction_List_Date_Range_Valid_From
                    cbx_sanction_is_approx_from_date.Checked = .Sanction_List_Date_Range_Is_Approx_From_Date
                    df_sanction_valid_to.Text = .Sanction_List_Date_Range_Valid_To
                    cbx_sanction_is_approx_to_date.Checked = .Sanction_List_Date_Range_Is_Approx_To_Date
                    txt_sanction_comments.Value = .Comments
                End With
                WindowDetail_Sanction.Title = "Sanction Detail"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDetail_RelatedPerson(id As Long, Optional isDataNew As Boolean = True)
        Try
            If isDataNew Then
                objTempWIC_RelatedPerson_Edit = objListgoAML_Ref_RelatedPerson_New.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = id)
            Else
                objTempWIC_RelatedPerson_Edit = objListgoAML_Ref_RelatedPerson_Old.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = id)
            End If

            If Not objTempWIC_RelatedPerson_Edit Is Nothing Then
                FormPanel_RelatedPerson.Hidden = False
                WindowDetail_RelatedPerson.Hidden = False

                cmb_rp_person_relation.IsReadOnly = True
                'df_relation_date_range_valid_from.ReadOnly = True
                'cbx_relation_date_range_is_approx_from_date.ReadOnly = True
                'df_relation_date_range_valid_to.ReadOnly = True
                'cbx_relation_date_range_is_approx_to_date.ReadOnly = True
                txt_rp_comments.ReadOnly = True
                txt_rp_relation_comments.ReadOnly = True
                cmb_rp_gender.IsReadOnly = True
                txt_rp_title.ReadOnly = True
                txt_rp_first_name.ReadOnly = True
                txt_rp_middle_name.ReadOnly = True
                txt_rp_prefix.ReadOnly = True
                txt_rp_last_name.ReadOnly = True
                df_birthdate.ReadOnly = True
                txt_rp_birth_place.ReadOnly = True
                cmb_rp_country_of_birth.IsReadOnly = True
                txt_rp_mothers_name.ReadOnly = True
                txt_rp_alias.ReadOnly = True
                'txt_rp_full_name_frn.ReadOnly = True
                txt_rp_ssn.ReadOnly = True
                txt_rp_passport_number.ReadOnly = True
                cmb_rp_passport_country.IsReadOnly = True
                txt_rp_id_number.ReadOnly = True
                cmb_rp_nationality1.IsReadOnly = True
                cmb_rp_nationality2.IsReadOnly = True
                cmb_rp_nationality3.IsReadOnly = True
                cmb_rp_residence.IsReadOnly = True
                'df_rp_residence_since.ReadOnly = True
                txt_rp_occupation.ReadOnly = True
                txt_rp_employer_name.ReadOnly = True
                cbx_rp_deceased.ReadOnly = True
                df_rp_date_deceased.ReadOnly = True
                txt_rp_tax_number.ReadOnly = True
                cbx_rp_tax_reg_number.ReadOnly = True
                txt_rp_source_of_wealth.ReadOnly = True
                cbx_rp_is_protected.ReadOnly = True


                With objTempWIC_RelatedPerson_Edit
                    Dim refPersonRelation = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Person_Person_Relationship", .Person_Person_Relation)
                    Dim refGender = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Jenis_Kelamin", .Gender)
                    Dim refCountryBirth = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .Country_Of_Birth)
                    Dim refPassportCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .Passport_Country)
                    Dim refNationality1 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .Nationality1)
                    Dim refNationality2 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .Nationality2)
                    Dim refNationality3 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .Nationality3)
                    Dim refResidence = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .Residence)
                    If refPersonRelation IsNot Nothing Then
                        cmb_rp_person_relation.SetTextWithTextValue(refPersonRelation.Kode, refPersonRelation.Keterangan)
                    End If

                    'df_relation_date_range_valid_from.Text = .RELATION_DATE_RANGE_VALID_FROM
                    'cbx_relation_date_range_is_approx_from_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_relation_date_range_valid_to.Text = .RELATION_DATE_RANGE_VALID_TO
                    'cbx_relation_date_range_is_approx_to_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_rp_comments.Value = .Comments
                    txt_rp_relation_comments.Value = .relation_comments
                    If refGender IsNot Nothing Then
                        cmb_rp_gender.SetTextWithTextValue(refGender.Kode, refGender.Keterangan)
                    End If

                    txt_rp_title.Value = .Title
                    txt_rp_first_name.Value = .First_Name
                    txt_rp_middle_name.Value = .Middle_Name
                    txt_rp_prefix.Value = .Prefix
                    txt_rp_last_name.Value = .Last_Name
                    df_birthdate.Text = .Birth_Date
                    txt_rp_birth_place.Value = .Birth_Place
                    If refCountryBirth IsNot Nothing Then
                        cmb_rp_country_of_birth.SetTextWithTextValue(refCountryBirth.Kode, refCountryBirth.Keterangan)
                    End If

                    txt_rp_mothers_name.Value = .Mother_Name
                    txt_rp_alias.Value = ._Alias
                    'txt_rp_full_name_frn.Value = .FULL_NAME_FRN
                    txt_rp_ssn.Value = .SSN
                    txt_rp_passport_number.Value = .Passport_Number
                    If refPassportCountry IsNot Nothing Then
                        cmb_rp_passport_country.SetTextWithTextValue(refPassportCountry.Kode, refPassportCountry.Keterangan)
                    End If

                    txt_rp_id_number.Value = .ID_Number
                    If refNationality1 IsNot Nothing Then
                        cmb_rp_nationality1.SetTextWithTextValue(refNationality1.Kode, refNationality2.Keterangan)
                    End If

                    If refNationality2 IsNot Nothing Then
                        cmb_rp_nationality2.SetTextWithTextValue(refNationality2.Kode, refNationality2.Keterangan)
                    End If

                    If refNationality3 IsNot Nothing Then
                        cmb_rp_nationality3.SetTextWithTextValue(refNationality3.Kode, refNationality3.Keterangan)
                    End If

                    If refResidence IsNot Nothing Then
                        cmb_rp_residence.SetTextWithTextValue(refResidence.Kode, refResidence.Keterangan)
                    End If

                    'df_rp_residence_since.Text = .RESIDENCE_SINCE
                    txt_rp_occupation.Value = .Occupation
                    txt_rp_employer_name.Value = .EMPLOYER_NAME
                    cbx_rp_deceased.Checked = .Deceased
                    If .Deceased Then
                        df_rp_date_deceased.Hidden = False
                    Else
                        df_rp_date_deceased.Hidden = True
                    End If
                    df_rp_date_deceased.Text = .Date_Deceased
                    txt_rp_tax_number.Value = .Tax_Number
                    cbx_rp_tax_reg_number.Checked = .Is_PEP
                    txt_rp_source_of_wealth.Text = .Source_Of_Wealth
                    cbx_rp_is_protected.Checked = .Is_Protected

                End With

                WindowDetail_RelatedPerson.Title = "Related Person Detail"

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDetail_Url(id As Long, Optional isDataNew As Boolean = True)
        Try
            If isDataNew Then
                objTempWIC_EntityUrl_Edit = objListgoAML_Ref_EntityUrl_New.Find(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id)
            Else
                objTempWIC_EntityUrl_Edit = objListgoAML_Ref_EntityUrl_Old.Find(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id)
            End If
            If Not objTempWIC_EntityUrl_Edit Is Nothing Then
                FormPanel_URL.Hidden = False
                WindowDetail_URL.Hidden = False

                txt_url.ReadOnly = True

                With objTempWIC_EntityUrl_Edit
                    txt_url.Value = .URL
                End With

                WindowDetail_URL.Title = "Entity URL Detail"

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDetail_EntityIdentification(id As Long, Optional isDataNew As Boolean = True)
        Try
            If isDataNew Then
                objTempWIC_EntityIdentification_Edit = objListgoAML_Ref_EntityIdentification_New.Find(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id)
            Else
                objTempWIC_EntityIdentification_Edit = objListgoAML_Ref_EntityIdentification_Old.Find(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id)
            End If

            If Not objTempWIC_EntityIdentification_Edit Is Nothing Then
                FormPanel_EntityIdentification.Hidden = False
                WindowDetail_EntityIdentification.Hidden = False

                cmb_ef_type.IsReadOnly = True
                txt_ef_number.ReadOnly = True
                df_issue_date.ReadOnly = True
                df_expiry_date.ReadOnly = True
                txt_ef_issued_by.ReadOnly = True
                cmb_ef_issue_country.IsReadOnly = True
                txt_ef_comments.ReadOnly = True


                With objTempWIC_EntityIdentification_Edit
                    Dim refType = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Identifier_Type", .TYPE)
                    Dim refIssue = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .ISSUE_COUNTRY)

                    If refType IsNot Nothing Then
                        cmb_ef_type.SetTextWithTextValue(refType.Kode, refType.Keterangan)
                    End If

                    txt_ef_number.Value = .NUMBER
                    df_issue_date.Text = .ISSUE_DATE
                    df_expiry_date.Text = .EXPIRY_DATE
                    txt_ef_issued_by.Value = .ISSUED_BY
                    If refIssue IsNot Nothing Then
                        cmb_ef_issue_country.SetTextWithTextValue(refIssue.Kode, refIssue.Keterangan)
                    End If

                    txt_ef_comments.Value = .COMMENTS

                End With

                WindowDetail_EntityIdentification.Title = "Entity Identification Detail"

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDetail_RelatedEntity(id As Long, Optional isDataNew As Boolean = True)
        Try
            If isDataNew Then
                objTempWIC_RelatedEntity_Edit = objListgoAML_Ref_RelatedEntity_New.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id)
            Else
                objTempWIC_RelatedEntity_Edit = objListgoAML_Ref_RelatedEntity_Old.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id)
            End If

            If Not objTempWIC_RelatedEntity_Edit Is Nothing Then
                FormPanel_RelatedEntities.Hidden = False
                WindowDetail_RelatedEntity.Hidden = False

                cmb_re_entity_relation.IsReadOnly = True
                txt_re_comments.ReadOnly = True
                txt_re_relation_comments.ReadOnly = True
                txt_re_name.ReadOnly = True
                txt_re_commercial_name.ReadOnly = True
                cmb_re_incorporation_legal_form.IsReadOnly = True
                txt_re_incorporation_number.ReadOnly = True
                txt_re_business.ReadOnly = True
                txt_re_incorporation_state.ReadOnly = True
                cmb_re_incorporation_country_code.IsReadOnly = True
                df_re_incorporation_date.ReadOnly = True
                cbx_re_business_closed.ReadOnly = True
                df_re_date_business_closed.ReadOnly = True
                txt_re_tax_number.ReadOnly = True
                txt_re_tax_reg_number.ReadOnly = True


                With objTempWIC_RelatedEntity_Edit
                    Dim refRelation = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Person_Person_Relationship", .ENTITY_ENTITY_RELATION)
                    Dim refLegalForm = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Bentuk_Badan_Usaha", .INCORPORATION_LEGAL_FORM)
                    Dim refEntityStatus = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Status", .ENTITY_STATUS)
                    Dim refCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .INCORPORATION_COUNTRY_CODE)

                    If refRelation IsNot Nothing Then
                        cmb_re_entity_relation.SetTextWithTextValue(refRelation.Kode, refRelation.Keterangan)
                    End If

                    txt_re_comments.Value = .COMMENTS
                    txt_re_relation_comments.Value = .RELATION_COMMENTS
                    txt_re_name.Value = .NAME
                    txt_re_commercial_name.Value = .COMMERCIAL_NAME

                    If refLegalForm IsNot Nothing Then
                        cmb_re_incorporation_legal_form.SetTextWithTextValue(refLegalForm.Kode, refLegalForm.Keterangan)
                    End If

                    txt_re_incorporation_number.Value = .INCORPORATION_NUMBER
                    txt_re_business.Value = .BUSINESS
                    txt_re_incorporation_state.Value = .INCORPORATION_STATE
                    If refCountry IsNot Nothing Then
                        cmb_re_incorporation_country_code.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                    End If

                    df_re_incorporation_date.Text = .INCORPORATION_DATE
                    cbx_re_business_closed.Checked = .BUSINESS_CLOSED
                    If .BUSINESS_CLOSED Then
                        df_re_date_business_closed.Hidden = False
                        If .DATE_BUSINESS_CLOSED IsNot Nothing Then
                            df_re_date_business_closed.Text = .DATE_BUSINESS_CLOSED
                        End If
                    End If

                    txt_re_tax_number.Value = .TAX_NUMBER
                    txt_re_tax_reg_number.Value = .TAX_REG_NUMBER

                End With

                WindowDetail_RelatedEntity.Title = "Related Entity Detail"

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub GrdCmd_WIC_Email(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDetail_Email(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmd_WIC_PEP(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDetail_PEP(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmd_WIC_Sanction(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDetail_Sanction(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmd_WIC_RelatedPerson(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDetail_RelatedPerson(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmd_WIC_Url(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDetail_Url(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmd_WIC_EntityIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDetail_EntityIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmd_WIC_RelatedEntity(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDetail_RelatedEntity(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackWIC_Email_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Email.Hidden = True
            FormPanel_Email.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackWIC_PersonPEP_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Person_PEP.Hidden = True
            FormPanel_Person_PEP.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackWIC_Sanction_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_Sanction.Hidden = True
            FormPanel_Sanction.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackWIC_RelatedPerson_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_RelatedPerson.Hidden = True
            FormPanel_RelatedPerson.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackWIC_Url_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_URL.Hidden = True
            FormPanel_URL.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackWIC_EntityIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_EntityIdentification.Hidden = True
            FormPanel_EntityIdentification.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnBackWIC_RelatedEntities_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            WindowDetail_RelatedEntity.Hidden = True
            FormPanel_RelatedEntities.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_Email(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                If sender.ID.ToString().Contains("New") Then
                    LoadData_Email(id, True)
                Else
                    LoadData_Email(id, False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_PEP(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                If sender.ID.ToString().Contains("New") Then
                    LoadData_PEP(id, True)
                Else
                    LoadData_PEP(id, False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_Sanction(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                If sender.ID.ToString().Contains("New") Then
                    LoadData_Sanction(id, True)
                Else
                    LoadData_Sanction(id, False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_RelatedPerson(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                If sender.ID.ToString().Contains("New") Then
                    LoadData_RelatedPerson(id, True)
                Else
                    LoadData_RelatedPerson(id, False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_EntityIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                If sender.ID.ToString().Contains("New") Then
                    LoadData_EntityIdentification(id, True)
                Else
                    LoadData_EntityIdentification(id, False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_URL(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                If sender.ID.ToString().Contains("New") Then
                    LoadData_URL(id, True)
                Else
                    LoadData_URL(id, False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdWIC_RelatedEntities(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                If sender.ID.ToString().Contains("New") Then
                    LoadData_RelatedEntities(id, True)
                Else
                    LoadData_RelatedEntities(id, False)
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadData_Email(id As Long, isNew As Boolean)
        Try
            Dim isEdit = False
            If isNew Then
                objTempWIC_Email_Edit = objListgoAML_Ref_Email_New.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = id)
            Else
                objTempWIC_Email_Edit = objListgoAML_Ref_Email_Old.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = id)
            End If


            If Not objTempWIC_Email_Edit Is Nothing Then
                FormPanel_Email.Hidden = False
                WindowDetail_Email.Hidden = False

                txtWIC_email_address.ReadOnly = Not isEdit

                With objTempWIC_Email_Edit
                    txtWIC_email_address.Value = .email_address
                End With

                WindowDetail_Email.Title = "Email " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadData_PEP(id As Long, isNew As Boolean)
        Try
            Dim isEdit = False
            If isNew Then
                objTempWIC_PersonPEP_Edit = objListgoAML_Ref_PersonPEP_New.Find(Function(x) x.PK_goAML_Ref_WIC_PEP_ID = id)
            Else
                objTempWIC_PersonPEP_Edit = objListgoAML_Ref_PersonPEP_Old.Find(Function(x) x.PK_goAML_Ref_WIC_PEP_ID = id)
            End If

            If Not objTempWIC_PersonPEP_Edit Is Nothing Then
                FormPanel_Person_PEP.Hidden = False
                WindowDetail_Person_PEP.Hidden = False

                cmb_PersonPEP_Country.IsReadOnly = Not isEdit
                txt_function_name.ReadOnly = Not isEdit
                txt_function_description.ReadOnly = Not isEdit
                df_person_pep_valid_from.ReadOnly = Not isEdit
                cbx_pep_is_approx_from_date.ReadOnly = Not isEdit
                df_pep_valid_to.ReadOnly = Not isEdit
                cbx_pep_is_approx_to_date.ReadOnly = Not isEdit
                txt_pep_comments.ReadOnly = Not isEdit


                With objTempWIC_PersonPEP_Edit
                    Dim reference = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .pep_country)

                    If reference IsNot Nothing Then
                        cmb_PersonPEP_Country.SetTextWithTextValue(reference.Kode, reference.Keterangan)
                    End If

                    txt_function_name.Value = .function_name
                    txt_function_description.Value = .function_description
                    'df_person_pep_valid_from.Text = .pep_date_range_valid_from
                    'cbx_pep_is_approx_from_date.Checked = .pep_date_range_is_approx_from_date
                    'df_pep_valid_to.Text = .pep_date_range_valid_to
                    'cbx_pep_is_approx_to_date.Checked = .pep_date_range_is_approx_to_date
                    txt_pep_comments.Text = .comments
                End With

                WindowDetail_Person_PEP.Title = "Person PEP " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadData_Sanction(id As Long, isNew As Boolean)
        Try
            Dim isEdit = False
            If isNew Then
                objTempWIC_Sanction_Edit = objListgoAML_Ref_Sanction_New.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = id)
            Else
                objTempWIC_Sanction_Edit = objListgoAML_Ref_Sanction_Old.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = id)
            End If

            If Not objTempWIC_Sanction_Edit Is Nothing Then
                FormPanel_Sanction.Hidden = False
                WindowDetail_Sanction.Hidden = False

                txt_sanction_provider.ReadOnly = Not isEdit
                txt_sanction_sanction_list_name.ReadOnly = Not isEdit
                txt_sanction_match_criteria.ReadOnly = Not isEdit
                txt_sanction_link_to_source.ReadOnly = Not isEdit
                txt_sanction_sanction_list_attributes.ReadOnly = Not isEdit
                df_sanction_valid_from.ReadOnly = Not isEdit
                cbx_sanction_is_approx_from_date.ReadOnly = Not isEdit
                df_sanction_valid_to.ReadOnly = Not isEdit
                cbx_sanction_is_approx_to_date.ReadOnly = Not isEdit
                txt_sanction_comments.ReadOnly = Not isEdit


                With objTempWIC_Sanction_Edit
                    txt_sanction_provider.Value = .Provider
                    txt_sanction_sanction_list_name.Value = .Sanction_List_Name
                    txt_sanction_match_criteria.Value = .Match_Criteria
                    txt_sanction_link_to_source.Value = .Link_To_Source
                    txt_sanction_sanction_list_attributes.Value = .Sanction_List_Attributes
                    df_sanction_valid_from.Text = .Sanction_List_Date_Range_Valid_From
                    cbx_sanction_is_approx_from_date.Checked = .Sanction_List_Date_Range_Is_Approx_From_Date
                    df_sanction_valid_to.Text = .Sanction_List_Date_Range_Valid_To
                    cbx_sanction_is_approx_to_date.Checked = .Sanction_List_Date_Range_Is_Approx_To_Date
                    txt_sanction_comments.Value = .Comments
                End With

                WindowDetail_Sanction.Title = "Sanction " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadData_RelatedPerson(id As Long, isNew As Boolean)
        Try
            Dim isEdit = False
            If isNew Then
                objTempWIC_RelatedPerson_Edit = objListgoAML_Ref_RelatedPerson_New.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = id)
            Else
                objTempWIC_RelatedPerson_Edit = objListgoAML_Ref_RelatedPerson_Old.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = id)
            End If

            If Not objTempWIC_RelatedPerson_Edit Is Nothing Then
                FormPanel_RelatedPerson.Hidden = False
                WindowDetail_RelatedPerson.Hidden = False

                cmb_rp_person_relation.IsReadOnly = Not isEdit
                'df_relation_date_range_valid_from.ReadOnly = Not isEdit
                'cbx_relation_date_range_is_approx_from_date.ReadOnly = Not isEdit
                'df_relation_date_range_valid_to.ReadOnly = Not isEdit
                'cbx_relation_date_range_is_approx_to_date.ReadOnly = Not isEdit
                txt_rp_comments.ReadOnly = Not isEdit
                txt_rp_relation_comments.ReadOnly = Not isEdit
                cmb_rp_gender.IsReadOnly = Not isEdit
                txt_rp_title.ReadOnly = Not isEdit
                txt_rp_first_name.ReadOnly = Not isEdit
                txt_rp_middle_name.ReadOnly = Not isEdit
                txt_rp_prefix.ReadOnly = Not isEdit
                txt_rp_last_name.ReadOnly = Not isEdit
                df_birthdate.ReadOnly = Not isEdit
                txt_rp_birth_place.ReadOnly = Not isEdit
                cmb_rp_country_of_birth.IsReadOnly = Not isEdit
                txt_rp_mothers_name.ReadOnly = Not isEdit
                txt_rp_alias.ReadOnly = Not isEdit
                'txt_rp_full_name_frn.ReadOnly = Not isEdit
                txt_rp_ssn.ReadOnly = Not isEdit
                txt_rp_passport_number.ReadOnly = Not isEdit
                cmb_rp_passport_country.IsReadOnly = Not isEdit
                txt_rp_id_number.ReadOnly = Not isEdit
                cmb_rp_nationality1.IsReadOnly = Not isEdit
                cmb_rp_nationality2.IsReadOnly = Not isEdit
                cmb_rp_nationality3.IsReadOnly = Not isEdit
                cmb_rp_residence.IsReadOnly = Not isEdit
                'df_rp_residence_since.ReadOnly = Not isEdit
                txt_rp_occupation.ReadOnly = Not isEdit
                cbx_rp_deceased.ReadOnly = Not isEdit
                df_rp_date_deceased.ReadOnly = Not isEdit
                txt_rp_tax_number.ReadOnly = Not isEdit
                cbx_rp_tax_reg_number.ReadOnly = Not isEdit
                txt_rp_source_of_wealth.ReadOnly = Not isEdit
                cbx_rp_is_protected.ReadOnly = Not isEdit


                With objTempWIC_RelatedPerson_Edit
                    Dim refPersonRelation = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Person_Person_Relationship", .Person_Person_Relation)
                    Dim refGender = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Jenis_Kelamin", .Gender)
                    Dim refCountryBirth = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .Country_Of_Birth)
                    Dim refPassportCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .Passport_Country)
                    Dim refNationality1 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .Nationality1)
                    Dim refNationality2 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .Nationality2)
                    Dim refNationality3 = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .Nationality3)
                    Dim refResidence = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .Residence)

                    If refPersonRelation IsNot Nothing Then
                        cmb_rp_person_relation.SetTextWithTextValue(refPersonRelation.Kode, refPersonRelation.Keterangan)
                    End If

                    'df_relation_date_range_valid_from.Text = .RELATION_DATE_RANGE_VALID_FROM
                    'cbx_relation_date_range_is_approx_from_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_relation_date_range_valid_to.Text = .RELATION_DATE_RANGE_VALID_TO
                    'cbx_relation_date_range_is_approx_to_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    txt_rp_comments.Value = .Comments
                    txt_rp_relation_comments.Value = .relation_comments
                    If refGender IsNot Nothing Then
                        cmb_rp_gender.SetTextWithTextValue(refGender.Kode, refGender.Keterangan)
                    End If

                    txt_rp_title.Value = .Title
                    txt_rp_first_name.Value = .First_Name
                    txt_rp_middle_name.Value = .Middle_Name
                    txt_rp_prefix.Value = .Prefix
                    txt_rp_last_name.Value = .Last_Name
                    If .Birth_Date IsNot Nothing Then
                        df_birthdate.Text = .Birth_Date
                    End If

                    txt_rp_birth_place.Value = .Birth_Place
                    If refCountryBirth IsNot Nothing Then
                        cmb_rp_country_of_birth.SetTextWithTextValue(refCountryBirth.Kode, refCountryBirth.Keterangan)
                    End If

                    txt_rp_mothers_name.Value = .Mother_Name
                    txt_rp_alias.Value = ._Alias
                    'txt_rp_full_name_frn.Value = .FULL_NAME_FRN
                    txt_rp_ssn.Value = .SSN
                    txt_rp_passport_number.Value = .Passport_Number
                    If refPassportCountry IsNot Nothing Then
                        cmb_rp_passport_country.SetTextWithTextValue(refPassportCountry.Kode, refPassportCountry.Keterangan)
                    End If

                    txt_rp_id_number.Value = .ID_Number
                    If refNationality1 IsNot Nothing Then
                        cmb_rp_nationality1.SetTextWithTextValue(refNationality1.Kode, refNationality1.Keterangan)
                    End If
                    If refNationality2 IsNot Nothing Then
                        cmb_rp_nationality2.SetTextWithTextValue(refNationality2.Kode, refNationality2.Keterangan)
                    End If

                    If refNationality3 IsNot Nothing Then
                        cmb_rp_nationality3.SetTextWithTextValue(refNationality3.Kode, refNationality3.Keterangan)
                    End If

                    If refResidence IsNot Nothing Then
                        cmb_rp_residence.SetTextWithTextValue(refResidence.Kode, refResidence.Keterangan)
                    End If

                    'df_rp_residence_since.Text = .RESIDENCE_SINCE
                    txt_rp_occupation.Value = .Occupation
                    cbx_rp_deceased.Checked = .Deceased
                    df_rp_date_deceased.Value = IIf(.Date_Deceased Is Nothing, "", .Date_Deceased)
                    txt_rp_tax_number.Value = .Tax_Number

                    If .Is_PEP IsNot Nothing Then
                        cbx_rp_tax_reg_number.Checked = .Is_PEP
                    End If

                    txt_rp_source_of_wealth.Text = .Source_Of_Wealth
                    cbx_rp_is_protected.Checked = .Is_Protected

                    GridAddress_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Address)
                    GridAddressWork_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Address_Work)
                    GridPhone_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Phone)
                    GridPhoneWork_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Phone_Work)
                    GridIdentification_RelatedPerson.LoadData(.ObjList_GoAML_Person_Identification)

                    GridEmail_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Customer_Email)
                    GridSanction_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Customer_Sanction)
                    GridPEP_RelatedPerson.LoadData(.ObjList_GoAML_Ref_Customer_PEP)
                End With

                WindowDetail_RelatedPerson.Title = "Related Person " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadData_EntityIdentification(id As Long, isNew As Boolean)
        Try
            Dim isEdit = False
            If isNew Then
                objTempWIC_EntityIdentification_Edit = objListgoAML_Ref_EntityIdentification_New.Find(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id)
            Else
                objTempWIC_EntityIdentification_Edit = objListgoAML_Ref_EntityIdentification_Old.Find(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = id)
            End If

            If Not objTempWIC_EntityIdentification_Edit Is Nothing Then
                FormPanel_EntityIdentification.Hidden = False
                WindowDetail_EntityIdentification.Hidden = False

                cmb_ef_type.IsReadOnly = Not isEdit
                txt_ef_number.ReadOnly = Not isEdit
                df_issue_date.ReadOnly = Not isEdit
                df_expiry_date.ReadOnly = Not isEdit
                txt_ef_issued_by.ReadOnly = Not isEdit
                cmb_ef_issue_country.IsReadOnly = Not isEdit
                txt_ef_comments.ReadOnly = Not isEdit


                With objTempWIC_EntityIdentification_Edit
                    Dim refType = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Identifier_Type", .TYPE)
                    Dim refIssue = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .ISSUE_COUNTRY)

                    If refType IsNot Nothing Then
                        cmb_ef_type.SetTextWithTextValue(refType.Kode, refType.Keterangan)
                    End If

                    txt_ef_number.Value = .NUMBER
                    df_issue_date.Text = IIf(.ISSUE_DATE Is Nothing, "", .ISSUE_DATE)
                    df_expiry_date.Text = IIf(.EXPIRY_DATE Is Nothing, "", .EXPIRY_DATE)
                    txt_ef_issued_by.Value = .ISSUED_BY

                    If refIssue IsNot Nothing Then
                        cmb_ef_issue_country.SetTextWithTextValue(refIssue.Kode, refIssue.Keterangan)
                    End If

                    txt_ef_comments.Value = .COMMENTS

                End With

                WindowDetail_EntityIdentification.Title = "Entity Identification " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadData_URL(id As Long, isNew As Boolean)
        Try
            Dim isEdit = False
            If isNew Then
                objTempWIC_EntityUrl_Edit = objListgoAML_Ref_EntityUrl_New.Find(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id)
            Else
                objTempWIC_EntityUrl_Edit = objListgoAML_Ref_EntityUrl_Old.Find(Function(x) x.PK_goAML_Ref_WIC_URL_ID = id)
            End If

            If Not objTempWIC_EntityUrl_Edit Is Nothing Then
                FormPanel_URL.Hidden = False
                WindowDetail_URL.Hidden = False

                txt_url.ReadOnly = Not isEdit

                With objTempWIC_EntityUrl_Edit
                    txt_url.Value = .URL
                End With

                WindowDetail_URL.Title = "Entity URL " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadData_RelatedEntities(id As Long, isNew As Boolean)
        Try
            Dim isEdit = False
            If isNew Then
                objTempWIC_RelatedEntity_Edit = objListgoAML_Ref_RelatedEntity_New.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id)
            Else
                objTempWIC_RelatedEntity_Edit = objListgoAML_Ref_RelatedEntity_Old.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = id)
            End If

            If Not objTempWIC_RelatedEntity_Edit Is Nothing Then
                GridPhone_RelatedEntity.IsViewMode = Not isEdit
                GridAddress_RelatedEntity.IsViewMode = Not isEdit
                GridEmail_RelatedEntity.IsViewMode = Not isEdit
                GridEntityIdentification_RelatedEntity.IsViewMode = Not isEdit
                GridURL_RelatedEntity.IsViewMode = Not isEdit
                GridSanction_RelatedEntity.IsViewMode = Not isEdit

                FormPanel_RelatedEntities.Hidden = False
                WindowDetail_RelatedEntity.Hidden = False

                cmb_re_entity_relation.IsReadOnly = Not isEdit
                'df_re_relation_date_range_valid_from.ReadOnly = Not isEdit
                'cbx_re_relation_date_range_is_approx_from_date.ReadOnly = Not isEdit
                'df_re_relation_date_range_valid_to.ReadOnly = Not isEdit
                'cbx_re_relation_date_range_is_approx_to_date.ReadOnly = Not isEdit
                'nfd_re_share_percentage.ReadOnly = Not isEdit
                txt_re_comments.ReadOnly = Not isEdit
                txt_re_relation_comments.ReadOnly = Not isEdit
                txt_re_name.ReadOnly = Not isEdit
                txt_re_commercial_name.ReadOnly = Not isEdit
                cmb_re_incorporation_legal_form.IsReadOnly = Not isEdit
                txt_re_incorporation_number.ReadOnly = Not isEdit
                txt_re_business.ReadOnly = Not isEdit
                'cmb_re_entity_status.IsReadOnly = Not isEdit
                'df_re_entity_status_date.ReadOnly = Not isEdit
                txt_re_incorporation_state.ReadOnly = Not isEdit
                cmb_re_incorporation_country_code.IsReadOnly = Not isEdit
                df_re_incorporation_date.ReadOnly = Not isEdit
                cbx_re_business_closed.ReadOnly = Not isEdit
                df_re_date_business_closed.ReadOnly = Not isEdit
                txt_re_tax_number.ReadOnly = Not isEdit
                txt_re_tax_reg_number.ReadOnly = Not isEdit


                With objTempWIC_RelatedEntity_Edit
                    Dim refRelation = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Entity_Relationship", .ENTITY_ENTITY_RELATION)
                    Dim refLegalForm = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Bentuk_Badan_Usaha", .INCORPORATION_LEGAL_FORM)
                    Dim refEntityStatus = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Entity_Status", .ENTITY_STATUS)
                    Dim refCountry = goAML_CustomerBLL.GetReferenceByCode("goAML_Ref_Nama_Negara", .INCORPORATION_COUNTRY_CODE)

                    If refRelation IsNot Nothing Then
                        cmb_re_entity_relation.SetTextWithTextValue(refRelation.Kode, refRelation.Keterangan)
                    End If

                    'df_re_relation_date_range_valid_from.Text = .RELATION_DATE_RANGE_VALID_FROM
                    'cbx_re_relation_date_range_is_approx_from_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'df_re_relation_date_range_valid_to.Text = .RELATION_DATE_RANGE_VALID_TO
                    'cbx_re_relation_date_range_is_approx_to_date.Checked = .RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    'nfd_re_share_percentage.Value = .SHARE_PERCENTAGE
                    txt_re_comments.Value = .COMMENTS
                    txt_re_relation_comments.Value = .RELATION_COMMENTS
                    txt_re_name.Value = .NAME
                    txt_re_commercial_name.Value = .COMMERCIAL_NAME
                    If refLegalForm IsNot Nothing Then
                        cmb_re_incorporation_legal_form.SetTextWithTextValue(refLegalForm.Kode, refLegalForm.Keterangan)
                    End If

                    txt_re_incorporation_number.Value = .INCORPORATION_NUMBER
                    txt_re_business.Value = .BUSINESS
                    'cmb_re_entity_status.SetTextWithTextValue(refEntityStatus.Kode, refEntityStatus.Keterangan)
                    'df_re_entity_status_date.Text = .ENTITY_STATUS
                    txt_re_incorporation_state.Value = .INCORPORATION_STATE
                    If refCountry IsNot Nothing Then
                        cmb_re_incorporation_country_code.SetTextWithTextValue(refCountry.Kode, refCountry.Keterangan)
                    End If

                    df_re_incorporation_date.Value = IIf(.INCORPORATION_DATE Is Nothing, "", .INCORPORATION_DATE)
                    cbx_re_business_closed.Checked = .BUSINESS_CLOSED
                    df_re_date_business_closed.Text = IIf(.DATE_BUSINESS_CLOSED Is Nothing, "", .DATE_BUSINESS_CLOSED)
                    txt_re_tax_number.Value = .TAX_NUMBER
                    txt_re_tax_reg_number.Value = .TAX_REG_NUMBER

                    Dim phone_list = If(.ObjList_GoAML_Ref_Customer_Phone IsNot Nothing, .ObjList_GoAML_Ref_Customer_Phone,
                        goAML_Customer_Service.GetPhoneByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim address_list = If(.ObjList_GoAML_Ref_Customer_Address IsNot Nothing, .ObjList_GoAML_Ref_Customer_Address,
                        goAML_Customer_Service.GetAddressByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim email_list = If(.ObjList_GoAML_Ref_Customer_Email IsNot Nothing, .ObjList_GoAML_Ref_Customer_Email,
                        goAML_Customer_Service.GetEmailByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim entity_identification_list = If(.ObjList_GoAML_Ref_Customer_Entity_Identification IsNot Nothing, .ObjList_GoAML_Ref_Customer_Entity_Identification,
                        goAML_Customer_Service.GetEntityIdentificationByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim url_list = If(.ObjList_GoAML_Ref_Customer_URL IsNot Nothing, .ObjList_GoAML_Ref_Customer_URL,
                        goAML_Customer_Service.GetUrlByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))
                    Dim sanction_list = If(.ObjList_GoAML_Ref_Customer_Sanction IsNot Nothing, .ObjList_GoAML_Ref_Customer_Sanction,
                        goAML_Customer_Service.GetSanctionByPkFk(.PK_goAML_Ref_WIC_Related_Entities_ID, 36))

                    GridPhone_RelatedEntity.LoadData(phone_list)
                    GridAddress_RelatedEntity.LoadData(address_list)
                    GridEmail_RelatedEntity.LoadData(email_list)
                    GridEntityIdentification_RelatedEntity.LoadData(entity_identification_list)
                    GridURL_RelatedEntity.LoadData(url_list)
                    GridSanction_RelatedEntity.LoadData(sanction_list)
                End With

                WindowDetail_RelatedEntity.Title = "Related Entities " & If(isEdit, "Edit", "Detail")

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class
