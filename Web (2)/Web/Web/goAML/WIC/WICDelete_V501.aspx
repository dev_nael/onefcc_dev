﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="WICDelete_V501.aspx.vb" Inherits="goAML_WICDelete_V501" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>
<%@ Register Src="~/goAML/WIC/Component/GridAddress.ascx" TagPrefix="nds" TagName="GridAddress" %>
<%@ Register Src="~/goAML/WIC/Component/GridEmail.ascx" TagPrefix="nds" TagName="GridEmail" %>
<%@ Register Src="~/goAML/WIC/Component/GridIdentification.ascx" TagPrefix="nds" TagName="GridIdentification" %>
<%@ Register Src="~/goAML/WIC/Component/GridPEP.ascx" TagPrefix="nds" TagName="GridPEP" %>
<%@ Register Src="~/goAML/WIC/Component/GridPhone.ascx" TagPrefix="nds" TagName="GridPhone" %>
<%@ Register Src="~/goAML/WIC/Component/GridSanction.ascx" TagPrefix="nds" TagName="GridSanction" %>
<%@ Register Src="~/goAML/WIC/Component/GridEntityIdentification.ascx" TagPrefix="nds" TagName="GridEntityIdentification" %>
<%@ Register Src="~/goAML/WIC/Component/GridURL.ascx" TagPrefix="nds" TagName="GridURL" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-form-item-label.x-form-item-label-default { width : 200px !important; }
    </style></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <ext:Window ID="WindowDetailDirector" runat="server" Icon="ApplicationViewDetail" Title="Customer Director Detail" Hidden="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="FormPanelDirectorDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:Hidden runat="server" ID="Hidden1"></ext:Hidden>
                    <ext:DisplayField ID="DspPeranDirector" runat="server" FieldLabel="Role" AnchorHorizontal="70%" LabelWidth="250"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250" ID="DspGender" runat="server" FieldLabel="Gender" AnchorHorizontal="70%" ></ext:DisplayField>
                    <ext:DisplayField ID="DspGelarDirector" LabelWidth="250" runat="server" FieldLabel="Title" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaLengkap"  LabelWidth="250" runat="server" FieldLabel="Full Name" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250"  runat="server" FieldLabel="Birth Of Date" ID="DspTanggalLahir" AnchorHorizontal="90%"  />
                    <ext:DisplayField ID="DspTempatLahir"  LabelWidth="250" runat="server" FieldLabel="Birth Of Place" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaIbuKandung" LabelWidth="250" runat="server" FieldLabel="Mother Maiden Name" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaAlias" LabelWidth="250" runat="server" FieldLabel="Alias" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNIK" LabelWidth="250" runat="server" FieldLabel="NIK" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNoPassport" LabelWidth="250" runat="server" FieldLabel="No. Passport" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraPenerbitPassport" LabelWidth="250" runat="server" FieldLabel="Passport Issued Country" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNoIdentitasLain" LabelWidth="250" runat="server" FieldLabel="Other ID Number" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250"  ID="DspKewarganegaraan1" runat="server" FieldLabel="Nationality 1" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspKewarganegaraan2" LabelWidth="250" runat="server" FieldLabel="Nationality 2" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspKewarganegaraan3" LabelWidth="250" runat="server" FieldLabel="Nationality 3" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDomisili"  LabelWidth="250" runat="server" FieldLabel="Domicile Country" AnchorHorizontal="70%"></ext:DisplayField>

                    <ext:GridPanel ID="GridPanel1" runat="server" Title="Phone" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorPhone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column1" runat="server" DataIndex="Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanel2" runat="server" Title="Address" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorAddress" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column7" runat="server" DataIndex="Type_Address" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="Addres" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="Cty" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="CountryCode" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DisplayField ID="DspEmail" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspOccupation"  LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspTempatBekerja" LabelWidth="250" runat="server" FieldLabel="Work Place" AnchorHorizontal="90%"></ext:DisplayField>

                    <ext:GridPanel ID="GridPanel3" runat="server" Title="Work Place Phone" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorPhoneEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model8" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column15" runat="server" DataIndex="Contact_Type" Text="Contact Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="Communication_Type" Text="Communication Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="number" Text="Phone Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanel4" runat="server" Title="Work Place Address" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorAddressEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model17" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column21" runat="server" DataIndex="Type_Address" Text="Address Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="Addres" Text="Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="Cty" Text="City" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="CountryCode" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn4" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanel5" runat="server" Title="Identity Information" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreIdentificationDirector" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model18" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column6" runat="server" DataIndex="Tipe" Text="Identity Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="No" Text="Identity Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="IssueDate" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="ExpiryDate" Text="Expired Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="IssuedCountry" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn5" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DisplayField runat="server" LabelWidth="250" ID="DspDeceased" FieldLabel="Deceased ?"></ext:DisplayField>
                    <%--daniel 2020 des 29--%>
                    <ext:DisplayField runat="server" LabelWidth="250" Hidden="true" FieldLabel="Deceased Date " ID="DspDeceasedDate" />
                    <%--end 2020 des 29--%>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tax Number" ID="DspNPWP" AnchorHorizontal="90%" />
                    <ext:DisplayField runat="server" LabelWidth="250" ID="DspPEP" FieldLabel="PEP ?"></ext:DisplayField>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Source of Wealth" ID="DspSourceofWealth" AnchorHorizontal="90%" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Notes" ID="DspCatatan" AnchorHorizontal="90%" />

                    <ext:Panel ID="Panel_Director_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridEmail runat="server" ID="GridEmail_Director" FK_REF_DETAIL_OF="6" UniqueName = "Director"/>
                        </Content>
                    </ext:Panel>

                    <ext:Panel ID="Panel_Director_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridSanction runat="server" ID="GridSanction_Director" UniqueName = "Director" FK_REF_DETAIL_OF="6"/>
                        </Content>
                    </ext:Panel>

                    <ext:Panel ID="Panel_Director_GridPEP" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridPEP runat="server" ID="GridPEP_Director" UniqueName = "Director" FK_REF_DETAIL_OF="6"/>
                        </Content>
                    </ext:Panel>
                </Items>
                <Buttons>
                    <ext:Button ID="Button5" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveCustomerDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorPhone" runat="server" Icon="ApplicationViewDetail" Title="Phone" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="PanelPhoneDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspKategoriKontakDirector" runat="server" FieldLabel="Contact Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspJenisAlatKomunikasiDirector" runat="server" FieldLabel="Communication Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodeAreaTelpDirector" runat="server" FieldLabel="Country Prefix"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorTeleponDirector" runat="server" FieldLabel="Phone Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorExtensiDirector" runat="server" FieldLabel="Extension"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanDirectorPhoneDirector" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>

                <Buttons>
                    <ext:Button ID="Button3" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelPhoneDirectorEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspKategoriKontakDirectorEmployer" runat="server" FieldLabel="Contact Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspJenisAlatKomunikasiDirectorEmployer" runat="server" FieldLabel="Communication Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodeAreaTelpDirectorEmployer" runat="server" FieldLabel="Country Prefix"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorTeleponDirectorEmployer" runat="server" FieldLabel="Phone Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorExtensiDirectorEmployer" runat="server" FieldLabel="Extension"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanDirectorPhoneDirectorEmployer" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button4" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorEmployerPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorIdentification" runat="server" Icon="ApplicationViewDetail" Title="Phone" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <ext:FormPanel ID="PanelDetailIdentificationDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DsptTypeDirector" runat="server" FieldLabel="Identification Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspNumberDirector" runat="server" FieldLabel="Identification Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssueDateDirector" runat="server" FieldLabel="Issue Date"></ext:DisplayField>
                    <ext:DisplayField ID="DspExpiryDateDirector" runat="server" FieldLabel="Expired Date"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedByDirector" runat="server" FieldLabel=" Issue By"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedCountryDirector" runat="server" FieldLabel="Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspIdentificationCommentDirector" runat="server" FieldLabel="Notes"></ext:DisplayField>               
                </Items>
                <Buttons>
                    <ext:Button ID="Button6" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelIdentificationDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailDirectorIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailIdentification" runat="server" Icon="ApplicationViewDetail" Title="Phone" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <%-- 2023-10-14, Nael: ButtonAlign Center --%>
            <ext:FormPanel ID="PanelDetailIdentification" ButtonAlign="Center" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DsptType" runat="server" FieldLabel="Identification Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspNumber" runat="server" FieldLabel="Identification Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssueDate" runat="server" FieldLabel="Issue Date"></ext:DisplayField>
                    <ext:DisplayField ID="DspExpiryDate" runat="server" FieldLabel="Expired Date"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedBy" runat="server" FieldLabel="Issue By"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedCountry" runat="server" FieldLabel="Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspIdentificationComment" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button8" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelIdentification_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorAddress" runat="server" Icon="ApplicationViewDetail" Title="Address" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet4" runat="server" Title="Address" Collapsible="true" Layout="FitLayout">
                <Content>
                    <ext:FormPanel ID="PanelAddressDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                        <Items>
                        <ext:DisplayField ID="DspTipeAlamatDirector" runat="server" FieldLabel="Address Type"></ext:DisplayField>
                        <ext:DisplayField ID="DspAlamatDirector" runat="server" FieldLabel="Address"></ext:DisplayField>
                        <ext:DisplayField ID="DspKecamatanDirector" runat="server" FieldLabel="Town"></ext:DisplayField>
                        <ext:DisplayField ID="DspKotaKabupatenDirector" runat="server" FieldLabel="City"></ext:DisplayField>
                        <ext:DisplayField ID="DspKodePosDirector" runat="server" FieldLabel="Zip Code"></ext:DisplayField>
                        <ext:DisplayField ID="DspNegaraDirector" runat="server" FieldLabel="Country"></ext:DisplayField>
                        <ext:DisplayField ID="DspProvinsiDirector" runat="server" FieldLabel="Province"></ext:DisplayField>
                        <ext:DisplayField ID="DspCatatanAddressDirector" runat="server" FieldLabel="Notes"></ext:DisplayField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button1" runat="server" Icon="Cancel" Text="Cancel">
                                <DirectEvents>
                                    <Click OnEvent="BtnCancelDirectorDetailAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <ext:FormPanel ID="PanelAddressDirectorEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                        <Items>
                            <ext:DisplayField ID="DspTipeAlamatDirectorEmployer" runat="server" FieldLabel="Address Type2"></ext:DisplayField>
                            <ext:DisplayField ID="DspAlamatDirectorEmployer" runat="server" FieldLabel="Address"></ext:DisplayField>
                            <ext:DisplayField ID="DspKecamatanDirectorEmployer" runat="server" FieldLabel="Town"></ext:DisplayField>
                            <ext:DisplayField ID="DspKotaKabupatenDirectorEmployer" runat="server" FieldLabel="City"></ext:DisplayField>
                            <ext:DisplayField ID="DspKodePosDirectorEmployer" runat="server" FieldLabel="Zip Code"></ext:DisplayField>
                            <ext:DisplayField ID="DspNegaraDirectorEmployer" runat="server" FieldLabel="Country"></ext:DisplayField>
                            <ext:DisplayField ID="DspProvinsiDirectorEmployer" runat="server" FieldLabel="Province"></ext:DisplayField>
                            <ext:DisplayField ID="DspCatatanAddressDirectorEmployer" runat="server" FieldLabel="Notes"></ext:DisplayField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button2" runat="server" Icon="Cancel" Text="Cancel">
                                <DirectEvents>
                                    <Click OnEvent="BtnCancelDirectorDetailEmployerAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailPhone" runat="server" Icon="ApplicationViewDetail" Title="Phone" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <ext:FormPanel ID="PanelDetailPhone" ButtonAlign="Center" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="Dsptph_contact_type" runat="server" FieldLabel="Contact Type"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_communication_type" runat="server" FieldLabel="Communication Type"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_country_prefix" runat="server" FieldLabel="Prefix"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_number" runat="server" FieldLabel="Phone Number"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_extension" runat="server" FieldLabel="Extention"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsPhone" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnCancelPhone" runat="server" Text="Back">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <%-- 2023-10-14, Nael: ButtonAlign Center --%>
            <ext:FormPanel ID="PanelDetailPhoneEmployer" ButtonAlign="Center" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="Dsptph_contact_typeEmployer" runat="server" FieldLabel="Contact Type"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_communication_typeEmployer" runat="server" FieldLabel="Communication Type"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_country_prefixEmployer" runat="server" FieldLabel="Prefix"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_numberEmployer" runat="server" FieldLabel="Phone Number"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_extensionEmployer" runat="server" FieldLabel="Extention"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsPhoneEmployer" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button16" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelPhoneEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailPhone}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailAddress" runat="server" Icon="ApplicationViewDetail" Title="Address" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <%-- 2023-10-14, Nael: Button Align Center --%>
            <ext:FormPanel ID="PanelDetailAddress" runat="server" ButtonAlign="Center" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspAddress_type" runat="server" FieldLabel="Address Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspAddress" runat="server" FieldLabel="Address"></ext:DisplayField>
                    <ext:DisplayField ID="DspTown" runat="server" FieldLabel="Town"></ext:DisplayField>
                    <ext:DisplayField ID="DspCity" runat="server" FieldLabel="City"></ext:DisplayField>
                    <ext:DisplayField ID="DspZip" runat="server" FieldLabel="Zip Code"></ext:DisplayField>
                    <ext:DisplayField ID="Dspcountry_code" runat="server" FieldLabel="Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspState" runat="server" FieldLabel="Province"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsAddress" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnCancelAddress" runat="server" Text="Back">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <%-- 2023-10-14, Nael --%>
            <ext:FormPanel ID="PanelDetailAddressEmployer" ButtonAlign="Center" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspAddress_typeEmployer" runat="server" FieldLabel="Address Type"></ext:DisplayField>
                    <ext:DisplayField ID="DspAddressEmployer" runat="server" FieldLabel="Address"></ext:DisplayField>
                    <ext:DisplayField ID="DspTownEmployer" runat="server" FieldLabel="Town"></ext:DisplayField>
                    <ext:DisplayField ID="DspCityEmployer" runat="server" FieldLabel="City"></ext:DisplayField>
                    <ext:DisplayField ID="DspZipEmployer" runat="server" FieldLabel="Zip Code"></ext:DisplayField>
                    <ext:DisplayField ID="Dspcountry_codeEmployer" runat="server" FieldLabel="Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspStateEmployer" runat="server" FieldLabel="Province"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsAddressEmployer" runat="server" FieldLabel="Notes"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button19" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelAddressEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailAddress}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowEmail" runat="server" Icon="ApplicationViewDetail" Title="Email" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <%-- 2023-10-14, Nael: Align Button Center --%>
            <ext:FormPanel ID="FormPanelEmail" runat="server" AnchorHorizontal="100%" ButtonAlign="Center" BodyPadding="10"  DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspWindowEmail" runat="server" FieldLabel="Email"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="ButtonEmail" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelEmailDirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowPersonPEP" runat="server" Icon="ApplicationViewDetail" Title="Person PEP" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <%-- 2023-10-14, Nael: ButtonAlign Center --%>
            <ext:FormPanel ID="FormPanelPersonPEP" runat="server" AnchorHorizontal="100%" ButtonAlign="Center" BodyPadding="10"  DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspCountry" runat="server" FieldLabel="Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspFunctionName" runat="server" FieldLabel="Function Name"></ext:DisplayField>
                    <ext:DisplayField ID="DspWindowDescription" runat="server" FieldLabel="Description"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspWindowValidFrom" runat="server" FieldLabel="Valid From"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspWindowIsApproxFromDate" runat="server" FieldLabel="Is Approx From Date"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspWindowValidTo" runat="server" FieldLabel="Valid To"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspWindowIsApproxToDate" runat="server" FieldLabel="Is Approx To Date"></ext:DisplayField>
                    <ext:DisplayField ID="DspWindowPersonPEPComments" runat="server" FieldLabel="Comments"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="ButtonPersonPEP" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelPersonPEPDirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowSanction" runat="server" Icon="ApplicationViewDetail" Title="Sanction" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <%-- START: 2023-10-14, Nael: Button align center --%>
            <ext:FormPanel ID="FormPanelSanction" ButtonAlign="Center" runat="server" AnchorHorizontal="100%" BodyPadding="10"  DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspProvider" runat="server" FieldLabel="Provider"></ext:DisplayField>
                    <ext:DisplayField ID="DspSanctionListName" runat="server" FieldLabel="Sanction List Name"></ext:DisplayField>
                    <ext:DisplayField ID="DspMatchCriteria" runat="server" FieldLabel="Match Criteria"></ext:DisplayField>
                    <ext:DisplayField ID="DspLinktoSource" runat="server" FieldLabel="Link to Source"></ext:DisplayField>
                    <ext:DisplayField ID="DspSanctionListAttributes" runat="server" FieldLabel="Sanction List Attributes"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspWindowSanctionValidFrom" runat="server" FieldLabel="Valid From"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspWindowSanctionIsApproxFromDate" runat="server" FieldLabel="Is Approx From Date"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspWindowSanctionValidTo" runat="server" FieldLabel="Valid To"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspWindowSanctionIsApproxToDate" runat="server" FieldLabel="Is Approx To Date"></ext:DisplayField>
                    <ext:DisplayField ID="DisplayWindowSanctionComments" runat="server" FieldLabel="Comments"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="ButtonSanction" runat="server" Icon="Cancel" Text="Back">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelSanctionDirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <%-- END: 2023-10-14, Nael: Button align center --%>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowRelatedPerson" runat="server" Icon="ApplicationViewDetail" Title="Related Person" Hidden="true" Layout="FitLayout" Modal="true" MinWidth="800" Height="500" AutoScroll="true">
        <Items>
            <%-- 2023-10-14, Nael: ButtonAlign Center --%>
            <ext:FormPanel ID="FormPanelRelatedPerson" ButtonAlign="Center" runat="server" AnchorHorizontal="100%" BodyPadding="10"  DefaultAlign="center" AutoScroll="true">
                <Items>
                    <%--<ext:DisplayField ID="DspPersonPersonRelation" runat="server" FieldLabel="Person Person Relation"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspRelationDateRangeValidFrom" runat="server" FieldLabel="Relation Date Range - Valid From"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspRelationDateRangeIsApproxFromDate" runat="server" FieldLabel="Relation Date Range - Is Approx From Date"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspRelationDateRangeValidTo" runat="server" FieldLabel="Relation Date Range - Valid To"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspRelationDateRangeIsApproxToDate" runat="server" FieldLabel="Relation Date Range - Is Approx To Date"></ext:DisplayField>
                    <ext:DisplayField ID="DspWindowRelatedPersonComments" runat="server" FieldLabel="Comments"></ext:DisplayField>
                    <ext:DisplayField ID="DspWindowGender" runat="server" FieldLabel="Gender"></ext:DisplayField>
                    <ext:DisplayField ID="DspTitle" runat="server" FieldLabel="Title"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspFirstName" runat="server" FieldLabel="First Name"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspMiddleName" runat="server" FieldLabel="Middle Name"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspPrefix" runat="server" FieldLabel="Prefix"></ext:DisplayField>
                    <ext:DisplayField ID="DspLastName" runat="server" FieldLabel="Full Name"></ext:DisplayField>
                    <ext:DisplayField ID="DspBirthDate" runat="server" FieldLabel="Birth Date"></ext:DisplayField>
                    <ext:DisplayField ID="DspBirthPlace" runat="server" FieldLabel="Birth Place"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspCountryofBirth" runat="server" FieldLabel="Country of Birth"></ext:DisplayField>
                    <ext:DisplayField ID="DspMotherName" runat="server" FieldLabel="Mother Name"></ext:DisplayField>
                    <ext:DisplayField ID="DspAlias" runat="server" FieldLabel="Alias"></ext:DisplayField>
                    <ext:DisplayField ID="DspFullForeignName" runat="server" FieldLabel="Full Foreign Name"></ext:DisplayField>
                    <ext:DisplayField ID="DspSSN" runat="server" FieldLabel="SSN"></ext:DisplayField>
                    <ext:DisplayField ID="DspPassportNumber" runat="server" FieldLabel="Passport Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspPassportCountry" runat="server" FieldLabel="Passport Country"></ext:DisplayField>
                    <ext:DisplayField ID="DspIDNumber" runat="server" FieldLabel="ID Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspNationality1" runat="server" FieldLabel="Nationality 1"></ext:DisplayField>
                    <ext:DisplayField ID="DspNationality2" runat="server" FieldLabel="Nationality 2"></ext:DisplayField>
                    <ext:DisplayField ID="DspNationality3" runat="server" FieldLabel="Nationality 3"></ext:DisplayField>
                    <ext:DisplayField ID="DspResidence" runat="server" FieldLabel="Residence"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="DspResidenceSince" runat="server" FieldLabel="Residence Since"></ext:DisplayField>
                    <ext:DisplayField ID="DspWindowOccupation" runat="server" FieldLabel="Occupation"></ext:DisplayField>
                    <ext:DisplayField ID="DspWindowDeceased" runat="server" FieldLabel="Deceased"></ext:DisplayField>
                    <ext:DisplayField ID="DspDateDeceased" runat="server" FieldLabel="Date Deceased"></ext:DisplayField>
                    <ext:DisplayField ID="DspTaxNumber" runat="server" FieldLabel="Tax Number"></ext:DisplayField>
                    <ext:DisplayField ID="DspIsPEP" runat="server" FieldLabel="Is PEP"></ext:DisplayField>
                    <ext:DisplayField ID="DspWindowSourceofWealth" runat="server" FieldLabel="Source of Wealth"></ext:DisplayField>
                    <ext:DisplayField ID="DspIsProtected" runat="server" FieldLabel="Is Protected"></ext:DisplayField>--%>

                    <ext:DisplayField ID="DisplayWIC_RelatedPerson" runat="server" FieldLabel="WIC" AnchorHorizontal="80%"  MaxLength="500" ></ext:DisplayField>
                    <ext:Panel ID="Pnl_Person_Person_Relation" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_Person_Person_Relation" Label="Person Person Relation" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="goAML_Ref_Person_Person_Relationship" AnchorHorizontal="70%" AllowBlank="false"/>
                        </Content>
                    </ext:Panel>
                    
         <%--           <ext:DateField ID="dt_Relation_Date_Range_Valid_From" runat="server" FieldLabel="Relation Date Range -  Valid From" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Relation_Date_Range_Is_Approx_From_Date" FieldLabel="Is Approx From Date"></ext:Checkbox>
                    <ext:DateField ID="dt_Relation_Date_Range_Valid_To" runat="server" FieldLabel="Relation Date Range -  Valid To" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Relation_Date_Range_Is_Approx_To_Date" FieldLabel="Is Approx From Date"></ext:Checkbox>--%>
                    <ext:TextField ID="TxtRelatedPersonRelationComments" runat="server" FieldLabel="Relation Comments" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <%--<ext:TextField ID="TxtRelatedPersonGender" runat="server" FieldLabel="Gender" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>--%>
                    <ext:Panel ID="Panel_cmb_rp_gender" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
	                    <Content>
		                    <nds:NDSDropDownField ID="cmb_rp_gender" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Jenis_Kelamin" StringFilter="Active = 1" EmptyText="Select One" Label="Gender" AnchorHorizontal="70%" />
	                    </Content>
                    </ext:Panel>
                    <ext:TextField ID="TxtRelatedPersonTitle" runat="server" FieldLabel="Title" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField Hidden="true" ID="TxtRelatedPersonFirst_Name" runat="server" FieldLabel="First Name" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField Hidden="true" ID="TxtRelatedPersonMiddle_Name" runat="server" FieldLabel="Middle Name" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField Hidden="true" ID="TxtRelatedPersonPrefix" runat="server" FieldLabel="Prefix" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField ID="TxtRelatedPersonLast_Name" runat="server" FieldLabel="Full Name" AnchorHorizontal="80%"  MaxLength="500"  AllowBlank="false"></ext:TextField>
                    <ext:DateField ID="dt_RelatedPersonBirth_Date" runat="server" FieldLabel="Date of Birth" AnchorHorizontal="40%" Format="dd-MMM-yyyy" />
                    <ext:TextField ID="TxtRelatedPersonBirth_Place" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:Panel ID="Pnl_RelatedPersonCountry_Of_Birth" Hidden="true" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_RelatedPersonCountry_Of_Birth" Label="Country of Birth" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                        </Content>
                    </ext:Panel>
                    
                    <ext:TextField ID="TxtRelatedPersonMother_Name" runat="server" FieldLabel="Mother Name" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField ID="TxtRelatedPersonAlias" runat="server" FieldLabel="Alias" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                  <%--  <ext:TextField ID="TxtRelatedPersonFull_Name_Frn" runat="server" FieldLabel="Full Name Frn" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>--%>
                    <ext:TextField ID="TxtRelatedPersonSSN" runat="server" FieldLabel="SSN" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField ID="TxtPassport_Number" runat="server" FieldLabel="Passport Number" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:Panel ID="Pnl_Passport_Country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_Passport_Country" Label="Passport Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                        </Content>
                    </ext:Panel>
                    
                    <ext:TextField ID="TxtRelatedPersonID_Number" runat="server" FieldLabel="ID Number" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:Panel ID="Pnl_RelatedPersonNationality1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_RelatedPersonNationality1" Label="Nationality 1" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                        </Content>
                    </ext:Panel>
                    
                    <ext:Panel ID="Pnl_RelatedPersonNationality2" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_RelatedPersonNationality2" Label="Nationality 2" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                        </Content>
                    </ext:Panel>
                    
                    <ext:Panel ID="Pnl_RelatedPersonNationality3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_RelatedPersonNationality3" Label="Nationality 3" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                        </Content>
                    </ext:Panel>
                    
                    <ext:Panel ID="Pnl_RelatedPersonResidence" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="dd_RelatedPersonResidence" Label="Residence" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                        </Content>
                    </ext:Panel>
                    
                    <%--<ext:DateField ID="dt_RelatedPersonResidence_Since" runat="server" FieldLabel="Residence Since" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" />--%>
                    <ext:TextField ID="TxtRelatedPersonOccupation" runat="server" FieldLabel="Occupation" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:TextField ID="TxtRelatedPersonEmployerName" runat="server" FieldLabel="Work Place" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_RelatedPersonDeceased" FieldLabel="Deceased"></ext:Checkbox>
                    <ext:DateField ID="dt_TxtRelatedPersonDate_Deceased" runat="server" FieldLabel="Date Deceased" AnchorHorizontal="40%" Format="dd-MMM-yyyy" />
                    <ext:TextField ID="TxtRelatedPersonTax_Number" runat="server" FieldLabel="Tax Number" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_RelatedPersonIs_PEP" FieldLabel="Is PEP"></ext:Checkbox>
                    <ext:TextField ID="TxtRelatedPersonSource_Of_Wealth" runat="server" FieldLabel="Source of Wealth" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_RelatedPersonIs_Protected" FieldLabel="Is Protected"></ext:Checkbox>    

                    <ext:Panel ID="Panel_RelatedPerson_GridAddress" Title="Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridAddress runat="server" ID="GridAddress_RelatedPerson" UniqueName="RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="31"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridAddressWork" Title="Work Place Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridAddress runat="server" ID="GridAddressWork_RelatedPerson" UniqueName="Work.RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="32"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridPhone" Title="Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridPhone runat="server" ID="GridPhone_RelatedPerson" UniqueName="RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="31"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridPhoneWork" Title="Work Place Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridPhone runat="server" ID="GridPhoneWork_RelatedPerson" UniqueName="Work.RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="32" />
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridIdentification" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridIdentification runat="server" ID="GridIdentification_RelatedPerson" UniqueName="RelatedPerson.ADD" IsCustomer="true" FK_Ref_Detail_Of="31" />
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridEmail runat="server" ID="GridEmail_RelatedPerson" UniqueName="RelatedPerson.ADD" FK_REF_DETAIL_OF="31"/>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridSanction runat="server" ID="GridSanction_RelatedPerson" UniqueName="RelatedPerson.ADD" FK_REF_DETAIL_OF="31" />
                        </Content>
                                
                    </ext:Panel>
                    <ext:Panel ID="Panel_RelatedPerson_GridPEP" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:GridPEP runat="server" ID="GridPEP_RelatedPerson" UniqueName="RelatedPerson.ADD" FK_REF_DETAIL_OF="31" />
                        </Content>
                    </ext:Panel>

                    <ext:TextField ID="TxtRelatedPersonComments" runat="server" FieldLabel="Comments" AnchorHorizontal="80%"  MaxLength="500" ></ext:TextField>

                </Items>
            <Buttons>
                <ext:Button ID="ButtonRelatedPerson" runat="server" Icon="Cancel" Text="Cancel">
                    <DirectEvents>
                        <Click OnEvent="BtnCancelRelatedPersonDirectEvent">
                            <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.7});" />
            <Resize Handler="#{WindowDetailIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <%-- Add By Septian, goAML 5.0.1, 2022-03-02 --%>
    <ext:Window ID="WindowDetail_URL" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity URL/Website Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet12" runat="server" Title="Entity URL/Website" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_URL" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_url" runat="server" FieldLabel="URL" AnchorHorizontal="90%"  MaxLength="255"></ext:TextField>
                        </Items>
                        <Buttons>
                          <%--  <ext:Button ID="btnSaveWIC_URL" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveWIC_URL_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>--%>
                            <ext:Button ID="btnBacKWIC_URL" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBacKWIC_URL_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_EntityIdentification" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Entity Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet13" runat="server" Title="Entity Identification" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_EntityIdentification" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="Panel_cmb_ef_type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
    	                        <Content>
    		                        <NDS:NDSDropDownField ID="cmb_ef_type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Identifier_Type" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Identifier Type" AnchorHorizontal="70%"  />
    	                        </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_ef_number"  LabelWidth="250" runat="server" FieldLabel="Number" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Issued Date" ID="df_issue_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Expiry Date" ID="df_expiry_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_ef_issued_by"  LabelWidth="250" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <ext:Panel ID="pnl_ef_issue_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
    	                        <Content>
    		                        <NDS:NDSDropDownField ID="cmb_ef_issue_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Issued Country" AnchorHorizontal="70%"  />
    	                        </Content>
                            </ext:Panel>
                            <ext:TextField ID="txt_ef_comments"  LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="50"></ext:TextField>
                        </Items>
                        <Buttons>
                           <%-- <ext:Button ID="btnSaveWIC_EntityIdentification" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveWIC_EntityIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>--%>
                            <ext:Button ID="btnBacKWIC_EntityIdentification" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBacKWIC_EntityIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetail_RelatedEntities" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Related Entity Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet14" runat="server" Title="Entity Identification" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanel_RelatedEntities" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--5 --%>
                            <ext:Panel ID="Panel_cmb_re_entity_relation" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
    	                        <Content>
    		                        <NDS:NDSDropDownField ID="cmb_re_entity_relation" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Entity_Relationship" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Relation" AnchorHorizontal="70%"  />
    	                        </Content>
                            </ext:Panel>
                            <%--6 --%>
                            <ext:DateField Editable="false" Hidden="true" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid From" ID="df_re_relation_date_range_valid_from" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%--7 --%>
                            <ext:Checkbox runat="server" Hidden="true" LabelWidth="250" ID="cbx_re_relation_date_range_is_approx_from_date" FieldLabel="Relation Date Range -  Is Approx From Date?"></ext:Checkbox>
                            <%--8 --%>
                            <ext:DateField Editable="false" Hidden="true" LabelWidth="250"  runat="server" FieldLabel="Relation Date Range -  Valid To" ID="df_re_relation_date_range_valid_to" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%--9 --%>
                            <ext:Checkbox runat="server" Hidden="true" LabelWidth="250" ID="cbx_re_relation_date_range_is_approx_to_date" FieldLabel="Relation Date Range -  Is Approx To Date?"></ext:Checkbox>
                            <%--10 --%>
                            <ext:NumberField runat="server" Hidden="true" LabelWidth="250" ID="nfd_re_share_percentage" FieldLabel="Share Percentage" AnchorHorizontal="90%" MouseWheelEnabled="false" FormatText="#,##0.00" />
                            <%--11 --%>
                            <ext:TextField ID="txt_re_relation_comments"  LabelWidth="250" runat="server" FieldLabel="Relation Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>
                            <%--12 --%>
                            <ext:TextField ID="txt_re_name"  LabelWidth="250" runat="server" FieldLabel="Corporate Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--13 --%>
                            <ext:TextField ID="txt_re_commercial_name"  LabelWidth="250" runat="server" FieldLabel="Commercial Name" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--14 --%>
                            <ext:Panel ID="Panel_cmb_re_incorporation_legal_form" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
    	                        <Content>
    		                        <NDS:NDSDropDownField ID="cmb_re_incorporation_legal_form" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Bentuk_Badan_Usaha" StringFilter="Active = 1" EmptyText="Select One" Label="Legal Form" AnchorHorizontal="70%"  />
    	                        </Content>
                            </ext:Panel>
                            <%--15 --%>
                            <ext:TextField ID="txt_re_incorporation_number"  LabelWidth="250" runat="server" FieldLabel="License Number" AnchorHorizontal="90%" MaxLength="50"></ext:TextField>
                            <%--16 --%>
                            <ext:TextField ID="txt_re_business"  LabelWidth="250" runat="server" FieldLabel="Line of Business" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--17 --%>
                            <ext:Panel ID="Panel_cmb_re_entity_status" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
    	                        <Content>
    		                        <NDS:NDSDropDownField ID="cmb_re_entity_status" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Entity_Status" StringFilter="Active = 1" EmptyText="Select One" Label="Entity Status" AnchorHorizontal="70%"  />
    	                        </Content>
                            </ext:Panel>
                            <%--18 --%>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Entity Status Date" ID="df_re_entity_status_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%--19 --%>
                            <ext:TextField ID="txt_re_incorporation_state"  LabelWidth="250" runat="server" FieldLabel="Province" AnchorHorizontal="90%" MaxLength="255"></ext:TextField>
                            <%--20 --%>
                            <ext:Panel ID="Panel_cmb_re_incorporation_country_code" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
    	                        <Content>
    		                        <NDS:NDSDropDownField ID="cmb_re_incorporation_country_code" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Incorproation Country Code" AnchorHorizontal="70%"  />
    	                        </Content>
                            </ext:Panel>
                            <%--21 --%>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Date of Established" ID="df_re_incorporation_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <%--22 --%>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cbx_re_business_closed" FieldLabel="Businesss Closed?"></ext:Checkbox>
                            <%--23 --%>
                            <ext:DateField Editable="false" LabelWidth="250"  runat="server" FieldLabel="Date Business Closed" ID="df_re_date_business_closed" AnchorHorizontal="90%" Format="dd-MMM-yyyy" Hidden="false" />
                            <%--24 --%>
                            <ext:TextField ID="txt_re_tax_number"  LabelWidth="250" runat="server" FieldLabel="Tax Number" AnchorHorizontal="90%" MaxLength="16"></ext:TextField>
                            <%--25 --%>
                            <ext:TextField ID="txt_re_tax_reg_number"  LabelWidth="250" Hidden="true" runat="server" FieldLabel="Tax Reg Number" AnchorHorizontal="90%" MaxLength="100"></ext:TextField>

                            <ext:Panel ID="Panel_RelatedEntity_GridPhone" Title="Phone" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <NDS:GridPhone runat="server" ID="GridPhone_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridAddress" Title="Address" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <NDS:GridAddress runat="server" ID="GridAddress_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridEmail" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEmail runat="server" ID="GridEmail_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_EntityIdentification" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridEntityIdentification runat="server" ID="GridEntityIdentification_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridURL" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridURL runat="server" ID="GridURL_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_RelatedEntity_GridSanction" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:GridSanction runat="server" ID="GridSanction_RelatedEntity" UniqueName="RelatedEntity" FK_REF_DETAIL_OF="36"/>
                                </Content>
                            </ext:Panel>

                            <ext:TextField ID="txt_re_comments"  LabelWidth="250" runat="server" FieldLabel="Comments" AnchorHorizontal="90%" MaxLength="8000"></ext:TextField>

                        </Items>
                        <Buttons>
                           <%-- <ext:Button ID="btnSaveWIC_RelatedEntities" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveWIC_RelatedEntities_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>--%>
                            <ext:Button ID="btnBacKWIC_RelatedEntities" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBacKWIC_RelatedEntities_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    %-- End Add --%>
    <ext:FormPanel runat="server" Title="WIC Delete" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="WIC_Panel">
        <Defaults>
             
            <ext:Parameter Name="LabelWidth" Value="200" />
        </Defaults>

        <Items>
            <ext:DisplayField ID="PKWIC" runat="server" FieldLabel="ID"></ext:DisplayField>
            <ext:DisplayField ID="DSpWICNo" runat="server" FieldLabel="WIC No"></ext:DisplayField>
            <ext:DisplayField ID="DspIsUpdateFromDataSource" runat="server" LabelWidth="200" FieldLabel="Update From Data Source ?"></ext:DisplayField>
            <ext:DisplayField ID="DspIsRealWIC" runat="server" FieldLabel="Is Real WIC ?"></ext:DisplayField>
            <ext:FormPanel runat="server" Title="WIC Person" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" ID="WIC_Individu" Border="true">
                <Defaults>
                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
            <ext:Parameter Name="LabelWidth" Value="200" />

                </Defaults>
                <Items>
                    <ext:DisplayField ID="TxtINDV_Gender" runat="server" FieldLabel="Gender"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Title" runat="server" FieldLabel="Title" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_last_name" runat="server" FieldLabel="Full Name" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>                   
                    <ext:DisplayField ID="TxtINDV_Frgn_Lng_Full_Name" runat="server" FieldLabel="Foreign Language Full Name" AnchorHorizontal="80%" LabelWidth="200" Hidden="true"></ext:DisplayField>

                    <ext:DisplayField ID="DateINDV_Birthdate" runat="server" Flex="1" FieldLabel="Date of Birth" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Birth_place" runat="server" FieldLabel="Place of Birth" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <%-- 2023-11-10, Nael --%>
                    <ext:DisplayField ID="TxtINDV_Country_Birth" runat="server" FieldLabel="Country of Birth" AnchorHorizontal="80%" LabelWidth="175" Hidden="true"></ext:DisplayField>
                    
                    <ext:DisplayField ID="TxtINDV_Mothers_name" runat="server" FieldLabel="Mother Maiden Name" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Alias" runat="server" FieldLabel="Alias Name" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_SSN" runat="server" FieldLabel="NIK/SSN" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Passport_number" runat="server" FieldLabel="Passport No" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Passport_country" runat="server" FieldLabel="Passport Issued Country" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_ID_Number" runat="server" FieldLabel="Other ID Number" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CmbINDV_Nationality1" runat="server" FieldLabel="Nationality 1" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CmbINDV_Nationality2" runat="server" FieldLabel="Nationality 2" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CmbINDV_Nationality3" runat="server" FieldLabel="Nationality 3" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CmbINDV_residence" runat="server" FieldLabel="Domicile Country" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    
                    <%-- 2023-11-10, Nael: Hide field D --%>
                    <ext:DisplayField ID="TxtINDV_Residence_Since" runat="server" FieldLabel="Residence Since" AnchorHorizontal="80%" LabelWidth="175" Hidden="true"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Is_Protected" runat="server" FieldLabel="Is Protected" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    
                    <ext:GridPanel ID="GridPanelPhoneIndividuDetail" runat="server" Title="Phone" AutoScroll="true" EmptyText="No Available Data" Border="true" >
                        <Store>
                            <ext:Store ID="StorePhoneIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="ModelDetail" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumberPhoneDetailIndividu" Text="No"  Width="60" runat="server"></ext:RowNumbererColumn>
                                <ext:Column ID="colTph_Contact_TypeIndividu" runat="server" DataIndex="Contact_Type" Text="Contact Type" flex="1"></ext:Column>
                                <ext:Column ID="coltph_communication_typeIndividu" runat="server" DataIndex="Communication_Type" Text="Communication Type" flex="1"></ext:Column>
                                <ext:Column ID="coltph_numberIndividu" runat="server" DataIndex="number" Text="Phone Number" flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhoneIndividuDetail" runat="server" Text="Action" Flex="1">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhoneIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar15" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelAddressIndividuDetail" runat="server" Title="Address" AutoScroll="true" EmptyText="No Available Data" Border="true" >
                        <Store>
                            <ext:Store ID="StoreAddressIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                            </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailIndividu" Text="No"  Width="60" runat="server"></ext:RowNumbererColumn>
                                <ext:Column ID="ColAddress_typeIndividu" runat="server" DataIndex="Type_Address" Text="Address Type" flex="1"></ext:Column>
                                <ext:Column ID="ColAddressIndividu" runat="server" DataIndex="Addres" Text="Address" flex="2"></ext:Column>
                                <ext:Column ID="ColCityIndividu" runat="server" DataIndex="Cty" Text="Town" flex="1"></ext:Column>
                                <ext:Column ID="Colcountry_codeIndividu" runat="server" DataIndex="CountryCode" Text="Country" flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAddressIndividuDetail" runat="server" Text="Action" Flex="1" >
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddressIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    
                    <%--<ext:DisplayField ID="TxtINDV_Email" runat="server" FieldLabel="Email" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Email2" runat="server" FieldLabel="Email2" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Email3" runat="server" FieldLabel="Email3" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Email4" runat="server" FieldLabel="Email4" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Email5" runat="server" FieldLabel="Email5" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>--%>
                   
                    <ext:GridPanel ID="GridPanelEmail" runat="server" Title="Email" AutoScroll="true" EmptyText="No Available Data" Border="true">
                        <Store>
                            <ext:Store ID="StoreEmail" runat="server">
                                <Model>
                                    <ext:Model ID="ModelEmail" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Email_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="email_address" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumnEmail" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="ColumnEmail_address" runat="server" DataIndex="email_address" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnEmailAction" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdEmail">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Email_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar19" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:DisplayField ID="TxtINDV_Occupation" runat="server" FieldLabel="Occupation" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_employer_name" runat="server" FieldLabel="Work Place" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                  
                    <ext:GridPanel ID="GridPanelPhoneIndividualEmployerDetail" runat="server" Title="Work Place Phone" AutoScroll="true" EmptyText="No Available Data" Border="true">
                        <Store>
                            <ext:Store ID="StorePhoneEmployerIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model20" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn12" Text="No"  Width="60" runat="server"></ext:RowNumbererColumn>
                                <ext:Column ID="Column27" runat="server" DataIndex="Contact_Type" Text="Contact Type" flex="1"></ext:Column>
                                <ext:Column ID="Column28" runat="server" DataIndex="Communication_Type" Text="Communication Type" flex="1"></ext:Column>
                                <ext:Column ID="Column29" runat="server" DataIndex="number" Text="Phone Number" flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhoneIndividualEmployerDetail" runat="server" Text="Action" Flex="1" >
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhoneEmployerIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelAddressIndividualEmployerDetail" runat="server" Title="Work Place Address" AutoScroll="true" EmptyText="No Available Data" Border="true">
                        <Store>
                            <ext:Store ID="StoreAddressEmployerIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model29" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                            </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn13" Text="No" runat="server" Width="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column30" runat="server" DataIndex="Type_Address" Text="Address Type" flex="1"></ext:Column>
                                <ext:Column ID="Column33" runat="server" DataIndex="Addres" Text="Address" flex="1"></ext:Column>
                                <ext:Column ID="Column34" runat="server" DataIndex="Cty" Text="City" flex="1"></ext:Column>
                                <ext:Column ID="Column35" runat="server" DataIndex="CountryCode" Text="Country" flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAddressIndividualEmployerDetail" runat="server" Text="Action" Flex="1" >
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddressEmployerIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelIdentificationIndividualDetail" runat="server" Title="Identification" AutoScroll="true" EmptyText="No Available Data" Border="true">
                        <Store>
                            <ext:Store ID="StoreIdentificationDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model30" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn14" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column36" runat="server" DataIndex="Tipe" Text="Identification Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column37" runat="server" DataIndex="No" Text="Identification Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column38" runat="server" DataIndex="IssueDate" Text="Issue Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column39" runat="server" DataIndex="ExpiryDate" Text="Expired Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column40" runat="server" DataIndex="IssuedCountry" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnIdentificationIndividualDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DisplayField ID="CbINDV_Deceased" runat="server" FieldLabel="Has Passed Away?" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="DateINDV_Deceased_Date" runat="server" Flex="1" FieldLabel="Date of Passed Away" Hidden="true" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Tax_Number" runat="server" FieldLabel="Tax Number" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CbINDV_Tax_Reg_Number" runat="server" FieldLabel="Is PEP ?" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    
                    <ext:TextField ID="TxtINDV_Source_Of_Wealth" runat="server" FieldLabel="Source of Wealth" AnchorHorizontal="80%" MaxLength="255"></ext:TextField>
                    <ext:TextArea ID="TxtINDV_Comments" runat="server" FieldLabel="Notes" AnchorHorizontal="80%" MaxLength="4000"></ext:TextArea>

                    <ext:DisplayField Hidden="true" ID="TxtINDV_Function_Name" runat="server" FieldLabel="Function Name" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="TxtINDV_Description" runat="server" FieldLabel="Description" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="TxtINDV_Valid_From" runat="server" FieldLabel="Valid From" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="TxtINDV_Is_Approx_From_Date" runat="server" FieldLabel="Is Approx From Date" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="TxtINDV_Valid_To" runat="server" FieldLabel="Valid To" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField Hidden="true" ID="TxtINDV_Is_Approx_To_Date" runat="server" FieldLabel="Is Approx To Date" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <%--<ext:DisplayField ID="TxtINDV_Comments" runat="server" FieldLabel="Comments" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>--%>
                                                                       
                    <%--<ext:DisplayField ID="TxtINDV_Source_Of_Wealth" runat="server" FieldLabel="Source of Wealth" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>--%>
                    <%--<ext:DisplayField ID="TxtINDV_Comments" runat="server" Flex="1" FieldLabel="Notes" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>--%>

                    
                    <ext:GridPanel ID="GridPanelPersonPEP" runat="server" Title="Person PEP" AutoScroll="true" EmptyText="No Available Data" Border="true">
                        <Store>
                            <ext:Store ID="StorePersonPEP" runat="server">
                                <Model>
                                    <ext:Model ID="ModelPersonPEP" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_PEP_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="pep_country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="function_name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="function_description" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="pep_date_range_valid_from" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="pep_date_range_is_approx_from_date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="pep_date_range_valid_to" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="pep_date_range_is_approx_to_date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumnPersonPEP" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="ColumnPep_Country" runat="server" DataIndex="pep_country" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnFunction_Name" runat="server" DataIndex="function_name" Text="Function Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnFunction_Description" runat="server" DataIndex="function_description" Text="Description" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="ColumnPep_date_range_valid_from" runat="server" DataIndex="pep_date_range_valid_from" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnPep_date_range_is_approx_from_date" runat="server" DataIndex="pep_date_range_is_approx_from_date" Text="Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnPep_date_range_valid_to" runat="server" DataIndex="pep_date_range_valid_to" Text="Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnPep_date_range_is_approx_to_date" runat="server" DataIndex="pep_date_range_is_approx_to_date" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="ColumnPersonPEPComments" runat="server" DataIndex="comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPersonPEP" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdPersonPEP">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_PEP_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar17" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelSanction" runat="server" Title="Sanction" AutoScroll="true" EmptyText="No Available Data" Border="true">
                        <Store>
                            <ext:Store ID="StoreSanction" runat="server">
                                <Model>
                                    <ext:Model ID="ModelSanction" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Sanction_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Provider" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Match_Criteria" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Link_To_Source" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Attributes" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_From" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_From_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_To" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_To_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumnSanction" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="ColumnProvider" runat="server" DataIndex="Provider" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSanction_List_Name" runat="server" DataIndex="Sanction_List_Name" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnMatch_Criteria" runat="server" DataIndex="Match_Criteria" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnLink_To_Source" runat="server" DataIndex="Link_To_Source" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSanction_List_Attributes" runat="server" DataIndex="Sanction_List_Attributes" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                               <%-- <ext:Column ID="ColumnSanction_List_Date_Range_Valid_From" runat="server" DataIndex="Sanction_List_Date_Range_Valid_From" Text="Sanction List Date Range Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSanction_List_Date_Range_Is_Approx_From_Date" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_From_Date" Text="Sanction List Date Range Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSanction_List_Date_Range_Valid_To" runat="server" DataIndex="Sanction_List_Date_Range_Valid_To" Text="Sanction List Date Range Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSanction_List_Date_Range_Is_Approx_To_Date" runat="server" DataIndex="Sanction_List_Date_Range_Is_Approx_To_Date" Text="Sanction List Date Range Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="ColumnCommentsSanction" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                
                                <ext:CommandColumn ID="CommandColumnActionSanction" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdSanction">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar16" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelRelatedPerson" runat="server" Title="Related Person" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="StoreRelatedPerson" runat="server">
                                <Model>
                                    <ext:Model ID="ModelRelatedPerson" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Related_Person_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Person_Person_Relation" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Valid_From" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Is_Approx_From_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Valid_To" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Is_Approx_To_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Gender" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Title" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="First_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Middle_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Birth_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country_Of_Birth" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Mother_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Alias" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Full_Name_Frn" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Passport_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Passport_Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nationality1" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nationality2" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nationality3" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Residence" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Residence_Since" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Occupation" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Deceased" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Date_Deceased" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tax_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Is_PEP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Source_Of_Wealth" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Is_Protected" Type="String"></ext:ModelField>
                                            
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumnRelatedPerson" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="ColumnPerson_Person_Relation" runat="server" DataIndex="Person_Person_Relation" Text="Person Person Relation" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="ColumnRelation_Date_Range_Valid_From" runat="server" DataIndex="Relation_Date_Range_Valid_From" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnRelation_Date_Range_Is_Approx_From_Date" runat="server" DataIndex="Relation_Date_Range_Is_Approx_From_Date" Text="rom Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnRelation_Date_Range_Valid_To" runat="server" DataIndex="Relation_Date_Range_Valid_To" Text="mValid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnRelation_Date_Range_Is_Approx_To_Date" runat="server" DataIndex="Relation_Date_Range_Is_Approx_To_Date" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnCommentsRelatedPerson" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnGender" runat="server" DataIndex="Gender" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnTitle" runat="server" DataIndex="Title" Text="Title" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnFirst_NameRelatedPerson" runat="server" DataIndex="First_Name" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnMiddle_Name" runat="server" DataIndex="Middle_Name" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnPrefix" runat="server" DataIndex="Prefix" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="ColumnLast_NameRelatedPerson" runat="server" DataIndex="Last_Name" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="ColumnBirth_Date" runat="server" DataIndex="Birth_Date" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnBirth_Place" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnCountry_Of_Birth" runat="server" DataIndex="Country_Of_Birth" Text="Country Of Birth" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnMother_Name" runat="server" DataIndex="Mother_Name" Text="Mother Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnAlias" runat="server" DataIndex="Alias" Text="Alias" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnFull_Name_Frn" runat="server" DataIndex="Full_Name_Frn" Text="Full Foreign Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSSN" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnPassport_Number" runat="server" DataIndex="Passport_Number" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnPassport_Country" runat="server" DataIndex="Passport_Country" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnID_Number" runat="server" DataIndex="ID_Number" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="ColumnNationality1" runat="server" DataIndex="Nationality1" Hidden="true" Text="Nationality 1" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="ColumnNationality2" runat="server" DataIndex="Nationality2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnNationality3" runat="server" DataIndex="Nationality3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnResidence" runat="server" DataIndex="Residence" Text="Residence" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnResidence_Since" runat="server" DataIndex="Residence_Since" Text="Residence Since" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnOccupation" runat="server" DataIndex="Occupation" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnDeceased" runat="server" DataIndex="Deceased" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnDate_Deceased" runat="server" DataIndex="Date_Deceased" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnTax_Number" runat="server" DataIndex="Tax_Number" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnIs_PEP" runat="server" DataIndex="Is_PEP" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnSource_Of_Wealth" runat="server" DataIndex="Source_Of_Wealth" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnIs_Protected" runat="server" DataIndex="Is_Protected" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:CommandColumn ID="CommandColumnRelatedPersonAction" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdRelatedPerson">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar22" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
            
                </Items>
            </ext:FormPanel>

            <ext:FormPanel runat="server" Title="WIC Corporate" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="WIC_Corporate">
                <Defaults>
                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                </Defaults>
                <Items>
                    <ext:DisplayField ID="TxtCorp_Name" runat="server" FieldLabel="Corporate Name" AnchorHorizontal="80%"  LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtCorp_Commercial_name" runat="server" FieldLabel="Comerical Name" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CmbCorp_Incorporation_legal_form" runat="server" FieldLabel="Legal Form" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtCorp_Incorporation_number" runat="server" FieldLabel="Corporate License Number" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtCorp_Business" runat="server" FieldLabel="Line of Business" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>

                    <ext:GridPanel ID="GridPanelPhoneCorporateDetail" runat="server" Title="Phone" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="StorePhoneCorporateDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Ref_Detail_Of" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_for_Table_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererPhoneDetailCorporate" Text="No"  Width="60" runat="server"></ext:RowNumbererColumn>
                                <ext:Column ID="colTph_Contact_TypeCorporate" runat="server" DataIndex="Contact_Type" Text="Contact Type" flex="1"></ext:Column>
                                <ext:Column ID="coltph_communication_typeCorporate" runat="server" DataIndex="Communication_Type" Text="Communication Type" flex="1"></ext:Column>
                                <ext:Column ID="coltph_numberCorporate" runat="server" DataIndex="tph_number" Text="Phone Number" flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhoneCorporateDetail" runat="server" Text="Action" Flex="1" >
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhoneCorporate">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelAddressCorporateDetail" runat="server" Title="Address" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="StoreAddressCorporateDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="FK_Ref_detail_of" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_to_Table_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailCorporate" Text="No"  Width="60" runat="server"></ext:RowNumbererColumn>
                                <ext:Column ID="ColAddress_typeCorporate" runat="server" DataIndex="Type_Address" Text="Address Type" flex="1"></ext:Column>
                                <ext:Column ID="ColAddressCorporate" runat="server" DataIndex="Address" Text="Address" flex="2"></ext:Column>
                                <ext:Column ID="ColCityCorporate" runat="server" DataIndex="City" Text="City" flex="1"></ext:Column>
                                <ext:Column ID="Colcountry_codeCorporate" runat="server" DataIndex="CountryCode" Text="Country" flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAddressCorporateDetail" runat="server" Text="Action" Flex="1" >
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddressCorporate">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    
                    <ext:GridPanel ID="GPWIC_CORP_EntityIdentification" runat="server" Title="Entity Identification" AutoScroll="true">
                        <Store>
                            <ext:Store ID="Store_WIC_Entity_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_ent_identify" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Entity_Identifications_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TYPE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUE_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EXPIRY_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUED_BY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ISSUE_COUNTRY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_identify" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:CommandColumn ID="CC_CORP_EntityIdentification" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdWIC_EntityIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Entity_Identifications_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="clm_identify_type" runat="server" DataIndex="TYPE" Text="Entity Identifier Type" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_number" runat="server" DataIndex="NUMBER" Text="Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_issued_date" runat="server" DataIndex="ISSUE_DATE" Text="Issued Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_expiry_date" runat="server" DataIndex="EXPIRY_DATE" Text="Expiry Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_issued_by" runat="server" DataIndex="ISSUED_BY" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_issued_country" runat="server" DataIndex="ISSUE_COUNTRY" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="clm_identify_comments" runat="server" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_identify" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--<ext:DisplayField ID="TxtCorp_Email" runat="server" FieldLabel="Email Korporasi" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtCorp_url" runat="server" FieldLabel="Website Korporasi" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>--%>
                    <ext:GridPanel ID="GPWIC_Corp_Email" runat="server" Title="Email" AutoScroll="true">
                        <Store>
                            <ext:Store ID="Store_WIC_Email" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="model_email" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Email_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="email_address" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_indv_email" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CC_INDV_Email" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdEmail">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Email_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="clm_indv_email_address" runat="server" DataIndex="email_address" Text="Email Address" MinWidth="130" Flex="1"></ext:Column>
                                
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_indv_email" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GPWIC_CORP_URL" runat="server" Title="Entity URL" AutoScroll="true">
                        <Store>
                            <ext:Store ID="Store_WIC_Entity_URL" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_entity_url" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_URL_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="URL" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_entity_url" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:CommandColumn ID="CC_CORP_URL" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdWIC_URL">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_URL_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="clm_url_type" runat="server" DataIndex="URL" Text="URL" MinWidth="130" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_entity_url" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:DisplayField ID="TxtCorp_incorporation_state" runat="server" FieldLabel="Province" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CmbCorp_incorporation_country_code" runat="server" FieldLabel="Country" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>

                    <ext:GridPanel ID="GP_Corp_Director" runat="server" Title="Director Corp" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="Store_Director_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_Director_Corp" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="NO_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Roles" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column31" runat="server" DataIndex="Last_Name" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column32" runat="server" DataIndex="Roles" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnDirector" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.NO_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--daniel 12 jan 2021--%>
                    <%--<ext:DisplayField ID="CmbCorp_Role" runat="server" FieldLabel="Role" AnchorHorizontal="80%"></ext:DisplayField>--%>
                    <%--end 12 jan 2021--%>
                    <ext:DisplayField ID="DateCorp_incorporation_date" runat="server" Flex="1" LabelWidth="175" FieldLabel="Date Of Established"></ext:DisplayField>
                    <ext:DisplayField ID="chkCorp_business_closed" runat="server" FieldLabel="Is Closed?" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="DateCorp_date_business_closed" runat="server" Flex="1" FieldLabel="Closed date" Hidden="true" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtCorp_tax_number" runat="server" FieldLabel="Tax Number" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtCorp_Comments" runat="server" FieldLabel="Notes" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    
                    <ext:GridPanel ID="GP_Corp_Sanction" runat="server" Title="Sanction" AutoScroll="true" EmptyText="No Available Data" Border="true">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model6" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Sanction_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Provider" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Match_Criteria" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Link_To_Source" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Attributes" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_From" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_From_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Valid_To" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Sanction_List_Date_Range_Is_Approx_To_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumn6" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdSanction">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column3" runat="server" DataIndex="Provider" Text="Provider" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column5" runat="server" DataIndex="Sanction_List_Name" Text="Sanction List Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="Match_Criteria" Text="Match Criteria" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="Link_To_Source" Text="Link To Source" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="Sanction_List_Attributes" Text="Sanction List Attributes" MinWidth="130" Flex="1"></ext:Column>
                                <%--START: 2023-10-14, Nael --%>
                                <ext:Column ID="Column23" runat="server" Hidden="true" DataIndex="Sanction_List_Date_Range_Valid_From" Text="Sanction List Date Range Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column25" runat="server" Hidden="true" DataIndex="Sanction_List_Date_Range_Is_Approx_From_Date" Text="Sanction List Date Range Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column41" runat="server" Hidden="true" DataIndex="Sanction_List_Date_Range_Valid_To" Text="Sanction List Date Range Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column42" runat="server" Hidden="true" DataIndex="Sanction_List_Date_Range_Is_Approx_To_Date" Text="Sanction List Date Range Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>
                                <%--END: 2023-10-14, Nael --%>
                                <ext:Column ID="Column43" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar13" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GP_Corp_RelatedPerson" runat="server" Hidden="true" Title="Related Person" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model7" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Related_Person_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Person_Person_Relation" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Valid_From" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Is_Approx_From_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Valid_To" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Relation_Date_Range_Is_Approx_To_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Gender" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Title" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="First_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Middle_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Birth_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Birth_Place" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country_Of_Birth" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Mother_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Alias" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Full_Name_Frn" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SSN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Passport_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Passport_Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ID_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nationality1" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nationality2" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Nationality3" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Residence" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Residence_Since" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Occupation" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Deceased" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Date_Deceased" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tax_Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Is_PEP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Source_Of_Wealth" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Is_Protected" Type="String"></ext:ModelField>
                                            
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumn7" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdRelatedPerson">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column44" runat="server" DataIndex="Person_Person_Relation" Text="Person Person Relation" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column45" runat="server" DataIndex="Relation_Date_Range_Valid_From" Text="Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column46" runat="server" DataIndex="Relation_Date_Range_Is_Approx_From_Date" Text="rom Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column47" runat="server" DataIndex="Relation_Date_Range_Valid_To" Text="mValid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column48" runat="server" DataIndex="Relation_Date_Range_Is_Approx_To_Date" Text="Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column49" runat="server" DataIndex="Comments" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column50" runat="server" DataIndex="Gender" Text="Gender" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column51" runat="server" DataIndex="Title" Text="Title" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column52" runat="server" DataIndex="First_Name" Text="First Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column53" runat="server" DataIndex="Middle_Name" Text="Middle Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column54" runat="server" DataIndex="Prefix" Text="Prefix" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column55" runat="server" DataIndex="Last_Name" Text="Last Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column56" runat="server" DataIndex="Birth_Date" Text="Birth Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column57" runat="server" DataIndex="Birth_Place" Text="Birth Place" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column58" runat="server" DataIndex="Country_Of_Birth" Text="Country Of Birth" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column59" runat="server" DataIndex="Mother_Name" Text="Mother Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column60" runat="server" DataIndex="Alias" Text="Alias" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column61" runat="server" DataIndex="Full_Name_Frn" Text="Full Foreign Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column62" runat="server" DataIndex="SSN" Text="SSN" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column63" runat="server" DataIndex="Passport_Number" Text="Passport Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column64" runat="server" DataIndex="Passport_Country" Text="Passport Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column65" runat="server" DataIndex="ID_Number" Text="ID Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column66" runat="server" DataIndex="Nationality1" Text="Nationality 1" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column67" runat="server" DataIndex="Nationality2" Text="Nationality 2" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column68" runat="server" DataIndex="Nationality3" Text="Nationality 3" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column69" runat="server" DataIndex="Residence" Text="Residence" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column70" runat="server" DataIndex="Residence_Since" Text="Residence Since" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column71" runat="server" DataIndex="Occupation" Text="Occupation" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column72" runat="server" DataIndex="Deceased" Text="Deceased" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column73" runat="server" DataIndex="Date_Deceased" Text="Date Deceased" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column74" runat="server" DataIndex="Tax_Number" Text="Tax Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column75" runat="server" DataIndex="Is_PEP" Text="Is PEP" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column76" runat="server" DataIndex="Source_Of_Wealth" Text="Source of Wealth" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column77" runat="server" DataIndex="Is_Protected" Text="Is Protected" MinWidth="130" Flex="1"></ext:Column>
                                
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- Related Entities --%>
                    <ext:GridPanel ID="GPWIC_CORP_RelatedEntities" runat="server" Title="Related Entity" AutoScroll="true">
                        <Store>
                            <ext:Store ID="Store_Related_Entities" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="mdl_related_entities" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_WIC_Related_Entities_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ENTITY_ENTITY_RELATION" Type="String"></ext:ModelField>
                                       <%--     <ext:ModelField Name="RELATION_DATE_RANGE_VALID_FROM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RELATION_DATE_RANGE_IS_APPROX_FROM_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RELATION_DATE_RANGE_VALID_TO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RELATION_DATE_RANGE_IS_APPROX_TO_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SHARE_PERCENTAGE" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="COMMENTS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="COMMERCIAL_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INCORPORATION_LEGAL_FORM" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INCORPORATION_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="BUSINESS" Type="String"></ext:ModelField>
                                      <%--      <ext:ModelField Name="ENTITY_STATUS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ENTITY_STATUS_DATE" Type="String"></ext:ModelField>--%>
                                            <ext:ModelField Name="INCORPORATION_STATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INCORPORATION_COUNTRY_CODE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="INCORPORATION_DATE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DATE_BUSINESS_CLOSED" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TAX_NUMBER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TAX_REG_NUMBER" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="rnc_related_etities" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CC_CORP_RelatedEntities" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdWIC_RelatedEntities">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_WIC_Related_Entities_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_WIC_Previous_Name_ID" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="clm_re_email_address" runat="server" DataIndex="ENTITY_ENTITY_RELATION" Text="Entity Relation" MinWidth="130" Flex="1"></ext:Column>
                             <%--   <ext:Column ID="Column84" runat="server" DataIndex="RELATION_DATE_RANGE_VALID_FROM" Text="Relation Date Range - Valid From" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column85" runat="server" DataIndex="RELATION_DATE_RANGE_IS_APPROX_FROM_DATE" Text="Relation Date Range - Is Approx From Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column86" runat="server" DataIndex="RELATION_DATE_RANGE_VALID_TO" Text="Relation Date Range - Valid To" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column87" runat="server" DataIndex="RELATION_DATE_RANGE_IS_APPROX_TO_DATE" Text="Relation Date Range - Is Approx To Date" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column88" runat="server" DataIndex="SHARE_PERCENTAGE" Text="Share Percentage" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column89" runat="server" Hidden="true" DataIndex="COMMENTS" Text="Comments" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column90" runat="server" DataIndex="NAME" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column91" runat="server" DataIndex="COMMERCIAL_NAME" Hidden="true" Text="Commercial Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column92" runat="server" DataIndex="INCORPORATION_LEGAL_FORM" Hidden="true" Text="Incorporation Legal Form" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column93" runat="server" DataIndex="INCORPORATION_NUMBER" Hidden="true" Text="License Number" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column94" runat="server" DataIndex="BUSINESS" Hidden="true" Text="Line of Business" MinWidth="130" Flex="1"></ext:Column>
                              <%--  <ext:Column ID="Column95" runat="server" DataIndex="ENTITY_STATUS" Text="Entity Status" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column96" runat="server" DataIndex="ENTITY_STATUS_DATE" Text="Entity Status Date" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:Column ID="Column97" runat="server" DataIndex="INCORPORATION_STATE" Hidden="true" Text="Province" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column98" runat="server" DataIndex="INCORPORATION_COUNTRY_CODE" Hidden="true" Text="Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column99" runat="server" DataIndex="INCORPORATION_DATE" Hidden="true" Text="Date of Established" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column100" runat="server" DataIndex="BUSINESS_CLOSED" Hidden="true" Text="Businesss Closed" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column101" runat="server" DataIndex="DATE_BUSINESS_CLOSED" Hidden="true" Text="Date Business Closed" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column102" runat="server" DataIndex="TAX_NUMBER" Text="Tax Number" Hidden="true" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column103" runat="server" DataIndex="TAX_REG_NUMBER" Text="Tax Reg Number" Hidden="true" MinWidth="130" Flex="1"></ext:Column>
                                
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pt_related_entities" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%-- End of Related Entities --%>
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btnSave" ClientIDMode="Static" runat="server" Text="Delete" Icon="DiskBlack">
                <Listeners>
                    <Click Handler="if (!#{WIC_Panel}.getForm().isValid()) return false;"></Click>
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="BtnDelete_Click">
                        <ExtraParams>
                            <ext:Parameter Name="command" Value="Delete" Mode="Value"></ext:Parameter>
                        </ExtraParams>
                        <EventMask ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo"></ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
