﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CUSTOMER_DETAIL.aspx.vb" Inherits="CUSTOMER_DETAIL" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Pop Up--%>
    <ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Phone Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSetCustomerPhoneAdd" runat="server" Title="Phones" Collapsible="true" Layout="FitLayout">
                <Items>                
                </Items>
                <Content>>
                    <ext:FormPanel ID="FormPanelTaskDetail"  runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" >
                        <Items>
                            <ext:Hidden runat="server" ID="fld_sender"></ext:Hidden>
                            <%--<ext:TextField ID="txtCet" runat="server" LabelWidth="180" FieldLabel="CET No" AnchorHorizontal="90%"></ext:TextField>--%>
                            <ext:ComboBox ID="cb_phone_Contact_Type" runat="server" FieldLabel="Kategori Kontak" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Contact_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model_Contact_Type">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                           </ext:ComboBox>
                            <ext:ComboBox ID="cb_phone_Communication_Type" runat="server" FieldLabel="Jenis Alat Komunikasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Communication_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model_Comminication_Type">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                           </ext:ComboBox>

                            <ext:TextField ID="txt_phone_Country_prefix" runat="server" FieldLabel="Kode Area Telp" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_phone_number" runat="server" FieldLabel="Nomor Telepon" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_phone_extension" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_phone_comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
<%--                            <ext:Button ID="btnSavedetail" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomerPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>--%>
                            <ext:Button ID="btnBackSaveDetail" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>

                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailAddress" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Address Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet2" runat="server" Title="Address" Collapsible="true" Layout="FitLayout">
                <Items>                
                </Items>
                <Content>
                    <ext:FormPanel ID="FP_Address_INDVD" runat="server" Hidden="true"  ButtonAlign="Center" AutoScroll="true" >
                        <Items>
                            <ext:Hidden runat="server" ID="Hidden1"></ext:Hidden>
                            <%--<ext:TextField ID="txtCet" runat="server" LabelWidth="180" FieldLabel="CET No" AnchorHorizontal="90%"></ext:TextField>--%>
                            <ext:ComboBox ID="cmb_kategoriaddress" runat="server" FieldLabel="Kategori Alamat" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_KategoriAddress">
                                        <Model>
                                            <ext:Model runat="server" ID="ModelKategoriAddress">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                           </ext:ComboBox>
                            <ext:TextField ID="Address_INDV" runat="server" FieldLabel="Alamat" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Town_INDV" runat="server" FieldLabel="Kecamatan" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="City_INDV" runat="server" FieldLabel="Kota" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Zip_INDV" runat="server" FieldLabel="Kode Pos" AnchorHorizontal="90%"></ext:TextField>
                            <ext:ComboBox ID="cmb_kodenegara" runat="server" FieldLabel="Kode Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Kode_Negara">
                                        <Model>
                                            <ext:Model runat="server" ID="ModelKodeNegara">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                           </ext:ComboBox>
                           <ext:TextField ID="State_INDVD" runat="server" FieldLabel="Provinsi" AnchorHorizontal="90%"></ext:TextField>     
                             <ext:TextField ID="Comment_Address_INDVD" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>     
                        </Items>
                        <Buttons>
                            <ext:Button ID="Btn_Back_Address" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailIdentification" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet3" runat="server" Title="Identification" Collapsible="true" Layout="FitLayout">
                <Items>                
                </Items>
                <Content>
                    <ext:FormPanel ID="FP_Identification_INDV" runat="server"  Hidden="true" ButtonAlign="Center" AutoScroll="true" >
                        <Items>
                            <ext:TextField ID="txt_number_identification" runat="server" FieldLabel="Nomor" AnchorHorizontal="90%"></ext:TextField>                            
                            <ext:ComboBox ID="cmb_Jenis_Dokumen" runat="server" FieldLabel="Jenis Dokumen Identitas" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_jenis_dokumen">
                                        <Model>
                                            <ext:Model runat="server" ID="Model3">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                </DirectEvents>
                           </ext:ComboBox>
                            <ext:ComboBox ID="cmb_issued_country" runat="server" FieldLabel="Issued Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_issued_country">
                                        <Model>
                                            <ext:Model runat="server" ID="Model4">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                           </ext:ComboBox>
                            <ext:TextField ID="txt_issued_by" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%"></ext:TextField>
                            <ext:DateField ID="txt_issued_date" runat="server" Format="dd-MMM-yyyy" FieldLabel="Issued Date" AnchorHorizontal="90%"></ext:DateField>
                            <ext:DateField ID="txt_issued_expired_date" runat="server" Format="dd-MMM-yyyy" FieldLabel="Issued Expired Date" AnchorHorizontal="90%"></ext:DateField>
                            <ext:TextField ID="txt_identification_comment" runat="server" FieldLabel="Comment" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button1" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                                        <ext:FormPanel ID="FP_Identification_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" >
                        <Items>
                            <ext:TextField ID="txt_number_identification_Director" runat="server" FieldLabel="Nomor" AnchorHorizontal="90%"></ext:TextField>                            
                            <ext:ComboBox ID="cmb_Jenis_Dokumen_Director" runat="server" FieldLabel="Jenis Dokumen Identitas" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Storejenisdokumen_Director">
                                        <Model>
                                            <ext:Model runat="server" ID="Model30">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                </DirectEvents>
                           </ext:ComboBox>
                            <ext:ComboBox ID="cmb_issued_country_Director" runat="server" FieldLabel="Issued Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreIssueCountry_Director">
                                        <Model>
                                            <ext:Model runat="server" ID="Model31">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                           </ext:ComboBox>
                            <ext:TextField ID="txt_issued_by_Director" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%"></ext:TextField>
                            <ext:DateField ID="txt_issued_date_Director" runat="server" FieldLabel="Issued Date" AnchorHorizontal="90%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:DateField ID="txt_issued_expired_date_Director" runat="server" FieldLabel="Issued Expired Date" AnchorHorizontal="90%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:TextField ID="txt_identification_comment_Director" runat="server" FieldLabel="Comment" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button8" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>


        <%--Pop Up--%>
    <ext:Window ID="WindowDetailDirector" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Director Add" Hidden="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet4" runat="server" Title="Director" Collapsible="true" Layout="FitLayout">
                <Items>                
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanelDetailDirector" runat="server" Hidden="true"  ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                            <ext:Hidden runat="server" ID="Hidden2"></ext:Hidden>
                            <%--<ext:TextField ID="txtCet" runat="server" LabelWidth="180" FieldLabel="CET No" AnchorHorizontal="90%"></ext:TextField>--%>
                           <ext:DisplayField ID="Name" runat="server" FieldLabel="Nama Lengkap"></ext:DisplayField>
                           <ext:DisplayField ID="Role" runat="server" FieldLabel="Role"></ext:DisplayField>
                           <ext:DisplayField ID="BirthPlace" runat="server" FieldLabel="Tempat Lahir"></ext:DisplayField>
                           <ext:DisplayField ID="BirthDate" runat="server" FieldLabel="Tanggal Lahir" ></ext:DisplayField>
                           <ext:DisplayField ID="MomName" runat="server" FieldLabel="Nama Ibu Kandung"></ext:DisplayField>
                           <ext:DisplayField ID="Aliase" runat="server" FieldLabel="Nama Alias"></ext:DisplayField>
                           <ext:DisplayField ID="SSN" runat="server" FieldLabel="NIK"></ext:DisplayField>
                           <ext:DisplayField ID="PassportNo" runat="server" FieldLabel="No. Passport" ></ext:DisplayField>
                           <ext:DisplayField ID="PassportFrom" runat="server" FieldLabel="Negara Penerbit Passport"></ext:DisplayField>
                           <ext:DisplayField ID="No_Identity" runat="server" FieldLabel="No. Identitas Lain" ></ext:DisplayField>
                           <ext:DisplayField ID="Nasionality1" runat="server" FieldLabel="Kewarganegaraan 1" ></ext:DisplayField>
                           <ext:DisplayField ID="Nasionality2" runat="server" FieldLabel="Kewarganegaraan 2" ></ext:DisplayField>
                           <ext:DisplayField ID="Nasionality3" runat="server" FieldLabel="Kewarganegaraan 3" ></ext:DisplayField>
                           <ext:DisplayField ID="Residence" runat="server" FieldLabel="Residence" ></ext:DisplayField>
                           <ext:DisplayField ID="Email" runat="server" FieldLabel="Email" ></ext:DisplayField>
                            <ext:Checkbox runat="server" ID="cb_Director_Deceased" FieldLabel="Deceased ?">
                                <DirectEvents>
                                    <Change OnEvent="checkBoxcb_Director_Deceased_click" />
                                </DirectEvents>
                            </ext:Checkbox>
                            <ext:DisplayField runat="server" Hidden="true" FieldLabel="Deceased Date " ID="txt_Director_Deceased_Date" AnchorHorizontal="90%"/>
                            <ext:Checkbox runat="server" ID="cb_Director_Tax_Reg_Number" FieldLabel="PEP ?">
                            </ext:Checkbox>
                            <ext:DisplayField runat="server" FieldLabel="NPWP" ID="txt_Director_Tax_Number" AnchorHorizontal="90%"/>
                            <ext:DisplayField runat="server"  FieldLabel="Source of Wealth" ID="txt_Director_Source_of_Wealth" AnchorHorizontal="90%"/>
                            <ext:DisplayField ID="Pekerjaan" runat="server" FieldLabel="Occupation" ></ext:DisplayField>
                            <ext:DisplayField runat="server" FieldLabel="Catatan" ID="txt_Director_Comments" AnchorHorizontal="90%"/>
                            <ext:DisplayField ID="txt_Director_employer_name" runat="server" FieldLabel="Tempat Bekerja" AnchorHorizontal="90%"></ext:DisplayField> 

                            <%--Director Identificiation--%>
                            <ext:GridPanel ID="GP_DirectorIdentification" runat="server" Title="Identification" AutoScroll="true">
                                <Store>
                                    <ext:Store ID="Store_Director_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model29" runat="server">
                                                <%--to do--%>
                                                <Fields>
                                                    <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Type_Document" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Issued_By" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Issue_Date" Type="Date"></ext:ModelField>
                                                    <ext:ModelField Name="Expiry_Date" Type="Date"></ext:ModelField>
                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Identification_Comment" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                        <ext:Column ID="Column75" runat="server" DataIndex="Number" Text="Nomor" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column77" runat="server" DataIndex="Type_Document" Text="Jenis Dokumen" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column78" runat="server" DataIndex="Issued_By" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:DateColumn ID="Column79" runat="server" DataIndex="Issue_Date" Text="Issued Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                        <ext:DateColumn ID="Column80" runat="server" DataIndex="Expiry_Date" Text="Expired Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                        <ext:Column ID="Column81" runat="server" DataIndex="Country" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column82" runat="server" DataIndex="Identification_Comment" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn13" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdDirectorIdentification">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--end of Director Identification--%>
                           
                             <%--Address--%>                    
                    <ext:GridPanel ID="GP_Director_Address" runat="server" Title="Address" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="StoreDirectorAddress" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="modelDirectorAddress" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column29" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column23" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column68" runat="server" DataIndex="Country" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column25" runat="server" DataIndex="State" Text="Provinsi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn56" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdAddressDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--End of Address--%>

                    <%--Phone--%>                    
                    <ext:GridPanel ID="GP_Director_Phone" runat="server" Title="Phone" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="StoreDirectorPhone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="modelDirectorPhone" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="column27" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column28" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column30" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column31" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column32" runat="server" DataIndex="tph_extension" Text="Nomor Ekstensi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column33" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn6" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdPhoneDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--End of Phone--%>

                             <%--Address--%>                    
                    <ext:GridPanel ID="GP_Director_Employer_Address" runat="server" Title="Employer Address" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="StoreDirectorEmployerAddress" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="modelDirectorEmployerAddress" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column56" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column57" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column58" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column67" runat="server" DataIndex="Country" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column59" runat="server" DataIndex="State" Text="Provinsi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column60" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn10" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdAddressDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--End of Address--%>

                    <%--Phone--%>                    
                    <ext:GridPanel ID="GP_Director_Employer_Phone" runat="server" Title="Employer Phone" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="StoreDirectorEmployerPhone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="modelDirectorEmployerPhone" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="column61" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column62" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column63" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column64" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column65" runat="server" DataIndex="tph_extension" Text="Nomor Ekstensi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column66" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn11" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdPhoneDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--End of Phone--%>

                            </Items>
                        <Buttons>
                            <ext:Button ID="Button3" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirector_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>	
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>


    <%--Pop Up--%>
    <ext:Window ID="WindowDetailDirectorPhone" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Director Phone Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSetDirectorPhoneAdd" runat="server" Title="Phones" Collapsible="true" Layout="FitLayout">
                <Items>                
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanelDirectorPhone" runat="server" Hidden="true"  ButtonAlign="Center" AutoScroll="true" >
                        <Items>
                            <ext:ComboBox ID="Director_cb_phone_Contact_Type" runat="server" FieldLabel="Kategori Kontak" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_Contact_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model21">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <ext:ComboBox ID="Director_cb_phone_Communication_Type" runat="server" FieldLabel="Jenis Alat Komunikasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_Communication_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model22">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                           </ext:ComboBox>

                            <ext:TextField ID="Director_txt_phone_Country_prefix" runat="server" FieldLabel="Kode Area Telp" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_txt_phone_number" runat="server" FieldLabel="Nomor Telepon" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_txt_phone_extension" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_txt_phone_comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnBackSaveDirectorPhoneDetail" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>

                    <ext:FormPanel ID="FormPanelEmpDirectorTaskDetail" runat="server" Hidden="true"  ButtonAlign="Center" AutoScroll="true" >
                        <Items>
                            <ext:ComboBox ID="Director_Emp_cb_phone_Contact_Type" runat="server" FieldLabel="Kategori Kontak" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Emp_Store_Contact_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model23">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                           </ext:ComboBox>
                            <ext:ComboBox ID="Director_Emp_cb_phone_Communication_Type" runat="server" FieldLabel="Jenis Alat Komunikasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Emp_Store_Communication_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model24">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                           </ext:ComboBox>

                            <ext:TextField ID="Director_Emp_txt_phone_Country_prefix" runat="server" FieldLabel="Kode Area Telp" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Emp_txt_phone_number" runat="server" FieldLabel="Nomor Telepon" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Emp_txt_phone_extension" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Emp_txt_phone_comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button13" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailDirectorAddress" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Director Address Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet1" runat="server" Title="Address" Collapsible="true" Layout="FitLayout">
                <Items>                
                </Items>
                <Content>
                    <ext:FormPanel ID="FP_Address_Director" runat="server"  Hidden="true" ButtonAlign="Center" AutoScroll="true" >
                        <Items>
                            <ext:ComboBox ID="Director_cmb_kategoriaddress" runat="server" FieldLabel="Kategori Alamat" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_KategoriAddress">
                                        <Model>
                                            <ext:Model runat="server" ID="Model25">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                           </ext:ComboBox>
                            <ext:TextField ID="Director_Address" runat="server" FieldLabel="Alamat" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Town" runat="server" FieldLabel="Kecamatan" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_City" runat="server" FieldLabel="Kota" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Zip" runat="server" FieldLabel="Kode Pos" AnchorHorizontal="90%"></ext:TextField>
                            <ext:ComboBox ID="Director_cmb_kodenegara" runat="server" FieldLabel="Kode Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_Kode_Negara">
                                        <Model>
                                            <ext:Model runat="server" ID="Model26">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                           </ext:ComboBox>
                           <ext:TextField ID="Director_State" runat="server" FieldLabel="Provinsi" AnchorHorizontal="90%"></ext:TextField>     
                             <ext:TextField ID="Director_Comment_Address" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>     
                        </Items>
                        <Buttons>
                            <ext:Button ID="Btn_Back_Director_Address" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                    <ext:FormPanel ID="FP_Address_Emp_Director" runat="server"  Hidden="true" ButtonAlign="Center" AutoScroll="true" >
                        <Items>
                            <ext:ComboBox ID="Director_cmb_emp_kategoriaddress" runat="server" FieldLabel="Kategori Alamat" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_emp_KategoriAddress">
                                        <Model>
                                            <ext:Model runat="server" ID="Model27">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                           </ext:ComboBox>
                            <ext:TextField ID="Director_emp_Address" runat="server" FieldLabel="Alamat" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_Town" runat="server" FieldLabel="Kecamatan" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_City" runat="server" FieldLabel="Kota" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_Zip" runat="server" FieldLabel="Kode Pos" AnchorHorizontal="90%"></ext:TextField>
                            <ext:ComboBox ID="Director_cmb_emp_kodenegara" runat="server" FieldLabel="Kode Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_emp_Kode_Negara">
                                        <Model>
                                            <ext:Model runat="server" ID="Model28">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                           </ext:ComboBox>
                           <ext:TextField ID="Director_emp_State" runat="server" FieldLabel="Provinsi" AnchorHorizontal="90%"></ext:TextField>     
                             <ext:TextField ID="Director_emp_Comment_Address" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>     
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button15" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <ext:FormPanel ID="FormPanelInput" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="Customer Detail">
        <Items>
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="CIF" ID="txt_CIF" />
            <%--Dedy added 25082020--%>
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="GCN" ID="txt_GCN" />
            <ext:Checkbox runat="server" LabelWidth="250" ID="GCNPrimary" FieldLabel="GCN Primary ?" ReadOnly="true">
            </ext:Checkbox>
             <ext:DateField ID="TextFieldOpeningDate" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Opening Date"  AnchorHorizontal="70%"  Format="dd-MMM-yyyy" ReadOnly="true"></ext:DateField>
            <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                  <content>
                        <nds:NDSDropDownField ID="cmb_openingBranch" ValueField="FK_AML_BRANCH_CODE" DisplayField="BRANCH_NAME" runat="server" StringField="FK_AML_BRANCH_CODE,BRANCH_NAME"  StringTable="AML_BRANCH" Label="Opening Branch Code" AnchorHorizontal="70%" AllowBlank="true"  LabelWidth="250"  IsReadOnly="true"/>
                        <nds:NDSDropDownField ID="cmb_Status" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan"  StringTable="goAML_Ref_Status_Rekening" Label="Status" AnchorHorizontal="70%" AllowBlank="True" LabelWidth="250"  IsReadOnly="true" />
             </content>
            </ext:Panel> 
            <ext:DateField ID="TextFieldClosingDate" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Closing Date" AnchorHorizontal="70%"  Format="dd-MMM-yyyy" ReadOnly="true"></ext:DateField>


            <%--Dedy end added 25082020--%>
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Status Rekening" ID="txt_statusrekening" />
            <ext:Checkbox runat="server" LabelWidth="250" ID="UpdateFromDataSource" FieldLabel="Update From Data Source ?" ReadOnly="true">
            </ext:Checkbox>
            <%--bikin panel buat Individu ama Korporasi--%>
            
            <ext:Panel runat="server" Title="Data Individu" Collapsible="true" AnchorHorizontal="100%" ID="panel_INDV" Border="true" BodyStyle="padding:10px">
                <Items>
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Gelar" ID="txt_INDV_Title" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Nama Lengkap" ID="txt_INDV_Last_Name" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Jenis Kelamin" ID="txt_INDV_Gender" ReadOnly="true"/>
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Tanggal Lahir" ID="txt_INDV_Birthdate" ReadOnly="true"/>
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Tempat Lahir" ID="txt_INDV_Birth_place" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Nama Ibu Kandung" ID="txt_INDV_Mothers_name" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Nama Alias" ID="txt_INDV_Alias" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="NIK" ID="txt_INDV_SSN" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="No. Passport" ID="txt_INDV_Passport_number" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Negara Penerbit Passport" ID="txt_INDV_Passport_country" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="No. Identitas Lain" ID="txt_INDV_ID_Number" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 1" ID="txt_INDV_Nationality1" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 2" ID="txt_INDV_Nationality2" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 3" ID="txt_INDV_Nationality3" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Negara Domisili" ID="txt_INDV_Residence" />
                 
                   <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Email" ID="txt_INDV_Email" />
                   <ext:DisplayField ID="txt_INDV_Email2" LabelWidth="250" runat="server" FieldLabel="Email 2" ></ext:DisplayField>
                   <ext:DisplayField ID="txt_INDV_Email3" LabelWidth="250" runat="server" FieldLabel="Email 3" ></ext:DisplayField>
                   <ext:DisplayField ID="txt_INDV_Email4" LabelWidth="250" runat="server" FieldLabel="Email 4" ></ext:DisplayField>
                   <ext:DisplayField ID="txt_INDV_Email5" LabelWidth="250" runat="server" FieldLabel="Email 5" ></ext:DisplayField>  
                    <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Deceased" FieldLabel="Deceased ?" AnchorHorizontal="90%" ReadOnly="true"></ext:Checkbox>
                    <ext:Displayfield runat="server" LabelWidth="250" Hidden="true" FieldLabel="Deceased Date " ID="txt_Deceased_Date" AnchorHorizontal="90%"/>
                    <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Tax" FieldLabel="PEP ?" ReadOnly="true"></ext:Checkbox>
                    <ext:displayfield runat="server" LabelWidth="250" FieldLabel="NPWP" ID="txt_INDV_Tax_Number" />
                    <ext:displayfield runat="server" LabelWidth="250" FieldLabel="Source of Wealth" ID="txt_INDV_Source_of_Wealth" />
                        
                   <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Occupation" ID="txt_INDV_Occupation" />
                   <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tempat Bekerja" ID="txt_INDV_Employer_Name" />          
                <ext:GridPanel ID="gp_INDV_Phones" runat="server" Title="Informasi Telepon" AutoScroll="true">
                <Store>
                    <ext:Store ID="Store_INDV_Phones" runat="server" IsPagingStore="true" PageSize="8">
                        <Model>
                            <ext:Model ID="Model_INDV_Phones" runat="server">
                                <Fields>
                                    <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumber_INDV_Phones" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                        <ext:Column ID="coltph_contact_type" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="coltph_communication_type" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="coltph_country_prefix" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="coltph_number" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Coltph_extension" runat="server" DataIndex="tph_extension" Text="Nomor Extensi" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="ColComments" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                            <Commands>
                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                <%--<ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>--%>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GrdCmdCustPhone">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                    </Confirmation>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

             <%--INDVD Address--%>
            <ext:GridPanel ID="GP_INDVD_Address" runat="server" hidden="true" Title="Informasi Address" AutoScroll="true">
                <Store>
                    <ext:Store ID="Store_INDVD_Address" runat="server" IsPagingStore="true" PageSize="8">
                        <Model>
                            <ext:Model ID="Model_INDVD_Address" runat="server" IDProperty="PK_Customer_Address_ID">
                                <%--to do--%>
                                <Fields>
                                    <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                        <ext:Column ID="Column1" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column2" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column3" runat="server" DataIndex="Town" Text="Kecamatan" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column4" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column5" runat="server" DataIndex="Zip" Text="Kode Pos" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column6" runat="server" DataIndex="Country" Text="Kode Negara" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column7" runat="server" DataIndex="State" Text="Provinsi" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column8" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                            <Commands>
                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GrdCmdCustAddress">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                    </Confirmation>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            <%--end of INDVD Address--%> 

            <%--INDVD Identification--%> 
            <ext:GridPanel ID="GP_INDVD_Identification" runat="server" Title="Identification" AutoScroll="true">
                <Store>
                    <ext:Store ID="Store_INDV_Identification" runat="server" IsPagingStore="true" PageSize="8">
                        <Model>
                            <ext:Model ID="Model_INDV_Identification" runat="server">
                                <%--to do--%>
                                <Fields>
                                    <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Type_Document" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Issued_By" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Issue_Date" Type="Date"></ext:ModelField>
                                    <ext:ModelField Name="Expiry_Date" Type="Date"></ext:ModelField>
                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Identification_Comment" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                        <ext:Column ID="Column34" runat="server" DataIndex="Number" Text="Nomor" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column36" runat="server" DataIndex="Type_Document" Text="Jenis Dokumen" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column37" runat="server" DataIndex="Issued_By" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                        <ext:DateColumn ID="Column38" runat="server" DataIndex="Issue_Date" Text="Issued Date" MinWidth="130" Flex="1"></ext:DateColumn>
                        <ext:DateColumn ID="Column39" runat="server" DataIndex="Expiry_Date" Text="Expired Date" MinWidth="130" Flex="1"></ext:DateColumn>
                        <ext:Column ID="Column40" runat="server" DataIndex="Country" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column41" runat="server" DataIndex="Identification_Comment" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="CommandColumn7" runat="server" Text="Action" Flex="1" MinWidth="200">
                            <Commands>
                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GrdCmdCustIdentification">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                    </Confirmation>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            <%--end of INDV Identification--%>

                    <ext:GridPanel ID="GP_Empoloyer_Phone" runat="server" Title="Informasi Telepon Tempat Kerja" AutoScroll="true">
                    
                    <Store>
                        <ext:Store ID="Store_Empoloyer_Phone" runat="server" IsPagingStore="true" PageSize="8">
                            <Model>
                                <ext:Model ID="Model5" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                        <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                        <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                        <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                        <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                        <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                        <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                            <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                            <ext:Column ID="Column52" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                            <ext:Column ID="Column53" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                            <ext:Column ID="Column42" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                            <ext:Column ID="Column43" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                            <ext:Column ID="Column44" runat="server" DataIndex="tph_extension" Text="Nomor Extensi" MinWidth="130" Flex="1"></ext:Column>
                            <ext:Column ID="Column45" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                            <ext:CommandColumn ID="CommandColumn8" runat="server" Text="Action" Flex="1" MinWidth="200">
                                <Commands>
                                    <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="GrdCmdEmployerPhone">
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                        </Confirmation>
                                    </Command>
                                </DirectEvents>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <BottomBar>
                        <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                    </BottomBar>
                </ext:GridPanel>

                    <%--Employer Address--%>
                    <ext:GridPanel ID="GP_Employer_Address" runat="server" Title="Informasi Address Tempat Kerja" AutoScroll="true">
                        <Store>
                            <ext:Store ID="Store_Employer_Address" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model8" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column54" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column46" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column47" runat="server" DataIndex="Town" Text="Kecamatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column48" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column49" runat="server" DataIndex="Zip" Text="Kode Pos" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column50" runat="server" DataIndex="State" Text="Provinsi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column55" runat="server" DataIndex="Country" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column51" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn9" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdEmployerAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end of Employer Address--%>
                </Items>
            </ext:Panel>



            <ext:Panel runat="server" Title="Data Korporasi" Collapsible="true" AnchorHorizontal="100%" ID="panel_Corp">
                <Items>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Nama Korporasi" ID="txt_Corp_Name" PaddingSpec="0 10 0 0" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Nama Komersial" ID="txt_Corp_Commercial_Name" />
                    
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Bentuk Korporasi" ID="txt_Corp_Incorporation_Legal_Form" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Nomor Induk Berusaha" ID="txt_Corp_Incorporation_number" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Bidang Usaha" ID="txt_Corp_Business" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Email Korporasi" ID="txt_Corp_Email" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Website Korporasi" ID="txt_Corp_url" /> 
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Provinsi" ID="txt_Corp_incorporation_state" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Negara" ID="txt_Corp_Incorporation_Country_Code" />
<%--                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Peran" ID="txt_Corp_Role" />--%>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tanggal Pendirian" ID="txt_Corp_incorporation_date" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tutup?" ID="cbTutup" Selectable="false"></ext:DisplayField>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tanggal Tutup" ID="txt_Corp_date_business_closed" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="NPWP" ID="txt_Corp_tax_number" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Catatan" ID="txt_Corp_Comments" />
                </Items>
            </ext:Panel>

            <ext:GridPanel ID="GP_Phone_Corp" runat="server" hidden="true" Title="Informasi Telepon" AutoScroll="true">
                <Store>
                    <ext:Store ID="Store_Phone_Corp" runat="server" IsPagingStore="true" PageSize="8">
                        <Model>
                            <ext:Model ID="Model_Phone_Corp" runat="server">
                                <Fields>
                                    <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                        <ext:Column ID="Column9" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column10" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column11" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column12" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column13" runat="server" DataIndex="tph_extension" Text="Nomor Extensi" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column14" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                            <Commands>
                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GrdCmdCustPhone">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                    </Confirmation>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
             <%--Corp Address--%>
            <ext:GridPanel ID="GP_Address_Corp" runat="server" hidden="true" Title="Informasi Address" AutoScroll="true">
                <Store>
                    <ext:Store ID="Store_Address_Corp" runat="server" IsPagingStore="true" PageSize="8">
                        <Model>
                            <ext:Model ID="Model_Address_Corp" runat="server">
                                <%--to do--%>
                                <Fields>
                                    <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                        <ext:Column ID="Column15" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column16" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column17" runat="server" DataIndex="Town" Text="Kecamatan" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column18" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column19" runat="server" DataIndex="Zip" Text="Kode Pos" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column20" runat="server" DataIndex="Country" Text="Kode Negara" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column21" runat="server" DataIndex="State" Text="Provinsi" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column22" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="CommandColumn4" runat="server" Text="Action" Flex="1" MinWidth="200">
                            <Commands>
                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GrdCmdCustAddress">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                    </Confirmation>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            <%--end of Corp Address--%>
            
                                <%--Director--%>
                    <ext:GridPanel ID="gp_Director" runat="server" Title="Director" AutoScroll="true" Border="true" hidden="true" BodyStyle="padding:10px">
                        <%--<TopBar>
                            <ext:Toolbar ID="tb_Director" runat="server">
                                <Items>
                                    <ext:Button ID="Button1" runat="server" Text="Add Signatory" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddSignatory_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>--%>
                        <Store>
                            <ext:Store ID="store_Director" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="model_Director" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Entity_Director_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Role" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumber_Director" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="columnDirectorName" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnDirectorRole" runat="server" DataIndex="Role" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                <%--<ext:Column ID="Column4" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column5" runat="server" DataIndex="tph_extension" Text="Nomor Extensi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>--%>
                                <ext:CommandColumn ID="cmdDirectory" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <%--<ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>--%>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--End of Director--%>

         <%--   <ext:Panel runat="server" Title="Notes History" Collapsible="true" AnchorHorizontal="100%">
                <Items>
                    <ext:GridPanel ID="gp_history" runat="server">
                        <Store>
                            <ext:Store ID="gpStore_history" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" IDProperty="CIF">
                                        <Fields>
                                            <ext:ModelField Name="PK_TX_IFTI_Flow_History_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Module_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UnikID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="UserID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UserName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="RoleName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Notes" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>

                        <ColumnModel runat="server">
                            <Columns>
                                <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="70"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="User Name" DataIndex="UserName" CellWrap="true" flex="1" />
                                <ext:DateColumn runat="server" Text="Created Date" DataIndex="CreatedDate" CellWrap="true" flex="1" Format="dd-MMM-yyyy" />
                                <ext:Column runat="server" Text="Status" DataIndex="Status" CellWrap="true" Flex="1" />
                                <ext:Column runat="server" Text="Notes" DataIndex="Notes" CellWrap="true" flex="4" />
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>--%>
        </Items>

        <Buttons>

            <ext:Button ID="btnCancelIncoming" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnBack_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

</asp:Content>

