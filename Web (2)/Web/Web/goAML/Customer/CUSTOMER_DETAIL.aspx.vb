﻿Imports System.Data
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL
Partial Class CUSTOMER_DETAIL
    Inherits ParentPage
    Public objFormModuleView As NawaBLL.FormModuleDetail

    Private mModuleName As String

    Public Property ModulName() As String
        Get
            Return mModuleName
        End Get
        Set(ByVal value As String)
            mModuleName = value
        End Set
    End Property
    Sub ClearSession()
        objListgoAML_Ref_Director_Phone = New List(Of Vw_Director_Phones)
        objListgoAML_Ref_Director_Address = New List(Of Vw_Director_Addresses)
        objListgoAML_Ref_Director_Employer_Phone = New List(Of Vw_Director_Employer_Phones)
        objListgoAML_Ref_Director_Employer_Address = New List(Of Vw_Director_Employer_Addresses)
        objListgoAML_Ref_Identification_Director = New List(Of goAML_Person_Identification)
        objListgoAML_vw_Customer_Identification_Director = New List(Of Vw_Person_Identification)
        objListgoAML_Ref_Phone = Nothing
        objListgoAML_Ref_Address = Nothing
    End Sub

    Public Property objListgoAML_Ref_Address() As List(Of NawaDevDAL.Vw_Customer_Addresses)
        Get
            Return Session("CUSTOMER_DETAIL.objListgoAML_Ref_Address")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Customer_Addresses))
            Session("CUSTOMER_DETAIL.objListgoAML_Ref_Address") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Phone() As List(Of NawaDevDAL.Vw_Customer_Phones)
        Get
            Return Session("CUSTOMER_DETAIL.objListgoAML_Ref_Phone")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Customer_Phones))
            Session("CUSTOMER_DETAIL.objListgoAML_Ref_Phone") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Address() As List(Of NawaDevDAL.Vw_Director_Addresses)
        Get
            Return Session("CUSTOMER_DETAIL.objListgoAML_Ref_Director_Address")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Director_Addresses))
            Session("CUSTOMER_DETAIL.objListgoAML_Ref_Director_Address") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Phone() As List(Of NawaDevDAL.Vw_Director_Phones)
        Get
            Return Session("CUSTOMER_DETAIL.objListgoAML_Ref_Director_Phone")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Director_Phones))
            Session("CUSTOMER_DETAIL.objListgoAML_Ref_Director_Phone") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Employer_Address() As List(Of NawaDevDAL.Vw_Director_Employer_Addresses)
        Get
            Return Session("CUSTOMER_DETAIL.objListgoAML_Ref_Director_Employer_Address")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Director_Employer_Addresses))
            Session("CUSTOMER_DETAIL.objListgoAML_Ref_Director_Employer_Address") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Employer_Phone() As List(Of NawaDevDAL.Vw_Director_Employer_Phones)
        Get
            Return Session("CUSTOMER_DETAIL.objListgoAML_Ref_Director_Employer_Phone")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Director_Employer_Phones))
            Session("CUSTOMER_DETAIL.objListgoAML_Ref_Director_Employer_Phone") = value
        End Set
    End Property

    Public Property objTempCustomerAddresslEdit() As NawaDevDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_DETAIL.objTempCustomerAddresslEdit")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Address)
            Session("CUSTOMER_DETAIL.objTempCustomerAddresslEdit") = value
        End Set
    End Property

    Public Property objTempCustomerPhoneEdit() As NawaDevDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_DETAIL.objTempCustomerPhoneEdit")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Phone)
            Session("CUSTOMER_DETAIL.objTempCustomerPhoneEdit") = value
        End Set
    End Property

    Public Property obj_Customer As NawaDevDAL.goAML_Ref_Customer
        Get
            Return Session("CUSTOMER_DETAIL.obj_Customer")
        End Get
        Set(value As NawaDevDAL.goAML_Ref_Customer)
            Session("CUSTOMER_DETAIL.obj_Customer") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Identification_Director() As List(Of NawaDevDAL.goAML_Person_Identification)
        Get
            Return Session("CUSTOMER_DETAIL.objListgoAML_Ref_Identification_Director")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Person_Identification))
            Session("CUSTOMER_DETAIL.objListgoAML_Ref_Identification_Director") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Identification_Director() As List(Of NawaDevDAL.Vw_Person_Identification)
        Get
            Return Session("CUSTOMER_DETAIL.objListgoAML_vw_Customer_Identification_Director")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Person_Identification))
            Session("CUSTOMER_DETAIL.objListgoAML_vw_Customer_Identification_Director") = value
        End Set
    End Property


    Protected Sub BtnBack_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Private Function GetId() As String
        Dim strid As String = Request.Params("ID")
        Dim id As String = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
        Return id
    End Function

    Sub ClearinputCustomerPhones()

        txt_phone_Country_prefix.Text = ""
        txt_phone_number.Text = ""
        txt_phone_extension.Text = ""

    End Sub

    Private Sub CUSTOMER_DETAIL_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            'Dedy added 31082020 aktif is hidden
            txt_statusrekening.Hidden = True
            'Dedy end added 31082020
            ClearSession()
            GridPanelSetting()
            Maximizeable()
            Dim strid As String = GetId()
            Dim strModuleid As String = Request.Params("ModuleID")
            Dim INDV_gender As NawaDevDAL.goAML_Ref_Jenis_Kelamin = Nothing
            Dim INDV_statusCode As NawaDevDAL.goAML_Ref_Status_Rekening = Nothing
            Dim INDV_nationality1 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_nationality2 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_nationality3 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_residence As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
            Dim CORP_Incorporation_legal_form As NawaDevDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
            Dim CORP_incorporation_country_code As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
            Dim CORP_Role As NawaDevDAL.goAML_Ref_Party_Role = Nothing

            Try
                obj_Customer = goAML_CustomerBLL.GetCustomerbyCIF(strid)

                'Dedy added 01092020
                If obj_Customer IsNot Nothing Then

                    txt_CIF.Text = obj_Customer.CIF
                    txt_GCN.Text = obj_Customer.GCN
                    If obj_Customer.isGCNPrimary = True Then
                        GCNPrimary.Checked = True
                    End If
                    If obj_Customer.Closing_Date IsNot Nothing Then
                        TextFieldClosingDate.Text = obj_Customer.Closing_Date.Value.ToString("dd-MMM-yyyy")
                    End If
                    If obj_Customer.Opening_Date IsNot Nothing Then
                        TextFieldOpeningDate.Text = obj_Customer.Opening_Date.Value.ToString("dd-MMM-yyyy")
                    End If


                    Using objdbs As New NawaDevDAL.NawaDatadevEntities
                        If obj_Customer.Opening_Branch_Code IsNot Nothing And Not String.IsNullOrEmpty(obj_Customer.Opening_Branch_Code) Then
                            Dim objbranch = objdbs.AML_BRANCH.Where(Function(x) x.FK_AML_BRANCH_CODE = obj_Customer.Opening_Branch_Code).FirstOrDefault
                            If Not objbranch Is Nothing Then
                                cmb_openingBranch.SetTextWithTextValue(objbranch.FK_AML_BRANCH_CODE, objbranch.BRANCH_NAME)
                            End If
                        End If

                        If obj_Customer.Status IsNot Nothing And Not String.IsNullOrEmpty(obj_Customer.Status) Then
                            Dim objstatus = objdbs.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = obj_Customer.Status).FirstOrDefault
                            If Not objstatus Is Nothing Then
                                cmb_Status.SetTextWithTextValue(objstatus.Kode, objstatus.Keterangan)
                            End If
                        End If

                    End Using


                        If obj_Customer.isUpdateFromDataSource = True Then
                            UpdateFromDataSource.Checked = True
                        End If
                        Dim statusrekening = goAML_CustomerBLL.GetStatusRekeningbyID(obj_Customer.Status_Code)
                        'If txt_statusrekening IsNot Nothing Then
                        '    txt_statusrekening.Text = statusrekening.Keterangan
                        'End If

                        'fix object reference set to null saad 20102020
                        If statusrekening IsNot Nothing Then
                            txt_statusrekening.Text = statusrekening.Keterangan
                        End If

                        If obj_Customer.FK_Customer_Type_ID = 1 Then
                            txt_INDV_Title.Text = obj_Customer.INDV_Title

                            INDV_gender = goAML_CustomerBLL.GetJenisKelamin(obj_Customer.INDV_Gender)
                            If INDV_gender IsNot Nothing Then
                                txt_INDV_Gender.Text = INDV_gender.Keterangan
                            End If
                            txt_INDV_Last_Name.Text = obj_Customer.INDV_Last_Name
                            If obj_Customer.INDV_BirthDate.HasValue Then
                                txt_INDV_Birthdate.Text = obj_Customer.INDV_BirthDate.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                            Else
                                txt_INDV_Birthdate.Text = ""
                            End If
                            txt_INDV_Birth_place.Text = obj_Customer.INDV_Birth_Place
                            txt_INDV_Mothers_name.Text = obj_Customer.INDV_Mothers_Name
                            txt_INDV_Alias.Text = obj_Customer.INDV_Alias
                            txt_INDV_SSN.Text = obj_Customer.INDV_SSN
                            txt_INDV_Passport_number.Text = obj_Customer.INDV_Passport_Number
                            If Not String.IsNullOrEmpty(obj_Customer.INDV_Passport_Country) Then
                                txt_INDV_Passport_country.Text = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Passport_Country).Keterangan
                            Else
                                txt_INDV_Passport_country.Text = ""
                            End If
                            txt_INDV_ID_Number.Text = obj_Customer.INDV_ID_Number

                            INDV_nationality1 = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Nationality1)
                            If INDV_nationality1 IsNot Nothing Then
                                txt_INDV_Nationality1.Text = INDV_nationality1.Keterangan
                            End If

                            INDV_nationality2 = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Nationality2)
                            If INDV_nationality2 IsNot Nothing Then
                                txt_INDV_Nationality2.Text = INDV_nationality2.Keterangan
                            End If

                            INDV_nationality3 = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Nationality3)
                            If INDV_nationality3 IsNot Nothing Then
                                txt_INDV_Nationality3.Text = INDV_nationality3.Keterangan
                            End If

                            INDV_residence = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.INDV_Residence)
                            If INDV_residence IsNot Nothing Then
                                txt_INDV_Residence.Text = INDV_residence.Keterangan
                            End If

                            If obj_Customer.INDV_Email Is Nothing Then
                                txt_INDV_Email.Text = obj_Customer.INDV_Email
                            End If

                            If obj_Customer.INDV_Email2 Is Nothing Then
                                txt_INDV_Email2.Text = obj_Customer.INDV_Email2
                            End If


                            txt_INDV_Email3.Text = obj_Customer.INDV_Email3
                            txt_INDV_Email4.Text = obj_Customer.INDV_Email4
                            txt_INDV_Email5.Text = obj_Customer.INDV_Email5
                            txt_INDV_Tax_Number.Text = obj_Customer.INDV_Tax_Number
                            txt_INDV_Source_of_Wealth.Text = obj_Customer.INDV_Source_of_Wealth

                            If obj_Customer.INDV_Deceased = True Then
                                cb_Deceased.Checked = True
                                txt_Deceased_Date.Hidden = False
                                txt_Deceased_Date.Text = obj_Customer.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                            End If
                            If obj_Customer.INDV_Tax_Reg_Number = True Then
                                cb_Tax.Checked = True
                            End If
                            txt_INDV_Occupation.Text = obj_Customer.INDV_Occupation
                            txt_INDV_Employer_Name.Text = obj_Customer.INDV_Employer_Name

                            '' Phone
                            objListgoAML_Ref_Phone = NawaDevBLL.goAML_CustomerBLL.GetVWCustomerPhones(obj_Customer.PK_Customer_ID)
                            Store_INDV_Phones.DataSource = objListgoAML_Ref_Phone.ToList()
                            Store_INDV_Phones.DataBind()

                            '' Address
                            Dim objlist_address = NawaDevBLL.goAML_CustomerBLL.GetVWCustomerAddresses(obj_Customer.PK_Customer_ID)
                            Store_INDVD_Address.DataSource = objlist_address
                            Store_INDVD_Address.DataBind()

                            '' Identification
                            Dim objlist_Identification = NawaDevBLL.goAML_CustomerBLL.GetVWCustomerIdentifications(obj_Customer.PK_Customer_ID)
                            Store_INDV_Identification.DataSource = objlist_Identification
                            Store_INDV_Identification.DataBind()

                            '' Employer Phone
                            Store_Empoloyer_Phone.DataSource = NawaDevBLL.goAML_CustomerBLL.GetVWEmployerPhones(obj_Customer.PK_Customer_ID).ToList()
                            Store_Empoloyer_Phone.DataBind()

                            '' Employer Address
                            Store_Employer_Address.DataSource = NawaDevBLL.goAML_CustomerBLL.GetVWEmployerAddresses(obj_Customer.PK_Customer_ID).ToList()
                            Store_Employer_Address.DataBind()

                            panel_Corp.Visible = False
                            GP_INDVD_Address.Hidden = False
                            'panel_INDV.Visible = False
                        ElseIf obj_Customer.FK_Customer_Type_ID = 2 Then
                            txt_Corp_Name.Text = obj_Customer.Corp_Name
                            txt_Corp_Commercial_Name.Text = obj_Customer.Corp_Commercial_Name


                            CORP_Incorporation_legal_form = goAML_CustomerBLL.GetBentukBadanUsahaByID(obj_Customer.Corp_Incorporation_Legal_Form)
                            If CORP_Incorporation_legal_form IsNot Nothing Then
                                txt_Corp_Incorporation_Legal_Form.Text = CORP_Incorporation_legal_form.Keterangan
                            Else
                                txt_Corp_Incorporation_Legal_Form.Text = ""
                            End If


                            txt_Corp_Incorporation_number.Text = obj_Customer.Corp_Incorporation_Number
                            txt_Corp_Business.Text = obj_Customer.Corp_Business
                            txt_Corp_Email.Text = obj_Customer.Corp_Email
                            txt_Corp_url.Text = obj_Customer.Corp_Url
                            txt_Corp_incorporation_state.Text = obj_Customer.Corp_Incorporation_State

                            'Dedy Remarks 27082020 is nothing
                            'CORP_incorporation_country_code = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.Corp_Incorporation_Country_Code)
                            'txt_Corp_Incorporation_Country_Code.Text = CORP_incorporation_country_code.Keterangan
                            'Dedy End Remarks 27082020

                            'Dedy Added 27082020 for is nothing
                            CORP_incorporation_country_code = goAML_CustomerBLL.GetNamaNegaraByID(obj_Customer.Corp_Incorporation_Country_Code)
                            If (CORP_incorporation_country_code Is Nothing) Then
                                txt_Corp_Incorporation_Country_Code.Text = CORP_incorporation_country_code.Keterangan
                            End If
                            'Dedy End Added 27082020 for is nothing


                            'If Not obj_Customer.Corp_Role Is Nothing And Not obj_Customer.Corp_Role = "" Then
                            '    CORP_Role = goAML_CustomerBLL.GetPartyRolebyID(obj_Customer.Corp_Role)
                            '    txt_Corp_Role.Text = CORP_Role.Keterangan
                            'Else
                            '    txt_Corp_Role.Text = ""
                            'End If

                            If obj_Customer.Corp_Incorporation_Date.HasValue Then
                                txt_Corp_incorporation_date.Text = obj_Customer.Corp_Incorporation_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                            Else
                                txt_Corp_incorporation_date.Text = ""
                            End If
                            If Not obj_Customer.Corp_Business_Closed = "False" Then
                                cbTutup.Text = "Ya"
                            Else
                                cbTutup.Text = "Tidak"
                            End If
                            If obj_Customer.Corp_Date_Business_Closed.HasValue Then
                                txt_Corp_date_business_closed.Text = obj_Customer.Corp_Date_Business_Closed.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                            Else
                                txt_Corp_date_business_closed.Text = ""
                                txt_Corp_date_business_closed.Hidden = True
                            End If
                            txt_Corp_tax_number.Text = obj_Customer.Corp_Tax_Number
                            txt_Corp_Comments.Text = obj_Customer.Corp_Comments

                            GP_Phone_Corp.Hidden = False
                            GP_Address_Corp.Hidden = False

                            '' Phone
                            objListgoAML_Ref_Phone = goAML_CustomerBLL.GetVWCustomerPhones(obj_Customer.PK_Customer_ID)
                            Store_Phone_Corp.DataSource = objListgoAML_Ref_Phone
                            Store_Phone_Corp.DataBind()

                            '' Address
                            Dim obj_list_address_Corp = goAML_CustomerBLL.GetVWCustomerAddresses(obj_Customer.PK_Customer_ID)
                            Store_Address_Corp.DataSource = obj_list_address_Corp
                            Store_Address_Corp.DataBind()

                            gp_Director.Hidden = False
                            ''director
                            store_Director.DataSource = goAML_CustomerBLL.GetVWCustomerDirector(obj_Customer.CIF)
                            store_Director.DataBind()

                            For Each item As Vw_Customer_Director In goAML_CustomerBLL.GetVWCustomerDirector(obj_Customer.CIF)
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As Vw_Director_Addresses In NawaDevBLL.goAML_CustomerBLL.GetVWDirectorAddresses(pk_director)
                                    objListgoAML_Ref_Director_Address.Add(itemx)
                                Next
                            Next

                            For Each item As Vw_Customer_Director In goAML_CustomerBLL.GetVWCustomerDirector(obj_Customer.CIF)
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As Vw_Director_Phones In NawaDevBLL.goAML_CustomerBLL.GetVWDirectorPhones(pk_director)
                                    objListgoAML_Ref_Director_Phone.Add(itemx)
                                Next
                            Next

                            For Each item As Vw_Customer_Director In goAML_CustomerBLL.GetVWCustomerDirector(obj_Customer.CIF)
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As Vw_Director_Employer_Addresses In NawaDevBLL.goAML_CustomerBLL.GetVWDirectorEmployerAddresses(pk_director)
                                    objListgoAML_Ref_Director_Employer_Address.Add(itemx)
                                Next
                            Next

                            For Each item As Vw_Customer_Director In goAML_CustomerBLL.GetVWCustomerDirector(obj_Customer.CIF)
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As Vw_Director_Employer_Phones In NawaDevBLL.goAML_CustomerBLL.GetVWDirectorEmployerPhones(pk_director)
                                    objListgoAML_Ref_Director_Employer_Phone.Add(itemx)
                                Next
                            Next

                            For Each item As Vw_Customer_Director In goAML_CustomerBLL.GetVWCustomerDirector(obj_Customer.CIF)
                                Dim pk_director = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                For Each itemx As Vw_Person_Identification In NawaDevBLL.goAML_CustomerBLL.GetVWDirectorIdentifications(pk_director)
                                    objListgoAML_vw_Customer_Identification_Director.Add(itemx)
                                Next
                            Next

                            panel_INDV.Visible = False
                        End If
                    End If

            Catch ex As Exception
                Throw ex
            End Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub GridPanelSetting()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 2 Then
            GP_DirectorIdentification.ColumnModel.Columns.RemoveAt(GP_DirectorIdentification.ColumnModel.Columns.Count - 1)
            GP_DirectorIdentification.ColumnModel.Columns.Insert(1, CommandColumn13)
            GP_Director_Phone.ColumnModel.Columns.RemoveAt(GP_Director_Phone.ColumnModel.Columns.Count - 1)
            GP_Director_Phone.ColumnModel.Columns.Insert(1, CommandColumn6)
            GP_Director_Address.ColumnModel.Columns.RemoveAt(GP_Director_Address.ColumnModel.Columns.Count - 1)
            GP_Director_Address.ColumnModel.Columns.Insert(1, CommandColumn56)
            GP_Director_Employer_Phone.ColumnModel.Columns.RemoveAt(GP_Director_Employer_Phone.ColumnModel.Columns.Count - 1)
            GP_Director_Employer_Phone.ColumnModel.Columns.Insert(1, CommandColumn11)
            GP_Director_Employer_Address.ColumnModel.Columns.RemoveAt(GP_Director_Employer_Address.ColumnModel.Columns.Count - 1)
            GP_Director_Employer_Address.ColumnModel.Columns.Insert(1, CommandColumn10)
            gp_INDV_Phones.ColumnModel.Columns.RemoveAt(gp_INDV_Phones.ColumnModel.Columns.Count - 1)
            gp_INDV_Phones.ColumnModel.Columns.Insert(1, CommandColumn1)
            GP_INDVD_Address.ColumnModel.Columns.RemoveAt(GP_INDVD_Address.ColumnModel.Columns.Count - 1)
            GP_INDVD_Address.ColumnModel.Columns.Insert(1, CommandColumn2)
            GP_INDVD_Identification.ColumnModel.Columns.RemoveAt(GP_INDVD_Identification.ColumnModel.Columns.Count - 1)
            GP_INDVD_Identification.ColumnModel.Columns.Insert(1, CommandColumn7)
            GP_Empoloyer_Phone.ColumnModel.Columns.RemoveAt(GP_Empoloyer_Phone.ColumnModel.Columns.Count - 1)
            GP_Empoloyer_Phone.ColumnModel.Columns.Insert(1, CommandColumn8)
            GP_Employer_Address.ColumnModel.Columns.RemoveAt(GP_Employer_Address.ColumnModel.Columns.Count - 1)
            GP_Employer_Address.ColumnModel.Columns.Insert(1, CommandColumn9)
            GP_Phone_Corp.ColumnModel.Columns.RemoveAt(GP_Phone_Corp.ColumnModel.Columns.Count - 1)
            GP_Phone_Corp.ColumnModel.Columns.Insert(1, CommandColumn3)
            GP_Address_Corp.ColumnModel.Columns.RemoveAt(GP_Address_Corp.ColumnModel.Columns.Count - 1)
            GP_Address_Corp.ColumnModel.Columns.Insert(1, CommandColumn4)
            gp_Director.ColumnModel.Columns.RemoveAt(gp_Director.ColumnModel.Columns.Count - 1)
            gp_Director.ColumnModel.Columns.Insert(1, cmdDirectory)



        End If

    End Sub

    Sub Maximizeable()
        WindowDetailDirector.Maximizable = True
        WindowDetail.Maximizable = True
        WindowDetailAddress.Maximizable = True
        WindowDetailIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True

    End Sub


    Protected Sub GrdCmdCustPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Sub LoadDataDetail(id As Long)
        Dim objCustomerPhonesDetail As Vw_Customer_Phones = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            cb_phone_Contact_Type.ReadOnly = True
            cb_phone_Communication_Type.ReadOnly = True
            txt_phone_Country_prefix.ReadOnly = True
            txt_phone_number.ReadOnly = True
            txt_phone_extension.ReadOnly = True
            txt_phone_comments.ReadOnly = True
            'btnSavedetail.Hidden = True
            WindowDetail.Title = "Phone Detail"
            ClearinputCustomerPhones()
            With objCustomerPhonesDetail
                Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                Store_Contact_Type.DataBind()
                Phone_Contact_Type = goAML_CustomerBLL.GetListKategoriKontak.Where(Function(x) x.Keterangan = objCustomerPhonesDetail.Contact_Type).FirstOrDefault
                cb_phone_Contact_Type.SetValue(Phone_Contact_Type.Kode)
                Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                Store_Communication_Type.DataBind()
                Phone_Communication_Type = goAML_CustomerBLL.GetCommunicationTypeByID(objCustomerPhonesDetail.Tph_Communication_Type)
                cb_phone_Communication_Type.SetValue(Phone_Communication_Type.Kode)
                txt_phone_Country_prefix.Text = .tph_country_prefix
                txt_phone_number.Text = .tph_number
                txt_phone_extension.Text = .tph_extension
                txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Protected Sub GrdCmdCustAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataAddressDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataAddressDetail(id As Long)

        Dim objCustomerAddressDetail = goAML_CustomerBLL.GetVWCustomerAddressesbyPK(id)

        If Not objCustomerAddressDetail Is Nothing Then
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            cmb_kategoriaddress.ReadOnly = True
            Address_INDV.ReadOnly = True
            Town_INDV.ReadOnly = True
            City_INDV.ReadOnly = True
            Zip_INDV.ReadOnly = True
            cmb_kodenegara.ReadOnly = True
            State_INDVD.ReadOnly = True
            Comment_Address_INDVD.ReadOnly = True
            'btnSavedetail.Hidden = True
            WindowDetailAddress.Title = "Address Detail"
            ClearinputCustomerAddress()
            Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            Store_KategoriAddress.DataBind()
            Dim kategoriaddress = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
            If kategoriaddress IsNot Nothing Then
                cmb_kategoriaddress.SetValue(kategoriaddress.Kode)
            End If

            Address_INDV.Text = objCustomerAddressDetail.Address
            Town_INDV.Text = objCustomerAddressDetail.Town
            City_INDV.Text = objCustomerAddressDetail.City
            Zip_INDV.Text = objCustomerAddressDetail.Zip
            Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            Store_Kode_Negara.DataBind()
            Dim namanegara = goAML_CustomerBLL.GetNamaNegaraByKeterangan(objCustomerAddressDetail.Country)
            If namanegara IsNot Nothing Then
                cmb_kodenegara.SetValue(namanegara.Kode)
            End If

            State_INDVD.Text = objCustomerAddressDetail.State
            Comment_Address_INDVD.Text = objCustomerAddressDetail.Comments
        End If
    End Sub

    Sub LoadDataIdentificationDetail(id As Long)

        Dim objCustomerIdentificationDetail = goAML_CustomerBLL.GetVWCustomerIdentificationsbyPKPersonIdentification(id)

        If Not objCustomerIdentificationDetail Is Nothing Then
            FP_Identification_INDV.Hidden = False
            WindowDetailIdentification.Hidden = False
            WindowDetailIdentification.Title = "Identification Detail"
            txt_number_identification.ReadOnly = True
            cmb_Jenis_Dokumen.ReadOnly = True
            cmb_issued_country.ReadOnly = True
            txt_issued_by.ReadOnly = True
            txt_issued_date.ReadOnly = True
            txt_issued_expired_date.ReadOnly = True
            txt_identification_comment.ReadOnly = True
            ClearinputCustomerIdentification()
            txt_number_identification.Text = objCustomerIdentificationDetail.Number
            Store_jenis_dokumen.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            Store_jenis_dokumen.DataBind()
            Dim jenisdokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(x) x.Keterangan = objCustomerIdentificationDetail.Type_Document).FirstOrDefault()
            If jenisdokumen IsNot Nothing Then
                cmb_Jenis_Dokumen.SetValue(jenisdokumen.Kode)
            End If

            Store_issued_country.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            Store_issued_country.DataBind()
            Dim issuedcountry = goAML_CustomerBLL.GetNamaNegaraByKeterangan(objCustomerIdentificationDetail.Country)
            If issuedcountry IsNot Nothing Then
                cmb_issued_country.SetValue(issuedcountry.Kode)
            End If

            txt_issued_by.Text = objCustomerIdentificationDetail.Issued_By
            txt_issued_date.Value = objCustomerIdentificationDetail.Issue_Date
            txt_issued_expired_date.Value = objCustomerIdentificationDetail.Expiry_Date
            txt_identification_comment.Text = objCustomerIdentificationDetail.Identification_Comment
        End If
    End Sub

    Private Sub CUSTOMER_DETAIL_Default_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    Protected Sub GrdCmdDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailDirector(id As Long)
        Try
            Dim dateFormat As String = "dd-MM-yyyy"

            Dim objSysParamDateFormat As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(6)

            If objSysParamDateFormat IsNot Nothing Then
                dateFormat = objSysParamDateFormat.SettingValue
            End If

            FormPanelDetailDirector.Hidden = False
            cb_Director_Deceased.ReadOnly = True
            txt_Director_Deceased_Date.ReadOnly = True
            txt_Director_Tax_Number.ReadOnly = True
            cb_Director_Tax_Reg_Number.ReadOnly = True
            txt_Director_Source_of_Wealth.ReadOnly = True
            txt_Director_Comments.ReadOnly = True
            Dim Director_Detail = goAML_CustomerBLL.GetCustomerDirectorByPKDirector(id)

            Name.Text = Director_Detail.Last_Name
            Dim role_detail = goAML_CustomerBLL.GetRoleDirectorByID(Director_Detail.Role)
            If role_detail IsNot Nothing Then
                Role.Text = role_detail.Keterangan
            Else
                Role.Text = ""
            End If
            BirthPlace.Text = Director_Detail.Birth_Place
            BirthDate.Text = Convert.ToDateTime(Director_Detail.BirthDate).ToString(dateFormat)
            MomName.Text = Director_Detail.Mothers_Name
            Aliase.Text = Director_Detail.Alias
            SSN.Text = Director_Detail.SSN
            PassportNo.Text = Director_Detail.Passport_Number
            PassportFrom.Text = Director_Detail.Passport_Country
            No_Identity.Text = Director_Detail.ID_Number
            txt_Director_employer_name.Text = Director_Detail.Employer_Name
            If Director_Detail.Nationality1 IsNot Nothing And Director_Detail.Nationality1 IsNot "" Then
                Nasionality1.Text = goAML_CustomerBLL.GetNamaNegaraByID(Director_Detail.Nationality1).Keterangan
            End If
            If Not Director_Detail.Nationality2 = "" And Director_Detail.Nationality2 IsNot Nothing Then
                Nasionality2.Text = goAML_CustomerBLL.GetNamaNegaraByID(Director_Detail.Nationality2).Keterangan
            End If
            If Not Director_Detail.Nationality3 = "" And Director_Detail.Nationality3 IsNot Nothing Then
                Nasionality3.Text = goAML_CustomerBLL.GetNamaNegaraByID(Director_Detail.Nationality3).Keterangan
            End If
            If Not Director_Detail.Residence = "" And Director_Detail.Residence IsNot Nothing Then
                Residence.Text = goAML_CustomerBLL.GetNamaNegaraByID(Director_Detail.Residence).Keterangan
            End If
            Email.Text = Director_Detail.Email
            Pekerjaan.Text = Director_Detail.Occupation

            If Director_Detail.Deceased = True Then
                cb_Director_Deceased.Checked = True
                txt_Director_Deceased_Date.Value = Convert.ToDateTime(Director_Detail.Deceased_Date).ToString(dateFormat)
            End If
            txt_Director_Tax_Number.Text = Director_Detail.Tax_Number
            If Director_Detail.Tax_Reg_Number = True Then
                cb_Director_Tax_Reg_Number.Checked = True
            End If
            txt_Director_Source_of_Wealth.Text = Director_Detail.Source_of_Wealth
            txt_Director_Comments.Text = Director_Detail.Comments


            StoreDirectorPhone.DataSource = objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
            StoreDirectorPhone.DataBind()

            StoreDirectorAddress.DataSource = objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
            StoreDirectorAddress.DataBind()

            StoreDirectorEmployerPhone.DataSource = objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
            StoreDirectorEmployerPhone.DataBind()

            StoreDirectorEmployerAddress.DataSource = objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
            StoreDirectorEmployerAddress.DataBind()

            Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = Director_Detail.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
            Store_Director_Identification.DataBind()

            WindowDetailDirector.Hidden = False
            WindowDetailDirector.Title = "Director Detail"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdAddressDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddressDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorIdentificationDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub checkBoxcb_Director_Deceased_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_Director_Deceased.DirectCheck
        Try
            If cb_Director_Deceased.Checked Then
                txt_Director_Deceased_Date.Hidden = False
            Else
                txt_Director_Deceased_Date.Hidden = True
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ClearinputDirectorIdentification()
        cmb_Jenis_Dokumen_Director.Value = Nothing
        txt_identification_comment_Director.Text = ""
        txt_issued_by_Director.Text = ""
        txt_issued_date_Director.Text = ""
        txt_issued_expired_date_Director.Text = ""
        cmb_issued_country_Director.Value = Nothing
        txt_number_identification_Director.Text = ""
    End Sub

    Sub LoadDataDirectorIdentificationDetail(id As Long)

        Dim objCustomerIdentificationDetail = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.PK_Person_Identification_ID = id).FirstOrDefault()

        If Not objCustomerIdentificationDetail Is Nothing Then
            FP_Identification_Director.Hidden = False
            WindowDetailIdentification.Hidden = False
            WindowDetailIdentification.Title = "Identification Detail"
            txt_number_identification_Director.ReadOnly = True
            cmb_Jenis_Dokumen_Director.ReadOnly = True
            cmb_issued_country_Director.ReadOnly = True
            txt_issued_by_Director.ReadOnly = True
            txt_issued_date_Director.ReadOnly = True
            txt_issued_expired_date_Director.ReadOnly = True
            txt_identification_comment_Director.ReadOnly = True
            ClearinputDirectorIdentification()
            txt_number_identification_Director.Text = objCustomerIdentificationDetail.Number
            Storejenisdokumen_Director.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            Storejenisdokumen_Director.DataBind()
            Dim jenisdokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(X) X.Keterangan = objCustomerIdentificationDetail.Type_Document).FirstOrDefault
            If jenisdokumen IsNot Nothing Then
                cmb_Jenis_Dokumen_Director.SetValue(jenisdokumen.Kode)
            End If

            StoreIssueCountry_Director.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            StoreIssueCountry_Director.DataBind()
            Dim namanegara = goAML_CustomerBLL.GetNamaNegaraByKeterangan(objCustomerIdentificationDetail.Country)
            If namanegara IsNot Nothing Then
                cmb_issued_country_Director.SetValue(namanegara.Kode)
            End If
            txt_issued_by_Director.Text = objCustomerIdentificationDetail.Issued_By
            txt_issued_date_Director.Text = objCustomerIdentificationDetail.Issue_Date
            txt_issued_expired_date_Director.Text = objCustomerIdentificationDetail.Expiry_Date
            txt_identification_comment_Director.Text = objCustomerIdentificationDetail.Identification_Comment
        End If
    End Sub


    Sub LoadDataDetailAddressDirector(id As Long)
        Try
            Dim objCustomerAddressDetail = goAML_CustomerBLL.GetAddressByID(id)
            If Not objCustomerAddressDetail Is Nothing Then
                FP_Address_Director.Hidden = False
                WindowDetailDirectorAddress.Hidden = False
                WindowDetailDirectorAddress.Title = "Address Detail"
                Director_cmb_kategoriaddress.ReadOnly = True
                Director_Address.ReadOnly = True
                Director_Town.ReadOnly = True
                Director_City.ReadOnly = True
                Director_Zip.ReadOnly = True
                Director_cmb_kodenegara.ReadOnly = True
                Director_State.ReadOnly = True
                Director_Comment_Address.ReadOnly = True
                Director_Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                Director_Store_KategoriAddress.DataBind()
                Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
                If kategorikontak IsNot Nothing Then
                    Director_cmb_kategoriaddress.SetValue(kategorikontak.Kode)
                End If
                Director_Address.Text = objCustomerAddressDetail.Address
                Director_Town.Text = objCustomerAddressDetail.Town
                Director_City.Text = objCustomerAddressDetail.City
                Director_Zip.Text = objCustomerAddressDetail.Zip
                Director_Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                Director_Store_Kode_Negara.DataBind()
                Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerAddressDetail.Country_Code)
                If kodenegara IsNot Nothing Then
                    Director_cmb_kodenegara.SetValue(kodenegara.Kode)
                End If

                Director_State.Text = objCustomerAddressDetail.State
                Director_Comment_Address.Text = objCustomerAddressDetail.Comments
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdPhoneDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhoneDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailPhoneDirector(id As Long)
        Try
            Dim objCustomerPhonesDetail As goAML_Ref_Phone = goAML_CustomerBLL.GetPhonesByID(id)


            Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

            If Not objCustomerPhonesDetail Is Nothing Then
                FormPanelDirectorPhone.Hidden = False
                WindowDetailDirectorPhone.Hidden = False
                Director_cb_phone_Contact_Type.ReadOnly = True
                Director_cb_phone_Communication_Type.ReadOnly = True
                Director_txt_phone_Country_prefix.ReadOnly = True
                Director_txt_phone_number.ReadOnly = True
                Director_txt_phone_extension.ReadOnly = True
                Director_txt_phone_comments.ReadOnly = True

                WindowDetailDirectorPhone.Title = "Phone Detail"
                With objCustomerPhonesDetail
                    Director_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                    Director_Store_Contact_Type.DataBind()
                    Director_cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)

                    Director_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                    Director_Store_Communication_Type.DataBind()
                    Director_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                    Director_txt_phone_Country_prefix.Text = .tph_country_prefix
                    Director_txt_phone_number.Text = .tph_number
                    Director_txt_phone_extension.Text = .tph_extension
                    Director_txt_phone_comments.Text = .comments
                End With
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btnBackSaveDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelDetailDirector.Hidden = True
            WindowDetailDirector.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelDirectorPhone.Hidden = True
            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_Director.Hidden = True
            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataIdentificationDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ClearinputCustomerIdentification()
        cmb_Jenis_Dokumen.Value = Nothing
        txt_identification_comment.Text = ""
        txt_issued_by.Text = ""
        txt_issued_date.Text = ""
        txt_issued_expired_date.Text = ""
        cmb_issued_country.Value = Nothing
        txt_number_identification.Text = ""
    End Sub

    Sub ClearinputCustomerAddress()
        cmb_kategoriaddress.Value = Nothing
        cmb_kodenegara.Value = Nothing
        Address_INDV.Text = ""
        Town_INDV.Text = ""
        City_INDV.Text = ""
        Zip_INDV.Text = ""
        State_INDVD.Text = ""
        Comment_Address_INDVD.Text = ""
    End Sub

    Protected Sub btnBackSaveCustomerAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_INDVD.Hidden = True
            WindowDetailAddress.Hidden = True
            ClearinputCustomerAddress()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Identification_INDV.Hidden = True
            WindowDetailIdentification.Hidden = True
            ClearinputCustomerIdentification()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailEmployerPhone(id As Long)

        Dim objCustomerPhonesDetail As goAML_Ref_Phone = goAML_CustomerBLL.GetPhonesByID(id)

        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            cb_phone_Contact_Type.ReadOnly = True
            cb_phone_Communication_Type.ReadOnly = True
            txt_phone_Country_prefix.ReadOnly = True
            txt_phone_number.ReadOnly = True
            txt_phone_extension.ReadOnly = True
            txt_phone_comments.ReadOnly = True


            WindowDetail.Title = "Phone Detail"
            ClearinputCustomerPhones()
            With objCustomerPhonesDetail
                Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                Store_Contact_Type.DataBind()
                'Phone_Contact_Type = db.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Keterangan = objCustomerPhonesDetail.Contact_Type).FirstOrDefault
                'cb_phone_Contact_Type.SetValue(Phone_Contact_Type.Kode)
                cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)

                Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                Store_Communication_Type.DataBind()
                'Phone_Communication_Type = db.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Keterangan = objCustomerPhonesDetail.Communcation_Type).FirstOrDefault
                'cb_phone_Communication_Type.SetValue(Phone_Communication_Type.Kode)
                cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                txt_phone_Country_prefix.Text = .tph_country_prefix
                txt_phone_number.Text = .tph_number
                txt_phone_extension.Text = .tph_extension
                txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Protected Sub GrdCmdEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailEmployerPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()
            objTempCustomerPhoneEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Identification_Director.Hidden = True
            WindowDetailIdentification.Hidden = True
            ClearinputDirectorIdentification()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataEmployerAddressDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataEmployerAddressDetail(id As Long)

        Dim objCustomerAddressDetail = goAML_CustomerBLL.GetAddressByID(id)
        If Not objCustomerAddressDetail Is Nothing Then
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            WindowDetailAddress.Title = "Address Detail"
            cmb_kategoriaddress.ReadOnly = True
            Address_INDV.ReadOnly = True
            Town_INDV.ReadOnly = True
            City_INDV.ReadOnly = True
            Zip_INDV.ReadOnly = True
            cmb_kodenegara.ReadOnly = True
            State_INDVD.ReadOnly = True
            Comment_Address_INDVD.ReadOnly = True
            ClearinputCustomerAddress()
            Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            Store_KategoriAddress.DataBind()
            Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
            If kategorikontak IsNot Nothing Then
                cmb_kategoriaddress.SetValue(kategorikontak.Kode)
            End If

            Address_INDV.Text = objCustomerAddressDetail.Address
            Town_INDV.Text = objCustomerAddressDetail.Town
            City_INDV.Text = objCustomerAddressDetail.City
            Zip_INDV.Text = objCustomerAddressDetail.Zip
            Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            Store_Kode_Negara.DataBind()
            Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerAddressDetail.Country_Code)
            If kodenegara IsNot Nothing Then
                cmb_kodenegara.SetValue(kodenegara.Kode)
            End If

            State_INDVD.Text = objCustomerAddressDetail.State
            Comment_Address_INDVD.Text = objCustomerAddressDetail.Comments
        End If
    End Sub


End Class

