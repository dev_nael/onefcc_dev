﻿Imports NawaBLL
Imports Ext
Imports NawaDevBLL
Imports System.Data.SqlClient
Imports NawaDAL
Imports NawaDevDAL
Imports Elmah
Imports System.Data
Imports System.Net.Mail
Imports Checkbox = Ext.Net.Checkbox

Partial Class CUSTOMER_ADD
    Inherits ParentPage
    Public objFormModuleView As NawaBLL.FormModuleDetail
    Public objgoAML_CustomerBLL As New NawaDevBLL.goAML_CustomerBLL
    Private mModuleName As String
    Private TempPK As Int32

    Public Property ModulName() As String
        Get
            Return mModuleName
        End Get
        Set(ByVal value As String)
            mModuleName = value
        End Set
    End Property
    Sub ClearSession()
        tempflag = New Integer
        obj_customer = New goAML_Ref_Customer
        objListgoAML_Ref_Phone = New List(Of goAML_Ref_Phone)
        objListgoAML_Ref_Address = New List(Of goAML_Ref_Address)
        objListgoAML_Ref_Director_Phone = New List(Of goAML_Ref_Phone)
        objListgoAML_Ref_Director_Address = New List(Of goAML_Ref_Address)
        objListgoAML_Ref_Director_Employer_Phone = New List(Of goAML_Ref_Phone)
        objListgoAML_Ref_Director_Employer_Address = New List(Of goAML_Ref_Address)
        objListgoAML_Ref_Identification = New List(Of goAML_Person_Identification)
        objListgoAML_Ref_Identification_Director = New List(Of goAML_Person_Identification)
        objListgoAML_Ref_Director = New List(Of goAML_Ref_Customer_Entity_Director)
        objListgoAML_Ref_Employer_Phone = New List(Of goAML_Ref_Phone)
        objListgoAML_Ref_Employer_Address = New List(Of goAML_Ref_Address)
        objListgoAML_vw_Customer_Phones = New List(Of Vw_Customer_Phones)
        objListgoAML_vw_Customer_Addresses = New List(Of Vw_Customer_Addresses)
        objListgoAML_vw_Director_Phones = New List(Of Vw_Director_Phones)
        objListgoAML_vw_Director_Addresses = New List(Of Vw_Director_Addresses)
        objListgoAML_vw_Director_Employer_Phones = New List(Of Vw_Director_Employer_Phones)
        objListgoAML_vw_Director_Employer_Addresses = New List(Of Vw_Director_Employer_Addresses)
        objListgoAML_vw_Customer_Identification = New List(Of Vw_Person_Identification)
        objListgoAML_vw_Customer_Identification_Director = New List(Of Vw_Person_Identification)
        objListgoAML_vw_Employer_Phones = New List(Of Vw_Employer_Phones)
        objListgoAML_vw_Employer_Addresses = New List(Of Vw_Employer_Addresses)
        objListgoAML_vw_Customer_Entity_Director = New List(Of Vw_Customer_Director)
    End Sub

    Public Property tempflag() As Integer
        Get
            Return Session("CUSTOMER_EDIT.tempflag")
        End Get
        Set(ByVal value As Integer)
            Session("CUSTOMER_EDIT.tempflag") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Employer_Address() As List(Of NawaDevDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_Ref_Employer_Address")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Ref_Address))
            Session("CUSTOMER_ADD.objListgoAML_Ref_Employer_Address") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Employer_Addresses() As List(Of NawaDevDAL.Vw_Employer_Addresses)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_vw_Employer_Addresses")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Employer_Addresses))
            Session("CUSTOMER_ADD.objListgoAML_vw_Employer_Addresses") = value
        End Set
    End Property

    Public Property objTempEmployerAddressEdit() As NawaDevDAL.Vw_Employer_Addresses
        Get
            Return Session("CUSTOMER_ADD.objTempEmployerAddressEdit")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Employer_Addresses)
            Session("CUSTOMER_ADD.objTempEmployerAddressEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_EmployerAddresses() As NawaDevDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_ADD.objTempgoAML_Ref_EmployerAddresses")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Address)
            Session("CUSTOMER_ADD.objTempgoAML_Ref_EmployerAddresses") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Employer_Phone() As List(Of NawaDevDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_Ref_Employer_Phone")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Ref_Phone))
            Session("CUSTOMER_ADD.objListgoAML_Ref_Employer_Phone") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Employer_Phones() As List(Of NawaDevDAL.Vw_Employer_Phones)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_vw_Employer_Phones")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Employer_Phones))
            Session("CUSTOMER_ADD.objListgoAML_vw_Employer_Phones") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_EmployerPhones() As NawaDevDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_ADD.objTempgoAML_Ref_EmployerPhones")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Phone)
            Session("CUSTOMER_ADD.objTempgoAML_Ref_EmployerPhones") = value
        End Set
    End Property

    Public Property objTempEmployerPhonesEdit() As NawaDevDAL.Vw_Employer_Phones
        Get
            Return Session("CUSTOMER_ADD.objTempEmployerPhonesEdit")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Employer_Phones)
            Session("CUSTOMER_ADD.objTempEmployerPhonesEdit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Identification() As List(Of NawaDevDAL.goAML_Person_Identification)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_Ref_Identification")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Person_Identification))
            Session("CUSTOMER_ADD.objListgoAML_Ref_Identification") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Identification() As List(Of NawaDevDAL.Vw_Person_Identification)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_vw_Customer_Identification")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Person_Identification))
            Session("CUSTOMER_ADD.objListgoAML_vw_Customer_Identification") = value
        End Set
    End Property

    Public Property objTempCustomerIdentificationEdit() As NawaDevDAL.Vw_Person_Identification
        Get
            Return Session("CUSTOMER_ADD.objTempCustomerIdentificationEdit")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Person_Identification)
            Session("CUSTOMER_ADD.objTempCustomerIdentificationEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Identification() As NawaDevDAL.goAML_Person_Identification
        Get
            Return Session("CUSTOMER_ADD.objTempgoAML_Ref_Identification")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Person_Identification)
            Session("CUSTOMER_ADD.objTempgoAML_Ref_Identification") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Address() As NawaDevDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_ADD.objTempgoAML_Ref_Address")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Address)
            Session("CUSTOMER_ADD.objTempgoAML_Ref_Address") = value
        End Set
    End Property


    Public Property objListgoAML_Ref_Address() As List(Of NawaDevDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_Ref_Address")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Ref_Address))
            Session("CUSTOMER_ADD.objListgoAML_Ref_Address") = value
        End Set
    End Property


    Public Property objTempCustomerAddressEdit() As NawaDevDAL.Vw_Customer_Addresses
        Get
            Return Session("CUSTOMER_ADD.objTempCustomerAddressEdit")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Customer_Addresses)
            Session("CUSTOMER_ADD.objTempCustomerAddressEdit") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Addresses() As List(Of NawaDevDAL.Vw_Customer_Addresses)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_vw_Customer_Addresses")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Customer_Addresses))
            Session("CUSTOMER_ADD.objListgoAML_vw_Customer_Addresses") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Phones() As List(Of NawaDevDAL.Vw_Customer_Phones)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_vw_Customer_Phones")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Customer_Phones))
            Session("CUSTOMER_ADD.objListgoAML_vw_Customer_Phones") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Phone() As List(Of NawaDevDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_ADD.goAML_Ref_Phone")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Ref_Phone))
            Session("CUSTOMER_ADD.goAML_Ref_Phone") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Phone() As NawaDevDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_ADD.objTempgoAML_Ref_Phone")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Phone)
            Session("CUSTOMER_ADD.objTempgoAML_Ref_Phone") = value
        End Set
    End Property

    Public Property objTempCustomerPhoneEdit() As NawaDevDAL.Vw_Customer_Phones
        Get
            Return Session("CUSTOMER_ADD.objTempCustomerPhoneEdit")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Customer_Phones)
            Session("CUSTOMER_ADD.objTempCustomerPhoneEdit") = value
        End Set
    End Property

    Public Property obj_customer() As goAML_Ref_Customer
        Get
            If Session("CUSTOMER_ADD.obj_customer") Is Nothing Then
                Session("CUSTOMER_ADD.obj_customer") = New goAML_Ref_Customer
            End If
            Return Session("CUSTOMER_ADD.obj_customer")
        End Get
        Set(ByVal value As goAML_Ref_Customer)
            Session("CUSTOMER_ADD.obj_customer") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director() As List(Of NawaDevDAL.goAML_Ref_Customer_Entity_Director)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_Ref_Director")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Ref_Customer_Entity_Director))
            Session("CUSTOMER_ADD.objListgoAML_Ref_Director") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Entity_Director() As List(Of NawaDevDAL.Vw_Customer_Director)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_vw_Customer_Entity_Director")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Customer_Director))
            Session("CUSTOMER_ADD.objListgoAML_vw_Customer_Entity_Director") = value
        End Set
    End Property

    Public Property objTempCustomerDirectorEdit() As NawaDevDAL.Vw_Customer_Director
        Get
            Return Session("CUSTOMER_ADD.objTempCustomerDirectorEdit")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Customer_Director)
            Session("CUSTOMER_ADD.objTempCustomerDirectorEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director() As NawaDevDAL.goAML_Ref_Customer_Entity_Director
        Get
            Return Session("CUSTOMER_ADD.objTempgoAML_Ref_Director")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Customer_Entity_Director)
            Session("CUSTOMER_ADD.objTempgoAML_Ref_Director") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Employer_Address() As List(Of NawaDevDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_Ref_Director_Employer_Address")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Ref_Address))
            Session("CUSTOMER_ADD.objListgoAML_Ref_Director_Employer_Address") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Director_Employer_Addresses() As List(Of NawaDevDAL.Vw_Director_Employer_Addresses)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_vw_Director_Employer_Addresses")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Director_Employer_Addresses))
            Session("CUSTOMER_ADD.objListgoAML_vw_Director_Employer_Addresses") = value
        End Set
    End Property

    Public Property objTempDirectorEmployerAddressEdit() As NawaDevDAL.Vw_Director_Employer_Addresses
        Get
            Return Session("CUSTOMER_ADD.objTempDirectorEmployerAddressEdit")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Director_Employer_Addresses)
            Session("CUSTOMER_ADD.objTempDirectorEmployerAddressEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director_EmployerAddresses() As NawaDevDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_ADD.objTempgoAML_Ref_Director_EmployerAddresses")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Address)
            Session("CUSTOMER_ADD.objTempgoAML_Ref_Director_EmployerAddresses") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Address() As List(Of NawaDevDAL.goAML_Ref_Address)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_Ref_Director_Address")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Ref_Address))
            Session("CUSTOMER_ADD.objListgoAML_Ref_Director_Address") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Director_Addresses() As List(Of NawaDevDAL.Vw_Director_Addresses)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_vw_Director_Addresses")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Director_Addresses))
            Session("CUSTOMER_ADD.objListgoAML_vw_Director_Addresses") = value
        End Set
    End Property

    Public Property objTempDirectorAddressEdit() As NawaDevDAL.Vw_Director_Addresses
        Get
            Return Session("CUSTOMER_ADD.objTempDirectorAddressEdit")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Director_Addresses)
            Session("CUSTOMER_ADD.objTempDirectorAddressEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director_Addresses() As NawaDevDAL.goAML_Ref_Address
        Get
            Return Session("CUSTOMER_ADD.objTempgoAML_Ref_Director_Addresses")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Address)
            Session("CUSTOMER_ADD.objTempgoAML_Ref_Director_Addresses") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Employer_Phone() As List(Of NawaDevDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_Ref_Director_Employer_Phone")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Ref_Phone))
            Session("CUSTOMER_ADD.objListgoAML_Ref_Director_Employer_Phone") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Director_Employer_Phones() As List(Of NawaDevDAL.Vw_Director_Employer_Phones)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_vw_Director_Employer_Phones")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Director_Employer_Phones))
            Session("CUSTOMER_ADD.objListgoAML_vw_Director_Employer_Phones") = value
        End Set
    End Property

    Public Property objTempDirectorEmployerPhoneEdit() As NawaDevDAL.Vw_Director_Employer_Phones
        Get
            Return Session("CUSTOMER_ADD.objTempDirectorEmployerPhoneEdit")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Director_Employer_Phones)
            Session("CUSTOMER_ADD.objTempDirectorEmployerPhoneEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director_EmployerPhones() As NawaDevDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_ADD.objTempgoAML_Ref_Director_EmployerPhones")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Phone)
            Session("CUSTOMER_ADD.objTempgoAML_Ref_Director_EmployerPhones") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Director_Phone() As List(Of NawaDevDAL.goAML_Ref_Phone)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_Ref_Director_Phone")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Ref_Phone))
            Session("CUSTOMER_ADD.objListgoAML_Ref_Director_Phone") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Director_Phones() As List(Of NawaDevDAL.Vw_Director_Phones)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_vw_Director_Phones")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Director_Phones))
            Session("CUSTOMER_ADD.objListgoAML_vw_Director_Phones") = value
        End Set
    End Property

    Public Property objTempDirectorPhoneEdit() As NawaDevDAL.Vw_Director_Phones
        Get
            Return Session("CUSTOMER_ADD.objTempDirectorPhoneEdit")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Director_Phones)
            Session("CUSTOMER_ADD.objTempDirectorPhoneEdit") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Director_Phones() As NawaDevDAL.goAML_Ref_Phone
        Get
            Return Session("CUSTOMER_ADD.objTempgoAML_Ref_Director_Phones")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Phone)
            Session("CUSTOMER_ADD.objTempgoAML_Ref_Director_Phones") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Identification_Director() As List(Of NawaDevDAL.goAML_Person_Identification)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_Ref_Identification_Director")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Person_Identification))
            Session("CUSTOMER_ADD.objListgoAML_Ref_Identification_Director") = value
        End Set
    End Property

    Public Property objListgoAML_vw_Customer_Identification_Director() As List(Of NawaDevDAL.Vw_Person_Identification)
        Get
            Return Session("CUSTOMER_ADD.objListgoAML_vw_Customer_Identification_Director")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Person_Identification))
            Session("CUSTOMER_ADD.objListgoAML_vw_Customer_Identification_Director") = value
        End Set
    End Property

    Public Property objTempCustomerIdentificationEdit_Director() As NawaDevDAL.Vw_Person_Identification
        Get
            Return Session("CUSTOMER_ADD.objTempCustomerIdentificationEdit_Director")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Person_Identification)
            Session("CUSTOMER_ADD.objTempCustomerIdentificationEdit_Director") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Identification_Director() As NawaDevDAL.goAML_Person_Identification
        Get
            Return Session("CUSTOMER_ADD.objTempgoAML_Ref_Identification_Director")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Person_Identification)
            Session("CUSTOMER_ADD.objTempgoAML_Ref_Identification_Director") = value
        End Set
    End Property


    Function IsDataCustomerValid() As Boolean
        'If Cmb_TypePerson.SelectedItemValue = 1 Then
        '    If txt_INDV_Occupation.Text = "" Then
        '        Throw New Exception("Occupation is required")
        '    End If
        '    If txt_INDV_Source_of_Wealth.Text = "" Then
        '        Throw New Exception("Source of Wealth is required")
        '    End If
        '    If txt_cif.Text = "" Then
        '        Throw New Exception("CIF is required")
        '    End If
        '    If txt_gcn.Text = "" Then
        '        Throw New Exception("GCN is required")
        '    End If
        '    If GCNPrimary.Checked = False Then
        '        If txt_gcn.Value IsNot Nothing Then
        '            Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_gcn.Value)
        '            If checkGCN Is Nothing Then
        '                'DirectCast(GCNPrimary, Checkbox).Checked = True
        '                Throw New Exception("Untuk GCN " + txt_gcn.Value + " belum memiliki GCNPrimary")
        '            End If
        '        End If
        '    End If
        '    If txt_INDV_Last_Name.Text = "" Then
        '        Throw New Exception("Full Name is required")
        '    End If
        '    If txt_INDV_Birthdate.RawValue = "" Then
        '        Throw New Exception("Birth Date is required")
        '    End If
        '    If txt_INDV_Birth_place.Text = "" Then
        '        Throw New Exception("Birth Place is required")
        '    End If
        '    'Update: Zikri_08102020
        '    If txt_INDV_SSN.Text.Trim IsNot "" Then
        '        If Not IsNumber(txt_INDV_SSN.Text) Then
        '            Throw New Exception("NIK is Invalid")
        '        End If
        '        If txt_INDV_SSN.Text.Trim.Length <> 16 Then
        '            Throw New Exception("NIK must contains 16 digit number")
        '        End If
        '    End If
        '    'End Update
        '    If cmb_INDV_Gender.SelectedItemValue = "" Then
        '        Throw New Exception("Gender is required")
        '    End If
        '    If cmb_INDV_Nationality1.SelectedItemValue = "" Then
        '        Throw New Exception("Nationality 1  is required")
        '    End If
        '    If cmb_INDV_Residence.SelectedItemValue = "" Then
        '        Throw New Exception("Residence is required")
        '    End If
        '    'Update: Zikri_11092020 Menambah Validasi format email
        '    If txt_INDV_Email.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email.Text) Then
        '            Throw New Exception("Invalid Email Address in Email")
        '        End If
        '    End If
        '    If txt_INDV_Email2.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email2.Text) Then
        '            Throw New Exception("Invalid Email Address in Email 2")
        '        End If
        '    End If
        '    If txt_INDV_Email3.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email3.Text) Then
        '            Throw New Exception("Invalid Email Address in Email 3")
        '        End If
        '    End If
        '    If txt_INDV_Email4.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email4.Text) Then
        '            Throw New Exception("Invalid Email Address in Email 4")
        '        End If
        '    End If
        '    If txt_INDV_Email5.Text IsNot "" Then
        '        If Not isEmailAddress(txt_INDV_Email5.Text) Then
        '            Throw New Exception("Invalid Email Address in Email 5")
        '        End If
        '    End If
        '    'End Update
        '    If Not String.IsNullOrEmpty(txt_INDV_ID_Number.Text) Then
        '        If objListgoAML_Ref_Identification.Count = 0 Then
        '            Throw New Exception("Identification is required")
        '        End If
        '    End If

        '    'If objListgoAML_Ref_Employer_Phone.Count = 0 Then
        '    '    Throw New Exception("Employer Phone is required")
        '    'End If
        '    'If objListgoAML_Ref_Employer_Address.Count = 0 Then
        '    '    Throw New Exception("Employer Address is required")
        '    'End If
        '    If cb_Deceased.Checked = True Then
        '            If txt_deceased_date.RawValue = "" Then
        '                Throw New Exception("Date Deceased is required")
        '            End If
        '        End If

        '    ElseIf Cmb_TypePerson.SelectedItemValue = 2 Then
        '        If GCNPrimary.Checked = False Then
        '        If txt_gcn.Value IsNot Nothing Then
        '            Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_gcn.Value)
        '            If checkGCN Is Nothing Then
        '                'DirectCast(GCNPrimary, Checkbox).Checked = True
        '                Throw New Exception("Untuk GCN " + txt_gcn.Value + " belum memiliki GCNPrimary")
        '            End If
        '        End If
        '    End If
        '    If txt_Corp_Business.Text = "" Then
        '        Throw New Exception("Corp Bussiness is required")
        '    End If
        '    'Update: Zikri_11092020 Menambah Validasi format email
        '    If txt_Corp_Email.Text IsNot "" Then
        '        If Not isEmailAddress(txt_Corp_Email.Text) Then
        '            Throw New Exception("Invalid Email Address in Email Korporasi")
        '        End If
        '    End If
        '    'End Update
        '    If txt_cif.Text = "" Then
        '        Throw New Exception("CIF is required")
        '    End If
        '    If txt_gcn.Text = "" Then
        '        Throw New Exception("GCN is required")
        '    End If
        '    If txt_Corp_Name.Text = "" Then
        '        Throw New Exception("Corp Name is required")
        '    End If
        '    If cmb_Corp_Incorporation_Country_Code.SelectedItemValue = "" Then
        '        Throw New Exception("Country is required")
        '    End If
        '    'If cmb_Corp_Role.RawValue = "" Then
        '    '    Throw New Exception("Role is required")
        '    'End If
        '    If objListgoAML_Ref_Director.Count = 0 Then
        '        Throw New Exception("Director is required")
        '    End If

        'End If
        Dim dataCIF = goAML_CustomerBLL.GetCustomerbyCIF(txt_cif.Text)
        If dataCIF IsNot Nothing Then
            Throw New Exception("CIF Already Exist")
        End If

        'If obj_customer.FK_Customer_Type_ID.Value = 1 Then
        If Cmb_TypePerson.SelectedItemValue = 1 Then ' agam 07122020
            If Replace(txt_INDV_Occupation.Text, " ", "") = "" Then
                Throw New Exception("Occupation is required")
            End If
            If Replace(txt_INDV_Source_of_Wealth.Text, " ", "") = "" Then
                Throw New Exception("Source of Wealth is required")
            End If
            If Replace(txt_cif.Text, " ", "") = "" Then
                Throw New Exception("CIF is required")
            End If
            'dedy added 25082020
            If Replace(txt_gcn.Text, " ", "") = "" Then
                Throw New Exception("GCN is required")
            End If
            'dedy end added 25082020
            'Update: Zikri_18092020 Validasi GCN
            If GCNPrimary.Checked = False Then
                If txt_gcn.Value IsNot Nothing Then
                    Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_gcn.Value)
                    If checkGCN Is Nothing Then
                        'DirectCast(GCNPrimary, Checkbox).Checked = True
                        Throw New Exception("Untuk GCN " + txt_gcn.Value + " belum memiliki GCNPrimary")
                        'ElseIf checkGCN.GCN = txt_GCN.Value Then
                        '    Throw New Exception("Untuk GCN " + txt_GCN.Value + " belum memiliki GCNPrimary")
                    End If
                End If
            End If
            'End Update
            If Replace(txt_INDV_Last_Name.Text, " ", "") = "" Then
                Throw New Exception("Full Name is required")
            End If
            If Replace(txt_INDV_Birthdate.RawValue, " ", "") = "" Then
                Throw New Exception("Birth Date is required")
            End If
            If Replace(txt_INDV_Birth_place.Text, " ", "") = "" Then
                Throw New Exception("Birth Place is required")
            End If
            'Update: Zikri_08102020
            If txt_INDV_SSN.Text.Trim IsNot "" Then
                If Not IsNumber(txt_INDV_SSN.Text) Then
                    Throw New Exception("NIK is Invalid")
                End If
                If txt_INDV_SSN.Text.Trim.Length <> 16 Then
                    Throw New Exception("NIK must contains 16 digit number")
                End If
            End If
            'End Update
            If cmb_INDV_Gender.SelectedItemValue = "" Then
                Throw New Exception("Gender is required")
            End If
            If cmb_INDV_Nationality1.SelectedItemValue = "" Then
                Throw New Exception("Nationality 1  is required")
            End If
            If cmb_INDV_Residence.SelectedItemValue = "" Then
                Throw New Exception("Residence is required")
            End If
            'Update: Zikri_11092020 Menambah Validasi format email

            If Replace(txt_INDV_Email.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email.Text) Then
                    Throw New Exception("Invalid Email Address in Email")
                End If
            End If
            If Replace(txt_INDV_Email2.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email2.Text) Then
                    Throw New Exception("Invalid Email Address in Email 2")
                End If
            End If
            If Replace(txt_INDV_Email3.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email3.Text) Then
                    Throw New Exception("Invalid Email Address in Email 3")
                End If
            End If
            If Replace(txt_INDV_Email4.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email4.Text) Then
                    Throw New Exception("Invalid Email Address in Email 4")
                End If
            End If
            If Replace(txt_INDV_Email5.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_INDV_Email5.Text) Then
                    Throw New Exception("Invalid Email Address in Email 5")
                End If
            End If
            ''End Update
            If Not String.IsNullOrEmpty(txt_INDV_ID_Number.Text) Then
                If objListgoAML_Ref_Identification.Count = 0 Then
                    Throw New Exception("Identification is required")
                End If
            End If
            'If objListgoAML_Ref_Employer_Phone.Count = 0 Then
            '    Throw New Exception("Employer Phone is required")
            'End If
            'If objListgoAML_Ref_Employer_Address.Count = 0 Then
            '    Throw New Exception("Employer Address is required")
            'End If
            If cb_Deceased.Checked = True Then
                If txt_deceased_date.RawValue = "" Then
                    Throw New Exception("Date Deceased is required")
                End If
            End If
            ' ElseIf obj_customer.FK_Customer_Type_ID.Value = 2 Then
        ElseIf Cmb_TypePerson.SelectedItemValue = 2 Then ' agam 07122020
            'Update: Zikri_18092020 Validasi GCN
            If GCNPrimary.Checked = False Then
                If txt_gcn.Value IsNot Nothing Then
                    Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_gcn.Value)
                    If checkGCN Is Nothing Then
                        'DirectCast(GCNPrimary, Checkbox).Checked = True
                        Throw New Exception("Untuk GCN " + txt_gcn.Value + " belum memiliki GCNPrimary")
                        'ElseIf checkGCN.GCN = txt_GCN.Value Then
                        '    Throw New Exception("Untuk GCN " + txt_GCN.Value + " belum memiliki GCNPrimary")
                    End If
                End If
            End If
            'End Update
            If Replace(txt_Corp_Business.Text, " ", "") = "" Then
                Throw New Exception("Corp Bussiness is required")
            End If
            'Update: Zikri_11092020 Menambah Validasi format email
            If Replace(txt_Corp_Email.Text, " ", "") IsNot Nothing Then
                If Not isEmailAddress(txt_Corp_Email.Text) Then
                    Throw New Exception("Invalid Email Address in Email Korporasi")
                End If
            End If
            'End Update
            If Replace(txt_cif.Text, " ", "") = "" Then
                Throw New Exception("CIF is required")
            End If
            'dedy added 25082020
            If Replace(txt_gcn.Text, " ", "") = "" Then
                Throw New Exception("GCN is required")
            End If
            'dedy end added 25082020
            If Replace(txt_Corp_Name.Text, " ", "") = "" Then
                Throw New Exception("Corp Name is required")
            End If
            If cmb_Corp_Incorporation_Country_Code.SelectedItemValue = "" Then
                Throw New Exception("Country is required")
            End If
            'If cmb_corp_role.RawValue = "" Then
            '    Throw New Exception("Role is required")
            'End If
            If objListgoAML_Ref_Director.Count = 0 Then
                Throw New Exception("Director is required")
            End If
        End If

        If objListgoAML_Ref_Phone.Count = 0 Then
            Throw New Exception("Phone is required")
        End If
        If objListgoAML_Ref_Address.Count = 0 Then
            Throw New Exception("Address is required")
        End If
        Return True
    End Function
    'Update: Zikri_11092020 Menambah Validasi format email
    Private Function isEmailAddress(text As String) As Boolean
        Try

            Dim email As New Regex("([\w-+]+(?:\.[\w-+]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7})")
            Return email.IsMatch(text)

        Catch ex As Exception
            Return False
        End Try
    End Function
    'End Update
    'Update: Zikri_14092020 Add Validation GCN
    Protected Sub CheckGCN_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If GCNPrimary.Checked = True Then
                If txt_gcn.Value Is Nothing Or txt_gcn.Value = "" Then
                    DirectCast(GCNPrimary, Checkbox).Checked = False
                    Throw New Exception("GCN is required")

                Else
                    Dim checkGCN As goAML_Ref_Customer = goAML_CustomerBLL.GetCheckGCN(txt_gcn.Value)
                    If checkGCN IsNot Nothing Then
                        Throw New ApplicationException("Konfirmasi : Untuk GCN " + txt_gcn.Value + " sudah memiliki GCNPrimary pada CIF " + checkGCN.CIF + " Jika di save, status GCNPrimary di CIF " + checkGCN.CIF + " akan dilepas dan akan dipasang di CIF ini")
                    End If
                End If
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update
    Protected Sub BtnSubmit_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomerValid() Then
                With obj_customer
                    ''dedy added
                    'Dim IntResult As Integer = 0
                    'Dim StrQuery As String = ""
                    'StrQuery = "select GCN, count(1) from goAML_Ref_Customer where GCN = '" & txt_gcn.Value & "' GROUP BY GCN"
                    'Dim dtSet As Data.DataSet = SQLHelper.ExecuteDataSet(SQLHelper.strConnectionString, Data.CommandType.Text, StrQuery)
                    'If dtSet.Tables.Count > 0 Then
                    '    Dim dtTable As Data.DataTable = dtSet.Tables(0)
                    '    If dtTable.Rows.Count > 0 Then
                    '        'IntResult = dtTable.Rows(0).Item(0).ToString
                    '        '.isGCNPrimary = Convert.ToBoolean(IIf(cmbisGCNPrimary.Checked = True, 1, 1))
                    '        'GCNPrimary.Checked = True
                    '        GCNPrimary.Checked = True
                    '    End If
                    'End If
                    ''Dedy end added

                    'Dim yesBtn = New MessageBoxButtonConfig
                    'yesBtn.Handler = "NawadataDirect.Yes()"
                    'yesBtn.Text = "Ok"


                    'Dim listBtn = New MessageBoxButtonsConfig
                    'listBtn.Yes = yesBtn

                    'Ext.Net.X.MessageBox.Confirm("Test", "Are you sure?", listBtn).Show()

                    .CIF = txt_cif.Value
                    .GCN = txt_gcn.Value
                    If TextFieldOpeningDate.RawValue IsNot Nothing Then
                        .Opening_Date = Convert.ToDateTime(TextFieldOpeningDate.RawValue)
                    End If

                    If TextFieldClosingDate.RawValue IsNot Nothing Then
                        .Closing_Date = Convert.ToDateTime(TextFieldClosingDate.RawValue)
                    End If

                    .Opening_Branch_Code = cmb_openingBranch.SelectedItemValue
                    .Status = cmb_Status.SelectedItemValue
                    .isGCNPrimary = Convert.ToBoolean(IIf(GCNPrimary.Checked = True, 1, 0))
                    .Status_Code = Cmb_StatusCode.Value
                    .FK_Customer_Type_ID = Convert.ToInt64(Cmb_TypePerson.SelectedItemValue)
                    .isUpdateFromDataSource = Convert.ToBoolean(IIf(UpdateFromDataSource.Checked = True, 1, 0))

                    If Cmb_TypePerson.SelectedItemValue = 1 Then
                        .INDV_Title = txt_INDV_Title.Text
                        .INDV_Gender = cmb_INDV_Gender.SelectedItemValue
                        .INDV_Last_Name = txt_INDV_Last_Name.Text
                        If txt_INDV_Birthdate.RawValue IsNot Nothing Then
                            .INDV_BirthDate = Convert.ToDateTime(txt_INDV_Birthdate.RawValue)
                        Else
                            .INDV_BirthDate = Nothing
                        End If

                        .INDV_Birth_Place = txt_INDV_Birth_place.Text
                        .INDV_Mothers_Name = txt_INDV_Mothers_name.Text
                        .INDV_Alias = txt_INDV_Alias.Text
                        .INDV_SSN = txt_INDV_SSN.Text
                        .INDV_Passport_Number = txt_INDV_Passport_number.Text
                        'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
                        .INDV_Passport_Country = cmb_INDV_Passport_country.SelectedItemValue
                        'End Update
                        .INDV_ID_Number = txt_INDV_ID_Number.Text
                        .INDV_Nationality1 = cmb_INDV_Nationality1.SelectedItemValue
                        .INDV_Nationality2 = cmb_INDV_Nationality2.SelectedItemValue
                        .INDV_Nationality3 = cmb_INDV_Nationality3.SelectedItemValue
                        .INDV_Residence = cmb_INDV_Residence.SelectedItemValue
                        .INDV_Email = txt_INDV_Email.Text
                        .INDV_Email2 = txt_INDV_Email2.Text
                        .INDV_Email3 = txt_INDV_Email3.Text
                        .INDV_Email4 = txt_INDV_Email4.Text
                        .INDV_Email5 = txt_INDV_Email5.Text
                        .INDV_Occupation = txt_INDV_Occupation.Text
                        .INDV_Employer_Name = txt_INDV_Employer_Name.Text
                        .INDV_Deceased = Convert.ToBoolean(IIf(cb_Deceased.Checked = True, 1, 0))
                        If cb_Deceased.Checked Then
                            .INDV_Deceased_Date = Convert.ToDateTime(txt_deceased_date.RawValue)
                        Else
                            .INDV_Deceased_Date = Nothing
                        End If
                        .INDV_Tax_Number = txt_INDV_Tax_Number.Text
                        .INDV_Tax_Reg_Number = Convert.ToBoolean(IIf(cb_tax.Checked = True, 1, 0))
                        .INDV_Source_of_Wealth = txt_INDV_Source_of_Wealth.Text
                        .INDV_Comments = txt_INDV_Comments.Text

                    Else
                        .Corp_Name = txt_Corp_Name.Text
                        '.Corp_Role = cmb_Corp_Role.Value
                        .Corp_Commercial_Name = txt_Corp_Commercial_Name.Text
                        .Corp_Incorporation_Legal_Form = cmb_Corp_Incorporation_Legal_Form.SelectedItemValue
                        .Corp_Incorporation_Number = txt_Corp_Incorporation_number.Text
                        .Corp_Business = txt_Corp_Business.Text
                        .Corp_Email = txt_Corp_Email.Text
                        .Corp_Url = txt_Corp_url.Text
                        .Corp_Incorporation_State = txt_Corp_incorporation_state.Text
                        .Corp_Incorporation_Country_Code = cmb_Corp_Incorporation_Country_Code.SelectedItemValue
                        If txt_Corp_incorporation_date.RawValue IsNot Nothing Then
                            .Corp_Incorporation_Date = Convert.ToDateTime(txt_Corp_incorporation_date.RawValue)
                        Else
                            .Corp_Incorporation_Date = Nothing
                        End If
                        .Corp_Business_Closed = cbTutup.Value
                        If cbTutup.Checked Then
                            .Corp_Date_Business_Closed = Convert.ToDateTime(txt_Corp_date_business_closed.RawValue)
                        Else
                            .Corp_Date_Business_Closed = Nothing
                        End If
                        .Corp_Tax_Number = txt_Corp_tax_number.Text
                        .Corp_Comments = txt_Corp_Comments.Text
                    End If

                    .Active = True
                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .CreatedDate = DateTime.Now
                    .LastUpdateDate = DateTime.Now
                End With
            End If


            If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                objgoAML_CustomerBLL.SaveAddTanpaApproval(obj_customer, objListgoAML_Ref_Director, ObjModule, objListgoAML_Ref_Phone, objListgoAML_Ref_Identification, objListgoAML_Ref_Address, objListgoAML_Ref_Employer_Address, objListgoAML_Ref_Employer_Phone, objListgoAML_Ref_Director_Employer_Address, objListgoAML_Ref_Director_Employer_Phone, objListgoAML_Ref_Director_Address, objListgoAML_Ref_Director_Phone, objListgoAML_Ref_Identification_Director)
                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Saved into Database"
            Else
                objgoAML_CustomerBLL.SaveAddApproval(obj_customer, objListgoAML_Ref_Director, ObjModule, objListgoAML_Ref_Phone, objListgoAML_Ref_Identification, objListgoAML_Ref_Address, objListgoAML_Ref_Employer_Address, objListgoAML_Ref_Employer_Phone, objListgoAML_Ref_Director_Employer_Address, objListgoAML_Ref_Director_Employer_Phone, objListgoAML_Ref_Director_Address, objListgoAML_Ref_Director_Phone, objListgoAML_Ref_Identification_Director)
                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Saved into Pending Approval"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnBack_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub btnAddCustomerAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            cmb_kategoriaddress.IsReadOnly = False
            Address_INDV.ReadOnly = False
            Town_INDV.ReadOnly = False
            City_INDV.ReadOnly = False
            Zip_INDV.ReadOnly = False
            cmb_kodenegara.IsReadOnly = False
            State_INDVD.ReadOnly = False
            Comment_Address_INDVD.ReadOnly = False
            Btn_Save_Address.Hidden = False
            WindowDetailAddress.Title = "Customer Address Add"

            'Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Store_KategoriAddress.DataBind()

            'Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Kode_Negara.DataBind()


            ClearinputCustomerAddress()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddDirectorAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FP_Address_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            Director_cmb_kategoriaddress.IsReadOnly = False
            Director_Address.ReadOnly = False
            Director_Town.ReadOnly = False
            Director_City.ReadOnly = False
            Director_Zip.ReadOnly = False
            Director_cmb_kodenegara.IsReadOnly = False
            Director_State.ReadOnly = False
            Director_Comment_Address.ReadOnly = False
            Btn_Save_Director_Address.Hidden = False
            WindowDetailDirectorAddress.Title = "Director Address Add"

            'Director_Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Store_KategoriAddress.DataBind()

            'Director_Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Director_Store_Kode_Negara.DataBind()

            ClearinputDirectorAddress()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btnAddCustomerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            btnSavedetail.Hidden = False
            cb_phone_Contact_Type.IsReadOnly = False
            cb_phone_Communication_Type.IsReadOnly = False
            txt_phone_Country_prefix.ReadOnly = False
            txt_phone_number.ReadOnly = False
            txt_phone_extension.ReadOnly = False
            txt_phone_comments.ReadOnly = False
            btnSavedetail.Hidden = False
            WindowDetail.Title = "Customer Phone Add"

            Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing
            'Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Store_Contact_Type.DataBind()

            ' Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
            'Store_Communication_Type.DataBind()

            ClearinputCustomerPhones()
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddDirectorPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FormPanelDirectorPhone.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            btnSaveDirectorPhonedetail.Hidden = False
            Director_cb_phone_Contact_Type.IsReadOnly = False
            Director_cb_phone_Communication_Type.IsReadOnly = False
            Director_txt_phone_Country_prefix.ReadOnly = False
            Director_txt_phone_number.ReadOnly = False
            Director_txt_phone_extension.ReadOnly = False
            Director_txt_phone_comments.ReadOnly = False
            btnSaveDirectorPhonedetail.Hidden = False
            WindowDetailDirectorPhone.Title = "Director Phone Add"

            Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
            Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

            'Director_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Store_Contact_Type.DataBind()

            'Director_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
            'Director_Store_Communication_Type.DataBind()
            ClearinputDirectorPhones()
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()
            objTempCustomerPhoneEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelDirectorPhone.Hidden = True
            FormPanelEmpDirectorTaskDetail.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
            ClearinputDirectorEmpPhones()
            ClearinputDirectorPhones()
            objTempDirectorPhoneEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_INDVD.Hidden = True
            WindowDetailAddress.Hidden = True
            ClearinputCustomerAddress()
            objTempCustomerAddressEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_Director.Hidden = True
            FP_Address_Emp_Director.Hidden = True
            WindowDetailDirectorAddress.Hidden = True
            ClearinputDirectorAddress()
            ClearinputDirectorEmpAddress()
            objTempCustomerAddressEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Identification_INDV.Hidden = True
            WindowDetailIdentification.Hidden = True
            ClearinputCustomerIdentification()
            objTempCustomerIdentificationEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveDirectorIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Identification_Director.Hidden = True
            WindowDetailIdentification.Hidden = True
            ClearinputDirectorIdentification()
            objTempCustomerIdentificationEdit_Director = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddDirectorIdentifications_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FP_Identification_Director.Hidden = False
            WindowDetailIdentification.Hidden = False
            cmb_Jenis_Dokumen_Director.IsReadOnly = False
            txt_identification_comment_Director.ReadOnly = False
            txt_issued_by_Director.ReadOnly = False
            txt_issued_date_Director.ReadOnly = False
            txt_issued_expired_date_Director.ReadOnly = False
            cmb_issued_country_Director.IsReadOnly = False
            txt_number_identification_Director.ReadOnly = False
            btn_save_identification_Director.Hidden = False
            WindowDetailIdentification.Title = "Director Identification Add"

            'Storejenisdokumen_Director.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            'Storejenisdokumen_Director.DataBind()

            'StoreIssueCountry_Director.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'StoreIssueCountry_Director.DataBind()
            ClearinputDirectorIdentification()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomerAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomerAddressValid() Then
                If objTempCustomerAddressEdit Is Nothing Then
                    SaveAddAddressDetail()
                Else
                    SaveEditAddressDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveDirectorAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataDirectorAddressValid() Then
                If objTempDirectorAddressEdit Is Nothing Then
                    SaveAddDirectorAddressDetail()
                Else
                    SaveEditDirectorAddressDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomerEmpAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomerEmpAddressValid() Then
                If objTempEmployerAddressEdit Is Nothing Then
                    SaveAddEmpAddressDetail()
                Else
                    SaveEditEmpAddressDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveDirectorEmpAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataDirectorEmpAddressValid() Then
                If objTempDirectorEmployerAddressEdit Is Nothing Then
                    SaveAddDirectorEmpAddressDetail()
                Else
                    SaveEditDirectorEmpAddressDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btnSaveCustomerPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            If IsDataCustomerPhoneValid() Then
                If objTempCustomerPhoneEdit Is Nothing Then
                    SaveAddTaskDetail()
                Else
                    SaveEditTaskDetail()
                End If
            End If
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveDirectorPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            If IsDataDirectorPhoneValid() Then
                If objTempDirectorPhoneEdit Is Nothing Then
                    SaveAddDirectorTaskDetail()
                Else
                    SaveEditDirectorTaskDetail()
                End If
            End If
            FormPanelDirectorPhone.Hidden = True
            WindowDetailDirectorPhone.Hidden = True
            ClearinputDirectorPhones()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btnSaveCustomerEmpPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            If IsDataCustomerEmpPhoneValid() Then
                If objTempEmployerPhonesEdit Is Nothing Then
                    SaveAddEmpTaskDetail()
                Else
                    SaveEditEmpTaskDetail()
                End If
            End If
            FormPanelEmpTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerEmpPhones()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveDirectorEmpPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            If IsDataDirectorEmpPhoneValid() Then
                If objTempDirectorEmployerPhoneEdit Is Nothing Then
                    SaveAddDirectorEmpTaskDetail()
                Else
                    SaveEditDirectorEmpTaskDetail()
                End If
            End If
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SaveEditTaskDetail()

        With objTempCustomerPhoneEdit
            .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
            .tph_number = txt_phone_number.Text.Trim
            .tph_extension = txt_phone_extension.Text.Trim
            .comments = txt_phone_comments.Text.Trim
        End With

        With objTempgoAML_Ref_Phone
            .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
            .tph_number = txt_phone_number.Text.Trim
            .tph_extension = txt_phone_extension.Text.Trim
            .comments = txt_phone_comments.Text.Trim
        End With

        objTempCustomerPhoneEdit = Nothing
        objTempgoAML_Ref_Phone = Nothing

        If Cmb_TypePerson.SelectedItemValue = 1 Then
            Store_INDV_Phones.DataSource = objListgoAML_vw_Customer_Phones.ToList()
            Store_INDV_Phones.DataBind()
        Else
            Store_Phone_Corp.DataSource = objListgoAML_vw_Customer_Phones.ToList()
            Store_Phone_Corp.DataBind()
        End If

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerPhones()
    End Sub

    Sub SaveEditDirectorTaskDetail()

        With objTempDirectorPhoneEdit
            .Tph_Contact_Type = Director_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Director_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Director_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Director_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Director_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_txt_phone_number.Text.Trim
            .tph_extension = Director_txt_phone_extension.Text.Trim
            .comments = Director_txt_phone_comments.Text.Trim
        End With

        With objTempgoAML_Ref_Director_Phones
            .Tph_Contact_Type = Director_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Director_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Director_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_txt_phone_number.Text.Trim
            .tph_extension = Director_txt_phone_extension.Text.Trim
            .comments = Director_txt_phone_comments.Text.Trim
        End With

        objTempDirectorPhoneEdit = Nothing
        objTempgoAML_Ref_Director_Phones = Nothing

        Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
        Store_DirectorPhone.DataBind()

        FormPanelDirectorPhone.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorPhones()
    End Sub

    Sub SaveEditEmpTaskDetail()

        With objTempEmployerPhonesEdit
            .Tph_Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Emp_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Emp_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Emp_txt_phone_number.Text.Trim
            .tph_extension = Emp_txt_phone_extension.Text.Trim
            .comments = Emp_txt_phone_comments.Text.Trim
        End With

        With objTempgoAML_Ref_EmployerPhones
            .Tph_Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Emp_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Emp_txt_phone_number.Text.Trim
            .tph_extension = Emp_txt_phone_extension.Text.Trim
            .comments = Emp_txt_phone_comments.Text.Trim
        End With

        objTempEmployerPhonesEdit = Nothing
        objTempgoAML_Ref_EmployerPhones = Nothing

        Store_Empoloyer_Phone.DataSource = objListgoAML_vw_Employer_Phones.ToList()
        Store_Empoloyer_Phone.DataBind()

        FormPanelEmpTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerEmpPhones()
    End Sub

    Sub SaveEditDirectorEmpTaskDetail()

        With objTempDirectorEmployerPhoneEdit
            .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_Emp_txt_phone_number.Text.Trim
            .tph_extension = Director_Emp_txt_phone_extension.Text.Trim
            .comments = Director_Emp_txt_phone_comments.Text.Trim
        End With

        With objTempgoAML_Ref_Director_EmployerPhones
            .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_Emp_txt_phone_number.Text.Trim
            .tph_extension = Director_Emp_txt_phone_extension.Text.Trim
            .comments = Director_Emp_txt_phone_comments.Text.Trim
        End With

        objTempDirectorEmployerPhoneEdit = Nothing
        objTempgoAML_Ref_Director_EmployerPhones = Nothing

        Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
        Store_Director_Employer_Phone.DataBind()

        FormPanelEmpDirectorTaskDetail.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorEmpPhones()
    End Sub

    Sub SaveEditAddressDetail()

        With objTempCustomerAddressEdit
            .Address_Type = cmb_kategoriaddress.SelectedItemValue
            .Type_Address_Detail = cmb_kategoriaddress.SelectedItemText
            .Address = Address_INDV.Value
            .City = City_INDV.Value
            .Town = Town_INDV.Value
            .Country_Code = cmb_kodenegara.SelectedItemValue
            .Country = cmb_kodenegara.SelectedItemText
            .State = State_INDVD.Value
            .Zip = Zip_INDV.Value
            .Comments = Comment_Address_INDVD.Text.Trim
        End With

        With objTempgoAML_Ref_Address
            .Address_Type = cmb_kategoriaddress.SelectedItemValue
            .City = City_INDV.Value
            .Address = Address_INDV.Value
            .Town = Town_INDV.Value
            .Country_Code = cmb_kodenegara.SelectedItemValue
            .State = State_INDVD.Value
            .Zip = Zip_INDV.Value
            .Comments = Comment_Address_INDVD.Text.Trim
        End With

        objTempCustomerAddressEdit = Nothing
        objTempgoAML_Ref_Address = Nothing

        If Cmb_TypePerson.SelectedItemValue = 1 Then
            Store_INDV_Addresses.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
            Store_INDV_Addresses.DataBind()
        Else
            Store_Address_Corp.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
            Store_Address_Corp.DataBind()
        End If

        FP_Address_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerAddress()
    End Sub

    Sub SaveEditDirectorAddressDetail()

        With objTempDirectorAddressEdit
            .Address_Type = Director_cmb_kategoriaddress.SelectedItemValue
            .Type_Address_Detail = Director_cmb_kategoriaddress.SelectedItemText
            .Address = Director_Address.Value
            .City = Director_City.Value
            .Town = Director_Town.Value
            .Country = Director_cmb_kodenegara.SelectedItemText
            .Country_Code = Director_cmb_kodenegara.SelectedItemValue
            .State = Director_State.Value
            .Zip = Director_Zip.Value
            .Comments = Director_Comment_Address.Text.Trim
        End With

        With objTempgoAML_Ref_Director_Addresses
            .Address_Type = Director_cmb_kategoriaddress.SelectedItemValue '
            .City = Director_City.Value
            .Address = Director_Address.Value
            .Town = Director_Town.Value
            .Country_Code = Director_cmb_kodenegara.SelectedItemValue
            .State = Director_State.Value
            .Zip = Director_Zip.Value
            .Comments = Director_Comment_Address.Text.Trim
        End With

        objTempDirectorAddressEdit = Nothing
        objTempgoAML_Ref_Director_Addresses = Nothing

        Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList()
        Store_DirectorAddress.DataBind()

        FP_Address_Director.Hidden = True
        WindowDetailDirectorAddress.Hidden = True
        ClearinputDirectorAddress()
    End Sub


    Sub SaveEditEmpAddressDetail()

        With objTempEmployerAddressEdit
            .Address_Type = cmb_emp_kategoriaddress.SelectedItemValue
            .Type_Address_Detail = cmb_emp_kategoriaddress.SelectedItemText
            .Address = emp_Address_INDV.Value
            .City = emp_City_INDV.Value
            .Town = emp_Town_INDV.Value
            .Country_Code = cmb_emp_kodenegara.SelectedItemValue
            .Comments = emp_Comment_Address_INDVD.Value
            .Country = cmb_emp_kodenegara.SelectedItemText
            .State = emp_State_INDVD.Value
            .Zip = emp_Zip_INDV.Value
            .Comments = emp_Comment_Address_INDVD.Text.Trim
        End With

        With objTempgoAML_Ref_EmployerAddresses
            .Address_Type = cmb_emp_kategoriaddress.SelectedItemValue '
            .City = emp_City_INDV.Value
            .Address = emp_Address_INDV.Value
            .Town = emp_Town_INDV.Value
            .Country_Code = cmb_emp_kodenegara.SelectedItemValue
            .Comments = emp_Comment_Address_INDVD.Value
            .State = emp_State_INDVD.Value
            .Zip = emp_Zip_INDV.Value
            .Comments = emp_Comment_Address_INDVD.Text.Trim
        End With

        objTempEmployerAddressEdit = Nothing
        objTempgoAML_Ref_EmployerAddresses = Nothing

        Store_Employer_Address.DataSource = objListgoAML_vw_Employer_Addresses.ToList()
        Store_Employer_Address.DataBind()

        FP_Address_Emp_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerEmpAddress()
    End Sub

    Sub SaveEditDirectorEmpAddressDetail()

        With objTempDirectorEmployerAddressEdit
            .Address_Type = Director_cmb_emp_kategoriaddress.SelectedItemValue
            .Type_Address_Detail = Director_cmb_emp_kategoriaddress.SelectedItemText
            .Address = Director_emp_Address.Value
            .City = Director_emp_City.Value
            .Town = Director_emp_Town.Value
            .Country_Code = Director_cmb_emp_kodenegara.SelectedItemValue
            .Country = Director_cmb_emp_kodenegara.SelectedItemText
            .State = Director_emp_State.Value
            .Zip = Director_emp_Zip.Value
            .Comments = Director_emp_Comment_Address.Text.Trim
        End With

        With objTempgoAML_Ref_Director_EmployerAddresses
            .Address_Type = Director_cmb_emp_kategoriaddress.SelectedItemValue '
            .City = Director_emp_City.Value
            .Address = Director_emp_Address.Value
            .Town = Director_emp_Town.Value
            .Country_Code = Director_cmb_emp_kodenegara.SelectedItemValue
            .State = Director_emp_State.Value
            .Zip = Director_emp_Zip.Value
            .Comments = Director_emp_Comment_Address.Text.Trim
        End With

        objTempDirectorEmployerAddressEdit = Nothing
        objTempgoAML_Ref_Director_EmployerAddresses = Nothing

        Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList()
        Store_Director_Employer_Address.DataBind()

        FP_Address_Emp_Director.Hidden = True
        WindowDetailDirectorAddress.Hidden = True
        ClearinputDirectorEmpAddress()
    End Sub


    Sub SaveAddTaskDetail()

        Dim objNewVWgoAML_Customer As New NawaDevDAL.Vw_Customer_Phones
        Dim objNewgoAML_Customer As New NawaDevDAL.goAML_Ref_Phone
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Phone = intpk

            .FK_Ref_Detail_Of = 1
            .FK_for_Table_ID = obj_customer.PK_Customer_ID
            .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
            .tph_number = txt_phone_number.Text.Trim
            .tph_extension = txt_phone_extension.Text.Trim
            .comments = txt_phone_comments.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_goAML_Ref_Phone = intpk
            .FK_Ref_Detail_Of = 1
            .FK_for_Table_ID = obj_customer.PK_Customer_ID
            .Tph_Contact_Type = cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = txt_phone_Country_prefix.Text.Trim
            .tph_number = txt_phone_number.Text.Trim
            .tph_extension = txt_phone_extension.Text.Trim
            .comments = txt_phone_comments.Text.Trim
        End With

        objListgoAML_vw_Customer_Phones.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Phone.Add(objNewgoAML_Customer)

        If Cmb_TypePerson.SelectedItemValue = 1 Then
            Store_INDV_Phones.DataSource = objListgoAML_vw_Customer_Phones.ToList()
            Store_INDV_Phones.DataBind()
        Else
            Store_Phone_Corp.DataSource = objListgoAML_vw_Customer_Phones.ToList()
            Store_Phone_Corp.DataBind()
        End If

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerPhones()
    End Sub

    Sub SaveAddDirectorTaskDetail()

        Dim objNewVWgoAML_Customer As New NawaDevDAL.Vw_Director_Phones
        Dim objNewgoAML_Customer As New NawaDevDAL.goAML_Ref_Phone
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Phone = intpk

            .FK_Ref_Detail_Of = 6
            .FK_for_Table_ID = tempflag
            .Tph_Contact_Type = Director_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Director_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Director_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Director_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Director_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_txt_phone_number.Text.Trim
            .tph_extension = Director_txt_phone_extension.Text.Trim
            .comments = Director_txt_phone_comments.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_goAML_Ref_Phone = intpk
            .FK_Ref_Detail_Of = 6
            .FK_for_Table_ID = tempflag
            .Tph_Contact_Type = Director_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Director_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Director_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_txt_phone_number.Text.Trim
            .tph_extension = Director_txt_phone_extension.Text.Trim
            .comments = Director_txt_phone_comments.Text.Trim
        End With

        objListgoAML_vw_Director_Phones.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Director_Phone.Add(objNewgoAML_Customer)


        Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
        Store_DirectorPhone.DataBind()

        FormPanelDirectorPhone.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorPhones()
    End Sub


    Sub SaveAddEmpTaskDetail()

        Dim objNewVWgoAML_Customer As New NawaDevDAL.Vw_Employer_Phones
        Dim objNewgoAML_Customer As New NawaDevDAL.goAML_Ref_Phone
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Phone = intpk

            .FK_Ref_Detail_Of = 5
            .FK_for_Table_ID = obj_customer.PK_Customer_ID
            .Tph_Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Emp_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Emp_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Emp_txt_phone_number.Text.Trim
            .tph_extension = Emp_txt_phone_extension.Text.Trim
            .comments = Emp_txt_phone_comments.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_goAML_Ref_Phone = intpk
            .FK_Ref_Detail_Of = 5
            .FK_for_Table_ID = obj_customer.PK_Customer_ID
            .Tph_Contact_Type = Emp_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Emp_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Emp_txt_phone_number.Text.Trim
            .tph_extension = Emp_txt_phone_extension.Text.Trim
            .comments = Emp_txt_phone_comments.Text.Trim
        End With

        objListgoAML_vw_Employer_Phones.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Employer_Phone.Add(objNewgoAML_Customer)

        Store_Empoloyer_Phone.DataSource = objListgoAML_vw_Employer_Phones.ToList()
        Store_Empoloyer_Phone.DataBind()

        FormPanelEmpTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerEmpPhones()
    End Sub

    Sub SaveAddDirectorEmpTaskDetail()

        Dim objNewVWgoAML_Customer As New NawaDevDAL.Vw_Director_Employer_Phones
        Dim objNewgoAML_Customer As New NawaDevDAL.goAML_Ref_Phone
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Phone = intpk

            .FK_Ref_Detail_Of = 7
            .FK_for_Table_ID = tempflag
            .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemValue
            .Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemText
            .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemValue
            .Communcation_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemText
            .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_Emp_txt_phone_number.Text.Trim
            .tph_extension = Director_Emp_txt_phone_extension.Text.Trim
            .comments = Director_Emp_txt_phone_comments.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_goAML_Ref_Phone = intpk
            .FK_Ref_Detail_Of = 7
            .FK_for_Table_ID = tempflag
            .Tph_Contact_Type = Director_Emp_cb_phone_Contact_Type.SelectedItemValue
            .Tph_Communication_Type = Director_Emp_cb_phone_Communication_Type.SelectedItemValue
            .tph_country_prefix = Director_Emp_txt_phone_Country_prefix.Text.Trim
            .tph_number = Director_Emp_txt_phone_number.Text.Trim
            .tph_extension = Director_Emp_txt_phone_extension.Text.Trim
            .comments = Director_Emp_txt_phone_comments.Text.Trim
        End With

        objListgoAML_vw_Director_Employer_Phones.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Director_Employer_Phone.Add(objNewgoAML_Customer)

        Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
        Store_Director_Employer_Phone.DataBind()

        FormPanelEmpDirectorTaskDetail.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorEmpPhones()
    End Sub

    Sub SaveAddAddressDetail()

        Dim objNewVWgoAML_Customer As New NawaDevDAL.Vw_Customer_Addresses
        Dim objNewgoAML_Customer As New NawaDevDAL.goAML_Ref_Address
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 1
            .Address_Type = cmb_kategoriaddress.SelectedItemValue
            .Address = Address_INDV.Value
            .City = City_INDV.Value
            .Town = Town_INDV.Value
            .Country_Code = cmb_kodenegara.SelectedItemValue
            .Comments = Comment_Address_INDVD.Value
            .State = State_INDVD.Value
            .Zip = Zip_INDV.Value
            .Type_Address_Detail = cmb_kategoriaddress.SelectedItemText
            .Country = cmb_kodenegara.SelectedItemText
            .Comments = Comment_Address_INDVD.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 1
            .FK_To_Table_ID = obj_customer.PK_Customer_ID
            .Address_Type = cmb_kategoriaddress.SelectedItemValue
            .City = City_INDV.Value
            .Address = Address_INDV.Value
            .Town = Town_INDV.Value
            .Country_Code = cmb_kodenegara.SelectedItemValue
            .Comments = Comment_Address_INDVD.Value
            .State = State_INDVD.Value
            .Zip = Zip_INDV.Value
            .Comments = Comment_Address_INDVD.Text.Trim
        End With

        objListgoAML_vw_Customer_Addresses.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Address.Add(objNewgoAML_Customer)

        If Cmb_TypePerson.SelectedItemValue = 1 Then
            Store_INDV_Addresses.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
            Store_INDV_Addresses.DataBind()
        Else
            Store_Address_Corp.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
            Store_Address_Corp.DataBind()
        End If

        FP_Address_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerAddress()
    End Sub

    Sub SaveAddDirectorAddressDetail()

        Dim objNewVWgoAML_Customer As New NawaDevDAL.Vw_Director_Addresses
        Dim objNewgoAML_Customer As New NawaDevDAL.goAML_Ref_Address
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 6
            .FK_To_Table_ID = tempflag
            .Address_Type = Director_cmb_kategoriaddress.SelectedItemValue
            .Address = Director_Address.Value
            .City = Director_City.Value
            .Town = Director_Town.Value
            .Country_Code = Director_cmb_kodenegara.SelectedItemValue
            .State = Director_State.Value
            .Zip = Director_Zip.Value
            .Type_Address_Detail = Director_cmb_kategoriaddress.SelectedItemText
            .Country = Director_cmb_kodenegara.SelectedItemText
            .Comments = Director_Comment_Address.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 6
            .FK_To_Table_ID = tempflag
            .Address_Type = Director_cmb_kategoriaddress.SelectedItemValue '
            .City = Director_City.Value
            .Address = Director_Address.Value
            .Town = Director_Town.Value
            .Country_Code = Director_cmb_kodenegara.SelectedItemValue
            .State = Director_State.Value
            .Zip = Director_Zip.Value
            .Comments = Director_Comment_Address.Text.Trim
        End With

        objListgoAML_vw_Director_Addresses.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Director_Address.Add(objNewgoAML_Customer)

        Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList()
        Store_DirectorAddress.DataBind()

        FP_Address_Director.Hidden = True
        WindowDetailDirectorAddress.Hidden = True
        ClearinputDirectorAddress()
    End Sub

    Sub SaveAddEmpAddressDetail()

        Dim objNewVWgoAML_Customer As New NawaDevDAL.Vw_Employer_Addresses
        Dim objNewgoAML_Customer As New NawaDevDAL.goAML_Ref_Address
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 5
            .Address_Type = cmb_emp_kategoriaddress.SelectedItemValue
            .Address = emp_Address_INDV.Value
            .City = emp_City_INDV.Value
            .Town = emp_Town_INDV.Value
            .Country_Code = cmb_emp_kodenegara.SelectedItemValue
            .Comments = emp_Comment_Address_INDVD.Value
            .State = emp_State_INDVD.Value
            .Zip = emp_Zip_INDV.Value
            .Type_Address_Detail = cmb_emp_kategoriaddress.SelectedItemText
            .Country = cmb_emp_kodenegara.SelectedItemText
            .Comments = emp_Comment_Address_INDVD.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 5
            .FK_To_Table_ID = obj_customer.PK_Customer_ID
            .Address_Type = cmb_emp_kategoriaddress.SelectedItemValue '
            .City = emp_City_INDV.Value
            .Address = emp_Address_INDV.Value
            .Town = emp_Town_INDV.Value
            .Country_Code = cmb_emp_kodenegara.SelectedItemValue
            .Comments = emp_Comment_Address_INDVD.Value
            .State = emp_State_INDVD.Value
            .Zip = emp_Zip_INDV.Value
            .Comments = emp_Comment_Address_INDVD.Text.Trim
        End With

        objListgoAML_vw_Employer_Addresses.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Employer_Address.Add(objNewgoAML_Customer)

        Store_Employer_Address.DataSource = objListgoAML_vw_Employer_Addresses.ToList()
        Store_Employer_Address.DataBind()

        FP_Address_Emp_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerEmpAddress()
    End Sub

    Sub SaveAddDirectorEmpAddressDetail()

        Dim objNewVWgoAML_Customer As New NawaDevDAL.Vw_Director_Employer_Addresses
        Dim objNewgoAML_Customer As New NawaDevDAL.goAML_Ref_Address
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Director_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 7
            .FK_To_Table_ID = tempflag
            .Address_Type = Director_cmb_emp_kategoriaddress.SelectedItemValue
            .Address = Director_emp_Address.Value
            .City = Director_emp_City.Value
            .Town = Director_emp_Town.Value
            .Country_Code = Director_cmb_emp_kodenegara.SelectedItemValue
            .State = Director_emp_State.Value
            .Zip = Director_emp_Zip.Value
            .Type_Address_Detail = Director_cmb_emp_kategoriaddress.SelectedItemText
            .Country = Director_cmb_emp_kodenegara.SelectedItemText
            .Comments = Director_emp_Comment_Address.Text.Trim
        End With

        With objNewgoAML_Customer
            .PK_Customer_Address_ID = intpk
            .FK_Ref_Detail_Of = 7
            .FK_To_Table_ID = tempflag
            .Address_Type = Director_cmb_emp_kategoriaddress.SelectedItemValue '
            .City = Director_emp_City.Value
            .Address = Director_emp_Address.Value
            .Town = Director_emp_Town.Value
            .Country_Code = Director_cmb_emp_kodenegara.SelectedItemValue
            .State = Director_emp_State.Value
            .Zip = Director_emp_Zip.Value
            .Comments = Director_emp_Comment_Address.Text.Trim
        End With

        objListgoAML_vw_Director_Employer_Addresses.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Director_Employer_Address.Add(objNewgoAML_Customer)

        Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList()
        Store_Director_Employer_Address.DataBind()

        FP_Address_Emp_Director.Hidden = True
        WindowDetailDirectorAddress.Hidden = True
        ClearinputDirectorEmpAddress()
    End Sub

    Function IsDataCustomerPhoneValid() As Boolean
        If cb_phone_Contact_Type.SelectedItemValue = "" Then
            Throw New Exception("Contact Type is required")
        End If
        If cb_phone_Communication_Type.SelectedItemValue = "" Then
            Throw New Exception("Communication Type is required")
        End If
        If txt_phone_Country_prefix.Text.Length > 3 Then
            Throw New Exception("Max Country Prefix is 3")
        End If
        If txt_phone_extension.Text.Length > 9 Then
            Throw New Exception("Max Phone Extension is 9")
        End If
        If txt_phone_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(txt_phone_number.Text) Then
            Throw New Exception("Phone Number is Invalid")
        End If
        'End Update
        Return True
    End Function
    'Update: Zikri_08102020
    Private Function IsNumber(text As String) As Boolean
        Try

            Dim number As New Regex("^[0-9 ]+$")
            Return number.IsMatch(text)

        Catch ex As Exception
            Return False
        End Try
    End Function
    'End Update
    Function IsDataDirectorPhoneValid() As Boolean
        If Director_cb_phone_Contact_Type.SelectedItemValue = "" Then
            Throw New Exception("Contact Type is required")
        End If
        If Director_cb_phone_Communication_Type.SelectedItemValue = "" Then
            Throw New Exception("Communication Type is required")
        End If
        If Director_txt_phone_Country_prefix.Text.Length > 3 Then
            Throw New Exception("Max Country Prefix is 3")
        End If
        If Director_txt_phone_extension.Text.Length > 9 Then
            Throw New Exception("Max Phone Extension is 9")
        End If
        If Director_txt_phone_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(Director_txt_phone_number.Text) Then
            Throw New Exception("Phone Number is Invalid")
        End If
        'End Update
        Return True
    End Function

    Function IsDataCustomerEmpPhoneValid() As Boolean
        If Emp_cb_phone_Contact_Type.SelectedItemValue = "" Then
            Throw New Exception("Contact Type is required")
        End If
        If Emp_cb_phone_Communication_Type.SelectedItemValue = "" Then
            Throw New Exception("Communication Type is required")
        End If
        If Emp_txt_phone_Country_prefix.Text.Length > 3 Then
            Throw New Exception("Max Country Prefix is 3")
        End If
        If Emp_txt_phone_extension.Text.Length > 9 Then
            Throw New Exception("Max Phone Extension is 9")
        End If
        If Emp_txt_phone_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(Emp_txt_phone_number.Text) Then
            Throw New Exception("Phone Number is Invalid")
        End If
        'End Update
        Return True
    End Function

    Function IsDataDirectorEmpPhoneValid() As Boolean
        If Director_Emp_cb_phone_Contact_Type.SelectedItemValue = "" Then
            Throw New Exception("Contact Type is required")
        End If
        If Director_Emp_cb_phone_Communication_Type.SelectedItemValue = "" Then
            Throw New Exception("Communication Type is required")
        End If
        If Director_Emp_txt_phone_Country_prefix.Text.Length > 3 Then
            Throw New Exception("Max Country Prefix is 3")
        End If
        If Director_Emp_txt_phone_extension.Text.Length > 9 Then
            Throw New Exception("Max Phone Extension is 9")
        End If
        If Director_Emp_txt_phone_number.Text.Trim = "" Then
            Throw New Exception("Number is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(Director_Emp_txt_phone_number.Text) Then
            Throw New Exception("Phone Number is Invalid")
        End If
        'End Update
        Return True
    End Function


    Function IsDataCustomerAddressValid() As Boolean
        If cmb_kodenegara.SelectedItemValue = "" Then
            Throw New Exception("Country Code is required")
        End If
        If cmb_kategoriaddress.SelectedItemValue = "" Then
            Throw New Exception("Address Type is required")
        End If
        If Address_INDV.Text.Trim = "" Then
            Throw New Exception("Address is required")
        End If
        If City_INDV.Text.Trim = "" Then
            Throw New Exception("City is required")
        End If
        ''Add 09-Jan-2023, Felix tambah validasi Kalau Kode negara = ID, State wajib
        If cmb_kodenegara.SelectedItemValue = "ID" And State_INDVD.Text.Trim = "" Then
            Throw New Exception("If Country Code is ID (Indonesia), State (Provinsi) is required")
        End If
        ''End 09-Jan-2023
        Return True
    End Function

    Function IsDataDirectorAddressValid() As Boolean
        If Director_cmb_kodenegara.SelectedItemValue = "" Then
            Throw New Exception("Country Code is required")
        End If
        If Director_cmb_kategoriaddress.SelectedItemValue = "" Then
            Throw New Exception("Address Type is required")
        End If
        If Director_Address.Text.Trim = "" Then
            Throw New Exception("Address is required")
        End If
        If Director_City.Text.Trim = "" Then
            Throw New Exception("City is required")
        End If
        ''Add 09-Jan-2023, Felix tambah validasi Kalau Kode negara = ID, State wajib
        If Director_cmb_kodenegara.SelectedItemValue = "ID" And Director_State.Text.Trim = "" Then
            Throw New Exception("If Country Code is ID (Indonesia), State (Provinsi) is required")
        End If
        ''End 09-Jan-2023
        Return True
    End Function


    Function IsDataCustomerEmpAddressValid() As Boolean
        If cmb_emp_kodenegara.SelectedItemValue = "" Then
            Throw New Exception("Country Code is required")
        End If
        If cmb_emp_kategoriaddress.SelectedItemValue = "" Then
            Throw New Exception("Address Type is required")
        End If
        If emp_Address_INDV.Text.Trim = "" Then
            Throw New Exception("Address is required")
        End If
        If emp_City_INDV.Text.Trim = "" Then
            Throw New Exception("City is required")
        End If
        ''Add 09-Jan-2023, Felix tambah validasi Kalau Kode negara = ID, State wajib
        If cmb_emp_kodenegara.SelectedItemValue = "ID" And emp_State_INDVD.Text.Trim = "" Then
            Throw New Exception("If Country Code is ID (Indonesia), State (Provinsi) is required")
        End If
        ''End 09-Jan-2023
        Return True
    End Function

    Function IsDataDirectorEmpAddressValid() As Boolean
        If Director_cmb_emp_kodenegara.SelectedItemValue = "" Then
            Throw New Exception("Country Code is required")
        End If
        If Director_cmb_emp_kategoriaddress.SelectedItemValue = "" Then
            Throw New Exception("Address Type is required")
        End If
        If Director_emp_Address.Text.Trim = "" Then
            Throw New Exception("Address is required")
        End If
        If Director_emp_City.Text.Trim = "" Then
            Throw New Exception("City is required")
        End If
        ''Add 09-Jan-2023, Felix tambah validasi Kalau Kode negara = ID, State wajib
        If Director_cmb_emp_kodenegara.SelectedItemValue = "ID" And Director_emp_State.Text.Trim = "" Then
            Throw New Exception("If Country Code is ID (Indonesia), State (Provinsi) is required")
        End If
        ''End 09-Jan-2023
        Return True
    End Function

    Sub ClearinputCustomerPhones()
        cb_phone_Contact_Type.SetTextValue("")
        cb_phone_Communication_Type.SetTextValue("")
        txt_phone_Country_prefix.Text = ""
        txt_phone_number.Text = ""
        txt_phone_extension.Text = ""
        txt_phone_comments.Text = ""

    End Sub

    Sub ClearinputDirectorPhones()
        Director_cb_phone_Contact_Type.SetTextValue("")
        Director_cb_phone_Communication_Type.SetTextValue("")
        Director_txt_phone_Country_prefix.Text = ""
        Director_txt_phone_number.Text = ""
        Director_txt_phone_extension.Text = ""
        Director_txt_phone_comments.Text = ""

    End Sub

    Sub ClearinputCustomerEmpPhones()
        Emp_cb_phone_Contact_Type.SetTextValue("")
        Emp_cb_phone_Communication_Type.SetTextValue("")
        Emp_txt_phone_Country_prefix.Text = ""
        Emp_txt_phone_number.Text = ""
        Emp_txt_phone_extension.Text = ""
        Emp_txt_phone_comments.Text = ""
    End Sub


    Sub ClearinputDirectorEmpPhones()
        Director_Emp_cb_phone_Contact_Type.SetTextValue("")
        Director_Emp_cb_phone_Communication_Type.SetTextValue("")
        Director_Emp_txt_phone_Country_prefix.Text = ""
        Director_Emp_txt_phone_number.Text = ""
        Director_Emp_txt_phone_extension.Text = ""
        Director_Emp_txt_phone_comments.Text = ""

    End Sub

    Sub ClearinputCustomerAddress()
        cmb_kategoriaddress.SetTextValue("")
        cmb_kodenegara.SetTextValue("")
        Address_INDV.Text = ""
        Town_INDV.Text = ""
        City_INDV.Text = ""
        Zip_INDV.Text = ""
        State_INDVD.Text = ""
        Comment_Address_INDVD.Text = ""

    End Sub

    Sub ClearinputDirectorAddress()
        Director_cmb_kategoriaddress.SetTextValue("")
        Director_cmb_kodenegara.SetTextValue("")
        Director_Address.Text = ""
        Director_Town.Text = ""
        Director_City.Text = ""
        Director_Zip.Text = ""
        Director_State.Text = ""
        Director_Comment_Address.Text = ""

    End Sub

    Sub ClearinputCustomerEmpAddress()
        cmb_emp_kategoriaddress.SetTextValue("")
        cmb_emp_kodenegara.SetTextValue("")
        emp_Address_INDV.Text = ""
        emp_Town_INDV.Text = ""
        emp_City_INDV.Text = ""
        emp_Zip_INDV.Text = ""
        emp_State_INDVD.Text = ""
        emp_Comment_Address_INDVD.Text = ""

    End Sub

    Sub ClearinputDirectorEmpAddress()
        Director_cmb_emp_kategoriaddress.SetTextValue("")
        Director_cmb_emp_kodenegara.SetTextValue("")
        Director_emp_Address.Text = ""
        Director_emp_Town.Text = ""
        Director_emp_City.Text = ""
        Director_emp_Zip.Text = ""
        Director_emp_State.Text = ""
        Director_emp_Comment_Address.Text = ""

    End Sub

    Sub GridPanelSetting()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 2 Then
            GP_DirectorIdentification.ColumnModel.Columns.RemoveAt(GP_DirectorIdentification.ColumnModel.Columns.Count - 1)
            GP_DirectorIdentification.ColumnModel.Columns.Insert(1, CommandColumn13)
            GP_DirectorPhone.ColumnModel.Columns.RemoveAt(GP_DirectorPhone.ColumnModel.Columns.Count - 1)
            GP_DirectorPhone.ColumnModel.Columns.Insert(1, CommandColumn11)
            GP_DirectorAddress.ColumnModel.Columns.RemoveAt(GP_DirectorAddress.ColumnModel.Columns.Count - 1)
            GP_DirectorAddress.ColumnModel.Columns.Insert(1, CommandColumn12)
            GP_DirectorEmpPhone.ColumnModel.Columns.RemoveAt(GP_DirectorEmpPhone.ColumnModel.Columns.Count - 1)
            GP_DirectorEmpPhone.ColumnModel.Columns.Insert(1, CommandColumn9)
            GP_DirectorEmpAddress.ColumnModel.Columns.RemoveAt(GP_DirectorEmpAddress.ColumnModel.Columns.Count - 1)
            GP_DirectorEmpAddress.ColumnModel.Columns.Insert(1, CommandColumn10)
            gp_INDV_Phones.ColumnModel.Columns.RemoveAt(gp_INDV_Phones.ColumnModel.Columns.Count - 1)
            gp_INDV_Phones.ColumnModel.Columns.Insert(1, CommandColumn1)
            gp_INDV_Address.ColumnModel.Columns.RemoveAt(gp_INDV_Address.ColumnModel.Columns.Count - 1)
            gp_INDV_Address.ColumnModel.Columns.Insert(1, CommandColumn2)
            GP_INDVD_Identification.ColumnModel.Columns.RemoveAt(GP_INDVD_Identification.ColumnModel.Columns.Count - 1)
            GP_INDVD_Identification.ColumnModel.Columns.Insert(1, CommandColumn5)
            GP_Empoloyer_Phone.ColumnModel.Columns.RemoveAt(GP_Empoloyer_Phone.ColumnModel.Columns.Count - 1)
            GP_Empoloyer_Phone.ColumnModel.Columns.Insert(1, CommandColumn6)
            GP_Employer_Address.ColumnModel.Columns.RemoveAt(GP_Employer_Address.ColumnModel.Columns.Count - 1)
            GP_Employer_Address.ColumnModel.Columns.Insert(1, CommandColumn7)
            GP_Phone_Corp.ColumnModel.Columns.RemoveAt(GP_Phone_Corp.ColumnModel.Columns.Count - 1)
            GP_Phone_Corp.ColumnModel.Columns.Insert(1, CommandColumn3)
            GP_Address_Corp.ColumnModel.Columns.RemoveAt(GP_Address_Corp.ColumnModel.Columns.Count - 1)
            GP_Address_Corp.ColumnModel.Columns.Insert(1, CommandColumn4)
            GP_Corp_Director.ColumnModel.Columns.RemoveAt(GP_Corp_Director.ColumnModel.Columns.Count - 1)
            GP_Corp_Director.ColumnModel.Columns.Insert(1, CommandColumn8)



        End If

    End Sub

    Sub Maximizeable()
        WindowDetailDirector.Maximizable = True
        WindowDetail.Maximizable = True
        WindowDetailAddress.Maximizable = True
        WindowDetailIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True

    End Sub

    Private Sub LoadGender()
        'cmb_INDV_Gender.PageSize = SystemParameterBLL.GetPageSize
        ' StoreINDVGender.Reload()
        'cmb_Director_Gender.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Gender.Reload()
    End Sub

    Protected Sub Gender_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Kelamin", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub LoadKontak()
        'cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        'Store_Contact_Type.Reload()

        'Emp_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        'Emp_Store_Contact_Type.Reload()

        'Director_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_Contact_Type.Reload()

        'Director_Emp_cb_phone_Contact_Type.PageSize = SystemParameterBLL.GetPageSize
        'Director_Emp_Store_Contact_Type.Reload()


    End Sub

    Private Sub LoadJenisAlatKomunikasi()
        'Director_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_Communication_Type.Reload()
        'Director_Emp_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        'Director_Emp_Store_Communication_Type.Reload()
        'cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        'Store_Communication_Type.Reload()
        'Emp_cb_phone_Communication_Type.PageSize = SystemParameterBLL.GetPageSize
        'Emp_Store_Communication_Type.Reload()
    End Sub

    Private Sub LoadKategoriKontak()
        'cmb_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        'Store_KategoriAddress.Reload()
        'cmb_emp_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        'Store_emp_KategoriAddress.Reload()
        'Director_cmb_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_KategoriAddress.Reload()
        'Director_cmb_emp_kategoriaddress.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_emp_KategoriAddress.Reload()
    End Sub

    Protected Sub Communication_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Alat_Komunikasi", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub Contact_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Kategori_Kontak", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub LoadNegara()

        'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
        'cmb_INDV_Passport_country.PageSize = SystemParameterBLL.GetPageSize
        'Store_INDV_PassportCountry.Reload()
        'cmb_Director_Passport_Country.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_PassportCountry.Reload()
        'End Update
        'cmb_Director_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Nationality1.Reload()
        'cmb_INDV_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        'StoreNationality1.Reload()
        'cmb_Director_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Nationality2.Reload()
        'cmb_INDV_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        ' StoreNationality2.Reload()
        'cmb_Director_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Nationality3.Reload()
        'cmb_INDV_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        'StoreNationality3.Reload()
        'cmb_Director_Residence.PageSize = SystemParameterBLL.GetPageSize
        'Store_Director_Residence.Reload()
        'cmb_INDV_Residence.PageSize = SystemParameterBLL.GetPageSize
        ' StoreResidence.Reload()
        ' Director_cmb_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_Kode_Negara.Reload()
        'Director_cmb_emp_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'Director_Store_emp_Kode_Negara.Reload()

        'cmb_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'Store_Kode_Negara.Reload()
        'cmb_emp_kodenegara.PageSize = SystemParameterBLL.GetPageSize
        'Store_emp_Kode_Negara.Reload()

        'cmb_issued_country_Director.PageSize = SystemParameterBLL.GetPageSize
        'StoreIssueCountry_Director.Reload()
        'cmb_issued_country.PageSize = SystemParameterBLL.GetPageSize
        'Store_issued_country.Reload()


    End Sub

    Private Sub LoadTypeIdentitas()
        'cmb_Jenis_Dokumen.PageSize = SystemParameterBLL.GetPageSize
        'Store_jenis_dokumen.Reload()

        'cmb_Jenis_Dokumen_Director.PageSize = SystemParameterBLL.GetPageSize
        'Storejenisdokumen_Director.Reload()
    End Sub


    Protected Sub Identification_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goaml_ref_jenis_dokumen_identitas", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub National_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Nama_Negara", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub CUSTOMER_Add_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                'Dedy added 31082020 aktif is hidden
                Cmb_StatusCode.Hidden = True
                LoadNegara()
                LoadGender()
                LoadKontak()
                LoadJenisAlatKomunikasi()
                LoadKategoriKontak()
                LoadTypeIdentitas()

                'Dedy end added 31082020
                ClearSession()
                GridPanelSetting()
                Maximizeable()
                'StoreTypePerson.DataSource = goAML_CustomerBLL.GetListCustomerType
                'StoreTypePerson.DataBind()

                StoreStatusCode.DataSource = goAML_CustomerBLL.GetListStatusRekening
                StoreStatusCode.DataBind()
                Dim obj_statuscode = goAML_CustomerBLL.GetStatusRekeningbyID("AKT")
                If obj_statuscode IsNot Nothing Then
                    Cmb_StatusCode.SetValue(obj_statuscode.Kode)
                End If



            End If




        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub GrdCmdCustPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailCustPhone(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditCustPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailCustPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailDirectorPhone(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditDirectorPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailDirectorPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataAddressDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditCustAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailCustAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorAddressDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataDirectorEditAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailDirectorAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Sub LoadDataAddressDetail(id As Long)

        Dim objCustomerPhonesDetail = objListgoAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        If Not objCustomerPhonesDetail Is Nothing Then
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            Btn_Save_Address.Hidden = True
            WindowDetailAddress.Title = "Address Detail"
            cmb_kategoriaddress.IsReadOnly = True
            Address_INDV.ReadOnly = True
            Town_INDV.ReadOnly = True
            City_INDV.ReadOnly = True
            Zip_INDV.ReadOnly = True
            cmb_kodenegara.IsReadOnly = True
            State_INDVD.ReadOnly = True
            Comment_Address_INDVD.ReadOnly = True
            ClearinputCustomerAddress()
            'Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            ' Store_KategoriAddress.DataBind()
            Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(objCustomerPhonesDetail.Address_Type)
            If kategorikontak IsNot Nothing Then
                cmb_kategoriaddress.SetTextWithTextValue(kategorikontak.Kode, kategorikontak.Keterangan)
            End If
            Address_INDV.Text = objCustomerPhonesDetail.Address
            Town_INDV.Text = objCustomerPhonesDetail.Town
            City_INDV.Text = objCustomerPhonesDetail.City
            Zip_INDV.Text = objCustomerPhonesDetail.Zip
            'Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Kode_Negara.DataBind()
            Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerPhonesDetail.Country_Code)
            If kodenegara IsNot Nothing Then
                cmb_kodenegara.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
            End If
            State_INDVD.Text = objCustomerPhonesDetail.State
            Comment_Address_INDVD.Text = objCustomerPhonesDetail.Comments
        End If
    End Sub

    Sub LoadDataDirectorAddressDetail(id As Long)

        Dim objCustomerPhonesDetail = objListgoAML_Ref_Director_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        If Not objCustomerPhonesDetail Is Nothing Then
            FP_Address_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            Btn_Save_Director_Address.Hidden = True
            WindowDetailDirectorAddress.Title = "Address Detail"
            Director_cmb_kategoriaddress.IsReadOnly = True
            Director_Address.ReadOnly = True
            Director_Town.ReadOnly = True
            Director_City.ReadOnly = True
            Director_Zip.ReadOnly = True
            Director_cmb_kodenegara.IsReadOnly = True
            Director_State.ReadOnly = True
            Director_Comment_Address.ReadOnly = True
            ClearinputDirectorAddress()
            'Director_Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Store_KategoriAddress.DataBind()
            Dim kategorikontak_director_detail = goAML_CustomerBLL.GetContactTypeByID(objCustomerPhonesDetail.Address_Type)
            If kategorikontak_director_detail IsNot Nothing Then
                Director_cmb_kategoriaddress.SetTextWithTextValue(kategorikontak_director_detail.Kode, kategorikontak_director_detail.Keterangan)
            End If
            Director_Address.Text = objCustomerPhonesDetail.Address
            Director_Town.Text = objCustomerPhonesDetail.Town
            Director_City.Text = objCustomerPhonesDetail.City
            Director_Zip.Text = objCustomerPhonesDetail.Zip
            'Director_Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Director_Store_Kode_Negara.DataBind()
            Dim kodenegara_director_detail = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerPhonesDetail.Country_Code)
            If kodenegara_director_detail IsNot Nothing Then
                Director_cmb_kodenegara.SetTextWithTextValue(kodenegara_director_detail.Kode, kodenegara_director_detail.Keterangan)
            End If

            Director_State.Text = objCustomerPhonesDetail.State
            Director_Comment_Address.Text = objCustomerPhonesDetail.Comments
            '' Add 09-Jan-2023, Felix. Tambah logic pas load kalau Countrynya ID, Statenya jd Mandatory jg
            If Director_cmb_kodenegara.SelectedItemValue = "ID" Then
                Director_State.AllowBlank = False
                Director_State.FieldStyle = "background-color: #FFE4C4;"
            Else
                Director_State.AllowBlank = True
                Director_State.FieldStyle = "background-color: #FFFFFF;"
            End If
            '' End 09-Jan-2023
        End If
    End Sub

    Sub LoadDataEditCustAddress(id As Long)
        objTempgoAML_Ref_Address = objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        objTempCustomerAddressEdit = objListgoAML_vw_Customer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)

        Dim Address_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Country As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing

        If Not objTempCustomerAddressEdit Is Nothing Then
            FP_Address_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            Btn_Save_Address.Hidden = False

            btnSavedetail.Hidden = False
            cmb_kategoriaddress.IsReadOnly = False
            Address_INDV.ReadOnly = False
            Town_INDV.ReadOnly = False
            City_INDV.ReadOnly = False
            Zip_INDV.ReadOnly = False
            cmb_kodenegara.IsReadOnly = False
            State_INDVD.ReadOnly = False
            Comment_Address_INDVD.ReadOnly = False
            WindowDetailAddress.Title = "Address Edit"
            ClearinputCustomerAddress()
            With objTempCustomerAddressEdit
                'Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_KategoriAddress.DataBind()
                Dim kategorikontak = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                If kategorikontak IsNot Nothing Then
                    cmb_kategoriaddress.SetTextWithTextValue(kategorikontak.Kode, kategorikontak.Keterangan)
                End If

                Address_INDV.Text = .Address
                Town_INDV.Text = .Town
                City_INDV.Text = .City
                Zip_INDV.Text = .Zip
                'Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Store_Kode_Negara.DataBind()
                Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByID(.Country_Code)
                If kodenegara IsNot Nothing Then
                    cmb_kodenegara.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
                End If

                State_INDVD.Text = .State
                Comment_Address_INDVD.Text = .Comments
            End With

            '' Add 09-Jan-2023, Felix. Tambah logic pas load kalau Countrynya ID, Statenya jd Mandatory jg
            If cmb_kodenegara.SelectedItemValue = "ID" Then
                State_INDVD.AllowBlank = False
                State_INDVD.FieldStyle = "background-color: #FFE4C4;"
            Else
                State_INDVD.AllowBlank = True
                State_INDVD.FieldStyle = "background-color: #FFFFFF;"
            End If
            '' End 09-Jan-2023
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataDirectorEditAddress(id As Long)
        objTempgoAML_Ref_Director_Addresses = objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        objTempDirectorAddressEdit = objListgoAML_vw_Director_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)

        Dim Address_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Country As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing

        If Not objTempDirectorAddressEdit Is Nothing Then
            FP_Address_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            Btn_Save_Director_Address.Hidden = False
            Director_cmb_kategoriaddress.IsReadOnly = False
            Director_Address.ReadOnly = False
            Director_Town.ReadOnly = False
            Director_City.ReadOnly = False
            Director_Zip.ReadOnly = False
            Director_cmb_kodenegara.IsReadOnly = False
            Director_State.ReadOnly = False
            Director_Comment_Address.ReadOnly = False
            WindowDetailDirectorAddress.Title = "Address Edit"
            ClearinputDirectorAddress()
            With objTempDirectorAddressEdit
                'Director_Store_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_KategoriAddress.DataBind()
                Dim kategorikontak_director_edit = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                If kategorikontak_director_edit IsNot Nothing Then
                    Director_cmb_kategoriaddress.SetTextWithTextValue(kategorikontak_director_edit.Kode, kategorikontak_director_edit.Keterangan)
                End If

                Director_Address.Text = .Address
                Director_Town.Text = .Town
                Director_City.Text = .City
                Director_Zip.Text = .Zip
                'Director_Store_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Director_Store_Kode_Negara.DataBind()
                Dim kodenegara_director_edit = goAML_CustomerBLL.GetNamaNegaraByID(.Country_Code)
                If kodenegara_director_edit IsNot Nothing Then
                    Director_cmb_kodenegara.SetTextWithTextValue(kodenegara_director_edit.Kode, kodenegara_director_edit.Keterangan)
                End If

                Director_State.Text = .State
                Director_Comment_Address.Text = .Comments
                '' Add 09-Jan-2023, Felix. Tambah logic pas load kalau Countrynya ID, Statenya jd Mandatory jg
                If Director_cmb_kodenegara.SelectedItemValue = "ID" Then
                    Director_State.AllowBlank = False
                    Director_State.FieldStyle = "background-color: #FFE4C4;"
                Else
                    Director_State.AllowBlank = True
                    Director_State.FieldStyle = "background-color: #FFFFFF;"
                End If
                '' End 09-Jan-2023
            End With
        End If
    End Sub

    Sub DeleteRecordTaskDetailCustAddress(id As Long)
        Dim objDelVWAddress As NawaDevDAL.Vw_Customer_Addresses = objListgoAML_vw_Customer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objxDelAddress As NawaDevDAL.goAML_Ref_Address = objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = id)

        If Not objxDelAddress Is Nothing Then
            objListgoAML_Ref_Address.Remove(objxDelAddress)
        End If

        If Not objDelVWAddress Is Nothing Then
            objListgoAML_vw_Customer_Addresses.Remove(objDelVWAddress)

            If Cmb_TypePerson.SelectedItemValue = 1 Then
                Store_INDV_Addresses.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
                Store_INDV_Addresses.DataBind()
            Else
                Store_Address_Corp.DataSource = objListgoAML_vw_Customer_Addresses.ToList()
                Store_Address_Corp.DataBind()
            End If
        End If

        FP_Address_INDVD.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerAddress()

    End Sub

    Sub DeleteRecordTaskDetailDirectorAddress(id As Long)
        Dim objDelVWAddress As NawaDevDAL.Vw_Director_Addresses = objListgoAML_vw_Director_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objxDelAddress As NawaDevDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID = id)

        If Not objxDelAddress Is Nothing Then
            objListgoAML_Ref_Director_Address.Remove(objxDelAddress)
        End If

        If Not objDelVWAddress Is Nothing Then
            objListgoAML_vw_Director_Addresses.Remove(objDelVWAddress)

            Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.ToList()
            Store_DirectorAddress.DataBind()
        End If

        FP_Address_INDVD.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerAddress()

    End Sub

    Sub LoadDataDetailCustPhone(id As Long)
        Dim objCustomerPhonesDetail As goAML_Ref_Phone = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            cb_phone_Contact_Type.IsReadOnly = True
            cb_phone_Communication_Type.IsReadOnly = True
            txt_phone_Country_prefix.ReadOnly = True
            txt_phone_number.ReadOnly = True
            txt_phone_extension.ReadOnly = True
            txt_phone_comments.ReadOnly = True
            btnSavedetail.Hidden = True

            WindowDetail.Title = "Phone Detail"
            ClearinputCustomerPhones()
            With objCustomerPhonesDetail
                'Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_Contact_Type.DataBind()
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_contact_type IsNot Nothing Then
                    cb_phone_Contact_Type.SetTextWithTextValue(obj_contact_type.Kode, obj_contact_type.Keterangan)
                End If
                'cb_phone_Contact_Type.SetValue(.Tph_Contact_Type,)

                'Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Store_Communication_Type.DataBind()
                Dim obj_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_communication_type IsNot Nothing Then
                    cb_phone_Communication_Type.SetTextWithTextValue(obj_communication_type.Kode, obj_communication_type.Keterangan)
                End If
                'cb_phone_Communication_Type.SetTextValue(.Tph_Communication_Type)
                'End Update
                txt_phone_Country_prefix.Text = .tph_country_prefix
                txt_phone_number.Text = .tph_number
                txt_phone_extension.Text = .tph_extension
                txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataDetailDirectorPhone(id As Long)
        Dim objCustomerPhonesDetail As goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelDirectorPhone.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            Director_cb_phone_Contact_Type.IsReadOnly = True
            Director_cb_phone_Communication_Type.IsReadOnly = True
            Director_txt_phone_Country_prefix.ReadOnly = True
            Director_txt_phone_number.ReadOnly = True
            Director_txt_phone_extension.ReadOnly = True
            Director_txt_phone_comments.ReadOnly = True
            btnSaveDirectorPhonedetail.Hidden = True

            WindowDetailDirectorPhone.Title = "Phone Detail"
            ClearinputDirectorPhones()
            With objCustomerPhonesDetail
                'Director_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_Contact_Type.DataBind()
                'Director_cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_director_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_director_contact_type IsNot Nothing Then
                    Director_cb_phone_Contact_Type.SetTextWithTextValue(obj_director_contact_type.Kode, obj_director_contact_type.Keterangan)
                End If
                'Dim obj_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                'If obj_communication_type IsNot Nothing Then
                '    cb_phone_Communication_Type.SetTextWithTextValue(obj_communication_type.Kode, obj_communication_type.Keterangan)
                'End If


                'Director_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Store_Communication_Type.DataBind()
                'Director_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                Dim obj_director_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_director_communication_type IsNot Nothing Then
                    Director_cb_phone_Communication_Type.SetTextWithTextValue(obj_director_communication_type.Kode, obj_director_communication_type.Keterangan)
                End If
                'End Update
                Director_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_txt_phone_number.Text = .tph_number
                Director_txt_phone_extension.Text = .tph_extension
                Director_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub

    Sub LoadDataDetailEmpDirectorPhone(id As Long)
        Dim objCustomerPhonesDetail As goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelEmpDirectorTaskDetail.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            Director_Emp_cb_phone_Contact_Type.IsReadOnly = True
            Director_Emp_cb_phone_Communication_Type.IsReadOnly = True
            Director_Emp_txt_phone_Country_prefix.ReadOnly = True
            Director_Emp_txt_phone_number.ReadOnly = True
            Director_Emp_txt_phone_extension.ReadOnly = True
            Director_Emp_txt_phone_comments.ReadOnly = True
            btn_saveDirector_empphone.Hidden = True

            WindowDetailDirectorPhone.Title = "Phone Detail"
            ClearinputDirectorEmpPhones()
            With objCustomerPhonesDetail
                'Director_Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Emp_Store_Contact_Type.DataBind()
                'Director_Emp_cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_director_emp_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_director_emp_contact_type IsNot Nothing Then
                    Director_Emp_cb_phone_Contact_Type.SetTextWithTextValue(obj_director_emp_contact_type.Kode, obj_director_emp_contact_type.Keterangan)
                End If
                'Director_Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Emp_Store_Communication_Type.DataBind()
                'Director_Emp_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                Dim obj_director_emp_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_director_emp_communication_type IsNot Nothing Then
                    Director_Emp_cb_phone_Communication_Type.SetTextWithTextValue(obj_director_emp_communication_type.Kode, obj_director_emp_communication_type.Keterangan)
                End If
                'End Update
                Director_Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_Emp_txt_phone_number.Text = .tph_number
                Director_Emp_txt_phone_extension.Text = .tph_extension
                Director_Emp_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub


    Sub LoadDataEditCustPhone(id As Long)
        objTempgoAML_Ref_Phone = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        objTempCustomerPhoneEdit = objListgoAML_vw_Customer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objTempCustomerPhoneEdit Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            cb_phone_Contact_Type.IsReadOnly = False
            cb_phone_Communication_Type.IsReadOnly = False
            txt_phone_Country_prefix.ReadOnly = False
            txt_phone_number.ReadOnly = False
            txt_phone_extension.ReadOnly = False
            txt_phone_comments.ReadOnly = False
            btnSavedetail.Hidden = False

            WindowDetail.Title = "Phone Edit"
            ClearinputCustomerPhones()
            With objTempCustomerPhoneEdit
                'Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_Contact_Type.DataBind()
                'cb_phone_Contact_Type.SetTextValue(objTempCustomerPhoneEdit.Tph_Contact_Type)
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_contact_type IsNot Nothing Then
                    cb_phone_Contact_Type.SetTextWithTextValue(obj_contact_type.Kode, obj_contact_type.Keterangan)
                End If
                'cb_phone_Contact_Type.SetValue(.Tph_Contact_Type,)

                'Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Store_Communication_Type.DataBind()
                Dim obj_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_communication_type IsNot Nothing Then
                    cb_phone_Communication_Type.SetTextWithTextValue(obj_communication_type.Kode, obj_communication_type.Keterangan)
                End If
                'cb_phone_Communication_Type.SetTextValue(.Tph_Communication_Type)
                'End Update
                'Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Store_Communication_Type.DataBind()
                'cb_phone_Communication_Type.SetTextValue(objTempCustomerPhoneEdit.Tph_Communication_Type)

                txt_phone_Country_prefix.Text = .tph_country_prefix
                txt_phone_number.Text = .tph_number
                txt_phone_extension.Text = .tph_extension
                txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataEditDirectorPhone(id As Long)
        objTempgoAML_Ref_Director_Phones = objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        objTempDirectorPhoneEdit = objListgoAML_vw_Director_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objTempDirectorPhoneEdit Is Nothing Then
            FormPanelDirectorPhone.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            Director_cb_phone_Contact_Type.IsReadOnly = False
            Director_cb_phone_Communication_Type.IsReadOnly = False
            Director_txt_phone_Country_prefix.ReadOnly = False
            Director_txt_phone_number.ReadOnly = False
            Director_txt_phone_extension.ReadOnly = False
            Director_txt_phone_comments.ReadOnly = False
            btnSaveDirectorPhonedetail.Hidden = False

            WindowDetailDirectorPhone.Title = "Phone Edit"
            ClearinputDirectorPhones()
            With objTempDirectorPhoneEdit
                'Director_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_Contact_Type.DataBind()
                'Director_cb_phone_Contact_Type.SetValue(objTempDirectorPhoneEdit.Tph_Contact_Type)
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_director_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_director_contact_type IsNot Nothing Then
                    Director_cb_phone_Contact_Type.SetTextWithTextValue(obj_director_contact_type.Kode, obj_director_contact_type.Keterangan)
                End If
                Dim obj_director_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_director_communication_type IsNot Nothing Then
                    Director_cb_phone_Communication_Type.SetTextWithTextValue(obj_director_communication_type.Kode, obj_director_communication_type.Keterangan)
                End If
                'End Update

                'Director_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Store_Communication_Type.DataBind()
                'Director_cb_phone_Communication_Type.SetValue(objTempDirectorPhoneEdit.Tph_Communication_Type)

                Director_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_txt_phone_number.Text = .tph_number
                Director_txt_phone_extension.Text = .tph_extension
                Director_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub

    Sub LoadDataEditEmpDirectorPhone(id As Long)
        objTempgoAML_Ref_Director_EmployerPhones = objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        objTempDirectorEmployerPhoneEdit = objListgoAML_vw_Director_Employer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objTempDirectorEmployerPhoneEdit Is Nothing Then
            FormPanelEmpDirectorTaskDetail.Hidden = False
            WindowDetailDirectorPhone.Hidden = False
            Director_Emp_cb_phone_Contact_Type.IsReadOnly = False
            Director_Emp_cb_phone_Communication_Type.IsReadOnly = False
            Director_Emp_txt_phone_Country_prefix.ReadOnly = False
            Director_Emp_txt_phone_number.ReadOnly = False
            Director_Emp_txt_phone_extension.ReadOnly = False
            Director_Emp_txt_phone_comments.ReadOnly = False
            btn_saveDirector_empphone.Hidden = False

            WindowDetailDirectorPhone.Title = "Phone Edit"
            ClearinputDirectorEmpPhones()
            With objTempDirectorEmployerPhoneEdit
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_director_emp_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_director_emp_contact_type IsNot Nothing Then
                    Director_Emp_cb_phone_Contact_Type.SetTextWithTextValue(obj_director_emp_contact_type.Kode, obj_director_emp_contact_type.Keterangan)
                End If
                Dim obj_director_emp_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_director_emp_communication_type IsNot Nothing Then
                    Director_Emp_cb_phone_Communication_Type.SetTextWithTextValue(obj_director_emp_communication_type.Kode, obj_director_emp_communication_type.Keterangan)
                End If
                'End Update
                'Director_Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Emp_Store_Contact_Type.DataBind()
                'Director_Emp_cb_phone_Contact_Type.SetValue(objTempDirectorEmployerPhoneEdit.Tph_Contact_Type)

                'Director_Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Emp_Store_Communication_Type.DataBind()
                'Director_Emp_cb_phone_Communication_Type.SetValue(objTempDirectorEmployerPhoneEdit.Tph_Communication_Type)

                Director_Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Director_Emp_txt_phone_number.Text = .tph_number
                Director_Emp_txt_phone_extension.Text = .tph_extension
                Director_Emp_txt_phone_comments.Text = .comments
            End With
        End If
    End Sub

    Sub DeleteRecordTaskDetailEmpDirectorPhone(id As Long)
        Dim objDelVWPhone As NawaDevDAL.Vw_Director_Employer_Phones = objListgoAML_vw_Director_Employer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objxDelPhone As NawaDevDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        If Not objxDelPhone Is Nothing Then
            objListgoAML_Ref_Director_Employer_Phone.Remove(objxDelPhone)
        End If

        If Not objDelVWPhone Is Nothing Then
            objListgoAML_vw_Director_Employer_Phones.Remove(objDelVWPhone)

            Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.ToList()
            Store_Director_Employer_Phone.DataBind()
        End If

        FormPanelEmpDirectorTaskDetail.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorEmpPhones()
    End Sub

    Sub DeleteRecordTaskDetailDirectorPhone(id As Long)
        Dim objDelVWPhone As NawaDevDAL.Vw_Director_Phones = objListgoAML_vw_Director_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objxDelPhone As NawaDevDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        If Not objxDelPhone Is Nothing Then
            objListgoAML_Ref_Director_Phone.Remove(objxDelPhone)
        End If

        If Not objDelVWPhone Is Nothing Then
            objListgoAML_vw_Director_Phones.Remove(objDelVWPhone)

            Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.ToList()
            Store_DirectorPhone.DataBind()
        End If

        FormPanelDirectorPhone.Hidden = True
        WindowDetailDirectorPhone.Hidden = True
        ClearinputDirectorPhones()
    End Sub

    Sub DeleteRecordTaskDetailCustPhone(id As Long)
        Dim objDelVWPhone As NawaDevDAL.Vw_Customer_Phones = objListgoAML_vw_Customer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objxDelPhone As NawaDevDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        If Not objxDelPhone Is Nothing Then
            objListgoAML_Ref_Phone.Remove(objxDelPhone)
        End If

        If Not objDelVWPhone Is Nothing Then
            objListgoAML_vw_Customer_Phones.Remove(objDelVWPhone)

            If Cmb_TypePerson.SelectedItemValue = 1 Then
                Store_INDV_Phones.DataSource = objListgoAML_vw_Customer_Phones.ToList()
                Store_INDV_Phones.DataBind()
            Else
                Store_Phone_Corp.DataSource = objListgoAML_vw_Customer_Phones.ToList()
                Store_Phone_Corp.DataBind()
            End If
        End If

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerPhones()

    End Sub

    Protected Sub btnBackSaveDetail_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = True
            FP_Address_INDVD.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub CUSTOMER_EDIT_Default_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    'Protected Sub Cmb_TypePerson_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles Cmb_TypePerson.DirectSelect
    Protected Sub Cmb_TypePerson_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            txt_cif.Hidden = False
            txt_gcn.Hidden = False
            'GCNPrimary.Hidden = False
            Dim pk As String = Cmb_TypePerson.SelectedItemValue
            If pk = 1 Then
                panel_INDV.Hidden = False
                panel_Corp.Hidden = True
                'StoreINDVGender.DataSource = goAML_CustomerBLL.GetListJenisKelamin()
                'StoreINDVGender.DataBind()
                'StoreNationality1.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'StoreNationality1.DataBind()
                'StoreNationality2.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'StoreNationality2.DataBind()
                'StoreNationality3.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                ' StoreNationality3.DataBind()
                'StoreResidence.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'StoreResidence.DataBind()
                ClearinputCustomerCorp()
                ClearinputCustomerINDVD()

            Else
                panel_INDV.Hidden = True
                panel_Corp.Hidden = False
                'Store_Corp_Incorporation_Country_Code.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Store_Corp_Incorporation_Country_Code.DataBind()
                'Store_Corp_Incorporation_Legal_Form.DataSource = goAML_CustomerBLL.GetListBentukBadanUsaha()
                'Store_Corp_Incorporation_Legal_Form.DataBind()
                'Store_Corp_Role.DataSource = goAML_CustomerBLL.GetListPartyRole()
                'Store_Corp_Role.DataBind()
                ClearinputCustomerCorp()
                ClearinputCustomerINDVD()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub checkBoxIsClosed_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbTutup.DirectCheck
        Try
            If cbTutup.Checked Then
                txt_Corp_date_business_closed.Hidden = False
            Else
                txt_Corp_date_business_closed.Hidden = True
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub checkBoxcb_Deceased_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_Deceased.DirectCheck
        Try
            If cb_Deceased.Checked Then
                txt_deceased_date.Hidden = False
            Else
                txt_deceased_date.Hidden = True
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub checkBoxcb_Director_Deceased_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_Director_Deceased.DirectCheck
        Try
            If cb_Director_Deceased.Checked Then
                txt_Director_Deceased_Date.Hidden = False
            Else
                txt_Director_Deceased_Date.Hidden = True
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Protected Sub checkBoxcb_tax_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_tax.DirectCheck
    '    Try
    '        If cb_tax.Checked Then
    '            txt_INDV_Tax_Number.Hidden = False
    '        Else
    '            txt_INDV_Tax_Number.Hidden = True
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub checkBoxcb_Director_tax_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_Director_Tax_Reg_Number.DirectCheck
    '    Try
    '        If cb_Director_Tax_Reg_Number.Checked Then
    '            txt_Director_Tax_Number.Hidden = False
    '        Else
    '            txt_Director_Tax_Number.Hidden = True
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Sub ClearinputCustomerINDVD()
        txt_cif.Text = ""
        txt_gcn.Text = ""
        txt_INDV_Title.Text = ""
        txt_INDV_Last_Name.Text = ""
        txt_INDV_SSN.Text = ""
        txt_INDV_Birthdate.Text = ""
        txt_INDV_Birth_place.Text = ""
        txt_INDV_Mothers_name.Text = ""
        txt_INDV_Alias.Text = ""
        txt_INDV_Passport_number.Text = ""
        'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
        cmb_INDV_Passport_country.SetTextValue("")
        'End Update
        txt_INDV_ID_Number.Text = ""
        'Update: Zikri_11092020 Change Dropdown to NDSDropdown
        cmb_INDV_Nationality1.SetTextValue("")
        cmb_INDV_Nationality2.SetTextValue("")
        cmb_INDV_Nationality3.SetTextValue("")
        cmb_INDV_Gender.SetTextValue("")
        cmb_INDV_Residence.SetTextValue("")
        'End Update
        txt_INDV_Email.Text = ""
        txt_INDV_Email2.Text = ""
        txt_INDV_Email3.Text = ""
        txt_INDV_Email4.Text = ""
        txt_INDV_Email5.Text = ""
        txt_INDV_Occupation.Text = ""
        txt_INDV_Employer_Name.Text = ""
        cb_Deceased.Checked = False
        txt_deceased_date.Value = ""
        txt_INDV_Tax_Number.Text = ""
        cb_tax.Checked = False
        txt_INDV_Source_of_Wealth.Text = ""
        txt_INDV_Comments.Text = ""
        Store_INDV_Addresses.DataBind()
        Store_INDV_Phones.DataBind()
        objListgoAML_Ref_Phone = Nothing
        objListgoAML_Ref_Address = Nothing
        objListgoAML_Ref_Identification = Nothing
        objListgoAML_Ref_Employer_Address = Nothing
        objListgoAML_Ref_Employer_Phone = Nothing
        ClearSession()
    End Sub

    Sub ClearinputCustomerCorp()
        txt_cif.Text = ""
        txt_gcn.Text = ""
        txt_Corp_Name.Value = ""
        txt_Corp_Commercial_Name.Value = ""
        cmb_Corp_Incorporation_Legal_Form.SetTextValue("")
        txt_Corp_Incorporation_number.Value = ""
        txt_Corp_Business.Value = ""
        txt_Corp_Email.Value = ""
        'cmb_director.Value = ""
        txt_Corp_url.Value = ""
        txt_Corp_incorporation_state.Value = ""
        cmb_Corp_Incorporation_Country_Code.SetTextValue("")
        txt_Corp_incorporation_date.Value = ""
        txt_Corp_date_business_closed.Value = ""
        txt_Corp_tax_number.Value = ""
        cbTutup.Checked = False
        txt_Corp_Comments.Value = ""
        Store_Address_Corp.DataBind()
        Store_Phone_Corp.DataBind()
        objListgoAML_Ref_Phone = Nothing
        objListgoAML_Ref_Address = Nothing
        objListgoAML_Ref_Director_Phone = Nothing
        objListgoAML_Ref_Director_Address = Nothing
        objListgoAML_Ref_Director_Employer_Phone = Nothing
        objListgoAML_Ref_Director_Employer_Address = Nothing
        objListgoAML_Ref_Director = Nothing

        ClearSession()
    End Sub

    Protected Sub btnAddCustomerIdentifications_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FP_Identification_INDV.Hidden = False
            WindowDetailIdentification.Hidden = False
            cmb_Jenis_Dokumen.IsReadOnly = False
            txt_identification_comment.ReadOnly = False
            txt_issued_by.ReadOnly = False
            txt_issued_date.ReadOnly = False
            txt_issued_expired_date.ReadOnly = False
            cmb_issued_country.IsReadOnly = False
            txt_number_identification.ReadOnly = False
            btn_save_identification.Hidden = False
            WindowDetailIdentification.Title = "Customer Identification Add"

            'Store_jenis_dokumen.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            'Store_jenis_dokumen.DataBind()

            'Store_issued_country.DataSource = goAML_CustomerBLL.GetListNamaNegara
            'Store_issued_country.DataBind()

            ClearinputCustomerIdentification()
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveCustomerIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomerIdentificationValid() Then
                'If objTempCustomerPhoneEdit Is Nothing Then
                If objTempCustomerIdentificationEdit Is Nothing Then
                    SaveAddIdentificationDetail()
                Else
                    SaveEditIdentificationDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveDirectorIdentification_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataDirectorIdentificationValid() Then
                'If objTempCustomerPhoneEdit Is Nothing Then
                If objTempCustomerIdentificationEdit_Director Is Nothing Then
                    SaveAddDirectorIdentificationDetail()
                Else
                    SaveEditDirectorIdentificationDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ClearinputCustomerIdentification()
        cmb_Jenis_Dokumen.SetTextValue("")
        txt_identification_comment.Text = ""
        txt_issued_by.Text = ""
        txt_issued_date.Text = ""
        txt_issued_expired_date.Text = ""
        cmb_issued_country.SetTextValue("")
        txt_number_identification.Text = ""
    End Sub

    Function IsDataCustomerIdentificationValid() As Boolean
        If cmb_issued_country.SelectedItemValue = "" Then
            Throw New Exception("Country is required")
        End If
        If cmb_Jenis_Dokumen.SelectedItemValue = "" Then
            Throw New Exception("Document Type is required")
        End If
        If txt_number_identification.Text.Trim = "" Then
            Throw New Exception("Number Identification is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(txt_number_identification.Text) Then
            Throw New Exception("Identification Number is Invalid")
        End If
        'End Update
        Return True
    End Function

    Function IsDataDirectorIdentificationValid() As Boolean
        If cmb_issued_country_Director.SelectedItemValue = "" Then
            Throw New Exception("Country is required")
        End If
        If cmb_Jenis_Dokumen_Director.SelectedItemValue = "" Then
            Throw New Exception("Document Type is required")
        End If
        If txt_number_identification_Director.Text.Trim = "" Then
            Throw New Exception("Number Identification is required")
        End If
        'Update: Zikri_08102020
        If Not IsNumber(txt_number_identification_Director.Text) Then
            Throw New Exception("Identification Number is Invalid")
        End If
        'End Update
        Return True
    End Function

    Sub SaveAddIdentificationDetail()

        Dim objNewVWgoAML_Customer As New NawaDevDAL.Vw_Person_Identification
        Dim objNewgoAML_Customer As New NawaDevDAL.goAML_Person_Identification
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Person_Identification_ID = intpk
            .isReportingPerson = 0
            .FK_Person_Type = 1
            .FK_Person_ID = obj_customer.PK_Customer_ID
            .Number = txt_number_identification.Value
            .Issued_By = txt_issued_by.Value
            If txt_issued_date.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment.Value
            .Country = cmb_issued_country.SelectedItemText
            .Type_Document = cmb_Jenis_Dokumen.SelectedItemText
        End With

        With objNewgoAML_Customer
            .PK_Person_Identification_ID = intpk
            .isReportingPerson = 0
            .FK_Person_Type = 1
            .FK_Person_ID = obj_customer.PK_Customer_ID
            .Number = txt_number_identification.Value
            .Issued_Country = cmb_issued_country.SelectedItemValue
            .Issued_By = txt_issued_by.Value
            If txt_issued_date.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment.Value
            .Issued_Country = cmb_issued_country.SelectedItemValue
            .Type = cmb_Jenis_Dokumen.SelectedItemValue

        End With

        objListgoAML_vw_Customer_Identification.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Identification.Add(objNewgoAML_Customer)

        Store_INDV_Identification.DataSource = objListgoAML_vw_Customer_Identification.ToList()
        Store_INDV_Identification.DataBind()

        FP_Identification_INDV.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputCustomerIdentification()
    End Sub

    Sub SaveAddDirectorIdentificationDetail()

        Dim objNewVWgoAML_Customer As New NawaDevDAL.Vw_Person_Identification
        Dim objNewgoAML_Customer As New NawaDevDAL.goAML_Person_Identification
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_Person_Identification_ID = intpk
            .isReportingPerson = 0
            .FK_Person_Type = 4
            .FK_Person_ID = tempflag
            .Number = txt_number_identification_Director.Value
            .Issued_By = txt_issued_by_Director.Value
            If txt_issued_date_Director.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date_Director.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date_Director.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date_Director.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment_Director.Value
            .Country = cmb_issued_country_Director.SelectedItemText
            .Type_Document = cmb_Jenis_Dokumen_Director.SelectedItemText
        End With

        With objNewgoAML_Customer
            .PK_Person_Identification_ID = intpk
            .isReportingPerson = 0
            .FK_Person_Type = 4
            .FK_Person_ID = tempflag
            .Number = txt_number_identification_Director.Value
            .Issued_Country = cmb_issued_country_Director.SelectedItemValue
            .Issued_By = txt_issued_by_Director.Value
            If txt_issued_date_Director.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date_Director.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date_Director.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date_Director.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment_Director.Value
            .Issued_Country = cmb_issued_country_Director.SelectedItemValue
            .Type = cmb_Jenis_Dokumen_Director.SelectedItemValue

        End With

        objListgoAML_vw_Customer_Identification_Director.Add(objNewVWgoAML_Customer)
        objListgoAML_Ref_Identification_Director.Add(objNewgoAML_Customer)

        Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = tempflag).ToList()
        Store_Director_Identification.DataBind()

        FP_Identification_Director.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputDirectorIdentification()
    End Sub

    Sub ClearinputDirectorIdentification()
        cmb_Jenis_Dokumen_Director.SetTextValue("")
        txt_identification_comment_Director.Text = ""
        txt_issued_by_Director.Text = ""
        txt_issued_date_Director.Text = ""
        txt_issued_expired_date_Director.Text = ""
        cmb_issued_country_Director.SetTextValue("")
        txt_number_identification_Director.Text = ""
    End Sub

    Sub SaveEditIdentificationDetail()


        With objTempCustomerIdentificationEdit
            .isReportingPerson = 0
            .FK_Person_Type = 1
            .FK_Person_ID = obj_customer.PK_Customer_ID
            .Number = txt_number_identification.Value
            .Issued_By = txt_issued_by.Value
            If txt_issued_date.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment.Value
            .Country = cmb_issued_country.SelectedItemText
            .Type_Document = cmb_Jenis_Dokumen.SelectedItemText
        End With

        With objTempgoAML_Ref_Identification
            .isReportingPerson = 0
            .FK_Person_Type = 1
            .FK_Person_ID = obj_customer.PK_Customer_ID
            .Number = txt_number_identification.Value
            .Issued_By = txt_issued_by.Value
            If txt_issued_date.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment.Value
            .Issued_Country = cmb_issued_country.SelectedItemValue
            .Type = cmb_Jenis_Dokumen.SelectedItemValue
        End With

        objTempCustomerIdentificationEdit = Nothing
        objTempgoAML_Ref_Identification = Nothing

        Store_INDV_Identification.DataSource = objListgoAML_vw_Customer_Identification.ToList()
        Store_INDV_Identification.DataBind()

        FP_Identification_INDV.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputCustomerIdentification()
    End Sub

    Sub SaveEditDirectorIdentificationDetail()


        With objTempCustomerIdentificationEdit_Director
            .isReportingPerson = 0
            .Number = txt_number_identification_Director.Value
            .Issued_By = txt_issued_by_Director.Value
            If txt_issued_date_Director.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date_Director.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date_Director.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date_Director.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment_Director.Value
            .Country = cmb_issued_country_Director.SelectedItemText
            .Type_Document = cmb_Jenis_Dokumen_Director.SelectedItemText
        End With

        With objTempgoAML_Ref_Identification_Director
            .isReportingPerson = 0
            .Number = txt_number_identification_Director.Value
            .Issued_By = txt_issued_by_Director.Value
            If txt_issued_date_Director.RawValue IsNot Nothing Then
                .Issue_Date = Convert.ToDateTime(txt_issued_date_Director.RawValue)
            Else
                .Issue_Date = Nothing
            End If
            If txt_issued_expired_date_Director.RawValue IsNot Nothing Then
                .Expiry_Date = Convert.ToDateTime(txt_issued_expired_date_Director.RawValue)
            Else
                .Expiry_Date = Nothing
            End If
            .Identification_Comment = txt_identification_comment_Director.Value
            .Issued_Country = cmb_issued_country_Director.SelectedItemValue
            .Type = cmb_Jenis_Dokumen_Director.SelectedItemValue
        End With

        objTempCustomerIdentificationEdit_Director = Nothing
        objTempgoAML_Ref_Identification_Director = Nothing

        Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = tempflag).ToList()
        Store_Director_Identification.DataBind()

        FP_Identification_Director.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputDirectorIdentification()
    End Sub

    Protected Sub GrdCmdCustIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataIdentificationDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditCustIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailCustIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorIdentificationDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditDirectorIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailDirectorIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Sub LoadDataIdentificationDetail(id As Long)

        Dim objCustomerIdentificationDetail = objListgoAML_vw_Customer_Identification.Where(Function(x) x.PK_Person_Identification_ID = id).FirstOrDefault()

        If Not objCustomerIdentificationDetail Is Nothing Then
            FP_Identification_INDV.Hidden = False
            WindowDetailIdentification.Hidden = False
            btn_save_identification.Hidden = True
            WindowDetailIdentification.Title = "Identification Detail"
            txt_number_identification.ReadOnly = True
            cmb_Jenis_Dokumen.IsReadOnly = True
            cmb_issued_country.IsReadOnly = True
            txt_issued_by.ReadOnly = True
            txt_issued_date.ReadOnly = True
            txt_issued_expired_date.ReadOnly = True
            txt_identification_comment.ReadOnly = True
            ClearinputCustomerIdentification()
            txt_number_identification.Text = objCustomerIdentificationDetail.Number
            'Store_jenis_dokumen.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            'Store_jenis_dokumen.DataBind()
            Dim jenisdokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(X) X.Keterangan = objCustomerIdentificationDetail.Type_Document).FirstOrDefault
            If jenisdokumen IsNot Nothing Then
                cmb_Jenis_Dokumen.SetTextWithTextValue(jenisdokumen.Kode, jenisdokumen.Keterangan)
            End If

            'Store_issued_country.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_issued_country.DataBind()
            Dim namanegara = goAML_CustomerBLL.GetNamaNegaraByKeterangan(objCustomerIdentificationDetail.Country)
            If namanegara IsNot Nothing Then
                cmb_issued_country.SetTextWithTextValue(namanegara.Kode, namanegara.Keterangan)
            End If
            txt_issued_by.Text = objCustomerIdentificationDetail.Issued_By
            txt_issued_date.Value = objCustomerIdentificationDetail.Issue_Date
            txt_issued_expired_date.Value = objCustomerIdentificationDetail.Expiry_Date
            txt_identification_comment.Text = objCustomerIdentificationDetail.Identification_Comment
        End If
    End Sub

    Sub LoadDataDirectorIdentificationDetail(id As Long)

        Dim objCustomerIdentificationDetail = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.PK_Person_Identification_ID = id).FirstOrDefault()

        If Not objCustomerIdentificationDetail Is Nothing Then
            FP_Identification_Director.Hidden = False
            WindowDetailIdentification.Hidden = False
            btn_save_identification_Director.Hidden = True
            WindowDetailIdentification.Title = "Identification Detail"
            txt_number_identification_Director.ReadOnly = True
            cmb_Jenis_Dokumen_Director.IsReadOnly = True
            cmb_issued_country_Director.IsReadOnly = True
            txt_issued_by_Director.ReadOnly = True
            txt_issued_date_Director.ReadOnly = True
            txt_issued_expired_date_Director.ReadOnly = True
            txt_identification_comment_Director.ReadOnly = True
            ClearinputDirectorIdentification()
            txt_number_identification_Director.Text = objCustomerIdentificationDetail.Number
            'Storejenisdokumen_Director.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
            ' Storejenisdokumen_Director.DataBind()
            Dim jenisdokumen_director_detail = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(X) X.Keterangan = objCustomerIdentificationDetail.Type_Document).FirstOrDefault
            If jenisdokumen_director_detail IsNot Nothing Then
                cmb_Jenis_Dokumen_Director.SetTextWithTextValue(jenisdokumen_director_detail.Kode, jenisdokumen_director_detail.Keterangan)
            End If

            'StoreIssueCountry_Director.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'StoreIssueCountry_Director.DataBind()
            Dim namanegara_director_detail = goAML_CustomerBLL.GetNamaNegaraByKeterangan(objCustomerIdentificationDetail.Country)
            If namanegara_director_detail IsNot Nothing Then
                cmb_issued_country_Director.SetTextWithTextValue(namanegara_director_detail.Kode, namanegara_director_detail.Keterangan)
            End If
            txt_issued_by_Director.Text = objCustomerIdentificationDetail.Issued_By
            txt_issued_date_Director.Value = objCustomerIdentificationDetail.Issue_Date
            txt_issued_expired_date_Director.Value = objCustomerIdentificationDetail.Expiry_Date
            txt_identification_comment_Director.Text = objCustomerIdentificationDetail.Identification_Comment
        End If
    End Sub

    Sub DeleteRecordTaskDetailCustIdentification(id As Long)
        Dim objDelVWIdentification As NawaDevDAL.Vw_Person_Identification = objListgoAML_vw_Customer_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)
        Dim objxDelIdentification As NawaDevDAL.goAML_Person_Identification = objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)

        If Not objxDelIdentification Is Nothing Then
            objListgoAML_Ref_Identification.Remove(objxDelIdentification)
        End If

        If Not objDelVWIdentification Is Nothing Then
            objListgoAML_vw_Customer_Identification.Remove(objDelVWIdentification)


            Store_INDV_Identification.DataSource = objListgoAML_vw_Customer_Identification.ToList()
            Store_INDV_Identification.DataBind()
        End If

        FP_Identification_INDV.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputCustomerIdentification()

    End Sub

    Sub DeleteRecordTaskDetailDirectorIdentification(id As Long)
        Dim objDelVWIdentification As NawaDevDAL.Vw_Person_Identification = objListgoAML_vw_Customer_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = id)
        Dim objxDelIdentification As NawaDevDAL.goAML_Person_Identification = objListgoAML_Ref_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = id)

        If Not objxDelIdentification Is Nothing Then
            objListgoAML_Ref_Identification_Director.Remove(objxDelIdentification)
        End If

        If Not objDelVWIdentification Is Nothing Then
            objListgoAML_vw_Customer_Identification_Director.Remove(objDelVWIdentification)


            Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.ToList()
            Store_Director_Identification.DataBind()
        End If

        FP_Identification_Director.Hidden = True
        WindowDetailIdentification.Hidden = True
        ClearinputDirectorIdentification()

    End Sub


    Sub LoadDataEditCustIdentification(id As Long)
        objTempgoAML_Ref_Identification = objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)
        objTempCustomerIdentificationEdit = objListgoAML_vw_Customer_Identification.Find(Function(x) x.PK_Person_Identification_ID = id)


        If Not objTempCustomerIdentificationEdit Is Nothing Then
            FP_Identification_INDV.Hidden = False
            WindowDetailIdentification.Hidden = False
            btn_save_identification.Hidden = False
            cmb_Jenis_Dokumen.IsReadOnly = False
            txt_issued_by.ReadOnly = False
            txt_issued_date.ReadOnly = False
            txt_issued_expired_date.ReadOnly = False
            txt_identification_comment.ReadOnly = False
            cmb_issued_country.IsReadOnly = False
            txt_number_identification.ReadOnly = False
            WindowDetailIdentification.Title = "Identification Edit"
            ClearinputCustomerIdentification()
            With objTempCustomerIdentificationEdit
                'Store_jenis_dokumen.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
                'Store_jenis_dokumen.DataBind()
                Dim jenisdokumen = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(x) x.Keterangan = .Type_Document).FirstOrDefault()
                If jenisdokumen IsNot Nothing Then
                    cmb_Jenis_Dokumen.SetTextWithTextValue(jenisdokumen.Kode, jenisdokumen.Keterangan)
                End If
                txt_issued_by.Text = .Issued_By
                txt_issued_date.Value = .Issue_Date
                txt_issued_expired_date.Value = .Expiry_Date
                txt_identification_comment.Text = .Identification_Comment
                'Store_issued_country.DataSource = goAML_CustomerBLL.GetListNamaNegara().ToList()
                'Store_issued_country.DataBind()
                Dim kodenegara = goAML_CustomerBLL.GetNamaNegaraByKeterangan(.Country)
                If kodenegara IsNot Nothing Then
                    cmb_issued_country.SetTextWithTextValue(kodenegara.Kode, kodenegara.Keterangan)
                End If

                txt_number_identification.Text = .Number
            End With

            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataEditDirectorIdentification(id As Long)
        objTempgoAML_Ref_Identification_Director = objListgoAML_Ref_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = id)
        objTempCustomerIdentificationEdit_Director = objListgoAML_vw_Customer_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = id)


        If Not objTempCustomerIdentificationEdit_Director Is Nothing Then
            FP_Identification_Director.Hidden = False
            WindowDetailIdentification.Hidden = False
            btn_save_identification_Director.Hidden = False
            cmb_Jenis_Dokumen_Director.IsReadOnly = False
            txt_issued_by_Director.ReadOnly = False
            txt_issued_date_Director.ReadOnly = False
            txt_issued_expired_date_Director.ReadOnly = False
            txt_identification_comment_Director.ReadOnly = False
            cmb_issued_country_Director.IsReadOnly = False
            txt_number_identification_Director.ReadOnly = False
            WindowDetailIdentification.Title = "Identification Edit"
            ClearinputDirectorIdentification()
            With objTempCustomerIdentificationEdit_Director
                'Storejenisdokumen_Director.DataSource = goAML_CustomerBLL.getlistjenisdokumenidentitas()
                'Storejenisdokumen_Director.DataBind()
                Dim jenisdokumen_director_edit = goAML_CustomerBLL.getlistjenisdokumenidentitas().Where(Function(x) x.Keterangan = .Type_Document).FirstOrDefault()
                If jenisdokumen_director_edit IsNot Nothing Then
                    cmb_Jenis_Dokumen_Director.SetTextWithTextValue(jenisdokumen_director_edit.Kode, jenisdokumen_director_edit.Keterangan)
                End If
                txt_number_identification_Director.Value = .Number
                txt_issued_by_Director.Text = .Issued_By
                txt_issued_date_Director.Value = .Issue_Date
                txt_issued_expired_date_Director.Value = .Expiry_Date
                txt_identification_comment_Director.Text = .Identification_Comment
                'StoreIssueCountry_Director.DataSource = goAML_CustomerBLL.GetListNamaNegara().ToList()
                'StoreIssueCountry_Director.DataBind()
                Dim kodenegara_director_edit = goAML_CustomerBLL.GetNamaNegaraByKeterangan(.Country)
                If kodenegara_director_edit IsNot Nothing Then
                    cmb_issued_country_Director.SetTextWithTextValue(kodenegara_director_edit.Kode, kodenegara_director_edit.Keterangan)
                End If


            End With

            'EnvironmentForm("Detail")
        End If
    End Sub


    Protected Sub btnAddCustomerDirector_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            tempflag = 0
            FP_Director.Hidden = False
            WindowDetailDirector.Hidden = False
            'cmb_director.ReadOnly = False
            cmb_peran.IsReadOnly = False
            txt_Director_Title.ReadOnly = False
            txt_Director_Last_Name.ReadOnly = False
            txt_Director_SSN.ReadOnly = False
            txt_Director_BirthDate.ReadOnly = False
            txt_Director_Birth_Place.ReadOnly = False
            txt_Director_Mothers_Name.ReadOnly = False
            txt_Director_Alias.ReadOnly = False
            txt_Director_Passport_Number.ReadOnly = False
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            cmb_Director_Passport_Country.IsReadOnly = False
            'End Update
            txt_Director_ID_Number.ReadOnly = False
            cmb_Director_Nationality1.IsReadOnly = False
            cmb_Director_Nationality2.IsReadOnly = False
            cmb_Director_Nationality3.IsReadOnly = False
            cmb_Director_Gender.IsReadOnly = False
            cmb_Director_Residence.IsReadOnly = False
            txt_Director_Email.ReadOnly = False
            txt_Director_Email2.ReadOnly = False
            txt_Director_Email3.ReadOnly = False
            txt_Director_Email4.ReadOnly = False
            txt_Director_Email5.ReadOnly = False
            txt_Director_Occupation.ReadOnly = False
            txt_Director_employer_name.ReadOnly = False
            cb_Director_Deceased.ReadOnly = False
            txt_Director_Deceased_Date.ReadOnly = False
            txt_Director_Tax_Number.ReadOnly = False
            cb_Director_Tax_Reg_Number.ReadOnly = False
            txt_Director_Source_of_Wealth.ReadOnly = False
            txt_Director_Comments.ReadOnly = False
            btnsaveDirector.Hidden = False
            btnsavedirectorphone.Hidden = False
            btnsavedirectorAddress.Hidden = False
            btnsaveEmpdirectorPhone.Hidden = False
            btnsaveEmpdirectorAddress.Hidden = False
            btnaddidentificationdirector.Hidden = False
            WindowDetailDirector.Title = "Customer Director Add"


            'StorePeran.DataSource = goAML_CustomerBLL.GetListPeranDalamKorporasi()
            'StorePeran.DataBind()
            'Store_Director_Gender.DataSource = goAML_CustomerBLL.GetListJenisKelamin()
            'Store_Director_Gender.DataBind()
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            'Store_Director_PassportCountry.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_PassportCountry.DataBind()
            'End Update
            'Store_Director_Nationality1.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_Nationality1.DataBind()
            'Store_Director_Nationality2.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_Nationality2.DataBind()
            'Store_Director_Nationality3.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_Nationality3.DataBind()
            'Store_Director_Residence.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_Director_Residence.DataBind()

            ClearinputCustomerDirector()
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdCustDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditCustDirector(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailCustDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ClearinputCustomerDirector()
        'cmb_director.Value = Nothing
        cmb_peran.SetTextValue("")
        txt_Director_Title.Text = ""
        txt_Director_Last_Name.Text = ""
        txt_Director_SSN.Text = ""
        txt_Director_BirthDate.Text = ""
        txt_Director_Birth_Place.Text = ""
        txt_Director_Mothers_Name.Text = ""
        txt_Director_Alias.Text = ""
        txt_Director_Passport_Number.Text = ""
        'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
        cmb_Director_Passport_Country.SetTextValue("")
        'End Update
        txt_Director_ID_Number.Text = ""
        cmb_Director_Nationality1.SetTextValue("")
        cmb_Director_Nationality2.SetTextValue("")
        cmb_Director_Nationality3.SetTextValue("")
        cmb_Director_Gender.SetTextValue("")
        cmb_Director_Residence.SetTextValue("")
        txt_Director_Email.Text = ""
        txt_Director_Email2.Text = ""
        txt_Director_Email3.Text = ""
        txt_Director_Email4.Text = ""
        txt_Director_Email5.Text = ""
        txt_Director_Occupation.Text = ""
        txt_Director_employer_name.Text = ""
        cb_Director_Deceased.Checked = False
        txt_Director_Deceased_Date.Value = ""
        txt_Director_Tax_Number.Text = ""
        cb_Director_Tax_Reg_Number.Checked = False
        txt_Director_Source_of_Wealth.Text = ""
        txt_Director_Comments.Text = ""
        Store_Director_Employer_Address.DataBind()
        Store_Director_Employer_Phone.DataBind()
        Store_DirectorAddress.DataBind()
        Store_DirectorPhone.DataBind()
        Store_Director_Identification.DataBind()
    End Sub

    Sub LoadDataDirectorDetail(id As Long)

        Dim objCustomerDirectorDetail = objListgoAML_Ref_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id).FirstOrDefault()
        If Not objCustomerDirectorDetail Is Nothing Then
            FP_Director.Hidden = False
            WindowDetailDirector.Hidden = False
            btnsaveDirector.Hidden = True
            WindowDetailDirector.Title = "Director Detail"
            'cmb_director.ReadOnly = True
            cmb_peran.IsReadOnly = True
            txt_Director_Title.ReadOnly = True
            txt_Director_Last_Name.ReadOnly = True
            txt_Director_SSN.ReadOnly = True
            txt_Director_BirthDate.ReadOnly = True
            txt_Director_Birth_Place.ReadOnly = True
            txt_Director_Mothers_Name.ReadOnly = True
            txt_Director_Alias.ReadOnly = True
            txt_Director_Passport_Number.ReadOnly = True
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            cmb_Director_Passport_Country.IsReadOnly = True
            'End Update
            txt_Director_ID_Number.ReadOnly = True
            cmb_Director_Nationality1.IsReadOnly = True
            cmb_Director_Nationality2.IsReadOnly = True
            cmb_Director_Nationality3.IsReadOnly = True
            cmb_Director_Gender.IsReadOnly = True
            cmb_Director_Residence.IsReadOnly = True
            txt_Director_Email.ReadOnly = True
            txt_Director_Email2.ReadOnly = True
            txt_Director_Email3.ReadOnly = True
            txt_Director_Email4.ReadOnly = True
            txt_Director_Email5.ReadOnly = True
            txt_Director_Occupation.ReadOnly = True
            txt_Director_employer_name.ReadOnly = True
            cb_Director_Deceased.ReadOnly = True
            txt_Director_Deceased_Date.ReadOnly = True
            txt_Director_Tax_Number.ReadOnly = True
            cb_Director_Tax_Reg_Number.ReadOnly = True
            txt_Director_Source_of_Wealth.ReadOnly = True
            txt_Director_Comments.ReadOnly = True
            'StorePeran.DataSource = goAML_CustomerBLL.GetListPeranDalamKorporasi()
            'StorePeran.DataBind()
            Dim peran = goAML_CustomerBLL.GetRoleDirectorByID(objCustomerDirectorDetail.Role)
            If peran IsNot Nothing Then
                cmb_peran.SetTextWithTextValue(peran.Kode, peran.Keterangan)
            End If
            txt_Director_Title.Text = objCustomerDirectorDetail.Title
            txt_Director_Last_Name.Text = objCustomerDirectorDetail.Last_Name
            txt_Director_SSN.Text = objCustomerDirectorDetail.SSN
            If objCustomerDirectorDetail.BirthDate IsNot Nothing Then
                txt_Director_BirthDate.Text = objCustomerDirectorDetail.BirthDate
            End If
            txt_Director_Birth_Place.Text = objCustomerDirectorDetail.Birth_Place
            txt_Director_Mothers_Name.Text = objCustomerDirectorDetail.Mothers_Name
            txt_Director_Alias.Text = objCustomerDirectorDetail.Alias
            txt_Director_ID_Number.Text = objCustomerDirectorDetail.ID_Number
            txt_Director_Passport_Number.Text = objCustomerDirectorDetail.Passport_Number
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            Dim director_passport_country = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Passport_Country)
            If director_passport_country IsNot Nothing Then
                cmb_Director_Passport_Country.SetTextWithTextValue(director_passport_country.Kode, director_passport_country.Keterangan)
            End If
            Dim director_Nationality1 = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Nationality1)
            If director_Nationality1 IsNot Nothing Then
                cmb_Director_Nationality1.SetTextWithTextValue(director_Nationality1.Kode, director_Nationality1.Keterangan)
            End If
            Dim director_Nationality2 = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Nationality2)
            If director_Nationality2 IsNot Nothing Then
                cmb_Director_Nationality2.SetTextWithTextValue(director_Nationality2.Kode, director_Nationality2.Keterangan)
            End If
            Dim director_Nationality3 = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Nationality3)
            If director_Nationality3 IsNot Nothing Then
                cmb_Director_Nationality3.SetTextWithTextValue(director_Nationality3.Kode, director_Nationality3.Keterangan)
            End If
            Dim director_Residence = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerDirectorDetail.Residence)
            If director_Residence IsNot Nothing Then
                cmb_Director_Residence.SetTextWithTextValue(director_Residence.Kode, director_Residence.Keterangan)
            End If
            Dim gender = goAML_CustomerBLL.GetJenisKelamin(objCustomerDirectorDetail.Gender)
            If gender IsNot Nothing Then
                cmb_Director_Gender.SetTextWithTextValue(gender.Kode, gender.Keterangan)
            End If
            'End Update
            'If objCustomerDirectorDetail.Nationality1 IsNot Nothing Then
            '    cmb_Director_Nationality1.Value = objCustomerDirectorDetail.Nationality1
            'End If
            'If objCustomerDirectorDetail.Nationality2 IsNot Nothing Then
            '    cmb_Director_Nationality2.Value = objCustomerDirectorDetail.Nationality2
            'End If
            'If objCustomerDirectorDetail.Nationality3 IsNot Nothing Then
            '    cmb_Director_Nationality3.Value = objCustomerDirectorDetail.Nationality3
            'End If
            'If objCustomerDirectorDetail.Residence IsNot Nothing Then
            '    cmb_Director_Residence.Value = objCustomerDirectorDetail.Residence
            'End If

            'If objCustomerDirectorDetail.Gender IsNot Nothing Then
            '    cmb_Director_Gender.Value = objCustomerDirectorDetail.Gender
            'End If
            txt_Director_Email.Text = objCustomerDirectorDetail.Email
            txt_Director_Email2.Text = objCustomerDirectorDetail.Email2
            txt_Director_Email3.Text = objCustomerDirectorDetail.Email3
            txt_Director_Email4.Text = objCustomerDirectorDetail.Email4
            txt_Director_Email5.Text = objCustomerDirectorDetail.Email5
            txt_Director_Occupation.Text = objCustomerDirectorDetail.Occupation
            txt_Director_employer_name.Text = objCustomerDirectorDetail.Employer_Name
            If objCustomerDirectorDetail.Deceased = True Then
                cb_Director_Deceased.Checked = True
                txt_Director_Deceased_Date.Value = objCustomerDirectorDetail.Deceased_Date
            End If
            txt_Director_Tax_Number.Text = objCustomerDirectorDetail.Tax_Number
            If objCustomerDirectorDetail.Tax_Reg_Number = True Then
                cb_Director_Tax_Reg_Number.Checked = True
            End If
            txt_Director_Source_of_Wealth.Text = objCustomerDirectorDetail.Source_of_Wealth
            txt_Director_Comments.Text = objCustomerDirectorDetail.Comments
            btnaddidentificationdirector.Hidden = True
            btnsavedirectorphone.Hidden = True
            btnsavedirectorAddress.Hidden = True
            btnsaveEmpdirectorPhone.Hidden = True
            btnsaveEmpdirectorAddress.Hidden = True
            Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
            Store_DirectorAddress.DataBind()
            Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
            Store_DirectorPhone.DataBind()
            Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
            Store_Director_Employer_Address.DataBind()
            Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
            Store_Director_Employer_Phone.DataBind()
            Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()
            Store_Director_Identification.DataBind()

        End If
    End Sub

    Sub LoadDataEditCustDirector(id As Long)
        objTempgoAML_Ref_Director = objListgoAML_Ref_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
        objTempCustomerDirectorEdit = objListgoAML_vw_Customer_Entity_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
        tempflag = id
        If Not objTempCustomerDirectorEdit Is Nothing Then
            FP_Director.Hidden = False
            WindowDetailDirector.Hidden = False
            btnsaveDirector.Hidden = False
            'cmb_director.ReadOnly = False
            cmb_peran.IsReadOnly = False
            txt_Director_Title.ReadOnly = False
            txt_Director_Last_Name.ReadOnly = False
            txt_Director_SSN.ReadOnly = False
            txt_Director_BirthDate.ReadOnly = False
            txt_Director_Birth_Place.ReadOnly = False
            txt_Director_Mothers_Name.ReadOnly = False
            txt_Director_Alias.ReadOnly = False
            txt_Director_Passport_Number.ReadOnly = False
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            cmb_Director_Passport_Country.IsReadOnly = False
            'End Update
            txt_Director_ID_Number.ReadOnly = False
            cmb_Director_Nationality1.IsReadOnly = False
            cmb_Director_Nationality2.IsReadOnly = False
            cmb_Director_Nationality3.IsReadOnly = False
            cmb_Director_Gender.IsReadOnly = False
            cmb_Director_Residence.IsReadOnly = False
            txt_Director_Email.ReadOnly = False
            txt_Director_Email2.ReadOnly = False
            txt_Director_Email3.ReadOnly = False
            txt_Director_Email4.ReadOnly = False
            txt_Director_Email5.ReadOnly = False
            txt_Director_Occupation.ReadOnly = False
            txt_Director_employer_name.ReadOnly = False
            cb_Director_Deceased.ReadOnly = False
            txt_Director_Deceased_Date.ReadOnly = False
            txt_Director_Tax_Number.ReadOnly = False
            cb_Director_Tax_Reg_Number.ReadOnly = False
            txt_Director_Source_of_Wealth.ReadOnly = False
            txt_Director_Comments.ReadOnly = False
            btnsavedirectorphone.Hidden = False
            btnsavedirectorAddress.Hidden = False
            btnsaveEmpdirectorPhone.Hidden = False
            btnsaveEmpdirectorAddress.Hidden = False
            btnaddidentificationdirector.Hidden = False
            btn_save_identification_Director.Hidden = False
            WindowDetailDirector.Title = "Director Edit"
            With objTempCustomerDirectorEdit
                'StoreDirector.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,INDV_Last_Name,cb_value from Vw_CB_Signatory_Person", Nothing)
                'StoreDirector.DataBind()
                'cmb_director.SetValue(objTempgoAML_Ref_Director.FK_Customer_ID)
                'StorePeran.DataSource = goAML_CustomerBLL.GetListPeranDalamKorporasi()
                'StorePeran.DataBind()
                Dim peran = goAML_CustomerBLL.GetRoleDirectorByID(objTempgoAML_Ref_Director.Role)
                If peran IsNot Nothing Then
                    cmb_peran.SetTextWithTextValue(peran.Kode, peran.Keterangan)
                End If
                txt_Director_Title.Text = objTempgoAML_Ref_Director.Title
                txt_Director_Last_Name.Text = objTempgoAML_Ref_Director.Last_Name
                txt_Director_SSN.Text = objTempgoAML_Ref_Director.SSN
                If objTempgoAML_Ref_Director.BirthDate IsNot Nothing Then
                    txt_Director_BirthDate.Text = objTempgoAML_Ref_Director.BirthDate
                End If
                txt_Director_Birth_Place.Text = objTempgoAML_Ref_Director.Birth_Place
                txt_Director_Mothers_Name.Text = objTempgoAML_Ref_Director.Mothers_Name
                txt_Director_Alias.Text = objTempgoAML_Ref_Director.Alias
                txt_Director_ID_Number.Text = objTempgoAML_Ref_Director.ID_Number
                txt_Director_Passport_Number.Text = objTempgoAML_Ref_Director.Passport_Number
                'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
                Dim director_passport_country = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Passport_Country)
                If director_passport_country IsNot Nothing Then
                    cmb_Director_Passport_Country.SetTextWithTextValue(director_passport_country.Kode, director_passport_country.Keterangan)
                End If
                Dim director_Nationality1 = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Nationality1)
                If director_Nationality1 IsNot Nothing Then
                    cmb_Director_Nationality1.SetTextWithTextValue(director_Nationality1.Kode, director_Nationality1.Keterangan)
                End If
                Dim director_Nationality2 = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Nationality2)
                If director_Nationality2 IsNot Nothing Then
                    cmb_Director_Nationality2.SetTextWithTextValue(director_Nationality2.Kode, director_Nationality2.Keterangan)
                End If
                Dim director_Nationality3 = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Nationality3)
                If director_Nationality3 IsNot Nothing Then
                    cmb_Director_Nationality3.SetTextWithTextValue(director_Nationality3.Kode, director_Nationality3.Keterangan)
                End If
                Dim director_Residence = goAML_CustomerBLL.GetNamaNegaraByID(objTempgoAML_Ref_Director.Residence)
                If director_Residence IsNot Nothing Then
                    cmb_Director_Residence.SetTextWithTextValue(director_Residence.Kode, director_Residence.Keterangan)
                End If
                Dim gender = goAML_CustomerBLL.GetJenisKelamin(objTempgoAML_Ref_Director.Gender)
                If gender IsNot Nothing Then
                    cmb_Director_Gender.SetTextWithTextValue(gender.Kode, gender.Keterangan)
                End If
                'End Update
                'End Update
                'If objTempgoAML_Ref_Director.Nationality1 IsNot Nothing Then
                '    cmb_Director_Nationality1.Value = objTempgoAML_Ref_Director.Nationality1
                'End If
                'If objTempgoAML_Ref_Director.Nationality2 IsNot Nothing Then
                '    cmb_Director_Nationality2.Value = objTempgoAML_Ref_Director.Nationality2
                'End If
                'If objTempgoAML_Ref_Director.Nationality3 IsNot Nothing Then
                '    cmb_Director_Nationality3.Value = objTempgoAML_Ref_Director.Nationality3
                'End If
                'If objTempgoAML_Ref_Director.Residence IsNot Nothing Then
                '    cmb_Director_Residence.Value = objTempgoAML_Ref_Director.Residence
                'End If

                'If objTempgoAML_Ref_Director.Gender IsNot Nothing Then
                '    cmb_Director_Gender.SelectedItemValue = objTempgoAML_Ref_Director.Gender
                'End If
                txt_Director_Email.Text = objTempgoAML_Ref_Director.Email
                txt_Director_Email2.Text = objTempgoAML_Ref_Director.Email2
                txt_Director_Email3.Text = objTempgoAML_Ref_Director.Email3
                txt_Director_Email4.Text = objTempgoAML_Ref_Director.Email4
                txt_Director_Email5.Text = objTempgoAML_Ref_Director.Email5
                txt_Director_Occupation.Text = objTempgoAML_Ref_Director.Occupation
                txt_Director_employer_name.Text = objTempgoAML_Ref_Director.Employer_Name
                If objTempgoAML_Ref_Director.Deceased = True Then
                    cb_Director_Deceased.Checked = True
                    txt_Director_Deceased_Date.Value = objTempgoAML_Ref_Director.Deceased_Date
                End If
                txt_Director_Tax_Number.Text = objTempgoAML_Ref_Director.Tax_Number
                If objTempgoAML_Ref_Director.Tax_Reg_Number = True Then
                    cb_Director_Tax_Reg_Number.Checked = True
                End If
                txt_Director_Source_of_Wealth.Text = objTempgoAML_Ref_Director.Source_of_Wealth
                txt_Director_Comments.Text = objTempgoAML_Ref_Director.Comments

                Store_DirectorAddress.DataSource = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
                Store_DirectorAddress.DataBind()
                Store_DirectorPhone.DataSource = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
                Store_DirectorPhone.DataBind()
                Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
                Store_Director_Employer_Address.DataBind()
                Store_Director_Employer_Phone.DataSource = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
                Store_Director_Employer_Phone.DataBind()
                Store_Director_Identification.DataSource = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()
                Store_Director_Identification.DataBind()
                'Dim DirectorEmpAddress = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
                'If DirectorEmpAddress.Count > 0 Then
                '    btnsaveEmpdirectorAddress.Hidden = True
                'End If
                'Dim DirectorEmpPhone = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
                'If DirectorEmpPhone.Count > 0 Then
                '    btnsaveEmpdirectorPhone.Hidden = True
                'End If
            End With

            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub DeleteRecordTaskDetailCustDirector(id As Long)
        Dim objDelVWDirector As NawaDevDAL.Vw_Customer_Director = objListgoAML_vw_Customer_Entity_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
        Dim objDelVWAddress As List(Of NawaDevDAL.Vw_Director_Addresses) = objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        Dim objDelVWPhone As List(Of NawaDevDAL.Vw_Director_Phones) = objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        Dim objDelVWEMPAddress As List(Of NawaDevDAL.Vw_Director_Employer_Addresses) = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        Dim objDelEMPVWPhone As List(Of NawaDevDAL.Vw_Director_Employer_Phones) = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        Dim objDelVWIdentification As List(Of NawaDevDAL.Vw_Person_Identification) = objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()

        Dim objxDelDirector As NawaDevDAL.goAML_Ref_Customer_Entity_Director = objListgoAML_Ref_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = id)
        Dim objxDelAddress As List(Of NawaDevDAL.goAML_Ref_Address) = objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        Dim objxDelPhone As List(Of NawaDevDAL.goAML_Ref_Phone) = objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        Dim objxDelEMPAddress As List(Of NawaDevDAL.goAML_Ref_Address) = objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = id).ToList()
        Dim objxDelEMPPhone As List(Of NawaDevDAL.goAML_Ref_Phone) = objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = id).ToList()
        Dim objxDelIdentification As List(Of NawaDevDAL.goAML_Person_Identification) = objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = id).ToList()

        If Not objxDelDirector Is Nothing Then
            objListgoAML_Ref_Director.Remove(objxDelDirector)
            For Each item As goAML_Ref_Address In objxDelAddress
                objListgoAML_Ref_Director_Address.Remove(item)
            Next
            For Each item As goAML_Ref_Phone In objxDelPhone
                objListgoAML_Ref_Director_Phone.Remove(item)
            Next
            For Each item As goAML_Ref_Address In objxDelEMPAddress
                objListgoAML_Ref_Director_Employer_Address.Remove(item)
            Next
            For Each item As goAML_Ref_Phone In objxDelEMPPhone
                objListgoAML_Ref_Director_Employer_Phone.Remove(item)
            Next
            For Each item As goAML_Person_Identification In objxDelIdentification
                objListgoAML_Ref_Identification_Director.Remove(item)
            Next
        End If

        If Not objDelVWDirector Is Nothing Then
            objListgoAML_vw_Customer_Entity_Director.Remove(objDelVWDirector)
            For Each item As Vw_Director_Addresses In objDelVWAddress
                objListgoAML_vw_Director_Addresses.Remove(item)
            Next
            For Each item As Vw_Director_Phones In objDelVWPhone
                objListgoAML_vw_Director_Phones.Remove(item)
            Next
            For Each item As Vw_Director_Employer_Addresses In objDelVWEMPAddress
                objListgoAML_vw_Director_Employer_Addresses.Remove(item)
            Next
            For Each item As Vw_Director_Employer_Phones In objDelEMPVWPhone
                objListgoAML_vw_Director_Employer_Phones.Remove(item)
            Next
            For Each item As Vw_Person_Identification In objDelVWIdentification
                objListgoAML_vw_Customer_Identification_Director.Remove(item)
            Next

            Store_Director_Corp.DataSource = objListgoAML_vw_Customer_Entity_Director.ToList()
            Store_Director_Corp.DataBind()
        End If


        FP_Director.Hidden = True
        WindowDetailDirector.Hidden = True
        ClearinputCustomerDirector()

    End Sub

    Protected Sub btnSaveCustomerDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataCustomerDirectorValid() Then
                'If objTempCustomerPhoneEdit Is Nothing Then
                If objTempCustomerDirectorEdit Is Nothing Then
                    SaveAddDirectorDetail()
                Else
                    SaveEditDirectorDetail()
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Function IsDataCustomerDirectorValid() As Boolean
        'If cmb_director.RawValue = "Select One" Then
        '    Throw New Exception("Please Select Person")
        'End If
        If cmb_peran.SelectedItemValue = "" Then
            Throw New Exception("Role is required")
        End If
        If txt_Director_Occupation.Text = "" Then
            Throw New Exception("Occupation  is required")
        End If
        If txt_Director_Source_of_Wealth.Text = "" Then
            Throw New Exception("Source of Wealth is required")
        End If
        If txt_Director_Last_Name.Text = "" Then
            Throw New Exception("Full Name is required")
        End If
        If txt_Director_BirthDate.RawValue = "" Then
            Throw New Exception("Birth Date is required")
        End If
        If txt_Director_Birth_Place.Text = "" Then
            Throw New Exception("Birth Place is required")
        End If
        'Update: Zikri_08102020
        If txt_Director_SSN.Text.Trim IsNot "" Then
            If Not IsNumber(txt_Director_SSN.Text) Then
                Throw New Exception("NIK is Invalid")
            End If
            If txt_Director_SSN.Text.Trim.Length <> 16 Then
                Throw New Exception("NIK must contains 16 digit number")
            End If
        End If
        'End Update
        If cmb_Director_Gender.SelectedItemValue = "" Then
            Throw New Exception("Gender is required")
        End If
        If cmb_Director_Nationality1.SelectedItemValue = "" Then
            Throw New Exception("Nationality 1 is required")
        End If
        If cmb_Director_Residence.SelectedItemValue = "" Then
            Throw New Exception("Residence is required")
        End If
        If objListgoAML_Ref_Director_Phone.Count = 0 Then
            Throw New Exception("Phone Director is required")
        End If
        If objListgoAML_Ref_Director_Address.Count = 0 Then
            Throw New Exception("Address Director is required")
        End If
        If Not String.IsNullOrEmpty(txt_Director_ID_Number.Text) Then
            If objListgoAML_Ref_Identification_Director.Count = 0 Then
                Throw New Exception("Identification is required")
            End If
        End If
        'If objListgoAML_Ref_Director_Employer_Address.Count = 0 Then
        '    Throw New Exception("Employer Address Director is required")
        'End If
        'If objListgoAML_Ref_Director_Employer_Phone.Count = 0 Then
        '    Throw New Exception("Employer Phone Director is required")
        'End If
        Return True
    End Function

    Sub SaveAddDirectorDetail()

        Dim objNewVWgoAML_Customer As New NawaDevDAL.Vw_Customer_Director
        Dim objNewgoAML_Customer As New NawaDevDAL.goAML_Ref_Customer_Entity_Director
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_Customer
            .PK_goAML_Ref_Customer_Entity_Director_ID = intpk
            .LastName = txt_Director_Last_Name.Text
            .Role = cmb_peran.SelectedItemText
        End With
        objListgoAML_vw_Customer_Entity_Director.Add(objNewVWgoAML_Customer)

        With objNewgoAML_Customer
            .PK_goAML_Ref_Customer_Entity_Director_ID = intpk
            ''.FK_Customer_ID = cmb_director.Value
            .Role = cmb_peran.SelectedItemValue
            .Title = txt_Director_Title.Text
            .Gender = cmb_Director_Gender.SelectedItemValue
            .Last_Name = txt_Director_Last_Name.Text
            If txt_Director_BirthDate.RawValue IsNot Nothing Then
                .BirthDate = Convert.ToDateTime(txt_Director_BirthDate.RawValue)
            Else
                .BirthDate = Nothing
            End If

            .Birth_Place = txt_Director_Birth_Place.Text
            .Mothers_Name = txt_Director_Mothers_Name.Text
            .Alias = txt_Director_Alias.Text
            .SSN = txt_Director_SSN.Text
            .Passport_Number = txt_Director_Passport_Number.Text
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            .Passport_Country = cmb_Director_Passport_Country.SelectedItemValue
            'End Update
            .ID_Number = txt_Director_ID_Number.Text
            .Nationality1 = cmb_Director_Nationality1.SelectedItemValue
            .Nationality2 = cmb_Director_Nationality2.SelectedItemValue
            .Nationality3 = cmb_Director_Nationality3.SelectedItemValue
            .Residence = cmb_Director_Residence.SelectedItemValue
            .Email = txt_Director_Email.Text
            .Email2 = txt_Director_Email2.Text
            .Email3 = txt_Director_Email3.Text
            .Email4 = txt_Director_Email4.Text
            .Email5 = txt_Director_Email5.Text
            .Occupation = txt_Director_Occupation.Text
            .Employer_Name = txt_Director_employer_name.Text
            .Deceased = Convert.ToBoolean(IIf(cb_Director_Deceased.Checked = True, 1, 0))
            If cb_Director_Deceased.Checked Then
                .Deceased_Date = Convert.ToDateTime(txt_Director_Deceased_Date.RawValue)
            Else
                .Deceased_Date = Nothing
            End If
            .Tax_Number = txt_Director_Tax_Number.Text
            .Tax_Reg_Number = Convert.ToBoolean(IIf(cb_Director_Tax_Reg_Number.Checked = True, 1, 0))
            .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
            .Comments = txt_Director_Comments.Text
        End With

        objListgoAML_Ref_Director.Add(objNewgoAML_Customer)
        For Each item As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
            item.FK_To_Table_ID = intpk
        Next

        For Each item As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
            item.FK_for_Table_ID = intpk
        Next

        For Each item As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
            item.FK_To_Table_ID = intpk
        Next

        For Each item As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
            item.FK_for_Table_ID = intpk
        Next

        For Each item As NawaDevDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
            item.FK_Person_ID = intpk
        Next

        For Each item As NawaDevDAL.Vw_Person_Identification In objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
            item.FK_Person_ID = intpk
        Next

        For Each item As NawaDevDAL.Vw_Director_Addresses In objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
            item.FK_To_Table_ID = intpk
        Next

        For Each item As NawaDevDAL.Vw_Director_Phones In objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
            item.FK_for_Table_ID = intpk
        Next

        For Each item As NawaDevDAL.Vw_Director_Employer_Addresses In objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
            item.FK_To_Table_ID = intpk
        Next

        For Each item As NawaDevDAL.Vw_Director_Employer_Phones In objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
            item.FK_for_Table_ID = intpk
        Next


        Store_Director_Corp.DataSource = objListgoAML_vw_Customer_Entity_Director.ToList()
        Store_Director_Corp.DataBind()

        FP_Director.Hidden = True
        WindowDetailDirector.Hidden = True
        ClearinputCustomerDirector()
    End Sub

    Sub SaveEditDirectorDetail()


        With objTempCustomerDirectorEdit
            .Role = cmb_peran.SelectedItemText
            .LastName = txt_Director_Last_Name.Text


        End With

        With objTempgoAML_Ref_Director
            .Role = cmb_peran.SelectedItemValue
            .Title = txt_Director_Title.Text
            .Last_Name = txt_Director_Last_Name.Text
            .SSN = txt_Director_SSN.Text
            If txt_Director_BirthDate.RawValue IsNot Nothing Then
                .BirthDate = Convert.ToDateTime(txt_Director_BirthDate.RawValue)
            Else
                .BirthDate = Nothing
            End If
            .Birth_Place = txt_Director_Birth_Place.Text
            .Mothers_Name = txt_Director_Mothers_Name.Text
            .Alias = txt_Director_Alias.Text
            .Passport_Number = txt_Director_Passport_Number.Text
            'Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown
            .Passport_Country = cmb_Director_Passport_Country.SelectedItemValue
            'End Update
            .ID_Number = txt_Director_ID_Number.Text
            .Nationality1 = cmb_Director_Nationality1.SelectedItemValue
            .Nationality2 = cmb_Director_Nationality2.SelectedItemValue
            .Nationality3 = cmb_Director_Nationality3.SelectedItemValue
            .Residence = cmb_Director_Residence.SelectedItemValue
            .Gender = cmb_Director_Gender.SelectedItemValue

            .Email = txt_Director_Email.Text
            .Email2 = txt_Director_Email2.Text
            .Email3 = txt_Director_Email3.Text
            .Email4 = txt_Director_Email4.Text
            .Email5 = txt_Director_Email5.Text
            .Occupation = txt_Director_Occupation.Text
            .Employer_Name = txt_Director_employer_name.Text
            .Deceased = Convert.ToBoolean(IIf(cb_Director_Deceased.Checked = True, 1, 0))
            If cb_Director_Deceased.Checked Then
                .Deceased_Date = Convert.ToDateTime(txt_Director_Deceased_Date.RawValue)
            Else
                .Deceased_Date = Nothing
            End If
            .Tax_Number = txt_Director_Tax_Number.Text
            .Tax_Reg_Number = Convert.ToBoolean(IIf(cb_Director_Tax_Reg_Number.Checked = True, 1, 0))
            .Source_of_Wealth = txt_Director_Source_of_Wealth.Text
            .Comments = txt_Director_Comments.Text

            For Each item As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                item.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                item.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                item.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                item.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As NawaDevDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
                item.FK_Person_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As NawaDevDAL.Vw_Person_Identification In objListgoAML_vw_Customer_Identification_Director.Where(Function(x) x.FK_Person_ID = 0).ToList()
                item.FK_Person_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As NawaDevDAL.Vw_Director_Addresses In objListgoAML_vw_Director_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                item.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As NawaDevDAL.Vw_Director_Phones In objListgoAML_vw_Director_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                item.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As NawaDevDAL.Vw_Director_Employer_Addresses In objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = 0).ToList()
                item.FK_To_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next

            For Each item As NawaDevDAL.Vw_Director_Employer_Phones In objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = 0).ToList()
                item.FK_for_Table_ID = .PK_goAML_Ref_Customer_Entity_Director_ID
            Next


        End With


        objTempCustomerDirectorEdit = Nothing
        objTempgoAML_Ref_Director = Nothing

        Store_Director_Corp.DataSource = objListgoAML_vw_Customer_Entity_Director.ToList()
        Store_Director_Corp.DataBind()

        FP_Director.Hidden = True
        WindowDetailDirector.Hidden = True
        ClearinputCustomerDirector()
    End Sub

    Protected Sub btnBackSaveCustomerDirector_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Director.Hidden = True
            WindowDetailDirector.Hidden = True
            ClearinputCustomerDirector()
            objTempCustomerDirectorEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailEmployerPhone(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditEmployerPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailEmployerPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailEmpDirectorPhone(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditEmpDirectorPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailEmpDirectorPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DeleteRecordTaskDetailEmployerPhone(id As Long)
        Dim objDelVWPhone As NawaDevDAL.Vw_Employer_Phones = objListgoAML_vw_Employer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objxDelPhone As NawaDevDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)

        If Not objxDelPhone Is Nothing Then
            objListgoAML_Ref_Employer_Phone.Remove(objxDelPhone)
        End If

        If Not objDelVWPhone Is Nothing Then
            objListgoAML_vw_Employer_Phones.Remove(objDelVWPhone)
            Store_Empoloyer_Phone.DataSource = objListgoAML_vw_Employer_Phones.ToList()
            Store_Empoloyer_Phone.DataBind()
        End If

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputCustomerPhones()

    End Sub

    Sub LoadDataEditEmployerPhone(id As Long)
        objTempgoAML_Ref_EmployerPhones = objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        objTempEmployerPhonesEdit = objListgoAML_vw_Employer_Phones.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objTempEmployerPhonesEdit Is Nothing Then
            FormPanelEmpTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            Emp_cb_phone_Contact_Type.IsReadOnly = False
            Emp_cb_phone_Communication_Type.IsReadOnly = False
            Emp_txt_phone_Country_prefix.ReadOnly = False
            Emp_txt_phone_number.ReadOnly = False
            Emp_txt_phone_extension.ReadOnly = False
            Emp_txt_phone_comments.ReadOnly = False
            btn_saveempphone.Hidden = False

            WindowDetail.Title = "Edit Phone"
            ClearinputCustomerEmpPhones()
            With objTempEmployerPhonesEdit
                'Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak
                'Emp_Store_Contact_Type.DataBind()
                'Emp_cb_phone_Contact_Type.SetValue(objTempEmployerPhonesEdit.Tph_Contact_Type)
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_emp_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_emp_contact_type IsNot Nothing Then
                    Emp_cb_phone_Contact_Type.SetTextWithTextValue(obj_emp_contact_type.Kode, obj_emp_contact_type.Keterangan)
                End If

                'Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Emp_Store_Communication_Type.DataBind()
                'Emp_cb_phone_Communication_Type.SetValue(objTempEmployerPhonesEdit.Tph_Communication_Type)
                Dim obj_emp_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_emp_communication_type IsNot Nothing Then
                    Emp_cb_phone_Communication_Type.SetTextWithTextValue(obj_emp_communication_type.Kode, obj_emp_communication_type.Keterangan)
                End If
                'End Update

                Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Emp_txt_phone_number.Text = .tph_number
                Emp_txt_phone_extension.Text = .tph_extension
                Emp_txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataDetailEmployerPhone(id As Long)
        Dim objCustomerPhonesDetail As goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelEmpTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            Emp_cb_phone_Contact_Type.IsReadOnly = True
            Emp_cb_phone_Communication_Type.IsReadOnly = True
            Emp_txt_phone_Country_prefix.ReadOnly = True
            Emp_txt_phone_number.ReadOnly = True
            Emp_txt_phone_extension.ReadOnly = True
            Emp_txt_phone_comments.ReadOnly = True
            btn_saveempphone.Hidden = True

            WindowDetail.Title = "Phone Detail"
            ClearinputCustomerEmpPhones()
            With objCustomerPhonesDetail
                'Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Emp_Store_Contact_Type.DataBind()
                'Emp_cb_phone_Contact_Type.SetValue(.Tph_Contact_Type)
                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                Dim obj_emp_contact_type = goAML_CustomerBLL.GetContactTypeByID(.Tph_Contact_Type)
                If obj_emp_contact_type IsNot Nothing Then
                    Emp_cb_phone_Contact_Type.SetTextWithTextValue(obj_emp_contact_type.Kode, obj_emp_contact_type.Keterangan)
                End If

                'Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Emp_Store_Communication_Type.DataBind()
                'Emp_cb_phone_Communication_Type.SetValue(.Tph_Communication_Type)
                Dim obj_emp_communication_type = goAML_CustomerBLL.GetCommunicationTypeByID(.Tph_Communication_Type)
                If obj_emp_communication_type IsNot Nothing Then
                    Emp_cb_phone_Communication_Type.SetTextWithTextValue(obj_emp_communication_type.Kode, obj_emp_communication_type.Keterangan)
                End If
                'End Update
                Emp_txt_phone_Country_prefix.Text = .tph_country_prefix
                Emp_txt_phone_number.Text = .tph_number
                Emp_txt_phone_extension.Text = .tph_extension
                Emp_txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Protected Sub GrdCmdEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataEmployerAddressDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditEmployerAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailEmployerAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdDirectorEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDirectorEmployerAddressDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditDirectorEmployerAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailDirectorEmployerAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Sub DeleteRecordTaskDetailEmployerAddress(id As Long)
        Dim objDelVWAddress As NawaDevDAL.Vw_Employer_Addresses = objListgoAML_vw_Employer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objxDelAddress As NawaDevDAL.goAML_Ref_Address = objListgoAML_Ref_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = id)

        If Not objxDelAddress Is Nothing Then
            objListgoAML_Ref_Employer_Address.Remove(objxDelAddress)
        End If

        If Not objDelVWAddress Is Nothing Then
            objListgoAML_vw_Employer_Addresses.Remove(objDelVWAddress)
            Store_Employer_Address.DataSource = objListgoAML_vw_Employer_Addresses.ToList()
            Store_Employer_Address.DataBind()
        End If

        FP_Address_Emp_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerEmpAddress()

    End Sub

    Sub DeleteRecordTaskDetailDirectorEmployerAddress(id As Long)
        Dim objDelVWAddress As NawaDevDAL.Vw_Director_Employer_Addresses = objListgoAML_vw_Director_Employer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objxDelAddress As NawaDevDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = id)

        If Not objxDelAddress Is Nothing Then
            objListgoAML_Ref_Director_Employer_Address.Remove(objxDelAddress)
        End If

        If Not objDelVWAddress Is Nothing Then
            objListgoAML_vw_Director_Employer_Addresses.Remove(objDelVWAddress)
            Store_Director_Employer_Address.DataSource = objListgoAML_vw_Director_Employer_Addresses.ToList()
            Store_Director_Employer_Address.DataBind()
        End If

        FP_Address_Emp_INDVD.Hidden = True
        WindowDetailAddress.Hidden = True
        ClearinputCustomerEmpAddress()

    End Sub

    Sub LoadDataEmployerAddressDetail(id As Long)

        Dim objCustomerAddressDetail = objListgoAML_Ref_Employer_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        If Not objCustomerAddressDetail Is Nothing Then
            FP_Address_Emp_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            btn_saveempaddress.Hidden = True
            WindowDetailAddress.Title = "Address Detail"
            cmb_emp_kategoriaddress.IsReadOnly = True
            emp_Address_INDV.ReadOnly = True
            emp_Town_INDV.ReadOnly = True
            emp_City_INDV.ReadOnly = True
            emp_Zip_INDV.ReadOnly = True
            cmb_emp_kodenegara.IsReadOnly = True
            emp_State_INDVD.ReadOnly = True
            emp_Comment_Address_INDVD.ReadOnly = True
            ClearinputCustomerEmpAddress()
            'Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Store_emp_KategoriAddress.DataBind()
            Dim kategorikontak_emp_detail = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
            If kategorikontak_emp_detail IsNot Nothing Then
                cmb_emp_kategoriaddress.SetTextWithTextValue(kategorikontak_emp_detail.Kode, kategorikontak_emp_detail.Keterangan)
            End If

            emp_Address_INDV.Text = objCustomerAddressDetail.Address
            emp_Town_INDV.Text = objCustomerAddressDetail.Town
            emp_City_INDV.Text = objCustomerAddressDetail.City
            emp_Zip_INDV.Text = objCustomerAddressDetail.Zip
            'Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Store_emp_Kode_Negara.DataBind()
            Dim kodenegara_emp_detail = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerAddressDetail.Country_Code)
            If kodenegara_emp_detail IsNot Nothing Then
                cmb_emp_kodenegara.SetTextWithTextValue(kodenegara_emp_detail.Kode, kodenegara_emp_detail.Keterangan)
            End If

            emp_State_INDVD.Text = objCustomerAddressDetail.State
            emp_Comment_Address_INDVD.Text = objCustomerAddressDetail.Comments
            '' Add 09-Jan-2023, Felix. Tambah logic pas load kalau Countrynya ID, Statenya jd Mandatory jg
            If cmb_emp_kodenegara.SelectedItemValue = "ID" Then
                emp_State_INDVD.AllowBlank = False
                emp_State_INDVD.FieldStyle = "background-color: #FFE4C4;"
            Else
                emp_State_INDVD.AllowBlank = True
                emp_State_INDVD.FieldStyle = "background-color: #FFFFFF;"
            End If
            '' End 09-Jan-2023
        End If
    End Sub

    Sub LoadDataDirectorEmployerAddressDetail(id As Long)

        Dim objCustomerAddressDetail = objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault()
        If Not objCustomerAddressDetail Is Nothing Then
            FP_Address_Emp_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            btn_Director_saveempaddress.Hidden = True
            WindowDetailDirectorAddress.Title = "Address Detail"
            Director_cmb_emp_kategoriaddress.IsReadOnly = True
            Director_emp_Address.ReadOnly = True
            Director_emp_Town.ReadOnly = True
            Director_emp_City.ReadOnly = True
            Director_emp_Zip.ReadOnly = True
            Director_cmb_emp_kodenegara.IsReadOnly = True
            Director_emp_State.ReadOnly = True
            Director_emp_Comment_Address.ReadOnly = True
            ClearinputDirectorEmpAddress()
            'Director_Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
            'Director_Store_emp_KategoriAddress.DataBind()
            Dim kategorikontak_director_emp_detail = goAML_CustomerBLL.GetContactTypeByID(objCustomerAddressDetail.Address_Type)
            If kategorikontak_director_emp_detail IsNot Nothing Then
                Director_cmb_emp_kategoriaddress.SetTextWithTextValue(kategorikontak_director_emp_detail.Kode, kategorikontak_director_emp_detail.Keterangan)
            End If

            Director_emp_Address.Text = objCustomerAddressDetail.Address
            Director_emp_Town.Text = objCustomerAddressDetail.Town
            Director_emp_City.Text = objCustomerAddressDetail.City
            Director_emp_Zip.Text = objCustomerAddressDetail.Zip
            'Director_Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
            'Director_Store_emp_Kode_Negara.DataBind()
            Dim kodenegara_director_emp_detail = goAML_CustomerBLL.GetNamaNegaraByID(objCustomerAddressDetail.Country_Code)
            If kodenegara_director_emp_detail IsNot Nothing Then
                Director_cmb_emp_kodenegara.SetTextWithTextValue(kodenegara_director_emp_detail.Kode, kodenegara_director_emp_detail.Keterangan)
            End If

            Director_emp_State.Text = objCustomerAddressDetail.State
            Director_emp_Comment_Address.Text = objCustomerAddressDetail.Comments

            '' Add 09-Jan-2023, Felix. Tambah logic pas load kalau Countrynya ID, Statenya jd Mandatory jg
            If Director_cmb_emp_kodenegara.SelectedItemValue = "ID" Then
                Director_emp_State.AllowBlank = False
                Director_emp_State.FieldStyle = "background-color: #FFE4C4;"
            Else
                Director_emp_State.AllowBlank = True
                Director_emp_State.FieldStyle = "background-color: #FFFFFF;"
            End If
            '' End 09-Jan-2023
        End If
    End Sub

    Sub LoadDataEditDirectorEmployerAddress(id As Long)
        objTempgoAML_Ref_Director_EmployerAddresses = objListgoAML_Ref_Director_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        objTempDirectorEmployerAddressEdit = objListgoAML_vw_Director_Employer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)

        Dim Address_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Country As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing

        If Not objTempDirectorEmployerAddressEdit Is Nothing Then
            FP_Address_Emp_Director.Hidden = False
            WindowDetailDirectorAddress.Hidden = False
            btn_Director_saveempaddress.Hidden = False
            Director_cmb_emp_kategoriaddress.IsReadOnly = False
            Director_emp_Address.ReadOnly = False
            Director_emp_Town.ReadOnly = False
            Director_emp_City.ReadOnly = False
            Director_emp_Zip.ReadOnly = False
            Director_cmb_emp_kodenegara.IsReadOnly = False
            Director_emp_State.ReadOnly = False
            Director_emp_Comment_Address.ReadOnly = False

            WindowDetailDirectorAddress.Title = "Address Edit"
            ClearinputDirectorEmpAddress()
            With objTempDirectorEmployerAddressEdit
                'Director_Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_emp_KategoriAddress.DataBind()
                Dim kategorikontak_emp_director = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                If kategorikontak_emp_director IsNot Nothing Then
                    Director_cmb_emp_kategoriaddress.SetTextWithTextValue(kategorikontak_emp_director.Kode, kategorikontak_emp_director.Keterangan)
                End If
                Director_emp_Address.Text = .Address
                Director_emp_Town.Text = .Town
                Director_emp_City.Text = .City
                Director_emp_Zip.Text = .Zip
                'Director_Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Director_Store_emp_Kode_Negara.DataBind()
                Dim kodenegara_emp_director = goAML_CustomerBLL.GetNamaNegaraByID(.Country_Code)
                If kodenegara_emp_director IsNot Nothing Then
                    Director_cmb_emp_kodenegara.SetTextWithTextValue(kodenegara_emp_director.Kode, kodenegara_emp_director.Keterangan)
                End If

                Director_emp_State.Text = .State
                Director_emp_Comment_Address.Text = .Comments
            End With

            '' Add 09-Jan-2023, Felix. Tambah logic pas load kalau Countrynya ID, Statenya jd Mandatory jg
            If Director_cmb_emp_kodenegara.SelectedItemValue = "ID" Then
                Director_emp_State.AllowBlank = False
                Director_emp_State.FieldStyle = "background-color: #FFE4C4;"
            Else
                Director_emp_State.AllowBlank = True
                Director_emp_State.FieldStyle = "background-color: #FFFFFF;"
            End If
            '' End 09-Jan-2023
            'EnvironmentForm("Detail")
        End If
    End Sub

    Sub LoadDataEditEmployerAddress(id As Long)
        objTempgoAML_Ref_EmployerAddresses = objListgoAML_Ref_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = id)
        objTempEmployerAddressEdit = objListgoAML_vw_Employer_Addresses.Find(Function(x) x.PK_Customer_Address_ID = id)

        Dim Address_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Country As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing

        If Not objTempEmployerAddressEdit Is Nothing Then
            FP_Address_Emp_INDVD.Hidden = False
            WindowDetailAddress.Hidden = False
            btn_saveempaddress.Hidden = False
            cmb_emp_kategoriaddress.IsReadOnly = False
            emp_Address_INDV.ReadOnly = False
            emp_Town_INDV.ReadOnly = False
            emp_City_INDV.ReadOnly = False
            emp_Zip_INDV.ReadOnly = False
            cmb_emp_kodenegara.IsReadOnly = False
            emp_State_INDVD.ReadOnly = False
            emp_Comment_Address_INDVD.ReadOnly = False

            WindowDetailAddress.Title = "Address Edit"
            ClearinputCustomerEmpAddress()
            With objTempEmployerAddressEdit
                'Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_emp_KategoriAddress.DataBind()
                Dim kategorikontak_emp_edit = goAML_CustomerBLL.GetContactTypeByID(.Address_Type)
                If kategorikontak_emp_edit IsNot Nothing Then
                    cmb_emp_kategoriaddress.SetTextWithTextValue(kategorikontak_emp_edit.Kode, kategorikontak_emp_edit.Keterangan)
                End If
                emp_Address_INDV.Text = .Address
                emp_Town_INDV.Text = .Town
                emp_City_INDV.Text = .City
                emp_Zip_INDV.Text = .Zip
                'Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Store_emp_Kode_Negara.DataBind()
                Dim kodenegara_emp_edit = goAML_CustomerBLL.GetNamaNegaraByID(.Country_Code)
                If kodenegara_emp_edit IsNot Nothing Then
                    cmb_emp_kodenegara.SetTextWithTextValue(kodenegara_emp_edit.Kode, kodenegara_emp_edit.Keterangan)
                End If

                emp_State_INDVD.Text = .State
                emp_Comment_Address_INDVD.Text = .Comments
            End With
            '' Add 09-Jan-2023, Felix. Tambah logic pas load kalau Countrynya ID, Statenya jd Mandatory jg
            If cmb_emp_kodenegara.SelectedItemValue = "ID" Then
                emp_State_INDVD.AllowBlank = False
                emp_State_INDVD.FieldStyle = "background-color: #FFE4C4;"
            Else
                emp_State_INDVD.AllowBlank = True
                emp_State_INDVD.FieldStyle = "background-color: #FFFFFF;"
            End If
            '' End 09-Jan-2023
            'EnvironmentForm("Detail")
        End If
    End Sub

    Protected Sub btnAddEmployerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            If objListgoAML_vw_Employer_Phones.Count = 0 Then
                FormPanelEmpTaskDetail.Hidden = False
                WindowDetail.Hidden = False
                btn_saveempphone.Hidden = False
                Emp_cb_phone_Contact_Type.IsReadOnly = False
                Emp_cb_phone_Communication_Type.IsReadOnly = False
                Emp_txt_phone_Country_prefix.ReadOnly = False
                Emp_txt_phone_number.ReadOnly = False
                Emp_txt_phone_extension.ReadOnly = False
                Emp_txt_phone_comments.ReadOnly = False
                btn_saveempphone.Hidden = False
                WindowDetail.Title = "Employer Phone Add"

                Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
                Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

                'Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Emp_Store_Contact_Type.DataBind()

                'Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Emp_Store_Communication_Type.DataBind()
                ClearinputCustomerEmpPhones()
            Else
                Throw New Exception("can only enter one Workplace Phone Information")
            End If
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddDirectorEmployerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim objdirectorempphone = objListgoAML_vw_Director_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = tempflag).ToList()
            If objdirectorempphone.Count = 0 Then
                FormPanelEmpDirectorTaskDetail.Hidden = False
                WindowDetailDirectorPhone.Hidden = False
                btn_saveDirector_empphone.Hidden = False
                Director_Emp_cb_phone_Contact_Type.IsReadOnly = False
                Director_Emp_cb_phone_Communication_Type.IsReadOnly = False
                Director_Emp_txt_phone_Country_prefix.ReadOnly = False
                Director_Emp_txt_phone_number.ReadOnly = False
                Director_Emp_txt_phone_extension.ReadOnly = False
                Director_Emp_txt_phone_comments.ReadOnly = False
                btn_saveDirector_empphone.Hidden = False
                WindowDetailDirectorPhone.Title = "Employer Phone Add"

                Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
                Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing
                'Director_Emp_Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Emp_Store_Contact_Type.DataBind()

                'Director_Emp_Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                'Director_Emp_Store_Communication_Type.DataBind()
                ClearinputDirectorEmpPhones()
            End If
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddEmployerAddresses_DirectClick(sender As Object, e As DirectEventArgs)

        Try
            If objListgoAML_vw_Employer_Addresses.Count = 0 Then
                FP_Address_Emp_INDVD.Hidden = False
                WindowDetailAddress.Hidden = False
                cmb_emp_kategoriaddress.IsReadOnly = False
                emp_Address_INDV.ReadOnly = False
                emp_Town_INDV.ReadOnly = False
                emp_City_INDV.ReadOnly = False
                emp_Zip_INDV.ReadOnly = False
                cmb_emp_kodenegara.IsReadOnly = False
                emp_State_INDVD.ReadOnly = False
                emp_Comment_Address_INDVD.ReadOnly = False
                btn_saveempaddress.Hidden = False
                WindowDetailAddress.Title = "Employer Address Add"

                'Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Store_emp_KategoriAddress.DataBind()

                'Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Store_emp_Kode_Negara.DataBind()
                ClearinputCustomerEmpAddress()
            Else
                Throw New Exception("can only enter one Workplace Address Information")
            End If
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddDirectorEmployerAddresses_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim objdirectorempaddress = objListgoAML_vw_Director_Employer_Addresses.Where(Function(x) x.FK_To_Table_ID = tempflag).ToList
            If objdirectorempaddress.Count = 0 Then
                FP_Address_Emp_Director.Hidden = False
                WindowDetailDirectorAddress.Hidden = False
                Director_cmb_emp_kategoriaddress.IsReadOnly = False
                Director_emp_Address.ReadOnly = False
                Director_emp_Town.ReadOnly = False
                Director_emp_City.ReadOnly = False
                Director_emp_Zip.ReadOnly = False
                Director_cmb_emp_kodenegara.IsReadOnly = False
                Director_emp_State.ReadOnly = False
                Director_emp_Comment_Address.ReadOnly = False
                btn_Director_saveempaddress.Hidden = False
                WindowDetailDirectorAddress.Title = "Employer Address Add"


                'Director_Store_emp_KategoriAddress.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                'Director_Store_emp_KategoriAddress.DataBind()

                'Director_Store_emp_Kode_Negara.DataSource = goAML_CustomerBLL.GetListNamaNegara()
                'Director_Store_emp_Kode_Negara.DataBind()
                ClearinputDirectorEmpAddress()

            End If
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#Region "Add 09-Jan-2023 Felix tambah validasi kalau Country ID, State Wajib diisi"
    Protected Sub cmb_kodenegara_OnValueChanged(sender As Object, e As EventArgs)
        Try

            If cmb_kodenegara.SelectedItemValue = "ID" Then
                State_INDVD.AllowBlank = False
                State_INDVD.FieldStyle = "background-color: #FFE4C4;"
            Else
                State_INDVD.AllowBlank = True
                State_INDVD.FieldStyle = "background-color: #FFFFFF;"
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub cmb_emp_kodenegara_OnValueChanged(sender As Object, e As EventArgs)
        Try

            If cmb_emp_kodenegara.SelectedItemValue = "ID" Then
                emp_State_INDVD.AllowBlank = False
                emp_State_INDVD.FieldStyle = "background-color: #FFE4C4;"
            Else
                emp_State_INDVD.AllowBlank = True
                emp_State_INDVD.FieldStyle = "background-color: #FFFFFF;"
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Director_cmb_kodenegara_OnValueChanged(sender As Object, e As EventArgs)
        Try

            If Director_cmb_kodenegara.SelectedItemValue = "ID" Then
                Director_State.AllowBlank = False
                Director_State.FieldStyle = "background-color: #FFE4C4;"
            Else
                Director_State.AllowBlank = True
                Director_State.FieldStyle = "background-color: #FFFFFF;"
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Director_cmb_emp_kodenegara_OnValueChanged(sender As Object, e As EventArgs)
        Try

            If Director_cmb_emp_kodenegara.SelectedItemValue = "ID" Then
                Director_emp_State.AllowBlank = False
                Director_emp_State.FieldStyle = "background-color: #FFE4C4;"
            Else
                Director_emp_State.AllowBlank = True
                Director_emp_State.FieldStyle = "background-color: #FFFFFF;"
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class


