﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CUSTOMER_ADD.aspx.vb" Inherits="CUSTOMER_ADD" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailDirector" runat="server" Modal="true" Icon="ApplicationViewDetail" Title="Customer Director Add" Hidden="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet3" runat="server" Title="Director" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>

                <Content>
                    <ext:FormPanel ID="FP_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Hidden runat="server" ID="Hidden2"></ext:Hidden>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_peran" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_peran" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Peran_orang_dalam_Korporasi" StringFilter="Active = 1" EmptyText="Select One" Label="Peran" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cmb_peran" runat="server" AllowBlank="false" FieldLabel="Peran" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" LabelWidth="250" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="StorePeran">
                                        <Model>
                                            <ext:Model runat="server" ID="Model7">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                </DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="txt_Director_Title" LabelWidth="250" runat="server" FieldLabel="Gelar" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Last_Name" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Nama Lengkap" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_Director_Gender" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Director_Gender" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Jenis_Kelamin" StringFilter="Active = 1" EmptyText="Select One" Label="Gender" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox LabelWidth="250" ID="cmb_Director_Gender" runat="server" FieldLabel="Gender" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" AllowBlank="false" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ID="Store_Director_Gender" OnReadData="Gender_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model9">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                            --%>        <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:DateField Editable="false" LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Tanggal Lahir" ID="txt_Director_BirthDate" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_Director_Birth_Place" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Mothers_Name" LabelWidth="250" runat="server" FieldLabel="Nama Ibu Kandung" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Alias" LabelWidth="250" runat="server" FieldLabel="Nama Alias" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_SSN" LabelWidth="250" runat="server" FieldLabel="NIK" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Passport_Number" LabelWidth="250" runat="server" FieldLabel="No. Passport" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown--%>
                            <ext:Panel ID="Pnl_cmb_Director_Passport_Country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Director_Passport_Country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Negara Penerbit Passport" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <%--end Update--%>
                            <ext:TextField ID="txt_Director_ID_Number" LabelWidth="250" runat="server" FieldLabel="No. Identitas Lain" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_Director_Nationality1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Director_Nationality1" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Kewarganegaraan 1" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_cmb_Director_Nationality2" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Director_Nationality2" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Kewarganegaraan 2" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_cmb_Director_Nationality3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Director_Nationality3" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Kewarganegaraan 3" AnchorHorizontal="70%" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_cmb_Director_Residence" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Director_Residence" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Negara Domisili" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox LabelWidth="250" AllowBlank="False" ID="cmb_Director_Nationality1" runat="server" FieldLabel="Kewarganegaraan 1"
                                DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ID="Store_Director_Nationality1" OnReadData="National_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model10">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>
                            <%--<ext:ComboBox ID="cmb_Director_Nationality2" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 2" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Director_Nationality2" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model11">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>
                            <ext:ComboBox ID="cmb_Director_Nationality3" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 3" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Director_Nationality3" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model12">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>
                            <%--<ext:ComboBox ID="cmb_Director_Residence" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Negara Domisili" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="80%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Director_Residence" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model13">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%-- </DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="txt_Director_Email" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Email2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Email3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Email4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_Director_Email5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%"></ext:TextField>
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Director_Deceased" FieldLabel="Deceased ?">
                                <DirectEvents>
                                    <Change OnEvent="checkBoxcb_Director_Deceased_click" />
                                </DirectEvents>
                            </ext:Checkbox>
                            <ext:DateField Editable="false" runat="server" LabelWidth="250" Hidden="true" FieldLabel="Deceased Date " Format="dd-MMM-yyyy" ID="txt_Director_Deceased_Date" AnchorHorizontal="90%" />
                            <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Director_Tax_Reg_Number" FieldLabel="PEP ?">
                            </ext:Checkbox>
                            <ext:TextField runat="server" LabelWidth="250" FieldLabel="NPWP" ID="txt_Director_Tax_Number" AnchorHorizontal="90%" />
                            <ext:TextField runat="server" LabelWidth="250" AllowBlank="false" FieldLabel="Source of Wealth" ID="txt_Director_Source_of_Wealth" AnchorHorizontal="90%" />
                            <ext:TextField ID="txt_Director_Occupation" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%"></ext:TextField>

                            <ext:TextField runat="server" LabelWidth="250" FieldLabel="Catatan" ID="txt_Director_Comments" AnchorHorizontal="90%" />
                            <ext:TextField ID="txt_Director_employer_name" LabelWidth="250" runat="server" FieldLabel="Tempat Bekerja" AnchorHorizontal="90%"></ext:TextField>
                            <%--Director Identificiation--%>
                            <ext:GridPanel ID="GP_DirectorIdentification" runat="server" Title="Identification" AutoScroll="true">
                                <TopBar>
                                    <ext:Toolbar ID="toolbar12" runat="server">
                                        <Items>
                                            <ext:Button ID="btnaddidentificationdirector" runat="server" Text="Add Identification" Icon="Add">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAddDirectorIdentifications_DirectClick">
                                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="Store_Director_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model29" runat="server">
                                                <%--to do--%>
                                                <Fields>
                                                    <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Type_Document" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Issued_By" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Issue_Date" Type="Date"></ext:ModelField>
                                                    <ext:ModelField Name="Expiry_Date" Type="Date"></ext:ModelField>
                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Identification_Comment" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                        <ext:Column ID="Column75" runat="server" DataIndex="Number" Text="Nomor" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column77" runat="server" DataIndex="Type_Document" Text="Jenis Dokumen" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column78" runat="server" DataIndex="Issued_By" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:DateColumn ID="Column79" runat="server" DataIndex="Issue_Date" Text="Issued Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                        <ext:DateColumn ID="Column80" runat="server" DataIndex="Expiry_Date" Text="Expired Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                        <ext:Column ID="Column81" runat="server" DataIndex="Country" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column82" runat="server" DataIndex="Identification_Comment" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn13" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdDirectorIdentification">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--end of Director Identification--%>


                            <ext:GridPanel ID="GP_DirectorPhone" runat="server" Title="Informasi Telepon" AutoScroll="true">
                                <TopBar>
                                    <ext:Toolbar ID="toolbar10" runat="server">
                                        <Items>
                                            <ext:Button ID="btnsavedirectorphone" runat="server" Text="Add Phones" Icon="Add">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAddDirectorPhones_DirectClick">
                                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="Store_DirectorPhone" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model6" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                        <ext:Column ID="Column61" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column62" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column63" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column64" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column65" runat="server" DataIndex="tph_extension" Text="Nomor Extensi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column66" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn11" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdDirectorPhone">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>

                            <%--Corp Address--%>
                            <ext:GridPanel ID="GP_DirectorAddress" runat="server" Title="Informasi Address" AutoScroll="true">
                                <TopBar>
                                    <ext:Toolbar ID="toolbar11" runat="server">
                                        <Items>
                                            <ext:Button ID="btnsavedirectorAddress" runat="server" Text="Add Addresses" Icon="Add">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAddDirectorAddresses_DirectClick">
                                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="Store_DirectorAddress" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model16" runat="server">
                                                <%--to do--%>
                                                <Fields>
                                                    <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                        <ext:Column ID="Column67" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column68" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column69" runat="server" DataIndex="Town" Text="Kecamatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column70" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column71" runat="server" DataIndex="Zip" Text="Kode Pos" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column72" runat="server" DataIndex="Country" Text="Kode Negara" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column73" runat="server" DataIndex="State" Text="Provinsi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column74" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn12" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdDirectorAddress">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--end of Corp Address--%>
                            <ext:GridPanel ID="GP_DirectorEmpPhone" runat="server" Title="Informasi Telepon Tempat Kerja" AutoScroll="true">
                                <TopBar>
                                    <ext:Toolbar ID="toolbar8" runat="server">
                                        <Items>
                                            <ext:Button ID="btnsaveEmpdirectorPhone" runat="server" Text="Add Phones" Icon="Add">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAddDirectorEmployerPhones_DirectClick">
                                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="Store_Director_Employer_Phone" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model14" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                        <ext:Column ID="Column47" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column48" runat="server" DataIndex="Communcation_Type" Text="Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column49" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column50" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column51" runat="server" DataIndex="tph_extension" Text="Nomor Extensi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column52" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn9" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdDirectorEmployerPhone">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>

                            <%--Employer Address--%>
                            <ext:GridPanel ID="GP_DirectorEmpAddress" runat="server" Title="Informasi Address Tempat Kerja" AutoScroll="true">
                                <TopBar>
                                    <ext:Toolbar ID="toolbar9" runat="server">
                                        <Items>
                                            <ext:Button ID="btnsaveEmpdirectorAddress" runat="server" Text="Add Addresses" Icon="Add">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAddDirectorEmployerAddresses_DirectClick">
                                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="Store_Director_Employer_Address" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model15" runat="server">
                                                <%--to do--%>
                                                <Fields>
                                                    <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                        <ext:Column ID="Column53" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column54" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column55" runat="server" DataIndex="Town" Text="Kecamatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column56" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column57" runat="server" DataIndex="Zip" Text="Kode Pos" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column58" runat="server" DataIndex="Country" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column59" runat="server" DataIndex="State" Text="Provinsi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column60" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn10" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdDirectorEmployerAddress">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--end of Employer Address--%>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnsaveDirector" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomerDirector_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button9" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerDirector_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>


    <%--Pop Up--%>
    <ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Phone Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSetCustomerPhoneAdd" runat="server" Title="Phones" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanelTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Hidden runat="server" ID="fld_sender"></ext:Hidden>

                            <ext:Panel ID="Pnl_cb_phone_Contact_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cb_phone_Contact_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringFilter="Active = 1" EmptyText="Select One" StringTable="goAML_Ref_Kategori_Kontak" Label="Kategori Kontak" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_cb_phone_Communication_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cb_phone_Communication_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringFilter="Active = 1" EmptyText="Select One" StringTable="goAML_Ref_Jenis_Alat_Komunikasi" Label="Jenis Alat Komunikasi" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--<ext:ComboBox ID="cb_phone_Contact_Type" runat="server" AllowBlank="False" FieldLabel="Kategori Kontak" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Contact_Type" OnReadData="Contact_Type_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model_Contact_Type">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>
                            <%--<ext:ComboBox ID="cb_phone_Communication_Type" runat="server" AllowBlank="False" FieldLabel="Jenis Alat Komunikasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Communication_Type" OnReadData="Communication_Type_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model_Comminication_Type">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>

                            <ext:TextField ID="txt_phone_Country_prefix" runat="server" FieldLabel="Kode Area Telp" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_phone_number" runat="server" AllowBlank="False" FieldLabel="Nomor Telepon" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_phone_extension" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_phone_comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSavedetail" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomerPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBackSaveDetail" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>

                    <ext:FormPanel ID="FormPanelEmpTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Emp_cb_phone_Contact_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Emp_cb_phone_Contact_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Kategori_Kontak" StringFilter="Active = 1" EmptyText="Select One" Label="Kategori Kontak" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_Emp_cb_phone_Communication_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Emp_cb_phone_Communication_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Jenis_Alat_Komunikasi" StringFilter="Active = 1" EmptyText="Select One" Label="Jenis Alat Komunikasi" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="Emp_cb_phone_Contact_Type" AllowBlank="False" runat="server" FieldLabel="Kategori Kontak" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Emp_Store_Contact_Type" OnReadData="Contact_Type_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model17">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                            --%>        <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>
                            <%-- <ext:ComboBox ID="Emp_cb_phone_Communication_Type" AllowBlank="False" runat="server" FieldLabel="Jenis Alat Komunikasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Emp_Store_Communication_Type" OnReadData="Communication_Type_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model18">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>

                            <ext:TextField ID="Emp_txt_phone_Country_prefix" runat="server" FieldLabel="Kode Area Telp" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Emp_txt_phone_number" AllowBlank="False" runat="server" FieldLabel="Nomor Telepon" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Emp_txt_phone_extension" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Emp_txt_phone_comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_saveempphone" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomerEmpPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button14" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailAddress" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Address Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet1" runat="server" Title="Address" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FP_Address_INDVD" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Hidden runat="server" ID="Hidden1"></ext:Hidden>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_kategoriaddress" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_kategoriaddress" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" EmptyText="Select One" StringFilter="Active = 1" StringTable="goAML_Ref_Kategori_Kontak" Label="Kategori Alamat" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cmb_kategoriaddress" AllowBlank="False" runat="server" FieldLabel="Kategori Alamat" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_KategoriAddress" OnReadData="Contact_Type_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="ModelKategoriAddress">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--  </DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="Address_INDV" runat="server" AllowBlank="False" FieldLabel="Alamat" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Town_INDV" runat="server" FieldLabel="Kecamatan" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="City_INDV" runat="server" AllowBlank="False" FieldLabel="Kota" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Zip_INDV" runat="server" FieldLabel="Kode Pos" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_kodenegara" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_kodenegara" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" EmptyText="Select One" StringFilter="Active = 1" StringTable="goAML_Ref_Nama_Negara" Label="Kode Negara" AnchorHorizontal="70%" AllowBlank="false" OnOnValueChanged="cmb_kodenegara_OnValueChanged"/>
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cmb_kodenegara" runat="server" AllowBlank="False" FieldLabel="Kode Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ID="Store_Kode_Negara" OnReadData="National_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="ModelKodeNegara">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="State_INDVD" runat="server" FieldLabel="Provinsi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Comment_Address_INDVD" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Btn_Save_Address" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomerAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Btn_Back_Address" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                    <ext:FormPanel ID="FP_Address_Emp_INDVD" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:Hidden runat="server" ID="Hidden5"></ext:Hidden>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_emp_kategoriaddress" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_emp_kategoriaddress" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Kategori_Kontak" StringFilter="Active = 1" EmptyText="Select One" Label="Kategori Alamat" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>

                            <%--<ext:ComboBox ID="cmb_emp_kategoriaddress" runat="server" AllowBlank="False" FieldLabel="Kategori Alamat" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ID="Store_emp_KategoriAddress" OnReadData="Contact_Type_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model19">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%-- </DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="emp_Address_INDV" runat="server" AllowBlank="False" FieldLabel="Alamat" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="emp_Town_INDV" runat="server" FieldLabel="Kecamatan" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="emp_City_INDV" runat="server" AllowBlank="False" FieldLabel="Kota" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="emp_Zip_INDV" runat="server" FieldLabel="Kode Pos" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_emp_kodenegara" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_emp_kodenegara" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Kode Negara" AnchorHorizontal="70%" AllowBlank="false" OnOnValueChanged="cmb_emp_kodenegara_OnValueChanged"/>
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%-- <ext:ComboBox ID="cmb_emp_kodenegara" AllowBlank="False" runat="server" FieldLabel="Kode Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_emp_Kode_Negara" OnReadData="National_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model20">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>--%>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%-- </DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="emp_State_INDVD" runat="server" FieldLabel="Provinsi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="emp_Comment_Address_INDVD" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_saveempaddress" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomerEmpAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button16" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailIdentification" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Identification Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet2" runat="server" Title="Identification" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FP_Identification_INDV" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_number_identification" AllowBlank="False" runat="server" FieldLabel="Nomor" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_Jenis_Dokumen" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Jenis_Dokumen" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringFilter="Active = 1" StringTable="goaml_ref_jenis_dokumen_identitas" EmptyText="Select One" Label="Jenis Dokumen Identitas" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_cmb_issued_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_issued_country" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Issued Country" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cmb_Jenis_Dokumen" runat="server" AllowBlank="False" FieldLabel="Jenis Dokumen Identitas" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ID="Store_jenis_dokumen" OnReadData="Identification_Type_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model3">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                </DirectEvents>
                            </ext:ComboBox>--%>
                            <%--<ext:ComboBox ID="cmb_issued_country" AllowBlank="False" runat="server" FieldLabel="Issued Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_issued_country" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model4">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                            --%>        <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="txt_issued_by" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%"></ext:TextField>
                            <ext:DateField Editable="false" ID="txt_issued_date" Format="dd-MMM-yyyy" runat="server" FieldLabel="Issued Date" AnchorHorizontal="90%"></ext:DateField>
                            <ext:DateField Editable="false" ID="txt_issued_expired_date" Format="dd-MMM-yyyy" runat="server" FieldLabel="Issued Expired Date" AnchorHorizontal="90%"></ext:DateField>
                            <ext:TextField ID="txt_identification_comment" runat="server" FieldLabel="Comment" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_save_identification" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCustomerIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button5" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <ext:FormPanel ID="FP_Identification_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <ext:TextField ID="txt_number_identification_Director" AllowBlank="False" runat="server" FieldLabel="Nomor" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_Jenis_Dokumen_Director" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Jenis_Dokumen_Director" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goaml_ref_jenis_dokumen_identitas" StringFilter="Active = 1" EmptyText="Select One" Label="Jenis Dokumen Identitas" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_cmb_issued_country_Director" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_issued_country_Director" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Issued Country" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cmb_Jenis_Dokumen_Director" runat="server" AllowBlank="False" FieldLabel="Jenis Dokumen Identitas" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Storejenisdokumen_Director" OnReadData="Identification_Type_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model30">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                </DirectEvents>
                            </ext:ComboBox>--%>
                            <%--<ext:ComboBox ID="cmb_issued_country_Director" AllowBlank="False" runat="server" FieldLabel="Issued Country" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreIssueCountry_Director" OnReadData="National_readData" IsPagingStore="true" AutoLoad="false">
                                        <Model>
                                            <ext:Model runat="server" ID="Model31">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                            --%>        <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%--</DirectEvents>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="txt_issued_by_Director" runat="server" FieldLabel="Issued By" AnchorHorizontal="90%"></ext:TextField>
                            <ext:DateField Editable="false" ID="txt_issued_date_Director" runat="server" FieldLabel="Issued Date" AnchorHorizontal="90%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:DateField Editable="false" ID="txt_issued_expired_date_Director" runat="server" FieldLabel="Issued Expired Date" AnchorHorizontal="90%" Format="dd-MMM-yyyy"></ext:DateField>
                            <ext:TextField ID="txt_identification_comment_Director" runat="server" FieldLabel="Comment" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_save_identification_Director" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveDirectorIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button8" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorIdentification_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailDirectorPhone" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Director Phone Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSetDirectorPhoneAdd" runat="server" Title="Phones" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanelDirectorPhone" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Director_cb_phone_Contact_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_cb_phone_Contact_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Kategori_Kontak" StringFilter="Active = 1" EmptyText="Select One" Label="Kategori Kontak" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_Director_cb_phone_Communication_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_cb_phone_Communication_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Jenis_Alat_Komunikasi" StringFilter="Active = 1" EmptyText="Select One" Label="Jenis Alat Komunikasi" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="Director_cb_phone_Contact_Type" AllowBlank="False" runat="server" FieldLabel="Kategori Kontak" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_Contact_Type" OnReadData="Contact_Type_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model21">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>
                            <%--<ext:ComboBox ID="Director_cb_phone_Communication_Type" AllowBlank="False" runat="server" FieldLabel="Jenis Alat Komunikasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_Communication_Type" OnReadData="Communication_Type_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model22">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>

                            <ext:TextField ID="Director_txt_phone_Country_prefix" runat="server" FieldLabel="Kode Area Telp" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_txt_phone_number" AllowBlank="False" runat="server" FieldLabel="Nomor Telepon" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_txt_phone_extension" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_txt_phone_comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSaveDirectorPhonedetail" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveDirectorPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBackSaveDirectorPhoneDetail" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                  
                    <ext:FormPanel ID="FormPanelEmpDirectorTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Director_Emp_cb_phone_Contact_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_Emp_cb_phone_Contact_Type" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Kategori_Kontak" StringFilter="Active = 1" EmptyText="Select One" Label="Kategori Kontak" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_Director_Emp_cb_phone_Communication_Type" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_Emp_cb_phone_Communication_Type" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Jenis_Alat_Komunikasi" StringFilter="Active = 1" EmptyText="Select One" Label="Jenis Alat Komunikasi" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="Director_Emp_cb_phone_Contact_Type" AllowBlank="False" runat="server" FieldLabel="Kategori Kontak" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Emp_Store_Contact_Type" OnReadData="Contact_Type_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model23">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>
                            <%--<ext:ComboBox ID="Director_Emp_cb_phone_Communication_Type" AllowBlank="False" runat="server" FieldLabel="Jenis Alat Komunikasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Emp_Store_Communication_Type" OnReadData="Communication_Type_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model24">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>

                            <ext:TextField ID="Director_Emp_txt_phone_Country_prefix" runat="server" FieldLabel="Kode Area Telp" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Emp_txt_phone_number" AllowBlank="False" runat="server" FieldLabel="Nomor Telepon" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Emp_txt_phone_extension" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Emp_txt_phone_comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_saveDirector_empphone" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveDirectorEmpPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button13" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailDirectorAddress" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Director Address Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet4" runat="server" Title="Address" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FP_Address_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Director_cmb_kategoriaddress" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_cmb_kategoriaddress" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Kategori_Kontak" StringFilter="Active = 1" EmptyText="Select One" Label="Kategori Alamat" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="Director_cmb_kategoriaddress" AllowBlank="False" runat="server" FieldLabel="Kategori Alamat" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_KategoriAddress" OnReadData="Contact_Type_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model25">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="Director_Address" AllowBlank="False" runat="server" FieldLabel="Alamat" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Town" runat="server" FieldLabel="Kecamatan" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_City" AllowBlank="False" runat="server" FieldLabel="Kota" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Zip" runat="server" FieldLabel="Kode Pos" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Director_cmb_kodenegara" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_cmb_kodenegara" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Kode Negara" AnchorHorizontal="70%" AllowBlank="false" OnOnValueChanged="Director_cmb_kodenegara_OnValueChanged"/>
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%-- <ext:ComboBox ID="Director_cmb_kodenegara" AllowBlank="False" runat="server" FieldLabel="Kode Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_Kode_Negara" OnReadData="National_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model26">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="Director_State" runat="server" FieldLabel="Provinsi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_Comment_Address" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Btn_Save_Director_Address" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveDirectorAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Btn_Back_Director_Address" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                    <ext:FormPanel ID="FP_Address_Emp_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                        <Items>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Director_cmb_emp_kategoriaddress" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_cmb_emp_kategoriaddress" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Kategori_Kontak" StringFilter="Active = 1" EmptyText="Select One" Label="Kategori Alamat" AnchorHorizontal="70%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="Director_cmb_emp_kategoriaddress" AllowBlank="False" runat="server" FieldLabel="Kategori Alamat" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_emp_KategoriAddress">
                                        <Model>
                                            <ext:Model runat="server" ID="Model27">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="Director_emp_Address" AllowBlank="False" runat="server" FieldLabel="Alamat" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_Town" runat="server" FieldLabel="Kecamatan" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_City" AllowBlank="False" runat="server" FieldLabel="Kota" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_Zip" runat="server" FieldLabel="Kode Pos" AnchorHorizontal="90%"></ext:TextField>
                            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_Director_cmb_emp_kodenegara" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="Director_cmb_emp_kodenegara" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Kode Negara" AnchorHorizontal="70%" AllowBlank="false" OnOnValueChanged="Director_cmb_emp_kodenegara_OnValueChanged"/>
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="Director_cmb_emp_kodenegara" AllowBlank="False" runat="server" FieldLabel="Kode Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Director_Store_emp_Kode_Negara" OnReadData="National_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="Model28">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>--%>
                            <ext:TextField ID="Director_emp_State" runat="server" FieldLabel="Provinsi" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="Director_emp_Comment_Address" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btn_Director_saveempaddress" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveDirectorEmpAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="Button15" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveDirectorAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <ext:FormPanel ID="FormPanelInput" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="Customer Add">
        <Items>
            <ext:Hidden runat="server" ID="PK_Customer" />
            <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
            <ext:Panel ID="Pnl_Cmb_TypePerson" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                <Content>
                    <nds:NDSDropDownField ID="Cmb_TypePerson" ValueField="Customer Type" DisplayField="Description" runat="server" StringField="[Customer Type],Description" StringTable="vw_goAML_Ref_Customer_Type" Label="Type Person" AnchorHorizontal="70%" AllowBlank="false" OnOnValueChanged="Cmb_TypePerson_DirectSelect" LabelWidth="250" />
                </Content>
            </ext:Panel>


            <%--End Update--%>
            <%--<ext:ComboBox ID="Cmb_TypePerson" LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Type Person" DisplayField="Description" ValueField="PK_Customer_Type_ID" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreTypePerson">
                        <Model>
                            <ext:Model runat="server" ID="ModelStatusCode">
                                <Fields>
                                    <ext:ModelField Name="PK_Customer_Type_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Description" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <DirectEvents>
                    <Change OnEvent="Cmb_TypePerson_DirectSelect">
                        <ExtraParams>
                            <ext:Parameter Name="PK_Customer_Type_ID" Value="PK_Customer_Type_ID" Mode="Raw" />
                        </ExtraParams>
                    </Change>
                </DirectEvents>
            </ext:ComboBox>--%>
            <ext:ComboBox ID="Cmb_StatusCode" LabelWidth="250" runat="server" AllowBlank="False" FieldLabel="Status Nasabah" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreStatusCode">
                        <Model>
                            <ext:Model runat="server" ID="Model1">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
            <ext:Checkbox runat="server" LabelWidth="250" ID="UpdateFromDataSource" FieldLabel="Update From Data Source ?">
            </ext:Checkbox>
            <ext:TextField ID="txt_cif" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="CIF" Hidden="true" AnchorHorizontal="90%"></ext:TextField>

            <%--Dedy Added 24082020--%>
            <ext:TextField ID="txt_gcn" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="GCN" Hidden="true" AnchorHorizontal="90%">
                <%--Update: Zikri_14092020 Add Validation GCN--%>
                <Listeners>
                    <Change Handler="#{GCNPrimary}.setValue(false)" />
                </Listeners>
                <%--End Update--%>
            </ext:TextField>
            <ext:Checkbox runat="server" LabelWidth="250" ID="GCNPrimary" FieldLabel="GCN Primary ?">
                <%--Update: Zikri_14092020 Add Validation GCN--%>
                <DirectEvents>
                    <Change OnEvent="CheckGCN_DirectEvent"></Change>
                </DirectEvents>
                <%--End Update--%>
            </ext:Checkbox>
            <%--Dedy End Added 24082020--%>
             <ext:DateField ID="TextFieldOpeningDate" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Opening Date"  AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>
            <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                  <content>
                        <nds:NDSDropDownField ID="cmb_openingBranch" ValueField="FK_AML_BRANCH_CODE" DisplayField="BRANCH_NAME" runat="server" StringField="FK_AML_BRANCH_CODE,BRANCH_NAME"  StringTable="AML_BRANCH" Label="Opening Branch Code" AnchorHorizontal="70%" AllowBlank="true" LabelWidth="250" />
                        <nds:NDSDropDownField ID="cmb_Status" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan"  StringTable="goAML_Ref_Status_Rekening" Label="Status" AnchorHorizontal="70%" AllowBlank="True" LabelWidth="250" />
             </content>
            </ext:Panel> 
            <ext:DateField ID="TextFieldClosingDate" AllowBlank="True" LabelWidth="250" runat="server" FieldLabel="Closing Date" AnchorHorizontal="70%"  Format="dd-MMM-yyyy"></ext:DateField>



            <ext:Panel runat="server" Title="Data Individu" Hidden="true" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="panel_INDV" Border="true" BodyStyle="padding:10px">
                <Items>

                    <ext:TextField ID="txt_INDV_Title" LabelWidth="250" runat="server" FieldLabel="Gelar" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Last_Name" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Nama Lengkap" AnchorHorizontal="90%"></ext:TextField>
                    <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                    <ext:Panel ID="Pnl_cmb_INDV_Gender" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_INDV_Gender" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" StringFilter="Active = 1" StringTable="goAML_Ref_Jenis_Kelamin" Label="Gender" AnchorHorizontal="70%" AllowBlank="false" LabelWidth="250" />
                        </Content>
                    </ext:Panel>
                    <%--End Update--%>
                    <%--<ext:ComboBox LabelWidth="250" ID="cmb_INDV_Gender" runat="server" FieldLabel="Gender" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" AllowBlank="false" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ID="StoreINDVGender" OnReadData="Gender_readData">
                                <Model>
                                    <ext:Model runat="server" ID="Model2">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%-- </DirectEvents>
                    </ext:ComboBox>--%>
                    <ext:DateField Editable="false" LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Tanggal Lahir" ID="txt_INDV_Birthdate" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                    <ext:TextField ID="txt_INDV_Birth_place" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Mothers_name" LabelWidth="250" runat="server" FieldLabel="Nama Ibu Kandung" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Alias" LabelWidth="250" runat="server" FieldLabel="Nama Alias" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_SSN" LabelWidth="250" runat="server" FieldLabel="NIK" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Passport_number" LabelWidth="250" runat="server" FieldLabel="No. Passport" AnchorHorizontal="90%"></ext:TextField>
                    <%--Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown--%>
                    <ext:Panel ID="Pnl_cmb_INDV_Passport_country" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_INDV_Passport_country" ValueField="kode" DisplayField="Nama Negara" runat="server" StringField="kode,keterangan" StringFilter="Active = 1" StringTable="goAML_Ref_Nama_Negara" Label="Negara Penerbit Passport" AnchorHorizontal="70%" LabelWidth="250" />
                        </Content>
                    </ext:Panel>
                    <%--End Update--%>

                    <%--Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown--%>
                    <%--<ext:ComboBox LabelWidth="250" ID="cmb_INDV_Passport_country" runat="server" FieldLabel="Negara Penerbit Passport" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="Store_INDV_PassportCountry" OnReadData="National_readData">
                                <Model>
                                    <ext:Model runat="server" ID="ModelPassportCountry">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>--%>
                    <%--end Update--%>
                    <ext:TextField ID="txt_INDV_ID_Number" LabelWidth="250" runat="server" FieldLabel="No. Identitas Lain" AnchorHorizontal="90%"></ext:TextField>
                    <%--Update: Zikri_11092020 Mengubah Negara Penerbit Passport menjadi dropdown--%>
                    <ext:Panel ID="Pnl_cmb_INDV_Nationality1" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_INDV_Nationality1" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringFilter="Active = 1" StringTable="goAML_Ref_Nama_Negara" Label="Kewarganegaraan 1" AnchorHorizontal="70%" AllowBlank="false" LabelWidth="250" />
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Pnl_cmb_INDV_Nationality2" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_INDV_Nationality2" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringFilter="Active = 1" StringTable="goAML_Ref_Nama_Negara" Label="Kewarganegaraan 2" AnchorHorizontal="70%" LabelWidth="250" />
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Pnl_cmb_INDV_Nationality3" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_INDV_Nationality3" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringFilter="Active = 1" StringTable="goAML_Ref_Nama_Negara" Label="Kewarganegaraan 3" AnchorHorizontal="70%" LabelWidth="250" />
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Pnl_cmb_INDV_Residence" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_INDV_Residence" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringFilter="Active = 1" StringTable="goAML_Ref_Nama_Negara" Label="Negara Domisili" AnchorHorizontal="70%" AllowBlank="false" LabelWidth="250" />
                        </Content>
                    </ext:Panel>
                    <%--End Update--%>
                    <%--<ext:ComboBox LabelWidth="250" AllowBlank="False" ID="cmb_INDV_Nationality1" runat="server" FieldLabel="Kewarganegaraan 1" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreNationality1" OnReadData="National_readData">
                                <Model>
                                    <ext:Model runat="server" ID="ModelNationality1">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%--</DirectEvents>
                    </ext:ComboBox>--%>
                    <%-- <ext:ComboBox ID="cmb_INDV_Nationality2" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 2" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreNationality2" OnReadData="National_readData">
                                <Model>
                                    <ext:Model runat="server" ID="ModelNationality2">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%--</DirectEvents>
                    </ext:ComboBox>
                    <ext:ComboBox ID="cmb_INDV_Nationality3" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 3" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreNationality3" OnReadData="National_readData">
                                <Model>
                                    <ext:Model runat="server" ID="ModelNationality3">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%-- </DirectEvents>
                    </ext:ComboBox>
                    <ext:ComboBox ID="cmb_INDV_Residence" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Negara Domisili" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreResidence" OnReadData="National_readData">
                                <Model>
                                    <ext:Model runat="server" ID="ModelResidence">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%--</DirectEvents>
                    </ext:ComboBox>--%>
                    <ext:TextField ID="txt_INDV_Email" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%" EmptyText="name@example.com"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Email2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%" EmptyText="name@example.com"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Email3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%" EmptyText="name@example.com"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Email4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%" EmptyText="name@example.com"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Email5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%" EmptyText="name@example.com"></ext:TextField>
                    <ext:Checkbox runat="server" LabelWidth="250" ID="cb_Deceased" FieldLabel="Deceased ?">
                        <DirectEvents>
                            <Change OnEvent="checkBoxcb_Deceased_click" />
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField Editable="false" runat="server" LabelWidth="250" Hidden="true" FieldLabel="Deceased Date " Format="dd-MMM-yyyy" ID="txt_deceased_date" AnchorHorizontal="90%" />
                    <ext:Checkbox runat="server" LabelWidth="250" ID="cb_tax" FieldLabel="PEP ?">
                    </ext:Checkbox>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="NPWP" ID="txt_INDV_Tax_Number" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Source of Wealth" AllowBlank="False" ID="txt_INDV_Source_of_Wealth" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Catatan" ID="txt_INDV_Comments" AnchorHorizontal="90%" />


                    <ext:TextField ID="txt_INDV_Occupation" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField ID="txt_INDV_Employer_Name" LabelWidth="250" runat="server" FieldLabel="Tempat Bekerja" AnchorHorizontal="90%"></ext:TextField>
                    <ext:GridPanel ID="gp_INDV_Phones" runat="server" Title="Informasi Telepon" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar" runat="server">
                                <Items>
                                    <ext:Button ID="btnAdd_INDV_Phones" runat="server" Text="Add Phones" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddCustomerPhones_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_INDV_Phones" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_INDV_Phones" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumber_INDV_Phones" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="coltph_contact_type" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="coltph_communication_type" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="coltph_country_prefix" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="coltph_number" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Coltph_extension" runat="server" DataIndex="tph_extension" Text="Nomor Extensi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColComments" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <%--Indv Address--%>
                    <ext:GridPanel ID="gp_INDV_Address" runat="server" Title="Informasi Address" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnAdd_INDV_Addresses" runat="server" Text="Add Addresses" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddCustomerAddresses_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_INDV_Addresses" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_INDV_Addresses" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column1" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column3" runat="server" DataIndex="Town" Text="Kecamatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column5" runat="server" DataIndex="Zip" Text="Kode Pos" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="Country" Text="Kode Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="State" Text="Provinsi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end of INDV Address--%>


                    <%--Indv Identificiation--%>
                    <ext:GridPanel ID="GP_INDVD_Identification" runat="server" Title="Identification" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar4" runat="server">
                                <Items>
                                    <ext:Button ID="Button1" runat="server" Text="Add Identification" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddCustomerIdentifications_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_INDV_Identification" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_INDV_Identification" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Document" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Issued_By" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Issue_Date" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Expiry_Date" Type="Date"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Identification_Comment" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column26" runat="server" DataIndex="Number" Text="Nomor" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column25" runat="server" DataIndex="Type_Document" Text="Jenis Dokumen" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column27" runat="server" DataIndex="Issued_By" Text="Issued By" MinWidth="130" Flex="1"></ext:Column>
                                <ext:DateColumn ID="Column28" runat="server" DataIndex="Issue_Date" Text="Issued Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                <ext:DateColumn ID="Column29" runat="server" DataIndex="Expiry_Date" Text="Expired Date" MinWidth="130" Flex="1"></ext:DateColumn>
                                <ext:Column ID="Column30" runat="server" DataIndex="Country" Text="Issued Country" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="Identification_Comment" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn5" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end of INDV Identification--%>
                    <ext:GridPanel ID="GP_Empoloyer_Phone" runat="server" Title="Informasi Telepon Tempat Kerja" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar5" runat="server">
                                <Items>
                                    <ext:Button ID="Button2" runat="server" Text="Add Phones" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddEmployerPhones_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Empoloyer_Phone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column37" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column42" runat="server" DataIndex="Communcation_Type" Text="Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column33" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column34" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column35" runat="server" DataIndex="tph_extension" Text="Nomor Extensi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column36" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn6" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdEmployerPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <%--Employer Address--%>
                    <ext:GridPanel ID="GP_Employer_Address" runat="server" Title="Informasi Address Tempat Kerja" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar6" runat="server">
                                <Items>
                                    <ext:Button ID="Button6" runat="server" Text="Add Addresses" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddEmployerAddresses_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Employer_Address" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model8" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column46" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column38" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column39" runat="server" DataIndex="Town" Text="Kecamatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column40" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column41" runat="server" DataIndex="Zip" Text="Kode Pos" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column45" runat="server" DataIndex="Country" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column43" runat="server" DataIndex="State" Text="Provinsi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column44" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn7" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdEmployerAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end of Employer Address--%>
                </Items>
            </ext:Panel>

            <ext:Panel runat="server" Title="Data Korporasi" Hidden="true" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" Border="true" BodyStyle="padding:10px" ID="panel_Corp">
                <Items>
                    <ext:TextField ID="txt_Corp_Name" FieldLabel="Nama Korporasi" AllowBlank="False" LabelWidth="250" runat="server" AnchorHorizontal="90%"></ext:TextField>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Nama Komersial" ID="txt_Corp_Commercial_Name" AnchorHorizontal="90%" />
                    <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                    <ext:Panel ID="Pnl_cmb_Corp_Incorporation_Legal_Form" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_Corp_Incorporation_Legal_Form" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Bentuk_Badan_Usaha" StringFilter="Active = 1" EmptyText="Select One" Label="Bentuk Korporasi" AnchorHorizontal="70%"/>
                        </Content>
                    </ext:Panel>
                    <%--End Update--%>
                    <%--<ext:ComboBox ID="cmb_Corp_Incorporation_Legal_Form" runat="server" LabelWidth="250" FieldLabel="Bentuk Korporasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" Editable="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="Store_Corp_Incorporation_Legal_Form">
                                <Model>
                                    <ext:Model runat="server" ID="Model_Corp_Incorporation_Legal_Form">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%-- </DirectEvents>
                    </ext:ComboBox>--%>

                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Nomor Induk Berusaha" ID="txt_Corp_Incorporation_number" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Bidang Usaha" AllowBlank="False" ID="txt_Corp_Business" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Email Korporasi" ID="txt_Corp_Email" AnchorHorizontal="90%" EmptyText="name@example.com" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Website Korporasi" ID="txt_Corp_url" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Provinsi" ID="txt_Corp_incorporation_state" AnchorHorizontal="90%" />
                    <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                    <ext:Panel ID="Pnl_cmb_Corp_Incorporation_Country_Code" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_Corp_Incorporation_Country_Code" ValueField="Kode" DisplayField="Keterangan" runat="server" StringField="Kode,Keterangan" LabelWidth="250" StringTable="goAML_Ref_Nama_Negara" StringFilter="Active = 1" EmptyText="Select One" Label="Negara" AnchorHorizontal="70%" AllowBlank="false" />
                        </Content>
                    </ext:Panel>
                    <%--End Update--%>
                    <%--<ext:ComboBox ID="cmb_Corp_Incorporation_Country_Code" AllowBlank="False" runat="server" LabelWidth="250" FieldLabel="Negara" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" Editable="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="Store_Corp_Incorporation_Country_Code">
                                <Model>
                                    <ext:Model runat="server" ID="Model_Corp_Incorporation_Country_Code">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                    <%-- </DirectEvents>
                    </ext:ComboBox>--%>
                    <%--                   <ext:ComboBox ID="cmb_Corp_Role" runat="server" LabelWidth="250" FieldLabel="Peran" DisplayField="Keterangan" ValueField="Kode" allowblank="false" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" Editable="false">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="Store_Corp_Role">
                                <Model>
                                    <ext:Model runat="server" ID="ModelCorpRole">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                            <Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>
                        </DirectEvents>
                   </ext:ComboBox>--%>
                    <ext:DateField Editable="false" runat="server" LabelWidth="250" FieldLabel="Tanggal Pendirian" ID="txt_Corp_incorporation_date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                    <ext:Checkbox runat="server" LabelWidth="250" ID="cbTutup" FieldLabel="Tutup?">
                        <DirectEvents>
                            <Change OnEvent="checkBoxIsClosed_click" />
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField Editable="false" runat="server" LabelWidth="250" Hidden="true" FieldLabel="Tanggal Tutup" ID="txt_Corp_date_business_closed" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="NPWP" ID="txt_Corp_tax_number" AnchorHorizontal="90%" />
                    <%--                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="NPWP Registasi Number" ID="txt_Corp_Tax_Registeration_Number" />--%>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Catatan" ID="txt_Corp_Comments" AnchorHorizontal="90%" />

                    <ext:GridPanel ID="GP_Phone_Corp" runat="server" Title="Informasi Telepon" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar2" runat="server">
                                <Items>
                                    <ext:Button ID="Button3" runat="server" Text="Add Phones" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddCustomerPhones_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Phone_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_Phone_Corp" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column9" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="tph_extension" Text="Nomor Extensi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <%--Corp Address--%>
                    <ext:GridPanel ID="GP_Address_Corp" runat="server" Title="Informasi Address" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar3" runat="server">
                                <Items>
                                    <ext:Button ID="Button4" runat="server" Text="Add Addresses" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddCustomerAddresses_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Address_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_Address_Corp" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column15" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="Town" Text="Kecamatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="Zip" Text="Kode Pos" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="Country" Text="Kode Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column21" runat="server" DataIndex="State" Text="Provinsi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn4" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end of Corp Address--%>

                    <%--Corp Director--%>
                    <ext:GridPanel ID="GP_Corp_Director" runat="server" Title="Director Corp" AutoScroll="true">
                        <TopBar>
                            <ext:Toolbar ID="toolbar7" runat="server">
                                <Items>
                                    <ext:Button ID="Button7" runat="server" Text="Add Director" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btnAddCustomerDirector_DirectClick">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="Store_Director_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_Director_Corp" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Entity_Director_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Role" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                                <ext:Column ID="Column31" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column32" runat="server" DataIndex="Role" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn8" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                        <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--end of Corp Director--%>
                </Items>
            </ext:Panel>



            <%--   <ext:Panel runat="server" Title="Notes History" Collapsible="true" AnchorHorizontal="100%">
                <Items>
                    <ext:GridPanel ID="gp_history" runat="server">
                        <Store>
                            <ext:Store ID="gpStore_history" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" IDProperty="CIF">
                                        <Fields>
                                            <ext:ModelField Name="PK_TX_IFTI_Flow_History_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Module_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UnikID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="UserID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UserName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="RoleName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Notes" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>

                        <ColumnModel runat="server">
                            <Columns>
                                <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="70"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="User Name" DataIndex="UserName" CellWrap="true" flex="1" />
                                <ext:DateColumn runat="server" Text="Created Date" DataIndex="CreatedDate" CellWrap="true" flex="1" Format="dd-MMM-yyyy" />
                                <ext:Column runat="server" Text="Status" DataIndex="Status" CellWrap="true" Flex="1" />
                                <ext:Column runat="server" Text="Notes" DataIndex="Notes" CellWrap="true" flex="4" />
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>--%>
        </Items>

        <Buttons>
            <ext:Button ID="btnSubmit" runat="server" Icon="Disk" Text="Submit">
                <DirectEvents>
                    <Click OnEvent="BtnSubmit_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancelIncoming" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnBack_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>

        </Items>

        <Buttons>

            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>

