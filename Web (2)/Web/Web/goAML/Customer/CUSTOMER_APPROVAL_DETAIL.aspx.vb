﻿Imports Ext.Net
Imports NawaDevBLL

Partial Class CUSTOMER_APPROVAL_DETAIL
    Inherits Parent

    Public Property CustomMessage As String

    Private _IDReq As Long
    Public Property IDReq() As Long
        Get
            Return _IDReq
        End Get
        Set(ByVal value As Long)
            _IDReq = value
        End Set
    End Property

    Private _objApproval As NawaDAL.ModuleApproval
    Public Property ObjApproval() As NawaDAL.ModuleApproval
        Get
            Return _objApproval
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            _objApproval = value
        End Set
    End Property



    Public Property objSchemaModule() As NawaDAL.Module
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL.objSchemaModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("CUSTOMER_APPROVAL_DETAIL.objSchemaModule") = value
        End Set
    End Property

    Public Property ObjFieldData() As String
        Get
            Return Session("CUSTOMER_APPROVAL_DETAIL.ObjFieldData")
        End Get
        Set(ByVal value As String)
            Session("CUSTOMER_APPROVAL_DETAIL.ObjFieldData") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim strid As String = Request.Params("ID")
            Dim strModuleid As String = Request.Params("ModuleID")

            Try

                IDReq = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim moduleid As Integer = NawaBLL.Common.DecryptQueryString(strModuleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                objSchemaModule = NawaBLL.ModuleBLL.GetModuleByModuleID(moduleid)
            Catch ex As Exception
                Throw New Exception("Invalid ID Approval.")
            End Try

            If Not Ext.Net.X.IsAjaxRequest Then


                ObjApproval = NawaBLL.ModuleApprovalBLL.GetModuleApprovalByID(IDReq)
                If Not ObjApproval Is Nothing Then
                    PanelInfo.Title = "Daftar Customer Approval"
                    lblModuleName.Text = objSchemaModule.ModuleLabel
                    lblModuleKey.Text = ObjApproval.ModuleKey
                    lblAction.Text = NawaBLL.ModuleBLL.GetModuleActionNamebyID(ObjApproval.PK_ModuleAction_ID)
                    LblCreatedBy.Text = ObjApproval.CreatedBy
                    lblCreatedDate.Text = ObjApproval.CreatedDate.Value.ToString("dd-MMM-yyyy")

                End If

                Select Case ObjApproval.PK_ModuleAction_ID
                    Case NawaBLL.Common.ModuleActionEnum.Insert

                        ObjFieldData = ObjApproval.ModuleField

                        Dim unikkey As String = Guid.NewGuid.ToString

                        Dim objNewEODTask As New NawaDevBLL.goAML_CustomerBLL
                        NawaDevBLL.goAML_CustomerBLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkey)


                        FormPanelOld.Visible = False
                    Case NawaBLL.Common.ModuleActionEnum.Delete
                        Dim unikkeyNew As String = Guid.NewGuid.ToString

                        Dim objNewMGroupAccess As New NawaDevBLL.goAML_CustomerBLL
                        NawaDevBLL.goAML_CustomerBLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkeyNew)




                        FormPanelOld.Visible = False
                    Case NawaBLL.Common.ModuleActionEnum.Update
                        ObjFieldData = ObjApproval.ModuleField
                        Dim unikkeyOld As String = Guid.NewGuid.ToString
                        Dim unikkeyNew As String = Guid.NewGuid.ToString

                        Dim objNewMGroupAccess As New NawaDevBLL.goAML_CustomerBLL
                        NawaDevBLL.goAML_CustomerBLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkeyNew)
                        NawaDevBLL.goAML_CustomerBLL.LoadPanel(FormPanelOld, ObjApproval.ModuleFieldBefore, ObjApproval.ModuleName, unikkeyOld)


                    'NawaDevBLL.goAML_CustomerBLL.SettingColor(FormPanelOld, FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleFieldBefore, unikkeyOld, unikkeyNew)


                    'objNewMGroupAccess.SettingColor(FormPanelOld, FormPanelNew, ObjApproval.ModuleFieldBefore, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkeyOld, unikkeyNew)

                    Case NawaBLL.Common.ModuleActionEnum.Activation
                        Dim unikkeyNew As String = Guid.NewGuid.ToString
                        Dim objNewMGroupAccess As New NawaDevBLL.goAML_CustomerBLL
                        NawaDevBLL.goAML_CustomerBLL.LoadPanel(FormPanelNew, ObjApproval.ModuleField, ObjApproval.ModuleName, unikkeyNew)

                        FormPanelOld.Visible = False
                End Select


            End If




        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Public Sub Yes()
        Try
            NawaDevBLL.goAML_CustomerBLL.Accept(Me.IDReq)

            LblConfirmation.Text = "Data Approved. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSave_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            If Not String.IsNullOrEmpty(ObjFieldData) Then
                Dim objNew As NawaDevBLL.goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(ObjFieldData, GetType(NawaDevBLL.goAML_CustomerDataBLL))
                Dim txtGCN As String = objNew.objgoAML_Ref_Customer.GCN
                Dim checkGCN As NawaDevDAL.goAML_Ref_Customer = NawaDevBLL.goAML_CustomerBLL.GetCheckGCN(txtGCN)
                If objNew.objgoAML_Ref_Customer.isGCNPrimary = True Then
                    If checkGCN IsNot Nothing Then
                        If checkGCN.CIF <> objNew.objgoAML_Ref_Customer.CIF Then
                            Dim yesBtn = New MessageBoxButtonConfig
                            'Handler = "App.direct.DoThing({ eventMask: { showMask: true, msg: 'Please wait ...' }})",
                            yesBtn.Handler = "NawadataDirect.Yes({ eventMask: { showMask: true, msg: 'Please wait ...' }})"
                            yesBtn.Text = "Ok"


                            Dim listBtn = New MessageBoxButtonsConfig
                            listBtn.Yes = yesBtn


                            Ext.Net.X.MessageBox.Confirm("Information", "Konfirmasi : Untuk GCN " + txtGCN + " sudah memiliki GCNPrimary pada CIF " + checkGCN.CIF + " Jika di save, status GCNPrimary di CIF " + checkGCN.CIF + " akan dilepas dan akan dipasang di CIF ini. Tekan Tombol 'OK' untuk melanjutkan", listBtn).Show()
                        Else
                            NawaDevBLL.goAML_CustomerBLL.Accept(Me.IDReq)

                            LblConfirmation.Text = "Data Approved. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
                            container.Hidden = True
                            Panelconfirmation.Hidden = False
                            container.Render()
                            Panelconfirmation.Render()
                        End If
                    Else
                        NawaDevBLL.goAML_CustomerBLL.Accept(Me.IDReq)

                        LblConfirmation.Text = "Data Approved. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
                        container.Hidden = True
                        Panelconfirmation.Hidden = False
                        container.Render()
                        Panelconfirmation.Render()
                    End If

                Else
                    NawaDevBLL.goAML_CustomerBLL.Accept(Me.IDReq)

                    LblConfirmation.Text = "Data Approved. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
                    container.Hidden = True
                    Panelconfirmation.Hidden = False
                    container.Render()
                    Panelconfirmation.Render()
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub


    Protected Sub BtnReject_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            NawaDevBLL.goAML_CustomerBLL.Reject(Me.IDReq)
            LblConfirmation.Text = "Data Rejected. Click Ok to Back To " & objSchemaModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objSchemaModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs) Handles BtnConfirmation.DirectClick
        Try


            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objSchemaModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
End Class