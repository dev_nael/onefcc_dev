﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="PpatkCsvResult.aspx.vb" Inherits="goAML_PpatkCsvResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:FormPanel runat="server" ButtonAlign="Center" ID="MainPanel"
        BodyPadding="15"
        Title="Upload File"
        MonitorResize="true"
        Layout="AnchorLayout" 
        Visible ="true"
        AutoScroll="true" 
        ClientIDMode="Static">
        <Items>
            <ext:FieldContainer runat="server" Layout="HBoxLayout" AnchorHorizontal="100%">
                <Items>
                    <ext:FileUploadField ID="UploadField" runat="server" Flex="1" Icon="Attach" FieldLabel="File"/>
                </Items>
            </ext:FieldContainer>
            <%-- The combo box is unused. It is hidden as per request. I leave it here just in case --%>
            <ext:ComboBox
                ID="JenisLaporan"
                runat="server"
                FieldLabel="Jenis Laporan"
                DisplayField="Jenis"
                Width="700"
                LabelWidth="100"
                Hidden="True">
                <Store>
                    <ext:Store runat="server" AutoDataBind="true">
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="Jenis" />
                                    <ext:ModelField Name="Kode" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
            <ext:Button ID="UploadButton" runat="server" Text="Submit File">
                <DirectEvents>
                    <Click OnEvent="UploadClick"></Click>
                </DirectEvents>
            </ext:Button>
            <ext:Label runat="server" ID="Warning"></ext:Label>
            <%-- Debug button is invisible unless the page is accessed directly using URL --%>
            <ext:Button ID="DebugButton" runat="server" Text="Debug Button">
                <DirectEvents>
                    <Click OnEvent="TestClick"></Click>
                </DirectEvents>
            </ext:Button>
            <%-- Console area is invisible unless the page is accessed directly using URL --%>
            <ext:TextArea runat="server" ID="ConsoleArea" Width="1200" Height="400" Hidden="True"></ext:TextArea>
            <ext:FieldContainer runat="server"></ext:FieldContainer>
        </Items>
    </ext:FormPanel>

    <ext:FormPanel ID="Panelconfirmation" 
        BodyPadding="20" 
        runat="server" 
        ClientIDMode="Static" 
        Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel" Text="File submitted successfully">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">    
                <DirectEvents>
                    <Click OnEvent="GoBackClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
