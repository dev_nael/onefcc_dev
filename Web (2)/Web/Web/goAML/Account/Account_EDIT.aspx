﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="Account_EDIT.aspx.vb" Inherits="Account_EDIT" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        //Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
        //    return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        //};


        //Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
        //    return {
        //        type: "string",
        //        op: "*",
        //        value: value
        //    };
        //};

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Pop Up--%>
    <ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Signatory Add" Hidden="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSetAccountSignatoryAdd" runat="server" Title="Signatory" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <ext:FormPanel ID="FormPanelTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                            <ext:Hidden runat="server" ID="fld_sender"></ext:Hidden>
                            <%--<ext:TextField ID="txtCet" runat="server" LabelWidth="180" FieldLabel="CET No" AnchorHorizontal="90%"></ext:TextField>--%>
                            <%--Dedy Added 27082020--%>
                            <ext:Checkbox runat="server" ID="cb_isCustomer" FieldLabel="Customer ?">
                                <DirectEvents>
                                    <Change OnEvent="IsCustomer_DirectEvent"></Change>
                                </DirectEvents>
                            </ext:Checkbox>
                            <%--Dedy end Added 27082020--%>
                            <%--Update: Zikri_24092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_SignatoryPerson" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_SignatoryPerson" ValueField="CIF" DisplayField="Name" runat="server" StringField="CIF,Name" StringTable="Vw_CB_Signatory_Person" DisplayTextWithValue="true" StringFilter="Active = 1" EmptyText="Select One" Label="Pilih Customer Individu" AnchorHorizontal="70%" AllowBlank="false" OnOnValueChanged="cmb_SignatoryPerson_DirectSelect" />
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Pnl_cmb_Not_SignatoryPerson" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_Not_SignatoryPerson" IsHidden="true" ValueField="WIC NO" DisplayField="Name" runat="server" StringField="[WIC NO],Name" StringTable="Vw_CB_Signatory_Not_Person" DisplayTextWithValue="true" StringFilter="Active = 1" EmptyText="Select One" Label="Pilih WIC Individu" AnchorHorizontal="70%" AllowBlank="false" OnOnValueChanged="cmb_SignatoryPerson_DirectSelect" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cmb_SignatoryPerson" runat="server" FieldLabel="Pilih Customer Individu" DisplayField="cb_value"
                                ValueField="CIF" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true"
                                AllowBlank="false" IndicatorCls="required-text" IndicatorText="*">
                                <Store>--%>
                            <%--<ext:Store runat="server" ID="store_SignatoryPerson" OnReadData="SignatoryPerson_readData" IsPagingStore="true"> --%>
                            <%--<ext:Store runat="server" ID="store_SignatoryPerson">
                                        <Model>
                                            <ext:Model runat="server" ID="Model1">
                                                <Fields>
                                                    <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="cb_value" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>--%>
                            <%--<Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>--%>
                            <%-- </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Change OnEvent="cmb_SignatoryPerson_DirectSelect">
                                        <ExtraParams>
                                            <ext:Parameter Name="CIF" Value="CIF" Mode="Raw" />
                                        </ExtraParams>
                                    </Change>
                                </DirectEvents>
                            </ext:ComboBox>--%>

                            <ext:Checkbox runat="server" ID="cbIsPrimary" FieldLabel="Pemilik Utama?" Checked="true"></ext:Checkbox>
                            <%--Update: Zikri_25092020 Change dropdown to NDFSDropdown--%>
                            <ext:Panel ID="Pnl_cmb_SignatoryRole" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                                <Content>
                                    <nds:NDSDropDownField ID="cmb_SignatoryRole" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Peran_orang_dalam_rekening" StringFilter="Active = 1" EmptyText="Select One" Label="Peran" AnchorHorizontal="90%" AllowBlank="false" />
                                </Content>
                            </ext:Panel>
                            <%--End Update--%>
                            <%--<ext:ComboBox ID="cmb_SignatoryRole" runat="server" AnchorHorizontal="90%" FieldLabel="Peran"
                                DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" ForceSelection="true"
                                IndicatorCls="required-text" IndicatorText="*" TypeAhead="false">
                                <Store>
                                    <ext:Store runat="server" ID="store_SignatoryRole" OnReadData="SignatoryRole_readData">
                                        <Model>
                                            <ext:Model runat="server" ID="model_SignatoryRole" IDProperty="Kode">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                            --%>        <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                            <%-- </DirectEvents>
                            </ext:ComboBox>--%>

                            <ext:TextField ID="txt_INDV_Title" runat="server" FieldLabel="Gelar" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_INDV_Last_Name" runat="server" FieldLabel="Nama Lengkap" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:DateField Editable="false"  runat="server" FieldLabel="Tanggal Lahir" ID="txt_INDV_Birthdate" Format="dd-MMM-yyyy" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%" />
                            <ext:TextField ID="txt_INDV_Birth_place" runat="server" FieldLabel="Tempat Lahir" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_INDV_Mothers_name" runat="server" FieldLabel="Nama Ibu Kandung" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_INDV_Alias" runat="server" FieldLabel="Nama Alias" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_INDV_SSN" runat="server" FieldLabel="NIK" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_INDV_Passport_number" runat="server" FieldLabel="No. Passport" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_INDV_Passport_country" runat="server" FieldLabel="Negara Penerbit Passport" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_INDV_ID_Number" runat="server" FieldLabel="No. Identitas Lain" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_INDV_Nationality1" runat="server" FieldLabel="Kewarganegaraan 1" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_INDV_Nationality2" runat="server" FieldLabel="Kewarganegaraan 2" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_INDV_Nationality3" runat="server" FieldLabel="Kewarganegaraan 3" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_INDV_Residence" runat="server" FieldLabel="Residence" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:TextField ID="txt_INDV_Email" runat="server" FieldLabel="Email" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>
                            <ext:Checkbox runat="server" ReadOnly="true" ID="cb_DeceasedSignatory" FieldLabel="Deceased ?">
                                <DirectEvents>
                                    <Change OnEvent="checkBoxcb_Deceased_click" />
                                </DirectEvents>
                            </ext:Checkbox>
                            <ext:DateField Editable="false" runat="server" Hidden="true" ReadOnly="true" FieldLabel="Deceased Date" FieldStyle="background-color:lightgray" Format="dd-MMM-yyyy" ID="txt_deceased_dateSignatory" AnchorHorizontal="90%" />
                            <ext:Checkbox runat="server" ReadOnly="true" ID="cb_taxSignatory" FieldLabel="PEP ?">
                            </ext:Checkbox>
                            <ext:TextField runat="server" ReadOnly="true" FieldStyle="background-color:lightgray" FieldLabel="NPWP" ID="txt_INDV_Tax_NumberSignatory" AnchorHorizontal="90%" />
                            <ext:TextField runat="server" ReadOnly="true" FieldStyle="background-color:lightgray" FieldLabel="Source of Wealth" ID="txt_INDV_Source_of_WealthSignatory" AnchorHorizontal="90%" />
                            <ext:TextField runat="server" ReadOnly="true" FieldStyle="background-color:lightgray" FieldLabel="Catatan" ID="txt_INDV_CommentsSignatory" AnchorHorizontal="90%" />
                            <ext:TextField ID="txt_INDV_Occupation" runat="server" FieldLabel="Occupation" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:TextField>

                            <%--Address Signatory--%>
                            <ext:GridPanel ID="GP_AddressSignatory" runat="server" Title=" Informasi Alamat" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                                <Store>
                                    <ext:Store ID="StoreAddressSignatory" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="ModelAddressSignatory" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column28" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column14" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column8" runat="server" DataIndex="Town" Text="Kecamatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column4" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column9" runat="server" DataIndex="Zip" Text="Kode Pos" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column12" runat="server" DataIndex="Country" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column6" runat="server" DataIndex="State" Text="Ibu Kota" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column13" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdAddressDirector">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--End of Address Signatory--%>

                            <%--Phone Signatory--%>
                            <ext:GridPanel ID="GP_PhoneSignatory" runat="server" Title="Informasi Telepon" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                                <Store>
                                    <ext:Store ID="StorePhoneSignatory" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="ModelPhoneSignatory" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="column22" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column23" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column24" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column25" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column26" runat="server" DataIndex="tph_extension" Text="Nomor Ekstensi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column27" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn4" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdPhoneDirector">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>

                            <ext:GridPanel ID="GP_EmployerAddressSignatory" runat="server" Title="Informasi Alamat Tempat Bekerja" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                                <Store>
                                    <ext:Store ID="StoreEmployerAddressSignatory" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model2" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column10" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column11" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column15" runat="server" DataIndex="Town" Text="Kecamatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column16" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column29" runat="server" DataIndex="Zip" Text="Kode Pos" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column31" runat="server" DataIndex="Country" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column32" runat="server" DataIndex="State" Text="Ibu Kota" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column33" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdEmployerAddressSignatory">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--Phone Signatory--%>
                            <ext:GridPanel ID="GP_EmployerPhoneSignatory" runat="server" Title="Informasi Telepon Tempat Bekerja" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                                <Store>
                                    <ext:Store ID="StoreEmployerPhoneSignatory" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model3" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="column34" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column35" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column36" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column37" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column38" runat="server" DataIndex="tph_extension" Text="Nomor Ekstensi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column39" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdEmployerPhoneSignatory">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--End of Phone Signatory--%>
                            <%--End of Phone Signatory--%>

                            <%--                           <ext:FormPanel ID="FormPanel_Employer" runat="server" Hidden="false" ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px" FieldStyle="background-color:lightgray">
                           <Items>
                               <ext:TextField ID="txt_INDV_Employer_Name" runat="server" FieldLabel="Tempat Bekerja" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                               
                               <ext:TextField ID="txt_INDV_Employer_Address_Address_Type" runat="server" FieldLabel="Tipe Alamat" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                               <ext:TextField ID="txt_INDV_Employer_Address_Address" runat="server" FieldLabel="Alamat" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                               <ext:TextField ID="txt_INDV_Employer_Address_Town" runat="server" FieldLabel="Kecamatan" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                               <ext:TextField ID="txt_INDV_Employer_Address_City" runat="server" FieldLabel="Kota/Kabupaten" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                               <ext:TextField ID="txt_INDV_Employer_Address_Zip" runat="server" FieldLabel="Kodepos" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                               <ext:TextField ID="txt_INDV_Employer_Address_country_code" runat="server" FieldLabel="Negara" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                               <ext:TextField ID="txt_INDV_Employer_Address_State" runat="server" FieldLabel="Provisi" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                               <ext:TextField ID="txt_INDV_Employer_Address_Comment" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>

                               <ext:TextField ID="txt_INDV_Employer_Phone_contact_type" runat="server" FieldLabel="Kategori Kontak" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                               <ext:TextField ID="txt_INDV_Employer_Phone_communication_type" runat="server" FieldLabel="Jenis Alat Komunikasi" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                               <ext:TextField ID="txt_INDV_Employer_Phone_country_prefix" runat="server" FieldLabel="Kode Area Telepon" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                               <ext:TextField ID="txt_INDV_Employer_Phone_number" runat="server" FieldLabel="Nomor Telepon" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                               <ext:TextField ID="txt_INDV_Employer_Phone_extension" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                               <ext:TextField ID="txt_INDV_Employer_Phone_Comment" runat="server" FieldLabel="Nomor Ekstensi" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray"></ext:TextField>
                           </Items>
                           </ext:FormPanel>--%>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSavedetail" runat="server" Text="Save" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveAccountSignatory_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBackSaveDetail" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveAccountSignatory_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailAddress" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Address Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet2" runat="server" Title="Address" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <%--Panel Address--%>
                    <ext:FormPanel ID="FP_Address" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="JenisAlamat" runat="server" FieldLabel="Jenis Tempat Tinggal"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Alamat" runat="server" FieldLabel="Alamat"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Kecamatan" runat="server" FieldLabel="Kecamatan"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Kota" runat="server" FieldLabel="Kota"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Kode_Pos" runat="server" FieldLabel="Kode Pos"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Kode_Negara" runat="server" FieldLabel="Kode Negara"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Ibu_Kota" runat="server" FieldLabel="Ibu Kota"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Catatan" runat="server" FieldLabel="Catatan"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button1" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveAccountAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <%--End Panel Address--%>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowDetailPhone" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Phone Add" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet3" runat="server" Title="Phone" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>
                    <%--Panel Phone--%>
                    <ext:FormPanel ID="FP_Phone" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="KategoriKontak" runat="server" FieldLabel="Kategori Kontak"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="JenisAlatKomunikasi" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="KodeAreaTelp" runat="server" FieldLabel="Kode Area Telp"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="NomorTelepon" runat="server" FieldLabel="Nomor Telepon"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="NomorEkstensi" runat="server" FieldLabel="Nomor Ekstensi"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Catatan_Phone" runat="server" FieldLabel="Catatan"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button3" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveAccountPhone_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <%--End Panel Phone--%>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <%--    <ext:Window ID="WindowDetailDirector" runat="server" Icon="ApplicationViewDetail" Title="Director Add" Hidden="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet4" runat="server" Title="Director" Collapsible="true" Layout="FitLayout">
                <Items>                
                </Items>
                <Content>--%>
    <%--Panel Director--%>
    <%--                    <ext:FormPanel ID="FormPanelDetailDirector" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                            <ext:Hidden runat="server" ID="Hidden1"></ext:Hidden>
                           <ext:TextField AnchorHorizontal="90%" ID="Name" runat="server" FieldLabel="Nama Lengkap"></ext:TextField>
                           <ext:DisplayField ID="Role" runat="server" FieldLabel="Role"></ext:DisplayField>
                           <ext:DisplayField ID="BirthPlace" runat="server" FieldLabel="Tempat Lahir"></ext:DisplayField>
                           <ext:DisplayField ID="BirthDate" runat="server" FieldLabel="Tanggal Lahir" ></ext:DisplayField>
                           <ext:DisplayField ID="MomName" runat="server" FieldLabel="Nama Ibu Kandung"></ext:DisplayField>
                           <ext:DisplayField ID="Aliase" runat="server" FieldLabel="Nama Alias"></ext:DisplayField>
                           <ext:DisplayField ID="SSN" runat="server" FieldLabel="NIK"></ext:DisplayField>
                           <ext:DisplayField ID="PassportNo" runat="server" FieldLabel="No. Passport" ></ext:DisplayField>
                           <ext:DisplayField ID="PassportFrom" runat="server" FieldLabel="Negara Penerbit Passport"></ext:DisplayField>
                           <ext:DisplayField ID="No_Identity" runat="server" FieldLabel="No. Identitas Lain" ></ext:DisplayField>
                           <ext:DisplayField ID="Nasionality1" runat="server" FieldLabel="Kewarganegaraan 1" ></ext:DisplayField>
                           <ext:DisplayField ID="Nasionality2" runat="server" FieldLabel="Kewarganegaraan 2" ></ext:DisplayField>
                           <ext:DisplayField ID="Nasionality3" runat="server" FieldLabel="Kewarganegaraan 3" ></ext:DisplayField>
                           <ext:DisplayField ID="Residence" runat="server" FieldLabel="Residence" ></ext:DisplayField>
                           <ext:DisplayField ID="Email" runat="server" FieldLabel="Email" ></ext:DisplayField>
                           <ext:DisplayField ID="Pekerjaan" runat="server" FieldLabel="Occupation" ></ext:DisplayField>--%>

    <%--Address--%>
    <%--                    <ext:GridPanel ID="GP_Director_Address" runat="server" Title="Address" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="StoreDirectorAddress" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="modelDirectorAddress" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column29" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="State" Text="Ibu Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdAddressDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>--%>
    <%--End of Address--%>

    <%--Phone--%>
    <%--<ext:GridPanel ID="GP_Director_Phone" runat="server" Title="Phone" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="StoreDirectorPhone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="modelDirectorPhone" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="column12" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="tph_extension" Text="Nomor Ekstensi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdPhoneDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>--%>
    <%--End of Phone--%>

    <%--                            </Items>
                        <Buttons>
                            <ext:Button ID="Button2" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveAccountDirector_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>--%>
    <%--End Panel Director--%>
    <%--</Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>--%>
    <%--Pop Up--%>

    <%--Pop Up--%>
    <ext:Window ID="WindowAddressandPhoneDirector" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Detail" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet1" runat="server" Title="Director" Collapsible="true" Layout="FitLayout">
                <Items>
                </Items>
                <Content>

                    <%--Panel Address Director--%>
                    <ext:FormPanel ID="FP_Address_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="JenisAlamatDirector" runat="server" FieldLabel="Jenis Tempat Tinggal"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="AlamatDirector" runat="server" FieldLabel="Alamat"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="KecamatanDirector" runat="server" FieldLabel="Kecamatan"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="KotaDirector" runat="server" FieldLabel="Kota"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Kode_PosDirector" runat="server" FieldLabel="Kode Pos"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Kode_NegaraDirector" runat="server" FieldLabel="Kode Negara"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Ibu_KotaDirector" runat="server" FieldLabel="Ibu Kota"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="CatatanAddressDirector" runat="server" FieldLabel="Catatan"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button4" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackPhoneandAddress">
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <%--End Panel Address Director--%>

                    <%--Panel Phone Director--%>
                    <ext:FormPanel ID="FP_Phone_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="KategoriKontakDirector" runat="server" FieldLabel="Kategori Kontak"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="JenisAlatKomunikasiDirector" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="KodeAreaTelpDirector" runat="server" FieldLabel="Kode Area Telp"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="NomorTeleponDirector" runat="server" FieldLabel="Nomor Telepon"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="NomorEkstensiDirector" runat="server" FieldLabel="Nomor Ekstensi"></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Catatan_PhoneDirector" runat="server" FieldLabel="Catatan"></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button5" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackPhoneandAddress">
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <%--End Panel Phone Director--%>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

    <ext:FormPanel ID="FormPanelInput" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="Account Edit" BodyStyle="padding:10px">
        <Items>
            <ext:Hidden runat="server" ID="PK_Customer_ID" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Account No" ID="txt_AccountNo" AnchorHorizontal="90%" MaxLength="50" EnforceMaxLength="true"/>
            <ext:TextField runat="server" AllowBlank="false" LabelWidth="250" FieldLabel="Client Number" ID="txt_CIF" AnchorHorizontal="90%" MaxLength="30" EnforceMaxLength="true"/>
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Currency Code" ID="txt_CurrencyCode" AnchorHorizontal="90%" />
            <ext:TextField ID="txt_Branch" LabelWidth="250" AllowBlank="false" runat="server" FieldLabel="Branch" AnchorHorizontal="90%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
            <ext:TextField runat="server" LabelWidth="250" FieldLabel="Account Name" ID="txt_AccountName" AnchorHorizontal="90%" MaxLength="255" EnforceMaxLength="true"/>
            <ext:TextField ID="txt_IBAN" LabelWidth="250" runat="server" FieldLabel="IBAN" AnchorHorizontal="90%" MaxLength="34" EnforceMaxLength="true"></ext:TextField>
            <%--Update: Zikri_23092020 Change dropdown to NDFSDropdown--%>
            <ext:Panel ID="Pnl_Cmb_PersonalAccountType" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                <Content>
                    <nds:NDSDropDownField ID="Cmb_PersonalAccountType" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_Jenis_Rekening" StringFilter="Active = 1" LabelWidth="250" EmptyText="Select One" Label="Personal Account Type" AnchorHorizontal="70%" AllowBlank="false" OnOnValueChanged="Cmb_PersonalAccountType_DirectSelect" />
                </Content>
            </ext:Panel>
            <%--End Update--%>

            <%--Dedy Added onread data--%>
            <%--<ext:ComboBox ID="Cmb_PersonalAccountType" LabelWidth="250" runat="server" FieldLabel="Personal Account Type" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" QueryMode="Local" AnyMatch="true" allowBlank="false" IndicatorCls="required-text" IndicatorText="*" TypeAhead="false">
                <Store>--%>
            <%--<ext:Store runat="server" ID="StorePersonalAccountType" OnReadData="PersonalAccountType_readData" IsPagingStore="true" AutoLoad="false">--%>
            <%--<ext:Store runat="server" ID="StorePersonalAccountType" OnReadData="PersonalAccountType_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server" ID="ModelPersonalAccountType" IDProperty="Kode">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <DirectEvents>--%>
            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
            <%--</DirectEvents>
            </ext:ComboBox>--%>
            <ext:DateField Editable="false" runat="server" LabelWidth="250" FieldLabel="Tanggal Pembukaan Rekening" AllowBlank="false" ID="txt_Opened" Format="dd-MMM-yyyy" AnchorHorizontal="90%" />
            <ext:DateField Editable="false" runat="server" LabelWidth="250" FieldLabel="Tanggal Penutupan Rekening" ID="txt_Closed" Format="dd-MMM-yyyy" AnchorHorizontal="90%" />
            <ext:NumberField runat="server" LabelWidth="250" FieldLabel="Saldo Akhir" ID="txt_Balance" DecimalPrecision="2" AnchorHorizontal="90%" />
            <ext:DateField Editable="false" runat="server" LabelWidth="250" FieldLabel="Tanggal Saldo" ID="txt_Balance_Date" Format="dd-MMM-yyyy" AnchorHorizontal="90%" />
            <%--Update: Zikri_23092020 Change dropdown to NDFSDropdown--%>
            <ext:Panel ID="Pnl_Cmb_StatusCode" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                <Content>
                    <nds:NDSDropDownField ID="Cmb_StatusCode" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="goAML_Ref_status_Rekening" StringFilter="Active = 1" LabelWidth="250" EmptyText="Select One" Label="Status Rekening" AnchorHorizontal="70%" AllowBlank="false" />
                </Content>
            </ext:Panel>
            <%--End Update--%>

            <%--Dedy Added 28082020 for onread data--%>
            <%--<ext:ComboBox ID="Cmb_StatusCode" LabelWidth="250" runat="server" FieldLabel="Status Rekening" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" QueryMode="Local" AnyMatch="true" AllowBlank="false" IndicatorCls="required-text" IndicatorText="*" TypeAhead="false">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreStatusCode" OnReadData="StatusCode_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server" ID="ModelStatusCode">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
                <DirectEvents>--%>
            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
            <%--</DirectEvents>
            </ext:ComboBox>--%>
            <ext:TextField ID="txt_beneficiary" LabelWidth="250" runat="server" FieldLabel="Nama Penerima Manfaat Utama" AnchorHorizontal="90%" MaxLength="50" EnforceMaxLength="true"></ext:TextField>
            <ext:TextField ID="txt_beneficiary_comment" LabelWidth="250" runat="server" FieldLabel="Catatan Terkait Penerima Manfaat Utama" AnchorHorizontal="90%" MaxLength="255" EnforceMaxLength="true"></ext:TextField>
            <ext:TextField ID="txt_comments" LabelWidth="250" runat="server" FieldLabel="Catatan" AnchorHorizontal="90%" MaxLength="4000" EnforceMaxLength="true"></ext:TextField>
            <ext:Checkbox runat="server" ID="cb_isUpdateFromDataSouce" LabelWidth="250" FieldLabel="Update From Data Source?" Checked="true" AnchorHorizontal="90%"></ext:Checkbox>
            <%--Entity--%>
            <ext:Checkbox runat="server" ID="cb_EntityAccount" LabelWidth="250" FieldLabel="Rekening Korporasi?" Checked="true" AnchorHorizontal="100%" Collapsible="true" Layout="AnchorLayout" Border="true" BodyStyle="padding:10px"></ext:Checkbox>
            <ext:Panel runat="server" Title="Rekening Korporasi" Collapsible="true" AnchorHorizontal="100%" Layout="AnchorLayout" ID="panel_Corp" Hidden="True" Border="true" BodyStyle="padding:10px">
                <Items>
                    <%--Update: Zikri_17092020 Change dropdown to NDFSDropdown--%>
                    <ext:Panel ID="Pnl_cmb_EntityAccount" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                        <Content>
                            <nds:NDSDropDownField ID="cmb_EntityAccount" ValueField="CIF" DisplayField="Corp_Name" runat="server" StringField="CIF,Corp_Name" StringTable="goAML_Ref_Customer" LabelWidth="250" StringFilter="(FK_Customer_Type_ID = 2) AND Active = 1" DisplayTextWithValue="true" EmptyText="Select One" Label="Pilih Customer Korporasi" AnchorHorizontal="70%" AllowBlank="false" OnOnValueChanged="cmb_EntityAccount_DirectSelect" />
                        </Content>
                    </ext:Panel>
                    <%--End Update--%>
                    <%--<ext:ComboBox ID="cmb_EntityAccount" LabelWidth="250" runat="server" FieldLabel="Pilih Customer Korporasi" DisplayField="cb_value" ValueField="CIF" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" PaddingSpec="0 10 0 0" AllowBlank="false" IndicatorCls="required-text" IndicatorText="*">
                        <Store>
                            <ext:Store runat="server" ID="store_EntityAccount" OnReadData="EntityAccount_readData">--%>
                    <%-- <ext:Store runat="server" ClientIDMode="Static" ID="store_EntityAccount" >--%>
                    <%--<Model>
                                    <ext:Model runat="server" ID="model_EntityAccount">
                                        <Fields>
                                            <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="cb_value" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:PageProxy>
                                    </ext:PageProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                            <Change OnEvent="cmb_EntityAccount_DirectSelect">
                                <ExtraParams>
                                    <ext:Parameter Name="CIF" Value="CIF" Mode="Raw" />
                                </ExtraParams>
                            </Change>
                        </DirectEvents>
                    </ext:ComboBox>--%>

                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Nama Korporasi" ID="txt_Corp_Name" ReadOnly="true" AnchorHorizontal="90%" FieldStyle="background-color:lightgray" MaxLength="255" EnforceMaxLength="true"/>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Nama Komersial" ID="txt_Corp_Commercial_Name" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Bentuk Korporasi" ID="txt_Corp_Legal_Form" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Nomor Induk Berusaha" ID="txt_Corp_Incorporation_number" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%" MaxLength="50" EnforceMaxLength="true"/>
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Bidang Usaha" ID="txt_Corp_Business" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Email Korporasi" ID="txt_Corp_Email" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Website Korporasi" ID="txt_Corp_url" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Provinsi" ID="txt_Corp_incorporation_state" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Negara" ID="txt_Corp_incorporation_country_code" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%" />
                    <%--Address--%>
                    <ext:GridPanel ID="GP_Address" runat="server" Title="Alamat" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="store_address" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="modelAddress" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column30" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column5" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="Town" Text="Kecamatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="Zip" Text="Kode Pos" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="Country" Text="Kode Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column21" runat="server" DataIndex="State" Text="Ibu Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cmdAddress" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdAddressKantor">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--End of Address--%>

                    <%--Phone--%>
                    <ext:GridPanel ID="GDPhone" runat="server" Title="Telepon" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="store_phone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="model_phone" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererPhone" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="columnKategoriKontak" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnJenisAlatKomunikasi" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnKodeArea" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnNomorTelepon" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnNomorEkstensi" runat="server" DataIndex="tph_extension" Text="Nomor Ekstensi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnCatatan" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhone" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdPhoneKantor">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--End of Phone--%>

                    <%--Director--%>
                    <%--                    <ext:GridPanel ID="gp_Director" runat="server" Title="Director" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="store_Director" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="model_Director" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Entity_Director_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Role" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumber_Director" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="columnDirectorName" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnDirectorRole" runat="server" DataIndex="Role" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cmdDirectory" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>--%>
                    <%--End of Director--%>
                    <ext:DateField Editable="false" runat="server" LabelWidth="250" FieldLabel="Tanggal Pendirian" ID="txt_Corp_incorporation_date" Format="dd-MMM-yyyy" ReadOnly="true" AnchorHorizontal="90%" FieldStyle="background-color:lightgray" />
                    <ext:Checkbox runat="server" LabelWidth="250" ID="cbTutup" FieldLabel="Tutup?" Checked="true" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%"></ext:Checkbox>
                    <ext:DateField Editable="false" runat="server" LabelWidth="250" FieldLabel="Tanggal Tutup" ID="txt_Corp_date_business_closed" Format="dd-MMM-yyyy" ReadOnly="true" FieldStyle="background-color:lightgray" AnchorHorizontal="90%" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="NPWP" ID="txt_Corp_tax_number" AnchorHorizontal="90%" ReadOnly="true" FieldStyle="background-color:lightgray" />
                    <ext:TextField runat="server" LabelWidth="250" FieldLabel="Catatan" ID="txt_Corp_Comments" ReadOnly="true" AnchorHorizontal="90%" FieldStyle="background-color:lightgray" />
                </Items>
            </ext:Panel>
            <%--end of Entity--%>
            <%--Signatory--%>
            <ext:GridPanel ID="gp_Signatory" runat="server" Title="Signatory" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                <TopBar>
                    <ext:Toolbar ID="tb_Signatory" runat="server">
                        <Items>
                            <ext:Button ID="btnAdd_Signatory" runat="server" Text="Add Signatory" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btnAddSignatory_DirectClick">
                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="Store_Signatory" runat="server" IsPagingStore="true" PageSize="8">
                        <Model>
                            <ext:Model ID="Model_Signatory" runat="server">
                                <Fields>
                                    <ext:ModelField Name="PK_Signatory_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Name" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Role" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Primary" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumber_Signatory" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                        <ext:Column ID="Column1" runat="server" DataIndex="Name" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column2" runat="server" DataIndex="Role" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column3" runat="server" DataIndex="Primary" Text="Primary" MinWidth="130" Flex="1"></ext:Column>
                        <%--<ext:Column ID="Column4" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column5" runat="server" DataIndex="tph_extension" Text="Nomor Extensi" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column6" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>--%>
                        <ext:CommandColumn ID="cmdSignatory" runat="server" Text="Action" Flex="1" MinWidth="200">
                            <Commands>
                                <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                <ext:GridCommand CommandName="Edit" Icon="Pencil" Text="Edit" ToolTip-Text="Edit"></ext:GridCommand>
                                <ext:GridCommand CommandName="Delete" Icon="PencilDelete" Text="Delete" ToolTip-Text="Delete"></ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GrdCmdSignatory">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Signatory_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                    </Confirmation>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
            <%--End of Signatory--%>
        </Items>

        <Buttons>
            <ext:Button ID="btnSubmit" runat="server" Icon="Disk" Text="Submit">
                <DirectEvents>
                    <Click OnEvent="BtnSubmit_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnCancelIncoming" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnBack_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true" BodyStyle="padding:10px">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>

        </Items>

        <Buttons>

            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
