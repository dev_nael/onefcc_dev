﻿Imports System.Data
Imports System.Data.SqlClient
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDAL
Imports NawaDevBLL
Imports NawaDevDAL
Partial Class ACCOUNT_EDIT
    Inherits ParentPage
    Public objFormModuleView As NawaBLL.FormModuleDetail
    Public objgoAML_AccountBLL As New NawaDevBLL.goAML_AccountBLL
    Private mModuleName As String

    Public Property ModulName() As String
        Get
            Return mModuleName
        End Get
        Set(ByVal value As String)
            mModuleName = value
        End Set
    End Property
    Sub ClearSession()
        objListgoAML_Ref_Account_Signatory = New List(Of goAML_Ref_Account_Signatory)
        objListVwAccountSignatory = New List(Of Vw_Account_Signatory)
    End Sub
    '' edited on 04 Dec 2020
    Public Property objSchemaModule() As NawaDAL.Module
        Get
            Return Session("ACCOUNT_EDIT.objSchemaModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("ACCOUNT_EDIT.objSchemaModule") = value
        End Set
    End Property

    Public Property objModuleApproval As NawaDevDAL.ModuleApproval
        Get
            Return Session("ACCOUNT_EDIT.objModuleApproval")
        End Get
        Set(value As NawaDevDAL.ModuleApproval)
            Session("ACCOUNT_EDIT.objModuleApproval") = value
        End Set
    End Property
    '' End of edit on 04 Dec 2020
    Public Property objListVwAccountSignatory() As List(Of NawaDevDAL.Vw_Account_Signatory)
        Get
            Return Session("ACCOUNT_EDIT.objListgoAML_vw_Customer_Phones")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Account_Signatory))
            Session("ACCOUNT_EDIT.objListgoAML_vw_Customer_Phones") = value
        End Set
    End Property

    Public Property objTempVwAccountSignatory() As NawaDevDAL.Vw_Account_Signatory
        Get
            Return Session("ACCOUNT_EDIT.objTempCustomerPhoneEdit")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Account_Signatory)
            Session("ACCOUNT_EDIT.objTempCustomerPhoneEdit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Account_Signatory() As List(Of NawaDevDAL.goAML_Ref_Account_Signatory)
        Get
            Return Session("ACCOUNT_EDIT.objListgoAML_Ref_Account_Signatory")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Ref_Account_Signatory))
            Session("ACCOUNT_EDIT.objListgoAML_Ref_Account_Signatory") = value
        End Set
    End Property

    'try do
    Public Property objListgoAML_Ref_Customer_Director() As List(Of NawaDevDAL.goAML_Ref_Customer_Entity_Director)
        Get
            Return Session("ACCOUNT_EDIT.objListgoAML_Ref_Customer_Director")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Ref_Customer_Entity_Director))
            Session("ACCOUNT_EDIT.objListgoAML_Ref_Customer_Director") = value
        End Set
    End Property

    Public Property objTempgoAML_Ref_Address_Director() As NawaDevDAL.goAML_Ref_Address
        Get
            Return Session("ACCOUNT_EDIT.objTempgoAML_Ref_Address_Director")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Address)
            Session("ACCOUNT_EDIT.objTempgoAML_Ref_Address_Director") = value
        End Set
    End Property
    'end try do

    Public Property objTempgoAML_Ref_Account_Signatory() As NawaDevDAL.goAML_Ref_Account_Signatory
        Get
            Return Session("ACCOUNT_EDIT.objTempgoAML_Ref_Account_Signatory")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Account_Signatory)
            Session("ACCOUNT_EDIT.objTempgoAML_Ref_Account_Signatory") = value
        End Set
    End Property

    Public Property obj_Account As NawaDevDAL.goAML_Ref_Account
        Get
            Return Session("ACCOUNT_EDIT.obj_Account")
        End Get
        Set(value As NawaDevDAL.goAML_Ref_Account)
            Session("ACCOUNT_EDIT.obj_Account") = value
        End Set
    End Property

    Function IsDataValid() As Boolean

        'If txt_AccountName.Text = "" Then
        '    Throw New Exception("Account Name is required")
        'End If

        If txt_CIF.Text = "" Then
            Throw New Exception("Client Number is required")
        End If
        If txt_Branch.Text = "" Then
            Throw New Exception("Branch is required")
        End If
        If txt_Opened.RawValue = "" Then
            Throw New Exception("Tanggal Pembukaan Rekening is required")
        End If
        If Cmb_PersonalAccountType.SelectedItemValue = "" Then
            Throw New Exception("Personal Account Type is required")
        End If
        If Cmb_StatusCode.SelectedItemValue = "" Then
            Throw New Exception("Status Rekenening is required")
        End If
        If objListgoAML_Ref_Account_Signatory.Count = 0 Then
            Throw New Exception("Signatory is required")
        End If

        Dim strMessage As String = "Account Closed Date or Balance Date must greater than or equal Opening Date"
        Dim strMessage2 As String = "Account Closed Date must greater than or equal Balance Date"
        Dim strSection As String = Nothing
        Dim OpeningDate As DateTime
        Dim ClosedDate As DateTime
        Dim BalanceDate As DateTime

        OpeningDate = CDate(txt_Opened.Value)
        ClosedDate = CDate(txt_Closed.Value)
        BalanceDate = CDate(txt_Balance_Date.Value)
        ' 6 Okt 2022 : Ari penambahan kondisi jika closed date nya kosong atau nothing
        If ClosedDate = DateTime.MinValue Then
            If BalanceDate <> DateTime.MinValue AndAlso OpeningDate > BalanceDate Then Throw New Exception(strMessage)
        Else
            If ClosedDate <> DateTime.MinValue AndAlso OpeningDate > ClosedDate Then Throw New Exception(strMessage)
            If BalanceDate <> DateTime.MinValue AndAlso OpeningDate > BalanceDate Then Throw New Exception(strMessage)
            If BalanceDate <> DateTime.MinValue AndAlso BalanceDate > ClosedDate Then Throw New Exception(strMessage2)
        End If


        Dim CIF As String = txt_CIF.Value
        ' 26 Dec 2022 : Client Number Boleh Karakter tidak digit semua
        'If IsNumeric(CIF) Then
        'Else
        '    Throw New Exception(txt_CIF.FieldLabel & " must a digit numeric only.")
        'End If

        Dim AccountNo As String = txt_AccountNo.Value
        If AccountNo IsNot Nothing Then
            If IsNumeric(AccountNo) Then
            Else
                Throw New Exception(txt_AccountNo.FieldLabel & " must a digit numeric only.")
            End If
        End If


        If cb_EntityAccount.Checked = True Then
            If cmb_EntityAccount.SelectedItemValue = "" Then
                Throw New Exception("Customer Korporasi is required")
            End If
        End If
        Return True
    End Function

    Function IfHaveChanges() As Boolean
        Dim IsSame As Boolean = True
        Dim oldobj_Account = goAML_AccountBLL.GetAccountByAccountNo(obj_Account.Account_No)
        Dim oldobjListgoAML_Ref_Account_Signatory = NawaDevBLL.goAML_AccountBLL.GetAccountSignatory(obj_Account.Account_No)
        If obj_Account.Account_Name <> oldobj_Account.Account_Name Then
            IsSame = False
        End If
        If obj_Account.Currency_Code <> oldobj_Account.Currency_Code Then
            IsSame = False
        End If
        If obj_Account.IBAN <> oldobj_Account.IBAN Then
            IsSame = False
        End If
        If obj_Account.client_number <> oldobj_Account.client_number Then
            IsSame = False
        End If
        If obj_Account.personal_account_type <> oldobj_Account.personal_account_type Then
            IsSame = False
        End If
        If obj_Account.opened.ToString <> oldobj_Account.opened.ToString Then
            IsSame = False
        End If
        If obj_Account.closed.ToString <> oldobj_Account.closed.ToString Then
            IsSame = False
        End If
        If obj_Account.balance.ToString <> oldobj_Account.balance.ToString Then
            IsSame = False
        End If
        If obj_Account.date_balance.ToString <> oldobj_Account.date_balance.ToString Then
            IsSame = False
        End If
        If obj_Account.beneficiary <> oldobj_Account.beneficiary Then
            IsSame = False
        End If
        If obj_Account.status_code <> oldobj_Account.status_code Then
            IsSame = False
        End If
        If obj_Account.beneficiary_comment <> oldobj_Account.beneficiary_comment Then
            IsSame = False
        End If
        If obj_Account.comments <> oldobj_Account.comments Then
            IsSame = False
        End If
        If obj_Account.isUpdateFromDataSource <> oldobj_Account.isUpdateFromDataSource Then
            IsSame = False
        End If
        If obj_Account.FK_CIF_Entity_ID <> oldobj_Account.FK_CIF_Entity_ID Then
            IsSame = False
        End If
        If obj_Account.Branch <> oldobj_Account.Branch Then
            IsSame = False
        End If
        If obj_Account.Account_No <> oldobj_Account.Account_No Then
            IsSame = False
        End If

        For Each item As goAML_Ref_Account_Signatory In objListgoAML_Ref_Account_Signatory

            Dim objsignatory = oldobjListgoAML_Ref_Account_Signatory.Where(Function(x) x.PK_Signatory_ID = item.PK_Signatory_ID).FirstOrDefault

            If objsignatory IsNot Nothing Then
                If item.FK_Account_No <> objsignatory.FK_Account_No Then
                    IsSame = False
                End If
                If item.Role <> objsignatory.Role Then
                    IsSame = False
                End If
                If item.isPrimary <> objsignatory.isPrimary Then
                    IsSame = False
                End If
                If item.FK_CIF_Person_ID <> objsignatory.FK_CIF_Person_ID Then
                    IsSame = False
                End If
                If item.WIC_No <> objsignatory.WIC_No Then
                    IsSame = False
                End If
            End If
        Next

        '---- New Code Defect ID 137----
        '---- if there are objsignatory deleted ----
        If objListgoAML_Ref_Account_Signatory.Count < oldobjListgoAML_Ref_Account_Signatory.Count Then
            IsSame = False
        End If

        '---- if there are objsignatory added  ----
        For Each item As goAML_Ref_Account_Signatory In objListgoAML_Ref_Account_Signatory
            If item.PK_Signatory_ID < 0 Then
                IsSame = False

                item.FK_Account_No = obj_Account.Account_No
                item.CreatedBy = Common.SessionCurrentUser.UserID
                item.CreatedDate = Date.Now
                item.LastUpdateBy = Common.SessionCurrentUser.UserID
                item.LastUpdateDate = Date.Now
                item.ApprovedBy = Common.SessionCurrentUser.UserID
                item.ApprovedDate = Date.Now
            End If
        Next
        '---- New Code Defect ID 137----

        If IsSame Then
            Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
        End If
        Return True
    End Function

    Protected Sub BtnSubmit_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataValid() Then
                With obj_Account
                    .Branch = txt_Branch.Text
                    .Account_No = txt_AccountNo.Text
                    .Currency_Code = txt_CurrencyCode.Text
                    .Account_Name = txt_AccountName.Text
                    .IBAN = txt_IBAN.Text
                    .client_number = txt_CIF.Text
                    .personal_account_type = Cmb_PersonalAccountType.SelectedItemValue
                    .opened = txt_Opened.Text
                    If txt_Closed.RawValue IsNot Nothing Then
                        .closed = Convert.ToDateTime(txt_Closed.RawValue)
                    Else
                        .closed = Nothing
                    End If
                    Dim IntBalance As Decimal
                    If Not Decimal.TryParse(txt_Balance.Text, IntBalance) Then
                        .balance = Nothing
                    Else
                        .balance = txt_Balance.Text
                    End If
                    If txt_Balance_Date.RawValue IsNot Nothing Then
                        .date_balance = Convert.ToDateTime(txt_Balance_Date.RawValue)
                    Else
                        .date_balance = Nothing
                    End If
                    .status_code = Cmb_StatusCode.SelectedItemValue
                    .beneficiary = txt_beneficiary.Text
                    .beneficiary_comment = txt_beneficiary_comment.Text
                    .comments = txt_comments.Text
                    .isUpdateFromDataSource = Convert.ToBoolean(IIf(cb_isUpdateFromDataSouce.Checked = True, 1, 0))


                    If cb_EntityAccount.Checked = True Then
                        .FK_CIF_Entity_ID = cmb_EntityAccount.SelectedItemValue
                    Else
                        .FK_CIF_Entity_ID = Nothing
                    End If

                    .Active = True
                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .CreatedDate = DateTime.Now
                    .LastUpdateDate = DateTime.Now
                End With
                IfHaveChanges()
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                    objgoAML_AccountBLL.SaveEditTanpaApproval(obj_Account, ObjModule, objListgoAML_Ref_Account_Signatory)
                    Panelconfirmation.Hidden = False
                    FormPanelInput.Hidden = True
                    LblConfirmation.Text = "Data Saved into Database"
                Else
                    objgoAML_AccountBLL.SaveEditApproval(obj_Account, ObjModule, objListgoAML_Ref_Account_Signatory)
                    Panelconfirmation.Hidden = False
                    FormPanelInput.Hidden = True
                    LblConfirmation.Text = "Data Saved into Pending Approval"
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnBack_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Private Function GetId() As String
        Dim strid As String = Request.Params("ID")
        Dim id As String = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
        Return id
    End Function

    Private Sub LoadSignatoryStoreData()
        'cmb_SignatoryPerson.PageSize = SystemParameterBLL.GetPageSize
        'store_SignatoryPerson.Reload()
        'cmb_SignatoryRole.PageSize = SystemParameterBLL.GetPageSize
        'store_SignatoryRole.Reload()
    End Sub
    Protected Sub SignatoryRole_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Peran_orang_dalam_rekening", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub btnAddSignatory_DirectClick(sender As Object, e As DirectEventArgs)


        Try
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            btnSavedetail.Hidden = False
            cb_isCustomer.Checked = True


            cbIsPrimary.ReadOnly = False
            cb_isCustomer.ReadOnly = False
            cmb_SignatoryRole.IsReadOnly = False
            cmb_SignatoryPerson.IsReadOnly = False
            cmb_Not_SignatoryPerson.IsReadOnly = False
            btnSavedetail.Hidden = False
            WindowDetail.Title = "Account Signatory Add"

            'Dedy Remarks 27082020
            'Dim objDt As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,INDV_Last_Name,cb_value from Vw_CB_Signatory_Person", Nothing)
            'Using db As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            '    store_SignatoryPerson.DataSource = objDt
            '    store_SignatoryPerson.DataBind()

            '    store_SignatoryRole.DataSource = goAML_AccountBLL.GetListPeranOrangDalamRekening()
            '    store_SignatoryRole.DataBind()
            'End Using
            'Dedy End Remarks 27082020

            'dedy added 27082020 for validation pilih customer individu

            'Load StoreData
            LoadSignatoryStoreData()

            'load bind data
            'If cb_isCustomer.Checked Then
            'cmb_SignatoryPerson.Value = "Select One"
            'Dim objDt As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,INDV_Last_Name,cb_value from Vw_CB_Signatory_Person where Active='1'", Nothing)
            'Using db As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            'store_SignatoryPerson.DataSource = objDt
            'store_SignatoryPerson.DataBind()

            'store_SignatoryRole.DataSource = goAML_AccountBLL.GetListPeranOrangDalamRekening()
            'store_SignatoryRole.DataBind()
            'End Using
            'Else
            'cmb_SignatoryPerson.Value = "Select One"
            'Dim objDt2 As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,INDV_Last_Name,cb_value from Vw_CB_Signatory_Not_Person where Active='1'", Nothing)
            'Using db As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            'store_SignatoryPerson.DataSource = objDt2
            'store_SignatoryPerson.DataBind()

            'store_SignatoryRole.DataSource = goAML_AccountBLL.GetListPeranOrangDalamRekening()
            'store_SignatoryRole.DataBind()
            'End Using
            'End If
            'dedy end added 27082020

            ClearinputAccount()

            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub SignatoryPerson_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            'Dim obj_Account_Entity As NawaDevDAL.goAML_Ref_Customer = Nothing

            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " cb_value  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            'objstore.DataSource = SQLHelper.ExecuteTabelPaging("Vw_CB_Signatory_Person", "CIF, cb_value", strfilter, "cb_value", e.Start, e.Limit, e.Total)
            'objstore.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,cb_value from Vw_CB_Signatory_Person", Nothing)
            'objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub


    Protected Sub btnBackSaveAccountSignatory_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputAccount()
            objTempgoAML_Ref_Account_Signatory = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveAccountAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address.Hidden = True
            WindowDetailAddress.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveAccountPhone_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Phone.Hidden = True
            WindowDetailPhone.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackPhoneandAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_Director.Hidden = True
            FP_Phone_Director.Hidden = True
            WindowAddressandPhoneDirector.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnSaveAccountSignatory_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            If IsDataAccountSignatoryValid() Then
                'If objTempCustomerPhoneEdit Is Nothing Then
                If objTempgoAML_Ref_Account_Signatory Is Nothing Then
                    SaveAddTaskDetail()
                Else
                    SaveEditTaskDetail()
                End If
            End If
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputAccount()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SaveEditTaskDetail()

        With objTempgoAML_Ref_Account_Signatory
            If cb_isCustomer.Checked = True Then
                .FK_CIF_Person_ID = cmb_SignatoryPerson.SelectedItemValue
            Else
                .FK_CIF_Person_ID = cmb_Not_SignatoryPerson.SelectedItemValue
            End If
            .Role = cmb_SignatoryRole.SelectedItemValue
            .isPrimary = Convert.ToBoolean(IIf(cbIsPrimary.Checked = True, 1, 0))
            'Update: Zikri_17092020 
            .isCustomer = Convert.ToBoolean(IIf(cb_isCustomer.Checked = True, 1, 0))
            If cb_isCustomer.Checked = True Then
                .FK_CIF_Person_ID = cmb_SignatoryPerson.SelectedItemValue
                .WIC_No = Nothing
            Else
                .WIC_No = cmb_Not_SignatoryPerson.SelectedItemValue
                .FK_CIF_Person_ID = Nothing
            End If
            'End Update-
        End With

        With objTempVwAccountSignatory
            If cb_isCustomer.Checked = True Then
                .CIF = cmb_SignatoryPerson.SelectedItemValue
            Else
                .CIF = cmb_Not_SignatoryPerson.SelectedItemValue
            End If
            .Name = txt_INDV_Last_Name.Text
            .Primary = IIf(cbIsPrimary.Checked = True, "Y", "N")
            .Role = cmb_SignatoryRole.SelectedItemText
        End With


        'objListgoAML_Ref_Phone.Add(objTempgoAML_Ref_Phone)

        objTempVwAccountSignatory = Nothing
        objTempgoAML_Ref_Account_Signatory = Nothing

        Store_Signatory.DataSource = objListVwAccountSignatory.ToList()
        Store_Signatory.DataBind()

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputAccount()
    End Sub

    Sub SaveAddTaskDetail()

        Dim objNewVWgoAML_AccountSignatory As New NawaDevDAL.Vw_Account_Signatory
        Dim objNewgoAML_AccountSignatory As New NawaDevDAL.goAML_Ref_Account_Signatory
        Dim objRand As New Random

        Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
        While Not objListgoAML_Ref_Account_Signatory.Find(Function(x) x.PK_Signatory_ID = intpk) Is Nothing
            intpk = objRand.Next
        End While

        With objNewVWgoAML_AccountSignatory
            .PK_Signatory_ID = intpk
            .AccountNo = txt_AccountNo.Text
            If cb_isCustomer.Checked = True Then
                .CIF = cmb_SignatoryPerson.SelectedItemValue
            Else
                .CIF = cmb_Not_SignatoryPerson.SelectedItemValue
            End If
            .Name = txt_INDV_Last_Name.Text
            .Primary = IIf(cbIsPrimary.Checked = True, "Y", "N")
            .Role = cmb_SignatoryRole.SelectedItemText
        End With

        With objNewgoAML_AccountSignatory
            .PK_Signatory_ID = intpk
            .FK_Account_No = obj_Account.Account_No
            .Role = cmb_SignatoryRole.SelectedItemValue
            'Update: Zikri_17092020 
            .isPrimary = Convert.ToBoolean(IIf(cbIsPrimary.Checked = True, 1, 0))
            .isCustomer = Convert.ToBoolean(IIf(cb_isCustomer.Checked = True, 1, 0))
            If cb_isCustomer.Checked = True Then
                .FK_CIF_Person_ID = cmb_SignatoryPerson.SelectedItemValue
            Else
                .WIC_No = cmb_Not_SignatoryPerson.SelectedItemValue
            End If
            'End Update-


        End With

        objListVwAccountSignatory.Add(objNewVWgoAML_AccountSignatory)
        objListgoAML_Ref_Account_Signatory.Add(objNewgoAML_AccountSignatory)
        Store_Signatory.DataSource = objListVwAccountSignatory.ToList()
        Store_Signatory.DataBind()

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputAccount()
    End Sub

    Function IsDataAccountSignatoryValid() As Boolean
        If cb_isCustomer.Checked = True Then
            If cmb_SignatoryPerson.SelectedItemValue = "" Then
                Throw New Exception("Person is required")
            End If
        Else
            If cmb_Not_SignatoryPerson.SelectedItemValue = "" Then
                Throw New Exception("Person is required")
            End If
        End If
        If cmb_SignatoryRole.SelectedItemValue = "" Then
            Throw New Exception("Role Type is required")
        End If
        Return True
    End Function

    Sub ClearinputAccount()
        cmb_SignatoryPerson.SetTextValue("")
        cmb_Not_SignatoryPerson.SetTextValue("")
        cbIsPrimary.Checked = Nothing
        cmb_SignatoryRole.SetTextValue("")
        txt_INDV_Title.Text = ""
        txt_INDV_Last_Name.Text = ""
        txt_INDV_Birthdate.Text = ""
        txt_INDV_Birth_place.Text = ""
        txt_INDV_Mothers_name.Text = ""
        txt_INDV_Alias.Text = ""
        txt_INDV_SSN.Text = ""
        txt_INDV_Passport_number.Text = ""
        txt_INDV_Passport_country.Text = ""
        txt_INDV_ID_Number.Text = ""
        txt_INDV_Nationality1.Text = ""
        txt_INDV_Nationality2.Text = ""
        txt_INDV_Nationality3.Text = ""
        txt_INDV_Residence.Text = ""
        txt_INDV_Email.Text = ""
        txt_INDV_Occupation.Text = ""
        txt_INDV_Tax_NumberSignatory.Text = ""
        txt_INDV_Source_of_WealthSignatory.Text = ""

        cb_DeceasedSignatory.Checked = False
        txt_deceased_dateSignatory.Text = ""

        cb_taxSignatory.Checked = False
        StoreAddressSignatory.DataBind()
        StorePhoneSignatory.DataBind()
        StoreEmployerPhoneSignatory.DataBind()
        StoreEmployerAddressSignatory.DataBind()
    End Sub

    Sub ClearEntityAccount()
        cmb_EntityAccount.SetTextValue("")
        txt_Corp_Name.Text = ""
        txt_Corp_Commercial_Name.Text = ""
        txt_Corp_Legal_Form.Text = ""
        txt_Corp_Incorporation_number.Text = ""
        txt_Corp_Business.Text = ""
        txt_Corp_Email.Text = ""
        txt_Corp_url.Text = ""
        txt_Corp_incorporation_country_code.Text = ""
        txt_Corp_incorporation_state.Text = ""
        txt_Corp_incorporation_date.Text = ""
        cbTutup.Checked = False
        txt_Corp_date_business_closed.Text = ""
        txt_Corp_tax_number.Text = ""
        txt_Corp_Comments.Text = ""

        'Try do

        'store_Director.DataBind()
        store_phone.DataBind()
        store_address.DataBind()

        'End Try do

    End Sub

    Private Sub LoadStoreData()
        'Cmb_PersonalAccountType.PageSize = SystemParameterBLL.GetPageSize
        'StorePersonalAccountType.Reload()
        'Cmb_StatusCode.PageSize = SystemParameterBLL.GetPageSize
        'StoreStatusCode.Reload()
        'cmb_EntityAccount.PageSize = SystemParameterBLL.GetPageSize
        'store_EntityAccount.Reload()
    End Sub

    Protected Sub EntityAccount_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            'Dim obj_Account_Entity As NawaDevDAL.goAML_Ref_Customer = Nothing

            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " cb_value  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("VW_EntityAccount", "CIF, cb_value", strfilter, "cb_value", e.Start, e.Limit, e.Total)
            'objstore.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,Corp_Name,cb_value from VW_EntityAccount", Nothing)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub StatusCode_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_status_Rekening", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub PersonalAccountType_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Rekening", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub ACCOUNT_EDIT_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then

                ClearSession()
                GridPanelSetting()
                Maximizeable()


                'load refrences
                LoadStoreData()

                'bind data edit

                Dim strid As String = GetId()
                Dim strModuleid As String = Request.Params("ModuleID")
                Dim PersonalAccountType As NawaDevDAL.goAML_Ref_Jenis_Rekening = Nothing
                Dim INDV_statusCode As NawaDevDAL.goAML_Ref_Status_Rekening = Nothing
                Dim INDV_nationality1 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
                Dim INDV_nationality2 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
                Dim INDV_nationality3 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
                Dim INDV_residence As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing


                Dim obj_Account_Entity As NawaDevDAL.goAML_Ref_Customer = Nothing

                Try
                    obj_Account = goAML_AccountBLL.GetAccountByAccountNo(strid)
                    If obj_Account IsNot Nothing Then
                        PK_Customer_ID.Value = obj_Account.PK_Account_ID

                        txt_AccountNo.Text = obj_Account.Account_No
                        txt_CIF.Text = obj_Account.client_number
                        txt_CurrencyCode.Text = obj_Account.Currency_Code
                        txt_Branch.Text = obj_Account.Branch
                        txt_AccountName.Text = obj_Account.Account_Name
                        txt_IBAN.Text = obj_Account.IBAN

                        'StorePersonalAccountType.DataSource = goAML_AccountBLL.GetListJenisRekening()
                        'StorePersonalAccountType.DataBind()

                        '---- Old Code ----
                        'PersonalAccountType = goAML_AccountBLL.GetJenisRekeningByID(obj_Account.personal_account_type)
                        'Cmb_PersonalAccountType.SetValue(PersonalAccountType.Kode)
                        '---- Old Code ----

                        'Dedy added is not nothing 01092020
                        PersonalAccountType = goAML_AccountBLL.GetJenisRekeningByID(obj_Account.personal_account_type)
                        If PersonalAccountType IsNot Nothing Then
                            Cmb_PersonalAccountType.SetTextWithTextValue(PersonalAccountType.Kode, PersonalAccountType.Keterangan)
                        End If

                        If obj_Account.opened.HasValue Then
                            txt_Opened.Text = obj_Account.opened.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            txt_Opened.Text = ""
                        End If

                        If obj_Account.closed.HasValue Then
                            txt_Closed.Text = obj_Account.closed.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            txt_Closed.Text = ""
                        End If

                        txt_Balance.Value = obj_Account.balance

                        If obj_Account.date_balance.HasValue Then
                            txt_Balance_Date.Text = obj_Account.date_balance.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            txt_Balance_Date.Text = ""
                        End If

                        'StoreStatusCode.DataSource = goAML_CustomerBLL.GetListStatusRekening()
                        'StoreStatusCode.DataBind()


                        '---- Old Code ----
                        'If INDV_statusCode IsNot Nothing Then
                        '    Cmb_StatusCode.SetValue(INDV_statusCode.Kode)
                        'End If
                        '---- Old Code ----
                        '---- Old Code ----

                        'Dedy added is not nothing 01092020
                        INDV_statusCode = goAML_CustomerBLL.GetStatusRekeningbyID(obj_Account.status_code)
                        If INDV_statusCode IsNot Nothing Then
                            Cmb_StatusCode.SetTextWithTextValue(INDV_statusCode.Kode, INDV_statusCode.Keterangan)
                        End If

                        txt_beneficiary.Text = obj_Account.beneficiary
                        txt_beneficiary_comment.Text = obj_Account.beneficiary_comment
                        txt_comments.Text = obj_Account.comments

                        'cb_isUpdateFromDataSouce.Checked = IIf(obj_Account.isUpdateFromDataSource = 1, True, False)

                        If obj_Account.isUpdateFromDataSource = True Then
                            cb_isUpdateFromDataSouce.Checked = True
                        ElseIf obj_Account.isUpdateFromDataSource Is Nothing Then
                            cb_isUpdateFromDataSouce.Checked = False
                        Else
                            cb_isUpdateFromDataSouce.Checked = False
                        End If

                        Dim objDtEntityAccount As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,Corp_Name,cb_value from Vw_CB_Account_Entity", Nothing)

                        'store_EntityAccount.DataSource = objDtEntityAccount
                        'store_EntityAccount.DataBind()

                        If Not String.IsNullOrEmpty(obj_Account.FK_CIF_Entity_ID) Then
                            obj_Account_Entity = goAML_AccountBLL.GetCustomerByCIFandCustomerType(obj_Account.FK_CIF_Entity_ID)

                            If obj_Account_Entity IsNot Nothing Then
                                cb_EntityAccount.Checked = True
                                cb_EntityAccount.ReadOnly = True
                                panel_Corp.Hidden = False

                                'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                                cmb_EntityAccount.SetTextWithTextValue(obj_Account_Entity.CIF, obj_Account_Entity.CIF + " - " + obj_Account_Entity.Corp_Name)

                                'End Update

                                'store_EntityAccount.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,Corp_Name,cb_value from Vw_CB_Account_Entity", Nothing)
                                'store_EntityAccount.DataBind()

                                'Dedy added for binding data
                                'cmb_EntityAccount.DoQuery(obj_Account.FK_CIF_Entity_ID, True)
                                'cmb_EntityAccount.SetValueAndFireSelect(obj_Account.FK_CIF_Entity_ID)

                                txt_Corp_Name.Text = obj_Account_Entity.Corp_Name
                                txt_Corp_Commercial_Name.Text = obj_Account_Entity.Corp_Commercial_Name
                                If Not obj_Account_Entity.Corp_Incorporation_Legal_Form = "" Then
                                    txt_Corp_Legal_Form.Text = goAML_CustomerBLL.GetBentukBadanUsahaByID(obj_Account_Entity.Corp_Incorporation_Legal_Form).Keterangan
                                End If
                                txt_Corp_Incorporation_number.Text = obj_Account_Entity.Corp_Incorporation_Number
                                txt_Corp_Business.Text = obj_Account_Entity.Corp_Business
                                txt_Corp_Email.Text = obj_Account_Entity.Corp_Email
                                txt_Corp_url.Text = obj_Account_Entity.Corp_Url
                                txt_Corp_incorporation_state.Text = obj_Account_Entity.Corp_Incorporation_State
                                If Not obj_Account_Entity.Corp_Incorporation_Country_Code = "" Then
                                    txt_Corp_incorporation_country_code.Text = goAML_CustomerBLL.GetNamaNegaraByID(obj_Account_Entity.Corp_Incorporation_Country_Code).Keterangan
                                End If

                                ''To do : Director
                                'store_Director.DataSource = db.Vw_Customer_Director.Where(Function(x) x.fk_entity_id = obj_Account_Entity.CIF).ToList()
                                'store_Director.DataBind()

                                store_phone.DataSource = goAML_AccountBLL.GetVw_Customer_Phones(obj_Account_Entity.PK_Customer_ID)
                                store_phone.DataBind()

                                store_address.DataSource = goAML_AccountBLL.GetVw_Customer_Addresses(obj_Account_Entity.PK_Customer_ID)
                                store_address.DataBind()


                                If obj_Account.date_balance.HasValue Then
                                    txt_Corp_incorporation_date.Text = obj_Account_Entity.Corp_Incorporation_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                                Else
                                    txt_Corp_incorporation_date.Text = ""
                                End If

                                If obj_Account_Entity.Corp_Business_Closed IsNot Nothing Then
                                    cbTutup.Checked = Not String.IsNullOrEmpty(obj_Account_Entity.Corp_Business_Closed)
                                Else
                                    cbTutup.Checked = False
                                End If

                                If obj_Account_Entity.Corp_Date_Business_Closed.HasValue Then
                                    txt_Corp_date_business_closed.Text = obj_Account_Entity.Corp_Incorporation_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                                Else
                                    txt_Corp_date_business_closed.Text = ""
                                End If

                                txt_Corp_tax_number.Text = obj_Account_Entity.Corp_Tax_Number
                                txt_Corp_Comments.Text = obj_Account_Entity.Corp_Comments
                            Else
                                cb_EntityAccount.Checked = False
                                panel_Corp.Hidden = True
                            End If
                        Else
                            cb_EntityAccount.Checked = False
                            cb_EntityAccount.Hidden = True
                            panel_Corp.Hidden = True
                        End If

                        '' Signatory
                        objListVwAccountSignatory = NawaDevBLL.goAML_AccountBLL.GetVw_Account_Signatory(obj_Account.Account_No)
                        objListgoAML_Ref_Account_Signatory = NawaDevBLL.goAML_AccountBLL.GetAccountSignatory(obj_Account.Account_No)
                        Store_Signatory.DataSource = objListVwAccountSignatory.ToList()
                        Store_Signatory.DataBind()

                        'panel_Corp.Visible = False
                        'panel_INDV.Visible = False
                    End If
                    '' edited on 04 Dec 2020
                    Dim ID As String = Request.Params("ID")
                    Dim moduleid As Integer = NawaBLL.Common.DecryptQueryString(strModuleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                    objSchemaModule = NawaBLL.ModuleBLL.GetModuleByModuleID(moduleid)
                    Dim strUnikKey As String = NawaBLL.Common.DecryptQueryString(ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                    Dim pk As String = goAML_AccountBLL.getPKAccount(strUnikKey)
                    objModuleApproval = NawaDevBLL.goAML_AccountBLL.getDataApproval(pk, objSchemaModule.ModuleName)
                    If objModuleApproval IsNot Nothing Then
                        btnSubmit.Hidden = True
                        Throw New ApplicationException("This data is already in approval.")
                    End If
                    '' End of edit on 04 Dec 2020
                Catch ex As Exception
                    Throw ex
                End Try
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub GridPanelSetting()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 2 Then
            GP_Address.ColumnModel.Columns.RemoveAt(GP_Address.ColumnModel.Columns.Count - 1)
            GP_Address.ColumnModel.Columns.Insert(1, cmdAddress)
            gp_Signatory.ColumnModel.Columns.RemoveAt(gp_Signatory.ColumnModel.Columns.Count - 1)
            gp_Signatory.ColumnModel.Columns.Insert(1, cmdSignatory)
            GP_AddressSignatory.ColumnModel.Columns.RemoveAt(GP_AddressSignatory.ColumnModel.Columns.Count - 1)
            GP_AddressSignatory.ColumnModel.Columns.Insert(1, CommandColumn1)
            GP_PhoneSignatory.ColumnModel.Columns.RemoveAt(GP_PhoneSignatory.ColumnModel.Columns.Count - 1)
            GP_PhoneSignatory.ColumnModel.Columns.Insert(1, CommandColumn4)
            GDPhone.ColumnModel.Columns.RemoveAt(GDPhone.ColumnModel.Columns.Count - 1)
            GDPhone.ColumnModel.Columns.Insert(1, CommandColumnPhone)

            GP_EmployerAddressSignatory.ColumnModel.Columns.RemoveAt(GP_EmployerAddressSignatory.ColumnModel.Columns.Count - 1)
            GP_EmployerAddressSignatory.ColumnModel.Columns.Insert(1, CommandColumn2)
            GP_EmployerPhoneSignatory.ColumnModel.Columns.RemoveAt(GP_EmployerPhoneSignatory.ColumnModel.Columns.Count - 1)
            GP_EmployerPhoneSignatory.ColumnModel.Columns.Insert(1, CommandColumn3)
        End If

    End Sub

    Sub Maximizeable()
        WindowAddressandPhoneDirector.Maximizable = True
        WindowDetailPhone.Maximizable = True
        WindowDetailAddress.Maximizable = True
        WindowDetailAddress.Maximizable = True
        WindowDetail.Maximizable = True
    End Sub


    Protected Sub GrdCmdSignatory(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAccountSignatory(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAccountSignatory(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetailAccountSignatory(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub GrdCmdAddressKantor(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddressKantor(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailAddressKantor(id As Long)
        Try
            FP_Address.Hidden = False

            Dim Address_Kantor = goAML_AccountBLL.GetVw_Customer_AddressesbyPK(id)
            JenisAlamat.Text = Address_Kantor.Type_Address_Detail
            Alamat.Text = Address_Kantor.Address
            Kecamatan.Text = Address_Kantor.Town
            Kota.Text = Address_Kantor.City
            Kode_Pos.Text = Address_Kantor.Zip
            Kode_Negara.Text = Address_Kantor.Country
            Ibu_Kota.Text = Address_Kantor.State
            Catatan.Text = Address_Kantor.Comments

            WindowDetailAddress.Hidden = False
            btnSavedetail.Hidden = False
            WindowDetailAddress.Title = "Address Detail"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdPhoneKantor(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhoneKantor(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailPhoneKantor(id As Long)
        Try
            FP_Phone.Hidden = False

            Dim Phone_Kantor = goAML_AccountBLL.GetPhonebyPK(id)
            Dim Contact_Type = goAML_CustomerBLL.GetContactTypeByID(Phone_Kantor.Tph_Contact_Type)
            If Contact_Type IsNot Nothing Then
                KategoriKontak.Text = Contact_Type.Keterangan
            Else
                KategoriKontak.Text = ""
            End If
            Dim ComunicationType = goAML_CustomerBLL.GetCommunicationTypeByID(Phone_Kantor.Tph_Communication_Type)
            If ComunicationType IsNot Nothing Then
                JenisAlatKomunikasi.Text = ComunicationType.Keterangan
            Else
                JenisAlatKomunikasi.Text = ""
            End If
            KodeAreaTelp.Text = Phone_Kantor.tph_country_prefix
            NomorTelepon.Text = Phone_Kantor.tph_number
            NomorEkstensi.Text = Phone_Kantor.tph_extension
            Catatan_Phone.Text = Phone_Kantor.comments

            WindowDetailPhone.Hidden = False
            btnSavedetail.Hidden = False
            WindowDetailPhone.Title = "Phone Detail"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdAddressDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddressDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailAddressDirector(id As Long)
        Try
            WindowAddressandPhoneDirector.Hidden = False
            FP_Address_Director.Hidden = False
            WindowAddressandPhoneDirector.Title = "Address Detail"

            ' dibantu CIMB 21 Oct 2020
            'Dim Address_Kantor = goAML_AccountBLL.GetVw_Customer_AddressesbyPK(id)
            'JenisAlamatDirector.Text = Address_Kantor.Type_Address_Detail
            'AlamatDirector.Text = Address_Kantor.Address
            'KecamatanDirector.Text = Address_Kantor.Town
            'KotaDirector.Text = Address_Kantor.City
            'Kode_PosDirector.Text = Address_Kantor.Zip
            'Kode_NegaraDirector.Text = Address_Kantor.Country
            'Ibu_KotaDirector.Text = Address_Kantor.State
            'CatatanAddressDirector.Text = Address_Kantor.Comments

            Dim Address_Kantor = goAML_AccountBLL.GetAddressbyPK(id)
            Dim type_contact = goAML_CustomerBLL.GetContactTypeByID(Address_Kantor.Address_Type)
            Dim country_code = goAML_CustomerBLL.GetNamaNegaraByID(Address_Kantor.Country_Code)

            If type_contact IsNot Nothing Then
                JenisAlamatDirector.Text = type_contact.Keterangan
            Else
                JenisAlamatDirector.Text = ""
            End If

            If country_code IsNot Nothing Then
                Kode_NegaraDirector.Text = country_code.Keterangan
            Else
                Kode_NegaraDirector.Text = ""
            End If

            AlamatDirector.Text = Address_Kantor.Address
            KecamatanDirector.Text = Address_Kantor.Town
            KotaDirector.Text = Address_Kantor.City
            Kode_PosDirector.Text = Address_Kantor.Zip
            Ibu_KotaDirector.Text = Address_Kantor.State
            CatatanAddressDirector.Text = Address_Kantor.Comments
            ' dibantu CIMB 21 Oct 2020

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdPhoneDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhoneDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailPhoneDirector(id As Long)
        Try
            WindowAddressandPhoneDirector.Hidden = False
            FP_Phone_Director.Hidden = False
            WindowAddressandPhoneDirector.Title = "Phone Detail"

            ' dibantu CIMB 21 Oct 2020
            'Dim Phone_Kantor = goAML_AccountBLL.GetVw_Customer_PhonebyPK(id)
            'KategoriKontakDirector.Text = Phone_Kantor.Contact_Type
            'JenisAlatKomunikasiDirector.Text = Phone_Kantor.Communcation_Type
            'KodeAreaTelpDirector.Text = Phone_Kantor.tph_country_prefix
            'NomorTeleponDirector.Text = Phone_Kantor.tph_number
            'NomorEkstensiDirector.Text = Phone_Kantor.tph_extension
            'Catatan_PhoneDirector.Text = Phone_Kantor.comments

            Dim Phone_Kantor = goAML_AccountBLL.GetPhonebyPK(id)
            Dim type_contact = goAML_CustomerBLL.GetContactTypeByID(Phone_Kantor.Tph_Contact_Type)
            Dim type_device = goAML_CustomerBLL.GetCommunicationTypeByID(Phone_Kantor.Tph_Communication_Type)
            KategoriKontakDirector.Text = type_contact.Keterangan
            JenisAlatKomunikasiDirector.Text = type_device.Keterangan
            KodeAreaTelpDirector.Text = Phone_Kantor.tph_country_prefix
            NomorTeleponDirector.Text = Phone_Kantor.tph_number
            NomorEkstensiDirector.Text = Phone_Kantor.tph_extension
            Catatan_PhoneDirector.Text = Phone_Kantor.comments
            ' dibantu CIMB 21 Oct 2020

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'try do 
    Sub LoadDataDetailAccountSignatory(id As Long)
        'Dim objListgoAML_Ref_Account_Signatory = goAML_AccountBLL.GetAccount_Signatory
        'Dim objListgoAML_Ref_Account_Signatory = NawaDevBLL.goAML_AccountBLL.GetAccountSignatory(obj_Account.Account_No)
        Dim objAccountSignatoryDetail As goAML_Ref_Account_Signatory = objListgoAML_Ref_Account_Signatory.Find(Function(x) x.PK_Signatory_ID = id)
        Dim objAccountSignatoryDetailCustomer As goAML_Ref_Customer

        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing
        Dim SignatoryRole As NawaDevDAL.goAML_Ref_Peran_orang_dalam_rekening = Nothing
        Dim signatoryperson As NawaDevDAL.goAML_Ref_Customer = Nothing
        Dim SignatoryNotPerson As NawaDevDAL.goAML_Ref_WIC = Nothing

        'Dedy added 27082020
        Dim objAccountSignatoryDetailWic As goAML_Ref_WIC
        objAccountSignatoryDetailWic = goAML_AccountBLL.GetWicByCIF(objAccountSignatoryDetail.WIC_No)
        cb_isCustomer.ReadOnly = True
        'Dedy End added 27082020
        cmb_SignatoryPerson.IsReadOnly = True
        cmb_Not_SignatoryPerson.IsReadOnly = True
        objAccountSignatoryDetailCustomer = goAML_AccountBLL.GetCustomerByCIF(objAccountSignatoryDetail.FK_CIF_Person_ID)

        If Not objAccountSignatoryDetail Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            If cb_isCustomer.Checked = True Then
                cmb_Not_SignatoryPerson.IsHidden = True
            Else
                cmb_SignatoryPerson.IsHidden = True

            End If



            cbIsPrimary.ReadOnly = True
            cmb_SignatoryRole.IsReadOnly = True
            btnSavedetail.Hidden = True

            WindowDetail.Title = "Account Signatory"

            With objAccountSignatoryDetail
                If .isPrimary IsNot Nothing Then
                    cbIsPrimary.Checked = .isPrimary

                    'store_SignatoryRole.DataSource = goAML_AccountBLL.GetListPeranOrangDalamRekening()
                    'store_SignatoryRole.DataBind()
                    SignatoryRole = goAML_AccountBLL.GetRoleSignatoryByID(.Role)
                    If SignatoryRole IsNot Nothing Then
                        cmb_SignatoryRole.SetTextWithTextValue(SignatoryRole.Kode, SignatoryRole.Keterangan)
                    End If


                    If objAccountSignatoryDetailCustomer IsNot Nothing Then
                        cb_isCustomer.Checked = True
                        'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                        signatoryperson = goAML_AccountBLL.GetNameSignatoryByID(.FK_CIF_Person_ID)
                        If signatoryperson IsNot Nothing Then
                            cmb_SignatoryPerson.SetTextWithTextValue(signatoryperson.CIF, signatoryperson.CIF + " - " + signatoryperson.INDV_Last_Name)
                        End If

                        'End Update
                        txt_INDV_Title.Text = objAccountSignatoryDetailCustomer.INDV_Title
                        txt_INDV_Last_Name.Text = objAccountSignatoryDetailCustomer.INDV_Last_Name
                        If obj_Account.date_balance.HasValue Then
                            txt_INDV_Birthdate.Text = objAccountSignatoryDetailCustomer.INDV_BirthDate.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            txt_INDV_Birthdate.Text = ""
                        End If
                        txt_INDV_Birth_place.Text = objAccountSignatoryDetailCustomer.INDV_Birth_Place
                        txt_INDV_Mothers_name.Text = objAccountSignatoryDetailCustomer.INDV_Mothers_Name
                        txt_INDV_Alias.Text = objAccountSignatoryDetailCustomer.INDV_Alias
                        txt_INDV_SSN.Text = objAccountSignatoryDetailCustomer.INDV_SSN
                        txt_INDV_Passport_number.Text = objAccountSignatoryDetailCustomer.INDV_Passport_Number
                        'Update: Zikri_25092020 Change dropdown to NDFSDropdown
                        If Not String.IsNullOrEmpty(objAccountSignatoryDetailCustomer.INDV_Passport_Country) Then
                            txt_INDV_Passport_country.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Passport_Country).Keterangan
                        Else
                            txt_INDV_Passport_country.Text = ""
                        End If
                        'End Update
                        txt_INDV_ID_Number.Text = objAccountSignatoryDetailCustomer.INDV_ID_Number
                        If Not objAccountSignatoryDetailCustomer.INDV_Nationality1 = "" And Not objAccountSignatoryDetailCustomer.INDV_Nationality1 Is Nothing Then
                            txt_INDV_Nationality1.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Nationality1).Keterangan
                        End If
                        If Not objAccountSignatoryDetailCustomer.INDV_Nationality2 = "" And Not objAccountSignatoryDetailCustomer.INDV_Nationality2 Is Nothing Then
                            txt_INDV_Nationality2.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Nationality2).Keterangan
                        End If
                        If Not objAccountSignatoryDetailCustomer.INDV_Nationality3 = "" And Not objAccountSignatoryDetailCustomer.INDV_Nationality3 Is Nothing Then
                            txt_INDV_Nationality3.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Nationality3).Keterangan
                        End If
                        If Not objAccountSignatoryDetailCustomer.INDV_Residence = "" And Not objAccountSignatoryDetailCustomer.INDV_Residence Is Nothing Then
                            txt_INDV_Residence.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Residence).Keterangan
                        End If

                        txt_INDV_Email.Text = objAccountSignatoryDetailCustomer.INDV_Email
                        txt_INDV_Occupation.Text = objAccountSignatoryDetailCustomer.INDV_Occupation
                        txt_INDV_Tax_NumberSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Tax_Number
                        txt_INDV_Source_of_WealthSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Source_of_Wealth

                        If objAccountSignatoryDetailCustomer.INDV_Deceased = True Then
                            cb_DeceasedSignatory.Checked = True
                            txt_deceased_dateSignatory.Hidden = False
                            txt_deceased_dateSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        End If
                        If objAccountSignatoryDetailCustomer.INDV_Tax_Reg_Number = True Then
                            cb_taxSignatory.Checked = True
                        End If
                        'txt_INDV_Employer_Name.Text = objAccountSignatoryDetailCustomer.INDV_Employer_Name
                        'txt_INDV_Employer_Address_Address_Type.Text =
                        'txt_INDV_Employer_Address_Address.Text =
                        'txt_INDV_Employer_Address_Town.Text =
                        'txt_INDV_Employer_Address_City.Text =
                        'txt_INDV_Employer_Address_Zip.Text =
                        'txt_INDV_Employer_Address_country_code.Text =
                        'txt_INDV_Employer_Address_State.Text =
                        'txt_INDV_Employer_Address_Comment.Text =
                        'txt_INDV_Employer_Phone_contact_type.Text =
                        'txt_INDV_Employer_Phone_communication_type.Text =
                        'txt_INDV_Employer_Phone_country_prefix.Text =
                        'txt_INDV_Employer_Phone_number.Text =
                        'txt_INDV_Employer_Phone_extension.Text =
                        'txt_INDV_Employer_Phone_Comment.Text =
                        If goAML_AccountBLL.GetVw_Customer_AddressesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID).Count > 0 Then
                            StoreAddressSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_AddressesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID)
                        End If
                        StoreAddressSignatory.DataBind()
                        If goAML_AccountBLL.GetVw_Customer_PhonesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID).Count > 0 Then
                            StorePhoneSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_PhonesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID)
                        End If
                        StorePhoneSignatory.DataBind()
                        If goAML_AccountBLL.GetVw_Customer_EmployerPhonesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID).Count > 0 Then
                            StoreEmployerPhoneSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_EmployerPhonesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID)
                        End If
                        StoreEmployerPhoneSignatory.DataBind()
                        If goAML_AccountBLL.GetVw_Customer_EmployerAddressCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID).Count > 0 Then
                            StoreEmployerAddressSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_EmployerAddressCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID)
                        End If

                        'Dedy added 27082020
                    Else
                        cb_isCustomer.Checked = False
                        SignatoryNotPerson = goAML_AccountBLL.GetCBSignatoryNotPerson(.WIC_No)
                        If SignatoryNotPerson IsNot Nothing Then
                            cmb_Not_SignatoryPerson.SetTextWithTextValue(SignatoryNotPerson.WIC_No, SignatoryNotPerson.WIC_No + " - " + SignatoryNotPerson.INDV_Last_Name)
                        End If
                        txt_INDV_Title.Text = objAccountSignatoryDetailWic.INDV_Title
                        txt_INDV_Last_Name.Text = objAccountSignatoryDetailWic.INDV_Last_Name
                        If obj_Account.date_balance.HasValue Then
                            txt_INDV_Birthdate.Text = objAccountSignatoryDetailWic.INDV_BirthDate.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            txt_INDV_Birthdate.Text = ""
                        End If
                        txt_INDV_Birth_place.Text = objAccountSignatoryDetailWic.INDV_Birth_Place
                        txt_INDV_Mothers_name.Text = objAccountSignatoryDetailWic.INDV_Mothers_Name
                        txt_INDV_Alias.Text = objAccountSignatoryDetailWic.INDV_Alias
                        txt_INDV_SSN.Text = objAccountSignatoryDetailWic.INDV_SSN
                        txt_INDV_Passport_number.Text = objAccountSignatoryDetailWic.INDV_Passport_Number
                        txt_INDV_Passport_country.Text = objAccountSignatoryDetailWic.INDV_Passport_Country
                        'Update: Zikri_25092020 Change dropdown to NDFSDropdown
                        If Not String.IsNullOrEmpty(objAccountSignatoryDetailWic.INDV_Passport_Country) Then
                            txt_INDV_Passport_country.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Passport_Country).Keterangan
                        Else
                            txt_INDV_Passport_country.Text = ""
                        End If
                        'End Update
                        txt_INDV_ID_Number.Text = objAccountSignatoryDetailWic.INDV_ID_Number
                        If Not objAccountSignatoryDetailWic.INDV_Nationality1 = "" And Not objAccountSignatoryDetailWic.INDV_Nationality1 Is Nothing Then
                            txt_INDV_Nationality1.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Nationality1).Keterangan
                        End If
                        If Not objAccountSignatoryDetailWic.INDV_Nationality2 = "" And Not objAccountSignatoryDetailWic.INDV_Nationality2 Is Nothing Then
                            txt_INDV_Nationality2.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Nationality2).Keterangan
                        End If
                        If Not objAccountSignatoryDetailWic.INDV_Nationality3 = "" And Not objAccountSignatoryDetailWic.INDV_Nationality3 Is Nothing Then
                            txt_INDV_Nationality3.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Nationality3).Keterangan
                        End If
                        If Not objAccountSignatoryDetailWic.INDV_Residence = "" And Not objAccountSignatoryDetailWic.INDV_Residence Is Nothing Then
                            txt_INDV_Residence.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Residence).Keterangan
                        End If

                        txt_INDV_Email.Text = objAccountSignatoryDetailWic.INDV_Email
                        txt_INDV_Occupation.Text = objAccountSignatoryDetailWic.INDV_Occupation
                        txt_INDV_Tax_NumberSignatory.Text = objAccountSignatoryDetailWic.INDV_Tax_Number


                        If objAccountSignatoryDetailWic.INDV_Deceased = True Then
                            cb_DeceasedSignatory.Checked = True
                            txt_deceased_dateSignatory.Hidden = False
                            txt_deceased_dateSignatory.Text = objAccountSignatoryDetailWic.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        End If
                        If objAccountSignatoryDetailWic.INDV_Tax_Reg_Number = True Then
                            cb_taxSignatory.Checked = True
                        End If
                        'Update: Zikri_12102020
                        If goAML_AccountBLL.Getvw_wic_addresses(objAccountSignatoryDetailWic.PK_Customer_ID).Count > 0 Then
                            StoreAddressSignatory.DataSource = goAML_AccountBLL.Getvw_wic_addresses(objAccountSignatoryDetailWic.PK_Customer_ID)
                        End If
                        StoreAddressSignatory.DataBind()
                        If goAML_AccountBLL.GetVw_WIC_Phones(objAccountSignatoryDetailWic.PK_Customer_ID).Count > 0 Then
                            StorePhoneSignatory.DataSource = goAML_AccountBLL.GetVw_WIC_Phones(objAccountSignatoryDetailWic.PK_Customer_ID)
                        End If
                        StorePhoneSignatory.DataBind()
                        If goAML_AccountBLL.GetVw_WIC_Employer_Phones(objAccountSignatoryDetailWic.PK_Customer_ID).Count > 0 Then
                            StoreEmployerPhoneSignatory.DataSource = goAML_AccountBLL.GetVw_WIC_Employer_Phones(objAccountSignatoryDetailWic.PK_Customer_ID)
                        End If
                        StoreEmployerPhoneSignatory.DataBind()
                        If goAML_AccountBLL.Getvw_wic_employer_addresses(objAccountSignatoryDetailWic.PK_Customer_ID).Count > 0 Then
                            StoreEmployerAddressSignatory.DataSource = goAML_AccountBLL.Getvw_wic_employer_addresses(objAccountSignatoryDetailWic.PK_Customer_ID)
                        End If
                        StoreEmployerAddressSignatory.DataBind()
                        'End Update
                        'Dedy end Added 27082020
                    End If
                Else
                    cbIsPrimary.Checked = False
                End If
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub
    Sub LoadDataEditAccountSignatory(id As Long)
        objTempgoAML_Ref_Account_Signatory = objListgoAML_Ref_Account_Signatory.Find(Function(x) x.PK_Signatory_ID = id)
        objTempVwAccountSignatory = objListVwAccountSignatory.Find(Function(x) x.PK_Signatory_ID = id)
        Dim objAccountSignatoryDetailCustomer As goAML_Ref_Customer

        'Dedy Added 27082020
        LoadSignatoryStoreData()
        cb_isCustomer.ReadOnly = False
        Dim objAccountSignatoryDetailWic As goAML_Ref_WIC
        objAccountSignatoryDetailWic = goAML_AccountBLL.GetWicByCIF(objTempgoAML_Ref_Account_Signatory.WIC_No)
        'Dedy Added 27082020

        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing
        Dim signatoryperson As NawaDevDAL.goAML_Ref_Customer = Nothing
        Dim SignatoryNotPerson As NawaDevDAL.goAML_Ref_WIC = Nothing
        Dim RoleSignatory As NawaDevDAL.goAML_Ref_Peran_orang_dalam_rekening = Nothing

        objAccountSignatoryDetailCustomer = goAML_AccountBLL.GetCustomerByCIF(objTempgoAML_Ref_Account_Signatory.FK_CIF_Person_ID)

        If Not objTempgoAML_Ref_Account_Signatory Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            If cb_isCustomer.Checked = True Then
                cmb_SignatoryPerson.IsHidden = False
                cmb_SignatoryPerson.IsReadOnly = False
                cmb_Not_SignatoryPerson.IsReadOnly = False
                cmb_Not_SignatoryPerson.IsHidden = True
            Else
                cmb_Not_SignatoryPerson.IsHidden = False
                cmb_Not_SignatoryPerson.IsReadOnly = False
                cmb_SignatoryPerson.IsHidden = True
                cmb_SignatoryPerson.IsReadOnly = False
            End If

            cbIsPrimary.ReadOnly = False
            cmb_SignatoryRole.IsReadOnly = False
            btnSavedetail.Hidden = False
            WindowDetail.Title = "Edit Account Signatory"

            With objTempgoAML_Ref_Account_Signatory

                'Dim objDt As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,INDV_Last_Name,cb_value from Vw_CB_Signatory_Person", Nothing)


                'Dim objDt2 As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,INDV_Last_Name,cb_value from Vw_CB_Signatory_Not_Person", Nothing)

                If .isCustomer = True Then
                    'cmb_SignatoryPerson.SetValue(.FK_CIF_Person_ID)
                    'store_SignatoryPerson.DataSource = objDt
                    'store_SignatoryPerson.DataBind()

                    cb_isCustomer.Checked = True

                    'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                    signatoryperson = goAML_AccountBLL.GetNameSignatoryByID(.FK_CIF_Person_ID)
                    If signatoryperson IsNot Nothing Then
                        cmb_SignatoryPerson.SetTextWithTextValue(signatoryperson.CIF, signatoryperson.CIF + " - " + signatoryperson.INDV_Last_Name)
                    End If

                    'End Update
                    'cmb_SignatoryPerson.DoQuery(.FK_CIF_Person_ID, True)
                    'cmb_SignatoryPerson.SetValueAndFireSelect(.FK_CIF_Person_ID)
                Else
                    'store_SignatoryPerson.DataSource = objDt2
                    'store_SignatoryPerson.DataBind()

                    'Update: Zikri_17092020 Change dropdown to NDFSDropdown
                    cb_isCustomer.Checked = False
                    SignatoryNotPerson = goAML_AccountBLL.GetCBSignatoryNotPerson(.WIC_No)
                    If SignatoryNotPerson IsNot Nothing Then
                        cmb_Not_SignatoryPerson.SetTextWithTextValue(SignatoryNotPerson.WIC_No, SignatoryNotPerson.WIC_No + " - " + SignatoryNotPerson.INDV_Last_Name)
                    End If
                    'End Update
                    'cmb_SignatoryPerson.DoQuery(.WIC_No, True)
                    'cmb_SignatoryPerson.SetValueAndFireSelect(.WIC_No)
                End If

                If .isPrimary IsNot Nothing Then
                    cbIsPrimary.Checked = .isPrimary

                    'store_SignatoryRole.DataSource = goAML_AccountBLL.GetListPeranOrangDalamRekening()
                    'store_SignatoryRole.DataBind()
                    RoleSignatory = goAML_AccountBLL.GetRoleSignatoryByID(.Role)
                    If RoleSignatory IsNot Nothing Then
                        cmb_SignatoryRole.SetTextWithTextValue(RoleSignatory.Kode, RoleSignatory.Keterangan)
                    End If


                    If objAccountSignatoryDetailCustomer IsNot Nothing Then
                        txt_INDV_Title.Text = objAccountSignatoryDetailCustomer.INDV_Title
                        txt_INDV_Last_Name.Text = objAccountSignatoryDetailCustomer.INDV_Last_Name
                        If obj_Account.date_balance.HasValue Then
                            txt_INDV_Birthdate.Text = objAccountSignatoryDetailCustomer.INDV_BirthDate.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            txt_INDV_Birthdate.Text = ""
                        End If
                        txt_INDV_Birth_place.Text = objAccountSignatoryDetailCustomer.INDV_Birth_Place
                        txt_INDV_Mothers_name.Text = objAccountSignatoryDetailCustomer.INDV_Mothers_Name
                        txt_INDV_Alias.Text = objAccountSignatoryDetailCustomer.INDV_Alias
                        txt_INDV_SSN.Text = objAccountSignatoryDetailCustomer.INDV_SSN
                        txt_INDV_Passport_number.Text = objAccountSignatoryDetailCustomer.INDV_Passport_Number
                        'Update: Zikri_25092020 Change dropdown to NDFSDropdown
                        If Not String.IsNullOrEmpty(objAccountSignatoryDetailCustomer.INDV_Passport_Country) Then
                            txt_INDV_Passport_country.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Passport_Country).Keterangan
                        Else
                            txt_INDV_Passport_country.Text = ""
                        End If
                        'End Update
                        txt_INDV_ID_Number.Text = objAccountSignatoryDetailCustomer.INDV_ID_Number
                        If Not objAccountSignatoryDetailCustomer.INDV_Nationality1 = "" And Not objAccountSignatoryDetailCustomer.INDV_Nationality1 Is Nothing Then
                            txt_INDV_Nationality1.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Nationality1).Keterangan
                        End If
                        If Not objAccountSignatoryDetailCustomer.INDV_Nationality2 = "" And Not objAccountSignatoryDetailCustomer.INDV_Nationality2 Is Nothing Then
                            txt_INDV_Nationality2.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Nationality2).Keterangan
                        End If
                        If Not objAccountSignatoryDetailCustomer.INDV_Nationality3 = "" And Not objAccountSignatoryDetailCustomer.INDV_Nationality3 Is Nothing Then
                            txt_INDV_Nationality3.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Nationality3).Keterangan
                        End If
                        If Not objAccountSignatoryDetailCustomer.INDV_Residence = "" And Not objAccountSignatoryDetailCustomer.INDV_Residence Is Nothing Then
                            txt_INDV_Residence.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Residence).Keterangan
                        End If
                        txt_INDV_Email.Text = objAccountSignatoryDetailCustomer.INDV_Email
                        txt_INDV_Occupation.Text = objAccountSignatoryDetailCustomer.INDV_Occupation
                        txt_INDV_Tax_NumberSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Tax_Number
                        txt_INDV_Source_of_WealthSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Source_of_Wealth

                        If objAccountSignatoryDetailCustomer.INDV_Deceased = True Then
                            cb_DeceasedSignatory.Checked = True
                            txt_deceased_dateSignatory.Hidden = False
                            txt_deceased_dateSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        End If
                        If objAccountSignatoryDetailCustomer.INDV_Tax_Reg_Number = True Then
                            cb_taxSignatory.Checked = True
                        End If

                        If goAML_AccountBLL.GetVw_Customer_AddressesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID).Count > 0 Then
                            StoreAddressSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_AddressesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID)
                        End If
                        StoreAddressSignatory.DataBind()
                        If goAML_AccountBLL.GetVw_Customer_PhonesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID).Count > 0 Then
                            StorePhoneSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_PhonesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID)
                        End If
                        StorePhoneSignatory.DataBind()
                        If goAML_AccountBLL.GetVw_Customer_EmployerPhonesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID).Count > 0 Then
                            StoreEmployerPhoneSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_EmployerPhonesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID)
                        End If
                        StoreEmployerPhoneSignatory.DataBind()
                        If goAML_AccountBLL.GetVw_Customer_EmployerAddressCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID).Count > 0 Then
                            StoreEmployerAddressSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_EmployerAddressCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID)
                        End If

                        'Dedy Added 27082020
                    Else

                        txt_INDV_Title.Text = objAccountSignatoryDetailWic.INDV_Title
                        txt_INDV_Last_Name.Text = objAccountSignatoryDetailWic.INDV_Last_Name
                        If obj_Account.date_balance.HasValue Then
                            txt_INDV_Birthdate.Text = objAccountSignatoryDetailWic.INDV_BirthDate.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            txt_INDV_Birthdate.Text = ""
                        End If
                        txt_INDV_Birth_place.Text = objAccountSignatoryDetailWic.INDV_Birth_Place
                        txt_INDV_Mothers_name.Text = objAccountSignatoryDetailWic.INDV_Mothers_Name
                        txt_INDV_Alias.Text = objAccountSignatoryDetailWic.INDV_Alias
                        txt_INDV_SSN.Text = objAccountSignatoryDetailWic.INDV_SSN
                        txt_INDV_Passport_number.Text = objAccountSignatoryDetailWic.INDV_Passport_Number
                        'Update: Zikri_25092020 Change dropdown to NDFSDropdown
                        If Not String.IsNullOrEmpty(objAccountSignatoryDetailWic.INDV_Passport_Country) Then
                            txt_INDV_Passport_country.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Passport_Country).Keterangan
                        Else
                            txt_INDV_Passport_country.Text = ""
                        End If
                        'End Update
                        txt_INDV_ID_Number.Text = objAccountSignatoryDetailWic.INDV_ID_Number
                        If Not objAccountSignatoryDetailWic.INDV_Nationality1 = "" And Not objAccountSignatoryDetailWic.INDV_Nationality1 Is Nothing Then
                            txt_INDV_Nationality1.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Nationality1).Keterangan
                        End If
                        If Not objAccountSignatoryDetailWic.INDV_Nationality2 = "" And Not objAccountSignatoryDetailWic.INDV_Nationality2 Is Nothing Then
                            txt_INDV_Nationality2.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Nationality2).Keterangan
                        End If
                        If Not objAccountSignatoryDetailWic.INDV_Nationality3 = "" And Not objAccountSignatoryDetailWic.INDV_Nationality3 Is Nothing Then
                            txt_INDV_Nationality3.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Nationality3).Keterangan
                        End If
                        If Not objAccountSignatoryDetailWic.INDV_Residence = "" And Not objAccountSignatoryDetailWic.INDV_Residence Is Nothing Then
                            txt_INDV_Residence.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Residence).Keterangan
                        End If
                        txt_INDV_Email.Text = objAccountSignatoryDetailWic.INDV_Email
                        txt_INDV_Occupation.Text = objAccountSignatoryDetailWic.INDV_Occupation
                        txt_INDV_Tax_NumberSignatory.Text = objAccountSignatoryDetailWic.INDV_Tax_Number


                        If objAccountSignatoryDetailWic.INDV_Deceased = True Then
                            cb_DeceasedSignatory.Checked = True
                            txt_deceased_dateSignatory.Hidden = False
                            txt_deceased_dateSignatory.Text = objAccountSignatoryDetailWic.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        End If
                        If objAccountSignatoryDetailWic.INDV_Tax_Reg_Number = True Then
                            cb_taxSignatory.Checked = True
                        End If

                        If goAML_AccountBLL.Getvw_wic_addresses(objAccountSignatoryDetailWic.PK_Customer_ID).Count > 0 Then
                            StoreAddressSignatory.DataSource = goAML_AccountBLL.Getvw_wic_addresses(objAccountSignatoryDetailWic.PK_Customer_ID)
                        End If
                        StoreAddressSignatory.DataBind()
                        If goAML_AccountBLL.GetVw_WIC_Phones(objAccountSignatoryDetailWic.PK_Customer_ID).Count > 0 Then
                            StorePhoneSignatory.DataSource = goAML_AccountBLL.GetVw_WIC_Phones(objAccountSignatoryDetailWic.PK_Customer_ID)
                        End If
                        StorePhoneSignatory.DataBind()
                        If goAML_AccountBLL.GetVw_WIC_Employer_Phones(objAccountSignatoryDetailWic.PK_Customer_ID).Count > 0 Then
                            StoreEmployerPhoneSignatory.DataSource = goAML_AccountBLL.GetVw_WIC_Employer_Phones(objAccountSignatoryDetailWic.PK_Customer_ID)
                        End If
                        StoreEmployerPhoneSignatory.DataBind()
                        If goAML_AccountBLL.Getvw_wic_employer_addresses(objAccountSignatoryDetailWic.PK_Customer_ID).Count > 0 Then
                            StoreEmployerAddressSignatory.DataSource = goAML_AccountBLL.Getvw_wic_employer_addresses(objAccountSignatoryDetailWic.PK_Customer_ID)
                        End If
                        'Dedy End Added 27082020
                    End If

                    StoreEmployerAddressSignatory.DataBind()
                Else
                    cbIsPrimary.Checked = False
                    'store_SignatoryRole.DataSource = goAML_AccountBLL.GetListPeranOrangDalamRekening()
                    'store_SignatoryRole.DataBind()
                End If
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Protected Sub GrdCmdEmployerPhoneSignatory(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhoneDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdEmployerAddressSignatory(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddressDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub checkBoxcb_Deceased_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_DeceasedSignatory.DirectCheck
        Try
            If cb_DeceasedSignatory.Checked Then
                txt_deceased_dateSignatory.Hidden = False
            Else
                txt_deceased_dateSignatory.Hidden = True
                txt_deceased_dateSignatory.Value = Nothing
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DeleteRecordTaskDetailAccountSignatory(id As Long)
        Dim objDelVWSignatory As NawaDevDAL.Vw_Account_Signatory = objListVwAccountSignatory.Find(Function(x) x.PK_Signatory_ID = id)
        Dim objxDelSignatory As NawaDevDAL.goAML_Ref_Account_Signatory = objListgoAML_Ref_Account_Signatory.Find(Function(x) x.PK_Signatory_ID = id)

        If Not objxDelSignatory Is Nothing Then
            objListgoAML_Ref_Account_Signatory.Remove(objxDelSignatory)
        End If

        If Not objDelVWSignatory Is Nothing Then
            objListVwAccountSignatory.Remove(objDelVWSignatory)

            Store_Signatory.DataSource = objListVwAccountSignatory
            Store_Signatory.DataBind()
        End If

        FormPanelTaskDetail.Hidden = True
        WindowDetail.Hidden = True
        ClearinputAccount()

    End Sub

    Protected Sub btnBackSaveDetail_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputAccount()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Private Sub ACCOUNT_EDIT_Default_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Update
    End Sub

    Protected Sub cb_EntityAccount_CheckedChanged(sender As Object, e As EventArgs) Handles cb_EntityAccount.DirectCheck

        panel_Corp.Hidden = Not cb_EntityAccount.Checked

        If panel_Corp.Hidden = True Then
            ClearEntityAccount()
        End If
    End Sub

    Protected Sub cmb_SignatoryPerson_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim CIF As String
            If cb_isCustomer.Checked = True Then
                CIF = cmb_SignatoryPerson.SelectedItemValue
            Else
                CIF = cmb_Not_SignatoryPerson.SelectedItemValue
            End If


            If Not String.IsNullOrEmpty(CIF) Then
                Dim objCustomer As NawaDevDAL.goAML_Ref_Customer = NawaDevBLL.goAML_AccountBLL.GetCustomerByCIF(cmb_SignatoryPerson.SelectedItemValue)
                ''dedy added for show data wic
                Dim objWic As NawaDevDAL.goAML_Ref_WIC = NawaDevBLL.goAML_AccountBLL.GetWicByCIF(cmb_Not_SignatoryPerson.SelectedItemValue)


                If Not objCustomer Is Nothing Then

                    txt_INDV_Title.Text = objCustomer.INDV_Title
                    txt_INDV_Last_Name.Text = objCustomer.INDV_Last_Name

                    If objCustomer.INDV_BirthDate IsNot Nothing Then
                        txt_INDV_Birthdate.Text = objCustomer.INDV_BirthDate
                        txt_INDV_Birth_place.Text = objCustomer.INDV_Birth_Place
                        txt_INDV_Mothers_name.Text = objCustomer.INDV_Mothers_Name
                        txt_INDV_Alias.Text = objCustomer.INDV_Alias
                        txt_INDV_SSN.Text = objCustomer.INDV_SSN
                        txt_INDV_Passport_number.Text = objCustomer.INDV_Passport_Number
                        'Update: Zikri_25092020 Change dropdown to NDFSDropdown
                        If Not String.IsNullOrEmpty(objCustomer.INDV_Passport_Country) Then
                            txt_INDV_Passport_country.Text = goAML_CustomerBLL.GetNamaNegaraByID(objCustomer.INDV_Passport_Country).Keterangan
                        Else
                            txt_INDV_Passport_country.Text = ""
                        End If
                        'End Update
                        'txt_INDV_Passport_country.Text = objCustomer.INDV_Passport_Country
                        txt_INDV_ID_Number.Text = objCustomer.INDV_ID_Number
                        If Not objCustomer.INDV_Nationality1 = "" Then
                            txt_INDV_Nationality1.Text = goAML_CustomerBLL.GetNamaNegaraByID(objCustomer.INDV_Nationality1).Keterangan
                        End If
                        If Not objCustomer.INDV_Nationality2 = "" Then
                            txt_INDV_Nationality2.Text = goAML_CustomerBLL.GetNamaNegaraByID(objCustomer.INDV_Nationality2).Keterangan
                        End If
                        If Not objCustomer.INDV_Nationality3 = "" Then
                            txt_INDV_Nationality3.Text = goAML_CustomerBLL.GetNamaNegaraByID(objCustomer.INDV_Nationality3).Keterangan
                        End If
                        If Not objCustomer.INDV_Residence = "" Then
                            txt_INDV_Residence.Text = goAML_CustomerBLL.GetNamaNegaraByID(objCustomer.INDV_Residence).Keterangan
                        End If



                        txt_INDV_Email.Text = objCustomer.INDV_Email
                        txt_INDV_Occupation.Text = objCustomer.INDV_Occupation
                    End If


                    'txt_INDV_Employer_Name.Text = objCustomer.INDV_Employer_Name
                    txt_INDV_Tax_NumberSignatory.Text = objCustomer.INDV_Tax_Number
                    txt_INDV_Source_of_WealthSignatory.Text = objCustomer.INDV_Source_of_Wealth

                    If objCustomer.INDV_Deceased = True Then
                        cb_DeceasedSignatory.Checked = True
                        txt_deceased_dateSignatory.Hidden = False
                        txt_deceased_dateSignatory.Text = objCustomer.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    End If
                    If objCustomer.INDV_Tax_Reg_Number = True Then
                        cb_taxSignatory.Checked = True
                    End If


                    StoreAddressSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_Addresses(objCustomer.PK_Customer_ID)
                    StoreAddressSignatory.DataBind()
                    StorePhoneSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_Phones(objCustomer.PK_Customer_ID)
                    StorePhoneSignatory.DataBind()
                    StoreEmployerPhoneSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_EmployerPhonesCustomer(objCustomer.PK_Customer_ID)
                    StoreEmployerPhoneSignatory.DataBind()
                    StoreEmployerAddressSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_EmployerAddressCustomer(objCustomer.PK_Customer_ID)
                    StoreEmployerAddressSignatory.DataBind()
                    'Dim objDt As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,INDV_Last_Name,cb_value from Vw_CB_Signatory_Person", Nothing)
                    'store_SignatoryPerson.DataSource = objDt
                    'store_SignatoryPerson.DataBind()

                    cmb_SignatoryPerson.SetTextWithTextValue(objCustomer.CIF, objCustomer.CIF + " - " + objCustomer.INDV_Last_Name)


                    '    'Dedy Added 27082020
                Else
                    txt_INDV_Title.Text = objWic.INDV_Title
                    txt_INDV_Last_Name.Text = objWic.INDV_Last_Name

                    If objWic.INDV_BirthDate IsNot Nothing Then
                        txt_INDV_Birthdate.Text = objWic.INDV_BirthDate
                        txt_INDV_Birth_place.Text = objWic.INDV_Birth_Place
                        txt_INDV_Mothers_name.Text = objWic.INDV_Mothers_Name
                        txt_INDV_Alias.Text = objWic.INDV_Alias
                        txt_INDV_SSN.Text = objWic.INDV_SSN
                        txt_INDV_Passport_number.Text = objWic.INDV_Passport_Number
                        If Not String.IsNullOrEmpty(objWic.INDV_Passport_Country) Then
                            txt_INDV_Passport_country.Text = goAML_CustomerBLL.GetNamaNegaraByID(objWic.INDV_Passport_Country).Keterangan
                        Else
                            txt_INDV_Passport_country.Text = ""
                        End If
                        txt_INDV_Passport_country.Text = objWic.INDV_Passport_Country
                        txt_INDV_ID_Number.Text = objWic.INDV_ID_Number
                        If Not objWic.INDV_Nationality1 = "" Then
                            txt_INDV_Nationality1.Text = goAML_CustomerBLL.GetNamaNegaraByID(objWic.INDV_Nationality1).Keterangan
                        End If
                        If Not objWic.INDV_Nationality2 = "" Then
                            txt_INDV_Nationality2.Text = goAML_CustomerBLL.GetNamaNegaraByID(objWic.INDV_Nationality2).Keterangan
                        End If
                        If Not objWic.INDV_Nationality3 = "" Then
                            txt_INDV_Nationality3.Text = goAML_CustomerBLL.GetNamaNegaraByID(objWic.INDV_Nationality3).Keterangan
                        End If
                        If Not objWic.INDV_Residence = "" Then
                            txt_INDV_Residence.Text = goAML_CustomerBLL.GetNamaNegaraByID(objWic.INDV_Residence).Keterangan
                        End If
                        txt_INDV_Email.Text = objWic.INDV_Email
                        txt_INDV_Occupation.Text = objWic.INDV_Occupation
                    End If

                    txt_INDV_Tax_NumberSignatory.Text = objWic.INDV_Tax_Number
                    'txt_INDV_Source_of_WealthSignatory.Text = objWic.INDV_Source_of_Wealth

                    If objWic.INDV_Deceased = True Then
                        cb_DeceasedSignatory.Checked = True
                        txt_deceased_dateSignatory.Hidden = False
                        txt_deceased_dateSignatory.Text = objWic.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    End If
                    If objWic.INDV_Tax_Reg_Number = True Then
                        cb_taxSignatory.Checked = True
                    End If


                    StoreAddressSignatory.DataSource = goAML_AccountBLL.Getvw_wic_addresses(objWic.PK_Customer_ID)
                    StoreAddressSignatory.DataBind()
                    StorePhoneSignatory.DataSource = goAML_AccountBLL.GetVw_WIC_Phones(objWic.PK_Customer_ID)
                    StorePhoneSignatory.DataBind()
                    StoreEmployerPhoneSignatory.DataSource = goAML_AccountBLL.GetVw_WIC_Employer_Phones(objWic.PK_Customer_ID)
                    StoreEmployerPhoneSignatory.DataBind()
                    StoreEmployerAddressSignatory.DataSource = goAML_AccountBLL.Getvw_wic_employer_addresses(objWic.PK_Customer_ID)
                    StoreEmployerAddressSignatory.DataBind()
                    'Dim objDt As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,INDV_Last_Name,cb_value from Vw_CB_Signatory_Not_Person", Nothing)
                    'store_SignatoryPerson.DataSource = objDt
                    'store_SignatoryPerson.DataBind()

                    cmb_Not_SignatoryPerson.SetTextWithTextValue(objWic.WIC_No, objWic.WIC_No + " - " + objWic.INDV_Last_Name)


                    '    '    'Dedy end added 27082020

                End If

            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Cmb_PersonalAccountType_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim CODE As String = Cmb_PersonalAccountType.SelectedItemValue

            If Not String.IsNullOrEmpty(CODE) Then
                Dim personalaccounttype As NawaDevDAL.goAML_Ref_Mapping_Jenis_Rekening_INDV_CORP = NawaDevBLL.goAML_AccountBLL.GetMappingAccountTypebyCode(CODE)
                If personalaccounttype IsNot Nothing Then
                    If personalaccounttype.INDV_CORP = 1 Then
                        cb_EntityAccount.Checked = False
                        cb_EntityAccount.Hidden = True
                    Else
                        cb_EntityAccount.Checked = True
                        cb_EntityAccount.Hidden = False
                        cb_EntityAccount.ReadOnly = True
                    End If
                End If

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub cmb_EntityAccount_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim CIF As String = cmb_EntityAccount.SelectedItemValue

            If Not String.IsNullOrEmpty(CIF) Then
                Dim objCustomer As NawaDevDAL.goAML_Ref_Customer = NawaDevBLL.goAML_AccountBLL.GetCustomerByCIF(cmb_EntityAccount.SelectedItemValue)

                If Not objCustomer Is Nothing Then

                    txt_Corp_Name.Text = objCustomer.Corp_Name
                    txt_Corp_Commercial_Name.Text = objCustomer.Corp_Commercial_Name
                    If Not objCustomer.Corp_Incorporation_Legal_Form = "" Then
                        txt_Corp_Legal_Form.Text = goAML_CustomerBLL.GetBentukBadanUsahaByID(objCustomer.Corp_Incorporation_Legal_Form).Keterangan
                    End If
                    txt_Corp_Incorporation_number.Text = objCustomer.Corp_Incorporation_Number
                    txt_Corp_Business.Text = objCustomer.Corp_Business
                    txt_Corp_Email.Text = objCustomer.Corp_Email
                    txt_Corp_url.Text = objCustomer.Corp_Url
                    txt_Corp_incorporation_state.Text = objCustomer.Corp_Incorporation_State
                    If Not objCustomer.Corp_Incorporation_Country_Code = "" Then
                        txt_Corp_incorporation_country_code.Text = goAML_CustomerBLL.GetNamaNegaraByID(objCustomer.Corp_Incorporation_Country_Code).Keterangan
                    End If

                    If objCustomer.Corp_Incorporation_Date IsNot Nothing Then
                        txt_Corp_incorporation_date.Text = objCustomer.Corp_Incorporation_Date
                    Else
                        txt_Corp_incorporation_date.Text = ""
                    End If

                    'try do



                    store_phone.DataSource = goAML_AccountBLL.GetVw_Customer_Phones(objCustomer.PK_Customer_ID)
                    store_phone.DataBind()

                    store_address.DataSource = goAML_AccountBLL.GetVw_Customer_Addresses(objCustomer.PK_Customer_ID)
                    store_address.DataBind()

                    If Not objCustomer.Corp_Business_Closed Is Nothing Then
                        cbTutup.Checked = IIf(objCustomer.Corp_Business_Closed = 1, True, False)
                    End If

                    If cbTutup.Checked Then
                        txt_Corp_date_business_closed.Text = objCustomer.Corp_Date_Business_Closed.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    Else
                        txt_Corp_date_business_closed.Text = ""
                    End If
                    txt_Corp_tax_number.Text = objCustomer.Corp_Tax_Number
                    txt_Corp_Comments.Text = objCustomer.Corp_Comments

                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub IsCustomer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            If cb_isCustomer.Checked = True Then
                cmb_SignatoryPerson.IsHidden = False
                cmb_Not_SignatoryPerson.IsHidden = True
                cmb_Not_SignatoryPerson.SetTextValue("")
            Else
                cmb_Not_SignatoryPerson.IsHidden = False
                cmb_SignatoryPerson.IsHidden = True
                cmb_SignatoryPerson.SetTextValue("")

            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    'Private Sub cb_isCustomer_CheckedChanged(sender As Object, e As DirectEventArgs) Handles cb_isCustomer.DirectCheck
    'dedy added 27082020 for validation pilih customer individu
    'cb_isCustomer.Checked Then
    'cmb_SignatoryPerson.Value = "Select One"
    'Dim objDt As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,INDV_Last_Name,cb_value from Vw_CB_Signatory_Person", Nothing)
    'Using db As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
    'store_SignatoryPerson.DataSource = objDt
    'store_SignatoryPerson.DataBind()

    'store_SignatoryRole.DataSource = goAML_AccountBLL.GetListPeranOrangDalamRekening()
    'store_SignatoryRole.DataBind()
    'End Using
    'Else
    'cmb_SignatoryPerson.Value = "Select One"
    'Dim objDt2 As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select CIF,INDV_Last_Name,cb_value from Vw_CB_Signatory_Not_Person", Nothing)
    'Using db As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
    'store_SignatoryPerson.DataSource = objDt2
    'store_SignatoryPerson.DataBind()

    'store_SignatoryRole.DataSource = goAML_AccountBLL.GetListPeranOrangDalamRekening()
    'store_SignatoryRole.DataBind()
    'End Using
    'End If
    'dedy end added 27082020
    'End Sub


End Class
