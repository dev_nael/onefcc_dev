﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="Account_DETAIL.aspx.vb" Inherits="Account_DETAIL" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--Pop Up--%>
    <ext:Window ID="WindowDetail" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Customer Phone Add" Hidden="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSetCustomerPhoneAdd" runat="server" Title="Phones" Collapsible="true" Layout="FitLayout">
                <Items>                
                </Items>
                <Content>
                    <%--Panel Director--%>
<%--                    <ext:FormPanel ID="FormPanelDetailDirector" runat="server" Hidden="true"  ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                            <ext:Hidden runat="server" ID="Hidden2"></ext:Hidden>
                           <ext:DisplayField ID="Name" runat="server" FieldLabel="Nama Lengkap"></ext:DisplayField>
                           <ext:DisplayField ID="Role" runat="server" FieldLabel="Role"></ext:DisplayField>
                           <ext:DisplayField ID="BirthPlace" runat="server" FieldLabel="Tempat Lahir"></ext:DisplayField>
                           <ext:DisplayField ID="BirthDate" runat="server" FieldLabel="Tanggal Lahir" ></ext:DisplayField>
                           <ext:DisplayField ID="MomName" runat="server" FieldLabel="Nama Ibu Kandung"></ext:DisplayField>
                           <ext:DisplayField ID="Aliase" runat="server" FieldLabel="Nama Alias"></ext:DisplayField>
                           <ext:DisplayField ID="SSN" runat="server" FieldLabel="NIK"></ext:DisplayField>
                           <ext:DisplayField ID="PassportNo" runat="server" FieldLabel="No. Passport" ></ext:DisplayField>
                           <ext:DisplayField ID="PassportFrom" runat="server" FieldLabel="Negara Penerbit Passport"></ext:DisplayField>
                           <ext:DisplayField ID="No_Identity" runat="server" FieldLabel="No. Identitas Lain" ></ext:DisplayField>
                           <ext:DisplayField ID="Nasionality1" runat="server" FieldLabel="Kewarganegaraan 1" ></ext:DisplayField>
                           <ext:DisplayField ID="Nasionality2" runat="server" FieldLabel="Kewarganegaraan 2" ></ext:DisplayField>
                           <ext:DisplayField ID="Nasionality3" runat="server" FieldLabel="Kewarganegaraan 3" ></ext:DisplayField>
                           <ext:DisplayField ID="Residence" runat="server" FieldLabel="Residence" ></ext:DisplayField>
                           <ext:DisplayField ID="Email" runat="server" FieldLabel="Email" ></ext:DisplayField>
                           <ext:DisplayField ID="Pekerjaan" runat="server" FieldLabel="Occupation" ></ext:DisplayField>
                           
                    <ext:GridPanel ID="GP_Director_Address" runat="server" Title="Address" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="StoreDirectorAddress" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="modelDirectorAddress" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column29" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="State" Text="Ibu Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdAddressDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GP_Director_Phone" runat="server" Title="Phone" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="StoreDirectorPhone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="modelDirectorPhone" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="column12" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column15" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="tph_extension" Text="Nomor Ekstensi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdPhoneDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                           
                            </Items>
                        <Buttons>
                            <ext:Button ID="Button2" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveAccountDirector_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>--%>
                    <%--End Panel Director--%>

                    <%--Panel Signatory--%>
                    <ext:FormPanel ID="FP_Signatory" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                            <ext:Hidden runat="server" ID="Hidden1"></ext:Hidden>
<%--                            <ext:ComboBox ID="cmb_SignatoryPerson" runat="server" FieldLabel="Pilih Customer Individu" DisplayField="cb_value" ValueField="CIF" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" QueryMode="Local" AnyMatch="true" allowBlank="false" IndicatorCls="required-text" IndicatorText="*" TypeAhead="false">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="store_SignatoryPerson">
                                        <Model>
                                            <ext:Model runat="server" ID="Model1">
                                                <Fields>
                                                    <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="cb_value" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Change OnEvent="cmb_SignatoryPerson_DirectSelect">
                                        <ExtraParams>
                                            <ext:Parameter Name="CIF" Value="CIF" Mode="Raw" />
                                        </ExtraParams>
                                    </Change>
                                </DirectEvents>
                           </ext:ComboBox>
                           <ext:Checkbox runat="server" ID="cbIsPrimary" FieldLabel="Pemilik Utama?" Checked="true"></ext:Checkbox>
                            <ext:ComboBox ID="cmb_SignatoryRole" runat="server" FieldLabel="Peran" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" ForceSelection="true" QueryMode="Local" AnyMatch="true" allowBlank="false" IndicatorCls="required-text" IndicatorText="*" TypeAhead="false">
                                <Store>
                                    <ext:Store runat="server" ID="store_SignatoryRole">
                                        <Model>
                                            <ext:Model runat="server" ID="model_SignatoryRole" IDProperty="Kode">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                </DirectEvents>
                            </ext:ComboBox>--%>

                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_TitleSignatory" runat="server" FieldLabel="Gelar" ReadOnly="true" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_Last_NameSignatory" runat="server" FieldLabel="Nama Lengkap" ReadOnly="true" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" runat="server" FieldLabel="Tanggal Lahir" ID="txt_INDV_BirthdateSignatory"/>
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_Birth_placeSignatory" runat="server" FieldLabel="Tempat Lahir" ReadOnly="true" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_Mothers_nameSignatory" runat="server" FieldLabel="Nama Ibu Kandung" ReadOnly="true" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_AliasSignatory" runat="server" FieldLabel="Nama Alias" ReadOnly="true" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_SSNSignatory" runat="server" FieldLabel="NIK" ReadOnly="true" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_Passport_numberSignatory" runat="server" FieldLabel="No. Passport" ReadOnly="true" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_Passport_countrySignatory" runat="server" FieldLabel="Negara Penerbit Passport" ReadOnly="true" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_ID_NumberSignatory" runat="server" FieldLabel="No. Identitas Lain" ReadOnly="true" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_Nationality1Signatory" runat="server" FieldLabel="Kewarganegaraan 1" ReadOnly="true" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_Nationality2Signatory" runat="server" FieldLabel="Kewarganegaraan 2" ReadOnly="true" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_Nationality3Signatory" runat="server" FieldLabel="Kewarganegaraan 3" ReadOnly="true" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_ResidenceSignatory" runat="server" FieldLabel="Residence" ReadOnly="true" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_EmailSignatory" runat="server" FieldLabel="Email" ReadOnly="true" ></ext:TextField>
                            <ext:Checkbox runat="server" ReadOnly="true" ID="cb_DeceasedSignatory" FieldLabel="Deceased ?">
                                    <DirectEvents>
                                        <Change OnEvent="checkBoxcb_Deceased_click" />
                                    </DirectEvents>
                                </ext:Checkbox>
                                <ext:DateField runat="server" Hidden="true" FieldLabel="Deceased Date" Format="dd-MMM-yyyy" ID="txt_deceased_dateSignatory" AnchorHorizontal="90%"/>
                            <ext:Checkbox runat="server" ReadOnly="true"  ID="cb_taxSignatory" FieldLabel="PEP ?">
                        
                                </ext:Checkbox>
                                <ext:TextField runat="server" ReadOnly="true" FieldLabel="NPWP" ID="txt_INDV_Tax_NumberSignatory" AnchorHorizontal="90%"/>
                                <ext:TextField runat="server" ReadOnly="true" FieldLabel="Source of Wealth" ID="txt_INDV_Source_of_WealthSignatory" AnchorHorizontal="90%"/>
                            <ext:TextField runat="server" ReadOnly="true" FieldLabel="Catatan" ID="txt_INDV_CommentsSignatory" AnchorHorizontal="90%"/>   
                           <ext:TextField AnchorHorizontal="90%" ID="txt_INDV_OccupationSignatory" runat="server" FieldLabel="Occupation" ReadOnly="true" ></ext:TextField>
                         
                            <%--Address Signatory--%>                    
                            <ext:GridPanel ID="GP_AddressSignatory" runat="server" Title="Informasi Alamat" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                                <Store>
                                    <ext:Store ID="StoreAddressSignatory" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="ModelAddressSignatory" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column19" runat="server" DataIndex="Type_Address_Detail" Text="Type Address" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column18" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column6" runat="server" DataIndex="Town" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                       <ext:Column ID="Column28" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                       <ext:Column ID="Column4" runat="server" DataIndex="Zip" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column31" runat="server" DataIndex="Country" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column20" runat="server" DataIndex="State" Text="Ibu Kota" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column21" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdAddressSignatory">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--Phone Signatory--%>                    
                            <ext:GridPanel ID="GP_PhoneSignatory" runat="server" Title="Informasi Telepon" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                                <Store>
                                    <ext:Store ID="StorePhoneSignatory" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="ModelPhoneSignatory" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="column22" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column23" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column24" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column25" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column26" runat="server" DataIndex="tph_extension" Text="Nomor Ekstensi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column27" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn4" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdPhoneSignatory">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--End of Phone Signatory--%>
                           <ext:GridPanel ID="GP_EmployerAddressSignatory" runat="server" Title="Informasi Alamat Tempat Bekerja" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                                <Store>
                                    <ext:Store ID="StoreEmployerAddressSignatory" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column13" runat="server" DataIndex="Type_Address_Detail" Text="Type Address" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column14" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column15" runat="server" DataIndex="Town" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                       <ext:Column ID="Column16" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                       <ext:Column ID="Column17" runat="server" DataIndex="Zip" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column29" runat="server" DataIndex="Country" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column32" runat="server" DataIndex="State" Text="Ibu Kota" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column33" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdEmployerAddressSignatory">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--Phone Signatory--%>                    
                            <ext:GridPanel ID="GP_EmployerPhoneSignatory" runat="server" Title="Informasi Telepon Tempat Bekerja" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                                <Store>
                                    <ext:Store ID="StoreEmployerPhoneSignatory" runat="server" IsPagingStore="true" PageSize="8">
                                        <Model>
                                            <ext:Model ID="Model2" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                        <ext:Column ID="column34" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column35" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column36" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column37" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column38" runat="server" DataIndex="tph_extension" Text="Nomor Ekstensi" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:Column ID="Column39" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                        <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                            <Commands>
                                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                            </Commands>
                                            <DirectEvents>
                                                <Command OnEvent="GrdCmdEmployerPhoneSignatory">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                                    </Confirmation>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                            <%--End of Phone Signatory--%>

                        </Items>
                         <Buttons>
                            <ext:Button ID="Button3" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveAccountSignatory_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                        </ext:FormPanel>
                    <%--End Panel Signatory--%>

                    <ext:FormPanel ID="FormPanelTaskDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" >
                        <Items>
                            <ext:Hidden runat="server" ID="fld_sender"></ext:Hidden>
                            <%--<ext:TextField AnchorHorizontal="90%" ID="txtCet" runat="server" LabelWidth="180" FieldLabel="CET No" AnchorHorizontal="90%"></ext:TextField>--%>
                            <ext:ComboBox  ID="cb_phone_Contact_Type" runat="server" FieldLabel="Kategori Kontak" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" Editable="false" ReadOnly="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Contact_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model_Contact_Type">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                           </ext:ComboBox>
                            <ext:ComboBox ID="cb_phone_Communication_Type" runat="server" FieldLabel="Jenis Alat Komunikasi" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" Editable="false" ReadOnly="true">
                                <Store>
                                    <ext:Store runat="server" ClientIDMode="Static" ID="Store_Communication_Type">
                                        <Model>
                                            <ext:Model runat="server" ID="Model_Comminication_Type">
                                                <Fields>
                                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                                </DirectEvents>
                           </ext:ComboBox>

                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="txt_phone_Country_prefix" runat="server" FieldLabel="Kode Area Telp" ></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="txt_phone_number" runat="server" FieldLabel="Nomor Telepon" ></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="txt_phone_extension" runat="server" FieldLabel="Nomor Ekstensi" ></ext:TextField>
                            <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="txt_phone_comments" runat="server" FieldLabel="Catatan" ></ext:TextField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSavedetail" runat="server" Text="Save" DefaultAlign="center">
                                <%--<DirectEvents>
                                    <Click OnEvent="btnSavedetail_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>--%>
                            </ext:Button>
                            <ext:Button ID="btnBackSaveDetail" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveCustomerPhones_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>

                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>
    <%--Pop Up--%>

            <ext:Window ID="WindowAddressPhone" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Detail" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSetAddressPhone" runat="server" Title="Director" Collapsible="true" Layout="FitLayout">
                <Items>                
                </Items>
                <Content>
                    <%--Panel Address--%>
                    <ext:FormPanel ID="FP_Address" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="JenisAlamat" runat="server" FieldLabel="Jenis Tempat Tinggal"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Alamat" runat="server" FieldLabel="Alamat"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Kecamatan" runat="server" FieldLabel="Kecamatan"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Kota" runat="server" FieldLabel="Kota" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Kode_Pos" runat="server" FieldLabel="Kode Pos"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Kode_Negara" runat="server" FieldLabel="Kode Negara"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Ibu_Kota" runat="server" FieldLabel="Ibu Kota"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Catatan" runat="server" FieldLabel="Catatan" ></ext:TextField>                           
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button6" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveAccountAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <%--End Panel Address--%>

                    <%--Panel Phone--%>
                    <ext:FormPanel ID="FP_Phone" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="KategoriKontak" runat="server" FieldLabel="Kategori Kontak"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="JenisAlatKomunikasi" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="KodeAreaTelp" runat="server" FieldLabel="Kode Area Telp"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="NomorTelepon" runat="server" FieldLabel="Nomor Telepon" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="NomorEkstensi" runat="server" FieldLabel="Nomor Ekstensi"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Catatan_Phone" runat="server" FieldLabel="Catatan" ></ext:TextField>                           
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button7" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackSaveAccountPhone_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <%--End Panel Phone--%>
                    

                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>


        <ext:Window ID="WindowAddressandPhoneDirector" Modal="true" runat="server" Icon="ApplicationViewDetail" Title="Detail" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldAddressPhone" runat="server" Title="Director" Collapsible="true" Layout="FitLayout">
                <Items>                
                </Items>
                <Content>
                    
                     <%--Panel Address Director--%>
                    <ext:FormPanel ID="FP_Address_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="JenisAlamatDirector" runat="server" FieldLabel="Jenis Tempat Tinggal"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="AlamatDirector" runat="server" FieldLabel="Alamat"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="KecamatanDirector" runat="server" FieldLabel="Kecamatan"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="KotaDirector" runat="server" FieldLabel="Kota" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Kode_PosDirector" runat="server" FieldLabel="Kode Pos"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Kode_NegaraDirector" runat="server" FieldLabel="Kode Negara"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Ibu_KotaDirector" runat="server" FieldLabel="Ibu Kota"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="CatatanAddressDirector" runat="server" FieldLabel="Catatan" ></ext:TextField>                           
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button4" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackPhoneandAddress">
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <%--End Panel Address Director--%>

                    <%--Panel Phone Director--%>
                    <ext:FormPanel ID="FP_Phone_Director" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Items>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="KategoriKontakDirector" runat="server" FieldLabel="Kategori Kontak"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="JenisAlatKomunikasiDirector" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="KodeAreaTelpDirector" runat="server" FieldLabel="Kode Area Telp"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="NomorTeleponDirector" runat="server" FieldLabel="Nomor Telepon" ></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="NomorEkstensiDirector" runat="server" FieldLabel="Nomor Ekstensi"></ext:TextField>
                           <ext:TextField AnchorHorizontal="90%" ReadOnly="true" ID="Catatan_PhoneDirector" runat="server" FieldLabel="Catatan" ></ext:TextField>                           
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button5" runat="server" Text="Back" DefaultAlign="center">
                                <DirectEvents>
                                    <Click OnEvent="btnBackPhoneandAddress">                                        
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <%--End Panel Phone Director--%>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:FormPanel ID="FormPanelInput" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="Account Detail">
        <Items>
            <ext:DisplayField runat="server" LabelWidth="300" FieldLabel="Account No" ID="txt_AccountNo" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Client Number" ID="txt_CIF" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Currency Code" ID="txt_CurrencyCode" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Branch" ID="txt_Branch" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Account Name" ID="txt_AccountName" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="IBAN" ID="txt_IBAN" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Personal Account Type" ID="txt_PersonalAccountType" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tanggal Pembukaan Rekening" ID="txt_TanggalPembukaanRekening" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tanggal Penutupan Rekening" ID="txt_TanggalPenutupanRekening" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Saldo Akhir" ID="txt_SaldoAkhir" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tanggal Saldo" ID="txt_TanggalSaldo" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Status Rekening" ID="txt_StatusRekening" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Nama Penerima Manfaat Utama" ID="txt_NamaPenerimaManfaatUtama" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Catatan Terkait Penerima Manfaat Utama" ID="txt_CatatanTerkaitPenerimaManfaatUtama" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Catatan" ID="txt_Catatan" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Update From Data Source?" ID="txt_UpdateFromDataSource" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Rekening Korporasi?" ID="txt_RekeningKorporasi" />



            
            <%--dikomen--%>
                    
<%--            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tanggal Pembukaan Rekening" ID="txt_Opened" ReadOnly="true"/>
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tanggal Penutupan Rekening" ID="txt_Closed" ReadOnly="true"/>
            <ext:ComboBox ID="Cmb_gender" LabelWidth="250" runat="server" FieldLabel="Gender" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" QueryMode="Local" AnyMatch="true" allowBlank="false" IndicatorCls="required-text" IndicatorText="*" TypeAhead="false" ReadOnly="true">
                <Store>
                    <ext:Store runat="server" ID="StoreGender">
                        <Model>
                            <ext:Model runat="server" ID="ModelGender" IDProperty="Kode">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
<%--                </DirectEvents>
            </ext:ComboBox>
            <ext:NumberField runat="server" LabelWidth="250" FieldLabel="Saldo Akhir" ID="txt_Balance" DecimalPrecision="2"/>
            <ext:DateField runat="server" LabelWidth="250" FieldLabel="Tanggal Saldo" ID="txt_Balance_Date" Format="dd-MMM-yyyy" ReadOnly="true"/>
            <ext:ComboBox ID="Cmb_StatusCode" LabelWidth="250" runat="server" FieldLabel="Status Rekening" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" Editable="false" ReadOnly="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreStatusCode">
                        <Model>
                            <ext:Model runat="server" ID="ModelStatusCode">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>

                <DirectEvents>--%>
                    <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
<%--                </DirectEvents>
            </ext:ComboBox>
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Nama Penerima Manfaat Utama" ID="txt_beneficiary"/>
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Catatan Terkait Penerima Manfaat Utama" ID="txt_beneficiary_comment" />
            <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Catatan" ID="txt_comments" />--%>
            <%--End dikomen--%>
            <%--bikin panel buat Individu ama Korporasi--%>
            
            <ext:Panel runat="server" Title="Korporasi" Collapsible="true" AnchorHorizontal="100%" ID="panel_INDV" Hidden="true" Border="true" BodyStyle="padding:10px">
                <Items>
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Gelar" ID="txt_INDV_Title" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Nama Lengkap" ID="txt_INDV_Last_Name" />
                   <ext:DateField LabelWidth="250" runat="server" FieldLabel="Tanggal Lahir" ID="txt_INDV_Birthdate" Format="dd-MMM-yyyy" ReadOnly="true"/>
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Tempat Lahir" ID="txt_INDV_Birth_place" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Nama Ibu Kandung" ID="txt_INDV_Mothers_name" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Nama Alias" ID="txt_INDV_Alias" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="NIK" ID="txt_INDV_SSN" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="No. Passport" ID="txt_INDV_Passport_number" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="Negara Penerbit Passport" ID="txt_INDV_Passport_country" />
                   <ext:DisplayField LabelWidth="250" runat="server" FieldLabel="No. Identitas Lain" ID="txt_INDV_ID_Number" />
                   <ext:ComboBox LabelWidth="250" ID="cmb_INDV_Nationality1" runat="server" FieldLabel="Kewarganegaraan 1" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" readonly="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreNationality1">
                                <Model>
                                    <ext:Model runat="server" ID="ModelNationality1">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                        </DirectEvents>
                   </ext:ComboBox>
                   <ext:ComboBox ID="cmb_INDV_Nationality2" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 2" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" Editable="false" ReadOnly="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreNationality2">
                                <Model>
                                    <ext:Model runat="server" ID="ModelNationality2">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                        </DirectEvents>
                   </ext:ComboBox>
                    <ext:ComboBox ID="cmb_INDV_Nationality3" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 3" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" Editable="false" ReadOnly="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreNationality3">
                                <Model>
                                    <ext:Model runat="server" ID="ModelNationality3">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                        </DirectEvents>
                   </ext:ComboBox>
                     <ext:ComboBox ID="cmb_INDV_Residence" LabelWidth="250" runat="server" FieldLabel="Negara Domisili" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="70%" ForceSelection="true" Editable="false" ReadOnly="true">
                        <Store>
                            <ext:Store runat="server" ClientIDMode="Static" ID="StoreResidence">
                                <Model>
                                    <ext:Model runat="server" ID="ModelResidence">
                                        <Fields>
                                            <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                            <%--<Change OnEvent="Cmb_jenisLaporan_DirectSelect"></Change>--%>
                        </DirectEvents>
                   </ext:ComboBox>

                <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Email" ID="txt_INDV_Email" />
                <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Occupation" ID="txt_INDV_Occupation" />
                <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tempat Bekerja" ID="txt_INDV_Employer_Name" />

                <ext:GridPanel ID="gp_INDV_Phones" runat="server" Title="Informasi Telepon" AutoScroll="true">
                <TopBar>
                    <ext:Toolbar ID="toolbar" runat="server">
                        <%--<Items>
                            <ext:Button ID="btnAdd_INDV_Phones" runat="server" Text="Add Phones" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btnAddCustomerPhones_DirectClick">
                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="200"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>--%>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="Store_INDV_Phones" runat="server" IsPagingStore="true" PageSize="8">
                        <Model>
                            <ext:Model ID="Model_INDV_Phones" runat="server">
                                <Fields>
                                    <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumber_INDV_Phones" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                        <%--<ext:Column ID="colPK_goAML_Ref_Phone" runat="server" DataIndex="PK_goAML_Ref_Phone" Text="PK" MinWidth="130" Flex="1" Visible ="false"></ext:Column>--%>
                        <ext:Column ID="coltph_contact_type" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="coltph_communication_type" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="coltph_country_prefix" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="coltph_number" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Coltph_extension" runat="server" DataIndex="tph_extension" Text="Nomor Extensi" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="ColComments" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
<%--                        <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                            <Commands>
                        
                                <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                               
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GrdCmdCustPhone">
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                    <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                    </Confirmation>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>--%>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

                </Items>
            </ext:Panel>

            <ext:Panel runat="server" Title="Data Korporasi" Collapsible="true" AnchorHorizontal="100%" ID="panel_Corp">
                <Items>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Nama Korporasi" ID="txt_Corp_Name" PaddingSpec="0 10 0 0" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Nama Komersial" ID="txt_Corp_Commercial_Name" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Bentuk Korporasi" ID="txt_Corp_Incorporation_Legal_Form" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Nomor Induk Berusaha" ID="txt_Corp_Incorporation_Number" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Bidang Usaha" ID="txt_Corp_Business" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Email Korporasi" ID="txt_Corp_Email" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Website Korporasi" ID="txt_Corp_url" /> 
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Provinsi" ID="txt_Corp_incorporation_state" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Negara" ID="txt_Corp_incorporation_country_code" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Peran" ID="txt_Corp_Role" />        
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tanggal Pendirian" ID="txt_Corp_incorporation_date" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tutup ?" ID="txt_Bussiness_close" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Tutup ?" ID="txt_Corp_date_business_closed" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="NPWP" ID="txt_Corp_tax_number" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Catatan" ID="txt_Corp_Comments" />
                    <%--Address--%>                    
                    <ext:GridPanel ID="GP_Address" runat="server" Title="Alamat" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="store_address" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="modelAddress" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address_Detail" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Town" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Zip" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="State" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column30" runat="server" DataIndex="Type_Address_Detail" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column5" runat="server" DataIndex="Address" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="Town" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="City" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="Zip" Text="Ibu Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="Country" Text="Ibu Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="State" Text="Ibu Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="Comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cmdAddress" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdAddressKantor">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--End of Address--%>

                    <%--Phone--%>                    
                    <ext:GridPanel ID="GDPhone" runat="server" Title="Telepon" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="store_phone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="model_phone" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communcation_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="column1" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="Communcation_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column3" runat="server" DataIndex="tph_country_prefix" Text="Kode Area Telp" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnNomorTelepon" runat="server" DataIndex="tph_number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnNomorEkstensi" runat="server" DataIndex="tph_extension" Text="Nomor Ekstensi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnCatatan" runat="server" DataIndex="comments" Text="Catatan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhone" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdPhoneKantor">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--End of Phone--%>
                    
                    <%--Director--%>
<%--                    <ext:GridPanel ID="gp_Director" runat="server" Title="Director" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                       
                        <Store>
                            <ext:Store ID="store_Director" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="model_Director" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Entity_Director_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Role" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumber_Director" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="columnDirectorName" runat="server" DataIndex="LastName" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnDirectorRole" runat="server" DataIndex="Role" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cmdDirectory" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>--%>
                    <%--End of Director--%>
                </Items>
            </ext:Panel>
            
            <%--Signatory--%>
                    <ext:GridPanel ID="GDSignatory" runat="server" Title="Signatory" AutoScroll="true" Border="true" BodyStyle="padding:10px">
                        <Store>
                            <ext:Store ID="store_Signatory" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="model_Signatory" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Signatory_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Role" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Primary" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererPhone" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="columnKategoriKontak" runat="server" DataIndex="Name" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnJenisAlatKomunikasi" runat="server" DataIndex="Role" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="ColumnKodeArea" runat="server" DataIndex="Primary" Text="Primary" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cmdSignatory" runat="server" Text="Action" Flex="1" MinWidth="200">
                                <Commands>
                                    <%--<ext:GridCommand CommandName="Download" Icon="DiskDownload"  ></ext:GridCommand>--%>
                                    <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="GrdCmdSignatory">
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="record.data.PK_Signatory_ID" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                        <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                        </Confirmation>
                                    </Command>
                                </DirectEvents>
                            </ext:CommandColumn>
<%--                                <ext:CommandColumn ID="CommandColumnPhone" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdPhoneKantor">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>--%>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--End of Phone--%>
                <%--End of Signatory--%>


            
             
         <%--   <ext:Panel runat="server" Title="Notes History" Collapsible="true" AnchorHorizontal="100%">
                <Items>
                    <ext:GridPanel ID="gp_history" runat="server">
                        <Store>
                            <ext:Store ID="gpStore_history" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" IDProperty="CIF">
                                        <Fields>
                                            <ext:ModelField Name="PK_TX_IFTI_Flow_History_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Module_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UnikID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="UserID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UserName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="RoleName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Notes" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>

                        <ColumnModel runat="server">
                            <Columns>
                                <ext:RowNumbererColumn runat="server" Text="No" CellWrap="true" Width="70"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="User Name" DataIndex="UserName" CellWrap="true" flex="1" />
                                <ext:DateColumn runat="server" Text="Created Date" DataIndex="CreatedDate" CellWrap="true" flex="1" Format="dd-MMM-yyyy" />
                                <ext:Column runat="server" Text="Status" DataIndex="Status" CellWrap="true" Flex="1" />
                                <ext:Column runat="server" Text="Notes" DataIndex="Notes" CellWrap="true" flex="4" />
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar14" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>--%>
        </Items>

        <Buttons>

            <ext:Button ID="btnCancelIncoming" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="BtnBack_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

</asp:Content>

