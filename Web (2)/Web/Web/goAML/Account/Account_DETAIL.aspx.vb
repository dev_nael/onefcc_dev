﻿Imports System.Data
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL
Partial Class Account_DETAIL
    Inherits ParentPage
    Public objFormModuleView As NawaBLL.FormModuleDetail

    Private mModuleName As String

    Public Property ModulName() As String
        Get
            Return mModuleName
        End Get
        Set(ByVal value As String)
            mModuleName = value
        End Set
    End Property
    Sub ClearSession()
        objListgoAML_Ref_Phone = Nothing
    End Sub

    Public Property objListgoAML_Ref_Address() As List(Of NawaDevDAL.goAML_Ref_Address)
        Get
            Return Session("Account_DETAIL.objListgoAML_Ref_Address")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Ref_Address))
            Session("Account_DETAIL.objListgoAML_Ref_Address") = value
        End Set
    End Property

    Public Property objTempCustomerAddresslEdit() As NawaDevDAL.goAML_Ref_Address
        Get
            Return Session("Account_DETAIL.objTempCustomerAddresslEdit")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Address)
            Session("Account_DETAIL.objTempCustomerAddresslEdit") = value
        End Set
    End Property

    Public Property objListgoAML_Ref_Phone() As List(Of NawaDevDAL.Vw_Customer_Phones)
        Get
            Return Session("Account_DETAIL.objListgoAML_Ref_Phone")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.Vw_Customer_Phones))
            Session("Account_DETAIL.objListgoAML_Ref_Phone") = value
        End Set
    End Property

    Public Property objTempCustomerPhoneEdit() As NawaDevDAL.goAML_Ref_Phone
        Get
            Return Session("Account_DETAIL.objTempCustomerPhoneEdit")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Phone)
            Session("Account_DETAIL.objTempCustomerPhoneEdit") = value
        End Set
    End Property

    Public Property obj_Account As NawaDevDAL.goAML_Ref_Account
        Get
            Return Session("Account_DETAIL.obj_Account")
        End Get
        Set(value As NawaDevDAL.goAML_Ref_Account)
            Session("Account_DETAIL.obj_Account") = value
        End Set
    End Property


    Protected Sub BtnBack_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Private Function GetId() As String
        Dim strid As String = Request.Params("ID")
        Dim id As String = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
        Return id
    End Function

    Protected Sub btnAddCustomerPhones_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            btnSavedetail.Hidden = False
            WindowDetail.Title = "Customer Phone Add"

            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveCustomerPhones_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True
            ClearinputCustomerPhones()
            objTempCustomerPhoneEdit = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ClearinputCustomerPhones()

        txt_phone_Country_prefix.Text = ""
        txt_phone_number.Text = ""
        txt_phone_extension.Text = ""
        txt_Catatan.Text = ""

    End Sub

    Private Sub Account_Detail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            ClearSession()
            GridPanelSetting()
            Maximizeable()
            Dim strid As String = GetId()
            Dim strModuleid As String = Request.Params("ModuleID")
            Dim INDV_gender As NawaDevDAL.goAML_Ref_Jenis_Kelamin = Nothing
            Dim INDV_statusCode As NawaDevDAL.goAML_Ref_Status_Rekening = Nothing
            Dim INDV_nationality1 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_nationality2 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_nationality3 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
            Dim INDV_residence As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
            Dim CORP_Incorporation_legal_form As NawaDevDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
            Dim CORP_incorporation_country_code As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
            Dim CORP_Role As NawaDevDAL.goAML_Ref_Party_Role = Nothing

            Try
                obj_Account = goAML_AccountBLL.GetAccountByAccountNo(strid)

                'Dim obj_Account_Jenis_Rekening As NawaDevDAL.goAML_Ref_Jenis_Rekening = goAML_AccountBLL.GetJenisRekeningByID(obj_Account.personal_account_type)
                'Dim obj_Account_Status_Rekening As NawaDevDAL.goAML_Ref_Status_Rekening = goAML_CustomerBLL.GetStatusRekeningbyID(obj_Account.status_code)
                'Dim custType As NawaDevDAL.IFTI_NasabahType

                'dedy added 01092020
                If obj_Account IsNot Nothing Then
                    Dim obj_Account_Jenis_Rekening As NawaDevDAL.goAML_Ref_Jenis_Rekening = goAML_AccountBLL.GetJenisRekeningByID(obj_Account.personal_account_type)
                    Dim obj_Account_Status_Rekening As NawaDevDAL.goAML_Ref_Status_Rekening = goAML_CustomerBLL.GetStatusRekeningbyID(obj_Account.status_code)

                    If obj_Account.Account_No IsNot Nothing Then
                        txt_AccountNo.Text = obj_Account.Account_No
                    Else
                        txt_AccountNo.Text = Nothing
                    End If
                    If obj_Account.Currency_Code IsNot Nothing Then
                        txt_CurrencyCode.Text = obj_Account.Currency_Code
                    Else
                        txt_CurrencyCode.Text = Nothing
                    End If
                    If obj_Account.Branch IsNot Nothing Then
                        txt_Branch.Text = obj_Account.Branch
                    Else
                        txt_Branch.Text = Nothing
                    End If
                    If obj_Account.Account_Name IsNot Nothing Then
                        txt_AccountName.Text = obj_Account.Account_Name
                    Else
                        txt_AccountName.Text = Nothing
                    End If
                    If obj_Account.IBAN IsNot Nothing Then
                        txt_IBAN.Text = obj_Account.IBAN
                    Else
                        txt_IBAN.Text = Nothing
                    End If
                    If obj_Account_Jenis_Rekening IsNot Nothing Then
                        txt_PersonalAccountType.Text = obj_Account_Jenis_Rekening.Keterangan
                    Else
                        txt_PersonalAccountType.Text = Nothing
                    End If
                    'txt_PersonalAccountType.Text = obj_Account.personal_account_type

                    If obj_Account.balance IsNot Nothing Then
                        txt_SaldoAkhir.Text = obj_Account.balance
                    End If
                    'txt_StatusRekening.Text = obj_Account.status_code
                    If obj_Account_Status_Rekening Is Nothing Then
                        txt_StatusRekening.Text = Nothing
                    Else
                        txt_StatusRekening.Text = obj_Account_Status_Rekening.Keterangan
                    End If

                    txt_NamaPenerimaManfaatUtama.Text = obj_Account.beneficiary
                        txt_CatatanTerkaitPenerimaManfaatUtama.Text = obj_Account.beneficiary_comment
                        txt_Catatan.Text = obj_Account.comments


                        If Not obj_Account.isUpdateFromDataSource = 0 Then
                            txt_UpdateFromDataSource.Text = "Ya"
                        Else
                            txt_UpdateFromDataSource.Text = "Tidak"
                        End If


                        If obj_Account.FK_CIF_Entity_ID IsNot Nothing And Not obj_Account.FK_CIF_Entity_ID = "" Then

                        Dim obj_Account_Entity = goAML_AccountBLL.GetCustomerByCIF(obj_Account.FK_CIF_Entity_ID)
                        If obj_Account_Entity IsNot Nothing Then
                            txt_RekeningKorporasi.Text = "Ya"
                            panel_Corp.Hidden = False
                            store_phone.DataSource = goAML_AccountBLL.GetVw_Customer_Phones(obj_Account_Entity.PK_Customer_ID)
                            store_phone.DataBind()
                            store_address.DataSource = goAML_AccountBLL.GetVw_Customer_Addresses(obj_Account_Entity.PK_Customer_ID)
                            store_address.DataBind()
                        Else
                            txt_RekeningKorporasi.Text = "Ya"
                            panel_Corp.Hidden = False
                        End If

                    Else
                            txt_RekeningKorporasi.Text = "Tidak"
                            panel_Corp.Hidden = True
                        End If

                        If obj_Account.client_number IsNot Nothing Then
                            txt_CIF.Text = obj_Account.client_number
                        Else
                            txt_CIF.Text = ""
                        End If

                        If obj_Account.date_balance.HasValue Then
                            txt_TanggalSaldo.Text = obj_Account.date_balance.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            txt_TanggalSaldo.Text = ""
                        End If
                        If obj_Account.opened.HasValue Then
                            txt_TanggalPembukaanRekening.Text = obj_Account.opened.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            txt_TanggalPembukaanRekening.Text = ""
                        End If

                        If obj_Account.closed.HasValue Then
                            txt_TanggalPenutupanRekening.Text = obj_Account.closed.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                        Else
                            txt_TanggalPenutupanRekening.Text = ""
                        End If

                        Dim obj_Entity = goAML_AccountBLL.GetCustomerByCIF(obj_Account.FK_CIF_Entity_ID)
                        If obj_Entity IsNot Nothing Then
                            Dim obj_Entity_Nama_Negara As NawaDevDAL.goAML_Ref_Nama_Negara = goAML_CustomerBLL.GetNamaNegaraByID(obj_Entity.Corp_Incorporation_Country_Code)

                            txt_Corp_Name.Text = obj_Entity.Corp_Name
                            txt_Corp_Commercial_Name.Text = obj_Entity.Corp_Commercial_Name
                            If Not obj_Entity.Corp_Incorporation_Legal_Form = "" Then
                                txt_Corp_Incorporation_Legal_Form.Text = goAML_CustomerBLL.GetBentukBadanUsahaByID(obj_Entity.Corp_Incorporation_Legal_Form).Keterangan
                            End If
                            txt_Corp_Incorporation_Number.Text = obj_Entity.Corp_Incorporation_Number
                            txt_Corp_Business.Text = obj_Entity.Corp_Business
                            txt_Corp_Email.Text = obj_Entity.Corp_Email
                            txt_Corp_url.Text = obj_Entity.Corp_Url
                            txt_Corp_incorporation_state.Text = obj_Entity.Corp_Incorporation_State

                        'txt_Corp_incorporation_country_code.Text = obj_Entity.Corp_Incorporation_Country_Code
                        If obj_Entity_Nama_Negara IsNot Nothing Then
                            txt_Corp_incorporation_country_code.Text = obj_Entity_Nama_Negara.Keterangan
                        Else
                            txt_Corp_incorporation_country_code.Text = Nothing
                        End If


                        If Not String.IsNullOrEmpty(obj_Entity.Corp_Role) Then
                                txt_Corp_Role.Text = goAML_CustomerBLL.GetPartyRolebyID(obj_Entity.Corp_Role).Keterangan
                            Else
                                txt_Corp_Role.Text = ""
                            End If

                            If obj_Entity.Corp_Incorporation_Date.HasValue Then
                                txt_Corp_incorporation_date.Text = obj_Entity.Corp_Incorporation_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                            Else
                                txt_Corp_incorporation_date.Text = ""
                            End If
                            If obj_Entity.Corp_Business_Closed IsNot Nothing Then
                                txt_Bussiness_close.Value = "Yes"
                            Else
                                txt_Bussiness_close.Value = "No"
                            End If
                            If obj_Entity.Corp_Date_Business_Closed.HasValue Then
                                txt_Corp_date_business_closed.Text = obj_Entity.Corp_Date_Business_Closed.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                            Else
                                txt_Corp_date_business_closed.Text = ""
                            End If
                            txt_Corp_tax_number.Text = obj_Entity.Corp_Tax_Number
                            txt_Corp_Comments.Text = obj_Entity.Corp_Comments
                        End If

                        store_Signatory.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from vw_account_signatory where AccountNo ='" & obj_Account.Account_No & "'", Nothing)
                        store_Signatory.DataBind()

                    End If

                'store_Signatory.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from vw_account_signatory where AccountNo ='" & obj_Account.Account_No & "'", Nothing)
                'store_Signatory.DataBind()

            Catch ex As Exception
                Throw ex
            End Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Sub GridPanelSetting()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 2 Then
            GP_Address.ColumnModel.Columns.RemoveAt(GP_Address.ColumnModel.Columns.Count - 1)
            GP_Address.ColumnModel.Columns.Insert(1, cmdAddress)
            GDPhone.ColumnModel.Columns.RemoveAt(GDPhone.ColumnModel.Columns.Count - 1)
            GDPhone.ColumnModel.Columns.Insert(1, CommandColumnPhone)
            GDSignatory.ColumnModel.Columns.RemoveAt(GDSignatory.ColumnModel.Columns.Count - 1)
            GDSignatory.ColumnModel.Columns.Insert(1, cmdSignatory)
            GP_AddressSignatory.ColumnModel.Columns.RemoveAt(GP_AddressSignatory.ColumnModel.Columns.Count - 1)
            GP_AddressSignatory.ColumnModel.Columns.Insert(1, CommandColumn1)
            GP_PhoneSignatory.ColumnModel.Columns.RemoveAt(GP_PhoneSignatory.ColumnModel.Columns.Count - 1)
            GP_PhoneSignatory.ColumnModel.Columns.Insert(1, CommandColumn4)

            GP_EmployerAddressSignatory.ColumnModel.Columns.RemoveAt(GP_EmployerAddressSignatory.ColumnModel.Columns.Count - 1)
            GP_EmployerAddressSignatory.ColumnModel.Columns.Insert(1, CommandColumn2)
            GP_EmployerPhoneSignatory.ColumnModel.Columns.RemoveAt(GP_EmployerPhoneSignatory.ColumnModel.Columns.Count - 1)
            GP_EmployerPhoneSignatory.ColumnModel.Columns.Insert(1, CommandColumn3)

        End If

    End Sub

    Sub Maximizeable()
        WindowAddressandPhoneDirector.Maximizable = True
        WindowAddressPhone.Maximizable = True
        WindowDetail.Maximizable = True
    End Sub


    Protected Sub GrdCmdCustPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdSignatory(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAccountSignatory(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailAccountSignatory(id As Long)
        'Dim objListgoAML_Ref_Account_Signatory = goAML_AccountBLL.GetAccount_Signatory
        Dim objAccountSignatoryDetail As goAML_Ref_Account_Signatory = goAML_AccountBLL.GetAccountSignatorybyID(id)
        Dim objAccountSignatoryDetailCustomer As goAML_Ref_Customer
        Dim objAccountSignatoryDetailWic As goAML_Ref_WIC
        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        objAccountSignatoryDetailCustomer = goAML_AccountBLL.GetCustomerByCIF(objAccountSignatoryDetail.FK_CIF_Person_ID)
        objAccountSignatoryDetailWic = goAML_AccountBLL.GetWicByCIF(objAccountSignatoryDetail.WIC_No)

        If Not objAccountSignatoryDetail Is Nothing Then
            FP_Signatory.Hidden = False
            WindowDetail.Hidden = False
            btnSavedetail.Hidden = True

            WindowDetail.Title = "Account Signatory"
            FieldSetCustomerPhoneAdd.Title = "Signatory"
            ClearinputAccount()
            With objAccountSignatoryDetail
                If objAccountSignatoryDetailCustomer IsNot Nothing Then
                    txt_INDV_TitleSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Title
                    txt_INDV_Last_NameSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Last_Name
                    If obj_Account.date_balance.HasValue Then
                        txt_INDV_BirthdateSignatory.Text = objAccountSignatoryDetailCustomer.INDV_BirthDate.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    Else
                        txt_INDV_BirthdateSignatory.Text = ""
                    End If
                    txt_INDV_Birth_placeSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Birth_Place
                    txt_INDV_Mothers_nameSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Mothers_Name
                    txt_INDV_AliasSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Alias
                    txt_INDV_SSNSignatory.Text = objAccountSignatoryDetailCustomer.INDV_SSN
                    txt_INDV_Passport_numberSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Passport_Number
                    If Not String.IsNullOrEmpty(objAccountSignatoryDetailCustomer.INDV_Passport_Country) Then
                        txt_INDV_Passport_countrySignatory.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Passport_Country).Keterangan
                    Else
                        txt_INDV_Passport_countrySignatory.Text = ""
                    End If
                    txt_INDV_ID_NumberSignatory.Text = objAccountSignatoryDetailCustomer.INDV_ID_Number
                    If Not objAccountSignatoryDetailCustomer.INDV_Nationality1 = "" And Not objAccountSignatoryDetailCustomer.INDV_Nationality1 Is Nothing Then
                        txt_INDV_Nationality1Signatory.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Nationality1).Keterangan
                    End If
                    If Not objAccountSignatoryDetailCustomer.INDV_Nationality2 = "" And Not objAccountSignatoryDetailCustomer.INDV_Nationality2 Is Nothing Then
                        txt_INDV_Nationality2Signatory.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Nationality2).Keterangan
                    End If
                    If Not objAccountSignatoryDetailCustomer.INDV_Nationality3 = "" And Not objAccountSignatoryDetailCustomer.INDV_Nationality3 Is Nothing Then
                        txt_INDV_Nationality3Signatory.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Nationality3).Keterangan
                    End If
                    If Not objAccountSignatoryDetailCustomer.INDV_Residence = "" And Not objAccountSignatoryDetailCustomer.INDV_Residence Is Nothing Then
                        txt_INDV_ResidenceSignatory.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailCustomer.INDV_Residence).Keterangan
                    End If

                    txt_INDV_EmailSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Email
                    txt_INDV_OccupationSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Occupation
                    txt_INDV_Tax_NumberSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Tax_Number
                    txt_INDV_Source_of_WealthSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Source_of_Wealth

                    If objAccountSignatoryDetailCustomer.INDV_Deceased = True Then
                        cb_DeceasedSignatory.Checked = True
                        txt_deceased_dateSignatory.Hidden = False
                        txt_deceased_dateSignatory.Text = objAccountSignatoryDetailCustomer.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    End If
                    If objAccountSignatoryDetailCustomer.INDV_Tax_Reg_Number = True Then
                        cb_taxSignatory.Checked = True
                    End If

                    StorePhoneSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_PhonesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID)
                    StorePhoneSignatory.DataBind()

                    StoreAddressSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_AddressesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID)
                    StoreAddressSignatory.DataBind()

                    StoreEmployerPhoneSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_EmployerPhonesCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID)
                    StoreEmployerPhoneSignatory.DataBind()

                    StoreEmployerAddressSignatory.DataSource = goAML_AccountBLL.GetVw_Customer_EmployerAddressCustomer(objAccountSignatoryDetailCustomer.PK_Customer_ID)
                    StoreEmployerAddressSignatory.DataBind()
                Else
                    txt_INDV_TitleSignatory.Text = objAccountSignatoryDetailWic.INDV_Title
                    txt_INDV_Last_NameSignatory.Text = objAccountSignatoryDetailWic.INDV_Last_Name
                    If obj_Account.date_balance.HasValue Then
                        txt_INDV_BirthdateSignatory.Text = objAccountSignatoryDetailWic.INDV_BirthDate.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    Else
                        txt_INDV_BirthdateSignatory.Text = ""
                    End If
                    txt_INDV_Birth_placeSignatory.Text = objAccountSignatoryDetailWic.INDV_Birth_Place
                    txt_INDV_Mothers_nameSignatory.Text = objAccountSignatoryDetailWic.INDV_Mothers_Name
                    txt_INDV_AliasSignatory.Text = objAccountSignatoryDetailWic.INDV_Alias
                    txt_INDV_SSNSignatory.Text = objAccountSignatoryDetailWic.INDV_SSN
                    txt_INDV_Passport_numberSignatory.Text = objAccountSignatoryDetailWic.INDV_Passport_Number
                    If Not String.IsNullOrEmpty(objAccountSignatoryDetailWic.INDV_Passport_Country) Then
                        txt_INDV_Passport_countrySignatory.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Passport_Country).Keterangan
                    Else
                        txt_INDV_Passport_countrySignatory.Text = ""
                    End If
                    txt_INDV_ID_NumberSignatory.Text = objAccountSignatoryDetailWic.INDV_ID_Number
                    If Not objAccountSignatoryDetailWic.INDV_Nationality1 = "" And Not objAccountSignatoryDetailWic.INDV_Nationality1 Is Nothing Then
                        txt_INDV_Nationality1Signatory.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Nationality1).Keterangan
                    End If
                    If Not objAccountSignatoryDetailWic.INDV_Nationality2 = "" And Not objAccountSignatoryDetailWic.INDV_Nationality2 Is Nothing Then
                        txt_INDV_Nationality2Signatory.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Nationality2).Keterangan
                    End If
                    If Not objAccountSignatoryDetailWic.INDV_Nationality3 = "" And Not objAccountSignatoryDetailWic.INDV_Nationality3 Is Nothing Then
                        txt_INDV_Nationality3Signatory.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Nationality3).Keterangan
                    End If
                    If Not objAccountSignatoryDetailWic.INDV_Residence = "" And Not objAccountSignatoryDetailWic.INDV_Residence Is Nothing Then
                        txt_INDV_ResidenceSignatory.Text = goAML_CustomerBLL.GetNamaNegaraByID(objAccountSignatoryDetailWic.INDV_Residence).Keterangan
                    End If

                    txt_INDV_EmailSignatory.Text = objAccountSignatoryDetailWic.INDV_Email
                    txt_INDV_OccupationSignatory.Text = objAccountSignatoryDetailWic.INDV_Occupation
                    txt_INDV_Tax_NumberSignatory.Text = objAccountSignatoryDetailWic.INDV_Tax_Number


                    If objAccountSignatoryDetailWic.INDV_Deceased = True Then
                        cb_DeceasedSignatory.Checked = True
                        txt_deceased_dateSignatory.Hidden = False
                        txt_deceased_dateSignatory.Text = objAccountSignatoryDetailWic.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    End If
                    If objAccountSignatoryDetailWic.INDV_Tax_Reg_Number = True Then
                        cb_taxSignatory.Checked = True
                    End If

                    If goAML_AccountBLL.Getvw_wic_addresses(objAccountSignatoryDetailWic.PK_Customer_ID).Count > 0 Then
                        StoreAddressSignatory.DataSource = goAML_AccountBLL.Getvw_wic_addresses(objAccountSignatoryDetailWic.PK_Customer_ID)
                    End If
                    StoreAddressSignatory.DataBind()
                    If goAML_AccountBLL.GetVw_WIC_Phones(objAccountSignatoryDetailWic.PK_Customer_ID).Count > 0 Then
                        StorePhoneSignatory.DataSource = goAML_AccountBLL.GetVw_WIC_Phones(objAccountSignatoryDetailWic.PK_Customer_ID)
                    End If
                    StorePhoneSignatory.DataBind()
                    If goAML_AccountBLL.GetVw_WIC_Employer_Phones(objAccountSignatoryDetailWic.PK_Customer_ID).Count > 0 Then
                        StoreEmployerPhoneSignatory.DataSource = goAML_AccountBLL.GetVw_WIC_Employer_Phones(objAccountSignatoryDetailWic.PK_Customer_ID)
                    End If
                    StoreEmployerPhoneSignatory.DataBind()
                    If goAML_AccountBLL.Getvw_wic_employer_addresses(objAccountSignatoryDetailWic.PK_Customer_ID).Count > 0 Then
                        StoreEmployerAddressSignatory.DataSource = goAML_AccountBLL.Getvw_wic_employer_addresses(objAccountSignatoryDetailWic.PK_Customer_ID)
                    End If
                    StoreEmployerAddressSignatory.DataBind()
                End If


            End With
            'EnvironmentForm("Detail")
        End If
    End Sub

    Protected Sub checkBoxcb_Deceased_click(sender As Object, e As Ext.Net.DirectEventArgs) Handles cb_DeceasedSignatory.DirectCheck
        Try
            If cb_DeceasedSignatory.Checked Then
                txt_deceased_dateSignatory.Hidden = False
            Else
                txt_deceased_dateSignatory.Hidden = True
                txt_deceased_dateSignatory.Value = Nothing
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub ClearinputAccount()
        txt_INDV_TitleSignatory.Text = ""
        txt_INDV_Last_NameSignatory.Text = ""
        txt_INDV_BirthdateSignatory.Text = ""
        txt_INDV_Birth_placeSignatory.Text = ""
        txt_INDV_Mothers_nameSignatory.Text = ""
        txt_INDV_AliasSignatory.Text = ""
        txt_INDV_SSNSignatory.Text = ""
        txt_INDV_Passport_numberSignatory.Text = ""
        txt_INDV_Passport_countrySignatory.Text = ""
        txt_INDV_ID_NumberSignatory.Text = ""
        txt_INDV_Nationality1Signatory.Text = ""
        txt_INDV_Nationality2Signatory.Text = ""
        txt_INDV_Nationality3Signatory.Text = ""
        txt_INDV_ResidenceSignatory.Text = ""
        txt_INDV_EmailSignatory.Text = ""
        txt_INDV_OccupationSignatory.Text = ""
        txt_INDV_Tax_NumberSignatory.Text = ""
        txt_INDV_Source_of_WealthSignatory.Text = ""

        cb_DeceasedSignatory.Checked = False
        txt_deceased_dateSignatory.Text = ""

        cb_taxSignatory.Checked = False

    End Sub

    Sub LoadDataDetail(id As Long)
        Dim objCustomerPhonesDetail As Vw_Customer_Phones = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = id)


        Dim Phone_Contact_Type As NawaDevDAL.goAML_Ref_Kategori_Kontak = Nothing
        Dim Phone_Communication_Type As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = Nothing

        If Not objCustomerPhonesDetail Is Nothing Then
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            cb_phone_Contact_Type.ReadOnly = True
            cb_phone_Communication_Type.ReadOnly = True
            txt_phone_Country_prefix.ReadOnly = True
            txt_phone_number.ReadOnly = True
            txt_phone_extension.ReadOnly = True
            txt_phone_comments.ReadOnly = True
            btnSavedetail.Hidden = True
            WindowDetail.Title = "Phone Detail"
            ClearinputCustomerPhones()
            With objCustomerPhonesDetail
                Store_Contact_Type.DataSource = goAML_CustomerBLL.GetListKategoriKontak()
                Store_Contact_Type.DataBind()
                Phone_Contact_Type = goAML_AccountBLL.GetContactTypeByKeterangan(objCustomerPhonesDetail.Contact_Type)
                cb_phone_Contact_Type.SetValue(Phone_Contact_Type.Kode)

                Store_Communication_Type.DataSource = goAML_CustomerBLL.GetListTypeKomunikasi()
                Store_Communication_Type.DataBind()
                Phone_Communication_Type = goAML_AccountBLL.GetCommunicationTypeByKeterangan(objCustomerPhonesDetail.Communcation_Type)
                cb_phone_Communication_Type.SetValue(Phone_Communication_Type.Kode)
                txt_phone_Country_prefix.Text = .tph_country_prefix
                txt_phone_number.Text = .tph_number
                txt_phone_extension.Text = .tph_extension
                txt_phone_comments.Text = .comments
            End With
            'EnvironmentForm("Detail")
        End If
    End Sub


    Protected Sub btnBackPhoneandAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address_Director.Hidden = True
            FP_Phone_Director.Hidden = True
            WindowAddressandPhoneDirector.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Private Sub CUSTOMER_DETAIL_Default_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Detail
    End Sub

    Protected Sub GrdCmdAddressKantor(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddressKantor(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailAddressKantor(id As Long)
        Try
            FP_Address.Hidden = False

            Dim Address_Kantor = goAML_AccountBLL.GetAddressbyPK(id)
            Dim type_contact = goAML_CustomerBLL.GetContactTypeByID(Address_Kantor.Address_Type)
            If type_contact IsNot Nothing Then
                JenisAlamat.Text = type_contact.Keterangan
            Else
                JenisAlamat.Text = ""
            End If
            Alamat.Text = Address_Kantor.Address
            Kecamatan.Text = Address_Kantor.Town
            Kota.Text = Address_Kantor.City
            Kode_Pos.Text = Address_Kantor.Zip
            Dim country_code = goAML_CustomerBLL.GetNamaNegaraByID(Address_Kantor.Country_Code)
            If country_code IsNot Nothing Then
                Kode_Negara.Text = country_code.Keterangan
            Else
                Kode_Negara.Text = ""
            End If
            Ibu_Kota.Text = Address_Kantor.State
            Catatan.Text = Address_Kantor.Comments

            WindowAddressPhone.Hidden = False
            btnSavedetail.Hidden = False
            WindowAddressPhone.Title = "Address Detail"
            FieldSetAddressPhone.Title = "Address"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdPhoneKantor(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhoneKantor(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailPhoneKantor(id As Long)
        Try
            FP_Phone.Hidden = False

            Dim Phone_Kantor = goAML_AccountBLL.GetPhonebyPK(id)
            Dim type_device = goAML_CustomerBLL.GetCommunicationTypeByID(Phone_Kantor.Tph_Communication_Type)
            Dim type_contact = goAML_CustomerBLL.GetContactTypeByID(Phone_Kantor.Tph_Contact_Type)

            If type_contact IsNot Nothing Then
                KategoriKontak.Text = type_contact.Keterangan
            Else
                KategoriKontak.Text = ""
            End If
            If type_device IsNot Nothing Then
                JenisAlatKomunikasi.Text = type_device.Keterangan
            Else
                JenisAlatKomunikasi.Text = ""
            End If

            KodeAreaTelp.Text = Phone_Kantor.tph_country_prefix
            NomorTelepon.Text = Phone_Kantor.tph_number
            NomorEkstensi.Text = Phone_Kantor.tph_extension
            Catatan_Phone.Text = Phone_Kantor.comments

            WindowAddressPhone.Hidden = False
            btnSavedetail.Hidden = False
            WindowAddressPhone.Title = "Phone Detail"
            FieldSetAddressPhone.Title = "Phone"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdAddressDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddressDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdEmployerAddressSignatory(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddressDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdAddressSignatory(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddressDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdPhoneSignatory(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhoneDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailAddressDirector(id As Long)
        Try
            WindowAddressandPhoneDirector.Hidden = False
            FP_Address_Director.Hidden = False
            WindowAddressandPhoneDirector.Title = "Address Detail"
            FieldAddressPhone.Title = "Address"

            Dim Address_Kantor = goAML_AccountBLL.GetAddressbyPK(id)
            Dim type_contact = goAML_CustomerBLL.GetContactTypeByID(Address_Kantor.Address_Type)
            Dim country_code = goAML_CustomerBLL.GetNamaNegaraByID(Address_Kantor.Country_Code)

            If type_contact IsNot Nothing Then
                JenisAlamatDirector.Text = type_contact.Keterangan
            Else
                JenisAlamatDirector.Text = ""
            End If

            If country_code IsNot Nothing Then
                Kode_NegaraDirector.Text = country_code.Keterangan
            Else
                Kode_NegaraDirector.Text = ""
            End If

            AlamatDirector.Text = Address_Kantor.Address
            KecamatanDirector.Text = Address_Kantor.Town
            KotaDirector.Text = Address_Kantor.City
            Kode_PosDirector.Text = Address_Kantor.Zip
            Ibu_KotaDirector.Text = Address_Kantor.State
            CatatanAddressDirector.Text = Address_Kantor.Comments

            FieldSetAddressPhone.Title = "Address"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdPhoneDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhoneDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GrdCmdEmployerPhoneSignatory(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhoneDirector(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadDataDetailPhoneDirector(id As Long)
        Try
            WindowAddressandPhoneDirector.Hidden = False
            FP_Phone_Director.Hidden = False
            WindowAddressandPhoneDirector.Title = "Phone Detail"
            FieldAddressPhone.Title = "Phone"

            Dim Phone_Kantor = goAML_AccountBLL.GetPhonebyPK(id)
            Dim type_contact = goAML_CustomerBLL.GetContactTypeByID(Phone_Kantor.Tph_Contact_Type)
            Dim type_device = goAML_CustomerBLL.GetCommunicationTypeByID(Phone_Kantor.Tph_Communication_Type)
            KategoriKontakDirector.Text = type_contact.Keterangan
            JenisAlatKomunikasiDirector.Text = type_device.Keterangan
            KodeAreaTelpDirector.Text = Phone_Kantor.tph_country_prefix
            NomorTeleponDirector.Text = Phone_Kantor.tph_number
            NomorEkstensiDirector.Text = Phone_Kantor.tph_extension
            Catatan_PhoneDirector.Text = Phone_Kantor.comments


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'try do 
    Protected Sub btnBackSaveAccountSignatory_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Signatory.Hidden = True
            WindowDetail.Hidden = True
            ClearinputAccount()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveAccountAddress_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Address.Hidden = True
            WindowAddressPhone.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnBackSaveAccountPhone_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FP_Phone.Hidden = True
            WindowAddressPhone.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

End Class
