﻿Imports Ext
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL
Imports Elmah
Imports System.Data

Partial Class goAML_WICDetail
    Inherits Parent
    'Dim objWIC As New goAML_Ref_WIC
    Public objgoAML_WIC As WICBLL
    Public Property StrUnikKey() As String
        Get
            Return Session("goAML_WICDetail.StrUnikKey")
        End Get
        Set(ByVal value As String)
            Session("goAML_WICDetail.StrUnikKey") = value
        End Set
    End Property

    Public Property objmodule() As NawaDAL.Module
        Get
            Return Session("goAML_WICDetail.objmodule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_WICDetail.objmodule") = value
        End Set
    End Property

    Public Property ObjWIC As goAML_Ref_WIC
        Get
            Return Session("goAML_WICDetail.ObjWIC")
        End Get
        Set(ByVal value As goAML_Ref_WIC)
            Session("goAML_WICDetail.ObjWIC") = value
        End Set
    End Property
    Public Property ObjDirector As goAML_Ref_Walk_In_Customer_Director
        Get
            Return Session("goAML_WICDetail.ObjDirector")
        End Get
        Set(ByVal value As goAML_Ref_Walk_In_Customer_Director)
            Session("goAML_WICDetail.ObjDirector") = value
        End Set
    End Property
    Public Property ListDirectorDetail As List(Of goAML_Ref_Walk_In_Customer_Director)
        Get
            If Session("goAML_WICDetail.ListDirectorDetail") Is Nothing Then
                Session("goAML_WICDetail.ListDirectorDetail") = New List(Of goAML_Ref_Walk_In_Customer_Director)
            End If
            Return Session("goAML_WICDetail.ListDirectorDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Walk_In_Customer_Director))
            Session("goAML_WICDetail.ListDirectorDetail") = value
        End Set
    End Property
    Public Property ListPhoneDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICDetail.ListPhoneDetail") Is Nothing Then
                Session("goAML_WICDetail.ListPhoneDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICDetail.ListPhoneDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICDetail.ListPhoneDetail") = value
        End Set
    End Property
    Public Property ListPhoneEmployerDetail As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICDetail.ListPhoneEmployerDetail") Is Nothing Then
                Session("goAML_WICDetail.ListPhoneEmployerDetail") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICDetail.ListPhoneEmployerDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICDetail.ListPhoneEmployerDetail") = value
        End Set
    End Property
    Public Property ListPhoneDetailDirector As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICDetail.ListPhoneDetailDirector") Is Nothing Then
                Session("goAML_WICDetail.ListPhoneDetailDirector") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICDetail.ListPhoneDetailDirector")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICDetail.ListPhoneDetailDirector") = value
        End Set
    End Property
    Public Property ListPhoneDetailDirectorEmployer As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_WICDetail.ListPhoneDetailDirectorEmployer") Is Nothing Then
                Session("goAML_WICDetail.ListPhoneDetailDirectorEmployer") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_WICDetail.ListPhoneDetailDirectorEmployer")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_WICDetail.ListPhoneDetailDirectorEmployer") = value
        End Set
    End Property
    Public Property ListAddressDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICDetail.ListAddressDetail") Is Nothing Then
                Session("goAML_WICDetail.ListAddressDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICDetail.ListAddressDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICDetail.ListAddressDetail") = value
        End Set
    End Property
    Public Property ListAddressEmployerDetail As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICDetail.ListAddressEmployerDetail") Is Nothing Then
                Session("goAML_WICDetail.ListAddressEmployerDetail") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICDetail.ListAddressEmployerDetail")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICDetail.ListAddressEmployerDetail") = value
        End Set
    End Property
    Public Property ListAddressDetailDirector As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICDetail.ListAddressDetailDirector") Is Nothing Then
                Session("goAML_WICDetail.ListAddressDetailDirector") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICDetail.ListAddressDetailDirector")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICDetail.ListAddressDetailDirector") = value
        End Set
    End Property
    Public Property ListIdentificationDetailDirector As List(Of goAML_Person_Identification)
        Get
            If Session("goAML_WICDetail.ListIdentificationDetailDirector") Is Nothing Then
                Session("goAML_WICDetail.ListIdentificationDetailDirector") = New List(Of goAML_Person_Identification)
            End If
            Return Session("goAML_WICDetail.ListIdentificationDetailDirector")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_WICDetail.ListIdentificationDetailDirector") = value
        End Set
    End Property
    Public Property ListIdentificationDetail As List(Of goAML_Person_Identification)
        Get
            If Session("goAML_WICDetail.ListIdentificationDetail") Is Nothing Then
                Session("goAML_WICDetail.ListIdentificationDetail") = New List(Of goAML_Person_Identification)
            End If
            Return Session("goAML_WICDetail.ListIdentificationDetail")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_WICDetail.ListIdentificationDetail") = value
        End Set
    End Property
    Public Property ListAddressDetailDirectorEmployer As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_WICDetail.ListAddressDetailDirectorEmployer") Is Nothing Then
                Session("goAML_WICDetail.ListAddressDetailDirectorEmployer") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_WICDetail.ListAddressDetailDirectorEmployer")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_WICDetail.ListAddressDetailDirectorEmployer") = value
        End Set
    End Property
    Public Property ObjDetailPhone As goAML_Ref_Phone
        Get
            Return Session("goAML_WICDetail.ObjDetailPhone")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICDetail.ObjDetailPhone") = value
        End Set
    End Property
    Public Property ObjDetailPhoneEmployer As goAML_Ref_Phone
        Get
            Return Session("goAML_WICDetail.ObjDetailPhoneEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICDetail.ObjDetailPhoneEmployer") = value
        End Set
    End Property
    Public Property ObjDetailPhoneDirector As goAML_Ref_Phone
        Get
            Return Session("goAML_WICDetail.ObjDetailPhoneDirector")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICDetail.ObjDetailPhoneDirector") = value
        End Set
    End Property
    Public Property ObjDetailPhoneDirectorEmployer As goAML_Ref_Phone
        Get
            Return Session("goAML_WICDetail.ObjDetailPhoneDirectorEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Phone)
            Session("goAML_WICDetail.ObjDetailPhoneDirectorEmployer") = value
        End Set
    End Property
    Public Property ObjDetailAddress As goAML_Ref_Address
        Get
            Return Session("goAML_WICDetail.ObjDetailAddress")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICDetail.ObjDetailAddress") = value
        End Set
    End Property
    Public Property ObjDetailAddressEmployer As goAML_Ref_Address
        Get
            Return Session("goAML_WICDetail.ObjDetailAddressEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICDetail.ObjDetailAddressEmployer") = value
        End Set
    End Property
    Public Property ObjDetailAddressDirector As goAML_Ref_Address
        Get
            Return Session("goAML_WICDetail.ObjDetailAddressDirector")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICDetail.ObjDetailAddressDirector") = value
        End Set
    End Property
    Public Property ObjDetailIdentificationDirector As goAML_Person_Identification
        Get
            Return Session("goAML_WICDetail.ObjDetailIdentificationDirector")
        End Get
        Set(ByVal value As goAML_Person_Identification)
            Session("goAML_WICDetail.ObjDetailIdentificationDirector") = value
        End Set
    End Property
    Public Property ObjDetailIdentification As goAML_Person_Identification
        Get
            Return Session("goAML_WICDetail.ObjDetailIdentification")
        End Get
        Set(ByVal value As goAML_Person_Identification)
            Session("goAML_WICDetail.ObjDetailIdentification") = value
        End Set
    End Property
    Public Property ObjDetailAddressDirectorEmployer As goAML_Ref_Address
        Get
            Return Session("goAML_WICDetail.ObjDetailAddressDirectorEmployer")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_WICDetail.ObjDetailAddressDirectorEmployer") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                Dim Moduleid As String = Request.Params("ModuleID")
                Dim intModuleid As Integer = Common.DecryptQueryString(Moduleid, SystemParameterBLL.GetEncriptionKey)
                objmodule = ModuleBLL.GetModuleByModuleID(intModuleid)

                Dim IDData As String = Request.Params("ID")
                StrUnikKey = Common.DecryptQueryString(IDData, SystemParameterBLL.GetEncriptionKey)

                If Not objmodule Is Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If

                'FormPanelWICDelete.Title = objmodule.ModuleLabel & " Detail"
                'Dim objWICBLL As New WICBLL
                'objWICBLL.LoadPanelDelete(FormPanelWICDelete, objmodule.ModuleName, StrUnikKey)

                ObjWIC = WICBLL.GetWICByID(StrUnikKey)
                ListPhoneDetail = objgoAML_WIC.GetWICByPKIDDetailPhone(StrUnikKey)
                ListAddressDetail = objgoAML_WIC.GetWICByPKIDDetailAddress(StrUnikKey)
                ListPhoneEmployerDetail = objgoAML_WIC.GetWICByPKIDDetailEmployerPhone(StrUnikKey)
                ListAddressEmployerDetail = objgoAML_WIC.GetWICByPKIDDetailEmployerAddress(StrUnikKey)
                ListIdentificationDetail = objgoAML_WIC.GetWICByPKIDDetailIdentification(StrUnikKey)

                If ObjWIC.FK_Customer_Type_ID = 2 Then
                    ListDirectorDetail = objgoAML_WIC.GetWICByPKIDDetailDirector(StrUnikKey)
                End If

                LoadDataWIC()
                LoadColumn()
                LoadPopup()

            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Private Sub LoadPopup()
        WindowDetailAddress.Maximizable = True
        WindowDetailPhone.Maximizable = True
        WindowDetailDirector.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True
        WindowDetailDirectorIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailIdentification.Maximizable = True
    End Sub

    Private Sub LoadColumn()

        ColumnActionLocation(GridPanel1, CommandColumn1)
        ColumnActionLocation(GridPanel2, CommandColumn2)
        ColumnActionLocation(GridPanel3, CommandColumn3)
        ColumnActionLocation(GridPanel4, CommandColumn4)
        ColumnActionLocation(GridPanel5, CommandColumn5)

        ColumnActionLocation(GridPanelPhoneIndividuDetail, CommandColumnPhoneIndividuDetail)
        ColumnActionLocation(GridPanelAddressIndividuDetail, CommandColumnAddressIndividuDetail)
        ColumnActionLocation(GridPanelPhoneIndividualEmployerDetail, CommandColumnPhoneIndividualEmployerDetail)
        ColumnActionLocation(GridPanelAddressIndividualEmployerDetail, CommandColumnAddressIndividualEmployerDetail)
        ColumnActionLocation(GridPanelIdentificationIndividualDetail, CommandColumnIdentificationIndividualDetail)

        ColumnActionLocation(GridPanelPhoneCorporateDetail, CommandColumnPhoneCorporateDetail)
        ColumnActionLocation(GridPanelAddressCorporateDetail, CommandColumnAddressCorporateDetail)
        ColumnActionLocation(GP_Corp_Director, CommandColumnDirector)

    End Sub

    Private Sub ColumnActionLocation(gridPanel As GridPanel, commandColumn As CommandColumn)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridPanel.ColumnModel.Columns.RemoveAt(gridPanel.ColumnModel.Columns.Count - 1)
                gridPanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
        objgoAML_WIC = New WICBLL(WIC_Panel)
    End Sub
    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordIdentificationDirector(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordIdentification(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordAddressDirector(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordPhoneDirectorEmployer(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordAddressDirectorEmployer(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordAddress(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressEmployerIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordAddressEmployer(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneCorporate(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordPhone(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressCorporate(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordAddress(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdCustDirector(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordDirector(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordDirector(id As String)
        ObjDirector = ListDirectorDetail.Find(Function(x) x.NO_ID = id)
        If Not ObjDirector Is Nothing Then

            FormPanelDirectorDetail.Show()
            WindowDetailDirector.Show()
            With ObjDirector
                If GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role) IsNot Nothing Then
                    DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
                Else
                    DspPeranDirector.Text = ""
                End If

                DspGelarDirector.Text = .Title
                DspNamaLengkap.Text = .Last_Name
                If GlobalReportFunctionBLL.getGenderbyKode(.Gender) IsNot Nothing Then
                    DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                Else
                    DspGender.Text = ""
                End If


                If .BirthDate IsNot Nothing Then
                    DspTanggalLahir.Text = ObjDirector.BirthDate.Value.ToString("dd-MMM-yy")
                End If
                DspTempatLahir.Text = .Birth_Place
                DspNamaIbuKandung.Text = .Mothers_Name
                DspNamaAlias.Text = .Alias
                DspNIK.Text = .SSN
                DspNoPassport.Text = .Passport_Number
                'daniel 19 jan 2021
                'DspNegaraPenerbitPassport.Text = .Passport_Country
                DspNegaraPenerbitPassport.Text = GlobalReportFunctionBLL.getCountryByCode(.Passport_Country)
                DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                DspNoIdentitasLain.Text = .ID_Number
                'If GlobalReportFunctionBLL.getCountryByCode(.Nationality1) IsNot Nothing Then
                '    DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.Nationality2) IsNot Nothing Then
                '    DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                'Else
                '    DspKewarganegaraan2.Text = ""
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.Nationality3) IsNot Nothing Then
                '    DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                'Else
                '    DspKewarganegaraan3.Text = ""
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.Residence) IsNot Nothing Then
                '    DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                'Else
                '    DspNegaraDomisili.Text = ""
                'End If
                'end 19 jan 2021

                DspEmail.Text = .Email
                DspEmail2.Text = .Email2
                DspEmail3.Text = .Email3
                DspEmail4.Text = .Email4
                DspEmail5.Text = .Email5
                DspDeceased.Text = .Deceased
                If DspDeceased.Text = "True" Then
                    If .Deceased_Date IsNot Nothing Then
                        DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                        'Daniel 2021 Jan 7
                        DspDeceasedDate.Hidden = False
                    End If
                Else
                    DspDeceasedDate.Hidden = True
                    'end 2021 Jan 7
                End If
                DspPEP.Text = .Tax_Reg_Number
                DspNPWP.Text = .Tax_Number
                DspSourceofWealth.Text = .Source_of_Wealth
                DspOccupation.Text = .Occupation
                DspCatatan.Text = .Comments
                DspTempatBekerja.Text = .Employer_Name
            End With

            ListPhoneDetailDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorPhone(ObjDirector.NO_ID)
            ListPhoneDetailDirectorEmployer = objgoAML_WIC.GetWICByPKIDDetailDirectorPhoneEmployer(ObjDirector.NO_ID)
            ListAddressDetailDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorAddress(ObjDirector.NO_ID)
            ListAddressDetailDirectorEmployer = objgoAML_WIC.GetWICByPKIDDetailDirectorAddressEmployer(ObjDirector.NO_ID)
            ListIdentificationDetailDirector = objgoAML_WIC.GetWICByPKIDDetailDirectorIdentification(ObjDirector.NO_ID)

            BindDetailPhone(StoreDirectorPhone, ListPhoneDetailDirector)
            BindDetailPhone(StoreDirectorPhoneEmployer, ListPhoneDetailDirectorEmployer)
            BindDetailAddress(StoreDirectorAddress, ListAddressDetailDirector)
            BindDetailAddress(StoreDirectorAddressEmployer, ListAddressDetailDirectorEmployer)
            BindDetailIdentification(StoreIdentificationDirector, ListIdentificationDetailDirector)

        End If
    End Sub

    Private Sub DetailRecordAddress(id As String)
        ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddress Is Nothing Then

            PanelDetailAddress.Show()
            WindowDetailAddress.Show()
            With ObjDetailAddress
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                Else
                    DspAddress_type.Text = ""
                End If

                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                Else
                    Dspcountry_code.Text = ""
                End If
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
    End Sub
    Private Sub DetailRecordAddressEmployer(id As String)
        ObjDetailAddressEmployer = ListAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = id And x.FK_Ref_Detail_Of = 10)
        If Not ObjDetailAddressEmployer Is Nothing Then

            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Show()
            WindowDetailAddress.Show()
            With ObjDetailAddressEmployer
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                Else
                    DspAddress_typeEmployer.Text = ""
                End If
                DspAddressEmployer.Text = .Address
                DspTownEmployer.Text = .Town
                DspCityEmployer.Text = .City
                DspZipEmployer.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                Else
                    Dspcountry_codeEmployer.Text = ""
                End If

                DspStateEmployer.Text = .State
                DspcommentsAddressEmployer.Text = .Comments
            End With
        End If
    End Sub
    Protected Sub GridCommandPhoneIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordPhone(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneEmployerIndividu(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordPhoneEmployer(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordPhone(id As String)
        ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhone Is Nothing Then

            PanelDetailPhone.Show()
            WindowDetailPhone.Show()
            With ObjDetailPhone
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Else
                    Dsptph_contact_type.Text = ""
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Else
                    Dsptph_communication_type.Text = ""
                End If
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
    End Sub
    Private Sub DetailRecordPhoneEmployer(id As String)
        ObjDetailPhoneEmployer = ListPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = id And x.FK_Ref_Detail_Of = 10)
        If Not ObjDetailPhoneEmployer Is Nothing Then

            PanelDetailPhone.Hide()
            PanelDetailPhoneEmployer.Show()
            WindowDetailPhone.Show()
            With ObjDetailPhoneEmployer
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Else
                    Dsptph_contact_typeEmployer.Text = ""
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Else
                    Dsptph_communication_typeEmployer.Text = ""
                End If
                Dsptph_country_prefixEmployer.Text = .tph_country_prefix
                Dsptph_numberEmployer.Text = .tph_number
                Dsptph_extensionEmployer.Text = .tph_extension
                DspcommentsPhoneEmployer.Text = .comments
            End With
        End If
    End Sub
    Protected Sub btnBackSaveCustomerDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirector.Hide()
            FormPanelDirectorDetail.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()
            PanelDetailPhone.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailAddress.Hide()
            PanelDetailAddress.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorDetailAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorDetailEmployerAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorAddress.Hide()
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()

            PanelPhoneDirector.Show()
            PanelPhoneDirectorEmployer.Hide()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelDirectorEmployerPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorPhone.Hide()

            PanelPhoneDirector.Show()
            PanelPhoneDirectorEmployer.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelIdentificationDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailDirectorIdentification.Hide()
            PanelDetailIdentificationDirector.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelIdentification_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailIdentification.Hide()
            PanelDetailIdentification.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelPhoneEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailPhone.Hide()
            PanelDetailPhoneEmployer.Hide()
            PanelDetailPhone.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAddressEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            WindowDetailAddress.Hide()
            PanelDetailAddress.Hide()
            PanelDetailAddressEmployer.Hide()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub DetailRecordAddressDirectorEmployer(id As String)
        ObjDetailAddressDirectorEmployer = ListAddressDetailDirectorEmployer.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddressDirectorEmployer Is Nothing Then

            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Show()
            WindowDetailDirectorAddress.Show()
            With ObjDetailAddressDirectorEmployer
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                Else
                    DspTipeAlamatDirectorEmployer.Text = ""
                End If
                DspAlamatDirectorEmployer.Text = .Address
                DspKecamatanDirectorEmployer.Text = .Town
                DspKotaKabupatenDirectorEmployer.Text = .City
                DspKodePosDirectorEmployer.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                Else
                    DspNegaraDirectorEmployer.Text = ""
                End If

                DspProvinsiDirectorEmployer.Text = .State
                DspCatatanAddressDirectorEmployer.Text = .Comments
            End With
        End If
    End Sub

    Private Sub DetailRecordPhoneDirectorEmployer(id As String)
        ObjDetailPhoneDirectorEmployer = ListPhoneDetailDirectorEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneDirectorEmployer Is Nothing Then

            PanelPhoneDirector.Hide()
            WindowDetailDirectorPhone.Show()
            PanelPhoneDirectorEmployer.Show()
            'Daniel 2021 Jan 5
            'With ObjDetailPhoneDirector
            With ObjDetailPhoneDirectorEmployer
                'end 2021 Jan 5
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    DspKategoriKontakDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Else
                    DspKategoriKontakDirectorEmployer.Text = ""
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    DspJenisAlatKomunikasiDirectorEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Else
                    DspJenisAlatKomunikasiDirectorEmployer.Text = ""
                End If
                DspKodeAreaTelpDirectorEmployer.Text = .tph_country_prefix
                DspNomorTeleponDirectorEmployer.Text = .tph_number
                DspNomorExtensiDirectorEmployer.Text = .tph_extension
                DspCatatanDirectorPhoneDirectorEmployer.Text = .comments
            End With
        End If
    End Sub
    Private Sub DetailRecordIdentificationDirector(id As String)
        ObjDetailIdentificationDirector = ListIdentificationDetailDirector.Find(Function(x) x.PK_Person_Identification_ID = id And x.FK_Person_Type = 7)
        If Not ObjDetailIdentificationDirector Is Nothing Then

            PanelDetailIdentificationDirector.Show()
            WindowDetailDirectorIdentification.Show()
            With ObjDetailIdentificationDirector
                If GlobalReportFunctionBLL.getjenisDokumenByKode(.Type) IsNot Nothing Then
                    DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                Else
                    DsptTypeDirector.Text = ""
                End If
                DspNumberDirector.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDateDirector.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDateDirector.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedByDirector.Text = .Issued_By
                If GlobalReportFunctionBLL.getCountryByCode(.Issued_Country) IsNot Nothing Then
                    DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                Else
                    DspIssuedCountryDirector.Text = ""
                End If
                DspIdentificationCommentDirector.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub DetailRecordIdentification(id As String)
        ObjDetailIdentification = ListIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = id And x.FK_Person_Type = 8)
        If Not ObjDetailIdentification Is Nothing Then

            PanelDetailIdentification.Show()
            WindowDetailIdentification.Show()
            With ObjDetailIdentification
                If GlobalReportFunctionBLL.getjenisDokumenByKode(.Type) IsNot Nothing Then
                    DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                Else
                    DsptType.Text = ""
                End If

                DspNumber.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDate.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDate.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedBy.Text = .Issued_By
                If GlobalReportFunctionBLL.getCountryByCode(.Issued_Country) IsNot Nothing Then
                    DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                Else
                    DspIssuedCountry.Text = ""
                End If

                DspIdentificationComment.Text = .Identification_Comment
            End With
        End If
    End Sub
    Private Sub DetailRecordAddressDirector(id As String)
        ObjDetailAddressDirector = ListAddressDetailDirector.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjDetailAddressDirector Is Nothing Then

            PanelAddressDirector.Show()
            PanelAddressDirectorEmployer.Hide()
            WindowDetailDirectorAddress.Show()
            With ObjDetailAddressDirector
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                Else
                    DspTipeAlamatDirector.Text = ""
                End If
                DspAlamatDirector.Text = .Address
                DspKecamatanDirector.Text = .Town
                DspKotaKabupatenDirector.Text = .City
                DspKodePosDirector.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                End If
                DspProvinsiDirector.Text = .State
                DspCatatanAddressDirector.Text = .Comments
            End With
        End If
    End Sub

    Protected Sub GrdCmdDirectorPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            DetailRecordPhoneDirector(id)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DetailRecordPhoneDirector(id As String)
        ObjDetailPhoneDirector = ListPhoneDetailDirector.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjDetailPhoneDirector Is Nothing Then

            PanelPhoneDirector.Show()
            WindowDetailDirectorPhone.Show()
            PanelPhoneDirectorEmployer.Hide()
            With ObjDetailPhoneDirector
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                End If
                DspKodeAreaTelpDirector.Text = .tph_country_prefix
                DspNomorTeleponDirector.Text = .tph_number
                DspNomorExtensiDirector.Text = .tph_extension
                DspCatatanDirectorPhoneDirector.Text = .comments
            End With
        End If
    End Sub

    Private Sub LoadDataPhone()
        ObjDetailPhone = ListPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = ID And x.FK_Ref_Detail_Of = 3)
        If Not ObjDetailPhone Is Nothing Then
            PanelDetailPhone.Show()
            WindowDetailPhone.Show()
            With ObjDetailPhone
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                End If
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataPhoneDirector()
        ObjDetailPhoneDirector = ListPhoneDetailDirector.Find(Function(x) x.PK_goAML_Ref_Phone = ID And x.FK_Ref_Detail_Of = 8)
        If Not ObjDetailPhone Is Nothing Then
            PanelPhoneDirector.Show()
            PanelPhoneDirectorEmployer.Hide()
            WindowDetailDirectorPhone.Show()
            With ObjDetailPhone
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                End If
                DspKodeAreaTelpDirector.Text = .tph_country_prefix
                DspNomorTeleponDirector.Text = .tph_number
                DspNomorExtensiDirector.Text = .tph_extension
                DspCatatanDirectorPhoneDirector.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataPhoneDirectorEmployer()
        ObjDetailPhoneDirectorEmployer = ListPhoneDetailDirectorEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = ID And x.FK_Ref_Detail_Of = 4)
        If Not ObjDetailPhone Is Nothing Then
            PanelPhoneDirector.Hide()
            PanelPhoneDirectorEmployer.Show()
            WindowDetailDirectorPhone.Show()
            With ObjDetailPhone
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type) IsNot Nothing Then
                    DspKategoriKontakDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                End If
                If GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type) IsNot Nothing Then
                    DspJenisAlatKomunikasiDirectorEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                End If
                DspKodeAreaTelpDirectorEmployer.Text = .tph_country_prefix
                DspNomorTeleponDirectorEmployer.Text = .tph_number
                DspNomorExtensiDirectorEmployer.Text = .tph_extension
                DspCatatanDirectorPhoneDirectorEmployer.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataAddress()
        ObjDetailAddress = ListAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = ID And x.FK_Ref_Detail_Of = 3)
        If Not ObjDetailAddress Is Nothing Then
            PanelDetailAddress.Show()
            WindowDetailAddress.Show()
            With ObjDetailAddress
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                End If
                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                End If
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
    End Sub
    Private Sub LoadDataAddressDirector()
        ObjDetailAddressDirector = ListAddressDetailDirector.Find(Function(x) x.PK_Customer_Address_ID = ID And x.FK_Ref_Detail_Of = 8)
        If Not ObjDetailAddress Is Nothing Then
            PanelAddressDirector.Show()
            PanelAddressDirectorEmployer.Hide()
            WindowDetailAddress.Show()
            With ObjDetailAddress
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                End If
                DspAlamatDirector.Text = .Address
                DspKecamatanDirector.Text = .Town
                DspKotaKabupatenDirector.Text = .City
                DspKodePosDirector.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                End If
                DspProvinsiDirector.Text = .State
                DspCatatanAddressDirector.Text = .Comments
            End With
        End If
    End Sub
    Private Sub LoadDataAddressDirectorEmployer()
        ObjDetailAddressDirectorEmployer = ListAddressDetailDirectorEmployer.Find(Function(x) x.PK_Customer_Address_ID = ID And x.FK_Ref_Detail_Of = 4)
        If Not ObjDetailAddress Is Nothing Then
            PanelAddressDirector.Hide()
            PanelAddressDirectorEmployer.Show()
            WindowDetailAddress.Show()
            With ObjDetailAddress
                If GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type) IsNot Nothing Then
                    DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                End If
                DspAlamatDirectorEmployer.Text = .Address
                DspKecamatanDirectorEmployer.Text = .Town
                DspKotaKabupatenDirectorEmployer.Text = .City
                DspKodePosDirectorEmployer.Text = .Zip
                If GlobalReportFunctionBLL.getCountryByCode(.Country_Code) IsNot Nothing Then
                    DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                End If
                DspProvinsiDirectorEmployer.Text = .State
                DspCatatanAddressDirectorEmployer.Text = .Comments
            End With
        End If
    End Sub
    Private Sub LoadDataWIC()
        PKWIC.Text = ObjWIC.PK_Customer_ID
        DSpWICNo.Text = ObjWIC.WIC_No
        If ObjWIC.isUpdateFromDataSource = True Then
            DspIsUpdateFromDataSource.Text = "True"
        ElseIf ObjWIC.isUpdateFromDataSource = False Then
            DspIsUpdateFromDataSource.Text = "False"
        Else
            DspIsUpdateFromDataSource.Text = ""
        End If

        If ObjWIC.FK_Customer_Type_ID = 1 Then
            With ObjWIC
                TxtINDV_Title.Text = .INDV_Title
                TxtINDV_last_name.Text = .INDV_Last_Name
                If GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender) IsNot Nothing Then
                    TxtINDV_Gender.Text = GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender)
                End If
                'agam 02112020
                If .INDV_BirthDate IsNot Nothing Then
                    DateINDV_Birthdate.Text = .INDV_BirthDate.Value.ToString("dd-MMM-yy")
                End If
                ' DateINDV_Birthdate.Text = .INDV_BirthDate
                TxtINDV_Birth_place.Text = .INDV_Birth_Place
                TxtINDV_Mothers_name.Text = .INDV_Mothers_Name
                TxtINDV_Alias.Text = .INDV_Alias
                TxtINDV_SSN.Text = .INDV_SSN
                TxtINDV_Passport_number.Text = .INDV_Passport_Number
                'daniel 19 jan 2021
                'TxtINDV_Passport_country.Text = .INDV_Passport_Country
                TxtINDV_Passport_country.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Passport_Country)
                CmbINDV_Nationality1.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1)
                CmbINDV_Nationality2.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2)
                CmbINDV_Nationality3.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3)
                CmbINDV_residence.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence)
                TxtINDV_ID_Number.Text = .INDV_ID_Number
                'If GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1) IsNot Nothing Then
                '    CmbINDV_Nationality1.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1)
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2) IsNot Nothing Then
                '    CmbINDV_Nationality2.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2)
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3) IsNot Nothing Then
                '    CmbINDV_Nationality3.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3)
                'End If
                'If GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence) IsNot Nothing Then
                '    CmbINDV_residence.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence)
                'End If
                'daniel 19 jan 2021
                TxtINDV_Email.Text = .INDV_Email
                TxtINDV_Email2.Text = .INDV_Email2
                TxtINDV_Email3.Text = .INDV_Email3
                TxtINDV_Email4.Text = .INDV_Email4
                TxtINDV_Email5.Text = .INDV_Email5
                TxtINDV_Occupation.Text = .INDV_Occupation
                'daniel 11 jan 2021
                TxtINDV_employer_name.Text = .INDV_Employer_Name
                'end 11 jan 2021
                TxtINDV_Source_Of_Wealth.Text = .INDV_SumberDana
                TxtINDV_Tax_Number.Text = .INDV_Tax_Number
                CbINDV_Tax_Reg_Number.Text = .INDV_Tax_Reg_Number
                If .INDV_Deceased = True Then
                    CbINDV_Deceased.Text = "True"
                ElseIf .INDV_Deceased = True Then
                    CbINDV_Deceased.Text = "False"
                Else
                    CbINDV_Deceased.Text = ""
                End If

                If .INDV_Deceased Then
                    If .INDV_Deceased_Date IsNot Nothing Then
                        DateINDV_Deceased_Date.Text = .INDV_Deceased_Date.ToString
                    End If
                End If
                TxtINDV_Comments.Text = .INDV_Comment
            End With

            BindDetailAddress(StoreAddressIndividuDetail, ListAddressDetail)
            BindDetailPhone(StorePhoneIndividuDetail, ListPhoneDetail)
            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListAddressEmployerDetail)
            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListPhoneEmployerDetail)
            BindDetailIdentification(StoreIdentificationDetail, ListIdentificationDetail)

            WIC_Individu.Show()
            WIC_Corporate.Hide()
        Else
            With ObjWIC
                TxtCorp_Name.Text = .Corp_Name
                TxtCorp_Commercial_name.Text = .Corp_Commercial_Name
                If GlobalReportFunctionBLL.getBentukKorporasiByKode(.Corp_Incorporation_Legal_Form) IsNot Nothing Then
                    CmbCorp_Incorporation_legal_form.Text = GlobalReportFunctionBLL.getBentukKorporasiByKode(.Corp_Incorporation_Legal_Form)
                End If
                TxtCorp_Incorporation_number.Text = .Corp_Incorporation_Number
                TxtCorp_Business.Text = .Corp_Business
                TxtCorp_Email.Text = .Corp_Email
                TxtCorp_url.Text = .Corp_Url
                TxtCorp_incorporation_state.Text = .Corp_Incorporation_State
                If GlobalReportFunctionBLL.getCountryByCode(.Corp_Incorporation_Country_Code) IsNot Nothing Then
                    CmbCorp_incorporation_country_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Corp_Incorporation_Country_Code)
                End If
                'daniel 12 jam 2021
                'If GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Corp_Role) IsNot Nothing Then
                '    CmbCorp_Role.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Corp_Role)
                'End If
                'end 12 jam 2021
                If .Corp_Incorporation_Date IsNot Nothing Then
                    DateCorp_incorporation_date.Text = .Corp_Incorporation_Date.Value.ToString("dd-MMM-yy")
                End If
                chkCorp_business_closed.Text = If(.Corp_Business_Closed, True, False)
                If chkCorp_business_closed.Text = "True" Then
                    If .Corp_Date_Business_Closed IsNot Nothing Then
                        DateCorp_date_business_closed.Text = .Corp_Date_Business_Closed.Value.ToString("dd-MMM-yy")
                    End If
                End If
                TxtCorp_tax_number.Text = .Corp_Tax_Number
                TxtCorp_Comments.Text = .Corp_Comments
            End With

            BindDetailAddress(StoreAddressCorporateDetail, ListAddressDetail)
            BindDetailDirector(Store_Director_Corp, ListDirectorDetail)
            BindDetailPhone(StorePhoneCorporateDetail, ListPhoneDetail)

            WIC_Individu.Hide()
            WIC_Corporate.Show()
        End If
    End Sub
    Private Sub BindDetailIdentification(store As Store, list As List(Of goAML_Person_Identification))
        Dim objtable As DataTable = NawaBLL.Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Tipe", GetType(String)))
        objtable.Columns.Add(New DataColumn("No", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssueDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("ExpiryDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssuedCountry", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Tipe") = GlobalReportFunctionBLL.getjenisDokumenByKode(item("Type"))
                item("No") = item("Number")
                item("IssueDate") = item("Issue_Date")
                item("ExpiryDate") = item("Expiry_Date")
                item("IssuedCountry") = GlobalReportFunctionBLL.getCountryByCode(item("Issued_Country"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailPhone(store As Store, list As List(Of goAML_Ref_Phone))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
                item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailAddress(store As Store, list As List(Of goAML_Ref_Address))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Type_Address", GetType(String)))
        objtable.Columns.Add(New DataColumn("Addres", GetType(String)))
        objtable.Columns.Add(New DataColumn("Cty", GetType(String)))
        objtable.Columns.Add(New DataColumn("CountryCode", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Type_Address") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type"))
                item("Addres") = item("Address")
                item("Cty") = item("City")
                item("CountryCode") = GlobalReportFunctionBLL.getCountryByCode(item("Country_Code"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailDirector(store As Store, list As List(Of goAML_Ref_Walk_In_Customer_Director))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("LastName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Roles", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("LastName") = item("Last_Name")
                item("Roles") = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(item("Role"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Net.X.Redirect(Common.GetApplicationPath & objmodule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
End Class
