﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="WICDetail.aspx.vb" Inherits="goAML_WICDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-form-item-label.x-form-item-label-default { width : 175px !important; }
    </style></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <ext:Window ID="WindowDetailDirector" runat="server" Icon="ApplicationViewDetail" Title="Customer Director Detail" Hidden="true" MinWidth="800" Height="500" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="FormPanelDirectorDetail" runat="server" Hidden="true" ButtonAlign="Center" AutoScroll="true">
                <Items>
                    <ext:Hidden runat="server" ID="Hidden1"></ext:Hidden>
                    <ext:DisplayField ID="DspPeranDirector" runat="server" FieldLabel="Peran" AnchorHorizontal="70%" LabelWidth="250"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250" ID="DspGender" runat="server" FieldLabel="Gender" AnchorHorizontal="70%" AllowBlank="false"></ext:DisplayField>
                    <ext:DisplayField ID="DspGelarDirector" LabelWidth="250" runat="server" FieldLabel="Gelar" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaLengkap" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Nama Lengkap" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Tanggal Lahir" ID="DspTanggalLahir" AnchorHorizontal="90%"  />
                    <ext:DisplayField ID="DspTempatLahir" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaIbuKandung" LabelWidth="250" runat="server" FieldLabel="Nama Ibu Kandung" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNamaAlias" LabelWidth="250" runat="server" FieldLabel="Nama Alias" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNIK" LabelWidth="250" runat="server" FieldLabel="NIK" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNoPassport" LabelWidth="250" runat="server" FieldLabel="No. Passport" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraPenerbitPassport" LabelWidth="250" runat="server" FieldLabel="Negara Penerbit Passport" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNoIdentitasLain" LabelWidth="250" runat="server" FieldLabel="No. Identitas Lain" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField LabelWidth="250" AllowBlank="False" ID="DspKewarganegaraan1" runat="server" FieldLabel="Kewarganegaraan 1" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspKewarganegaraan2" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 2" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspKewarganegaraan3" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan 3" AnchorHorizontal="70%"></ext:DisplayField>
                    <ext:DisplayField ID="DspNegaraDomisili" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Negara Domisili" AnchorHorizontal="70%"></ext:DisplayField>

                    <ext:GridPanel ID="GridPanel1" runat="server" Title="Informasi Telepon" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorPhone" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column1" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanel2" runat="server" Title="Informasi Address" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorAddress" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column7" runat="server" DataIndex="Type_Address" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="Addres" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="Cty" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="CountryCode" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DisplayField ID="DspEmail" LabelWidth="250" runat="server" FieldLabel="Email" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail2" LabelWidth="250" runat="server" FieldLabel="Email 2" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail3" LabelWidth="250" runat="server" FieldLabel="Email 3" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail4" LabelWidth="250" runat="server" FieldLabel="Email 4" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspEmail5" LabelWidth="250" runat="server" FieldLabel="Email 5" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspOccupation" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Occupation" AnchorHorizontal="90%"></ext:DisplayField>
                    <ext:DisplayField ID="DspTempatBekerja" LabelWidth="250" runat="server" FieldLabel="Tempat Bekerja" AnchorHorizontal="90%"></ext:DisplayField>

                    <ext:GridPanel ID="GridPanel3" runat="server" Title="Informasi Telepon Tempat Kerja" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorPhoneEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model8" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column15" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="Communication_Type" Text="Alat Komunikasi" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column18" runat="server" DataIndex="number" Text="Nomor Telepon" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanel4" runat="server" Title="Informasi Address Tempat Kerja" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreDirectorAddressEmployer" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model17" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column21" runat="server" DataIndex="Type_Address" Text="Kategori Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="Addres" Text="Alamat" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="Cty" Text="Kota" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="CountryCode" Text="Negara" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn4" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorEmployerAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanel5" runat="server" Title="Informasi Identitas" AutoScroll="true">
                        <Store>
                            <ext:Store ID="StoreIdentificationDirector" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model18" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column6" runat="server" DataIndex="Tipe" Text="Jenis Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="No" Text="Nomor Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="IssueDate" Text="Tanggal Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="ExpiryDate" Text="Tanggal Kedaluwarsa" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="IssuedCountry" Text="Negara Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn5" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdDirectorIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DisplayField runat="server" LabelWidth="250" ID="DspDeceased" FieldLabel="Deceased ?"></ext:DisplayField>
                    <%--daniel 2020 des 29--%>
                    <ext:DisplayField runat="server" LabelWidth="250" Hidden="true" FieldLabel="Deceased Date " ID="DspDeceasedDate" />
                    <%--end 2020 des 29--%>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="NPWP" ID="DspNPWP" AnchorHorizontal="90%" />
                    <ext:DisplayField runat="server" LabelWidth="250" ID="DspPEP" FieldLabel="PEP ?"></ext:DisplayField>
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Source of Wealth" ID="DspSourceofWealth" AnchorHorizontal="90%" />
                    <ext:DisplayField runat="server" LabelWidth="250" FieldLabel="Catatan" ID="DspCatatan" AnchorHorizontal="90%" />

                </Items>
                <Buttons>
                    <ext:Button ID="Button5" runat="server" Text="Back" DefaultAlign="center">
                        <DirectEvents>
                            <Click OnEvent="btnBackSaveCustomerDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorPhone" runat="server" Icon="ApplicationViewDetail" Title="Director Informasi Telepon" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="PanelPhoneDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspKategoriKontakDirector" runat="server" FieldLabel="Kategori Kontak"></ext:DisplayField>
                    <ext:DisplayField ID="DspJenisAlatKomunikasiDirector" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodeAreaTelpDirector" runat="server" FieldLabel="Kode Area Telp"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorTeleponDirector" runat="server" FieldLabel="Nomor Telepon"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorExtensiDirector" runat="server" FieldLabel="Nomor Extensi"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanDirectorPhoneDirector" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button3" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelPhoneDirectorEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspKategoriKontakDirectorEmployer" runat="server" FieldLabel="Kategori Kontak"></ext:DisplayField>
                    <ext:DisplayField ID="DspJenisAlatKomunikasiDirectorEmployer" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:DisplayField>
                    <ext:DisplayField ID="DspKodeAreaTelpDirectorEmployer" runat="server" FieldLabel="Kode Area Telp"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorTeleponDirectorEmployer" runat="server" FieldLabel="Nomor Telepon"></ext:DisplayField>
                    <ext:DisplayField ID="DspNomorExtensiDirectorEmployer" runat="server" FieldLabel="Nomor Extensi"></ext:DisplayField>
                    <ext:DisplayField ID="DspCatatanDirectorPhoneDirectorEmployer" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button4" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelDirectorEmployerPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorIdentification" runat="server" Icon="ApplicationViewDetail" Title="Informasi Telepon" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <ext:FormPanel ID="PanelDetailIdentificationDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DsptTypeDirector" runat="server" FieldLabel="Jenis Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspNumberDirector" runat="server" FieldLabel="Nomor Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssueDateDirector" runat="server" FieldLabel="Tanggal Diterbitkan"></ext:DisplayField>
                    <ext:DisplayField ID="DspExpiryDateDirector" runat="server" FieldLabel="Tanggal Kadaluarsa"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedByDirector" runat="server" FieldLabel="Diterbitkan Oleh"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedCountryDirector" runat="server" FieldLabel="Negara Penerbit Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspIdentificationCommentDirector" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button6" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelIdentificationDirector_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailDirectorIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailIdentification" runat="server" Icon="ApplicationViewDetail" Title="Informasi Telepon" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <ext:FormPanel ID="PanelDetailIdentification" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DsptType" runat="server" FieldLabel="Jenis Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspNumber" runat="server" FieldLabel="Nomor Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssueDate" runat="server" FieldLabel="Tanggal Diterbitkan"></ext:DisplayField>
                    <ext:DisplayField ID="DspExpiryDate" runat="server" FieldLabel="Tanggal Kadaluarsa"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedBy" runat="server" FieldLabel="Diterbitkan Oleh"></ext:DisplayField>
                    <ext:DisplayField ID="DspIssuedCountry" runat="server" FieldLabel="Negara Penerbit Identitas"></ext:DisplayField>
                    <ext:DisplayField ID="DspIdentificationComment" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button8" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelIdentification_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailIdentification}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailDirectorAddress" runat="server" Icon="ApplicationViewDetail" Title="Informasi Alamat Director" Hidden="true" MinWidth="800" Height="300" BodyPadding="6" Layout="FitLayout">
        <Items>
            <ext:FieldSet ID="FieldSet4" runat="server" Title="Address" Collapsible="true" Layout="FitLayout">
                <Content>
                    <ext:FormPanel ID="PanelAddressDirector" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                        <Items>
                            <ext:DisplayField ID="DspTipeAlamatDirector" runat="server" FieldLabel="Tipe Alamat"></ext:DisplayField>
                            <ext:DisplayField ID="DspAlamatDirector" runat="server" FieldLabel="Alamat"></ext:DisplayField>
                            <ext:DisplayField ID="DspKecamatanDirector" runat="server" FieldLabel="Kecamatan"></ext:DisplayField>
                            <ext:DisplayField ID="DspKotaKabupatenDirector" runat="server" FieldLabel="Kota/Kabupaten"></ext:DisplayField>
                            <ext:DisplayField ID="DspKodePosDirector" runat="server" FieldLabel="Kode Pos"></ext:DisplayField>
                            <ext:DisplayField ID="DspNegaraDirector" runat="server" FieldLabel="Negara"></ext:DisplayField>
                            <ext:DisplayField ID="DspProvinsiDirector" runat="server" FieldLabel="Provinsi"></ext:DisplayField>
                            <ext:DisplayField ID="DspCatatanAddressDirector" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button1" runat="server" Icon="Cancel" Text="Cancel">
                                <DirectEvents>
                                    <Click OnEvent="BtnCancelDirectorDetailAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <ext:FormPanel ID="PanelAddressDirectorEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                        <Items>
                            <ext:DisplayField ID="DspTipeAlamatDirectorEmployer" runat="server" FieldLabel="Tipe Alamat"></ext:DisplayField>
                            <ext:DisplayField ID="DspAlamatDirectorEmployer" runat="server" FieldLabel="Alamat"></ext:DisplayField>
                            <ext:DisplayField ID="DspKecamatanDirectorEmployer" runat="server" FieldLabel="Kecamatan"></ext:DisplayField>
                            <ext:DisplayField ID="DspKotaKabupatenDirectorEmployer" runat="server" FieldLabel="Kota/Kabupaten"></ext:DisplayField>
                            <ext:DisplayField ID="DspKodePosDirectorEmployer" runat="server" FieldLabel="Kode Pos"></ext:DisplayField>
                            <ext:DisplayField ID="DspNegaraDirectorEmployer" runat="server" FieldLabel="Negara"></ext:DisplayField>
                            <ext:DisplayField ID="DspProvinsiDirectorEmployer" runat="server" FieldLabel="Provinsi"></ext:DisplayField>
                            <ext:DisplayField ID="DspCatatanAddressDirectorEmployer" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                        </Items>
                        <Buttons>
                            <ext:Button ID="Button2" runat="server" Icon="Cancel" Text="Cancel">
                                <DirectEvents>
                                    <Click OnEvent="BtnCancelDirectorDetailEmployerAddress_DirectEvent">
                                        <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                </Content>
            </ext:FieldSet>
        </Items>
    </ext:Window>

    <ext:Window ID="WindowDetailPhone" runat="server" Icon="ApplicationViewDetail" Title="Informasi Telepon" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <ext:FormPanel ID="PanelDetailPhone" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="Dsptph_contact_type" runat="server" FieldLabel="Kategori Kontak"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_communication_type" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_country_prefix" runat="server" FieldLabel="Kode Area Telp"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_number" runat="server" FieldLabel="Nomor Telepon"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_extension" runat="server" FieldLabel="Nomor Extensi"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsPhone" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnCancelPhone" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelPhone_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelDetailPhoneEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="Dsptph_contact_typeEmployer" runat="server" FieldLabel="Kategori Kontak"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_communication_typeEmployer" runat="server" FieldLabel="Jenis Alat Komunikasi"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_country_prefixEmployer" runat="server" FieldLabel="Kode Area Telp"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_numberEmployer" runat="server" FieldLabel="Nomor Telepon"></ext:DisplayField>
                    <ext:DisplayField ID="Dsptph_extensionEmployer" runat="server" FieldLabel="Nomor Extensi"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsPhoneEmployer" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button16" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelPhoneEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailPhone}.center()" />
        </Listeners>
    </ext:Window>

    <ext:Window ID="WindowDetailAddress" runat="server" Icon="ApplicationViewDetail" Title="Informasi Alamat" Hidden="true" Layout="FitLayout" Modal="true" MinHeight="150">
        <Items>
            <ext:FormPanel ID="PanelDetailAddress" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspAddress_type" runat="server" FieldLabel="Tipe Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspAddress" runat="server" FieldLabel="Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspTown" runat="server" FieldLabel="Kecamatan"></ext:DisplayField>
                    <ext:DisplayField ID="DspCity" runat="server" FieldLabel="Kota/Kabupaten"></ext:DisplayField>
                    <ext:DisplayField ID="DspZip" runat="server" FieldLabel="Kode Pos"></ext:DisplayField>
                    <ext:DisplayField ID="Dspcountry_code" runat="server" FieldLabel="Negara"></ext:DisplayField>
                    <ext:DisplayField ID="DspState" runat="server" FieldLabel="Provinsi"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsAddress" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="BtnCancelAddress" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelAddress_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:FormPanel ID="PanelDetailAddressEmployer" runat="server" AnchorHorizontal="100%" BodyPadding="10" Hidden="true" DefaultAlign="center" AutoScroll="true">
                <Items>
                    <ext:DisplayField ID="DspAddress_typeEmployer" runat="server" FieldLabel="Tipe Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspAddressEmployer" runat="server" FieldLabel="Alamat"></ext:DisplayField>
                    <ext:DisplayField ID="DspTownEmployer" runat="server" FieldLabel="Kecamatan"></ext:DisplayField>
                    <ext:DisplayField ID="DspCityEmployer" runat="server" FieldLabel="Kota/Kabupaten"></ext:DisplayField>
                    <ext:DisplayField ID="DspZipEmployer" runat="server" FieldLabel="Kode Pos"></ext:DisplayField>
                    <ext:DisplayField ID="Dspcountry_codeEmployer" runat="server" FieldLabel="Negara"></ext:DisplayField>
                    <ext:DisplayField ID="DspStateEmployer" runat="server" FieldLabel="Provinsi"></ext:DisplayField>
                    <ext:DisplayField ID="DspcommentsAddressEmployer" runat="server" FieldLabel="Catatan"></ext:DisplayField>
                </Items>
                <Buttons>
                    <ext:Button ID="Button19" runat="server" Icon="Cancel" Text="Cancel">
                        <DirectEvents>
                            <Click OnEvent="BtnCancelAddressEmployer_DirectEvent">
                                <EventMask Msg="Loading..." ShowMask="true" MinDelay="500"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowDetailAddress}.center()" />
        </Listeners>
    </ext:Window>

    <ext:FormPanel runat="server" Title="WIC Detail" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="WIC_Panel">
        <Defaults>
             
            <ext:Parameter Name="LabelWidth" Value="175" />
        </Defaults>

        <Items>
            <ext:DisplayField ID="PKWIC" runat="server" FieldLabel="ID"></ext:DisplayField>
            <ext:DisplayField ID="DSpWICNo" runat="server" FieldLabel="WIC No"></ext:DisplayField>
            <ext:DisplayField ID="DspIsUpdateFromDataSource" runat="server" FieldLabel="Update From Data Source ?"></ext:DisplayField>
            <ext:FormPanel runat="server" Title="WIC Individu" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="WIC_Individu">
                <Defaults>
                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
            <ext:Parameter Name="LabelWidth" Value="175" />

                </Defaults>
                <Items>
                    <ext:DisplayField ID="TxtINDV_Gender" runat="server" FieldLabel="Jenis Kelamin"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Title" runat="server" FieldLabel="Gelar" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_last_name" runat="server" FieldLabel="Nama Lengkap" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="DateINDV_Birthdate" runat="server" Flex="1" FieldLabel="Tanggal Lahir" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Birth_place" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Mothers_name" runat="server" FieldLabel="Nama Ibu Kandung" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Alias" runat="server" FieldLabel="Nama Alias" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_SSN" runat="server" FieldLabel="NIK" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Passport_number" runat="server" FieldLabel="No. Passport" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Passport_country" runat="server" FieldLabel="Negara Penerbit Passport" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_ID_Number" runat="server" FieldLabel="No. Identitas Lain" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CmbINDV_Nationality1" runat="server" FieldLabel="Kewarganegaraan 1" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CmbINDV_Nationality2" runat="server" FieldLabel="Kewarganegaraan 2" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CmbINDV_Nationality3" runat="server" FieldLabel="Kewarganegaraan 3" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CmbINDV_residence" runat="server" FieldLabel="Negara Domisili" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    
                    <ext:GridPanel ID="GridPanelPhoneIndividuDetail" runat="server" Title="Informasi Telepon" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="StorePhoneIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="ModelDetail" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumberPhoneDetailIndividu" Text="No"  Width="60" runat="server"></ext:RowNumbererColumn>
                                <ext:Column ID="colTph_Contact_TypeIndividu" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" flex="1"></ext:Column>
                                <ext:Column ID="coltph_communication_typeIndividu" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" flex="1"></ext:Column>
                                <ext:Column ID="coltph_numberIndividu" runat="server" DataIndex="number" Text="Nomor Telepon" flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhoneIndividuDetail" runat="server" Text="Action" Flex="1">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhoneIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar15" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelAddressIndividuDetail" runat="server" Title="Informasi Alamat" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="StoreAddressIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                            </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailIndividu" Text="No"  Width="60" runat="server"></ext:RowNumbererColumn>
                                <ext:Column ID="ColAddress_typeIndividu" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" flex="1"></ext:Column>
                                <ext:Column ID="ColAddressIndividu" runat="server" DataIndex="Addres" Text="Alamat" flex="2"></ext:Column>
                                <ext:Column ID="ColCityIndividu" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" flex="1"></ext:Column>
                                <ext:Column ID="Colcountry_codeIndividu" runat="server" DataIndex="CountryCode" Text="Negara" flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAddressIndividuDetail" runat="server" Text="Action" Flex="1" >
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddressIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    
                    <ext:DisplayField ID="TxtINDV_Email" runat="server" FieldLabel="Email" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Email2" runat="server" FieldLabel="Email2" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Email3" runat="server" FieldLabel="Email3" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Email4" runat="server" FieldLabel="Email4" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Email5" runat="server" FieldLabel="Email5" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Occupation" runat="server" FieldLabel="Pekerjaan" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_employer_name" runat="server" FieldLabel="Tempat Bekerja" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>

                    <ext:GridPanel ID="GridPanelPhoneIndividualEmployerDetail" runat="server" Title="Informasi Telepon Tempat Bekerja" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="StorePhoneEmployerIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model20" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn12" Text="No"  Width="60" runat="server"></ext:RowNumbererColumn>
                                <ext:Column ID="Column27" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" flex="1"></ext:Column>
                                <ext:Column ID="Column28" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" flex="1"></ext:Column>
                                <ext:Column ID="Column29" runat="server" DataIndex="number" Text="Nomor Telepon" flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhoneIndividualEmployerDetail" runat="server" Text="Action" Flex="1" >
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhoneEmployerIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar7" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelAddressIndividualEmployerDetail" runat="server" Title="Informasi Alamat Tempat Bekerja" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="StoreAddressEmployerIndividuDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model29" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Addres" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Cty" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                            </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn13" Text="No" runat="server" Width="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column30" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" flex="1"></ext:Column>
                                <ext:Column ID="Column33" runat="server" DataIndex="Addres" Text="Alamat" flex="1"></ext:Column>
                                <ext:Column ID="Column34" runat="server" DataIndex="Cty" Text="Kota/Kabupaten" flex="1"></ext:Column>
                                <ext:Column ID="Column35" runat="server" DataIndex="CountryCode" Text="Negara" flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAddressIndividualEmployerDetail" runat="server" Text="Action" Flex="1" >
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddressEmployerIndividu">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar8" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelIdentificationIndividualDetail" runat="server" Title="Informasi Identitas" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="StoreIdentificationDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model30" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Tipe" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssueDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ExpiryDate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IssuedCountry" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn14" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column36" runat="server" DataIndex="Tipe" Text="Jenis Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column37" runat="server" DataIndex="No" Text="Nomor Identitas" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column38" runat="server" DataIndex="IssueDate" Text="Tanggal Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column39" runat="server" DataIndex="ExpiryDate" Text="Tanggal Kedaluwarsa" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column40" runat="server" DataIndex="IssuedCountry" Text="Negara Diterbitkan" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnIdentificationIndividualDetail" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar9" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DisplayField ID="CbINDV_Deceased" runat="server" FieldLabel="Sudah Meninggal" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="DateINDV_Deceased_Date" runat="server" Flex="1" FieldLabel="Tanggal Meninggal" Hidden="true" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Tax_Number" runat="server" FieldLabel="NPWP" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CbINDV_Tax_Reg_Number" runat="server" FieldLabel="is PEP ?" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Source_Of_Wealth" runat="server" FieldLabel="Sumber Dana" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtINDV_Comments" runat="server" Flex="1" FieldLabel="Catatan" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>

                </Items>
            </ext:FormPanel>

            <ext:FormPanel runat="server" Title="WIC Corporate" BodyPadding="30" MonitorResize="true" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" ID="WIC_Corporate">
                <Defaults>
                    <ext:Parameter Name="margin" Value="0 0 10 0" Mode="Value" />
                </Defaults>
                <Items>
                    <ext:DisplayField ID="TxtCorp_Name" runat="server" FieldLabel="Nama Korporasi" AnchorHorizontal="80%"  LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtCorp_Commercial_name" runat="server" FieldLabel="Nama Komersial" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CmbCorp_Incorporation_legal_form" runat="server" FieldLabel="Bentuk Korporasi" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtCorp_Incorporation_number" runat="server" FieldLabel="Nomor Induk Berusaha" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtCorp_Business" runat="server" FieldLabel="Bidang Usaha" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>

                    <ext:GridPanel ID="GridPanelPhoneCorporateDetail" runat="server" Title="Informasi Telepon" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="StorePhoneCorporateDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Ref_Detail_Of" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_for_Table_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Contact_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Communication_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_extension" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="comments" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererPhoneDetailCorporate" Text="No"  Width="60" runat="server"></ext:RowNumbererColumn>
                                <ext:Column ID="colTph_Contact_TypeCorporate" runat="server" DataIndex="Contact_Type" Text="Kategori Kontak" flex="1"></ext:Column>
                                <ext:Column ID="coltph_communication_typeCorporate" runat="server" DataIndex="Communication_Type" Text="Jenis Alat Komunikasi" flex="1"></ext:Column>
                                <ext:Column ID="coltph_numberCorporate" runat="server" DataIndex="tph_number" Text="Nomor Telepon" flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnPhoneCorporateDetail" runat="server" Text="Action" Flex="1" >
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhoneCorporate">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar10" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelAddressCorporateDetail" runat="server" Title="Informasi Alamat" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="StoreAddressCorporateDetail" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="FK_Ref_detail_of" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_to_Table_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Type_Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CountryCode" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererAddressDetailCorporate" Text="No"  Width="60" runat="server"></ext:RowNumbererColumn>
                                <ext:Column ID="ColAddress_typeCorporate" runat="server" DataIndex="Type_Address" Text="Tipe Alamat" flex="1"></ext:Column>
                                <ext:Column ID="ColAddressCorporate" runat="server" DataIndex="Address" Text="Alamat" flex="2"></ext:Column>
                                <ext:Column ID="ColCityCorporate" runat="server" DataIndex="City" Text="Kota/Kabupaten" flex="1"></ext:Column>
                                <ext:Column ID="Colcountry_codeCorporate" runat="server" DataIndex="CountryCode" Text="Negara" flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAddressCorporateDetail" runat="server" Text="Action" Flex="1" >
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddressCorporate">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar11" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:DisplayField ID="TxtCorp_Email" runat="server" FieldLabel="Email Korporasi" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtCorp_url" runat="server" FieldLabel="Website Korporasi" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtCorp_incorporation_state" runat="server" FieldLabel="Provinsi" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="CmbCorp_incorporation_country_code" runat="server" FieldLabel="Negara" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>

                    <ext:GridPanel ID="GP_Corp_Director" runat="server" Title="Director Corp" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="Store_Director_Corp" runat="server" IsPagingStore="true" PageSize="8">
                                <Model>
                                    <ext:Model ID="Model_Director_Corp" runat="server">
                                        <%--to do--%>
                                        <Fields>
                                            <ext:ModelField Name="NO_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Roles" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No" MinWidth="60"></ext:RowNumbererColumn>
                                <ext:Column ID="Column31" runat="server" DataIndex="Last_Name" Text="Name" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column32" runat="server" DataIndex="Roles" Text="Role" MinWidth="130" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnDirector" runat="server" Text="Action" Flex="1" MinWidth="200">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="Application" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GrdCmdCustDirector">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.NO_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar12" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <%--daniel 12 jan 2021--%>
                    <%--<ext:DisplayField ID="CmbCorp_Role" runat="server" FieldLabel="Peran" AnchorHorizontal="80%"></ext:DisplayField>--%>
                    <%--end 12 jan 2021--%>
                    <ext:DisplayField ID="DateCorp_incorporation_date" runat="server" Flex="1" LabelWidth="175" FieldLabel="Tanggal Pendirian"></ext:DisplayField>
                    <ext:DisplayField ID="chkCorp_business_closed" runat="server" FieldLabel="Tutup?" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="DateCorp_date_business_closed" runat="server" Flex="1" FieldLabel="Tanggal Tutup" Hidden="true" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtCorp_tax_number" runat="server" FieldLabel="NPWP" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                    <ext:DisplayField ID="TxtCorp_Comments" runat="server" FieldLabel="Catatan" AnchorHorizontal="80%" LabelWidth="175"></ext:DisplayField>
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btnCancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
