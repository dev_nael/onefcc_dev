﻿<%@ Page Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="GeneratePPATKAll.aspx.vb" Inherits="goAML_GeneratePPATKAll" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ext:Window ID="WindowLastUpdateDate" Layout="AnchorLayout" Title="Last Update Date" runat="server" Modal="true" Hidden="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:ComboBox ID="cmblast_update_date" runat="server" FieldLabel="Last Update Date" DisplayField="LastUpdateDate" ValueField="LastUpdateDate" EmptyText="Pilih Salah Satu" AnchorHorizontal="80%" ForceSelection="true" AutoLoadOnValue="true" AllowBlank="false" Editable="false">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreLastUpdateDateReport" OnReadData="ReportLastUpdateDate_readData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server" ID="Model356">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="LastUpdateDate" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Proxy>
                            <ext:PageProxy>
                            </ext:PageProxy>
                        </Proxy>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.5, height: size.height * 0.5});" />
            <Resize Handler="#{WindowLastUpdateDate}.center()" />
        </Listeners>
        <Buttons>

            <ext:Button ID="BtnsaveLastUpdateDate" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">
                <DirectEvents>
                    <Click OnEvent="btnSaveLastUpdateDate_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancelLastUpdateDate" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancelLastUpdateDate_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:FormPanel ID="FormPanelInput" runat="server" Layout="AnchorLayout" ButtonAlign="Center" Title="" BodyStyle="padding:20px" AutoScroll="true" Hidden="false">
        <Items>
            <ext:DateField LabelWidth="250" AllowBlank="False" runat="server" FieldLabel="Tanggal Transaksi" ID="txt_Report_Transaction_Date" AnchorHorizontal="90%" Format="dd-MMM-yyyy" />
            <ext:ComboBox ID="Cmb_Jenis_Laporan" AllowBlank="False" LabelWidth="250" runat="server" FieldLabel="Jenis Laporan" DisplayField="Keterangan" ValueField="Kode" EmptyText="Select One" AnchorHorizontal="90%" ForceSelection="true">
                <Store>
                    <ext:Store runat="server" ClientIDMode="Static" ID="StoreReportJenis" OnReadData="JenisLaporan_ReadData" IsPagingStore="true" AutoLoad="false">
                        <Model>
                            <ext:Model runat="server" ID="Model31">
                                <Fields>
                                    <ext:ModelField Name="Kode" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
            <ext:Panel runat="server" ID="PanelReport" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Last Update Date Report" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                <Items>
                    <ext:GridPanel ID="GridPanelReport" runat="server" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="BtnAddLastUpdate" Text="Add Last Update Date" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddLastUpdate_click">
                                                <EventMask ShowMask="true" Msg="Processing..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>

                                    <%-- Add button untuk semua Last Update Date by Transaction Date --%>
                                    <ext:Button runat="server" ID="BtnAddLastUpdateAll" Text="Add All Last Update Date" Icon="TableAdd">
                                        <DirectEvents>
                                            <Click OnEvent="BtnAddLastUpdateAll_click">
                                                <EventMask ShowMask="true" Msg="Processing..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <%-- End of Add button untuk semua Last Update Date by Transaction Date --%>

                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreLastUpdateDateGridReport" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model355" IDProperty="IDPk">
                                        <Fields>
                                            <ext:ModelField Name="IDPk" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="transactiondate" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="jenislaporan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="lastupdatedate" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn88" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column2" runat="server" DataIndex="IDPk" Text="ID" Flex="1" Hidden="true"></ext:Column>
                                <ext:DateColumn ID="Column312" runat="server" DataIndex="transactiondate" Text="Transaction Date" Width="150" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column1" runat="server" DataIndex="jenislaporan" Text="Jenis Laporan" Width="300"></ext:Column>
                                <ext:DateColumn ID="Column313" runat="server" DataIndex="lastupdatedate" Text="Last Update Date" Width="150" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:CommandColumn ID="CommandColumn87" runat="server" Text="Action" Flex="1">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridcommandReportUpdateDate">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.IDPk" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:GridPanel ID="GridPanelValidation" ClientIDMode="Static" runat="server" Height="400" Title="Validation Schema">
                        <Store>
                            <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="StoreValidation_ReadData">
                                <Sorters>
                                </Sorters>
                                <Proxy>

                                    <ext:PageProxy />
                                </Proxy>
                                <Reader>
                                </Reader>

                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="UnikReference" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ErrorMessage" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column7" runat="server" DataIndex="UnikReference" Text="Unik Reference" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="ErrorMessage" Text="ErrorMessage" MinWidth="130" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>

                         <BottomBar>

                <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True">
                    <Items>

                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
                         <View>
                <ext:GridView runat="server" EnableTextSelection="true" />
            </View>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btnValidateSchema" runat="server" Icon="Disk" Text="Validate Schema" ValidationGroup="MainForm">
                <DirectEvents>
                    <Click OnEvent="btnValidatingSchema_Click">
                        <EventMask ShowMask="true" Msg="Validating Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btnSaveUpload" runat="server" Text="Generate" Icon="Disk"  >
                <DirectEvents>
                    <Click OnEvent="BtnSave_DirectClick" IsUpload="true">
                      <%--  <EventMask ShowMask="true" MinDelay="10" Msg="Wait.."></EventMask>--%>
                    </Click>

                </DirectEvents>
            </ext:Button>
           <%-- <ext:Button ID="btnCancelUpload" runat="server" Text="Back" Icon="Cancel">
                <DirectEvents>
                    <Click OnEvent="btnCancelUpload_Click"></Click>
                </DirectEvents>
            </ext:Button>--%>
        </Buttons>
    </ext:FormPanel>
</asp:Content>