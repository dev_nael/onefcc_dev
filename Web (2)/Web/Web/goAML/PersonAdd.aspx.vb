﻿Imports NawaDevDAL
Imports NawaBLL
Imports System.Data.SqlClient
Imports NawaDevBLL.GlobalReportFunctionBLL
Imports System.Data
Imports System.Globalization
Imports NawaDAL
Imports Elmah
Imports Ext

Partial Class goAML_PersonAdd
    Inherits ParentPage
    Public goAML_PersonBLL As New NawaDevBLL.ReportingPerson()
#Region "Page Load"
    Protected Sub goAML_PersonAdd_PreLoad() Handles Me.PreLoad
        Me.ActionType = NawaBLL.Common.ModuleActionEnum.Insert
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Ext.Net.X.IsAjaxRequest Then
            ClearSession()

            Dim moduleStr As String = Request.Params("ModuleID")
            Dim moduleID As Integer = NawaBLL.Common.DecryptQueryString(moduleStr, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Me.ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(moduleID)

            'LoadPlaceHolder()
            goAML_PersonBLL = New NawaDevBLL.ReportingPerson()
            Dim conn As New System.Data.SqlClient.SqlConnection(NawaDAL.SQLHelper.strConnectionString)

            'Load jenis dokumen identitas
            Dim kue4 As String = "SELECT Kode code, Keterangan display FROM goAML_Ref_Jenis_Dokumen_Identitas WHERE Active = 1"
            Dim dokuStore As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(conn, Data.CommandType.Text, kue4)
            IDModal_Type.GetStore().DataSource = dokuStore
            IDModal_Type.GetStore().DataBind()

            conn.Close()
            LoadDataComboBox()

            ColumnActionLocation(PhoneField_Grid, CommandColumnPhone)
            ColumnActionLocation(AddressField_Grid, CommandColumnAddress)
            ColumnActionLocation(GridPanel_employerAddress, CommandColumnAddressEmp)
            ColumnActionLocation(GridPanel_EmployerPhone, CommandColumnPhoneEmp)
            ColumnActionLocation(IDField_Grid, CommandColumnIdent)
        End If
    End Sub

#End Region
    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase)
        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If


        If bsettingRight = 1 Then
            'GridPanelReportIndicator.ColumnModel.Columns.Add(CommandColumn87)
        ElseIf bsettingRight = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub
#Region "Session"
    Public Property ReportingPersonClass As NawaDevBLL.ReportingPersonData
        Get
            Return Session("goAML_PersonAdd.ReportingPersonClass")
        End Get
        Set(ByVal value As NawaDevBLL.ReportingPersonData)
            Session("goAML_PersonAdd.ReportingPersonClass") = value
        End Set
    End Property

    Public Property ObjIdentification As NawaDevDAL.goAML_Person_Identification
        Get
            Return Session("goAML_PersonAdd.ObjIdentification")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Person_Identification)
            Session("goAML_PersonAdd.ObjIdentification") = value
        End Set
    End Property

    Public Property ObjPhone As NawaDevDAL.goAML_Ref_Phone
        Get
            Return Session("goAML_PersonAdd.ObjPhone")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Phone)
            Session("goAML_PersonAdd.ObjPhone") = value
        End Set
    End Property

    Public Property ObjEmployerPhone As NawaDevDAL.goAML_Ref_Phone
        Get
            Return Session("goAML_PersonAdd.ObjEmployerPhone")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Phone)
            Session("goAML_PersonAdd.ObjEmployerPhone") = value
        End Set
    End Property

    Public Property ObjAddress As NawaDevDAL.goAML_Ref_Address
        Get
            Return Session("goAML_PersonAdd.ObjAddress")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Address)
            Session("goAML_PersonAdd.ObjAddress") = value
        End Set
    End Property

    Public Property ObjEmployerAddress As NawaDevDAL.goAML_Ref_Address
        Get
            Return Session("goAML_PersonAdd.ObjEmployerAddress")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_Ref_Address)
            Session("goAML_PersonAdd.ObjEmployerAddress") = value
        End Set
    End Property

    Public Property ObjVwIdentification As NawaDevDAL.Vw_Temp_Identification
        Get
            Return Session("goAML_PersonAdd.ObjVwIdentification")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Temp_Identification)
            Session("goAML_PersonAdd.ObjVwIdentification") = value
        End Set
    End Property

    Public Property ObjVwPhone As NawaDevDAL.Vw_Temp_Phone
        Get
            Return Session("goAML_PersonAdd.ObjVwPhone")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Temp_Phone)
            Session("goAML_PersonAdd.ObjVwPhone") = value
        End Set
    End Property

    Public Property ObjVwAddress As NawaDevDAL.Vw_Temp_Address
        Get
            Return Session("goAML_PersonAdd.ObjVwAddress")
        End Get
        Set(ByVal value As NawaDevDAL.Vw_Temp_Address)
            Session("goAML_PersonAdd.ObjVwAddress") = value
        End Set
    End Property

    Public Property ListIdentification As List(Of goAML_Person_Identification)
        Get
            If Session("goAML_PersonAdd.ListIdentification") Is Nothing Then
                Session("goAML_PersonAdd.ListIdentification") = New List(Of goAML_Person_Identification)
            End If
            Return Session("goAML_PersonAdd.ListIdentification")
        End Get
        Set(ByVal value As List(Of goAML_Person_Identification))
            Session("goAML_PersonAdd.ListIdentification") = value
        End Set
    End Property


    Public Property ListPhone As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_PersonAdd.ListPhone") Is Nothing Then
                Session("goAML_PersonAdd.ListPhone") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_PersonAdd.ListPhone")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_PersonAdd.ListPhone") = value
        End Set
    End Property
    Public Property ListPhoneEmployer As List(Of goAML_Ref_Phone)
        Get
            If Session("goAML_PersonAdd.ListPhoneEmployer") Is Nothing Then
                Session("goAML_PersonAdd.ListPhoneEmployer") = New List(Of goAML_Ref_Phone)
            End If
            Return Session("goAML_PersonAdd.ListPhoneEmployer")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Phone))
            Session("goAML_PersonAdd.ListPhoneEmployer") = value
        End Set
    End Property

    Public Property ListAddress As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_PersonAdd.ListAddress") Is Nothing Then
                Session("goAML_PersonAdd.ListAddress") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_PersonAdd.ListAddress")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_PersonAdd.ListAddress") = value
        End Set
    End Property
    Public Property ListAddressEmployer As List(Of goAML_Ref_Address)
        Get
            If Session("goAML_PersonAdd.ListAddressEmployer") Is Nothing Then
                Session("goAML_PersonAdd.ListAddressEmployer") = New List(Of goAML_Ref_Address)
            End If
            Return Session("goAML_PersonAdd.ListAddressEmployer")
        End Get
        Set(ByVal value As List(Of goAML_Ref_Address))
            Session("goAML_PersonAdd.ListAddressEmployer") = value
        End Set
    End Property
    Public Property VwListIdentification As List(Of Vw_Temp_Identification)
        Get
            If Session("goAML_PersonAdd.VwListIdentification") Is Nothing Then
                Session("goAML_PersonAdd.VwListIdentification") = New List(Of Vw_Temp_Identification)
            End If
            Return Session("goAML_PersonAdd.VwListIdentification")
        End Get
        Set(ByVal value As List(Of Vw_Temp_Identification))
            Session("goAML_PersonAdd.VwListIdentification") = value
        End Set
    End Property

    Public Property VwListPhone As List(Of Vw_Temp_Phone)
        Get
            If Session("goAML_PersonAdd.VwListPhone") Is Nothing Then
                Session("goAML_PersonAdd.VwListPhone") = New List(Of Vw_Temp_Phone)
            End If
            Return Session("goAML_PersonAdd.VwListPhone")
        End Get
        Set(ByVal value As List(Of Vw_Temp_Phone))
            Session("goAML_PersonAdd.VwListPhone") = value
        End Set
    End Property

    Public Property VwListAddress As List(Of Vw_Temp_Address)
        Get
            If Session("goAML_PersonAdd.VwListAddress") Is Nothing Then
                Session("goAML_PersonAdd.VwListAddress") = New List(Of Vw_Temp_Address)
            End If
            Return Session("goAML_PersonAdd.VwListAddress")
        End Get
        Set(ByVal value As List(Of Vw_Temp_Address))
            Session("goAML_PersonAdd.VwListAddress") = value
        End Set
    End Property

    Public Property objListReportPersonAdd() As List(Of goAML_ODM_Reporting_Person)
        Get
            If Session("goAML_PersonAdd.objListReportPersonAdd") Is Nothing Then
                Dim oNewScheduler As New List(Of goAML_ODM_Reporting_Person)

                Session("goAML_PersonAdd.objListReportPersonAdd") = oNewScheduler

            End If
            Return Session("goAML_PersonAdd.objListReportPersonAdd")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Reporting_Person))
            Session("goAML_PersonAdd.objListReportPersonAdd") = value
        End Set
    End Property


    Public Property objReportPersonAddData() As goAML_ODM_Reporting_Person
        Get
            If Session("goAML_PersonAdd.objReportPersonAdd") Is Nothing Then
                Session("goAML_PersonAdd.objReportPersonAdd") = New goAML_ODM_Reporting_Person
            End If
            Return Session("goAML_PersonAdd.objReportPersonAdd")
        End Get
        Set(ByVal value As goAML_ODM_Reporting_Person)
            Session("goAML_PersonAdd.objReportPersonAdd") = value
        End Set
    End Property

#End Region
    Protected Sub UserID_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("VwUserID", "kode, keterangan", strfilter, "keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub National_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Nama_Negara", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub JenisIdentitas_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Dokumen_Identitas", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Communication_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Alat_Komunikasi", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub Contact_Type_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Store = CType(sender, Store)

            objstore.DataSource = SQLHelper.ExecuteTabelPaging("goAML_Ref_Kategori_Kontak", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()

        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub Gender_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan  like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Kelamin", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub JenisLaporan_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("goAML_Ref_Jenis_Laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Private Sub LoadDataComboBox()
        'Negara
        RP_CountryPassport.PageSize = SystemParameterBLL.GetPageSize
        StoreCountryPassport.Reload()
        RP_Nationality1.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignNational1.Reload()
        RP_Nationality2.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignNational2.Reload()
        RP_Nationality3.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignNational3.Reload()
        RP_Residence.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignResisdance.Reload()
        EmployerAddress_Negara.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignCountry_Code.Reload()
        AddrModal_Country.PageSize = SystemParameterBLL.GetPageSize
        StoreFromForeignCountry_CodeAddress.Reload()
        IDModal_Country.PageSize = SystemParameterBLL.GetPageSize
        StoreIDModal_Country.Reload()

        'Gender
        RP_Gender.PageSize = SystemParameterBLL.GetPageSize
        StoreGender.Reload()

        'User ID
        cmb_UserID.PageSize = SystemParameterBLL.GetPageSize
        StoreUserID.Reload()

        'Jenis Laporan
        RP_KodeLaporan.PageSize = SystemParameterBLL.GetPageSize
        StoreKodeLaporan.Reload()

        'Kategori
        PhoneModal_Type.PageSize = SystemParameterBLL.GetPageSize
        StorePhoneModal_Type.Reload()
        EmployerPhone_Kategori.PageSize = SystemParameterBLL.GetPageSize
        StoreEmployerPhone_Kategori.Reload()
        AddrModal_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreAddrModal_Type.Reload()
        EmployerAddress_Tipe.PageSize = SystemParameterBLL.GetPageSize
        StoreEmployerAddress_Tipe.Reload()

        'Alat Komunikasi
        PhoneModal_Tool.PageSize = SystemParameterBLL.GetPageSize
        StorePhoneModal_Tool.Reload()
        EmployerPhone_Jenis.PageSize = SystemParameterBLL.GetPageSize
        StoreEmployerPhone_Jenis.Reload()

        'Jenis Identitas
        IDModal_Type.PageSize = SystemParameterBLL.GetPageSize
        StoreIDModal_Type.Reload()
    End Sub

    Sub ClearSession()
        Me.ActionType = Nothing
        Me.ObjModule = Nothing
        objListReportPersonAdd = Nothing
        objReportPersonAddData = Nothing
        ListPhone = New List(Of goAML_Ref_Phone)
        VwListPhone = New List(Of Vw_Temp_Phone)
        ListAddress = New List(Of goAML_Ref_Address)
        VwListAddress = New List(Of Vw_Temp_Address)
        ListIdentification = New List(Of goAML_Person_Identification)
        VwListIdentification = New List(Of Vw_Temp_Identification)
        ListPhoneEmployer = New List(Of goAML_Ref_Phone)
        ListAddressEmployer = New List(Of goAML_Ref_Address)
        ReportingPersonClass = New NawaDevBLL.ReportingPersonData
        ReportingPersonClass.ObjAddress = New List(Of goAML_Ref_Address)
        ReportingPersonClass.ObjEmpAddress = New List(Of goAML_Ref_Address)
        ReportingPersonClass.ObjPhone = New List(Of goAML_Ref_Phone)
        ReportingPersonClass.ObjEmpPhone = New List(Of goAML_Ref_Phone)
        ReportingPersonClass.ObjIdent = New List(Of goAML_Person_Identification)
        ObjIdentification = Nothing
        ObjPhone = Nothing
        ObjAddress = Nothing
        ObjVwIdentification = Nothing
        ObjVwPhone = Nothing
        ObjVwAddress = Nothing
        ObjEmployerAddress = Nothing
        ObjEmployerPhone = Nothing
    End Sub

    Protected Sub PhoneField_BtnAdd(sender As Object, e As DirectEventArgs)
        PhoneModal.Show()
        AddrModal.Hide()
        IDModal.Hide()

        PhoneModal_Number.Clear()
        PhoneModal_NumberEx.Clear()
        PhoneModal_RegionCode.Clear()
        PhoneModal_Tool.Clear()
        PhoneModal_Type.Clear()
        PhoneModal_Comment.Clear()
    End Sub

    Protected Sub Btn_AddEmployerAddress_click(sender As Object, e As DirectEventArgs)
        WindowEmployerAddress.Show()

        EmployerAddress_Tipe.Clear()
        EmployerAddress_Alamat.Clear()
        EmployerAddress_Kec.Clear()
        EmployerAddress_Kab.Clear()
        EmployerAddress_Pos.Clear()
        EmployerAddress_Negara.Clear()
        EmployerAddress_Prov.Clear()
        EmployerAddress_Note.Clear()
    End Sub

    Protected Sub Btn_AddEmployerPhone_click(sender As Object, e As DirectEventArgs)
        WindowEmployerPhone.Show()

        EmployerPhone_Kategori.Clear()
        EmployerPhone_Jenis.Clear()
        EmployerPhone_Kode.Clear()
        EmployerPhone_Nomor.Clear()
        EmployerPhone_Extensi.Clear()
        EmployerPhone_Nomor.Clear()
        EmployerPhone_Note.Clear()
    End Sub

    Protected Sub IDModal_BtnCancel(sender As Object, e As DirectEventArgs)
        IDModal.Hide()
        IDModalDetail.Hide()
    End Sub

    Protected Sub PhoneModal_BtnCancel(sender As Object, e As DirectEventArgs)
        PhoneModal.Hide()
        PhoneModalDetail.Hide()
        WindowEmployerPhone.Hide()
        WindowEmployerPhoneDetail.Hide()
        ObjPhone = Nothing
        ObjVwPhone = Nothing
        ObjEmployerPhone = Nothing
    End Sub

    Protected Sub AddressModal_BtnCancel(sender As Object, e As DirectEventArgs)
        AddrModal.Hide()
        AddrModalDetail.Hide()
        WindowEmployerAddress.Hide()
        WindowEmployerAddressDetail.Hide()
        ObjAddress = Nothing
        ObjVwAddress = Nothing
        ObjEmployerAddress = Nothing
    End Sub

    Protected Sub IdentificationModal_BtnAdd(sender As Object, e As DirectEventArgs)
        Try
            If ObjIdentification Is Nothing Then
                'Data Store untuk save ke DB
                SaveAddIdentification()
                'Data Store untuk view
                SaveVwListIdenfitication()
            Else
                SaveEditIdentification()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub PhoneModal_BtnAdd(sender As Object, e As DirectEventArgs)
        Try

            If ObjPhone Is Nothing Then
                'Data Store untuk save ke DB
                SaveAddPhone()
                'Data Store untuk view
                SaveVwListPhone()
            Else
                SaveEditPhone()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub PhoneEmployerModal_BtnAdd(sender As Object, e As DirectEventArgs)
        Try
            If ObjEmployerPhone Is Nothing Then
                'Data Store untuk save ke DB
                SaveAddEmployerPhone()
            Else
                SaveEditEmployerPhone()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub AddressModal_BtnAdd(sender As Object, e As DirectEventArgs)
        Try
            If ObjAddress Is Nothing Then
                'Data Store untuk save ke DB
                SaveAddAddress()
                'Data Store untuk view
                SaveVwListAddress()
            Else
                SaveEditAddress()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub AddressEmployerModal_BtnAdd(sender As Object, e As DirectEventArgs)
        Try
            If ObjEmployerAddress Is Nothing Then
                'Data Store untuk save ke DB
                SaveAddAddressEmployer()
            Else
                SaveEditAddressEmployer()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Protected Sub AddressField_BtnAdd(sender As Object, e As DirectEventArgs)
        PhoneModal.Hide()
        AddrModal.Show()
        IDModal.Hide()

        AddrModal_Address.Clear()
        AddrModal_City.Clear()
        AddrModal_Comment.Clear()
        AddrModal_Country.Clear()
        AddrModal_PostalCode.Clear()
        AddrModal_Province.Clear()
        AddrModal_Town.Clear()
        AddrModal_Type.Clear()
    End Sub

    Protected Sub IDField_BtnAdd(sender As Object, e As DirectEventArgs)
        PhoneModal.Hide()
        AddrModal.Hide()
        IDModal.Show()

        IDModal_Comments.Clear()
        IDModal_Country.Clear()
        IDModal_ExpiryDate.Clear()
        IDModal_IssueDate.Clear()
        IDModal_IssuedBy.Clear()
        IDModal_Number.Clear()
        IDModal_Type.Clear()
    End Sub

    Protected Sub RP_IsDeceased_CheckedChanged(sender As Object, e As EventArgs) Handles RP_IsDeceased.DirectCheck

        RP_DeceasedDate.Disabled = Not RP_IsDeceased.Checked

        If RP_IsDeceased.Value = False Then
            RP_DeceasedDate.Clear()
        End If
    End Sub

    'Protected Sub RP_Email1_Changed(sender As Object, e As EventArgs) Handles RP_Email1.DirectChange, RP_Email2.DirectChange, RP_Email3.DirectChange, RP_Email4.DirectChange

    '    If Not RP_Email1.Text = "" Then
    '        RP_Email2.Disabled = False
    '        If Not RP_Email2.Text = "" Then
    '            RP_Email3.Disabled = False
    '            If Not RP_Email3.Text = "" Then
    '                RP_Email4.Disabled = False
    '                If Not RP_Email4.Text = "" Then
    '                    RP_Email5.Disabled = False
    '                Else
    '                    RP_Email5.Disabled = True
    '                    RP_Email5.Clear()
    '                End If
    '            Else
    '                RP_Email4.Disabled = True
    '                RP_Email4.Clear()
    '                RP_Email5.Disabled = True
    '                RP_Email5.Clear()
    '            End If
    '        Else
    '            RP_Email3.Disabled = True
    '            RP_Email3.Clear()
    '            RP_Email4.Disabled = True
    '            RP_Email4.Clear()
    '            RP_Email5.Disabled = True
    '            RP_Email5.Clear()
    '        End If
    '    Else
    '        RP_Email2.Disabled = True
    '        RP_Email2.Clear()
    '        RP_Email3.Disabled = True
    '        RP_Email3.Clear()
    '        RP_Email4.Disabled = True
    '        RP_Email4.Clear()
    '        RP_Email5.Disabled = True
    '        RP_Email5.Clear()
    '    End If
    'End Sub



    Private Sub BindDetail()

        IDField_Store.DataSource = VwListIdentification.OrderBy(Function(x) x.PK_Person_Identification_ID).ToList
        IDField_Store.DataBind()

        PhoneField_Store.DataSource = VwListPhone.OrderBy(Function(x) x.PK_goAML_Ref_Phone).ToList
        PhoneField_Store.DataBind()

        AddressField_Store.DataSource = VwListAddress.OrderBy(Function(x) x.PK_Customer_Address_ID).ToList
        AddressField_Store.DataBind()

        bindPhone(StorePhoneEmployer, ListPhoneEmployer)

        bindAddress(StoreAddressEmployer, ListAddressEmployer)
    End Sub

    Sub bindPhone(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Ref_Phone))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("PK_goAML_ref_P", GetType(Integer)))
        objtable.Columns.Add(New DataColumn("TipeKontak", GetType(String)))
        objtable.Columns.Add(New DataColumn("country_prefix", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_ref_P") = item("PK_goAML_Ref_Phone")
                item("TipeKontak") = getTypeKontakAlamatbyKode(item("Tph_Contact_Type").ToString)
                item("country_prefix") = item("tph_country_prefix")
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Sub bindAddress(store As Ext.Net.Store, listPhone As List(Of NawaDevDAL.goAML_Ref_Address))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
        objtable.Columns.Add(New DataColumn("PK_goAML_Ref_A", GetType(Integer)))
        objtable.Columns.Add(New DataColumn("TypeAlamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Alamat", GetType(String)))
        objtable.Columns.Add(New DataColumn("Kota", GetType(String)))
        objtable.Columns.Add(New DataColumn("Negara", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("PK_goAML_Ref_A") = item("PK_Customer_Address_ID")
                item("TypeAlamat") = getTypeKontakAlamatbyKode(item("Address_Type").ToString)
                item("Alamat") = item("Address")
                item("Kota") = item("City")
                item("Negara") = getCountryByCode(item("Country_Code"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Protected Sub GridCommandIdentification(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordIdentification(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailIdentification(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridCommandPhone(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhone(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordPhone(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhone(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandPhoneEmployer(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ListPhoneEmployer.Remove(ListPhoneEmployer.Where(Function(x) x.PK_goAML_Ref_Phone = ID).FirstOrDefault)
                BindDetail()
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditPhoneEmployer(ID)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailPhoneEmployer(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridCommandAddress(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddress(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordAddress(id)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddress(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandAddressEmployer(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ListAddressEmployer.Remove(ListAddressEmployer.Where(Function(x) x.PK_Customer_Address_ID = ID).FirstOrDefault)
                BindDetail()
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                LoadDataEditAddressEmployer(ID)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                LoadDataDetailAddressEmployer(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub SaveAddIdentification()
        Dim objNewIdentification As New goAML_Person_Identification
        With objNewIdentification
            .PK_Person_Identification_ID = ListIdentification.Select(Function(x) x.PK_Person_Identification_ID).DefaultIfEmpty(0).Max() + 1
            .isReportingPerson = True
            .Type = IDModal_Type.Value
            .Number = IDModal_Number.Text
            .Issue_Date = IDModal_IssueDate.Value
            .Expiry_Date = IDModal_ExpiryDate.Value
            .Issued_By = IDModal_IssuedBy.Text
            .Issued_Country = IDModal_Country.Value
            .Identification_Comment = IDModal_Comments.Text
        End With

        If IDModal_IssueDate.RawValue Is Nothing Then
            objNewIdentification.Issue_Date = IDModal_IssueDate.RawValue
        End If

        If IDModal_ExpiryDate.RawValue Is Nothing Then
            objNewIdentification.Expiry_Date = IDModal_ExpiryDate.RawValue
        End If
        ListIdentification.Add(objNewIdentification)

    End Sub

    Sub SaveAddPhone()
        Dim objNewPhone As New goAML_Ref_Phone

        With objNewPhone
            .PK_goAML_Ref_Phone = ListPhone.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
            .Tph_Contact_Type = PhoneModal_Type.SelectedItem.Value
            .Tph_Communication_Type = PhoneModal_Tool.SelectedItem.Value
            .FK_Ref_Detail_Of = 2
            .tph_country_prefix = PhoneModal_RegionCode.Text
            .tph_number = PhoneModal_Number.Text
            .tph_extension = PhoneModal_NumberEx.Text
            .comments = PhoneModal_Comment.Text
        End With
        ListPhone.Add(objNewPhone)
    End Sub

    Sub SaveAddEmployerPhone()
        Dim objNewEmpPhone As New goAML_Ref_Phone

        With objNewEmpPhone
            .PK_goAML_Ref_Phone = ListPhoneEmployer.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
            .Tph_Contact_Type = EmployerPhone_Kategori.SelectedItem.Value
            .Tph_Communication_Type = EmployerPhone_Jenis.SelectedItem.Value
            .FK_Ref_Detail_Of = 11
            .tph_country_prefix = EmployerPhone_Kode.Text
            .tph_number = EmployerPhone_Nomor.Text
            .tph_extension = EmployerPhone_Extensi.Text
            .comments = EmployerPhone_Note.Text
        End With
        ListPhoneEmployer.Add(objNewEmpPhone)
        BindDetail()

        WindowEmployerPhone.Hide()
    End Sub

    Sub SaveAddAddress()

        Dim objNewAddress As New goAML_Ref_Address
        With objNewAddress
            .PK_Customer_Address_ID = ListAddress.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
            .Address_Type = AddrModal_Type.Value
            .FK_Ref_Detail_Of = 2
            .Address = AddrModal_Address.Text
            .Town = AddrModal_Town.Text
            .City = AddrModal_City.Text
            .Zip = AddrModal_PostalCode.Text
            .Country_Code = AddrModal_Country.Value
            .State = AddrModal_Province.Text
            .Comments = AddrModal_Comment.Text
        End With

        ListAddress.Add(objNewAddress)
    End Sub

    Sub SaveAddAddressEmployer()

        Dim objNewAddressEmployer As New goAML_Ref_Address
        With objNewAddressEmployer
            .PK_Customer_Address_ID = ListAddressEmployer.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
            .Address_Type = EmployerAddress_Tipe.Value
            .FK_Ref_Detail_Of = 11
            .Address = EmployerAddress_Alamat.Text
            .Town = EmployerAddress_Kec.Text
            .City = EmployerAddress_Kab.Text
            .Zip = EmployerAddress_Pos.Text
            .Country_Code = EmployerAddress_Negara.Value
            .State = EmployerAddress_Prov.Text
            .Comments = EmployerAddress_Note.Text
        End With

        ListAddressEmployer.Add(objNewAddressEmployer)
        BindDetail()
        WindowEmployerAddress.Hide()

    End Sub

    Sub SaveVwListIdenfitication()
        Dim objVwListIdenfitication As New Vw_Temp_Identification
        With objVwListIdenfitication
            .PK_Person_Identification_ID = VwListIdentification.Select(Function(x) x.PK_Person_Identification_ID).DefaultIfEmpty(0).Max() + 1
            .type = IDModal_Type.RawText
            .Number = IDModal_Number.Text
            .Issue_Date = IDModal_IssueDate.Value
            .Expiry_Date = IDModal_ExpiryDate.Value
            .Issued_By = IDModal_IssuedBy.Text
            .negara = IDModal_Country.RawText
            .Identification_Comment = IDModal_Comments.Text
        End With

        VwListIdentification.Add(objVwListIdenfitication)

        BindDetail()
        IDModal.Hidden = True
    End Sub

    Sub SaveVwListPhone()
        Dim objVwListPhone As New Vw_Temp_Phone

        With objVwListPhone
            .PK_goAML_Ref_Phone = VwListPhone.Select(Function(x) x.PK_goAML_Ref_Phone).DefaultIfEmpty(0).Max() + 1
            .kategori_kontak = PhoneModal_Type.RawText
            .ja_komunikasi = PhoneModal_Tool.RawText
            .tph_country_prefix = PhoneModal_RegionCode.Text
            .tph_number = PhoneModal_Number.Text
            .tph_extension = PhoneModal_NumberEx.Text
            .comments = PhoneModal_Comment.Text
        End With

        PhoneModal_Comment.Text = ""

        VwListPhone.Add(objVwListPhone)
        BindDetail()

        PhoneModal.Hidden = True
    End Sub

    Sub SaveVwListAddress()
        Dim objVwListAddress As New Vw_Temp_Address

        With objVwListAddress
            .PK_Customer_Address_ID = VwListAddress.Select(Function(x) x.PK_Customer_Address_ID).DefaultIfEmpty(0).Max() + 1
            .kontak = AddrModal_Type.RawText
            .address = AddrModal_Address.Text
            .town = AddrModal_Town.Text
            .city = AddrModal_City.Text
            .zip = AddrModal_PostalCode.Text
            .negara = AddrModal_Country.RawText
            .state = AddrModal_Province.Text
            .comments = AddrModal_Comment.Text
        End With

        VwListAddress.Add(objVwListAddress)
        BindDetail()

        AddrModal.Hidden = True
    End Sub


    Private Sub DeleteRecordIdentification(id As Long)
        Dim objVwDeleteIdentification As Vw_Temp_Identification = VwListIdentification.Find(Function(x) x.PK_Person_Identification_ID = id)
        Dim objDeleteIdentification As goAML_Person_Identification = ListIdentification.Find(Function(x) x.PK_Person_Identification_ID = id)
        If Not objVwDeleteIdentification Is Nothing Then
            VwListIdentification.Remove(objVwDeleteIdentification)
            ListIdentification.Remove(objDeleteIdentification)
            BindDetail()
        End If
    End Sub

    Private Sub DeleteRecordPhone(id As Long)
        Dim objVwDeletePhone As Vw_Temp_Phone = VwListPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        Dim objDeletePhone As goAML_Ref_Phone = ListPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not objVwDeletePhone Is Nothing Then
            VwListPhone.Remove(objVwDeletePhone)
            ListPhone.Remove(objDeletePhone)
            BindDetail()
        End If
    End Sub

    Private Sub DeleteRecordAddress(id As Long)
        Dim objVwDeleteAddress As Vw_Temp_Address = VwListAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        Dim objDeleteAddress As goAML_Ref_Address = ListAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not objVwDeleteAddress Is Nothing Then
            VwListAddress.Remove(objVwDeleteAddress)
            ListAddress.Remove(objDeleteAddress)
            BindDetail()
        End If
    End Sub

    'Edit data yang di object Identification
    Sub SaveEditIdentification()
        'Data Identification
        ObjIdentification.Type = IDModal_Type.SelectedItem.Value
        ObjIdentification.Number = IDModal_Number.Text
        ObjIdentification.Issue_Date = IDModal_IssueDate.Value
        ObjIdentification.Expiry_Date = IDModal_ExpiryDate.Value
        ObjIdentification.Issued_By = IDModal_IssuedBy.Text
        ObjIdentification.Issued_Country = IDModal_Country.Value
        ObjIdentification.Identification_Comment = IDModal_Comments.Text

        If IDModal_IssueDate.RawValue Is Nothing Then
            ObjIdentification.Issue_Date = IDModal_IssueDate.RawValue
        End If

        If IDModal_ExpiryDate.RawValue Is Nothing Then
            ObjIdentification.Expiry_Date = IDModal_ExpiryDate.RawValue
        End If

        'View Identification
        ObjVwIdentification.type = IDModal_Type.RawText
        ObjVwIdentification.Number = IDModal_Number.Text
        ObjVwIdentification.Issue_Date = IDModal_IssueDate.Value
        ObjVwIdentification.Expiry_Date = IDModal_ExpiryDate.Value
        ObjVwIdentification.Issued_By = IDModal_IssuedBy.Text
        ObjVwIdentification.negara = IDModal_Country.RawText
        ObjVwIdentification.Identification_Comment = IDModal_Comments.Text

        ObjIdentification = Nothing
        ObjVwIdentification = Nothing
        BindDetail()

        IDModal.Hidden = True
    End Sub
    'Edit data yang di object Phone
    Sub SaveEditPhone()
        'Data Identification
        ObjPhone.Tph_Contact_Type = PhoneModal_Type.SelectedItem.Value
        ObjPhone.Tph_Communication_Type = PhoneModal_Tool.SelectedItem.Value
        ObjPhone.FK_Ref_Detail_Of = 2
        ObjPhone.tph_country_prefix = PhoneModal_RegionCode.Text
        ObjPhone.tph_number = PhoneModal_Number.Text
        ObjPhone.tph_extension = PhoneModal_NumberEx.Text
        ObjPhone.comments = PhoneModal_Comment.Text

        'View Identification
        ObjVwPhone.kategori_kontak = PhoneModal_Type.RawText
        ObjVwPhone.ja_komunikasi = PhoneModal_Tool.RawText
        ObjVwPhone.tph_country_prefix = PhoneModal_RegionCode.Text
        ObjVwPhone.tph_number = PhoneModal_Number.Text
        ObjVwPhone.tph_extension = PhoneModal_NumberEx.Text
        ObjVwPhone.comments = PhoneModal_Comment.Text

        ObjPhone = Nothing
        ObjVwPhone = Nothing
        BindDetail()

        PhoneModal.Hidden = True
    End Sub

    Sub SaveEditEmployerPhone()
        'Data Phone
        With ObjEmployerPhone
            .Tph_Contact_Type = EmployerPhone_Kategori.SelectedItem.Value
            .Tph_Communication_Type = EmployerPhone_Jenis.SelectedItem.Value
            .FK_Ref_Detail_Of = 11
            .tph_country_prefix = EmployerPhone_Kode.Text
            .tph_number = EmployerPhone_Nomor.Text
            .tph_extension = EmployerPhone_Extensi.Text
            .comments = EmployerPhone_Note.Text
        End With
        BindDetail()
        ObjEmployerPhone = Nothing

        WindowEmployerPhone.Hidden = True
    End Sub
    'Edit data yang di object Address
    Sub SaveEditAddress()
        'Data Address
        ObjAddress.Address_Type = AddrModal_Type.Value
        ObjAddress.FK_Ref_Detail_Of = 2
        ObjAddress.Address = AddrModal_Address.Text
        ObjAddress.Town = AddrModal_Town.Text
        ObjAddress.City = AddrModal_City.Text
        ObjAddress.Zip = AddrModal_PostalCode.Text
        ObjAddress.Country_Code = AddrModal_Country.Value
        ObjAddress.State = AddrModal_Province.Text
        ObjAddress.Comments = AddrModal_Comment.Text

        'View Address
        ObjVwAddress.kontak = AddrModal_Type.RawText
        ObjVwAddress.address = AddrModal_Address.Text
        ObjVwAddress.town = AddrModal_Town.Text
        ObjVwAddress.city = AddrModal_City.Text
        ObjVwAddress.zip = AddrModal_PostalCode.Text
        ObjVwAddress.negara = AddrModal_Country.RawText
        ObjVwAddress.state = AddrModal_Province.Text
        ObjVwAddress.comments = AddrModal_Comment.Text

        ObjAddress = Nothing
        ObjVwAddress = Nothing
        BindDetail()

        AddrModal.Hidden = True
    End Sub

    Sub SaveEditAddressEmployer()
        'Data Address
        With ObjEmployerAddress
            .Address_Type = EmployerAddress_Tipe.Value
            .FK_Ref_Detail_Of = 11
            .Address = EmployerAddress_Alamat.Text
            .Town = EmployerAddress_Kec.Text
            .City = EmployerAddress_Kab.Text
            .Zip = EmployerAddress_Pos.Text
            .Country_Code = EmployerAddress_Negara.Value
            .State = EmployerAddress_Prov.Text
            .Comments = EmployerAddress_Note.Text
        End With
        BindDetail()
        ObjEmployerAddress = Nothing

        WindowEmployerAddress.Hidden = True
    End Sub

    Private Sub LoadDataEditIdentification(id As Long)
        ObjIdentification = ListIdentification.Find(Function(x) x.PK_Person_Identification_ID = id)
        ObjVwIdentification = VwListIdentification.Find(Function(x) x.PK_Person_Identification_ID = id)
        If Not ObjIdentification Is Nothing Then
            IDModal.Hidden = False
            'ClearInput()
            With ObjIdentification
                IDModal_Type.SetValueAndFireSelect(.Type)
                IDModal_Number.Text = .Number.Trim
                IDModal_IssueDate.Value = .Issue_Date
                IDModal_ExpiryDate.Value = .Expiry_Date
                IDModal_IssuedBy.Text = .Issued_By
                IDModal_Country.SetValueAndFireSelect(.Issued_Country)
                IDModal_Comments.Text = .Identification_Comment
            End With
        End If
    End Sub


    Private Sub LoadDataEditPhone(id As Long)
        ObjPhone = ListPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        ObjVwPhone = VwListPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjPhone Is Nothing Then
            PhoneModal.Hidden = False
            'ClearInput()
            With ObjPhone
                PhoneModal_Type.SetValueAndFireSelect(.Tph_Contact_Type)
                PhoneModal_Tool.SetValueAndFireSelect(.Tph_Communication_Type)
                PhoneModal_RegionCode.Text = .tph_country_prefix.Trim
                PhoneModal_Number.Text = .tph_number
                PhoneModal_NumberEx.Text = .tph_extension
                PhoneModal_Comment.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataEditPhoneEmployer(id As Long)
        ObjEmployerPhone = ListPhoneEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjEmployerPhone Is Nothing Then
            WindowEmployerPhone.Hidden = False
            'ClearInput()
            With ObjEmployerPhone
                EmployerPhone_Kategori.SetValueAndFireSelect(.Tph_Contact_Type)
                EmployerPhone_Jenis.SetValueAndFireSelect(.Tph_Communication_Type)
                EmployerPhone_Kode.Text = .tph_country_prefix.Trim
                EmployerPhone_Nomor.Text = .tph_number
                EmployerPhone_Extensi.Text = .tph_extension
                EmployerPhone_Note.Text = .comments
            End With
        End If
    End Sub

    Private Sub LoadDataEditAddressEmployer(id As Long)
        ObjEmployerAddress = ListAddressEmployer.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjEmployerAddress Is Nothing Then
            WindowEmployerAddress.Hidden = False
            'ClearInput()
            With ObjEmployerAddress
                EmployerAddress_Tipe.SetValueAndFireSelect(.Address_Type)
                EmployerAddress_Alamat.Text = .Address.Trim
                EmployerAddress_Kec.Text = .Town
                EmployerAddress_Kab.Text = .City
                EmployerAddress_Pos.Text = .Zip
                EmployerAddress_Negara.SetValueAndFireSelect(.Country_Code)
                EmployerAddress_Prov.Text = .State
                EmployerAddress_Note.Text = .Comments
            End With
        End If
    End Sub

    Private Sub LoadDataEditAddress(id As Long)
        ObjAddress = ListAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        ObjVwAddress = VwListAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjAddress Is Nothing Then
            AddrModal.Hidden = False
            'ClearInput()
            With ObjAddress
                AddrModal_Type.SetValueAndFireSelect(.Address_Type)
                AddrModal_Address.Text = .Address.Trim
                AddrModal_Town.Text = .Town
                AddrModal_City.Text = .City
                AddrModal_PostalCode.Text = .Zip
                AddrModal_Country.SetValueAndFireSelect(.Country_Code)
                AddrModal_Province.Text = .State
                AddrModal_Comment.Text = .Comments
            End With
        End If
    End Sub


    Private Sub LoadDataDetailIdentification(id As Long)
        ObjIdentification = ListIdentification.Find(Function(x) x.PK_Person_Identification_ID = id)
        ObjVwIdentification = VwListIdentification.Find(Function(x) x.PK_Person_Identification_ID = id)
        If Not ObjIdentification Is Nothing Then
            IDModalDetail.Hidden = False
            'ClearInput()
            With ObjVwIdentification
                RP_jenis_identitas_detail.Text = .type
                RP_no_identitas_detail.Text = .Number.Trim
                If .Issue_Date = #1/1/0001 12:00:00 AM# Then
                    RP_tgl_terbit_detail.Text = Nothing
                Else
                    RP_tgl_terbit_detail.Text = .Issue_Date
                End If
                If .Expiry_Date = #1/1/0001 12:00:00 AM# Then
                    RP_tgl_kadaluarsa_detail.Text = Nothing
                Else
                    RP_tgl_kadaluarsa_detail.Text = .Expiry_Date
                End If
                RP_penerbit_detail.Text = .Issued_By
                RP_negara_penerbit_detail.Text = .negara
                RP_note_identitas_detail.Text = .Identification_Comment
            End With
        End If
    End Sub

    Private Sub LoadDataDetailAddress(id As Long)
        ObjAddress = ListAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        ObjVwAddress = VwListAddress.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjAddress Is Nothing Then
            AddrModalDetail.Hidden = False
            'ClearInput()
            With ObjVwAddress
                RP_tipe_alamat_detail.Text = .kontak
                RP_alamat_detail.Text = .address.Trim
                RP_kecamatan_detail.Text = .town
                RP_kabupaten_detail.Text = .city
                RP_kodepos_detail.Text = .zip
                RP_negara_detail.Text = .negara
                RP_propinsi_detail.Text = .state
                RP_note_alamat_detail.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataDetailPhone(id As Long)
        ObjPhone = ListPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        ObjVwPhone = VwListPhone.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjPhone Is Nothing Then
            PhoneModalDetail.Hidden = False
            'ClearInput()
            With ObjVwPhone
                RP_tipe_kontak_detail.Text = .kategori_kontak
                RP_jenis_komunikasi_detail.Text = .ja_komunikasi
                RP_kode_area_detail.Text = .tph_country_prefix.Trim
                RP_no_telepon_detail.Text = .tph_number
                RP_phone_ext_detail.Text = .tph_extension
                RP_note_phone_detail.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataDetailPhoneEmployer(id As Long)
        ObjEmployerPhone = ListPhoneEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = id)
        If Not ObjEmployerPhone Is Nothing Then
            WindowEmployerPhoneDetail.Hidden = False
            'ClearInput()
            With ObjEmployerPhone
                EmployerPhoneDetail_Kategori.Text = getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                EmployerPhoneDetail_jenis.Text = getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                EmployerPhoneDetail_kode.Text = .tph_country_prefix.Trim
                EmployerPhoneDetail_nomor.Text = .tph_number
                EmployerPhoneDetail_extensi.Text = .tph_extension
                EmployerPhoneDetail_note.Text = .comments
            End With
        End If
    End Sub
    Private Sub LoadDataDetailAddressEmployer(id As Long)
        ObjEmployerAddress = ListAddressEmployer.Find(Function(x) x.PK_Customer_Address_ID = id)
        If Not ObjEmployerAddress Is Nothing Then
            WindowEmployerAddressDetail.Hidden = False
            'ClearInput()
            With ObjEmployerAddress
                EmployerAddressDetail_tipe.Text = getTypeKontakAlamatbyKode(.Address_Type)
                EmployerAddressDetail_alamat.Text = .Address
                EmployerAddressDetail_kec.Text = .Town
                EmployerAddressDetail_kab.Text = .City
                EmployerAddressDetail_pos.Text = .Zip
                EmployerAddressDetail_negara.Text = getCountryByCode(.Country_Code)
                EmployerAddressDetail_prov.Text = .State
                EmployerAddressDetail_note.Text = .Comments
            End With
        End If
    End Sub
    Sub AddToReportClass()
        With ReportingPersonClass
            .objPerson = objReportPersonAddData
            .ObjPhone = ListPhone
            .ObjEmpPhone = ListPhoneEmployer
            .ObjAddress = ListAddress
            .ObjEmpAddress = ListAddressEmployer
            .ObjIdent = ListIdentification
        End With
    End Sub

    Protected Sub IDModal_BtnAdd(sender As Object, e As DirectEventArgs)
        IDModal.Hide()
        Try
            'Dim objReportPersonAdd As New goAML_ODM_Reporting_Person

            With objReportPersonAddData
                .UserID = cmb_UserID.Value
                .Kode_Laporan = RP_KodeLaporan.Value
                .Title = RP_Title.Text
                .Last_Name = RP_FulltName.Text
                .BirthDate = RP_BirthDate.Value
                .Birth_Place = RP_BirthPlace.Text
                .Gender = RP_Gender.Value
                .Mothers_Name = RP_MotherName.Text
                .Alias = RP_Alias.Text
                .SSN = RP_SSN.Text
                .Passport_Number = RP_Passport.Text
                .Passport_Country = RP_CountryPassport.Text
                .Id_Number = RP_OtherID.Text
                .Nationality1 = RP_Nationality1.Text
                .Nationality2 = RP_Nationality2.Text
                .Nationality3 = RP_Nationality3.Text
                .Residence = RP_Residence.Text
                .Email1 = RP_Email1.Text
                .Email2 = RP_Email2.Text
                .Email3 = RP_Email3.Text
                .Email4 = RP_Email4.Text
                .Email5 = RP_Email5.Text
                .Occupation = RP_Job.Text
                .Employer_Name = RP_JobPlace.Text
                .Deceased = RP_IsDeceased.Value
                .Deceased_Date = RP_DeceasedDate.Value
                .Tax_Number = RP_TaxNumber.Text
                .Tax_Reg_Number = RP_TaxRegNumber.Value
                .Source_Of_Wealth = RP_SourceOfWealth.Text
                .Comment = RP_Comments.Text
                .Active = True
                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                .CreatedDate = Date.Now
                .LastUpdateDate = Date.Now
            End With

            If RP_DeceasedDate.RawValue Is Nothing Then
                objReportPersonAddData.Deceased_Date = RP_DeceasedDate.RawValue
            End If

            AddToReportClass()


            If ListAddress.Count > 0 Then
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                    goAML_PersonBLL.SaveAddTanpaApproval(ReportingPersonClass, Me.ObjModule)
                    LblConfirmation.Text = "Data Saved into Database"
                Else
                    goAML_PersonBLL.SaveAddApproval(ReportingPersonClass, Me.ObjModule)
                    LblConfirmation.Text = "Data Saved into Pending Approval"
                End If

                RP_EntireForm.Hidden = True
                Panelconfirmation.Hidden = False
            Else
                Ext.Net.X.Msg.Alert("Message", "Minimal Alamat Person Harus Ada 1(Satu)").Show()
            End If

        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnCancelRP()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Moduleid = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

End Class
