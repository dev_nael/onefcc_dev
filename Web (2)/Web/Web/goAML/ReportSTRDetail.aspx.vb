﻿Imports System.Data
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevDAL
Imports NawaBLL
Imports Ext
Imports NawaDevBLL
Imports System.Data.SqlClient
Imports NawaDAL
Imports Elmah
Imports System.IO
Imports OfficeOpenXml

Partial Class ReportSTRDetail
    Inherits Parent
    Public UploadReportSTR As New NawaDevBLL.UploadReportSTR
    Private this As Object

#Region "Session"
    Private Sub ClearSession()
        ListReportValid = New goAML_ReportSTR
        ListIndikatorLaporanValid = New List(Of goAML_Indikator_LaporanSTR)
        ListTransactionBipartyValid = New List(Of goAML_Transaction_BiPartySTR)
        ListTransactionMultipartyValid = New List(Of goAML_Transaction_MultiPartySTR)
        ListActivityValid = New List(Of goAML_ActivitySTR)


        ListIndikatorLaporanNotValid = New List(Of goAML_Indikator_LaporanSTR)
        ListTransactionBipartyNotValid = New List(Of goAML_Transaction_BiPartySTR)
        ListTransactionMultipartyNotValid = New List(Of goAML_Transaction_MultiPartySTR)
        ListActivityNotValid = New List(Of goAML_ActivitySTR)

    End Sub

    Private Sub Bindgrid()


        If ListIndikatorLaporanValid.ToList().Count > 0 Then
            GridPanelIndikatorLaporan.Hidden = False
            PanelIndikatorLaporan.Hidden = False
            PanelValid.Hidden = False
            StoreIndikatorLaporanData.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from vw_indikator_laporan where fk_report_id = " & ListReportValid.PK_ID, Nothing)
            StoreIndikatorLaporanData.DataBind()
        End If

        If ListTransactionBipartyValid.ToList().Count > 0 Then
            GridPanelTransactionBiParty.Hidden = False
            PanelTransactionBiParty.Hidden = False
            PanelValid.Hidden = False
            StoreTransactionBiParty.DataSource = ListTransactionBipartyValid.ToList()
            StoreTransactionBiParty.DataBind()
        End If

        If ListTransactionMultipartyValid.ToList().Count > 0 Then
            GridPanelTransactionMultiParty.Hidden = False
            PanelTransactionMultiParty.Hidden = False
            PanelValid.Hidden = False
            StoreTransactionMultiParty.DataSource = ListTransactionMultipartyValid.ToList()
            StoreTransactionMultiParty.DataBind()
        End If

        If ListActivityValid.Where(Function(x) x.SubNodeType = 1).ToList().Count > 0 Then
            GridPanelActivityAccount.Hidden = False
            PanelActivityAccount.Hidden = False
            PanelValid.Hidden = False
            StoreActivityAccount.DataSource = ListActivityValid.Where(Function(x) x.SubNodeType = 1).ToList()
            StoreActivityAccount.DataBind()

        End If


        If ListActivityValid.Where(Function(x) x.SubNodeType = 2).ToList().Count > 0 Then
            GridPanelActivityPerson.Hidden = False
            PanelActivityPerson.Hidden = False
            PanelValid.Hidden = False
            StoreActivityPerson.DataSource = ListActivityValid.Where(Function(x) x.SubNodeType = 2).ToList()
            StoreActivityPerson.DataBind()

        End If

        If ListActivityValid.Where(Function(x) x.SubNodeType = 3).ToList().Count > 0 Then
            GridPanelActivityEntity.Hidden = False
            PanelActivityEntity.Hidden = False
            PanelValid.Hidden = False
            StoreActivityEntity.DataSource = ListActivityValid.Where(Function(x) x.SubNodeType = 3).ToList()
            StoreActivityEntity.DataBind()

        End If

        If ListIndikatorLaporanNotValid.ToList().Count > 0 Then
            GridPanelIndikatorLaporanInvalid.Hidden = False
            PanelIndikatorLaporanInvalid.Hidden = False
            PanelInValid.Hidden = False
            StoreIndikatorLaporanInvalid.DataSource = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from vw_indikator_laporan where fk_report_id = " & ListReportValid.PK_ID, Nothing)
            StoreIndikatorLaporanInvalid.DataBind()
        End If
        If ListTransactionBipartyNotValid.ToList().Count > 0 Then
            GridPanelTransactionBipartyInvalid.Hidden = False
            PanelTransactionBipartyInvalid.Hidden = False
            PanelInValid.Hidden = False
            StoreTransactionBipartyInvalid.DataSource = ListTransactionBipartyNotValid.ToList()
            StoreTransactionBipartyInvalid.DataBind()
        End If
        If ListTransactionMultipartyNotValid.ToList().Count > 0 Then
            GridPanelTransactionMultiPartyInvalid.Hidden = False
            PanelTransactionMultiPartyInvalid.Hidden = False
            PanelInValid.Hidden = False
            StoreTransactionMultiPartyInvalid.DataSource = ListTransactionMultipartyNotValid.ToList()
            StoreTransactionMultiPartyInvalid.DataBind()
        End If
        If ListActivityNotValid.Where(Function(x) x.SubNodeType = 1).ToList().Count > 0 Then
            GridPanelActivityAccountInvalid.Hidden = False
            PanelActivityAccountInvalid.Hidden = False
            PanelInValid.Hidden = False
            StoreActivityAccountInvalid.DataSource = ListActivityNotValid.Where(Function(x) x.SubNodeType = 1).ToList()
            StoreActivityAccountInvalid.DataBind()
        End If
        If ListActivityNotValid.Where(Function(x) x.SubNodeType = 2).ToList().Count > 0 Then
            GridPanelActivityPersonInvalid.Hidden = False
            PanelActivityPersonInvalid.Hidden = False
            PanelInValid.Hidden = False
            StoreActivityPersonInvalid.DataSource = ListActivityNotValid.Where(Function(x) x.SubNodeType = 2).ToList()
            StoreActivityPersonInvalid.DataBind()
        End If
        If ListActivityNotValid.Where(Function(x) x.SubNodeType = 3).ToList().Count > 0 Then
            GridPanelActivityEntityInvalid.Hidden = False
            PanelActivityEntityInvalid.Hidden = False
            PanelInValid.Hidden = False
            StoreActivityEntityInvalid.DataSource = ListActivityNotValid.Where(Function(x) x.SubNodeType = 3).ToList()
            StoreActivityEntityInvalid.DataBind()
        End If

    End Sub


    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("ReportSTRDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("ReportSTRDetail.ObjModule") = value
        End Set
    End Property

    Public Property ListReportValid As NawaDevDAL.goAML_ReportSTR
        Get
            Return Session("ReportSTRDetail.ListReportValid")
        End Get
        Set(ByVal value As NawaDevDAL.goAML_ReportSTR)
            Session("ReportSTRDetail.ListReportValid") = value
        End Set
    End Property


    Public Property ListIndikatorLaporanValid As List(Of NawaDevDAL.goAML_Indikator_LaporanSTR)
        Get
            If Session("ReportSTRDetail.ListIndikatorLaporanValid") Is Nothing Then
                Session("ReportSTRDetail.ListIndikatorLaporanValid") = New List(Of NawaDevDAL.goAML_Indikator_LaporanSTR)
            End If
            Return Session("ReportSTRDetail.ListIndikatorLaporanValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Indikator_LaporanSTR))
            Session("ReportSTRDetail.ListIndikatorLaporanValid") = value
        End Set
    End Property

    Public Property ListIndikatorLaporanNotValid As List(Of NawaDevDAL.goAML_Indikator_LaporanSTR)
        Get
            If Session("ReportSTRDetail.ListIndikatorLaporanNotValid") Is Nothing Then
                Session("ReportSTRDetail.ListIndikatorLaporanNotValid") = New List(Of NawaDevDAL.goAML_Indikator_LaporanSTR)
            End If
            Return Session("ReportSTRDetail.ListIndikatorLaporanNotValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Indikator_LaporanSTR))
            Session("ReportSTRDetail.ListIndikatorLaporanNotValid") = value
        End Set
    End Property

    Public Property ListTransactionBipartyValid As List(Of NawaDevDAL.goAML_Transaction_BiPartySTR)
        Get
            If Session("ReportSTRDetail.ListTransactionBipartyValid") Is Nothing Then
                Session("ReportSTRDetail.ListTransactionBipartyValid") = New List(Of NawaDevDAL.goAML_Transaction_BiPartySTR)
            End If
            Return Session("ReportSTRDetail.ListTransactionBipartyValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Transaction_BiPartySTR))
            Session("ReportSTRDetail.ListTransactionBipartyValid") = value
        End Set
    End Property

    Public Property ListTransactionBipartyNotValid As List(Of NawaDevDAL.goAML_Transaction_BiPartySTR)
        Get
            If Session("ReportSTRDetail.ListTransactionBipartyNotValid") Is Nothing Then
                Session("ReportSTRDetail.ListTransactionBipartyNotValid") = New List(Of NawaDevDAL.goAML_Transaction_BiPartySTR)
            End If
            Return Session("ReportSTRDetail.ListTransactionBipartyNotValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Transaction_BiPartySTR))
            Session("ReportSTRDetail.ListTransactionBipartyNotValid") = value
        End Set
    End Property

    Public Property ListTransactionMultipartyValid As List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR)
        Get
            If Session("ReportSTRDetail.ListTransactionMultipartyValid") Is Nothing Then
                Session("ReportSTRDetail.ListTransactionMultipartyValid") = New List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR)
            End If
            Return Session("ReportSTRDetail.ListTransactionMultipartyValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR))
            Session("ReportSTRDetail.ListTransactionMultipartyValid") = value
        End Set
    End Property

    Public Property ListTransactionMultipartyNotValid As List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR)
        Get
            If Session("ReportSTRDetail.ListTransactionMultipartyNotValid") Is Nothing Then
                Session("ReportSTRDetail.ListTransactionMultipartyNotValid") = New List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR)
            End If
            Return Session("ReportSTRDetail.ListTransactionMultipartyNotValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR))
            Session("ReportSTRDetail.ListTransactionMultipartyNotValid") = value
        End Set
    End Property

    Public Property ListActivityValid As List(Of NawaDevDAL.goAML_ActivitySTR)
        Get
            If Session("ReportSTRDetail.ListActivityValid") Is Nothing Then
                Session("ReportSTRDetail.ListActivityValid") = New List(Of NawaDevDAL.goAML_ActivitySTR)
            End If
            Return Session("ReportSTRDetail.ListActivityValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ActivitySTR))
            Session("ReportSTRDetail.ListActivityValid") = value
        End Set
    End Property

    Public Property ListActivityNotValid As List(Of NawaDevDAL.goAML_ActivitySTR)
        Get
            If Session("ReportSTRDetail.ListActivityNotValid") Is Nothing Then
                Session("ReportSTRDetail.ListActivityNotValid") = New List(Of NawaDevDAL.goAML_ActivitySTR)
            End If
            Return Session("ReportSTRDetail.ListActivityNotValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ActivitySTR))
            Session("ReportSTRDetail.ListActivityNotValid") = value
        End Set
    End Property


#End Region

#Region "Page Load"
    Protected Sub Page_(sender As Object, e As EventArgs) Handles Me.Load
        Try
            GridPanelSetting()
            Paging()
            If Not Ext.Net.X.IsAjaxRequest Then
                Dim strmodule As String = Request.Params("ModuleID")
                ClearSession()
                Dim intmodule As Integer = NawaBLL.Common.DecryptQueryString(strmodule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intmodule)

                If Not ObjModule Is Nothing Then
                    If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Insert) Then
                        Dim strIDCode As String = 1
                        strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                        Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                    FormPanelInput.Title = "Report STR  - Detail"
                    PanelValid.Hidden = True
                    PanelInValid.Hidden = True
                    GridPanelReportInvalid.Hidden = True
                    GridPanelIndikatorLaporanInvalid.Hidden = True
                    GridPanelTransactionBipartyInvalid.Hidden = True
                    GridPanelTransactionMultiPartyInvalid.Hidden = True
                    GridPanelActivityPersonInvalid.Hidden = True
                    GridPanelActivityAccountInvalid.Hidden = True
                    GridPanelActivityEntityInvalid.Hidden = True

                    PanelReportInvalid.Hidden = True
                    PanelIndikatorLaporanInvalid.Hidden = True
                    PanelTransactionBipartyInvalid.Hidden = True
                    PanelTransactionMultiPartyInvalid.Hidden = True
                    PanelActivityPersonInvalid.Hidden = True
                    PanelActivityAccountInvalid.Hidden = True
                    PanelActivityEntityInvalid.Hidden = True

                    GridPanelReport.Hidden = True
                    GridPanelIndikatorLaporan.Hidden = True
                    GridPanelTransactionBiParty.Hidden = True
                    GridPanelTransactionMultiParty.Hidden = True
                    GridPanelActivityPerson.Hidden = True
                    GridPanelActivityAccount.Hidden = True
                    GridPanelActivityEntity.Hidden = True

                    PanelReport.Hidden = True
                    PanelIndikatorLaporan.Hidden = True
                    PanelTransactionBiParty.Hidden = True
                    PanelTransactionMultiParty.Hidden = True
                    PanelActivityPerson.Hidden = True
                    PanelActivityAccount.Hidden = True
                    PanelActivityEntity.Hidden = True


                    ListReportValid = UploadReportSTR.getReportSTR(GetId())

                    If ListReportValid IsNot Nothing Then
                        Case_ID.Text = ListReportValid.Case_ID
                        Tanggal_Laporan.Text = ListReportValid.Tanggal_Laporan
                        Jenis_laporan.Text = ListReportValid.Jenis_laporan
                        Alasan.Text = ListReportValid.Alasan
                        Ref_Num.Text = ListReportValid.Ref_Num
                        Tindakan_Pelapor.Text = ListReportValid.Tindakan_Pelapor
                        isValid.Text = ListReportValid.isValid


                    End If

                    ListIndikatorLaporanValid = UploadReportSTR.GetListIndikatorLaporan(ListReportValid.PK_ID).Where(Function(x) x.MessageValidation = "").ToList
                    ListTransactionBipartyValid = UploadReportSTR.GetListTransactionBiparty(ListReportValid.PK_ID).Where(Function(x) x.MessageValidation = "").ToList
                    ListTransactionMultipartyValid = UploadReportSTR.GetListTransactionMultiparty(ListReportValid.PK_ID).Where(Function(x) x.MessageValidation = "").ToList
                    ListActivityValid = UploadReportSTR.GetListActivity(ListReportValid.PK_ID).Where(Function(x) x.MessageValidation = "").ToList

                    ListIndikatorLaporanNotValid = UploadReportSTR.GetListIndikatorLaporan(ListReportValid.PK_ID).Where(Function(x) x.MessageValidation <> "").ToList
                    ListTransactionBipartyNotValid = UploadReportSTR.GetListTransactionBiparty(ListReportValid.PK_ID).Where(Function(x) x.MessageValidation <> "").ToList
                    ListTransactionMultipartyNotValid = UploadReportSTR.GetListTransactionMultiparty(ListReportValid.PK_ID).Where(Function(x) x.MessageValidation <> "").ToList
                    ListActivityNotValid = UploadReportSTR.GetListActivity(ListReportValid.PK_ID).Where(Function(x) x.MessageValidation <> "").ToList





                    Bindgrid()




                Else
                    Throw New Exception("Invalid Module ID")
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
    '    ObjEODTask = New NawaDevBLL.EODTaskBLL(FormPanelInput)
    'End Sub
#End Region

#Region "Method"

    Private Function GetId() As String
        Dim strid As String = Request.Params("ID")
        Dim id As String = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
        Return id
    End Function

    Protected Sub BtnCancel_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub GridCmdTransactionMultiparty(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadPanelTransactionMultipartyDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridCmdTransactionBiparty(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadPanelTransactionBipartyDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadPanelTransactionBipartyDetail(id As Long)
        Try
            windowDetailTransactionBiparty.Hidden = False
            Dim item = UploadReportSTR.getDetailTransactionBipartySTR(id)
            If Not item.Case_ID Is Nothing Then
                textCaseID.Text = item.Case_ID
            End If
            If Not item.TransactionNumber Is Nothing Then
                txtTransactionNumber.Text = item.TransactionNumber
            End If
            If Not item.Internal_Ref_Number Is Nothing Then
                txtInternal_Ref_Number.Text = item.Internal_Ref_Number
            End If
            If Not item.Transaction_Location Is Nothing Then
                txtTransaction_Location.Text = item.Transaction_Location
            End If
            If Not item.Transaction_Remark Is Nothing Then
                txtTransaction_Remark.Text = item.Transaction_Remark
            End If
            If Not item.Date_Transaction Is Nothing Then
                txtDate_Transaction.Text = item.Date_Transaction
            End If
            If Not item.Teller Is Nothing Then
                txtTeller.Text = item.Teller
            End If
            If Not item.Authorized Is Nothing Then
                txtAuthorized.Text = item.Authorized
            End If
            If Not item.Late_Deposit Is Nothing Then
                txtLate_Deposit.Text = item.Late_Deposit
            End If
            If Not item.Date_Posting Is Nothing Then
                txtDate_Posting.Text = item.Date_Posting
            End If
            If Not item.Transmode_Code Is Nothing Then
                If Not item.Transmode_Code = "" Then
                    Dim variable = UploadReportSTR.getTransmodecode(item.Transmode_Code)
                    If variable IsNot Nothing Then
                        txtTransmode_Code.Text = UploadReportSTR.getTransmodecode(item.Transmode_Code).Keterangan
                    End If
                End If
            End If
            'txtValue_Date.Text = item.Value_Date
            If Not item.Transmode_Comment Is Nothing Then
                txtTransmode_Comment.Text = item.Transmode_Comment
            End If
            If Not item.Amount_Local Is Nothing Then
                txtAmount_Local.Text = item.Amount_Local
            End If
            If Not item.isMyClient_FROM Is Nothing Then
                txtisMyClient_FROM.Text = item.isMyClient_FROM
            End If
            If Not item.From_Funds_Code Is Nothing Then
                If Not item.From_Funds_Code = "" Then
                    Dim variable = UploadReportSTR.getFundsCode(item.From_Funds_Code)
                    If Not variable Is Nothing Then
                        txtFrom_Funds_Code.Text = UploadReportSTR.getFundsCode(item.From_Funds_Code).Keterangan
                    End If
                End If
            End If
            If Not item.From_Funds_Comment Is Nothing Then
                txtFrom_Funds_Comment.Text = item.From_Funds_Comment
            End If
            If Not item.From_Foreign_Currency_Code Is Nothing Then
                If Not item.From_Foreign_Currency_Code = "" Then
                    Dim variable = UploadReportSTR.getMataUang(item.From_Foreign_Currency_Code)
                    If variable IsNot Nothing Then
                        txtFrom_Foreign_Currency_Code.Text = UploadReportSTR.getMataUang(item.From_Foreign_Currency_Code).Keterangan
                    End If
                End If
            End If
            If Not item.From_Foreign_Currency_Amount Is Nothing Then
                txtFrom_Foreign_Currency_Amount.Text = item.From_Foreign_Currency_Amount
            End If

            If Not item.From_Foreign_Currency_Exchange_Rate Is Nothing Then
                txtFrom_Foreign_Currency_Exchange_Rate.Text = item.From_Foreign_Currency_Exchange_Rate
            End If
            If Not item.From_Country Is Nothing Then
                If Not item.From_Country = "" Then
                    Dim variable = UploadReportSTR.getNamaNegara(item.From_Country)
                    If variable IsNot Nothing Then

                        txtFrom_Country.Text = UploadReportSTR.getNamaNegara(item.From_Country).Keterangan
                    End If
                End If
            End If
            If Not item.isMyClient_TO Is Nothing Then
                txtisMyClient_TO.Text = item.isMyClient_TO
            End If
            If Not item.To_Funds_Code Is Nothing Then
                If Not item.To_Funds_Code = "" Then
                    Dim variable = UploadReportSTR.getFundsCode(item.To_Funds_Code)
                    If variable IsNot Nothing Then
                        txtTo_Funds_Code.Text = UploadReportSTR.getFundsCode(item.To_Funds_Code).Keterangan
                    End If
                End If
            End If
            If Not item.From_Funds_Comment Is Nothing Then
                txtTo_Funds_Comment.Text = item.From_Funds_Comment
            End If
            If Not item.To_Foreign_Currency_Code Is Nothing Then
                If Not item.To_Foreign_Currency_Code = "" Then
                    Dim variable = UploadReportSTR.getMataUang(item.To_Foreign_Currency_Code)
                    If variable IsNot Nothing Then
                        txtTo_Foreign_Currency_Code.Text = UploadReportSTR.getMataUang(item.To_Foreign_Currency_Code).Keterangan
                    End If
                End If
            End If
            If Not item.To_Foreign_Currency_Amount Is Nothing Then
                txtTo_Foreign_Currency_Amount.Text = item.To_Foreign_Currency_Amount
            End If
            If Not item.To_Foreign_Currency_Exchange_Rate Is Nothing Then
                txtTo_Foreign_Currency_Exchange_Rate.Text = item.To_Foreign_Currency_Exchange_Rate
            End If
            If Not item.To_Country Is Nothing Then
                If Not item.To_Country = "" Then
                    Dim variable = UploadReportSTR.getNamaNegara(item.To_Country)
                    If variable IsNot Nothing Then
                        txtTo_Country.Text = UploadReportSTR.getNamaNegara(item.To_Country).Keterangan
                    End If
                End If
            End If
            If Not item.Comments Is Nothing Then
                txtComments.Text = item.Comments
            End If
            If Not item.isSwift Is Nothing Then
                txtisSwift.Text = item.isSwift
            End If

            'HSBC 20201117 Fachmi -- penambahan field swift code lawan
            If Not item.Swift_Code_Lawan Is Nothing Then
                txtSwiftCodeLawan.Text = item.Swift_Code_Lawan
            End If

            'Update "" ke 0 20201109
            If Not item.FK_Source_Data_ID Is Nothing Then
                If Not item.FK_Source_Data_ID = 0 Then
                    Dim variable = UploadReportSTR.getSourceData(item.FK_Source_Data_ID)
                    txtFK_Source_Data_ID.Text = UploadReportSTR.getSourceData(item.FK_Source_Data_ID).Description
                End If
            End If

            If Not item.FROM_Account_No Is Nothing Then
                txtFROM_Account_No.Text = item.FROM_Account_No
            End If
            If Not item.FROM_CIFNO Is Nothing Then
                txtFROM_CIFNO.Text = item.FROM_CIFNO
            End If
            If Not item.From_WIC_NO Is Nothing Then
                txtFrom_WIC_NO.Text = item.From_WIC_NO
            End If
            If Not item.TO_ACCOUNT_NO Is Nothing Then
                txtTO_ACCOUNT_NO.Text = item.TO_ACCOUNT_NO
            End If
            If Not item.TO_CIF_NO Is Nothing Then
                txtTO_CIF_NO.Text = item.TO_CIF_NO
            End If
            If Not item.TO_WIC_NO Is Nothing Then
                txtTO_WIC_NO.Text = item.TO_WIC_NO
            End If
            If Not item.Condutor_ID Is Nothing Then
                txtCondutor_ID.Text = item.Condutor_ID

                txtIsUsedConductor.Text = "True"
            Else
                txtIsUsedConductor.Text = "False"
            End If


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadPanelTransactionMultipartyDetail(id As Long)
        Try

            windowDetailTransactionMultiparty.Hidden = False
            Dim item = UploadReportSTR.getDetailTransactionMultipartySTR(id)
            If Not item.Case_ID Is Nothing Then
                MultitxtCaseID.Text = item.Case_ID
            End If
            If Not item.TransactionNumber Is Nothing Then
                txtMultiTransactionNumber.Text = item.TransactionNumber
            End If
            If Not item.Internal_Ref_Number Is Nothing Then
                txtMultiInternal_Ref_Number.Text = item.Internal_Ref_Number
            End If
            If Not item.Transaction_Location Is Nothing Then
                txtMultiTransaction_Location.Text = item.Transaction_Location
            End If
            If Not item.Transaction_Remark Is Nothing Then
                txtMultiTransaction_Remark.Text = item.Transaction_Remark
            End If
            If Not item.Date_Transaction Is Nothing Then
                txtMultiDate_Transaction.Text = item.Date_Transaction
            End If
            If Not item.Teller Is Nothing Then
                txtMultiTeller.Text = item.Teller
            End If
            If Not item.Authorized Is Nothing Then
                txtMultiAuthorized.Text = item.Authorized
            End If
            If Not item.Late_Deposit Is Nothing Then
                txtMultiLate_Deposit.Text = item.Late_Deposit
            End If
            If Not item.Date_Posting Is Nothing Then
                txtMultiDate_Posting.Text = item.Date_Posting
            End If
            If Not item.Transmode_Code Is Nothing Then
                If Not item.Transmode_Code = "" Then
                    Dim variable = UploadReportSTR.getTransmodecode(item.Transmode_Code)
                    If variable IsNot Nothing Then
                        txtMultiTransmode_Code.Text = UploadReportSTR.getTransmodecode(item.Transmode_Code).Keterangan
                    End If
                End If
            End If
            If Not item.Transmode_Comment Is Nothing Then
                txtMultiTransmode_Comment.Text = item.Transmode_Comment
            End If
            If Not item.Amount_Local Is Nothing Then
                txtMultiAmount_Local.Text = item.Amount_Local
            End If
            If Not item.isMyClient Is Nothing Then
                txtMultiisMyClient.Text = item.isMyClient
            End If
            If Not item.Funds_Code Is Nothing Then
                If Not item.Funds_Code = "" Then
                    Dim variable = UploadReportSTR.getFundsCode(item.Funds_Code)
                    If variable IsNot Nothing Then
                        txtMultiFunds_Code.Text = UploadReportSTR.getFundsCode(item.Funds_Code).Keterangan
                    End If
                End If
            End If
            If Not item.Funds_Comment Is Nothing Then
                txtMultiFunds_Comment.Text = item.Funds_Comment
            End If
            If Not item.Foreign_Currency_Code Is Nothing Then
                If Not item.Foreign_Currency_Code = "" Then
                    Dim variable = UploadReportSTR.getMataUang(item.Foreign_Currency_Code)
                    If variable IsNot Nothing Then
                        txtMultiForeign_Currency_Code.Text = UploadReportSTR.getMataUang(item.Foreign_Currency_Code).Keterangan
                    End If
                End If
            End If
            If Not item.Foreign_Currency_Amount Is Nothing Then
                txtMultiForeign_Currency_Amount.Text = item.Foreign_Currency_Amount
            End If
            If Not item.Foreign_Currency_Exchange_Rate Is Nothing Then
                txtMultiForeign_Currency_Exchange_Rate.Text = item.Foreign_Currency_Exchange_Rate
            End If
            If Not item.Country Is Nothing Then
                If Not item.Country = "" Then
                    Dim variable = UploadReportSTR.getNamaNegara(item.Country)
                    If variable IsNot Nothing Then
                        txtMultiCountry.Text = UploadReportSTR.getNamaNegara(item.Country).Keterangan
                    End If
                End If
            End If
            If Not item.Comments Is Nothing Then
                txtMultiComments.Text = item.Comments
            End If
            If Not item.isSwift Is Nothing Then
                txtMultiisSwift.Text = item.isSwift
            End If

            'Update "" ke 0 20201109
            If Not item.FK_Source_Data_ID Is Nothing Then
                If Not item.FK_Source_Data_ID = 0 Then
                    Dim variable = UploadReportSTR.getSourceData(item.FK_Source_Data_ID)
                    txtMultiFK_Source_Data_ID.Text = UploadReportSTR.getSourceData(item.FK_Source_Data_ID).Description
                End If
            End If

            If Not item.Account_No Is Nothing Then
                txtMultiAccount_No.Text = item.Account_No
            End If
            If Not item.CIFNO Is Nothing Then
                txtMultiCIFNO.Text = item.CIFNO
            End If
            If Not item.WIC_NO Is Nothing Then
                txtMultiWIC_NO.Text = item.WIC_NO
            End If
            If Not item.Condutor_ID Is Nothing Then
                txtMultiCondutor_ID.Text = item.IsUsedConductor
                txtMultiIsUsedConductor.Text = "True"
            Else
                txtMultiIsUsedConductor.Text = "False"
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub GridPanelSetting()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 2 Then
            GridPanelTransactionBiParty.ColumnModel.Columns.RemoveAt(GridPanelTransactionBiParty.ColumnModel.Columns.Count - 1)
            GridPanelTransactionBiParty.ColumnModel.Columns.Insert(1, cmdbipartyvalid)
            GridPanelTransactionBipartyInvalid.ColumnModel.Columns.RemoveAt(GridPanelTransactionBipartyInvalid.ColumnModel.Columns.Count - 1)
            GridPanelTransactionBipartyInvalid.ColumnModel.Columns.Insert(1, cmdBipartyInvalid)
            GridPanelTransactionMultiParty.ColumnModel.Columns.RemoveAt(GridPanelTransactionMultiParty.ColumnModel.Columns.Count - 1)
            GridPanelTransactionMultiParty.ColumnModel.Columns.Insert(1, cmdmultipartyvalid)
            GridPanelTransactionMultiPartyInvalid.ColumnModel.Columns.RemoveAt(GridPanelTransactionMultiPartyInvalid.ColumnModel.Columns.Count - 1)
            GridPanelTransactionMultiPartyInvalid.ColumnModel.Columns.Insert(1, cmdMultipartyinValid)

        End If

    End Sub


    Sub Paging()

        StoreReportData.PageSize = SystemParameterBLL.GetPageSize
        StoreReportInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreIndikatorLaporanData.PageSize = SystemParameterBLL.GetPageSize
        StoreIndikatorLaporanInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreTransactionBiParty.PageSize = SystemParameterBLL.GetPageSize
        StoreTransactionBipartyInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreTransactionMultiParty.PageSize = SystemParameterBLL.GetPageSize
        StoreTransactionMultiPartyInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityAccount.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityAccountInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityEntity.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityEntityInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityPerson.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityPersonInvalid.PageSize = SystemParameterBLL.GetPageSize

    End Sub

#End Region

End Class