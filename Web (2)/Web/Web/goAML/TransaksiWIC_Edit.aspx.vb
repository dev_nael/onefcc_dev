﻿Imports NawaDevBLL
Imports NawaDevDAL

Partial Class TransaksiWIC_Edit
    Inherits ParentPage
    Public objFormModuleEdit As NawaBLL.FormModuleEdit

    'Public Property objViewTempRCSAEdit() As NawaDevDAL.vw_WP_Template_RCSA
    '    Get
    '        If Session("TemplateRCSA_Edit.objViewTempRCSAEdit") Is Nothing Then
    '            Session("TemplateRCSA_Edit.objViewTempRCSAEdit") = New NawaDevDAL.vw_WP_Template_RCSA
    '        End If
    '        Return Session("TemplateRCSA_Edit.objViewTempRCSAEdit")
    '    End Get
    '    Set(ByVal value As NawaDevDAL.vw_WP_Template_RCSA)
    '        Session("TemplateRCSA_Edit.objViewTempRCSAEdit") = value
    '    End Set
    'End Property

    'Public Property objTempRCSADetailEdit() As NawaDevDAL.vw_WP_Template_RCSA_Detail
    '    Get
    '        Return Session("TemplateRCSA_Edit.objEodTaskDetailEdit")
    '    End Get
    '    Set(ByVal value As NawaDevDAL.vw_WP_Template_RCSA_Detail)
    '        Session("TemplateRCSA_Edit.objEodTaskDetailEdit") = value
    '    End Set
    'End Property

    'Public Property objListTempRCSADetailEdit() As List(Of NawaDevDAL.vw_WP_Template_RCSA_Detail)
    '    Get
    '        Return Session("TemplateRCSA_Edit.objListTempRCSADetailEdit")
    '    End Get
    '    Set(ByVal value As List(Of NawaDevDAL.vw_WP_Template_RCSA_Detail))
    '        Session("TemplateRCSA_Edit.objListTempRCSADetailEdit") = value
    '    End Set
    'End Property

    'Public Property objRCSADetailEdit() As NawaDevDAL.WP_TR_RCSA_Detail
    '    Get
    '        Return Session("TemplateRCSA_Edit.objRCSADetailEdit")
    '    End Get
    '    Set(ByVal value As NawaDevDAL.WP_TR_RCSA_Detail)
    '        Session("TemplateRCSA_Edit.objRCSADetailEdit") = value
    '    End Set
    'End Property

    'Public Property objListRCSADetailEdit() As List(Of NawaDevDAL.WP_TR_RCSA_Detail)
    '    Get
    '        Return Session("TemplateRCSA_Edit.objListRCSADetailEdit")
    '    End Get
    '    Set(ByVal value As List(Of NawaDevDAL.WP_TR_RCSA_Detail))
    '        Session("TemplateRCSA_Edit.objListRCSADetailEdit") = value
    '    End Set
    'End Property

    Private Sub WorkingPaperQA_RCSA_TemplateRCSA_Edit_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleEdit = New NawaBLL.FormModuleEdit(FormPanelTaskDetail, Panelconfirmation, LblConfirmation)
    End Sub

    Sub ClearSession()
        ObjModule = Nothing
        'objViewTempRCSAEdit = Nothing
        'objTempRCSADetailEdit = Nothing
        'objRCSADetailEdit = Nothing
        'objListTempRCSADetailEdit = Nothing
        'objListRCSADetailEdit = Nothing
    End Sub

    'Sub LoadData(id As String)
    '    objListTempRCSADetailEdit = NawaDevBLL.TemplateRCSABLL.GetViewRCSADetailById(id)
    '    objListRCSADetailEdit = NawaDevBLL.TemplateRCSABLL.GetRCSADetailById(id)

    '    If Not objViewTempRCSAEdit Is Nothing Then
    '        With objViewTempRCSAEdit
    '            txtTemplateVersion.Text = .No_RCSA
    '            ddfTipeRCSA.SetTextValue(.Kode_Tipe_RCSA + "-" + .Tipe_RCSA)
    '            dateEffectiveDate.SelectedDate = .EffectiveDate
    '        End With
    '        BindDetail()
    '    End If
    'End Sub


    Protected Sub Page_Load1(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                Dim strmodule As String = Request.Params("ModuleID")
                ClearSession()
                Try
                    Dim intmodule As Integer = NawaBLL.Common.DecryptQueryString(strmodule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                    Me.ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intmodule)

                    If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Update) Then
                        Dim strIDCode As String = 1
                        strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                        Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    FormPanelTaskDetail.Title = "Transaksi WIC Edit"
                    Panelconfirmation.Title = "Transaksi WIC Edit"

                    Dim strid As String = Request.Params("ID")
                    Dim id As String = NawaBLL.Common.DecryptQueryString(strid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                    'objViewTempRCSAEdit = NawaDevBLL.TemplateRCSABLL.GetViewRCSAById(id)

                    'LoadData(id)
                    'loaddataapproval(id)
                Catch ex As Exception
                    Throw New Exception("Invalid Module ID")
                End Try
            End If
            'objEodTask.BentukformAdd()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Public Function IsDataAddValid() As Boolean
        Try
            'If dateEffectiveDate.RawText.Trim = "" Then
            '    Throw New Exception("Please Enter Effective Date")
            'End If
            'If String.IsNullOrEmpty(ddfTipeRCSA.TextValue) Then
            '    Throw New Exception("Please Select Tipe RCSA")
            'End If
            'If objListTempRCSADetailEdit.Count = 0 Then
            '    Throw New Exception("Risk Issue still empty.")
            'End If


            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function
    Protected Sub BtnSubmit_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            If IsDataAddValid() Then
                Dim alternateby As String = ""
                'If objViewTempRCSAEdit.Alternateby IsNot Nothing Then
                '    alternateby = objViewTempRCSAEdit.Alternateby
                'ElseIf NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                '    alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                'End If

                'Dim objdb As New NawaDataDevEntities
                'Dim objRCSAEdit As WP_TR_RCSA_Header = objdb.WP_TR_RCSA_Header.Where(Function(x) x.PK_RCSA_Header_ID = objViewTempRCSAEdit.PK_RCSA_Header_ID).FirstOrDefault()

                'objViewTempRCSAEdit.Kode_Tipe_RCSA = ddfTipeRCSA.TextValue
                'objViewTempRCSAEdit.EffectiveDate = dateEffectiveDate.SelectedDate
                'objViewTempRCSAEdit.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                'objViewTempRCSAEdit.LastUpdateDate = DateTime.Now
                'objViewTempRCSAEdit.Alternateby = alternateby


                'Dim objRCSAEdit As New WP_TR_RCSA_Header
                'With objRCSAEdit
                '    '.PK_RCSA_Header_ID = objViewTempRCSAEdit.PK_RCSA_Header_ID
                '    .No_RCSA = objViewTempRCSAEdit.No_RCSA
                '    .FK_Kode_Tipe_RCSA = ddfTipeRCSA.TextValue
                '    .EffectiveDate = dateEffectiveDate.SelectedDate
                '    .CreatedBy = objViewTempRCSAEdit.CreatedBy
                '    .CreatedDate = objViewTempRCSAEdit.CreatedDate
                '    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                '    .LastUpdateDate = DateTime.Now
                '    .Alternateby = alternateby
                'End With


                'If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                '    NawaDevBLL.TemplateRCSABLL.saveEditTanpaApproval(objViewTempRCSAEdit, objListRCSADetailEdit, ObjModule)
                '    Panelconfirmation.Hidden = False
                '    FormPanelInput.Hidden = True
                '    LblConfirmation.Text = "Data Saved into Database"
                'Else
                '    NawaDevBLL.TemplateRCSABLL.saveEditApproval(objViewTempRCSAEdit, objListRCSADetailEdit, ObjModule)
                '    Panelconfirmation.Hidden = False
                '    FormPanelInput.Hidden = True
                '    LblConfirmation.Text = "Data Saved into Pending Approval"
                'End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Function IsDataDetailValid() As Boolean
    '    If String.IsNullOrEmpty(ddfRiskType.TextValue) Then
    '        Throw New Exception("Please Select Risk Type")
    '    End If
    '    If txtRiskDescription.Text.Trim = "" Then
    '        Throw New Exception("Please Enter Risk Description")
    '    End If
    '    If txtSource.Text.Trim = "" Then
    '        Throw New Exception("Please Enter Source")
    '    End If
    '    If String.IsNullOrEmpty(ddfRegRequirement.TextValue) Then
    '        Throw New Exception("Please Select Regulatory Requirement")
    '    End If
    '    If txtControlDescription.Text.Trim = "" Then
    '        Throw New Exception("Please Enter Control Description")
    '    End If
    '    If txtControlDependency.Text.Trim = "" Then
    '        Throw New Exception("Please Enter Control Dependency")
    '    End If
    '    If String.IsNullOrEmpty(ddfControlDesignAdequacy.TextValue) Then
    '        Throw New Exception("Please Select Control Design Adequacy")
    '    End If
    '    If String.IsNullOrEmpty(ddfResidualRiskRating.TextValue) Then
    '        Throw New Exception("Please Select Residual Risk Rating")
    '    End If
    '    If txtControlTestingDescription.Text.Trim = "" Then
    '        Throw New Exception("Please Enter Control Testing Description")
    '    End If
    '    If String.IsNullOrEmpty(ddfControlFrequency.TextValue) Then
    '        Throw New Exception("Please Select Control Frequency")
    '    End If
    '    If txtMinSampleSize.Text.Trim = "" Then
    '        Throw New Exception("Please Enter Minimum Sample Size")
    '    End If
    '    If String.IsNullOrEmpty(ddfTestingFrequency.TextValue) Then
    '        Throw New Exception("Please Select Testing Frequency")
    '    End If
    '    If txtRemark.Text.Trim = "" Then
    '        Throw New Exception("Please Enter Remark")
    '    End If
    '    Return True
    'End Function

    'Sub SaveAddTaskDetail()
    '    Dim objNewViewRCSADetail As New NawaDevDAL.vw_WP_Template_RCSA_Detail
    '    Dim objNewRCSADetail As New NawaDevDAL.WP_TR_RCSA_Detail
    '    Dim objRand As New Random
    '    Dim intpk As Long = objRand.Next(Integer.MinValue, -1)
    '    While Not objListTempRCSADetailEdit.Find(Function(x) x.PK_RCSA_Detail_ID = intpk) Is Nothing
    '        intpk = objRand.Next
    '    End While
    '    With objNewViewRCSADetail
    '        .PK_RCSA_Detail_ID = intpk
    '        .PK_RCSA_Header_ID = objViewTempRCSAEdit.PK_RCSA_Header_ID
    '        .No_RCSA = txtTemplateVersion.Text
    '        .Kode_Tipe_RCSA = ddfTipeRCSA.TextValue
    '        .Tipe_RCSA = txtTipeRCSA.Text
    '        .EffectiveDate = dateEffectiveDate.SelectedDate
    '        .CET_No = txtCet.Text.Trim
    '        .Kode_Branch = ddfBranch.TextValue
    '        If Not ddfBranch.TextValue Is Nothing Then
    '            .Branch_Name = ddfBranch.TextRawValue.Split("-")(1)
    '        End If
    '        .Kode_Risk_Type = ddfRiskType.TextValue
    '        .Risk_Type = ddfRiskType.TextRawValue.Split("-")(1)
    '        .Risk_Description = txtRiskDescription.Text.Trim
    '        .Source = txtSource.Text.Trim
    '        .FK_Kode_Regulatory_Requirement = ddfRegRequirement.TextValue
    '        .Regulatory_Requirement = ddfRegRequirement.TextRawValue.Split("-")(1)
    '        .CM_Ref = txtCMRef.Text.Trim
    '        .Applicable_Regulation = txtApplicableRegulation.Text.Trim
    '        .Control_Description = txtControlDescription.Text.Trim
    '        .Control_Dependency = txtControlDependency.Text.Trim
    '        .Control_Design_Adequacy = ddfControlDesignAdequacy.TextValue
    '        .CDA = ddfControlDesignAdequacy.TextRawValue.Split("-")(1)
    '        .Justification = txtJustification.Text.Trim
    '        .CIM = txtCIM.Text.Trim
    '        .FK_Kode_Residual_Risk_Rating = ddfResidualRiskRating.TextValue
    '        .Residual_Risk_Rating = ddfResidualRiskRating.TextRawValue.Split("-")(1)
    '        .Control_Testing_Description = txtControlTestingDescription.Text.Trim
    '        .FK_Kode_Control_Frequency = ddfControlFrequency.TextValue
    '        .Control_Frequency = ddfControlFrequency.TextRawValue.Split("-")(1)
    '        .FK_Minimum_Sample_Size = txtMinSampleSize.Text.Trim
    '        .FK_Kode_Testing_Frequency = ddfTestingFrequency.TextValue
    '        .Testing_Frequency = ddfTestingFrequency.TextRawValue.Split("-")(1)
    '        .Remark = txtRemark.Text.Trim
    '        '.OrderNo = objListEodTaskDetailEdit.Count + 1
    '    End With

    '    With objNewRCSADetail
    '        .PK_RCSA_Detail_ID = intpk
    '        .FK_RCSA_Header_ID = objViewTempRCSAEdit.PK_RCSA_Header_ID
    '        .CET_No = txtCet.Text.Trim
    '        .FK_Kode_Branch = ddfBranch.TextValue
    '        .FK_Kode_Risk_Type = ddfRiskType.TextValue
    '        .Risk_Description = txtRiskDescription.Text.Trim
    '        .Source = txtSource.Text.Trim
    '        .FK_Kode_Regulatory_Requirement = ddfRegRequirement.TextValue
    '        .CM_Ref = txtCMRef.Text.Trim
    '        .Applicable_Regulation = txtApplicableRegulation.Text.Trim
    '        .Control_Description = txtControlDescription.Text.Trim
    '        .Control_Dependency = txtControlDependency.Text.Trim
    '        .Control_Design_Adequacy = ddfControlDesignAdequacy.TextValue
    '        .Justification = txtJustification.Text.Trim
    '        .CIM = txtCIM.Text.Trim
    '        .FK_Kode_Residual_Risk_Rating = ddfResidualRiskRating.TextValue
    '        .Control_Testing_Description = txtControlTestingDescription.Text.Trim
    '        .FK_Kode_Control_Frequency = ddfControlFrequency.TextValue
    '        .FK_Minimum_Sample_Size = txtMinSampleSize.Text.Trim
    '        .FK_Kode_Testing_Frequency = ddfTestingFrequency.TextValue
    '        .FK_Kode_Tipe_RCSA = ddfTipeRCSA.TextValue
    '        .EffectiveDate = dateEffectiveDate.SelectedDate
    '        .Remark = txtRemark.Text.Trim
    '    End With

    '    objListTempRCSADetailEdit.Add(objNewViewRCSADetail)
    '    objListRCSADetailEdit.Add(objNewRCSADetail)
    '    BindDetail()
    '    FormPanelTaskDetail.Hidden = True
    '    WindowDetail.Hidden = True
    '    Clearinput()
    'End Sub

    'Sub SaveEditTaskDetail()

    '    With objTempRCSADetailEdit
    '        .No_RCSA = txtTemplateVersion.Text
    '        .Kode_Tipe_RCSA = ddfTipeRCSA.TextValue
    '        .Tipe_RCSA = txtTipeRCSA.Text
    '        .EffectiveDate = dateEffectiveDate.SelectedDate
    '        .CET_No = txtCet.Text.Trim
    '        .Kode_Branch = ddfBranch.TextValue
    '        If Not ddfBranch.TextValue Is Nothing Then
    '            .Branch_Name = ddfBranch.TextRawValue.Split("-")(1)
    '        End If
    '        .Kode_Risk_Type = ddfRiskType.TextValue
    '        .Risk_Type = ddfRiskType.TextRawValue.Split("-")(1)
    '        .Risk_Description = txtRiskDescription.Text.Trim
    '        .Source = txtSource.Text.Trim
    '        .FK_Kode_Regulatory_Requirement = ddfRegRequirement.TextValue
    '        .Regulatory_Requirement = ddfRegRequirement.TextRawValue.Split("-")(1)
    '        .CM_Ref = txtCMRef.Text.Trim
    '        .Applicable_Regulation = txtApplicableRegulation.Text.Trim
    '        .Control_Description = txtControlDescription.Text.Trim
    '        .Control_Dependency = txtControlDependency.Text.Trim
    '        .Control_Design_Adequacy = ddfControlDesignAdequacy.TextValue
    '        .CDA = ddfControlDesignAdequacy.TextRawValue.Split("-")(1)
    '        .Justification = txtJustification.Text.Trim
    '        .CIM = txtCIM.Text.Trim
    '        .FK_Kode_Residual_Risk_Rating = ddfResidualRiskRating.TextValue
    '        .Residual_Risk_Rating = ddfResidualRiskRating.TextRawValue.Split("-")(1)
    '        .Control_Testing_Description = txtControlTestingDescription.Text.Trim
    '        .FK_Kode_Control_Frequency = ddfControlFrequency.TextValue
    '        .Control_Frequency = ddfControlFrequency.TextRawValue.Split("-")(1)
    '        .FK_Minimum_Sample_Size = txtMinSampleSize.Text.Trim
    '        .FK_Kode_Testing_Frequency = ddfTestingFrequency.TextValue
    '        .Testing_Frequency = ddfTestingFrequency.TextRawValue.Split("-")(1)
    '        .Remark = txtRemark.Text.Trim
    '    End With

    '    With objRCSADetailEdit
    '        .CET_No = txtCet.Text.Trim
    '        .FK_Kode_Branch = ddfBranch.TextValue
    '        .FK_Kode_Risk_Type = ddfRiskType.TextValue
    '        .Risk_Description = txtRiskDescription.Text.Trim
    '        .Source = txtSource.Text.Trim
    '        .FK_Kode_Regulatory_Requirement = ddfRegRequirement.TextValue
    '        .CM_Ref = txtCMRef.Text.Trim
    '        .Applicable_Regulation = txtApplicableRegulation.Text.Trim
    '        .Control_Description = txtControlDescription.Text.Trim
    '        .Control_Dependency = txtControlDependency.Text.Trim
    '        .Control_Design_Adequacy = ddfControlDesignAdequacy.TextValue
    '        .Justification = txtJustification.Text.Trim
    '        .CIM = txtCIM.Text.Trim
    '        .FK_Kode_Residual_Risk_Rating = ddfResidualRiskRating.TextValue
    '        .Control_Testing_Description = txtControlTestingDescription.Text.Trim
    '        .FK_Kode_Control_Frequency = ddfControlFrequency.TextValue
    '        .FK_Minimum_Sample_Size = txtMinSampleSize.Text.Trim
    '        .FK_Kode_Testing_Frequency = ddfTestingFrequency.TextValue
    '        .FK_Kode_Tipe_RCSA = ddfTipeRCSA.TextValue
    '        .EffectiveDate = dateEffectiveDate.SelectedDate
    '        .Remark = txtRemark.Text.Trim
    '    End With

    '    objTempRCSADetailEdit = Nothing
    '    objRCSADetailEdit = Nothing
    '    BindDetail()
    '    FormPanelTaskDetail.Hidden = True
    '    WindowDetail.Hidden = True
    '    Clearinput()
    'End Sub

    Protected Sub btnSavedetail_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'If IsDataDetailValid() Then
            '    If objTempRCSADetailEdit Is Nothing Then
            '        SaveAddTaskDetail()
            '    Else
            '        SaveEditTaskDetail()
            '    End If
            'End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub DeleteRecordTaskDetail(id As Long)
        btnBackSaveDetail_DirectEvent(Nothing, Nothing)
        'Dim objDel As NawaDevDAL.WP_TR_RCSA_Detail = objListRCSADetailEdit.Find(Function(x) x.PK_RCSA_Detail_ID = id)
        'Dim objxDel As NawaDevDAL.vw_WP_Template_RCSA_Detail = objListTempRCSADetailEdit.Find(Function(x) x.PK_RCSA_Detail_ID = id)

        'If Not objDel Is Nothing Then
        '    objListRCSADetailEdit.Remove(objDel)
        'End If

        'If Not objxDel Is Nothing Then
        '    objListTempRCSADetailEdit.Remove(objxDel)
        '    BindDetail()
        'End If
    End Sub

    'Sub LoadDataDetail(id As Long)
    '    Dim objRCSADetail_Detail As vw_WP_Template_RCSA_Detail = objListTempRCSADetailEdit.Find(Function(x) x.PK_RCSA_Detail_ID = id)

    '    If Not objRCSADetail_Detail Is Nothing Then
    '        FormPanelTaskDetail.Hidden = False
    '        WindowDetail.Hidden = False
    '        btnSavedetail.Hidden = True
    '        WindowDetail.Title = "Risk Issue Detail"
    '        Clearinput()
    '        With objRCSADetail_Detail
    '            ddfBranch.SetTextValue(.Kode_Branch + "-" + .Branch_Name)
    '            ddfRiskType.SetTextValue(.Kode_Risk_Type + "-" + .Risk_Type)
    '            ddfRegRequirement.SetTextValue(.FK_Kode_Regulatory_Requirement + "-" + .Regulatory_Requirement)

    '            ddfControlDesignAdequacy.SetTextValue(.Control_Design_Adequacy + "-" + .CDA)
    '            ddfResidualRiskRating.SetTextValue(.FK_Kode_Residual_Risk_Rating + "-" + .Residual_Risk_Rating)
    '            ddfControlFrequency.SetTextValue(.FK_Kode_Control_Frequency + "-" + .Control_Frequency)
    '            ddfTestingFrequency.SetTextValue(.FK_Kode_Testing_Frequency + "-" + .Testing_Frequency)

    '            txtCet.Text = .CET_No
    '            txtRiskDescription.Text = .Risk_Description
    '            txtSource.Text = .Source
    '            txtCMRef.Text = .CM_Ref
    '            txtApplicableRegulation.Text = .Applicable_Regulation
    '            txtControlDescription.Text = .Control_Description
    '            txtControlDependency.Text = .Control_Dependency
    '            txtJustification.Text = .Justification
    '            txtCIM.Text = .CIM
    '            txtControlTestingDescription.Text = .Control_Testing_Description
    '            txtMinSampleSize.Text = .FK_Minimum_Sample_Size
    '            txtRemark.Text = .Remark
    '            txtTipeRCSA.Text = ddfTipeRCSA.TextRawValue.Split("-")(1)
    '            txtEffectiveDate.Text = dateEffectiveDate.RawText
    '        End With

    '        EnvironmentForm("Detail")
    '    End If
    'End Sub

    'Sub LoadDataEdit(id As Long)
    '    objRCSADetailEdit = objListRCSADetailEdit.Find(Function(x) x.PK_RCSA_Detail_ID = id)
    '    objTempRCSADetailEdit = objListTempRCSADetailEdit.Find(Function(x) x.PK_RCSA_Detail_ID = id)

    '    If Not objTempRCSADetailEdit Is Nothing Then
    '        FormPanelTaskDetail.Hidden = False
    '        WindowDetail.Hidden = False
    '        btnSavedetail.Hidden = False
    '        WindowDetail.Title = "Risk Issue Edit"
    '        Clearinput()
    '        With objTempRCSADetailEdit
    '            ddfBranch.SetTextValue(.Kode_Branch + "-" + .Branch_Name)
    '            ddfRiskType.SetTextValue(.Kode_Risk_Type + "-" + .Risk_Type)
    '            ddfRegRequirement.SetTextValue(.FK_Kode_Regulatory_Requirement + "-" + .Regulatory_Requirement)

    '            ddfControlDesignAdequacy.SetTextValue(.Control_Design_Adequacy + "-" + .CDA)
    '            ddfResidualRiskRating.SetTextValue(.FK_Kode_Residual_Risk_Rating + "-" + .Residual_Risk_Rating)
    '            ddfControlFrequency.SetTextValue(.FK_Kode_Control_Frequency + "-" + .Control_Frequency)
    '            ddfTestingFrequency.SetTextValue(.FK_Kode_Testing_Frequency + "-" + .Testing_Frequency)

    '            If .Control_Design_Adequacy.ToLower = "true" Then
    '                txtJustification.ReadOnly = False
    '            Else
    '                txtCIM.ReadOnly = False
    '            End If

    '            txtCet.Text = .CET_No
    '            txtRiskDescription.Text = .Risk_Description
    '            txtSource.Text = .Source
    '            txtCMRef.Text = .CM_Ref
    '            txtApplicableRegulation.Text = .Applicable_Regulation
    '            txtControlDescription.Text = .Control_Description
    '            txtControlDependency.Text = .Control_Dependency
    '            txtJustification.Text = .Justification
    '            txtCIM.Text = .CIM
    '            txtControlTestingDescription.Text = .Control_Testing_Description
    '            txtMinSampleSize.Text = .FK_Minimum_Sample_Size
    '            txtRemark.Text = .Remark
    '            txtTipeRCSA.Text = ddfTipeRCSA.TextRawValue.Split("-")(1)
    '            txtEffectiveDate.Text = dateEffectiveDate.RawText
    '        End With

    '        EnvironmentForm("Edit")
    '    End If
    'End Sub



    Protected Sub GridCommand(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                'Clearinput()
                'LoadDataDetail(id)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                'Clearinput()
                'LoadDataEdit(id)
            ElseIf e.ExtraParams(1).Value = "Delete" Then
                DeleteRecordTaskDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BindDetail()
        'StoreRiskIssue.DataSource = objListTempRCSADetailEdit.ToList()
        'StoreRiskIssue.DataBind()
    End Sub

    'Sub Clearinput()
    '    ddfBranch.SetTextValue("")
    '    ddfRiskType.SetTextValue("")
    '    ddfRegRequirement.SetTextValue("")
    '    ddfResidualRiskRating.SetTextValue("")
    '    ddfControlDesignAdequacy.SetTextValue("")
    '    ddfResidualRiskRating.SetTextValue("")
    '    ddfControlFrequency.SetTextValue("")
    '    ddfTestingFrequency.SetTextValue("")

    '    txtCet.Text = ""
    '    txtRiskDescription.Text = ""
    '    txtSource.Text = ""
    '    txtCMRef.Text = ""
    '    txtApplicableRegulation.Text = ""
    '    txtControlDescription.Text = ""
    '    txtControlDependency.Text = ""
    '    txtJustification.Text = ""
    '    txtCIM.Text = ""
    '    txtControlTestingDescription.Text = ""
    '    txtMinSampleSize.Text = ""
    '    txtRemark.Text = ""
    '    txtTipeRCSA.Text = ""
    '    txtEffectiveDate.Text = ""
    'End Sub

    'Sub EnvironmentForm(FormMode As String)
    '    If FormMode = "Add" Then
    '        txtTipeRCSA.Text = ddfTipeRCSA.TextRawValue.Split("-")(1)
    '        txtEffectiveDate.Text = dateEffectiveDate.RawText

    '        txtCet.ReadOnly = False
    '        txtRiskDescription.ReadOnly = False
    '        txtSource.ReadOnly = False
    '        txtCMRef.ReadOnly = False
    '        txtApplicableRegulation.ReadOnly = False
    '        txtControlDescription.ReadOnly = False
    '        txtControlDependency.ReadOnly = False
    '        txtControlTestingDescription.ReadOnly = False
    '        txtMinSampleSize.ReadOnly = False
    '        txtRemark.ReadOnly = False

    '        ddfBranch.IsReadOnly = False
    '        ddfRiskType.IsReadOnly = False
    '        ddfRegRequirement.IsReadOnly = False
    '        ddfControlDesignAdequacy.IsReadOnly = False
    '        ddfResidualRiskRating.IsReadOnly = False
    '        ddfControlFrequency.IsReadOnly = False
    '        ddfTestingFrequency.IsReadOnly = False

    '        ddfBranch.StringFieldStyle = "background-color:#ffccb3;"
    '        ddfRiskType.StringFieldStyle = "background-color:#ffccb3;"
    '        ddfRegRequirement.StringFieldStyle = "background-color:#ffccb3;"
    '        ddfControlDesignAdequacy.StringFieldStyle = "background-color:#ffccb3;"
    '        ddfResidualRiskRating.StringFieldStyle = "background-color:#ffccb3;"
    '        ddfControlFrequency.StringFieldStyle = "background-color:#ffccb3;"
    '        ddfTestingFrequency.StringFieldStyle = "background-color:#ffccb3;"

    '    ElseIf FormMode = "Edit" Then
    '        txtCet.ReadOnly = True
    '        txtRiskDescription.ReadOnly = False
    '        txtSource.ReadOnly = False
    '        txtCMRef.ReadOnly = False
    '        txtApplicableRegulation.ReadOnly = False
    '        txtControlDescription.ReadOnly = False
    '        txtControlDependency.ReadOnly = False
    '        txtControlTestingDescription.ReadOnly = False
    '        txtMinSampleSize.ReadOnly = False
    '        txtRemark.ReadOnly = False

    '        ddfBranch.IsReadOnly = True
    '        ddfRiskType.IsReadOnly = False
    '        ddfRegRequirement.IsReadOnly = False
    '        ddfControlDesignAdequacy.IsReadOnly = False
    '        ddfResidualRiskRating.IsReadOnly = False
    '        ddfControlFrequency.IsReadOnly = False
    '        ddfTestingFrequency.IsReadOnly = False

    '        ddfRiskType.StringFieldStyle = "background-color:#ffccb3;"
    '        ddfRegRequirement.StringFieldStyle = "background-color:#ffccb3;"
    '        ddfControlDesignAdequacy.StringFieldStyle = "background-color:#ffccb3;"
    '        ddfResidualRiskRating.StringFieldStyle = "background-color:#ffccb3;"
    '        ddfControlFrequency.StringFieldStyle = "background-color:#ffccb3;"
    '        ddfTestingFrequency.StringFieldStyle = "background-color:#ffccb3;"

    '    ElseIf FormMode = "Detail" Then
    '        txtCet.ReadOnly = True
    '        txtRiskDescription.ReadOnly = True
    '        txtSource.ReadOnly = True
    '        txtCMRef.ReadOnly = True
    '        txtApplicableRegulation.ReadOnly = True
    '        txtControlDescription.ReadOnly = True
    '        txtControlDependency.ReadOnly = True
    '        txtJustification.ReadOnly = True
    '        txtCIM.ReadOnly = True
    '        txtControlTestingDescription.ReadOnly = True
    '        txtMinSampleSize.ReadOnly = True
    '        txtRemark.ReadOnly = True

    '        ddfBranch.IsReadOnly = True
    '        ddfRiskType.IsReadOnly = True
    '        ddfRegRequirement.IsReadOnly = True
    '        ddfControlDesignAdequacy.IsReadOnly = True
    '        ddfResidualRiskRating.IsReadOnly = True
    '        ddfControlFrequency.IsReadOnly = True
    '        ddfTestingFrequency.IsReadOnly = True
    '    End If
    'End Sub

    Protected Sub btnBackSaveDetail_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = True
            WindowDetail.Hidden = True

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnBack_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnAddRiskIssue_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            FormPanelTaskDetail.Hidden = False
            WindowDetail.Hidden = False
            btnSavedetail.Hidden = False
            'Clearinput()
            'objTempRCSADetailEdit = Nothing
            'objRCSADetailEdit = Nothing

            WindowDetail.Title = "Risk Issue Add"
            'EnvironmentForm("Add")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub Page_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad

        ActionType = NawaBLL.Common.ModuleActionEnum.Update
    End Sub
End Class
