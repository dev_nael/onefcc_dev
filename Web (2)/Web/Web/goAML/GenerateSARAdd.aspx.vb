﻿Imports System.Data
Imports NawaBLL.Nawa.BLL.NawaFramework
Imports NawaDevDAL
Imports NawaBLL
Imports Ext
Imports NawaDevBLL
Imports System.Data.SqlClient
Imports NawaDAL
Imports Elmah
Imports System.IO
Imports OfficeOpenXml
Partial Class goAML_GenerateSARAdd
    Inherits Parent

#Region "Session"
    Public objFormModuleView As FormModuleView
    Public Property strWhereClause() As String
        Get
            Return Session("goAML_GenerateSARAdd.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSARAdd.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("goAML_GenerateSARAdd.strSort")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSARAdd.strSort") = value
        End Set
    End Property

    Public Property indexStart() As String
        Get
            Return Session("goAML_GenerateSARAdd.indexStart")
        End Get
        Set(ByVal value As String)
            Session("goAML_GenerateSARAdd.indexStart") = value
        End Set
    End Property
    'Private ObjEODTask As NawaDevBLL.EODTaskBLL
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("goAML_GenerateSARAdd.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_GenerateSARAdd.ObjModule") = value
        End Set
    End Property
    Public Property ListTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi)
        Get
            If Session("goAML_GenerateSARAdd.ListTransaction") Is Nothing Then
                Session("goAML_GenerateSARAdd.ListTransaction") = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
            End If
            Return Session("goAML_GenerateSARAdd.ListTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Transaksi))
            Session("goAML_GenerateSARAdd.ListTransaction") = value
        End Set
    End Property
    Public Property ListSelectedTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi)
        Get
            If Session("goAML_GenerateSARAdd.ListSelectedTransaction") Is Nothing Then
                Session("goAML_GenerateSARAdd.ListSelectedTransaction") = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
            End If
            Return Session("goAML_GenerateSARAdd.ListSelectedTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Transaksi))
            Session("goAML_GenerateSARAdd.ListSelectedTransaction") = value
        End Set
    End Property
    Public Property ListSendTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi)
        Get
            If Session("goAML_GenerateSARAdd.ListSendTransaction") Is Nothing Then
                Session("goAML_GenerateSARAdd.ListSendTransaction") = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
            End If
            Return Session("goAML_GenerateSARAdd.ListSendTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Transaksi))
            Session("goAML_GenerateSARAdd.ListSendTransaction") = value
        End Set
    End Property
    Public Property ListGenerateSarTransaction As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction)
        Get
            If Session("goAML_GenerateSARAdd.ListGenerateSarTransaction") Is Nothing Then
                Session("goAML_GenerateSARAdd.ListGenerateSarTransaction") = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction)
            End If
            Return Session("goAML_GenerateSARAdd.ListGenerateSarTransaction")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction))
            Session("goAML_GenerateSARAdd.ListGenerateSarTransaction") = value
        End Set
    End Property
    Public Property objGenerateSAR As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR)
        Get
            If Session("goAML_GenerateSARAdd.objGenerateSAR") Is Nothing Then
                Session("goAML_GenerateSARAdd.objGenerateSAR") = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR)
            End If
            Return Session("goAML_GenerateSARAdd.objGenerateSAR")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR))
            Session("goAML_GenerateSARAdd.objGenerateSAR") = value
        End Set
    End Property
    Public Property ObjSARData As NawaDevBLL.GenerateSARData
        Get
            Return Session("goAML_GenerateSARAdd.ObjSARData")
        End Get
        Set(ByVal value As NawaDevBLL.GenerateSARData)
            Session("goAML_GenerateSARAdd.ObjSARData") = value
        End Set
    End Property
    Public Property ListIndicator As List(Of NawaDevDAL.goAML_Report_Indicator)
        Get
            Return Session("goAML_Report_ReportAdd.ListIndicator")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_Report_Indicator))
            Session("goAML_Report_ReportAdd.ListIndicator") = value
        End Set
    End Property
    Public Property ListDokumen As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment)
        Get
            Return Session("goAML_Report_ReportAdd.ListDokumen")
        End Get
        Set(value As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment))
            Session("goAML_Report_ReportAdd.ListDokumen") = value
        End Set
    End Property

    Public Property IDIndicator As String
        Get
            Return Session("goAML_Report_ReportAdd.IDIndicator")
        End Get
        Set(value As String)
            Session("goAML_Report_ReportAdd.IDIndicator") = value
        End Set
    End Property

    Public Property IDDokumen As String
        Get
            Return Session("goAML_Report_ReportAdd.IDDokumen")
        End Get
        Set(value As String)
            Session("goAML_Report_ReportAdd.IDDokumen") = value
        End Set
    End Property

#End Region

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                Dim strmodule As String = Request.Params("ModuleID")
                ClearSession()
                Dim intmodule As Integer = NawaBLL.Common.DecryptQueryString(strmodule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intmodule)

                If Not ObjModule Is Nothing Then
                    If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Insert) Then
                        Dim strIDCode As String = 1
                        strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                        Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    FormPanelInput.Title = ObjModule.ModuleLabel & " Add"
                    'Panelconfirmation.Title = ObjModule.ModuleLabel & " Add"
                    'StoreDetailType.Reload()

                    'Dim objrand As New Random
                    'ObjTask.PK_EODTask_ID = objrand.Next

                    LoadStoreComboBox()
                    sar_DateFrom.Value = Date.Now
                    sar_DateTo.Value = Date.Now
                    '' Add 27-Dec-2022 , Felix. Thanks to Try.
                    sar_DateFrom.MaxDate = Now
                    sar_DateTo.MaxDate = Now
                    TanggalLaporan.MaxDate = Now
                    '' End 27-Dec-2022
                    FileDoc.FieldStyle = "background-color: #FFE4C4"
                    NoRefPPATK.FieldStyle = "background-color: #FFE4C4"
                    sar_reportIndicator.FieldStyle = "background-color: #FFE4C4"
                    ColumnActionLocation()
                    PopupMaximizale()
                Else
                    Throw New Exception("Invalid Module ID")
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
    '    ObjEODTask = New NawaDevBLL.EODTaskBLL(FormPanelInput)
    'End Sub
#End Region

#Region "Method"
    Sub ColumnActionLocation()
        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 1 Then
            'GridPanelReportIndicator.ColumnModel.Columns.Add(CommandColumn87)
        ElseIf bsettingRight = 2 Then
            GridPanelReportIndicator.ColumnModel.Columns.RemoveAt(GridPanelReportIndicator.ColumnModel.Columns.Count - 1)
            GridPanelReportIndicator.ColumnModel.Columns.Insert(1, CommandColumn87)

            GridPanelAttachment.ColumnModel.Columns.RemoveAt(GridPanelAttachment.ColumnModel.Columns.Count - 1)
            GridPanelAttachment.ColumnModel.Columns.Insert(1, CommandColumnAttachment)
        End If

    End Sub

    Sub PopupMaximizale()
        WindowReportIndicator.Maximizable = True
        WindowAttachment.Maximizable = True
    End Sub
    Private Sub ClearSession()
        ListTransaction = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
        ListSelectedTransaction = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
        ListSendTransaction = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
        objGenerateSAR = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR)
        ListIndicator = New List(Of NawaDevDAL.goAML_Report_Indicator)
        ObjSARData = New NawaDevBLL.GenerateSARData
        ListGenerateSarTransaction = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Transaction)
        ListDokumen = New List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment)
    End Sub
    Sub LoadStoreComboBox()
        sar_jenisLaporan.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreJenisLaporan.Reload()

        sar_reportIndicator.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        StoreReportIndicator.Reload()

        '' Remark 27-Dec-2022, Felix. Ganti Dropdown jd search Window.
        'sar_CIF.PageSize = NawaBLL.SystemParameterBLL.GetPageSize
        'StoreCIF.Reload()
    End Sub
    Protected Sub CIF_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Customer_GenerateSAR", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub ReportIndicator_readData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goaml_ref_indicator_laporan", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub JenisLaporan_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim query As String = e.Parameters("query")
            If query Is Nothing Then query = ""

            Dim strfilter As String = ""
            If query.Length > 0 Then

                strfilter = " Keterangan Like '%" & query & "%'"

            End If
            If strfilter.Length > 0 Then
                strfilter += " and active=1"
            Else
                strfilter += "active=1"
            End If

            Dim objstore As Ext.Net.Store = CType(sender, Ext.Net.Store)

            objstore.DataSource = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Jenis_Laporan_STR", "Kode, Keterangan", strfilter, "Keterangan", e.Start, e.Limit, e.Total)
            objstore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Sub bindTransaction(store As Ext.Net.Store, listTransaction As List(Of NawaDevDAL.goAML_ODM_Transaksi))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listTransaction)
        objtable.Columns.Add(New DataColumn("Valid", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("NoRefTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("TipeTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("LokasiTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("KeteranganTran", GetType(String)))
        objtable.Columns.Add(New DataColumn("DateTransaction", GetType(Date)))
        objtable.Columns.Add(New DataColumn("AccountNo", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaTeller", GetType(String)))
        objtable.Columns.Add(New DataColumn("NamaPejabat", GetType(String)))
        objtable.Columns.Add(New DataColumn("TglPembukuan", GetType(Date)))
        objtable.Columns.Add(New DataColumn("CaraTranDilakukan", GetType(String)))
        objtable.Columns.Add(New DataColumn("CaraTranLain", GetType(String)))
        objtable.Columns.Add(New DataColumn("DebitCredit", GetType(String)))
        objtable.Columns.Add(New DataColumn("OriginalAmount", GetType(String)))
        objtable.Columns.Add(New DataColumn("IDR", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                Dim CifLawan As String = item("CIF_No_Lawan").ToString
                Dim WicLawan As String = item("WIC_No_Lawan").ToString
                Dim AccountLawan As String = item("ACCOUNT_No_Lawan").ToString
                Dim BiMulti As String = item("BiMultiParty").ToString
                If CifLawan = "" And WicLawan = "" And AccountLawan = "" And BiMulti = "1" Then
                    item("Valid") = "Tidak"
                Else
                    item("Valid") = "Ya"
                End If
                item("NoTran") = item("Transaction_Number")
                item("NoRefTran") = item("Ref_Num")
                Dim SourceData As String = item("Source_Data").ToString
                Dim SourceDataTemp As Integer
                If Integer.TryParse(SourceData, SourceDataTemp) Then
                    If SourceData <> "" Then
                        item("TipeTran") = NawaDevBLL.GenerateSarBLL.getSourceData(SourceData)
                    End If
                Else
                    item("TipeTran") = SourceData
                End If
                item("LokasiTran") = item("Transaction_Location")
                item("KeteranganTran") = item("Transaction_Remark")
                item("DateTransaction") = item("Date_Transaction")
                item("AccountNo") = item("Account_NO")
                item("NamaTeller") = item("Teller")
                item("NamaPejabat") = item("Authorized")
                item("TglPembukuan") = item("Date_Posting")
                item("CaraTranDilakukan") = item("Transmode_Code")
                item("CaraTranLain") = item("Transmode_Comment")
                item("DebitCredit") = item("Debit_Credit")
                If item("Original_Amount").ToString <> "" Then
                    Dim OriginalAmount As Decimal = item("Original_Amount").ToString
                    item("OriginalAmount") = OriginalAmount.ToString("#,###.00")
                End If
                If item("IDR_Amount").ToString <> "" Then
                    Dim IDR As Decimal = item("IDR_Amount").ToString
                    item("IDR") = IDR.ToString("#,###.00")
                End If
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindAttachment(store As Ext.Net.Store, listAttachment As List(Of NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAttachment)
        objtable.Columns.Add(New DataColumn("FileName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("FileName") = item("File_Name")
                item("Keterangan") = item("Remarks")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Sub bindReportIndicator(store As Ext.Net.Store, listAddresss As List(Of NawaDevDAL.goAML_Report_Indicator))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listAddresss)
        '  objtable.Columns.Add(New DataColumn("Kode_Indicator", GetType(String)))
        objtable.Columns.Add(New DataColumn("Keterangan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                '     item("Kode_Indicator") = item("FK_Indicator")
                item("Keterangan") = getReportIndicatorByKode(item("FK_Indicator").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub

    Shared Function getReportIndicatorByKode(Kode As String) As String
        Dim strKategori As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim kategori As NawaDevDAL.goAML_Ref_Indikator_Laporan = objdb.goAML_Ref_Indikator_Laporan.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If kategori IsNot Nothing Then
                strKategori = kategori.Keterangan
            End If
        End Using
        Return strKategori
    End Function


    Sub editIndicator(id As String, command As String)
        Try
            sar_reportIndicator.Clear()

            WindowReportIndicator.Hidden = False
            Dim reportIndicator As NawaDevDAL.goAML_Report_Indicator
            reportIndicator = ListIndicator.Where(Function(x) x.PK_Report_Indicator = id).FirstOrDefault


            sar_reportIndicator.SetValue(reportIndicator.FK_Indicator)
            If command = "Detail" Then
                sar_reportIndicator.Selectable = False
                BtnsaveReportIndicator.Hidden = True
            Else
                sar_reportIndicator.Selectable = True
                BtnsaveReportIndicator.Hidden = False
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub editDokumen(id As String, command As String)
        Try
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()

            WindowAttachment.Hidden = False
            Dim Dokumen As New NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment
            Dokumen = ListDokumen.Where(Function(x) x.PK_ID = id).FirstOrDefault

            txtFileName.Text = Dokumen.File_Name
            txtKeterangan.Text = Dokumen.Remarks

            If command = "Detail" Then
                FileDoc.Hidden = True
                BtnsaveAttachment.Hidden = True
                txtKeterangan.Editable = False
            Else
                FileDoc.Hidden = False
                BtnsaveAttachment.Hidden = False
                txtKeterangan.Editable = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub DownloadFile(id As Long)
        Dim objdownload As NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment = ListDokumen.Find(Function(x) x.PK_ID = id)
        If Not objdownload Is Nothing Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & objdownload.File_Name)
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            'Response.ContentType = "ContentType"
            Response.ContentType = MimeMapping.GetMimeMapping(objdownload.File_Name)
            Response.BinaryWrite(objdownload.File_Doc)
            Response.End()
        End If
    End Sub

    Protected Sub CreateExcel2007WithData(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try


            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            'Dim intTotalRow As Long
            Dim objSchemaModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(12226)
            Dim objSchemaModuleField As List(Of NawaDAL.ModuleField) = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(12226)
            Dim objSchemaModuleFieldDefault As New List(Of NawaDAL.ModuleFieldDefault)
            Dim objmdl As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(12226)

            'Dim cif As String = sar_CIF.SelectedItem.Value '' Edit 27-Dec-2022, Felix. Ganti Dropdown jd search Window.
            Dim cif As String = txt_CIF.Value
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate

            Dim objList = NawaDevBLL.GenerateSarBLL.getListTransactionBySearch(cif, datefrom, dateto)



            objFormModuleView = New FormModuleView(GridPaneldetail, BtnExport)
            objFormModuleView.ModuleID = objmdl.PK_Module_ID
            objFormModuleView.ModuleName = objmdl.ModuleName

            Dim delimitercheckboxparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(39)
            Dim strdelimiter As String = ";"
            If Not delimitercheckboxparam Is Nothing Then
                strdelimiter = delimitercheckboxparam.SettingValue
            End If

            Dim objtb As DataTable = NawaBLL.Common.CopyGenericToDataTable(ListTransaction)
            Dim intmaxrow As Integer
            If objtb.Rows.Count + 1000 > 1048575 Then
                intmaxrow = 1048575
            Else
                intmaxrow = objtb.Rows.Count + 1000
            End If


            Using objtbl As Data.DataTable = objtb
                'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                objtbl.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                Dim intTotalPage As Integer = (intmaxrow + 1048575 - 1) / 1048575
                Dim dsTable As New Data.DataSet

                If intTotalPage = 1 Then
                    'objFormModuleView.changeHeader(objtbl)

                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    DataSetToExcel(dsTable, objfileinfo)

                    dsTable.Dispose()
                    dsTable = Nothing



                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If



                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")



                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()
                ElseIf intTotalPage > 1 Then
                    'objFormModuleView.changeHeader(objtbl)



                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    'ambil page 2 dst sampe page terakhir
                    For index = 1 To intTotalPage - 2
                        Using objtbltemp As Data.DataTable = objtb
                            objtbltemp.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                            dsTable.Tables.Add(objtbltemp.Copy)
                        End Using
                    Next
                    DataSetToExcel(dsTable, objfileinfo)
                    dsTable.Dispose()
                    dsTable = Nothing

                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If
                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()

                End If
            End Using

        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub
    Protected Sub CreateExcel2007WithDataNoChecked(sender As Object, e As DirectEventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing

        Try


            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
            'Dim intTotalRow As Long
            Dim objSchemaModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(12226)
            Dim objSchemaModuleField As List(Of NawaDAL.ModuleField) = NawaBLL.ModuleBLL.GetModuleFieldByModuleID(12226)
            Dim objSchemaModuleFieldDefault As New List(Of NawaDAL.ModuleFieldDefault)
            Dim objmdl As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(12226)

            'Dim cif As String = sar_CIF.SelectedItem.Value '' Edit 27-Dec-2022, Felix. Ganti Dropdown jd search Window.
            Dim cif As String = txt_CIF.Value
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate

            Dim objList = NawaDevBLL.GenerateSarBLL.getListTransactionBySearch(cif, datefrom, dateto)



            objFormModuleView = New FormModuleView(GridPanel1, BtnExportNoChecked)
            objFormModuleView.ModuleID = objmdl.PK_Module_ID
            objFormModuleView.ModuleName = objmdl.ModuleName

            Dim delimitercheckboxparam As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(39)
            Dim strdelimiter As String = ";"
            If Not delimitercheckboxparam Is Nothing Then
                strdelimiter = delimitercheckboxparam.SettingValue
            End If

            Dim objtb As DataTable = NawaBLL.Common.CopyGenericToDataTable(ListTransaction)
            Dim intmaxrow As Integer
            If objtb.Rows.Count + 1000 > 1048575 Then
                intmaxrow = 1048575
            Else
                intmaxrow = objtb.Rows.Count + 1000
            End If


            Using objtbl As Data.DataTable = objtb
                'Using objtbl As Data.DataTable = objFormModuleView.getDataPagingNew(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                objtbl.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & "1"
                Dim intTotalPage As Integer = (intmaxrow + 1048575 - 1) / 1048575
                Dim dsTable As New Data.DataSet

                If intTotalPage = 1 Then
                    'objFormModuleView.changeHeader(objtbl)

                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns

                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If

                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    DataSetToExcel(dsTable, objfileinfo)

                    dsTable.Dispose()
                    dsTable = Nothing



                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If



                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".xlsx")



                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()
                ElseIf intTotalPage > 1 Then
                    'objFormModuleView.changeHeader(objtbl)



                    If objtbl.Columns.Contains("pk_moduleapproval_id") Then
                        objtbl.Columns.Remove("pk_moduleapproval_id")
                    End If
                    For Each item As Ext.Net.ColumnBase In GridPaneldetail.ColumnModel.Columns
                        If item.Hidden Then
                            If objtbl.Columns.Contains(item.DataIndex) Then
                                objtbl.Columns.Remove(item.DataIndex)
                            End If
                        End If
                    Next
                    dsTable.Tables.Add(objtbl.Copy)
                    'ambil page 2 dst sampe page terakhir
                    For index = 1 To intTotalPage - 2
                        Using objtbltemp As Data.DataTable = objtb
                            objtbltemp.TableName = objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", "") & index + 1
                            dsTable.Tables.Add(objtbltemp.Copy)
                        End Using
                    Next
                    DataSetToExcel(dsTable, objfileinfo)
                    dsTable.Dispose()
                    dsTable = Nothing

                    Dim strfilezipexcel As String = objFormModuleView.ZipExcelFile(objfileinfo.FullName, objSchemaModule.ModuleLabel.Replace(" ", "").Replace("/", ""))
                    If IO.File.Exists(strfilezipexcel) Then
                        objfileinfo = New IO.FileInfo(strfilezipexcel)
                    End If
                    Response.Clear()
                    Response.ClearHeaders()
                    ' Response.ContentType = System.Web.MimeMapping.GetMimeMapping(objFormModuleView.objSchemaModule.ModuleLabel.Replace(" ", "") & ".xlsx")
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & objSchemaModule.ModuleLabel.Replace(" ", "") & ".zip")
                    Response.Charset = ""
                    Response.AddHeader("cache-control", "max-age=0")
                    Me.EnableViewState = False
                    Response.BinaryWrite(IO.File.ReadAllBytes(strfilezipexcel))
                    Response.End()

                End If
            End Using

        Catch ex As Exception
            If Not objfileinfo Is Nothing Then
                If File.Exists(objfileinfo.FullName) Then
                    File.Delete(objfileinfo.FullName)
                End If
            End If
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub
    Private Shared Sub DataSetToExcel(ByVal dataSet As DataSet, ByVal objfileinfo As FileInfo)
        Using pck As ExcelPackage = New ExcelPackage()

            For Each dataTable As DataTable In dataSet.Tables
                Dim workSheet As ExcelWorksheet = pck.Workbook.Worksheets.Add(dataTable.TableName)
                workSheet.Cells("A1").LoadFromDataTable(dataTable, True)

                Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                Dim intcolnumber As Integer = 1
                For Each item As System.Data.DataColumn In dataTable.Columns
                    If item.DataType = GetType(Date) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = dateformat
                    End If
                    If item.DataType = GetType(Integer) Or item.DataType = GetType(Double) Or item.DataType = GetType(Long) Or item.DataType = GetType(Decimal) Or item.DataType = GetType(Single) Then
                        workSheet.Column(intcolnumber).Style.Numberformat.Format = "#,##0.00"
                    End If

                    intcolnumber = intcolnumber + 1
                Next
                workSheet.Cells(workSheet.Dimension.Address).AutoFitColumns()
            Next

            pck.SaveAs(objfileinfo)
        End Using
    End Sub

    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

            'strfilter = strfilter.Replace("Active", objFormModuleView.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter

            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            End If

            Me.strOrder = strsort
            If strsort = "" Then
                strsort = "NO_ID asc"
            End If

            Dim DataPaging As DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)

            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If

            e.Total = inttotalRecord
            GridPaneldetail.GetStore.DataSource = DataPaging
            GridPaneldetail.GetStore.DataBind()


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Direct Event"
    Protected Sub OnSelect_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim data As New goAML_ODM_Transaksi
            Dim idx As String
            idx = e.ExtraParams(0).Value

            data.NO_ID = e.ExtraParams(0).Value

            ListSelectedTransaction.Add(data)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub OnDeSelect_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim data As New goAML_ODM_Transaksi

            For Each item In ListSelectedTransaction
                If item.NO_ID = CInt(e.ExtraParams(0).Value) Then
                    data = item
                    Exit For
                End If
            Next

            ListSelectedTransaction.Remove(data)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnGenerate_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            '' Add 27-Dec-2022
            If sar_IsSelectedAll.Value Then
                ListSelectedTransaction = New List(Of NawaDevDAL.goAML_ODM_Transaksi)
                ListSelectedTransaction = ListTransaction
            End If
            '' End 27-Dec-2022

            Dim ListSar As New List(Of goAML_ODM_Generate_STR_SAR)
            Dim ListODMTransactionIncomplete As New List(Of goAML_ODM_Transaksi)
            Dim ListTransactionIncomplete As New List(Of goAML_ODM_Generate_STR_SAR_Transaction)
            Dim getIsGenerateSAR As String = NawaDevBLL.GenerateSarBLL.getIsGenerateSAR("IsGenerateSAR")
            Dim ListTransactionNoTrnMode As New List(Of goAML_ODM_Generate_STR_SAR_Transaction)
            Dim ListODMTransactionNoTrnMode As New List(Of goAML_ODM_Transaksi)
            Dim dateReport As Date = TanggalLaporan.SelectedDate
            Dim maxDate As DateTime = DateTime.Now

            'If sar_jenisLaporan.Value = "LTKMP" And NoRefPPATK.Text = "" Then
            'Update Product untuk No ref PPATK/Fiu_Ref_Number
            If checkjenislaporan() = True And NoRefPPATK.Text = "" Then
                Throw New ApplicationException("Jika Jenis Laporannya LTKMP maka No Ref PPATK Tidak Boleh Kosong")
            ElseIf dateReport <> DateTime.MinValue AndAlso dateReport > maxDate Then
                Throw New Exception("Tanngal Laporan : Can not select future date")
            End If
            If ListIndicator.Count = 0 Then
                Throw New ApplicationException("Indikator Tidak Boleh Kosong")
            End If




            Dim GenerateSAR As New goAML_ODM_Generate_STR_SAR
            With GenerateSAR
                '.CIF_NO = sar_CIF.SelectedItem.Value '' Edit 27-Dec-2022, Felix. Ganti Dropdown jd search Window.
                .CIF_NO = txt_CIF.Value
                .Transaction_Code = sar_jenisLaporan.SelectedItem.Value
                .Date_Report = TanggalLaporan.Value
                .Fiu_Ref_Number = NoRefPPATK.Text
                .Reason = alasan.Text
                .total_transaction = ListSelectedTransaction.Count
                .Fk_Report_ID = 0
                .status = 1
                If ListTransaction.Count = ListSelectedTransaction.Count Or sar_IsSelectedAll.Value Then
                    .IsSelectedAll = True
                Else
                    .IsSelectedAll = False
                End If

                If ListIndicator.Count > 0 Then
                    Dim i As Integer
                    Dim temp As String
                    i = 0
                    temp = ""
                    For Each item In ListIndicator
                        i += 1
                        If ListIndicator.Count = 1 Then
                            'temp = getReportIndicatorByKode(item.FK_Indicator)
                            temp = item.FK_Indicator
                        Else
                            If i = 1 Then
                                'temp = getReportIndicatorByKode(item.FK_Indicator)
                                temp = item.FK_Indicator
                            Else
                                'temp += "," + getReportIndicatorByKode(item.FK_Indicator)
                                temp += "," + item.FK_Indicator
                            End If
                        End If
                    Next
                    .indicator = temp
                End If
            End With

            ObjSARData.objGenerateSAR = GenerateSAR


            ListSar = NawaDevBLL.GenerateSarBLL.getListSar(GenerateSAR.CIF_NO, GenerateSAR.Transaction_Code, GenerateSAR.Date_Report)
            If ListSar.Count > 0 Then
                Throw New ApplicationException("Laporan Dengan CIF: " & GenerateSAR.CIF_NO & ", Jenis Laporan: " & GenerateSAR.Transaction_Code & " dan Tanggal Laporan: " & GenerateSAR.Date_Report.Value.ToString("dd-MMM-yyyy") & " Sudah Ada.")
            End If

            For Each item In ListDokumen
                ObjSARData.listObjDokumen.Add(item)
            Next

            If GenerateSAR.IsSelectedAll = False Then
                If ListSelectedTransaction.Count = 0 Then
                    Throw New ApplicationException("Tidak Ada Transaksi yang dipilih")
                End If

                Dim objODMTransaksi As New goAML_ODM_Transaksi
                Dim objSARTransaksi As New goAML_ODM_Generate_STR_SAR_Transaction

                ObjSARData.listObjGenerateSARTransaction = New List(Of goAML_ODM_Generate_STR_SAR_Transaction)

                For Each item In ListSelectedTransaction
                    objODMTransaksi = ListTransaction.Where(Function(x) x.NO_ID = item.NO_ID).FirstOrDefault

                    With objSARTransaksi
                        .Date_Transaction = objODMTransaksi.Date_Transaction
                        .CIF_NO = objODMTransaksi.CIF_NO
                        .Account_NO = objODMTransaksi.Account_NO
                        .WIC_NO = objODMTransaksi.WIC_No
                        .Ref_Num = objODMTransaksi.Ref_Num
                        .Debit_Credit = objODMTransaksi.Debit_Credit
                        .Original_Amount = objODMTransaksi.Original_Amount
                        .Currency = objODMTransaksi.Currency
                        .Exchange_Rate = objODMTransaksi.Exchange_Rate
                        .IDR_Amount = objODMTransaksi.IDR_Amount
                        .Transaction_Code = objODMTransaksi.Transaction_Code
                        .Source_Data = objODMTransaksi.Source_Data
                        .Transaction_Remark = objODMTransaksi.Transaction_Remark
                        .Transaction_Number = objODMTransaksi.Transaction_Number
                        .Transaction_Location = objODMTransaksi.Transaction_Location
                        .Teller = objODMTransaksi.Teller
                        .Authorized = objODMTransaksi.Authorized
                        .Date_Posting = objODMTransaksi.Date_Posting
                        .Transmode_Code = objODMTransaksi.Transmode_Code
                        .Transmode_Comment = objODMTransaksi.Transmode_Comment
                        .Comments = objODMTransaksi.Comments
                        .CIF_No_Lawan = objODMTransaksi.CIF_No_Lawan
                        .ACCOUNT_No_Lawan = objODMTransaksi.ACCOUNT_No_Lawan
                        .WIC_No_Lawan = objODMTransaksi.WIC_No_Lawan
                        .Conductor_ID = objODMTransaksi.Conductor_ID
                        .Swift_Code_Lawan = objODMTransaksi.Swift_Code_Lawan
                        .country_code_lawan = objODMTransaksi.Country_Code_Lawan
                        .MsgTypeSwift = objODMTransaksi.MsgSwiftType
                        .From_Funds_Code = objODMTransaksi.From_Funds_Code
                        .To_Funds_Code = objODMTransaksi.To_Funds_Code
                        .country_code = objODMTransaksi.Country_Code
                        .Currency_Lawan = objODMTransaksi.Currency_Lawan
                        If objODMTransaksi.BiMultiParty.HasValue Then
                            .BiMultiParty = objODMTransaksi.BiMultiParty
                        End If
                        .GCN = objODMTransaksi.GCN
                        .GCN_Lawan = objODMTransaksi.GCN_Lawan
                        .Active = 1
                        .Business_date = objODMTransaksi.Business_date
                        If objODMTransaksi.From_Type.HasValue Then
                            .From_Type = objODMTransaksi.From_Type
                        End If
                        If objODMTransaksi.To_Type.HasValue Then
                            .To_Type = objODMTransaksi.To_Type
                        End If
                    End With

                    ObjSARData.listObjGenerateSARTransaction.Add(objSARTransaksi)

                    objODMTransaksi = New goAML_ODM_Transaksi
                    objSARTransaksi = New goAML_ODM_Generate_STR_SAR_Transaction
                Next

                If getIsGenerateSAR = "1" Then
                    ListTransactionIncomplete = ObjSARData.listObjGenerateSARTransaction.Where(Function(x) x.BiMultiParty = 1 And x.CIF_No_Lawan Is Nothing And x.WIC_No_Lawan Is Nothing And x.ACCOUNT_No_Lawan Is Nothing).ToList
                    If ListTransactionIncomplete.Count > 0 Then
                        Throw New ApplicationException("Any Counter Party of Transaction is not completed")
                    End If
                End If

                ListTransactionNoTrnMode = ObjSARData.listObjGenerateSARTransaction.Where(Function(x) x.Transmode_Code = "" Or x.Transmode_Code Is Nothing).ToList
                If ListTransactionNoTrnMode.Count > 0 Then
                    Throw New ApplicationException("Cara Tranaksi Dilakukan Kosong Di " + ListTransactionNoTrnMode.Count.ToString + " Transaksi")
                End If

            ElseIf GenerateSAR.IsSelectedAll = True Then
                If ListTransaction.Count = 0 Then
                    Throw New ApplicationException("Tidak Ada Transaksi")
                End If
                If getIsGenerateSAR = "1" Then
                    ListODMTransactionIncomplete = ListTransaction.Where(Function(x) x.BiMultiParty = 1 And x.CIF_No_Lawan Is Nothing And x.WIC_No_Lawan Is Nothing And x.ACCOUNT_No_Lawan Is Nothing).ToList
                    If ListODMTransactionIncomplete.Count > 0 Then
                        Throw New ApplicationException("Any Counter Party of Transaction is not completed")
                    End If
                End If

                ListODMTransactionNoTrnMode = ListTransaction.Where(Function(x) x.Transmode_Code = "" Or x.Transmode_Code Is Nothing).ToList
                If ListODMTransactionNoTrnMode.Count > 0 Then
                    Throw New ApplicationException("Cara Tranaksi Dilakukan Kosong Di " + ListODMTransactionNoTrnMode.Count.ToString + " Transaksi")
                End If
            End If

            If Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                'Dim dirPath As String = Server.MapPath(Request.ServerVariables("PATH_INFO")).Substring(0, Server.MapPath(Request.ServerVariables("PATH_INFO")).LastIndexOf("\") + 1) & "DikumenSAR\"
                'Dim dirPath As String = Server.MapPath(Request.ServerVariables("PATH_INFO")).Substring(0, Server.MapPath(Request.ServerVariables("PATH_INFO")).LastIndexOf("\") + 1) & "FolderSAR\"
                'Dim dirPathTemplate As String = Server.MapPath(Request.ServerVariables("PATH_INFO")).Substring(0, Server.MapPath(Request.ServerVariables("PATH_INFO")).LastIndexOf("\") + 1) & "FolderTemplate\"
                'Dim FileReturn As String = ""
                'dirPath = dirPath & Common.SessionCurrentUser.UserID & "\"
                'If Not Directory.Exists(dirPath) Then
                '    Directory.CreateDirectory(dirPath)
                'End If
                'Dim path As String = ConfigurationManager.AppSettings("FilePathGenerateSARAttachment").ToString
                'Dim path As String = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(9018).SettingValue
                'NawaDevBLL.GenerateSarBLL.SaveGenerateSarTanpaApproval(ObjSARData, ObjModule, 1, path)



                Dim DateFrom As DateTime = sar_DateFrom.Value
                Dim DateTo As DateTime = sar_DateTo.Value
                NawaDevBLL.GenerateSarBLL.SaveGenerateSTRSarTanpaApproval(ObjSARData, ObjModule, 1, DateFrom, DateTo)
                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Saved into Database."
            Else
                If ObjSARData.objGenerateSAR.IsSelectedAll Then
                    Dim SarTransaction As New goAML_ODM_Generate_STR_SAR_Transaction
                    SarTransaction.Date_Transaction = sar_DateFrom.Value
                    SarTransaction.Date_Posting = sar_DateTo.Value

                    ObjSARData.listObjGenerateSARTransaction.Add(SarTransaction)
                End If
                ObjSARData.objGenerateSAR.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                ObjSARData.objGenerateSAR.CreatedDate = DateTime.Now

                'NawaDevBLL.GenerateSarBLL.SaveGenerateSarApproval(ObjSARData, ObjModule, 1)

                '5-Mei-2021 Adi : Test pindah save with Approval ke local Function dulu (agar tidak mengubah NawaDevBLL)
                NawaDevBLL.GenerateSarBLL.SaveGenerateSTRSarApproval(ObjSARData, ObjModule, 1)
                'Me.SaveGenerateSTRSarApproval(ObjSARData, ObjModule, 1)

                Panelconfirmation.Hidden = False
                FormPanelInput.Hidden = True
                LblConfirmation.Text = "Data Saved into Pending Approval"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnSearch_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            'Dim cif As String = sar_CIF.SelectedItem.Value '' Edit 27-Dec-2022, Felix. Ganti Dropdown jd search Window.
            Dim cif As String = txt_CIF.Value
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate
            Dim maxDate As DateTime = DateTime.Now
            'Update 20210426 Adi Y : Pindah buttons ke Form Panel
            If cif Is Nothing Then
                Throw New ApplicationException("CIF No Tidak boleh kosong")
            ElseIf datefrom <> DateTime.MinValue AndAlso datefrom > maxDate Then
                Throw New Exception("Date From : Can not select future date")
            ElseIf dateto <> DateTime.MinValue AndAlso dateto > maxDate Then
                Throw New Exception("Date To : Can not select future date")
            ElseIf sar_DateFrom.Text = "1/1/0001 12:00:00 AM" Then
                Throw New ApplicationException("Date From : Tidak Boleh Kosong")
            ElseIf sar_DateTo.Text = "1/1/0001 12:00:00 AM" Then
                Throw New ApplicationException("Date To : Tidak Boleh Kosong")
            End If
            'Update 20210426 Adi Y : Pindah buttons ke Form Panel
            ListTransaction = NawaDevBLL.GenerateSarBLL.getListTransactionBySearch(cif, datefrom, dateto)

            'If ListTransaction.Count > 0 Then
            'bindTransaction(StoreTransaction, ListTransaction)
            'bindTransaction(StoreTransaksiNoChecked, ListTransaction)
            'End If
            '' Add 27-Dec-2022, Felix. Thanks to Try
            StoreTransaction.Reload()
            StoreTransaksiNoChecked.Reload()
            '' End 27-Dec-2022, Felix. Thanks to Try

            If ListTransaction.Count = 0 Then
                Ext.Net.X.Msg.Alert("Message", "Tidak Ada Transaksi").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub btn_addIndicator_click(sender As Object, e As DirectEventArgs)
        Try
            sar_reportIndicator.Clear()
            WindowReportIndicator.Hidden = False
            sar_reportIndicator.Selectable = True
            BtnsaveReportIndicator.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btn_addAttachment_click(sender As Object, e As DirectEventArgs)
        Try
            FileDoc.Reset()
            txtFileName.Clear()
            txtKeterangan.Clear()
            WindowAttachment.Hidden = False
            FileDoc.Hidden = False
            BtnsaveAttachment.Hidden = False
            txtKeterangan.Editable = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandReportIndicator(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = ID).FirstOrDefault)
                bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                editIndicator(ID, "Edit")
                IDIndicator = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editIndicator(ID, "Detail")
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandAttachment(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                ListDokumen.Remove(ListDokumen.Where(Function(x) x.PK_ID = ID).FirstOrDefault)
                bindAttachment(StoreAttachment, ListDokumen)
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                editDokumen(ID, "Edit")
                IDDokumen = ID
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                editDokumen(ID, "Detail")
            ElseIf e.ExtraParams(1).Value = "Download" Then
                DownloadFile(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveReportIndicator_Click(sender As Object, e As DirectEventArgs)
        Try
            If String.IsNullOrEmpty(sar_reportIndicator.SelectedItem.Value) Then
                Throw New ApplicationException(sar_reportIndicator.FieldLabel + " Tidak boleh kosong")
            End If

            For Each item In ListIndicator
                If item.FK_Indicator = sar_reportIndicator.SelectedItem.Value Then
                    Throw New ApplicationException(sar_reportIndicator.SelectedItem.Value + " Sudah Ada")
                End If
            Next

            Dim reportIndicator As New NawaDevDAL.goAML_Report_Indicator
            If IDIndicator IsNot Nothing Or IDIndicator <> "" Then
                ListIndicator.Remove(ListIndicator.Where(Function(x) x.PK_Report_Indicator = IDIndicator).FirstOrDefault)
            End If
            If ListIndicator.Count = 0 Then
                reportIndicator.PK_Report_Indicator = -1
            ElseIf ListIndicator.Count >= 1 Then
                reportIndicator.PK_Report_Indicator = ListIndicator.Min(Function(x) x.PK_Report_Indicator) - 1
            End If

            reportIndicator.FK_Indicator = sar_reportIndicator.SelectedItem.Value
            ListIndicator.Add(reportIndicator)
            bindReportIndicator(StoreReportIndicatorData, ListIndicator)
            WindowReportIndicator.Hidden = True
            IDIndicator = Nothing
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelreportIndicator_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowReportIndicator.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnSaveAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim status As Boolean = False
            Dim DokumenOld As New NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment
            Dim Dokumen As New NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment
            If IDDokumen IsNot Nothing Or IDDokumen <> "" Then
                status = True
                DokumenOld = ListDokumen.Where(Function(x) x.PK_ID = IDDokumen).FirstOrDefault
                ListDokumen.Remove(ListDokumen.Where(Function(x) x.PK_ID = IDDokumen).FirstOrDefault)
            ElseIf Not FileDoc.HasFile Then
                Throw New Exception("Please Upload File")
            End If

            Dim docName As String = IO.Path.GetFileName(FileDoc.FileName)
            For Each item In ListDokumen
                If item.File_Name = docName Then
                    Throw New ApplicationException(docName + " Sudah Ada")
                End If
            Next

            If ListDokumen.Count = 0 Then
                Dokumen.PK_ID = -1
            ElseIf ListDokumen.Count >= 1 Then
                Dokumen.PK_ID = ListDokumen.Min(Function(x) x.PK_ID) - 1
            End If

            If status = True And Not FileDoc.HasFile Then
                Dokumen.File_Name = DokumenOld.File_Name
                Dokumen.File_Doc = DokumenOld.File_Doc
            Else
                Dokumen.File_Name = IO.Path.GetFileName(FileDoc.FileName)
                Dokumen.File_Doc = FileDoc.FileBytes
            End If
            Dokumen.Remarks = txtKeterangan.Text.Trim

            Dokumen.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            Dokumen.CreatedDate = DateTime.Now

            ListDokumen.Add(Dokumen)

            bindAttachment(StoreAttachment, ListDokumen)
            WindowAttachment.Hidden = True
            IDDokumen = Nothing

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancelAttachment_Click(sender As Object, e As DirectEventArgs)
        Try
            WindowAttachment.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub sar_jenisLaporan_DirectSelect(sender As Object, e As DirectEventArgs)
        Try
            'If sar_jenisLaporan.SelectedItem.Value = "LTKMT" Or sar_jenisLaporan.SelectedItem.Value = "LAPT" Or sar_jenisLaporan.SelectedItem.Value = "LTKM" Then
            If sar_jenisLaporan.SelectedItem.Value IsNot Nothing Then
                PanelReportIndicator.Hidden = False
            Else
                PanelReportIndicator.Hidden = True
            End If

            'Update Product untuk No ref PPATK/Fiu_Ref_Number
            'If sar_jenisLaporan.SelectedItem.Value = "LTKMP" Then
            If checkjenislaporan() = True Then
                NoRefPPATK.Hidden = False
            Else
                NoRefPPATK.Hidden = True
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Protected Sub sar_DateFrom_DirectSelect(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim dateFrom As DateTime = sar_DateFrom.Value
    '        Dim maxDate As DateTime = DateTime.Now

    '        If dateFrom <> DateTime.MinValue AndAlso dateFrom > maxDate Then
    '            Throw New Exception("Date From : Can not select future date")
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    'Protected Sub sar_DateTo_DirectSelect(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim dateTo As DateTime = sar_DateTo.Value
    '        Dim maxDate As DateTime = DateTime.Now

    '        If dateTo <> DateTime.MinValue AndAlso dateTo > maxDate Then
    '            Throw New Exception("Date To : Can not select future date")
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub sar_IsSelectedAll_CheckedChanged(sender As Object, e As EventArgs) Handles sar_IsSelectedAll.DirectCheck
        If sar_IsSelectedAll.Value Then
            PanelChecked.Hide()
            PanelnoCheked.Show()
        Else
            PanelChecked.Show()
            PanelnoCheked.Hide()
        End If
    End Sub

    'Protected Sub sar_IsSelectedAll_CheckedChanged(sender As Object, e As EventArgs) Handles sar_IsSelectedAll.DirectCheck
    '    If sar_IsSelectedAll.Value Then
    '        'pake yg ini
    '        'GridPaneldetail.Selectable = False

    '        'CheckboxSelectionModel1.SelectAll()
    '        'GridPaneldetail.SelectionModel.Clear() 
    '        'CheckboxSelectionModel1.SetLocked(True)
    '        CheckboxSelectionModel1.Visible = False
    '        'Dim selmodel = New CheckboxSelectionModel()

    '        'With selmodel
    '        '    .CheckOnly = True
    '        '    .SelectAll()
    '        'End With
    '        'GridPaneldetail.SelectionModel.Add(selmodel)
    '    Else
    '        'GridPaneldetail.DisableSelection = False
    '        CheckboxSelectionModel1.Visible = True
    '        'CheckboxSelectionModel1.SetLocked(False)
    '        'CheckboxSelectionModel1.DeselectAll()
    '    End If

    'End Sub

#End Region

#Region "5-Mei-2021 Adi : Test save tanpa audit trail"
    Sub SaveGenerateSTRSarApproval(GenerateSAR As GenerateSARData, objmodule As NawaDAL.Module, action As Integer)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            'Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
            Try
                    '5-Mei-2021 Adi : Insert ke EODLogSP untuk ukur performance
                    Dim strQuery As String = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Generate STR SAR', 'Save Audit Trail Started')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim actsInsert As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim actsUpdate As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim actsDelete As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim modulename As String = objmodule.ModuleName
                    Dim objModuleApproval As New NawaDevDAL.ModuleApproval
                    Dim xmldata As String = NawaBLL.Common.Serialize(GenerateSAR)

                    Dim alternateby As String = ""
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    If action = 1 Then ' insert
                        With objModuleApproval
                            .ModuleName = objmodule.ModuleName
                            .ModuleKey = GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR
                            .ModuleField = xmldata
                            .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                            .CreatedDate = Now
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        End With

                        Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, actsInsert, modulename)

                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, GenerateSAR.objGenerateSAR)

                        'For Each item In GenerateSAR.listObjDokumen
                        '    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, item)
                        'Next

                        'Audit Trail using SP
                        Dim param(3) As SqlParameter
                        param(0) = New SqlParameter
                        param(0).ParameterName = "@PK_goAML_ODM_Generate_STR_SAR"
                        param(0).Value = CType(GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR, Int64)
                        param(0).DbType = SqlDbType.BigInt

                        Dim dtGenerateSAR As DataTable = NawaBLL.Common.CopyGenericToDataTable(GenerateSAR.listObjGenerateSARTransaction)
                        param(1) = New SqlParameter
                        param(1).ParameterName = "@TMP_udt_goAML_ODM_Generate_STR_SAR_Transaction"
                        param(1).Value = dtGenerateSAR
                        param(1).SqlDbType = SqlDbType.Structured
                        param(1).TypeName = "dbo.udt_goAML_ODM_Generate_STR_SAR_Transaction"

                        param(2) = New SqlParameter
                        param(2).ParameterName = "@pkaudittrailheader"
                        param(2).Value = objaudittrailheader.PK_AuditTrail_ID
                        param(2).SqlDbType = SqlDbType.BigInt

                        param(3) = New SqlParameter
                        param(3).ParameterName = "@action"
                        param(3).Value = 1
                        param(3).SqlDbType = SqlDbType.Int

                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AuditTrailDetail_GenerateSAR", param)

                    ElseIf action = 2 Then ' update
                        Dim ObjGenerateSARBefore As New GenerateSARData
                        ObjGenerateSARBefore.objGenerateSAR = objdb.goAML_ODM_Generate_STR_SAR.Where(Function(x) x.PK_goAML_ODM_Generate_STR_SAR = GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR).FirstOrDefault
                        ObjGenerateSARBefore.listObjGenerateSARTransaction = objdb.goAML_ODM_Generate_STR_SAR_Transaction.Where(Function(x) x.Fk_goAML_Generate_STR_SAR = GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR).ToList
                        ObjGenerateSARBefore.listObjDokumen = objdb.goAML_ODM_Generate_STR_SAR_Attachment.Where(Function(x) x.Fk_goAML_ODM_Generate_STR_SAR = GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR).ToList
                        Dim xmldataOld As String = Common.Serialize(ObjGenerateSARBefore)

                        With objModuleApproval
                            .ModuleName = objmodule.ModuleName
                            .ModuleKey = GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR
                            .ModuleField = xmldata
                            .ModuleFieldBefore = xmldataOld
                            .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                            .CreatedDate = Now
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        End With

                        Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, actsUpdate, modulename)

                        NawaFramework.CreateAuditTrailDetailEdit(objdb, objaudittrailheader.PK_AuditTrail_ID, GenerateSAR.objGenerateSAR, ObjGenerateSARBefore.objGenerateSAR)

                        Dim objDokumenOld As New goAML_ODM_Generate_STR_SAR_Attachment
                        For Each item In GenerateSAR.listObjDokumen
                            objDokumenOld = objdb.goAML_ODM_Generate_STR_SAR_Attachment.Where(Function(x) x.Fk_goAML_ODM_Generate_STR_SAR = GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR).FirstOrDefault

                            NawaFramework.CreateAuditTrailDetailEdit(objdb, objaudittrailheader.PK_AuditTrail_ID, item, objDokumenOld)
                        Next

                    ElseIf action = 3 Then ' Delete
                        With objModuleApproval
                            .ModuleName = objmodule.ModuleName
                            .ModuleKey = GenerateSAR.objGenerateSAR.PK_goAML_ODM_Generate_STR_SAR
                            .ModuleField = xmldata
                            .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                            .CreatedDate = Now
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        End With

                        Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, actsDelete, modulename)

                        NawaFramework.CreateAuditTrailDetailDelete(objdb, objaudittrailheader.PK_AuditTrail_ID, GenerateSAR.objGenerateSAR)

                        For Each item In GenerateSAR.listObjDokumen
                            NawaFramework.CreateAuditTrailDetailDelete(objdb, objaudittrailheader.PK_AuditTrail_ID, item)
                        Next

                    End If

                    '5-Mei-2021 Adi : Insert ke EODLogSP untuk ukur performance
                    strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Generate STR SAR', 'Save Audit Trail Finished')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    '5-Mei-2021 Adi : Insert ke EODLogSP untuk ukur performance
                    strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Generate STR SAR', 'Save to Module Approval Started')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    objdb.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objdb.SaveChanges()

                    '5-Mei-2021 Adi : Insert ke EODLogSP untuk ukur performance
                    strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Generate STR SAR', 'Save to Module Approval Finished')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                'objtrans.Commit()
            Catch ex As Exception
                'objtrans.Rollback()
                Throw ex
                End Try
            'End Using
        End Using
    End Sub
#End Region

#Region "27-Dec-2022 Felix, Thanks to Try. Ubah load ke grid nya pakai StoreReadData"
    'Update Product untuk No ref PPATK/Fiu_Ref_Number
    Function checkjenislaporan() As Boolean
        Try
            Using objdb As New NawaDatadevEntities
                Dim jenisLaporan As String = sar_jenisLaporan.Value
                If objdb.GoAML_Laporan_Req_PPATK.Where(Function(x) x.Kode = jenisLaporan And x.Active = True).FirstOrDefault IsNot Nothing Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            Throw New ApplicationException("There is an error occurred on checkjenislaporan() function")
        End Try
    End Function

    Protected Sub Store_ReadDataTrans(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)
            Dim strsort As String = ""
            'Dim CifAndNameIndvOrCorp As String = sar_CIF.SelectedItem.Text
            'Dim CustomerID As String = getCIF(CifAndNameIndvOrCorp)
            Dim CustomerID As String = txt_CIF.Value
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter

            If String.IsNullOrEmpty(strWhereClause) Then
                strWhereClause = " CIF_NO = '" & CustomerID & "' AND cast(Date_Transaction as Date) >= cast('" & datefrom & "' as Date) AND cast(Date_Transaction as Date) < cast('" & dateto & "' as date)"
            Else
                strWhereClause += " AND CIF_NO = '" & CustomerID & "' AND cast(Date_Transaction as Date) >= cast('" & datefrom & "' as Date) AND cast(Date_Transaction as Date) < cast('" & dateto & "' as date)"

            End If

            Me.strOrder = strsort
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_strsar", "NO_ID,Valid,NoTran,NoRefTran,TipeTran,LokasiTran,KeteranganTran,DateTransaction,AccountNo,NamaTeller,NamaPejabat,TglPembukuan,CaraTranDilakukan,CaraTranLain,DebitCredit,OriginalAmount,IDR", strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            '-- start paging ----------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            '-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridPaneldetail.GetStore.DataSource = DataPaging
            GridPaneldetail.GetStore.DataBind()

            'End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadDataTransNochecked(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            Dim strsort As String = ""
            'Dim CifAndNameIndvOrCorp As String = sar_CIF.SelectedItem.Text
            'Dim CustomerID As String = getCIF(CifAndNameIndvOrCorp)
            Dim CustomerID As String = txt_CIF.Value
            Dim datefrom As Date = sar_DateFrom.SelectedDate
            Dim dateto As Date = sar_DateTo.SelectedDate
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter


            If String.IsNullOrEmpty(strWhereClause) Then
                strWhereClause = " CIF_NO = '" & CustomerID & "' AND Date_Transaction >= '" & datefrom & "' AND Date_Transaction < '" & dateto & "'"
            Else
                strWhereClause += " AND CIF_NO = '" & CustomerID & "' AND Date_Transaction >= '" & datefrom & "' AND Date_Transaction < '" & dateto & "'"
            End If


            Me.strOrder = strsort
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_strsar", "NO_ID,Valid,NoTran,NoRefTran,TipeTran,LokasiTran,KeteranganTran,DateTransaction,AccountNo,NamaTeller,NamaPejabat,TglPembukuan,CaraTranDilakukan,CaraTranLain,DebitCredit,OriginalAmount,IDR", strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            '-- start paging ----------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If

            '-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridPanel1.GetStore.DataSource = DataPaging
            GridPanel1.GetStore.DataBind()

            'End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "27-Dec-2022 Felix : Ubah combo box"
    Protected Sub Btn_Import_Customer_Click()
        Try
            window_Import_Customer.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadData_Import_Customer(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            intLimit = 10

            'strfilter = strfilter.Replace("Active", objTransactor.ModuleName & ".Active")

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            'Me.indexStart = intStart
            'Me.strWhereClause = strfilter
            'Me.strOrder = strsort
            If strsort = "" Then
                strsort = "CIFNo asc"
            End If
            'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
            Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("vw_goAML_Ref_Customer_GenerateSAR", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            store_Import_Customer.DataSource = DataPaging
            store_Import_Customer.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_Import_Customer(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim strCIFNo As String = e.ExtraParams(0).Value
            Dim strCustomerName As String = e.ExtraParams(1).Value
            txt_CIF.Value = strCIFNo
            txt_Cust_Name.Value = strCustomerName
            window_Import_Customer.Hidden = True
            btn_OpenWindowCustomer.Hidden = False
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Import_Customer_Back_Click()
        Try
            window_Import_Customer.Hidden = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class
