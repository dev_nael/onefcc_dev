﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="ReportView_IFTI.aspx.vb" Inherits="goAML_Report_ReportView_IFTI" %>



<%--<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>--%>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var getRowClass = function (record, rowIndex, rowParams, store) {
            if (record.data.Aging >= 4) {
                return "red-row-class";
            }
        }
        var columnAutoResize = function (grid) {
            App.GridpanelView.columns.forEach(function (col) {
                if (col.xtype != 'commandcolumn') {

                    col.autoSize();
                }
            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };
        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };

        var prepareCommandCollection = function (grid, toolbar, rowIndex, record) {
            var EditButton = toolbar.items.get(1);
            var DeleteBUtton = toolbar.items.get(2);
            var wf = record.data.statusID
            if (EditButton != undefined) {
                if (wf == "1" || wf == "5" || wf == "2" || wf == "9" || wf == "0") { /*Edited by Felix 24 Nov 2020, skrng Generated (2) masih bisa di edit*/
                    EditButton.setDisabled(false)
                } else {
                    EditButton.setDisabled(true)
                }
            }
            if (DeleteBUtton != undefined) {
                if (wf == "1" || wf == "5" || wf == "2" || wf == "9" || wf == "0") { /*Edited by Felix 24 Nov 2020, skrng Generated (2) masih bisa di edit*/
                    DeleteBUtton.setDisabled(false)
                } else {
                    DeleteBUtton.setDisabled(true)
                }
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:GridPanel ID="GridpanelView" ClientIDMode="Static" runat="server" Title="Title" Width="2000">
        <View>
            <ext:GridView runat="server" EnableTextSelection="true">
                <GetRowClass Fn="getRowClass" />
            </ext:GridView>
        </View>
        <Store>
            <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadData">

                <Sorters>
                    <%--<ext:DataSorter Property="" Direction="ASC" />--%>
                </Sorters>
                <Proxy>
                    <ext:PageProxy />
                </Proxy>
            </ext:Store>
        </Store>
        <Plugins>
            <%--<ext:GridFilters ID="GridFilters1" runat="server" />--%>
            <ext:FilterHeader ID="grdiheaderfilter" runat="server" Remote="true"></ext:FilterHeader>

        </Plugins>
        <BottomBar>

            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True">
                <Items>
                    <ext:Button runat="server" ID="BtnExportForImport" ClientIDMode="Static" Text="Export Selected Report" AutoPostBack="true" OnClick="ExportXmlForImport" Hidden="true"/>
                </Items>
            </ext:PagingToolbar>
        </BottomBar>
               <DockedItems>
            <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                <Items>
                    <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export :">
                        <Items>
                            <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                            <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                        </Items>
                    </ext:ComboBox>
                    <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" AutoPostBack="true" OnClick="ExportExcel" ClientIDMode="Static" />
                    <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page" AutoPostBack="true" OnClick="ExportAllExcel" />
                    <%--<ext:Button ID="Button1" runat="server" Text="Print Current Page" Icon="Printer" Handler="this.up('grid').print();" />--%>
                    <ext:Button ID="BtnAdd" runat="server" Visible="true" Text="Add New Record" Icon="Add" Handler="NawadataDirect.BtnAdd_Click()" />
                    <ext:Button ID="btn_EditSelectedReport" runat="server" Visible="true" Text="Edit Selected Report in New Tabs" Icon="Add" Handler="NawadataDirect.btn_EditSelectedReport_Click()" />  <%--Add 14-Mar-2023, Felix. Thank to Pak Adi ,untuk Edit Selected Report in New Tab--%>
                    <ext:Button ID="AdvancedFilter" runat="server" Text="Advanced Filter" Icon="Magnifier" Handler="NawadataDirect.BtnAdvancedFilter_Click()"></ext:Button>
                </Items>
            </ext:Toolbar>
             <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true" Dock="Top" Hidden="true">
                <Items>
                    <ext:HyperlinkButton ID="HyperlinkButton1" runat="server" Text="Clear Advanced Filter">
                        <DirectEvents>
                            <Click OnEvent="btnClear_Click"></Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                    <ext:Label ID="LblAdvancedFilter" runat="server" Text="">
                    </ext:Label>
                </Items>
            </ext:Toolbar>


             </DockedItems>
        <Listeners>
            <ViewReady Handler="columnAutoResize(this);
                    this.getStore().on('load', Ext.bind(columnAutoResize, null, [this]));"
                Delay="10" />
        </Listeners>
        <%--diremark karena ga dipake 15 Oct 2020--%>
        <SelectionModel>
            <ext:CheckboxSelectionModel runat="server" Mode="Multi">
            </ext:CheckboxSelectionModel>
        </SelectionModel>
        <%--diremark karena ga dipake--%>
    </ext:GridPanel>
    <%--  <ext:Panel ID="Panel1" runat="server" Hidden="true">
            <Content>
                <uc1:AdvancedFilter runat="server" ID="AdvancedFilter1" />
            </Content>
            <Items>
            </Items>
        </ext:Panel>--%>
        <ext:Panel ID="Panel1" runat="server" Hidden="true">
        <Content>
            <uc1:AdvancedFilter runat="server" ID="AdvancedFilter1" />
        </Content>
        <Items>
        </Items>
    </ext:Panel>
    <ext:Window ID="PanelShowResult" Modal="true" runat="server" Layout="AnchorLayout" Border="false" Frame="false" FrameHeader="false" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center" Hidden="true" Maximized="true">
        <Items>
            <ext:Container runat="server" Layout="HBoxLayout" MarginSpec="0 0 10 0">
                <Items>
                    <ext:Container runat="server" Flex="1" MarginSpec="0 10 0 0">
                        <Items>
                            <ext:GridPanel ID="GridPanelReportID" runat="server" Title="Selected Report ID" AutoScroll="true" EmptyText="No Report Selected">
                                <Store>
                                    <ext:Store ID="StoreListReportID" runat="server" PageSize="5">
                                        <Model>
                                            <ext:Model ID="ModelDetail" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="ListReportID" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumberPhoneDetailIndividu" Text="No" runat="server" Width="60" Align="Center"></ext:RowNumbererColumn>
                                        <ext:Column ID="ColListReportID" runat="server" DataIndex="ListReportID" Text="Report ID" Flex="1" Align="Center"></ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                        </Items>
                    </ext:Container>
                    <ext:Container runat="server" Flex="1" MarginSpec="0 0 75 0">
                        <Defaults>
                            <ext:Parameter Name="LabelWidth" Value="200" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:DisplayField runat="server" ID="lblTotalValid" FieldLabel="Total Valid" Text="A value here 1"/>
                            <ext:DisplayField runat="server" ID="lblTotalInvalid" FieldLabel="Total Invalid" Text="A value here 1"/>
                            <ext:DisplayField runat="server" ID="lblStatusUpload" FieldLabel="Status Uploaded to PPATK" Text="A value here 1"/>
                            <ext:DisplayField runat="server" ID="lblAlreadyValidateSchema" FieldLabel="Already Validate Schema" Text="A value here 1"/>
                            <ext:DisplayField runat="server" ID="lblValidateSchemaResultValid" FieldLabel="Validate Schema Result" Text="A value here 1"/>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Container>
            
            <ext:GridPanel ID="GridPanelValidation" runat="server" AutoScroll="true" EmptyText="No Error Validation Message" Title="Validation Schema">
                <Store>
                    <ext:Store ID="Store1" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="StoreValidation_ReadData">
                        <Model>
                            <ext:Model ID="Model5" runat="server">
                                <Fields>
                                    <ext:ModelField Name="UnikReference" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ErrorMessage" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                        <ext:Column ID="Column7" runat="server" DataIndex="UnikReference" Text="Unik Reference" MinWidth="130" Flex="1"></ext:Column>
                        <ext:Column ID="Column8" runat="server" DataIndex="ErrorMessage" Text="ErrorMessage" MinWidth="130" Flex="1"></ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True">
                        <Items>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
            </ext:GridPanel>
        </Items>
        <Buttons>
            <ext:Button ID="BtnDownload" runat="server" Text="Export Selected Report" Icon="Disk" >
                <DirectEvents>
                    <Click OnEvent="BtnDownloadSelected_DirectClick"  Success="NawadataDirect.Download({isUpload : true});">
                        <ExtraParams>
                            <ext:Parameter Name="command" Value="New" Mode="Value">
                            </ext:Parameter>
                        </ExtraParams>
                        <EventMask Msg="Loading..." ShowMask="true" ></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
<%--            <ext:Button ID="btnCancelDownload" runat="server" Text="Back" Icon="Cancel" Handler="NawadataDirect.BtnCancel_DirectClick()">
            </ext:Button>--%>

            <ext:Button ID="btnCancelDownload" runat="server" Text="Back" Icon="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_DirectClick">
                        <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

        </Buttons>
    </ext:Window>
</asp:Content>

