﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="ReportApprovalDetail.aspx.vb" Inherits="ReportApprovalDetail" %>

<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="NDS" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var prepareCommandCollection = function (grid, toolbar, rowIndex, record) {
            var EditButton = toolbar.items.get(0);
            var DeleteBUtton = toolbar.items.get(2);
            var wf = record.data.statusID
            if (EditButton != undefined) {
                    EditButton.setVisible(false)
            }
            if (DeleteBUtton != undefined) {
                DeleteBUtton.setVisible(false)
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelReport" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="SiPendar Profile Data">
        <DockedItems>
            <ext:Toolbar ID="ToolbarInput" ClientIDMode="Static" runat="server" EnableOverflow="true" Dock="Top" Layout="FitLayout" MaxHeight="150"  >                
                <Items>
                      <ext:InfoPanel ID="info_ValidationResult" ClientIDMode="Static" runat="server" UI="Warning" Layout="AnchorLayout" Title="Validation Result" AutoScroll="true" IconCls="#Error" MarginSpec="10 0" Border="True" Hidden="true"></ext:InfoPanel>
                </Items>
            </ext:Toolbar>                                                     
        </DockedItems>
        <Items>
            <%-- Report General Information --%>
            <ext:Panel runat="server" ID="pnl_GeneralInformation" Title="General Information" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="10">
                <Content>
                    <ext:DisplayField ID="txt_FK_Report_ID" runat="server" FieldLabel="Report ID" AnchorHorizontal="100%" />
                    <ext:DisplayField ID="txt_ReportStatus" runat="server" FieldLabel="Status" AnchorHorizontal="100%" />
                    <ext:DisplayField ID="txt_RentityID" runat="server" FieldLabel="Organization ID" AnchorHorizontal="70%" />
                    <ext:DisplayField ID="txt_RentityBranch" runat="server" FieldLabel="Reporting Office" AnchorHorizontal="70%" />
                    <NDS:NDSDropDownField ID="cmb_SubmisionCode" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Submission_Type" Label="Submission Type" AnchorHorizontal="70%" AllowBlank="false" />
                    <NDS:NDSDropDownField ID="cmb_JenisLaporan" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" OnOnValueChanged="cmb_jenisLaporan_Change" StringTable="goAML_Ref_Jenis_Laporan" Label="Report Code" AnchorHorizontal="70%" AllowBlank="false" />
                    <NDS:NDSDropDownField ID="cmb_ReportType" ValueField="PK_Ref_Report_Type" DisplayField="Description" runat="server" StringField="PK_Ref_Report_Type,Description" StringTable="vw_MappingJenisLaporanUnikID" Label="Report Unique Type" AnchorHorizontal="70%" AllowBlank="false" />

                    <ext:TextField runat="server" ID="txt_ReportUniqueValue" FieldLabel="Report Unique Value" EnforceMaxLength="True" maxlength="50"  AllowBlank="false" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" ID="txt_PrevRejectedNumber" FieldLabel="Previous Rejected Number" EnforceMaxLength="True" maxlength="255"  AllowBlank="true" AnchorHorizontal="70%" /> <!-- Add Felix 06-Oct-2023 -->
                    <ext:DisplayField ID="txt_Entity_Reference" runat="server" FieldLabel="Report Ref. Number" AnchorHorizontal="70%" />
                    
                    <ext:TextField ID="txt_FIU_Ref_Number" EnforceMaxLength="True" maxlength="255" runat="server" FieldLabel="PPATK FIU Ref. Number" AnchorHorizontal="70%" Hidden="true" />
                    <ext:DateField ID="txt_SubmissionDate" runat="server" FieldLabel="Submission Date" AnchorHorizontal="40%" Format="dd-MMM-yyyy" AllowBlank="false" />
                    <ext:DisplayField ID="txt_CurrencyLocal" runat="server" FieldLabel="Local Currency" AnchorHorizontal="70%" />
                    <ext:TextArea ID="txt_Catatan" runat="server" FieldLabel="Reason" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000" />
                    <ext:TextArea ID="txt_Action" runat="server" FieldLabel="Reporter Action" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000" />
                </Content>
            </ext:Panel>
            <%-- End of Report General Information --%>

            <%-- Reporting Office Location --%>
            <ext:Panel runat="server" ID="pnl_ReportingOffice" Title="Reporting Office Location" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="10">
                <Items>
                    <ext:DisplayField ID="txt_Rentity_AddressType" runat="server" FieldLabel="Address Type" AnchorHorizontal="100%" />
                    <ext:DisplayField ID="txt_Rentity_Address" runat="server" FieldLabel="Address" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="100"  />
                    <ext:DisplayField ID="txt_Rentity_Town" runat="server" FieldLabel="Town" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <ext:DisplayField ID="txt_Rentity_City" runat="server" FieldLabel="City" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <ext:DisplayField ID="txt_Rentity_Zip" runat="server" FieldLabel="Zip Code" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="10" />
                    <ext:DisplayField ID="txt_Rentity_Country" runat="server" FieldLabel="Country" AnchorHorizontal="100%" />
                    <ext:DisplayField ID="txt_Rentity_State" runat="server" FieldLabel="Province" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <ext:DisplayField ID="txt_Rentity_Comments" runat="server" FieldLabel="Notes" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="4000"  />
                </Items>
            </ext:Panel>
            <%-- End of Reporting Office Location --%>

            <%-- List of Transaction --%>
            <ext:Panel runat="server" ID="pnl_Transaction" Title="List of Transactions" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout">
                <Items>
                    <ext:GridPanel ID="gp_Transaction" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Transaction_Add" Text="Add Transaction" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Transaction_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Transaction" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model43" IDProperty="PK_Transaction_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Transaction_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="TransactionNumber" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Date_Transaction" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Transmode_Code" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Amount_Local" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IsValidYesNo" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column412" runat="server" DataIndex="TransactionNumber" Text="Transaction Number" MinWidth="200" Flex="1"></ext:Column>
                                <ext:DateColumn ID="Column1" runat="server" DataIndex="Date_Transaction" Text="Transaction Date" MinWidth="130" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column2" runat="server" DataIndex="Transmode_Code" Text="Transaction Method" MinWidth="150"></ext:Column>
                                <ext:NumberColumn ID="Column3" runat="server" DataIndex="Amount_Local" Text="Local Amount" MinWidth="150" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column4" runat="server" DataIndex="IsValidYesNo" Text="Valid" MinWidth="50"></ext:Column>
                                <ext:CommandColumn ID="cc_Transaction" runat="server" Text="Action" MinWidth="72">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70" Hidden="true">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70" Hidden="true">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="gc_Transaction">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Transaction_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:Panel runat="server" Layout="ColumnLayout" BodyPadding="10">
                        <Items>
                            <ext:DisplayField runat="server" ID="txt_TotalTransactionInvalid" FieldLabel="Total Invalid" ColumnWidth="0.5" />
                            <ext:DisplayField runat="server" ID="txt_TotalTransactionLocalAmount" FieldLabel="Total Transaction Local Amount" LabelWidth="220" ColumnWidth="0.5" />
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Panel>
            <%-- End of List of Transaction --%>

            <%-- List of Activity --%>
            <ext:Panel runat="server" ID="pnl_Activity" Title="List of Activities" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="0">
                <Items>
                    <ext:GridPanel ID="gp_Activity" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Activity_Add" Text="Add Activity" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Activity_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Activity" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model357" IDProperty="PK_goAML_Act_ReportPartyType_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Act_ReportPartyType_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="PartyType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="significance" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="reason" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn89" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column314" runat="server" DataIndex="PartyType" Text="Activity Subject" Flex="1"></ext:Column>
                                <ext:Column ID="Column315" runat="server" DataIndex="significance" Text="Significance" Flex="1"></ext:Column>
                                <ext:Column ID="Column316" runat="server" DataIndex="reason" Text="Reason" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Activity" runat="server" Text="Action" MinWidth="72">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70" Hidden="true">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70" Hidden="true">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_Activity">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Act_ReportPartyType_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            <%-- End of List of Activity --%>

            <%-- List of Indicator --%>
            <ext:Panel runat="server" ID="pnl_Indicator" Title="List of Indicators" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="0">
                <Items>
                    <ext:GridPanel ID="gp_Indicator" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Indicator_Add" Text="Add Indicator" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Indicator_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Indicator" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model355" IDProperty="PK_Report_Indicator">
                                        <Fields>
                                            <ext:ModelField Name="PK_Report_Indicator" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Indicator" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn88" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column312" runat="server" DataIndex="FK_Indicator" Text="Kode" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column313" runat="server" DataIndex="Keterangan" Text="Keterangan" Flex="1" CellWrap="true"></ext:Column>
                                <ext:CommandColumn ID="cc_Indicator" runat="server" Text="Action" MinWidth="72">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70" Hidden="true">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70" Hidden="true">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_Indicator">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Report_Indicator" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            <%-- End of List of Indicator --%>

            <%-- Approval History --%>
            <ext:Panel runat="server" ID="pnl_ApprovalHistory" Title="Approval History" MarginSpec="0 0 10 0" Collapsible="true" Hidden="false" Border="true" Layout="AnchorLayout" BodyPadding="0">
                <Items>
                    <ext:GridPanel ID="gp_ApprovalHistory" runat="server">
                        <Store>
                            <ext:Store ID="store_ApprovalHistory" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" IDProperty="PK_TX_IFTI_Flow_History_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_TX_IFTI_Flow_History_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="FK_Module_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UnikID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="UserID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="UserName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Notes" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CreatedDate" Type="Date"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>

                        <ColumnModel runat="server">
                            <Columns>
                                <ext:RowNumbererColumn runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="User Name" DataIndex="UserName" CellWrap="true" flex="1" />
                                <ext:Column runat="server" Text="Status" DataIndex="Status" CellWrap="true" flex="1" />
                                <ext:DateColumn runat="server" Text="Created Date" DataIndex="CreatedDate" CellWrap="true" flex="1" Format="dd-MMM-yyyy hh:mm:ss" />
                                <ext:Column runat="server" Text="Notes" DataIndex="Notes" CellWrap="true" flex="4" />
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar24" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>

                    <ext:TextArea ID="txt_Report_ApprovalNotes" runat="server" FieldLabel="Approval/Rejection Note" AnchorHorizontal="100%" AllowBlank="false" Margin="10"/>
                </Items>
            </ext:Panel>
            <%-- End of Approval History --%>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Report_Save" runat="server" Icon="Disk" Text="Save Report">
                <DirectEvents>
                    <Click OnEvent="btn_Report_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Report..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Report_Accept" runat="server" Icon="Accept" Text="Accept">
                <DirectEvents>
                    <Click OnEvent="btn_Report_Accept_Click">
                        <EventMask ShowMask="true" Msg="Accepting Request..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Report_Reject" runat="server" Icon="Decline" Text="Need Correction">
                <DirectEvents>
                    <Click OnEvent="btn_Report_Reject_Click">
                        <EventMask ShowMask="true" Msg="Rejecting Request..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Report_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_Report_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>


    <%-- ================== TRANSACTION WINDOWS ========================== --%>
    <ext:Window ID="window_Transaction" Title="Add/Edit Transaction" runat="server" Modal="true" Maximized="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
            <ext:InfoPanel ID="info_ValidationResultTransaction" ClientIDMode="Static" runat="server" UI="Warning" Layout="AnchorLayout" Title="Validation Result" AutoScroll="true" IconCls="#Error" MarginSpec="10 0" Border="True" Hidden="true" MaxHeight="150" Collapsible="true"></ext:InfoPanel>

            <%-- Transaction General Info --%>
            <ext:Panel runat="server" ID="pnl_TransactionInfo" MarginSpec="0 0 10 0" Border="true" Layout="ColumnLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <ext:FieldSet runat="server" ColumnWidth="0.49" ID="FieldSet1" Border="false" Layout="AnchorLayout">
                        <Content>
                            <ext:TextField runat="server" LabelWidth="200" ID="txt_TransactionNumber" FieldLabel="Transaction Number" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="50" />
                            <ext:TextField runat="server" LabelWidth="200" ID="txt_InternalRefNumber" FieldLabel="Transaction Ref. No." AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="50" />
                            <ext:DateField runat="server" LabelWidth="200" ID="txt_TransactionDate" FieldLabel="Date of Transaction" AnchorHorizontal="100%" Format="dd-MMM-yyyy" />
                            <ext:DateField runat="server" LabelWidth="200" ID="txt_PostingDate" FieldLabel="Posting Date" AnchorHorizontal="100%" Format="dd-MMM-yyyy" />
                            <ext:TextField runat="server" LabelWidth="200" ID="txt_Teller" FieldLabel="Teller" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="20" />
                        </Content>
                    </ext:FieldSet>

                    <ext:FieldSet runat="server" ColumnWidth="0.49" ID="FieldSet2" Border="false" Layout="AnchorLayout">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_TransmodeCode" ValueField="Kode" DisplayField="Keterangan" StringField="Kode, Keterangan" StringTable="vw_mappingJenisLaporan_JenisTransaksi" Label="Transaction Method" AnchorHorizontal="100%" AllowBlank="false" OnOnValueChanged="cmb_TransmodeCode_Changed" />
                            <ext:TextField runat="server" LabelWidth="200" ID="txt_TransmodeComment" FieldLabel="Other Transaction Method" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="50" />
                            <ext:NumberField runat="server" LabelWidth="200" ID="txt_AmountLocal" FieldLabel="Transaction Local Amount" AnchorHorizontal="100%" MouseWheelEnabled="false" FormatText="#,##0.00" />
                            <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_TransactionType" ValueField="PK_Transaction_Type_ID" DisplayField="Transaction_Type" StringField="PK_Transaction_Type_ID, Transaction_Type" StringTable="goAML_Ref_Transaction_Type" Label="Transaction Type" AnchorHorizontal="100%" AllowBlank="false" OnOnValueChanged="cmb_TransactionType_Changed" />
                            <ext:TextField runat="server" LabelWidth="200" ID="txt_Authorized" FieldLabel="Transaction Authorizer" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="20"/>
                        </Content>
                    </ext:FieldSet>

                    <ext:FieldSet runat="server" ColumnWidth="0.98" ID="FieldSet3" Border="false" Layout="AnchorLayout">
                        <Content>
                               <ext:TextField runat="server" LabelWidth="200" ID="txt_AgentName" FieldLabel="Agent Name" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="255" />
                               <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_transactionchannel" ValueField="kode" DisplayField="keterangan" StringField="kode, keterangan" StringTable="goAML_Ref_Transaction_Channel" Label="Transaction Chanel" AnchorHorizontal="100%" AllowBlank="true" IsReadOnly="true"/>   
                               <ext:TextField runat="server" LabelWidth="200" ID="txt_TransactionLocation" FieldLabel="Transaction Location" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="255" />
                               <ext:Panel runat="server" ID="pnl_TransactionAddress" Border="true" Layout="AnchorLayout" BodyPadding="10" Title="Transaction Address">
                                    <Content>
                                        <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_AddressType_TransactionAddress" Label="Address Type" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Kategori_Kontak" AnchorHorizontal="100%" AllowBlank="true" IsReadOnly="true"  />
                                        <ext:TextArea runat="server" LabelWidth="200" ID="txt_Address_TransactionAddress" FieldLabel="Address" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="8000" AllowBlank="true" ReadOnly="true" />
                                        <ext:TextField runat="server" LabelWidth="200" ID="txt_Town_TransactionAddress" FieldLabel="Town" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="255" />
                                        <ext:TextField runat="server" LabelWidth="200" ID="txt_City_TransactionAddress" FieldLabel="City" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="255" AllowBlank="true" ReadOnly="true" />
                                        <ext:TextField runat="server" LabelWidth="200" ID="txt_ZipCode_TransactionAddress" FieldLabel="Zip Code" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="255" />
                                        <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_CountryCode_TransactionAddress" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="100%" AllowBlank="true" IsReadOnly="true"  />
                                        <ext:TextField runat="server" LabelWidth="200" ID="txt_Province_TransactionAddress" FieldLabel="Province" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="255" />   
                                    </Content>
                                </ext:Panel>
                            <ext:TextField runat="server" LabelWidth="200" ID="txt_TransactionDescription" FieldLabel="Transaction Description" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="4000" StyleSpec="margin-top:10px"/>
                            <ext:TextArea runat="server" LabelWidth="200" ID="txt_TransactionComment" FieldLabel="Transaction Notes" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="4000" />
                        </Content>
                    </ext:FieldSet>
                   
                </Content>
            </ext:Panel>

            <%-- Transaction Bi-Party --%>
            <ext:Panel runat="server" ID="pnl_TransactionBiParty" Border="true" Layout="ColumnLayout" Hidden="true" BodyPadding="10">
                <Items>
                    <ext:FieldSet runat="server" ColumnWidth="0.49" ID="fs_From" Title="<b>From</b>" Layout="AnchorLayout" Padding="10">
                        <Content>
                            <ext:Checkbox runat="server" LabelWidth="150" ID="chk_From_IsMyClient" FieldLabel="My Client"></ext:Checkbox>
                            <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_From_FundsCode" ValueField="Kode" DisplayField="Keterangan" StringField="Kode,Keterangan" StringTable="vw_instrumenFrom" Label="Transaction Instrument" AnchorHorizontal="100%" AllowBlank="false" OnOnValueChanged="cmb_From_FundsCode_Changed" />
                            <ext:TextField runat="server" LabelWidth="150" ID="txt_From_FundsCodeComment" Editable="false" FieldLabel="Other Instument" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                            <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_From_ForeignCurrencyCode" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Mata_Uang" Label="Foreign Currency" AnchorHorizontal="100%"  OnOnValueChanged="cmb_From_ForeignCurrencyCode_Changed"  />
                            <ext:NumberField runat="server" LabelWidth="150" ID="txt_From_ForeignCurrencyAmount" FieldLabel="Foreign Amount" AnchorHorizontal="100%" MouseWheelEnabled="false" />
                            <ext:NumberField runat="server" LabelWidth="150" ID="txt_From_ForeignCurrencyExchangeRate" FieldLabel="Exchange Rate" AnchorHorizontal="100%" MouseWheelEnabled="false" />
                            <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_From_Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="100%" />
                            <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_From_Information" ValueField="PK_Sender_To_Information" DisplayField="Sender_To_Information" StringField="PK_Sender_To_Information, Sender_To_Information" StringTable="GoAML_Ref_Sender_To_Information" Label="Sender Type" AnchorHorizontal="100%" OnOnValueChanged="cmb_From_Information_Changed" />

                            <ext:FieldSet runat="server" AnchorHorizontal="100%" ID="fs_FromInfo" Layout="AnchorLayout" Padding="10">
                                <Content>
                                    <ext:DisplayField runat="server" LabelWidth="150" ID="txt_From_CIFNO_ACCOUNTNO" FieldLabel="CIF | Account No." AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:DisplayField runat="server" LabelWidth="150" ID="df_From_Name" FieldLabel="Name" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:TextField runat="server" LabelWidth="150" ID="txt_From_Name" FieldLabel="Name" Hidden="true" AnchorHorizontal="100%"></ext:TextField>
                                    <ext:DisplayField runat="server" LabelWidth="150" ID="txt_From_Address" FieldLabel="Address" Hidden="true" AnchorHorizontal="100%"></ext:DisplayField>

                                    <ext:Button ID="btn_From_Edit" runat="server" Icon="Pencil" Text="Edit" MarginSpec="0 10 0 0">
                                        <DirectEvents>
                                            <Click OnEvent="btn_PartyEntry_Click">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Edit'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partyside" Value="'From'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btn_From_Detail" runat="server" Icon="ApplicationViewDetail" Text="Detail">
                                        <DirectEvents>
                                            <Click OnEvent="btn_PartyEntry_Click">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Detail'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partyside" Value="'From'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Content>
                            </ext:FieldSet>
                        </Content>
                    </ext:FieldSet>
                    <ext:FieldSet runat="server" ColumnWidth="0.49" ID="fs_To" Title="<b>To</b>" Layout="AnchorLayout" Padding="10">
                        <Content>
                            <ext:Checkbox runat="server" LabelWidth="150" ID="chk_To_IsMyClient" FieldLabel="My Client"></ext:Checkbox>
                            <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_To_FundsCode" ValueField="Kode" DisplayField="Keterangan" StringField="Kode,Keterangan" StringTable="vw_instrumenTo" Label="Transaction Instrument" AnchorHorizontal="100%" AllowBlank="false" OnOnValueChanged="cmb_To_FundsCode_Changed" />
                            <ext:TextField runat="server" LabelWidth="150" ID="txt_To_FundsCodeComment" Editable="false" FieldLabel="Other Instument" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                            <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_To_ForeignCurrencyCode" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Mata_Uang" Label="Foreign Currency" AnchorHorizontal="100%"  OnOnValueChanged="cmb_To_ForeignCurrencyCode_Changed"  />
                            <ext:NumberField runat="server" LabelWidth="150" ID="txt_To_ForeignCurrencyAmount" FieldLabel="Foreign Amount" AnchorHorizontal="100%" MouseWheelEnabled="false" />
                            <ext:NumberField runat="server" LabelWidth="150" ID="txt_To_ForeignCurrencyExchangeRate" FieldLabel="Exchange Rate" AnchorHorizontal="100%" MouseWheelEnabled="false" />
                            <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_To_Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="100%" />
                            <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_To_Information" ValueField="PK_Sender_To_Information" DisplayField="Sender_To_Information" StringField="PK_Sender_To_Information, Sender_To_Information" StringTable="GoAML_Ref_Sender_To_Information" Label="Recipient Type" AnchorHorizontal="100%" OnOnValueChanged="cmb_To_Information_Changed" />

                            <ext:FieldSet runat="server" AnchorHorizontal="100%" ID="fs_To_Info" Layout="AnchorLayout" Padding="10">
                                <Content>
                                    <ext:DisplayField runat="server" LabelWidth="150" ID="txt_To_CIFNO_ACCOUNTNO" FieldLabel="CIF | Account No." AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:DisplayField runat="server" LabelWidth="150" ID="df_To_Name" FieldLabel="Name" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:TextField runat="server" LabelWidth="150" ID="txt_To_Name" FieldLabel="Name" Hidden="true" AnchorHorizontal="100%"></ext:TextField>
                                    <ext:DisplayField runat="server" LabelWidth="150" ID="txt_To_Address" FieldLabel="Address" Hidden="true" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:Button ID="btn_To_Edit" runat="server" Icon="Pencil" Text="Edit" MarginSpec="0 10 0 0">
                                        <DirectEvents>
                                            <Click OnEvent="btn_PartyEntry_Click">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Edit'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partyside" Value="'To'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btn_To_Detail" runat="server" Icon="ApplicationViewDetail" Text="Detail">
                                        <DirectEvents>
                                            <Click OnEvent="btn_PartyEntry_Click">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Detail'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partyside" Value="'To'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Content>
                            </ext:FieldSet>
                        </Content>
                    </ext:FieldSet>
                    <ext:FieldSet runat="server" ColumnWidth="0.98" ID="fs_Conductor" Title="<b>Conductor</b>" Layout="AnchorLayout">
                        <Content>
                            <ext:Checkbox runat="server" LabelWidth="150" ID="chk_IsConductor" FieldLabel="Use Conductor" Checked="false" MarginSpec="0 0 10 0">
                                <DirectEvents>
                                    <Change OnEvent="chk_IsConductor_Changed"></Change>
                                </DirectEvents>
                            </ext:Checkbox>

                            <ext:FieldSet runat="server" AnchorHorizontal="100%" ID="fs_Conductor_Info" Layout="AnchorLayout" Padding="10" Hidden="true">
                                <Content>
                                    <ext:DisplayField runat="server" LabelWidth="150" ID="df_Conductor_Name" FieldLabel="Name" AnchorHorizontal="100%"></ext:DisplayField>
                                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Conductor_Name" FieldLabel="Name" Hidden="true" AnchorHorizontal="100%"></ext:TextField>
                                    <ext:DisplayField runat="server" LabelWidth="150" ID="txt_Conductor_Address" FieldLabel="Address" Hidden="true"></ext:DisplayField>

                                    <ext:Button ID="btn_Conductor_Edit" runat="server" Icon="Pencil" Text="Edit" MarginSpec="0 10 0 0">
                                        <DirectEvents>
                                            <Click OnEvent="btn_PartyEntry_Click">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Edit'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partyside" Value="'Conductor'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btn_Conductor_Detail" runat="server" Icon="ApplicationViewDetail" Text="Detail">
                                        <DirectEvents>
                                            <Click OnEvent="btn_PartyEntry_Click">
                                                <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Detail'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partyside" Value="'Conductor'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Content>
                            </ext:FieldSet>
                        </Content>
                    </ext:FieldSet>
                </Items>
            </ext:Panel>

            <%-- Transaction Multi Party --%>
            <ext:Panel runat="server" ID="pnl_TransactionMultiParty" Title="Transaction Multi Party" MarginSpec="0 0 10 0" Collapsible="true" Border="true" Layout="AnchorLayout" AnchorHorizontal="100%" Hidden="true">
                <Content>
                    <ext:GridPanel ID="gp_MultiParty" runat="server" EmptyText="No data available">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_MultiParty_Add" Text="Add Party" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_MultiParty_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_MultiParty" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model1" IDProperty="PK_Trn_Party_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Trn_Party_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="PartyRole" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PartyType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PartyName" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn10" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column33" runat="server" DataIndex="PartyRole" Text="Role" Flex="1" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column34" runat="server" DataIndex="PartyType" Text="Party Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column35" runat="server" DataIndex="PartyName" Text="Party Name" Flex="1" MinWidth="300"></ext:Column>
                                <ext:CommandColumn ID="cc_MultiParty" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_MultiParty">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Trn_Party_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Transaction_Save" runat="server" Icon="Disk" Text="Save Transaction">
                <DirectEvents>
                    <Click OnEvent="btn_Transaction_Save_Click">
                        <EventMask ShowMask="true" Msg="Validating and Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Transaction_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Transaction_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.95});" />
            <Resize Handler="#{window_Transaction}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF TRANSACTION WINDOWS ========================== --%>


    <%-- ================== ACCOUNT PARTY ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_AccountParty" Title="Account Party" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
            <ext:Button ID="btn_Account_Import_Account" runat="server" Icon="Zoom" Text="Search Data Account" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_Account_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

            <%-- Account General Information --%>
            <ext:Panel runat="server" ID="pnl_Account_GeneralInfo" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_InstitutionName" FieldLabel="Financial Institution Name" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_InstitutionCode" FieldLabel="Financial Institution Code" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="50"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_SwiftCode" FieldLabel="Swift Code" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="50"/>
                    <ext:Checkbox runat="server"  LabelWidth="200" ID="chk_Account_NonBankingInstitution" FieldLabel="Non Bank Institution"></ext:Checkbox>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Branch" FieldLabel="Account Opening Branch" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Number" FieldLabel="Account Number" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="50" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Account_CurrencyCode" Label="Account Currency" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Mata_Uang" AnchorHorizontal="70%"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Label" FieldLabel="Account Label" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_IBAN" FieldLabel="IBAN" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="34" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_ClientNumber" FieldLabel="Client Number" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="30"/>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Account_Type" Label="Account Type" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Jenis_Rekening" AnchorHorizontal="70%"/>

                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Account_OpeningDate" FieldLabel="Opening Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Account_ClosingDate" FieldLabel="Closing Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />
                    <ext:NumberField runat="server" LabelWidth="200" ID="txt_Account_Balance" FieldLabel="Balance Amount" AnchorHorizontal="70%" MouseWheelEnabled="false" />
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Account_BalanceDate" FieldLabel="Balance Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Account_StatusCode" Label="Account Status" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Status_Rekening" AnchorHorizontal="70%"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Beneficiary" FieldLabel="Beneficiary Name" AnchorHorizontal="70%"  EnforceMaxLength="True" maxlength="50"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_BeneficiaryComment" FieldLabel="Beneficiary Note" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_Account_Comment" FieldLabel="Notes" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"  />

                </Content>
            </ext:Panel>

            <%--Signatory--%>
            <ext:GridPanel ID="gp_Account_Signatory" runat="server" AutoScroll="true" Title="Account Signatory" Border="true" EmptyText="No data available" MarginSpec="10 0">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <TopBar>
                    <ext:Toolbar runat="server">
                        <Items>
                            <ext:Button runat="server" ID="btn_Account_Signatory_Add" Text="Add Signatory" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btn_Person_Entry_Click">
                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="persontype" Value="'AccountSignatory'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="store_Account_Signatory" runat="server">
                        <Model>
                            <ext:Model runat="server" ID="Model134" IDProperty="PK_Account_Signatory_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_Account_Signatory_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="isPrimary" Type="Boolean"></ext:ModelField>
                                    <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="RoleName" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn26" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="Column88" runat="server" DataIndex="isPrimary" Text="Primary" Flex="1"></ext:Column>
                        <ext:Column ID="Column89" runat="server" DataIndex="Last_Name" Text="Name" Flex="1"></ext:Column>
                        <ext:Column ID="Column90" runat="server" DataIndex="RoleName" Text="Role" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="cc_Account_Signatory" runat="server" Text="Action" MinWidth="220">
                            <Commands>
                                <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                    <ToolTip Text="Edit"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                    <ToolTip Text="Delete"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="btn_Person_Entry_Click">
                                    <EventMask ShowMask="true"></EventMask>
                                    <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_Account_Signatory_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="partytype" Value="'AccountSignatory'" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
            </ext:GridPanel>

             <%--Related Entity--%>
            <%-- 2023-11-17, Nael: Hide grid related entity, related person, dan sanction (fixing defect 418) --%>
            <ext:GridPanel ID="gp_Account_RelatedEntity" runat="server" AutoScroll="true" Title="Related Entity" Border="true" EmptyText="No data available" MarginSpec="10 0" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <TopBar>
                    <ext:Toolbar runat="server">
                        <Items>
                            <ext:Button runat="server" ID="btn_Account_RelatedEntity" Text="Add Related Entity" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btn_Account_RelatedEntity_Click">
                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="persontype" Value="'Account'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="store1" runat="server">
                        <Model>
                            <ext:Model runat="server" ID="Model2" IDProperty="PK_goAML_Ref_Customer_Related_Entities_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_goAML_Ref_Customer_Related_Entities_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ENTITY_ENTITY_RELATION" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="NAME" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn11" runat="server" Text="No"></ext:RowNumbererColumn>
                           <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" MinWidth="220">
                            <Commands>
                                <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                    <ToolTip Text="Edit"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                    <ToolTip Text="Delete"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="btn_Account_Entity_Click">
                                    <EventMask ShowMask="true"></EventMask>
                                    <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Entities_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="partytype" Value="'Account'" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                        <ext:Column ID="Column37" runat="server" DataIndex="ENTITY_ENTITY_RELATION" Text="Entity Relation" Flex="1"></ext:Column>
                        <ext:Column ID="Column38" runat="server" DataIndex="NAME" Text="Name" Flex="1"></ext:Column>
                     
                    </Columns>
                </ColumnModel>
            </ext:GridPanel>

            <%--Related Account--%>
            <ext:GridPanel ID="gp_Account_RelatedAccount" runat="server" AutoScroll="true" Title="Related Account" Border="true" EmptyText="No data available" MarginSpec="10 0" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <TopBar>
                    <ext:Toolbar runat="server">
                        <Items>
                            <ext:Button runat="server" ID="btn_Account_Add_RelatedAccount" Text="Add Related Account" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btn_Account_Add_RelatedAccount_Click">
                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="persontype" Value="'Account'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="store2" runat="server">
                        <Model>
                            <ext:Model runat="server" ID="Model3" IDProperty="PK_goAML_Ref_Account_Rel_Account_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_goAML_Ref_Account_Rel_Account_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_ACCOUNT_RELATIONSHIP" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="ACCOUNT_NO" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="account_name" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn12" runat="server" Text="No"></ext:RowNumbererColumn>
                           <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Action" MinWidth="220">
                            <Commands>
                                <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                    <ToolTip Text="Edit"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                    <ToolTip Text="Delete"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="btn_Related_Account_Click">
                                    <EventMask ShowMask="true"></EventMask>
                                    <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Related_Entities_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="partytype" Value="'Account'" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                        <ext:Column ID="Column36" runat="server" DataIndex="ACCOUNT_ACCOUNT_RELATIONSHIP" Text="Account Relation" Flex="1"></ext:Column>
                        <ext:Column ID="Column39" runat="server" DataIndex="ACCOUNT_NO" Text="Account No" Flex="1"></ext:Column>
                        <ext:Column ID="Column40" runat="server" DataIndex="account_name" Text="Account Name" Flex="1"></ext:Column>
                    </Columns>
                </ColumnModel>
            </ext:GridPanel>

                <%--Sanction--%>
            <ext:GridPanel ID="GridPanel3" runat="server" AutoScroll="true" Title="Sanction" Border="true" EmptyText="No data available" MarginSpec="10 0" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <TopBar>
                    <ext:Toolbar runat="server">
                        <Items>
                            <ext:Button runat="server" ID="Button2" Text="Add Sanction" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btn_Sanction_Add_Click">
                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="persontype" Value="'Account'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="store3" runat="server">
                        <Model>
                            <ext:Model runat="server" ID="Model4" IDProperty="PK_goAML_Ref_Customer_Sanction_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_goAML_Ref_Customer_Sanction_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="PROVIDER" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="SANCTION_LIST_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_CRITERIA" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn13" runat="server" Text="No"></ext:RowNumbererColumn>
                           <ext:CommandColumn ID="CommandColumn3" runat="server" Text="Action" MinWidth="220">
                            <Commands>
                                <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                    <ToolTip Text="Edit"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                    <ToolTip Text="Delete"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_sanction">
                                    <EventMask ShowMask="true"></EventMask>
                                    <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Sanction_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="persontype" Value="'Account'" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                        <ext:Column ID="Column41" runat="server" DataIndex="PROVIDER" Text="Provider" Flex="1"></ext:Column>
                        <ext:Column ID="Column42" runat="server" DataIndex="SANCTION_LIST_NAME" Text="Sanction Name" Flex="1"></ext:Column>
                        <ext:Column ID="Column43" runat="server" DataIndex="MATCH_CRITERIA" Text="Match Criteria" Flex="1"></ext:Column>
                    </Columns>
                </ColumnModel>
            </ext:GridPanel>


   


            <%-- Account Entity --%>
            <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Account_IsEntity" FieldLabel="Corporate Account" Checked="false" MarginSpec="10 0 0 0">
                <DirectEvents>
                    <Change OnEvent="chk_Account_IsEntity_Changed"></Change>
                </DirectEvents>
            </ext:Checkbox>

            <ext:Panel runat="server" ID="pnl_Account_Entity" MarginSpec="0 0 10 0" Border="true" Title="Corporate Account Information" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%" Hidden="true">
                <Content>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_CorporateName" FieldLabel="Corporate Name" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_CommercialName" FieldLabel="Commercial Name" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Account_Entity_IncorporationLegalForm" Label="Corporate Legal Form" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Bentuk_Badan_Usaha" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_IncorporationNumber" FieldLabel="Business License Number" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_Business" FieldLabel="Line of Business" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />

                    <%-- Account Entity Address --%>
                    <ext:GridPanel ID="gp_Account_Entity_Address" runat="server" AutoScroll="true" Title="Address" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Account_Entity_Address_Add" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Address_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Account_Entity_Address" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model19" IDProperty="PK_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AddressType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column8" runat="server" DataIndex="AddressType" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column9" runat="server" DataIndex="Address" Text="Address" Flex="1"></ext:Column>
                                <ext:Column ID="Column10" runat="server" DataIndex="City" Text="City" Flex="1"></ext:Column>
                                <ext:Column ID="Column11" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Account_Entity_Address" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Address_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Account Entity Phone --%>
                    <ext:GridPanel ID="gp_Account_Entity_Phone" runat="server" AutoScroll="true" Title="Phone" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Account_Entity_Phone_Add" Text="Add Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Phone_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Account_Entity_Phone" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model18" IDProperty="PK_Phone_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Phone_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ContactType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column5" runat="server" DataIndex="ContactType" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" Flex="1"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="tph_number" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Account_Entity_Phone" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Phone_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Phone_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                   <%-- <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_Email" FieldLabel="Corporate Email" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_URL" FieldLabel="Corporate Website" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />--%>
                    
                    <%-- Email --%>
                    <ext:GridPanel ID="gp_email_accountentity" runat="server" AutoScroll="true" Title="Email" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Email_AccountEntity" Text="Add Email" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Email_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="parenttable" Value="'Person'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store25" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model34" IDProperty="PK_goAML_Report_Email_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Email_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="email_address" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn38" runat="server" Text="No"></ext:RowNumbererColumn>
                                      <ext:CommandColumn ID="CommandColumn25" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_email">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Email_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column104" runat="server" DataIndex="email_address" Text="Contact Type" Flex="1"></ext:Column>
                          
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Website --%>
                    <ext:GridPanel ID="gp_website_accountentity" runat="server" AutoScroll="true" Title="Website" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Website_AccountEntity" Text="Add Email" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Website_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="parenttable" Value="'Person'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store26" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model35" IDProperty="PK_goAML_Report_Url_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Url_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="url" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn39" runat="server" Text="No"></ext:RowNumbererColumn>
                                      <ext:CommandColumn ID="CommandColumn26" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_website">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Url_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column105" runat="server" DataIndex="url" Text="Contact Type" Flex="1"></ext:Column>
                          
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                    
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_IncorporationState" FieldLabel="State/Province" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255"  />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Account_Entity_IncorporationCountryCode" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="70%"  />

                    <%-- Account Entity Director --%>
                    <ext:GridPanel ID="gp_Account_Entity_Director" runat="server" AutoScroll="true" Title="Director" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Account_Entity_Director_Add" Text="Add Director" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Person_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'AccountEntityDirector'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Account_Entity_Director" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model21" IDProperty="PK_Account_Entity_Director_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Account_Entity_Director_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleName" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn36" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column129" runat="server" DataIndex="Last_Name" Text="Name" Flex="1"></ext:Column>
                                <ext:Column ID="Column130" runat="server" DataIndex="RoleName" Text="Role" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Account_Entity_Director" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Person_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Account_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'AccountEntityDirector'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%--Related Entity--%>
                    <ext:GridPanel ID="gp_RelatedEntity_AccountEntity" runat="server" AutoScroll="true" Title="Related Entity" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                        <ext:Toolbar runat="server">
                            <Items>
                            <ext:Button runat="server" ID="btn_RelatedEntity_AccountEntity" Text="Add Related Entity" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btn_Account_RelatedEntity_Click">
                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="persontype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                        </ext:Toolbar>
                       </TopBar>
                        <Store>
                            <ext:Store ID="store27" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model39" IDProperty="PK_goAML_Report_Related_Entities_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Related_Entities_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="entity_entity_relation" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="name" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn40" runat="server" Text="No"></ext:RowNumbererColumn>
                                   <ext:CommandColumn ID="CommandColumn27" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <%--<Command OnEvent="btn_Account_Entity_Click">--%>
                                        <Command OnEvent="btn_Account_RelatedEntity_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Related_Entities_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column106" runat="server" DataIndex="entity_entity_relation" Text="Entity Relation" Flex="1"></ext:Column>
                                <ext:Column ID="Column107" runat="server" DataIndex="name" Text="Name" Flex="1"></ext:Column>
                     
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Account_Entity_IncorporationDate" FieldLabel="Date of Establishment" AnchorHorizontal="40%" Format="dd-MMM-yyyy" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Account_Entity_IsClosed" FieldLabel="Is Closed">
                        <DirectEvents>
                            <Change OnEvent="chk_Account_Entity_IsClosed_Changed"></Change>
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField runat="server" LabelWidth="200" Hidden="true" ID="txt_Account_Entity_ClosingDate" FieldLabel="Closing Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Account_Entity_TaxNumber" FieldLabel="Tax Number" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="100"  />
                    
                     <%-- Entity Identification --%>
                    <ext:GridPanel ID="gp_EntityIdentification_AccountEntity" runat="server" AutoScroll="true" Title="Entity Identification" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Identification_AccountEntity" Text="Add Identification" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Identification_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <%-- 2023-11-06, Nael: Menambahkan parameter partytype --%>
                                                    <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter> 
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store29" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model41" IDProperty="PK_goAML_Report_Identifications_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Identifications_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="issue_date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="expiry_date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn42" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumn29" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Identification_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Identifications_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter> 
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column116" runat="server" DataIndex="type" Text="Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column117" runat="server" DataIndex="number" Text="Number" Flex="1" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="DateColumn7" runat="server" DataIndex="issue_date" Text="Issue Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:DateColumn ID="DateColumn8" runat="server" DataIndex="expiry_date" Text="Expired Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column118" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                         <%--Sanction--%>
                        <ext:GridPanel ID="gp_sanction_accountentity" runat="server" AutoScroll="true" Title="Sanction" Border="true" EmptyText="No data available" MarginSpec="10 0">
                            <View>
                                <ext:GridView runat="server" EnableTextSelection="true" />
                            </View>
                            <TopBar>
                                <ext:Toolbar runat="server">
                                    <Items>
                                        <ext:Button runat="server" ID="btn_Sanction_AccountEntity" Text="Add Sanction" Icon="Add">
                                            <DirectEvents>
                                                <Click OnEvent="btn_Sanction_Add_Click">
                                                    <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="persontype" Value="'Sanction'" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Items>
                                </ext:Toolbar>
                            </TopBar>
                            <Store>
                                <ext:Store ID="store30" runat="server">
                                    <Model>
                                        <ext:Model runat="server" ID="Model42" IDProperty="PK_goAML_Report_Sanction_ID">
                                            <Fields>
                                                <ext:ModelField Name="PK_goAML_Report_Sanction_ID" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="provider" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="sanction_list_name" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="match_criteria" Type="String"></ext:ModelField>
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:RowNumbererColumn ID="RowNumbererColumn44" runat="server" Text="No"></ext:RowNumbererColumn>
                                       <ext:CommandColumn ID="CommandColumn30" runat="server" Text="Action" MinWidth="220">
                                        <Commands>
                                            <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                                <ToolTip Text="Edit"></ToolTip>
                                            </ext:GridCommand>
                                            <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                                <ToolTip Text="Detail"></ToolTip>
                                            </ext:GridCommand>
                                            <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                                <ToolTip Text="Delete"></ToolTip>
                                            </ext:GridCommand>
                                        </Commands>
                                        <DirectEvents>
                                            <Command OnEvent="gc_Sanction">
                                                <EventMask ShowMask="true"></EventMask>
                                                <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Command>
                                        </DirectEvents>
                                    </ext:CommandColumn>
                                    <ext:Column ID="Column119" runat="server" DataIndex="provider" Text="Provider" Flex="1"></ext:Column>
                                    <ext:Column ID="Column120" runat="server" DataIndex="sanction_list_name" Text="Sanction Name" Flex="1"></ext:Column>
                                    <ext:Column ID="Column121" runat="server" DataIndex="match_criteria" Text="Match Criteria" Flex="1"></ext:Column>
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>
                    
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_Account_Entity_Comment" FieldLabel="Notes" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"  />
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Account_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Account_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Account_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Account_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.95});" />
            <Resize Handler="#{window_AccountParty}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF PARTY EDIT WINDOWS (ACCOUNT) ========================== --%>

    <%-- ================== PERSON PARTY ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_PersonParty" Title="Person Party" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
            <ext:Button ID="btn_Person_Import_Customer" runat="server" Icon="Zoom" Text="Search Data Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_Customer_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Person_Import_WIC" runat="server" Icon="Zoom" Text="Search Data Non Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_WIC_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

            <%-- Person Information --%>
            <ext:Panel runat="server" ID="pnl_PersonParty" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_Gender" Label="Gender" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Jenis_Kelamin" AnchorHorizontal="70%"   />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Title" FieldLabel="Title" EnforceMaxLength="True" maxlength="30" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_LastName" FieldLabel="Full Name" EnforceMaxLength="True" maxlength="100" AnchorHorizontal="70%" />
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Person_BirthDate" FieldLabel="Date of Birth" AnchorHorizontal="60%" Format="dd-MMM-yyyy" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_BirthPlace" FieldLabel="Place of Birth" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_MotherName" FieldLabel="Mother's Name" EnforceMaxLength="True" maxlength="100" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Alias" FieldLabel="Alias Name" EnforceMaxLength="True" maxlength="100" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_SSN" FieldLabel="SSN/NIK" EnforceMaxLength="True" maxlength="25" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_PassportNumber" FieldLabel="Passport No." EnforceMaxLength="True" maxlength="25" AnchorHorizontal="70%" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_PassportCountry" Label="Passport Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_IDNumber" FieldLabel="Other ID Number" EnforceMaxLength="True" maxlength="25" AnchorHorizontal="70%" />

                    <%-- Person Address --%>
                    <ext:GridPanel ID="gp_Person_Address" runat="server" AutoScroll="true" Title="Address" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Person_Address_Add" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Address_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="parenttable" Value="'person'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Person_Address" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model31" IDProperty="PK_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AddressType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn7" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column21" runat="server" DataIndex="AddressType" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="Address" Text="Address" Flex="1"></ext:Column>
                                <ext:Column ID="Column23" runat="server" DataIndex="City" Text="City" Flex="1"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Person_Address" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Address_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                                <%-- 2023-11-02, Nael: person harus berada dalam kutip -> 'Person' --%>
                                                <ext:Parameter Name="parenttable" Value="'Person'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Person Phone --%>
                    <ext:GridPanel ID="gp_Person_Phone" runat="server" AutoScroll="true" Title="Phone" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Person_Phone_Add" Text="Add Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Phone_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Person_Phone" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model30" IDProperty="PK_Phone_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Phone_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ContactType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column18" runat="server" DataIndex="ContactType" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column19" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" Flex="1"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="tph_number" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Person_Phone" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Phone_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Phone_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_Nationality1" Label="Nationality 1" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_Nationality2" Label="Nationality 2" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_Nationality3" Label="Nationality 3" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_Residence" Label="Domicile Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                    
                      <%-- Email --%>
                    <ext:GridPanel ID="gp_email_person" runat="server" AutoScroll="true" Title="Email" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Email_Person" Text="Add Email" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Email_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="parenttable" Value="'person'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store13" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model14" IDProperty="PK_goAML_Report_Email_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Email_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="email_address" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn23" runat="server" Text="No"></ext:RowNumbererColumn>
                                      <ext:CommandColumn ID="CommandColumn13" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_email">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Email_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="parenttable" Value="'Person'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column74" runat="server" DataIndex="email_address" Text="Contact Type" Flex="1"></ext:Column>
                          
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                    
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Email1" FieldLabel="Email 1" EnforceMaxLength="True" maxlength="25" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Email2" FieldLabel="Email 2" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Email3" FieldLabel="Email 3" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Email4" FieldLabel="Email 4" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Email5" FieldLabel="Email 5" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_Occupation" FieldLabel="Occupation" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_EmployerName" FieldLabel="Work Place" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />

                    <%-- Person Employer Address --%>
                    <ext:GridPanel ID="gp_Person_EmployerAddress" runat="server" AutoScroll="true" Title="Work Place Address" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Person_EmployerAddress_Add" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Address_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="1" Mode="Raw"></ext:Parameter>
                                                     <ext:Parameter Name="parenttable" Value="'person'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Person_EmployerAddress" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model36" IDProperty="PK_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AddressType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn8" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column25" runat="server" DataIndex="AddressType" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="Address" Text="Address" Flex="1"></ext:Column>
                                <ext:Column ID="Column27" runat="server" DataIndex="City" Text="City" Flex="1"></ext:Column>
                                <ext:Column ID="Column28" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Person_EmployerAddress" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Address_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="1" Mode="Raw"></ext:Parameter>
                                                 <ext:Parameter Name="parenttable" Value="'Person'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Person Employer Phone --%>
                    <ext:GridPanel ID="gp_Person_EmployerPhone" runat="server" AutoScroll="true" Title="Work Place Phone" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Person_EmployerPhone_Add" Text="Add Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Phone_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="1" Mode="Raw"></ext:Parameter>
                                                     <ext:Parameter Name="parenttable" Value="'person'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Person_EmployerPhone" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model37" IDProperty="PK_Phone_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Phone_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ContactType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn9" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column29" runat="server" DataIndex="ContactType" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column30" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" Flex="1"></ext:Column>
                                <ext:Column ID="Column31" runat="server" DataIndex="tph_number" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Person_EmployerPhone" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Phone_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Phone_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="1" Mode="Raw"></ext:Parameter>
                                                <%-- 2023-11-02, Nael: Person harus berada dalam kutip 'Person' --%>
                                                 <ext:Parameter Name="parenttable" Value="'Person'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Person Identification --%>
                    <ext:GridPanel ID="gp_Person_Identification" runat="server" AutoScroll="true" Title="Identification" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Person_Identification_Add" Text="Add Identification" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Identification_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                     <ext:Parameter Name="parenttable" Value="'person'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Person_Identification" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model38" IDProperty="PK_Identification_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Issue_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Expiry_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn32" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column108" runat="server" DataIndex="Type" Text="Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column109" runat="server" DataIndex="Number" Text="Number" Flex="1" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="Column110" runat="server" DataIndex="Issue_Date" Text="Issue Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:DateColumn ID="Column111" runat="server" DataIndex="Expiry_Date" Text="Expired Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column112" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Person_Identification" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Identification_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <%-- 2023-11-02, Nael: Person harus berada dalam kutip 'Person' --%>
                                                 <ext:Parameter Name="parenttable" Value="'Person'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Person_IsDecease" FieldLabel="Has Passed Away">
                        <DirectEvents>
                            <Change OnEvent="chk_Person_IsDecease_Changed" />
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Person_DeceaseDate" FieldLabel="Date of Passed Away" AnchorHorizontal="60%" Format="dd-MMM-yyyy" Hidden="true"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_TaxNumber" FieldLabel="Tax Number" EnforceMaxLength="True" maxlength="100" AnchorHorizontal="70%" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Person_IsPEP" FieldLabel="PEP"></ext:Checkbox>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Person_SourceOfWealth" FieldLabel="Source of Wealth" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    
                           <%-- PEP --%>
                    <ext:GridPanel ID="gp_PEP_Person" runat="server" AutoScroll="true" Title="Politically Exposed Person" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_PEP_Person" Text="Add Email" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_PEP_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="parenttable" Value="'person'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store18" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model23" IDProperty="PK_goAML_Report_Person_PEP_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Person_PEP_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="pep_country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="function_name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="function_description" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn29" runat="server" Text="No"></ext:RowNumbererColumn>
                                      <ext:CommandColumn ID="CommandColumn18" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_pep">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Person_PEP_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <%-- 2023-11-02, Nael: Person harus berada dalam kutip 'Person' --%>
                                                <ext:Parameter Name="parenttable" Value="'Person'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column75" runat="server" DataIndex="pep_country" Text="PEP Country" Flex="1"></ext:Column>
                                <ext:Column ID="Column76" runat="server" DataIndex="function_name" Text="Function Name" Flex="1"></ext:Column>
                                <ext:Column ID="Column77" runat="server" DataIndex="function_description" Text="Function Description" Flex="1"></ext:Column>

                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                     <%--Sanction--%>
                    <ext:GridPanel ID="gp_sanction_person" runat="server" AutoScroll="true" Title="Sanction" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Sanction_Person" Text="Add Sanction" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Sanction_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="parenttable" Value="'person'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store19" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model26" IDProperty="PK_goAML_Report_Sanction_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Sanction_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="provider" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="sanction_list_name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="match_criteria" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn30" runat="server" Text="No"></ext:RowNumbererColumn>
                                   <ext:CommandColumn ID="CommandColumn19" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_Sanction">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <%-- 2023-11-02, Nael: Person harus berada dalam kutip 'Person' --%>
                                                 <ext:Parameter Name="parenttable" Value="'Person'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column92" runat="server" DataIndex="provider" Text="Provider" Flex="1"></ext:Column>
                                <ext:Column ID="Column93" runat="server" DataIndex="sanction_list_name" Text="Sanction Name" Flex="1"></ext:Column>
                                <ext:Column ID="Column94" runat="server" DataIndex="match_criteria" Text="Match Criteria" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                     <%--Related Person--%>
                    <ext:GridPanel ID="gp_relatedperson_person" runat="server" AutoScroll="true" Title="Related Person" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Add_RelatedPerson_Person" Text="Add Related Person" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Add_RelatedPerson_Person_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="parenttable" Value="'person'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store20" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model27" IDProperty="PK_goAML_Report_Related_Person_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Related_Person_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="person_person_relation" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="last_name" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                            <ext:RowNumbererColumn ID="RowNumbererColumn31" runat="server" Text="No"></ext:RowNumbererColumn>
                            <ext:CommandColumn ID="CommandColumn20" runat="server" Text="Action" MinWidth="220">
                            <Commands>
                                <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                    <ToolTip Text="Edit"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                    <ToolTip Text="Delete"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="btn_RelatedPerson_Person_Click">
                                    <EventMask ShowMask="true"></EventMask>
                                    <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Related_Person_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="parenttable" Value="'person'" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                        <ext:Column ID="Column95" runat="server" DataIndex="person_person_relation" Text="Entity Relation" Flex="1"></ext:Column>
                        <ext:Column ID="Column96" runat="server" DataIndex="last_name" Text="Name" Flex="1"></ext:Column>
                     
                    </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                    
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Person_Protected" FieldLabel="Is Protected"></ext:Checkbox>
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_Person_Comment" FieldLabel="Notes" EnforceMaxLength="True" maxlength="4000" AnchorHorizontal="70%" />

                    <%-- For Account Signatory --%>
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Person_AccountSignatory_IsPrimary" FieldLabel="Is Primary"></ext:Checkbox>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_AccountSignatory_Role" Label="Role" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Peran_orang_dalam_rekening" AnchorHorizontal="70%"/>

                    <%-- For Director --%>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Person_Director_Role" Label="Role" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Peran_orang_dalam_Korporasi" AnchorHorizontal="70%"/>
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Person_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Person_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Person_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Person_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.85, height: size.height * 0.8});" />
            <Resize Handler="#{window_PersonParty}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF PERSON PARTY ENTRY WINDOWS ========================== --%>


    <%-- ================== ENTITY PARTY ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_EntityParty" Title="Entity Party" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
            <ext:Button ID="btn_Entity_Import_Customer" runat="server" Icon="Zoom" Text="Search Data Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_Customer_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Entity_Import_WIC" runat="server" Icon="Zoom" Text="Search Data Non Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_WIC_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

            <%-- Entity Information --%>
            <ext:Panel runat="server" ID="pnl_EntityParty" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_CorporateName" FieldLabel="Corporate Name" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_CommercialName" FieldLabel="Commercial Name" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Entity_IncorporationLegalForm" Label="Legal Form" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Bentuk_Badan_Usaha" AnchorHorizontal="70%"   />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_IncorporationNumber" FieldLabel="Corporate License Number" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_Business" FieldLabel="Line of Business" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />

                    <%-- Entity Address --%>
                    <ext:GridPanel ID="gp_Entity_Address" runat="server" AutoScroll="true" Title="Address" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Entity_Address_Add" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Address_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                     <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Entity_Address" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model25" IDProperty="PK_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Address_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="AddressType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column15" runat="server" DataIndex="AddressType" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="Address" Text="Address" Flex="1"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="City" Text="City" Flex="1"></ext:Column>
                                <ext:Column ID="Column32" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Entity_Address" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <%--<ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>--%>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <%--<ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>--%>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Address_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Entity Phone --%>
                    <ext:GridPanel ID="gp_Entity_Phone" runat="server" AutoScroll="true" Title="Phone" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Entity_Phone_Add" Text="Add Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Phone_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                     <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Entity_Phone" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model24" IDProperty="PK_Phone_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Phone_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="ContactType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column12" runat="server" DataIndex="ContactType" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" Flex="1"></ext:Column>
                                <ext:Column ID="Column14" runat="server" DataIndex="tph_number" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Entity_Phone" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Phone_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Phone_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                 <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Email --%>
                    <ext:GridPanel ID="gp_email_entity" runat="server" AutoScroll="true" Title="Email" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Email_Entity" Text="Add Email" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Email_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store21" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model28" IDProperty="PK_goAML_Report_Email_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Email_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="email_address" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn33" runat="server" Text="No"></ext:RowNumbererColumn>
                                      <ext:CommandColumn ID="CommandColumn21" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_email">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Email_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column97" runat="server" DataIndex="email_address" Text="Email Address" Flex="1"></ext:Column>
                          
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Website --%>
                    <ext:GridPanel ID="gp_website_entity" runat="server" AutoScroll="true" Title="Website" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Website_Entity" Text="Add Email" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Website_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store22" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model29" IDProperty="PK_goAML_Report_Url_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Url_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="url" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn34" runat="server" Text="No"></ext:RowNumbererColumn>
                                      <ext:CommandColumn ID="CommandColumn22" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_Website">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Email_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column98" runat="server" DataIndex="url" Text="Contact Type" Flex="1"></ext:Column>
                          
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_Email" FieldLabel="Corporate Email" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_URL" FieldLabel="Corporate Website" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_IncorporationState" FieldLabel="Province" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255"  />
                    <NDS:NDSDropDownField LabelWidth="200" ID="cmb_Entity_IncorporationCountryCode" Label="Country" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"  />

                    <%-- Entity Director --%>
                    <ext:GridPanel ID="gp_Entity_Director" runat="server" AutoScroll="true" Title="Director" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Entity_Director_Add" Text="Add Director" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Person_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="partytype" Value="'EntityDirector'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Entity_Director" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model22" IDProperty="PK_Entity_Director_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Entity_Director_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="Last_Name" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="RoleName" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn43" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column154" runat="server" DataIndex="Last_Name" Text="Name" Flex="1"></ext:Column>
                                <ext:Column ID="Column155" runat="server" DataIndex="RoleName" Text="Role" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="cc_Entity_Director" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Person_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Entity_Director_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'EntityDirector'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    
                    <%--Related Entity--%>
                    <ext:GridPanel ID="gp_RelatedEntity_Entity" runat="server" AutoScroll="true" Title="Related Entity" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                        <ext:Toolbar runat="server">
                            <Items>
                            <ext:Button runat="server" ID="btn_RelatedEntity_Entity" Text="Add Related Entity" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btn_Account_RelatedEntity_Click">
                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="persontype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                             <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                        </ext:Toolbar>
                       </TopBar>
                        <Store>
                            <ext:Store ID="store23" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model32" IDProperty="PK_goAML_Report_Related_Entities_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Related_Entities_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="entity_entity_relation" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="name" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn35" runat="server" Text="No"></ext:RowNumbererColumn>
                                   <ext:CommandColumn ID="CommandColumn23" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Account_RelatedEntity_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Related_Entities_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="partytype" Value="'AccountEntity'" Mode="Raw"></ext:Parameter>
                                                 <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column99" runat="server" DataIndex="entity_entity_relation" Text="Entity Relation" Flex="1"></ext:Column>
                                <ext:Column ID="Column100" runat="server" DataIndex="name" Text="Name" Flex="1"></ext:Column>
                     
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Entity_IncorporationDate" FieldLabel="Date of Established" AnchorHorizontal="40%" Format="dd-MMM-yyyy" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Entity_IsClosed" FieldLabel="Is Closed">
                        <DirectEvents>
                            <Change OnEvent="chk_Entity_IsClosed_Changed"></Change>
                        </DirectEvents>
                    </ext:Checkbox>

                    <ext:DateField runat="server" Hidden="true" LabelWidth="200" ID="txt_Entity_ClosingDate" FieldLabel="Closing Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Entity_TaxNumber" FieldLabel="Tax Number" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="100"  />
                    
                      <%-- Entity Identification --%>
                    <ext:GridPanel ID="gp_entity_identification" runat="server" AutoScroll="true" Title="Entity Identification" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Entity_Identification" Text="Add Identification" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Identification_Entry_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                     <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store24" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model33" IDProperty="PK_goAML_Report_Identifications_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Identifications_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="issue_date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="expiry_date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn37" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumn24" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Identification_Entry_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                 <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column101" runat="server" DataIndex="type" Text="Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column102" runat="server" DataIndex="number" Text="Number" Flex="1" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="DateColumn3" runat="server" DataIndex="issue_date" Text="Issue Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:DateColumn ID="DateColumn4" runat="server" DataIndex="expiry_date" Text="Expired Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column103" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                         <%--Sanction--%>
                        <ext:GridPanel ID="gp_sanction_entity" runat="server" AutoScroll="true" Title="Sanction" Border="true" EmptyText="No data available" MarginSpec="10 0">
                            <View>
                                <ext:GridView runat="server" EnableTextSelection="true" />
                            </View>
                            <TopBar>
                                <ext:Toolbar runat="server">
                                    <Items>
                                        <ext:Button runat="server" ID="btn_Sanction_Entity" Text="Add Sanction" Icon="Add">
                                            <DirectEvents>
                                                <Click OnEvent="btn_Sanction_Add_Click">
                                                    <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="persontype" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Items>
                                </ext:Toolbar>
                            </TopBar>
                            <Store>
                                <ext:Store ID="store28" runat="server">
                                    <Model>
                                        <ext:Model runat="server" ID="Model40" IDProperty="PK_goAML_Report_Sanction_ID">
                                            <Fields>
                                                <ext:ModelField Name="PK_goAML_Report_Sanction_ID" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="provider" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="sanction_list_name" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="match_criteria" Type="String"></ext:ModelField>
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:RowNumbererColumn ID="RowNumbererColumn41" runat="server" Text="No"></ext:RowNumbererColumn>
                                       <ext:CommandColumn ID="CommandColumn28" runat="server" Text="Action" MinWidth="220">
                                        <Commands>
                                            <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                                <ToolTip Text="Edit"></ToolTip>
                                            </ext:GridCommand>
                                            <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                                <ToolTip Text="Detail"></ToolTip>
                                            </ext:GridCommand>
                                            <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                                <ToolTip Text="Delete"></ToolTip>
                                            </ext:GridCommand>
                                        </Commands>
                                        <DirectEvents>
                                            <Command OnEvent="gc_sanction">
                                                <EventMask ShowMask="true"></EventMask>
                                                <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                     <ext:Parameter Name="parenttable" Value="'Entity'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Command>
                                        </DirectEvents>
                                    </ext:CommandColumn>
                                    <ext:Column ID="Column113" runat="server" DataIndex="provider" Text="Provider" Flex="1"></ext:Column>
                                    <ext:Column ID="Column114" runat="server" DataIndex="sanction_list_name" Text="Sanction Name" Flex="1"></ext:Column>
                                    <ext:Column ID="Column115" runat="server" DataIndex="match_criteria" Text="Match Criteria" Flex="1"></ext:Column>
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>

                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_Entity_Comment" FieldLabel="Notes" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"  />
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Entity_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Entity_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Entity_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Entity_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.95});" />
            <Resize Handler="#{window_EntityParty}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF ENTITY PARTY ENTRY WINDOWS ========================== --%>


    <%-- ================== PHONE ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_Phone" Layout="AnchorLayout" Title="Phone" runat="server" Maximizable="true" Modal="true" Hidden="true" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:Panel runat="server" ID="pnl_Phone" MarginSpec="0 0 10 0" Layout="AnchorLayout" BodyPadding="20" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_Phone_ContactType" Label="Contact Type" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Kategori_Kontak" AnchorHorizontal="100%"  />
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_Phone_CommunicationType" Label="Communication Type" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Jenis_Alat_Komunikasi" AnchorHorizontal="100%"  />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Phone_CountryPrefix" FieldLabel="Country Prefix" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="4" />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Phone_Number" FieldLabel="Phone Number" AnchorHorizontal="100%" EnableKeyEvents="true" MaskRe="[0-9]" EnforceMaxLength="True" maxlength="50" />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Phone_Extension" FieldLabel="Extension" AnchorHorizontal="100%" EnableKeyEvents="true" MaskRe="[0-9]" EnforceMaxLength="True" maxlength="50" />
                    <ext:TextArea runat="server" LabelWidth="150" ID="txt_Phone_Comment" FieldLabel="Notes" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="4000"  />
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Phone_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Phone_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Phone_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Phone_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.6, height: size.height * 0.8});" />
            <Resize Handler="#{window_Phone}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF PHONE ENTRY WINDOWS ========================== --%>


    <%-- ================== ADDRESS ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_Address" Layout="AnchorLayout" Title="Address" runat="server" Modal="true" Hidden="true" Maximizable="true" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:Panel runat="server" ID="pnl_Address" MarginSpec="0 0 10 0" Layout="AnchorLayout" BodyPadding="20" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_Address_Type" Label="Address Type" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Kategori_Kontak" AnchorHorizontal="100%"  />
                    <ext:TextArea runat="server" LabelWidth="150" ID="txt_Address_Address" FieldLabel="Address" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="100"  />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Address_Town" FieldLabel="Town" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Address_City" FieldLabel="City" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Address_Zip" FieldLabel="Zip Code" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="10" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_Address_Country" Label="Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="100%" OnOnValueChanged="cmb_Address_Country_Changed" />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Address_State" FieldLabel="Province" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <ext:TextArea runat="server" LabelWidth="150" ID="txt_Address_Comment" FieldLabel="Notes" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="4000"  />
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Address_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Address_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Address_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Address_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.6, height: size.height * 0.95});" />
            <Resize Handler="#{window_Address}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF ADDRESS ENTRY WINDOWS ========================== --%>


    <%-- ================== IDENTIFICATION ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_Identification" Layout="AnchorLayout" Title="Identification" runat="server" Modal="true" Hidden="true" Maximizable="true" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:Panel runat="server" ID="pnl_Identification" MarginSpec="0 0 10 0" Layout="AnchorLayout" BodyPadding="20" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_Identification_Type" Label="Identification Type" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Jenis_Dokumen_Identitas" AnchorHorizontal="100%"  />
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_Identification_Entity_Type" Label="Identification Type" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Jenis_Dokumen_Identitas_Entity" AnchorHorizontal="100%"  /> <!-- Add 11-Sep-2023, Felix. Untuk Entity -->
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Identification_Number" FieldLabel="Identification Number" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <ext:DateField runat="server" LabelWidth="150" ID="txt_Identification_IssueDate" FieldLabel="Issue Date" Format="dd-MMM-yyyy" AnchorHorizontal="60%" />
                    <ext:DateField runat="server" LabelWidth="150" ID="txt_Identification_ExpiredDate" FieldLabel="Expired Date" Format="dd-MMM-yyyy" AnchorHorizontal="60%" />
                    <ext:TextField runat="server" LabelWidth="150" ID="txt_Identification_IssueBy" FieldLabel="Issue By" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="255" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_Identification_Country" Label="Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="100%"  />
                    <ext:TextArea runat="server" LabelWidth="150" ID="txt_Identification_Comment" FieldLabel="Notes" AnchorHorizontal="100%" EnforceMaxLength="True" maxlength="4000"  />
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Identification_Save" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">
                <DirectEvents>
                    <Click OnEvent="btn_Identification_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Identification_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Identification_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.6, height: size.height * 0.8});" />
            <Resize Handler="#{Window_Identification}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF IDENTIFICATION ENTRY WINDOWS ========================== --%>


    <%-- ================== MULTIPARTY ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_MultiParty" Layout="AnchorLayout" Title="T-Party" runat="server" Modal="true" Hidden="true" Maximizable="true" BodyPadding="10" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:Panel runat="server" ID="Panel1" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_TParty_IsMyClient" FieldLabel="My Client"></ext:Checkbox>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_TParty_Role" Label="Role" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goaml_ref_party_role" AnchorHorizontal="70%"/>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_TParty_FundsCode" Label="Transaction Instrument" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Instrumen_TransaksiParty" AnchorHorizontal="70%"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_TParty_FundsComment" FieldLabel="Other Instrument" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255"/>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_TParty_CurrencyCode" Label="Foreign Currency" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Mata_Uang" AnchorHorizontal="70%" OnOnValueChanged="cmb_TParty_CurrencyCode_Changed"/>
                    <ext:NumberField runat="server" LabelWidth="200" ID="txt_TParty_ForeignCurrencyAmount" FieldLabel="Foreign Currency Amount" AnchorHorizontal="70%" MouseWheelEnabled="false" />
                    <ext:NumberField runat="server" LabelWidth="200" ID="txt_TParty_ForeignCurrencyExrate" FieldLabel="Exchange Rate" AnchorHorizontal="70%" MouseWheelEnabled="false" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_TParty_Country" Label="Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"  />
                    <ext:NumberField runat="server" LabelWidth="200" ID="txt_TParty_Significance" FieldLabel="Significance" AnchorHorizontal="70%" MouseWheelEnabled="false" MinValue="0" MaxValue="10" />
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_TParty_Comment" FieldLabel="Notes" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"  />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_TParty_Information" Label="Party Type" ValueField="PK_Sender_To_Information" DisplayField="Sender_To_Information" StringField="PK_Sender_To_Information,Sender_To_Information" StringTable="GoAML_Ref_Sender_To_Information" AnchorHorizontal="70%" />

                    <ext:FieldSet runat="server" ColumnWidth="0.5" ID="FieldSet4" Layout="AnchorLayout" Padding="10">
                        <Content>
                            <ext:DisplayField runat="server" LabelWidth="150" ID="txt_TParty_CIFNO_ACCOUNTNO" FieldLabel="CIF | Account No." AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:DisplayField runat="server" LabelWidth="150" ID="df_TParty_Name" FieldLabel="Name" AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:TextField runat="server" LabelWidth="150" ID="txt_TParty_Name" FieldLabel="Name" Hidden="true" AnchorHorizontal="100%"></ext:TextField>
                            <ext:DisplayField runat="server" LabelWidth="150" ID="txt_TParty_Address" FieldLabel="Address" Hidden="true" AnchorHorizontal="100%"></ext:DisplayField>

                            <ext:Button ID="btn_TParty_Edit" runat="server" Icon="Pencil" Text="Edit" MarginSpec="0 10 0 0">
                                <DirectEvents>
                                    <Click OnEvent="btn_PartyEntry_Click">
                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Edit'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="partyside" Value="'TParty'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btn_TParty_Detail" runat="server" Icon="ApplicationViewDetail" Text="Detail">
                                <DirectEvents>
                                    <Click OnEvent="btn_PartyEntry_Click">
                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Detail'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="partyside" Value="'TParty'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Content>
                    </ext:FieldSet>
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_MultiParty_Save" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">
                <DirectEvents>
                    <Click OnEvent="btn_MultiParty_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_MultiParty_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_MultiParty_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.95});" />
            <Resize Handler="#{Window_MultiParty}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF MULTIPARTY ENTRY WINDOWS ========================== --%>


    <%-- ================== ACTIVITY ENTRY WINDOWS ========================== --%>
    <ext:Window ID="window_Activity" Layout="AnchorLayout" Title="Activity" runat="server" Modal="true" Hidden="true" Maximized="true" BodyPadding="10" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:InfoPanel ID="info_ValidationResultActivity" ClientIDMode="Static" runat="server" UI="Warning" Layout="AnchorLayout" Title="Validation Result" AutoScroll="true" IconCls="#Error" MarginSpec="10 0" Border="True" Hidden="true"></ext:InfoPanel>

            <ext:Panel runat="server" ID="Panel2" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Activity_Information" Label="Party Type" ValueField="PK_Sender_To_Information" DisplayField="Sender_To_Information" StringField="PK_Sender_To_Information,Sender_To_Information" StringTable="GoAML_Ref_Sender_To_Information" AnchorHorizontal="70%" OnOnValueChanged="cmb_Activity_Information_Changed" />
                    <ext:NumberField runat="server" LabelWidth="200" ID="txt_Activity_Significance" FieldLabel="Significance" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Activity_Reason" FieldLabel="Reason" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"/>
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_Activity_Comment" FieldLabel="Notes" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"  />

                    <ext:FieldSet runat="server" ColumnWidth="0.5" ID="FieldSet5" Layout="AnchorLayout" Padding="10" MarginSpec="20 0 0 0" Title="Party Information">
                        <Content>
                            <ext:DisplayField runat="server" LabelWidth="150" ID="txt_Activity_CIFNO_ACCOUNTNO" FieldLabel="CIF | Account No." AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:DisplayField runat="server" LabelWidth="150" ID="df_Activity_Name" FieldLabel="Name" AnchorHorizontal="100%"></ext:DisplayField>
                            <ext:TextField runat="server" LabelWidth="150" ID="txt_Activity_Name" FieldLabel="Name" Hidden="true" AnchorHorizontal="100%"></ext:TextField>
                            <ext:DisplayField runat="server" LabelWidth="150" ID="txt_Activity_Address" FieldLabel="Address" Hidden="true" AnchorHorizontal="100%"></ext:DisplayField>

                            <ext:Button ID="btn_Activity_Edit" runat="server" Icon="Pencil" Text="Edit" MarginSpec="0 10 0 0">
                                <DirectEvents>
                                    <Click OnEvent="btn_PartyEntry_Click">
                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Edit'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="partyside" Value="'Activity'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btn_Activity_Detail" runat="server" Icon="ApplicationViewDetail" Text="Detail">
                                <DirectEvents>
                                    <Click OnEvent="btn_PartyEntry_Click">
                                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Detail'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="partyside" Value="'Activity'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Content>
                    </ext:FieldSet>
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Activity_Save" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">
                <DirectEvents>
                    <Click OnEvent="btn_Activity_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Activity_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Activity_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.95});" />
            <Resize Handler="#{Window_Activity}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF ACTIVITY ENTRY WINDOWS ========================== --%>


    <%-- ================== INDICATOR ENTRY WINDOWS ========================== --%>
    <ext:Window ID="Window_Indicator" Layout="AnchorLayout" Title="Report Indicator" runat="server" Modal="true" Hidden="true" Maximizable="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
		   <ext:Panel ID="pnl_Cmb_Indicator" runat="server" Layout="AnchorLayout" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField ID="cmb_Indicator" LabelWidth="120" ValueField="kode" DisplayField="keterangan" runat="server" StringField="kode,keterangan" StringTable="vw_goaml_ref_indicator_laporan" StringFilter="active=1" Label="Report Indicator" AnchorHorizontal="100%" AllowBlank="false" />
                </Content>
            </ext:Panel>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.8, height: size.height * 0.5});" />
            <Resize Handler="#{Window_Indicator}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_Indicator_Save" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">
                <DirectEvents>
                    <Click OnEvent="btn_Indicator_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Indicator_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Indicator_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%-- ================== END OF INDICATOR ENTRY WINDOWS ========================== --%>


    <%-- ================== IMPORT PARTY WINDOWS ========================== --%>
    <ext:Window ID="window_Import_Party" runat="server" Modal="true" Hidden="true" BodyStyle="padding:0px" AutoScroll="true" ButtonAlign="Center" MinHeight="430">
        <Items>
            <%-- Grid Panel Account --%>
            <ext:GridPanel ID="gp_Import_Party_Account" runat="server" ClientIDMode="Static" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                    <ext:Store ID="store_Import_Party_Account" runat="server" IsPagingStore="true" RemoteFilter="true"  RemoteSort="true" 
                        OnReadData="store_ReadData_Import_Party_Account" RemotePaging="true" ClientIDMode="Static" PageSize="10">
                        <Model>
                            <ext:Model runat="server" ID="model_Import_Party_Account" IDProperty="Account_No">
                                <Fields>
                                    <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Account_No" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="Account_Name" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters></Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="filterHeader_Import_Party_Account" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumber_Import_Party_Account" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="col_Import_Party_Account_CIF" runat="server" DataIndex="CIF" Text="CIF" Width="150"></ext:Column>
                        <ext:Column ID="col_Import_Party_Account_AccNo" runat="server" DataIndex="Account_No" Text="Account No" Width="150" ></ext:Column>
                        <ext:Column ID="col_Import_Party_Account_Name" runat="server" DataIndex="Account_Name" Text="Name" Flex="1"></ext:Column>
                        <ext:CommandColumn ID="cc_Import_Party_Account" runat="server" Text="Action">

                        <Commands>
                            <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
                                <ToolTip Text="Select"></ToolTip>
                            </ext:GridCommand>
                        </Commands>

                        <DirectEvents>
                            <Command OnEvent="gc_Import_Party_Account">
                                <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="200"></EventMask>
                                <ExtraParams>
                                    <ext:Parameter Name="Account_No" Value="record.data.Account_No" Mode="Raw"></ext:Parameter>
                                    <ext:Parameter Name="CIF" Value="record.data.CIF" Mode="Raw"></ext:Parameter>
                                </ExtraParams>
                            </Command>
                        </DirectEvents>
                    </ext:CommandColumn>
                </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_Import_Party_Account" runat="server" HideRefresh="false" >
                        <Items>  
                            <%--<ext:Button runat="server" Text="Close Picker" ID="Button5">
                                <Listeners>
                                    <Click Handler="#{window_Import_Party}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>--%>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>

            <%-- Grid Panel WIC Individual --%>
            <ext:GridPanel ID="gp_Import_Party_WIC_Individual" runat="server" ClientIDMode="Static" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="false" />
                </View>
                <Store>
                    <ext:Store ID="store_Import_Party_WIC_Individual" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" 
                        OnReadData="store_ReadData_Import_Party_WIC_Individual" RemotePaging="true" ClientIDMode="Static" PageSize="10">
                <Model>
                    <ext:Model runat="server" ID="model_Import_Party_WIC_Individual" IDProperty="PK_Customer_ID">
                        <Fields>
                            <ext:ModelField Name="PK_Customer_ID" Type="String"></ext:ModelField>
                            <ext:ModelField Name="WIC_No" Type="String"></ext:ModelField>
                            <ext:ModelField Name="FK_Customer_Type_ID" Type="String"></ext:ModelField>
                            <ext:ModelField Name="WIC_Type" Type="String"></ext:ModelField>
                            <ext:ModelField Name="NIK" Type="String"></ext:ModelField>
                            <ext:ModelField Name="TanggalLahir" Type="String"></ext:ModelField>
                            <ext:ModelField Name="NamaLengkap" Type="String"></ext:ModelField>
                        </Fields>
                    </ext:Model>
                </Model>
                        <Sorters>
                        </Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="filterHeader_Import_Party_WIC_Individual" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumber_Import_Party_WIC_Individual" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="col_Import_Party_WIC_Individual_PK" runat="server" DataIndex="PK_Customer_ID" Text="PK_Customer_ID" Flex="1" Hidden="true"></ext:Column>
                        <ext:Column ID="col_Import_Party_WIC_Individual_No" runat="server" DataIndex="WIC_No" Text="WIC No" Flex="1"></ext:Column>
                        <ext:Column ID="col_Import_Party_WIC_Individual_Type" runat="server" DataIndex="WIC_Type" Text="WIC Type" Flex="1"></ext:Column>
                        <ext:Column ID="col_Import_Party_WIC_Individual_NIK" runat="server" DataIndex="NIK" Text="NIK" Width="150"></ext:Column>
                        <ext:DateColumn ID="col_Import_Party_WIC_Individual_TanggalLahir" runat="server" DataIndex="TanggalLahir" Text="Tanggal Lahir" Flex="1" Format="dd-MMM-Y"></ext:DateColumn>
                        <ext:Column ID="col_Import_Party_WIC_Individual_Name" runat="server" DataIndex="NamaLengkap" Text="Name" Width="300"></ext:Column>
                        <ext:CommandColumn ID="cc_Import_Party_WIC_Individual" runat="server" Text="Action">
                            <Commands>
                                <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
                                    <ToolTip Text="Select"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_Import_Party_WIC">
                                    <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="200"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="PK_Customer_ID" Value="record.data.PK_Customer_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="WIC_No" Value="record.data.WIC_No" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_Import_Party_WIC_Individual" runat="server" HideRefresh="false" >
                        <Items>  
                            <%--<ext:Button runat="server" Text="Close Picker" ID="Button6">
                                <Listeners>
                                    <Click Handler="#{window_Import_Party}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>--%>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            
            <%-- Grid Panel WIC Corporate --%>
            <ext:GridPanel ID="gp_Import_Party_WIC_Corporate" runat="server" ClientIDMode="Static" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="false" />
                </View>
                <Store>
                    <ext:Store ID="store_Import_Party_WIC_Corporate" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" 
                        OnReadData="store_ReadData_Import_Party_WIC_Corporate" RemotePaging="true" ClientIDMode="Static" PageSize="10">
                <Model>
                    <ext:Model runat="server" ID="model_Import_Party_WIC_Corporate" IDProperty="PK_Customer_ID">
                        <Fields>
                            <ext:ModelField Name="PK_Customer_ID" Type="String"></ext:ModelField>
                            <ext:ModelField Name="WIC_No" Type="String"></ext:ModelField>
                            <ext:ModelField Name="FK_Customer_Type_ID" Type="String"></ext:ModelField>
                            <ext:ModelField Name="WIC_Type" Type="String"></ext:ModelField>
                            <ext:ModelField Name="NIK" Type="String"></ext:ModelField>
                            <ext:ModelField Name="TanggalLahir" Type="String"></ext:ModelField>
                            <ext:ModelField Name="NamaLengkap" Type="String"></ext:ModelField>
                        </Fields>
                    </ext:Model>
                </Model>
                        <Sorters>
                        </Sorters>
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="filterHeader_Import_Party_WIC_Corporate" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumber_Import_Party_WIC_Corporate" runat="server" Text="No"></ext:RowNumbererColumn>
                        <ext:Column ID="col_Import_Party_WIC_Corporate_PK" runat="server" DataIndex="PK_Customer_ID" Text="PK_Customer_ID" Flex="1" Hidden="true"></ext:Column>
                        <ext:Column ID="col_Import_Party_WIC_Corporate_No" runat="server" DataIndex="WIC_No" Text="WIC No" Flex="1"></ext:Column>
                        <ext:Column ID="col_Import_Party_WIC_Corporate_Type" runat="server" DataIndex="WIC_Type" Text="WIC Type" Flex="1"></ext:Column>
                        <ext:Column ID="col_Import_Party_WIC_Corporate_NIK" runat="server" DataIndex="NIK" Text="NIK" Width="150"></ext:Column>
                        <ext:DateColumn ID="col_Import_Party_WIC_Corporate_TanggalLahir" runat="server" DataIndex="TanggalLahir" Text="Tanggal Lahir" Flex="1" Format="dd-MMM-Y"></ext:DateColumn>
                        <ext:Column ID="col_Import_Party_WIC_Corporate_Name" runat="server" DataIndex="NamaLengkap" Text="Name" Width="300"></ext:Column>
                        <ext:CommandColumn ID="cc_Import_Party_WIC_Corporate" runat="server" Text="Action">
                            <Commands>
                                <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
                                    <ToolTip Text="Select"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_Import_Party_WIC">
                                    <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="200"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="PK_Customer_ID" Value="record.data.PK_Customer_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="WIC_No" Value="record.data.WIC_No" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_Import_Party_WIC_Corporate" runat="server" HideRefresh="false" >
                        <Items>  
                            <%--<ext:Button runat="server" Text="Close Picker" ID="Button1">
                                <Listeners>
                                    <Click Handler="#{window_Import_Party}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>--%>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>

            <%-- Grid Panel Customer Individual --%>
            <ext:GridPanel ID="gp_Import_Party_Customer_Individual" runat="server" ClientIDMode="Static" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="false" />
                </View>
                <Store>
                    <ext:Store ID="store_Import_Party_Customer_Individual" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" 
                        OnReadData="store_ReadData_Import_Party_Customer_Individual" RemotePaging="true" ClientIDMode="Static" PageSize="10">
		                <Model>
		                    <ext:Model runat="server" ID="model_Import_Party_Customer_Individual" IDProperty="PK_Customer_ID">
		                        <Fields>
		                            <ext:ModelField Name="PK_Customer_ID" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="FK_Customer_Type_ID" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="Customer_Type" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="NIK" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="TanggalLahir" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="NamaLengkap" Type="String"></ext:ModelField>
		                        </Fields>
		                    </ext:Model>
		                </Model>
		                <Sorters></Sorters>
		                <Proxy>
		                    <ext:PageProxy />
		                </Proxy>
	                </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="filterHeader_Import_Party_Customer_Individual" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
		                <ext:RowNumbererColumn ID="RowNumber_Import_Party_Customer_Individual" runat="server" Text="No"></ext:RowNumbererColumn>
		                <ext:Column ID="col_Import_Party_Customer_Individual_PK" runat="server" DataIndex="PK_Customer_ID" Text="PK_Customer_ID" Flex="1" Hidden="true"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Individual_CIF" runat="server" DataIndex="CIF" Text="CIF" Flex="1"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Individual_Type" runat="server" DataIndex="Customer_Type" Text="Customer Type" Flex="1"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Individual_NIK" runat="server" DataIndex="NIK" Text="NIK" Width="150"></ext:Column>
		                <ext:DateColumn ID="col_Import_Party_Customer_Individual_TanggalLahir" runat="server" DataIndex="TanggalLahir" Text="Tanggal Lahir" Flex="1" Format="dd-MMM-Y"></ext:DateColumn>
		                <ext:Column ID="col_Import_Party_Customer_Individual_Name" runat="server" DataIndex="NamaLengkap" Text="Name" Width="300"></ext:Column>
		                <ext:CommandColumn ID="cc_Import_Party_Customer_Individual" runat="server" Text="Action">
	                        <Commands>
	                            <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
	                                <ToolTip Text="Select"></ToolTip>
	                            </ext:GridCommand>
	                        </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_Import_Party_Customer">
                                    <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="200"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="PK_Customer_ID" Value="record.data.PK_Customer_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="CIF" Value="record.data.CIF" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_Import_Party_Customer_Individual" runat="server" HideRefresh="false" >
                        <Items>  
                            <%--<ext:Button runat="server" Text="Close Picker" ID="Button7">
                                <Listeners>
                                    <Click Handler="#{window_Import_Party}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>--%>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>

            <%-- Grid Panel Customer Corporate --%>
            <ext:GridPanel ID="gp_Import_Party_Customer_Corporate" runat="server" ClientIDMode="Static" Hidden="true">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="false" />
                </View>
                <Store>
                    <ext:Store ID="store_Import_Party_Customer_Corporate" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" 
                        OnReadData="store_ReadData_Import_Party_Customer_Corporate" RemotePaging="true" ClientIDMode="Static" PageSize="10">
		                <Model>
		                    <ext:Model runat="server" ID="model_Import_Party_Customer_Corporate" IDProperty="PK_Customer_ID">
		                        <Fields>
		                            <ext:ModelField Name="PK_Customer_ID" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="CIF" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="FK_Customer_Type_ID" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="Customer_Type" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="NIK" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="TanggalLahir" Type="String"></ext:ModelField>
		                            <ext:ModelField Name="NamaLengkap" Type="String"></ext:ModelField>
		                        </Fields>
		                    </ext:Model>
		                </Model>
		                <Sorters></Sorters>
		                <Proxy>
		                    <ext:PageProxy />
		                </Proxy>
	                </ext:Store>
                </Store>
                <Plugins>
                    <ext:FilterHeader ID="filterHeader_Import_Party_Customer_Corporate" runat="server" Remote="true"></ext:FilterHeader>
                </Plugins>
                <ColumnModel>
                    <Columns>
		                <ext:RowNumbererColumn ID="RowNumber_Import_Party_Customer_Corporate" runat="server" Text="No"></ext:RowNumbererColumn>
		                <ext:Column ID="col_Import_Party_Customer_Corporate_PK" runat="server" DataIndex="PK_Customer_ID" Text="PK_Customer_ID" Flex="1" Hidden="true"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Corporate_CIF" runat="server" DataIndex="CIF" Text="CIF" Flex="1"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Corporate_Type" runat="server" DataIndex="Customer_Type" Text="Customer Type" Flex="1"></ext:Column>
		                <ext:Column ID="col_Import_Party_Customer_Corporate_NIK" runat="server" DataIndex="NIK" Text="NIK" Width="150"></ext:Column>
		                <ext:DateColumn ID="col_Import_Party_Customer_Corporate_TanggalLahir" runat="server" DataIndex="TanggalLahir" Text="Tanggal Lahir" Flex="1" Format="dd-MMM-Y"></ext:DateColumn>
		                <ext:Column ID="col_Import_Party_Customer_Corporate_Name" runat="server" DataIndex="NamaLengkap" Text="Name" Width="300"></ext:Column>
		                <ext:CommandColumn ID="cc_Import_Party_Customer_Corporate" runat="server" Text="Action">
	                        <Commands>
	                            <ext:GridCommand Text="Select" CommandName="Select" Icon="Accept" MinWidth="70">
	                                <ToolTip Text="Select"></ToolTip>
	                            </ext:GridCommand>
	                        </Commands>
                            <DirectEvents>
                                <Command OnEvent="gc_Import_Party_Customer">
                                    <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="200"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="PK_Customer_ID" Value="record.data.PK_Customer_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="CIF" Value="record.data.CIF" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="pt_Import_Party_Customer_Corporate" runat="server" HideRefresh="false" >
                        <Items>  
                            <%--<ext:Button runat="server" Text="Close Picker" ID="Button2">
                                <Listeners>
                                    <Click Handler="#{window_Import_Party}.hide()"> </Click>
                                </Listeners>
                            </ext:Button>--%>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>

        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.9});" />
            <Resize Handler="#{window_Import_Party}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_Import_Party_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Import_Party_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <%-- ================== END OF IMPORT PARTY WINDOWS ========================== --%>


    <%-- ================== CONFIRMATION PANEL ========================== --%>
    <ext:FormPanel ID="pnl_Confirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="lbl_Confirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Confirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <%-- ================== END OF CONFIRMATION PANEL ========================== --%>

           <%-- ================== Related Entity WINDOWS ========================== --%>
    <ext:Window ID="window_related_entity" Title="Related Entity" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
            <ext:Button ID="btn_RE_Import_Customer" runat="server" Icon="Zoom" Text="Search Data Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_RE_Import_Customer_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_RE_Import_WIC" runat="server" Icon="Zoom" Text="Search Data Non Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_RE_Import_WIC_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

            <%-- Person Information --%>
            <ext:Panel runat="server" ID="Panel3" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_RE_entityrelation" Label="Entity Relation" ValueField="PK_goAML_Ref_Account_Related_Entity_ID" DisplayField="ACCOUNT_ENTITY_RELATION" StringField="PK_goAML_Ref_Account_Related_Entity_ID,ACCOUNT_ENTITY_RELATION" StringTable="goAML_Ref_Account_Related_Entity" AnchorHorizontal="70%"   />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RE_Notes_Relation" FieldLabel="Notes" EnforceMaxLength="True" maxlength="30" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RE_CorporateName" FieldLabel="Corporate Name" EnforceMaxLength="True" maxlength="100" AnchorHorizontal="70%" />
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_RE_CommercialName" FieldLabel="Commercial Name" AnchorHorizontal="60%" Format="dd-MMM-yyyy" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_RE_LegalForm" Label="Legal Form" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Bentuk_Badan_Usaha" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RE_CorporateLicenseNumber" FieldLabel="Corporate License Number" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RE_LineOfBusiness" FieldLabel="Line Of Business" EnforceMaxLength="True" maxlength="100" AnchorHorizontal="70%" />
                    
                    <%-- Entity Address --%>
                    <ext:GridPanel ID="gp_RE_Address" runat="server" AutoScroll="true" Title="Address" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_RE_Address" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_RE_Address_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store4" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model5" IDProperty="PK_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AddressType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn14" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumn4" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_RE_Address_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column44" runat="server" DataIndex="AddressType" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column45" runat="server" DataIndex="Address" Text="Address" Flex="1"></ext:Column>
                                <ext:Column ID="Column46" runat="server" DataIndex="City" Text="City" Flex="1"></ext:Column>
                                <ext:Column ID="Column47" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Entity Phone --%>
                    <ext:GridPanel ID="gp_RE_Phone" runat="server" AutoScroll="true" Title="Phone" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_RE_Phone_Entry" Text="Add Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_RE_Phone_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store5" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model6" IDProperty="PK_Phone_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Phone_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ContactType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn15" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumn5" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_RE_Phone_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Phone_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column48" runat="server" DataIndex="ContactType" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column49" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" Flex="1"></ext:Column>
                                <ext:Column ID="Column50" runat="server" DataIndex="tph_number" Text="Phone Number" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                      <%-- Email --%>
                    <ext:GridPanel ID="gp_email_relatedentity" runat="server" AutoScroll="true" Title="Email" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="Button3" Text="Add Email" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Account_RelatedEntity_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="parenttable" Value="'Person'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store31" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model44" IDProperty="PK_goAML_Report_Email_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Email_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="email_address" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn45" runat="server" Text="No"></ext:RowNumbererColumn>
                                      <ext:CommandColumn ID="CommandColumn31" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Account_Entity_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <%-- 2023-11-17, Nael: PK yg benar adalah "PK_goAML_Report_Email_ID" --%>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Email_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="parenttable" Value="'Person'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column122" runat="server" DataIndex="email_address" Text="Email Address" Flex="1"></ext:Column>
                          
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Website --%>
                    <ext:GridPanel ID="gp_website_relatedentity" runat="server" AutoScroll="true" Title="Website" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="Button12" Text="Add Email" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Website_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="parenttable" Value="'Person'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store32" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model45" IDProperty="PK_goAML_Report_Url_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Url_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="url" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn46" runat="server" Text="No"></ext:RowNumbererColumn>
                                      <ext:CommandColumn ID="CommandColumn32" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_website">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <%-- 2023-11-17, Nael: PK yg benar adalah "PK_goAML_Report_Url_ID" --%>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Url_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="parenttable" Value="'Person'" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column123" runat="server" DataIndex="url" Text="URL" Flex="1"></ext:Column>
                          
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RE_state" FieldLabel="State/Province" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255"  />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_RE_Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" Label="Country" AnchorHorizontal="70%"  />
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_RE_DateOfEstablishment" FieldLabel="Date of Establishment" AnchorHorizontal="40%" Format="dd-MMM-yyyy" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_RE_IsClosed" FieldLabel="Is Closed">
                        <DirectEvents>
                            <Change OnEvent="chk_RE_IsClosed_Changed"></Change>
                        </DirectEvents>
                    </ext:Checkbox>
                    <ext:DateField runat="server" LabelWidth="200" Hidden="true" ID="txt_RE_ClosingDate" FieldLabel="Closing Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RE_TaxNumber" FieldLabel="Tax Number" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="100"  />
                    
                        <%-- Entity Identification --%>
                    <ext:GridPanel ID="gp_RE_Identification" runat="server" AutoScroll="true" Title="Entity Identification" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_RE_Identification_RelatedEntity" Text="Add Identification" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_RE_Identification_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store6" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model7" IDProperty="PK_goAML_Report_Identifications_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Report_Identifications_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="issue_date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="expirty_date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn16" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumn6" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_RE_Identification_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Identifications_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column51" runat="server" DataIndex="type" Text="Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column52" runat="server" DataIndex="number" Text="Number" Flex="1" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="issue_date" Text="Issue Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="expirty_date" Text="Expired Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column53" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                          <%--Sanction--%>
                        <ext:GridPanel ID="gp_RE_Sanction" runat="server" AutoScroll="true" Title="Sanction" Border="true" EmptyText="No data available" MarginSpec="10 0">
                            <View>
                                <ext:GridView runat="server" EnableTextSelection="true" />
                            </View>
                            <TopBar>
                                <ext:Toolbar runat="server">
                                    <Items>
                                        <ext:Button runat="server" ID="btn_Sanction_RelatedEntity" Text="Add Sanction" Icon="Add">
                                            <DirectEvents>
                                                <Click OnEvent="btn_Sanction_Add_Click">
                                                    <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                    <ExtraParams>
                                                        <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                        <ext:Parameter Name="persontype" Value="'Sanction'" Mode="Raw"></ext:Parameter>
                                                    </ExtraParams>
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Items>
                                </ext:Toolbar>
                            </TopBar>
                            <Store>
                                <ext:Store ID="store7" runat="server">
                                    <Model>
                                        <ext:Model runat="server" ID="Model8" IDProperty="PK_goAML_Report_Sanction_ID">
                                            <Fields>
                                                <ext:ModelField Name="PK_goAML_Report_Sanction_ID" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="provider" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="sanction_list_name" Type="String"></ext:ModelField>
                                                <ext:ModelField Name="match_criteria" Type="String"></ext:ModelField>
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:RowNumbererColumn ID="RowNumbererColumn17" runat="server" Text="No"></ext:RowNumbererColumn>
                                       <ext:CommandColumn ID="CommandColumn7" runat="server" Text="Action" MinWidth="220">
                                        <Commands>
                                            <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                                <ToolTip Text="Edit"></ToolTip>
                                            </ext:GridCommand>
                                            <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                                <ToolTip Text="Detail"></ToolTip>
                                            </ext:GridCommand>
                                            <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                                <ToolTip Text="Delete"></ToolTip>
                                            </ext:GridCommand>
                                        </Commands>
                                        <DirectEvents>
                                            <Command OnEvent="gc_sanction">
                                                <EventMask ShowMask="true"></EventMask>
                                                <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                                <ExtraParams>
                                                    <%-- 2023-11-17, Nael: Pk yg benar adalah "PK_goAML_Report_Sanction_ID" --%>
                                                    <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Report_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Command>
                                        </DirectEvents>
                                    </ext:CommandColumn>
                                    <ext:Column ID="Column54" runat="server" DataIndex="provider" Text="Provider" Flex="1"></ext:Column>
                                    <ext:Column ID="Column55" runat="server" DataIndex="sanction_list_name" Text="Sanction Name" Flex="1"></ext:Column>
                                    <ext:Column ID="Column56" runat="server" DataIndex="match_criteria" Text="Match Criteria" Flex="1"></ext:Column>
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>
                                        <ext:TextArea runat="server" LabelWidth="200" ID="txt_RE_notes" FieldLabel="Notes" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"  />
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_RE_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_RE_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_RE_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_RE_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.85, height: size.height * 0.8});" />
            <Resize Handler="#{window_PersonParty}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF PERSON PARTY ENTRY WINDOWS ========================== --%>

                      <%-- ================== Related Person WINDOWS ========================== --%>
    <ext:Window ID="window_related_person" Title="Related Person" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
            <ext:Button ID="btn_RP_Import_Customer" runat="server" Icon="Zoom" Text="Search Data Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_RP_Import_Customer_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_RP_Import_WIC" runat="server" Icon="Zoom" Text="Search Data Non Customer" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_RP_Import_WIC_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

            <%-- Person Information --%>
            <ext:Panel runat="server" ID="Panel4" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="Cmb_PersonRelation_RelatedPerson" Label="Person Relation" ValueField="Kode" DisplayField="Keterangan" StringField="Kode,Keterangan" StringTable="goAML_Ref_Person_Person_Relationship" AnchorHorizontal="70%" />
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_RelationNotes_RelatedPerson" FieldLabel="Notes" EnforceMaxLength="True" MaxLength="8000" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Gender_RelatedPerson" FieldLabel="Gender" EnforceMaxLength="True" MaxLength="100" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Title_RelatedPerson" FieldLabel="Title" EnforceMaxLength="True" MaxLength="30" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_FullName_RelatedPerson" FieldLabel="Full Name" EnforceMaxLength="True" MaxLength="100" AnchorHorizontal="70%"  />
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_BirthDate_RelatedPerson" FieldLabel="Date of Birth" AnchorHorizontal="60%" Format="dd-MMM-yyyy" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_BirthPlace_RelatedPerson" FieldLabel="Place of Birth" EnforceMaxLength="True" MaxLength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_MothersName_RelatedPerson" FieldLabel="Mothers Name" EnforceMaxLength="True" MaxLength="100" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Alias_RelatedPerson" FieldLabel="Alias" EnforceMaxLength="True" MaxLength="100" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_SSN_RelatedPerson" FieldLabel="SSN/NIK" EnforceMaxLength="True" MaxLength="16" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_PassportNumber_RelatedPerson" FieldLabel="Passport No" EnforceMaxLength="True" MaxLength="255" AnchorHorizontal="70%" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_PassportCountry_RelatedPerson" Label="Passport Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_IDNumber_RelatedPerson" FieldLabel="Other ID Number" EnforceMaxLength="True" MaxLength="255" AnchorHorizontal="70%" />

                    <%-- Person Address --%>
                    <ext:GridPanel ID="gp_Address_RelatedPerson" runat="server" AutoScroll="true" Title="Address" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Address_RelatedPerson" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Address_RelatedPerson_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store9" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model10" IDProperty="PK_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AddressType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn19" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column61" runat="server" DataIndex="AddressType" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column62" runat="server" DataIndex="Address" Text="Address" Flex="1"></ext:Column>
                                <ext:Column ID="Column63" runat="server" DataIndex="City" Text="City" Flex="1"></ext:Column>
                                <ext:Column ID="Column64" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn9" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Address_RelatedPerson_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Entity Phone --%>
                    <ext:GridPanel ID="Gp_Phone_RelatedPerson" runat="server" AutoScroll="true" Title="Phone" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Phone_RelatedPerson" Text="Add Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Phone_RelatedPerson_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store10" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model11" IDProperty="PK_Phone_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Phone_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ContactType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn20" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column65" runat="server" DataIndex="ContactType" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column66" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" Flex="1"></ext:Column>
                                <ext:Column ID="Column67" runat="server" DataIndex="tph_number" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn10" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Phone_RelatedPerson_click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Phone_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>


                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Nationality1_RelatedPerson" Label="Nationality 1" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" AnchorHorizontal="70%" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Nationality2_RelatedPerson" Label="Nationality 2" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" AnchorHorizontal="70%" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Nationality3_RelatedPerson" Label="Nationality 3" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" AnchorHorizontal="70%" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_Residence_RelatedPerson" Label="Domicile Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="goAML_Ref_Nama_Negara" AnchorHorizontal="70%" />

                    <%-- Email --%>
                    <ext:GridPanel ID="gp_email_relatedperson" runat="server" AutoScroll="true" Title="Email" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Email_RelatedPerson" Text="Add Email" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Email_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store14" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model15" IDProperty="PK_goAML_Ref_Customer_Email_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Email_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="EMAIL_ADDRESS" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn24" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumn14" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_email">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Email_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column78" runat="server" DataIndex="EMAIL_ADDRESS" Text="Contact Type" Flex="1"></ext:Column>

                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%--                    <ext:DateField runat="server" LabelWidth="200" ID="txt_ResidenceSince_RelatedPerson" FieldLabel="Residence Since" AnchorHorizontal="60%" Format="dd-MMM-yyyy" />--%>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Occupation_RelatedPerson" FieldLabel="Occupation" EnforceMaxLength="True" MaxLength="255" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_WorkPlace_RelatedPerson" FieldLabel="Work Place" EnforceMaxLength="True" MaxLength="16" AnchorHorizontal="70%" />

                    <%-- Person Work Place Address --%>
                    <ext:GridPanel ID="gp_Address_WorkPlace_RelatedPerson" runat="server" AutoScroll="true" Title="Address" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Address_WorkPlace_RelatedPerson" Text="Add Address" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Address_RelatedPerson_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="1" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store15" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model16" IDProperty="PK_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Address_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AddressType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="City" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn25" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column79" runat="server" DataIndex="AddressType" Text="Address Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column80" runat="server" DataIndex="Address" Text="Address" Flex="1"></ext:Column>
                                <ext:Column ID="Column81" runat="server" DataIndex="City" Text="City" Flex="1"></ext:Column>
                                <ext:Column ID="Column82" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn15" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Address_RelatedPerson_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="1" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Entity Workplace Phone --%>
                    <ext:GridPanel ID="gp_Phone_Workplace_RelatedPerson" runat="server" AutoScroll="true" Title="Phone" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Phone_WorkPlace_RelatedPerson" Text="Add Phone" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Phone_RelatedPerson_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="1" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store16" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model17" IDProperty="PK_Phone_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Phone_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ContactType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="tph_number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn27" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column83" runat="server" DataIndex="ContactType" Text="Contact Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column84" runat="server" DataIndex="tph_country_prefix" Text="Country Prefix" Flex="1"></ext:Column>
                                <ext:Column ID="Column85" runat="server" DataIndex="tph_number" Text="Phone Number" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn16" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Phone_RelatedPerson_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Phone_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="1" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%-- Person Identification --%>
                    <ext:GridPanel ID="gp_identification_relatedperson" runat="server" AutoScroll="true" Title="Identification" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Identification_RelatedPerson" Text="Add Identification" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Identification_RelatedPerson_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store17" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model20" IDProperty="PK_Identification_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Identification_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Number" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Issue_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Expiry_Date" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn28" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column86" runat="server" DataIndex="Type" Text="Type" Flex="1"></ext:Column>
                                <ext:Column ID="Column87" runat="server" DataIndex="Number" Text="Number" Flex="1" MinWidth="150"></ext:Column>
                                <ext:DateColumn ID="DateColumn5" runat="server" DataIndex="Issue_Date" Text="Issue Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:DateColumn ID="DateColumn6" runat="server" DataIndex="Expiry_Date" Text="Expired Date" Flex="1" Format="dd-MMM-yyyy"></ext:DateColumn>
                                <ext:Column ID="Column91" runat="server" DataIndex="Country" Text="Country" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumn17" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="btn_Identification_RelatedPerson_Click">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_Deceased_RelatedPerson" FieldLabel="Has Passed Away"></ext:Checkbox>
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_Date_Deceased_RelatedPerson" FieldLabel="Date Passed Away" AnchorHorizontal="60%" Format="dd-MMM-yyyy" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_TaxNumber_RelatedPerson" FieldLabel="Tax Number" EnforceMaxLength="True" MaxLength="16" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_SourceOfWealth_RelatedPerson" FieldLabel="Source Of Wealth" EnforceMaxLength="True" MaxLength="255" AnchorHorizontal="70%" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_TaxRegNumber_RelatedPerson" FieldLabel="Is PEP?" EnforceMaxLength="True" maxlength="1" AnchorHorizontal="70%" />

                    <%-- PEP --%>
                    <ext:GridPanel ID="gp_PEP_RelatedPerson" runat="server" AutoScroll="true" Title="Politically Exposed Person" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_PEP_RelatedPerson" Text="Add Email" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_PEP_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store11" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model12" IDProperty="PK_goAML_Ref_Customer_PEP_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_PEP_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PEP_COUNTRY" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FUNCTION_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FUNCTION_DESCRIPTION" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn21" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumn11" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_PEP">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_PEP_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="isemployer" Value="0" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column68" runat="server" DataIndex="PEP_COUNTRY" Text="PEP Country" Flex="1"></ext:Column>
                                <ext:Column ID="Column69" runat="server" DataIndex="FUNCTION_NAME" Text="Function Name" Flex="1"></ext:Column>
                                <ext:Column ID="Column70" runat="server" DataIndex="FUNCTION_DESCRIPTION" Text="Function Description" Flex="1"></ext:Column>

                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <%--Sanction--%>
                    <ext:GridPanel ID="gp_sanction_relatedperson" runat="server" AutoScroll="true" Title="Sanction" Border="true" EmptyText="No data available" MarginSpec="10 0">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Sanction_relatedperson" Text="Add Sanction" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Sanction_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                                <ExtraParams>
                                                    <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                                    <ext:Parameter Name="persontype" Value="'Sanction'" Mode="Raw"></ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store12" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model13" IDProperty="PK_goAML_Ref_Customer_Sanction_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Customer_Sanction_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PROVIDER" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SANCTION_LIST_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="MATCH_CRITERIA" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn22" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumn12" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gc_sanction">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Sanction_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column71" runat="server" DataIndex="PROVIDER" Text="Provider" Flex="1"></ext:Column>
                                <ext:Column ID="Column72" runat="server" DataIndex="SANCTION_LIST_NAME" Text="Sanction Name" Flex="1"></ext:Column>
                                <ext:Column ID="Column73" runat="server" DataIndex="MATCH_CRITERIA" Text="Match Criteria" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>

                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_IsProtected_RelatedPerson" FieldLabel="Is Protected"></ext:Checkbox>
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_notes_relatedperson" FieldLabel="Notes" EnforceMaxLength="True" MaxLength="8000" AnchorHorizontal="70%" />

                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_RelatedPerson_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_RelatedPerson_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_RelatedPerson_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_RelatedPerson_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
                        <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.85, height: size.height * 0.8});" />
            <Resize Handler="#{window_related_person}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF PERSON PARTY ENTRY WINDOWS ========================== --%>

                <%-- ================== Related Account WINDOWS ========================== --%>
    <ext:Window ID="window_related_Account" Title="Related Account" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
           <ext:Button ID="btn_RelatedAccount_Import_Account" runat="server" Icon="Zoom" Text="Search Data Account" MarginSpec="0 10 10 0">
                <DirectEvents>
                    <Click OnEvent="btn_Import_RelatedAccount_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

            <%-- Account General Information --%>
            <ext:Panel runat="server" ID="pnl_RelatedAccount_GeneralInfo" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RelatedAccount_InstitutionName" FieldLabel="Financial Institution Name" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RelatedAccount_InstitutionCode" FieldLabel="Financial Institution Code" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="50"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RelatedAccount_SwiftCode" FieldLabel="Swift Code" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="50"/>
                    <ext:Checkbox runat="server"  LabelWidth="200" ID="chk_RelatedAccount_NonBankingInstitution" FieldLabel="Non Bank Institution"></ext:Checkbox>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RelatedAccount_Branch" FieldLabel="Account Opening Branch" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RelatedAccount_Number" FieldLabel="Account Number" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="50" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_RelatedAccount_CurrencyCode" Label="Account Currency" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Mata_Uang" AnchorHorizontal="70%"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RelatedAccount_Label" FieldLabel="Account Label" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="255"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RelatedAccount_IBAN" FieldLabel="IBAN" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="34" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RelatedAccount_ClientNumber" FieldLabel="Client Number" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="30"/>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_RelatedAccount_Type" Label="Account Type" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Jenis_Rekening" AnchorHorizontal="70%"/>

                    <ext:DateField runat="server" LabelWidth="200" ID="txt_RelatedAccount_OpeningDate" FieldLabel="Opening Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_RelatedAccount_ClosingDate" FieldLabel="Closing Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />
                    <ext:NumberField runat="server" LabelWidth="200" ID="txt_RelatedAccount_Balance" FieldLabel="Balance Amount" AnchorHorizontal="70%" MouseWheelEnabled="false" />
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_RelatedAccount_BalanceDate" FieldLabel="Balance Date" Format="dd-MMM-yyyy" AnchorHorizontal="40%" />
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_RelatedAccount_StatusCode" Label="Account Status" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Status_Rekening" AnchorHorizontal="70%"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RelatedAccount_Beneficiary" FieldLabel="Beneficiary Name" AnchorHorizontal="70%"  EnforceMaxLength="True" maxlength="50"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_RelatedAccount_BeneficiaryComment" FieldLabel="Beneficiary Note" EnforceMaxLength="True" maxlength="255"  AnchorHorizontal="70%" />
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_RelatedAccount_Comment" FieldLabel="Notes" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="4000"  />

                </Content>
            </ext:Panel>

      
                <%--Sanction--%>
       <%--     <ext:GridPanel ID="gp_sanction_relatedaccount" runat="server" AutoScroll="true" Title="Sanction" Border="true" EmptyText="No data available" MarginSpec="10 0">
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <TopBar>
                    <ext:Toolbar runat="server">
                        <Items>
                            <ext:Button runat="server" ID="Button7" Text="Add Sanction" Icon="Add">
                                <DirectEvents>
                                    <Click OnEvent="btn_Sanction_Click">
                                        <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="unikkey" Value="0" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="command" Value="'Add'" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="persontype" Value="'Account'" Mode="Raw"></ext:Parameter>
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="store8" runat="server">
                        <Model>
                            <ext:Model runat="server" ID="Model9" IDProperty="PK_goAML_Ref_Customer_Sanction_ID">
                                <Fields>
                                    <ext:ModelField Name="PK_goAML_Ref_Customer_Sanction_ID" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="PROVIDER" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="SANCTION_LIST_NAME" Type="String"></ext:ModelField>
                                    <ext:ModelField Name="MATCH_CRITERIA" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn18" runat="server" Text="No"></ext:RowNumbererColumn>
                           <ext:CommandColumn ID="CommandColumn8" runat="server" Text="Action" MinWidth="220">
                            <Commands>
                                <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                    <ToolTip Text="Edit"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail"></ToolTip>
                                </ext:GridCommand>
                                <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                    <ToolTip Text="Delete"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="btn_Sanction_Click">
                                    <EventMask ShowMask="true"></EventMask>
                                    <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Customer_Sanction_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="persontype" Value="'Account'" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                        <ext:Column ID="Column57" runat="server" DataIndex="PROVIDER" Text="Provider" Flex="1"></ext:Column>
                        <ext:Column ID="Column58" runat="server" DataIndex="SANCTION_LIST_NAME" Text="Sanction Name" Flex="1"></ext:Column>
                        <ext:Column ID="Column59" runat="server" DataIndex="MATCH_CRITERIA" Text="Match Criteria" Flex="1"></ext:Column>
                    </Columns>
                </ColumnModel>
            </ext:GridPanel>--%>
        </Items>
        <Buttons>
            <ext:Button ID="btn_RelatedAccount_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_RelatedAccount_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_RelatedAccount_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_RelatedAccount_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.85, height: size.height * 0.8});" />
            <Resize Handler="#{window_AccountParty}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF ACCOUNT PARTY ENTRY WINDOWS ========================== --%>

            
    <%-- ================== Sanction ========================== --%>
    <ext:Window ID="window_sanction" Title="Sanction" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
          
            <%-- Account Information --%>
            <ext:Panel runat="server" ID="Panel5" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Provider_Sanction" FieldLabel="Provider" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%"  />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_Sanction_List_Name_Sanction" FieldLabel="Sanction List Name" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" AllowBlank="false" />
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_MatchCriteria_Sanction" FieldLabel="Match Criteria" EnforceMaxLength="True" maxlength="8000" AnchorHorizontal="70%" />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_LinkToSource_Sanction" FieldLabel="Link To Source" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%" />
                    <ext:TextArea runat="server" LabelWidth="200" ID="txt_SanctionListAttributes_Sanction" FieldLabel="Sanction List Attributes" EnforceMaxLength="True" maxlength="8000" AnchorHorizontal="70%" />
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_ValidFrom_Sanction" FieldLabel="Valid From" AnchorHorizontal="60%" Format="dd-MMM-yyyy" Hidden="true" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_IsApproxFrom_Sanction" FieldLabel="Is Approx From" Hidden="true"></ext:Checkbox>
                    <ext:DateField runat="server" LabelWidth="200" ID="txt_ValidTo_Sanction" FieldLabel="Valid To" AnchorHorizontal="60%" Format="dd-MMM-yyyy" Hidden="true" />
                    <ext:Checkbox runat="server" LabelWidth="200" ID="chk_IsApproxTo_Sanction" FieldLabel="Is Approx To" Hidden="true"></ext:Checkbox>
                    <ext:TextArea runat="server"  LabelWidth="200" ID="txt_Comments_Sanction" FieldLabel="Comments" AnchorHorizontal="70%" EnforceMaxLength="True" maxlength="8000"  />
                                                                      </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Sanction_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Sanctions_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Sanction_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Sanction_Cancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.85, height: size.height * 0.8});" />
            <Resize Handler="#{window_sanction}.center()" />
        </Listeners>
    </ext:Window>

               <%-- ================== Email ========================== --%>
    <ext:Window ID="window_email" Title="Email" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
          
            <%-- Account Information --%>
            <ext:Panel runat="server" ID="Panel6" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_emailaddress" FieldLabel="Email Address" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%"  />
                                                                                        </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Email_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Email_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Email_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Email_Cancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.85, height: size.height * 0.8});" />
            <Resize Handler="#{window_email}.center()" />
        </Listeners>
    </ext:Window>

               <%-- ================== Website ========================== --%>
    <ext:Window ID="window_website" Title="Website" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
          
            <%-- Account Information --%>
            <ext:Panel runat="server" ID="Panel7" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_url" FieldLabel="URL" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%"  />
                                                                                        </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Url_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Website_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Url_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Website_Cancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.85, height: size.height * 0.8});" />
            <Resize Handler="#{window_website}.center()" />
        </Listeners>
    </ext:Window>

             <%-- ================== PEP ========================== --%>
    <ext:Window ID="window_pep" Title="PEP" runat="server" Modal="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="Vertical" ButtonAlign="Center" BodyPadding="10">
        <Items>
          
            <%-- PEP Information --%>
            <ext:Panel runat="server" ID="Panel8" MarginSpec="0 0 10 0" Border="true" Layout="AnchorLayout" BodyPadding="10" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField runat="server" LabelWidth="200" ID="cmb_PEPCountry" Label="PEP Country" ValueField="kode" DisplayField="keterangan" StringField="kode,keterangan" StringTable="vw_goAML_Ref_Nama_Negara" AnchorHorizontal="70%"/>
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_functionname_pep" FieldLabel="Function Name" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%"  />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_functiondescription_pep" FieldLabel="Function Description" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%"  />
                    <ext:TextField runat="server" LabelWidth="200" ID="txt_notes_pep" FieldLabel="Notes" EnforceMaxLength="True" maxlength="255" AnchorHorizontal="70%"  />
                                                                   
                    </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_PEP_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_PEP_Save_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_PEP_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_PEP_Cancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.85, height: size.height * 0.8});" />
            <Resize Handler="#{window_pep}.center()" />
        </Listeners>
    </ext:Window>

</asp:Content>

