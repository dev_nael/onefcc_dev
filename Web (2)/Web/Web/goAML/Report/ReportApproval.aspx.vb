﻿Imports Ext
Imports OfficeOpenXml
Imports System.Data
Partial Class goAML_Report_ReportApproval
    Inherits Parent
    Public objFormModuleApproval As NawaBLL.FormModuleView
    Public Property strWhereClause() As String
        Get
            Return Session("goAML_Report_ReportApproval.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("goAML_Report_ReportApproval.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("goAML_Report_ReportApproval.strSort")
        End Get
        Set(ByVal value As String)
            Session("goAML_Report_ReportApproval.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("goAML_Report_ReportApproval.indexStart")
        End Get
        Set(ByVal value As String)
            Session("goAML_Report_ReportApproval.indexStart") = value
        End Set
    End Property

    Public Property objModule() As NawaDAL.Module
        Get
            Return Session("goAML_Report_ReportView.objModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_Report_ReportView.objModule") = value
        End Set
    End Property


    Private Sub goAML_Report_ReportApproval_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            If Not NawaBLL.Common.SessionCurrentUser Is Nothing Then
                Dim Moduleid As String = Request.Params("ModuleID")
                Dim intModuleid As Integer

                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                'Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)
                objModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Approval) Then
                    Dim strIDCode As String = 1
                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If

                objFormModuleApproval.ModuleID = objmodule.PK_Module_ID
                objFormModuleApproval.ModuleName = objmodule.ModuleName

                objFormModuleApproval.AddField("PK_Report_ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.INTValue, , , , , )
                objFormModuleApproval.AddField("Aging", "Aging", 2, False, True, NawaBLL.Common.MFieldType.INTValue, , , , , )
                objFormModuleApproval.AddField("submission_date", "Tanggal Laporan", 3, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )
                objFormModuleApproval.AddField("Report", "Jenis Report", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleApproval.AddField("submission", "Cara Penyampaian Laporan", 5, False, False, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleApproval.AddField("transaction_date", "Tanggal Transaksi", 6, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,,)
                objFormModuleApproval.AddField("TransaksiUnik", "Transaksi Unik", 7, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleApproval.AddField("ValueTransaksiUnik", "Value Transaksi Unik", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleApproval.AddField("submit_date", "Tanggal Submit", 9, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,,)
                objFormModuleApproval.AddField("PPATKNo", "No PPATK", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleApproval.AddField("statusPPATK", "Status", 11, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleApproval.AddField("isValid", "Valid", 12, False, True, NawaBLL.Common.MFieldType.BooleanValue,,,,, )
                objFormModuleApproval.AddField("lastUpdateDate", "Tanggal Last Update", 13, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,,)
                objFormModuleApproval.AddField("statusID", "StatusID", 13, False, False, NawaBLL.Common.MFieldType.INTValue,,,,,)
                objFormModuleApproval.SettingFormView()

                'Remove default form View buttons
                Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
                GridpanelView.ColumnModel.Columns.Remove(objcommandcol)

                'Add button Approval Detail
                Using objCustomCommandColumn As New CommandColumn
                    objCustomCommandColumn.ID = "columncrudapproval"
                    objCustomCommandColumn.ClientIDMode = Web.UI.ClientIDMode.Static
                    objCustomCommandColumn.Width = 100

                    Dim extparam As New Ext.Net.Parameter
                    extparam.Name = "unikkey"
                    extparam.Value = "record.data.PK_Report_ID"
                    extparam.Mode = ParameterMode.Raw

                    Dim extparamcommand As New Ext.Net.Parameter
                    extparamcommand.Name = "command"
                    extparamcommand.Value = "command"
                    extparamcommand.Mode = ParameterMode.Raw

                    objCustomCommandColumn.DirectEvents.Command.ExtraParams.Add(extparam)
                    objCustomCommandColumn.DirectEvents.Command.ExtraParams.Add(extparamcommand)
                    ''objCustomCommandColumn.DirectEvents.Command.Confirmation.Title = "Delete"
                    ''objCustomCommandColumn.DirectEvents.Command.Confirmation.BeforeConfirm = "if (command='Edit') return false;"
                    ''objCustomCommandColumn.DirectEvents.Command.Confirmation.ConfirmRequest = True
                    ''objCustomCommandColumn.DirectEvents.Command.Confirmation.Message = "Are You Sure To Delete This Record ?"

                    Dim gcApprovalDetail As New GridCommand
                    gcApprovalDetail.CommandName = "ApprovalDetail"
                    gcApprovalDetail.Icon = Icon.ApplicationForm
                    gcApprovalDetail.Text = "Detail"
                    gcApprovalDetail.ToolTip.Text = "Detail"
                    objCustomCommandColumn.Commands.Add(gcApprovalDetail)

                    AddHandler objCustomCommandColumn.DirectEvents.Command.Event, AddressOf GridCommandCustom

                    GridpanelView.ColumnModel.Columns.Insert(1, objCustomCommandColumn)
                End Using

                GridpanelView.Title = objmodule.ModuleLabel + " - Request Approval"

                If Not Net.X.IsAjaxRequest Then
                    cboExportExcel.SelectedItem.Text = "Excel"
                End If
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub goAML_Report_ReportApproval_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleApproval = New NawaBLL.FormModuleView(Me.GridpanelView, Me.Button1)
    End Sub
    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Using objdb As New NawaDevDAL.NawaDatadevEntities
                Dim intStart As Integer = e.Start
                Dim intLimit As Int16 = e.Limit
                Dim inttotalRecord As Integer
                Dim strfilter As String = objFormModuleApproval.GetWhereClauseHeader(e)

                '' added by Apuy 20200902
                Dim ReportType As String = Request.Params("ReportType")

                strfilter = strfilter.Replace("Active", objFormModuleApproval.ModuleName & ".Active")

                If String.IsNullOrEmpty(strfilter) Then
                    strfilter += "statusID = 4"
                Else
                    strfilter += " AND statusID = 4"
                End If

                '' added by Apuy 20200902
                If Not String.IsNullOrEmpty(ReportType) Then
                    If String.IsNullOrEmpty(strfilter) Then
                        strfilter += "ReportType like ('%" & ReportType & "%')"
                    Else
                        strfilter += " AND ReportType like ('%" & ReportType & "%')"
                    End If
                End If

                '' Add 27-May-2022 Felix , filter juga by Role
                Dim UserID_SameRole As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select  replace(replace('<' + STUFF(  " &
                "       (      " &
                "           SELECT UsrLain.userID " &
                "           FROM   MUser UsrLain " &
                "		   inner join mUser userLogin " &
                "		   On UsrLain.FK_MRole_ID = userLogin.FK_MRole_ID " &
                "			WHERE userLogin.userID =  '" & NawaBLL.Common.SessionCurrentUser.UserID & "' " &
                "			For XML PATH('')      " &
                "       ),       " &
                "       1,       " &
                "       1,       " &
                "       ''       " &
                "   ) " &
                "   ,'<userID>',''''), '</userID>', ''',') + ' '''' ' ", Nothing)

                If Not String.IsNullOrEmpty(UserID_SameRole) Then
                    If String.IsNullOrEmpty(strfilter) Then
                        strfilter += "LastUpdateBy in (" & UserID_SameRole & ")"
                    Else
                        strfilter += " AND LastUpdateBy in (" & UserID_SameRole & ")"
                    End If
                End If

                '' End 27-May-2022

                Dim strsort As String = ""
                For Each item As DataSorter In e.Sort
                    strsort += item.Property & " " & item.Direction.ToString
                Next
                Me.indexStart = intStart
                Me.strWhereClause = strfilter
                Me.strOrder = strsort
                If strsort = "" Then
                    strsort = "transaction_date asc"
                End If
                Dim DataPaging As Data.DataTable = objFormModuleApproval.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
                ''-- start paging ------------------------------------------------------------
                Dim limit As Integer = e.Limit
                If (e.Start + e.Limit) > inttotalRecord Then
                    limit = inttotalRecord - e.Start
                End If
                ''-- end paging ------------------------------------------------------------
                e.Total = inttotalRecord
                GridpanelView.GetStore.DataSource = DataPaging
                GridpanelView.GetStore.DataBind()
            End Using
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleApproval.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleApproval.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleApproval.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            'Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.AddHeader("content-disposition", "attachment;filename=" & objModule.ModuleLabel & " Approval.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleApproval.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleApproval.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        'Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.AddHeader("content-disposition", "attachment;filename=" & objModule.ModuleLabel & " Approval.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleApproval.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleApproval.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleApproval.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            'Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.AddHeader("content-disposition", "attachment;filename=" & objModule.ModuleLabel & " Approval.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleApproval.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleApproval.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        'Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.AddHeader("content-disposition", "attachment;filename=" & objModule.ModuleLabel & " Approval.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    'Custom CommandColumn for Approval Detail
    Protected Sub GridCommandCustom(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            Dim ModuleID As String = Request.Params("ModuleID")
            Dim intModuleID As Integer

            intModuleID = NawaBLL.Common.DecryptQueryString(ModuleID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleID)

            Dim ID As String = e.ExtraParams(0).Value
            ID = NawaBLL.Common.EncryptQueryString(ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            If e.ExtraParams(1).Value = "ApprovalDetail" Then
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApprovalDetail & "?ID={0}&ModuleID={1}", ID, ModuleID), "Loading...")
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class
