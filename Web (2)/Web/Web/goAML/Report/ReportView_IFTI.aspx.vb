﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Ext.Net
Imports NawaBLL
Imports NawaDAL
Imports OfficeOpenXml
Partial Class goAML_Report_ReportView_IFTI
    Inherits Parent
    Public objFormModuleView As NawaBLL.FormModuleView

    Public Property Filetodownload() As String
        Get
            Return Session("goAML_GeneratePPATK.Filetodownload")
        End Get
        Set(ByVal value As String)
            Session("goAML_GeneratePPATK.Filetodownload") = value
        End Set
    End Property

    Public Property strWhereClause() As String
        Get
            Return Session("goAML_Report_ReportView_IFTI.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("goAML_Report_ReportView_IFTI.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("goAML_Report_ReportView_IFTI.strSort")
        End Get
        Set(ByVal value As String)
            Session("goAML_Report_ReportView_IFTI.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("goAML_Report_ReportView_IFTI.indexStart")
        End Get
        Set(ByVal value As String)
            Session("goAML_Report_ReportView_IFTI.indexStart") = value
        End Set
    End Property

    Public Property objModule() As NawaDAL.Module
        Get
            Return Session("goAML_Report_ReportView_IFTI.objModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_Report_ReportView_IFTI.objModule") = value
        End Set
    End Property

    Public Property combinedReportID() As String
        Get
            Return Session("goAML_Report_ReportView_IFTI.combinedReportID")
        End Get
        Set(value As String)
            Session("goAML_Report_ReportView_IFTI.combinedReportID") = value
        End Set
    End Property

    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            'Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.AddHeader("content-disposition", "attachment;filename=" & objModule.ModuleLabel & ".xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        'Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.AddHeader("content-disposition", "attachment;filename=" & objModule.ModuleLabel & ".csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            'Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.AddHeader("content-disposition", "attachment;filename=" & objModule.ModuleLabel & ".xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        'Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.AddHeader("content-disposition", "attachment;filename=" & objModule.ModuleLabel & ".csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub


    <DirectMethod>
    Public Sub BtnAdd_Click()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            'Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")

            '----------------25/01/2020 BSIM
            Dim tempUrl As String = ""

            If Request.Url.ToString().Contains("?") Then

                tempUrl = Request.Url.ToString().Split("?")(1)

            End If

            If String.IsNullOrEmpty(tempUrl) Then
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")
            Else
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?{0}", tempUrl), "Loading")
            End If
            '-----------------------------

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub goAML_Report_ReportView_IFTI_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer

            Try
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                'Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)
                Me.objModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                    Dim strIDCode As String = 1
                    BtnAdd.Hidden = True

                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If

                objFormModuleView.ModuleID = objModule.PK_Module_ID
                objFormModuleView.ModuleName = objModule.ModuleName

                objFormModuleView.AddField("PK_Report_ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.INTValue, , , , , )
                objFormModuleView.AddField("Aging", "Aging", 2, False, True, NawaBLL.Common.MFieldType.INTValue, , , , , )
                objFormModuleView.AddField("submission_date", "Tanggal Laporan", 3, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )
                objFormModuleView.AddField("report_code", "Kode Jenis Laporan", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("Report", "Jenis Laporan", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                '' Add 26-Dec-2022, Felix. ANZ minta info Currency & Amount
                objFormModuleView.AddField("TrnCurrency", "Currency", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleView.AddField("TrnAmount", "Amount", 7, False, True, NawaBLL.Common.MFieldType.NUMERICDECIMALValue,,,,, 2)
                '' End 26-Dec-2022
                objFormModuleView.AddField("submission", "Cara Penyampaian Laporan", 8, False, False, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
                objFormModuleView.AddField("transaction_date", "Tanggal Transaksi", 9, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,,)
                objFormModuleView.AddField("TransaksiUnik", "Transaksi Unik", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleView.AddField("ValueTransaksiUnik", "Unik Reference", 11, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleView.AddField("submit_date", "Tanggal Submit", 12, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,,)
                objFormModuleView.AddField("PPATKNo", "No PPATK", 13, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleView.AddField("statusPPATK", "Status", 14, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleView.AddField("ID_Laporan_PPATK_Result", "ID PPATK Result", 15, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)
                objFormModuleView.AddField("isValid", "Valid", 16, False, True, NawaBLL.Common.MFieldType.BooleanValue,,,,, )
                objFormModuleView.AddField("lastUpdateDate", "Tanggal Last Update", 17, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,,)
                objFormModuleView.AddField("statusID", "StatusID", 18, False, False, NawaBLL.Common.MFieldType.INTValue,,,,,)
                objFormModuleView.AddField("ErrorMessage", "Error Message", 19, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,,)

                objFormModuleView.SettingFormView()


                Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
                objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"

                'Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")

                'Dim gridCommandResponse As New GridCommand

                'objcommandcol.Width = 200
                'Dim objDrop As New GridCommand
                'objDrop.CommandName = "Delete"
                'objDrop.Icon = Icon.PencilDelete
                'objDrop.Text = "Delete"
                'objDrop.ToolTip.Text = ""
                'objcommandcol.Commands.Add(objDrop)

                'AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf Delete

                '' Add 13-Mar-2023
                'objcommandcol.Width = 360
                'Dim objEditReport As New GridCommand
                'objEditReport.CommandName = "cmdEditReport"
                'objEditReport.Icon = Icon.ArrowRight
                'objEditReport.Text = "Edit"
                'objEditReport.ToolTip.Text = ""
                'objcommandcol.Commands.Add(objEditReport)

                'Dim extparam As New Ext.Net.Parameter
                'extparam.Name = "unikkey"
                'extparam.Value = "record.data.PK_Report_ID"
                'extparam.Mode = ParameterMode.Raw
                'objcommandcol.DirectEvents.Command.ExtraParams.Add(extparam)

                'objcommandcol.DirectEvents.Command.IsUpload = True
                'objcommandcol.DirectEvents.Command.EventMask.ShowMask = True
                'objcommandcol.DirectEvents.Command.EventMask.Msg = "Loading..."

                'Dim extParamCommandName As New Ext.Net.Parameter
                'extParamCommandName.Name = "command"
                'extParamCommandName.Value = "command"
                'extParamCommandName.Mode = ParameterMode.Raw
                'objcommandcol.DirectEvents.Command.ExtraParams.Add(extParamCommandName)

                'AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf EditReport_Click
                '' End 13-Mar-2023

                '' Add 14-Mar-2023, Felix. Hidden / Show btn_EditSelectedReport
                Dim paramValueMultiTab As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "Select ParameterValue FROm [goAML_Ref_ReportGlobalParameter] where PK_GlobalReportParameter_ID = '3005'", Nothing)
                ' 2023-11-07, Nael
                'btn_EditSelectedReport.Hidden = Not (paramValueMultiTab = "1" Or paramValueMultiTab = "Y" Or paramValueMultiTab = "Yes" Or paramValueMultiTab = "TRUE")
                '' End 14-Mar-2023

            Catch ex As Exception
            End Try

            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            'Begin Penambahan Advanced Filter
            If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                Toolbar2.Hidden = True
            Else
                Toolbar2.Hidden = False
            End If
            LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")
            'END Penambahan Advanced Filter

            Using objdb As New NawaDevDAL.NawaDatadevEntities

                '' added by Apuy 20200902
                Dim ReportType As String = Request.Params("ReportType")

                '' Added By Felix 09 Jul 2020
                Dim ReportCode As String = Request.Params("ReportCode")
                Dim TanggalTransaksi As String = Request.Params("TanggalTransaksi")
                Dim isValid As String = Request.Params("Valid")
                '' End of Felix 09 Jul 2020

                Dim intStart As Integer = e.Start
                Dim intLimit As Int16 = e.Limit
                Dim inttotalRecord As Integer
                Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

                '' added by Apuy 20200902
                If Not String.IsNullOrEmpty(ReportType) Then
                    If String.IsNullOrEmpty(strfilter) Then
                        strfilter += "ReportType like ('%" & ReportType & "%')"
                    Else
                        strfilter += " AND ReportType like ('%" & ReportType & "%')"
                    End If
                End If

                '  strfilter = strfilter.Replace("Active", objFormModuleView.ModuleName & ".Active")
                '' Added By Felix 09 Jul 2020

                If Not String.IsNullOrEmpty(ReportCode) Then

                    '' added by Apuy 20200902
                    If String.IsNullOrEmpty(strfilter) Then
                        'objFormModuleView.ModuleName = "Testing aja" ''objmodule.ModuleName
                        strfilter += "report_code='" & ReportCode & "' and transaction_date='" & TanggalTransaksi _
                                & "' and IsValid=" & isValid & ""
                    Else
                        strfilter += " AND report_code='" & ReportCode & "' and transaction_date='" & TanggalTransaksi _
                               & "' and IsValid=" & isValid & ""
                    End If

                End If
                '' End of Felix 09 Jul 2020


                Dim strsort As String = ""
                For Each item As DataSorter In e.Sort
                    strsort += item.Property & " " & item.Direction.ToString
                Next
                Me.indexStart = intStart
                Me.strWhereClause = strfilter

                'Begin Penambahan Advanced Filter
                If strWhereClause.Length > 0 Then
                    If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                        strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                        intStart = 0
                    End If
                Else
                    If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                        strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                        intStart = 0
                    End If
                End If
                'END Penambahan Advanced Filter

                'Fix eror export 'Edited CIMB 15 Oct 2020
                'Me.strOrder = strsort
                If strsort = "" Then
                    strsort = "transaction_date Desc"
                End If
                Me.strOrder = strsort

                Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
                'Dim DataPaging As Data.DataTable = DLL.SQLHelper.ExecuteTabelPaging("CustomerInformation_WebTempTable", "CIFNo, Name, DateOfBirth, AccountOwnerId, OpeningDate, WorkingUnitName, IsCustomerInList", strfilter, strsort, intStart, intLimit, inttotalRecord)
                ''-- start paging ------------------------------------------------------------
                Dim limit As Integer = e.Limit
                If (e.Start + e.Limit) > inttotalRecord Then
                    limit = inttotalRecord - e.Start
                End If
                'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
                ''-- end paging ------------------------------------------------------------
                e.Total = inttotalRecord
                GridpanelView.GetStore.DataSource = DataPaging
                GridpanelView.GetStore.DataBind()
            End Using
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update Penambahan Advance Filter

    Protected Sub ExportXmlForImport(sender As Object, e As EventArgs)
        Try
            Dim dtyperowselectionmodel As RowSelectionModel = TryCast(Me.GridpanelView.GetSelectionModel(), RowSelectionModel)
            If dtyperowselectionmodel.SelectedRows.Count > 0 Then
                Dim SelectionID As New List(Of String)
                Dim IDResultTemp As String = ""
                Dim counterflag As Integer = 1
                For Each item As SelectedRow In dtyperowselectionmodel.SelectedRows
                    SelectionID.Add(item.RecordID.ToString())
                    If counterflag = 1 Then
                        IDResultTemp = item.RecordID.ToString()
                        counterflag = counterflag + 1
                    Else
                        IDResultTemp = IDResultTemp & ", " & item.RecordID.ToString()
                    End If
                Next
                combinedReportID = IDResultTemp

                '5-Apr-2022 Adi : Hanya report2 yang statusnya 1=Waiting For Generate, 2=Waiting For Upload Reference No,
                '3=Reference No Uploaded can be selected
                Dim strQuery As String = "SELECT COUNT(1) FROM goAML_Report WHERE Status IN (1,2,3) AND PK_Report_ID IN (" & combinedReportID & ")"
                Dim countNotReadyForGenerate As Integer = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                If Not IsNothing(countNotReadyForGenerate) AndAlso countNotReadyForGenerate <> dtyperowselectionmodel.SelectedRows.Count Then
                    Throw New ApplicationException("The selected rows contain report(s) with status that not allowed to Generate XML.<br><br>Report status allowed to Generate XML:<br>1. Waiting For Generate<br>2. Waiting For Upload Reference No<br>3. Reference No Uploaded")
                End If

                BindReportID(StoreListReportID, SelectionID)
                Dim UserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                NawaDevBLL.GeneratePPATKBLL.InsertToTabelSelectedReport(UserID, IDResultTemp)
                NawaDevBLL.GeneratePPATKBLL.ValidateSchemaReportSelected(IDResultTemp)
                Dim dtreport As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT TOP(1) * FROM goAML_Generate_XML_" & UserID, Nothing)
                If dtreport IsNot Nothing Then
                    For Each item In dtreport.Rows
                        lblStatusUpload.Text = item(9)
                        lblTotalInvalid.Text = item(5)
                        lblTotalValid.Text = item(4)
                        If Convert.ToString(item(11)).ToLower = "true" Then
                            lblAlreadyValidateSchema.Text = "Yes"
                        Else
                            lblAlreadyValidateSchema.Text = "No"
                        End If
                        If Convert.ToString(item(12)).ToLower = "true" Then
                            lblValidateSchemaResultValid.Text = "Valid"
                        Else
                            lblValidateSchemaResultValid.Text = "Not Valid"
                        End If
                    Next
                End If
                Store1.Reload()
                PanelShowResult.Show()
            Else    '5-Apr-2022 Adi : show information if no reports selected
                Throw New ApplicationException("Please select minimum 1 report.")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub BindReportID(store As Store, list As List(Of String))
        Try
            Dim objtable As New DataTable
            objtable.Columns.Add(New DataColumn("ListReportID", GetType(String)))
            For Each Items As String In list
                Dim rows As DataRow = objtable.NewRow()
                rows("ListReportID") = Items
                objtable.Rows.Add(rows)
            Next
            store.DataSource = objtable
            store.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub BtnDownloadSelected_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim ReportType As String = Request.Params("ReportType")
            'Dim FileReturn As String = ""

            '5-Apr-2022 Adi : Cegah Generate XML jika masih ada yg invalid. Confirmed by Bombom and Nunung
            'Dim isHarusValid As String = getParameterGlobalByPK(12)
            'If lblTotalValid.Text = "0" Then
            '    Throw New Exception("There is not data valid")
            'End If

            'If (lblValidateSchemaResultValid.Text = "Not Valid" Or lblValidateSchemaResultValid.Text = "") And isHarusValid = "1" Then
            '    'Throw New Exception("There is Validate Schema result Fail. Please fix the fail result first before generate xml")
            'End If

            If lblTotalInvalid.Text <> "0" Then
                Throw New ApplicationException("Can't generate XML. There are some invalid data.")
            End If
            If (lblValidateSchemaResultValid.Text = "Not Valid" Or lblValidateSchemaResultValid.Text = "") Then
                Throw New ApplicationException("There are some invalid data after Validate XSD Schema.<br>Please fix the invalid data before generate XML.")
            End If

            Dim path As String = Server.MapPath("~\goaml\FolderExport\")

            Dim Dirpath = path & NawaBLL.Common.SessionCurrentUser.UserID & "\"
            If Not Directory.Exists(Dirpath) Then
                Directory.CreateDirectory(Dirpath)
            End If

            Filetodownload = NawaDevBLL.GeneratePPATKBLL.GenerateReportXMLBySelectedReport(Date.Now, ReportType, Dirpath, combinedReportID)
            If Filetodownload Is Nothing Then
                'Throw New Exception("Not Exists Report")
                Throw New ApplicationException("There is no XML result to be downloaded.")
            End If
            'FileReturn = "C:\Users\NDS.NDS-LPT-0160\Documents\GoAML\Development\Web\Web\goaml\FolderExport\Sysadmin\20210211-20210127-001-LAPT.zip"
            'Response.Clear()
            'Response.ClearHeaders()
            'Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(FileReturn))
            'Response.Charset = ""
            'Response.AddHeader("cache-control", "max-age=0")
            'Me.EnableViewState = False
            'Response.ContentType = "application/zip"
            'Response.BinaryWrite(File.ReadAllBytes(FileReturn))
            'Response.End()
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Sub Download()
        Try
            If Filetodownload <> "" Then
                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(Filetodownload))
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "application/zip"
                Response.BinaryWrite(IO.File.ReadAllBytes(Filetodownload))
                Response.End()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub BtnCancel_DirectClick()
        Try
            PanelShowResult.Hidden = True
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Function getParameterGlobalByPK(id As Integer) As String
        Try
            Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
                Dim paramValues As String = ""
                Dim paramGlobal As NawaDevDAL.goAML_Ref_ReportGlobalParameter = objdb.goAML_Ref_ReportGlobalParameter.Where(Function(x) x.PK_GlobalReportParameter_ID = id).FirstOrDefault

                If Not paramGlobal Is Nothing Then
                    paramValues = paramGlobal.ParameterValue
                End If
                Return paramValues
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub StoreValidation_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            StoreView.PageSize = SystemParameterBLL.GetPageSize
            Dim intStart As Integer = e.Start
            Dim intTotalRowCount As Long = 0

            Dim intLimit As Int16 = e.Limit
            Dim strfilter As String = Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)
            Dim strsort As String = ""

            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Dim strQuerySchema As String = "SELECT UnikReference, ErrorMessage FROM goAML_ValidateSchemaXSD_Report_" & Common.SessionCurrentUser.UserID
            If strfilter.Length > 0 Then
                strQuerySchema &= " where " & strfilter
            End If
            Dim objListParam(3) As SqlParameter
            objListParam(0) = New SqlParameter
            objListParam(0).ParameterName = "@querydata"
            objListParam(0).SqlDbType = SqlDbType.VarChar
            objListParam(0).Value = strQuerySchema
            objListParam(1) = New SqlParameter
            objListParam(1).ParameterName = "@orderby"
            objListParam(1).SqlDbType = SqlDbType.VarChar
            objListParam(1).Value = strsort
            objListParam(2) = New SqlParameter
            objListParam(2).ParameterName = "@PageNum"
            objListParam(2).SqlDbType = SqlDbType.Int
            objListParam(2).Value = intStart
            objListParam(3) = New SqlParameter
            objListParam(3).ParameterName = "@PageSize"
            objListParam(3).SqlDbType = SqlDbType.Int
            objListParam(3).Value = intLimit
            'query hanya ambil 1 record untuk simpen schemanya saja(pagesize=1) biar enteng
            Dim DataPaging As DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuerySchema, strsort, intStart, intLimit, intTotalRowCount)
            e.Total = intTotalRowCount

            GridPanelValidation.GetStore.DataSource = DataPaging
            GridPanelValidation.GetStore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

#Region "Felix, 13-Mar-2023. Custom Edit for open in new tab"
    'Protected Sub EditReport_Click(sender As Object, e As DirectEventArgs)
    '    Try
    '        If e.ExtraParams("command") = "cmdEditReport" Then
    '            Dim paramValueMultiTab As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "Select ParameterValue FROm [goAML_Ref_ReportGlobalParameter] where PK_GlobalReportParameter_ID = '3005'", Nothing)

    '            Dim pkReportID As String = e.ExtraParams("unikkey")
    '            Dim strEncModuleID As String = NawaBLL.Common.EncryptQueryString(objModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
    '            Dim strEnfPKReportID As String = NawaBLL.Common.EncryptQueryString(pkReportID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

    '            If paramValueMultiTab = "1" Or paramValueMultiTab = "Y" Or paramValueMultiTab = "Yes" Or paramValueMultiTab = "TRUE" Then

    '                If InStr(objModule.UrlEdit, "?") > 0 Then
    '                    'Session("GenerateReportSTR") = True
    '                    'Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objModule.UrlView & "&ModuleID=" & strEncModuleID & "&CaseID=" & pkReportID)

    '                    'Page.ClientScript.RegisterStartupScript(Me.GetType(), "OpenWindow", "window.open('" & NawaBLL.Common.GetApplicationPath & objModule.UrlEdit & "&ModuleID=" & strEncModuleID & "&ID=" & strEnfPKReportID & "','_newtab');", True)
    '                    Dim Script As String = "window.open('" & NawaBLL.Common.GetApplicationPath & objModule.UrlEdit & "&ModuleID=" & strEncModuleID & "&ID=" & strEnfPKReportID & "');"
    '                    GridpanelView.AddScript(Script)
    '                Else
    '                    'Session("GenerateReportSTR") = True
    '                    'Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objModule.UrlView & "?ModuleID=" & strEncModuleID & "&CaseID=" & pkReportID)

    '                    'Page.ClientScript.RegisterStartupScript(Me.GetType(), "OpenWindow", "window.open('" & NawaBLL.Common.GetApplicationPath & objModule.UrlEdit & "?ModuleID=" & strEncModuleID & "&ID=" & strEnfPKReportID & "','_newtab');", True)
    '                    Dim Script As String = "window.open('" & NawaBLL.Common.GetApplicationPath & objModule.UrlEdit & "?ModuleID=" & strEncModuleID & "&ID=" & strEnfPKReportID & "');"
    '                    GridpanelView.AddScript(Script)
    '                End If
    '            Else
    '                If InStr(objModule.UrlEdit, "?") > 0 Then
    '                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objModule.UrlEdit & "&ID=" & strEnfPKReportID & "&ModuleID=" & strEncModuleID)
    '                Else
    '                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objModule.UrlEdit & "?ID=" & strEnfPKReportID & "&ModuleID=" & strEncModuleID)
    '                End If
    '            End If
    '            ' End 16-Feb-2022
    '        End If

    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

#End Region

#Region "Felix, 14-Mar-2023. Thanks to Pak Adi, ganti jd button di atas"
    <DirectMethod>
    Sub btn_EditSelectedReport_Click()
        Try
            'Cek minimal 1 profile hasil screening dipilih
            Dim smScreeningResult As RowSelectionModel = TryCast(GridpanelView.GetSelectionModel(), RowSelectionModel)
            If smScreeningResult.SelectedRows.Count = 0 Then
                Throw New Exception("Minimal 1 data harus dipilih!")
            End If

            'Cek status yang tidak boleh edit
            For Each item As SelectedRow In smScreeningResult.SelectedRows
                Dim pkProfileID = item.RecordID.ToString
                Dim fkReportStatus As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select isnull(Status,'') FROM goAML_Report where PK_Report_ID = ' " & pkProfileID & " '", Nothing)

                If fkReportStatus <> "1" And fkReportStatus <> "5" And fkReportStatus <> "2" And fkReportStatus <> "9" And fkReportStatus <> "0" Then
                    Throw New Exception("Report ID " & pkProfileID & " tidak boleh diedit !")
                End If

            Next

            For Each item As SelectedRow In smScreeningResult.SelectedRows
                Dim pkProfileID = item.RecordID.ToString
                Dim strEncryptedModuleID = NawaBLL.Common.EncryptQueryString(objModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim strEncryptedReportID = NawaBLL.Common.EncryptQueryString(pkProfileID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim strURL = NawaBLL.Common.GetApplicationPath & objModule.UrlEdit & "&ModuleID=" & strEncryptedModuleID & "&ID=" & strEncryptedReportID
                'Dim Script As String = "window.open('" & strURL & "' , '_newtab')"
                'btn_EditSelectedReport.AddScript(Script)                 Dim s As String = "window.open('http://localhost" & strURL & "', '_blank');"
                'Page.ClientScript.RegisterStartupScript(Me.GetType(), "alertscript", s, True)

                If InStr(objModule.UrlEdit, "?") > 0 Then
                    Dim Script As String = "window.open('" & NawaBLL.Common.GetApplicationPath & objModule.UrlEdit & "&ModuleID=" & strEncryptedModuleID & "&ID=" & strEncryptedReportID & "');"
                    GridpanelView.AddScript(Script)
                Else
                    Dim Script As String = "window.open('" & NawaBLL.Common.GetApplicationPath & objModule.UrlEdit & "?ModuleID=" & strEncryptedModuleID & "&ID=" & strEncryptedReportID & "');"
                    GridpanelView.AddScript(Script)
                End If

            Next
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class
