﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PersonDetail.aspx.vb" MasterPageFile="~/Site1.Master" Inherits="goAML_PersonDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- MODAL: Identity Detail--%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="IDModalDetail" Width="700" Title="Detail Identitas">
        <Items>
            <ext:FormPanel runat="server" BodyPadding="5"
                MonitorResize="true" ID="IDModal_Form_Detail">
                <Items>
                    <ext:DisplayField ID="RP_jenis_identitas_detail" runat="server" FieldLabel="Jenis Identitas" />
                    <ext:DisplayField ID="RP_no_identitas_detail" runat="server" FieldLabel="Nomor Identitas" />
                    <ext:DisplayField ID="RP_tgl_terbit_detail" runat="server" FieldLabel="Tanggal Diterbitkan" />
                    <ext:DisplayField ID="RP_tgl_kadaluarsa_detail" runat="server" FieldLabel="Tanggal Kadaluarsa" />
                    <ext:DisplayField ID="RP_penerbit_detail" runat="server" FieldLabel="Diterbitkan Oleh" />
                    <ext:DisplayField ID="RP_negara_penerbit_detail" runat="server" FieldLabel="Negara penerbit identitas" />
                    <ext:DisplayField ID="RP_note_identitas_detail" runat="server" FieldLabel="Catatan" />
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <%-- Close --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="IDModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <%-- MODAL: Phone Detail --%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="PhoneModalDetail" Width="700" Title="Detail Telepon">
        <Items>
            <%--FORM PANEL--%>
            <ext:FormPanel
                runat="server"
                BodyPadding="5"
                MonitorResize="true"
                ID="PhoneModal_Form_Detail">
                <Items>
                    <%-- Kategori Kontak --%>
                    <ext:DisplayField ID="RP_tipe_kontak_detail" runat="server" FieldLabel="Kategori Kontak" />
                    <%-- Jenis alat komunikasi --%>
                    <ext:DisplayField ID="RP_jenis_komunikasi_detail" runat="server" FieldLabel="Jenis Alat Komunikasi" />
                    <%-- Kode area telepon --%>
                    <ext:DisplayField ID="RP_kode_area_detail" runat="server" FieldLabel="Kode area telepon" />
                    <%-- Nomor Telepon --%>
                    <ext:DisplayField ID="RP_no_telepon_detail" runat="server" FieldLabel="Nomor Telepon" />
                    <%-- Nomor Telepon Extensi--%>
                    <ext:DisplayField ID="RP_phone_ext_detail" runat="server" FieldLabel="Nomor Extensi" />
                    <%-- Notes --%>
                    <ext:DisplayField ID="RP_note_phone_detail" runat="server" FieldLabel="Catatan" />
                </Items>
            </ext:FormPanel>
        </Items>
        <%-- Buttons --%>
        <Buttons>
            <%-- Close --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="PhoneModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <%-- MODAL: Employer Phone Detail --%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="WindowEmployerPhoneDetail" Width="700" Title="Detail Telepon Tempat Bekerja">
        <Items>
            <%--FORM PANEL--%>
            <ext:FormPanel
                runat="server"
                BodyPadding="5"
                MonitorResize="true"
                ID="FormPanelEmployerPhoneDetail">
                <Items>
                    <%-- Kategori Kontak --%>
                    <ext:DisplayField ID="EmployerPhoneDetail_Kategori" runat="server" FieldLabel="Kategori Kontak" />
                    <%-- Jenis alat komunikasi --%>
                    <ext:DisplayField ID="EmployerPhoneDetail_jenis" runat="server" FieldLabel="Jenis Alat Komunikasi" />
                    <%-- Kode area telepon --%>
                    <ext:DisplayField ID="EmployerPhoneDetail_kode" runat="server" FieldLabel="Kode area telepon" />
                    <%-- Nomor Telepon --%>
                    <ext:DisplayField ID="EmployerPhoneDetail_nomor" runat="server" FieldLabel="Nomor Telepon" />
                    <%-- Nomor Telepon Extensi--%>
                    <ext:DisplayField ID="EmployerPhoneDetail_extensi" runat="server" FieldLabel="Nomor Extensi" />
                    <%-- Notes --%>
                    <ext:DisplayField ID="EmployerPhoneDetail_note" runat="server" FieldLabel="Catatan" />
                </Items>
            </ext:FormPanel>
        </Items>
        <%-- Buttons --%>
        <Buttons>
            <%-- Close --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="PhoneModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>

    </ext:Window>

    <%-- MODAL: Address Detail--%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="AddrModalDetail" Width="700" Title="Detail Alamat">
        <Items>
            <ext:FormPanel
                runat="server"
                BodyPadding="5"
                MonitorResize="true"
                ID="AddrModal_Form_Detail">
                <Items>
                    <%-- Tipe alamat --%>
                    <ext:DisplayField ID="RP_tipe_alamat_detail" runat="server" FieldLabel="Tipe Alamat" />
                    <%-- Alamat --%>
                    <ext:DisplayField ID="RP_alamat_detail" runat="server" FieldLabel="Alamat" />
                    <%-- Kecamatan --%>
                    <ext:DisplayField ID="RP_kecamatan_detail" runat="server" FieldLabel="Kecamatan" />
                    <%-- Kabupaten --%>
                    <ext:DisplayField ID="RP_kabupaten_detail" runat="server" FieldLabel="Kota/Kabupaten" />
                    <%-- Kodepos --%>
                    <ext:DisplayField ID="RP_kodepos_detail" runat="server" FieldLabel="Kode Pos" />
                    <%-- Negara --%>
                    <ext:DisplayField ID="RP_negara_detail" runat="server" FieldLabel="Negara" />
                    <%-- Province --%>
                    <ext:DisplayField ID="RP_propinsi_detail" runat="server" FieldLabel="Provinsi" />
                    <%-- Note --%>
                    <ext:DisplayField ID="RP_note_alamat_detail" runat="server" FieldLabel="Catatan" />
                </Items>
            </ext:FormPanel>
        </Items>
        <%-- Buttons --%>
        <Buttons>
            <%-- Cancel --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="AddressModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>

    <%-- MODAL: Employer Address Detail--%>
    <ext:Window runat="server" Maximizable="true" Hidden="true" ID="WindowEmployerAddressDetail" Width="700" Title="Detail Alamat Tempat Bekerja">
        <Items>
            <ext:FormPanel
                runat="server"
                BodyPadding="5"
                MonitorResize="true"
                ID="FormPanelEmployerAddressDetail">
                <Items>
                    <%-- Tipe alamat --%>
                    <ext:DisplayField ID="EmployerAddressDetail_tipe" runat="server" FieldLabel="Tipe Alamat" />
                    <%-- Alamat --%>
                    <ext:DisplayField ID="EmployerAddressDetail_alamat" runat="server" FieldLabel="Alamat" />
                    <%-- Kecamatan --%>
                    <ext:DisplayField ID="EmployerAddressDetail_kec" runat="server" FieldLabel="Kecamatan" />
                    <%-- Kabupaten --%>
                    <ext:DisplayField ID="EmployerAddressDetail_kab" runat="server" FieldLabel="Kota/Kabupaten" />
                    <%-- Kodepos --%>
                    <ext:DisplayField ID="EmployerAddressDetail_pos" runat="server" FieldLabel="Kode Pos" />
                    <%-- Negara --%>
                    <ext:DisplayField ID="EmployerAddressDetail_negara" runat="server" FieldLabel="Negara" />
                    <%-- Province --%>
                    <ext:DisplayField ID="EmployerAddressDetail_prov" runat="server" FieldLabel="Provinsi" />
                    <%-- Note --%>
                    <ext:DisplayField ID="EmployerAddressDetail_note" runat="server" FieldLabel="Catatan" />
                </Items>
            </ext:FormPanel>
        </Items>
        <%-- Buttons --%>
        <Buttons>
            <%-- Cancel --%>
            <ext:Button runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="AddressModal_BtnCancel">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>


    <ext:FormPanel ID="FormPanelInput" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false"  ButtonAlign="Center" DefaultAnchor="100%" AutoScroll="true" Flex="1"> 
        <Items>
            <%-- PK ID (Display only) --%>
            <ext:DisplayField ID="LblID" runat="server" FieldLabel="ID" AnchorHorizontal="80%" />
            <%-- User ID --%>
            <ext:DisplayField
                ID="cmb_UserID"
                runat="server"
                FieldLabel="User Id"
                Flex="1"
                 
                AnchorHorizontal="80%">
            </ext:DisplayField>
            <%-- Kode Laporan --%>
            <ext:DisplayField
                ID="RP_KodeLaporan"
                runat="server"
                FieldLabel="Kode Laporan"
                Flex="1"
                AnchorHorizontal="80%">
            </ext:DisplayField>
            <%-- Gelar --%>
            <ext:DisplayField runat="server" ID="RP_Title" Flex="1" FieldLabel="Gelar" AnchorHorizontal="80%"/>
            <%-- Nama lengkap --%>
            <ext:DisplayField runat="server" FieldLabel="Nama Lengkap" Flex="1" AnchorHorizontal="80%" ID="RP_FulltName"   />
            <%-- Tanggal Lahir --%>
            <ext:DisplayField runat="server" Flex="1" FieldLabel="Tanggal Lahir" ID="RP_BirthDate"  >
            </ext:DisplayField>
            <%-- Tempat lahir --%>
            <ext:DisplayField runat="server" ID="RP_BirthPlace" Flex="1" FieldLabel="Tempat Lahir" AnchorHorizontal="80%">
            </ext:DisplayField>
            <%-- Jenis kelamin --%>
            <ext:DisplayField
                ID="RP_Gender"
                runat="server"
                FieldLabel="Jenis Kelamin"
                Flex="1"
                AnchorHorizontal="80%">
            </ext:DisplayField>
            <%-- Nama ibu kandung --%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_MotherName" FieldLabel="Nama Ibu Kandung" AnchorHorizontal="80%" />
            <%-- Nama alias --%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_Alias" FieldLabel="Nama Alias" AnchorHorizontal="80%" />
            <%-- NIK --%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_SSN" FieldLabel="NIK" AnchorHorizontal="80%" />
            <%-- No passport --%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_Passport" FieldLabel="No. Paspor" AnchorHorizontal="80%" />
            <%-- Negara penerbit passport --%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_CountryPassport" FieldLabel="Negara Penerbit Paspor" AnchorHorizontal="80%" />
            <%-- No identitas lain --%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_OtherID" FieldLabel="No. Identitas Lainnya" AnchorHorizontal="80%" />
            <%-- PHONE FIELD SET --%>
            <ext:FieldSet runat="server" Title="Telepon">
                <Items>
                    <%---- GRID PANEL -->--%>
                    <ext:GridPanel ID="PhoneField_Grid" runat="server" Title="Telepon" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="PhoneField_Store" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model2" IDProperty="PK_goAML_Ref_Phone">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" />
                                            <ext:ModelField Name="kategori_kontak" />
                                            <ext:ModelField Name="ja_komunikasi" />
                                            <ext:ModelField Name="tph_country_prefix" />
                                            <ext:ModelField Name="tph_number" />
                                            <ext:ModelField Name="tph_extension" />
                                            <ext:ModelField Name="comments" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererPhone" runat="server"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="Kategori Kontak" DataIndex="kategori_kontak" Flex="1" />
                                <ext:Column runat="server" Text="Jenis Alat Komunikasi" DataIndex="ja_komunikasi" Flex="1" />
                                <ext:Column runat="server" Text="Nomor Telepon" DataIndex="tph_number" Flex="1" />
                                <ext:CommandColumn ID="CommandColumnPhone" runat="server" Width="200" Text="Action" Flex="2">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationViewDetail" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandPhone">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                    <ext:DisplayField runat="server" Text="&nbsp;" />
                </Items>
            </ext:FieldSet>

            <%-- ADDRESS FIELD SET --%>
            <ext:FieldSet runat="server" Title="Alamat">
                <Items>
                    <%-- THE ADDRESS GRID PANEL --%>
                    <ext:GridPanel runat="server" ID="AddressField_Grid" Title="Alamat" AutoScroll="true" EmptyText="No Available Data">
                        <Store>
                            <ext:Store ID="AddressField_Store" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model1" IDProperty="PK_Customer_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" />
                                            <ext:ModelField Name="kontak" />
                                            <ext:ModelField Name="address" />
                                            <ext:ModelField Name="city" />
                                            <ext:ModelField Name="negara" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererAddress" runat="server"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="Tipe Alamat" DataIndex="kontak" Flex="1" />
                                <ext:Column runat="server" Text="Alamat" DataIndex="address" Flex="1" />
                                <ext:Column runat="server" Text="Kota/Kabupaten" DataIndex="city" Flex="1" />
                                <ext:Column runat="server" Text="Negara" DataIndex="negara" Flex="1" />
                                <ext:CommandColumn id="CommandColumnAddress"  runat="server" Width="200" Text="Action" Flex="2">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationViewDetail" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandAddress">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                    <ext:DisplayField runat="server" Text="&nbsp;" />
                </Items>
            </ext:FieldSet>

            <%-- Kewarganegaraan 1,2,3 --%>
            <ext:DisplayField
                ID="RP_Nationality1"
                runat="server"
                FieldLabel="Kewarganegaraan 1"
                Flex="1"
                AnchorHorizontal="80%">
            </ext:DisplayField>
            <ext:DisplayField
                ID="RP_Nationality2"
                runat="server"
                FieldLabel="Kewarganegaraan 2"
                Flex="1"
                AnchorHorizontal="80%">
            </ext:DisplayField>
            <ext:DisplayField
                ID="RP_Nationality3"
                runat="server"
                FieldLabel="Kewarganegaraan 3"
                Flex="1"
                AnchorHorizontal="80%">
            </ext:DisplayField>
            <%-- Negara domisili --%>
            <ext:DisplayField
                ID="RP_Residence"
                runat="server"
                FieldLabel="Negara Domisili"
                Flex="1"
                AnchorHorizontal="80%">
            </ext:DisplayField>
            <%-- Email 1--%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_Email1" FieldLabel="Email 1" AnchorHorizontal="80%" />
            <%-- Email 2--%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_Email2" FieldLabel="Email 2" AnchorHorizontal="80%" />
            <%-- Email 3--%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_Email3" FieldLabel="Email 3" AnchorHorizontal="80%" />
            <%-- Email 4--%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_Email4" FieldLabel="Email 4" AnchorHorizontal="80%" />
            <%-- Email 5--%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_Email5" FieldLabel="Email 5" AnchorHorizontal="80%" />
            <%-- Pekerjaan --%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_Job"   FieldLabel="Pekerjaan" AnchorHorizontal="80%" />
            <%-- Nama Tempat bekerja --%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_JobPlace" FieldLabel="Tempat Bekerja" AnchorHorizontal="80%" />
            <%--EMPLOYER ADDRESS FIELD SET--%>
            <ext:FieldSet runat="server" Title="Alamat">
                <Items>
                    <ext:GridPanel ID="GridPanel_employerAddress" runat="server" Title="Alamat Tempat Bekerja" Height="150" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreAddressEmployer" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model148" IDProperty="PK_Customer_Address_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Customer_Address_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="kontak" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="address" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="city" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="negara" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn30" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column101" runat="server" DataIndex="kontak" Text="Type Alamat" Flex="1"></ext:Column>
                                <ext:Column ID="Column102" runat="server" DataIndex="address" Text="Alamat" Flex="1"></ext:Column>
                                <ext:Column ID="Column103" runat="server" DataIndex="city" Text="Kota" Flex="1"></ext:Column>
                                <ext:Column ID="Column104" runat="server" DataIndex="negara" Text="Negara" Flex="1"></ext:Column>
                                <ext:CommandColumn ID="CommandColumnAddressEmp" runat="server" Text="Action" Flex="2">

                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="GridcommandAddressEmployer">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Customer_Address_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                </Items>
            </ext:FieldSet>

            <%--EMPLOYER PHONE FIELD SET--%>
            <ext:FieldSet runat="server" Title="Telepon">
                <Items>
                    <ext:GridPanel ID="GridPanel_EmployerPhone" runat="server" Title="Telepon Tempat Bekerja" Height="150" EmptyText="No Available Data">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StorePhoneEmployer" runat="server">
                                <Model>
                                    <ext:Model runat="server" ID="Model151" IDProperty="PK_goAML_Ref_Phone">
                                        <Fields>
                                            <ext:ModelField Name="PK_goAML_Ref_Phone" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="kategori_kontak" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ja_komunikasi" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="country_prefix" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="number" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn31" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column105" runat="server" DataIndex="kategori_kontak" Text="Kategori Kontak" Flex="1"></ext:Column>
                                <ext:Column ID="Column1" runat="server" DataIndex="ja_komunikasi" Text="Jenis Alat Komunikasi" Flex="1"></ext:Column>
                                <ext:Column ID="Column107" runat="server" DataIndex="number" Text="Nomor Telepon" Flex="1"></ext:Column>

                                <ext:CommandColumn ID="CommandColumnPhoneEmp" runat="server" Text="Action" Flex="2">

                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="GridcommandPhoneEmployer">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_goAML_Ref_Phone" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                    </ext:GridPanel>
                </Items>
            </ext:FieldSet>

            <%-- IDENTITY FIELD SET --%>
            <ext:FieldSet runat="server" Title="Dokumen Identitas">
                <Items>
                    <ext:GridPanel runat="server" ID="IDField_Grid" Title="Dokumen Identitas" AutoScroll="true" EmptyText="No Available Data">
                        <%-- STORES --%>
                        <Store>
                            <ext:Store runat="server" ID="IDField_Store">
                                <Model>
                                    <ext:Model runat="server" ID="Model3" IDProperty="PK_Person_Identification_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_Person_Identification_ID" />
                                            <ext:ModelField Name="type" />
                                            <ext:ModelField Name="no" />
                                            <ext:ModelField Name="negara" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <%-- COLUMNS --%>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumberIdentity" runat="server"></ext:RowNumbererColumn>
                                <ext:Column runat="server" Text="Jenis identitas" DataIndex="type" Flex="1" />
                                <ext:Column runat="server" Text="Nomor identitas" DataIndex="no" Flex="1" />
                                <ext:Column runat="server" Text="Negara penerbit identitas" DataIndex="negara" Flex="1" />
                                <ext:CommandColumn id="CommandColumnIdent" runat="server" Width="200" Text="Action" Flex="2">
                                    <Commands>
                                        <ext:GridCommand CommandName="Detail" Icon="ApplicationViewDetail" Text="Detail" ToolTip-Text="Detail"></ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridCommandIdentification">
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_Person_Identification_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                            <Confirmation Message="Are You Sure To Delete This Record ?" BeforeConfirm="if (command=='Detail' || command=='Edit') return false; " ConfirmRequest="true" Title="Delete">
                                            </Confirmation>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <%-- TOP BAR --%>
                    </ext:GridPanel>
                    <ext:DisplayField runat="server" Text="&nbsp;" />
                </Items>
            </ext:FieldSet>

            <%-- Sudah meninggal? --%>
            <ext:DisplayField runat="server" ID="RP_IsDeceased" FieldLabel="Sudah Meninggal?" AnchorHorizontal="80%" />
            <%-- Kapan? --%>
            <ext:DisplayField runat="server"
                Flex="1" ID="RP_DeceasedDate"
                FieldLabel="Tanggal Meninggal"
                AnchorHorizontal="80%" />
            <%-- NPWP --%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_TaxNumber" FieldLabel="NPWP" AnchorHorizontal="80%" />
            <%-- Tax Reg Number --%>
            <ext:DisplayField runat="server" ID="RP_TaxRegNumber" FieldLabel="PEPs?" AnchorHorizontal="80%" />
            <%-- Sumber Dana --%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_SourceOfWealth" FieldLabel="Sumber Dana" AnchorHorizontal="80%" />
            <%-- Catatan --%>
            <ext:DisplayField runat="server" Flex="1" ID="RP_Comments" FieldLabel="Catatan" AnchorHorizontal="80%" />
        </Items>
        <Buttons>
            <ext:Button ID="btnCancel" runat="server" Text="Cancel" Icon="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancel_Click">
                        <EventMask ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>

    </ext:FormPanel>
</asp:Content>
