﻿Imports System.IO
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL
Imports System.Data.SqlClient
Imports System.Data

Partial Class goAML_GenerateSiPesatUpload
    Inherits Parent

    Public Property ObjModule() As NawaDAL.Module
        Get
            Return Session("goAML_GenerateSiPesatUpload.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_GenerateSiPesatUpload.ObjModule") = value
        End Set
    End Property
    Public Property stringModuleID As String
        Get
            Return Session("goAML_GenerateSiPesatUpload.StringModuleID")
        End Get
        Set(value As String)
            Session("goAML_GenerateSiPesatUpload.StringModuleID") = value
        End Set
    End Property
    Public Property ObjGeneratedSiPesat() As AML_Sipesat_GeneratedFileList
        Get
            Return Session("goAML_GenerateSiPesat.ObjGeneratedSiPesat")
        End Get
        Set(ByVal value As AML_Sipesat_GeneratedFileList)
            Session("goAML_GenerateSiPesat.ObjGeneratedSiPesat") = value
        End Set
    End Property
    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()

                Dim Moduleid As String = Request.Params("ModuleID")
                stringModuleID = Moduleid
                Try
                    Dim intmodule As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                    Me.ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intmodule)

                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If

                    FormPanelInput.Title = ObjModule.ModuleLabel & " - Import"

                    LoadData()
                Catch ex As Exception
                    ErrorSignal.FromCurrentContext.Raise(ex)
                    Net.X.Msg.Alert("Error", ex.Message).Show()
                End Try
            End If


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub ClearSession()
        ObjModule = Nothing
        ObjGeneratedSiPesat = Nothing
    End Sub
    Private Sub LoadData()
        Dim IDData As String = Request.Params("ID")
        Dim IDReq As String = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)

        ObjGeneratedSiPesat = GenerateSiPesatBLL.GetGeneratedSiPesatByID(IDReq)

        If ObjGeneratedSiPesat IsNot Nothing Then
            With ObjGeneratedSiPesat
                lblIDPJK.Text = .PK_IDPJK
                lblPeriod.Text = .Period
                lblTotalValid.Text = .Total_Valid
                lblTotalInvalid.Text = .Total_Invalid
                Text_no_ref.Text = .No_Referensi
                If .Report_PPATK_Date.HasValue Then
                    Date_no_ref_date.Value = .Report_PPATK_Date
                End If
                lblStatus.Text = NawaDevBLL.GenerateSiPesatBLL.getStatusReportByPK(.Status_Report)
            End With
        End If
    End Sub

    Protected Sub BtnSave_DirectClick(sender As Object, e As EventArgs)
        Try
            Dim date_ref As Date
            Dim Strno_ref As String

            If Date_no_ref_date.Text = "1/1/0001 12:00:00 AM" Or Date_no_ref_date.Value Is Nothing Then
                Throw New Exception("Tanggal Referensi PPATK boleh kosong")
            End If

            If Text_no_ref.Text = "" Then
                Throw New Exception("No. Referensi PPATK boleh kosong")
            End If

            date_ref = Date_no_ref_date.RawValue
            Strno_ref = Text_no_ref.Text

            NawaDevBLL.GenerateSiPesatBLL.SubmitionData(ObjModule, ObjGeneratedSiPesat.PK_Sipesat_GeneratedFileList_ID, Strno_ref, date_ref)

            Panelconfirmation.Hidden = False
            FormPanelInput.Hidden = True
            LblConfirmation.Text = "Data Saved into Database"
        Catch ex As Exception
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class
