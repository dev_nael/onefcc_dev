﻿Imports Ext
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL
Imports Elmah
Imports System.Data

Partial Class goAML_WICApprovalDetail
    Inherits Parent

#Region "Session"
    Public Property ObjApproval As NawaDAL.ModuleApproval
        Get
            Return Session("goAML_WICApprovalDetail.ObjApproval")
        End Get
        Set(ByVal value As NawaDAL.ModuleApproval)
            Session("goAML_WICApprovalDetail.ObjApproval") = value
        End Set
    End Property
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("goAML_WICApprovalDetail.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_WICApprovalDetail.ObjModule") = value
        End Set
    End Property
    Public Property objWICNew() As NawaDevBLL.WICDataBLL
        Get
            Return Session("goAML_WICApprovalDetail.objWICNew")
        End Get
        Set(value As NawaDevBLL.WICDataBLL)
            Session("goAML_WICApprovalDetail.objWICNew") = value
        End Set
    End Property
    Public Property objWICOld() As NawaDevBLL.WICDataBLL
        Get
            Return Session("goAML_WICApprovalDetail.objWICOld")
        End Get
        Set(value As NawaDevBLL.WICDataBLL)
            Session("goAML_WICApprovalDetail.objWICOld") = value
        End Set
    End Property
    Public Property objWICDirectorNew() As List(Of NawaDevBLL.WICDirectorDataBLL)
        Get
            Return Session("goAML_WICApprovalDetail.objWICDirectorNew")
        End Get
        Set(value As List(Of NawaDevBLL.WICDirectorDataBLL))
            Session("goAML_WICApprovalDetail.objWICDirectorNew") = value
        End Set
    End Property
    Public Property objWICDirectorOld() As List(Of NawaDevBLL.WICDirectorDataBLL)
        Get
            Return Session("goAML_WICApprovalDetail.objWICDirectorOld")
        End Get
        Set(value As List(Of NawaDevBLL.WICDirectorDataBLL))
            Session("goAML_WICApprovalDetail.objWICDirectorOld") = value
        End Set
    End Property
    Public Property objWIC() As goAML_Ref_WIC
        Get
            Return Session("goAML_WICApprovalDetail.objWIC")
        End Get
        Set(value As goAML_Ref_WIC)
            Session("goAML_WICApprovalDetail.objWIC") = value
        End Set
    End Property
    Public Property objWICBefore() As goAML_Ref_WIC
        Get
            Return Session("goAML_WICApprovalDetail.objWICBefore")
        End Get
        Set(value As goAML_Ref_WIC)
            Session("goAML_WICApprovalDetail.objWICBefore") = value
        End Set
    End Property
    Public Property objWICDirector() As goAML_Ref_Walk_In_Customer_Director
        Get
            Return Session("goAML_WICApprovalDetail.objWICDirector")
        End Get
        Set(value As goAML_Ref_Walk_In_Customer_Director)
            Session("goAML_WICApprovalDetail.objWICDirector") = value
        End Set
    End Property
    Public Property objWICDirectorBefore() As goAML_Ref_Walk_In_Customer_Director
        Get
            Return Session("goAML_WICApprovalDetail.objWICDirectorBefore")
        End Get
        Set(value As goAML_Ref_Walk_In_Customer_Director)
            Session("goAML_WICApprovalDetail.objWICDirectorBefore") = value
        End Set
    End Property
    Public Property objWICPhone() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail.objWICPhone")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail.objWICPhone") = value
        End Set
    End Property
    Public Property objWICPhoneBefore() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail.objWICPhoneBefore")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail.objWICPhoneBefore") = value
        End Set
    End Property
    Public Property objWICPhoneEmployer() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail.objWICPhoneEmployer")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail.objWICPhoneEmployer") = value
        End Set
    End Property
    Public Property objWICPhoneEmployerBefore() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail.objWICPhoneEmployerBefore")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail.objWICPhoneEmployerBefore") = value
        End Set
    End Property
    Public Property objWICPhoneDirector() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail.objWICPhoneDirector")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail.objWICPhoneDirector") = value
        End Set
    End Property
    Public Property objWICPhoneDirectorBefore() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail.objWICPhoneDirectorBefore")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail.objWICPhoneDirectorBefore") = value
        End Set
    End Property
    Public Property objWICPhoneDirectorEmployer() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail.objWICPhoneDirectorEmployer")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail.objWICPhoneDirectorEmployer") = value
        End Set
    End Property
    Public Property objWICPhoneDirectorEmployerBefore() As goAML_Ref_Phone
        Get
            Return Session("goAML_WICApprovalDetail.objWICPhoneDirectorEmployerBefore")
        End Get
        Set(value As goAML_Ref_Phone)
            Session("goAML_WICApprovalDetail.objWICPhoneDirectorEmployerBefore") = value
        End Set
    End Property
    Public Property objWICAddress() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail.objWICAddress")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail.objWICAddress") = value
        End Set
    End Property
    Public Property objWICAddressBefore() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail.objWICAddressBefore")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail.objWICAddressBefore") = value
        End Set
    End Property
    Public Property objWICAddressEmployer() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail.objWICAddressEmployer")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail.objWICAddressEmployer") = value
        End Set
    End Property
    Public Property objWICAddressEmployerBefore() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail.objWICAddressEmployerBefore")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail.objWICAddressEmployerBefore") = value
        End Set
    End Property
    Public Property objWICAddressDirector() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail.objWICAddressDirector")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail.objWICAddressDirector") = value
        End Set
    End Property
    Public Property objWICAddressDirectorBefore() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail.objWICAddressDirectorBefore")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail.objWICAddressDirectorBefore") = value
        End Set
    End Property
    Public Property objWICAddressDirectorEmployer() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail.objWICAddressDirectorEmployer")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail.objWICAddressDirectorEmployer") = value
        End Set
    End Property
    Public Property objWICAddressDirectorEmployerBefore() As goAML_Ref_Address
        Get
            Return Session("goAML_WICApprovalDetail.objWICAddressDirectorEmployerBefore")
        End Get
        Set(value As goAML_Ref_Address)
            Session("goAML_WICApprovalDetail.objWICAddressDirectorEmployerBefore") = value
        End Set
    End Property
    Public Property objWICIdentification() As goAML_Person_Identification
        Get
            Return Session("goAML_WICApprovalDetail.objWICIdentification")
        End Get
        Set(value As goAML_Person_Identification)
            Session("goAML_WICApprovalDetail.objWICIdentification") = value
        End Set
    End Property
    Public Property objWICIdentificationBefore() As goAML_Person_Identification
        Get
            Return Session("goAML_WICApprovalDetail.objWICIdentificationBefore")
        End Get
        Set(value As goAML_Person_Identification)
            Session("goAML_WICApprovalDetail.objWICIdentificationBefore") = value
        End Set
    End Property
    Public Property objWICIdentificationDirector() As goAML_Person_Identification
        Get
            Return Session("goAML_WICApprovalDetail.objWICIdentificationDirector")
        End Get
        Set(value As goAML_Person_Identification)
            Session("goAML_WICApprovalDetail.objWICIdentificationDirector") = value
        End Set
    End Property
    Public Property objWICIdentificationDirectorBefore() As goAML_Person_Identification
        Get
            Return Session("goAML_WICApprovalDetail.objWICIdentificationDirectorBefore")
        End Get
        Set(value As goAML_Person_Identification)
            Session("goAML_WICApprovalDetail.objWICIdentificationDirectorBefore") = value
        End Set
    End Property
    Public Property ListObjWICAddress() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICAddress")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail.ListObjWICAddress") = value
        End Set
    End Property
    Public Property ListObjWICAddressBefore() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICAddressBefore")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail.ListObjWICAddressBefore") = value
        End Set
    End Property
    Public Property ListObjWICAddressEmployer() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICAddressEmployer")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail.ListObjWICAddressEmployer") = value
        End Set
    End Property
    Public Property ListObjWICAddressEmployerBefore() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICAddressEmployerBefore")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail.ListObjWICAddressEmployerBefore") = value
        End Set
    End Property
    Public Property ListObjWICAddressDirector() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICAddressDirector")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail.ListObjWICAddressDirector") = value
        End Set
    End Property
    Public Property ListObjWICAddressDirectorBefore() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICAddressDirectorBefore")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail.ListObjWICAddressDirectorBefore") = value
        End Set
    End Property
    Public Property ListObjWICAddressDirectorEmployer() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICAddressDirectorEmployer")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail.ListObjWICAddressDirectorEmployer") = value
        End Set
    End Property
    Public Property ListObjWICAddressDirectorEmployerBefore() As List(Of goAML_Ref_Address)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICAddressDirectorEmployerBefore")
        End Get
        Set(value As List(Of goAML_Ref_Address))
            Session("goAML_WICApprovalDetail.ListObjWICAddressDirectorEmployerBefore") = value
        End Set
    End Property
    Public Property ListObjWICIdentification() As List(Of goAML_Person_Identification)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICIdentification")
        End Get
        Set(value As List(Of goAML_Person_Identification))
            Session("goAML_WICApprovalDetail.ListObjWICIdentification") = value
        End Set
    End Property
    Public Property ListObjWICIdentificationBefore() As List(Of goAML_Person_Identification)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICIdentificationBefore")
        End Get
        Set(value As List(Of goAML_Person_Identification))
            Session("goAML_WICApprovalDetail.ListObjWICIdentificationBefore") = value
        End Set
    End Property
    Public Property ListObjWICIdentificationDirector() As List(Of goAML_Person_Identification)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICIdentificationDirector")
        End Get
        Set(value As List(Of goAML_Person_Identification))
            Session("goAML_WICApprovalDetail.ListObjWICIdentificationDirector") = value
        End Set
    End Property
    Public Property ListObjWICIdentificationDirectorBefore() As List(Of goAML_Person_Identification)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICIdentificationDirectorBefore")
        End Get
        Set(value As List(Of goAML_Person_Identification))
            Session("goAML_WICApprovalDetail.ListObjWICIdentificationDirectorBefore") = value
        End Set
    End Property
    Public Property ListObjWICPhone() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICPhone")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail.ListObjWICPhone") = value
        End Set
    End Property
    Public Property ListObjWICPhoneBefore() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICPhoneBefore")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail.ListObjWICPhoneBefore") = value
        End Set
    End Property
    Public Property ListObjWICPhoneEmployer() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICPhoneEmployer")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail.ListObjWICPhoneEmployer") = value
        End Set
    End Property
    Public Property ListObjWICPhoneEmployerBefore() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICPhonEmployereBefore")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail.ListObjWICPhonEmployereBefore") = value
        End Set
    End Property
    Public Property ListObjWICPhoneDirector() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICPhoneDirector")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail.ListObjWICPhoneDirector") = value
        End Set
    End Property
    Public Property ListObjWICPhoneDirectorBefore() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICPhoneDirectorBefore")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail.ListObjWICPhoneDirectorBefore") = value
        End Set
    End Property
    Public Property ListObjWICPhoneDirectorEmployer() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICPhoneDirectorEmployer")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail.ListObjWICPhoneDirectorEmployer") = value
        End Set
    End Property
    Public Property ListObjWICPhoneDirectorEmployerBefore() As List(Of goAML_Ref_Phone)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICPhoneDirectorEmployerBefore")
        End Get
        Set(value As List(Of goAML_Ref_Phone))
            Session("goAML_WICApprovalDetail.ListObjWICPhoneDirectorEmployerBefore") = value
        End Set
    End Property
    Public Property ListObjWICDirector() As List(Of goAML_Ref_Walk_In_Customer_Director)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICDirector")
        End Get
        Set(value As List(Of goAML_Ref_Walk_In_Customer_Director))
            Session("goAML_WICApprovalDetail.ListObjWICDirector") = value
        End Set
    End Property
    Public Property ListObjWICDirectorBefore() As List(Of goAML_Ref_Walk_In_Customer_Director)
        Get
            Return Session("goAML_WICApprovalDetail.ListObjWICDirectorBefore")
        End Get
        Set(value As List(Of goAML_Ref_Walk_In_Customer_Director))
            Session("goAML_WICApprovalDetail.ListObjWICDirectorBefore") = value
        End Set
    End Property
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try

            If Not Net.X.IsAjaxRequest Then
                ClearSession()
                Dim moduleStr As String = Request.Params("ModuleID")
                Dim moduleID As Integer = Common.DecryptQueryString(moduleStr, SystemParameterBLL.GetEncriptionKey)
                ObjModule = ModuleBLL.GetModuleByModuleID(moduleID)

                Dim dataStr As String = Request.Params("ID")
                Dim dataID As Long = Common.DecryptQueryString(dataStr, SystemParameterBLL.GetEncriptionKey)
                ObjApproval = ModuleApprovalBLL.GetModuleApprovalByID(dataID)

                If ObjModule Is Nothing Then
                    Throw New Exception("Module not found")
                End If

                If Not ObjApproval Is Nothing Then
                    'Validasi hak akses & created by
                    If ObjApproval.CreatedBy <> Common.SessionCurrentUser.UserID Then
                        If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Approval) Then
                            Dim strIDCode As String = 1
                            strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                            Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                            Exit Sub
                        End If
                    End If

                    PanelInfo.Title = ObjModule.ModuleLabel & " Approval"
                    lblModuleName.Text = ObjModule.ModuleLabel
                    lblModuleKey.Text = ObjApproval.ModuleKey
                    lblAction.Text = ModuleBLL.GetModuleActionNamebyID(ObjApproval.PK_ModuleAction_ID)
                    LblCreatedBy.Text = ObjApproval.CreatedBy
                    lblCreatedDate.Text = ObjApproval.CreatedDate.Value.ToString("dd-MMM-yyyy")

                    LoadPopup()
                    LoadColumn()


                Else
                    Throw New Exception("Invalid ID Approval")
                End If

                'FormPanelOld.Visible = (ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update)
                BtnSave.Visible = ObjApproval.CreatedBy <> Common.SessionCurrentUser.UserID
                BtnReject.Visible = ObjApproval.CreatedBy <> Common.SessionCurrentUser.UserID

                Select Case ObjApproval.PK_ModuleAction_ID
                    Case Common.ModuleActionEnum.Insert
                        FormPanelOld.Hide()
                        FormPanelNew.Show()
                        'Dim unikID As String = Guid.NewGuid.ToString
                        'Dim objWIC As New WICBLL
                        'objWIC.LoadPanel(FormPanelNew, ObjApproval.ModuleField, unikID)

                        objWICNew = Common.Deserialize(ObjApproval.ModuleField, GetType(WICDataBLL))

                        ListObjWICPhone = objWICNew.ObjWICPhone
                        ListObjWICAddress = objWICNew.ObjWICAddress

                        PKWIC.Text = objWICNew.ObjWIC.PK_Customer_ID
                        DspWICNo.Text = objWICNew.ObjWIC.WIC_No
                        If objWICNew.ObjWIC.isUpdateFromDataSource = True Then
                            DspIsUpdateFromDataSource.Text = "True"
                        Else
                            DspIsUpdateFromDataSource.Text = "False"
                        End If
                        If objWICNew.ObjWIC.FK_Customer_Type_ID = 1 Then

                            ListObjWICPhoneEmployer = objWICNew.OBjWICPhoneEmployer
                            ListObjWICAddressEmployer = objWICNew.ObjWICAddressEmployer
                            ListObjWICIdentification = objWICNew.ObjWICIdentification

                            With objWICNew.ObjWIC
                                TxtINDV_Title.Text = .INDV_Title
                                TxtINDV_last_name.Text = .INDV_Last_Name
                                If GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender) IsNot Nothing Then
                                    TxtINDV_Gender.Text = GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender)
                                End If
                                If .INDV_BirthDate IsNot Nothing Then
                                    DateINDV_Birthdate.Text = .INDV_BirthDate
                                Else
                                    DateINDV_Birthdate.Text = ""
                                End If
                                TxtINDV_Birth_place.Text = .INDV_Birth_Place
                                TxtINDV_Mothers_name.Text = .INDV_Mothers_Name
                                TxtINDV_Alias.Text = .INDV_Alias
                                TxtINDV_SSN.Text = .INDV_SSN
                                TxtINDV_Passport_number.Text = .INDV_Passport_Number
                                'daniel 19 jan 2021
                                'TxtINDV_Passport_country.Text = .INDV_Passport_Country
                                TxtINDV_Passport_country.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Passport_Country)
                                'end 19 jan 2021
                                TxtINDV_ID_Number.Text = .INDV_ID_Number
                                CmbINDV_Nationality1.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1)
                                CmbINDV_Nationality2.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2)
                                CmbINDV_Nationality3.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3)
                                CmbINDV_residence.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence)
                                TxtINDV_Email.Text = .INDV_Email
                                TxtINDV_Email2.Text = .INDV_Email2
                                TxtINDV_Email3.Text = .INDV_Email3
                                TxtINDV_Email4.Text = .INDV_Email4
                                TxtINDV_Email5.Text = .INDV_Email5
                                TxtINDV_Occupation.Text = .INDV_Occupation
                                TxtINDV_employer_name.Text = .INDV_Employer_Name
                                TxtINDV_Source_Of_Wealth.Text = .INDV_SumberDana
                                TxtINDV_Tax_Number.Text = .INDV_Tax_Number
                                CbINDV_Tax_Reg_Number.Text = .INDV_Tax_Reg_Number
                                If .INDV_Deceased = True Then
                                    CbINDV_Deceased.Text = "True"
                                Else
                                    CbINDV_Deceased.Text = "False"
                                End If
                                If .INDV_Deceased Then
                                    If .INDV_Deceased_Date IsNot Nothing Then
                                        DateINDV_Deceased_Date.Show()
                                        DateINDV_Deceased_Date.Text = .INDV_Deceased_Date
                                    End If
                                End If
                                TxtINDV_Comments.Text = .INDV_Comment
                            End With

                            BindDetailPhone(StorePhoneIndividuDetail, ListObjWICPhone)
                            BindDetailAddress(StoreAddressIndividuDetail, ListObjWICAddress)
                            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListObjWICPhoneEmployer)
                            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListObjWICAddressEmployer)
                            BindDetailIdentification(StoreIdentificationDetail, ListObjWICIdentification)

                            WIC_Individu.Show()
                            WIC_Corporate.Hide()
                            WIC_IndividuOld.Show()
                            WIC_CorporateOld.Hide()

                        Else
                            objWICDirectorNew = objWICNew.ObjDirector

                            If objWICDirectorNew.Count > 0 Then
                                For Each Director In objWICDirectorNew
                                    ListObjWICDirector.Add(Director.ObjDirector)
                                    If Director.ListDirectorAddress.Count > 0 Then
                                        For Each item In Director.ListDirectorAddress
                                            ListObjWICAddressDirector.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each item In Director.ListDirectorAddressEmployer
                                            ListObjWICAddressDirectorEmployer.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorPhone.Count > 0 Then
                                        For Each item In Director.ListDirectorPhone
                                            ListObjWICPhoneDirector.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each item In Director.ListDirectorPhoneEmployer
                                            ListObjWICPhoneDirectorEmployer.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorIdentification.Count > 0 Then
                                        For Each item In Director.ListDirectorIdentification
                                            ListObjWICIdentificationDirector.Add(item)
                                        Next
                                    End If
                                Next
                            End If

                            With objWICNew.ObjWIC
                                TxtCorp_Name.Text = .Corp_Name
                                TxtCorp_Commercial_name.Text = .Corp_Commercial_Name
                                CmbCorp_Incorporation_legal_form.Text = GlobalReportFunctionBLL.getBentukKorporasiByKode(.Corp_Incorporation_Legal_Form)
                                TxtCorp_Incorporation_number.Text = .Corp_Incorporation_Number
                                TxtCorp_Business.Text = .Corp_Business
                                TxtCorp_Email.Text = .Corp_Email
                                TxtCorp_url.Text = .Corp_Url
                                TxtCorp_incorporation_state.Text = .Corp_Incorporation_State
                                'daniel 19 jan 2021
                                'CmbCorp_incorporation_country_code.Text = .Corp_Incorporation_Country_Code
                                CmbCorp_incorporation_country_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Corp_Incorporation_Country_Code)
                                'daniel 19 jan 2021
                                'CmbCorp_Role.Text = .Corp_Role
                                'agam 03112020
                                'DateCorp_incorporation_date.Text = .Corp_Incorporation_Date
                                If .Corp_Incorporation_Date IsNot Nothing Then
                                    DateCorp_incorporation_date.Text = .Corp_Incorporation_Date
                                End If
                                chkCorp_business_closed.Text = .Corp_Business_Closed
                                'agam 03112020
                                If .Corp_Business_Closed Then
                                    If .Corp_Date_Business_Closed IsNot Nothing Then
                                        DateCorp_date_business_closed.Text = .Corp_Date_Business_Closed
                                    End If
                                End If
                                TxtCorp_tax_number.Text = .Corp_Tax_Number
                                TxtCorp_Comments.Text = .Corp_Comments
                            End With

                            BindDetailPhone(StorePhoneCorporateDetail, ListObjWICPhone)
                            BindDetailAddress(StoreAddressCorporateDetail, ListObjWICAddress)
                            BindDetailDirector(Store_Director_Corp, ListObjWICDirector)

                            WIC_Individu.Hide()
                            WIC_Corporate.Show()
                            WIC_IndividuOld.Hide()
                            WIC_CorporateOld.Show()

                        End If

                    Case Common.ModuleActionEnum.Update
                        FormPanelOld.Show()
                        FormPanelNew.Show()
                        'Dim unikID As String = Guid.NewGuid.ToString
                        'Dim objWIC As New WICBLL
                        'objWIC.LoadPanel(FormPanelNew, ObjApproval.ModuleField, unikID)

                        objWICNew = Common.Deserialize(ObjApproval.ModuleField, GetType(WICDataBLL))

                        ListObjWICPhone = objWICNew.ObjWICPhone
                        ListObjWICAddress = objWICNew.ObjWICAddress


                        objWICOld = Common.Deserialize(ObjApproval.ModuleFieldBefore, GetType(WICDataBLL))

                        ListObjWICPhoneBefore = objWICOld.ObjWICPhone
                        ListObjWICAddressBefore = objWICOld.ObjWICAddress

                        PKWIC.Text = objWICNew.ObjWIC.PK_Customer_ID
                        DspWICNo.Text = objWICNew.ObjWIC.WIC_No
                        DspIsUpdateFromDataSource.Text = objWICNew.ObjWIC.isUpdateFromDataSource

                        PKWICOld.Text = objWICOld.ObjWIC.PK_Customer_ID
                        DspWICNoOld.Text = objWICOld.ObjWIC.WIC_No
                        DspIsUpdateFromDataSourceOld.Text = objWICOld.ObjWIC.isUpdateFromDataSource

                        If objWICNew.ObjWIC.FK_Customer_Type_ID = 1 Then

                            ListObjWICPhoneEmployer = objWICNew.OBjWICPhoneEmployer
                            ListObjWICAddressEmployer = objWICNew.ObjWICAddressEmployer
                            ListObjWICIdentification = objWICNew.ObjWICIdentification

                            ListObjWICPhoneEmployerBefore = objWICOld.OBjWICPhoneEmployer
                            ListObjWICAddressEmployerBefore = objWICOld.ObjWICAddressEmployer
                            ListObjWICIdentificationBefore = objWICOld.ObjWICIdentification

                            With objWICNew.ObjWIC
                                TxtINDV_Title.Text = .INDV_Title
                                TxtINDV_last_name.Text = .INDV_Last_Name
                                TxtINDV_Gender.Text = GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender)
                                If .INDV_BirthDate IsNot Nothing Then
                                    DateINDV_Birthdate.Text = .INDV_BirthDate
                                End If
                                TxtINDV_Birth_place.Text = .INDV_Birth_Place
                                TxtINDV_Mothers_name.Text = .INDV_Mothers_Name
                                TxtINDV_Alias.Text = .INDV_Alias
                                TxtINDV_SSN.Text = .INDV_SSN
                                TxtINDV_Passport_number.Text = .INDV_Passport_Number
                                'daniel 19 jan 2021
                                'TxtINDV_Passport_country.Text = .INDV_Passport_Country
                                TxtINDV_Passport_country.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Passport_Country)
                                'end 19 jan 2021
                                TxtINDV_ID_Number.Text = .INDV_ID_Number
                                CmbINDV_Nationality1.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1)
                                CmbINDV_Nationality2.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2)
                                CmbINDV_Nationality3.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3)
                                CmbINDV_residence.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence)
                                TxtINDV_Email.Text = .INDV_Email
                                TxtINDV_Email2.Text = .INDV_Email2
                                TxtINDV_Email3.Text = .INDV_Email3
                                TxtINDV_Email4.Text = .INDV_Email4
                                TxtINDV_Email5.Text = .INDV_Email5
                                TxtINDV_Occupation.Text = .INDV_Occupation
                                TxtINDV_employer_name.Text = .INDV_Employer_Name
                                TxtINDV_Source_Of_Wealth.Text = .INDV_SumberDana
                                TxtINDV_Tax_Number.Text = .INDV_Tax_Number
                                CbINDV_Tax_Reg_Number.Text = Convert.ToString(.INDV_Tax_Reg_Number)
                                CbINDV_Deceased.Text = Convert.ToString(.INDV_Deceased)
                                If .INDV_Deceased Then
                                    DateINDV_Deceased_Date.Show()
                                    If .INDV_Deceased_Date IsNot Nothing Then
                                        DateINDV_Deceased_Date.Text = .INDV_Deceased_Date
                                    End If
                                End If
                                TxtINDV_Comments.Text = .INDV_Comment
                            End With

                            With objWICOld.ObjWIC
                                TxtINDV_TitleOld.Text = .INDV_Title
                                TxtINDV_last_nameOld.Text = .INDV_Last_Name
                                TxtINDV_GenderOld.Text = GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender)
                                If .INDV_BirthDate IsNot Nothing Then
                                    DateINDV_BirthdateOld.Text = .INDV_BirthDate
                                End If
                                TxtINDV_Birth_placeOld.Text = .INDV_Birth_Place
                                TxtINDV_Mothers_nameOld.Text = .INDV_Mothers_Name
                                TxtINDV_AliasOld.Text = .INDV_Alias
                                TxtINDV_SSNOld.Text = .INDV_SSN
                                TxtINDV_Passport_numberOld.Text = .INDV_Passport_Number
                                'daniel 19 jan 2021
                                'TxtINDV_Passport_countryOld.Text = .INDV_Passport_Country
                                TxtINDV_Passport_countryOld.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Passport_Country)
                                'daniel 19 jan 2021
                                TxtINDV_ID_NumberOld.Text = .INDV_ID_Number
                                CmbINDV_Nationality1Old.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1)
                                CmbINDV_Nationality2Old.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2)
                                CmbINDV_Nationality3Old.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3)
                                CmbINDV_residenceOld.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence)
                                TxtINDV_EmailOld.Text = .INDV_Email
                                TxtINDV_Email2Old.Text = .INDV_Email2
                                TxtINDV_Email3Old.Text = .INDV_Email3
                                TxtINDV_Email4Old.Text = .INDV_Email4
                                TxtINDV_Email5Old.Text = .INDV_Email5
                                TxtINDV_OccupationOld.Text = .INDV_Occupation
                                TxtINDV_employer_nameOld.Text = .INDV_Employer_Name
                                TxtINDV_Source_Of_WealthOld.Text = .INDV_SumberDana
                                TxtINDV_Tax_NumberOld.Text = .INDV_Tax_Number
                                CbINDV_Tax_Reg_NumberOld.Text = Convert.ToString(.INDV_Tax_Reg_Number)
                                CbINDV_DeceasedOld.Text = Convert.ToString(.INDV_Deceased)
                                If .INDV_Deceased Then
                                    DateINDV_Deceased_DateOld.Show()
                                    If .INDV_Deceased_Date IsNot Nothing Then
                                        DateINDV_Deceased_DateOld.Text = .INDV_Deceased_Date
                                    End If
                                End If
                                TxtINDV_CommentsOld.Text = .INDV_Comment
                            End With
                            'GridPanelPhoneIndividuDetailBefore.Show()
                            'GridPanelAddressIndividuDetailBefore.Show()

                            'GridPanelIdentificationIndividualDetailBefore.Show()
                            'GridPanelDirectorIdentificationBefore.Show()

                            'GridPanelPhoneCorporateDetailBefore.Show()
                            'GridPanelAddressCorporateDetailBefore.Show()

                            'GridPanelPhoneIndividualEmployerDetailBefore.Show()
                            'GridPanelAddressIndividualEmployerDetailBefore.Show()

                            'BindDetailPhone(StorePhoneIndividuDetailBefore, ListObjWICPhoneBefore)

                            'BindDetailAddress(StoreAddressIndividuDetailBefore, ListObjWICAddressBefore)


                            'BindDetailPhone(StorePhoneEmployerIndividuDetailBefore, ListObjWICPhoneEmployerBefore)

                            'BindDetailAddress(StoreAddressEmployerIndividuDetailBefore, ListObjWICAddressEmployerBefore)

                            'BindDetailIdentification(StoreIdentificationDetailBefore, ListObjWICIdentificationBefore)

                            BindDetailPhone(StorePhoneIndividuDetail, ListObjWICPhone)
                            BindDetailAddress(StoreAddressIndividuDetail, ListObjWICAddress)
                            BindDetailPhone(StorePhoneEmployerIndividuDetail, ListObjWICPhoneEmployer)
                            BindDetailAddress(StoreAddressEmployerIndividuDetail, ListObjWICAddressEmployer)
                            BindDetailIdentification(StoreIdentificationDetail, ListObjWICIdentification)

                            BindDetailPhone(StorePhoneIndividuDetailOld, ListObjWICPhoneBefore)
                            BindDetailAddress(StoreAddressIndividuDetailOld, ListObjWICAddressBefore)
                            BindDetailPhone(StorePhoneEmployerIndividuDetailOld, ListObjWICPhoneEmployerBefore)
                            BindDetailAddress(StoreAddressEmployerIndividuDetailOld, ListObjWICAddressEmployerBefore)
                            BindDetailIdentification(StoreIdentificationDetailOld, ListObjWICIdentificationBefore)

                            WIC_Individu.Show()
                            WIC_Corporate.Hide()
                            WIC_IndividuOld.Show()
                            WIC_CorporateOld.Hide()
                        Else
                            objWICDirectorNew = objWICNew.ObjDirector

                            If objWICDirectorNew.Count > 0 Then
                                For Each Director In objWICDirectorNew
                                    ListObjWICDirector.Add(Director.ObjDirector)
                                    If Director.ListDirectorAddress.Count > 0 Then
                                        For Each item In Director.ListDirectorAddress
                                            ListObjWICAddressDirector.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each item In Director.ListDirectorAddressEmployer
                                            ListObjWICAddressDirectorEmployer.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorPhone.Count > 0 Then
                                        For Each item In Director.ListDirectorPhone
                                            ListObjWICPhoneDirector.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each item In Director.ListDirectorPhoneEmployer
                                            ListObjWICPhoneDirectorEmployer.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorIdentification.Count > 0 Then
                                        For Each item In Director.ListDirectorIdentification
                                            ListObjWICIdentificationDirector.Add(item)
                                        Next
                                    End If
                                Next
                            End If

                            objWICDirectorOld = objWICOld.ObjDirector

                            If objWICDirectorOld.Count > 0 Then
                                For Each Director In objWICDirectorOld
                                    'agam 22102020
                                    If Director.ObjDirector IsNot Nothing Then
                                        ListObjWICDirectorBefore.Add(Director.ObjDirector)
                                        If Director.ListDirectorAddress.Count > 0 Then
                                            For Each item In Director.ListDirectorAddress
                                                ListObjWICAddressDirectorBefore.Add(item)
                                            Next
                                        End If
                                        If Director.ListDirectorAddressEmployer.Count > 0 Then
                                            For Each item In Director.ListDirectorAddressEmployer
                                                ListObjWICAddressDirectorEmployerBefore.Add(item)
                                            Next
                                        End If
                                        If Director.ListDirectorPhone.Count > 0 Then
                                            For Each item In Director.ListDirectorPhone
                                                ListObjWICPhoneDirectorBefore.Add(item)
                                            Next
                                        End If
                                        If Director.ListDirectorPhoneEmployer.Count > 0 Then
                                            For Each item In Director.ListDirectorPhoneEmployer
                                                ListObjWICPhoneDirectorEmployerBefore.Add(item)
                                            Next
                                        End If
                                        If Director.ListDirectorIdentification.Count > 0 Then
                                            For Each item In Director.ListDirectorIdentification
                                                ListObjWICIdentificationDirectorBefore.Add(item)
                                            Next
                                        End If
                                    End If
                                Next
                            End If

                            With objWICNew.ObjWIC
                                TxtCorp_Name.Text = .Corp_Name
                                TxtCorp_Commercial_name.Text = .Corp_Commercial_Name
                                CmbCorp_Incorporation_legal_form.Text = GlobalReportFunctionBLL.getBentukKorporasiByKode(.Corp_Incorporation_Legal_Form)
                                TxtCorp_Incorporation_number.Text = .Corp_Incorporation_Number
                                TxtCorp_Business.Text = .Corp_Business
                                TxtCorp_Email.Text = .Corp_Email
                                TxtCorp_url.Text = .Corp_Url
                                TxtCorp_incorporation_state.Text = .Corp_Incorporation_State
                                'daniel 19 jan 2021
                                'CmbCorp_incorporation_country_code.Text = .Corp_Incorporation_Country_Code
                                CmbCorp_incorporation_country_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Corp_Incorporation_Country_Code)
                                'daniel 19 jan 2021
                                'CmbCorp_Role.Text = .Corp_Role
                                'agam 22102020
                                If .Corp_Incorporation_Date <> Date.MinValue Then
                                    DateCorp_incorporation_date.Text = .Corp_Incorporation_Date
                                End If

                                chkCorp_business_closed.Text = .Corp_Business_Closed
                                If .Corp_Business_Closed Then

                                    DateCorp_date_business_closed.Text = .Corp_Date_Business_Closed
                                End If
                                TxtCorp_tax_number.Text = .Corp_Tax_Number
                                TxtCorp_Comments.Text = .Corp_Comments
                            End With

                            With objWICOld.ObjWIC
                                TxtCorp_NameOld.Text = .Corp_Name
                                TxtCorp_Commercial_nameOld.Text = .Corp_Commercial_Name
                                CmbCorp_Incorporation_legal_formOld.Text = GlobalReportFunctionBLL.getBentukKorporasiByKode(.Corp_Incorporation_Legal_Form)
                                TxtCorp_Incorporation_numberOld.Text = .Corp_Incorporation_Number
                                TxtCorp_BusinessOld.Text = .Corp_Business
                                TxtCorp_EmailOld.Text = .Corp_Email
                                TxtCorp_urlOld.Text = .Corp_Url
                                TxtCorp_incorporation_stateOld.Text = .Corp_Incorporation_State
                                'daniel 19 jan 2021
                                'CmbCorp_incorporation_country_codeOld.Text = .Corp_Incorporation_Country_Code
                                CmbCorp_incorporation_country_codeOld.Text = GlobalReportFunctionBLL.getCountryByCode(.Corp_Incorporation_Country_Code)
                                'daniel 19 jan 2021
                                'CmbCorp_RoleOld.Text = .Corp_Role
                                'agam 22102020
                                If .Corp_Incorporation_Date <> Date.MinValue Then
                                    DateCorp_incorporation_dateOld.Text = .Corp_Incorporation_Date
                                End If

                                chkCorp_business_closedOld.Text = .Corp_Business_Closed
                                If .Corp_Business_Closed Then
                                    DateCorp_date_business_closedOld.Text = .Corp_Date_Business_Closed
                                End If
                                TxtCorp_tax_numberOld.Text = .Corp_Tax_Number
                                TxtCorp_CommentsOld.Text = .Corp_Comments
                            End With
                            'GridPanelPhoneCorporateDetailBefore.Show()
                            'GridPanelAddressCorporateDetailBefore.Show()
                            'GridPanelDirector_CorpBefore.Show()

                            'BindDetailPhone(StorePhoneCorporateDetailBefore, ListObjWICPhoneBefore)

                            'BindDetailAddress(StoreAddressCorporateDetailBefore, ListObjWICAddressBefore)

                            'BindDetailDirector(Store_Director_CorpBefore, ListObjWICDirectorBefore)
                            BindDetailPhone(StorePhoneCorporateDetail, ListObjWICPhone)
                            BindDetailAddress(StoreAddressCorporateDetail, ListObjWICAddress)
                            BindDetailDirector(Store_Director_Corp, ListObjWICDirector)

                            BindDetailPhone(StorePhoneCorporateDetailOld, ListObjWICPhoneBefore)
                            BindDetailAddress(StoreAddressCorporateDetailOld, ListObjWICAddressBefore)
                            BindDetailDirector(Store_Director_CorpOld, ListObjWICDirectorBefore)

                            WIC_Individu.Hide()
                            WIC_Corporate.Show()
                            WIC_IndividuOld.Hide()
                            WIC_CorporateOld.Show()

                        End If

                    Case Common.ModuleActionEnum.Delete
                        FormPanelOld.Show()
                        FormPanelNew.Hide()
                        'Dim unikOldID As String = Guid.NewGuid.ToString
                        'Dim unikNewID As String = Guid.NewGuid.ToString

                        'Dim objWIC As New WICBLL
                        'objWIC.LoadPanel(FormPanelOld, ObjApproval.ModuleFieldBefore, unikOldID)
                        'objWIC.LoadPanel(FormPanelNew, ObjApproval.ModuleField, unikNewID)

                        'Case Common.ModuleActionEnum.Activation
                        '    Dim unikID As String = Guid.NewGuid.ToString
                        '    Dim objWIC As New WICBLL
                        '    objWIC.LoadPanel(FormPanelNew, ObjApproval.ModuleField, unikID)

                        objWICNew = Common.Deserialize(ObjApproval.ModuleField, GetType(WICDataBLL))

                        ListObjWICPhoneBefore = objWICNew.ObjWICPhone
                        ListObjWICAddressBefore = objWICNew.ObjWICAddress

                        PKWICOld.Text = objWICNew.ObjWIC.PK_Customer_ID
                        DspWICNoOld.Text = objWICNew.ObjWIC.WIC_No
                        DspIsUpdateFromDataSourceOld.Text = objWICNew.ObjWIC.isUpdateFromDataSource
                        If objWICNew.ObjWIC.FK_Customer_Type_ID = 1 Then

                            ListObjWICPhoneEmployerBefore = objWICNew.OBjWICPhoneEmployer
                            ListObjWICAddressEmployerBefore = objWICNew.ObjWICAddressEmployer
                            ListObjWICIdentificationBefore = objWICNew.ObjWICIdentification

                            With objWICNew.ObjWIC
                                TxtINDV_TitleOld.Text = .INDV_Title
                                TxtINDV_last_nameOld.Text = .INDV_Last_Name
                                TxtINDV_GenderOld.Text = GlobalReportFunctionBLL.getGenderbyKode(.INDV_Gender)
                                If .INDV_BirthDate IsNot Nothing Then
                                    DateINDV_BirthdateOld.Text = .INDV_BirthDate
                                End If
                                TxtINDV_Birth_placeOld.Text = .INDV_Birth_Place
                                TxtINDV_Mothers_nameOld.Text = .INDV_Mothers_Name
                                TxtINDV_AliasOld.Text = .INDV_Alias
                                TxtINDV_SSNOld.Text = .INDV_SSN
                                TxtINDV_Passport_numberOld.Text = .INDV_Passport_Number
                                'daniel 19 jan 2021
                                'TxtINDV_Passport_countryOld.Text = .INDV_Passport_Country
                                TxtINDV_Passport_countryOld.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Passport_Country)
                                'end 19 jan 2021
                                TxtINDV_ID_NumberOld.Text = .INDV_ID_Number
                                CmbINDV_Nationality1Old.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality1)
                                CmbINDV_Nationality2Old.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality2)
                                CmbINDV_Nationality3Old.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Nationality3)
                                CmbINDV_residenceOld.Text = GlobalReportFunctionBLL.getCountryByCode(.INDV_Residence)
                                TxtINDV_EmailOld.Text = .INDV_Email
                                TxtINDV_Email2Old.Text = .INDV_Email2
                                TxtINDV_Email3Old.Text = .INDV_Email3
                                TxtINDV_Email4Old.Text = .INDV_Email4
                                TxtINDV_Email5Old.Text = .INDV_Email5
                                TxtINDV_OccupationOld.Text = .INDV_Occupation
                                TxtINDV_employer_nameOld.Text = .INDV_Employer_Name
                                TxtINDV_Source_Of_WealthOld.Text = .INDV_SumberDana
                                TxtINDV_Tax_NumberOld.Text = .INDV_Tax_Number
                                CbINDV_Tax_Reg_NumberOld.Text = .INDV_Tax_Reg_Number
                                CbINDV_DeceasedOld.Text = .INDV_Deceased
                                If .INDV_Deceased Then
                                    DateINDV_Deceased_DateOld.Show()
                                    If .INDV_Deceased_Date IsNot Nothing Then
                                        DateINDV_Deceased_DateOld.Text = .INDV_Deceased_Date
                                    End If
                                End If
                                TxtINDV_CommentsOld.Text = .INDV_Comment

                                BindDetailPhone(StorePhoneIndividuDetailOld, ListObjWICPhoneBefore)
                                BindDetailAddress(StoreAddressIndividuDetailOld, ListObjWICAddressBefore)
                                BindDetailPhone(StorePhoneEmployerIndividuDetailOld, ListObjWICPhoneEmployerBefore)
                                BindDetailAddress(StoreAddressEmployerIndividuDetailOld, ListObjWICAddressEmployerBefore)
                                BindDetailIdentification(StoreIdentificationDetailOld, ListObjWICIdentificationBefore)
                            End With

                            WIC_Individu.Hide()
                            WIC_Corporate.Hide()
                            WIC_IndividuOld.Show()
                            WIC_CorporateOld.Hide()
                        Else
                            objWICDirectorNew = objWICNew.ObjDirector

                            If objWICDirectorNew.Count > 0 Then
                                For Each Director In objWICDirectorNew
                                    ListObjWICDirectorBefore.Add(Director.ObjDirector)
                                    If Director.ListDirectorAddress.Count > 0 Then
                                        For Each item In Director.ListDirectorAddress
                                            ListObjWICAddressDirectorBefore.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each item In Director.ListDirectorAddressEmployer
                                            ListObjWICAddressDirectorEmployerBefore.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorPhone.Count > 0 Then
                                        For Each item In Director.ListDirectorPhone
                                            ListObjWICPhoneDirectorBefore.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each item In Director.ListDirectorPhoneEmployer
                                            ListObjWICPhoneDirectorEmployerBefore.Add(item)
                                        Next
                                    End If
                                    If Director.ListDirectorIdentification.Count > 0 Then
                                        For Each item In Director.ListDirectorIdentification
                                            ListObjWICIdentificationDirectorBefore.Add(item)
                                        Next
                                    End If
                                Next
                            End If

                            With objWICNew.ObjWIC
                                TxtCorp_NameOld.Text = .Corp_Name
                                TxtCorp_Commercial_nameOld.Text = .Corp_Commercial_Name
                                CmbCorp_Incorporation_legal_formOld.Text = GlobalReportFunctionBLL.getBentukKorporasiByKode(.Corp_Incorporation_Legal_Form)
                                TxtCorp_Incorporation_numberOld.Text = .Corp_Incorporation_Number
                                TxtCorp_BusinessOld.Text = .Corp_Business
                                TxtCorp_EmailOld.Text = .Corp_Email
                                TxtCorp_urlOld.Text = .Corp_Url
                                TxtCorp_incorporation_stateOld.Text = .Corp_Incorporation_State
                                'daniel 19 jan 2021
                                'CmbCorp_incorporation_country_codeOld.Text = .Corp_Incorporation_Country_Code
                                CmbCorp_incorporation_country_codeOld.Text = GlobalReportFunctionBLL.getCountryByCode(.Corp_Incorporation_Country_Code)
                                'daniel 19 jan 2021
                                'CmbCorp_RoleOld.Text = .Corp_Role
                                DateCorp_incorporation_dateOld.Text = .Corp_Incorporation_Date
                                chkCorp_business_closedOld.Text = .Corp_Business_Closed
                                If .Corp_Business_Closed Then
                                    DateCorp_date_business_closedOld.Text = .Corp_Date_Business_Closed
                                End If
                                TxtCorp_tax_numberOld.Text = .Corp_Tax_Number
                                TxtCorp_CommentsOld.Text = .Corp_Comments
                            End With

                            BindDetailPhone(StorePhoneCorporateDetailOld, ListObjWICPhoneBefore)
                            BindDetailAddress(StoreAddressCorporateDetailOld, ListObjWICAddressBefore)
                            BindDetailDirector(Store_Director_CorpOld, ListObjWICDirectorBefore)

                            WIC_Individu.Hide()
                            WIC_Corporate.Hide()
                            WIC_IndividuOld.Hide()
                            WIC_CorporateOld.Show()

                        End If

                End Select
            End If
        Catch ex As Exception
            BtnSave.Visible = False
            BtnReject.Visible = False
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub LoadPopup()
        WindowDetailAddress.Maximizable = True
        WindowDetailPhone.Maximizable = True
        WindowDetailDirector.Maximizable = True
        WindowDetailDirectorAddress.Maximizable = True
        WindowDetailDirectorIdentification.Maximizable = True
        WindowDetailDirectorPhone.Maximizable = True
        WindowDetailIdentification.Maximizable = True
    End Sub

    Private Sub LoadColumn()

        ColumnActionLocation(GridPanelDirectorPhoneBefore, CommandColumn5)
        ColumnActionLocation(GridPanelDirectorPhone, CommandColumn1)
        ColumnActionLocation(GridPanelDirectorAddressBefore, CommandColumn6)
        ColumnActionLocation(GridPanelDirectorAddress, CommandColumn2)
        ColumnActionLocation(GridPanelDirectorPhoneEmployerBefore, CommandColumn7)
        ColumnActionLocation(GridPanelDirectorPhoneEmployer, CommandColumn3)
        ColumnActionLocation(GridPanelDirectorAddressEmployerBefore, CommandColumn9)
        ColumnActionLocation(GridPanelDirectorAddressEmployer, CommandColumn4)
        ColumnActionLocation(GridPanelDirectorIdentificationBefore, CommandColumn13)
        ColumnActionLocation(GridPanelDirectordentification, CommandColumn12)

        'ColumnActionLocation(GridPanelPhoneIndividuDetailBefore, CommandColumnPhoneIndividuDetailBefore)
        ColumnActionLocation(GridPanelPhoneIndividuDetail, CommandColumnPhoneIndividuDetail)
        ColumnActionLocation(GridPanelPhoneIndividuDetailOld, CommandColumnPhoneIndividuDetailOld)
        'ColumnActionLocation(GridPanelAddressIndividuDetailBefore, CommandColumnAddressIndividuDetailBefore)
        ColumnActionLocation(GridPanelAddressIndividuDetail, CommandColumnAddressIndividuDetail)
        ColumnActionLocation(GridPanelAddressIndividuDetailOld, CommandColumnAddressIndividuDetailOld)
        'ColumnActionLocation(GridPanelPhoneIndividualEmployerDetailBefore, CommandColumnPhoneIndividualEmployerDetailBefore)
        ColumnActionLocation(GridPanelPhoneIndividualEmployerDetail, CommandColumnPhoneIndividualEmployerDetail)
        ColumnActionLocation(GridPanelPhoneIndividualEmployerDetailOld, CommandColumnPhoneIndividualEmployerDetailOld)
        'ColumnActionLocation(GridPanelAddressIndividualEmployerDetailBefore, CommandColumnAddressIndividualEmployerDetailBefore)
        ColumnActionLocation(GridPanelAddressIndividualEmployerDetail, CommandColumnAddressIndividualEmployerDetail)
        ColumnActionLocation(GridPanelAddressIndividualEmployerDetailOld, CommandColumnAddressIndividualEmployerDetailOld)
        'ColumnActionLocation(GridPanelIdentificationIndividualDetailBefore, CommandColumnIdentificationIndividualDetailBefore)
        ColumnActionLocation(GridPanelIdentificationIndividualDetail, CommandColumnIdentificationIndividualDetail)
        ColumnActionLocation(GridPanelIdentificationIndividualDetailOld, CommandColumnIdentificationIndividualDetailOld)

        'ColumnActionLocation(GridPanelPhoneCorporateDetailBefore, CommandColumnPhoneCorporateDetailBefore)
        ColumnActionLocation(GridPanelPhoneCorporateDetail, CommandColumnPhoneCorporateDetail)
        ColumnActionLocation(GridPanelPhoneCorporateDetailOld, CommandColumnPhoneCorporateDetailOld)
        'ColumnActionLocation(GridPanelAddressCorporateDetailBefore, CommandColumnAddressCorporateDetailBefore)
        ColumnActionLocation(GridPanelAddressCorporateDetail, CommandColumnAddressCorporateDetail)
        ColumnActionLocation(GridPanelAddressCorporateDetailOld, CommandColumnAddressCorporateDetailOld)
        'ColumnActionLocation(GridPanelDirector_CorpBefore, CommandColumnDirectorBefore)
        ColumnActionLocation(GP_Corp_Director, CommandColumnDirector)
        ColumnActionLocation(GP_Corp_DirectorOld, CommandColumnDirectorOld)

    End Sub
    Private Sub ColumnActionLocation(gridPanel As GridPanel, commandColumn As CommandColumn)
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim bsettingRight As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                bsettingRight = objParamSettingbutton.SettingValue
            End If
            If bsettingRight = 1 Then

            ElseIf bsettingRight = 2 Then
                gridPanel.ColumnModel.Columns.RemoveAt(gridPanel.ColumnModel.Columns.Count - 1)
                gridPanel.ColumnModel.Columns.Insert(1, commandColumn)
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub ClearSession()

        objWIC = New goAML_Ref_WIC
        objWICAddress = New goAML_Ref_Address
        objWICAddressBefore = New goAML_Ref_Address
        objWICAddressDirector = New goAML_Ref_Address
        objWICAddressDirectorBefore = New goAML_Ref_Address
        objWICAddressDirectorEmployer = New goAML_Ref_Address
        objWICAddressDirectorEmployerBefore = New goAML_Ref_Address
        objWICAddressEmployer = New goAML_Ref_Address
        objWICAddressEmployerBefore = New goAML_Ref_Address
        objWICBefore = New goAML_Ref_WIC
        objWICDirector = New goAML_Ref_Walk_In_Customer_Director
        objWICDirectorBefore = New goAML_Ref_Walk_In_Customer_Director
        objWICDirectorNew = New List(Of WICDirectorDataBLL)
        objWICDirectorOld = New List(Of WICDirectorDataBLL)
        objWICIdentification = New goAML_Person_Identification
        objWICIdentificationBefore = New goAML_Person_Identification
        objWICIdentificationDirector = New goAML_Person_Identification
        objWICIdentificationDirectorBefore = New goAML_Person_Identification
        objWICNew = New WICDataBLL
        objWICOld = New WICDataBLL
        objWICPhone = New goAML_Ref_Phone
        objWICPhoneBefore = New goAML_Ref_Phone
        objWICPhoneDirector = New goAML_Ref_Phone
        objWICPhoneDirectorBefore = New goAML_Ref_Phone
        objWICPhoneDirectorEmployer = New goAML_Ref_Phone
        objWICPhoneDirectorEmployerBefore = New goAML_Ref_Phone
        objWICPhoneEmployer = New goAML_Ref_Phone
        objWICPhoneEmployerBefore = New goAML_Ref_Phone



        ListObjWICAddress = New List(Of goAML_Ref_Address)
        ListObjWICAddressBefore = New List(Of goAML_Ref_Address)
        ListObjWICAddressDirector = New List(Of goAML_Ref_Address)
        ListObjWICAddressDirectorBefore = New List(Of goAML_Ref_Address)
        ListObjWICAddressDirectorEmployer = New List(Of goAML_Ref_Address)
        ListObjWICAddressDirectorEmployerBefore = New List(Of goAML_Ref_Address)
        ListObjWICAddressEmployer = New List(Of goAML_Ref_Address)
        ListObjWICAddressEmployerBefore = New List(Of goAML_Ref_Address)
        ListObjWICDirector = New List(Of goAML_Ref_Walk_In_Customer_Director)
        ListObjWICDirectorBefore = New List(Of goAML_Ref_Walk_In_Customer_Director)
        ListObjWICIdentification = New List(Of goAML_Person_Identification)
        ListObjWICIdentificationBefore = New List(Of goAML_Person_Identification)
        ListObjWICIdentificationDirector = New List(Of goAML_Person_Identification)
        ListObjWICIdentificationDirectorBefore = New List(Of goAML_Person_Identification)
        ListObjWICPhone = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneBefore = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneDirector = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneDirectorBefore = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneDirectorEmployer = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneDirectorEmployerBefore = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneEmployer = New List(Of goAML_Ref_Phone)
        ListObjWICPhoneEmployerBefore = New List(Of goAML_Ref_Phone)

    End Sub
#End Region

#Region "Bind"
    Private Sub BindDetailIdentification(store As Store, list As List(Of goAML_Person_Identification))
        Dim objtable As DataTable = NawaBLL.Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Tipe", GetType(String)))
        objtable.Columns.Add(New DataColumn("No", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssueDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("ExpiryDate", GetType(String)))
        objtable.Columns.Add(New DataColumn("IssuedCountry", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Tipe") = GlobalReportFunctionBLL.getjenisDokumenByKode(item("Type"))
                item("No") = item("Number")
                item("IssueDate") = item("Issue_Date")
                item("ExpiryDate") = item("Expiry_Date")
                item("IssuedCountry") = GlobalReportFunctionBLL.getCountryByCode(item("Issued_Country"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailPhone(store As Store, list As List(Of goAML_Ref_Phone))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Contact_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("Communication_Type", GetType(String)))
        objtable.Columns.Add(New DataColumn("number", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Contact_Type") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Tph_Contact_Type"))
                item("Communication_Type") = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(item("Tph_Communication_Type"))
                item("number") = item("tph_number")
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailAddress(store As Store, list As List(Of goAML_Ref_Address))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("Type_Address", GetType(String)))
        objtable.Columns.Add(New DataColumn("Addres", GetType(String)))
        objtable.Columns.Add(New DataColumn("Cty", GetType(String)))
        objtable.Columns.Add(New DataColumn("CountryCode", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("Type_Address") = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(item("Address_Type"))
                item("Addres") = item("Address")
                item("Cty") = item("City")
                item("CountryCode") = GlobalReportFunctionBLL.getCountryByCode(item("Country_Code"))
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
    Private Sub BindDetailDirector(store As Store, list As List(Of goAML_Ref_Walk_In_Customer_Director))
        Dim objtable As DataTable = Common.CopyGenericToDataTable(list)
        objtable.Columns.Add(New DataColumn("LastName", GetType(String)))
        objtable.Columns.Add(New DataColumn("Roles", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                item("LastName") = item("Last_Name")
                item("Roles") = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(item("Role").ToString)
            Next
        End If
        store.DataSource = objtable
        store.DataBind()
    End Sub
#End Region

#Region "Grid"
    Protected Sub GrdCmdDirectorPhoneBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneDirectorBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorPhone(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneDirector(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorAddressBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressDirectorBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorAddress(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressDirector(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerPhoneBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneEmployerDirectorBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerPhone(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneEmployerDirector(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerAddressBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressEmployerDirectorBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorEmployerAddress(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressEmployerDirector(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorIdentificationBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentidicationDirectorBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdDirectorIdentification(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentidicationDirector(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdIdentification(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentidication(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GrdCmdIdentificationOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailIdentidication(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GrdCmdIdentificationBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailIdentidicationBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneIndividuBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneINDVBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneIndividu(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneINDV(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GridCommandPhoneIndividuOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailPhoneINDV(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GridCommandPhoneEmployerIndividuBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneINDVEmployerBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneEmployerIndividu(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneINDVEmployer(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GridCommandPhoneEmployerIndividuOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailPhoneINDVEmployer(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GridCommandAddressIndividuBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressINDVBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressIndividu(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressINDV(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GridCommandAddressIndividuOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailAddressINDV(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GridCommandAddressEmployerIndividuBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressINDVEmployerBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressEmployerIndividu(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressINDVEmployer(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GridCommandAddressEmployerIndividuOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailAddressINDVEmployer(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GridCommandPhoneCorporateBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneCorpBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandPhoneCorporate(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailPhoneCorp(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GridCommandPhoneCorporateOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailPhoneCorp(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GridCommandAddressCorporateBefore(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressCorpBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridCommandAddressCorporate(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailAddressCorp(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Protected Sub GridCommandAddressCorporateOld(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailAddressCorp(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    'Protected Sub GrdCmdCustDirectorBefore(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim ID As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            DetailDirectorCorpBefore(ID)
    '        End If
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub
    Protected Sub GrdCmdCustDirector(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailDirectorCorp(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GrdCmdCustDirectorOld(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                DetailDirectorCorpBefore(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region

#Region "Detail"
    Private Sub ClearDetailDirector()
        DspPeranDirector.Text = ""
        DspGelarDirector.Text = ""
        DspNamaLengkap.Text = ""
        DspGender.Text = ""
        DspTanggalLahir.Text = ""
        DspTempatLahir.Text = ""
        DspNamaIbuKandung.Text = ""
        DspNamaAlias.Text = ""
        DspNIK.Text = ""
        DspNoPassport.Text = ""
        DspNegaraPenerbitPassport.Text = ""
        DspNoIdentitasLain.Text = ""
        DspKewarganegaraan1.Text = ""
        DspKewarganegaraan2.Text = ""
        DspKewarganegaraan3.Text = ""
        DspNegaraDomisili.Text = ""
        DspEmail.Text = ""
        DspEmail2.Text = ""
        DspEmail3.Text = ""
        DspEmail4.Text = ""
        DspEmail5.Text = ""
        DspDeceased.Text = ""
        DspDeceasedDate.Text = ""
        DspPEP.Text = ""
        DspNPWP.Text = ""
        DspSourceofWealth.Text = ""
        DspOccupation.Text = ""
        DspCatatan.Text = ""
        DspTempatBekerja.Text = ""
    End Sub
    Private Sub ClearDetailDirectorEdit()
        DspPeranDirector.Text = " => "
        DspGelarDirector.Text = " => "
        DspNamaLengkap.Text = " => "
        DspGender.Text = " => "
        DspTanggalLahir.Text = " => "
        DspTempatLahir.Text = " => "
        DspNamaIbuKandung.Text = " => "
        DspNamaAlias.Text = " => "
        DspNIK.Text = " => "
        DspNoPassport.Text = " => "
        DspNegaraPenerbitPassport.Text = " => "
        DspNoIdentitasLain.Text = " => "
        DspKewarganegaraan1.Text = " => "
        DspKewarganegaraan2.Text = " => "
        DspKewarganegaraan3.Text = " => "
        DspNegaraDomisili.Text = " => "
        DspEmail.Text = " => "
        DspEmail2.Text = " => "
        DspEmail3.Text = " => "
        DspEmail4.Text = " => "
        DspEmail5.Text = " => "
        DspDeceased.Text = " => "
        DspDeceasedDate.Text = " => "
        DspPEP.Text = " => "
        DspNPWP.Text = " => "
        DspSourceofWealth.Text = " => "
        DspOccupation.Text = " => "
        DspCatatan.Text = " => "
        DspTempatBekerja.Text = " => "
    End Sub
    Private Sub DetailDirectorCorp(iD As String)
        WindowDetailDirector.Show()

        GridPanelDirectorPhoneBefore.Hide()
        GridPanelDirectorAddressBefore.Hide()
        GridPanelDirectorPhoneEmployerBefore.Hide()
        GridPanelDirectorAddressEmployerBefore.Hide()
        GridPanelDirectorIdentificationBefore.Hide()

        GridPanelDirectorPhone.Show()
        GridPanelDirectorAddress.Show()
        GridPanelDirectorPhoneEmployer.Show()
        GridPanelDirectorAddressEmployer.Show()
        GridPanelDirectordentification.Show()

        Dim objDirector As goAML_Ref_Walk_In_Customer_Director = ListObjWICDirector.Where(Function(x) x.NO_ID = iD).FirstOrDefault
        If objDirector IsNot Nothing Then
            ClearDetailDirector()
            With objDirector
                DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
                DspGelarDirector.Text = .Title
                DspNamaLengkap.Text = .Last_Name
                DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                If .BirthDate IsNot Nothing Then
                    DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yyy")
                End If
                DspTempatLahir.Text = .Birth_Place
                DspNamaIbuKandung.Text = .Mothers_Name
                DspNamaAlias.Text = .Alias
                DspNIK.Text = .SSN
                DspNoPassport.Text = .Passport_Number
                'daniel 19 jan 2021
                'DspNegaraPenerbitPassport.Text = .Passport_Country
                DspNegaraPenerbitPassport.Text = GlobalReportFunctionBLL.getCountryByCode(.Passport_Country)
                'end 19 jan 2021
                DspNoIdentitasLain.Text = .ID_Number
                DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                DspEmail.Text = .Email
                DspEmail2.Text = .Email2
                DspEmail3.Text = .Email3
                DspEmail4.Text = .Email4
                DspEmail5.Text = .Email5
                DspDeceased.Text = .Deceased
                If .Deceased_Date IsNot Nothing Then
                    DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                End If
                DspPEP.Text = .Tax_Reg_Number
                DspNPWP.Text = .Tax_Number
                DspSourceofWealth.Text = .Source_of_Wealth
                DspOccupation.Text = .Occupation
                DspCatatan.Text = .Comments
                DspTempatBekerja.Text = .Employer_Name

                BindDetailAddress(StoreDirectorAddress, ListObjWICAddressDirector)
                BindDetailAddress(StoreDirectorAddressEmployer, ListObjWICAddressDirectorEmployer)
                BindDetailPhone(StoreDirectorPhone, ListObjWICPhoneDirector)
                BindDetailPhone(StoreDirectorPhoneEmployer, ListObjWICPhoneDirectorEmployer)
                BindDetailIdentification(StoreIdentificationDirector, ListObjWICIdentificationDirector)
            End With
        End If

        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then

        '    'agam 22102020
        '    Dim objDirectorOld As goAML_Ref_Walk_In_Customer_Director
        '    If ListObjWICDirectorBefore.Count > 0 Then
        '        objDirectorOld = ListObjWICDirectorBefore.Where(Function(x) x.NO_ID = iD).FirstOrDefault
        '    End If

        '    If objDirectorOld Is Nothing Then
        '        ClearDetailDirector()
        '        With objDirector
        '            DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
        '            DspGelarDirector.Text = .Title
        '            DspNamaLengkap.Text = .Last_Name
        '            DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
        '            If .BirthDate IsNot Nothing Then
        '                DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yyy")
        '            End If
        '            DspTempatLahir.Text = .Birth_Place
        '            DspNamaIbuKandung.Text = .Mothers_Name
        '            DspNamaAlias.Text = .Alias
        '            DspNIK.Text = .SSN
        '            DspNoPassport.Text = .Passport_Number
        '            DspNegaraPenerbitPassport.Text = .Passport_Country
        '            DspNoIdentitasLain.Text = .ID_Number
        '            DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
        '            DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
        '            DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
        '            DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
        '            DspEmail.Text = .Email
        '            DspEmail2.Text = .Email2
        '            DspEmail3.Text = .Email3
        '            DspEmail4.Text = .Email4
        '            DspEmail5.Text = .Email5
        '            DspDeceased.Text = .Deceased
        '            If .Deceased_Date IsNot Nothing Then
        '                DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            DspPEP.Text = .Tax_Reg_Number
        '            DspNPWP.Text = .Tax_Number
        '            DspSourceofWealth.Text = .Source_of_Wealth
        '            DspOccupation.Text = .Occupation
        '            DspCatatan.Text = .Comments
        '            DspTempatBekerja.Text = .Employer_Name

        '            BindDetailAddress(StoreDirectorAddress, ListObjWICAddressDirector)
        '            BindDetailAddress(StoreDirectorAddressEmployer, ListObjWICAddressDirectorEmployer)

        '            BindDetailPhone(StoreDirectorPhoneBefore, ListObjWICPhoneDirector)
        '            BindDetailPhone(StoreDirectorPhoneEmployer, ListObjWICPhoneDirectorEmployer)

        '            BindDetailIdentification(StoreIdentificationDirector, ListObjWICIdentificationDirector)

        '        End With
        '    Else
        '        ClearDetailDirectorEdit()
        '        With objDirector
        '            DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(objDirectorOld.Role) + " => " + GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
        '            DspGelarDirector.Text = objDirectorOld.Title + " => " + .Title
        '            DspNamaLengkap.Text = objDirectorOld.Last_Name + " => " + .Last_Name
        '            DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(objDirectorOld.Gender) + " => " + GlobalReportFunctionBLL.getGenderbyKode(.Gender)
        '            If .BirthDate IsNot Nothing And objDirectorOld.BirthDate IsNot Nothing Then
        '                DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yyy") + " => " + objDirectorOld.BirthDate.Value.ToString("dd-MMM-yy")
        '            ElseIf .BirthDate Is Nothing And objDirectorOld.BirthDate Is Nothing Then
        '                DspTanggalLahir.Text = " => "
        '            ElseIf .BirthDate IsNot Nothing Then
        '                DspTanggalLahir.Text = .BirthDate + " => "
        '            ElseIf objWICOld.ObjWIC.INDV_BirthDate IsNot Nothing Then
        '                DspTanggalLahir.Text = " => " + objDirectorOld.BirthDate
        '            End If
        '            DspTempatLahir.Text = objDirectorOld.Birth_Place + " => " + .Birth_Place
        '            DspNamaIbuKandung.Text = objDirectorOld.Mothers_Name + " => " + .Mothers_Name
        '            DspNamaAlias.Text = objDirectorOld.Alias + " => " + .Alias
        '            DspNIK.Text = objDirectorOld.SSN + " => " + .SSN
        '            DspNoPassport.Text = objDirectorOld.Passport_Number + " => " + .Passport_Number
        '            DspNegaraPenerbitPassport.Text = objDirectorOld.Passport_Country + " => " + .Passport_Country
        '            DspNoIdentitasLain.Text = objDirectorOld.ID_Number + " => " + .ID_Number
        '            DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(objDirectorOld.Nationality1) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
        '            DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(objDirectorOld.Nationality2) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
        '            DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(objDirectorOld.Nationality3) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
        '            DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(objDirectorOld.Residence) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Residence)
        '            DspEmail.Text = objDirectorOld.Email + " => " + .Email
        '            DspEmail2.Text = objDirectorOld.Email2 + " => " + .Email2
        '            DspEmail3.Text = objDirectorOld.Email3 + " => " + .Email3
        '            DspEmail4.Text = objDirectorOld.Email4 + " => " + .Email4
        '            DspEmail5.Text = objDirectorOld.Email5 + " => " + .Email5
        '            DspDeceased.Text = Convert.ToString(objDirectorOld.Deceased) + " => " + Convert.ToString(.Deceased)
        '            If objDirectorOld.Deceased_Date IsNot Nothing And .Deceased_Date IsNot Nothing Then
        '                DspDeceasedDate.Text = objDirectorOld.Deceased_Date + " => " + .Deceased_Date
        '            ElseIf objDirectorOld.Deceased_Date Is Nothing And .Deceased_Date Is Nothing Then
        '                DspDeceasedDate.Text = " => "
        '            ElseIf objDirectorOld.Deceased_Date IsNot Nothing Then
        '                DspDeceasedDate.Text = objDirectorOld.Deceased_Date + " => "
        '            ElseIf .Deceased_Date IsNot Nothing Then
        '                DspDeceasedDate.Text = " => " + .Deceased_Date
        '            End If
        '            DspPEP.Text = objDirectorOld.Tax_Reg_Number + " => " + .Tax_Reg_Number
        '            DspNPWP.Text = objDirectorOld.Tax_Number + " => " + .Tax_Number
        '            DspSourceofWealth.Text = objDirectorOld.Source_of_Wealth + " => " + .Source_of_Wealth
        '            DspOccupation.Text = objDirectorOld.Occupation + " => " + .Occupation
        '            DspCatatan.Text = objDirectorOld.Comments + " => " + .Comments
        '            DspTempatBekerja.Text = objDirectorOld.Employer_Name + " => " + .Employer_Name

        '            GridPanelDirectorAddressBefore.Show()
        '            GridPanelDirectorAddressEmployerBefore.Show()
        '            GridPanelDirectorPhoneBefore.Show()
        '            GridPanelDirectorPhoneEmployerBefore.Show()
        '            GridPanelDirectorIdentificationBefore.Show()

        '            GridPanelDirectorPhone.Show()
        '            GridPanelDirectorAddress.Show()
        '            GridPanelDirectorPhoneEmployer.Show()
        '            GridPanelDirectorAddressEmployer.Show()
        '            GridPanelDirectordentification.Show()

        '            BindDetailAddress(StoreDirectorAddressBefore, ListObjWICAddressDirectorBefore)
        '            BindDetailAddress(StoreDirectorAddressEmployerBefore, ListObjWICAddressDirectorEmployerBefore)
        '            BindDetailAddress(StoreDirectorAddress, ListObjWICAddressDirector)
        '            BindDetailAddress(StoreDirectorAddressEmployer, ListObjWICAddressDirectorEmployer)

        '            BindDetailPhone(StoreDirectorPhoneBefore, ListObjWICPhoneDirectorBefore)
        '            BindDetailPhone(StoreDirectorPhone, ListObjWICPhoneDirector)
        '            BindDetailPhone(StoreDirectorPhoneEmployerBefore, ListObjWICPhoneDirectorEmployerBefore)
        '            BindDetailPhone(StoreDirectorPhoneEmployer, ListObjWICPhoneDirectorEmployer)

        '            BindDetailIdentification(StoreIdentificationDirector, ListObjWICIdentificationDirector)
        '            BindDetailIdentification(StoreIdentificationDirectorBefore, ListObjWICIdentificationDirectorBefore)

        '        End With
        '    End If

        'Else
        '    Dim objDirector As goAML_Ref_Walk_In_Customer_Director = ListObjWICDirector.Where(Function(x) x.NO_ID = iD).FirstOrDefault
        '    ClearDetailDirector()
        '    With objDirector
        '        DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
        '        DspGelarDirector.Text = .Title
        '        DspNamaLengkap.Text = .Last_Name
        '        DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
        '        If .BirthDate IsNot Nothing Then
        '            DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yyy")
        '        End If
        '        DspTempatLahir.Text = .Birth_Place
        '        DspNamaIbuKandung.Text = .Mothers_Name
        '        DspNamaAlias.Text = .Alias
        '        DspNIK.Text = .SSN
        '        DspNoPassport.Text = .Passport_Number
        '        DspNegaraPenerbitPassport.Text = .Passport_Country
        '        DspNoIdentitasLain.Text = .ID_Number
        '        DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
        '        DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
        '        DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
        '        DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
        '        DspEmail.Text = .Email
        '        DspEmail2.Text = .Email2
        '        DspEmail3.Text = .Email3
        '        DspEmail4.Text = .Email4
        '        DspEmail5.Text = .Email5
        '        DspDeceased.Text = .Deceased
        '        If .Deceased_Date IsNot Nothing Then
        '            DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
        '        End If
        '        DspPEP.Text = .Tax_Reg_Number
        '        DspNPWP.Text = .Tax_Number
        '        DspSourceofWealth.Text = .Source_of_Wealth
        '        DspOccupation.Text = .Occupation
        '        DspCatatan.Text = .Comments
        '        DspTempatBekerja.Text = .Employer_Name

        '        BindDetailAddress(StoreDirectorAddress, ListObjWICAddressDirector)
        '        BindDetailAddress(StoreDirectorAddressEmployer, ListObjWICAddressDirectorEmployer)

        '        BindDetailPhone(StoreDirectorPhone, ListObjWICPhoneDirector)
        '        BindDetailPhone(StoreDirectorPhoneEmployer, ListObjWICPhoneDirectorEmployer)

        '        BindDetailIdentification(StoreIdentificationDirector, ListObjWICIdentificationDirector)

        '    End With
        'End If
    End Sub
    Private Sub DetailDirectorCorpBefore(iD As String)
        WindowDetailDirector.Show()

        GridPanelDirectorPhoneBefore.Show()
        GridPanelDirectorAddressBefore.Show()
        GridPanelDirectorPhoneEmployerBefore.Show()
        GridPanelDirectorAddressEmployerBefore.Show()
        GridPanelDirectorIdentificationBefore.Show()

        GridPanelDirectorPhone.Hide()
        GridPanelDirectorAddress.Hide()
        GridPanelDirectorPhoneEmployer.Hide()
        GridPanelDirectorAddressEmployer.Hide()
        GridPanelDirectordentification.Hide()

        Dim objDirector As goAML_Ref_Walk_In_Customer_Director = ListObjWICDirectorBefore.Where(Function(x) x.NO_ID = iD).FirstOrDefault
        If objDirector IsNot Nothing Then
            ClearDetailDirector()
            With objDirector
                DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
                DspGelarDirector.Text = .Title
                DspNamaLengkap.Text = .Last_Name
                DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
                If .BirthDate IsNot Nothing Then
                    DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yyy")
                End If
                DspTempatLahir.Text = .Birth_Place
                DspNamaIbuKandung.Text = .Mothers_Name
                DspNamaAlias.Text = .Alias
                DspNIK.Text = .SSN
                DspNoPassport.Text = .Passport_Number
                DspNegaraPenerbitPassport.Text = .Passport_Country
                DspNoIdentitasLain.Text = .ID_Number
                DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
                DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
                DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
                DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
                DspEmail.Text = .Email
                DspEmail2.Text = .Email2
                DspEmail3.Text = .Email3
                DspEmail4.Text = .Email4
                DspEmail5.Text = .Email5
                DspDeceased.Text = .Deceased
                If .Deceased_Date IsNot Nothing Then
                    DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
                End If
                DspPEP.Text = .Tax_Reg_Number
                DspNPWP.Text = .Tax_Number
                DspSourceofWealth.Text = .Source_of_Wealth
                DspOccupation.Text = .Occupation
                DspCatatan.Text = .Comments
                DspTempatBekerja.Text = .Employer_Name

                BindDetailAddress(StoreDirectorAddressBefore, ListObjWICAddressDirectorBefore)
                BindDetailAddress(StoreDirectorAddressEmployerBefore, ListObjWICAddressDirectorEmployerBefore)
                BindDetailPhone(StoreDirectorPhoneBefore, ListObjWICPhoneDirectorBefore)
                BindDetailPhone(StoreDirectorPhoneEmployerBefore, ListObjWICPhoneDirectorEmployerBefore)
                BindDetailIdentification(StoreIdentificationDirectorBefore, ListObjWICIdentificationDirectorBefore)
            End With
        End If
        'WindowDetailDirector.Show()
        'ClearDetailDirector()
        'Dim objDirector As goAML_Ref_Walk_In_Customer_Director = ListObjWICDirectorBefore.Where(Function(x) x.NO_ID = iD).FirstOrDefault

        'With objDirector
        '    DspPeranDirector.Text = GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(.Role)
        '    DspGelarDirector.Text = .Title
        '    DspNamaLengkap.Text = .Last_Name
        '    DspGender.Text = GlobalReportFunctionBLL.getGenderbyKode(.Gender)
        '    If .BirthDate IsNot Nothing Then
        '        DspTanggalLahir.Text = .BirthDate.Value.ToString("dd-MMM-yyy")
        '    End If
        '    DspTempatLahir.Text = .Birth_Place
        '    DspNamaIbuKandung.Text = .Mothers_Name
        '    DspNamaAlias.Text = .Alias
        '    DspNIK.Text = .SSN
        '    DspNoPassport.Text = .Passport_Number
        '    DspNegaraPenerbitPassport.Text = .Passport_Country
        '    DspNoIdentitasLain.Text = .ID_Number
        '    DspKewarganegaraan1.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality1)
        '    DspKewarganegaraan2.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality2)
        '    DspKewarganegaraan3.Text = GlobalReportFunctionBLL.getCountryByCode(.Nationality3)
        '    DspNegaraDomisili.Text = GlobalReportFunctionBLL.getCountryByCode(.Residence)
        '    DspEmail.Text = .Email
        '    DspEmail2.Text = .Email2
        '    DspEmail3.Text = .Email3
        '    DspEmail4.Text = .Email4
        '    DspEmail5.Text = .Email5
        '    DspDeceased.Text = .Deceased
        '    If .Deceased_Date IsNot Nothing Then
        '        DspDeceasedDate.Text = .Deceased_Date.Value.ToString("dd-MMM-yy")
        '    End If
        '    DspPEP.Text = .Tax_Reg_Number
        '    DspNPWP.Text = .Tax_Number
        '    DspSourceofWealth.Text = .Source_of_Wealth
        '    DspOccupation.Text = .Occupation
        '    DspCatatan.Text = .Comments
        '    DspTempatBekerja.Text = .Employer_Name

        '    GridPanelDirectorAddress.Hide()
        '    GridPanelDirectorAddressBefore.Show()

        '    GridPanelDirectorAddressEmployer.Hide()
        '    GridPanelDirectorAddressEmployerBefore.Show()

        '    GridPanelDirectorPhone.Hide()
        '    GridPanelDirectorPhoneBefore.Show()

        '    GridPanelDirectorPhoneEmployer.Hide()
        '    GridPanelDirectorPhoneEmployerBefore.Show()

        '    GridPanelDirectordentification.Hide()
        '    GridPanelDirectorIdentificationBefore.Show()

        '    BindDetailAddress(StoreDirectorAddressBefore, ListObjWICAddressDirectorBefore)
        '    BindDetailAddress(StoreDirectorAddressEmployerBefore, ListObjWICAddressDirectorEmployerBefore)

        '    BindDetailPhone(StoreDirectorPhoneBefore, ListObjWICPhoneDirectorBefore)
        '    BindDetailPhone(StoreDirectorPhoneEmployerBefore, ListObjWICPhoneDirectorEmployerBefore)
        'End With
    End Sub

    'Address
    Private Sub ClearDetailAddress()
        DspAddress_type.Text = ""
        DspAddress.Text = ""
        DspTown.Text = ""
        DspCity.Text = ""
        DspZip.Text = ""
        Dspcountry_code.Text = ""
        DspState.Text = ""
        DspcommentsAddress.Text = ""
    End Sub
    Private Sub ClearDetailAddressEmployer()
        DspAddress_typeEmployer.Text = ""
        DspAddressEmployer.Text = ""
        DspTownEmployer.Text = ""
        DspCityEmployer.Text = ""
        DspZipEmployer.Text = ""
        Dspcountry_codeEmployer.Text = ""
        DspStateEmployer.Text = ""
        DspcommentsAddressEmployer.Text = ""
    End Sub
    Private Sub ClearDetailAddressDirector()
        DspTipeAlamatDirector.Text = ""
        DspAlamatDirector.Text = ""
        DspKecamatanDirector.Text = ""
        DspKotaKabupatenDirector.Text = ""
        DspKodePosDirector.Text = ""
        DspNegaraDirector.Text = ""
        DspProvinsiDirector.Text = ""
        DspCatatanAddressDirector.Text = ""
    End Sub
    Private Sub ClearDetailAddressDirectorEmployer()
        DspTipeAlamatDirectorEmployer.Text = ""
        DspAlamatDirectorEmployer.Text = ""
        DspKecamatanDirectorEmployer.Text = ""
        DspKotaKabupatenDirectorEmployer.Text = ""
        DspKodePosDirectorEmployer.Text = ""
        DspNegaraDirectorEmployer.Text = ""
        DspProvinsiDirectorEmployer.Text = ""
        DspCatatanAddressDirectorEmployer.Text = ""
    End Sub
    Private Sub DetailAddressCorp(iD As String)
        WindowDetailAddress.Show()
        ClearDetailAddress()
        Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    'Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    'Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        '    Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault

        '    If objAddressOld Is Nothing Then
        '        With objAddress
        '            DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAddress.Text = .Address
        '            DspTown.Text = .Town
        '            DspCity.Text = .City
        '            DspZip.Text = .Zip
        '            Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspState.Text = .State
        '            DspcommentsAddress.Text = .Comments
        '        End With
        '    Else
        '        With objAddress
        '            DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objAddressOld.Address_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAddress.Text = objAddressOld.Address + " => " + .Address
        '            DspTown.Text = objAddressOld.Town + " => " + .Town
        '            DspCity.Text = objAddressOld.City + " => " + .City
        '            DspZip.Text = objAddressOld.Zip + " => " + .Zip
        '            Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(objAddressOld.Country_Code) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspState.Text = objAddressOld.State + " => " + .State
        '            DspcommentsAddress.Text = objAddressOld.Comments + " => " + .Comments
        '        End With
        '    End If

        'Else
        '    'Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault

        '    With objAddress
        '        DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '        DspAddress.Text = .Address
        '        DspTown.Text = .Town
        '        DspCity.Text = .City
        '        DspZip.Text = .Zip
        '        Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '        DspState.Text = .State
        '        DspcommentsAddress.Text = .Comments
        '    End With
        'End If
    End Sub

    Private Sub DetailAddressCorpBefore(iD As String)
        WindowDetailAddress.Show()
        ClearDetailAddress()
        'Dim objAddress As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
    End Sub

    Private Sub DetailAddressINDV(iD As String)
        WindowDetailAddress.Show()
        PanelDetailAddress.Show()
        PanelDetailAddressEmployer.Hide()
        ClearDetailAddress()
        Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    'Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    'Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        '    Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault

        '    If objAddressOld Is Nothing Then
        '        With objAddress
        '            DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAddress.Text = .Address
        '            DspTown.Text = .Town
        '            DspCity.Text = .City
        '            DspZip.Text = .Zip
        '            Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspState.Text = .State
        '            DspcommentsAddress.Text = .Comments
        '        End With
        '    Else
        '        With objAddress
        '            DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objAddressOld.Address_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAddress.Text = objAddressOld.Address + " => " + .Address
        '            DspTown.Text = objAddressOld.Town + " => " + .Town
        '            DspCity.Text = objAddressOld.City + " => " + .City
        '            DspZip.Text = objAddressOld.Zip + " => " + .Zip
        '            Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(objAddressOld.Country_Code) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspState.Text = objAddressOld.State + " => " + .State
        '            DspcommentsAddress.Text = objAddressOld.Comments + " => " + .Comments
        '        End With
        '    End If

        'Else
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddress.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault

        '    With objAddress
        '        DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '        DspAddress.Text = .Address
        '        DspTown.Text = .Town
        '        DspCity.Text = .City
        '        DspZip.Text = .Zip
        '        Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '        DspState.Text = .State
        '        DspcommentsAddress.Text = .Comments
        '    End With
        'End If
    End Sub

    Private Sub DetailAddressINDVBefore(iD As String)
        WindowDetailAddress.Show()
        PanelDetailAddress.Show()
        PanelDetailAddressEmployer.Hide()
        ClearDetailAddress()
        'Dim objAddress As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspAddress_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddress.Text = .Address
                DspTown.Text = .Town
                DspCity.Text = .City
                DspZip.Text = .Zip
                Dspcountry_code.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspState.Text = .State
                DspcommentsAddress.Text = .Comments
            End With
        End If
    End Sub

    Private Sub DetailAddressINDVEmployer(iD As String)
        WindowDetailAddress.Show()
        PanelDetailAddress.Hide()
        PanelDetailAddressEmployer.Show()
        ClearDetailAddressEmployer()
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressEmployer.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddressEmployer.Text = .Address
                DspTownEmployer.Text = .Town
                DspCityEmployer.Text = .City
                DspZipEmployer.Text = .Zip
                Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspStateEmployer.Text = .State
                DspcommentsAddressEmployer.Text = .Comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddressEmployer.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault
        '    Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressEmployerBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault

        '    If objAddressOld Is Nothing Then
        '        With objAddress
        '            DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAddressEmployer.Text = .Address
        '            DspTownEmployer.Text = .Town
        '            DspCityEmployer.Text = .City
        '            DspZipEmployer.Text = .Zip
        '            Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspStateEmployer.Text = .State
        '            DspcommentsAddressEmployer.Text = .Comments
        '        End With
        '    Else
        '        With objAddress
        '            DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objAddressOld.Address_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAddressEmployer.Text = objAddressOld.Address + " => " + .Address
        '            DspTownEmployer.Text = objAddressOld.Town + " => " + .Town
        '            DspCityEmployer.Text = objAddressOld.City + " => " + .City
        '            DspZipEmployer.Text = objAddressOld.Zip + " => " + .Zip
        '            Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(objAddressOld.Country_Code) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspStateEmployer.Text = objAddressOld.State + " => " + .State
        '            DspcommentsAddressEmployer.Text = objAddressOld.Comments + " => " + .Comments
        '        End With
        '    End If

        'Else
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddressEmployer.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault

        '    With objAddress
        '        DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '        DspAddressEmployer.Text = .Address
        '        DspTownEmployer.Text = .Town
        '        DspCityEmployer.Text = .City
        '        DspZipEmployer.Text = .Zip
        '        Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '        DspStateEmployer.Text = .State
        '        DspcommentsAddressEmployer.Text = .Comments
        '    End With
        'End If
    End Sub

    Private Sub DetailAddressINDVEmployerBefore(iD As String)
        WindowDetailAddress.Show()
        PanelDetailAddress.Hide()
        PanelDetailAddressEmployer.Show()
        ClearDetailAddressEmployer()
        'Dim objAddress As goAML_Ref_Address = ListObjWICAddressEmployerBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressEmployerBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspAddress_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAddressEmployer.Text = .Address
                DspTownEmployer.Text = .Town
                DspCityEmployer.Text = .City
                DspZipEmployer.Text = .Zip
                Dspcountry_codeEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspStateEmployer.Text = .State
                DspcommentsAddressEmployer.Text = .Comments
            End With
        End If
    End Sub

    Private Sub DetailAddressEmployerDirector(iD As String)
        WindowDetailDirectorAddress.Show()
        PanelAddressDirector.Hide()
        PanelAddressDirectorEmployer.Show()
        ClearDetailAddressDirectorEmployer()
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorEmployer.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAlamatDirectorEmployer.Text = .Address
                DspKecamatanDirectorEmployer.Text = .Town
                DspKotaKabupatenDirectorEmployer.Text = .City
                DspKodePosDirectorEmployer.Text = .Zip
                DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspProvinsiDirectorEmployer.Text = .State
                DspCatatanAddressDirectorEmployer.Text = .Comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorEmployer.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 9).FirstOrDefault
        '    Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressDirectorEmployerBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 9).FirstOrDefault

        '    If objAddressOld Is Nothing Then
        '        With objAddress
        '            DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAlamatDirectorEmployer.Text = .Address
        '            DspKecamatanDirectorEmployer.Text = .Town
        '            DspKotaKabupatenDirectorEmployer.Text = .City
        '            DspKodePosDirectorEmployer.Text = .Zip
        '            DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspProvinsiDirectorEmployer.Text = .State
        '            DspCatatanAddressDirectorEmployer.Text = .Comments
        '        End With
        '    Else
        '        With objAddress
        '            DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objAddressOld.Address_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAlamatDirectorEmployer.Text = objAddressOld.Address + " => " + .Address
        '            DspKecamatanDirectorEmployer.Text = objAddressOld.Town + " => " + .Town
        '            DspKotaKabupatenDirectorEmployer.Text = objAddressOld.City + " => " + .City
        '            DspKodePosDirectorEmployer.Text = objAddressOld.Zip + " => " + .Zip
        '            DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(objAddressOld.Country_Code) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspProvinsiDirectorEmployer.Text = objAddressOld.State + " => " + .State
        '            DspCatatanAddressDirectorEmployer.Text = objAddressOld.Comments + " => " + .Comments
        '        End With
        '    End If

        'Else
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorEmployer.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 9).FirstOrDefault

        '    With objAddress
        '        DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '        DspAlamatDirectorEmployer.Text = .Address
        '        DspKecamatanDirectorEmployer.Text = .Town
        '        DspKotaKabupatenDirectorEmployer.Text = .City
        '        DspKodePosDirectorEmployer.Text = .Zip
        '        DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '        DspProvinsiDirectorEmployer.Text = .State
        '        DspCatatanAddressDirectorEmployer.Text = .Comments
        '    End With
        'End If
    End Sub

    Private Sub DetailAddressEmployerDirectorBefore(iD As String)
        WindowDetailDirectorAddress.Show()
        PanelAddressDirector.Hide()
        PanelAddressDirectorEmployer.Show()
        ClearDetailAddressDirectorEmployer()
        'Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorEmployerBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 9).FirstOrDefault
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorEmployerBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspTipeAlamatDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAlamatDirectorEmployer.Text = .Address
                DspKecamatanDirectorEmployer.Text = .Town
                DspKotaKabupatenDirectorEmployer.Text = .City
                DspKodePosDirectorEmployer.Text = .Zip
                DspNegaraDirectorEmployer.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspProvinsiDirectorEmployer.Text = .State
                DspCatatanAddressDirectorEmployer.Text = .Comments
            End With
        End If
    End Sub

    Private Sub DetailAddressDirector(iD As String)
        WindowDetailDirectorAddress.Show()
        PanelAddressDirector.Show()
        PanelAddressDirectorEmployer.Hide()
        ClearDetailAddressDirector()
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirector.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAlamatDirector.Text = .Address
                DspKecamatanDirector.Text = .Town
                DspKotaKabupatenDirector.Text = .City
                DspKodePosDirector.Text = .Zip
                DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspProvinsiDirector.Text = .State
                DspCatatanAddressDirector.Text = .Comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirector.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault
        '    Dim objAddressOld As goAML_Ref_Address = ListObjWICAddressDirectorBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault

        '    If objAddressOld Is Nothing Then
        '        With objAddress
        '            DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAlamatDirector.Text = .Address
        '            DspKecamatanDirector.Text = .Town
        '            DspKotaKabupatenDirector.Text = .City
        '            DspKodePosDirector.Text = .Zip
        '            DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspProvinsiDirector.Text = .State
        '            DspCatatanAddressDirector.Text = .Comments
        '        End With
        '    Else
        '        With objAddress
        '            DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objAddressOld.Address_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '            DspAlamatDirector.Text = objAddressOld.Address + " => " + .Address
        '            DspKecamatanDirector.Text = objAddressOld.Town + " => " + .Town
        '            DspKotaKabupatenDirector.Text = objAddressOld.City + " => " + .City
        '            DspKodePosDirector.Text = objAddressOld.Zip + " => " + .Zip
        '            DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(objAddressOld.Country_Code) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '            DspProvinsiDirector.Text = objAddressOld.State + " => " + .State
        '            DspCatatanAddressDirector.Text = objAddressOld.Comments + " => " + .Comments
        '        End With
        '    End If

        'Else
        '    Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirector.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault

        '    With objAddress
        '        DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
        '        DspAlamatDirector.Text = .Address
        '        DspKecamatanDirector.Text = .Town
        '        DspKotaKabupatenDirector.Text = .City
        '        DspKodePosDirector.Text = .Zip
        '        DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
        '        DspProvinsiDirector.Text = .State
        '        DspCatatanAddressDirector.Text = .Comments
        '    End With
        'End If
    End Sub

    Private Sub DetailAddressDirectorBefore(iD As String)
        WindowDetailDirectorAddress.Show()
        PanelAddressDirector.Show()
        PanelAddressDirectorEmployer.Hide()
        ClearDetailAddressDirector()
        'Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorBefore.Where(Function(x) x.PK_Customer_Address_ID = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault
        Dim objAddress As goAML_Ref_Address = ListObjWICAddressDirectorBefore.Where(Function(x) x.PK_Customer_Address_ID = iD).FirstOrDefault
        If objAddress IsNot Nothing Then
            With objAddress
                DspTipeAlamatDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Address_Type)
                DspAlamatDirector.Text = .Address
                DspKecamatanDirector.Text = .Town
                DspKotaKabupatenDirector.Text = .City
                DspKodePosDirector.Text = .Zip
                DspNegaraDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Country_Code)
                DspProvinsiDirector.Text = .State
                DspCatatanAddressDirector.Text = .Comments
            End With
        End If
    End Sub


    'Phone
    Private Sub ClearDetailPhone()
        Dsptph_contact_type.Text = ""
        Dsptph_communication_type.Text = ""
        Dsptph_country_prefix.Text = ""
        Dsptph_number.Text = ""
        Dsptph_extension.Text = ""
        DspcommentsPhone.Text = ""
    End Sub

    Private Sub ClearDetailPhoneEmployer()
        Dsptph_contact_typeEmployer.Text = ""
        Dsptph_communication_typeEmployer.Text = ""
        Dsptph_country_prefixEmployer.Text = ""
        Dsptph_numberEmployer.Text = ""
        Dsptph_extensionEmployer.Text = ""
        DspcommentsPhoneEmployer.Text = ""
    End Sub

    Private Sub ClearDetailPhoneDirector()
        DspKategoriKontakDirector.Text = ""
        DspJenisAlatKomunikasiDirector.Text = ""
        DspKodeAreaTelpDirector.Text = ""
        DspNomorTeleponDirector.Text = ""
        DspNomorExtensiDirector.Text = ""
        DspCatatanDirectorPhoneDirector.Text = ""
    End Sub

    Private Sub ClearDetailPhoneDirectorEmployer()
        DspKategoriKontakDirectorEmployer.Text = ""
        DspJenisAlatKomunikasiDirectorEmployer.Text = ""
        DspKodeAreaTelpDirectorEmployer.Text = ""
        DspNomorTeleponDirectorEmployer.Text = ""
        DspNomorExtensiDirectorEmployer.Text = ""
        DspCatatanDirectorPhoneDirectorEmployer.Text = ""
    End Sub
    Private Sub DetailPhoneCorp(iD As String)
        WindowDetailPhone.Show()
        ClearDetailPhone()
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhone.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhone.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    Dim objPhoneOld As goAML_Ref_Phone = ListObjWICPhoneBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault

        '    If objPhoneOld Is Nothing Then
        '        With objPhone
        '            Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            Dsptph_country_prefix.Text = .tph_country_prefix
        '            Dsptph_number.Text = .tph_number
        '            Dsptph_extension.Text = .tph_extension
        '            DspcommentsPhone.Text = .comments
        '        End With
        '    Else
        '        With objPhone
        '            Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objPhone.Tph_Contact_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(objPhoneOld.Tph_Communication_Type) + " => " + GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            Dsptph_country_prefix.Text = objPhoneOld.tph_country_prefix + " => " + .tph_country_prefix
        '            Dsptph_number.Text = objPhoneOld.tph_number + " => " + .tph_number
        '            Dsptph_extension.Text = objPhoneOld.tph_extension + " => " + .tph_extension
        '            DspcommentsPhone.Text = objPhoneOld.comments + " => " + .comments
        '        End With
        '    End If

        'Else
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhone.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault

        '    With objPhone
        '        Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '        Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '        Dsptph_country_prefix.Text = .tph_country_prefix
        '        Dsptph_number.Text = .tph_number
        '        Dsptph_extension.Text = .tph_extension
        '        DspcommentsPhone.Text = .comments
        '    End With
        'End If
    End Sub

    Private Sub DetailPhoneCorpBefore(iD As String)
        WindowDetailPhone.Show()
        ClearDetailPhone()
        'Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
    End Sub

    Private Sub DetailPhoneINDV(iD As String)
        WindowDetailPhone.Show()
        PanelDetailPhone.Show()
        PanelDetailPhoneEmployer.Hide()
        ClearDetailPhone()
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhone.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhone.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        '    Dim objPhoneOld As goAML_Ref_Phone = ListObjWICPhoneBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault

        '    If objPhoneOld Is Nothing Then
        '        With objPhone
        '            Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            Dsptph_country_prefix.Text = .tph_country_prefix
        '            Dsptph_number.Text = .tph_number
        '            Dsptph_extension.Text = .tph_extension
        '            DspcommentsPhone.Text = .comments
        '        End With
        '    Else
        '        With objPhone
        '            Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objPhone.Tph_Contact_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(objPhoneOld.Tph_Communication_Type) + " => " + GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            Dsptph_country_prefix.Text = objPhoneOld.tph_country_prefix + " => " + .tph_country_prefix
        '            Dsptph_number.Text = objPhoneOld.tph_number + " => " + .tph_number
        '            Dsptph_extension.Text = objPhoneOld.tph_extension + " => " + .tph_extension
        '            DspcommentsPhone.Text = objPhoneOld.comments + " => " + .comments
        '        End With
        '    End If

        'Else
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhone.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault

        '    With objPhone
        '        Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '        Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '        Dsptph_country_prefix.Text = .tph_country_prefix
        '        Dsptph_number.Text = .tph_number
        '        Dsptph_extension.Text = .tph_extension
        '        DspcommentsPhone.Text = .comments
        '    End With
        'End If
    End Sub

    Private Sub DetailPhoneINDVBefore(iD As String)
        WindowDetailPhone.Show()
        PanelDetailPhone.Show()
        PanelDetailPhoneEmployer.Hide()
        ClearDetailPhone()
        'Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 3).FirstOrDefault
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                Dsptph_contact_type.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_type.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefix.Text = .tph_country_prefix
                Dsptph_number.Text = .tph_number
                Dsptph_extension.Text = .tph_extension
                DspcommentsPhone.Text = .comments
            End With
        End If
    End Sub

    Private Sub DetailPhoneINDVEmployer(iD As String)
        WindowDetailPhone.Show()
        PanelDetailPhone.Hide()
        PanelDetailPhoneEmployer.Show()
        ClearDetailPhoneEmployer()
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneEmployer.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefixEmployer.Text = .tph_country_prefix
                Dsptph_numberEmployer.Text = .tph_number
                Dsptph_extensionEmployer.Text = .tph_extension
                DspcommentsPhoneEmployer.Text = .comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneEmployer.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault
        '    Dim objPhoneOld As goAML_Ref_Phone = ListObjWICPhoneEmployerBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault

        '    If objPhoneOld Is Nothing Then
        '        With objPhone
        '            Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            Dsptph_country_prefixEmployer.Text = .tph_country_prefix
        '            Dsptph_numberEmployer.Text = .tph_number
        '            Dsptph_extensionEmployer.Text = .tph_extension
        '            DspcommentsPhoneEmployer.Text = .comments
        '        End With
        '    Else
        '        With objPhone
        '            Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objPhone.Tph_Contact_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(objPhoneOld.Tph_Communication_Type) + " => " + GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            Dsptph_country_prefixEmployer.Text = objPhoneOld.tph_country_prefix + " => " + .tph_country_prefix
        '            Dsptph_numberEmployer.Text = objPhoneOld.tph_number + " => " + .tph_number
        '            Dsptph_extensionEmployer.Text = objPhoneOld.tph_extension + " => " + .tph_extension
        '            DspcommentsPhoneEmployer.Text = objPhoneOld.comments + " => " + .comments
        '        End With
        '    End If

        'Else
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneEmployer.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault

        '    With objPhone
        '        Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '        Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '        Dsptph_country_prefixEmployer.Text = .tph_country_prefix
        '        Dsptph_numberEmployer.Text = .tph_number
        '        Dsptph_extensionEmployer.Text = .tph_extension
        '        DspcommentsPhoneEmployer.Text = .comments
        '    End With
        'End If
    End Sub

    Private Sub DetailPhoneINDVEmployerBefore(iD As String)
        WindowDetailPhone.Show()
        PanelDetailPhone.Hide()
        PanelDetailPhoneEmployer.Show()
        ClearDetailPhoneEmployer()
        'Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneEmployerBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 10).FirstOrDefault
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneEmployerBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                Dsptph_contact_typeEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                Dsptph_communication_typeEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                Dsptph_country_prefixEmployer.Text = .tph_country_prefix
                Dsptph_numberEmployer.Text = .tph_number
                Dsptph_extensionEmployer.Text = .tph_extension
                DspcommentsPhoneEmployer.Text = .comments
            End With
        End If
    End Sub

    Private Sub DetailPhoneEmployerDirector(iD As String)
        WindowDetailDirectorPhone.Show()
        PanelPhoneDirector.Hide()
        PanelPhoneDirectorEmployer.Show()
        ClearDetailPhoneDirectorEmployer()
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirectorEmployer.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        With objPhone
            DspKategoriKontakDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
            DspJenisAlatKomunikasiDirectorEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
            DspKodeAreaTelpDirectorEmployer.Text = .tph_country_prefix
            DspNomorTeleponDirectorEmployer.Text = .tph_number
            DspNomorExtensiDirectorEmployer.Text = .tph_extension
            DspCatatanDirectorPhoneDirectorEmployer.Text = .comments
        End With
    End Sub

    Private Sub DetailPhoneEmployerDirectorBefore(iD As String)
        WindowDetailDirectorPhone.Show()
        PanelPhoneDirector.Hide()
        PanelPhoneDirectorEmployer.Show()
        ClearDetailPhoneDirectorEmployer()
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirectorEmployerBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault

        With objPhone
            DspKategoriKontakDirectorEmployer.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
            DspJenisAlatKomunikasiDirectorEmployer.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
            DspKodeAreaTelpDirectorEmployer.Text = .tph_country_prefix
            DspNomorTeleponDirectorEmployer.Text = .tph_number
            DspNomorExtensiDirectorEmployer.Text = .tph_extension
            DspCatatanDirectorPhoneDirectorEmployer.Text = .comments
        End With
    End Sub

    Private Sub DetailPhoneDirector(iD As String)
        WindowDetailDirectorPhone.Show()
        PanelPhoneDirector.Show()
        PanelPhoneDirectorEmployer.Hide()
        ClearDetailPhoneDirector()
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirector.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                DspKodeAreaTelpDirector.Text = .tph_country_prefix
                DspNomorTeleponDirector.Text = .tph_number
                DspNomorExtensiDirector.Text = .tph_extension
                DspCatatanDirectorPhoneDirector.Text = .comments
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirector.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault
        '    Dim objPhoneOld As goAML_Ref_Phone = ListObjWICPhoneDirectorBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault

        '    If objPhoneOld Is Nothing Then
        '        With objPhone
        '            DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            DspKodeAreaTelpDirector.Text = .tph_country_prefix
        '            DspNomorTeleponDirector.Text = .tph_number
        '            DspNomorExtensiDirector.Text = .tph_extension
        '            DspCatatanDirectorPhoneDirector.Text = .comments
        '        End With
        '    Else
        '        With objPhone
        '            DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(objPhone.Tph_Contact_Type) + " => " + GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '            DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(objPhoneOld.Tph_Communication_Type) + " => " + GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '            DspKodeAreaTelpDirector.Text = objPhoneOld.tph_country_prefix + " => " + .tph_country_prefix
        '            DspNomorTeleponDirector.Text = objPhoneOld.tph_number + " => " + .tph_number
        '            DspNomorExtensiDirector.Text = objPhoneOld.tph_extension + " => " + .tph_extension
        '            DspCatatanDirectorPhoneDirector.Text = objPhoneOld.comments + " => " + .comments
        '        End With
        '    End If

        'Else
        '    Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirector.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault

        '    With objPhone
        '        DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
        '        DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
        '        DspKodeAreaTelpDirector.Text = .tph_country_prefix
        '        DspNomorTeleponDirector.Text = .tph_number
        '        DspNomorExtensiDirector.Text = .tph_extension
        '        DspCatatanDirectorPhoneDirector.Text = .comments
        '    End With
        'End If
    End Sub

    Private Sub DetailPhoneDirectorBefore(iD As String)
        WindowDetailDirectorPhone.Show()
        PanelPhoneDirector.Show()
        PanelPhoneDirectorEmployer.Hide()
        ClearDetailPhoneDirector()
        'Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirectorBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD And x.FK_Ref_Detail_Of = 8).FirstOrDefault
        Dim objPhone As goAML_Ref_Phone = ListObjWICPhoneDirectorBefore.Where(Function(x) x.PK_goAML_Ref_Phone = iD).FirstOrDefault
        If objPhone IsNot Nothing Then
            With objPhone
                DspKategoriKontakDirector.Text = GlobalReportFunctionBLL.getTypeKontakAlamatbyKode(.Tph_Contact_Type)
                DspJenisAlatKomunikasiDirector.Text = GlobalReportFunctionBLL.getjenisAlatKomunikasiByKode(.Tph_Communication_Type)
                DspKodeAreaTelpDirector.Text = .tph_country_prefix
                DspNomorTeleponDirector.Text = .tph_number
                DspNomorExtensiDirector.Text = .tph_extension
                DspCatatanDirectorPhoneDirector.Text = .comments
            End With
        End If
    End Sub


    'Identification
    Private Sub ClearDetailIdentification()
        DsptType.Text = ""
        DspNumber.Text = ""
        DspIssueDate.Text = ""
        DspExpiryDate.Text = ""
        DspIssuedBy.Text = ""
        DspIssuedCountry.Text = ""
        DspIdentificationComment.Text = ""
    End Sub
    Private Sub ClearDetailIdentificationDirector()
        DsptTypeDirector.Text = ""
        DspNumberDirector.Text = ""
        DspIssueDateDirector.Text = ""
        DspExpiryDateDirector.Text = ""
        DspIssuedByDirector.Text = ""
        DspIssuedCountryDirector.Text = ""
        DspIdentificationCommentDirector.Text = ""
    End Sub
    Private Sub DetailIdentidicationDirector(iD As String)
        WindowDetailDirectorIdentification.Show()
        PanelDetailIdentificationDirector.Show()
        ClearDetailIdentificationDirector()
        Dim objIdentification As goAML_Person_Identification = ListObjWICIdentificationDirector.Where(Function(x) x.PK_Person_Identification_ID = iD).FirstOrDefault
        If objIdentification IsNot Nothing Then
            With objIdentification
                DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                DspNumberDirector.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDateDirector.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDateDirector.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedByDirector.Text = .Issued_By
                DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                DspIdentificationCommentDirector.Text = .Identification_Comment
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objIdentification As goAML_Person_Identification = ListObjWICIdentificationDirector.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 7).FirstOrDefault
        '    Dim objIdentificationOld As goAML_Person_Identification = ListObjWICIdentificationDirectorBefore.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 7).FirstOrDefault

        '    If objIdentificationOld Is Nothing Then
        '        With objIdentificationOld
        '            If .Type IsNot Nothing Then
        '                DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
        '            End If
        '            DspNumberDirector.Text = .Number
        '            If .Issue_Date IsNot Nothing Then
        '                DspIssueDateDirector.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            If .Expiry_Date IsNot Nothing Then
        '                DspExpiryDateDirector.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            DspIssuedByDirector.Text = .Issued_By
        '            DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
        '            DspIdentificationCommentDirector.Text = .Identification_Comment
        '        End With
        '    Else
        '        With objIdentificationOld
        '            DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(objIdentificationOld.Type) + " => " + GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
        '            DspNumberDirector.Text = objIdentificationOld.Number + " => " + .Number
        '            If objIdentificationOld.Issue_Date IsNot Nothing And .Issue_Date IsNot Nothing Then
        '                DspIssueDateDirector.Text = objIdentificationOld.Issue_Date.Value.ToString("dd-MMM-yy") + " => " + .Issue_Date.Value.ToString("dd-MMM-yy")
        '            ElseIf objIdentificationOld.Issue_Date Is Nothing And .Issue_Date Is Nothing Then
        '                DspIssueDateDirector.Text = " => "
        '            ElseIf objIdentificationOld.Issue_Date IsNot Nothing Then
        '                DspIssueDateDirector.Text = objIdentificationOld.Issue_Date.Value.ToString("dd-MMM-yy") + " => "
        '            ElseIf .Issue_Date IsNot Nothing Then
        '                DspIssueDateDirector.Text = " => " + .Issue_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            If objIdentificationOld.Expiry_Date IsNot Nothing And .Expiry_Date IsNot Nothing Then
        '                DspExpiryDateDirector.Text = objIdentificationOld.Expiry_Date.Value.ToString("dd-MMM-yy") + " => " + .Expiry_Date.Value.ToString("dd-MMM-yy")
        '            ElseIf objIdentificationOld.Expiry_Date Is Nothing And .Expiry_Date Is Nothing Then
        '                DspExpiryDateDirector.Text = " => "
        '            ElseIf objIdentificationOld.Expiry_Date IsNot Nothing Then
        '                DspExpiryDateDirector.Text = objIdentificationOld.Expiry_Date.Value.ToString("dd-MMM-yy") + " => "
        '            ElseIf .Expiry_Date IsNot Nothing Then
        '                DspExpiryDateDirector.Text = " => " + .Expiry_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            DspIssuedByDirector.Text = objIdentificationOld.Issued_By + " => " + .Issued_By
        '            DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(objIdentificationOld.Issued_Country) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
        '            DspIdentificationCommentDirector.Text = objIdentificationOld.Identification_Comment + " => " + .Identification_Comment
        '        End With
        '    End If

        'Else
        '    Dim objIdentification As goAML_Person_Identification = ListObjWICIdentificationDirector.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 7).FirstOrDefault

        '    With objIdentification
        '        DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
        '        DspNumberDirector.Text = .Number
        '        If .Issue_Date IsNot Nothing Then
        '            DspIssueDateDirector.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
        '        End If
        '        If .Expiry_Date IsNot Nothing Then
        '            DspExpiryDateDirector.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
        '        End If
        '        DspIssuedByDirector.Text = .Issued_By
        '        DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
        '        DspIdentificationCommentDirector.Text = .Identification_Comment
        '    End With
        'End If
    End Sub

    Private Sub DetailIdentidicationDirectorBefore(iD As String)
        WindowDetailDirectorIdentification.Show()
        PanelDetailIdentificationDirector.Show()
        ClearDetailIdentificationDirector()
        'Dim objIdentification As goAML_Person_Identification = ListObjWICIdentificationDirectorBefore.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 7).FirstOrDefault
        Dim objIdentification As goAML_Person_Identification = ListObjWICIdentificationDirectorBefore.Where(Function(x) x.PK_Person_Identification_ID = iD).FirstOrDefault
        If objIdentification IsNot Nothing Then
            With objIdentification
                DsptTypeDirector.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                DspNumberDirector.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDateDirector.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDateDirector.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedByDirector.Text = .Issued_By
                DspIssuedCountryDirector.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                DspIdentificationCommentDirector.Text = .Identification_Comment
            End With
        End If
    End Sub

    Private Sub DetailIdentidication(iD As String)
        WindowDetailIdentification.Show()
        PanelDetailIdentification.Show()
        ClearDetailIdentification()
        Dim objIdentification As goAML_Person_Identification = ListObjWICIdentification.Where(Function(x) x.PK_Person_Identification_ID = iD).FirstOrDefault
        If objIdentification IsNot Nothing Then
            With objIdentification
                If .Type IsNot Nothing Then
                    DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                End If
                DspNumber.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDate.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDate.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedBy.Text = .Issued_By
                DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                DspIdentificationComment.Text = .Identification_Comment
            End With
        End If
        'If ObjApproval.PK_ModuleAction_ID = Common.ModuleActionEnum.Update Then
        '    Dim objIdentification As goAML_Person_Identification = ListObjWICIdentification.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 8).FirstOrDefault
        '    Dim objIdentificationOld As goAML_Person_Identification = ListObjWICIdentificationBefore.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 8).FirstOrDefault

        '    If objIdentificationOld Is Nothing Then
        '        With objIdentificationOld
        '            DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
        '            DspNumber.Text = .Number
        '            If .Issue_Date IsNot Nothing Then
        '                DspIssueDate.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            If .Expiry_Date IsNot Nothing Then
        '                DspExpiryDate.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            DspIssuedBy.Text = .Issued_By
        '            DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
        '            DspIdentificationComment.Text = .Identification_Comment
        '        End With
        '    Else
        '        With objIdentificationOld
        '            DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(objIdentificationOld.Type) + " => " + GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
        '            DspNumber.Text = objIdentificationOld.Number + " => " + .Number

        '            If objIdentificationOld.Issue_Date IsNot Nothing And .Issue_Date IsNot Nothing Then
        '                DspIssueDate.Text = objIdentificationOld.Issue_Date.Value.ToString("dd-MMM-yy") + " => " + .Issue_Date.Value.ToString("dd-MMM-yy")
        '            ElseIf objIdentificationOld.Issue_Date Is Nothing And .Issue_Date Is Nothing Then
        '                DspIssueDate.Text = " => "
        '            ElseIf objIdentificationOld.Issue_Date IsNot Nothing Then
        '                DspIssueDate.Text = objIdentificationOld.Issue_Date.Value.ToString("dd-MMM-yy") + " => "
        '            ElseIf .Issue_Date IsNot Nothing Then
        '                DspIssueDate.Text = " => " + .Issue_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            If objIdentificationOld.Expiry_Date IsNot Nothing And .Expiry_Date IsNot Nothing Then
        '                DspExpiryDate.Text = objIdentificationOld.Expiry_Date.Value.ToString("dd-MMM-yy") + " => " + .Expiry_Date.Value.ToString("dd-MMM-yy")
        '            ElseIf objIdentificationOld.Expiry_Date Is Nothing And .Expiry_Date Is Nothing Then
        '                DspExpiryDate.Text = " => "
        '            ElseIf objIdentificationOld.Expiry_Date IsNot Nothing Then
        '                DspExpiryDate.Text = objIdentificationOld.Expiry_Date.Value.ToString("dd-MMM-yy") + " => "
        '            ElseIf .Expiry_Date IsNot Nothing Then
        '                DspExpiryDate.Text = " => " + .Expiry_Date.Value.ToString("dd-MMM-yy")
        '            End If
        '            DspIssuedBy.Text = objIdentificationOld.Issued_By + " => " + .Issued_By
        '            DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(objIdentificationOld.Issued_Country) + " => " + GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
        '            DspIdentificationComment.Text = objIdentificationOld.Identification_Comment + " => " + .Identification_Comment
        '        End With
        '    End If

        'Else
        '    Dim objIdentification As goAML_Person_Identification = ListObjWICIdentification.Where(Function(x) x.PK_Person_Identification_ID = iD And x.FK_Person_Type = 8).FirstOrDefault

        '    With objIdentification
        '        DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
        '        DspNumber.Text = .Number
        '        If .Issue_Date IsNot Nothing Then
        '            DspIssueDate.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
        '        End If
        '        If .Expiry_Date IsNot Nothing Then
        '            DspExpiryDate.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
        '        End If
        '        DspIssuedBy.Text = .Issued_By
        '        DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
        '        DspIdentificationComment.Text = .Identification_Comment
        '    End With
        'End If
    End Sub

    Private Sub DetailIdentidicationBefore(iD As String)
        WindowDetailIdentification.Show()
        PanelDetailIdentification.Show()
        ClearDetailIdentification()
        Dim objIdentification As goAML_Person_Identification = ListObjWICIdentificationBefore.Where(Function(x) x.PK_Person_Identification_ID = iD).FirstOrDefault
        If objIdentification IsNot Nothing Then
            With objIdentification
                If .Type IsNot Nothing Then
                    DsptType.Text = GlobalReportFunctionBLL.getjenisDokumenByKode(.Type)
                End If
                DspNumber.Text = .Number
                If .Issue_Date IsNot Nothing Then
                    DspIssueDate.Text = .Issue_Date.Value.ToString("dd-MMM-yy")
                End If
                If .Expiry_Date IsNot Nothing Then
                    DspExpiryDate.Text = .Expiry_Date.Value.ToString("dd-MMM-yy")
                End If
                DspIssuedBy.Text = .Issued_By
                DspIssuedCountry.Text = GlobalReportFunctionBLL.getCountryByCode(.Issued_Country)
                DspIdentificationComment.Text = .Identification_Comment
            End With
        End If
    End Sub


#End Region


#Region "Direct Events"
    Protected Sub btnBackSaveCustomerDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailDirector.Hide()
        'daniel 19 jan 2021
        'FormPanelDirectorDetail.Hide()
        'end 19 jan 2021
    End Sub
    Protected Sub BtnCancelIdentificationDirector_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailDirectorIdentification.Hide()
        PanelDetailIdentificationDirector.Hide()
        FormPanelDirectorDetail.Show()
    End Sub
    Protected Sub BtnCancelIdentification_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailIdentification.Hide()
        PanelDetailIdentification.Hide()
    End Sub
    Protected Sub BtnCancelDirectorPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailDirectorPhone.Hide()
        PanelPhoneDirector.Hide()
        FormPanelDirectorDetail.Show()
    End Sub
    Protected Sub BtnCancelDirectorEmployerPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailDirectorPhone.Hide()
        PanelPhoneDirectorEmployer.Hide()
        FormPanelDirectorDetail.Show()
    End Sub
    Protected Sub BtnCancelDirectorDetailAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailDirectorAddress.Hide()
        PanelAddressDirector.Hide()
        FormPanelDirectorDetail.Show()
    End Sub
    Protected Sub BtnCancelDirectorDetailEmployerAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailDirectorAddress.Hide()
        PanelAddressDirectorEmployer.Hide()
        FormPanelDirectorDetail.Show()
    End Sub
    Protected Sub BtnCancelPhone_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailPhone.Hide()
        PanelDetailPhone.Hide()
    End Sub
    Protected Sub BtnCancelPhoneEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailPhone.Hide()
        PanelDetailPhoneEmployer.Hide()
    End Sub
    Protected Sub BtnCancelAddress_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailAddress.Hide()
        PanelDetailAddress.Hide()
    End Sub
    Protected Sub BtnCancelAddressEmployer_DirectEvent(sender As Object, e As DirectEventArgs)
        WindowDetailAddress.Hide()
        PanelDetailAddressEmployer.Hide()
    End Sub
    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim objWIC As New WICBLL
            objWIC.Accept(ObjApproval.PK_ModuleApproval_ID)

            LblConfirmation.Text = "Data Approved. Click Ok to Back To " & ObjModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnReject_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim objWIC As New WICBLL
            objWIC.Reject(ObjApproval.PK_ModuleApproval_ID)

            LblConfirmation.Text = "Data Rejected. Click Ok to Back To " & ObjModule.ModuleLabel & " Approval."
            container.Hidden = True
            Panelconfirmation.Hidden = False
            container.Render()
            Panelconfirmation.Render()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim strURL As String
            strURL = String.Format(Common.GetApplicationPath & "/Parameter/WaitingApproval.aspx?ModuleID={0}", Request.Params("ModuleID"))

            If Not ObjApproval Is Nothing Then
                If ObjApproval.CreatedBy <> Common.SessionCurrentUser.UserID Then
                    strURL = String.Format(Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID"))
                End If
            End If

            Net.X.Redirect(strURL)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs) Handles BtnConfirmation.DirectClick
        Try
            Net.X.Redirect(String.Format(Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID={0}", Request.Params("ModuleID")))
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
#End Region
End Class
