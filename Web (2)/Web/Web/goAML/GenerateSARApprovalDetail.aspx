﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="GenerateSARApprovalDetail.aspx.vb" Inherits="goAML_GenerateSARApprovalDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .my-style .x-form-display-field-default {
            color: #FF0000;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:Window ID="WindowAttachment" Layout="AnchorLayout" Title="Dokumen" runat="server" Modal="true" Hidden="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center">
        <Items>
            <ext:FileUploadField ID="FileDoc" runat="server" FieldLabel="File Document" AnchorHorizontal="100%">
            </ext:FileUploadField>
            <ext:DisplayField ID="txtFileName" runat="server" FieldLabel="File Name" AnchorHorizontal="100%">
            </ext:DisplayField>
            <ext:TextArea ID="txtKeterangan" runat="server" FieldLabel="Description" AnchorHorizontal="100%">
            </ext:TextArea>
        </Items>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.4, height: size.height * 0.4});" />

            <Resize Handler="#{WindowAttachment}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="BtnsaveAttachment" runat="server" Icon="Disk" Text="Save" ValidationGroup="MainForm">

                <DirectEvents>
                    <Click OnEvent="btnSaveAttachment_Click">
                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancelAttachment" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="BtnCancelAttachment_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:Container ID="container" runat="server" Layout="VBoxLayout">
        <LayoutConfig>
            <ext:VBoxLayoutConfig Align="Stretch"></ext:VBoxLayoutConfig>
        </LayoutConfig>
        <Items>
            <ext:FormPanel ID="PanelInfo" runat="server" Title="Module Approval" BodyPadding="10">
                <Items>
                    <ext:DisplayField ID="lblModuleName" runat="server" FieldLabel="Module Name">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblModuleKey" runat="server" FieldLabel="Module Key">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblAction" runat="server" FieldLabel="Action">
                    </ext:DisplayField>
                    <ext:DisplayField ID="LblCreatedBy" runat="server" FieldLabel="Created By">
                    </ext:DisplayField>
                    <ext:DisplayField ID="lblCreatedDate" runat="server" FieldLabel="Created Date">
                    </ext:DisplayField>
                </Items>
            </ext:FormPanel>

            <%--<ext:Panel runat="server" ID="ReportGeneralInformationPanel" Flex="4" ButtonAlign="Center" Layout="AnchorLayout" ClientIDMode="Static" Title="Generate STR SAR Information" BodyStyle="padding:10px" Margin="5" Height="600" Border="True" AutoScroll="true" Collapsible="true">--%>

            <ext:FormPanel ID="FormPanel2" runat="server" Flex="1" BodyPadding="10" MarginSpec="0 5 0 0" Hidden="false" AutoScroll="true">
                <Items>
                    <ext:Panel ID="Panel1" runat="server" Layout="HBoxLayout" ButtonAlign="Center" Flex="1" AutoScroll="false">
                        <Items>
                            <ext:FormPanel ID="FormPanelOld" runat="server" Title="Old Value" Flex="1" BodyPadding="10" MarginSpec="0 5 0 0" Hidden="true">
                                <Items>
                                    <ext:DisplayField ID="sar_PkOld" runat="server" FieldLabel="ID" AnchorHorizontal="100%" />
                                    <ext:DisplayField ID="sar_cifOld" runat="server" FieldLabel="CIF No" AnchorHorizontal="70%" />
                                    <ext:DisplayField ID="sar_laporanOld" runat="server" FieldLabel="Jenis Laporan" AnchorHorizontal="70%" />
                                    <ext:DisplayField ID="sar_TanggalLaporanOld" runat="server" FieldLabel="Tanggal Laporan" AnchorHorizontal="70%" />
                                    <ext:DisplayField ID="NoRefPPATKOld" runat="server" FieldLabel="No Ref PPATK" AnchorHorizontal="70%" />
                                    <ext:DisplayField ID="AlasanOld" runat="server" FieldLabel="Alasan" AnchorHorizontal="70%" />
                                    <ext:Panel runat="server" ID="Panel3" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaction Data" BodyStyle="padding:10px" Margin="10" Collapsible="true" Hidden="false">
                                        <Items>
                                            <ext:GridPanel ID="GridPanel1" runat="server" AutoScroll="true" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <Store>
                                                    <%--<ext:Store ID="StoreTransactionOld" runat="server" IsPagingStore="true" PageSize="10">--%>
                                                    <ext:Store ID="StoreTransactionOld" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadDataTransOLD" RemotePaging="true" ClientIDMode="Static">
                                                          <Sorters>
                                                            </Sorters>
                                                            <Proxy>
                                                                <ext:PageProxy></ext:PageProxy>
                                                            </Proxy>
                                                        <Model>
                                                            <ext:Model ID="Model1" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="NO_ID" Type="Auto"></ext:ModelField>
                                                                    <ext:ModelField Name="NoTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="NoRefTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="TipeTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="LokasiTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="KeteranganTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="DateTransaction" Type="Date"></ext:ModelField>
                                                                    <ext:ModelField Name="AccountNo" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="NamaTeller" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="NamaPejabat" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="TglPembukuan" Type="Date"></ext:ModelField>
                                                                    <ext:ModelField Name="CaraTranDilakukan" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="CaraTranLain" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="DebitCredit" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="OriginalAmount" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="IDR" Type="String"></ext:ModelField>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column6" runat="server" DataIndex="NoTran" Text="Nomor Transaksi" Width="150"></ext:Column>
                                                        <ext:Column ID="Column7" runat="server" DataIndex="NoRefTran" Text="No Ref Transaksi" Width="150"></ext:Column>
                                                        <ext:Column ID="colTipeTran" runat="server" DataIndex="TipeTran" Text="Data Source" Width="150"></ext:Column>
                                                        <ext:Column runat="server" DataIndex="LokasiTran" Text="Lokasi Transaksi" Width="150"></ext:Column>
                                                        <ext:Column ID="Column8" runat="server" DataIndex="KeteranganTran" Text="Keterangan Transaksi" Width="170"></ext:Column>
                                                        <ext:DateColumn ID="DateColumn1" runat="server" DataIndex="DateTransaction" Text="Tanggal Transaksi" Width="150">
                                                            <Filter>
                                                                <ext:DateFilter />
                                                            </Filter>
                                                        </ext:DateColumn>
                                                        <ext:Column ID="colAccountNo" runat="server" DataIndex="AccountNo" Text="No Akun" Width="150"></ext:Column>
                                                        <ext:Column runat="server" DataIndex="NamaTeller" Text="Nama Teller" Width="120"></ext:Column>
                                                        <ext:Column ID="Column9" runat="server" DataIndex="NamaPejabat" Text="Nama Pejabat" Width="120"></ext:Column>
                                                        <ext:Column ID="Column10" runat="server" DataIndex="TglPembukuan" Text="Tanggal Pembukuan" Width="160"></ext:Column>
                                                        <ext:Column runat="server" DataIndex="CaraTranDilakukan" Text="Cara Transaksi Dilakukan" Width="185"></ext:Column>
                                                        <ext:Column ID="Column11" runat="server" DataIndex="CaraTranLain" Text="Cara Transaksi Lain" Width="150"></ext:Column>
                                                        <ext:Column ID="Column12" runat="server" DataIndex="DebitCredit" Text="Debit Credit" Width="150"></ext:Column>
                                                        <ext:Column ID="Column30" runat="server" DataIndex="OriginalAmount" Text="Original Amount" Width="150"></ext:Column>
                                                        <ext:Column ID="Column31" runat="server" DataIndex="IDR" Text="Nilai Transaksi(IDR)" Width="150"></ext:Column>
                                                    </Columns>
                                                </ColumnModel>
                                                <Plugins>
                                                    <ext:FilterHeader runat="server">
                                                    </ext:FilterHeader>
                                                </Plugins>
                                                <BottomBar>
                                                    <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                                                </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel runat="server" ID="Panel4" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Indikator" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanel2" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <Store>
                                                    <ext:Store ID="StoreIndicatorOld" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" ID="Model2" IDProperty="PK_Report_Indicator">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_Report_Indicator" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="FK_Indicator" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column13" runat="server" DataIndex="FK_Indicator" Text="Kode" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column14" runat="server" DataIndex="Keterangan" Text="Keterangan" Flex="2"></ext:Column>
                                                    </Columns>
                                                </ColumnModel>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel runat="server" ID="PanelAttachmentOld" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Attachment Document" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelAttachmentOld" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <Store>
                                                    <ext:Store ID="StoreAttachmentOld" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" ID="Model5" IDProperty="PK_ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_ID" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="FileName" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column24" runat="server" DataIndex="FileName" Text="File Name" Flex="1">
                                                            <Commands>
                                                                <ext:ImageCommand Icon="DiskDownload" CommandName="Download" ToolTip-Text="Download Dokumen"></ext:ImageCommand>
                                                            </Commands>
                                                            <DirectEvents>
                                                                <Command OnEvent="GridcommandAttachment">
                                                                    <ExtraParams>
                                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_ID" Mode="Raw"></ext:Parameter>
                                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                    </ExtraParams>
                                                                </Command>
                                                            </DirectEvents>
                                                        </ext:Column>
                                                        <ext:Column ID="Column25" runat="server" DataIndex="Keterangan" Text="Keterangan" Flex="1"></ext:Column>
                                                        <ext:CommandColumn ID="CommandColumnAttachmentOld" runat="server" Text="Action" Flex="1" Hidden="true">

                                                            <Commands>
                                                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                                                    <ToolTip Text="Detail"></ToolTip>
                                                                </ext:GridCommand>
                                                            </Commands>

                                                            <DirectEvents>

                                                                <Command OnEvent="GridcommandAttachmentOld">
                                                                    <EventMask ShowMask="true"></EventMask>
                                                                    <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>

                                                                    <ExtraParams>
                                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_ID" Mode="Raw"></ext:Parameter>
                                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                    </ExtraParams>
                                                                </Command>
                                                            </DirectEvents>
                                                        </ext:CommandColumn>
                                                    </Columns>
                                                </ColumnModel>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:FormPanel>

                            <ext:FormPanel ID="FormPanelNew" runat="server" Title="New Value" Flex="1" BodyPadding="10">
                                <Items>
                                    <ext:DisplayField ID="sar_PK" runat="server" FieldLabel="ID" AnchorHorizontal="100%" />
                                    <ext:DisplayField ID="sar_cif" runat="server" FieldLabel="CIF No" AnchorHorizontal="70%" />
                                    <ext:DisplayField ID="sar_laporan" runat="server" FieldLabel="Jenis Laporan" AnchorHorizontal="70%" />
                                    <ext:DisplayField ID="sar_TanggalLaporan" runat="server" FieldLabel="Tanggal Laporan" AnchorHorizontal="70%" />
                                    <ext:DisplayField ID="NoRefPPATK" runat="server" FieldLabel="No Ref PPATK" AnchorHorizontal="70%" />
                                    <ext:DisplayField ID="Alasan" runat="server" FieldLabel="Alasan" AnchorHorizontal="70%" />
                                    <ext:Panel runat="server" ID="Panel2" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaction Data" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPaneldetail" runat="server" AutoScroll="true" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <Store>
                                                    <ext:Store ID="StoreTransaction" runat="server" IsPagingStore="true" PageSize="10">
                                                        <Model>
                                                            <ext:Model ID="ModelDetail" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="NO_ID" Type="Auto"></ext:ModelField>
                                                                    <ext:ModelField Name="NoTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="NoRefTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="TipeTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="LokasiTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="KeteranganTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="DateTransaction" Type="Date"></ext:ModelField>
                                                                    <ext:ModelField Name="AccountNo" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="NamaTeller" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="NamaPejabat" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="TglPembukuan" Type="Date"></ext:ModelField>
                                                                    <ext:ModelField Name="CaraTranDilakukan" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="CaraTranLain" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="DebitCredit" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="OriginalAmount" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="IDR" Type="String"></ext:ModelField>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn28" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="colStoreProcedure" runat="server" DataIndex="NoTran" Text="Nomor Transaksi" Width="150"></ext:Column>
                                                        <ext:Column ID="colIsuseProcessDate" runat="server" DataIndex="NoRefTran" Text="No Ref Transaksi" Width="150"></ext:Column>
                                                        <ext:Column ID="Column26" runat="server" DataIndex="TipeTran" Text="Data Source" Width="150"></ext:Column>
                                                        <ext:Column runat="server" DataIndex="LokasiTran" Text="Lokasi Transaksi" Width="150"></ext:Column>
                                                        <ext:Column ID="Colorder" runat="server" DataIndex="KeteranganTran" Text="Keterangan Transaksi" Width="170"></ext:Column>
                                                        <ext:DateColumn ID="Column2" runat="server" DataIndex="DateTransaction" Text="Tanggal Transaksi" Width="150">
                                                            <Filter>
                                                                <ext:DateFilter />
                                                            </Filter>
                                                        </ext:DateColumn>
                                                        <ext:Column ID="Column27" runat="server" DataIndex="AccountNo" Text="No Akun" Width="150"></ext:Column>
                                                        <ext:Column runat="server" DataIndex="NamaTeller" Text="Nama Teller" Width="120"></ext:Column>
                                                        <ext:Column ID="Column1" runat="server" DataIndex="NamaPejabat" Text="Nama Pejabat" Width="120"></ext:Column>
                                                        <ext:Column ID="Column3" runat="server" DataIndex="TglPembukuan" Text="Tanggal Pembukuan" Width="160"></ext:Column>
                                                        <ext:Column runat="server" DataIndex="CaraTranDilakukan" Text="Cara Transaksi Dilakukan" Width="185"></ext:Column>
                                                        <ext:Column ID="Column4" runat="server" DataIndex="CaraTranLain" Text="Cara Transaksi Lain" Width="150"></ext:Column>
                                                        <ext:Column ID="Column5" runat="server" DataIndex="DebitCredit" Text="Debit Credit" Width="150"></ext:Column>
                                                        <ext:Column ID="Column32" runat="server" DataIndex="OriginalAmount" Text="Original Amount" Width="150"></ext:Column>
                                                        <ext:Column ID="Column33" runat="server" DataIndex="IDR" Text="Nilai Transaksi(IDR)" Width="150"></ext:Column>
                                                    </Columns>
                                                </ColumnModel>
                                                <Plugins>
                                                    <ext:FilterHeader runat="server">
                                                    </ext:FilterHeader>
                                                </Plugins>
                                                <BottomBar>
                                                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                                                </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>

                                    
                                    <ext:Panel runat="server" ID="Panel7" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaction Data" BodyStyle="padding:10px" Margin="10" Collapsible="true" Hidden="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanel4" runat="server" AutoScroll="true" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <Store>
                                                    <ext:Store ID="StoreNEW" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadDataTransNEW" RemotePaging="true" ClientIDMode="Static">
                                                    <Sorters>
                                                    </Sorters>
                                                    <Proxy>
                                                        <ext:PageProxy></ext:PageProxy>
                                                    </Proxy>
                                                        <Model>
                                                            <ext:Model ID="Model6" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="NO_ID" Type="Auto"></ext:ModelField>
                                                                    <ext:ModelField Name="NoTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="NoRefTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="TipeTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="LokasiTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="KeteranganTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="DateTransaction" Type="Date"></ext:ModelField>
                                                                    <ext:ModelField Name="AccountNo" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="NamaTeller" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="NamaPejabat" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="TglPembukuan" Type="Date"></ext:ModelField>
                                                                    <ext:ModelField Name="CaraTranDilakukan" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="CaraTranLain" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="DebitCredit" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="OriginalAmount" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="IDR" Type="String"></ext:ModelField>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column36" runat="server" DataIndex="NoTran" Text="Nomor Transaksi" Width="150"></ext:Column>
                                                        <ext:Column ID="Column37" runat="server" DataIndex="NoRefTran" Text="No Ref Transaksi" Width="150"></ext:Column>
                                                        <ext:Column ID="Column38" runat="server" DataIndex="TipeTran" Text="Tipe Transaksi" Width="150"></ext:Column>
                                                        <ext:Column runat="server" DataIndex="LokasiTran" Text="Lokasi Transaksi" Width="150"></ext:Column>
                                                        <ext:Column ID="Column39" runat="server" DataIndex="KeteranganTran" Text="Keterangan Transaksi" Width="170"></ext:Column>
                                                        <ext:DateColumn ID="DateColumn3" runat="server" DataIndex="DateTransaction" Text="Tanggal Transaksi" Width="150">
                                                            <Filter>
                                                                <ext:DateFilter />
                                                            </Filter>
                                                        </ext:DateColumn>
                                                        <ext:Column ID="Column40" runat="server" DataIndex="AccountNo" Text="No Akun" Width="150"></ext:Column>
                                                        <ext:Column runat="server" DataIndex="NamaTeller" Text="Nama Teller" Width="120"></ext:Column>
                                                        <ext:Column ID="Column41" runat="server" DataIndex="NamaPejabat" Text="Nama Pejabat" Width="120"></ext:Column>
                                                        <ext:Column ID="Column42" runat="server" DataIndex="TglPembukuan" Text="Tanggal Pembukuan" Width="160"></ext:Column>
                                                        <ext:Column runat="server" DataIndex="CaraTranDilakukan" Text="Cara Transaksi Dilakukan" Width="185"></ext:Column>
                                                        <ext:Column ID="Column43" runat="server" DataIndex="CaraTranLain" Text="Cara Transaksi Lain" Width="150"></ext:Column>
                                                        <ext:Column ID="Column44" runat="server" DataIndex="DebitCredit" Text="Debit Credit" Width="150"></ext:Column>
                                                        <ext:Column ID="Column45" runat="server" DataIndex="OriginalAmount" Text="Original Amount" Width="150"></ext:Column>
                                                        <ext:Column ID="Column46" runat="server" DataIndex="IDR" Text="Nilai Transaksi(IDR)" Width="150"></ext:Column>
                                                    </Columns>
                                                </ColumnModel>
                                                <Plugins>
                                                    <ext:FilterHeader runat="server">
                                                    </ext:FilterHeader>
                                                </Plugins>
                                                <BottomBar>
                                                    <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                                                </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>


                                    <ext:Panel runat="server" ID="Panel5" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Transaction Data" BodyStyle="padding:10px" Margin="10" Collapsible="true" Hidden="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanel3" runat="server" AutoScroll="true" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <Store>
                                                    <%--<ext:Store ID="StoreTransactionOdm" runat="server" IsPagingStore="true" PageSize="10">--%>
                                                     <ext:Store ID="StoreTransactionOdm"   runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadDataTrans" RemotePaging="true" ClientIDMode="Static">
                                                    <Sorters>
                                                    </Sorters>
                                                    <Proxy>
                                                        <ext:PageProxy></ext:PageProxy>
                                                    </Proxy>
                                                        <Model>
                                                            <ext:Model ID="Model3" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="NO_ID" Type="Auto"></ext:ModelField>
                                                                    <ext:ModelField Name="NoTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="NoRefTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="TipeTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="LokasiTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="KeteranganTran" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="DateTransaction" Type="Date"></ext:ModelField>
                                                                    <ext:ModelField Name="AccountNo" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="NamaTeller" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="NamaPejabat" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="TglPembukuan" Type="Date"></ext:ModelField>
                                                                    <ext:ModelField Name="CaraTranDilakukan" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="CaraTranLain" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="DebitCredit" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="OriginalAmount" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="IDR" Type="String"></ext:ModelField>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column15" runat="server" DataIndex="NoTran" Text="Nomor Transaksi" Width="150"></ext:Column>
                                                        <ext:Column ID="Column16" runat="server" DataIndex="NoRefTran" Text="No Ref Transaksi" Width="150"></ext:Column>
                                                        <ext:Column ID="Column28" runat="server" DataIndex="TipeTran" Text="Tipe Transaksi" Width="150"></ext:Column>
                                                        <ext:Column runat="server" DataIndex="LokasiTran" Text="Lokasi Transaksi" Width="150"></ext:Column>
                                                        <ext:Column ID="Column17" runat="server" DataIndex="KeteranganTran" Text="Keterangan Transaksi" Width="170"></ext:Column>
                                                        <ext:DateColumn ID="DateColumn2" runat="server" DataIndex="DateTransaction" Text="Tanggal Transaksi" Width="150">
                                                            <Filter>
                                                                <ext:DateFilter />
                                                            </Filter>
                                                        </ext:DateColumn>
                                                        <ext:Column ID="Column29" runat="server" DataIndex="AccountNo" Text="No Akun" Width="150"></ext:Column>
                                                        <ext:Column runat="server" DataIndex="NamaTeller" Text="Nama Teller" Width="120"></ext:Column>
                                                        <ext:Column ID="Column18" runat="server" DataIndex="NamaPejabat" Text="Nama Pejabat" Width="120"></ext:Column>
                                                        <ext:Column ID="Column19" runat="server" DataIndex="TglPembukuan" Text="Tanggal Pembukuan" Width="160"></ext:Column>
                                                        <ext:Column runat="server" DataIndex="CaraTranDilakukan" Text="Cara Transaksi Dilakukan" Width="185"></ext:Column>
                                                        <ext:Column ID="Column20" runat="server" DataIndex="CaraTranLain" Text="Cara Transaksi Lain" Width="150"></ext:Column>
                                                        <ext:Column ID="Column21" runat="server" DataIndex="DebitCredit" Text="Debit Credit" Width="150"></ext:Column>
                                                        <ext:Column ID="Column34" runat="server" DataIndex="OriginalAmount" Text="Original Amount" Width="150"></ext:Column>
                                                        <ext:Column ID="Column35" runat="server" DataIndex="IDR" Text="Nilai Transaksi(IDR)" Width="150"></ext:Column>
                                                    </Columns>
                                                </ColumnModel>
                                                <Plugins>
                                                    <ext:FilterHeader runat="server">
                                                    </ext:FilterHeader>
                                                </Plugins>
                                                <BottomBar>
                                                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                                                </BottomBar>
                                            </ext:GridPanel>

                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel runat="server" ID="PanelReportIndicator" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Indikator" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelReportIndicator" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <Store>
                                                    <ext:Store ID="StoreReportIndicatorData" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" ID="Model355" IDProperty="PK_Report_Indicator">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_Report_Indicator" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="FK_Indicator" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn88" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column312" runat="server" DataIndex="FK_Indicator" Text="Kode" Flex="1"></ext:Column>
                                                        <ext:Column ID="Column313" runat="server" DataIndex="Keterangan" Text="Keterangan" Flex="2"></ext:Column>
                                                    </Columns>
                                                </ColumnModel>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel runat="server" ID="PanelAttachment" Layout="AnchorLayout" ClientIDMode="Static" Border="TRUE" Title="Attachment Document" BodyStyle="padding:10px" Margin="10" Collapsible="true">
                                        <Items>
                                            <ext:GridPanel ID="GridPanelAttachment" runat="server" EmptyText="No Available Data">
                                                <View>
                                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                                </View>
                                                <Store>
                                                    <ext:Store ID="StoreAttachment" runat="server">
                                                        <Model>
                                                            <ext:Model runat="server" ID="Model4" IDProperty="PK_ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="PK_ID" Type="Int"></ext:ModelField>
                                                                    <ext:ModelField Name="FileName" Type="String"></ext:ModelField>
                                                                    <ext:ModelField Name="Keterangan" Type="String"></ext:ModelField>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No"></ext:RowNumbererColumn>
                                                        <ext:Column ID="Column22" runat="server" DataIndex="FileName" Text="File Name" Flex="1">
                                                            <Commands>
                                                                <ext:ImageCommand Icon="DiskDownload" CommandName="Download" ToolTip-Text="Download Dokumen"></ext:ImageCommand>
                                                            </Commands>
                                                            <DirectEvents>
                                                                <Command OnEvent="GridcommandAttachment">
                                                                    <ExtraParams>
                                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_ID" Mode="Raw"></ext:Parameter>
                                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                    </ExtraParams>
                                                                </Command>
                                                            </DirectEvents>
                                                        </ext:Column>
                                                        <ext:Column ID="Column23" runat="server" DataIndex="Keterangan" Text="Keterangan" Flex="1"></ext:Column>
                                                        <ext:CommandColumn ID="CommandColumnAttachment" runat="server" Text="Action" Flex="1" Hidden="true">

                                                            <Commands>
                                                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                                                    <ToolTip Text="Detail"></ToolTip>
                                                                </ext:GridCommand>
                                                            </Commands>

                                                            <DirectEvents>

                                                                <Command OnEvent="GridcommandAttachment">
                                                                    <EventMask ShowMask="true"></EventMask>
                                                                    <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>

                                                                    <ExtraParams>
                                                                        <ext:Parameter Name="unikkey" Value="record.data.PK_ID" Mode="Raw"></ext:Parameter>
                                                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                                    </ExtraParams>
                                                                </Command>
                                                            </DirectEvents>
                                                        </ext:CommandColumn>
                                                    </Columns>
                                                </ColumnModel>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:FormPanel>

                        </Items>


                    </ext:Panel>
                    <ext:Panel ID="Panel6" runat="server" Layout="HBoxLayout" ButtonAlign="Center" Flex="1" AutoScroll="false">
                        <Items>
                            <ext:FormPanel ID="FormPanel1" runat="server" Flex="1" BodyPadding="10" MarginSpec="0 5 0 0" Hidden="false">
                                <Items>
                                    <ext:TextArea ID="TextAreaReasonApproval" runat="server" FieldLabel="Approval/Rejection Note" AnchorHorizontal="100%" />
                                </Items>
                            </ext:FormPanel>

                        </Items>
                        <Buttons>
                            <ext:Button ID="BtnSave" runat="server" Text="Accept" Icon="DiskBlack">
                                <DirectEvents>
                                    <Click OnEvent="BtnSave_Click">
                                        <EventMask ShowMask="true" Msg="Saving Data..." MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="BtnReject" runat="server" Text="Reject" Icon="Decline">
                                <DirectEvents>
                                    <Click OnEvent="BtnReject_Click">
                                        <EventMask ShowMask="true" Msg="Saving Reject Data..." MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="BtnCancel" runat="server" Text="Cancel" Icon="PageBack">
                                <DirectEvents>
                                    <Click OnEvent="BtnCancel_Click">
                                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="500"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>

                    </ext:Panel>
                </Items>
            </ext:FormPanel>

        </Items>
    </ext:Container>

    <ext:FormPanel ID="Panelconfirmation" runat="server" ClientIDMode="Static" Title="Confirmation" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel" />
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>
