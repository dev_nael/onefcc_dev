﻿Imports Ext
Imports System.Data.SqlClient
Imports NawaDAL
Imports NawaDevBLL
Imports NawaDevDAL
Imports NawaBLL
Imports Elmah
Imports System.Data
Partial Class goAML_ReportAddressEdit
    Inherits Parent
#Region "Session"
    Public objgoAML_ReportAddress As ReportAddress
    'Dim fk_ref, fk_to As Integer
    Dim con As New SqlConnection(SQLHelper.strConnectionString)
    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("goAML_ReportAddressEdit.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("goAML_ReportAddressEdit.ObjModule") = value
        End Set
    End Property
    Public Property ObjReportAddress As goAML_Ref_Address
        Get
            Return Session("goAML_ReportAddressEdit.ObjWIC")
        End Get
        Set(ByVal value As goAML_Ref_Address)
            Session("goAML_ReportAddressEdit.ObjWIC") = value
        End Set
    End Property
    Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
        objgoAML_ReportAddress = New ReportAddress(PanelAddressReport)
    End Sub
#End Region
#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Net.X.IsAjaxRequest Then
                ClearSession()

                Dim strmodule As String = Request.Params("ModuleID")
                Dim intmodule As Integer = Common.DecryptQueryString(strmodule, SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = ModuleBLL.GetModuleByModuleID(intmodule)

                If Not ObjModule Is Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, Common.ModuleActionEnum.Update) Then
                        Dim strIDCode As String = 1
                        strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                        Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                Else
                    Throw New Exception("Invalid Module ID")
                End If

                PanelAddressReport.Title = ObjModule.ModuleLabel & " Edit"
                Panelconfirmation.Title = ObjModule.ModuleLabel & " Edit"

                Dim dataStr As String = Request.Params("ID")
                Dim dataID As String = Common.DecryptQueryString(dataStr, SystemParameterBLL.GetEncriptionKey)
                ObjReportAddress = objgoAML_ReportAddress.GetReportAddressByID(dataID)

                LoadData()
                LoadNegara()
                LoadKategoriKontak()
            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub LoadKategoriKontak()
        Dim strQueryKategoriKontak As String = "SELECT Kode code, Keterangan display FROM goAML_Ref_Kategori_Kontak  WHERE Active = 1"
        Dim KategoriKontakStore As DataTable = SQLHelper.ExecuteTable(con, CommandType.Text, strQueryKategoriKontak)
        SbAddress_type.GetStore().DataSource = KategoriKontakStore
        SbAddress_type.GetStore().DataBind()
    End Sub

    Private Sub LoadNegara()
        Dim strQueryCountry As String = "SELECT Kode code ,Keterangan display FROM goAML_Ref_Nama_Negara WHERE Active = 1"
        Dim CountryStore As DataTable = SQLHelper.ExecuteTable(con, CommandType.Text, strQueryCountry)
        Sbcountry_code.GetStore().DataSource = CountryStore
        Sbcountry_code.GetStore.DataBind()
    End Sub

    Private Sub LoadData()
        If Not ObjReportAddress Is Nothing Then
            With ObjReportAddress
                PKReportAddress.Text = .PK_Customer_Address_ID
                'fk_ref = .FK_Ref_Detail_Of
                'fk_to = .FK_To_Table_ID
                SbAddress_type.Text = .Address_Type
                TxtAddress.Text = .Address
                TxtTown.Text = .Town
                TxtCity.Text = .City
                TxtZip.Text = .Zip
                Sbcountry_code.Text = .Country_Code
                TxtState.Text = .State
                Txtcomments.Text = .Comments
            End With
        End If
    End Sub

    Private Sub ClearSession()
        ObjReportAddress = Nothing
        ObjModule = Nothing
    End Sub
#End Region
    Protected Sub BtnSave_Click(sender As Object, e As DirectEventArgs)
        Dim ObjSave As New goAML_Ref_Address With
        {
            .PK_Customer_Address_ID = PKReportAddress.Text,
            .FK_Ref_Detail_Of = 9,
            .FK_To_Table_ID = 0,
            .Address_Type = SbAddress_type.Value,
            .Address = TxtAddress.Text,
            .Town = TxtTown.Text,
            .City = TxtCity.Text,
            .Zip = TxtZip.Text,
            .Country_Code = Sbcountry_code.Value,
            .State = TxtState.Text,
            .Comments = Txtcomments.Text
        }
        Dim Status As Boolean = True

        With ObjSave
            If .Address_Type <> ObjReportAddress.Address_Type Then
                Status = False
            End If
            If .Address <> ObjReportAddress.Address Then
                Status = False
            End If
            If .Town <> ObjReportAddress.Town Then
                Status = False
            End If
            If .City <> ObjReportAddress.City Then
                Status = False
            End If
            If .Zip <> ObjReportAddress.Zip Then
                Status = False
            End If
            If .Country_Code <> ObjReportAddress.Country_Code Then
                Status = False
            End If
            If .State <> ObjReportAddress.State Then
                Status = False
            End If
            If .Comments <> ObjReportAddress.Comments Then
                Status = False
            End If
        End With

        If Status Then
            'Throw New ApplicationException("There is no Changes Data. Please Edit First before Saved")
            Ext.Net.X.Msg.Alert("Error", "There is no Changes Data. Please Edit First before Saved").Show()
        Else
            If Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                objgoAML_ReportAddress.SaveEditTanpaApproval(ObjSave, ObjModule)
                PanelAddressReport.Hide()
                Panelconfirmation.Show()
                LblConfirmation.Text = "Data Saved into Database"
            Else
                objgoAML_ReportAddress.SaveEditApproval(ObjSave, ObjModule)
                PanelAddressReport.Hide()
                Panelconfirmation.Show()
                LblConfirmation.Text = "Data Saved into Pending Approval"
            End If
        End If

    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub BtnConfirmation_DirectClick()
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Net.X.Redirect(Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class
