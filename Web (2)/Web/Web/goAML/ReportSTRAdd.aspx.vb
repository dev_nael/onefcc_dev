﻿Imports System.Data
Imports NawaDevDAL
Imports NawaBLL
Imports NawaDevBLL
Imports System.IO
Imports OfficeOpenXml
Imports System.Drawing


Partial Class ReportSTRAdd
    Inherits Parent
    Public UploadReportSTR As New NawaDevBLL.UploadReportSTR
    Private this As Object

#Region "Session"
    Private Sub ClearSession()
        ListReportValid = New List(Of goAML_ReportSTR)
        ListIndikatorLaporanValid = New List(Of goAML_Indikator_LaporanSTR)
        ListTransactionBipartyValid = New List(Of goAML_Transaction_BiPartySTR)
        ListTransactionMultipartyValid = New List(Of goAML_Transaction_MultiPartySTR)
        ListActivityValid = New List(Of goAML_ActivitySTR)
    End Sub

    Private Sub ClearSession_Upload()
        ListReport_Upload = New List(Of NawaDevDAL.goAML_ReportSTR_Upload)
        ListIndikatorLaporan_Upload = New List(Of NawaDevDAL.goAML_Indikator_LaporanSTR_Upload)
        ListTransactionBiparty_Upload = New List(Of NawaDevDAL.goAML_Transaction_BiPartySTR_Upload)
        ListTransactionMultiparty_Upload = New List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR_Upload)
        ListActivity_Upload = New List(Of NawaDevDAL.goAML_ActivitySTR_Upload)

        ListReportNotValid = New List(Of NawaDevDAL.goAML_ReportSTR_Upload)
        ListIndikatorLaporanNotValid = New List(Of NawaDevDAL.goAML_Indikator_LaporanSTR_Upload)
        ListTransactionBipartyNotValid = New List(Of NawaDevDAL.goAML_Transaction_BiPartySTR_Upload)
        ListTransactionMultipartyNotValid = New List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR_Upload)
        ListActivityNotValid = New List(Of NawaDevDAL.goAML_ActivitySTR_Upload)
    End Sub

    Private Sub ListUploadFromDatabase()
        ListReport_Upload = UploadReportSTR.GetListReport_Upload().Where(Function(x) x.KeteranganError = "").ToList()
        ListIndikatorLaporan_Upload = UploadReportSTR.GetListIndikatorLaporan_Upload().Where(Function(x) x.KeteranganError = "").ToList()
        ListTransactionBiparty_Upload = UploadReportSTR.GetListTransactionBiParty_Upload().Where(Function(x) x.KeteranganError = "").ToList()
        ListTransactionMultiparty_Upload = UploadReportSTR.GetListTransactionMultiParty_Upload().Where(Function(x) x.KeteranganError = "").ToList()
        ListActivity_Upload = UploadReportSTR.GetListActivity_Upload().Where(Function(x) x.KeteranganError = "").ToList()

        ListReportNotValid = UploadReportSTR.GetListReport_Upload().Where(Function(x) x.KeteranganError <> "").ToList()
        ListIndikatorLaporanNotValid = UploadReportSTR.GetListIndikatorLaporan_Upload().Where(Function(x) x.KeteranganError <> "").ToList()
        ListTransactionBipartyNotValid = UploadReportSTR.GetListTransactionBiParty_Upload().Where(Function(x) x.KeteranganError <> "").ToList()
        ListTransactionMultipartyNotValid = UploadReportSTR.GetListTransactionMultiParty_Upload().Where(Function(x) x.KeteranganError <> "").ToList()
        ListActivityNotValid = UploadReportSTR.GetListActivity_Upload().Where(Function(x) x.KeteranganError <> "").ToList()
    End Sub



    Private Sub Bindgrid()
        'My.Computer.FileSystem.DeleteFile(PathFile)

        StoreReportData.DataSource = ListReport_Upload.ToList()
        StoreReportData.DataBind()
        Dim objtbl2 As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("goaml_indikator_laporanstr_upload a INNER JOIN dbo.goaml_reF_indikator_laporan b ON a.FK_Indikator_Laporan = b.Kode", "a.PK_ID,a.Case_ID,a.FK_Indikator_Laporan,b.Keterangan", "a.KeteranganError = '' and a.nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'", "", 0, NawaBLL.SystemParameterBLL.GetPageSize, 0)
        StoreIndikatorLaporanData.DataSource = objtbl2
        StoreIndikatorLaporanData.DataBind()
        StoreTransactionBiParty.DataSource = ListTransactionBiparty_Upload.ToList()
        StoreTransactionBiParty.DataBind()
        StoreTransactionMultiParty.DataSource = ListTransactionMultiparty_Upload.ToList()
        StoreTransactionMultiParty.DataBind()
        StoreActivityAccount.DataSource = ListActivity_Upload.Where(Function(x) x.SubNodeType = 1).ToList()
        StoreActivityAccount.DataBind()
        StoreActivityPerson.DataSource = ListActivity_Upload.Where(Function(x) x.SubNodeType = 2).ToList()
        StoreActivityPerson.DataBind()
        StoreActivityEntity.DataSource = ListActivity_Upload.Where(Function(x) x.SubNodeType = 3).ToList()
        StoreActivityEntity.DataBind()

        If ListReportNotValid.ToList().Count > 0 Then
            GridPanelReportInvalid.Hidden = False
            PanelReportInvalid.Hidden = False
            StoreReportInvalid.DataSource = ListReportNotValid.ToList()
            StoreReportInvalid.DataBind()
        End If
        If ListIndikatorLaporanNotValid.ToList().Count > 0 Then
            Dim objtbl3 As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("goaml_indikator_laporanstr_upload a", "a.KeteranganError,a.PK_ID,a.Case_ID,a.FK_Indikator_Laporan", "a.KeteranganError <>'' and a.nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'", "", 0, NawaBLL.SystemParameterBLL.GetPageSize, 0)
            GridPanelIndikatorLaporanInvalid.Hidden = False
            PanelIndikatorLaporanInvalid.Hidden = False
            StoreIndikatorLaporanInvalid.DataSource = objtbl3
            StoreIndikatorLaporanInvalid.DataBind()
        End If
        If ListTransactionBipartyNotValid.ToList().Count > 0 Then
            GridPanelTransactionBipartyInvalid.Hidden = False
            PanelTransactionBipartyInvalid.Hidden = False
            StoreTransactionBipartyInvalid.DataSource = ListTransactionBipartyNotValid.ToList()
            StoreTransactionBipartyInvalid.DataBind()
        End If
        If ListTransactionMultipartyNotValid.ToList().Count > 0 Then
            GridPanelTransactionMultiPartyInvalid.Hidden = False
            PanelTransactionMultiPartyInvalid.Hidden = False
            StoreTransactionMultiPartyInvalid.DataSource = ListTransactionMultipartyNotValid.ToList()
            StoreTransactionMultiPartyInvalid.DataBind()
        End If
        If ListActivityNotValid.Where(Function(x) x.SubNodeType = 1).ToList().Count > 0 Then
            GridPanelActivityAccountInvalid.Hidden = False
            PanelActivityAccountInvalid.Hidden = False
            StoreActivityAccountInvalid.DataSource = ListActivityNotValid.Where(Function(x) x.SubNodeType = 1).ToList()
            StoreActivityAccountInvalid.DataBind()
        End If
        If ListActivityNotValid.Where(Function(x) x.SubNodeType = 2).ToList().Count > 0 Then
            GridPanelActivityPersonInvalid.Hidden = False
            PanelActivityPersonInvalid.Hidden = False
            StoreActivityPersonInvalid.DataSource = ListActivityNotValid.Where(Function(x) x.SubNodeType = 2).ToList()
            StoreActivityPersonInvalid.DataBind()
        End If
        If ListActivityNotValid.Where(Function(x) x.SubNodeType = 3).ToList().Count > 0 Then
            GridPanelActivityEntityInvalid.Hidden = False
            PanelActivityEntityInvalid.Hidden = False
            StoreActivityEntityInvalid.DataSource = ListActivityNotValid.Where(Function(x) x.SubNodeType = 3).ToList()
            StoreActivityEntityInvalid.DataBind()
        End If

    End Sub

    Private Sub Bindgridnothing()

        StoreReportData.DataBind()
        StoreIndikatorLaporanData.DataBind()
        StoreTransactionBiParty.DataBind()
        StoreTransactionMultiParty.DataBind()
        StoreActivityAccount.DataBind()
        StoreActivityPerson.DataBind()
        StoreActivityEntity.DataBind()

        StoreReportInvalid.DataBind()
        StoreIndikatorLaporanInvalid.DataBind()
        StoreTransactionBipartyInvalid.DataBind()
        StoreTransactionMultiPartyInvalid.DataBind()
        StoreActivityAccountInvalid.DataBind()
        StoreActivityPersonInvalid.DataBind()
        StoreActivityEntityInvalid.DataBind()

    End Sub


    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("ReportSTRAdd.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("ReportSTRAdd.ObjModule") = value
        End Set
    End Property
    Public Property ListReport_Upload As List(Of NawaDevDAL.goAML_ReportSTR_Upload)
        Get
            If Session("ReportSTRAdd.ListReport_Upload") Is Nothing Then
                Session("ReportSTRAdd.ListReport_Upload") = New List(Of NawaDevDAL.goAML_ReportSTR_Upload)
            End If
            Return Session("ReportSTRAdd.ListReport_Upload")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ReportSTR_Upload))
            Session("ReportSTRAdd.ListReport_Upload") = value
        End Set
    End Property
    Public Property ListIndikatorLaporan_Upload As List(Of NawaDevDAL.goAML_Indikator_LaporanSTR_Upload)
        Get
            If Session("ReportSTRAdd.ListIndikatorLaporan_Upload") Is Nothing Then
                Session("ReportSTRAdd.ListIndikatorLaporan_Upload") = New List(Of NawaDevDAL.goAML_Indikator_LaporanSTR_Upload)
            End If
            Return Session("ReportSTRAdd.ListIndikatorLaporan_Upload")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Indikator_LaporanSTR_Upload))
            Session("ReportSTRAdd.ListIndikatorLaporan_Upload") = value
        End Set
    End Property
    Public Property ListTransactionBiparty_Upload As List(Of NawaDevDAL.goAML_Transaction_BiPartySTR_Upload)
        Get
            If Session("ReportSTRAdd.ListTransactionBiparty_Upload") Is Nothing Then
                Session("ReportSTRAdd.ListTransactionBiparty_Upload") = New List(Of NawaDevDAL.goAML_Transaction_BiPartySTR_Upload)
            End If
            Return Session("ReportSTRAdd.ListTransactionBiparty_Upload")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Transaction_BiPartySTR_Upload))
            Session("ReportSTRAdd.ListTransactionBiparty_Upload") = value
        End Set
    End Property
    Public Property ListTransactionMultiparty_Upload As List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR_Upload)
        Get
            If Session("ReportSTRAdd.ListTransactionMultiparty_Upload") Is Nothing Then
                Session("ReportSTRAdd.ListTransactionMultiparty_Upload") = New List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR_Upload)
            End If
            Return Session("ReportSTRAdd.ListTransactionMultiparty_Upload")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR_Upload))
            Session("ReportSTRAdd.ListTransactionMultiparty_Upload") = value
        End Set
    End Property
    Public Property ListActivity_Upload As List(Of NawaDevDAL.goAML_ActivitySTR_Upload)
        Get
            If Session("ReportSTRAdd.ListActivity_Upload") Is Nothing Then
                Session("ReportSTRAdd.ListActivity_Upload") = New List(Of NawaDevDAL.goAML_ActivitySTR_Upload)
            End If
            Return Session("ReportSTRAdd.ListActivity_Upload")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ActivitySTR_Upload))
            Session("ReportSTRAdd.ListActivity_Upload") = value
        End Set
    End Property

    Public Property ListReportValid As List(Of NawaDevDAL.goAML_ReportSTR)
        Get
            If Session("ReportSTRAdd.ListReportValid") Is Nothing Then
                Session("ReportSTRAdd.ListReportValid") = New List(Of NawaDevDAL.goAML_ReportSTR)
            End If
            Return Session("ReportSTRAdd.ListReportValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ReportSTR))
            Session("ReportSTRAdd.ListReportValid") = value
        End Set
    End Property

    Public Property ListReportNotValid As List(Of NawaDevDAL.goAML_ReportSTR_Upload)
        Get
            If Session("ReportSTRAdd.ListReportNotValid") Is Nothing Then
                Session("ReportSTRAdd.ListReportNotValid") = New List(Of NawaDevDAL.goAML_ReportSTR_Upload)
            End If
            Return Session("ReportSTRAdd.ListReportNotValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ReportSTR_Upload))
            Session("ReportSTRAdd.ListReportNotValid") = value
        End Set
    End Property

    Public Property ListIndikatorLaporanValid As List(Of NawaDevDAL.goAML_Indikator_LaporanSTR)
        Get
            If Session("ReportSTRAdd.ListIndikatorLaporanValid") Is Nothing Then
                Session("ReportSTRAdd.ListIndikatorLaporanValid") = New List(Of NawaDevDAL.goAML_Indikator_LaporanSTR)
            End If
            Return Session("ReportSTRAdd.ListIndikatorLaporanValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Indikator_LaporanSTR))
            Session("ReportSTRAdd.ListIndikatorLaporanValid") = value
        End Set
    End Property

    Public Property ListIndikatorLaporanNotValid As List(Of NawaDevDAL.goAML_Indikator_LaporanSTR_Upload)
        Get
            If Session("ReportSTRAdd.ListIndikatorLaporanNotValid") Is Nothing Then
                Session("ReportSTRAdd.ListIndikatorLaporanNotValid") = New List(Of NawaDevDAL.goAML_Indikator_LaporanSTR_Upload)
            End If
            Return Session("ReportSTRAdd.ListIndikatorLaporanNotValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Indikator_LaporanSTR_Upload))
            Session("ReportSTRAdd.ListIndikatorLaporanNotValid") = value
        End Set
    End Property

    Public Property ListTransactionBipartyValid As List(Of NawaDevDAL.goAML_Transaction_BiPartySTR)
        Get
            If Session("ReportSTRAdd.ListTransactionBipartyValid") Is Nothing Then
                Session("ReportSTRAdd.ListTransactionBipartyValid") = New List(Of NawaDevDAL.goAML_Transaction_BiPartySTR)
            End If
            Return Session("ReportSTRAdd.ListTransactionBipartyValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Transaction_BiPartySTR))
            Session("ReportSTRAdd.ListTransactionBipartyValid") = value
        End Set
    End Property

    Public Property ListTransactionBipartyNotValid As List(Of NawaDevDAL.goAML_Transaction_BiPartySTR_Upload)
        Get
            If Session("ReportSTRAdd.ListTransactionBipartyNotValid") Is Nothing Then
                Session("ReportSTRAdd.ListTransactionBipartyNotValid") = New List(Of NawaDevDAL.goAML_Transaction_BiPartySTR_Upload)
            End If
            Return Session("ReportSTRAdd.ListTransactionBipartyNotValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Transaction_BiPartySTR_Upload))
            Session("ReportSTRAdd.ListTransactionBipartyNotValid") = value
        End Set
    End Property

    Public Property ListTransactionMultipartyValid As List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR)
        Get
            If Session("ReportSTRAdd.ListTransactionMultipartyValid") Is Nothing Then
                Session("ReportSTRAdd.ListTransactionMultipartyValid") = New List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR)
            End If
            Return Session("ReportSTRAdd.ListTransactionMultipartyValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR))
            Session("ReportSTRAdd.ListTransactionMultipartyValid") = value
        End Set
    End Property

    Public Property ListTransactionMultipartyNotValid As List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR_Upload)
        Get
            If Session("ReportSTRAdd.ListTransactionMultipartyNotValid") Is Nothing Then
                Session("ReportSTRAdd.ListTransactionMultipartyNotValid") = New List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR_Upload)
            End If
            Return Session("ReportSTRAdd.ListTransactionMultipartyNotValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR_Upload))
            Session("ReportSTRAdd.ListTransactionMultipartyNotValid") = value
        End Set
    End Property

    Public Property ListActivityValid As List(Of NawaDevDAL.goAML_ActivitySTR)
        Get
            If Session("ReportSTRAdd.ListActivityValid") Is Nothing Then
                Session("ReportSTRAdd.ListActivityValid") = New List(Of NawaDevDAL.goAML_ActivitySTR)
            End If
            Return Session("ReportSTRAdd.ListActivityValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ActivitySTR))
            Session("ReportSTRAdd.ListActivityValid") = value
        End Set
    End Property

    Public Property ListActivityNotValid As List(Of NawaDevDAL.goAML_ActivitySTR_Upload)
        Get
            If Session("ReportSTRAdd.ListActivityNotValid") Is Nothing Then
                Session("ReportSTRAdd.ListActivityNotValid") = New List(Of NawaDevDAL.goAML_ActivitySTR_Upload)
            End If
            Return Session("ReportSTRAdd.ListActivityNotValid")
        End Get
        Set(ByVal value As List(Of NawaDevDAL.goAML_ActivitySTR_Upload))
            Session("ReportSTRAdd.ListActivityNotValid") = value
        End Set
    End Property

    Public Property PathFile() As String
        Get
            Return Session("ReportSTRAdd.PathFile")
        End Get
        Set(ByVal value As String)
            Session("ReportSTRAdd.PathFile") = value
        End Set
    End Property


#End Region

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            GridPanelSetting()
            Paging()
            If Not Ext.Net.X.IsAjaxRequest Then
                Dim strmodule As String = Request.Params("ModuleID")
                ClearSession()
                Dim intmodule As Integer = NawaBLL.Common.DecryptQueryString(strmodule, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Me.ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intmodule)

                If Not ObjModule Is Nothing Then
                    If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Insert) Then
                        Dim strIDCode As String = 1
                        strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                        Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                        Exit Sub
                    End If
                    FormPanelInput.Title = "Report STR  - Add New Record"

                    GridPanelReportInvalid.Hidden = True
                    GridPanelIndikatorLaporanInvalid.Hidden = True
                    GridPanelTransactionBipartyInvalid.Hidden = True
                    GridPanelTransactionMultiPartyInvalid.Hidden = True
                    GridPanelActivityPersonInvalid.Hidden = True
                    GridPanelActivityAccountInvalid.Hidden = True
                    GridPanelActivityEntityInvalid.Hidden = True

                    PanelReportInvalid.Hidden = True
                    PanelIndikatorLaporanInvalid.Hidden = True
                    PanelTransactionBipartyInvalid.Hidden = True
                    PanelTransactionMultiPartyInvalid.Hidden = True
                    PanelActivityPersonInvalid.Hidden = True
                    PanelActivityAccountInvalid.Hidden = True
                    PanelActivityEntityInvalid.Hidden = True



                Else
                    Throw New Exception("Invalid Module ID")
                End If
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Private Sub Load_Init(sender As Object, e As EventArgs) Handles Me.Init
    '    ObjEODTask = New NawaDevBLL.EODTaskBLL(FormPanelInput)
    'End Sub
#End Region

#Region "Method"
    Protected Sub BtnGenerate_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            ClearSession()
            If ListReportNotValid.Count > 0 Or ListTransactionBipartyNotValid.Count > 0 Or ListTransactionMultipartyNotValid.Count > 0 Or ListActivityNotValid.Count > 0 Or ListIndikatorLaporanNotValid.Count > 0 Then
                Throw New Exception("There's Have Invalid Data")
            End If




            If FileTemplateSTR.HasFile Then
                If ListReport_Upload.Count > 0 Then
                    For Each item As goAML_ReportSTR_Upload In ListReport_Upload
                        Dim ObjData As New goAML_ReportSTR
                        With ObjData
                            'If Not item.Case_ID = "" Then
                            .Case_ID = item.Case_ID
                            'End If
                            'If Not item.Tanggal_Laporan = "" Then
                            .Tanggal_Laporan = item.Tanggal_Laporan
                            'End If
                            'If Not item.Jenis_laporan = "" Then
                            .Jenis_laporan = item.Jenis_laporan
                            'End If
                            'If Not item.Ref_Num = "" Then
                            .Ref_Num = item.Ref_Num
                            'End If
                            'If Not item.Alasan = "" Then
                            .Alasan = item.Alasan
                            'End If
                            'If Not item.Tindakan_Pelapor = "" Then
                            .Tindakan_Pelapor = item.Tindakan_Pelapor
                            'End If
                            'If Not item.Fiu_Ref_Number = "" Then 'Danamon Add new item model Fiu Ref Number
                            .Fiu_Ref_Number = item.Fiu_Ref_Number
                            'End If

                            .ValidationMessage = item.KeteranganError
                            .isValid = True
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                            .Status = 1

                        End With
                        ListReportValid.Add(ObjData)
                    Next

                End If

                If ListActivity_Upload.Count > 0 Then
                    For Each item As goAML_ActivitySTR_Upload In ListActivity_Upload
                        Dim ObjData As New goAML_ActivitySTR
                        With ObjData
                            'If Not item.Case_ID = "" Then
                            .Case_ID = item.Case_ID
                            'End If
                            'If Not item.Significance = "" Then
                            .Significance = item.Significance
                            'End If
                            'If Not item.Reason = "" Then
                            .Reason = item.Reason
                            'End If
                            'If Not item.Comments = "" Then
                            .Comments = item.Comments
                            'End If
                            'If Not item.SubNodeType = "" Then
                            .SubNodeType = item.SubNodeType
                            'End If
                            'If Not item.isMyClient = "" Then
                            .isMyClient = item.isMyClient
                            'End If

                            If item.SubNodeType = 1 Then
                                'If Not item.Account_No = "" Then
                                .Account_No = item.Account_No
                                'End If
                                'If Not item.NamaPJK = "" Then
                                .NamaPJK = item.NamaPJK
                                'End If
                                'If Not item.KodePJK = "" Then
                                .KodePJK = item.KodePJK
                                'End If
                                'If Not item.KodeSwift = "" Then
                                .KodeSwift = item.KodeSwift
                                'End If

                            ElseIf item.SubNodeType = 2 Then
                                'If Not item.WIC_No = "" Then
                                .WIC_No = item.WIC_No
                                'End If
                                'If Not item.Last_Name = "" Then
                                .Last_Name = item.Last_Name
                                'End If
                                'If Not item.SSN = "" Then
                                .SSN = item.SSN
                                'End If
                                'If Not item.Identity_No = "" Then
                                .Identity_No = item.Identity_No
                                'End If
                                'If Not item.Passport_No = "" Then
                                .Passport_No = item.Passport_No
                                'End If

                            ElseIf item.SubNodeType = 3 Then
                                'If Not item.WIC_No = "" Then
                                .WIC_No = item.WIC_No
                                'End If
                                'If Not item.Name = "" Then
                                .Name = item.Name
                                'End If
                                'If Not item.Bussiness = "" Then
                                .Bussiness = item.Bussiness
                                'End If

                            End If


                            'If Not item.Comments = "" Then
                            .Comments = item.Comments
                            'End If
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                            .MessageValidation = item.KeteranganError
                        End With
                        ListActivityValid.Add(ObjData)
                    Next

                End If

                If ListIndikatorLaporan_Upload.Count > 0 Then
                    Dim listindikator = New List(Of goAML_Indikator_LaporanSTR)
                    For Each item As goAML_Indikator_LaporanSTR_Upload In ListIndikatorLaporan_Upload
                        Dim ObjData As New goAML_Indikator_LaporanSTR
                        With ObjData
                            .Case_ID = item.Case_ID

                            'If Not item.FK_Indikator_Laporan = "" Then
                            Dim id_indikator_laporan = item.FK_Indikator_Laporan
                                .FK_Indikator_Laporan = item.FK_Indikator_Laporan
                            'End If

                            .MessageValidation = item.KeteranganError
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                        End With
                        ListIndikatorLaporanValid.Add(ObjData)
                    Next

                End If

                If ListTransactionBiparty_Upload.Count > 0 Then
                    For Each item As goAML_Transaction_BiPartySTR_Upload In ListTransactionBiparty_Upload
                        Dim ObjData As New goAML_Transaction_BiPartySTR
                        With ObjData
                            .Case_ID = item.Case_ID
                            'If ListReport_Upload.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().Jenis_laporan IsNot Nothing And Not ListReport_Upload.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().Jenis_laporan = "" Then
                            '    .FK_Jenis_Laporan_ID = ListReport_Upload.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().Jenis_laporan
                            'End If
                            'If Not item.FK_Report_Type_ID = "" Then
                            '    .FK_Jenis_Laporan_ID = item.FK_Report_Type_ID
                            'End If

                            'If Not item.FK_Report_Type_ID = "" Then
                            .FK_Report_Type_ID = UploadReportSTR.GetJenisLaporan(item.FK_Report_Type_ID).NO_ID
                            'End If
                            'If Not item.FK_Jenis_Laporan_ID = "" Then
                            .FK_Jenis_Laporan_ID = item.FK_Jenis_Laporan_ID
                            'End If
                            'If Not item.TransactionNumber = "" Then
                            .TransactionNumber = item.TransactionNumber
                            'End If
                            'If Not item.Internal_Ref_Number = "" Then
                            .Internal_Ref_Number = item.Internal_Ref_Number
                            'End If
                            'If Not item.Transaction_Location = "" Then
                            .Transaction_Location = item.Transaction_Location
                            'End If
                            'If Not item.Transaction_Remark = "" Then
                            .Transaction_Remark = item.Transaction_Remark
                            'End If
                            'If Not item.Date_Transaction = "" Then
                            .Date_Transaction = item.Date_Transaction
                            'End If
                            'If Not item.Teller = "" Then
                            '.Teller = item.Teller
                            'End If
                            'If Not item.Authorized = "" Then
                            .Authorized = item.Authorized
                            'End If
                            If Not item.Late_Deposit = "" Then
                            .Late_Deposit = item.Late_Deposit
                            End If
                            If Not item.Date_Posting = "" Then
                                .Date_Posting = item.Date_Posting
                            End If

                            'If Not item.Value_Date = "" Then
                            '    .Value_Date = item.Value_Date
                            'End If
                            'If Not item.Transmode_Code = "" Then
                            .Transmode_Code = item.Transmode_Code
                            'End If
                            'If Not item.Transmode_Comment = "" Then
                            .Transmode_Comment = item.Transmode_Comment
                            'End If

                            'If Not item.Amount_Local = "" Then
                            .Amount_Local = item.Amount_Local
                            'End If
                            'If Not item.isMyClient_FROM = "" Then
                            .isMyClient_FROM = item.isMyClient_FROM
                            'End If
                            'If Not item.From_Funds_Code = "" Then
                            .From_Funds_Code = item.From_Funds_Code
                            'End If
                            'If Not item.From_Funds_Comment = "" Then
                            .From_Funds_Comment = item.From_Funds_Comment
                            'End If
                            'Vicky Himawan edited 19 Mar 2021
                            If Not item.From_Foreign_Currency_Code = "" Then
                                .From_Foreign_Currency_Code = UploadReportSTR.getMataUangCode(item.From_Foreign_Currency_Code).Kode_Pelaporan
                            End If

                            If Not item.From_Foreign_Currency_Amount = "" Then
                                .From_Foreign_Currency_Amount = item.From_Foreign_Currency_Amount
                            End If
                            If Not item.From_Foreign_Currency_Exchange_Rate = "" Then
                                .From_Foreign_Currency_Exchange_Rate = item.From_Foreign_Currency_Exchange_Rate
                            End If

                            '.FK_Person_ID = item.FK_Person_ID

                            If Not item.FK_Sender_From_Information = "" Then
                                .FK_Sender_From_Information = Convert.ToInt32(item.FK_Sender_From_Information)
                            End If
                            'Vicky Himawan edited 19 Mar 2021
                            If Not item.From_Country = "" Then
                                .From_Country = UploadReportSTR.getCountryCode(item.From_Country).Kode
                            End If

                            'If Not item.isMyClient_TO = "" Then
                            .isMyClient_TO = item.isMyClient_TO
                            'End If
                            'If Not item.To_Funds_Code = "" Then
                            .To_Funds_Code = item.To_Funds_Code
                            'End If

                            'If Not item.To_Funds_Comment = "" Then
                            .To_Funds_Comment = item.To_Funds_Comment
                            'End If
                            'Vicky Himawan edited 19 Mar 2021
                            If Not item.To_Foreign_Currency_Code = "" Then
                                .To_Foreign_Currency_Code = UploadReportSTR.getMataUangCode(item.To_Foreign_Currency_Code).Kode_Pelaporan
                            End If
                            If Not item.To_Foreign_Currency_Amount = "" Then
                                .To_Foreign_Currency_Amount = item.To_Foreign_Currency_Amount
                            End If
                            If Not item.To_Foreign_Currency_Exchange_Rate = "" Then
                                .To_Foreign_Currency_Exchange_Rate = item.To_Foreign_Currency_Exchange_Rate
                            End If
                            'If Not item.FK_Sender_To_Information = "" Then
                            .FK_Sender_To_Information = Convert.ToInt32(item.FK_Sender_To_Information)
                            'End If
                            'vicky Himawan edited 19 Mar 2021
                            If Not item.To_Country = "" Then
                                .To_Country = UploadReportSTR.getCountryCode(item.To_Country).Kode
                            End If
                            'If Not item.Comments = "" Then
                            .Comments = item.Comments
                            'End If
                            'If Not item.FK_Source_Data_ID = "" Then
                            .FK_Source_Data_ID = item.FK_Source_Data_ID
                            'End If
                            'If Not item.FROM_Account_No = "" Then
                            .FROM_Account_No = item.FROM_Account_No
                            'End If
                            'If Not item.FROM_CIFNO = "" Then
                            .FROM_CIFNO = item.FROM_CIFNO
                            'End If
                            'If Not item.From_WIC_NO = "" Then
                            .From_WIC_NO = item.From_WIC_NO
                            'End If
                            'If Not item.TO_ACCOUNT_NO = "" Then
                            .TO_ACCOUNT_NO = item.TO_ACCOUNT_NO
                            'End If
                            'If Not item.TO_CIF_NO = "" Then
                            .TO_CIF_NO = item.TO_CIF_NO
                            'End If
                            'If Not item.TO_WIC_NO = "" Then
                            .TO_WIC_NO = item.TO_WIC_NO
                            'End If
                            If Not item.Condutor_ID = "" Then
                                .Condutor_ID = item.Condutor_ID
                                .IsUsedConductor = True
                            Else
                                .IsUsedConductor = False
                            End If

                            .isSwift = IIf(item.isSwift = "(1) Yes", True, False)

                            'HSBC 20201117 Fachmi -- penambahan field swift code lawan
                            'If Not item.Swift_Code_Lawan = "" Then
                            .Swift_Code_Lawan = item.Swift_Code_Lawan
                            'End If

                            'vicky himawan Add 19 Mar 2021
                            'If Not item.Kode_PJK_Lawan = "" Then 
                            .Kode_PJK_Lawan = item.Kode_PJK_Lawan 'vicky add PJK_Lawan (Menambahkan PJK Lawan)
                            'End If

                            'If Not item.Nama_PJK_Lawan = "" Then
                            .Nama_PJK_Lawan = item.Nama_PJK_Lawan 'vicky add PJK_Lawan (Menambahkan PJK Lawan)
                            'End If
                            'vicky himawan ended 19 Mar 2021

                            .MessageValidation = item.KeteranganError
                            .FK_Report_Type_ID = 4
                            .FK_Transaction_Type = 2
                            .isValid = True
                            .IsUsedConductor = IIf(item.Condutor_ID = "(1) Yes", True, False)
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                        End With
                        ListTransactionBipartyValid.Add(ObjData)
                    Next

                End If

                If ListTransactionMultiparty_Upload.Count > 0 Then

                    For Each item As goAML_Transaction_MultiPartySTR_Upload In ListTransactionMultiparty_Upload
                        Dim ObjData As New goAML_Transaction_MultiPartySTR
                        With ObjData
                            .Case_ID = item.Case_ID
                            If Not item.Peran = "" Then
                                .Peran = UploadReportSTR.getroleParty(item.Peran).NO_ID
                            End If

                            If Not item.Signifikasi = "" Then
                                .Signifikasi = item.Signifikasi
                            End If

                            If Not item.FK_Report_Type_ID = "" Then
                                .FK_Report_Type_ID = UploadReportSTR.GetJenisLaporan(item.FK_Report_Type_ID).NO_ID
                            End If



                            'If Not item.FK_Jenis_Laporan_ID = "" Then
                            .FK_Jenis_Laporan_ID = item.FK_Jenis_Laporan_ID
                            'End If
                            'If Not item.FK_Jenis_Laporan_ID = "" Then
                            .FK_Jenis_Laporan_ID = item.FK_Jenis_Laporan_ID
                            'End If
                            'If Not item.TransactionNumber = "" Then
                            .TransactionNumber = item.TransactionNumber
                            'End If
                            'If Not item.Internal_Ref_Number = "" Then
                            .Internal_Ref_Number = item.Internal_Ref_Number
                            'End If
                            'If Not item.Transaction_Location = "" Then
                            .Transaction_Location = item.Transaction_Location
                            'End If
                            'If Not item.Transaction_Remark = "" Then
                            .Transaction_Remark = item.Transaction_Remark
                            'End If
                            'If Not item.Date_Transaction = "" Then
                            .Date_Transaction = item.Date_Transaction
                            'End If
                            'If Not item.Teller = "" Then
                            .Teller = item.Teller
                            'End If
                            'If Not item.Authorized = "" Then
                            .Authorized = item.Authorized
                            'End If
                            If Not item.Late_Deposit = "" Then
                                .Late_Deposit = item.Late_Deposit
                            End If
                            If Not item.Date_Posting = "" Then
                                .Date_Posting = item.Date_Posting
                            End If

                            'If Not item.Value_Date = "" Then
                            '    .Value_Date = item.Value_Date
                            'End If
                            'If Not item.Transmode_Code = "" Then
                            .Transmode_Code = item.Transmode_Code
                            'End If
                            'If Not item.Transmode_Comment = "" Then
                            .Transmode_Comment = item.Transmode_Comment
                            'End If

                            'If Not item.Amount_Local = "" Then
                            .Amount_Local = item.Amount_Local
                            'End If
                            'If Not item.isMyClient = "" Then
                            .isMyClient = item.isMyClient
                            'End If
                            'If Not item.Funds_Code = "" Then
                            .Funds_Code = item.Funds_Code
                            'End If
                            'If Not item.Funds_Comment = "" Then
                            .Funds_Comment = item.Funds_Comment
                            'End If
                            'Vicky Himawan edited 19 Mar 2021
                            If Not item.Foreign_Currency_Code = "" Then
                                .Foreign_Currency_Code = UploadReportSTR.getMataUangCode(item.Foreign_Currency_Code).Kode_Pelaporan
                            End If

                            If Not item.Foreign_Currency_Amount = "" Then
                            .Foreign_Currency_Amount = item.Foreign_Currency_Amount
                            End If
                            If Not item.Foreign_Currency_Exchange_Rate = "" Then
                                .Foreign_Currency_Exchange_Rate = item.Foreign_Currency_Exchange_Rate
                            End If

                            '.FK_Person_ID = item.FK_Person_ID

                            'If Not item.FK_Sender_Information = "" Then
                            .FK_Sender_Information = item.FK_Sender_Information
                            'End If
                            'Vicky Himawan edited 19 Mar 2021
                            If Not item.Country = "" Then
                                .Country = UploadReportSTR.getCountryCode(item.Country).Kode
                            End If
                            'If Not item.Comments = "" Then
                            .Comments = item.Comments
                            'End If
                            'If Not item.FK_Source_Data_ID = "" Then
                            .FK_Source_Data_ID = item.FK_Source_Data_ID
                            'End If
                            'If Not item.Account_No = "" Then
                            .Account_No = item.Account_No
                            'End If
                            'If Not item.CIFNO = "" Then
                            .CIFNO = item.CIFNO
                            'End If
                            If Not item.WIC_NO = "" Then
                                .WIC_NO = item.WIC_NO
                            End If
                            If Not item.Condutor_ID = "" Then
                                .Condutor_ID = item.Condutor_ID
                                .IsUsedConductor = True
                            Else
                                .IsUsedConductor = False
                            End If
                            .isSwift = IIf(item.isSwift = "(1) Yes", True, False)
                            .FK_Report_Type_ID = 4
                            .isValid = True
                            .IsUsedConductor = IIf(item.Condutor_ID = "(1) Yes", True, False)
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                            .MessageValidation = item.KeteranganError
                        End With
                        ListTransactionMultipartyValid.Add(ObjData)
                    Next
                End If

                If ListReportNotValid.Count > 0 Then
                    For Each item As goAML_ReportSTR_Upload In ListReportNotValid
                        Dim ObjData As New goAML_ReportSTR
                        With ObjData
                            .Case_ID = item.Case_ID
                            'If Not item.Tanggal_Laporan = "" Then
                            .Tanggal_Laporan = item.Tanggal_Laporan
                            'End If

                            'If Not item.Jenis_laporan = "" Then
                            .Jenis_laporan = item.Jenis_laporan
                            'End If

                            'If Not item.Ref_Num = "" Then
                            .Ref_Num = item.Ref_Num
                            'End If

                            'If Not item.Alasan = "" Then
                            .Alasan = item.Alasan
                            'End If


                            'If Not item.Tindakan_Pelapor = "" Then
                            .Tindakan_Pelapor = item.Tindakan_Pelapor
                            'End If

                            .isValid = True
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                            .Status = 1
                            .ValidationMessage = item.KeteranganError
                        End With
                        ListReportValid.Add(ObjData)
                    Next

                End If

                If ListActivityNotValid.Count > 0 Then
                    For Each item As goAML_ActivitySTR_Upload In ListActivityNotValid
                        Dim ObjData As New goAML_ActivitySTR
                        With ObjData
                            .Case_ID = item.Case_ID
                            'If Not item.Significance = "" Then
                            .Significance = item.Significance
                            'End If
                            'If Not item.Reason = "" Then
                            .Reason = item.Reason
                            'End If
                            'If Not item.Comments = "" Then
                            .Comments = item.Comments
                            'End If
                            'If Not item.SubNodeType = "" Then
                            .SubNodeType = item.SubNodeType
                            'End If
                            'If Not item.isMyClient = "" Then
                            .isMyClient = item.isMyClient
                            'End If



                            If item.SubNodeType = 1 Then
                                'If Not item.Account_No = "" Then
                                .Account_No = item.Account_No
                                'End If
                                'If Not item.NamaPJK = "" Then
                                .NamaPJK = item.NamaPJK
                                'End If
                                'If Not item.KodePJK = "" Then
                                .KodePJK = item.KodePJK
                                'End If
                                'If Not item.KodeSwift = "" Then
                                .KodeSwift = item.KodeSwift
                                'End If

                            ElseIf item.SubNodeType = 2 Then
                                'If Not item.WIC_No = "" Then
                                .WIC_No = item.WIC_No
                                'End If
                                'If Not item.Last_Name = "" Then
                                .Last_Name = item.Last_Name
                                'End If
                                'If Not item.SSN = "" Then
                                .SSN = item.SSN
                                'End If
                                'If Not item.Identity_No = "" Then
                                .Identity_No = item.Identity_No
                                'End If
                                'If Not item.Passport_No = "" Then
                                .Passport_No = item.Passport_No
                                'End If

                            ElseIf item.SubNodeType = 3 Then
                                'If Not item.WIC_No = "" Then
                                .WIC_No = item.WIC_No
                                'End If
                                'If Not item.Name = "" Then
                                .Name = item.Name
                                'End If
                                'If Not item.Bussiness = "" Then
                                .Bussiness = item.Bussiness
                                'End If

                            End If


                            'If Not item.Comments = "" Then
                            .Comments = item.Comments
                            'End If
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                            .MessageValidation = item.KeteranganError
                        End With
                        ListActivityValid.Add(ObjData)
                    Next

                End If

                If ListIndikatorLaporanNotValid.Count > 0 Then
                    Dim listindikator = New List(Of goAML_Indikator_LaporanSTR)
                    For Each item As goAML_Indikator_LaporanSTR_Upload In ListIndikatorLaporanNotValid
                        Dim ObjData As New goAML_Indikator_LaporanSTR
                        With ObjData
                            .Case_ID = item.Case_ID
                            'If Not item.FK_Indikator_Laporan = "" Then
                            Dim id_indikator_laporan = item.FK_Indikator_Laporan
                                .FK_Indikator_Laporan = CodeFromSheet(item.FK_Indikator_Laporan)
                            'End If

                            .MessageValidation = item.KeteranganError
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                        End With
                        ListIndikatorLaporanValid.Add(ObjData)
                    Next

                End If

                If ListTransactionBipartyNotValid.Count > 0 Then
                    For Each item As goAML_Transaction_BiPartySTR_Upload In ListTransactionBipartyNotValid
                        Dim ObjData As New goAML_Transaction_BiPartySTR
                        With ObjData
                            .Case_ID = item.Case_ID

                            'If Not item.FK_Report_Type_ID = "" Then
                            '    .FK_Report_Type_ID = UploadReportSTR.GetJenisLaporan(item.FK_Report_Type_ID).NO_ID
                            'End If

                            'If Not item.FK_Jenis_Laporan_ID = "" Then
                            .FK_Jenis_Laporan_ID = item.FK_Jenis_Laporan_ID
                            'End If
                            'If Not item.TransactionNumber = "" Then
                            .TransactionNumber = item.TransactionNumber
                            'End If
                            'If Not item.Internal_Ref_Number = "" Then
                            .Internal_Ref_Number = item.Internal_Ref_Number
                            'End If
                            'If Not item.Transaction_Location = "" Then
                            .Transaction_Location = item.Transaction_Location
                            'End If
                            'If Not item.Transaction_Remark = "" Then
                            .Transaction_Remark = item.Transaction_Remark
                            'End If
                            'If Not item.Date_Transaction = "" Then
                            .Date_Transaction = item.Date_Transaction
                            'End If
                            'If Not item.Teller = "" Then
                            .Teller = item.Teller
                            'End If
                            'If Not item.Authorized = "" Then
                            .Authorized = item.Authorized
                            'End If
                            If Not item.Late_Deposit = "" Then
                                .Late_Deposit = item.Late_Deposit
                            End If
                            If Not item.Date_Posting = "" Then
                                .Date_Posting = item.Date_Posting
                            End If

                            'If Not item.Value_Date = "" Then
                            '    .Value_Date = item.Value_Date
                            'End If
                            'If Not item.Transmode_Code = "" Then
                            .Transmode_Code = item.Transmode_Code
                            'End If
                            'If Not item.Transmode_Comment = "" Then
                            .Transmode_Comment = item.Transmode_Comment
                            'End If

                            'If Not item.Amount_Local = "" Then
                            .Amount_Local = item.Amount_Local
                            'End If
                            'If Not item.isMyClient_FROM = "" Then
                            .isMyClient_FROM = item.isMyClient_FROM
                            'End If
                            'If Not item.From_Funds_Code = "" Then
                            .From_Funds_Code = item.From_Funds_Code
                            'End If
                            'If Not item.From_Funds_Comment = "" Then
                            .From_Funds_Comment = item.From_Funds_Comment
                            'End If
                            'Vicky Himawan edited 19 Mar 2021
                            If Not item.From_Foreign_Currency_Code = "" Then
                                .From_Foreign_Currency_Code = UploadReportSTR.getMataUangCode(item.From_Foreign_Currency_Code).Kode_Pelaporan
                            End If

                            If Not item.From_Foreign_Currency_Amount = "" Then
                                .From_Foreign_Currency_Amount = item.From_Foreign_Currency_Amount
                            End If
                            If Not item.From_Foreign_Currency_Exchange_Rate = "" Then
                                .From_Foreign_Currency_Exchange_Rate = item.From_Foreign_Currency_Exchange_Rate
                            End If

                            '.FK_Person_ID = item.FK_Person_ID

                            If Not item.FK_Sender_From_Information = "" Then
                                .FK_Sender_From_Information = Convert.ToInt32(item.FK_Sender_From_Information)
                            End If
                            'Vicky Himawan edited 19 Mar 2021
                            If Not item.From_Country = "" Then
                                .From_Country = UploadReportSTR.getCountryCode(item.From_Country).Kode
                            End If

                            'If Not item.isMyClient_TO = "" Then
                            .isMyClient_TO = item.isMyClient_TO
                            'End If
                            'If Not item.To_Funds_Code = "" Then
                            .To_Funds_Code = item.To_Funds_Code
                            'End If

                            'If Not item.To_Funds_Comment = "" Then
                            .To_Funds_Comment = item.To_Funds_Comment
                            'End If
                            'Vicky Himawan edited 19 Mar 2021
                            If Not item.To_Foreign_Currency_Code = "" Then
                                .To_Foreign_Currency_Code = UploadReportSTR.getMataUangCode(item.To_Foreign_Currency_Code).Kode_Pelaporan
                            End If

                            If Not item.To_Foreign_Currency_Amount = "" Then
                                .To_Foreign_Currency_Amount = item.To_Foreign_Currency_Amount
                            End If
                            If Not item.To_Foreign_Currency_Exchange_Rate = "" Then
                                .To_Foreign_Currency_Exchange_Rate = item.To_Foreign_Currency_Exchange_Rate
                            End If
                            If Not item.FK_Sender_To_Information = "" Then
                                .FK_Sender_To_Information = Convert.ToInt32(item.FK_Sender_To_Information)
                            End If
                            'Vicky Himawan edited 19 Mar 2021
                            If Not item.To_Country = "" Then
                                .To_Country = UploadReportSTR.getCountryCode(item.To_Country).Kode
                            End If
                            'If Not item.Comments = "" Then
                            .Comments = item.Comments
                            'End If
                            'If Not item.FK_Source_Data_ID = "" Then
                            .FK_Source_Data_ID = item.FK_Source_Data_ID
                            'End If
                            'If Not item.FROM_Account_No = "" Then
                            .FROM_Account_No = item.FROM_Account_No
                            'End If
                            'If Not item.FROM_CIFNO = "" Then
                            .FROM_CIFNO = item.FROM_CIFNO
                            'End If
                            'If Not item.From_WIC_NO = "" Then
                            .From_WIC_NO = item.From_WIC_NO
                            'End If
                            'If Not item.TO_ACCOUNT_NO = "" Then
                            .TO_ACCOUNT_NO = item.TO_ACCOUNT_NO
                            'End If
                            'If Not item.TO_CIF_NO = "" Then
                            .TO_CIF_NO = item.TO_CIF_NO
                            'End If
                            'If Not item.TO_WIC_NO = "" Then
                            .TO_WIC_NO = item.TO_WIC_NO
                            'End If
                            If Not item.Condutor_ID = "" Then
                                .Condutor_ID = item.Condutor_ID
                                .IsUsedConductor = True
                            Else
                                .IsUsedConductor = False
                            End If
                            .isSwift = IIf(item.isSwift = "(1) Yes", True, False)

                            'HSBC 20201117 Fachmi -- penambahan field swift code lawan
                            'If Not item.Swift_Code_Lawan = "" Then
                            .Swift_Code_Lawan = item.Swift_Code_Lawan
                            'End If

                            'vicky himawan Add 19 Mar 2021
                            'If Not item.Kode_PJK_Lawan = "" Then
                            .Kode_PJK_Lawan = item.Kode_PJK_Lawan 'vicky add PJK_Lawan (Menambahkan PJK Lawan)
                            'End If

                            'If Not item.Nama_PJK_Lawan = "" Then
                            .Nama_PJK_Lawan = item.Nama_PJK_Lawan 'vicky add PJK_Lawan (Menambahkan PJK Lawan)
                            'End If
                            'vicky himawan ended 19 Mar 2021

                            .MessageValidation = item.KeteranganError
                            .FK_Report_Type_ID = 4
                            .FK_Transaction_Type = 2
                            .isValid = 0
                            .IsUsedConductor = IIf(item.Condutor_ID = "(1) Yes", True, False)
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                        End With
                        ListTransactionBipartyValid.Add(ObjData)
                    Next

                End If

                If ListTransactionMultipartyNotValid.Count > 0 Then

                    For Each item As goAML_Transaction_MultiPartySTR_Upload In ListTransactionMultipartyNotValid
                        Dim ObjData As New goAML_Transaction_MultiPartySTR
                        With ObjData
                            .Case_ID = item.Case_ID
                            'If Not item.FK_Report_Type_ID = "" Then
                            '    .FK_Report_Type_ID = UploadReportSTR.GetJenisLaporan(item.FK_Report_Type_ID).NO_ID
                            'End If

                            If Not item.Peran = "" Then
                                .Peran = UploadReportSTR.getroleParty(item.Peran).NO_ID
                            End If

                            'If Not item.Signifikasi = "" Then
                            .Signifikasi = item.Signifikasi
                            'End If

                            'If Not item.FK_Jenis_Laporan_ID = "" Then
                            .FK_Jenis_Laporan_ID = item.FK_Jenis_Laporan_ID
                            'End If
                            'If Not item.TransactionNumber = "" Then
                            .TransactionNumber = item.TransactionNumber
                            'End If
                            'If Not item.Internal_Ref_Number = "" Then
                            .Internal_Ref_Number = item.Internal_Ref_Number
                            'End If
                            'If Not item.Transaction_Location = "" Then
                            .Transaction_Location = item.Transaction_Location
                            'End If
                            'If Not item.Transaction_Remark = "" Then
                            .Transaction_Remark = item.Transaction_Remark
                            'End If
                            'If Not item.Date_Transaction = "" Then
                            .Date_Transaction = item.Date_Transaction
                            'End If
                            'If Not item.Teller = "" Then
                            .Teller = item.Teller
                            'End If
                            'If Not item.Authorized = "" Then
                            .Authorized = item.Authorized
                            'End If
                            If Not item.Late_Deposit = "" Then
                                .Late_Deposit = item.Late_Deposit
                            End If
                            If Not item.Date_Posting = "" Then
                                .Date_Posting = item.Date_Posting
                            End If

                            'If Not item.Value_Date = "" Then
                            '    .Value_Date = item.Value_Date
                            'End If
                            'If Not item.Transmode_Code = "" Then
                            .Transmode_Code = item.Transmode_Code
                            'End If
                            'If Not item.Transmode_Comment = "" Then
                            .Transmode_Comment = item.Transmode_Comment
                            'End If

                            'If Not item.Amount_Local = "" Then
                            .Amount_Local = item.Amount_Local
                            'End If
                            'If Not item.isMyClient = "" Then
                            .isMyClient = item.isMyClient
                            'End If
                            'If Not item.Funds_Code = "" Then
                            .Funds_Code = item.Funds_Code
                            'End If
                            'If Not item.Funds_Comment = "" Then
                            .Funds_Comment = item.Funds_Comment
                            'End If
                            If Not item.Foreign_Currency_Code = "" Then
                                .Foreign_Currency_Code = item.Foreign_Currency_Code
                            End If

                            If Not item.Foreign_Currency_Amount = "" Then
                                .Foreign_Currency_Amount = item.Foreign_Currency_Amount
                            End If
                            If Not item.Foreign_Currency_Exchange_Rate = "" Then
                                .Foreign_Currency_Exchange_Rate = item.Foreign_Currency_Exchange_Rate
                            End If

                            '.FK_Person_ID = item.FK_Person_ID

                            'If Not item.FK_Sender_Information = "" Then
                            .FK_Sender_Information = item.FK_Sender_Information
                            'End If

                            'If Not item.Country = "" Then
                            .Country = item.Country
                            'End If
                            'If Not item.Comments = "" Then
                            .Comments = item.Comments
                            'End If
                            'If Not item.FK_Source_Data_ID = "" Then
                            .FK_Source_Data_ID = item.FK_Source_Data_ID
                            'End If
                            'If Not item.Account_No = "" Then
                            .Account_No = item.Account_No
                            'End If
                            'If Not item.CIFNO = "" Then
                            .CIFNO = item.CIFNO
                            'End If
                            'If Not item.WIC_NO = "" Then
                            .WIC_NO = item.WIC_NO
                            'End If
                            If Not item.Condutor_ID = "" Then
                                .Condutor_ID = item.Condutor_ID
                                .IsUsedConductor = True
                            Else
                                .IsUsedConductor = False
                            End If
                            .isSwift = IIf(item.isSwift = "(1) Yes", True, False)
                            .FK_Report_Type_ID = 4
                            .isValid = 0
                            .IsUsedConductor = IIf(item.Condutor_ID = "(1) Yes", True, False)
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                            .MessageValidation = item.KeteranganError
                        End With
                        ListTransactionMultipartyValid.Add(ObjData)
                    Next
                End If


                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID.ToString = 1 OrElse Not (ObjModule.IsUseApproval) Then
                    UploadReportSTR.SaveUploadReportSTRTanpaApproval(ListReportValid, ListIndikatorLaporanValid, ListTransactionBipartyValid, ListTransactionMultipartyValid, ListActivityValid, ObjModule)
                    Panelconfirmation.Hidden = False
                    FormPanelInput.Hidden = True
                    LblConfirmation.Text = "Data Saved into Database"
                Else
                    UploadReportSTR.SaveUploadReportSTRApproval(ListReportValid, ListIndikatorLaporanValid, ListTransactionBipartyValid, ListTransactionMultipartyValid, ListActivityValid, ObjModule)
                    Panelconfirmation.Hidden = False
                    FormPanelInput.Hidden = True
                    LblConfirmation.Text = "Data Saved into Pending Approval"
                End If



            Else
                Ext.Net.X.Msg.Alert("Error", "No File Choosen").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try

    End Sub

    Protected Sub BtnCancel_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub


    Protected Sub BtnImport_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            ClearSession_Upload()
            'ButtonImport.Hidden = True
            'ButtonCancelImport.Hidden = True

            Using objdb As New NawaDAL.NawaDataEntities
                If FileTemplateSTR.HasFile Then
                    'Directory file fisik yang di-write di server
                    Dim strFilePath As String = Server.MapPath("~/Temp/TempReportSTR/")
                    Dim strworkingpaper As String = Me.SaveFileImportToSave(FileTemplateSTR)
                    Dim br As BinaryReader
                    PathFile = strFilePath & strworkingpaper
                    ''Format yang diterima sementara hanya xlsx
                    If strworkingpaper.EndsWith(".xlsx") Then
                        Dim bData As Byte()
                        br = New BinaryReader(System.IO.File.OpenRead(strFilePath & strworkingpaper))
                        bData = br.ReadBytes(br.BaseStream.Length)
                        Dim ms As MemoryStream = New MemoryStream(bData, 0, bData.Length)
                        ms.Write(bData, 0, bData.Length)

                        Using excelFile As New ExcelPackage(ms)
                            If excelFile.Workbook.Worksheets("Report") IsNot Nothing Then
                                ReadSheetReport(excelFile.Workbook.Worksheets("Report"))
                            End If

                            If excelFile.Workbook.Worksheets("Indikator Laporan") IsNot Nothing Then
                                ReadSheetIndikatorLaporan(excelFile.Workbook.Worksheets("Indikator Laporan"))
                            Else
                                Ext.Net.X.Msg.Alert("Error", "Sheet in this file not valid!").Show()
                                Return
                            End If
                            If excelFile.Workbook.Worksheets("Transaction BiParty") IsNot Nothing Then
                                ReadSheetTransactionBiParty(excelFile.Workbook.Worksheets("Transaction BiParty"))
                            Else
                                Ext.Net.X.Msg.Alert("Error", "Sheet in this file not valid!").Show()
                                Return
                            End If
                            If excelFile.Workbook.Worksheets("Transaction MultiParty") IsNot Nothing Then
                                ReadSheetTransactionMultiParty(excelFile.Workbook.Worksheets("Transaction MultiParty"))
                            Else
                                Ext.Net.X.Msg.Alert("Error", "Sheet in this file not valid!").Show()
                                Return
                            End If
                            If excelFile.Workbook.Worksheets("Activity Account") IsNot Nothing Then
                                ReadSheetActivityAccount(excelFile.Workbook.Worksheets("Activity Account"))
                            Else
                                Ext.Net.X.Msg.Alert("Error", "Sheet in this file not valid!").Show()
                                Return
                            End If
                            If excelFile.Workbook.Worksheets("Activity Person") IsNot Nothing Then
                                ReadSheetActivityPerson(excelFile.Workbook.Worksheets("Activity Person"))
                            Else
                                Ext.Net.X.Msg.Alert("Error", "Sheet in this file not valid!").Show()
                                Return
                            End If
                            If excelFile.Workbook.Worksheets("Activity Entity") IsNot Nothing Then
                                ReadSheetActivityEntity(excelFile.Workbook.Worksheets("Activity Entity"))
                            Else
                                Ext.Net.X.Msg.Alert("Error", "Sheet in this file not valid!").Show()
                                Return
                            End If

                            'Hide upload UI
                            ButtonImport.Disabled = True

                            'btnUpload.Hidden = True
                            'Label3.Hidden = True
                            'GridUploadSuccess.Hidden = False
                        End Using
                        ms.Flush()
                        ms.Close()

                        ms.Dispose()  'HSBC Delete File ReportSTR 20210122
                        br.Close()  'HSBC Delete File ReportSTR 20210122
                        br.Dispose()  'HSBC Delete File ReportSTR 20210122

                    ElseIf strworkingpaper.EndsWith(".xls") Then
                        Ext.Net.X.Msg.Alert("Error", "Format Excel Not Supported!").Show()
                        Return
                    Else
                        Ext.Net.X.Msg.Alert("Error", "Unsupported format File!").Show()
                        Return
                    End If
                    'Delete temp file
                    UploadReportSTR.SaveAddUploadReportSTR(ListReport_Upload, ListIndikatorLaporan_Upload, ListTransactionBiparty_Upload, ListTransactionMultiparty_Upload, ListActivity_Upload, ObjModule)
                    UploadReportSTR.CheckValidationSheetSTR()
                    ClearSession_Upload()
                    ListUploadFromDatabase()
                    Bindgrid()

                    'HSBC Delete File ReportSTR 20210122
                    Dim DeletePathFile As String = PathFile
                    System.IO.File.Delete(DeletePathFile)
                Else
                    Ext.Net.X.Msg.Alert("Error", "No File Choosen").Show()
                End If
            End Using




        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try

    End Sub

    Public Function SaveFileImportToSave(ByVal FileUpload As FileUploadField) As String
        Dim filePath As String
        Dim FileNameToGetData As String = ""
        Try
            ''Save file upload ke file fisik di server untuk di-read
            If FileUpload.PostedFile.ContentLength <> 0 Then
                If FileUpload.HasFile Then
                    Dim strFileName As String = ""
                    If FileUpload.FileName.Contains("\") Then
                        Dim lstFileName() As String = FileUpload.FileName.Split("\")
                        strFileName = lstFileName(lstFileName.Count - 1)
                    Else
                        strFileName = FileUpload.FileName
                    End If

                    filePath = Server.MapPath("~/Temp/TempReportSTR/" & strFileName)
                    FileNameToGetData = strFileName
                    If System.IO.File.Exists(filePath) Then
                        Dim bexist As Boolean = True
                        Dim i As Integer = 1
                        While bexist
                            filePath = Server.MapPath("~/Temp/TempReportSTR/" & strFileName.Split(CChar("."))(0) & "-" & i.ToString & "." & strFileName.Split(CChar("."))(1))
                            FileNameToGetData = strFileName.Split(CChar("."))(0) & "-" & i.ToString & "." & strFileName.Split(CChar("."))(1)
                            If Not System.IO.File.Exists(filePath) Then
                                bexist = False
                                Exit While
                            End If
                            i = i + 1
                        End While
                    End If
                    'FileUpload.SaveAs(filePath)
                    System.IO.File.WriteAllBytes(filePath, FileUpload.FileBytes)
                End If
            End If
            Return FileNameToGetData
        Catch
            Throw
        End Try
    End Function

    Private Sub ReadSheetReport(ByVal sheet As ExcelWorksheet)
        Try
            Dim StrAnomalyDescription As String = ""
            Dim SBError As StringBuilder = New StringBuilder
            Dim IRow As Integer = 2

            While IRow <> 0
                Dim ErrorMessage As String = ""
                Dim StrModeUpload As String = ""

                'Gunakan Mode Sebagai Key... 
                ''Cari nama module, jika kosong diasumsikan bahwa itu row terakhir

                StrModeUpload = sheet.Cells(IRow, 1).Text.Trim
                If Not String.IsNullOrEmpty(StrModeUpload) Then
                    Dim ObjData As New goAML_ReportSTR_Upload
                    With ObjData

                        Dim objrand As New Random
                        Dim intpk As Long = objrand.Next(Integer.MinValue, -1)
                        While Not ListReport_Upload.Find(Function(x) x.PK_ID = intpk) Is Nothing
                            intpk = objrand.Next(Integer.MinValue, -1)
                        End While

                        .PK_ID = intpk
                        .Case_ID = sheet.Cells(IRow, 1).Text.Trim
                        .Tanggal_Laporan = sheet.Cells(IRow, 2).Text
                        Dim jenislaporan = sheet.Cells(IRow, 3).Text.Trim
                        .Jenis_laporan = CodeFromSheet(jenislaporan)
                        .Ref_Num = sheet.Cells(IRow, 4).Text.Trim
                        .Fiu_Ref_Number = sheet.Cells(IRow, 5).Text.Trim 'Danamon add new model Fiu_ref_number
                        .Alasan = sheet.Cells(IRow, 6).Text.Trim
                        .Tindakan_Pelapor = sheet.Cells(IRow, 7).Text.Trim
                        .isValid = False
                        .Comments = sheet.Cells(IRow, 8).Text.Trim
                        .Active = False

                    End With
                    ListReport_Upload.Add(ObjData)
                    IRow = IRow + 1
                Else
                    IRow = 0
                End If
            End While

            'Bind Grid
            'Me.BindDataTableAnomallyData()
            'Me.BindDataTableSuccessFull()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ReadSheetIndikatorLaporan(ByVal sheet As ExcelWorksheet)
        Try
            Dim StrAnomalyDescription As String = ""
            Dim SBError As StringBuilder = New StringBuilder
            Dim IRow As Integer = 2

            While IRow <> 0
                Dim ErrorMessage As String = ""
                Dim StrModeUpload As String = ""

                'Gunakan Mode Sebagai Key... 
                ''Cari nama module, jika kosong diasumsikan bahwa itu row terakhir
                StrModeUpload = sheet.Cells(IRow, 1).Text.Trim
                If Not String.IsNullOrEmpty(StrModeUpload) Then
                    Dim ObjData As New goAML_Indikator_LaporanSTR_Upload
                    With ObjData

                        Dim objrand As New Random
                        Dim intpk As Long = objrand.Next(Integer.MinValue, -1)
                        While Not ListIndikatorLaporan_Upload.Find(Function(x) x.PK_ID = intpk) Is Nothing
                            intpk = objrand.Next(Integer.MinValue, -1)
                        End While

                        .PK_ID = intpk
                        .Case_ID = sheet.Cells(IRow, 1).Text.Trim
                        'Dim id_indikator_laporan = sheet.Cells(IRow, 2).Text.Trim
                        'If UploadReportSTR.GetIDIndikatorLaporan(CodeFromSheet(id_indikator_laporan)) IsNot Nothing Then
                        '    .FK_Indikator_Laporan = UploadReportSTR.GetIDIndikatorLaporan(CodeFromSheet(id_indikator_laporan)).NO_ID
                        'Else
                        '    .FK_Indikator_Laporan = ""
                        'End If
                        .FK_Indikator_Laporan = CodeFromSheet(sheet.Cells(IRow, 2).Text.Trim)


                    End With
                    ListIndikatorLaporan_Upload.Add(ObjData)
                    IRow = IRow + 1
                Else
                    IRow = 0
                End If
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ReadSheetTransactionBiParty(ByVal sheet As ExcelWorksheet)
        Try
            Dim StrAnomalyDescription As String = ""
            Dim SBError As StringBuilder = New StringBuilder
            Dim IRow As Integer = 2

            While IRow <> 0
                Dim ErrorMessage As String = ""
                Dim StrModeUpload As String = ""

                'Gunakan Mode Sebagai Key... 
                ''Cari nama module, jika kosong diasumsikan bahwa itu row terakhir
                StrModeUpload = sheet.Cells(IRow, 1).Text.Trim
                If Not String.IsNullOrEmpty(StrModeUpload) Then

                    Dim ObjData As New goAML_Transaction_BiPartySTR_Upload
                    With ObjData
                        Dim objrand As New Random
                        Dim intpk As Long = objrand.Next(Integer.MinValue, -1)
                        While Not ListTransactionBiparty_Upload.Find(Function(x) x.PK_Transaction_ID = intpk) Is Nothing
                            intpk = objrand.Next(Integer.MinValue, -1)
                        End While

                        .PK_Transaction_ID = intpk
                        .Case_ID = sheet.Cells(IRow, 1).Text.Trim
                        .Date_Transaction = sheet.Cells(IRow, 2).Text.Trim
                        .Internal_Ref_Number = sheet.Cells(IRow, 8).Text.Trim
                        .Amount_Local = sheet.Cells(IRow, 3).Text.Trim
                        .FK_Source_Data_ID = CodeFromSheet(sheet.Cells(IRow, 4).Text.Trim)
                        Dim listchartransmodecode = sheet.Cells(IRow, 5).Text.Trim
                        .Transmode_Code = CodeFromSheet(listchartransmodecode)
                        .Transmode_Comment = sheet.Cells(IRow, 6).Text.Trim
                        .Transaction_Remark = sheet.Cells(IRow, 7).Text.Trim
                        .TransactionNumber = sheet.Cells(IRow, 8).Text.Trim
                        .Transaction_Location = sheet.Cells(IRow, 9).Text.Trim
                        .Teller = sheet.Cells(IRow, 10).Text.Trim
                        .Authorized = sheet.Cells(IRow, 11).Text.Trim


                        If Not sheet.Cells(IRow, 12).Text = "" Then
                            .Late_Deposit = IIf(sheet.Cells(IRow, 12).Text.Trim.ToLower = "ya", True, False)
                        Else
                            .Late_Deposit = ""
                        End If

                        .Date_Posting = sheet.Cells(IRow, 13).Text.Trim

                        If Not sheet.Cells(IRow, 14).Text.Trim = "" Then
                            .FK_Sender_From_Information = CodeFromSheet(sheet.Cells(IRow, 14).Text.Trim)
                        Else
                            .FK_Sender_From_Information = ""
                        End If


                        If ListReport_Upload IsNot Nothing Then
                            Dim jenislaporan = ListReport_Upload.Where(Function(x) x.Case_ID = .Case_ID).FirstOrDefault()
                            If jenislaporan IsNot Nothing Then
                                If jenislaporan.Jenis_laporan IsNot Nothing Then
                                    .FK_Report_Type_ID = ListReport_Upload.Where(Function(x) x.Case_ID = .Case_ID).FirstOrDefault().Jenis_laporan
                                End If
                            End If
                        End If

                        If Not sheet.Cells(IRow, 15).Text = "" Then
                            .isMyClient_FROM = IIf(sheet.Cells(IRow, 15).Text.Substring(1, 1) = "1", True, False)
                        Else
                            .isMyClient_FROM = ""
                        End If


                        'If Not sheet.Cells(IRow, 32).Text.Trim = "" Then
                        '    .FK_Sender_From_Information = 1
                        'ElseIf Not sheet.Cells(IRow, 33).Text.Trim = "" Or Not sheet.Cells(IRow, 34).Text.Trim = "" Then
                        '    Dim send = 0
                        '    If .isMyClient_FROM = True Then
                        '        send = UploadReportSTR.getsendermyclient(sheet.Cells(IRow, 33).Text.Trim).FK_Customer_Type_ID
                        '    Else
                        '        send = UploadReportSTR.getsendernotmyclient(sheet.Cells(IRow, 34).Text.Trim).FK_Customer_Type_ID
                        '    End If
                        '    If send = 1 Then
                        '        .FK_Sender_From_Information = 2
                        '    ElseIf send = 2 Then
                        '        .FK_Sender_From_Information = 3
                        '    Else
                        '        .FK_Sender_From_Information = ""
                        '    End If
                        'End If

                        Dim listcharfromfundscode = sheet.Cells(IRow, 16).Text.Trim
                        .From_Funds_Code = CodeFromSheet(listcharfromfundscode)
                        .From_Funds_Comment = sheet.Cells(IRow, 17).Text.Trim
                        'Dim listcharfromcurrencycode = sheet.Cells(IRow, 18).Text.Trim 'vicky edited 19 Mar 2021 tidak digunakan lagi karena langsung berbentuk kode
                        .From_Foreign_Currency_Code = sheet.Cells(IRow, 18).Text.Trim
                        .From_Foreign_Currency_Amount = sheet.Cells(IRow, 19).Text.Trim
                        .From_Foreign_Currency_Exchange_Rate = sheet.Cells(IRow, 20).Text.Trim
                        'Dim listcharfromcountry = sheet.Cells(IRow, 21).Text.Trim 'vicky edited 19 Mar 2021 tidak digunakan lagi karena langsung berbentuk kode
                        .From_Country = sheet.Cells(IRow, 21).Text.Trim

                        If Not sheet.Cells(IRow, 23).Text = "" Then
                            .isMyClient_TO = IIf(sheet.Cells(IRow, 23).Text.Substring(1, 1) = "1", True, False)
                        Else
                            .isMyClient_TO = ""
                        End If

                        'If Not sheet.Cells(IRow, 35).Text.Trim = "" Then
                        '    .FK_Sender_To_Information = 1
                        'ElseIf Not sheet.Cells(IRow, 37).Text.Trim = "" Or Not sheet.Cells(IRow, 36).Text.Trim = "" Then
                        '    Dim send = 0
                        '    If .isMyClient_TO = True Then
                        '        send = UploadReportSTR.getsendermyclient(sheet.Cells(IRow, 36).Text.Trim).FK_Customer_Type_ID
                        '    Else
                        '        send = UploadReportSTR.getsendernotmyclient(sheet.Cells(IRow, 37).Text.Trim).FK_Customer_Type_ID
                        '    End If
                        '    If send = 1 Then
                        '        .FK_Sender_To_Information = 2
                        '    ElseIf send = 2 Then
                        '        .FK_Sender_To_Information = 3
                        '    Else
                        '        .FK_Sender_To_Information = ""
                        '    End If
                        'End If

                        If Not sheet.Cells(IRow, 22).Text.Trim = "" Then
                            .FK_Sender_To_Information = CodeFromSheet(sheet.Cells(IRow, 22).Text.Trim)
                        Else
                            .FK_Sender_To_Information = ""
                        End If



                        Dim listchartofundscode = sheet.Cells(IRow, 24).Text.Trim
                        .To_Funds_Code = CodeFromSheet(listchartofundscode)
                        .To_Funds_Comment = sheet.Cells(IRow, 25).Text.Trim
                        'Dim listchartocurrencycode = sheet.Cells(IRow, 26).Text.Trim 'vicky edited 19 Mar 2021 tidak digunakan lagi karena langsung berbentuk kode
                        .To_Foreign_Currency_Code = sheet.Cells(IRow, 26).Text.Trim
                        .To_Foreign_Currency_Amount = sheet.Cells(IRow, 27).Text.Trim
                        .To_Foreign_Currency_Exchange_Rate = sheet.Cells(IRow, 28).Text.Trim
                        'Dim listchartocountry = sheet.Cells(IRow, 29).Text.Trim 'vicky edited 19 Mar 2021 tidak digunakan lagi karena langsung berbentuk kode
                        .To_Country = sheet.Cells(IRow, 29).Text.Trim
                        .Comments = sheet.Cells(IRow, 30).Text.Trim
                        .isSwift = sheet.Cells(IRow, 31).Text.Trim
                        .FROM_Account_No = sheet.Cells(IRow, 32).Text.Trim
                        .FROM_CIFNO = sheet.Cells(IRow, 33).Text.Trim
                        .From_WIC_NO = sheet.Cells(IRow, 34).Text.Trim
                        .TO_ACCOUNT_NO = sheet.Cells(IRow, 35).Text.Trim
                        .TO_CIF_NO = sheet.Cells(IRow, 36).Text.Trim
                        .TO_WIC_NO = sheet.Cells(IRow, 37).Text.Trim
                        .Condutor_ID = sheet.Cells(IRow, 38).Text.Trim
                        .Swift_Code_Lawan = sheet.Cells(IRow, 39).Text.Trim 'HSBC 20201117
                        .Kode_PJK_Lawan = sheet.Cells(IRow, 40).Text.Trim 'vicky add PJK_Lawan (Menambahkan PJK Lawan)
                        .Nama_PJK_Lawan = sheet.Cells(IRow, 41).Text.Trim 'vicky add PJK_Lawan (Menambahkan PJK Lawan)
                    End With
                    ListTransactionBiparty_Upload.Add(ObjData)
                    IRow = IRow + 1
                Else
                    IRow = 0
                End If
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ReadSheetTransactionMultiParty(ByVal sheet As ExcelWorksheet)
        Try
            Dim StrAnomalyDescription As String = ""
            Dim SBError As StringBuilder = New StringBuilder
            Dim IRow As Integer = 2

            While IRow <> 0
                Dim ErrorMessage As String = ""
                Dim StrModeUpload As String = ""

                'Gunakan Mode Sebagai Key... 
                ''Cari nama module, jika kosong diasumsikan bahwa itu row terakhir
                StrModeUpload = sheet.Cells(IRow, 1).Text.Trim
                If Not String.IsNullOrEmpty(StrModeUpload) Then

                    Dim ObjData As New goAML_Transaction_MultiPartySTR_Upload
                    With ObjData

                        Dim objrand As New Random
                        Dim intpk As Long = objrand.Next(Integer.MinValue, -1)
                        While Not ListTransactionMultiparty_Upload.Find(Function(x) x.PK_Transaction_ID = intpk) Is Nothing
                            intpk = objrand.Next(Integer.MinValue, -1)
                        End While

                        .PK_Transaction_ID = intpk
                        .Case_ID = sheet.Cells(IRow, 1).Text.Trim
                        .Date_Transaction = sheet.Cells(IRow, 2).Text.Trim
                        .Internal_Ref_Number = sheet.Cells(IRow, 8).Text.Trim
                        .Amount_Local = sheet.Cells(IRow, 3).Text.Trim
                        .FK_Source_Data_ID = CodeFromSheet(sheet.Cells(IRow, 4).Text.Trim)
                        Dim listchartransmodecode = sheet.Cells(IRow, 5).Text.Trim
                        .Transmode_Code = CodeFromSheet(listchartransmodecode)
                        .Transmode_Comment = sheet.Cells(IRow, 6).Text.Trim
                        .Transaction_Remark = sheet.Cells(IRow, 7).Text.Trim
                        .TransactionNumber = sheet.Cells(IRow, 8).Text.Trim
                        .Transaction_Location = sheet.Cells(IRow, 9).Text.Trim
                        .Teller = sheet.Cells(IRow, 10).Text.Trim
                        .Authorized = sheet.Cells(IRow, 11).Text.Trim
                        If ListReport_Upload IsNot Nothing Then
                            Dim jenislaporan = ListReport_Upload.Where(Function(x) x.Case_ID = .Case_ID).FirstOrDefault()
                            If jenislaporan IsNot Nothing Then
                                If jenislaporan.Jenis_laporan IsNot Nothing Then
                                    .FK_Report_Type_ID = ListReport_Upload.Where(Function(x) x.Case_ID = .Case_ID).FirstOrDefault().Jenis_laporan
                                End If
                            End If
                        End If

                        If Not sheet.Cells(IRow, 12).Text = "" Then
                            .Late_Deposit = IIf(sheet.Cells(IRow, 12).Text.Trim.ToLower = "ya", True, False)
                        Else
                            .Late_Deposit = ""
                        End If


                        .Date_Posting = sheet.Cells(IRow, 13).Text.Trim
                        Dim listcharperan = sheet.Cells(IRow, 14).Text.Trim
                        .Peran = CodeFromSheet(listcharperan)
                        .Signifikasi = sheet.Cells(IRow, 15).Text.Trim
                        If Not sheet.Cells(IRow, 16).Text = "" Then
                            .FK_Sender_Information = CodeFromSheet(sheet.Cells(IRow, 16).Text.Trim)
                        Else
                            .FK_Sender_Information = ""
                        End If

                        If Not sheet.Cells(IRow, 17).Text = "" Then
                            .isMyClient = IIf(sheet.Cells(IRow, 17).Text.Trim.Substring(1, 1) = "1", True, False)
                        Else
                            .isMyClient = ""
                        End If


                        'If Not sheet.Cells(IRow, 27).Text.Trim = "" Then
                        '    .FK_Sender_Information = 1
                        'ElseIf Not sheet.Cells(IRow, 29).Text.Trim = "" Or Not sheet.Cells(IRow, 28).Text.Trim = "" Then
                        '    Dim send = 0
                        '    If .isMyClient = True Then
                        '        send = UploadReportSTR.getsendermyclient(sheet.Cells(IRow, 28).Text.Trim).FK_Customer_Type_ID
                        '    Else
                        '        send = UploadReportSTR.getsendernotmyclient(sheet.Cells(IRow, 29).Text.Trim).FK_Customer_Type_ID
                        '    End If
                        '    If send = 1 Then
                        '        .FK_Sender_Information = 2
                        '    ElseIf send = 2 Then
                        '        .FK_Sender_Information = 3
                        '    Else
                        '        .FK_Sender_Information = ""
                        '    End If
                        'End If


                        Dim listcharfundscode = sheet.Cells(IRow, 18).Text.Trim
                        .Funds_Code = CodeFromSheet(listcharfundscode)
                        .Funds_Comment = sheet.Cells(IRow, 19).Text.Trim
                        'Dim listcharcurrency_code = sheet.Cells(IRow, 20).Text.Trim 'vicky edited 19 Mar 2021 tidak digunakan lagi karena langsung kode
                        .Foreign_Currency_Code = sheet.Cells(IRow, 20).Text.Trim
                        .Foreign_Currency_Amount = sheet.Cells(IRow, 21).Text.Trim
                        .Foreign_Currency_Exchange_Rate = sheet.Cells(IRow, 22).Text.Trim
                        'Dim listcharcountry = sheet.Cells(IRow, 23).Text.Trim 'vicky edited 19 Mar 2021 tidak digunakan lagi karena langsung kode
                        .Country = sheet.Cells(IRow, 23).Text.Trim
                        .Comments = sheet.Cells(IRow, 24).Text.Trim
                        .isSwift = sheet.Cells(IRow, 25).Text.Trim
                        .Account_No = sheet.Cells(IRow, 26).Text.Trim
                        .CIFNO = sheet.Cells(IRow, 27).Text.Trim
                        .WIC_NO = sheet.Cells(IRow, 28).Text.Trim
                        .Condutor_ID = sheet.Cells(IRow, 29).Text.Trim
                    End With
                    ListTransactionMultiparty_Upload.Add(ObjData)
                    IRow = IRow + 1
                Else
                    IRow = 0
                End If
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ReadSheetActivityPerson(ByVal sheet As ExcelWorksheet)
        Try
            Dim StrAnomalyDescription As String = ""
            Dim SBError As StringBuilder = New StringBuilder
            Dim IRow As Integer = 2

            While IRow <> 0
                Dim ErrorMessage As String = ""
                Dim StrModeUpload As String = ""

                'Gunakan Mode Sebagai Key... 
                ''Cari nama module, jika kosong diasumsikan bahwa itu row terakhir
                StrModeUpload = sheet.Cells(IRow, 1).Text.Trim
                If Not String.IsNullOrEmpty(StrModeUpload) Then

                    Dim ObjData As New goAML_ActivitySTR_Upload
                    With ObjData

                        Dim objrand As New Random
                        Dim intpk As Long = objrand.Next(Integer.MinValue, -1)
                        While Not ListActivity_Upload.Find(Function(x) x.PK_ID = intpk) Is Nothing
                            intpk = objrand.Next(Integer.MinValue, -1)
                        End While

                        .PK_ID = intpk
                        .SubNodeType = 2
                        .Case_ID = sheet.Cells(IRow, 1).Text.Trim
                        .Significance = sheet.Cells(IRow, 2).Text.Trim
                        .Reason = sheet.Cells(IRow, 3).Text.Trim
                        .Comments = sheet.Cells(IRow, 4).Text.Trim
                        .isMyClient = False
                        'If Not sheet.Cells(IRow, 5).Text = "" Then
                        '    .isMyClient = IIf(sheet.Cells(IRow, 5).Text.Trim.Substring(1, 1) = "1", True, False)
                        'Else
                        '    .isMyClient = ""
                        'End If

                        '.WIC_No = sheet.Cells(IRow, 6).Text.Trim
                        .Last_Name = sheet.Cells(IRow, 5).Text.Trim
                        .SSN = sheet.Cells(IRow, 6).Text.Trim
                        .Passport_No = sheet.Cells(IRow, 7).Text.Trim
                        .Identity_No = sheet.Cells(IRow, 8).Text.Trim

                    End With
                    ListActivity_Upload.Add(ObjData)
                    IRow = IRow + 1
                Else
                    IRow = 0
                End If
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ReadSheetActivityAccount(ByVal sheet As ExcelWorksheet)
        Try
            Dim StrAnomalyDescription As String = ""
            Dim SBError As StringBuilder = New StringBuilder
            Dim IRow As Integer = 2

            While IRow <> 0
                Dim ErrorMessage As String = ""
                Dim StrModeUpload As String = ""

                'Gunakan Mode Sebagai Key... 
                ''Cari nama module, jika kosong diasumsikan bahwa itu row terakhir
                StrModeUpload = sheet.Cells(IRow, 1).Text.Trim
                If Not String.IsNullOrEmpty(StrModeUpload) Then

                    Dim ObjData As New goAML_ActivitySTR_Upload
                    With ObjData

                        Dim objrand As New Random
                        Dim intpk As Long = objrand.Next(Integer.MinValue, -1)
                        While Not ListActivity_Upload.Find(Function(x) x.PK_ID = intpk) Is Nothing
                            intpk = objrand.Next(Integer.MinValue, -1)
                        End While

                        .PK_ID = intpk
                        .SubNodeType = 1
                        .Case_ID = sheet.Cells(IRow, 1).Text.Trim
                        .Significance = sheet.Cells(IRow, 2).Text.Trim
                        .Reason = sheet.Cells(IRow, 3).Text.Trim
                        .Comments = sheet.Cells(IRow, 4).Text.Trim
                        'If Not sheet.Cells(IRow, 5).Text = "" Then
                        '    .isMyClient = IIf(sheet.Cells(IRow, 5).Text.Trim.Substring(1, 1) = "1", True, False)
                        'Else
                        '    .isMyClient = ""
                        'End If
                        .isMyClient = False
                        .Account_No = sheet.Cells(IRow, 5).Text.Trim
                        .NamaPJK = sheet.Cells(IRow, 6).Text.Trim
                        .KodePJK = sheet.Cells(IRow, 7).Text.Trim
                        .KodeSwift = sheet.Cells(IRow, 8).Text.Trim

                    End With
                    ListActivity_Upload.Add(ObjData)
                    IRow = IRow + 1
                Else
                    IRow = 0
                End If
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ReadSheetActivityEntity(ByVal sheet As ExcelWorksheet)
        Try
            Dim StrAnomalyDescription As String = ""
            Dim SBError As StringBuilder = New StringBuilder
            Dim IRow As Integer = 2

            While IRow <> 0
                Dim ErrorMessage As String = ""
                Dim StrModeUpload As String = ""

                'Gunakan Mode Sebagai Key... 
                ''Cari nama module, jika kosong diasumsikan bahwa itu row terakhir
                StrModeUpload = sheet.Cells(IRow, 1).Text.Trim
                If Not String.IsNullOrEmpty(StrModeUpload) Then

                    Dim ObjData As New goAML_ActivitySTR_Upload
                    With ObjData

                        Dim objrand As New Random
                        Dim intpk As Long = objrand.Next(Integer.MinValue, -1)
                        While Not ListActivity_Upload.Find(Function(x) x.PK_ID = intpk) Is Nothing
                            intpk = objrand.Next(Integer.MinValue, -1)
                        End While

                        .PK_ID = intpk
                        .SubNodeType = 3
                        .Case_ID = sheet.Cells(IRow, 1).Text.Trim
                        .Significance = sheet.Cells(IRow, 2).Text.Trim
                        .Reason = sheet.Cells(IRow, 3).Text.Trim
                        .Comments = sheet.Cells(IRow, 4).Text.Trim
                        'If Not sheet.Cells(IRow, 5).Text = "" Then
                        '    .isMyClient = IIf(sheet.Cells(IRow, 5).Text.Trim.Substring(1, 1) = "1", True, False)
                        'Else
                        '    .isMyClient = ""
                        'End If
                        '.WIC_No = sheet.Cells(IRow, 6).Text.Trim
                        .isMyClient = False
                        .Name = sheet.Cells(IRow, 5).Text.Trim
                        .Bussiness = sheet.Cells(IRow, 6).Text.Trim
                    End With
                    ListActivity_Upload.Add(ObjData)
                    IRow = IRow + 1
                Else
                    IRow = 0
                End If
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function CodeFromSheet(StringFrom As String) As String
        Try

            Dim List = ""
            If Not StringFrom = "" Then
                List = StringFrom.Substring(1, StringFrom.Length - 1).ToCharArray()
            End If

            Dim StrResult = ""
            If List.Count > 0 Then
                For Each item As Char In List
                    If item = ")" Then
                        Exit For
                    End If
                    StrResult += item
                Next
            End If

            Return StrResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub GridCmdTransactionMultiparty(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadPanelTransactionMultipartyDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridCmdTransactionBiparty(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                LoadPanelTransactionBipartyDetail(id)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadPanelTransactionBipartyDetail(id As Long)
        Try
            windowDetailTransactionBiparty.Hidden = False
            Dim item = UploadReportSTR.getDetailTransactionBiparty(id)
            If Not item.Case_ID Is Nothing Then
                textCaseID.Text = item.Case_ID
            End If
            If Not item.TransactionNumber Is Nothing Then
                txtTransactionNumber.Text = item.TransactionNumber
            End If
            If Not item.Internal_Ref_Number Is Nothing Then
                txtInternal_Ref_Number.Text = item.Internal_Ref_Number
            End If
            If Not item.Transaction_Location Is Nothing Then
                txtTransaction_Location.Text = item.Transaction_Location
            End If
            If Not item.Transaction_Remark Is Nothing Then
                txtTransaction_Remark.Text = item.Transaction_Remark
            End If
            If Not item.Date_Transaction Is Nothing Then
                txtDate_Transaction.Text = item.Date_Transaction
            End If
            If Not item.Teller Is Nothing Then
                txtTeller.Text = item.Teller
            End If
            If Not item.Authorized Is Nothing Then
                txtAuthorized.Text = item.Authorized
            End If
            If Not item.Late_Deposit Is Nothing Then
                txtLate_Deposit.Text = item.Late_Deposit
            End If
            If Not item.Date_Posting Is Nothing Then
                txtDate_Posting.Text = item.Date_Posting
            End If
            If Not item.Transmode_Code Is Nothing Then
                If Not item.Transmode_Code = "" Then
                    Dim variable = UploadReportSTR.getTransmodecode(item.Transmode_Code)
                    If variable IsNot Nothing Then
                        txtTransmode_Code.Text = UploadReportSTR.getTransmodecode(item.Transmode_Code).Keterangan
                    End If
                End If
            End If
            'txtValue_Date.Text = item.Value_Date
            If Not item.Transmode_Comment Is Nothing Then
                txtTransmode_Comment.Text = item.Transmode_Comment
            End If
            If Not item.Amount_Local Is Nothing Then
                txtAmount_Local.Text = item.Amount_Local
            End If
            If Not item.isMyClient_FROM Is Nothing Then
                txtisMyClient_FROM.Text = item.isMyClient_FROM
            End If
            If Not item.From_Funds_Code Is Nothing Then
                If Not item.From_Funds_Code = "" Then
                    Dim variable = UploadReportSTR.getFundsCode(item.From_Funds_Code)
                    If Not variable Is Nothing Then
                        txtFrom_Funds_Code.Text = UploadReportSTR.getFundsCode(item.From_Funds_Code).Keterangan
                    End If
                End If
            End If
            If Not item.From_Funds_Comment Is Nothing Then
                txtFrom_Funds_Comment.Text = item.From_Funds_Comment
            End If
            If Not item.From_Foreign_Currency_Code Is Nothing Then
                If Not item.From_Foreign_Currency_Code = "" Then
                    Dim variable = UploadReportSTR.getMataUangCode(item.From_Foreign_Currency_Code)
                    If variable IsNot Nothing Then
                        txtFrom_Foreign_Currency_Code.Text = variable.Keterangan
                    End If
                End If
            End If
            If Not item.From_Foreign_Currency_Amount Is Nothing Then
                txtFrom_Foreign_Currency_Amount.Text = item.From_Foreign_Currency_Amount
            End If

            If Not item.From_Foreign_Currency_Exchange_Rate Is Nothing Then
                txtFrom_Foreign_Currency_Exchange_Rate.Text = item.From_Foreign_Currency_Exchange_Rate
            End If
            If Not item.From_Country Is Nothing Then
                If Not item.From_Country = "" Then
                    Dim variable = UploadReportSTR.getCountryCode(item.From_Country)
                    If variable IsNot Nothing Then

                        txtFrom_Country.Text = variable.Keterangan
                    End If
                End If
            End If
            If Not item.isMyClient_TO Is Nothing Then
                txtisMyClient_TO.Text = item.isMyClient_TO
            End If
            If Not item.To_Funds_Code Is Nothing Then
                If Not item.To_Funds_Code = "" Then
                    Dim variable = UploadReportSTR.getFundsCode(item.To_Funds_Code)
                    If variable IsNot Nothing Then
                        txtTo_Funds_Code.Text = UploadReportSTR.getFundsCode(item.To_Funds_Code).Keterangan
                    End If
                End If
            End If
            If Not item.From_Funds_Comment Is Nothing Then
                txtTo_Funds_Comment.Text = item.From_Funds_Comment
            End If
            If Not item.To_Foreign_Currency_Code Is Nothing Then
                If Not item.To_Foreign_Currency_Code = "" Then
                    Dim variable = UploadReportSTR.getMataUangCode(item.To_Foreign_Currency_Code)
                    If variable IsNot Nothing Then
                        txtTo_Foreign_Currency_Code.Text = variable.Keterangan
                    End If
                End If
            End If
            If Not item.To_Foreign_Currency_Amount Is Nothing Then
                txtTo_Foreign_Currency_Amount.Text = item.To_Foreign_Currency_Amount
            End If
            If Not item.To_Foreign_Currency_Exchange_Rate Is Nothing Then
                txtTo_Foreign_Currency_Exchange_Rate.Text = item.To_Foreign_Currency_Exchange_Rate
            End If
            If Not item.To_Country Is Nothing Then
                If Not item.To_Country = "" Then
                    Dim variable = UploadReportSTR.getCountryCode(item.To_Country)
                    If variable IsNot Nothing Then
                        txtTo_Country.Text = variable.Keterangan
                    End If
                End If
            End If
            If Not item.Comments Is Nothing Then
                txtComments.Text = item.Comments
            End If
            If Not item.isSwift Is Nothing Then
                txtisSwift.Text = item.isSwift
            End If
            If Not item.FK_Source_Data_ID Is Nothing Then
                If Not item.FK_Source_Data_ID = "" Then
                    Dim variable = UploadReportSTR.getSourceData(item.FK_Source_Data_ID)
                    If variable IsNot Nothing Then
                        txtFK_Source_Data_ID.Text = variable.Description
                    End If
                End If
            End If
            If Not item.FROM_Account_No Is Nothing Then
                txtFROM_Account_No.Text = item.FROM_Account_No
            End If
            If Not item.FROM_CIFNO Is Nothing Then
                txtFROM_CIFNO.Text = item.FROM_CIFNO
            End If
            If Not item.From_WIC_NO Is Nothing Then
                txtFrom_WIC_NO.Text = item.From_WIC_NO
            End If
            If Not item.TO_ACCOUNT_NO Is Nothing Then
                txtTO_ACCOUNT_NO.Text = item.TO_ACCOUNT_NO
            End If
            If Not item.TO_CIF_NO Is Nothing Then
                txtTO_CIF_NO.Text = item.TO_CIF_NO
            End If
            If Not item.TO_WIC_NO Is Nothing Then
                txtTO_WIC_NO.Text = item.TO_WIC_NO
            End If
            If Not item.Condutor_ID Is Nothing Then
                txtCondutor_ID.Text = item.Condutor_ID

                txtIsUsedConductor.Text = "True"
            Else
                txtIsUsedConductor.Text = "False"
            End If

            If Not item.Swift_Code_Lawan Is Nothing Then 'HSBC 20201117
                txtSwiftCodeLawan.Value = item.Swift_Code_Lawan
            End If
            'Vicky Himawan Added 19 Mar 2021 (Load kode PJK Lawan pada detail setelah upload)
            If Not item.Kode_PJK_Lawan Is Nothing Then
                txtKodePJKLawan.Value = item.Kode_PJK_Lawan 'vicky add PJK_Lawan (Menambahkan PJK Lawan)
            End If
            'Vicky Himawan Added 19 Mar 2021 (Load Nama PJK Lawan pada detail setelah upload)
            If Not item.Swift_Code_Lawan Is Nothing Then 'HSBC 20201117
                txtNamaPJKLawan.Value = item.Nama_PJK_Lawan 'vicky add PJK_Lawan (Menambahkan PJK Lawan)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Sub LoadPanelTransactionMultipartyDetail(id As Long)
        Try

            windowDetailTransactionMultiparty.Hidden = False
            Dim item = UploadReportSTR.getDetailTransactionMultiparty(id)
            If Not item.Case_ID Is Nothing Then
                MultitxtCaseID.Text = item.Case_ID
            End If
            If Not item.TransactionNumber Is Nothing Then
                txtMultiTransactionNumber.Text = item.TransactionNumber
            End If
            If Not item.Internal_Ref_Number Is Nothing Then
                txtMultiInternal_Ref_Number.Text = item.Internal_Ref_Number
            End If
            If Not item.Transaction_Location Is Nothing Then
                txtMultiTransaction_Location.Text = item.Transaction_Location
            End If
            If Not item.Transaction_Remark Is Nothing Then
                txtMultiTransaction_Remark.Text = item.Transaction_Remark
            End If
            If Not item.Date_Transaction Is Nothing Then
                txtMultiDate_Transaction.Text = item.Date_Transaction
            End If
            If Not item.Teller Is Nothing Then
                txtMultiTeller.Text = item.Teller
            End If
            If Not item.Authorized Is Nothing Then
                txtMultiAuthorized.Text = item.Authorized
            End If
            If Not item.Late_Deposit Is Nothing Then
                txtMultiLate_Deposit.Text = item.Late_Deposit
            End If
            If Not item.Date_Posting Is Nothing Then
                txtMultiDate_Posting.Text = item.Date_Posting
            End If
            If Not item.Transmode_Code Is Nothing Then
                If Not item.Transmode_Code = "" Then
                    Dim variable = UploadReportSTR.getTransmodecode(item.Transmode_Code)
                    If variable IsNot Nothing Then
                        txtMultiTransmode_Code.Text = UploadReportSTR.getTransmodecode(item.Transmode_Code).Keterangan
                    End If
                End If
            End If
            If Not item.Transmode_Comment Is Nothing Then
                txtMultiTransmode_Comment.Text = item.Transmode_Comment
            End If
            If Not item.Amount_Local Is Nothing Then
                txtMultiAmount_Local.Text = item.Amount_Local
            End If
            If Not item.isMyClient Is Nothing Then
                txtMultiisMyClient.Text = item.isMyClient
            End If
            If Not item.Funds_Code Is Nothing Then
                If Not item.Funds_Code = "" Then
                    Dim variable = UploadReportSTR.getFundsCode(item.Funds_Code)
                    If variable IsNot Nothing Then
                        txtMultiFunds_Code.Text = UploadReportSTR.getFundsCode(item.Funds_Code).Keterangan
                    End If
                End If
            End If
            If Not item.Funds_Comment Is Nothing Then
                txtMultiFunds_Comment.Text = item.Funds_Comment
            End If
            If Not item.Foreign_Currency_Code Is Nothing Then
                If Not item.Foreign_Currency_Code = "" Then
                    Dim variable = UploadReportSTR.getMataUang(item.Foreign_Currency_Code)
                    If variable IsNot Nothing Then
                        txtMultiForeign_Currency_Code.Text = UploadReportSTR.getMataUang(item.Foreign_Currency_Code).Keterangan
                    End If
                End If
            End If
            If Not item.Foreign_Currency_Amount Is Nothing Then
                txtMultiForeign_Currency_Amount.Text = item.Foreign_Currency_Amount
            End If
            If Not item.Foreign_Currency_Exchange_Rate Is Nothing Then
                txtMultiForeign_Currency_Exchange_Rate.Text = item.Foreign_Currency_Exchange_Rate
            End If
            If Not item.Country Is Nothing Then
                If Not item.Country = "" Then
                    Dim variable = UploadReportSTR.getNamaNegara(item.Country)
                    If variable IsNot Nothing Then
                        txtMultiCountry.Text = UploadReportSTR.getNamaNegara(item.Country).Keterangan
                    End If
                End If
            End If
            If Not item.Comments Is Nothing Then
                txtMultiComments.Text = item.Comments
            End If
            If Not item.isSwift Is Nothing Then
                txtMultiisSwift.Text = item.isSwift
            End If
            If Not item.FK_Source_Data_ID Is Nothing Then
                If Not item.FK_Source_Data_ID = "" Then
                    Dim variable = UploadReportSTR.getSourceData(item.FK_Source_Data_ID)
                    txtMultiFK_Source_Data_ID.Text = UploadReportSTR.getSourceData(item.FK_Source_Data_ID).Description
                End If
            End If
            If Not item.Account_No Is Nothing Then
                txtMultiAccount_No.Text = item.Account_No
            End If
            If Not item.CIFNO Is Nothing Then
                txtMultiCIFNO.Text = item.CIFNO
            End If
            If Not item.WIC_NO Is Nothing Then
                txtMultiWIC_NO.Text = item.WIC_NO
            End If
            If Not item.Condutor_ID Is Nothing Then
                txtMultiCondutor_ID.Text = item.IsUsedConductor
                txtMultiIsUsedConductor.Text = "True"
            Else
                txtMultiIsUsedConductor.Text = "False"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub Button_DownloadTemplate(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
            Dim objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\TempReportSTR\" & tempfilexls))

            Dim objReport As Data.DataTable = New Data.DataTable()
            objReport.Columns.Add(New Data.DataColumn("Case ID", GetType(String)))
            objReport.Columns.Add(New Data.DataColumn("Tanggal Laporan", GetType(Date)))
            objReport.Columns.Add(New Data.DataColumn("Jenis Laporan", GetType(String)))
            objReport.Columns.Add(New Data.DataColumn("No Ref Laporan", GetType(String)))
            objReport.Columns.Add(New Data.DataColumn("No Ref PPATK", GetType(String))) 'Danamon Add New Column (No Ref PPATK)
            objReport.Columns.Add(New Data.DataColumn("Alasan", GetType(String)))
            objReport.Columns.Add(New Data.DataColumn("Tindakan Pelapor", GetType(String)))
            objReport.Columns.Add(New Data.DataColumn("Catatan", GetType(String)))

            Dim actAccount As Data.DataTable = New Data.DataTable()
            actAccount.Columns.Add(New Data.DataColumn("Case ID", GetType(String)))
            actAccount.Columns.Add(New Data.DataColumn("Signifikansi", GetType(Date)))
            actAccount.Columns.Add(New Data.DataColumn("Alasan", GetType(String)))
            actAccount.Columns.Add(New Data.DataColumn("Catatan Report Party", GetType(String)))
            actAccount.Columns.Add(New Data.DataColumn("Account No", GetType(String)))
            actAccount.Columns.Add(New Data.DataColumn("Nama PJK", GetType(String)))
            actAccount.Columns.Add(New Data.DataColumn("Kode PJK", GetType(String)))
            actAccount.Columns.Add(New Data.DataColumn("Kode SWIFT", GetType(String)))

            Dim actPerson As Data.DataTable = New Data.DataTable()
            actPerson.Columns.Add(New Data.DataColumn("Case ID", GetType(String)))
            actPerson.Columns.Add(New Data.DataColumn("Signifikansi", GetType(Date)))
            actPerson.Columns.Add(New Data.DataColumn("Alasan", GetType(String)))
            actPerson.Columns.Add(New Data.DataColumn("Catatan Report Party", GetType(String)))
            actPerson.Columns.Add(New Data.DataColumn("Last Name", GetType(String)))
            actPerson.Columns.Add(New Data.DataColumn("SSN", GetType(String)))
            actPerson.Columns.Add(New Data.DataColumn("Passport Number", GetType(String)))
            actPerson.Columns.Add(New Data.DataColumn("ID Number", GetType(String)))

            Dim actEntity As Data.DataTable = New Data.DataTable()
            actEntity.Columns.Add(New Data.DataColumn("Case ID", GetType(String)))
            actEntity.Columns.Add(New Data.DataColumn("Signifikansi", GetType(Date)))
            actEntity.Columns.Add(New Data.DataColumn("Alasan", GetType(String)))
            actEntity.Columns.Add(New Data.DataColumn("Catatan Report Party", GetType(String)))
            actEntity.Columns.Add(New Data.DataColumn("Name", GetType(String)))
            actEntity.Columns.Add(New Data.DataColumn("Business", GetType(String)))


            Dim objindikatorlaporan As Data.DataTable = New Data.DataTable()
            objindikatorlaporan.Columns.Add(New Data.DataColumn("Case ID", GetType(String)))
            objindikatorlaporan.Columns.Add(New Data.DataColumn("Indikator Laporan", GetType(String)))

            Dim Biparty As Data.DataTable = New Data.DataTable()
            Biparty.Columns.Add(New Data.DataColumn("Case ID", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Date Transaction", GetType(Date)))
            'Biparty.Columns.Add(New Data.DataColumn("Ref Num", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Amount Local", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Source Data", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Transmode_Code", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Transmode Comment", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Transaction Remark", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Transaction Number", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Transaction Location", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Teller", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Authorized", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Late Deposit", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Date Posting", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Sender From", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("isMyClient_FROM", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("From_Funds_Code", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("From_Funds_Comment", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("From_Foreign_Currency_Code", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("From_Foreign_Currency_Amount", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("From_Foreign_Currency_Exchange_Rate", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("From_Country", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("sender To", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("isMyClient_TO", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("To_Funds_Code", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("To_Funds_Comment", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("To_Foreign_Currency_Code", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("To_Foreign_Currency_Amount", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("To_Foreign_Currency_Exchange_Rate", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("To_Country", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Comments", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("isSWIFT", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("From_Account_No", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("From_CIF_No", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("From_WIC_No", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("To_Account_No", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("To_CIF_No", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("To_WIC_No", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Conductor_ID", GetType(String)))
            Biparty.Columns.Add(New Data.DataColumn("Swift_Code_Lawan", GetType(String))) 'HSBC 20201117 Add Swift Code Lawan
            Biparty.Columns.Add(New Data.DataColumn("Kode_PJK_Lawan", GetType(String))) 'vicky add PJK_Lawan (Menambahkan PJK Lawan)
            Biparty.Columns.Add(New Data.DataColumn("Nama_PJK_Lawan", GetType(String))) 'vicky add PJK_Lawan (Menambahkan PJK Lawan)


            Dim Multiparty As Data.DataTable = New Data.DataTable()
            Multiparty.Columns.Add(New Data.DataColumn("Case ID", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Date Transaction", GetType(Date)))
            'Multiparty.Columns.Add(New Data.DataColumn("Ref Num", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Amount Local", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Source Data", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Transmode_Code", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Transmode Comment", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Transaction Remark", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Transaction Number", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Transaction Location", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Teller", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Authorized", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Late Deposit", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Date Posting", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Peran", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Signifikasi", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Sender", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("isMyClient", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Funds_Code", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Funds_Comment", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Foreign_Currency_Code", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Foreign_Currency_Amount", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Foreign_Currency_Exchange_Rate", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Country", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Comments", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("isSWIFT", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Account_No", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("CIF_No", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("WIC_No", GetType(String)))
            Multiparty.Columns.Add(New Data.DataColumn("Conductor ID", GetType(String)))


            Using resource As New ExcelPackage(objfileinfo)
                Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("Report")
                Dim ws2 As ExcelWorksheet = resource.Workbook.Worksheets.Add("Indikator Laporan")
                Dim ws3 As ExcelWorksheet = resource.Workbook.Worksheets.Add("Transaction Biparty")
                Dim ws4 As ExcelWorksheet = resource.Workbook.Worksheets.Add("Transaction Multiparty")
                Dim ws5 As ExcelWorksheet = resource.Workbook.Worksheets.Add("Activity Account")
                Dim ws7 As ExcelWorksheet = resource.Workbook.Worksheets.Add("Activity Person")
                Dim ws8 As ExcelWorksheet = resource.Workbook.Worksheets.Add("Activity Entity")
                Dim ws6 As ExcelWorksheet = resource.Workbook.Worksheets.Add("Parameter")

                ws.Cells("A1").LoadFromDataTable(objReport, True)
                ws2.Cells("A1").LoadFromDataTable(objindikatorlaporan, True)
                ws3.Cells("A1").LoadFromDataTable(Biparty, True)
                ws4.Cells("A1").LoadFromDataTable(Multiparty, True)
                ws5.Cells("A1").LoadFromDataTable(actAccount, True)
                ws7.Cells("A1").LoadFromDataTable(actPerson, True)
                ws8.Cells("A1").LoadFromDataTable(actEntity, True)




                Dim Length As String = ""

                Length = "C2:C" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Report2 As DataValidation.ExcelDataValidationList = ws.DataValidations.AddListValidation(Length)
                Dim lengthjenislaporan = UploadReportSTR.getlistJenisLaporan.Count
                Report2.Formula.ExcelFormula = "=Parameter!$A$2:$A$" & lengthjenislaporan + 1



                Length = "B2:B" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Indikator2 As DataValidation.ExcelDataValidationList = ws2.DataValidations.AddListValidation(Length)
                Dim lengthindikatorlaporan = UploadReportSTR.getlistindikatorlaporan.Count
                Indikator2.Formula.ExcelFormula = "=Parameter!$K$2:$K$" & lengthindikatorlaporan + 1

                Length = "D2:D" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim lengthsourcedata = UploadReportSTR.getlistSourceData.Count
                Dim Biparty1 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Biparty1.Formula.ExcelFormula = "=Parameter!$E$2:$E$" & lengthsourcedata + 1

                Length = "E2:E" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim lengthjenistransaksi = UploadReportSTR.getlistJenisTransaction.Count
                Dim Biparty2 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Biparty2.Formula.ExcelFormula = "=Parameter!$B$2:$B$" & lengthjenistransaksi + 1

                Length = "L2:L" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim lengthlatedeposit = 2
                Dim Biparty3 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Biparty3.Formula.ExcelFormula = "=Parameter!$C$2:$C$" & lengthlatedeposit + 1

                Length = "N2:N" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim subnotetype = 3
                Dim Biparty4 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Biparty4.Formula.ExcelFormula = "=Parameter!$D$2:$D$" & subnotetype + 1

                Length = "O2:O" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim ismyclientcount = 2
                Dim Biparty5 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Biparty5.Formula.ExcelFormula = "=Parameter!$G$2:$G$" & ismyclientcount + 1

                Length = "P2:P" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim fundcode = UploadReportSTR.getlistinstrumentransaksi.Count
                Dim Biparty6 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Biparty6.Formula.ExcelFormula = "=Parameter!$H$2:$H$" & fundcode + 1

                Length = "R2:R" & ((Int16.MaxValue) - 1).ToString
                Dim Biparty7 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Dim currencycodes = UploadReportSTR.getlistMappingmatauang().Count
                Biparty7.Formula.ExcelFormula = "=Parameter!$J$2:$J$" & currencycodes + 1

                Length = "U2:U" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim country = UploadReportSTR.getlistcountry.Count
                Dim Biparty8 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Biparty8.Formula.ExcelFormula = "=Parameter!$I$2:$I$" & country + 1

                Length = "V2:V" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Biparty9 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Biparty9.Formula.ExcelFormula = "=Parameter!$D$2:$D$" & subnotetype + 1

                Length = "W2:W" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Biparty10 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Biparty10.Formula.ExcelFormula = "=Parameter!$G$2:$G$" & ismyclientcount + 1

                Length = "X2:X" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Biparty11 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Biparty11.Formula.ExcelFormula = "=Parameter!$H$2:$H$" & fundcode + 1

                Length = "Z2:Z" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Biparty12 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Biparty12.Formula.ExcelFormula = "=Parameter!$J$2:$J$" & currencycodes + 1

                Length = "AC2:AC" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Biparty13 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Biparty13.Formula.ExcelFormula = "=Parameter!$I$2:$I$" & country + 1

                Length = "AE2:AE" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Biparty14 As DataValidation.ExcelDataValidationList = ws3.DataValidations.AddListValidation(Length)
                Biparty14.Formula.ExcelFormula = "=Parameter!$G$2:$G$" & ismyclientcount + 1


                Length = "D2:D" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Multiparty1 As DataValidation.ExcelDataValidationList = ws4.DataValidations.AddListValidation(Length)
                Multiparty1.Formula.ExcelFormula = "=Parameter!$E$2:$E$" & lengthsourcedata + 1

                Length = "E2:E" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Multiparty2 As DataValidation.ExcelDataValidationList = ws4.DataValidations.AddListValidation(Length)
                Multiparty2.Formula.ExcelFormula = "=Parameter!$B$2:$B$" & lengthjenistransaksi + 1

                Length = "L2:L" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Multiparty3 As DataValidation.ExcelDataValidationList = ws4.DataValidations.AddListValidation(Length)
                Multiparty3.Formula.ExcelFormula = "=Parameter!$C$2:$C$" & lengthlatedeposit + 1

                Length = "N2:N" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim listpartyrole = UploadReportSTR.getlistpartyrole.Count
                Dim Multiparty14 As DataValidation.ExcelDataValidationList = ws4.DataValidations.AddListValidation(Length)
                Multiparty14.Formula.ExcelFormula = "=Parameter!$F$2:$F$" & listpartyrole + 1


                Length = "P2:P" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Multiparty4 As DataValidation.ExcelDataValidationList = ws4.DataValidations.AddListValidation(Length)
                Multiparty4.Formula.ExcelFormula = "=Parameter!$D$2:$D$" & subnotetype + 1

                Length = "Q2:Q" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Multiparty5 As DataValidation.ExcelDataValidationList = ws4.DataValidations.AddListValidation(Length)
                Multiparty5.Formula.ExcelFormula = "=Parameter!$G$2:$G$" & ismyclientcount + 1

                Length = "R2:R" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Multiparty6 As DataValidation.ExcelDataValidationList = ws4.DataValidations.AddListValidation(Length)
                Multiparty6.Formula.ExcelFormula = "=Parameter!$H$2:$H$" & fundcode + 1

                Length = "T2:T" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Multiparty7 As DataValidation.ExcelDataValidationList = ws4.DataValidations.AddListValidation(Length)
                Multiparty7.Formula.ExcelFormula = "=Parameter!$J$2:$J$" & currencycodes + 1

                Length = "W2:W" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Multiparty8 As DataValidation.ExcelDataValidationList = ws4.DataValidations.AddListValidation(Length)
                Multiparty8.Formula.ExcelFormula = "=Parameter!$I$2:$I$" & country + 1

                Length = "Y2:Y" & ((Int16.MaxValue) - 1).ToString
                ''Pilihan Action
                Dim Multiparty9 As DataValidation.ExcelDataValidationList = ws4.DataValidations.AddListValidation(Length)
                Multiparty9.Formula.ExcelFormula = "=Parameter!$G$2:$G$" & ismyclientcount + 1


                If UploadReportSTR.getlistJenisLaporan() IsNot Nothing Then
                    Dim i = 1
                    ws6.Cells("A1").Value = "Jenis Laporan"
                    For Each item In UploadReportSTR.getlistJenisLaporan()
                        i += 1
                        ws6.Cells("A" & i).Value = "(" & item.Kode & ") " & item.Keterangan
                    Next
                End If

                If UploadReportSTR.getlistJenisTransaction() IsNot Nothing Then
                    Dim i = 1
                    ws6.Cells("B1").Value = "Transmode Code/Jenis Transaksi"
                    For Each item In UploadReportSTR.getlistJenisTransaction()
                        i += 1
                        ws6.Cells("B" & i).Value = "(" & item.Kode & ") " & item.Keterangan
                    Next
                End If
                ws6.Cells("C1").Value = "Late Deposit/Setoran Terlambat"
                ws6.Cells("C2").Value = "Ya"
                ws6.Cells("C3").Value = "Tidak"

                If UploadReportSTR.getlistSenderFromTo() IsNot Nothing Then
                    Dim i = 1
                    ws6.Cells("D1").Value = "Sender From/To"
                    For Each item In UploadReportSTR.getlistSenderFromTo()
                        i += 1
                        ws6.Cells("D" & i).Value = "(" & item.PK_Sender_To_Information & ") " & item.Sender_To_Information
                    Next
                End If

                If UploadReportSTR.getlistSourceData() IsNot Nothing Then
                    Dim i = 1
                    ws6.Cells("E1").Value = "Source Data"
                    For Each item In UploadReportSTR.getlistSourceData()
                        i += 1
                        ws6.Cells("E" & i).Value = "(" & item.PK_Source_Data_ID & ") " & item.Description
                    Next
                End If

                If UploadReportSTR.getlistpartyrole() IsNot Nothing Then
                    Dim i = 1
                    ws6.Cells("F1").Value = "Peran MultiParty"
                    For Each item In UploadReportSTR.getlistpartyrole()
                        i += 1
                        ws6.Cells("F" & i).Value = "(" & item.Kode & ") " & item.Keterangan
                    Next
                End If
                ws6.Cells("G1").Value = "isMyClient_From & isMyClient_To"
                ws6.Cells("G2").Value = "(0) No"
                ws6.Cells("G3").Value = "(1) Yes"


                If UploadReportSTR.getlistinstrumentransaksi() IsNot Nothing Then
                    Dim i = 1
                    ws6.Cells("H1").Value = "Funds Code/Instrumen Transaksi"
                    For Each item In UploadReportSTR.getlistinstrumentransaksi()
                        i += 1
                        ws6.Cells("H" & i).Value = "(" & item.Kode & ") " & item.Keterangan
                    Next
                End If

                If UploadReportSTR.getlistcountry() IsNot Nothing Then
                    Dim i = 1
                    ws6.Cells("I1").Value = "Country/Negara"
                    For Each item In UploadReportSTR.getlistcountry()
                        i += 1
                        ws6.Cells("I" & i).Value = item.Kode
                    Next
                End If

                If UploadReportSTR.getlistMappingmatauang IsNot Nothing Then
                    Dim i = 1
                    ws6.Cells("J1").Value = "Currency Code/Mata Uang"
                    For Each item In UploadReportSTR.getlistMappingmatauang
                        i += 1
                        ws6.Cells("J" & i).Value = item.Kode_Internal
                    Next
                End If

                If UploadReportSTR.getlistindikatorlaporan() IsNot Nothing Then
                    Dim i = 1
                    ws6.Cells("K1").Value = "Indikator Laporan"
                    For Each item In UploadReportSTR.getlistindikatorlaporan()
                        i += 1
                        ws6.Cells("K" & i).Value = "(" & item.Kode & ") " & item.Keterangan
                    Next
                End If

                Dim ValidationReport1 As DataValidation.ExcelDataValidationAny = ws.DataValidations.AddAnyValidation("A1")
                ValidationReport1.ShowInputMessage = True
                ValidationReport1.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationReport1.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nomor Case Management ID sesuai report yang ingin dilaporkan. Case ID bersifat Unik"

                Dim ValidationReport2 As DataValidation.ExcelDataValidationAny = ws.DataValidations.AddAnyValidation("B1")
                ValidationReport2.ShowInputMessage = True
                ValidationReport2.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationReport2.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan tanggal PJK mengetahui adanya unsur TKM"

                Dim ValidationReport3 As DataValidation.ExcelDataValidationAny = ws.DataValidations.AddAnyValidation("C1")
                ValidationReport3.ShowInputMessage = True
                ValidationReport3.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationReport3.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list jenis laporan"

                Dim ValidationReport4 As DataValidation.ExcelDataValidationAny = ws.DataValidations.AddAnyValidation("D1")
                ValidationReport4.ShowInputMessage = True
                ValidationReport4.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationReport4.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nomor referensi laporan internal PJK yang dapat mengidentifikasi laporan yang disampaikan"

                'Add new Catatan Excel
                Dim ValidationReport5 As DataValidation.ExcelDataValidationAny = ws.DataValidations.AddAnyValidation("E1")
                ValidationReport5.ShowInputMessage = True
                ValidationReport5.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationReport5.Prompt = (vbCrLf) & (vbCrLf) & "Jika jenis laporan di pilih adalah LTKMP, PITP, PDTP"

                Dim ValidationReport6 As DataValidation.ExcelDataValidationAny = ws.DataValidations.AddAnyValidation("F1")
                ValidationReport6.ShowInputMessage = True
                ValidationReport6.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationReport6.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan uraian latar belakang transaksi dan unsur TKM yang diidentifikasi"

                Dim ValidationReport7 As DataValidation.ExcelDataValidationAny = ws.DataValidations.AddAnyValidation("G1")
                ValidationReport7.ShowInputMessage = True
                ValidationReport7.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationReport7.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan tindakan yang sudah dilakukan oleh PJK untuk transaksi yang ditetapkan sbg TKM"

                Dim ValidationReport8 As DataValidation.ExcelDataValidationAny = ws.DataValidations.AddAnyValidation("H1")
                ValidationReport8.ShowInputMessage = True
                ValidationReport8.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationReport8.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan informasi tambahan terkait report yang tidak memiliki field pengisian"

                Dim ValidationIndikator1 As DataValidation.ExcelDataValidationAny = ws2.DataValidations.AddAnyValidation("A1")
                ValidationIndikator1.ShowInputMessage = True
                ValidationIndikator1.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationIndikator1.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nomor Case Management ID sesuai report yang ingin dilaporkan. Case ID bersifat Unik"

                Dim ValidationIndikator2 As DataValidation.ExcelDataValidationAny = ws2.DataValidations.AddAnyValidation("B1")
                ValidationIndikator2.ShowInputMessage = True
                ValidationIndikator2.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationIndikator2.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list indikator laporan yang menjadi penyebab transaksi dilaporkan sbg TKM"

                Dim ValidationActivityAccount1 As DataValidation.ExcelDataValidationAny = ws5.DataValidations.AddAnyValidation("A1")
                ValidationActivityAccount1.ShowInputMessage = True
                ValidationActivityAccount1.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationActivityAccount1.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nomor Case Management ID sesuai report yang ingin dilaporkan. Case ID bersifat Unik"

                Dim ValidationActivityAccount2 As DataValidation.ExcelDataValidationAny = ws5.DataValidations.AddAnyValidation("B1")
                ValidationActivityAccount2.ShowInputMessage = True
                ValidationActivityAccount2.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationActivityAccount2.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan seberapa besar pihak ini terkait dengan aktivitas yang dilakukan"

                Dim ValidationActivityAccount3 As DataValidation.ExcelDataValidationAny = ws5.DataValidations.AddAnyValidation("C1")
                ValidationActivityAccount3.ShowInputMessage = True
                ValidationActivityAccount3.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationActivityAccount3.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan penjelasan hubungan/peran pihak dengan aktivitas"

                Dim ValidationActivityAccount4 As DataValidation.ExcelDataValidationAny = ws5.DataValidations.AddAnyValidation("D1")
                ValidationActivityAccount4.ShowInputMessage = True
                ValidationActivityAccount4.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationActivityAccount4.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan informasi penting terkait aktivitas apabila informasi tersebut tidak memiliki field pengisian"


                Dim ValidationActivityAccount6 As DataValidation.ExcelDataValidationAny = ws5.DataValidations.AddAnyValidation("E1")
                ValidationActivityAccount6.ShowInputMessage = True
                ValidationActivityAccount6.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationActivityAccount6.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan Account Number"

                Dim ValidationActivityAccount7 As DataValidation.ExcelDataValidationAny = ws5.DataValidations.AddAnyValidation("F1")
                ValidationActivityAccount7.ShowInputMessage = True
                ValidationActivityAccount7.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationActivityAccount7.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan Nama PJK tempat rekening dibuka"

                Dim ValidationActivityAccount8 As DataValidation.ExcelDataValidationAny = ws5.DataValidations.AddAnyValidation("G1")
                ValidationActivityAccount8.ShowInputMessage = True
                ValidationActivityAccount8.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL" 'Danamon change to OPTIONAL
                ValidationActivityAccount8.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan Kode PJK" & (vbCrLf) & "NOTE:" & (vbCrLf) & "Wajib diisi dengan memilih salah satu, Kode SWIFT atau Kode PJK "

                Dim ValidationActivityAccount9 As DataValidation.ExcelDataValidationAny = ws5.DataValidations.AddAnyValidation("H1")
                ValidationActivityAccount9.ShowInputMessage = True
                ValidationActivityAccount9.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL" 'Danamon change to OPTIONAL
                ValidationActivityAccount9.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan Kode SWIFT" & (vbCrLf) & "NOTE:" & (vbCrLf) & "Wajib diisi dengan memilih salah satu, Kode SWIFT atau Kode PJK "


                Dim ValidationActivityPerson1 As DataValidation.ExcelDataValidationAny = ws7.DataValidations.AddAnyValidation("A1")
                ValidationActivityPerson1.ShowInputMessage = True
                ValidationActivityPerson1.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationActivityPerson1.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nomor Case Management ID sesuai report yang ingin dilaporkan. Case ID bersifat Unik"

                Dim ValidationActivityPerson2 As DataValidation.ExcelDataValidationAny = ws7.DataValidations.AddAnyValidation("B1")
                ValidationActivityPerson2.ShowInputMessage = True
                ValidationActivityPerson2.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationActivityPerson2.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan seberapa besar pihak ini terkait dengan aktivitas yang dilakukan"

                Dim ValidationActivityPerson3 As DataValidation.ExcelDataValidationAny = ws7.DataValidations.AddAnyValidation("C1")
                ValidationActivityPerson3.ShowInputMessage = True
                ValidationActivityPerson3.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationActivityPerson3.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan penjelasan hubungan/peran pihak dengan aktivitas"

                Dim ValidationActivityPerson4 As DataValidation.ExcelDataValidationAny = ws7.DataValidations.AddAnyValidation("D1")
                ValidationActivityPerson4.ShowInputMessage = True
                ValidationActivityPerson4.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationActivityPerson4.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan informasi penting terkait aktivitas apabila informasi tersebut tidak memiliki field pengisian"

                Dim ValidationActivityPerson5 As DataValidation.ExcelDataValidationAny = ws7.DataValidations.AddAnyValidation("E1")
                ValidationActivityPerson4.ShowInputMessage = True
                ValidationActivityPerson4.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationActivityPerson4.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan Nama Lengkap"

                Dim ValidationActivityPerson6 As DataValidation.ExcelDataValidationAny = ws7.DataValidations.AddAnyValidation("F1")
                ValidationActivityPerson6.ShowInputMessage = True
                ValidationActivityPerson6.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationActivityPerson6.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan Nomor KTP"

                Dim ValidationActivityPerson7 As DataValidation.ExcelDataValidationAny = ws7.DataValidations.AddAnyValidation("G1")
                ValidationActivityPerson7.ShowInputMessage = True
                ValidationActivityPerson7.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationActivityPerson7.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan Nomor Pasport"

                Dim ValidationActivityPerson8 As DataValidation.ExcelDataValidationAny = ws7.DataValidations.AddAnyValidation("H1")
                ValidationActivityPerson7.ShowInputMessage = True
                ValidationActivityPerson7.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationActivityPerson7.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan Nomor Identitas Lain selain SSN dan Nomor Pasport"

                Dim ValidationActivityEntity1 As DataValidation.ExcelDataValidationAny = ws8.DataValidations.AddAnyValidation("A1")
                ValidationActivityEntity1.ShowInputMessage = True
                ValidationActivityEntity1.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationActivityEntity1.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nomor Case Management ID sesuai report yang ingin dilaporkan. Case ID bersifat Unik"

                Dim ValidationActivityEntity2 As DataValidation.ExcelDataValidationAny = ws8.DataValidations.AddAnyValidation("B1")
                ValidationActivityEntity2.ShowInputMessage = True
                ValidationActivityEntity2.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationActivityEntity2.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan seberapa besar pihak ini terkait dengan aktivitas yang dilakukan"

                Dim ValidationActivityEntity3 As DataValidation.ExcelDataValidationAny = ws8.DataValidations.AddAnyValidation("C1")
                ValidationActivityEntity3.ShowInputMessage = True
                ValidationActivityEntity3.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationActivityEntity3.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan penjelasan hubungan/peran pihak dengan aktivitas"

                Dim ValidationActivityEntity4 As DataValidation.ExcelDataValidationAny = ws8.DataValidations.AddAnyValidation("D1")
                ValidationActivityEntity4.ShowInputMessage = True
                ValidationActivityEntity4.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                ValidationActivityEntity4.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan informasi penting terkait aktivitas apabila informasi tersebut tidak memiliki field pengisian"

                Dim ValidationActivityEntity5 As DataValidation.ExcelDataValidationAny = ws8.DataValidations.AddAnyValidation("E1")
                ValidationActivityEntity5.ShowInputMessage = True
                ValidationActivityEntity5.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                ValidationActivityEntity5.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan Nama Korporasi"

                Dim ValidationActivityEntity6 As DataValidation.ExcelDataValidationAny = ws8.DataValidations.AddAnyValidation("F1")
                ValidationActivityEntity6.ShowInputMessage = True
                ValidationActivityEntity6.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL" 'Danamon change to OPTIONAL
                ValidationActivityEntity6.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan Bidang Usaha"

                Dim Validationbiparty1 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("A1")
                Validationbiparty1.ShowInputMessage = True
                Validationbiparty1.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty1.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nomor Case Management ID sesuai report yang ingin dilaporkan. Case ID bersifat Unik"

                Dim Validationbiparty2 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("B1")
                Validationbiparty2.ShowInputMessage = True
                Validationbiparty2.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty2.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan tanggal transaksi yg memicu terjadinya kecurigaan atau yg memenuhi indikator TKM"

                'Dim Validationbiparty3 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("C1")
                'Validationbiparty3.ShowInputMessage = True
                'Validationbiparty3.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                'Validationbiparty3.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nomor referensi transaksi internal PJK yang dapat mengidentifikasi transaksi yang disampaikan"

                Dim Validationbiparty4 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("C1")
                Validationbiparty4.ShowInputMessage = True
                Validationbiparty4.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty4.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nilai transaksi dalam bentuk IDR"

                Dim Validationbiparty5 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("D1")
                Validationbiparty5.ShowInputMessage = True
                Validationbiparty5.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty5.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan pilihan kode source data"

                Dim Validationbiparty6 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("E1")
                Validationbiparty6.ShowInputMessage = True
                Validationbiparty6.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty6.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list cara transaksi dilakukan"

                Dim Validationbiparty7 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("F1")
                Validationbiparty7.ShowInputMessage = True
                Validationbiparty7.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationbiparty7.Prompt = (vbCrLf) & (vbCrLf) & "Menjadi MANDATORY apabila transmode_code /cara transaksi dilakukan diisi dengan 'Lainnya'"

                Dim Validationbiparty8 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("G1")
                Validationbiparty8.ShowInputMessage = True
                Validationbiparty8.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationbiparty8.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan berita, keterangan atau tujuan transaksi pada saat melakukan transaksi"

                Dim Validationbiparty9 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("H1")
                Validationbiparty9.ShowInputMessage = True
                Validationbiparty9.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty9.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nomor pencatatan di internal PJK yang dapat mengidentifikasi transaksi"

                Dim Validationbiparty10 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("I1")
                Validationbiparty10.ShowInputMessage = True
                Validationbiparty10.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationbiparty10.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nama lokasi/tempat terjadinya transaksi dengan format:'nama kantor-kabupaten/kota-provinsi'"

                Dim Validationbiparty11 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("J1")
                Validationbiparty11.ShowInputMessage = True
                Validationbiparty11.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationbiparty11.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nama teller/petugas front office yang berhubungan dengan Pengguna Jasa untuk transaksi yang memicu kecurigaan"

                Dim Validationbiparty12 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("K1")
                Validationbiparty12.ShowInputMessage = True
                Validationbiparty12.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationbiparty12.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nama pejabat pengotorisasi transaksi yang memicu terjadinya kecurigaan"

                Dim Validationbiparty13 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("L1")
                Validationbiparty13.ShowInputMessage = True
                Validationbiparty13.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationbiparty13.Prompt = (vbCrLf) & (vbCrLf) & "Jika tanggal setoran berbeda dgn tanggal pembukuan, maka PJK memilih 'Ya'"

                Dim Validationbiparty14 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("M1")
                Validationbiparty14.ShowInputMessage = True
                Validationbiparty14.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty14.Prompt = (vbCrLf) & (vbCrLf) & "Jika Late Deposit dipilih 'Ya', apabilah dipilih 'Tidak' maka Date Posting tidak diisi yang artinya OPTIONAL"

                Dim Validationbiparty15 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("N1")
                Validationbiparty15.ShowInputMessage = True
                Validationbiparty15.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty15.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list (From)"

                Dim Validationbiparty39 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("O1")
                Validationbiparty39.ShowInputMessage = True
                Validationbiparty39.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty39.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list (From)"

                Dim Validationbiparty16 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("P1")
                Validationbiparty16.ShowInputMessage = True
                Validationbiparty16.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty16.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list instrumen transaksi asal"

                Dim Validationbiparty17 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("Q1")
                Validationbiparty17.ShowInputMessage = True
                Validationbiparty17.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationbiparty17.Prompt = (vbCrLf) & (vbCrLf) & "Menjadi MANDATORY apabila from_funds_code /instrumen transaksi asal diisi dengan 'Lainnya'"

                Dim Validationbiparty18 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("R1")
                Validationbiparty18.ShowInputMessage = True
                Validationbiparty18.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY (apabila transaksi asal dilakukan dalam mata uang asing)"
                Validationbiparty18.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list mata uang asing"

                Dim Validationbiparty19 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("S1")
                Validationbiparty19.ShowInputMessage = True
                Validationbiparty19.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY (apabila transaksi asal dilakukan dalam mata uang asing)"
                Validationbiparty19.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nominal transaksi dalam mata uang asing"

                Dim Validationbiparty20 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("T1")
                Validationbiparty20.ShowInputMessage = True
                Validationbiparty20.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY (apabila transaksi asal dilakukan dalam mata uang asing)"
                Validationbiparty20.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nilai kurs yang digunakan pada saat transaksi"

                Dim Validationbiparty21 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("U1")
                Validationbiparty21.ShowInputMessage = True
                Validationbiparty21.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty21.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list nama negara transaksi asal"

                Dim Validationbiparty22 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("V1")
                Validationbiparty22.ShowInputMessage = True
                Validationbiparty22.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty22.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list (To)"

                Dim Validationbiparty38 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("W1")
                Validationbiparty38.ShowInputMessage = True
                Validationbiparty38.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty38.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list (To)"

                Dim Validationbiparty23 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("X1")
                Validationbiparty23.ShowInputMessage = True
                Validationbiparty23.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty23.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list instrumen transaksi Tujuan"

                Dim Validationbiparty24 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("Y1")
                Validationbiparty24.ShowInputMessage = True
                Validationbiparty24.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationbiparty24.Prompt = (vbCrLf) & (vbCrLf) & "Menjadi MANDATORY apabila to_funds_code /instrumen transaksi asal diisi dengan 'Lainnya'"

                Dim Validationbiparty25 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("Z1")
                Validationbiparty25.ShowInputMessage = True
                Validationbiparty25.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY (apabila transaksi asal dilakukan dalam mata uang asing)"
                Validationbiparty25.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list mata uang asing"

                Dim Validationbiparty26 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AA1")
                Validationbiparty26.ShowInputMessage = True
                Validationbiparty26.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY (apabila transaksi asal dilakukan dalam mata uang asing)"
                Validationbiparty26.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nominal transaksi dalam mata uang asing"

                Dim Validationbiparty27 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AB1")
                Validationbiparty27.ShowInputMessage = True
                Validationbiparty27.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY (apabila transaksi asal dilakukan dalam mata uang asing)"
                Validationbiparty27.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nilai kurs yang digunakan pada saat transaksi"

                Dim Validationbiparty28 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AC1")
                Validationbiparty28.ShowInputMessage = True
                Validationbiparty28.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty28.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list nama negara transaksi tujuan"

                Dim Validationbiparty29 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AD1")
                Validationbiparty29.ShowInputMessage = True
                Validationbiparty29.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationbiparty29.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan informasi penting terkait transaksi apabila informasi tersebut tidak memiliki field pengisian"

                Dim Validationbiparty30 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AE1")
                Validationbiparty30.ShowInputMessage = True
                Validationbiparty30.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY (apabila transaksi yang dilaporkan adalah LTKL/IFTI)"
                Validationbiparty30.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan 'Ya' jika transaksi SWIFT dan 'Tidak' jika bukan SWIFT"

                Dim Validationbiparty31 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AF1")
                Validationbiparty31.ShowInputMessage = True
                Validationbiparty31.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty31.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan Account Number disisi From"

                Dim Validationbiparty32 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AG1")
                Validationbiparty32.ShowInputMessage = True
                Validationbiparty32.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty32.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan CIF disisi From"

                Dim Validationbiparty33 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AH1")
                Validationbiparty33.ShowInputMessage = True
                Validationbiparty33.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty33.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan WIC disisi From"

                Dim Validationbiparty34 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AI1")
                Validationbiparty34.ShowInputMessage = True
                Validationbiparty34.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty34.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan Account Number disisi To"

                Dim Validationbiparty35 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AJ1")
                Validationbiparty35.ShowInputMessage = True
                Validationbiparty35.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty35.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan CIF disisi To"

                Dim Validationbiparty36 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AK1")
                Validationbiparty36.ShowInputMessage = True
                Validationbiparty36.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationbiparty36.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan WIC disisi To"

                Dim Validationbiparty37 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AL1")
                Validationbiparty37.ShowInputMessage = True
                Validationbiparty37.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationbiparty37.Prompt = (vbCrLf) & (vbCrLf) & "Menjadi MANDATORY jika transaksi dilakukan oleh Conductor dan wajib input Conductor ID tersebut saat transaksi dilakukan"

                Dim Validationbiparty40 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AM1")  ' HSBC 20201117
                Validationbiparty40.ShowInputMessage = True
                Validationbiparty40.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationbiparty40.Prompt = (vbCrLf) & (vbCrLf) & "Menjadi MANDATORY jika transaksi isMyClient_From [No] atau isMyClient_To [No]"

                Dim Validationbiparty41 As DataValidation.ExcelDataValidationAny = ws3.DataValidations.AddAnyValidation("AN1")
                Validationbiparty41.ShowInputMessage = True
                Validationbiparty41.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationbiparty41.Prompt = (vbCrLf) & (vbCrLf) & "Menjadi MANDATORY jika Swift Code Lawan tidak di isi" 'vicky add PJK_Lawan (Menambahkan PJK Lawan)

                Dim Validationmultiparty1 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("A1")
                Validationmultiparty1.ShowInputMessage = True
                Validationmultiparty1.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty1.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nomor Case Management ID sesuai report yang ingin dilaporkan. Case ID bersifat Unik"

                Dim Validationmultiparty2 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("B1")
                Validationmultiparty2.ShowInputMessage = True
                Validationmultiparty2.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty2.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan tanggal transaksi yg memicu terjadinya kecurigaan atau yg memenuhi indikator TKM"

                'Dim Validationmultiparty3 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("C1")
                'Validationmultiparty3.ShowInputMessage = True
                'Validationmultiparty3.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                'Validationmultiparty3.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nomor referensi transaksi internal PJK yang dapat mengidentifikasi transaksi yang disampaikan"

                Dim Validationmultiparty4 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("C1")
                Validationmultiparty4.ShowInputMessage = True
                Validationmultiparty4.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty4.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nilai transaksi dalam bentuk IDR"

                Dim Validationmultiparty5 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("D1")
                Validationmultiparty5.ShowInputMessage = True
                Validationmultiparty5.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty5.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan pilihan kode source data"

                Dim Validationmultiparty6 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("E1")
                Validationmultiparty6.ShowInputMessage = True
                Validationmultiparty6.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty6.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list cara transaksi dilakukan"

                Dim Validationmultiparty7 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("F1")
                Validationmultiparty7.ShowInputMessage = True
                Validationmultiparty7.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationmultiparty7.Prompt = (vbCrLf) & (vbCrLf) & "Menjadi MANDATORY apabila transmode_code /cara transaksi dilakukan diisi dengan 'Lainnya'"

                Dim Validationmultiparty8 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("G1")
                Validationmultiparty8.ShowInputMessage = True
                Validationmultiparty8.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationmultiparty8.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan berita, keterangan atau tujuan transaksi pada saat melakukan transaksi"

                Dim Validationmultiparty9 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("H1")
                Validationmultiparty9.ShowInputMessage = True
                Validationmultiparty9.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty9.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nomor pencatatan di internal PJK yang dapat mengidentifikasi transaksi"

                Dim Validationmultiparty10 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("I1")
                Validationmultiparty10.ShowInputMessage = True
                Validationmultiparty10.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationmultiparty10.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nama lokasi/tempat terjadinya transaksi dengan format:'nama kantor-kabupaten/kota-provinsi'"

                Dim Validationmultiparty11 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("J1")
                Validationmultiparty11.ShowInputMessage = True
                Validationmultiparty11.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationmultiparty11.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nama teller/petugas front office yang berhubungan dengan Pengguna Jasa untuk transaksi yang memicu kecurigaan"

                Dim Validationmultiparty12 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("K1")
                Validationmultiparty12.ShowInputMessage = True
                Validationmultiparty12.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationmultiparty12.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nama pejabat pengotorisasi transaksi yang memicu terjadinya kecurigaan"

                Dim Validationmultiparty13 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("L1")
                Validationmultiparty13.ShowInputMessage = True
                Validationmultiparty13.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationmultiparty13.Prompt = (vbCrLf) & (vbCrLf) & "Jika tanggal setoran berbeda dgn tanggal pembukuan, maka PJK memilih 'Ya'"

                Dim Validationmultiparty14 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("M1")
                Validationmultiparty14.ShowInputMessage = True
                Validationmultiparty14.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty14.Prompt = (vbCrLf) & (vbCrLf) & "Jika Late Deposit dipilih 'Ya', apabilah dipilih 'Tidak' maka Date Posting tidak diisi yang artinya OPTIONAL"


                Dim Validationmultiparty15 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("N1")
                Validationmultiparty15.ShowInputMessage = True
                Validationmultiparty15.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty15.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list peran dari Pengguna Jasa yang melakukan transaksi"

                Dim Validationmultiparty39 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("O1")
                Validationmultiparty39.ShowInputMessage = True
                Validationmultiparty39.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationmultiparty39.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan angka 0 s.d. 10 yang menunjukkan seberapa besar pihak ini terkait dengan transaksi yang dilakukan"


                Dim Validationmultiparty45 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("P1")
                Validationmultiparty45.ShowInputMessage = True
                Validationmultiparty45.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty45.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list (From)"

                Dim Validationmultiparty49 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("Q1")
                Validationmultiparty49.ShowInputMessage = True
                Validationmultiparty49.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty49.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list "

                Dim Validationmultiparty16 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("R1")
                Validationmultiparty16.ShowInputMessage = True
                Validationmultiparty16.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty16.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list instrumen transaksi"

                Dim Validationmultiparty17 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("S1")
                Validationmultiparty17.ShowInputMessage = True
                Validationmultiparty17.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationmultiparty17.Prompt = (vbCrLf) & (vbCrLf) & "Menjadi MANDATORY apabila funds_code /instrumen transaksi asal diisi dengan 'Lainnya'"

                Dim Validationmultiparty18 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("T1")
                Validationmultiparty18.ShowInputMessage = True
                Validationmultiparty18.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY (apabila transaksi asal dilakukan dalam mata uang asing)"
                Validationmultiparty18.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list mata uang asing"

                Dim Validationmultiparty19 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("U1")
                Validationmultiparty19.ShowInputMessage = True
                Validationmultiparty19.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY (apabila transaksi asal dilakukan dalam mata uang asing)"
                Validationmultiparty19.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nominal transaksi dalam mata uang asing"

                Dim Validationmultiparty20 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("V1")
                Validationmultiparty20.ShowInputMessage = True
                Validationmultiparty20.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY (apabila transaksi asal dilakukan dalam mata uang asing)"
                Validationmultiparty20.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan nilai kurs yang digunakan pada saat transaksi"

                Dim Validationmultiparty21 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("W1")
                Validationmultiparty21.ShowInputMessage = True
                Validationmultiparty21.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty21.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan memilih dropdown list nama negara transaksi asal"

                Dim Validationmultiparty29 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("X1")
                Validationmultiparty29.ShowInputMessage = True
                Validationmultiparty29.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationmultiparty29.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan informasi penting terkait transaksi apabila informasi tersebut tidak memiliki field pengisian"

                Dim Validationmultiparty30 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("Y1")
                Validationmultiparty30.ShowInputMessage = True
                Validationmultiparty30.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY (apabila transaksi yang dilaporkan adalah LTKL/IFTI)"
                Validationmultiparty30.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan 'Ya' jika transaksi SWIFT dan 'Tidak' jika bukan SWIFT"

                Dim Validationmultiparty31 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("Z1")
                Validationmultiparty31.ShowInputMessage = True
                Validationmultiparty31.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty31.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan Account Number"

                Dim Validationmultiparty32 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("AA1")
                Validationmultiparty32.ShowInputMessage = True
                Validationmultiparty32.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty32.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan CIF"

                Dim Validationmultiparty33 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("AB1")
                Validationmultiparty33.ShowInputMessage = True
                Validationmultiparty33.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini MANDATORY"
                Validationmultiparty33.Prompt = (vbCrLf) & (vbCrLf) & "Diisi dengan WIC No"

                Dim Validationmultiparty37 As DataValidation.ExcelDataValidationAny = ws4.DataValidations.AddAnyValidation("AC1")
                Validationmultiparty37.ShowInputMessage = True
                Validationmultiparty37.PromptTitle = "NAWADATA: " & (vbCrLf) & " field ini OPTIONAL"
                Validationmultiparty37.Prompt = (vbCrLf) & (vbCrLf) & "Menjadi MANDATORY jika transaksi dilakukan oleh Conductor dan wajib input Conductor ID tersebut saat transaksi dilakukan"






                ws.Cells(ws.Dimension.Address).AutoFitColumns()
                ws2.Cells(ws2.Dimension.Address).AutoFitColumns()
                ws3.Cells(ws3.Dimension.Address).AutoFitColumns()
                ws4.Cells(ws4.Dimension.Address).AutoFitColumns()
                ws5.Cells(ws5.Dimension.Address).AutoFitColumns()
                ws6.Cells(ws6.Dimension.Address).AutoFitColumns()
                ws7.Cells(ws7.Dimension.Address).AutoFitColumns()
                ws8.Cells(ws8.Dimension.Address).AutoFitColumns()
                resource.Save()
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("content-disposition", "attachment;filename=" & "TemplateSTR.xlsx")
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                Response.End()
            End Using


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Button_DownloadTemplatewithdata(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    'Protected Sub Button_DownloadTemplateWithdata(sender As Object, e As Ext.Net.DirectEventArgs)
    '    Try
    '        Dim objfileinfo As IO.FileInfo = Nothing
    '        objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\TempReportSTR\Template Upload STR.xlsx"))
    '        'Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)


    '        Using resource As New ExcelPackage(objfileinfo)
    '            Dim listReport = UploadReportSTR


    '            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
    '            ws.Cells("A1").LoadFromDataTable(objtbl, True)
    '            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
    '            Dim intcolnumber As Integer = 1
    '            For Each item As System.Data.DataColumn In objtbl.Columns
    '                If item.DataType = GetType(Date) Then
    '                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
    '                End If
    '                intcolnumber = intcolnumber + 1
    '            Next
    '            ws.Cells(ws.Dimension.Address).AutoFitColumns()
    '            resource.Save()
    '            Response.Clear()
    '            Response.ClearHeaders()
    '            Response.ContentType = "application/vnd.ms-excel"
    '            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
    '            Response.Charset = ""
    '            Response.AddHeader("cache-control", "max-age=0")
    '            Me.EnableViewState = False
    '            Response.ContentType = "ContentType"
    '            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
    '            Response.End()
    '        End Using
    '        End Using
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub BtnCancelImport_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            FileTemplateSTR.Reset()
            ButtonImport.Disabled = False
            Bindgridnothing()
            ClearSession_Upload()

            GridPanelReportInvalid.Hidden = True
            GridPanelIndikatorLaporanInvalid.Hidden = True
            GridPanelTransactionBipartyInvalid.Hidden = True
            GridPanelTransactionMultiPartyInvalid.Hidden = True
            GridPanelActivityPersonInvalid.Hidden = True
            GridPanelActivityAccountInvalid.Hidden = True
            GridPanelActivityEntityInvalid.Hidden = True

            PanelReportInvalid.Hidden = True
            PanelIndikatorLaporanInvalid.Hidden = True
            PanelTransactionBipartyInvalid.Hidden = True
            PanelTransactionMultiPartyInvalid.Hidden = True
            PanelActivityPersonInvalid.Hidden = True
            PanelActivityAccountInvalid.Hidden = True
            PanelActivityEntityInvalid.Hidden = True

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Sub GridPanelSetting()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = SystemParameterBLL.GetSystemParameterByPk(32)
        Dim bsettingRight As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            bsettingRight = objParamSettingbutton.SettingValue
        End If

        If bsettingRight = 2 Then
            GridPanelTransactionBiParty.ColumnModel.Columns.RemoveAt(GridPanelTransactionBiParty.ColumnModel.Columns.Count - 1)
            GridPanelTransactionBiParty.ColumnModel.Columns.Insert(1, cmdbipartyvalid)
            GridPanelTransactionBipartyInvalid.ColumnModel.Columns.RemoveAt(GridPanelTransactionBipartyInvalid.ColumnModel.Columns.Count - 1)
            GridPanelTransactionBipartyInvalid.ColumnModel.Columns.Insert(1, cmdBipartyInvalid)
            GridPanelTransactionMultiParty.ColumnModel.Columns.RemoveAt(GridPanelTransactionMultiParty.ColumnModel.Columns.Count - 1)
            GridPanelTransactionMultiParty.ColumnModel.Columns.Insert(1, cmdmultipartyvalid)
            GridPanelTransactionMultiPartyInvalid.ColumnModel.Columns.RemoveAt(GridPanelTransactionMultiPartyInvalid.ColumnModel.Columns.Count - 1)
            GridPanelTransactionMultiPartyInvalid.ColumnModel.Columns.Insert(1, cmdMultipartyinValid)

        End If

    End Sub


    Sub Paging()
        StoreReportData.PageSize = SystemParameterBLL.GetPageSize
        StoreReportInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreIndikatorLaporanData.PageSize = SystemParameterBLL.GetPageSize
        StoreIndikatorLaporanInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreTransactionBiParty.PageSize = SystemParameterBLL.GetPageSize
        StoreTransactionBipartyInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreTransactionMultiParty.PageSize = SystemParameterBLL.GetPageSize
        StoreTransactionMultiPartyInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityAccount.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityAccountInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityEntity.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityEntityInvalid.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityPerson.PageSize = SystemParameterBLL.GetPageSize
        StoreActivityPersonInvalid.PageSize = SystemParameterBLL.GetPageSize

    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")

            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
#End Region

End Class