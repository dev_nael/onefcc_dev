﻿Imports System.Data
Imports Ext
Imports NawaBLL
Imports NawaDAL
Imports OfficeOpenXml

Partial Class CRS_CRS_International_CRS_International_GenerateView
    Inherits Parent
    Public objFormModuleView As NawaBLL.FormModuleView ''11-Mar-2022

    Public Property strWhereClause() As String
        Get
            Return Session("CRS_International_GenerateView.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_GenerateView.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("CRS_International_GenerateView.strSort")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_GenerateView.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("CRS_International_GenerateView.indexStart")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_GenerateView.indexStart") = value
        End Set
    End Property

    Public Property QueryTable() As String
        Get
            Return Session("CRS_International_GenerateView.Table")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_GenerateView.Table") = value
        End Set
    End Property

    Public Property QueryField() As String
        Get
            Return Session("CRS_International_GenerateView.Field")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_GenerateView.Field") = value
        End Set
    End Property

    Public Property stringModuleID As String
        Get
            Return Session("CRS_International_GenerateView.StringModuleID")
        End Get
        Set(value As String)
            Session("CRS_International_GenerateView.StringModuleID") = value
        End Set
    End Property

    Public Property objModule As NawaDAL.Module
        Get
            Return Session("CRS_International_GenerateView.objModule")
        End Get
        Set(value As NawaDAL.Module)
            Session("CRS_International_GenerateView.objModule") = value
        End Set
    End Property

    'Private Sub Parameterview_Init(sender As Object, e As EventArgs) Handles Me.Init
    '    objFormModuleView = New FormModuleView(Me.GridpanelView, Me.BtnAdd)
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        Dim Moduleid As String = Request.Params("ModuleID")
        stringModuleID = Moduleid
        Dim intModuleid As Integer
        Try
            intModuleid = Common.DecryptQueryString(Moduleid, SystemParameterBLL.GetEncriptionKey)
            'Dim objmodule As NawaDAL.Module = ModuleBLL.GetModuleByModuleID(intModuleid)
            objModule = ModuleBLL.GetModuleByModuleID(intModuleid)

            If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objModule.PK_Module_ID, Common.ModuleActionEnum.view) Then
                Dim strIDCode As String = 1
                strIDCode = Common.EncryptQueryString(strIDCode, SystemParameterBLL.GetEncriptionKey)

                Net.X.Redirect(String.Format(Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                Exit Sub
            End If

            objFormModuleView.ModuleID = objModule.PK_Module_ID
            objFormModuleView.ModuleName = objModule.ModuleName
            '''Update BSIM 14Oct2020
            'objFormModuleView.AddField("NO_ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.INTValue, , , , , )
            'objFormModuleView.AddField("Transaction_Date", "Transaction Date", 2, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )
            'objFormModuleView.AddField("Jenis_Laporan", "Jenis Laporan", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
            'objFormModuleView.AddField("Last_Update_Date", "Last Update Date", 4, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )
            'objFormModuleView.AddField("Total_Valid", "Total Valid", 5, False, True, NawaBLL.Common.MFieldType.INTValue,,,,, )
            'objFormModuleView.AddField("Total_Invalid", "Total Invalid", 6, False, True, NawaBLL.Common.MFieldType.INTValue,,,,, )
            'objFormModuleView.AddField("Report_PPATK_Date", "Report PPATK Date", 7, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )
            'objFormModuleView.AddField("Last_Generate_Date", "Last Generate Date", 8, False, True, NawaBLL.Common.MFieldType.DATETIMEValue,,,,, )
            'objFormModuleView.AddField("Status", "Status", 9, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
            'objFormModuleView.AddField("StatusGenerateAPI", "Status API", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, ) '' Add 06-Jan2-2022
            'objFormModuleView.AddField("IsAlreadyValidateSchema", "Already Validate Schema", 11, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )
            'objFormModuleView.AddField("ValidateSchemaResultValid", "Validate Schema Result", 12, False, True, NawaBLL.Common.MFieldType.VARCHARValue,,,,, )

            objFormModuleView.AddField("PK_CRS_INTERNATIONAL_GENERATE_BULK_ID", "ID", 1, True, True, NawaBLL.Common.MFieldType.INTValue)
            objFormModuleView.AddField("Report_ID", "Report ID", 2, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("YearPeriode", "Year Periode", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Format", "Format", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Total_Report_Ready_to_Generate", "Total Report Ready To Generate", 5, False, False, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Status_API", "Status API", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Error_API", "Error API", 7, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Last_Download_Date", "Last Download Date", 8, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
            objFormModuleView.AddField("CreatedBy", "CreatedBy", 9, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            objFormModuleView.AddField("Valid_XSD", "Valid XSD", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
            'objFormModuleView.AddField("Filezip", "Filezip", 11, False, True, NawaBLL.Common.MFieldType.VarBinaryValue)
            objFormModuleView.SettingFormView() '' add 11-Mar-2022
            'Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
            'objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"

            ''14-Mar-2022 Adi : ubah button Detail jadi Generate
            'objcommandcol.Commands.Clear()

            'objcommandcol.Width = 90
            'Dim objGenerate As New GridCommand
            'objGenerate.CommandName = "Generate"
            'objGenerate.Icon = Icon.ApplicationViewDetail
            'objGenerate.Text = "Detail"
            'objGenerate.ToolTip.Text = ""
            'objcommandcol.Commands.Add(objGenerate)

            'Dim extparam As New Ext.Net.Parameter
            'extparam.Name = "unikkey"
            'extparam.Value = "record.data.NO_ID"
            'extparam.Mode = ParameterMode.Raw
            'objcommandcol.DirectEvents.Command.ExtraParams.Add(extparam)

            'Dim extParamCommandName As New Ext.Net.Parameter
            'extparam.Name = "command"
            'extparam.Value = "command"
            'extparam.Mode = ParameterMode.Raw
            'objcommandcol.DirectEvents.Command.ExtraParams.Add(extparam)

            'AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf Generate

            If Not Ext.Net.X.IsAjaxRequest Then
                Dim objcommandcolumn As Ext.Net.Column = GridpanelView.ColumnModel.Columns.Find(Function(x) x.DataIndex = "Status_API")
                objcommandcolumn.Width = 200


                cboExportExcel.SelectedItem.Value = "Excel"
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub



    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            'Begin Penambahan Advanced Filter
            If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                Toolbar2.Hidden = True
            Else
                Toolbar2.Hidden = False
            End If
            LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")
            'END Penambahan Advanced Filter
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer


            ''' added by Apuy 20200902
            'Dim ReportType As String = Request.Params("ReportType")

            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)


            ''' added by Apuy 20200902
            'If Not String.IsNullOrEmpty(ReportType) Then
            '    If String.IsNullOrEmpty(strfilter) Then
            '        strfilter += "Report_Type like ('%" & ReportType & "%')"
            '    Else
            '        strfilter += " AND Report_Type like ('%" & ReportType & "%')"
            '    End If
            'End If

            Dim strsort As String = ""
            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter

            'Begin Update Penambahan Advance Filter
            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            End If

            Me.strOrder = strsort

            If strOrder = "" Then
                strOrder = "PK_CRS_INTERNATIONAL_GENERATE_BULK_ID DESC"
            End If

            'QueryTable = "goaml_Generate_XML"
            'QueryTable = "Vw_goAML_ListOfGenerated_v2" '' Edit 06-Jan-2022
            'QueryField = "NO_ID,Transaction_Date,Jenis_Laporan,Last_Update_Date,Report_Type,Total_Valid,Total_Invalid,Report_PPATK_Date,Last_Generate_Date,Status,CASE WHEN  IsAlreadyValidateSchema =1 THEN 'Yes' ELSE 'No' END IsAlreadyValidateSchema ,CASE WHEN ValidateSchemaResultValid=1 THEN 'Valid' when ValidateSchemaResultValid is null then 'Not Yes Validate' ELSE 'Not Valid' END ValidateSchemaResultValid"
            'QueryField = "NO_ID,Transaction_Date,Jenis_Laporan,Last_Update_Date,Report_Type,Total_Valid,Total_Invalid,Report_PPATK_Date,Last_Generate_Date,Status,CASE WHEN  IsAlreadyValidateSchema =1 THEN 'Yes' ELSE 'No' END IsAlreadyValidateSchema ,CASE WHEN ValidateSchemaResultValid=1 THEN '1 - Valid' when ValidateSchemaResultValid is null then '2 - Not Yet Validate' ELSE '3 - Not Valid' END ValidateSchemaResultValid"
            'QueryField = "NO_ID,Transaction_Date,Jenis_Laporan,Last_Update_Date,Report_Type,Total_Valid,Total_Invalid,Report_PPATK_Date,Last_Generate_Date,Status,CASE WHEN  IsAlreadyValidateSchema =1 THEN 'Yes' ELSE 'No' END IsAlreadyValidateSchema ,CASE WHEN ValidateSchemaResultValid=1 THEN '1 - Valid' when ValidateSchemaResultValid is null then '2 - Not Yet Validate' ELSE '3 - Not Valid' END ValidateSchemaResultValid, StatusGenerateAPI" '' Edit 06-Jan-2022
            'QueryField = "NO_ID,Transaction_Date,Jenis_Laporan,Last_Update_Date,Report_Type,Total_Valid,Total_Invalid,Report_PPATK_Date,Last_Generate_Date,Status,IsAlreadyValidateSchema , ValidateSchemaResultValid, StatusGenerateAPI" '' Edit 11 Mar 2022

            'Dim DataPaging As DataTable = SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)
            Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strOrder, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            e.Total = inttotalRecord
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub GridcommandListGenerateView(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            stringModuleID = Moduleid
            Dim intModuleid As Integer

            intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            Dim id As String = e.ExtraParams(0).Value
            Dim PKID As String = NawaBLL.Common.EncryptQueryString(id, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            If e.ExtraParams(1).Value = "Generate" Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objmodule.UrlDetail & "?ModuleID=" & Moduleid & "&ID=" & PKID)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objmodule.UrlAdd & "?ModuleID=" & Moduleid & "&ID=" & PKID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                'If cboExportExcel.SelectedItem.Value = "Excel" Then '' Edit 14-Mar-2022
                If cboExportExcel.SelectedItem.Text = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CRSInternationalGenerateReport")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=CRSInternationalGenerateReport.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then '' Edit 14-Mar-2022
                ElseIf cboExportExcel.SelectedItem.Text = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                'If cboExportExcel.SelectedItem.Value = "Excel" Then '' Edit 14-Mar-2022
                If cboExportExcel.SelectedItem.Text = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CRSInternationalGenerateReport")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=CRSInternationalGenerateReport.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                    'ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then '' Edit 14-Mar-2022
                ElseIf cboExportExcel.SelectedItem.Text = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    'Begin Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update Penambahan Advance Filter

#Region "11-Mar-2022 Felix"
    Private Sub CRS_International_GenerateView_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub

#End Region

    <DirectMethod>
    Public Sub BtnAdd_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")


            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    '#Region "14-Mar-2022 Adi"
    '    Protected Sub Generate(sender As Object, e As DirectEventArgs)
    '        Try
    '            If e.ExtraParams("command") = "Generate" Then
    '                Dim PK_ID As Long = e.ExtraParams("unikkey")
    '                Dim strEncryptedID As String = NawaBLL.Common.EncryptQueryString(PK_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
    '                Dim strEncryptedModuleID = NawaBLL.Common.EncryptQueryString(objModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
    '                If InStr(objModule.UrlDetail, "?") > 0 Then
    '                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objModule.UrlDetail & "&ModuleID=" & strEncryptedModuleID & "&ID=" & strEncryptedID)
    '                Else
    '                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & objModule.UrlDetail & "?ModuleID=" & strEncryptedModuleID & "&ID=" & strEncryptedID)
    '                End If
    '            End If
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

    '        End Try
    '    End Sub
    '#End Region


End Class
