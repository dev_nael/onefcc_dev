﻿Imports System.Data
Imports NawaDAL
Imports CRSBLL
Imports CRSDAL
Imports System.Data.SqlClient
Partial Class CRS_CRS_International_CRS_International_Add
    Inherits ParentPage
    Public objFormModuleAdd As NawaBLL.FormModuleAdd
    Dim drTemp As DataRow
    Dim strSQL As String


    Public Property IDUnik() As Long
        Get
            Return Session("CRS_International_Add.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("CRS_International_Add.IDUnik") = value
        End Set
    End Property

    Public Property ReadOnlyLevel() As Integer
        Get
            Return Session("CRS_International_Add.ReadOnlyLevel")
        End Get
        Set(ByVal value As Integer)
            Session("CRS_International_Add.ReadOnlyLevel") = value
        End Set
    End Property


    'Ada bugs gak tau causenya tapi kalau saat load gridpanel dibind new datatable kemudian di load misal Address padahal datanya ada tapi gridpanelnya kosong
    Public Property IsNewRecord() As Boolean
        Get
            Return Session("CRS_International_Add.IsNewRecord")
        End Get
        Set(ByVal value As Boolean)
            Session("CRS_International_Add.IsNewRecord") = value
        End Set
    End Property

    Public Property CRS_Report_Class() As CRSBLL.CRS_Report_BLL.CRS_Reporting_Class
        Get
            Return Session("CRS_International_Add.CRS_Report_Class")
        End Get
        Set(ByVal value As CRSBLL.CRS_Report_BLL.CRS_Reporting_Class)
            Session("CRS_International_Add.CRS_Report_Class") = value
        End Set
    End Property

    Public Property CRS_Report_Group_Class() As CRSBLL.CRS_Report_BLL.CRS_Reporting_Group_Class
        Get
            Return Session("CRS_International_Add.CRS_Report_Group_Class")
        End Get
        Set(ByVal value As CRSBLL.CRS_Report_BLL.CRS_Reporting_Group_Class)
            Session("CRS_International_Add.CRS_Report_Group_Class") = value
        End Set
    End Property

#Region "Default Framework"

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        If buttonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub

    Sub SetCommandColumnLocation()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim buttonPosition As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            buttonPosition = objParamSettingbutton.SettingValue
        End If

        'Report Level
        ColumnActionLocation(gp_Report_Group, cc_Reporting_Group, buttonPosition)
        ColumnActionLocation(gp_payment, cc_payment, buttonPosition)
        ColumnActionLocation(gp_cp, cc_CP, buttonPosition)

    End Sub

    Private Sub CRS_International_Add_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                SetCommandColumnLocation()
                SetReportHeader()
                Session("AddNewGroup") = 0
                Session("ReportHeaderdanFIsudahDiSave") = 0
                txtMessageRefID.Value = "Auto Generated"
                txtDocRefID.Value = "Auto Generated"
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub CRS_International_Add_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Insert
    End Sub

    Private Sub CRS_International_Add_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleAdd = New NawaBLL.FormModuleAdd(fpMain, Panelconfirmation, LblConfirmation)
    End Sub

    Protected Sub btn_Confirmation_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim strEncryptedModuleID = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & strEncryptedModuleID)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & strEncryptedModuleID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
#End Region

#Region "Reporting Header"
    Private Sub SetReportHeader()

        txtCorrectionMessageRefID.Hidden = True
        'Create new Report Class
        CRS_Report_Class = New CRS_Report_BLL.CRS_Reporting_Class

        'Clean Report Input
        txtWarning.Value = Nothing
        txtMessageRefID.Value = Nothing

        Dim objmessagetype As New vw_CRS_INTERNATIONAL_MESSAGE_TYPE_INDICATOR
        Using objdb As New CRSDAL.CRSEntities
            objmessagetype = objdb.vw_CRS_INTERNATIONAL_MESSAGE_TYPE_INDICATOR.Where(Function(x) x.PK_CRS_INTERNATIONAL_MESSAGE_TYPE_INDICATOR_ID = 1).FirstOrDefault
        End Using

        With objmessagetype
            cmb_MessageTypeInd.SetTextWithTextValue(.FK_MESSAGE_TYPE_INDICATOR_CODE, .MESSAGE_TYPE_INDICATOR_NAME)
        End With

        txtCorrectionMessageRefID.Value = Nothing
        Dim thisyear As DateTime = DateTime.Now
        Dim thisyearconver As String = "31-12-" & (Year(thisyear) - 1)
        txtReportingPeriod.Value = thisyearconver
        txtReportingPeriod.Text = "31-Dec" & (Year(thisyear) - 1)
        txtTimeStamp.Value = DateTime.Now
        Dim objdoctype As New vw_CRS_International_Doc_Type_Indic
        Using objdb As New CRSDAL.CRSEntities
            objdoctype = objdb.vw_CRS_International_Doc_Type_Indic.Where(Function(x) x.PK_CRS_INTERNATIONAL_DOC_TYPE_INDIC_ID = 2).FirstOrDefault
        End Using

        With objdoctype
            cmb_DocTypeIndic.SetTextWithTextValue(.FK_CRS_DOC_TYPE_INDIC_CODE, .DOC_TYPE_INDIC_NAME)
        End With

        txtDocRefID.Value = Nothing
        txtCorDocRefID.Value = Nothing
        Dim objljk As New vw_crs_international_ljk_type
        Using objdb As New CRSDAL.CRSEntities
            objljk = objdb.vw_crs_international_ljk_type.Where(Function(x) x.PK_crs_international_ljk_type_ID = 2).FirstOrDefault
        End Using

        With objljk
            cmb_LJK.SetTextWithTextValue(.FK_CRS_LJK_TYPE_CODE, .LJK_TYPE_NAME)
        End With
        'cmb_LJK.SetTextValue("")
        cmb_ReceivingCountry.SetTextValue("")

        txtSendingCompany.Text = CRS_Global_BLL.getGlobalParameterValueByID(2)
        txtTransmittingCountry.Text = CRS_Global_BLL.getGlobalParameterValueByID(3)
        txtMessageType.Text = CRS_Global_BLL.getGlobalParameterValueByID(4)


        'Report FI
        Dim objrefreportfi As New CRS_INTERNATIONAL_REF_REPORTINGFI
        Using objdb As New CRSDAL.CRSEntities
            objrefreportfi = objdb.CRS_INTERNATIONAL_REF_REPORTINGFI.Where(Function(x) x.LJK_Type = "DI").FirstOrDefault
        End Using

        Using objDB As New CRSDAL.CRSEntities
            Dim listcontact As List(Of CRS_INTERNATIONAL_SENDER_CONTACT) = objDB.CRS_INTERNATIONAL_SENDER_CONTACT.Where(Function(x) x.LJK_Type = "DI").ToList
            Dim contact As String = ""
            Dim iterasi As Integer = 0
            For Each item In listcontact

                If iterasi > 0 Then
                    contact = contact + "-" + item.LAST_NAME + "_" + item.CONTACT
                Else
                    contact = item.LAST_NAME + "_" + item.CONTACT
                End If
                iterasi = iterasi + 1

            Next
            txtContact.Text = contact
        End Using
        If objrefreportfi IsNot Nothing Then
            With objrefreportfi
                'Set Default Value of RentityID and RentityBranch

                txtResCountryCode.Text = .ResCountryCode
                txtIN.Text = .Identification_Number
                txtIN_IssueBy.Text = .IN_IssueBy
                txtIN_INType.Text = .IN_INType
                txtName.Text = .Name
                txtName_Type.Text = .Name_Type
                txtAddress_LegalAddressType.Text = .Address_LegalAddressType
                txtAddress_CountryCode.Text = .Address_CountryCode
                txtAddress_AddressFree.Text = .Address_AddressFree
                txtAddress_AddressFix_Street.Text = .Address_AddressFix_Street
                txtAddress_AddressFix_BuildingIdentifier.Text = .Address_AddressFix_BuildingIdentifier
                txtAddress_AddressFix_SuiteIdentifier.Text = .Address_AddressFix_SuiteIdentifier
                txtAddress_AddressFix_FloorIdentifier.Text = .Address_AddressFix_FloorIdentifier
                txtAddress_AddressFix_DistrictName.Text = .Address_AddressFix_DistrictName
                txtAddress_AddressFix_POB.Text = .Address_AddressFix_POB
                txtAddress_AddressFix_PostCode.Text = .Address_AddressFix_PostCode
                txtAddress_AddressFix_City.Text = .Address_AddressFix_City
                txtAddress_AddressFix_CountrySubentity.Text = .Address_AddressFix_CountrySubentity
            End With
        End If


        cmb_LJK.StringFieldStyle = "background-color:#FFE4C4"
        cmb_ReceivingCountry.StringFieldStyle = "background-color:#FFE4C4"
        cmb_DocTypeIndic.StringFieldStyle = "background-color:#FFE4C4"
        cmb_MessageTypeInd.StringFieldStyle = "background-color:#FFE4C4"


    End Sub


    'Protected Sub btn_generatemessagerefid_Click(sender As Object, e As DirectEventArgs)
    '    Try
    '        Dim param(2) As SqlParameter

    '        If String.IsNullOrEmpty(cmb_ReceivingCountry.SelectedItemValue) Then
    '            Throw New ApplicationException(cmb_ReceivingCountry.Label + " Is required.")
    '        End If

    '        If txtReportingPeriod.SelectedDate = DateTime.MinValue Then
    '            Throw New ApplicationException(txtReportingPeriod.FieldLabel + " Is required.")
    '        End If

    '        param(0) = New SqlParameter
    '        param(0).ParameterName = "@ReportingYear"
    '        param(0).Value = txtReportingPeriod.Value
    '        param(0).SqlDbType = SqlDbType.VarChar



    '        param(1) = New SqlParameter
    '        param(1).ParameterName = "@ToCountry"
    '        param(1).Value = cmb_ReceivingCountry.SelectedItemValue
    '        param(1).SqlDbType = SqlDbType.VarChar

    '        If String.IsNullOrEmpty(cmb_LJK.SelectedItemValue) Then
    '            Throw New ApplicationException(cmb_LJK.Label + " Is required.")
    '        End If

    '        param(2) = New SqlParameter
    '        param(2).ParameterName = "@LJKTYPE"
    '        param(2).Value = cmb_LJK.SelectedItemValue
    '        param(2).SqlDbType = SqlDbType.VarChar


    '        Dim mssgrefid As String = ""
    '        mssgrefid = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Get_New_MessageRefID", param)

    '        txtMessageRefID.Text = mssgrefid
    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    'Protected Sub btn_generatedocrefid_Click(sender As Object, e As DirectEventArgs)
    '    Try

    '        If String.IsNullOrEmpty(cmb_LJK.SelectedItemValue) Then
    '            Throw New ApplicationException(cmb_LJK.Label + " Is required.")
    '        End If
    '        If String.IsNullOrEmpty(txtMessageRefID.Value) Then
    '            Throw New ApplicationException(txtMessageRefID.FieldLabel + " Is required.")
    '        End If
    '        'cek apakah data LJK,Periode, dan Receiving Country Sama
    '        Dim param2(1) As SqlParameter

    '        param2(0) = New SqlParameter
    '        param2(0).ParameterName = "@MessageRefID"
    '        param2(0).Value = txtMessageRefID.Value
    '        param2(0).SqlDbType = SqlDbType.VarChar

    '        param2(1) = New SqlParameter
    '        param2(1).ParameterName = "@LJK"
    '        param2(1).Value = cmb_LJK.SelectedItemValue
    '        param2(1).SqlDbType = SqlDbType.VarChar


    '        Dim docrefid_seq As Integer = 0
    '        docrefid_seq = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Get_New_DocRefID_ReportFI_Seq", param2)

    '        If docrefid_seq > 0 Then
    '            txtdisplay.Hidden = False
    '            txtdisplay.Value = "* This CRS International With LJK Type, Receiving Country And Report Periode Already Exist In CRS International Report FI"
    '            txtdisplay.FieldStyle = "color:red"
    '        Else
    '            txtdisplay.Hidden = True
    '        End If

    '        'Doc Ref ID Reporting FI
    '        Dim param(1) As SqlParameter

    '        param(0) = New SqlParameter
    '        param(0).ParameterName = "@MessageRefID"
    '        param(0).Value = txtMessageRefID.Value
    '        param(0).SqlDbType = SqlDbType.VarChar

    '        param(1) = New SqlParameter
    '        param(1).ParameterName = "@LJK"
    '        param(1).Value = cmb_LJK.SelectedItemValue
    '        param(1).SqlDbType = SqlDbType.VarChar


    '        Dim docrefid_FI As String = ""
    '        docrefid_FI = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Get_New_DocRefID_ReportFI", param)

    '        txtDocRefID.Text = docrefid_FI
    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Protected Sub btnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            If CRS_Report_Class.list_CRS_Reporting_Group.Count < 1 Then
                Throw New ApplicationException("Reporting Group Is Null, it's required.")
            End If
            'Create Submission Notes
            If Session("ReportHeaderdanFIsudahDiSave") = 0 Then
                Dim param(2) As SqlParameter

                If String.IsNullOrEmpty(cmb_ReceivingCountry.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_ReceivingCountry.Label + " is required.")
                End If

                If txtReportingPeriod.SelectedDate = DateTime.MinValue Then
                    Throw New ApplicationException(txtReportingPeriod.FieldLabel + " is required.")
                End If

                param(0) = New SqlParameter
                param(0).ParameterName = "@ReportingYear"
                param(0).Value = txtReportingPeriod.Value
                param(0).SqlDbType = SqlDbType.VarChar



                param(1) = New SqlParameter
                param(1).ParameterName = "@ToCountry"
                param(1).Value = cmb_ReceivingCountry.SelectedItemValue
                param(1).SqlDbType = SqlDbType.VarChar

                If String.IsNullOrEmpty(cmb_LJK.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_LJK.Label + " is required.")
                End If

                param(2) = New SqlParameter
                param(2).ParameterName = "@LJKTYPE"
                param(2).Value = cmb_LJK.SelectedItemValue
                param(2).SqlDbType = SqlDbType.VarChar


                Dim mssgrefid As String = ""
                mssgrefid = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Get_New_MessageRefID", param)
                Dim messagerefidstatus As Integer = 0
                messagerefidstatus = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select count(*) from  crs_international_Reporting_group where docspec_docrefid like '%" & mssgrefid.ToString & "%'", Nothing)
                If messagerefidstatus = 0 Then

                    Dim messagerefidstatus2 As Integer = 0
                    messagerefidstatus2 = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select count(*) from crs_international_Report_header where messagerefid = '" & mssgrefid & "'", Nothing)

                    If messagerefidstatus2 > 0 Then
                        Dim str As String = mssgrefid.ToString
                        Dim strrefidnew As String = str.Substring(0, str.Length - 3)
                        Dim seqmessagerefid As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "Select max(MessageRefID)  from CRS_International_Report_Header where MessageRefID like '' + '" & strrefidnew & "' + '%' ", Nothing)
                        Dim stringtoint As Integer = Convert.ToInt32(seqmessagerefid.Substring(Math.Max(0, str.Length - 3)))
                        Dim seq As Integer = stringtoint + 1
                        Dim strnew As String = str.Substring(str.Length - 3)
                        Dim strnew2 As String = ""
                        If seq < 10 Then
                            strnew2 = strrefidnew & "00" & seq
                        ElseIf seq < 100 Then
                            strnew2 = strrefidnew & "0" & seq
                        Else
                            strnew2 = strrefidnew & seq
                        End If

                        txtMessageRefID.Value = strnew2
                        txtMessageRefID.Text = strnew2
                    Else
                        Dim paramm(2) As SqlParameter



                        paramm(0) = New SqlParameter
                        paramm(0).ParameterName = "@ReportingYear"
                        paramm(0).Value = txtReportingPeriod.Value
                        paramm(0).SqlDbType = SqlDbType.VarChar



                        paramm(1) = New SqlParameter
                        paramm(1).ParameterName = "@ToCountry"
                        paramm(1).Value = cmb_ReceivingCountry.SelectedItemValue
                        paramm(1).SqlDbType = SqlDbType.VarChar

                        If String.IsNullOrEmpty(cmb_LJK.SelectedItemValue) Then
                            Throw New ApplicationException(cmb_LJK.Label + " is required.")
                        End If

                        paramm(2) = New SqlParameter
                        paramm(2).ParameterName = "@LJKTYPE"
                        paramm(2).Value = cmb_LJK.SelectedItemValue
                        paramm(2).SqlDbType = SqlDbType.VarChar

                        Dim mssgrefid2 As String = ""
                        mssgrefid2 = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Get_New_MessageRefID", paramm)

                        txtMessageRefID.Text = mssgrefid2

                    End If




                Else
                    Dim strrefidnew As String = mssgrefid.Substring(0, mssgrefid.Length - 3)
                    Dim stringtoint As Integer = Convert.ToInt32(mssgrefid.Substring(Math.Max(0, mssgrefid.Length - 3)))
                    Dim seq As Integer = stringtoint + 1
                    Dim strnew As String = mssgrefid.Substring(mssgrefid.Length - 3)
                    Dim strnew2 As String = ""
                    If seq < 10 Then
                        strnew2 = strrefidnew & "00" & seq
                    ElseIf seq < 100 Then
                        strnew2 = strrefidnew & "0" & seq
                    Else
                        strnew2 = strrefidnew & seq
                    End If
                    txtMessageRefID.Text = strnew2
                End If


                If String.IsNullOrEmpty(cmb_LJK.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_LJK.Label + " is required.")
                End If
                If String.IsNullOrEmpty(txtMessageRefID.Value) Then
                    Throw New ApplicationException(txtMessageRefID.FieldLabel + " is required.")
                End If
                'cek apakah data LJK,Periode, dan Receiving Country Sama
                Dim param22(1) As SqlParameter

                param22(0) = New SqlParameter
                param22(0).ParameterName = "@MessageRefID"
                param22(0).Value = txtMessageRefID.Value
                param22(0).SqlDbType = SqlDbType.VarChar

                param22(1) = New SqlParameter
                param22(1).ParameterName = "@LJK"
                param22(1).Value = cmb_LJK.SelectedItemValue
                param22(1).SqlDbType = SqlDbType.VarChar


                Dim docrefid_seq As Integer = 0
                docrefid_seq = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Get_New_DocRefID_ReportFI_Seq", param22)

                If docrefid_seq > 0 Then
                    txtdisplay.Hidden = False
                    txtdisplay.Value = "* This CRS International With LJK Type, Receiving Country and Report Periode Already Exist In CRS International Report FI"
                    txtdisplay.FieldStyle = "color:red"
                Else
                    txtdisplay.Hidden = True
                End If

                'Doc Ref ID Reporting FI
                Dim param3(1) As SqlParameter

                param3(0) = New SqlParameter
                param3(0).ParameterName = "@MessageRefID"
                param3(0).Value = txtMessageRefID.Value
                param3(0).SqlDbType = SqlDbType.VarChar

                param3(1) = New SqlParameter
                param3(1).ParameterName = "@LJK"
                param3(1).Value = cmb_LJK.SelectedItemValue
                param3(1).SqlDbType = SqlDbType.VarChar


                Dim docrefid_FI As String = ""
                docrefid_FI = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Get_New_DocRefID_ReportFI", param3)

                txtDocRefID.Text = docrefid_FI
                Session("ReportHeaderdanFIsudahDiSave") = 1
            End If
            'Validasi By Form
            ValidateReport()


            'Jalankan Validasi Level Report & Indicator

            Dim strValidationResult As String = CRSBLL.CRS_Report_BLL.validateReportAndReportFI(CRS_Report_Class)
            If Not String.IsNullOrWhiteSpace(strValidationResult) Then
                CRS_Report_Class.obj_CRS_Reporting_Header.isValid = False
                NawaDevBLL.NawaFramework.extInfoPanelupdate(fpMain, strValidationResult, "info_ValidationResult")
                fpMain.Body.ScrollTo(Direction.Top, 0)
                info_ValidationResult.Hidden = False
                Exit Sub
            Else
                CRS_Report_Class.obj_CRS_Reporting_Header.isValid = True
                info_ValidationResult.Hidden = True
            End If

            'Populate Data
            PopulateReportData()

            With CRS_Report_Class.obj_CRS_Reporting_Header
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not ObjModule.IsUseApproval Then
                    .Status = 1
                    lbl_Confirmation.Text = "Data Saved into Database"
                Else
                    .Status = 4
                    lbl_Confirmation.Text = "Data Saved into Pending Approval"
                End If
            End With

            'Save Report
            CRSBLL.CRS_Report_BLL.SaveReportAdd(ObjModule, CRS_Report_Class)

            'Recalculate List of Generated XML
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_UpdategoAML_Generate_XML", Nothing)

            'Show Confirmation Panel
            fpMain.Hidden = True
            pnl_Confirmation.Hidden = False

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnConfirmation_DirectClick(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = Request.Params("ModuleID")
            Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & Moduleid)
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub SetReportStatusAndValidationResult()
        Try
            With CRS_Report_Class.obj_CRS_Reporting_Header
                If Not IsNothing(.Status) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("CRS_International_Ref_Status_Report", "PK_Status_Report_ID", .Status)
                    If drTemp IsNot Nothing Then
                        'FormPanelReport.Title = ObjModule.ModuleLabel & " - Edit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ Status : " & .Status & " - " & drTemp("Description") & " ]"
                        fpMain.Title = ObjModule.ModuleLabel & " - Add [" & drTemp("Description") & "]"
                    End If
                End If

                Dim strValidationResult As String = ""
                strValidationResult = CRS_Report_BLL.getValidationReportByReportID(.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID)
                If Not String.IsNullOrEmpty(strValidationResult) Then
                    NawaDevBLL.NawaFramework.extInfoPanelupdate(fpMain, strValidationResult, "info_ValidationResult")
                    fpMain.Body.ScrollTo(Direction.Top, 0)
                    info_ValidationResult.Hidden = False
                Else
                    info_ValidationResult.Hidden = True
                End If
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Reporting Group"

    Protected Sub txt_accountnumber_group_Changed(sender As Object, e As EventArgs)

        Dim accountnumber As String = txt_accountnumber_group.Value

        Dim adaaccount As Integer = 0
        If CRS_Report_Class.list_CRS_Reporting_Group.Count = 0 Then
        Else
            Dim pkid = CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID
            Dim objcek = CRS_Report_Class.list_CRS_Reporting_Group.Where(Function(x) x.AccountNumber = accountnumber And x.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID <> pkid).FirstOrDefault
            'For Each item In CRS_Report_Class.list_CRS_Reporting_Group
            '    If item.AccountNumber = isaccount Then
            '        adaaccount = adaaccount + 1
            '    End If
            'Next
            If objcek IsNot Nothing Then
                adaaccount = 1
            End If
        End If
        If adaaccount > 0 Then
            txtwarningaccount.Hidden = False
            txtwarningaccount.Value = "This Account Number Is Already Exist In List Reporting Group"
            txtwarningaccount.FieldStyle = "color:red"
        Else
            txtwarningaccount.Hidden = True
        End If


    End Sub


    Protected Sub txt_street_cp_Changed(sender As Object, e As EventArgs)
        Dim isistreet As String = Nothing
        If sender.value IsNot Nothing Then
            isistreet = sender.value.ToString
        Else
            isistreet = Nothing
        End If

        Dim adaisiaddressfixcp As Integer = 0

        If Not String.IsNullOrEmpty(txt_buildingidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_suiteidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_flooridentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_districtname_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_pob_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_postcode_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_countrysubentity_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If

        If adaisiaddressfixcp > 0 Then
        Else
            If isistreet IsNot Nothing Then
                txt_city_cp.AllowBlank = False
                txt_city_cp.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_cp.AllowBlank = True
                txt_city_cp.FieldStyle = "background-color:#ffffff"
            End If
        End If

    End Sub

    Protected Sub txt_buildingidentifier_cp_Changed(sender As Object, e As EventArgs)
        Dim isbuilding As String = Nothing
        If sender.value IsNot Nothing Then
            isbuilding = sender.value.ToString
        Else
            isbuilding = Nothing
        End If

        Dim adaisiaddressfixcp As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If

        If Not String.IsNullOrEmpty(txt_suiteidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_flooridentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_districtname_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_pob_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_postcode_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_countrysubentity_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If

        If adaisiaddressfixcp > 0 Then
        Else
            If isbuilding IsNot Nothing Then
                txt_city_cp.AllowBlank = False
                txt_city_cp.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_cp.AllowBlank = True
                txt_city_cp.FieldStyle = "background-color:#ffffff"
            End If
        End If


    End Sub

    Protected Sub txt_suiteidentifier_cp_Changed(sender As Object, e As EventArgs)
        Dim issuite As String = Nothing
        If sender.value IsNot Nothing Then
            issuite = sender.value.ToString
        Else
            issuite = Nothing
        End If

        Dim adaisiaddressfixcp As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_buildingidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If

        If Not String.IsNullOrEmpty(txt_flooridentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_districtname_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_pob_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_postcode_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_countrysubentity_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If

        If adaisiaddressfixcp > 0 Then
        Else
            If issuite IsNot Nothing Then
                txt_city_cp.AllowBlank = False
                txt_city_cp.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_cp.AllowBlank = True
                txt_city_cp.FieldStyle = "background-color:#ffffff"
            End If
        End If


    End Sub

    Protected Sub txt_flooridentifier_cp_Changed(sender As Object, e As EventArgs)
        Dim isfloor As String = Nothing
        If sender.value IsNot Nothing Then
            isfloor = sender.value.ToString
        Else
            isfloor = Nothing
        End If

        Dim adaisiaddressfixcp As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_buildingidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_suiteidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If

        If Not String.IsNullOrEmpty(txt_districtname_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_pob_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_postcode_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_countrysubentity_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If

        If adaisiaddressfixcp > 0 Then
        Else
            If isfloor IsNot Nothing Then
                txt_city_cp.AllowBlank = False
                txt_city_cp.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_cp.AllowBlank = True
                txt_city_cp.FieldStyle = "background-color:#ffffff"
            End If
        End If


    End Sub

    Protected Sub txt_districtname_cp_Changed(sender As Object, e As EventArgs)
        Dim isdistrict As String = Nothing
        If sender.value IsNot Nothing Then
            isdistrict = sender.value.ToString
        Else
            isdistrict = Nothing
        End If

        Dim adaisiaddressfixcp As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_buildingidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_suiteidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_flooridentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If

        If Not String.IsNullOrEmpty(txt_pob_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_postcode_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_countrysubentity_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If

        If adaisiaddressfixcp > 0 Then
        Else
            If isdistrict IsNot Nothing Then
                txt_city_cp.AllowBlank = False
                txt_city_cp.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_cp.AllowBlank = True
                txt_city_cp.FieldStyle = "background-color:#ffffff"
            End If
        End If


    End Sub

    Protected Sub txt_pob_cp_Changed(sender As Object, e As EventArgs)
        Dim ispob As String = Nothing
        If sender.value IsNot Nothing Then
            ispob = sender.value.ToString
        Else
            ispob = Nothing
        End If

        Dim adaisiaddressfixcp As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_buildingidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_suiteidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_flooridentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_districtname_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If

        If Not String.IsNullOrEmpty(txt_postcode_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_countrysubentity_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If

        If adaisiaddressfixcp > 0 Then
        Else
            If ispob IsNot Nothing Then
                txt_city_cp.AllowBlank = False
                txt_city_cp.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_cp.AllowBlank = True
                txt_city_cp.FieldStyle = "background-color:#ffffff"
            End If
        End If


    End Sub

    Protected Sub txt_postcode_cp_Changed(sender As Object, e As EventArgs)
        Dim ispostcode As String = Nothing
        If sender.value IsNot Nothing Then
            ispostcode = sender.value.ToString
        Else
            ispostcode = Nothing
        End If

        Dim adaisiaddressfixcp As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_buildingidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_suiteidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_flooridentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_districtname_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_pob_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If

        If Not String.IsNullOrEmpty(txt_countrysubentity_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If

        If adaisiaddressfixcp > 0 Then
        Else
            If ispostcode IsNot Nothing Then
                txt_city_cp.AllowBlank = False
                txt_city_cp.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_cp.AllowBlank = True
                txt_city_cp.FieldStyle = "background-color:#ffffff"
            End If
        End If


    End Sub

    Protected Sub txt_countrysubentity_cp_Changed(sender As Object, e As EventArgs)
        Dim iscountrysub As String = Nothing
        If sender.value IsNot Nothing Then
            iscountrysub = sender.value.ToString
        Else
            iscountrysub = Nothing
        End If

        Dim adaisiaddressfixcp As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_buildingidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_suiteidentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_flooridentifier_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_districtname_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_pob_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If
        If Not String.IsNullOrEmpty(txt_postcode_cp.Value) Then
            adaisiaddressfixcp = adaisiaddressfixcp + 1
        End If


        If adaisiaddressfixcp > 0 Then
        Else
            If iscountrysub IsNot Nothing Then
                txt_city_cp.AllowBlank = False
                txt_city_cp.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_cp.AllowBlank = True
                txt_city_cp.FieldStyle = "background-color:#ffffff"
            End If
        End If


    End Sub

    Protected Sub txt_street_Group_Changed(sender As Object, e As EventArgs)

        Dim isistreet As String = Nothing
        If sender.value IsNot Nothing Then
            isistreet = sender.value.ToString
        Else
            isistreet = Nothing
        End If
        Dim adaisistreetfixnya As Integer = 0

        If Not String.IsNullOrEmpty(txt_buildingindentifier_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_suiteidentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_flooridentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_districtname_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_pob_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_postcode_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_countrysubentity_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If

        If adaisistreetfixnya > 0 Then
        Else
            If isistreet IsNot Nothing Then
                txt_city_group.AllowBlank = False
                txt_city_group.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_group.AllowBlank = True
                txt_city_group.FieldStyle = "background-color:#ffffff"
            End If
        End If

    End Sub

    Protected Sub txt_buildingindentifier_Group_Changed(sender As Object, e As EventArgs)
        Dim isbuilding As String = Nothing
        If sender.value IsNot Nothing Then
            isbuilding = sender.value.ToString
        Else
            isbuilding = Nothing
        End If

        Dim adaisistreetfixnya As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If

        If Not String.IsNullOrEmpty(txt_suiteidentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_flooridentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_districtname_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_pob_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_postcode_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_countrysubentity_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If

        If adaisistreetfixnya > 0 Then
        Else
            If isbuilding IsNot Nothing Then
                txt_city_group.AllowBlank = False
                txt_city_group.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_group.AllowBlank = True
                txt_city_group.FieldStyle = "background-color:#ffffff"
            End If
        End If


    End Sub

    Protected Sub txt_suiteidentifier_group_Changed(sender As Object, e As EventArgs)
        Dim issuite As String = Nothing
        If sender.value IsNot Nothing Then
            issuite = sender.value.ToString
        Else
            issuite = Nothing
        End If

        Dim adaisistreetfixnya As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_buildingindentifier_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If

        If Not String.IsNullOrEmpty(txt_flooridentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_districtname_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_pob_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_postcode_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_countrysubentity_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If

        If adaisistreetfixnya > 0 Then
        Else
            If issuite IsNot Nothing Then
                txt_city_group.AllowBlank = False
                txt_city_group.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_group.AllowBlank = True
                txt_city_group.FieldStyle = "background-color:#ffffff"
            End If
        End If


    End Sub

    Protected Sub txt_flooridentifier_group_Changed(sender As Object, e As EventArgs)
        Dim isfloor As String = Nothing
        If sender.value IsNot Nothing Then
            isfloor = sender.value.ToString
        Else
            isfloor = Nothing
        End If

        Dim adaisistreetfixnya As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_buildingindentifier_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_suiteidentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_districtname_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If

        If Not String.IsNullOrEmpty(txt_pob_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_postcode_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_countrysubentity_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If

        If adaisistreetfixnya > 0 Then
        Else
            If isfloor IsNot Nothing Then
                txt_city_group.AllowBlank = False
                txt_city_group.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_group.AllowBlank = True
                txt_city_group.FieldStyle = "background-color:#ffffff"
            End If
        End If


    End Sub

    Protected Sub txt_districtname_Group_Changed(sender As Object, e As EventArgs)
        Dim isdistrict As String = Nothing
        If sender.value IsNot Nothing Then
            isdistrict = sender.value.ToString
        Else
            isdistrict = Nothing
        End If

        Dim adaisistreetfixnya As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_buildingindentifier_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_suiteidentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_flooridentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If

        If Not String.IsNullOrEmpty(txt_pob_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_postcode_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_countrysubentity_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If

        If adaisistreetfixnya > 0 Then
        Else
            If isdistrict IsNot Nothing Then
                txt_city_group.AllowBlank = False
                txt_city_group.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_group.AllowBlank = True
                txt_city_group.FieldStyle = "background-color:#ffffff"
            End If
        End If


    End Sub

    Protected Sub txt_pob_group_Changed(sender As Object, e As EventArgs)
        Dim ispob As String = Nothing
        If sender.value IsNot Nothing Then
            ispob = sender.value.ToString
        Else
            ispob = Nothing
        End If

        Dim adaisistreetfixnya As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_buildingindentifier_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_suiteidentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_flooridentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_districtname_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If

        If Not String.IsNullOrEmpty(txt_postcode_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_countrysubentity_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If

        If adaisistreetfixnya > 0 Then
        Else
            If ispob IsNot Nothing Then
                txt_city_group.AllowBlank = False
                txt_city_group.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_group.AllowBlank = True
                txt_city_group.FieldStyle = "background-color:#ffffff"
            End If
        End If


    End Sub

    Protected Sub txt_postcode_group_Changed(sender As Object, e As EventArgs)
        Dim ispostcode As String = Nothing
        If sender.value IsNot Nothing Then
            ispostcode = sender.value.ToString
        Else
            ispostcode = Nothing
        End If

        Dim adaisistreetfixnya As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_buildingindentifier_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_suiteidentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_flooridentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_districtname_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_pob_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If

        If Not String.IsNullOrEmpty(txt_countrysubentity_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If

        If adaisistreetfixnya > 0 Then
        Else

            If ispostcode IsNot Nothing Then
                txt_city_group.AllowBlank = False
                txt_city_group.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_group.AllowBlank = True
                txt_city_group.FieldStyle = "background-color:#ffffff"
            End If
        End If

    End Sub

    Protected Sub txt_countrysubentity_group_Changed(sender As Object, e As EventArgs)
        Dim iscountrysub As String = Nothing
        If sender.value IsNot Nothing Then
            iscountrysub = sender.value.ToString
        Else
            iscountrysub = Nothing
        End If


        Dim adaisistreetfixnya As Integer = 0
        If Not String.IsNullOrEmpty(txt_street_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_buildingindentifier_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_suiteidentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_flooridentifier_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_districtname_Group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_pob_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If
        If Not String.IsNullOrEmpty(txt_postcode_group.Value) Then
            adaisistreetfixnya = adaisistreetfixnya + 1
        End If


        If adaisistreetfixnya > 0 Then
        Else
            If iscountrysub IsNot Nothing Then
                txt_city_group.AllowBlank = False
                txt_city_group.FieldStyle = "background-color:#FFE4C4"
            Else
                txt_city_group.AllowBlank = True
                txt_city_group.FieldStyle = "background-color:#ffffff"
            End If
        End If


    End Sub



    Protected Sub SetReadOnlyWindowReportingGroup(isReadOnly As Boolean)
        Try


            'General Information
            txt_CorDocTypeIndicator.ReadOnly = isReadOnly
            chk_IsIndividualGroup.ReadOnly = isReadOnly
            cmb_ResCountryCode.IsReadOnly = isReadOnly


            'Account Infromation Group
            txt_accountnumber_group.ReadOnly = isReadOnly
            cmb_AccountType_Group.IsReadOnly = isReadOnly
            chk_undocument_Group.ReadOnly = isReadOnly
            chk_closedaccount_Group.ReadOnly = isReadOnly
            chk_dormantaccount_Group.ReadOnly = isReadOnly
            cmb_AccountCurrency_Group.IsReadOnly = isReadOnly
            txt_accountbalance_Group.ReadOnly = isReadOnly
            cmb_AccountHolderType.IsReadOnly = isReadOnly

            'Individual Information Group
            txt_name_Group.ReadOnly = isReadOnly
            'cmb_NameType_Group.IsReadOnly = isReadOnly
            txt_precedingtitle_group.ReadOnly = isReadOnly
            txt_title_group.ReadOnly = isReadOnly
            txt_firstname_group.ReadOnly = isReadOnly
            'txt_firstname_type_group.ReadOnly = isReadOnly
            txt_middlename_group.ReadOnly = isReadOnly
            ''txt_middlename_type_group.ReadOnly = isReadOnly
            txt_lastname_group.ReadOnly = isReadOnly
            ''txt_lastname_type_group.ReadOnly = isReadOnly
            txt_nameprefix_group.ReadOnly = isReadOnly
            txt_generationidentifier_group.ReadOnly = isReadOnly
            txt_namesufix_group.ReadOnly = isReadOnly
            txt_generalsufix_group.ReadOnly = isReadOnly
            datebirthdate_group.ReadOnly = isReadOnly
            txt_birthcity_group.ReadOnly = isReadOnly
            txt_birthcitysubentity_group.ReadOnly = isReadOnly
            cmb_birthcountrycode_group.IsReadOnly = isReadOnly
            cmb_nationality1_group.IsReadOnly = isReadOnly
            cmb_nationality2_group.IsReadOnly = isReadOnly
            cmb_nationality3_group.IsReadOnly = isReadOnly
            cmb_nationality4_group.IsReadOnly = isReadOnly
            cmb_nationality5_group.IsReadOnly = isReadOnly

            'TIN Information Group
            txt_tin_group.ReadOnly = isReadOnly
            cmb_tinissuedby_group.IsReadOnly = isReadOnly
            cmb_org_tinissuedby_group.IsReadOnly = isReadOnly

            'Address Information Group
            cmb_addresslegaltype_group.IsReadOnly = isReadOnly
            cmb_countrycode_Group.IsReadOnly = isReadOnly
            txt_addressfree_group.ReadOnly = isReadOnly
            txt_street_group.ReadOnly = isReadOnly
            txt_buildingindentifier_Group.ReadOnly = isReadOnly
            txt_suiteidentifier_group.ReadOnly = isReadOnly
            txt_flooridentifier_group.ReadOnly = isReadOnly
            txt_districtname_Group.ReadOnly = isReadOnly
            txt_pob_group.ReadOnly = isReadOnly
            txt_postcode_group.ReadOnly = isReadOnly
            txt_city_group.ReadOnly = isReadOnly
            txt_countrysubentity_group.ReadOnly = isReadOnly

            'Controlling Person Information
            cmb_restcountrycode_cp.IsReadOnly = isReadOnly
            cmb_controllingpersontype_cp.IsReadOnly = isReadOnly
            txt_firstname_cp.ReadOnly = isReadOnly
            'txt_firstnametype_cp.ReadOnly = isReadOnly
            txt_middlename_cp.ReadOnly = isReadOnly
            'txt_middlenametype_cp.ReadOnly = isReadOnly
            txt_lastname_cp.ReadOnly = isReadOnly
            'txt_lastnametype_cp.ReadOnly = isReadOnly
            txt_nameprefix_cp.ReadOnly = isReadOnly
            txt_namesufix_cp.ReadOnly = isReadOnly
            txt_generalsufix_cp.ReadOnly = isReadOnly
            'cmb_nametype_cp.IsReadOnly = isReadOnly
            txt_precedingtitle_cp.ReadOnly = isReadOnly
            txt_title_cp.ReadOnly = isReadOnly
            DateBirthDate_cp.ReadOnly = isReadOnly
            cmb_nationality_cp.IsReadOnly = isReadOnly

            'TIN Controlling Person Information
            txt_tin_cp.ReadOnly = isReadOnly
            cmb_tinissuedby_cp.IsReadOnly = isReadOnly

            'Address Controlling Person Information
            cmb_legaladdresstype_cp.IsReadOnly = isReadOnly
            cmb_countrycode_cp.IsReadOnly = isReadOnly
            txt_addressfree_cp.ReadOnly = isReadOnly
            txt_street_cp.ReadOnly = isReadOnly
            txt_buildingidentifier_cp.ReadOnly = isReadOnly
            txt_suiteidentifier_cp.ReadOnly = isReadOnly
            txt_flooridentifier_cp.ReadOnly = isReadOnly
            txt_districtname_cp.ReadOnly = isReadOnly
            txt_pob_cp.ReadOnly = isReadOnly
            txt_postcode_cp.ReadOnly = isReadOnly
            txt_city_cp.ReadOnly = isReadOnly
            txt_countrysubentity_cp.ReadOnly = isReadOnly

            'Payment Information
            cmb_paymenttype.IsReadOnly = isReadOnly
            cmb_paymentcurrencycode.IsReadOnly = isReadOnly
            txt_paymentamount.ReadOnly = isReadOnly

            ''Action Grid Payment
            'cc_payment.Hidden = isReadOnly
            If isReadOnly = False Then
                cmb_DocTypeIndicator.StringFieldStyle = "background-color:#FFE4C4"
                cmb_AccountType_Group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_AccountCurrency_Group.StringFieldStyle = "background-color:#FFE4C4"
                cmb_AccountHolderType.StringFieldStyle = "background-color:#FFE4C4"
                cmb_NameType_Group.StringFieldStyle = "background-color:#FFE4C4"
                cmb_birthcountrycode_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality1_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality2_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality3_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality4_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality5_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_tinissuedby_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_addresslegaltype_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_countrycode_Group.StringFieldStyle = "background-color:#FFE4C4"
                cmb_controllingpersontype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nametype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nationality_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_ResCountryCode.StringFieldStyle = "background-color:#FFE4C4"
                cmb_tinissuedby_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_legaladdresstype_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_countrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_paymenttype.StringFieldStyle = "background-color:#FFE4C4"
                cmb_paymentcurrencycode.StringFieldStyle = "background-color:#FFE4C4"
                cmb_org_nametype.StringFieldStyle = "background-color:#FFE4C4"
                cmb_org_tintype.StringFieldStyle = "background-color:#FFFFFF"
                cmb_org_tinissuedby_group.StringFieldStyle = "background-color:#FFFFFF"
            Else
                cmb_DocTypeIndicator.StringFieldStyle = "background-color:#FFE4C4"
                cmb_AccountType_Group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_AccountCurrency_Group.StringFieldStyle = "background-color:#FFE4C4"
                cmb_AccountHolderType.StringFieldStyle = "background-color:#FFE4C4"
                cmb_NameType_Group.StringFieldStyle = "background-color:#FFE4C4"
                cmb_birthcountrycode_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality1_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality2_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality3_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality4_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality5_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_tinissuedby_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_addresslegaltype_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_countrycode_Group.StringFieldStyle = "background-color:#FFE4C4"
                cmb_controllingpersontype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nametype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nationality_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_ResCountryCode.StringFieldStyle = "background-color:#FFE4C4"
                cmb_tinissuedby_cp.StringFieldStyle = "background-color:#FFFFF"
                cmb_legaladdresstype_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_countrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_paymenttype.StringFieldStyle = "background-color:#FFE4C4"
                cmb_paymentcurrencycode.StringFieldStyle = "background-color:#FFE4C4"
                cmb_org_nametype.StringFieldStyle = "background-color:#FFE4C4"
                cmb_org_tintype.StringFieldStyle = "background-color:#FFFFFF"
                cmb_org_tinissuedby_group.StringFieldStyle = "background-color:#FFFFFF"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindReportingGroup()
        Try
            If CRS_Report_Class.list_CRS_Reporting_Group IsNot Nothing Then
                Dim query1 As String = "select PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID,AccountNumber,Account_AcctNumberType, " &
                " case when IsIndividualYN = 1 then 'Yes' when IsIndividualYN = 0 then 'No' end as IsIndividualYN,AccountBalance," &
                " case when IsValid = 1 then 'Yes' when IsValid = 0 then 'No' end as IsValid,DocSpec_DocTypeIndic " &
                " from CRS_INTERNATIONAL_REPORTING_GROUP where FK_CRS_INTERNATIONAL_REPORT_HEADER_ID =" & CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID.ToString
                'Dim dtReportingGroup As DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Class.list_CRS_Reporting_Group.OrderBy(Function(x) x.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID).ToList)
                Dim dtReportingGroup As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query1, Nothing)

                'Get Module ID by Module Name
                Dim intModuleID As Integer = 50053
                Dim objModuleCek As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("CRS_INTERNATIONAL_REPORTING_GROUP")
                If objModuleCek IsNot Nothing Then
                    intModuleID = objModuleCek.PK_Module_ID
                End If

                'Get status from count of Validation Report
                strSQL = "SELECT KeyFieldValue FROM CRS_International_ValidationReport"
                strSQL += " WHERE ModuleID = " & intModuleID     '13243 adalah module ID untuk CRS_INTERNATIONAL_REPORTING_GROUP
                strSQL += " And RecordID = " & IDUnik

                Dim dtCRS_International_ValidationReport As DataTable = CRS_Global_BLL.getDataTableByQuery(strSQL)

                'For Each row As DataRow In dtReportingGroup.Rows
                '    If dtCRS_International_ValidationReport IsNot Nothing Then

                '        If row("IsIndividualYN") = "1" Then
                '            row("IsIndividualYN") = "Yes"
                '        Else
                '            row("IsIndividualYN") = "No"
                '        End If
                '    End If
                'Next

                'Bind to gridpanel
                gp_Report_Group.GetStore.DataSource = dtReportingGroup
                gp_Report_Group.GetStore.DataBind()

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_Report_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim strIsDeleteDraft As String = e.ExtraParams(0).Value

            'Keep or Delete Draft before Exit
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID

            'If strIsDeleteDraft = "1" Then
            If strIsDeleteDraft = "1" OrElse (CRS_Report_Class.list_CRS_Reporting_Group Is Nothing OrElse CRS_Report_Class.list_CRS_Reporting_Group.Count = 0) Then
                CRS_Report_BLL.DeleteReportByReportID(CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, strUserID, strUserID)
            End If

            'Back to form View
            Dim strEncryptedModuleID = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & strEncryptedModuleID)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & strEncryptedModuleID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btn_Report_Group_Add_Click(sender As Object, e As DirectEventArgs)
        Try
            Me.IsNewRecord = True
            txtwarningaccount.Hidden = True

            If Session("ReportHeaderdanFIsudahDiSave") = 0 Then
                Dim param(2) As SqlParameter

                If String.IsNullOrEmpty(cmb_ReceivingCountry.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_ReceivingCountry.Label + " is required.")
                End If

                If txtReportingPeriod.SelectedDate = DateTime.MinValue Then
                    Throw New ApplicationException(txtReportingPeriod.FieldLabel + " is required.")
                End If

                param(0) = New SqlParameter
                param(0).ParameterName = "@ReportingYear"
                param(0).Value = txtReportingPeriod.Value
                param(0).SqlDbType = SqlDbType.VarChar



                param(1) = New SqlParameter
                param(1).ParameterName = "@ToCountry"
                param(1).Value = cmb_ReceivingCountry.SelectedItemValue
                param(1).SqlDbType = SqlDbType.VarChar

                If String.IsNullOrEmpty(cmb_LJK.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_LJK.Label + " is required.")
                End If

                param(2) = New SqlParameter
                param(2).ParameterName = "@LJKTYPE"
                param(2).Value = cmb_LJK.SelectedItemValue
                param(2).SqlDbType = SqlDbType.VarChar


                Dim mssgrefid As String = ""
                mssgrefid = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Get_New_MessageRefID", param)

                txtMessageRefID.Text = mssgrefid

                If String.IsNullOrEmpty(cmb_LJK.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_LJK.Label + " is required.")
                End If
                If String.IsNullOrEmpty(txtMessageRefID.Value) Then
                    Throw New ApplicationException(txtMessageRefID.FieldLabel + " is required.")
                End If
                'cek apakah data LJK,Periode, dan Receiving Country Sama
                Dim param22(1) As SqlParameter

                param22(0) = New SqlParameter
                param22(0).ParameterName = "@MessageRefID"
                param22(0).Value = txtMessageRefID.Value
                param22(0).SqlDbType = SqlDbType.VarChar

                param22(1) = New SqlParameter
                param22(1).ParameterName = "@LJK"
                param22(1).Value = cmb_LJK.SelectedItemValue
                param22(1).SqlDbType = SqlDbType.VarChar


                Dim docrefid_seq As Integer = 0
                docrefid_seq = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Get_New_DocRefID_ReportFI_Seq", param22)

                If docrefid_seq > 0 Then
                    txtdisplay.Hidden = False
                    txtdisplay.Value = "* This CRS International With LJK Type, Receiving Country and Report Periode Already Exist In CRS International Report FI"
                    txtdisplay.FieldStyle = "color:red"
                Else
                    txtdisplay.Hidden = True
                End If

                'Doc Ref ID Reporting FI
                Dim param3(1) As SqlParameter

                param3(0) = New SqlParameter
                param3(0).ParameterName = "@MessageRefID"
                param3(0).Value = txtMessageRefID.Value
                param3(0).SqlDbType = SqlDbType.VarChar

                param3(1) = New SqlParameter
                param3(1).ParameterName = "@LJK"
                param3(1).Value = cmb_LJK.SelectedItemValue
                param3(1).SqlDbType = SqlDbType.VarChar


                Dim docrefid_FI As String = ""
                docrefid_FI = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Get_New_DocRefID_ReportFI", param3)

                txtDocRefID.Text = docrefid_FI
                Session("ReportHeaderdanFIsudahDiSave") = 1
            End If

            'Validate Report first, because once user click save Reporting Group, report must be saved also
            ValidateReport()

            'Save Report as Draft (Status 0) to Get PK_Report_ID, so it can be assigned to all report Hierarchy
            'Populate Data
            PopulateReportData()
            Session("AddNewGroup") = 1
            Dim objdoctype As New vw_CRS_International_Doc_Type_Indic
            Using objdb As New CRSDAL.CRSEntities
                objdoctype = objdb.vw_CRS_International_Doc_Type_Indic.Where(Function(x) x.PK_CRS_INTERNATIONAL_DOC_TYPE_INDIC_ID = 2).FirstOrDefault
            End Using

            With objdoctype
                cmb_DocTypeIndicator.SetTextWithTextValue(.FK_CRS_DOC_TYPE_INDIC_CODE, .DOC_TYPE_INDIC_NAME)
            End With
            With CRS_Report_Class.obj_CRS_Reporting_Header
                .Status = 0     'Saved as Draft
                .isValid = True
            End With

            Dim lngReportID As Long = CRS_Report_BLL.SaveReportAdd(ObjModule, CRS_Report_Class)
            CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = lngReportID
            Session("PKReportHeaderID") = Nothing
            SetReportStatusAndValidationResult()
            btn_Report_Back_DeleteDraft.Hidden = False
            Session("PKReportHeaderID") = lngReportID
            'Clean Window Reporting Group
            CleanWindowReportingGroup()
            cmb_NameType_Group.SetTextWithTextValue("OECD202", "OECD202 - Individu")
            'Create new reporting group Class
            If CRS_Report_Class.list_CRS_Reporting_Group Is Nothing Then
                CRS_Report_Class.list_CRS_Reporting_Group = New List(Of CRSDAL.CRS_INTERNATIONAL_REPORTING_GROUP)
            End If

            Dim lngPK As Long = 0
            If CRS_Report_Class.list_CRS_Reporting_Group.Count = 0 Then
                lngPK = -1
            Else
                lngPK = CRS_Report_Class.list_CRS_Reporting_Group.Min(Function(x) x.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID) - 1
                If lngPK >= 0 Then
                    lngPK = -1
                End If
            End If

            CRS_Report_Group_Class = New CRSBLL.CRS_Report_BLL.CRS_Reporting_Group_Class
            With CRS_Report_Group_Class.obj_Reporting_Group
                .PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID = -1
                .FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID
            End With

            'Doc Ref ID Reporting Group
            Dim param2(2) As SqlParameter

            param2(0) = New SqlParameter
            param2(0).ParameterName = "@MessageRefID"
            param2(0).Value = txtMessageRefID.Value
            param2(0).SqlDbType = SqlDbType.VarChar

            param2(1) = New SqlParameter
            param2(1).ParameterName = "@LJK"
            param2(1).Value = cmb_LJK.SelectedItemValue
            param2(1).SqlDbType = SqlDbType.VarChar


            Dim seq As Integer = 0

            seq = CRS_Report_Class.list_CRS_Reporting_Group.Count

            seq = 0

            param2(2) = New SqlParameter
            param2(2).ParameterName = "@Seq"
            param2(2).Value = seq
            param2(2).SqlDbType = SqlDbType.Int


            Dim docrefid_Group As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Get_New_DocRefID_ReportGroup", param2)
            SetReadOnlyWindowReportingGroup(False)
            btn_ReportingGroup_Save.Hidden = False
            txt_DocRefIDGroup.Value = docrefid_Group
            chk_IsIndividualGroup.Checked = True
            pnl_Organization_Information_group.Hidden = True
            pnl_individualinformation_Group.Hidden = False
            cmb_AccountHolderType.IsHidden = True
            btn_Payment_Add.Hidden = False
            Pnl_payment_Detail.Hidden = True
            pnl_payment.Hidden = False

            panelcpdetail.Hidden = True
            panelCP.Hidden = True

            cmb_org_tintype.IsHidden = True
            cmb_org_tinissuedby_group.IsHidden = True
            txt_tin_group.FieldLabel = "TIN"
            cmb_tinissuedby_group.IsHidden = False

            window_reporting_group.Title = "Reporting Group - Add"
            window_reporting_group.Hidden = False
            window_reporting_group.Body.ScrollTo(Direction.Top, 0)

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub



    Protected Sub ValidateReport()
        Try
            If String.IsNullOrEmpty(txtSendingCompany.Value) Then
                Throw New ApplicationException(txtSendingCompany.FieldLabel + " is required.")
            ElseIf String.IsNullOrEmpty(txtTransmittingCountry.Value) Then
                Throw New ApplicationException(txtTransmittingCountry.FieldLabel + " is required.")
            ElseIf String.IsNullOrEmpty(cmb_ReceivingCountry.SelectedItemValue) Then
                Throw New ApplicationException(cmb_ReceivingCountry.Label + " is required.")
            ElseIf String.IsNullOrEmpty(txtMessageType.Value) Then
                Throw New ApplicationException(txtMessageType.FieldLabel + " is required.")
            ElseIf String.IsNullOrEmpty(txtContact.Value) Then
                Throw New ApplicationException(txtContact.FieldLabel + " is required.")
            ElseIf String.IsNullOrEmpty(txtMessageRefID.Value) Then
                Throw New ApplicationException(txtMessageRefID.FieldLabel + " is required.")
            ElseIf String.IsNullOrEmpty(cmb_DocTypeIndic.SelectedItemValue) Then
                Throw New ApplicationException(cmb_DocTypeIndic.Label + " is required.")
            ElseIf String.IsNullOrEmpty(txtDocRefID.Value) Then
                Throw New ApplicationException(txtDocRefID.FieldLabel + " is required.")
            ElseIf String.IsNullOrEmpty(txtResCountryCode.Value) Then
                Throw New ApplicationException(txtResCountryCode.FieldLabel + " is required.")
            ElseIf String.IsNullOrEmpty(txtIN.Value) Then
                Throw New ApplicationException(txtIN.FieldLabel + " is required.")
            ElseIf String.IsNullOrEmpty(txtIN_IssueBy.Value) Then
                Throw New ApplicationException(txtIN_IssueBy.FieldLabel + " is required.")
            ElseIf String.IsNullOrEmpty(txtIN_INType.Value) Then
                Throw New ApplicationException(txtIN_INType.FieldLabel + " is required.")
            ElseIf String.IsNullOrEmpty(txtName.Value) Then
                Throw New ApplicationException(txtName.FieldLabel + " is required.")
            ElseIf String.IsNullOrEmpty(cmb_MessageTypeInd.SelectedItemValue) Then
                Throw New ApplicationException(cmb_MessageTypeInd.Label + " is required.")
            ElseIf txtReportingPeriod.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException(txtReportingPeriod.FieldLabel + " is required.")
            ElseIf txtTimeStamp.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException(txtTimeStamp.FieldLabel + " is required.")
            End If

            If txtDocRefID.RawValue = "OECD2" Then
                If String.IsNullOrEmpty(txtCorDocRefID.Value) Then
                    Throw New ApplicationException(txtCorDocRefID.FieldLabel + " is required.")
                End If
            End If
            If String.IsNullOrEmpty(txtIN.Value) Then
                Throw New ApplicationException(txtIN.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtIN_IssueBy.Value) Then
                Throw New ApplicationException(txtIN_IssueBy.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtIN_INType.Value) Then
                Throw New ApplicationException(txtIN_INType.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtAddress_CountryCode.Value) Then
                Throw New ApplicationException(txtAddress_CountryCode.FieldLabel + " is required.")
            End If
            If String.IsNullOrEmpty(txtAddress_AddressFree.Value) Then
                Throw New ApplicationException(txtAddress_AddressFree.FieldLabel + " is required.")
            End If
            If Not String.IsNullOrEmpty(txtAddress_AddressFix_Street.Value) Then
                If String.IsNullOrEmpty(txtAddress_AddressFix_City.Value) Then
                    Throw New ApplicationException(txtAddress_AddressFix_City.FieldLabel + " is required.")
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub CleanWindowReportingGroup()
        Try
            'Clean Validation Result
            Dim strValidationResult As String = ""
            NawaDevBLL.NawaFramework.extInfoPanelupdate(fpMain, strValidationResult, "info_ValidationResultReportingGroup")
            info_ValidationResultReportingGroup.Hidden = True

            'General Information
            Dim objdoctype As New vw_CRS_International_Doc_Type_Indic
            Using objdb As New CRSDAL.CRSEntities
                objdoctype = objdb.vw_CRS_International_Doc_Type_Indic.Where(Function(x) x.PK_CRS_INTERNATIONAL_DOC_TYPE_INDIC_ID = 2).FirstOrDefault
            End Using

            With objdoctype
                cmb_DocTypeIndicator.SetTextWithTextValue(.FK_CRS_DOC_TYPE_INDIC_CODE, .DOC_TYPE_INDIC_NAME)
            End With

            txt_CorDocTypeIndicator.Value = Nothing
            chk_IsIndividualGroup.Checked = False
            cmb_ResCountryCode.SetTextValue("")

            'Account Infromation Group
            txt_accountnumber_group.Value = Nothing
            cmb_AccountType_Group.SetTextValue("")
            chk_undocument_Group.Checked = False
            chk_closedaccount_Group.Checked = False
            chk_dormantaccount_Group.Checked = False
            cmb_AccountCurrency_Group.SetTextValue("")
            txt_accountbalance_Group.Value = Nothing
            cmb_AccountHolderType.SetTextValue("")

            'Organtizaion Information Group
            txt_org_Name.Value = Nothing
            cmb_org_nametype.SetTextValue("")
            cmb_org_tintype.SetTextValue("")
            cmb_org_tinissuedby_group.SetTextValue("")

            'Individual Information Group
            txt_name_Group.Value = Nothing
            cmb_NameType_Group.SetTextValue("")
            txt_precedingtitle_group.Value = Nothing
            txt_title_group.Value = Nothing
            txt_firstname_group.Value = Nothing
            'txt_firstname_type_group.Value = Nothing
            txt_middlename_group.Value = Nothing
            ''txt_middlename_type_group.Value = Nothing
            txt_lastname_group.Value = Nothing
            ''txt_lastname_type_group.Value = Nothing
            txt_nameprefix_group.Value = Nothing
            txt_generationidentifier_group.Value = Nothing
            txt_namesufix_group.Value = Nothing
            txt_generalsufix_group.Value = Nothing
            datebirthdate_group.Value = Nothing
            txt_birthcity_group.Value = Nothing
            txt_birthcitysubentity_group.Value = Nothing
            cmb_birthcountrycode_group.SetTextValue("")
            cmb_nationality1_group.SetTextValue("")
            cmb_nationality2_group.SetTextValue("")
            cmb_nationality3_group.SetTextValue("")
            cmb_nationality4_group.SetTextValue("")
            cmb_nationality5_group.SetTextValue("")

            'TIN Information Group
            txt_tin_group.Value = Nothing
            cmb_tinissuedby_group.SetTextValue("")

            'Address Information Group
            cmb_addresslegaltype_group.SetTextValue("")
            cmb_countrycode_Group.SetTextValue("")
            txt_addressfree_group.Value = Nothing
            txt_street_group.Value = Nothing
            txt_buildingindentifier_Group.Value = Nothing
            txt_suiteidentifier_group.Value = Nothing
            txt_flooridentifier_group.Value = Nothing
            txt_districtname_Group.Value = Nothing
            txt_pob_group.Value = Nothing
            txt_postcode_group.Value = Nothing
            txt_city_group.Value = Nothing
            txt_countrysubentity_group.Value = Nothing




            If Session("AddNewGroup") = 1 Then
                Dim dt_payment As New DataTable
                gp_payment.GetStore.DataSource = dt_payment
                gp_payment.GetStore.DataBind()
                gp_payment_detail.GetStore.DataSource = dt_payment
                gp_payment_detail.GetStore.DataBind()
                gp_cp.GetStore.DataSource = dt_payment
                gp_cp.GetStore.DataBind()
                gp_cp_Detail.GetStore.DataSource = dt_payment
                gp_cp_Detail.GetStore.DataBind()

                ''Payment Information
                'cmb_paymenttype.SetTextValue("")
                'cmb_paymentcurrencycode.SetTextValue("")
                'txt_paymentamount.Value = Nothing
                'Session("AddNewGroup") = Nothing

                'panel
                cmb_AccountHolderType.IsHidden = True
                pnl_Organization_Information_group.Hidden = True
                pnl_individualinformation_Group.Hidden = False
                panelCP.Hidden = True

            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    'Protected Sub txtDocRefID_Changed(sender As Object, e As EventArgs)
    '    Dim isidocref As String = sender.value.ToString
    '    If isidocref = "OECD2" Then
    '        txtCorDocRefID.Hidden = False
    '    Else
    '        txtCorDocRefID.Hidden = True
    '    End If
    'End Sub

    'Protected Sub txtDocTypeIndGroup_Changed(sender As Object, e As EventArgs)
    '    Dim isidocref As String = sender.value.ToString
    '    If isidocref = "OECD2" Then
    '        txtCorDocRefID.Hidden = False
    '    Else
    '        txtCorDocRefID.Hidden = True
    '    End If
    'End Sub

    Protected Sub gc_Reporting_Group(sender As Object, e As DirectEventArgs)
        Try

            If Me.ReadOnlyLevel = 0 Then
                Me.ReadOnlyLevel = 1
            End If

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            If strCommandName = "Delete" Then
                'To Do langsung delete dari Database
                CRS_Report_BLL.DeleteReportGroupByReportGroupID(ID)
                CRS_Report_Class.list_CRS_Reporting_Group.Remove(CRS_Report_Class.list_CRS_Reporting_Group.Where(Function(x) x.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID = ID).FirstOrDefault)

                'Hapus saja Validation Report untuk Reporting Group ID bersangkutan jika sudah tidak ada invalid data
                Dim strQuery As String = "DELETE a FROM CRS_International_ValidationReport a JOIN Module m on a.ModuleID = m.PK_Module_ID AND m.ModuleName='CRS_INTERNATIONAL_REPORTING_GROUP' WHERE a.RecordID=" & CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID & " AND a.KeyFieldValue=" & ID
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                BindReportingGroup()

                'SetReportStatusAndValidationResult()
            ElseIf strCommandName = "Edit" Then
                SetReadOnlyWindowReportingGroup(False)
                Session("AddNewGroup") = 0
                'ValidateReport()
                LoadReportingGroup("Edit", ID)
                btn_ReportingGroup_Save.Hidden = False
                Bind_Reporting_Payment()
                Bind_Reporting_cp()
                btn_Payment_Add.Hidden = False
                pnl_payment.Hidden = False
                Pnl_payment_Detail.Hidden = True
                If cmb_AccountHolderType.TextValue = "CRS101" Then
                    panelCP.Hidden = False
                    panelcpdetail.Hidden = True

                Else
                    panelCP.Hidden = True
                    panelcpdetail.Hidden = True
                End If
            ElseIf strCommandName = "Detail" Then
                SetReadOnlyWindowReportingGroup(True)
                Session("AddNewGroup") = 0
                'ValidateReport()
                LoadReportingGroup("Detail", ID)
                btn_Payment_Add.Hidden = True

                btn_ReportingGroup_Save.Hidden = True
                Bind_Reporting_Payment()
                Bind_Reporting_cp()
                pnl_payment.Hidden = True
                Pnl_payment_Detail.Hidden = False
                If cmb_AccountHolderType.TextValue = "CRS101" Then
                    panelCP.Hidden = True
                    panelcpdetail.Hidden = False

                Else
                    panelCP.Hidden = True
                    panelcpdetail.Hidden = True
                End If

            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadReportingGroup(strCommandName As String, strID As String)
        Try
            Me.IsNewRecord = False

            'Change window_s title
            window_reporting_group.Title = "Reporting Group - " & strCommandName
            CleanWindowReportingGroup()

            'Load Reporting Group By ID
            CRS_Report_Group_Class = CRS_Report_BLL.GetReportingGroupClassByID(strID)

            'Load Reporting Group Header
            If CRS_Report_Group_Class IsNot Nothing Then
                With CRS_Report_Group_Class.obj_Reporting_Group
                    'General Information
                    If Not IsNothing(.DocSpec_DocTypeIndic) Then
                        drTemp = CRS_Global_BLL.getDataRowByID("CRS_INTERNATIONAL_DOC_TYPE_INDIC", "FK_CRS_DOC_TYPE_INDIC_CODE", .DocSpec_DocTypeIndic)
                        If drTemp IsNot Nothing Then
                            cmb_DocTypeIndicator.SetTextWithTextValue(drTemp("FK_CRS_DOC_TYPE_INDIC_CODE"), drTemp("DOC_TYPE_INDIC_NAME"))
                        End If
                    End If
                    txt_CorDocTypeIndicator.Value = .DocSpec_CorrDocRefID
                    txt_DocRefIDGroup.Value = .DocSpec_DocRefID
                    chk_IsIndividualGroup.Checked = .IsIndividualYN
                    If .IsIndividualYN = True Then
                        If Not IsNothing(.INDV_ResCountryCode) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_ResCountryCode)
                            If drTemp IsNot Nothing Then
                                cmb_ResCountryCode.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                    Else
                        If Not IsNothing(.ORG_ResCountryCode) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .ORG_ResCountryCode)
                            If drTemp IsNot Nothing Then
                                cmb_ResCountryCode.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                    End If


                    'Account Infromation Group
                    txt_accountnumber_group.Value = .AccountNumber
                    If Not IsNothing(.Account_AcctNumberType) Then
                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_ACCOUNT_NUMBER_TYPE", "FK_CRS_ACCOUNT_NUMBER_TYPE_CODE", .Account_AcctNumberType)
                        If drTemp IsNot Nothing Then
                            cmb_AccountType_Group.SetTextWithTextValue(drTemp("FK_CRS_ACCOUNT_NUMBER_TYPE_CODE"), drTemp("ACCOUNT_NUMBER_TYPE_NAME"))
                        End If
                    End If
                    chk_undocument_Group.Checked = .Account_UndocumentedAccount
                    chk_closedaccount_Group.Checked = .Account_ClosedAccount
                    chk_dormantaccount_Group.Checked = .Account_DormantAccount
                    If Not IsNothing(.AccountBalance_CurrencyCode) Then
                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_CURRENCY", "FK_CURRENCY_CODE", .AccountBalance_CurrencyCode)
                        If drTemp IsNot Nothing Then
                            cmb_AccountCurrency_Group.SetTextWithTextValue(drTemp("FK_CURRENCY_CODE"), drTemp("CURRENCY_NAME"))
                        End If
                    End If

                    txt_accountbalance_Group.Value = .AccountBalance

                    'Individual Information Group
                    If .IsIndividualYN = True Then
                        cmb_AccountHolderType.IsHidden = True
                        pnl_Organization_Information_group.Hidden = True
                        pnl_individualinformation_Group.Hidden = False
                        panelCP.Hidden = True

                        txt_name_Group.Value = .INDV_Name
                        If Not IsNothing(.INDV_Name_Type) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_NAME_TYPE", "FK_CRS_NAME_TYPE_CODE", .INDV_Name_Type)
                            If drTemp IsNot Nothing Then
                                cmb_NameType_Group.SetTextWithTextValue(drTemp("FK_CRS_NAME_TYPE_CODE"), drTemp("NAME_TYPE_NAME"))
                            End If
                        End If
                        txt_precedingtitle_group.Value = .INDV_Name_PrecedingTitle
                        txt_title_group.Value = .INDV_Name_Title
                        txt_firstname_group.Value = .INDV_Name_FirstName
                        'txt_firstname_type_group.Value = .INDV_Name_FirstName_Type
                        txt_middlename_group.Value = .INDV_Name_MiddleName
                        'txt_middlename_type_group.Value = .INDV_Name_MiddleName_Type
                        txt_lastname_group.Value = .INDV_Name_LastName
                        'txt_lastname_type_group.Value = .INDV_Name_LastName_Type
                        txt_nameprefix_group.Value = .INDV_Name_NamePrefix
                        txt_generationidentifier_group.Value = .INDV_Name_GenerationIdentifier
                        txt_namesufix_group.Value = .INDV_Name_Suffix
                        txt_generalsufix_group.Value = .INDV_Name_GeneralSuffix
                        datebirthdate_group.Value = .INDV_BirthInfo_BirthDate
                        txt_birthcity_group.Value = .INDV_BirthInfo_City
                        txt_birthcitysubentity_group.Value = .INDV_BirthInfo_CitySubEntity
                        If Not IsNothing(.INDV_BirthInfo_CountryInfo_CountryCode) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_BirthInfo_CountryInfo_CountryCode)
                            If drTemp IsNot Nothing Then
                                cmb_birthcountrycode_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If

                        If Not IsNothing(.INDV_Nationality1) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality1)
                            If drTemp IsNot Nothing Then
                                cmb_nationality1_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Nationality2) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality2)
                            If drTemp IsNot Nothing Then
                                cmb_nationality2_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Nationality3) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality3)
                            If drTemp IsNot Nothing Then
                                cmb_nationality3_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Nationality4) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality4)
                            If drTemp IsNot Nothing Then
                                cmb_nationality4_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Nationality5) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality5)
                            If drTemp IsNot Nothing Then
                                cmb_nationality5_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                    Else
                        If .ORG_AcctHolderType = "CRS101" Then
                            cmb_AccountHolderType.IsHidden = False
                            pnl_Organization_Information_group.Hidden = False
                            pnl_individualinformation_Group.Hidden = True
                            panelCP.Hidden = False
                        Else
                            cmb_AccountHolderType.IsHidden = False
                            pnl_Organization_Information_group.Hidden = False
                            pnl_individualinformation_Group.Hidden = True
                            panelCP.Hidden = True
                        End If
                        If Not IsNothing(.ORG_AcctHolderType) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_HOLDER_TYPE", "FK_CRS_ACCOUNT_HOLDER_TYPE_CODE", .ORG_AcctHolderType)
                            If drTemp IsNot Nothing Then
                                cmb_AccountHolderType.SetTextWithTextValue(drTemp("FK_CRS_ACCOUNT_HOLDER_TYPE_CODE"), drTemp("ACCOUNT_HOLDER_TYPE_NAME"))
                            End If
                        End If


                        txt_org_Name.Value = .ORG_Name
                        If Not IsNothing(.ORG_Name_NameType) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_NAME_TYPE", "FK_CRS_NAME_TYPE_CODE", .ORG_Name_NameType)
                            If drTemp IsNot Nothing Then
                                cmb_org_nametype.SetTextWithTextValue(drTemp("FK_CRS_NAME_TYPE_CODE"), drTemp("NAME_TYPE_NAME"))
                            End If
                        End If
                        txt_precedingtitle_group.Value = Nothing
                        txt_title_group.Value = Nothing
                        txt_firstname_group.Value = Nothing
                        'txt_firstname_type_group.Value = Nothing
                        txt_middlename_group.Value = Nothing
                        'txt_middlename_type_group.Value = Nothing
                        txt_lastname_group.Value = Nothing
                        'txt_lastname_type_group.Value = Nothing
                        txt_nameprefix_group.Value = Nothing
                        txt_generationidentifier_group.Value = Nothing
                        txt_namesufix_group.Value = Nothing
                        txt_generalsufix_group.Value = Nothing
                        datebirthdate_group.Value = Nothing
                        txt_birthcity_group.Value = Nothing
                        txt_birthcitysubentity_group.Value = Nothing
                        cmb_birthcountrycode_group.SetTextValue("")

                        cmb_nationality1_group.SetTextValue("")
                        cmb_nationality2_group.SetTextValue("")
                        cmb_nationality3_group.SetTextValue("")
                        cmb_nationality4_group.SetTextValue("")
                        cmb_nationality5_group.SetTextValue("")

                    End If

                    'TIN Information Group
                    If .IsIndividualYN = True Then
                        cmb_org_tinissuedby_group.IsHidden = True
                        cmb_tinissuedby_group.IsHidden = False
                        cmb_org_tintype.IsHidden = True
                        txt_tin_group.Value = .INDV_TIN
                        If Not IsNothing(.INDV_TIN_ISSUEDBY) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_TIN_ISSUEDBY)
                            If drTemp IsNot Nothing Then
                                cmb_tinissuedby_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                    Else
                        cmb_org_tintype.IsHidden = False
                        cmb_org_tinissuedby_group.IsHidden = False
                        cmb_tinissuedby_group.IsHidden = True
                        'TIN Information Group
                        txt_tin_group.Value = .ORG_IN
                        If Not IsNothing(.ORG_IN_INType) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_IN_TYPE", "FK_CRS_IN_TYPE_CODE", .ORG_IN_INType)
                            If drTemp IsNot Nothing Then
                                cmb_org_tintype.SetTextWithTextValue(drTemp("FK_CRS_IN_TYPE_CODE"), drTemp("IN_TYPE_NAME"))
                            End If
                        End If
                        If Not IsNothing(.ORG_IN_IssuedBy) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .ORG_IN_IssuedBy)
                            If drTemp IsNot Nothing Then
                                cmb_org_tinissuedby_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If

                    End If


                    'Address Information Group
                    If Not IsNothing(.Address_LegalAddressType) Then
                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_LEGAL_ADDRESS", "FK_CRS_LEGAL_ADDRESS_TYPE_CODE", .Address_LegalAddressType)
                        If drTemp IsNot Nothing Then
                            cmb_addresslegaltype_group.SetTextWithTextValue(drTemp("FK_CRS_LEGAL_ADDRESS_TYPE_CODE"), drTemp("LEGAL_ADDRESS_TYPE_NAME"))
                        End If
                    End If
                    If Not IsNothing(.Address_CountryCode) Then
                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .Address_CountryCode)
                        If drTemp IsNot Nothing Then
                            cmb_countrycode_Group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                        End If
                    End If
                    txt_addressfree_group.Value = .Address_AddressFree
                    txt_street_group.Value = .Address_AddressFix_Street
                    If .Address_AddressFix_Street IsNot Nothing Then
                        txt_city_group.AllowBlank = False
                        txt_city_group.FieldStyle = "background-color:#FFE4C4"
                    End If
                    txt_buildingindentifier_Group.Value = .Address_AddressFix_BuildingIdentifier
                    txt_suiteidentifier_group.Value = .Address_AddressFix_SuiteIdentifier
                    txt_flooridentifier_group.Value = .Address_AddressFix_FloorIdentifier
                    txt_districtname_Group.Value = .Address_AddressFix_DistrictName
                    txt_pob_group.Value = .Address_AddressFix_POB
                    txt_postcode_group.Value = .Address_AddressFix_PostCode
                    txt_city_group.Value = .Address_AddressFix_City
                    txt_countrysubentity_group.Value = .Address_AddressFix_CountrySubentity

                    'Dim reportCP As New CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON
                    'Using objdb As New CRSDAL.CRSEntities
                    '    reportCP = objdb.CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON.Where(Function(x) x.FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = .PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID).FirstOrDefault
                    '    With reportCP
                    '        txt_restcountrycode_cp.Value = .ResCountryCode
                    '        If Not IsNothing(.CtrlgPersonType) Then
                    '            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_CP_TYPE", "FK_CRS_CONTROLLING_PERSON_TYPE_CODE", .CtrlgPersonType)
                    '            If drTemp IsNot Nothing Then
                    '                cmb_controllingpersontype_cp.SetTextWithTextValue(drTemp("FK_CRS_CONTROLLING_PERSON_TYPE_CODE"), drTemp("CONTROLLING_PERSON_TYPE_NAME"))
                    '            End If
                    '        End If
                    '        txt_firstname_cp.Value = .FirstName
                    '        txt_firstnametype_cp.Value = .FirstNameType
                    '        txt_middlename_cp.Value = .MiddleName
                    '        txt_middlenametype_cp.Value = .MiddleNameType
                    '        txt_lastname_cp.Value = .LastName
                    '        txt_lastnametype_cp.Value = .LastNameType
                    '        txt_nameprefix_cp.Value = .Name_NamePrefix
                    '        txt_namesufix_cp.Value = .Name_Suffix
                    '        txt_generalsufix_cp.Value = .Name_GeneralSuffix
                    '        If Not IsNothing(.NameType) Then
                    '            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_NAME_TYPE", "FK_CRS_NAME_TYPE_CODE", .NameType)
                    '            If drTemp IsNot Nothing Then
                    '                cmb_nametype_cp.SetTextWithTextValue(drTemp("FK_CRS_NAME_TYPE_CODE"), drTemp("NAME_TYPE_NAME"))
                    '            End If
                    '        End If
                    '        txt_precedingtitle_cp.Value = .Name_PrecedingTitle
                    '        txt_title_cp.Value = .Name_Title
                    '        DateBirthDate_cp.Value = .BirthDate
                    '        If Not IsNothing(.Nationality) Then
                    '            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .Nationality)
                    '            If drTemp IsNot Nothing Then
                    '                cmb_nationality_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                    '            End If
                    '        End If


                    '        'TIN Controlling Person Information
                    '        txt_tin_cp.Value = .TIN
                    '        If Not IsNothing(.IssuedBy) Then
                    '            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .IssuedBy)
                    '            If drTemp IsNot Nothing Then
                    '                cmb_tinissuedby_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                    '            End If
                    '        End If

                    '        'Address Controlling Person Information
                    '        If Not IsNothing(.legalAddressType) Then
                    '            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_LEGAL_ADDRESS", "FK_CRS_LEGAL_ADDRESS_TYPE_CODE", .legalAddressType)
                    '            If drTemp IsNot Nothing Then
                    '                cmb_legaladdresstype_cp.SetTextWithTextValue(drTemp("FK_CRS_LEGAL_ADDRESS_TYPE_CODE"), drTemp("LEGAL_ADDRESS_TYPE_NAME"))
                    '            End If
                    '        End If
                    '        If Not IsNothing(.CountryCode) Then
                    '            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .CountryCode)
                    '            If drTemp IsNot Nothing Then
                    '                cmb_countrycode_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                    '            End If
                    '        End If
                    '        txt_addressfree_cp.Value = .AddressFree
                    '        txt_street_cp.Value = .AddressFix_Street
                    '        If .AddressFix_Street IsNot Nothing Then
                    '            txt_city_cp.AllowBlank = False
                    '            txt_city_cp.FieldStyle = "background-color:#FFE4C4"
                    '        End If
                    '        txt_buildingidentifier_cp.Value = .AddressFix_BuildingIdentifier
                    '        txt_suiteidentifier_cp.Value = .AddressFix_SuiteIdentifier
                    '        txt_flooridentifier_cp.Value = .AddressFix_FloorIdentifier
                    '        txt_districtname_cp.Value = .AddressFix_DistrictName
                    '        txt_pob_cp.Value = .AddressFix_POB
                    '        txt_postcode_cp.Value = .AddressFix_PostCode
                    '        txt_city_cp.Value = .AddressFix_City
                    '        txt_countrysubentity_cp.Value = .AddressFix_CountrySubentity
                    '    End With
                    'End Using




                End With



                'Load Validation Result
                Dim strValidationResult As String = CRS_Report_BLL.getValidationReportByReportGroupID(CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID)
                If Not String.IsNullOrWhiteSpace(strValidationResult) Then
                    NawaDevBLL.NawaFramework.extInfoPanelupdate(fpMain, strValidationResult, "info_ValidationResultReportingGroup")
                    info_ValidationResultReportingGroup.Hidden = False
                Else
                    info_ValidationResultReportingGroup.Hidden = True
                End If
                txt_accountnumber_group_Changed(Nothing, Nothing)
                'Show window_ Reporting Group
                window_reporting_group.Hidden = False
                window_reporting_group.Body.ScrollTo(Direction.Top, 0)

            Else
                Throw New ApplicationException("Reporting Group cannot be loaded")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_ReportingGroup_Save_Click(sender As Object, e As DirectEventArgs)
        Try
            ValidateReportingGroup()

            'Save Report Dulu untuk mengamankan data2 SavedState untuk perubahan status pertama kali ke 9
            'Also populate Report data, because once user click save Reporting Group, report must be saved also (save data per Reporting Group for performance)
            PopulateReportData()
            'Set report status to 9 (In Progress Editing)
            With CRS_Report_Class.obj_CRS_Reporting_Header

                .Status = 0     'Draft
                .isValid = True
            End With

            Dim lngReportID As Long = CRS_Report_BLL.SaveReportAdd(ObjModule, CRS_Report_Class)
            CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = lngReportID

            Dim objCek = CRS_Report_Class.list_CRS_Reporting_Group.Where(Function(x) x.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID = CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID).FirstOrDefault

            'Save Reporting Group General Information
            With CRS_Report_Group_Class.obj_Reporting_Group

                'General Information
                .DocSpec_DocTypeIndic = cmb_DocTypeIndicator.SelectedItemValue
                .DocSpec_CorrDocRefID = txt_CorDocTypeIndicator.Value
                .IsIndividualYN = chk_IsIndividualGroup.Checked
                .DocSpec_DocRefID = txt_DocRefIDGroup.Value
                .IsChanges = 0
                .IsValid = True
                If chk_IsIndividualGroup.Checked = True Then
                    .INDV_ResCountryCode = cmb_ResCountryCode.SelectedItemValue
                    .ORG_ResCountryCode = Nothing
                Else
                    .ORG_ResCountryCode = cmb_ResCountryCode.SelectedItemValue
                    .INDV_ResCountryCode = Nothing
                End If


                'Account Infromation Group
                .AccountNumber = txt_accountnumber_group.Value
                .Account_AcctNumberType = cmb_AccountType_Group.SelectedItemValue
                .Account_UndocumentedAccount = chk_undocument_Group.Checked
                .Account_ClosedAccount = chk_closedaccount_Group.Checked
                .Account_DormantAccount = chk_dormantaccount_Group.Checked
                .AccountBalance_CurrencyCode = cmb_AccountCurrency_Group.SelectedItemValue
                .AccountBalance = txt_accountbalance_Group.Value

                'Individual Information Group
                If chk_IsIndividualGroup.Checked = True Then
                    .INDV_Name = txt_name_Group.Value
                    .INDV_Name_Type = cmb_NameType_Group.SelectedItemValue
                    .INDV_Name_PrecedingTitle = txt_precedingtitle_group.Value
                    .INDV_Name_Title = txt_title_group.Value
                    .INDV_Name_FirstName = txt_firstname_group.Value
                    '.INDV_Name_FirstName_Type = 'txt_firstname_type_group.Value
                    .INDV_Name_MiddleName = txt_middlename_group.Value
                    '.INDV_Name_MiddleName_Type = 'txt_middlename_type_group.Value
                    .INDV_Name_LastName = txt_lastname_group.Value
                    '.INDV_Name_LastName_Type = 'txt_lastname_type_group.Value
                    .INDV_Name_NamePrefix = txt_nameprefix_group.Value
                    .INDV_Name_GenerationIdentifier = txt_generationidentifier_group.Value
                    .INDV_Name_Suffix = txt_namesufix_group.Value
                    .INDV_Name_GeneralSuffix = txt_generalsufix_group.Value
                    .INDV_BirthInfo_BirthDate = datebirthdate_group.Value
                    .INDV_BirthInfo_City = txt_birthcity_group.Value
                    .INDV_BirthInfo_CitySubEntity = txt_birthcitysubentity_group.Value
                    .INDV_BirthInfo_CountryInfo_CountryCode = cmb_birthcountrycode_group.SelectedItemValue
                    .INDV_Nationality1 = cmb_nationality1_group.SelectedItemValue
                    .INDV_Nationality2 = cmb_nationality2_group.SelectedItemValue
                    .INDV_Nationality3 = cmb_nationality3_group.SelectedItemValue
                    .INDV_Nationality4 = cmb_nationality4_group.SelectedItemValue
                    .INDV_Nationality5 = cmb_nationality5_group.SelectedItemValue
                    .ORG_AcctHolderType = Nothing
                    .ORG_Name = Nothing
                    .ORG_Name_NameType = Nothing

                Else
                    .ORG_AcctHolderType = cmb_AccountHolderType.SelectedItemValue
                    .ORG_Name = txt_org_Name.Value
                    .ORG_Name_NameType = cmb_org_nametype.SelectedItemValue
                    .ORG_IN_INType = cmb_org_tintype.SelectedItemValue
                    .INDV_Name = Nothing
                    .INDV_Name_Type = Nothing
                    .INDV_Name_PrecedingTitle = Nothing
                    .INDV_Name_Title = Nothing
                    .INDV_Name_FirstName = Nothing
                    '.INDV_Name_FirstName_Type = 'txt_firstname_type_group
                    .INDV_Name_MiddleName = Nothing
                    '.INDV_Name_MiddleName_Type = 'txt_middlename_type_group
                    .INDV_Name_LastName = Nothing
                    '.INDV_Name_LastName_Type = 'txt_lastname_type_group
                    .INDV_Name_NamePrefix = Nothing
                    .INDV_Name_GenerationIdentifier = Nothing
                    .INDV_Name_Suffix = Nothing
                    .INDV_Name_GeneralSuffix = Nothing
                    .INDV_BirthInfo_BirthDate = Nothing
                    .INDV_BirthInfo_City = Nothing
                    .INDV_BirthInfo_CitySubEntity = Nothing
                    .INDV_BirthInfo_CountryInfo_CountryCode = Nothing
                    .INDV_Nationality1 = Nothing
                    .INDV_Nationality2 = Nothing
                    .INDV_Nationality3 = Nothing
                    .INDV_Nationality4 = Nothing
                    .INDV_Nationality5 = Nothing
                End If



                'TIN Information Group
                If chk_IsIndividualGroup.Checked = True Then
                    .INDV_TIN = txt_tin_group.Value
                    .INDV_TIN_ISSUEDBY = cmb_tinissuedby_group.SelectedItemValue
                Else
                    .ORG_IN = txt_tin_group.Value
                    .ORG_IN_IssuedBy = cmb_org_tinissuedby_group.SelectedItemValue
                    .ORG_IN_INType = cmb_org_tintype.SelectedItemValue
                    .ORG_IN_INType = cmb_org_tintype.SelectedItemValue
                End If


                'Address Information Group
                .Address_LegalAddressType = cmb_addresslegaltype_group.SelectedItemValue
                .Address_CountryCode = cmb_countrycode_Group.SelectedItemValue
                .Address_AddressFree = txt_addressfree_group.Value
                .Address_AddressFix_Street = txt_street_group.Value
                .Address_AddressFix_BuildingIdentifier = txt_buildingindentifier_Group.Value
                .Address_AddressFix_SuiteIdentifier = txt_suiteidentifier_group.Value
                .Address_AddressFix_FloorIdentifier = txt_flooridentifier_group.Value
                .Address_AddressFix_DistrictName = txt_districtname_Group.Value
                .Address_AddressFix_POB = txt_pob_group.Value
                .Address_AddressFix_PostCode = txt_postcode_group.Value
                .Address_AddressFix_City = txt_city_group.Value
                .Address_AddressFix_CountrySubentity = txt_countrysubentity_group.Value




                If objCek Is Nothing Then
                    .Active = 1
                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .CreatedDate = DateTime.Now
                Else
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateDate = DateTime.Now
                End If
            End With
            If CRS_Report_Group_Class.list_Reporting_RGARPayment IsNot Nothing Then
                If CRS_Report_Group_Class.list_Reporting_RGARPayment.Count = 0 Then
                    CRS_Report_Group_Class.list_Reporting_RGARPayment = Nothing
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_RG_AR_PAYMENT WHERE FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = " & CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID, Nothing)
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID.ToString
                    Dim namatable As String = "CRS_INTERNATIONAL_RG_AR_PAYMENT_" & user.ToString
                    Dim queryexist = " declare @nilai bigint " &
                                        " If object_id('" & namatable & "')" &
                                        "Is Not null " &
                                        " begin" &
                                        " set @nilai = 1 " &
                                        "   End " &
                                        " else " &
                                        " begin " &
                                        " set @nilai = 0 " &
                                        " end " &
                                        " select @nilai"
                    Dim exist As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, queryexist, Nothing)
                    If exist > 0 Then
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_RG_AR_PAYMENT_" & user.ToString & " WHERE FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = " & CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID, Nothing)
                    End If


                End If
            Else
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_RG_AR_PAYMENT WHERE FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = " & CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID, Nothing)
                Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID.ToString
                Dim namatable As String = "CRS_INTERNATIONAL_RG_AR_PAYMENT_" & user.ToString
                Dim queryexist = " declare @nilai bigint " &
                                        " If object_id('" & namatable & "')" &
                                        "Is Not null " &
                                        " begin" &
                                        " set @nilai = 1 " &
                                        "   End " &
                                        " else " &
                                        " begin " &
                                        " set @nilai = 0 " &
                                        " end " &
                                        " select @nilai"
                Dim exist As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, queryexist, Nothing)
                If exist > 0 Then
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_RG_AR_PAYMENT_" & user.ToString & " WHERE FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = " & CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID, Nothing)
                End If

            End If
            If CRS_Report_Group_Class.list_Reporting_RGARPerson IsNot Nothing Then
                If CRS_Report_Group_Class.list_Reporting_RGARPerson.Count = 0 Then
                    CRS_Report_Group_Class.list_Reporting_RGARPerson = Nothing
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON WHERE FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = " & CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID, Nothing)
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID.ToString
                    Dim namatable As String = "CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_" & user.ToString
                    Dim queryexist = " declare @nilai bigint " &
                                        " If object_id('" & namatable & "')" &
                                        "Is Not null " &
                                        " begin" &
                                        " set @nilai = 1 " &
                                        "   End " &
                                        " else " &
                                        " begin " &
                                        " set @nilai = 0 " &
                                        " end " &
                                        " select @nilai"
                    Dim exist As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, queryexist, Nothing)
                    If exist > 0 Then
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_" & user.ToString & " WHERE FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = " & CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID, Nothing)
                    End If
                End If
            Else
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON WHERE FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = " & CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID, Nothing)
                Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID.ToString
                Dim namatable As String = "CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_" & user.ToString
                Dim queryexist = " declare @nilai bigint " &
                                        " If object_id('" & namatable & "')" &
                                        "Is Not null " &
                                        " begin" &
                                        " set @nilai = 1 " &
                                        "   End " &
                                        " else " &
                                        " begin " &
                                        " set @nilai = 0 " &
                                        " end " &
                                        " select @nilai"
                Dim exist As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, queryexist, Nothing)
                If exist > 0 Then
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_" & user.ToString & " WHERE FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = " & CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID, Nothing)
                End If
            End If
            'If CRS_Report_Class.list_CRS_Reporting_Group IsNot Nothing Then
            '    Dim objreportgroup = CRS_Report_Class.list_CRS_Reporting_Group.Where(Function(x) x.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID = CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID).FirstOrDefault
            '    If objreportgroup IsNot Nothing Then
            '        Dim reportgroupindex = CRS_Report_Class.list_CRS_Reporting_Group.IndexOf(objreportgroup)
            '        Dim objreportgroup2 = CRS_Report_Group_Class.obj_Reporting_Group
            '        objreportgroup = objreportgroup2
            '        CRS_Report_Class.list_CRS_Reporting_Group(reportgroupindex) = objreportgroup2
            '    End If
            'End If


            Dim strValidationResult As String = CRS_Report_BLL.validateReportingGroup(CRS_Report_Class, CRS_Report_Group_Class)
            If Not String.IsNullOrWhiteSpace(strValidationResult) Then
                CRS_Report_Group_Class.obj_Reporting_Group.IsValid = False
                CRS_Report_Class.obj_CRS_Reporting_Header.isValid = False
                NawaDevBLL.NawaFramework.extInfoPanelupdate(fpMain, strValidationResult, "info_ValidationResultReportingGroup")
                window_reporting_group.Body.ScrollTo(Direction.Top, 0)
                info_ValidationResultReportingGroup.Hidden = False

                Throw New ApplicationException("The Reporting Group still have some invalid data.")
                Exit Sub
            Else
                CRS_Report_Group_Class.obj_Reporting_Group.IsValid = True
                CRS_Report_Class.obj_CRS_Reporting_Header.isValid = True
                info_ValidationResultReportingGroup.Hidden = True
            End If
            If CRS_Report_Group_Class.list_Reporting_RGARPerson Is Nothing Then
                CRS_Report_Group_Class.list_Reporting_RGARPerson = New List(Of CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON)
            End If
            'Save Reporting Group
            CRS_Report_BLL.SaveReportingGroup(ObjModule, CRS_Report_Group_Class, CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID)

            'ReLoad Object Report Class agar ID Reporting Group update
            CRS_Report_Class = CRS_Report_BLL.GetCRSINTReportClassByID(CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID)

            'Bind Reporting Group
            BindReportingGroup()

            'Hapus saja Validation Report untuk ID bersangkutan jika sudah tidak ada invalid data
            Dim strQuery As String = "DELETE a FROM CRS_International_ValidationReport a JOIN Module m on a.ModuleID = m.PK_Module_ID AND m.ModuleName='CRS_INTERNATIONAL_REPORTING_GROUP' WHERE a.RecordID=" & CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID & " AND a.KeyFieldValue=" & CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Set Report Status
            SetReportStatusAndValidationResult()

            'Hide Reporting Group Window
            window_reporting_group.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btn_ReportingGroup_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            CRS_Report_Group_Class.list_Reporting_RGARPayment = Nothing
            window_reporting_group.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ValidateReportingGroup()
        Try
            If chk_IsIndividualGroup.Checked = True Then
                If Not String.IsNullOrEmpty(txt_tin_group.Value) Then
                    If txt_tin_group.Value = "NOTIN" Or txt_tin_group.Value = "notin" Then
                    Else
                        If String.IsNullOrEmpty(cmb_tinissuedby_group.SelectedItemValue) Then
                            Throw New ApplicationException(cmb_tinissuedby_group.Label + " is required.")
                        End If


                        If Not String.IsNullOrEmpty(cmb_tinissuedby_group.SelectedItemValue) Then
                                If String.IsNullOrEmpty(txt_tin_group.Value) Then
                                    Throw New ApplicationException(txt_tin_group.FieldLabel + " is required.")
                                End If

                            End If


                        If txt_tin_group.Value IsNot Nothing And cmb_tinissuedby_group.SelectedItemValue IsNot Nothing Then
                            Using objdb As New CRSEntities
                                Dim regexcocok As Integer = 0
                                Dim tinvalid As New DataTable
                                If chk_IsIndividualGroup.Checked = True Then
                                    tinvalid = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from TinsFormat where isocountry = '" & cmb_tinissuedby_group.SelectedItemValue & "' and TinsFormat <> '' and TinsFormat is not null")
                                Else
                                    tinvalid = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from TinsFormat where isocountry = '" & cmb_org_tinissuedby_group.SelectedItemValue & "' and TinsFormat <> '' and TinsFormat is not null")
                                End If                    'objdb.TinsFormats.Where(Function(x) x.TinsCountry = cmb_tinissuedby_group.SelectedItemValue).FirstOrDefault
                                If tinvalid Is Nothing Then
                                    Throw New ApplicationException(" Selected Issued By Country Doesn't Have TinsFormat")
                                Else
                                    If tinvalid.Rows.Count = 0 Then
                                        Throw New ApplicationException(" Selected Issued By Country Doesn't Have TinsFormat")
                                    End If
                                End If
                                For Each item As DataRow In tinvalid.Rows
                                    Dim tinformat As String = item("tinsformat").ToString
                                    If tinformat Is Nothing Or String.IsNullOrEmpty(tinformat) Then
                                        Throw New ApplicationException(" Selected Issued By Country Doesn't Have TinsFormat")
                                    End If
                                    Dim tinstring As String = txt_tin_group.Value
                                    'Dim charArray() As Char = tinformat.ToCharArray
                                    'Dim tinstring As String = txt_tin_group.Value
                                    'Dim lengthformat As Integer = Convert.ToInt32(tinformat.Length / 5) - 1
                                    'Dim lengthtin As Integer = tinstring.Length - 1
                                    'If lengthtin > lengthformat Or lengthtin < lengthformat Then
                                    '    Throw New ApplicationException("Format TIN Its Not The Same With Current TIN Issued By")
                                    '    Session("tinerrorgrup") = 1
                                    'Else
                                    '    For i = 0 To Convert.ToInt32(tinformat.Length / 5) - 1
                                    '        For j = 0 To tinstring.Length - 1
                                    '            If i = j Then
                                    '                Dim regex As Regex = New Regex(tinformat.Substring(i * 5, 5))
                                    '                If Not regex.IsMatch(tinstring(i)) Then

                                    '                    Session("tinerrorgrup") = 1
                                    '                    Throw New ApplicationException("Format TIN Its Not The Same With Current TIN Issued By")
                                    '                Else
                                    '                    Session("tinerrorgrup") = 0
                                    '                End If
                                    '            End If
                                    '        Next
                                    '    Next
                                    'End If
                                    Dim tinformat2 As String = tinformat
                                    Dim replaces As String = tinformat2.Replace("[", ",")
                                    Dim replace2 As String = replaces.Replace("]", ",")
                                    Dim replacekomadua As String = replace2.Replace(",,", ",")
                                    Dim hurufawal As String = replacekomadua.Substring(0, 1)
                                    Dim replacelength As Integer = replacekomadua.Length()

                                    If hurufawal = "," Then
                                        replacekomadua = replacekomadua.Remove(0, 1)
                                        replacelength = replacekomadua.Length()
                                        Dim hurufakhir As String = replacekomadua.Substring(replacelength - 1, 1)
                                        If hurufakhir = "," Then
                                            replacekomadua = replacekomadua.Remove(replacelength - 1, 1)
                                        End If
                                    Else
                                        replacelength = replacekomadua.Length()
                                        Dim hurufakhir As String = replacekomadua.Substring(replacelength - 1, 1)
                                        If hurufakhir = "," Then
                                            replacekomadua = replacekomadua.Remove(replacelength - 1, 1)
                                        End If
                                    End If
                                    Dim regexstring = replacekomadua
                                    Dim countregex As Integer = 0
                                    For Each c As Char In regexstring
                                        If c = "," Then
                                            countregex += 1
                                        End If
                                    Next
                                    Dim regexstringarray As String() = regexstring.Split(New Char() {","c})
                                    Dim tinchararray As Char() = tinformat.ToCharArray()
                                    Dim strbrackets(countregex) As String
                                    Dim strnobrackets(countregex) As String
                                    Dim indeksbrackets As New List(Of Integer)
                                    Dim indeksnonbrackets As New List(Of Integer)
                                    Dim foundopenbrackets As Integer = 0
                                    Dim foundclosebrackets As Integer = 0
                                    Dim strnonbracketawal As Integer = 0
                                    Dim afterclosebrackets As Integer = 0
                                    Dim afterclosebracketstobrackets As Integer = 0
                                    Dim afterclosebracketstononbrackets As Integer = 0
                                    Dim indeks As Integer = 0
                                    For i = 0 To tinchararray.Length - 1
                                        If afterclosebrackets = 1 Then
                                            If tinchararray(i) = "[" Then
                                                foundopenbrackets = 1
                                                foundclosebrackets = 0
                                                If strnonbracketawal = 1 Then
                                                    indeksnonbrackets.Add(indeks)
                                                    indeks = indeks + 1
                                                End If
                                                strnonbracketawal = 0
                                                afterclosebrackets = 0
                                            Else
                                                foundclosebrackets = 1
                                                foundopenbrackets = 0
                                                strnonbracketawal = 1
                                                afterclosebracketstononbrackets = 1
                                                afterclosebrackets = 0
                                            End If
                                        Else
                                            If tinchararray(i) = "[" Then
                                                foundopenbrackets = 1
                                                foundclosebrackets = 0
                                                If strnonbracketawal = 1 Then
                                                    indeksnonbrackets.Add(indeks)
                                                    indeks = indeks + 1
                                                End If
                                                strnonbracketawal = 0
                                            Else
                                                If foundopenbrackets = 0 Then
                                                    foundclosebrackets = 1
                                                    foundopenbrackets = 0
                                                    strnonbracketawal = 1
                                                    afterclosebracketstononbrackets = 1
                                                    afterclosebrackets = 0
                                                End If

                                            End If
                                        End If



                                        If foundopenbrackets = 1 Then
                                            'strbrackets.Add(c.ToString())
                                            strbrackets(indeks) = strbrackets(indeks) & tinchararray(i).ToString
                                            If tinchararray(i) = "]" Then
                                                indeksbrackets.Add(indeks)
                                                indeks = indeks + 1
                                                foundopenbrackets = 0
                                                foundclosebrackets = 1
                                                strnonbracketawal = 0
                                                afterclosebrackets = afterclosebrackets + 1
                                            End If
                                        End If

                                        If foundclosebrackets = 1 Then
                                            If afterclosebracketstononbrackets = 1 Then
                                                If strnonbracketawal > 0 Then
                                                    'strnobrackets.Add(c.ToString())
                                                    strnobrackets(indeks) = strnobrackets(indeks) & tinchararray(i).ToString
                                                End If
                                            End If


                                        End If
                                    Next
                                    Dim lengthformat As Integer = countregex
                                    Dim lengthtin As Integer = tinstring.Length - 1
                                    Dim indeksarraystring As Integer = 0
                                    Dim lengthnonbrackets As Integer = 0
                                    Dim tidakcocok As Integer = 0
                                    Dim totalkatavalidasi As Integer = 0
                                    Dim nilaiJ As Integer = 0
                                    If indeksnonbrackets IsNot Nothing And indeksnonbrackets.Count > 0 Then
                                        Dim index As Integer = 0
                                        For i = 0 To countregex
                                            Dim jnonbrackets As Integer = 0

                                            Dim bracketsketemu As Integer = 0
                                            Dim indeksbracketsarray As Integer = 0
                                            For Each itembrackets In indeksbrackets
                                                If itembrackets = i Then
                                                    bracketsketemu = 1
                                                    indeksbracketsarray = i
                                                End If

                                            Next

                                            Dim nonbracketsketemu As Integer = 0
                                            Dim indeksnonbracketsarray As Integer = 0
                                            For Each itemnonbrackets In indeksnonbrackets
                                                If itemnonbrackets = i Then
                                                    nonbracketsketemu = 1
                                                    indeksnonbracketsarray = i
                                                End If

                                            Next
                                            Dim strbrackets2 As String = ""
                                            Dim nonbracketsstrop As Integer = 0
                                            Dim dataawal As Integer = 0

                                            If nilaiJ = tinstring.Length - 1 Then
                                                tidakcocok = tidakcocok + 1
                                                Exit For
                                            End If
                                            For j = 0 To tinstring.Length - 1




                                                If bracketsketemu = 1 Then
                                                    If j = index Then

                                                        Dim regex As Regex = New Regex(strbrackets(i))
                                                        If Not regex.IsMatch(tinstring(j)) Then

                                                            tidakcocok = tidakcocok + 1
                                                            'Throw New ApplicationException("Format TIN Is Not The Same With Current TIN Issued By")
                                                            index = index + 1
                                                            nilaiJ = j
                                                            Exit For
                                                        Else
                                                            nilaiJ = j
                                                            index = index + 1
                                                            Exit For
                                                        End If

                                                    End If


                                                End If


                                                If nonbracketsketemu = 1 Then
                                                    If dataawal = 0 Then
                                                        lengthnonbrackets = lengthnonbrackets + strnobrackets(indeksnonbracketsarray).Length
                                                        jnonbrackets = i + lengthnonbrackets - 1
                                                        dataawal = 1
                                                    End If

                                                    If j >= i And j <= jnonbrackets Then
                                                        nonbracketsstrop = nonbracketsstrop + 1
                                                        strbrackets2 = strbrackets2 & tinstring(j)
                                                        index = index + 1
                                                    End If
                                                    If nonbracketsstrop = lengthnonbrackets Then
                                                        If strbrackets2 = strnobrackets(indeksnonbracketsarray) Then


                                                            'Throw New ApplicationException("Format TIN Is Not The Same With Current TIN Issued By")
                                                            nilaiJ = j
                                                            Exit For
                                                        Else
                                                            nilaiJ = j
                                                            tidakcocok = tidakcocok + 1
                                                            Exit For
                                                        End If
                                                    End If



                                                End If
                                            Next
                                            If i = countregex Then
                                                If nilaiJ < tinstring.Length - 1 Then
                                                    tidakcocok = tidakcocok + 1
                                                    Exit For
                                                End If
                                            End If
                                            indeksarraystring = indeksarraystring + 1
                                        Next

                                    Else
                                        If lengthtin > lengthformat Or lengthtin < lengthformat Then

                                            tidakcocok = tidakcocok + 1
                                        Else
                                            For i = 0 To countregex


                                                Dim bracketsketemu As Integer = 0
                                                Dim indeksbracketsarray As Integer = 0
                                                For Each itembrackets In indeksbrackets
                                                    If itembrackets = i Then
                                                        bracketsketemu = 1
                                                        indeksbracketsarray = i
                                                    End If

                                                Next

                                                For j = 0 To tinstring.Length - 1
                                                    If i = j Then
                                                        If bracketsketemu = 1 Then

                                                            Dim regex As Regex = New Regex(strbrackets(indeksbracketsarray))
                                                            If Not regex.IsMatch(tinstring(i)) Then

                                                                tidakcocok = tidakcocok + 1
                                                                'Throw New ApplicationException("Format TIN Is Not The Same With Current TIN Issued By")

                                                            Else

                                                            End If


                                                        End If
                                                    End If
                                                Next




                                            Next
                                        End If
                                    End If


                                    If tidakcocok = 0 Then
                                        regexcocok = regexcocok + 1
                                    End If

                                Next
                                If regexcocok = 0 Then
                                    Throw New ApplicationException("Format TIN Is Not The Same With Current TIN Issued By")

                                End If
                            End Using
                        End If
                    End If
                Else

                    If Not String.IsNullOrEmpty(cmb_tinissuedby_group.SelectedItemValue) Then
                        If String.IsNullOrEmpty(txt_tin_group.Value) Then
                            Throw New ApplicationException(txt_tin_group.FieldLabel + " is required.")
                        End If

                    End If


                End If


                Else
                If Not String.IsNullOrEmpty(txt_tin_group.Value) Then
                    If txt_tin_group.Value = "NOTIN" Or txt_tin_group.Value = "notin" Then
                    Else
                        If String.IsNullOrEmpty(cmb_org_tinissuedby_group.SelectedItemValue) Then
                            Throw New ApplicationException(cmb_org_tinissuedby_group.Label + " is required.")
                        End If


                        If Not String.IsNullOrEmpty(cmb_org_tinissuedby_group.SelectedItemValue) Then
                                If String.IsNullOrEmpty(txt_tin_group.Value) Then
                                    Throw New ApplicationException(txt_tin_group.FieldLabel + " is required.")
                                End If

                            End If


                        If txt_tin_group.Value IsNot Nothing And cmb_org_tinissuedby_group.SelectedItemValue IsNot Nothing Then
                            Using objdb As New CRSEntities
                                Dim regexcocok As Integer = 0
                                Dim tinvalid As New DataTable
                                If chk_IsIndividualGroup.Checked = True Then
                                    tinvalid = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from TinsFormat where isocountry = '" & cmb_tinissuedby_group.SelectedItemValue & "' and TinsFormat <> '' and TinsFormat is not null")
                                Else
                                    tinvalid = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from TinsFormat where isocountry = '" & cmb_org_tinissuedby_group.SelectedItemValue & "' and TinsFormat <> '' and TinsFormat is not null")
                                End If                    'objdb.TinsFormats.Where(Function(x) x.TinsCountry = cmb_tinissuedby_group.SelectedItemValue).FirstOrDefault
                                If tinvalid Is Nothing Then
                                    Throw New ApplicationException(" Selected Issued By Country Doesn't Have TinsFormat")
                                Else
                                    If tinvalid.Rows.Count = 0 Then
                                        Throw New ApplicationException(" Selected Issued By Country Doesn't Have TinsFormat")
                                    End If
                                End If
                                For Each item As DataRow In tinvalid.Rows
                                    Dim tinformat As String = item("tinsformat").ToString
                                    If tinformat Is Nothing Or String.IsNullOrEmpty(tinformat) Then
                                        Throw New ApplicationException(" Selected Issued By Country Doesn't Have TinsFormat")
                                    End If
                                    Dim tinstring As String = txt_tin_group.Value
                                    'Dim charArray() As Char = tinformat.ToCharArray
                                    'Dim tinstring As String = txt_tin_group.Value
                                    'Dim lengthformat As Integer = Convert.ToInt32(tinformat.Length / 5) - 1
                                    'Dim lengthtin As Integer = tinstring.Length - 1
                                    'If lengthtin > lengthformat Or lengthtin < lengthformat Then
                                    '    Throw New ApplicationException("Format TIN Its Not The Same With Current TIN Issued By")
                                    '    Session("tinerrorgrup") = 1
                                    'Else
                                    '    For i = 0 To Convert.ToInt32(tinformat.Length / 5) - 1
                                    '        For j = 0 To tinstring.Length - 1
                                    '            If i = j Then
                                    '                Dim regex As Regex = New Regex(tinformat.Substring(i * 5, 5))
                                    '                If Not regex.IsMatch(tinstring(i)) Then

                                    '                    Session("tinerrorgrup") = 1
                                    '                    Throw New ApplicationException("Format TIN Its Not The Same With Current TIN Issued By")
                                    '                Else
                                    '                    Session("tinerrorgrup") = 0
                                    '                End If
                                    '            End If
                                    '        Next
                                    '    Next
                                    'End If
                                    Dim tinformat2 As String = tinformat
                                    Dim replaces As String = tinformat2.Replace("[", ",")
                                    Dim replace2 As String = replaces.Replace("]", ",")
                                    Dim replacekomadua As String = replace2.Replace(",,", ",")
                                    Dim hurufawal As String = replacekomadua.Substring(0, 1)
                                    Dim replacelength As Integer = replacekomadua.Length()

                                    If hurufawal = "," Then
                                        replacekomadua = replacekomadua.Remove(0, 1)
                                        replacelength = replacekomadua.Length()
                                        Dim hurufakhir As String = replacekomadua.Substring(replacelength - 1, 1)
                                        If hurufakhir = "," Then
                                            replacekomadua = replacekomadua.Remove(replacelength - 1, 1)
                                        End If
                                    Else
                                        replacelength = replacekomadua.Length()
                                        Dim hurufakhir As String = replacekomadua.Substring(replacelength - 1, 1)
                                        If hurufakhir = "," Then
                                            replacekomadua = replacekomadua.Remove(replacelength - 1, 1)
                                        End If
                                    End If
                                    Dim regexstring = replacekomadua
                                    Dim countregex As Integer = 0
                                    For Each c As Char In regexstring
                                        If c = "," Then
                                            countregex += 1
                                        End If
                                    Next
                                    Dim regexstringarray As String() = regexstring.Split(New Char() {","c})
                                    Dim tinchararray As Char() = tinformat.ToCharArray()
                                    Dim strbrackets(countregex) As String
                                    Dim strnobrackets(countregex) As String
                                    Dim indeksbrackets As New List(Of Integer)
                                    Dim indeksnonbrackets As New List(Of Integer)
                                    Dim foundopenbrackets As Integer = 0
                                    Dim foundclosebrackets As Integer = 0
                                    Dim strnonbracketawal As Integer = 0
                                    Dim afterclosebrackets As Integer = 0
                                    Dim afterclosebracketstobrackets As Integer = 0
                                    Dim afterclosebracketstononbrackets As Integer = 0
                                    Dim indeks As Integer = 0
                                    For i = 0 To tinchararray.Length - 1
                                        If afterclosebrackets = 1 Then
                                            If tinchararray(i) = "[" Then
                                                foundopenbrackets = 1
                                                foundclosebrackets = 0
                                                If strnonbracketawal = 1 Then
                                                    indeksnonbrackets.Add(indeks)
                                                    indeks = indeks + 1
                                                End If
                                                strnonbracketawal = 0
                                                afterclosebrackets = 0
                                            Else
                                                foundclosebrackets = 1
                                                foundopenbrackets = 0
                                                strnonbracketawal = 1
                                                afterclosebracketstononbrackets = 1
                                                afterclosebrackets = 0
                                            End If
                                        Else
                                            If tinchararray(i) = "[" Then
                                                foundopenbrackets = 1
                                                foundclosebrackets = 0
                                                If strnonbracketawal = 1 Then
                                                    indeksnonbrackets.Add(indeks)
                                                    indeks = indeks + 1
                                                End If
                                                strnonbracketawal = 0
                                            Else
                                                If foundopenbrackets = 0 Then
                                                    foundclosebrackets = 1
                                                    foundopenbrackets = 0
                                                    strnonbracketawal = 1
                                                    afterclosebracketstononbrackets = 1
                                                    afterclosebrackets = 0
                                                End If

                                            End If
                                        End If



                                        If foundopenbrackets = 1 Then
                                            'strbrackets.Add(c.ToString())
                                            strbrackets(indeks) = strbrackets(indeks) & tinchararray(i).ToString
                                            If tinchararray(i) = "]" Then
                                                indeksbrackets.Add(indeks)
                                                indeks = indeks + 1
                                                foundopenbrackets = 0
                                                foundclosebrackets = 1
                                                strnonbracketawal = 0
                                                afterclosebrackets = afterclosebrackets + 1
                                            End If
                                        End If

                                        If foundclosebrackets = 1 Then
                                            If afterclosebracketstononbrackets = 1 Then
                                                If strnonbracketawal > 0 Then
                                                    'strnobrackets.Add(c.ToString())
                                                    strnobrackets(indeks) = strnobrackets(indeks) & tinchararray(i).ToString
                                                End If
                                            End If


                                        End If
                                    Next
                                    Dim lengthformat As Integer = countregex
                                    Dim lengthtin As Integer = tinstring.Length - 1
                                    Dim indeksarraystring As Integer = 0
                                    Dim lengthnonbrackets As Integer = 0
                                    Dim tidakcocok As Integer = 0
                                    Dim totalkatavalidasi As Integer = 0
                                    Dim nilaiJ As Integer = 0
                                    If indeksnonbrackets IsNot Nothing And indeksnonbrackets.Count > 0 Then
                                        Dim index As Integer = 0
                                        For i = 0 To countregex
                                            Dim jnonbrackets As Integer = 0

                                            Dim bracketsketemu As Integer = 0
                                            Dim indeksbracketsarray As Integer = 0
                                            For Each itembrackets In indeksbrackets
                                                If itembrackets = i Then
                                                    bracketsketemu = 1
                                                    indeksbracketsarray = i
                                                End If

                                            Next

                                            Dim nonbracketsketemu As Integer = 0
                                            Dim indeksnonbracketsarray As Integer = 0
                                            For Each itemnonbrackets In indeksnonbrackets
                                                If itemnonbrackets = i Then
                                                    nonbracketsketemu = 1
                                                    indeksnonbracketsarray = i
                                                End If

                                            Next
                                            Dim strbrackets2 As String = ""
                                            Dim nonbracketsstrop As Integer = 0
                                            Dim dataawal As Integer = 0

                                            If nilaiJ = tinstring.Length - 1 Then
                                                tidakcocok = tidakcocok + 1
                                                Exit For
                                            End If
                                            For j = 0 To tinstring.Length - 1




                                                If bracketsketemu = 1 Then
                                                    If j = index Then

                                                        Dim regex As Regex = New Regex(strbrackets(i))
                                                        If Not regex.IsMatch(tinstring(j)) Then

                                                            tidakcocok = tidakcocok + 1
                                                            'Throw New ApplicationException("Format TIN Is Not The Same With Current TIN Issued By")
                                                            index = index + 1
                                                            nilaiJ = j
                                                            Exit For
                                                        Else
                                                            nilaiJ = j
                                                            index = index + 1
                                                            Exit For
                                                        End If

                                                    End If


                                                End If


                                                If nonbracketsketemu = 1 Then
                                                    If dataawal = 0 Then
                                                        lengthnonbrackets = lengthnonbrackets + strnobrackets(indeksnonbracketsarray).Length
                                                        jnonbrackets = i + lengthnonbrackets - 1
                                                        dataawal = 1
                                                    End If

                                                    If j >= i And j <= jnonbrackets Then
                                                        nonbracketsstrop = nonbracketsstrop + 1
                                                        strbrackets2 = strbrackets2 & tinstring(j)
                                                        index = index + 1
                                                    End If
                                                    If nonbracketsstrop = lengthnonbrackets Then
                                                        If strbrackets2 = strnobrackets(indeksnonbracketsarray) Then


                                                            'Throw New ApplicationException("Format TIN Is Not The Same With Current TIN Issued By")
                                                            nilaiJ = j
                                                            Exit For
                                                        Else
                                                            nilaiJ = j
                                                            tidakcocok = tidakcocok + 1
                                                            Exit For
                                                        End If
                                                    End If



                                                End If
                                            Next
                                            If i = countregex Then
                                                If nilaiJ < tinstring.Length - 1 Then
                                                    tidakcocok = tidakcocok + 1
                                                    Exit For
                                                End If
                                            End If
                                            indeksarraystring = indeksarraystring + 1
                                        Next

                                    Else
                                        If lengthtin > lengthformat Or lengthtin < lengthformat Then

                                            tidakcocok = tidakcocok + 1
                                        Else
                                            For i = 0 To countregex


                                                Dim bracketsketemu As Integer = 0
                                                Dim indeksbracketsarray As Integer = 0
                                                For Each itembrackets In indeksbrackets
                                                    If itembrackets = i Then
                                                        bracketsketemu = 1
                                                        indeksbracketsarray = i
                                                    End If

                                                Next

                                                For j = 0 To tinstring.Length - 1
                                                    If i = j Then
                                                        If bracketsketemu = 1 Then

                                                            Dim regex As Regex = New Regex(strbrackets(indeksbracketsarray))
                                                            If Not regex.IsMatch(tinstring(i)) Then

                                                                tidakcocok = tidakcocok + 1
                                                                'Throw New ApplicationException("Format TIN Is Not The Same With Current TIN Issued By")

                                                            Else

                                                            End If


                                                        End If
                                                    End If
                                                Next




                                            Next
                                        End If
                                    End If


                                    If tidakcocok = 0 Then
                                        regexcocok = regexcocok + 1
                                    End If

                                Next
                                If regexcocok = 0 Then
                                    Throw New ApplicationException("Format TIN Is Not The Same With Current TIN Issued By")

                                End If
                            End Using
                        End If
                    End If
                Else
                    If Not String.IsNullOrEmpty(cmb_org_tinissuedby_group.SelectedItemValue) Then
                        If String.IsNullOrEmpty(txt_tin_group.Value) Then
                            Throw New ApplicationException(txt_tin_group.FieldLabel + " is required.")
                        End If

                    End If
                End If

            End If
            'General Info
            If String.IsNullOrEmpty(cmb_DocTypeIndicator.SelectedItemValue) Then
                Throw New ApplicationException(cmb_DocTypeIndicator.Label + " is required.")
            End If

            If cmb_DocTypeIndicator.SelectedItemValue = "OECD2" Then
                If String.IsNullOrEmpty(txt_CorDocTypeIndicator.Value) Then
                    Throw New ApplicationException(txt_CorDocTypeIndicator.FieldLabel + " is required.")
                End If
            End If

            If String.IsNullOrEmpty(chk_IsIndividualGroup.Checked) Then
                Throw New ApplicationException(chk_IsIndividualGroup.FieldLabel + " is required.")
            End If

            If String.IsNullOrEmpty(cmb_ResCountryCode.SelectedItemValue) Then
                Throw New ApplicationException(cmb_ResCountryCode.Label + " is required.")
            End If



            ' Account Information
            If String.IsNullOrEmpty(txt_accountnumber_group.Value) Then
                Throw New ApplicationException(txt_accountnumber_group.FieldLabel + " is required.")
            End If

            'If String.IsNullOrEmpty(cmb_AccountType_Group.SelectedItemValue) Then
            '    Throw New ApplicationException(cmb_AccountType_Group.Label + " is required.")
            'End If

            If String.IsNullOrEmpty(cmb_AccountCurrency_Group.SelectedItemValue) Then
                Throw New ApplicationException(cmb_AccountCurrency_Group.Label + " is required.")
            End If

            If String.IsNullOrEmpty(txt_accountbalance_Group.Value) Then
                Throw New ApplicationException(txt_accountbalance_Group.FieldLabel + " is required.")
            End If

            If chk_IsIndividualGroup.Checked = True Then
                ' Individual Information
                If String.IsNullOrEmpty(cmb_NameType_Group.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_NameType_Group.Label + " is required.")
                End If

                If String.IsNullOrEmpty(txt_firstname_group.Value) Then
                    Throw New ApplicationException(txt_firstname_group.FieldLabel + " is required.")
                End If

                If String.IsNullOrEmpty(txt_lastname_group.Value) Then
                    Throw New ApplicationException(txt_lastname_group.FieldLabel + " is required.")
                End If

                If datebirthdate_group.SelectedDate = DateTime.MinValue Then
                    Throw New ApplicationException(datebirthdate_group.FieldLabel + " is required.")
                End If
            Else

                If String.IsNullOrEmpty(cmb_AccountHolderType.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_AccountHolderType.Label + " is required.")
                End If

                If String.IsNullOrEmpty(txt_org_Name.Value) Then
                    Throw New ApplicationException(txt_org_Name.FieldLabel + " is required.")
                End If
                If String.IsNullOrEmpty(cmb_org_nametype.SelectedItemValue) Then
                    Throw New ApplicationException(cmb_org_nametype.Label + " is required.")
                End If

            End If


            'TIN Information
            'If String.IsNullOrEmpty(txt_tin_group.Value) Then
            '    Throw New ApplicationException(txt_tin_group.FieldLabel + " is required.")
            'End If

            'If String.IsNullOrEmpty(cmb_tinissuedby_group.SelectedItemValue) Then
            '    Throw New ApplicationException(cmb_tinissuedby_group.Label + " is required.")
            'End If

            'Address Information
            'If String.IsNullOrEmpty(cmb_addresslegaltype_group.SelectedItemValue) Then
            '    Throw New ApplicationException(cmb_addresslegaltype_group.Label + " is required.")
            'End If

            If String.IsNullOrEmpty(cmb_countrycode_Group.SelectedItemValue) Then
                Throw New ApplicationException(cmb_countrycode_Group.Label + " is required.")
            End If

            If String.IsNullOrEmpty(txt_addressfree_group.Value) Then
                Throw New ApplicationException(txt_addressfree_group.FieldLabel + " is required.")
            End If

            If Not String.IsNullOrEmpty(txt_street_group.Value) Then
                If String.IsNullOrEmpty(txt_city_group.Value) Then
                    Throw New ApplicationException(txt_city_group.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_buildingindentifier_Group.Value) Then
                If String.IsNullOrEmpty(txt_city_group.Value) Then
                    Throw New ApplicationException(txt_city_group.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_suiteidentifier_group.Value) Then
                If String.IsNullOrEmpty(txt_city_group.Value) Then
                    Throw New ApplicationException(txt_city_group.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_flooridentifier_group.Value) Then
                If String.IsNullOrEmpty(txt_city_group.Value) Then
                    Throw New ApplicationException(txt_city_group.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_districtname_Group.Value) Then
                If String.IsNullOrEmpty(txt_city_group.Value) Then
                    Throw New ApplicationException(txt_city_group.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_pob_group.Value) Then
                If String.IsNullOrEmpty(txt_city_group.Value) Then
                    Throw New ApplicationException(txt_city_group.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_postcode_group.Value) Then
                If String.IsNullOrEmpty(txt_city_group.Value) Then
                    Throw New ApplicationException(txt_city_group.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_countrysubentity_group.Value) Then
                If String.IsNullOrEmpty(txt_city_group.Value) Then
                    Throw New ApplicationException(txt_city_group.FieldLabel + " is required.")
                End If
            End If

            If CRS_Report_Group_Class.list_Reporting_RGARPayment IsNot Nothing Then
                If CRS_Report_Group_Class.list_Reporting_RGARPayment.Count > 0 Then
                    Dim i As Integer = 0
                    For Each item In CRS_Report_Group_Class.list_Reporting_RGARPayment
                        i = i + 1
                        If item.PaymentAmnt Is Nothing Then
                            Throw New ApplicationException("Payment Amount Data " + i + " Is Required")
                        End If
                        If item.Payment_PaymentAmnt_currCode Is Nothing Then
                            Throw New ApplicationException("Payment Amount Currency Data " + i + " Is Required")
                        End If
                        If item.Payment_Type Is Nothing Then
                            Throw New ApplicationException("Payment Type Data " + i + " Is Required")
                        End If
                    Next

                End If
            End If



            If cmb_AccountHolderType.SelectedItemValue = "CRS101" Then
                If CRS_Report_Group_Class.list_Reporting_RGARPerson Is Nothing Then
                    Throw New ApplicationException("Reporting Controlling Person Is Null, it's required.")
                Else
                    If CRS_Report_Group_Class.list_Reporting_RGARPerson.Count < 1 Then
                        Throw New ApplicationException("Reporting Controlling Person Is Null, it's required.")
                    End If
                End If

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub PopulateReportData()
        Try
            With CRS_Report_Class.obj_CRS_Reporting_Header
                If .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID <= 0 Then
                    .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = -1
                End If
                .SendingCompanyIN = CRS_Global_BLL.getGlobalParameterValueByID(2)
                .TransmittingCountry = CRS_Global_BLL.getGlobalParameterValueByID(3)
                .MessageType = CRS_Global_BLL.getGlobalParameterValueByID(4)


                .Warning = txtWarning.Value
                .ReceivingCountry = cmb_ReceivingCountry.SelectedItemValue
                .MessageRefID = txtMessageRefID.Value
                .MessageTypeIndic = cmb_MessageTypeInd.SelectedItemValue
                .CorrMessageRefID = txtCorrectionMessageRefID.Value
                .Reporting_Date = DateTime.Now
                .ReportingPeriod = txtReportingPeriod.Value
                .Timestamp = txtTimeStamp.Value
                .LJK_Type = cmb_LJK.SelectedItemValue
                .Contact = txtContact.Text
                .Contact = txtContact.Value

                'Reporting FI
                With CRS_Report_Class.obj_CRS_Reporting_FI
                    If .PK_CRS_INTERNATIONAL_REPORTING_FI_ID <= 0 Then
                        .PK_CRS_INTERNATIONAL_REPORTING_FI_ID = -1
                    End If

                    .ResCountryCode = txtResCountryCode.Value
                    .DocSpec_DocRefID = txtDocRefID.Value
                    .DocSpec_DocTypeIndic = cmb_DocTypeIndic.SelectedItemValue
                    .IN = txtIN.Value
                    .IN_IssueBy = txtIN_IssueBy.Value
                    .IN_INType = txtIN_INType.Value
                    .Name = txtName.Text = CRS_Global_BLL.getGlobalParameterValueByID(9)
                    .Name_Type = txtName_Type.Value
                    .Address_LegalAddressType = txtAddress_LegalAddressType.Value
                    .Address_CountryCode = txtAddress_CountryCode.Value
                    .Address_AddressFree = txtAddress_AddressFree.Value
                    .Address_AddressFix_Street = txtAddress_AddressFix_Street.Value
                    .Address_AddressFix_BuildingIdentifier = txtAddress_AddressFix_BuildingIdentifier.Value
                    .Address_AddressFix_SuiteIdentifier = txtAddress_AddressFix_SuiteIdentifier.Value
                    .Address_AddressFix_FloorIdentifier = txtAddress_AddressFix_FloorIdentifier.Value
                    .Address_AddressFix_DistrictName = txtAddress_AddressFix_DistrictName.Value
                    .Address_AddressFix_POB = txtAddress_AddressFix_POB.Value
                    .Address_AddressFix_PostCode = txtAddress_AddressFix_PostCode.Value
                    .Address_AddressFix_City = txtAddress_AddressFix_City.Value
                    .Address_AddressFix_CountrySubentity = txtAddress_AddressFix_CountrySubentity.Value
                    .Active = True
                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .CreatedDate = DateTime.Now
                End With


                If .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID < 0 Then
                    .Active = True
                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .CreatedDate = DateTime.Now
                Else
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateDate = DateTime.Now
                End If
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Payment"

    Public Property IDPayment() As Long
        Get
            Return Session("CRS_International_Add.IDPayment")
        End Get
        Set(ByVal value As Long)
            Session("CRS_International_Add.IDPayment") = value
        End Set
    End Property

    Public Property obj_Reporting_Payment_Edit() As CRS_INTERNATIONAL_RG_AR_PAYMENT
        Get
            Return Session("CRS_International_Add.obj_Reporting_Payment_Edit")
        End Get
        Set(ByVal value As CRS_INTERNATIONAL_RG_AR_PAYMENT)
            Session("CRS_International_Add.obj_Reporting_Payment_Edit") = value
        End Set
    End Property

    Protected Sub btn_Payment_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            obj_Reporting_Payment_Edit = Nothing
            Clean_Window_Payment()
            'setpayment()
            'Show window pop up
            cmb_paymenttype.StringFieldStyle = "background-color:#FFE4C4"
            cmb_paymentcurrencycode.StringFieldStyle = "background-color:#FFE4C4"
            Window_Payment.Title = "Reporting Payment - Add"
            Window_Payment.Hidden = False

            'Set IDPayment to 0
            IDPayment = 0

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_payment_group(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = CRS_Report_Group_Class.list_Reporting_RGARPayment.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID = strID)
                    If objToDelete IsNot Nothing Then
                        CRS_Report_Group_Class.list_Reporting_RGARPayment.Remove(objToDelete)
                    End If
                    Bind_Reporting_Payment()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Reporting_Payment_Edit = CRS_Report_Group_Class.list_Reporting_RGARPayment.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID = strID)
                    Load_Window_Report_Payment(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Payment_Cancel_Click()
        Try
            'Hide window pop up
            Window_Payment.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Payment_Save_Click()
        Try
            'Validate input
            If String.IsNullOrEmpty(txt_paymentamount.Value) Then
                Throw New ApplicationException(txt_paymentamount.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_paymenttype.SelectedItemValue) Then
                Throw New ApplicationException(cmb_paymenttype.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(cmb_paymentcurrencycode.SelectedItemValue) Then
                Throw New ApplicationException(cmb_paymentcurrencycode.Label & " harus diisi.")
            End If

            ''set account balance
            'If cmb_AccountCurrency_Group.SelectedItemValue Is Nothing Then
            '    cmb_AccountCurrency_Group.IsReadOnly = True
            '    If cmb_AccountCurrency_Group.SelectedItemValue IsNot Nothing Then
            '        Dim objpayment As New vw_CRS_INTERNATIONAL_CURRENCY
            '        Using objdb As New CRSDAL.CRSEntities
            '            objpayment = objdb.vw_CRS_INTERNATIONAL_CURRENCY.Where(Function(x) x.FK_CURRENCY_CODE = cmb_AccountCurrency_Group.SelectedItemValue).FirstOrDefault
            '        End Using

            '        With objpayment
            '            cmb_paymentcurrencycode.SetTextWithTextValue(.FK_CURRENCY_CODE, .CURRENCY_NAME)
            '        End With

            '    End If
            'Else
            '    cmb_AccountCurrency_Group.IsReadOnly = False
            '    cmb_paymentcurrencycode.SetTextValue("")
            'End If


            'Action save here
            Dim intPK As Long = -1

            If obj_Reporting_Payment_Edit Is Nothing Then  'Add
                Dim objAdd As New CRS_INTERNATIONAL_RG_AR_PAYMENT
                If CRS_Report_Group_Class.list_Reporting_RGARPayment IsNot Nothing Then
                    If CRS_Report_Group_Class.list_Reporting_RGARPayment.Count > 0 Then
                        intPK = CRS_Report_Group_Class.list_Reporting_RGARPayment.Min(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDPayment = intPK

                With objAdd
                    .PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID = intPK
                    .FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = Session("PKReportHeaderID")
                    .PaymentAmnt = txt_paymentamount.Value

                    If Not String.IsNullOrEmpty(cmb_paymenttype.SelectedItemValue) Then
                        .Payment_Type = cmb_paymenttype.SelectedItemValue
                    Else
                        .Payment_Type = Nothing
                    End If

                    If Not String.IsNullOrEmpty(cmb_paymentcurrencycode.SelectedItemValue) Then
                        .Payment_PaymentAmnt_currCode = cmb_paymentcurrencycode.SelectedItemValue
                    Else
                        .Payment_PaymentAmnt_currCode = Nothing
                    End If


                End With
                If CRS_Report_Group_Class.list_Reporting_RGARPayment Is Nothing Then
                    CRS_Report_Group_Class.list_Reporting_RGARPayment = New List(Of CRSDAL.CRS_INTERNATIONAL_RG_AR_PAYMENT)
                End If
                CRS_Report_Group_Class.list_Reporting_RGARPayment.Add(objAdd)
            Else    'Edit
                Dim objEdit = CRS_Report_Group_Class.list_Reporting_RGARPayment.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID = obj_Reporting_Payment_Edit.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID)
                If objEdit IsNot Nothing Then
                    With objEdit
                        IDPayment = .PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID

                        .PaymentAmnt = txt_paymentamount.Value

                        If Not String.IsNullOrEmpty(cmb_paymenttype.SelectedItemValue) Then
                            .Payment_Type = cmb_paymenttype.SelectedItemValue
                        Else
                            .Payment_Type = Nothing
                        End If

                        If Not String.IsNullOrEmpty(cmb_paymentcurrencycode.SelectedItemValue) Then
                            .Payment_PaymentAmnt_currCode = cmb_paymentcurrencycode.SelectedItemValue
                        Else
                            .Payment_PaymentAmnt_currCode = Nothing
                        End If


                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_Reporting_Payment()

            'Hide window popup
            Window_Payment.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_Reporting_Payment()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Group_Class.list_Reporting_RGARPayment)


        gp_payment.GetStore().DataSource = objtable
        gp_payment.GetStore().DataBind()
        gp_payment_detail.GetStore().DataSource = objtable
        gp_payment_detail.GetStore().DataBind()

    End Sub


    Protected Sub Clean_Window_Payment()
        'Clean fields
        txt_paymentamount.Value = Nothing
        cmb_paymentcurrencycode.SetTextValue("")
        cmb_paymenttype.SetTextValue("")

        'Show Buttons
        btn_Payment_Save.Hidden = False
    End Sub

    'Protected Sub setpayment()
    '    'Set Field
    '    If cmb_AccountCurrency_Group.SelectedItemValue IsNot Nothing Then
    '        cmb_AccountCurrency_Group.IsReadOnly = True
    '        Dim objpayment As New vw_CRS_INTERNATIONAL_CURRENCY
    '        Using objdb As New CRSDAL.CRSEntities
    '            objpayment = objdb.vw_CRS_INTERNATIONAL_CURRENCY.Where(Function(x) x.FK_CURRENCY_CODE = cmb_AccountCurrency_Group.SelectedItemValue).FirstOrDefault
    '        End Using

    '        With objpayment
    '            cmb_paymentcurrencycode.SetTextWithTextValue(.FK_CURRENCY_CODE, .CURRENCY_NAME)
    '        End With
    '    Else
    '        cmb_AccountCurrency_Group.IsReadOnly = False
    '        cmb_paymentcurrencycode.SetTextValue("")
    '    End If
    'End Sub

    Protected Sub Load_Window_Report_Payment(strAction As String)
        'Clean window pop up
        Clean_Window_Payment()

        If obj_Reporting_Payment_Edit IsNot Nothing Then
            'Populate fields
            With obj_Reporting_Payment_Edit
                txt_paymentamount.Value = .PaymentAmnt

                If .Payment_Type IsNot Nothing Then
                    Dim objPaymentType = CRS_Report_BLL.GetPaymentTypeByCode(.Payment_Type)
                    If Not objPaymentType Is Nothing Then
                        cmb_paymenttype.SetTextWithTextValue(objPaymentType.FK_CRS_PAYMENT_TYPE_CODE, objPaymentType.PAYMENT_TYPE_NAME)
                    End If
                End If


                If .Payment_PaymentAmnt_currCode IsNot Nothing Then
                    Dim objPaymentCurrency = CRS_Report_BLL.GetPaymentCurrencyByCode(.Payment_PaymentAmnt_currCode)
                    If Not objPaymentCurrency Is Nothing Then
                        cmb_paymentcurrencycode.SetTextWithTextValue(objPaymentCurrency.FK_CURRENCY_CODE, objPaymentCurrency.CURRENCY_NAME)
                    End If
                End If
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            txt_paymentamount.ReadOnly = False
            cmb_paymenttype.IsReadOnly = False
            cmb_paymentcurrencycode.IsReadOnly = False
            btn_Payment_Save.Hidden = False '' Added on 12 Aug 2021
        Else
            txt_paymentamount.ReadOnly = True
            cmb_paymenttype.IsReadOnly = True
            cmb_paymentcurrencycode.IsReadOnly = True

            btn_Payment_Save.Hidden = True '' Added on 12 Aug 2021
        End If
        cmb_paymenttype.StringFieldStyle = "background-color:#FFE4C4"
        cmb_paymentcurrencycode.StringFieldStyle = "background-color:#FFE4C4"
        'Bind ATM
        IDPayment = obj_Reporting_Payment_Edit.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID

        'Show window pop up
        Window_Payment.Title = "Reporting Payment - " & strAction
        Window_Payment.Hidden = False
    End Sub
#End Region

#Region "Controlling Person"

    Public Property IDCP() As Long
        Get
            Return Session("CRS_International_Add.IDCP")
        End Get
        Set(ByVal value As Long)
            Session("CRS_International_Add.IDCP") = value
        End Set
    End Property

    Public Property obj_Reporting_cp_Edit() As CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON
        Get
            Return Session("CRS_International_Add.obj_Reporting_cp_Edit")
        End Get
        Set(ByVal value As CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON)
            Session("CRS_International_Add.obj_Reporting_cp_Edit") = value
        End Set
    End Property

    Protected Sub btn_cp_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            obj_Reporting_cp_Edit = Nothing
            Clean_Window_cp()
            cmb_nametype_cp.SetTextWithTextValue("OECD202", "OECD202 - Individu")
            SetReadOnlyWindowReportingCP(False)
            btnsavecp.Hidden = False
            'Show window pop up
            windowcp.Title = "Reporting Controlling Person - Add"
            windowcp.Hidden = False

            'Set IDCP to 0
            IDCP = 0

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_cp_group(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = CRS_Report_Group_Class.list_Reporting_RGARPerson.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID = strID)
                    If objToDelete IsNot Nothing Then
                        CRS_Report_Group_Class.list_Reporting_RGARPerson.Remove(objToDelete)
                    End If
                    Bind_Reporting_cp()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Reporting_cp_Edit = CRS_Report_Group_Class.list_Reporting_RGARPerson.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID = strID)
                    Load_Window_Report_cp(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_cp_Cancel_Click()
        Try
            'Hide window pop up
            windowcp.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_cp_Save_Click()
        Try
            'Validate input
            ' Account Information CP
            If String.IsNullOrEmpty(cmb_restcountrycode_cp.SelectedItemValue) Then
                Throw New ApplicationException(cmb_restcountrycode_cp.Label + " is required.")
            End If

            If String.IsNullOrEmpty(cmb_controllingpersontype_cp.SelectedItemValue) Then
                Throw New ApplicationException(cmb_controllingpersontype_cp.Label + " is required.")
            End If

            If String.IsNullOrEmpty(cmb_nametype_cp.SelectedItemValue) Then
                Throw New ApplicationException(cmb_NameType_Group.Label + " is required.")
            End If

            If String.IsNullOrEmpty(txt_firstname_cp.Value) Then
                Throw New ApplicationException(txt_firstname_cp.FieldLabel + " is required.")
            End If

            If String.IsNullOrEmpty(txt_lastname_cp.Value) Then
                Throw New ApplicationException(txt_lastname_cp.FieldLabel + " is required.")
            End If

            If DateBirthDate_cp.SelectedDate = DateTime.MinValue Then
                Throw New ApplicationException(DateBirthDate_cp.FieldLabel + " is required.")
            End If



            'TIN Information CP
            If Not String.IsNullOrEmpty(txt_tin_cp.Value) Then
                If txt_tin_cp.Value = "NOTIN" Or txt_tin_cp.Value = "notin" Then
                Else
                    If String.IsNullOrEmpty(cmb_tinissuedby_cp.SelectedItemValue) Then
                        Throw New ApplicationException(cmb_tinissuedby_cp.Label + " is required.")
                    End If

                    If Not String.IsNullOrEmpty(cmb_tinissuedby_cp.SelectedItemValue) Then
                        If String.IsNullOrEmpty(txt_tin_cp.Value) Then
                            Throw New ApplicationException(txt_tin_cp.FieldLabel + " is required.")
                        End If

                    End If

                    If txt_tin_cp.Value IsNot Nothing And cmb_tinissuedby_cp.SelectedItemValue IsNot Nothing Then
                        Using objdb As New CRSEntities
                            Dim regexcocok As Integer = 0
                            Dim tinvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from TinsFormat where TinsCountry = '" & cmb_tinissuedby_cp.SelectedItemValue & "'")
                            'objdb.TinsFormats.Where(Function(x) x.TinsCountry = cmb_tinissuedby_group.SelectedItemValue).FirstOrDefault
                            If tinvalid Is Nothing Then
                                Throw New ApplicationException(" Selected Issued By Country Doesn't Have TinsFormat")
                            Else
                                If tinvalid.Rows.Count = 0 Then
                                    Throw New ApplicationException(" Selected Issued By Country Doesn't Have TinsFormat")
                                End If
                            End If
                            For Each item As DataRow In tinvalid.Rows
                                Dim tinformat As String = item("tinsformat").ToString
                                If tinformat Is Nothing Or String.IsNullOrEmpty(tinformat) Then
                                    Throw New ApplicationException(" Selected Issued By Country Doesn't Have TinsFormat")
                                End If
                                Dim tinstring As String = txt_tin_cp.Value
                                'Dim charArray() As Char = tinformat.ToCharArray
                                'Dim tinstring As String = txt_tin_group.Value
                                'Dim lengthformat As Integer = Convert.ToInt32(tinformat.Length / 5) - 1
                                'Dim lengthtin As Integer = tinstring.Length - 1
                                'If lengthtin > lengthformat Or lengthtin < lengthformat Then
                                '    Throw New ApplicationException("Format TIN Its Not The Same With Current TIN Issued By")
                                '    Session("tinerrorgrup") = 1
                                'Else
                                '    For i = 0 To Convert.ToInt32(tinformat.Length / 5) - 1
                                '        For j = 0 To tinstring.Length - 1
                                '            If i = j Then
                                '                Dim regex As Regex = New Regex(tinformat.Substring(i * 5, 5))
                                '                If Not regex.IsMatch(tinstring(i)) Then

                                '                    Session("tinerrorgrup") = 1
                                '                    Throw New ApplicationException("Format TIN Its Not The Same With Current TIN Issued By")
                                '                Else
                                '                    Session("tinerrorgrup") = 0
                                '                End If
                                '            End If
                                '        Next
                                '    Next
                                'End If
                                Dim tinformat2 As String = tinformat
                                Dim replaces As String = tinformat2.Replace("[", ",")
                                Dim replace2 As String = replaces.Replace("]", ",")
                                Dim replacekomadua As String = replace2.Replace(",,", ",")
                                Dim hurufawal As String = replacekomadua.Substring(0, 1)
                                Dim replacelength As Integer = replacekomadua.Length()

                                If hurufawal = "," Then
                                    replacekomadua = replacekomadua.Remove(0, 1)
                                    replacelength = replacekomadua.Length()
                                    Dim hurufakhir As String = replacekomadua.Substring(replacelength - 1, 1)
                                    If hurufakhir = "," Then
                                        replacekomadua = replacekomadua.Remove(replacelength - 1, 1)
                                    End If
                                Else
                                    replacelength = replacekomadua.Length()
                                    Dim hurufakhir As String = replacekomadua.Substring(replacelength - 1, 1)
                                    If hurufakhir = "," Then
                                        replacekomadua = replacekomadua.Remove(replacelength - 1, 1)
                                    End If
                                End If
                                Dim regexstring = replacekomadua
                                Dim countregex As Integer = 0
                                For Each c As Char In regexstring
                                    If c = "," Then
                                        countregex += 1
                                    End If
                                Next
                                Dim regexstringarray As String() = regexstring.Split(New Char() {","c})
                                Dim tinchararray As Char() = tinformat.ToCharArray()
                                Dim strbrackets(countregex) As String
                                Dim strnobrackets(countregex) As String
                                Dim indeksbrackets As New List(Of Integer)
                                Dim indeksnonbrackets As New List(Of Integer)
                                Dim foundopenbrackets As Integer = 0
                                Dim foundclosebrackets As Integer = 0
                                Dim strnonbracketawal As Integer = 0
                                Dim afterclosebrackets As Integer = 0
                                Dim afterclosebracketstobrackets As Integer = 0
                                Dim afterclosebracketstononbrackets As Integer = 0
                                Dim indeks As Integer = 0
                                For i = 0 To tinchararray.Length - 1
                                    If afterclosebrackets = 1 Then
                                        If tinchararray(i) = "[" Then
                                            foundopenbrackets = 1
                                            foundclosebrackets = 0
                                            If strnonbracketawal = 1 Then
                                                indeksnonbrackets.Add(indeks)
                                                indeks = indeks + 1
                                            End If
                                            strnonbracketawal = 0
                                            afterclosebrackets = 0
                                        Else
                                            foundclosebrackets = 1
                                            foundopenbrackets = 0
                                            strnonbracketawal = 1
                                            afterclosebracketstononbrackets = 1
                                            afterclosebrackets = 0
                                        End If
                                    Else
                                        If tinchararray(i) = "[" Then
                                            foundopenbrackets = 1
                                            foundclosebrackets = 0
                                            If strnonbracketawal = 1 Then
                                                indeksnonbrackets.Add(indeks)
                                                indeks = indeks + 1
                                            End If
                                            strnonbracketawal = 0
                                        Else
                                            If foundopenbrackets = 0 Then
                                                foundclosebrackets = 1
                                                foundopenbrackets = 0
                                                strnonbracketawal = 1
                                                afterclosebracketstononbrackets = 1
                                                afterclosebrackets = 0
                                            End If

                                        End If
                                    End If



                                    If foundopenbrackets = 1 Then
                                        'strbrackets.Add(c.ToString())
                                        strbrackets(indeks) = strbrackets(indeks) & tinchararray(i).ToString
                                        If tinchararray(i) = "]" Then
                                            indeksbrackets.Add(indeks)
                                            indeks = indeks + 1
                                            foundopenbrackets = 0
                                            foundclosebrackets = 1
                                            strnonbracketawal = 0
                                            afterclosebrackets = afterclosebrackets + 1
                                        End If
                                    End If

                                    If foundclosebrackets = 1 Then
                                        If afterclosebracketstononbrackets = 1 Then
                                            If strnonbracketawal > 0 Then
                                                'strnobrackets.Add(c.ToString())
                                                strnobrackets(indeks) = strnobrackets(indeks) & tinchararray(i).ToString
                                            End If
                                        End If


                                    End If
                                Next
                                Dim lengthformat As Integer = countregex
                                Dim lengthtin As Integer = tinstring.Length - 1
                                Dim indeksarraystring As Integer = 0
                                Dim lengthnonbrackets As Integer = 0
                                Dim tidakcocok As Integer = 0
                                Dim totalkatavalidasi As Integer = 0
                                Dim nilaiJ As Integer = 0
                                If indeksnonbrackets IsNot Nothing And indeksnonbrackets.Count > 0 Then
                                    Dim index As Integer = 0
                                    For i = 0 To countregex
                                        Dim jnonbrackets As Integer = 0

                                        Dim bracketsketemu As Integer = 0
                                        Dim indeksbracketsarray As Integer = 0
                                        For Each itembrackets In indeksbrackets
                                            If itembrackets = i Then
                                                bracketsketemu = 1
                                                indeksbracketsarray = i
                                            End If

                                        Next

                                        Dim nonbracketsketemu As Integer = 0
                                        Dim indeksnonbracketsarray As Integer = 0
                                        For Each itemnonbrackets In indeksnonbrackets
                                            If itemnonbrackets = i Then
                                                nonbracketsketemu = 1
                                                indeksnonbracketsarray = i
                                            End If

                                        Next
                                        Dim strbrackets2 As String = ""
                                        Dim nonbracketsstrop As Integer = 0
                                        Dim dataawal As Integer = 0

                                        If nilaiJ = tinstring.Length - 1 Then
                                            tidakcocok = tidakcocok + 1
                                            Exit For
                                        End If
                                        For j = 0 To tinstring.Length - 1




                                            If bracketsketemu = 1 Then
                                                If j = index Then

                                                    Dim regex As Regex = New Regex(strbrackets(i))
                                                    If Not regex.IsMatch(tinstring(j)) Then

                                                        tidakcocok = tidakcocok + 1
                                                        'Throw New ApplicationException("Format TIN Is Not The Same With Current TIN Issued By")
                                                        index = index + 1
                                                        nilaiJ = j
                                                        Exit For
                                                    Else
                                                        nilaiJ = j
                                                        index = index + 1
                                                        Exit For
                                                    End If

                                                End If


                                            End If


                                            If nonbracketsketemu = 1 Then
                                                If dataawal = 0 Then
                                                    lengthnonbrackets = lengthnonbrackets + strnobrackets(indeksnonbracketsarray).Length
                                                    jnonbrackets = i + lengthnonbrackets - 1
                                                    dataawal = 1
                                                End If

                                                If j >= i And j <= jnonbrackets Then
                                                    nonbracketsstrop = nonbracketsstrop + 1
                                                    strbrackets2 = strbrackets2 & tinstring(j)
                                                    index = index + 1
                                                End If
                                                If nonbracketsstrop = lengthnonbrackets Then
                                                    If strbrackets2 = strnobrackets(indeksnonbracketsarray) Then


                                                        'Throw New ApplicationException("Format TIN Is Not The Same With Current TIN Issued By")
                                                        nilaiJ = j
                                                        Exit For
                                                    Else
                                                        nilaiJ = j
                                                        tidakcocok = tidakcocok + 1
                                                        Exit For
                                                    End If
                                                End If



                                            End If
                                        Next
                                        If i = countregex Then
                                            If nilaiJ < tinstring.Length - 1 Then
                                                tidakcocok = tidakcocok + 1
                                                Exit For
                                            End If
                                        End If
                                        indeksarraystring = indeksarraystring + 1
                                    Next

                                Else
                                    If lengthtin > lengthformat Or lengthtin < lengthformat Then

                                        tidakcocok = tidakcocok + 1
                                    Else
                                        For i = 0 To countregex


                                            Dim bracketsketemu As Integer = 0
                                            Dim indeksbracketsarray As Integer = 0
                                            For Each itembrackets In indeksbrackets
                                                If itembrackets = i Then
                                                    bracketsketemu = 1
                                                    indeksbracketsarray = i
                                                End If

                                            Next

                                            For j = 0 To tinstring.Length - 1
                                                If i = j Then
                                                    If bracketsketemu = 1 Then

                                                        Dim regex As Regex = New Regex(strbrackets(indeksbracketsarray))
                                                        If Not regex.IsMatch(tinstring(i)) Then

                                                            tidakcocok = tidakcocok + 1
                                                            'Throw New ApplicationException("Format TIN Is Not The Same With Current TIN Issued By")

                                                        Else

                                                        End If


                                                    End If
                                                End If
                                            Next




                                        Next
                                    End If
                                End If


                                If tidakcocok = 0 Then
                                    regexcocok = regexcocok + 1
                                End If

                            Next
                            If regexcocok = 0 Then
                                Throw New ApplicationException("Format TIN Is Not The Same With Current TIN Issued By")

                            End If
                        End Using
                    End If
                End If
            Else

                If Not String.IsNullOrEmpty(cmb_tinissuedby_cp.SelectedItemValue) Then
                    If String.IsNullOrEmpty(txt_tin_cp.Value) Then
                        Throw New ApplicationException(txt_tin_cp.FieldLabel + " is required.")
                    End If

                End If

            End If





            'Address Information CP
            'If String.IsNullOrEmpty(cmb_legaladdresstype_cp.SelectedItemValue) Then
            '    Throw New ApplicationException(cmb_legaladdresstype_cp.Label + " is required.")
            'End If

            If String.IsNullOrEmpty(cmb_countrycode_cp.SelectedItemValue) Then
                Throw New ApplicationException(cmb_countrycode_cp.Label + " is required.")
            End If

            If String.IsNullOrEmpty(txt_addressfree_cp.Value) Then
                Throw New ApplicationException(txt_addressfree_cp.FieldLabel + " is required.")
            End If

            If Not String.IsNullOrEmpty(txt_street_cp.Value) Then
                If String.IsNullOrEmpty(txt_city_cp.Value) Then
                    Throw New ApplicationException(txt_city_cp.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_buildingidentifier_cp.Value) Then
                If String.IsNullOrEmpty(txt_city_cp.Value) Then
                    Throw New ApplicationException(txt_city_cp.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_suiteidentifier_cp.Value) Then
                If String.IsNullOrEmpty(txt_city_cp.Value) Then
                    Throw New ApplicationException(txt_city_cp.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_flooridentifier_cp.Value) Then
                If String.IsNullOrEmpty(txt_city_cp.Value) Then
                    Throw New ApplicationException(txt_city_cp.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_districtname_cp.Value) Then
                If String.IsNullOrEmpty(txt_city_cp.Value) Then
                    Throw New ApplicationException(txt_city_cp.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_pob_cp.Value) Then
                If String.IsNullOrEmpty(txt_city_cp.Value) Then
                    Throw New ApplicationException(txt_city_cp.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_postcode_cp.Value) Then
                If String.IsNullOrEmpty(txt_city_cp.Value) Then
                    Throw New ApplicationException(txt_city_cp.FieldLabel + " is required.")
                End If
            End If
            If Not String.IsNullOrEmpty(txt_countrysubentity_cp.Value) Then
                If String.IsNullOrEmpty(txt_city_cp.Value) Then
                    Throw New ApplicationException(txt_city_cp.FieldLabel + " is required.")
                End If
            End If




            'Action save here
            Dim intPK As Long = -1

            If obj_Reporting_cp_Edit Is Nothing Then  'Add
                Dim objAddcp As New CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON
                If CRS_Report_Group_Class.list_Reporting_RGARPerson IsNot Nothing Then
                    If CRS_Report_Group_Class.list_Reporting_RGARPerson.Count > 0 Then
                        intPK = CRS_Report_Group_Class.list_Reporting_RGARPerson.Min(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID)
                        If intPK > 0 Then
                            intPK = -1
                        Else
                            intPK = intPK - 1
                        End If
                    End If

                End If

                IDCP = intPK

                With objAddcp
                    .PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID = intPK
                    .FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = Session("PKReportHeaderID")


                    'Controlling Person Information
                    .ResCountryCode = cmb_restcountrycode_cp.SelectedItemValue
                    .CtrlgPersonType = cmb_controllingpersontype_cp.SelectedItemValue
                    .FirstName = txt_firstname_cp.Value
                    '.FirstNameType = txt_firstnametype_cp.Value
                    .MiddleName = txt_middlename_cp.Value
                    '.MiddleNameType = txt_middlenametype_cp.Value
                    .LastName = txt_lastname_cp.Value
                    '.LastNameType = txt_lastnametype_cp.Value
                    .Name_NamePrefix = txt_nameprefix_cp.Value
                    .Name_GenerationIdentifier = txt_generationidentifier_cp.Value
                    .Name_Suffix = txt_namesufix_cp.Value
                    .Name_GeneralSuffix = txt_generalsufix_cp.Value
                    .NameType = cmb_nametype_cp.SelectedItemValue
                    .Name_PrecedingTitle = txt_precedingtitle_cp.Value
                    .Name_Title = txt_title_cp.Value
                    .BirthDate = DateBirthDate_cp.Value
                    .Nationality = cmb_nationality_cp.SelectedItemValue


                    'TIN Controlling Person Information
                    .TIN = txt_tin_cp.Value
                    .IssuedBy = cmb_tinissuedby_cp.SelectedItemValue

                    'Address Controlling Person Information
                    .legalAddressType = cmb_legaladdresstype_cp.SelectedItemValue
                    .CountryCode = cmb_countrycode_cp.SelectedItemValue
                    .AddressFree = txt_addressfree_cp.Value
                    .AddressFix_Street = txt_street_cp.Value
                    .AddressFix_BuildingIdentifier = txt_buildingidentifier_cp.Value
                    .AddressFix_SuiteIdentifier = txt_suiteidentifier_cp.Value
                    .AddressFix_FloorIdentifier = txt_flooridentifier_cp.Value
                    .AddressFix_DistrictName = txt_districtname_cp.Value
                    .AddressFix_POB = txt_pob_cp.Value
                    .AddressFix_PostCode = txt_postcode_cp.Value
                    .AddressFix_City = txt_city_cp.Value
                    .AddressFix_CountrySubentity = txt_countrysubentity_cp.Value
                    .BirthInfo_City = txt_birthcity_cp.Value
                    .BirthInfo_SubEntity = txt_birthcitysubentity_cp.Value
                    If cmb_birthcountrycode_cp.SelectedItemValue = "" Then
                        .BirthInfo_FormerCountryName = Nothing
                    Else

                        .BirthInfo_FormerCountryName = cmb_birthcountrycode_cp.SelectedItemValue
                    End If

                    If Me.IsNewRecord Then
                        objAddcp.Active = True
                        objAddcp.CreatedDate = DateTime.Now
                        objAddcp.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID


                    Else
                        objAddcp.LastUpdateDate = DateTime.Now
                        objAddcp.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End If


                End With
                Dim test = txtAddress_LegalAddressType.Value
                If CRS_Report_Group_Class.list_Reporting_RGARPerson Is Nothing Then
                    CRS_Report_Group_Class.list_Reporting_RGARPerson = New List(Of CRSDAL.CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON)
                End If
                CRS_Report_Group_Class.list_Reporting_RGARPerson.Add(objAddcp)
            Else    'Edit
                Dim objEditcp = CRS_Report_Group_Class.list_Reporting_RGARPerson.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID = obj_Reporting_cp_Edit.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID)
                If objEditcp IsNot Nothing Then
                    With objEditcp
                        IDCP = .PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID

                        'Controlling Person Information
                        .ResCountryCode = cmb_restcountrycode_cp.SelectedItemValue
                        .CtrlgPersonType = cmb_controllingpersontype_cp.SelectedItemValue
                        .FirstName = txt_firstname_cp.Value
                        '.FirstNameType = txt_firstnametype_cp.Value
                        .MiddleName = txt_middlename_cp.Value
                        '.MiddleNameType = txt_middlenametype_cp.Value
                        .LastName = txt_lastname_cp.Value
                        '.LastNameType = txt_lastnametype_cp.Value
                        .Name_NamePrefix = txt_nameprefix_cp.Value
                        .Name_GenerationIdentifier = txt_generationidentifier_cp.Value
                        .Name_Suffix = txt_namesufix_cp.Value
                        .Name_GeneralSuffix = txt_generalsufix_cp.Value
                        .NameType = cmb_nametype_cp.SelectedItemValue
                        .Name_PrecedingTitle = txt_precedingtitle_cp.Value
                        .Name_Title = txt_title_cp.Value
                        .BirthDate = DateBirthDate_cp.Value
                        .Nationality = cmb_nationality_cp.SelectedItemValue


                        'TIN Controlling Person Information
                        .TIN = txt_tin_cp.Value
                        .IssuedBy = cmb_tinissuedby_cp.SelectedItemValue

                        'Address Controlling Person Information
                        .legalAddressType = cmb_legaladdresstype_cp.SelectedItemValue
                        .CountryCode = cmb_countrycode_cp.SelectedItemValue
                        .AddressFree = txt_addressfree_cp.Value
                        .AddressFix_Street = txt_street_cp.Value
                        .AddressFix_BuildingIdentifier = txt_buildingidentifier_cp.Value
                        .AddressFix_SuiteIdentifier = txt_suiteidentifier_cp.Value
                        .AddressFix_FloorIdentifier = txt_flooridentifier_cp.Value
                        .AddressFix_DistrictName = txt_districtname_cp.Value
                        .AddressFix_POB = txt_pob_cp.Value
                        .AddressFix_PostCode = txt_postcode_cp.Value
                        .AddressFix_City = txt_city_cp.Value
                        .AddressFix_CountrySubentity = txt_countrysubentity_cp.Value
                        .BirthInfo_City = txt_birthcity_cp.Value
                        .BirthInfo_SubEntity = txt_birthcitysubentity_cp.Value
                        If cmb_birthcountrycode_cp.SelectedItemValue = "" Then
                            .BirthInfo_FormerCountryName = Nothing
                        Else

                            .BirthInfo_FormerCountryName = cmb_birthcountrycode_cp.SelectedItemValue
                        End If



                    End With
                End If
            End If

            'Bind to GridPanel
            Bind_Reporting_cp()

            'Hide window popup
            windowcp.Hidden = True

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_Reporting_cp()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Group_Class.list_Reporting_RGARPerson)


        gp_cp.GetStore().DataSource = objtable
        gp_cp.GetStore().DataBind()
        gp_cp_Detail.GetStore().DataSource = objtable
        gp_cp_Detail.GetStore().DataBind()

    End Sub


    Protected Sub Clean_Window_cp()
        'Controlling Person Information
        cmb_restcountrycode_cp.SetTextValue("")
        cmb_controllingpersontype_cp.SetTextValue("")
        txt_firstname_cp.Value = Nothing
        'txt_firstnametype_cp.Value = Nothing
        txt_middlename_cp.Value = Nothing
        'txt_middlenametype_cp.Value = Nothing
        txt_lastname_cp.Value = Nothing
        'txt_lastnametype_cp.Value = Nothing
        txt_nameprefix_cp.Value = Nothing
        txt_namesufix_cp.Value = Nothing
        txt_generationidentifier_cp.Value = Nothing
        txt_generalsufix_cp.Value = Nothing
        cmb_nametype_cp.SetTextValue("")
        txt_precedingtitle_cp.Value = Nothing
        txt_title_cp.Value = Nothing
        DateBirthDate_cp.Value = Nothing
        cmb_nationality_cp.SetTextValue("")
        txt_birthcity_cp.Value = Nothing
        txt_birthcitysubentity_cp.Value = Nothing
        cmb_birthcountrycode_cp.SetTextValue("")


        'TIN Controlling Person Information
        txt_tin_cp.Value = Nothing
        cmb_tinissuedby_cp.SetTextValue("")

        'Address Controlling Person Information
        cmb_legaladdresstype_cp.SetTextValue("")
        cmb_countrycode_cp.SetTextValue("")
        txt_addressfree_cp.Value = Nothing
        txt_street_cp.Value = Nothing
        txt_buildingidentifier_cp.Value = Nothing
        txt_suiteidentifier_cp.Value = Nothing
        txt_flooridentifier_cp.Value = Nothing
        txt_districtname_cp.Value = Nothing
        txt_pob_cp.Value = Nothing
        txt_postcode_cp.Value = Nothing
        txt_city_cp.Value = Nothing
        txt_countrysubentity_cp.Value = Nothing
    End Sub


    Protected Sub Load_Window_Report_cp(strAction As String)
        'Clean window pop up
        Clean_Window_cp()

        If obj_Reporting_cp_Edit IsNot Nothing Then
            'Populate fields
            With obj_Reporting_cp_Edit
                If Not IsNothing(.ResCountryCode) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .ResCountryCode)
                    If drTemp IsNot Nothing Then
                        cmb_restcountrycode_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                    End If
                End If
                If Not IsNothing(.CtrlgPersonType) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_CP_TYPE", "FK_CRS_CONTROLLING_PERSON_TYPE_CODE", .CtrlgPersonType)
                    If drTemp IsNot Nothing Then
                        cmb_controllingpersontype_cp.SetTextWithTextValue(drTemp("FK_CRS_CONTROLLING_PERSON_TYPE_CODE"), drTemp("CONTROLLING_PERSON_TYPE_NAME"))
                    End If
                End If
                txt_firstname_cp.Value = .FirstName
                'txt_firstnametype_cp.Value = .FirstNameType
                txt_middlename_cp.Value = .MiddleName
                'txt_middlenametype_cp.Value = .MiddleNameType
                txt_lastname_cp.Value = .LastName
                'txt_lastnametype_cp.Value = .LastNameType
                txt_nameprefix_cp.Value = .Name_NamePrefix
                txt_generationidentifier_cp.Value = .Name_GenerationIdentifier
                txt_namesufix_cp.Value = .Name_Suffix
                txt_generalsufix_cp.Value = .Name_GeneralSuffix
                If Not IsNothing(.NameType) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_NAME_TYPE", "FK_CRS_NAME_TYPE_CODE", .NameType)
                    If drTemp IsNot Nothing Then
                        cmb_nametype_cp.SetTextWithTextValue(drTemp("FK_CRS_NAME_TYPE_CODE"), drTemp("NAME_TYPE_NAME"))
                    End If
                End If
                txt_precedingtitle_cp.Value = .Name_PrecedingTitle
                txt_title_cp.Value = .Name_Title
                DateBirthDate_cp.Value = .BirthDate
                If Not IsNothing(.Nationality) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .Nationality)
                    If drTemp IsNot Nothing Then
                        cmb_nationality_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                    End If
                End If


                'TIN Controlling Person Information
                txt_tin_cp.Value = .TIN
                If Not IsNothing(.IssuedBy) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .IssuedBy)
                    If drTemp IsNot Nothing Then
                        cmb_tinissuedby_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                    End If
                End If

                'Address Controlling Person Information
                If Not IsNothing(.legalAddressType) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_LEGAL_ADDRESS", "FK_CRS_LEGAL_ADDRESS_TYPE_CODE", .legalAddressType)
                    If drTemp IsNot Nothing Then
                        cmb_legaladdresstype_cp.SetTextWithTextValue(drTemp("FK_CRS_LEGAL_ADDRESS_TYPE_CODE"), drTemp("LEGAL_ADDRESS_TYPE_NAME"))
                    End If
                End If
                If Not IsNothing(.CountryCode) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .CountryCode)
                    If drTemp IsNot Nothing Then
                        cmb_countrycode_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                    End If
                End If
                txt_addressfree_cp.Value = .AddressFree
                txt_street_cp.Value = .AddressFix_Street
                If .AddressFix_Street IsNot Nothing Then
                    txt_city_cp.AllowBlank = False
                    txt_city_cp.FieldStyle = "background-color:#FFE4C4"
                End If
                txt_buildingidentifier_cp.Value = .AddressFix_BuildingIdentifier
                txt_suiteidentifier_cp.Value = .AddressFix_SuiteIdentifier
                txt_flooridentifier_cp.Value = .AddressFix_FloorIdentifier
                txt_districtname_cp.Value = .AddressFix_DistrictName
                txt_pob_cp.Value = .AddressFix_POB
                txt_postcode_cp.Value = .AddressFix_PostCode
                txt_city_cp.Value = .AddressFix_City
                txt_countrysubentity_cp.Value = .AddressFix_CountrySubentity
                txt_birthcity_cp.Value = .BirthInfo_City
                txt_birthcitysubentity_cp.Value = .BirthInfo_SubEntity
                If Not IsNothing(.BirthInfo_FormerCountryName) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .BirthInfo_FormerCountryName)
                    If drTemp IsNot Nothing Then
                        cmb_birthcountrycode_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                    End If
                End If
            End With

        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            SetReadOnlyWindowReportingCP(False)
            btnsavecp.Hidden = False '' Added on 12 Aug 2021
        Else
            SetReadOnlyWindowReportingCP(True)
            btnsavecp.Hidden = True '' Added on 12 Aug 2021
        End If

        'Bind ATM
        IDCP = obj_Reporting_cp_Edit.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID

        'Show window pop up
        windowcp.Title = "Reporting Controlling Person - " & strAction
        windowcp.Hidden = False
    End Sub

    Protected Sub SetReadOnlyWindowReportingCP(isReadOnly As Boolean)
        Try


            'Controlling Person Information
            cmb_restcountrycode_cp.IsReadOnly = isReadOnly
            cmb_controllingpersontype_cp.IsReadOnly = isReadOnly
            txt_firstname_cp.ReadOnly = isReadOnly
            'txt_firstnametype_cp.ReadOnly = isReadOnly
            txt_middlename_cp.ReadOnly = isReadOnly
            'txt_middlenametype_cp.ReadOnly = isReadOnly
            txt_lastname_cp.ReadOnly = isReadOnly
            'txt_lastnametype_cp.ReadOnly = isReadOnly
            txt_nameprefix_cp.ReadOnly = isReadOnly
            txt_generationidentifier_cp.ReadOnly = isReadOnly
            txt_namesufix_cp.ReadOnly = isReadOnly
            txt_generalsufix_cp.ReadOnly = isReadOnly
            'cmb_nametype_cp.IsReadOnly = isReadOnly
            txt_precedingtitle_cp.ReadOnly = isReadOnly
            txt_title_cp.ReadOnly = isReadOnly
            DateBirthDate_cp.ReadOnly = isReadOnly
            txt_birthcity_group.ReadOnly = isReadOnly
            txt_birthcitysubentity_group.ReadOnly = isReadOnly
            cmb_birthcountrycode_cp.IsReadOnly = isReadOnly
            cmb_nationality_cp.IsReadOnly = isReadOnly

            'TIN Controlling Person Information
            txt_tin_cp.ReadOnly = isReadOnly
            cmb_tinissuedby_cp.IsReadOnly = isReadOnly

            'Address Controlling Person Information
            cmb_legaladdresstype_cp.IsReadOnly = isReadOnly
            cmb_countrycode_cp.IsReadOnly = isReadOnly
            txt_addressfree_cp.ReadOnly = isReadOnly
            txt_street_cp.ReadOnly = isReadOnly
            txt_buildingidentifier_cp.ReadOnly = isReadOnly
            txt_suiteidentifier_cp.ReadOnly = isReadOnly
            txt_flooridentifier_cp.ReadOnly = isReadOnly
            txt_districtname_cp.ReadOnly = isReadOnly
            txt_pob_cp.ReadOnly = isReadOnly
            txt_postcode_cp.ReadOnly = isReadOnly
            txt_city_cp.ReadOnly = isReadOnly
            txt_countrysubentity_cp.ReadOnly = isReadOnly

            If isReadOnly = False Then
                cmb_controllingpersontype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nametype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nationality_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_tinissuedby_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_legaladdresstype_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_countrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_restcountrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_birthcountrycode_cp.StringFieldStyle = "background-color:#FFFFFF"
            Else
                cmb_controllingpersontype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nametype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nationality_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_tinissuedby_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_legaladdresstype_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_countrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_restcountrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_birthcountrycode_cp.StringFieldStyle = "background-color:#FFFFFF"
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region


    Protected Sub chk_IsIndividualGroup_CheckedChanged(sender As Object, e As EventArgs)
        Try
            If chk_IsIndividualGroup.Checked = True Then
                cmb_NameType_Group.SetTextWithTextValue("OECD202", "OECD202 - Individu")
                txt_tin_group.FieldLabel = "TIN"
                cmb_org_tinissuedby_group.IsHidden = True
                cmb_tinissuedby_group.IsHidden = False
                cmb_AccountHolderType.SetTextValue("")
                cmb_AccountHolderType.IsHidden = True
                cmb_org_tintype.IsHidden = True
                txt_org_Name.Value = Nothing
                pnl_Organization_Information_group.Hidden = True
                pnl_individualinformation_Group.Hidden = False
                panelCP.Hidden = True
                Clean_Window_cp()
                CRS_Report_Group_Class.list_Reporting_RGARPerson = Nothing
                'Individual Information Group
                txt_name_Group.Value = Nothing
                txt_precedingtitle_group.Value = Nothing
                txt_title_group.Value = Nothing
                txt_firstname_group.Value = Nothing
                'txt_firstname_type_group.Value = Nothing
                txt_middlename_group.Value = Nothing
                'txt_middlename_type_group.Value = Nothing
                txt_lastname_group.Value = Nothing
                'txt_lastname_type_group.Value = Nothing
                txt_nameprefix_group.Value = Nothing
                txt_generationidentifier_group.Value = Nothing
                txt_namesufix_group.Value = Nothing
                txt_generalsufix_group.Value = Nothing
                datebirthdate_group.Value = Nothing
                txt_birthcity_group.Value = Nothing
                txt_birthcitysubentity_group.Value = Nothing
                cmb_birthcountrycode_group.SetTextValue("")
                cmb_nationality1_group.SetTextValue("")
                cmb_nationality2_group.SetTextValue("")
                cmb_nationality3_group.SetTextValue("")
                cmb_nationality4_group.SetTextValue("")
                cmb_nationality5_group.SetTextValue("")
            Else
                cmb_org_nametype.SetTextWithTextValue("OECD207", "OECD207 - Legal Name")
                cmb_AccountHolderType.IsHidden = False
                cmb_org_tintype.IsHidden = False
                cmb_org_tintype.SetTextWithTextValue("TIN", "TIN - Tax Identification Number")
                cmb_AccountHolderType.SetTextValue("")
                pnl_Organization_Information_group.Hidden = False
                pnl_individualinformation_Group.Hidden = True
                txt_org_Name.Value = Nothing
                txt_tin_group.FieldLabel = "IN"
                cmb_org_tinissuedby_group.IsHidden = False
                cmb_tinissuedby_group.IsHidden = True


            End If


        Catch ex As Exception

        End Try

    End Sub

    Protected Sub cmb_AccountHolderType_OnValueChanged(sender As Object, e As EventArgs)
        Try
            If cmb_AccountHolderType.SelectedItemValue = "CRS101" Then
                panelCP.Hidden = False
                windowcp.Visible = True
            Else
                panelCP.Hidden = True
                Clean_Window_cp()
                CRS_Report_Group_Class.list_Reporting_RGARPerson = Nothing
                windowcp.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub

End Class
