﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CRS_International_View.aspx.vb" Inherits="CRS_CRS_International_CRS_International_View" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/Component/AdvancedFilter.ascx" TagPrefix="uc1" TagName="AdvancedFilter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var columnAutoResize = function (grid) {

            App.GridpanelView.columns.forEach(function (col) {

               col.size = 40

            });
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };

        var prepareCommandCollection = function (grid, toolbar, rowIndex, record) {
            var users = <%=UserCRS %>;
            var GenerateButtonExcel 
            var GenerateButtonXML 
            if (users == "57") {
                GenerateButtonExcel = toolbar.items.get(2);
                GenerateButtonXML = toolbar.items.get(1);
            } else {
                GenerateButtonExcel = toolbar.items.get(4);
                GenerateButtonXML = toolbar.items.get(3);
            }
          
            //if (msg == "FATCA" && ctr == "US") {
            //    GenerateButtonExcel = toolbar.items.get(1);
            //    GenerateButtonXML = toolbar.items.get(2);
            //    GenerateButtonExcel.setDisabled(true)
            //    GenerateButtonExcel.re
            //    GenerateButtonXML.setDisabled(true)

            //} else {
               
            //    GenerateButtonExcel.setDisabled(false)
            //    GenerateButtonXML.setDisabled(false)
            //}
           

          
            var EditButton = toolbar.items.get(1);
            var wfa = record.data.isvalid
           
            var DeleteBUtton = toolbar.items.get(2);
            var wf = record.data.StatusName
            var ctr = record.data.ReceivingCountry
            var msg = record.data.MessageType

            var statusneedto = <%=NeedToCorrection %>;
           
            if (statusneedto == 0) {
                if (wf == "Need To Correction" || wf == "Draft" || wf == "In Progress Editing" ) { /*Edited by Felix 24 Nov 2020, skrng Generated (2) masih bisa di edit*/
                    GenerateButtonExcel.setDisabled(true)
                    GenerateButtonXML.setDisabled(true)
                } else {
                    GenerateButtonExcel.setDisabled(false)
                    GenerateButtonXML.setDisabled(false)
                }
               
            } else {
                if (wf == "Draft" || wf == "In Progress Editing") { /*Edited by Felix 24 Nov 2020, skrng Generated (2) masih bisa di edit*/
                    GenerateButtonExcel.setDisabled(true)
                    GenerateButtonXML.setDisabled(true)
                } else {
                    GenerateButtonExcel.setDisabled(false)
                    GenerateButtonXML.setDisabled(false)
                }
              
            }

            if (EditButton != undefined) {
                if (wf == "Waiting For Generate" || wf == "Need To Correction" || wf == "Reference No Uploaded" || wf == "Waiting For Upload Reference No" || wf == "Excel Generated" || wf == "Draft" || wf == "In Progress Editing") { /*Edited by Felix 24 Nov 2020, skrng Generated (2) masih bisa di edit*/
                    EditButton.setDisabled(false)
                } else {
                    EditButton.setDisabled(true)
                }
            }
            if (DeleteBUtton != undefined) {
                if (wf == "Waiting For Generate" || wf == "Need To Correction" || wf == "Waiting For Upload Reference No" || wf == "Excel Generated" || wf == "Draft") { /*Edited by Felix 24 Nov 2020, skrng Generated (2) masih bisa di edit*/
                    DeleteBUtton.setDisabled(false)
                } else {
                    DeleteBUtton.setDisabled(true)
                }
            }
           
        }


       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <ext:GridPanel ID="GridpanelView" ClientIDMode="Static" runat="server" Title="Title" Width="2000">
        <View>
            <ext:GridView runat="server" EnableTextSelection="true" />
        </View>
        <Store>
            <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="Store_ReadData">

                <Sorters>
                    <%--<ext:DataSorter Property="" Direction="ASC" />--%>
                </Sorters>
                <Proxy>
                    <ext:PageProxy />
                </Proxy>
            </ext:Store>
        </Store>
        <SelectionModel>
            <ext:CheckboxSelectionModel runat="server" Mode="Multi" CheckOnly="true">
            </ext:CheckboxSelectionModel>
        </SelectionModel>
        <Plugins>
            <%--<ext:GridFilters ID="GridFilters1" runat="server" />--%>
            <ext:FilterHeader ID="grdiheaderfilter" runat="server" Remote="true"></ext:FilterHeader>

        </Plugins>
        <BottomBar>

            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />

        </BottomBar>

        <%--Begin Update Penambahan Advance Filter--%>
        <DockedItems>
            <%--End Update Penambahan Advance Filter--%>
            <ext:Toolbar ID="Toolbar1" runat="server" EnableOverflow="true">
                <Items>

                    <ext:ComboBox runat="server" ID="cboExportExcel" Editable="false" FieldLabel="Export :">
                        <Items>
                            <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                            <ext:ListItem Text="CSV" Value="CSV"></ext:ListItem>
                        </Items>
                    </ext:ComboBox>
                    <ext:Button runat="server" ID="BtnExport" Text="Export Current Page" AutoPostBack="true" OnClick="ExportExcel" ClientIDMode="Static" />
                    <ext:Button runat="server" ID="BtnExportAll" Text="Export All Page" AutoPostBack="true" OnClick="ExportAllExcel" />
                    <%--<ext:Button ID="Button1" runat="server" Text="Print Current Page" Icon="Printer" Handler="this.up('grid').print();" />--%>
                    <ext:Button ID="BtnAdd" runat="server" Text="Add New Record" Icon="Add" Handler="NawadataDirect.BtnAdd_Click()"  />
                    <%--Begin Update Penambahan Advance Filter--%>
                    <ext:Button ID="AdvancedFilter" runat="server" Text="Advanced Filter" Icon="Magnifier" Handler="NawadataDirect.BtnAdvancedFilter_Click()" />
                    <%--End Update Penambahan Advance Filter--%>

                    <%-- Generate XML Bulk --%>
                    <ext:Button ID="btn_GenerateXML_Bulk" runat="server" Text="Generate Bulk" Icon="DiskDownload">
                        <DirectEvents>
                            <Click OnEvent="GenerateXML_Bulk"  >
                                <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <%-- End of Generate XML Bulk --%>
        
                </Items>
            </ext:Toolbar>
            <%--Begin Update Penambahan Advance Filter--%>
            <ext:Toolbar ID="Toolbar2" runat="server" EnableOverflow="true" Dock="Top" Hidden="true">
                <Items>
                    <ext:HyperlinkButton ID="HyperlinkButton1" runat="server" Text="Clear Advanced Filter">
                        <DirectEvents>
                            <Click OnEvent="btnClear_Click"></Click>
                        </DirectEvents>
                    </ext:HyperlinkButton>
                    <ext:Label ID="LblAdvancedFilter" runat="server" Text="">
                    </ext:Label>
                </Items>
            </ext:Toolbar>
        </DockedItems>
        <%--End Update Penambahan Advance Filter--%>

        <Listeners>
            <ViewReady Handler="columnAutoResize(this);
                    this.getStore().on('load', Ext.bind(columnAutoResize, null, [this]));"
                Delay="10" />
        </Listeners>
        <%--<SelectionModel>
            <ext:CheckboxSelectionModel runat="server" Mode="Single">
            </ext:CheckboxSelectionModel>
        </SelectionModel>--%>
    </ext:GridPanel>
     <%--Begin Update Penambahan Advance Filter--%>
    <ext:Panel ID="Panel1" runat="server" Hidden="true">
        <Content>
            <uc1:AdvancedFilter runat="server" ID="AdvancedFilter1" />
        </Content>
        <Items>
        </Items>
    </ext:Panel>
    <%--End Update Penambahan Advance Filter--%>

  

</asp:Content>



