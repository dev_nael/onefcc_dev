﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CRS_International_GenerateAdd.aspx.vb" Inherits="CRS_CRS_International_CRS_International_GenerateAdd" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        var onKeyUp = function (combo, e) {
            var v = combo.getRawValue();
            combo.store.filter(combo.displayField, new RegExp(v, "i"));
            combo.onLoad();
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
    <ext:FormPanel runat="server" ID="fpMain" BodyPadding="10" ButtonAlign="Center" Scrollable="Both" Title="Generate Report International- Add" Layout="AnchorLayout" Border="true" >
        <Items>
                 <ext:ComboBox ID="cbopjenisformat" runat="server" FieldLabel="Format" ForceSelection="True" AllowBlank="false" AnchorHorizontal="50%" EnableKeyEvents="true" MinChars="1" ValueField="id" DisplayField="format" TypeAhead="false">
                                <Store>
                                    <ext:Store ID="storeformat" runat="server">
                                        <Model>
                                            <ext:Model ID="ModelType" runat="server" IDProperty="id">
                                                <Fields>
                                                    <ext:ModelField Name="id" Type="Auto"></ext:ModelField>
                                                    <ext:ModelField Name="format" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
              
                            </ext:ComboBox>
             <ext:DisplayField ID="display_year" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Year" MaxLength="500" EnforceMaxLength="true" Hidden="true" />
             <ext:ComboBox ID="cboyear" runat="server" FieldLabel="Year" ForceSelection="True" AllowBlank="false" AnchorHorizontal="50%" EnableKeyEvents="true" MinChars="1" ValueField="year" DisplayField="year" TypeAhead="false" Editable="false" >
                                <Store>
                                    <ext:Store ID="storeyear" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server" IDProperty="id">
                                                <Fields>
                                                    <ext:ModelField Name="id" Type="Auto"></ext:ModelField>
                                                    <ext:ModelField Name="year" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
              
                            </ext:ComboBox>
                             <ext:ComboBox ID="cbocountry" runat="server" FieldLabel="Country" ForceSelection="True" AllowBlank="false" AnchorHorizontal="50%" EnableKeyEvents="true" MinChars="1" ValueField="formatdata" DisplayField="formatdata" TypeAhead="false">
                                <Store>
                                    <ext:Store ID="storecountry" runat="server">
                                        <Model>
                                            <ext:Model ID="Model2" runat="server" IDProperty="id">
                                                <Fields>
                                                    <ext:ModelField Name="id" Type="Auto"></ext:ModelField>
                                                    <ext:ModelField Name="formatdata" Type="String"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
              
                            </ext:ComboBox>
            <ext:DisplayField ID="display_country" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Country" MaxLength="500" EnforceMaxLength="true" Hidden="true" />
                <ext:Button runat="server" ID="btnSearch" Text="Search" Hidden="false"  Icon="PlayGreen" >
              
                <DirectEvents>
                    <Click OnEvent="btnSearch_Click">
                        <EventMask Msg="Processing..." MinDelay="500" ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
             <ext:GridPanel ID="gp_country" runat="server" Border="true" MarginSpec="10 0" Hidden="true"  ClientIDMode="Static" Title="County Ready to Generate" >
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="Store1" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="model3" IDProperty="PK_CRS_INTERNATIONAL_REPORT_HEADER_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Type="String" ></ext:ModelField>
                                            <ext:ModelField Name="ReceivingCountry" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                          <ext:ModelField Name="TotalAccount" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                               
                                 <ext:Column ID="Column8" runat="server" DataIndex="PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Text="ID" MinWidth="100"></ext:Column>
                                        <ext:Column ID="Column1" runat="server" DataIndex="ReceivingCountry" Text="Country" MinWidth="200"></ext:Column>
                                        <ext:Column ID="Column6" runat="server" DataIndex="Status" Text="Status" MinWidth="400"></ext:Column>
                                        <ext:Column ID="Column7" runat="server" DataIndex="TotalAccount" Text="Total Account" MinWidth="400"></ext:Column>
                             

                             <%--                 
                                <ext:CommandColumn ID="gp_country_CommandColumn" runat="server" Text="Action" CellWrap="true">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail Data"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="gp_country_Gridcommand">
                                        <EventMask ShowMask="true"></EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>--%>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:CheckboxSelectionModel runat="server" Mode="Multi" CheckOnly="true">
                               <%-- <DirectEvents>
                                    <SelectionChange OnEvent="sm_DownloadFromScreeningResult_change">
                                    </SelectionChange>
                                </DirectEvents>--%>
                            </ext:CheckboxSelectionModel>
                        </SelectionModel>
                        <Plugins>
                        <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="false" >
                                <Items>  
                                    <%-- <ext:Button runat="server" Text="Close Picker" ID="Button5">
                                    <Listeners>
                                    <Click Handler="#{window_SEARCH_CUSTOMER}.hide()"> </Click>
                                    </Listeners>
                                    </ext:Button>--%>
                                    <ext:Label runat="server" ID="LabelScreeningResultCount">

                                    </ext:Label>
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
                    </ext:GridPanel>
                
         
                <ext:GridPanel ID="gp_country_All" runat="server" Border="true" MarginSpec="10 0" Hidden="true"  Title="Country ready to Generate" >
                 
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                     <ext:Store ID="Store2" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="model4" IDProperty="PK_CRS_INTERNATIONAL_REPORT_HEADER_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Type="String" ></ext:ModelField>
                                            <ext:ModelField Name="ReceivingCountry" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TotalAccount" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                 
                            <ext:Column ID="Column9" runat="server" DataIndex="PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Text="ID" MinWidth="100"></ext:Column>
                                        <ext:Column ID="Column15" runat="server" DataIndex="ReceivingCountry" Text="Country" MinWidth="200"></ext:Column>
                                        <ext:Column ID="Column16" runat="server" DataIndex="Status" Text="Status" MinWidth="400"></ext:Column>
                                        <ext:Column ID="Column4" runat="server" DataIndex="TotalAccount" Text="Total Account" MinWidth="400"></ext:Column>

                       

                   <%--     <ext:NumberColumn ID="NumberColumn2" runat="server" DataIndex="TotalSimilarityPct" Text="Similarity" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                        --%>
                    <%--    <ext:CommandColumn ID="gp_country_All_CommandColumn" runat="server" Text="Action" CellWrap="true">
                            <Commands>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail Data"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gp_country_Gridcommand">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>--%>
                    </Columns>
                </ColumnModel>
           
                <Plugins>
                    <ext:FilterHeader runat="server"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

                 <ext:GridPanel ID="gp_country_invalid" runat="server" Border="true" MarginSpec="10 0" Hidden="true"  Title="Country Not Ready To Generate" >
                 
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                     <ext:Store ID="Store3" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="model5" IDProperty="PK_CRS_INTERNATIONAL_REPORT_HEADER_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Type="String" ></ext:ModelField>
                                            <ext:ModelField Name="ReceivingCountry" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="TotalAccount" Type="String"></ext:ModelField>
                                        
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                 
                       <ext:Column ID="Column2" runat="server" DataIndex="PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Text="ID" MinWidth="100"></ext:Column>
                                        <ext:Column ID="Column3" runat="server" DataIndex="ReceivingCountry" Text="Country" MinWidth="200"></ext:Column>
                                        <ext:Column ID="Column18" runat="server" DataIndex="Status" Text="Status" MinWidth="400"></ext:Column>
                        <ext:Column ID="Column5" runat="server" DataIndex="TotalAccount" Text="Total Account" MinWidth="400"></ext:Column>
                                                 

                       

                   <%--     <ext:NumberColumn ID="NumberColumn2" runat="server" DataIndex="TotalSimilarityPct" Text="Similarity" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                        --%>
                      <%--  <ext:CommandColumn ID="gp_country_invalid_CommandColumn" runat="server" Text="Action" CellWrap="true">
                            <Commands>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail Data"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gp_country_Gridcommand">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>--%>
                    </Columns>
                </ColumnModel>
           
                <Plugins>
                   <ext:FilterHeader runat="server"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>
           <%-- <ext:TextField ID="txtName" runat="server" FieldLabel="Name" AnchorHorizontal="30%" AllowBlank="false" />
            <ext:TextArea ID="txtAddress" runat="server" FieldLabel="Address" AnchorHorizontal="30%" AllowBlank="false" />
            <ext:TextField ID="txtPhone" runat="server" FieldLabel="Phone" AnchorHorizontal="30%" AllowBlank="false" />
            <ext:TextField ID="txtEmail" runat="server" FieldLabel="Email" AnchorHorizontal="30%" AllowBlank="false" />--%>
        </Items>            
        <Buttons>
        
            <ext:Button runat="server" ID="btnSave" Text="Save Request"  Hidden="true" Icon="Disk">
                
                <DirectEvents>
                    <Click OnEvent="btnSave_Click">
                        <EventMask Msg="Processing..." MinDelay="500" ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="BtnCancel" runat="server" Text="Cancel" Icon="Cancel">
                <Listeners>
                    <Click Handler="#{BtnCancel}.disable()" />
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="btnCancel_DirectEvent">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="500">
                        </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    
    <%-- <ext:Window ID="windowreportheader" runat="server" Modal="true" Maximized="true" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true" Scrollable="both" ButtonAlign="Center" BodyPadding="10" Title="CRS International Report Detail">
        <Items>
             <ext:panel runat="server" ID="fpReportHeader"  BodyPadding="10" ButtonAlign="Center" Title="Report Header" Layout="AnchorLayout" Border="true">
                <Content>
                        <ext:DisplayField ID="txtID" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="ID" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                      <ext:DisplayField ID="displayStatus" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Status" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>    
                    <ext:TextField ID="txtSendingCompany" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Sending Company IN" MaxLength="500" EnforceMaxLength="true"  ReadOnly="true" />
                        <ext:TextField ID="txtTransmittingCountry" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Transmitting Country" MaxLength="500" EnforceMaxLength="true" readonly="true"/>
                        <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_ReceivingCountry" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Receiving Country" AnchorHorizontal="80%" AllowBlank="false" IsReadOnly="true" />
                        <ext:TextField ID="txtMessageType" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Sending Company IN" MaxLength="500" EnforceMaxLength="true" ReadOnly="true" />
                        <ext:TextArea ID="txtWarning" runat="server"  AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Warning" MaxLength="500" EnforceMaxLength="true"  ReadOnly="true"/>
                        <ext:TextField ID="txtContact" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Contact" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                         <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_LJK" ValueField="FK_CRS_LJK_TYPE_CODE" DisplayField="LJK_TYPE_NAME" StringField="FK_CRS_LJK_TYPE_CODE, LJK_TYPE_NAME" StringTable="vw_crs_international_ljk_type" Label="Jenis Lembaga Keuangan" AnchorHorizontal="80%" AllowBlank="false" IsReadOnly="true"/>   
                        <ext:TextField ID="txtMessageRefID" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Message Ref ID" MaxLength="500" EnforceMaxLength="true" ReadOnly="true" />
                        <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_MessageTypeInd" ValueField="FK_MESSAGE_TYPE_INDICATOR_CODE" DisplayField="MESSAGE_TYPE_INDICATOR_NAME" StringField="FK_MESSAGE_TYPE_INDICATOR_CODE, MESSAGE_TYPE_INDICATOR_NAME" StringTable="CRS_INTERNATIONAL_MESSAGE_TYPE_INDICATOR" Label="Message Type Indicator" AnchorHorizontal="80%" AllowBlank="false" IsReadOnly="true" />
                        <ext:TextField ID="txtCorrectionMessageRefID" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Correction Message Ref ID" MaxLength="500" EnforceMaxLength="true"  ReadOnly="true"/>   
                        <ext:DateField ID="txtReportingPeriod" runat="server"  AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Reporting Period"    Format="dd-MMM-yyyy"  ReadOnly="true"/>
                        <ext:DateField ID="txtTimeStamp" runat="server"  AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Timestamp"   Format="dd-MMM-yyyy"  ReadOnly="true"/>
                       <ext:Panel runat="server" ID="panelReportFI" Border="true" Layout="AnchorLayout" title="Reporting FI" BodyPadding="15" MarginSpec="10 0 0 0" >
                        <Content>
                        <ext:TextField ID="txtIN" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="IN" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtIN_IssueBy" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="IN Issued By" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtIN_INType" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="IN Type" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_DocTypeIndic" ValueField="FK_CRS_DOC_TYPE_INDIC_CODE" DisplayField="DOC_TYPE_INDIC_NAME" StringField="FK_CRS_DOC_TYPE_INDIC_CODE, DOC_TYPE_INDIC_NAME" StringTable="vw_CRS_International_Doc_Type_Indic" Label="Doc Type Indic" AnchorHorizontal="80%" AllowBlank="false" IsReadOnly="true"/>
                        <ext:TextField ID="txtDocRefID" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Doc Ref ID" MaxLength="500" EnforceMaxLength="true" ReadOnly="true" >
                        </ext:TextField>
                        <ext:TextField ID="txtCorDocRefID" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Correction Doc Ref ID" MaxLength="500" EnforceMaxLength="true"  ReadOnly="true"/>
                        <ext:TextField ID="txtResCountryCode" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Res Country Code" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>    
                        <ext:TextField ID="txtName" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Name" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtName_Type" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Name Type" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                       <ext:Panel runat="server" ID="panelreportfiaddressformation" Border="true" Layout="AnchorLayout" title="Address Information" BodyPadding="15" MarginSpec="10 0 0 0">
                        <Content>
                        <ext:TextField ID="txtAddress_LegalAddressType" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Legal Address Type" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_CountryCode" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Country Code" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFree" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Address Free" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_Street" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Street" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_BuildingIdentifier" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Building Identifier" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_SuiteIdentifier" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Suite Identifier" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_FloorIdentifier" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Floor Identifier" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_DistrictName" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="District Name" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_POB" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="POB" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_PostCode" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Post Code" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_City" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="City" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_CountrySubentity" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Country Sub Entity" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                    </Content>
                           </ext:Panel>
                        </Content>
                           </ext:Panel>
                        </Content>
            </ext:panel>
              <ext:panel runat="server" ID="fpReportingGroup"  BodyPadding="10" ButtonAlign="Center"  Title="Reporting Group" Layout="AnchorLayout" Border="true"  StyleSpec="border-color: #157fcc !important;margin-top:10px" >
                <Items>
                     <ext:GridPanel ID="gp_Report_Group" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                    
                        <Store>
                            <ext:Store ID="store_Report_Group" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model43" IDProperty="PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="AccountNumber" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Account_AcctNumberType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IsIndividualYN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AccountBalance" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IsValid" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="DocSpec_DocTypeIndic" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No"></ext:RowNumbererColumn>
                                 <ext:Column ID="Column17" runat="server" DataIndex="DocSpec_DocTypeIndic" Text="Doc Type Indic" MinWidth="200"></ext:Column>
                                  <ext:Column ID="Column412" runat="server" DataIndex="AccountNumber" Text="Account Number" MinWidth="200"></ext:Column>
                                
                                <ext:Column ID="Column4" runat="server" DataIndex="IsIndividualYN" Text="Is Individual?" MinWidth="150"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn1" runat="server" DataIndex="AccountBalance" Text="Account Balance" MinWidth="150" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                <ext:Column ID="Column10" runat="server" DataIndex="IsValid" Text="Is Valid?" MinWidth="150"></ext:Column>
                                <ext:CommandColumn ID="cc_Reporting_Group" runat="server" Text="Action" MinWidth="220">
                                    <Commands>        
                                       <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="gc_Reporting_Group">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                  
                </Items>
            </ext:panel>
             
               
        </Items>
        <Buttons>
          
            <ext:Button ID="btnCancelReportHeader" runat="server" Text="Back" Icon="PageBack">
                <Listeners>
                    <Click Handler="#{btnCancelReportHeader}.disable()" />
                </Listeners>
                <DirectEvents>
                    <Click OnEvent="btnCancelReportHeader_DirectEvent">
                        <EventMask ShowMask="true" Msg="Loading..." MinDelay="500">
                        </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
       
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.7});" />

            <Resize Handler="#{windowreportheader}.center()" />
        </Listeners>
    </ext:Window>
      <%-- ================== REPORTING GROUP WINDOWS ========================== --%>
   
    <%-- ================== END OF REPORTING GROUP WINDOWS ========================== --%>
        <%-- Pop Up Window Payment --%>

    <%-- End of Pop Up Window Payment --%>
  
      <%-- ================== CONFIRMATION PANEL ========================== --%>
    <ext:FormPanel ID="pnl_Confirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="lbl_Confirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Confirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <%-- ================== END OF CONFIRMATION PANEL ========================== --%>
</asp:Content>



