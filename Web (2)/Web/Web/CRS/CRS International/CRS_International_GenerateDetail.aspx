﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CRS_International_GenerateDetail.aspx.vb" Inherits="CRS_CRS_International_CRS_International_GenerateDetail" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        var onKeyUp = function (combo, e) {
            var v = combo.getRawValue();
            combo.store.filter(combo.displayField, new RegExp(v, "i"));
            combo.onLoad();
        };

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };


        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
    <ext:FormPanel runat="server" ID="fpMain" BodyPadding="10" ButtonAlign="Center" Scrollable="Both" Title="Generate Report International - Detail" Layout="AnchorLayout" Border="true" >
        <Items>
             <ext:DisplayField ID="display_format" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Format" MaxLength="500" EnforceMaxLength="true" />
             <ext:DisplayField ID="display_year" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Year" MaxLength="500" EnforceMaxLength="true" />
                    <ext:DisplayField ID="display_country" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Country" MaxLength="500" EnforceMaxLength="true" />
                        
         
                <ext:GridPanel ID="gp_country_All" runat="server" Border="true" MarginSpec="10 0"  Title="Country ready to Generate" Layout="AnchorLayout" >
                 
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                     <ext:Store ID="Store2" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="model4" IDProperty="PK_CRS_INTERNATIONAL_REPORT_HEADER_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Type="String" ></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                        <ext:ModelField Name="StatusName" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="TotalAccount" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                 
                            <ext:Column ID="Column9" runat="server" DataIndex="PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Text="ID" MinWidth="100"></ext:Column>
                                        <ext:Column ID="Column15" runat="server" DataIndex="Country" Text="Country" MinWidth="200"></ext:Column>
                                        <ext:Column ID="Column16" runat="server" DataIndex="StatusName" Text="Status" MinWidth="400"></ext:Column>
                         <ext:Column ID="Column5" runat="server" DataIndex="TotalAccount" Text="Total Account" MinWidth="400"></ext:Column>
                                                 

                       

                   <%--     <ext:NumberColumn ID="NumberColumn2" runat="server" DataIndex="TotalSimilarityPct" Text="Similarity" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                        --%>
                    <%--    <ext:CommandColumn ID="gp_country_All_CommandColumn" runat="server" Text="Action" CellWrap="true">
                            <Commands>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail Data"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gp_country_Gridcommand">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>--%>
                    </Columns>
                </ColumnModel>
           
                <Plugins>
                    <ext:FilterHeader runat="server"></ext:FilterHeader>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>

           <%--      <ext:GridPanel ID="gp_country_invalid" runat="server" Border="true" MarginSpec="10 0"  Title="Country Not Ready To Generate" >
                 
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <Store>
                     <ext:Store ID="Store3" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="model5" IDProperty="PK_report_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_report_ID" Type="String" ></ext:ModelField>
                                            <ext:ModelField Name="Country" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Status" Type="String"></ext:ModelField>
                                         <ext:ModelField Name="StatusName" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                 
                       <ext:Column ID="Column2" runat="server" DataIndex="PK_report_ID" Text="ID" MinWidth="100"></ext:Column>
                                        <ext:Column ID="Column3" runat="server" DataIndex="Country" Text="Country" MinWidth="200"></ext:Column>
                                        <ext:Column ID="Column18" runat="server" DataIndex="StatusName" Text="Status" MinWidth="400"></ext:Column>
                                                 

                       

                   <%--     <ext:NumberColumn ID="NumberColumn2" runat="server" DataIndex="TotalSimilarityPct" Text="Similarity" Format="#,##0.00 %" Align="Right"></ext:NumberColumn>
                        --%>
                      <%--  <ext:CommandColumn ID="gp_country_invalid_CommandColumn" runat="server" Text="Action" CellWrap="true">
                            <Commands>
                                <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                    <ToolTip Text="Detail Data"></ToolTip>
                                </ext:GridCommand>
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="gp_country_Gridcommand">
                                    <EventMask ShowMask="true"></EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID" Mode="Raw"></ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>--%>
                    <%--</Columns>
                </ColumnModel>
           
                <Plugins>
                    <%--<ext:FilterHeader runat="server"></ext:FilterHeader>--%>
               <%-- </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                </BottomBar>
            </ext:GridPanel>--%>
          <ext:DisplayField ID="display_totalxmlvalidate" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Total XML Validate" MaxLength="500" EnforceMaxLength="true" />
            <ext:DisplayField ID="display_totalxmlgenerated" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Total XML Generated" MaxLength="500" EnforceMaxLength="true" />
<ext:DisplayField ID="display_statusAPI" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Status API" MaxLength="500" EnforceMaxLength="true" />
<ext:DisplayField ID="display_errormassageapi" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Error Message API" MaxLength="500" EnforceMaxLength="true" />
           <%-- <ext:TextField ID="txtName" runat="server" FieldLabel="Name" AnchorHorizontal="30%" AllowBlank="false" />
            <ext:TextArea ID="txtAddress" runat="server" FieldLabel="Address" AnchorHorizontal="30%" AllowBlank="false" />
            <ext:TextField ID="txtPhone" runat="server" FieldLabel="Phone" AnchorHorizontal="30%" AllowBlank="false" />
            <ext:TextField ID="txtEmail" runat="server" FieldLabel="Email" AnchorHorizontal="30%" AllowBlank="false" />--%>
                <ext:Button ID="btnDownloadXML" runat="server" Text="Download" Icon="Disk" Hidden="true" StyleSpec="margin-top:10px;margin-bottom:10px;">
                                <DirectEvents>
                                    <Click OnEvent="btnDownloadXML_DirectClick" Success="NawadataDirect.Download({isUpload : true});">
                                        <EventMask MinDelay="500" Msg="Validating..." ShowMask="true"></EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                   <ext:GridPanel ID="GridPanelValidation" ClientIDMode="Static" runat="server" Height="400" Title="Validation Schema">
                <Store>
                    <ext:Store ID="StoreView" runat="server" RemoteFilter="true" RemoteSort="true" OnReadData="StoreValidation_ReadData">
                        <Sorters>
                        </Sorters>
                        <Proxy>

                            <ext:PageProxy />
                        </Proxy>
                        <Reader>
                        </Reader>

                        <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="UnikReference" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ErrorMessage" Type="String"></ext:ModelField>

                                        </Fields>
                                    </ext:Model>
                                </Model>
                    </ext:Store>

                </Store>
                  <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No" MinWidth="50"></ext:RowNumbererColumn>
                                <ext:Column ID="Column7" runat="server" DataIndex="UnikReference" Text="Unik Reference" MinWidth="130" Flex="1"></ext:Column>
                                <ext:Column ID="Column8" runat="server" DataIndex="ErrorMessage" Text="ErrorMessage" MinWidth="130" Flex="1"></ext:Column>

                            </Columns>
                        </ColumnModel>
                <BottomBar>

                <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True">
                    <Items>

                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
                 <View>
                <ext:GridView runat="server" EnableTextSelection="true" />
            </View>
            </ext:GridPanel>

             <%--add 07-Jan-2022--%>
            <ext:Panel ID="panel_TaskManager" runat="server" MarginSpec="10 0" Border="true" BodyPadding="10" Hidden="true">
                <Content>
                    <ext:TaskManager ID="TaskManager1" runat="server">
                        <Tasks>
                            <ext:Task
                                TaskID="refreshScreeningStatus"
                                Interval="20000"
                                AutoRun="false"
                                >
                                <DirectEvents>
                                    <Update OnEvent="LoadData">
                                        <%--<EventMask Msg="Loading..." ShowMask="true" MinDelay="200"></EventMask>--%>
                                    </Update>
                                </DirectEvents>
                            </ext:Task>
                        </Tasks>
                    </ext:TaskManager>
                </Content>
            </ext:Panel>
            <%--End 07-Jan-2022--%>
        </Items>            
        <Buttons>
        
         <ext:Button ID="btnValidateSchema" runat="server" Text="Validate Schema" Icon="Disk" >
                <DirectEvents>
                    <Click OnEvent="BtnValidate_DirectClick">
                        <EventMask MinDelay="500" Msg="Validating..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>

            <ext:Button ID="btnSaveUpload" runat="server" Text="Generate" Icon="Disk" >
                <DirectEvents>
                    <Click OnEvent="BtnSave_DirectClick"  Success="NawadataDirect.Download({isUpload : true});">
                        <ExtraParams>
                            <ext:Parameter Name="command" Value="New" Mode="Value">
                            </ext:Parameter>

                        </ExtraParams>
                        <EventMask Msg="Loading..." ShowMask="true" ></EventMask>
                    </Click>
                </DirectEvents>

            </ext:Button>
            <ext:Button ID="btnCancelUpload" runat="server" Text="Back" Icon="Cancel">
                <DirectEvents>
                    <Click OnEvent="btnCancelUpload_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
   
   
   
    <%-- ================== END OF REPORTING GROUP WINDOWS ========================== --%>
        <%-- Pop Up Window Payment --%>

    <%-- End of Pop Up Window Payment --%>
  
      <%-- ================== CONFIRMATION PANEL ========================== --%>
    <ext:FormPanel ID="pnl_Confirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="lbl_Confirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Confirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <%-- ================== END OF CONFIRMATION PANEL ========================== --%>
</asp:Content>
