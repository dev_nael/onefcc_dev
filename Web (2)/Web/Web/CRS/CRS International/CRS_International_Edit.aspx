﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CRS_International_Edit.aspx.vb" Inherits="CRS_CRS_International_CRS_International_Edit" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript">
        var onKeyUp = function (combo, e) {
            var v = combo.getRawValue();
            combo.store.filter(combo.displayField, new RegExp(v, "i"));
            combo.onLoad();
        };
       
      
    </script>
     <style type="text/css">
        .x-form-item-label.x-form-item-label-default { width : 230px !important; }
        #ContentPlaceHolder1_fpReportingGroup-innerCt{padding: 0px 0px 0px 0px !important;}
        #ContentPlaceHolder1_pnl_payment-innerCt{padding: 0px 0px 0px 0px !important;}
        #ContentPlaceHolder1_panelCP-innerCt{padding: 0px 0px 0px 0px !important;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <ext:FormPanel runat="server" ID="fpMain" BodyPadding="10" ButtonAlign="Center"  Title="CRS International Report Edit" Layout="AnchorLayout" Scrollable="Both">
         <DockedItems>
            <ext:Toolbar ID="ToolbarInput" ClientIDMode="Static" runat="server" EnableOverflow="true" Dock="Top" Layout="FitLayout" MaxHeight="150"  >                
                <Items>
                      <ext:InfoPanel ID="info_ValidationResult" ClientIDMode="Static" runat="server" UI="Warning" Layout="AnchorLayout" Title="Validation Result" AutoScroll="true" IconCls="#Error" MarginSpec="5 10 0 0" Border="True" Hidden="true" Collapsible="true"></ext:InfoPanel>
                </Items>
            </ext:Toolbar>                                                     
        </DockedItems>
        <Items>
            <ext:panel runat="server" ID="fpReportHeader"  BodyPadding="10" ButtonAlign="Center"  Title="Report Header" Layout="AnchorLayout" Border="true">
                <Content>
                        <ext:DisplayField ID="txtID" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="ID" MaxLength="500" EnforceMaxLength="true" />
                         <ext:DisplayField ID="displayStatus" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Status" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                    <ext:TextField ID="txtSendingCompany" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Sending Company IN" MaxLength="500" EnforceMaxLength="true" ReadOnly="true" />
                        <ext:TextField ID="txtTransmittingCountry" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Transmitting Country" MaxLength="500" EnforceMaxLength="true" readonly="true"/>
                        <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_ReceivingCountry" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Receiving Country" AnchorHorizontal="80%" AllowBlank="false" IsReadOnly="true" />
                        <ext:TextField ID="txtMessageType" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Message Type" MaxLength="500" EnforceMaxLength="true" ReadOnly="true" />
                        <ext:TextArea ID="txtWarning" runat="server"  AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Warning" MaxLength="4000" EnforceMaxLength="true" />
                        <ext:DateField ID="txtReportingPeriod" runat="server"  AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Reporting Period"    Format="dd-MMM-yyyy"  ReadOnly="true"/>
                         <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_LJK" ValueField="FK_CRS_LJK_TYPE_CODE" DisplayField="LJK_TYPE_NAME" StringField="FK_CRS_LJK_TYPE_CODE, LJK_TYPE_NAME" StringTable="CRS_INTERNATIONAL_LJK_TYPE" Label="Jenis Lembaga Keuangan" AnchorHorizontal="80%" AllowBlank="false"  OnOnValueChanged="cmb_LJK_OnValueChanged" IsReadOnly="true"/>   
                      <ext:TextField ID="txtContact" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Contact" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                    <ext:TextField ID="txtMessageRefID" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Message Ref ID" MaxLength="500" EnforceMaxLength="true" ReadOnly="true" />
                        <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_MessageTypeInd" ValueField="FK_MESSAGE_TYPE_INDICATOR_CODE" DisplayField="MESSAGE_TYPE_INDICATOR_NAME" StringField="FK_MESSAGE_TYPE_INDICATOR_CODE, MESSAGE_TYPE_INDICATOR_NAME" StringTable="vw_CRS_INTERNATIONAL_MESSAGE_TYPE_INDICATOR" Label="Message Type Indicator" AnchorHorizontal="80%" AllowBlank="false"  IsReadOnly="true" />
                        <ext:TextField ID="txtCorrectionMessageRefID" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Correction Message Ref ID" MaxLength="500" EnforceMaxLength="true" Hidden="true"/>   
                        <ext:DateField ID="txtTimeStamp" runat="server"  AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Timestamp"   Format="dd-MMM-yyyy" ReadOnly="true"/>
                        <ext:Panel runat="server" ID="panelReportFI" Border="true" Layout="AnchorLayout" title="Reporting FI" BodyPadding="15" MarginSpec="10 0 0 0">
                        <Content>
                        <ext:TextField ID="txtIN" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="IN" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtIN_IssueBy" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="IN Issued By" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtIN_INType" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="IN Type" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                              <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_DocTypeIndic" ValueField="FK_CRS_DOC_TYPE_INDIC_CODE" DisplayField="DOC_TYPE_INDIC_NAME" StringField="FK_CRS_DOC_TYPE_INDIC_CODE, DOC_TYPE_INDIC_NAME" StringTable="vw_CRS_International_Doc_Type_Indic" Label="Doc Type Indic" AnchorHorizontal="80%" AllowBlank="false"   OnOnValueChanged="cmb_DocTypeIndic_OnValueChanged" IsHidden="false"/>
                                        <NDS:NDSDropDownField runat="server" LabelWidth="150" ID="cmb_DocTypeIndic_Correction" ValueField="FK_CRS_DOC_TYPE_INDIC_CODE" DisplayField="DOC_TYPE_INDIC_NAME" StringField="FK_CRS_DOC_TYPE_INDIC_CODE, DOC_TYPE_INDIC_NAME" StringTable="Vw_CRS_International_ReportFI_DocTypeIndic" Label="Doc Type Indic" AnchorHorizontal="80%" AllowBlank="false" OnOnValueChanged="cmb_DocTypeIndic_OnValueChanged" IsHidden="true"/>
                            <ext:FieldSet runat="server" ColumnWidth="0.01" ID="FieldSet3" Border="false" Layout="ColumnLayout" StyleSpec="margin-left:-10px">    
                   <Content>
                        <ext:TextField ID="txtDocRefID" runat="server" AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Doc Ref ID" MaxLength="500" EnforceMaxLength="true" ReadOnly="true" Width="745" StyleSpec="margin-right:10px">
                            <%--    <DirectEvents>
                                    <Change OnEvent="txtDocRefID_Changed"></Change>
                                </DirectEvents>--%>
                        </ext:TextField>
                                   
                       </Content>
                        </ext:FieldSet>
                        <ext:TextField ID="txtCorDocRefID" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Correction Doc Ref ID" MaxLength="500" EnforceMaxLength="true" Hidden="true"/>
                               <ext:TextField ID="txtResCountryCode" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Res Country Code" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtName" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Name" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtName_Type" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Name Type" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
     
                            <ext:Panel runat="server" ID="panelreportfiaddressformation" Border="true" Layout="AnchorLayout" title="Address Information" BodyPadding="15" MarginSpec="10 0 0 0">
                        <Content>
                                 <ext:TextField ID="txtAddress_LegalAddressType" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Legal Address Type" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_CountryCode" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Country Code" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFree" runat="server" AnchorHorizontal="80%" AllowBlank="false" FieldLabel="Address Free" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_Street" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Street" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_BuildingIdentifier" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Building Identifier" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_SuiteIdentifier" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Suite Identifier" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_FloorIdentifier" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Floor Identifier" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_DistrictName" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="District Name" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_POB" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="POB" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_PostCode" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Post Code" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_City" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="City" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                        <ext:TextField ID="txtAddress_AddressFix_CountrySubentity" runat="server" AnchorHorizontal="80%" AllowBlank="true" FieldLabel="Country Sub Entity" MaxLength="500" EnforceMaxLength="true" ReadOnly="true"/>
                  </Content>
                                 </ext:Panel>
                            </Content>
                         </ext:Panel>                    </Content>
            </ext:panel>
            <%--Reporting CRS701--%>
              <ext:panel runat="server" ID="fpReportingGroup"  BodyPadding="10" ButtonAlign="Center" Title="Reporting Group" Layout="AnchorLayout" Border="true"  StyleSpec="border-color: #157fcc !important;margin-top:10px" >
                <Items>
                     <ext:GridPanel ID="gp_Report_Group" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Report_Group_Add" Text="Add New Record" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Report_Group_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="store_Report_Group" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model43" IDProperty="PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="AccountNumber" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Account_AcctNumberType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IsIndividualYN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AccountBalance" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IsValid" Type="String"></ext:ModelField>
                                              <ext:ModelField Name="DocSpec_DocTypeIndic" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                                 <ext:Column ID="Column17" runat="server" DataIndex="DocSpec_DocTypeIndic" Text="Doc Type Indic" MinWidth="200"></ext:Column>
                                   <ext:Column ID="Column412" runat="server" DataIndex="AccountNumber" Text="Account Number" MinWidth="200"></ext:Column>
                                
                                <ext:Column ID="Column2" runat="server" DataIndex="IsIndividualYN" Text="Is Individual?" MinWidth="150"></ext:Column>
                                <ext:NumberColumn ID="Column3" runat="server" DataIndex="AccountBalance" Text="Account Balance" MinWidth="150" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                 <ext:Column ID="Column10" runat="server" DataIndex="IsValid" Text="Is Valid?" MinWidth="150"></ext:Column>
                                <ext:CommandColumn ID="cc_Reporting_Group" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="gc_Reporting_Group">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                  
                </Items>
            </ext:panel>
            <%--Reporting CRS702--%>
                <ext:panel runat="server" ID="fpReportingGroupCorrection"  BodyPadding="10" ButtonAlign="Center" Scrollable="Default" Title="Reporting Group" Layout="AnchorLayout" Border="true"  StyleSpec="border-color: #157fcc !important;margin-top:10px" Hidden="true" >
                <Items>
                     <ext:GridPanel ID="gp_Report_group_correction" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                      
                        <Store>
                            <ext:Store ID="store_Report_Group_Correction" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model5" IDProperty="PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="AccountNumber" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Account_AcctNumberType" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IsIndividualYN" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="AccountBalance" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IsValid" Type="String"></ext:ModelField>
                                              <ext:ModelField Name="DocSpec_DocTypeIndic" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn6" runat="server" Text="No"></ext:RowNumbererColumn>
                                 <ext:Column ID="Column18" runat="server" DataIndex="DocSpec_DocTypeIndic" Text="Doc Type Indic" MinWidth="200"></ext:Column>
                                   <ext:Column ID="Column19" runat="server" DataIndex="AccountNumber" Text="Account Number" MinWidth="200"></ext:Column>

                                <ext:Column ID="Column21" runat="server" DataIndex="IsIndividualYN" Text="Is Individual?" MinWidth="150"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn3" runat="server" DataIndex="AccountBalance" Text="Account Balance" MinWidth="150" Format="#,###.00" Align="Right"></ext:NumberColumn>
                                 <ext:Column ID="Column22" runat="server" DataIndex="IsValid" Text="Is Valid?" MinWidth="150"></ext:Column>
                                <ext:CommandColumn ID="cc_Reporting_Group_Correction" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                     
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="gc_Reporting_Group_Correction">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar6" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                  
                </Items>
            </ext:panel>
          <%-- <ext:Panel runat="server"  BodyPadding="10" ButtonAlign="Center"  >
               <Content>
                    <ext:FieldSet runat="server" ColumnWidth="0.01" ID="FieldSet1" Border="false" StyleSpec="margin-left:-10px">    
                           <Content>
                               <label>Action : </label>
                                <asp:DropDownList ID="cboactions" runat="server" AllowBlank="false" AnchorHorizontal="80%" FieldLabel="Action" Width="500"/>
                               </content>
                        </ext:FieldSet>
                  
               </Content>
           </ext:Panel>--%>
            
            
            <ext:ComboBox ID="cboaction" runat="server" FieldLabel="Action" ForceSelection="True" AllowBlank="false" AnchorHorizontal="80%" EnableKeyEvents="true" MinChars="1" ValueField="id" DisplayField="action" TypeAhead="false" StyleSpec="margin-top:10px">
                <Store>
                    <ext:Store ID="storeaction" runat="server">
                        <Model>
                            <ext:Model ID="ModelType" runat="server" IDProperty="id">
                                <Fields>
                                    <ext:ModelField Name="id" Type="Int"></ext:ModelField>
                                    <ext:ModelField Name="action" Type="String"></ext:ModelField>
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                 <Listeners>
                    <KeyUp Fn="onKeyUp"></KeyUp>
                </Listeners>
            </ext:ComboBox>
            <%--  <ext:checkbox ID="rdaction" runat="server" ColumnsWidths="100,100" FieldLabel="Action" DataIndex="Actions">
                            <Items>
                                <ext:Radio ID="rdYes" runat="server" BoxLabel="Edit" Checked="false"/>
                                <ext:Radio ID="rdNo" runat="server" BoxLabel="Insert New Data" Checked="false"/>
                            </Items>
                   <DirectEvents>
                                    <Change OnEvent="RadioAction_Change" />
                                </DirectEvents>
       </ext:checkbox>
               --%>
            
        </Items>
        
        <Buttons>
            <ext:Button runat="server" ID="btnSave" Text="Save" Icon="Disk">
                <DirectEvents>
                    <Click OnEvent="btnSave_Click">
                        <EventMask Msg="Processing..." MinDelay="500" ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
              <ext:Button ID="btn_Report_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="btn_Report_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
             <ext:Button ID="btn_Cancel_Back" runat="server" Icon="PageBack" Text="Cancel Editing & Back to View" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btn_Cancel_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
     <ext:FormPanel ID="Panelconfirmation" runat="server" ClientIDMode="Static" Title="Confirmation" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel" Tex="aa">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="BtnConfirmation_DirectClick"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>

    <%-- ================== REPORTING GROUP WINDOWS ========================== --%>
    <ext:Window ID="window_reporting_group"  runat="server" Modal="true" Maximized="true" Scrollable="Both" Maximizable="true" Hidden="true" Layout="AnchorLayout" AutoScroll="true"  ButtonAlign="Center" BodyPadding="10">
        <Items>
            <ext:InfoPanel ID="info_ValidationResultReportingGroup" ClientIDMode="Static" runat="server" UI="Warning" Layout="AnchorLayout" Title="Validation Result" AutoScroll="true" IconCls="#Error" MarginSpec="10 0" Border="True" Hidden="true" MaxHeight="150" Collapsible="true"></ext:InfoPanel>

            <%-- Reporting Group General Info --%>
            <ext:Panel runat="server" ID="pnl_ReportingGroupInfo"  Border="true" Layout="AnchorLayout" BodyPadding="15" AnchorHorizontal="100%" Title="Reporting Group">
                <Content>
                        <ext:TextField runat="server"  ID="txt_DocRefIDGroup" FieldLabel="Doc Ref ID" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="255" readonly="true"/>
                       <%-- <ext:TextField ID="txt_DocTypeIndicator" runat="server"  AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Doc Type Indicator" MaxLength="500" EnforceMaxLength="true">
                                <DirectEvents>
                                    <Change OnEvent="txtDocTypeIndGroup_Changed"></Change>
                                </DirectEvents>
                        </ext:TextField>--%>
                      <ext:TextField ID="txt_CorDocRefID" runat="server"  AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Correction Doc Ref ID" MaxLength="500" EnforceMaxLength="true" Hidden="true"/>
                      <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_DocTypeIndicator" ValueField="FK_CRS_DOC_TYPE_INDIC_CODE" DisplayField="DOC_TYPE_INDIC_NAME" StringField="FK_CRS_DOC_TYPE_INDIC_CODE, DOC_TYPE_INDIC_NAME" StringTable="vw_CRS_International_Doc_Type_Indic" Label="Doc Type Indic" AnchorHorizontal="100%" AllowBlank="false" OnOnValueChanged="cmb_DocTypeIndicator_OnValueChanged" IsHidden="false"/>
                    <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_DocTypeIndicator_Correction" ValueField="FK_CRS_DOC_TYPE_INDIC_CODE" DisplayField="DOC_TYPE_INDIC_NAME" StringField="FK_CRS_DOC_TYPE_INDIC_CODE, DOC_TYPE_INDIC_NAME" StringTable="Vw_CRS_International_ReportGroup_DocTypeIndic" Label="Doc Type Indic" AnchorHorizontal="100%" AllowBlank="false" OnOnValueChanged="cmb_DocTypeIndicator_OnValueChanged" IsHidden="true"/>
                        <ext:Checkbox runat="server"  ID="chk_IsIndividualGroup" FieldLabel="*Is Individual?" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="255" >
                            <DirectEvents>
                                <FocusLeave OnEvent="chk_IsIndividualGroup_CheckedChanged" ></FocusLeave>
                        
                            </DirectEvents>    
                        </ext:Checkbox>
                    <NDS:NDSDropDownField ID="cmb_ResCountryCode" runat="server"  ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Res Country Code" AnchorHorizontal="100%" AllowBlank="false"   />
                    
                    <%-- Account Information Group Info --%>
                    <ext:Panel runat="server" ID="pnl_accountinformation_Group" Border="true" Layout="AnchorLayout" title="Account Information" BodyPadding="15" MarginSpec="10 0 0 0">
                        <Content>
                                 <ext:DisplayField ID="txtwarningaccount" runat="server" AnchorHorizontal="80%" AllowBlank="false"  MaxLength="500" EnforceMaxLength="true" Hidden="true" />

                                <ext:TextField ID="txt_accountnumber_group" runat="server"  AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Account Number" MaxLength="200" EnforceMaxLength="true" >
                                      <DirectEvents>
                                        <FocusLeave  OnEvent="txt_accountnumber_group_Changed"></FocusLeave>
                                 <%--   <Change OnEvent="txt_accountnumber_group_Changed"></Change>--%>
                                </DirectEvents>
                                    </ext:TextField>
                                <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_AccountType_Group" ValueField="FK_CRS_ACCOUNT_NUMBER_TYPE_CODE" DisplayField="ACCOUNT_NUMBER_TYPE_NAME" StringField="FK_CRS_ACCOUNT_NUMBER_TYPE_CODE, ACCOUNT_NUMBER_TYPE_NAME" StringTable="vw_CRS_INTERNATIONAL_ACCOUNT_NUMBER_TYPE" Label="Account Number Type" AnchorHorizontal="100%" AllowBlank="true"  />
                                <ext:Checkbox runat="server"  ID="chk_undocument_Group" FieldLabel="Undocumented Account" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="255" />
                                <ext:Checkbox runat="server"  ID="chk_closedaccount_Group" FieldLabel="Closed Account" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="255" />
                                <ext:Checkbox runat="server"  ID="chk_dormantaccount_Group" FieldLabel="Dormant Account" AnchorHorizontal="100%"  EnforceMaxLength="True" maxlength="255" />
                                <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_AccountCurrency_Group" ValueField="FK_CURRENCY_CODE" DisplayField="CURRENCY_NAME" StringField="FK_CURRENCY_CODE, CURRENCY_NAME" StringTable="vw_CRS_INTERNATIONAL_CURRENCY" Label="Account Currency Code" AnchorHorizontal="100%" AllowBlank="false"  />
                                <ext:NumberField ID="txt_accountbalance_Group" runat="server"  AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Account Balance" MouseWheelEnabled="false" FormatText="#,##0.00" EnforceMaxLength="True"  />
                                <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_AccountHolderType" ValueField="FK_CRS_ACCOUNT_HOLDER_TYPE_CODE" DisplayField="ACCOUNT_HOLDER_TYPE_NAME" StringField="FK_CRS_ACCOUNT_HOLDER_TYPE_CODE, ACCOUNT_HOLDER_TYPE_NAME" StringTable="vw_CRS_INTERNATIONAL_HOLDER_TYPE" Label="Account Holder Type" AnchorHorizontal="100%" AllowBlank="false" IsHidden="true" OnOnValueChanged="cmb_AccountHolderType_OnValueChanged"/>
                            </Content>
                    </ext:Panel>

                     <ext:Panel runat="server" ID="pnl_Organization_Information_group" Border="true" Layout="AnchorLayout" title="Organization Information" BodyPadding="15" MarginSpec="10 0 0 0" Hidden="true">
                        <Content>
                            <ext:TextField ID="txt_org_Name" runat="server"  AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Name" MaxLength="50" EnforceMaxLength="true"/>
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_org_nametype" ValueField="FK_CRS_NAME_TYPE_CODE" DisplayField="NAME_TYPE_NAME" StringField="FK_CRS_NAME_TYPE_CODE, NAME_TYPE_NAME" StringTable="vw_CRS_INTERNATIONAL_NAME_TYPE" Label="Name Type" AnchorHorizontal="100%" AllowBlank="false" IsReadOnly="true" />
                                                    </Content>
                     </ext:Panel>

                    <%-- Individual Information Group Info --%>
                     <ext:Panel runat="server" ID="pnl_individualinformation_Group" Border="true" Layout="AnchorLayout" title="Individual Information" bodypadding="15" MarginSpec="10 0 0 0">
                        <Content>
                            <ext:TextField ID="txt_name_Group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Name" MaxLength="200" EnforceMaxLength="true" />
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_NameType_Group" ValueField="FK_CRS_NAME_TYPE_CODE" DisplayField="NAME_TYPE_NAME" StringField="FK_CRS_NAME_TYPE_CODE, NAME_TYPE_NAME" StringTable="vw_CRS_INTERNATIONAL_NAME_TYPE" Label="Name Type" AnchorHorizontal="100%" AllowBlank="false" IsReadOnly="true"/>
                            <ext:TextField ID="txt_precedingtitle_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Preceding Title" MaxLength="200" EnforceMaxLength="true" />
                            <ext:TextField ID="txt_title_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Title" MaxLength="200" EnforceMaxLength="true" />
                             <ext:TextField ID="txt_firstname_group" runat="server"  AnchorHorizontal="100%" AllowBlank="false" FieldLabel="First Name" MaxLength="200" EnforceMaxLength="true"  />
                           <%-- <ext:TextField ID="txt_firstname_type_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="First Name Type" MaxLength="200" EnforceMaxLength="true" />--%>
                            <ext:TextField ID="txt_middlename_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Middle Name" MaxLength="200" EnforceMaxLength="true" />
                            <%--<ext:TextField ID="txt_middlename_type_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Middle Name Type" MaxLength="200" EnforceMaxLength="true" />--%>
                            <ext:TextField ID="txt_lastname_group" runat="server"  AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Last Name" MaxLength="200" EnforceMaxLength="true" />
                           <%-- <ext:TextField ID="txt_lastname_type_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Last Name Type" MaxLength="200" EnforceMaxLength="true" />--%>
                            <ext:TextField ID="txt_nameprefix_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Name Prefix" MaxLength="200" EnforceMaxLength="true" />
                            <ext:TextField ID="txt_generationidentifier_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Generation Identifier" MaxLength="200" EnforceMaxLength="true" />
                            <ext:TextField ID="txt_namesufix_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Name Suffix" MaxLength="200" EnforceMaxLength="true" />
                            <ext:TextField ID="txt_generalsufix_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="General Suffix" MaxLength="200" EnforceMaxLength="true" />
                            <ext:DateField ID="datebirthdate_group" runat="server"   AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Birth Date"    Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_birthcity_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Birth City" MaxLength="200" EnforceMaxLength="true" />
                            <ext:TextField ID="txt_birthcitysubentity_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Birth City Sub Entity" MaxLength="200" EnforceMaxLength="true" />
                            <%--<ext:TextField ID="txt_birthcountrycode_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Birth Country Code" MaxLength="50" EnforceMaxLength="true" />--%>
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_birthcountrycode_group" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Birth Country Code" AnchorHorizontal="100%" AllowBlank="true"  />
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_nationality1_group" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Nationality 1" AnchorHorizontal="100%" AllowBlank="true"  />
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_nationality2_group" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Nationality 2" AnchorHorizontal="100%" AllowBlank="true"  />
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_nationality3_group" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Nationality 3" AnchorHorizontal="100%" AllowBlank="true"  />
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_nationality4_group" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Nationality 4" AnchorHorizontal="100%" AllowBlank="true"  />
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_nationality5_group" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Nationality 5" AnchorHorizontal="100%" AllowBlank="true"  />
                        </Content>
                     </ext:Panel>

                    <%-- TIN Information Group Info --%>
                     <ext:Panel runat="server" ID="pnl_tiniformation_Group" Border="true" Layout="AnchorLayout" title="TIN Information" bodypadding="15" MarginSpec="10 0 0 0">
                        <Content>
                            <ext:TextField ID="txt_tin_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="TIN" MaxLength="100" EnforceMaxLength="true" />
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_tinissuedby_group" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="TIN Issued By" AnchorHorizontal="100%" AllowBlank="true"  />
                           <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_org_tinissuedby_group" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="IN Issued By" AnchorHorizontal="100%" AllowBlank="true" IsHidden="true" />
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_org_tintype" ValueField="FK_CRS_IN_TYPE_CODE" DisplayField="IN_TYPE_NAME" StringField="FK_CRS_IN_TYPE_CODE, IN_TYPE_NAME" StringTable="vw_CRS_INTERNATIONAL_IN_TYPE" Label="IN Type" AnchorHorizontal="100%" AllowBlank="false"  IsHidden="true" IsReadOnly="true"/>
                            </Content>
                     </ext:Panel>

                    <%-- Address Information Group Info --%>
                    <ext:Panel runat="server" ID="pnl_addressinformation_Group" Border="true" Layout="AnchorLayout" title="Address Information" bodypadding="15" MarginSpec="10 0 0 0">
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_addresslegaltype_group" ValueField="FK_CRS_LEGAL_ADDRESS_TYPE_CODE" DisplayField="LEGAL_ADDRESS_TYPE_NAME" StringField="FK_CRS_LEGAL_ADDRESS_TYPE_CODE, LEGAL_ADDRESS_TYPE_NAME" StringTable="vw_CRS_INTERNATIONAL_LEGAL_ADDRESS" Label="Legal Address Type" AnchorHorizontal="100%" AllowBlank="true"  />
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_countrycode_Group" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Country Code" AnchorHorizontal="100%" AllowBlank="false"  />
                            <ext:TextArea ID="txt_addressfree_group" runat="server"  AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Address Free" MaxLength="4000" EnforceMaxLength="true" />
                            <ext:TextField ID="txt_street_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Street" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                    <Change OnEvent="txt_street_Group_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                            <ext:TextField ID="txt_buildingindentifier_Group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Building Identifier" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_buildingindentifier_Group_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                            <ext:TextField ID="txt_suiteidentifier_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Suite Identifier" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_suiteidentifier_group_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                            <ext:TextField ID="txt_flooridentifier_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Floor Identifier" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_flooridentifier_group_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                            <ext:TextField ID="txt_districtname_Group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="District Name" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_districtname_Group_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                            <ext:TextField ID="txt_pob_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="POB" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_pob_group_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                            <ext:TextField ID="txt_postcode_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Post Code" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_postcode_group_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                            <ext:TextField ID="txt_city_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="City" MaxLength="200" EnforceMaxLength="true" />
                            <ext:TextField ID="txt_countrysubentity_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Country Sub Entity" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_countrysubentity_group_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                        </Content>
                    </ext:Panel>

                    <%-- Payment Info --%>
                    <ext:Panel runat="server" ID="pnl_payment" Border="true" Layout="AnchorLayout" title="Payment" bodypadding="15" MarginSpec="10 0 0 0" >
                        <Items>
                            <ext:GridPanel ID="gp_payment" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_Payment_Add" Text="Add New Record" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_Payment_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storepayment" runat="server" IsPagingStore="true" PageSize="10" >
                                <Model>
                                    <ext:Model runat="server" ID="Model1" IDProperty="PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Payment_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Payment_PaymentAmnt_currCode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PaymentAmnt" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column4" runat="server" DataIndex="Payment_Type" Text="Payment Type" MinWidth="220"></ext:Column>
                                <ext:Column ID="Column5" runat="server" DataIndex="Payment_PaymentAmnt_currCode" Text="Payment Currency Code" MinWidth="220"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn1" runat="server" DataIndex="PaymentAmnt" Text="Payment Amount" MinWidth="255" Format="#,###.00" Align="left"></ext:NumberColumn>
                                <ext:CommandColumn ID="cc_payment" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="gc_payment_group">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                  
                        </Items>
                     </ext:Panel>

                      <%-- Payment Info Detail--%>
                    <ext:Panel runat="server" ID="Pnl_payment_Detail" Border="true" Layout="AnchorLayout" title="Payment" BodyPadding="15"  MarginSpec="10 0 0 0" Hidden="true">
                        <Items>
                            <ext:GridPanel ID="gp_payment_detail" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                      
                        <Store>
                            <ext:Store ID="store2" runat="server" IsPagingStore="true" PageSize="10" >
                                <Model>
                                    <ext:Model runat="server" ID="Model4" IDProperty="PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="Payment_Type" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Payment_PaymentAmnt_currCode" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="PaymentAmnt" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                   <ext:CommandColumn ID="cc_payment_detail" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                     
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="gc_payment_group">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:RowNumbererColumn ID="RowNumbererColumn5" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column15" runat="server" DataIndex="Payment_Type" Text="Payment Type" MinWidth="220"></ext:Column>
                                <ext:Column ID="Column16" runat="server" DataIndex="Payment_PaymentAmnt_currCode" Text="Payment Currency Codee" MinWidth="220"></ext:Column>
                                <ext:NumberColumn ID="NumberColumn2" runat="server" DataIndex="PaymentAmnt" Text="Payment Amount" MinWidth="255" Format="#,###.00" Align="left"></ext:NumberColumn>
                             
                                
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                  
                        </Items>
                     </ext:Panel>

                    <%-- Controlling Person Info Edit/Add --%>
                    <ext:Panel runat="server" ID="panelCP" Border="true" Layout="AnchorLayout" title="Controlling Person" BodyPadding="15"  MarginSpec="10 0 0 0" hidden="true">
                        <Items>
                            <ext:GridPanel ID="gp_cp" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="Button2" Text="Add New Record" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="btn_CP_Add_Click">
                                                <EventMask ShowMask="true" Msg="Processing..." MinDelay="50"></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storecp" runat="server" IsPagingStore="true" PageSize="10" >
                                <Model>
                                    <ext:Model runat="server" ID="Model2" IDProperty="PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="FirstName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="MiddleName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CtrlgPersonType" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column1" runat="server" DataIndex="FirstName" Text="First Name" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="MiddleName" Text="Middle Name" MinWidth="200"></ext:Column>
                                 <ext:Column ID="Column8" runat="server" DataIndex="LastName" Text="Last Name" MinWidth="200"></ext:Column>
                                 <ext:Column ID="Column9" runat="server" DataIndex="CtrlgPersonType" Text="Controlling Person Type" MinWidth="200"></ext:Column>
                                <ext:CommandColumn ID="cc_CP" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationDelete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="gc_cp_group">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                             
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                  
                        </Items>
                     </ext:Panel>

                     <%-- Controlling Person Info Detail--%>
                    <ext:Panel runat="server" ID="panelcpdetail" Border="true" Layout="AnchorLayout" title="Controlling Person" BodyPadding="15"  MarginSpec="10 0 0 0" hidden="true">
                        <Items>
                            <ext:GridPanel ID="gp_cp_Detail" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                       
                        <Store>
                            <ext:Store ID="store1" runat="server" IsPagingStore="true" PageSize="10" >
                                <Model>
                                    <ext:Model runat="server" ID="Model3" IDProperty="PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="FirstName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="MiddleName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="LastName" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CtrlgPersonType" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                   <ext:CommandColumn ID="cc_cp_detail" runat="server" Text="Action" MinWidth="220">
                                    <Commands>
                                      
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                       
                                    </Commands>

                                    <DirectEvents>
                                        <Command OnEvent="gc_cp_group">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                  
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:Column ID="Column11" runat="server" DataIndex="FirstName" Text="First Name" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="MiddleName" Text="Middle Name" MinWidth="200"></ext:Column>
                                 <ext:Column ID="Column13" runat="server" DataIndex="LastName" Text="Last Name" MinWidth="200"></ext:Column>
                                 <ext:Column ID="Column14" runat="server" DataIndex="CtrlgPersonType" Text="Controlling Person Type" MinWidth="200"></ext:Column>
                             
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                  
                        </Items>
                     </ext:Panel>

                   
                </Content>
            </ext:Panel>

          
        </Items>
        <Buttons>
            <ext:Button ID="btn_ReportingGroup_Save" runat="server" Icon="Disk" Text="Save Report Group">
                <DirectEvents>
                    <Click OnEvent="btn_ReportingGroup_Save_Click">
                        <EventMask ShowMask="true" Msg="Validating and Saving Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_ReportingGroup_Back" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_ReportingGroup_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..." MinDelay="50"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.95, height: size.height * 0.95});" />
            <Resize Handler="#{window_reporting_group}.center()" />
        </Listeners>
    </ext:Window>
    <%-- ================== END OF REPORTING GROUP WINDOWS ========================== --%>

       <%-- Pop Up Window Payment --%>
    <ext:Window ID="Window_Payment" runat="server" Modal="true" Hidden="true" Maximized="true" Maximizable="true" AutoScroll="true" ButtonAlign="Center" MinWidth="800" Height="500" Layout="FitLayout">
        <Items>
            <ext:FormPanel ID="pn_payment" runat="server" AnchorHorizontal="100%" BodyPadding="20" DefaultAlign="center" AutoScroll="true">
                <Content>
                    <NDS:NDSDropDownField ID="cmb_paymenttype" ValueField="FK_CRS_PAYMENT_TYPE_CODE" DisplayField="PAYMENT_TYPE_NAME" runat="server" StringField="FK_CRS_PAYMENT_TYPE_CODE, PAYMENT_TYPE_NAME" StringTable="vw_CRS_INTERNATIONAL_PAYMENT_TYPE" Label="Payment Type" AnchorHorizontal="100%" AllowBlank="false"/>
                    <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_paymentcurrencycode" ValueField="FK_CURRENCY_CODE" DisplayField="CURRENCY_NAME" StringField="FK_CURRENCY_CODE, CURRENCY_NAME" StringTable="vw_CRS_INTERNATIONAL_CURRENCY" Label="Payment Currency Code" AnchorHorizontal="100%" AllowBlank="false"  />
                    <ext:NumberField ID="txt_paymentamount" runat="server"  AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Payment Amount"  MouseWheelEnabled="false" FormatText="#,##0.00" EnforceMaxLength="True"  />
                </Content>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Payment_Save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_Payment_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Payment_Cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_Payment_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.7});" />

            <Resize Handler="#{Window_Payment}.center()" />
        </Listeners>
    </ext:Window>
    <%-- End of Pop Up Window Payment --%>

     <%-- ================== CONFIRMATION PANEL ========================== --%>
    <ext:FormPanel ID="pnl_Confirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="lbl_Confirmation" runat="server" Align="center" Cls="NawaLabel">
            </ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Confirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <%-- ================== END OF CONFIRMATION PANEL ========================== --%>
       <%-- Pop Up Window Controlling Person --%>
    <ext:Window ID="windowcp" runat="server" Modal="true" Maximized="true" Maximizable="true" Scrollable="Both" Hidden="true" Layout="AnchorLayout"  AutoScroll="true"  ButtonAlign="Center" BodyPadding="10">
        <Items>
            <%-- Controlling Person Info --%>
                    <ext:Panel runat="server" ID="pnl_controlling_person" Border="true"  Layout="AnchorLayout" title="Controlling Person" BodyPadding="15"    >
                        <Content>
                            <NDS:NDSDropDownField ID="cmb_restcountrycode_cp" runat="server"  ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Res Country Code" AnchorHorizontal="100%" AllowBlank="false" LabelWidth="100"/>
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_controllingpersontype_cp" ValueField="FK_CRS_CONTROLLING_PERSON_TYPE_CODE" DisplayField="CONTROLLING_PERSON_TYPE_NAME" StringField="FK_CRS_CONTROLLING_PERSON_TYPE_CODE, CONTROLLING_PERSON_TYPE_NAME" StringTable="vw_CRS_INTERNATIONAL_CP_TYPE" Label="Controlling Person Type" AnchorHorizontal="100%" AllowBlank="false"  />
                            <ext:TextField ID="txt_firstname_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="false" FieldLabel="First Name" MaxLength="200" EnforceMaxLength="true" />
                            <%--<ext:TextField ID="txt_firstnametype_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="First Name Type" MaxLength="200" EnforceMaxLength="true" />--%>
                            <ext:TextField ID="txt_middlename_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Middle Name" MaxLength="200" EnforceMaxLength="true" />
                          <%--  <ext:TextField ID="txt_middlenametype_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Middle Name Type" MaxLength="200" EnforceMaxLength="true" />--%>
                            <ext:TextField ID="txt_lastname_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Last Name" MaxLength="200" EnforceMaxLength="true" />
                           <%-- <ext:TextField ID="txt_lastnametype_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Last Name Type" MaxLength="200" EnforceMaxLength="true" />--%>
                            <ext:TextField ID="txt_nameprefix_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Name Prefix" MaxLength="200" EnforceMaxLength="true" />
                             <ext:TextField ID="txt_generationidentifier_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Generation Identifier" MaxLength="200" EnforceMaxLength="true" />
                            <ext:TextField ID="txt_namesufix_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Name Suffix" MaxLength="200" EnforceMaxLength="true" />
                            <ext:TextField ID="txt_generalsufix_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="General Suffix" MaxLength="200" EnforceMaxLength="true" />
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_nametype_cp" ValueField="FK_CRS_NAME_TYPE_CODE" DisplayField="NAME_TYPE_NAME" StringField="FK_CRS_NAME_TYPE_CODE, NAME_TYPE_NAME" StringTable="vw_CRS_INTERNATIONAL_NAME_TYPE" Label="Name Type" AnchorHorizontal="100%" AllowBlank="false" IsReadOnly="true"/>
                           <ext:TextField ID="txt_title_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Title" MaxLength="200" EnforceMaxLength="true" />

                            <ext:TextField ID="txt_precedingtitle_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Preceding Title" MaxLength="200" EnforceMaxLength="true" />
                                                       <ext:DateField ID="DateBirthDate_cp" runat="server"   AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Birth Date"    Format="dd-MMM-yyyy" />
                            <ext:TextField ID="txt_birthcity_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Birth City" MaxLength="200" EnforceMaxLength="true" />
                            <ext:TextField ID="txt_birthcitysubentity_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Birth City Sub Entity" MaxLength="200" EnforceMaxLength="true" />
                          <%--  <ext:TextField ID="txt_birthcountrycode_group" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Birth Country Code" MaxLength="50" EnforceMaxLength="true" />--%>
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_birthcountrycode_cp" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Birth Country Code" AnchorHorizontal="100%" AllowBlank="true"  />

                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_nationality_cp" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Nationality 1" AnchorHorizontal="100%" AllowBlank="true"  />
                        </Content>
                    </ext:Panel>

                    <%-- TIN CP(Controlling Person) Info --%>
                    <ext:Panel runat="server" ID="pnl_tininformation_cp" Border="true" Layout="AnchorLayout" title="TIN Information CP" BodyPadding="15"   StyleSpec="margin-top:30px" >
                        <Content>
                            <ext:TextField ID="txt_tin_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="TIN" MaxLength="100" EnforceMaxLength="true"/>
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_tinissuedby_cp" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="TIN Issued By" AnchorHorizontal="100%" AllowBlank="true" />
                        </Content>
                    </ext:Panel>

                    <%-- Address Information CP(Controlling Person) Info --%>
                    <ext:Panel runat="server" ID="pnl_addressinformation_cp" Border="true" Layout="AnchorLayout" title="Address Information CP" BodyPadding="15"  StyleSpec="margin-top:30px" >
                        <Content>
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_legaladdresstype_cp" ValueField="FK_CRS_LEGAL_ADDRESS_TYPE_CODE" DisplayField="LEGAL_ADDRESS_TYPE_NAME" StringField="FK_CRS_LEGAL_ADDRESS_TYPE_CODE, LEGAL_ADDRESS_TYPE_NAME" StringTable="vw_CRS_INTERNATIONAL_LEGAL_ADDRESS" Label="Legal Address Type" AnchorHorizontal="100%" AllowBlank="true"  />
                            <NDS:NDSDropDownField runat="server" LabelWidth="100" ID="cmb_countrycode_cp" ValueField="FK_COUNTRY_CODE" DisplayField="CONTRY_NAME" StringField="FK_COUNTRY_CODE, CONTRY_NAME" StringTable="vw_CRS_INTERNATIONAL_COUNTRY" Label="Country Code" AnchorHorizontal="100%" AllowBlank="false"  />
                            <ext:TextArea ID="txt_addressfree_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="false" FieldLabel="Address Free" MaxLength="4000" EnforceMaxLength="true" />
                            <ext:TextField ID="txt_street_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Street" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                    <Change OnEvent="txt_street_cp_Changed"></Change>
                                </DirectEvents>
                                </ext:TextField>
                            <ext:TextField ID="txt_buildingidentifier_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Building Identifier" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_buildingidentifier_cp_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                            <ext:TextField ID="txt_suiteidentifier_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Suite Identifier" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_suiteidentifier_cp_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                            <ext:TextField ID="txt_flooridentifier_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Floor Identifier" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_flooridentifier_cp_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                            <ext:TextField ID="txt_districtname_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="District Name" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_districtname_cp_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                            <ext:TextField ID="txt_pob_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="POB" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_pob_cp_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                            <ext:TextField ID="txt_postcode_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Post Code" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_postcode_cp_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                            <ext:TextField ID="txt_city_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="City" MaxLength="200" EnforceMaxLength="true" />
                            <ext:TextField ID="txt_countrysubentity_cp" runat="server"  AnchorHorizontal="100%" AllowBlank="true" FieldLabel="Country Sub Entity" MaxLength="200" EnforceMaxLength="true" >
                                <DirectEvents>
                                        <Change OnEvent="txt_countrysubentity_cp_Changed"></Change>
                                </DirectEvents>
                            </ext:TextField>
                        </Content>
                    </ext:Panel>

        </Items>
        <Buttons>
            <ext:Button ID="btnsavecp" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btn_cp_Save_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btncancelcp" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="btn_cp_Cancel_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.7, height: size.height * 0.7});" />

            <Resize Handler="#{Window_Payment}.center()" />
        </Listeners>
    </ext:Window>
</asp:Content>






