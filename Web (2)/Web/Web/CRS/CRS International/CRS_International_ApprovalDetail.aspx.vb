﻿Imports System.Data
Imports NawaDAL
Imports CRSBLL
Imports CRSDAL
Imports System.Data.SqlClient
Partial Class CRS_CRS_International_CRS_International_ApprovalDetail
    Inherits ParentPage
    Public objFormModuleApprovalDetail As NawaBLL.FormModuleApprovalDetail
    Dim drTemp As DataRow
    Dim strSQL As String

#Region "Session"
    Public Property IDUnik() As Long
        Get
            Return Session("CRS_International_ApprovalDetail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("CRS_International_ApprovalDetail.IDUnik") = value
        End Set
    End Property
    Public Property IsNewRecord() As Boolean
        Get
            Return Session("CRS_International_ApprovalDetail.IsNewRecord")
        End Get
        Set(ByVal value As Boolean)
            Session("CRS_International_ApprovalDetail.IsNewRecord") = value
        End Set
    End Property

    Public Property CRS_Report_Class() As CRSBLL.CRS_Report_BLL.CRS_Reporting_Class
        Get
            Return Session("CRS_International_ApprovalDetail.CRS_Report_Class")
        End Get
        Set(ByVal value As CRSBLL.CRS_Report_BLL.CRS_Reporting_Class)
            Session("CRS_International_ApprovalDetail.CRS_Report_Class") = value
        End Set
    End Property

    Public Property CRS_Report_Group_Class() As CRSBLL.CRS_Report_BLL.CRS_Reporting_Group_Class
        Get
            Return Session("CRS_International_ApprovalDetail.CRS_Report_Group_Class")
        End Get
        Set(ByVal value As CRSBLL.CRS_Report_BLL.CRS_Reporting_Group_Class)
            Session("CRS_International_ApprovalDetail.CRS_Report_Group_Class") = value
        End Set
    End Property
#End Region

#Region "Framework"
    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        If buttonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub

    Sub SetCommandColumnLocation()

        Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
        Dim buttonPosition As Integer = 1
        If Not objParamSettingbutton Is Nothing Then
            buttonPosition = objParamSettingbutton.SettingValue
        End If

        'Report Level
        ColumnActionLocation(gp_Report_Group, cc_Reporting_Group, buttonPosition)
        ColumnActionLocation(gp_payment, cc_payment, buttonPosition)
        ColumnActionLocation(gp_cp_Detail, cc_cp_detail, buttonPosition)
    End Sub

    Private Sub CRS_International_ApprovalDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If

                SetCommandColumnLocation()
                SetReportHeader(IDUnik)
                'ReLoad Object Report Class agar ID Reporting Group update
                CRS_Report_Class = CRS_Report_BLL.GetCRSINTReportClassByID(IDUnik)
                'Load Reporting Group By ID
                'Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Class.list_CRS_Reporting_Group)

                Dim query1 As String = "select PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID,AccountNumber,Account_AcctNumberType, " &
                " case when IsIndividualYN = 1 then 'Yes' when IsIndividualYN = 0 then 'No' end as IsIndividualYN,AccountBalance," &
                " case when IsValid = 1 then 'Yes' when IsValid = 0 then 'No' end as IsValid,DocSpec_DocTypeIndic " &
                " from CRS_INTERNATIONAL_REPORTING_GROUP where FK_CRS_INTERNATIONAL_REPORT_HEADER_ID =" & IDUnik.ToString
                'Dim dtReportingGroup As DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Class.list_CRS_Reporting_Group.OrderBy(Function(x) x.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID).ToList)
                Dim objtable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query1, Nothing)

                gp_Report_Group.GetStore().DataSource = objtable
                gp_Report_Group.GetStore().DataBind()

                Dim querychangeslog As String = "select * from CRS_INTERNATIONAL_CHANGES_LOG_APPROVAL where REPORT_ID = " & IDUnik.ToString
                'Dim dtReportingGroup As DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Class.list_CRS_Reporting_Group.OrderBy(Function(x) x.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID).ToList)
                Dim objtablechangeslog As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, querychangeslog, Nothing)

                gp_changeslog.GetStore.DataSource = objtablechangeslog
                gp_changeslog.GetStore.DataBind()

            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub CRS_International_ApprovalDetail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Approval
    End Sub

    Private Sub CRS_International_ApprovalDetail_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleApprovalDetail = New NawaBLL.FormModuleApprovalDetail()
    End Sub

    Protected Sub btn_Report_Back_Click()
        Try
            Dim strEncryptedModuleID = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            If InStr(ObjModule.UrlApproval, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "&ModuleID=" & strEncryptedModuleID)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & strEncryptedModuleID)
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Confirmation_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim strEncryptedModuleID = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "&ModuleID=" & strEncryptedModuleID)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & strEncryptedModuleID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub
#End Region

#Region "Reporting Header & FI"
    Private Sub SetReportHeader(id As Long)

        Dim objrefreporheader As New CRS_INTERNATIONAL_REPORT_HEADER
        Dim pk As Long = id
        Using objdb As New CRSDAL.CRSEntities
            objrefreporheader = objdb.CRS_INTERNATIONAL_REPORT_HEADER.Where(Function(x) x.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = id).FirstOrDefault
        End Using

        With objrefreporheader

            'Report Input
            txtID.Value = .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID
            If .MarkedAsDelete = True Then
                displayStatus.Value = "4 - Waiting Approval : Delete Data"
            Else
                displayStatus.Value = "4 - Waiting Approval : ADD/EDIT Data"
            End If
            txtWarning.Value = .Warning
            txtMessageRefID.Value = .MessageRefID
            If .MessageTypeIndic IsNot Nothing Then
                Dim objMessageTypeIndic = CRS_Report_BLL.GetMessageTypeIndic(.MessageTypeIndic)
                If Not objMessageTypeIndic Is Nothing Then
                    cmb_MessageTypeInd.SetTextWithTextValue(objMessageTypeIndic.FK_MESSAGE_TYPE_INDICATOR_CODE, objMessageTypeIndic.MESSAGE_TYPE_INDICATOR_NAME)
                End If
            End If
            If .CorrMessageRefID IsNot Nothing Then
                txtCorrectionMessageRefID.Hidden = False
                txtCorrectionMessageRefID.Value = .CorrMessageRefID
            Else
                txtCorrectionMessageRefID.Hidden = True
            End If
            txtReportingPeriod.Value = .ReportingPeriod
            txtTimeStamp.Value = .Timestamp
            If .LJK_Type IsNot Nothing Then
                Dim objLJKType = CRS_Report_BLL.GetLJKType(.LJK_Type)
                If Not objLJKType Is Nothing Then
                    cmb_LJK.SetTextWithTextValue(objLJKType.FK_CRS_LJK_TYPE_CODE, objLJKType.LJK_TYPE_NAME)
                End If
            End If
            If .ReceivingCountry IsNot Nothing Then
                Dim objRCountry = CRS_Report_BLL.GetCountry(.ReceivingCountry)
                If Not objRCountry Is Nothing Then
                    cmb_ReceivingCountry.SetTextWithTextValue(objRCountry.FK_COUNTRY_CODE, objRCountry.CONTRY_NAME)
                End If
            End If


            txtSendingCompany.Text = CRS_Global_BLL.getGlobalParameterValueByID(2)
            txtTransmittingCountry.Text = CRS_Global_BLL.getGlobalParameterValueByID(3)
            txtMessageType.Text = CRS_Global_BLL.getGlobalParameterValueByID(4)

            'Using objDB As New CRSDAL.CRSEntities
            '    Dim listcontact As List(Of CRS_INTERNATIONAL_SENDER_CONTACT) = objDB.CRS_INTERNATIONAL_SENDER_CONTACT.ToList
            '    Dim contact As String = ""
            '    Dim iterasi As Integer = 0
            '    For Each item In listcontact

            '        If iterasi > 0 Then
            '            contact = contact + "-" + item.LAST_NAME + "_" + item.CONTACT
            '        Else
            '            contact = item.LAST_NAME + "_" + item.CONTACT
            '        End If
            '        iterasi = iterasi + 1

            '    Next
            '    txtContact.Text = contact
            'End Using
            txtContact.Text = .Contact
            'Reporting FI
            Dim objrefreportfi As New CRS_INTERNATIONAL_REPORTING_FI
            Dim fk As String = .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID
            Using objdb As New CRSDAL.CRSEntities
                objrefreportfi = objdb.CRS_INTERNATIONAL_REPORTING_FI.Where(Function(x) x.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = fk).FirstOrDefault
            End Using
            If objrefreportfi IsNot Nothing Then
                With objrefreportfi
                    If .DocSpec_DocTypeIndic IsNot Nothing Then
                        Dim objDocType = CRS_Report_BLL.GetDocTypeIndic(.DocSpec_DocTypeIndic)
                        If Not objDocType Is Nothing Then
                            cmb_DocTypeIndic.SetTextWithTextValue(objDocType.FK_CRS_DOC_TYPE_INDIC_CODE, objDocType.DOC_TYPE_INDIC_NAME)
                        End If
                    End If

                    txtDocRefID.Value = .DocSpec_DocRefID
                    If .DocSpec_CorrDocRefID IsNot Nothing Then
                        txtCorDocRefID.Hidden = False
                        txtCorDocRefID.Value = .DocSpec_CorrDocRefID
                    Else
                        txtCorDocRefID.Hidden = True
                    End If

                End With
            End If


            Dim objrefreportreffi As New CRS_INTERNATIONAL_REF_REPORTINGFI
            Dim fk2 As String = .LJK_Type
            Using objdb As New CRSDAL.CRSEntities
                objrefreportreffi = objdb.CRS_INTERNATIONAL_REF_REPORTINGFI.Where(Function(x) x.LJK_Type = fk2).FirstOrDefault
            End Using
            If objrefreportreffi IsNot Nothing Then
                With objrefreportreffi
                    'Set Default Value of RentityID and RentityBranch

                    txtResCountryCode.Text = .ResCountryCode
                    txtIN.Text = .Identification_Number
                    txtIN_IssueBy.Text = .IN_IssueBy
                    txtIN_INType.Text = .IN_INType
                    txtName.Text = .Name
                    txtName_Type.Text = .Name_Type
                    txtAddress_LegalAddressType.Text = .Address_LegalAddressType
                    txtAddress_CountryCode.Text = .Address_CountryCode
                    txtAddress_AddressFree.Text = .Address_AddressFree
                    txtAddress_AddressFix_Street.Text = .Address_AddressFix_Street
                    txtAddress_AddressFix_BuildingIdentifier.Text = .Address_AddressFix_BuildingIdentifier
                    txtAddress_AddressFix_SuiteIdentifier.Text = .Address_AddressFix_SuiteIdentifier
                    txtAddress_AddressFix_FloorIdentifier.Text = .Address_AddressFix_FloorIdentifier
                    txtAddress_AddressFix_DistrictName.Text = .Address_AddressFix_DistrictName
                    txtAddress_AddressFix_POB.Text = .Address_AddressFix_POB
                    txtAddress_AddressFix_PostCode.Text = .Address_AddressFix_PostCode
                    txtAddress_AddressFix_City.Text = .Address_AddressFix_City
                    txtAddress_AddressFix_CountrySubentity.Text = .Address_AddressFix_CountrySubentity
                End With
            End If


        End With

        cmb_LJK.StringFieldStyle = "background-color:#FFE4C4"
        cmb_ReceivingCountry.StringFieldStyle = "background-color:#FFE4C4"
        cmb_DocTypeIndic.StringFieldStyle = "background-color:#FFE4C4"
        cmb_MessageTypeInd.StringFieldStyle = "background-color:#FFE4C4"

    End Sub

    Protected Sub btn_Report_Accept_Click()
        Try
            'Change status
            With CRS_Report_Class.obj_CRS_Reporting_Header
                ' delete old data and changeslog
                Dim users As String = ""
                Dim lastupdatebys As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select lastupdateby from Crs_international_Report_header where pk_Crs_international_Report_header_id = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID.ToString, Nothing)
                Dim createdbys As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select createdby from Crs_international_Report_header where pk_Crs_international_Report_header_id = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID.ToString, Nothing)
                If lastupdatebys IsNot Nothing Or Not String.IsNullOrEmpty(lastupdatebys) Then
                    users = lastupdatebys
                Else
                    users = createdbys
                End If
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_CHANGES_LOG_APPROVAL WHERE REPORT_ID = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID.ToString, Nothing)
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_REPORT_HEADER_OLD_" & users.ToString & " WHERE PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, Nothing)
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_REPORTING_FI_OLD_" & users.ToString & " WHERE FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, Nothing)
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_REPORTING_GROUP_OLD_" & users.ToString & " WHERE FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID.ToString, Nothing)
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_Old_" & users.ToString & " WHERE FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID.ToString, Nothing)
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_RG_AR_PAYMENT_Old_" & users.ToString & " WHERE FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID.ToString, Nothing)
                If Not IsNothing(.MarkedAsDelete) AndAlso .MarkedAsDelete = True Then   'Delete
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    CRS_Report_BLL.DeleteReportByReportID(CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, CRS_Report_Class.obj_CRS_Reporting_Header.LastUpdateBy, strUserID)
                Else    'Add/Edit
                    .Status = 1     'Waiting For Generate
                    CRS_Report_BLL.SaveReportEdit(ObjModule, CRS_Report_Class)
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_Report_XML_SavedState WHERE FK_Report_ID = " & CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, Nothing)
                End If
            End With

            'Recalculate List of Generated XML
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_UpdategoAML_Generate_XML", Nothing)

            'Show Confirmation Panel
            lbl_Confirmation.Text = "Request Approved"
            fpMain.Hidden = True
            pnl_Confirmation.Hidden = False


        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Report_Reject_Click()
        Try

            'Change status
            With CRS_Report_Class.obj_CRS_Reporting_Header
                .Status = 5
                ' Jika data delete balikin ke nilai awal dan ubah isvalidnya jadi 1
                ' delete dari old dan changeslog
                Dim users As String = ""
                Dim lastupdatebys As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select lastupdateby from Crs_international_Report_header where pk_Crs_international_Report_header_id = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID.ToString, Nothing)
                Dim createdbys As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select createdby from Crs_international_Report_header where pk_Crs_international_Report_header_id = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID.ToString, Nothing)
                If lastupdatebys IsNot Nothing Or Not String.IsNullOrEmpty(lastupdatebys) Then
                    users = lastupdatebys
                Else
                    users = createdbys
                End If
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_CHANGES_LOG_APPROVAL WHERE REPORT_ID = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID.ToString, Nothing)
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_REPORT_HEADER_OLD_" & users.ToString & " WHERE PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, Nothing)
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_REPORTING_FI_OLD_" & users.ToString & " WHERE FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, Nothing)
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_REPORTING_GROUP_OLD_" & users.ToString & " WHERE FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID.ToString, Nothing)
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_Old_" & users.ToString & " WHERE FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID.ToString, Nothing)
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " DELETE FROM CRS_INTERNATIONAL_RG_AR_PAYMENT_Old_" & users.ToString & " WHERE FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID.ToString, Nothing)


                If .MarkedAsDelete IsNot Nothing AndAlso .MarkedAsDelete = True Then      'Set MarkAsDelete jadi False
                    .MarkedAsDelete = False
                    'Load Reporting Group By ID
                    'Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Class.list_CRS_Reporting_Group)
                    ''Save Report
                    'CRS_Report_BLL.SaveReportEdit(ObjModule, CRS_Report_Class)
                    CRSBLL.CRS_Report_BLL.RestoreStateByReportID(CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID)
                    Dim pkID As Integer = CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID
                    CRS_Report_Class = New CRS_Report_BLL.CRS_Reporting_Class
                    CRS_Report_Class = CRS_Report_BLL.GetCRSINTReportClassByID(pkID)
                    CRS_Report_Class.obj_CRS_Reporting_Header.isValid = 1
                    CRS_Report_BLL.SaveReportEdit(ObjModule, CRS_Report_Class)

                Else
                    'Load Reporting Group By ID
                    Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Class.list_CRS_Reporting_Group)
                    'Restore State (Fitur On/Off di Global Parameter ID 3002)
                    ' kasih kondisi apakah datanya itu koreksi atau initial
                    If CRS_Report_Class.obj_CRS_Reporting_Header.MessageTypeIndic = "CRS702" Then
                        ' jika koreksi kita cek apakah datanya ada di state
                        Dim apakahadaddatacorrectioneditatauadd As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT count(*) FROM CRS_INTERNATIONAL_Report_XML_SavedState WHERE FK_Report_ID = " & CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, Nothing)
                        ' jika ada data di state berarti edit biasa data koreksi
                        If apakahadaddatacorrectioneditatauadd > 0 Then
                            ' kita restore data awalnya
                            CRSBLL.CRS_Report_BLL.RestoreStateByReportID(CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID)
                            Dim pkID As Integer = CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID
                            CRS_Report_Class = New CRS_Report_BLL.CRS_Reporting_Class
                            CRS_Report_Class = CRS_Report_BLL.GetCRSINTReportClassByID(pkID)
                            CRS_Report_Class.obj_CRS_Reporting_Header.Status = 5
                            CRS_Report_Class.obj_CRS_Reporting_Header.isValid = 0
                            CRS_Report_BLL.SaveReportEdit(ObjModule, CRS_Report_Class)
                        Else
                            ' ini jika dari data insert new data di edit hanya edit biasa aja status udah diubah ke 5
                            CRS_Report_BLL.SaveReportEdit(ObjModule, CRS_Report_Class)
                        End If
                    Else
                        'jika ini yang di reject data intial
                        CRSBLL.CRS_Report_BLL.RestoreStateByReportID(CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID)
                        Dim pkID As Integer = CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID
                        CRS_Report_Class = New CRS_Report_BLL.CRS_Reporting_Class
                        CRS_Report_Class = CRS_Report_BLL.GetCRSINTReportClassByID(pkID)
                        CRS_Report_Class.obj_CRS_Reporting_Header.Status = 5
                        CRS_Report_Class.obj_CRS_Reporting_Header.isValid = 0
                        CRS_Report_BLL.SaveReportEdit(ObjModule, CRS_Report_Class)
                    End If
                End If

            End With
            Dim ApaAdaDataState As Integer = 0
            ApaAdaDataState = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select count(*) from CRS_INTERNATIONAL_Report_XML_SavedState WHERE FK_Report_ID = " & CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, Nothing)
            If ApaAdaDataState > 0 Then

            Else

            End If


            'Recalculate List of Generated XML
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_UpdategoAML_Generate_XML", Nothing)

            'Show Confirmation Panel
            lbl_Confirmation.Text = "Request Rejected / Need Correction"
            fpMain.Hidden = True
            pnl_Confirmation.Hidden = False

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


#End Region

#Region "Reporting Group & CP"
    Protected Sub BindReportingGroup()
        Try
            If CRS_Report_Class.list_CRS_Reporting_Group IsNot Nothing Then
                Dim query1 As String = "select PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID,AccountNumber,Account_AcctNumberType, " &
                " case when IsIndividualYN = 1 then 'Yes' when IsIndividualYN = 0 then 'No' end as IsIndividualYN,AccountBalance," &
                " case when IsValid = 1 then 'Yes' when IsValid = 0 then 'No' end as IsValid,DocSpec_DocTypeIndic " &
                " from CRS_INTERNATIONAL_REPORTING_GROUP where FK_CRS_INTERNATIONAL_REPORT_HEADER_ID =" & IDUnik.ToString
                'Dim dtReportingGroup As DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Class.list_CRS_Reporting_Group.OrderBy(Function(x) x.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID).ToList)
                Dim dtReportingGroup As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query1, Nothing)

                'Get Module ID by Module Name
                Dim intModuleID As Integer = 50053
                Dim objModuleCek As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("CRS_INTERNATIONAL_REPORTING_GROUP")
                If objModuleCek IsNot Nothing Then
                    intModuleID = objModuleCek.PK_Module_ID
                End If

                'Get status from count of Validation Report
                strSQL = "SELECT KeyFieldValue FROM CRS_International_ValidationReport"
                strSQL += " WHERE ModuleID = " & intModuleID     '13243 adalah module ID untuk CRS_INTERNATIONAL_REPORTING_GROUP
                strSQL += " And RecordID = " & IDUnik

                Dim dtCRS_International_ValidationReport As DataTable = CRS_Global_BLL.getDataTableByQuery(strSQL)

                'For Each row As DataRow In dtReportingGroup.Rows
                '    If dtCRS_International_ValidationReport IsNot Nothing Then

                '        If row("IsIndividualYN") = "1" Then
                '            row("IsIndividualYN") = "Yes"
                '        Else
                '            row("IsIndividualYN") = "No"
                '        End If
                '    End If
                'Next

                'Bind to gridpanel
                gp_Report_Group.GetStore.DataSource = dtReportingGroup
                gp_Report_Group.GetStore.DataBind()

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btn_ReportingGroup_Back_Click(sender As Object, e As DirectEventArgs)
        Try
            CRS_Report_Group_Class.list_Reporting_RGARPayment = Nothing
            window_reporting_group.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub gc_Reporting_Group(sender As Object, e As DirectEventArgs)
        Try

            Dim ID As String = e.ExtraParams(0).Value
            Dim strCommandName As String = e.ExtraParams(1).Value
            If strCommandName = "Detail" Then
                'SetReadOnlyWindowReportingGroup(True)
                'ValidateReport()
                LoadReportingGroup("Detail", ID)
                Bind_Reporting_Payment()
                Bind_Reporting_cp()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadReportingGroup(strCommandName As String, strID As String)
        Try
            Me.IsNewRecord = False


            'Change window_s title
            window_reporting_group.Title = "Reporting Group - " & strCommandName

            'Load Reporting Group By ID
            CRS_Report_Group_Class = CRS_Report_BLL.GetReportingGroupClassByID(strID)

            'Load Reporting Group Header
            If CRS_Report_Group_Class IsNot Nothing Then
                With CRS_Report_Group_Class.obj_Reporting_Group
                    'General Information
                    If Not IsNothing(.DocSpec_DocTypeIndic) Then
                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_International_Doc_Type_Indic", "FK_CRS_DOC_TYPE_INDIC_CODE", .DocSpec_DocTypeIndic)
                        If drTemp IsNot Nothing Then
                            cmb_DocTypeIndicator.SetTextWithTextValue(drTemp("FK_CRS_DOC_TYPE_INDIC_CODE"), drTemp("DOC_TYPE_INDIC_NAME"))
                        End If
                    End If
                    If .DocSpec_CorrDocRefID IsNot Nothing Then
                        txt_CorDocRefID.Hidden = False
                        txt_CorDocRefID.Value = .DocSpec_CorrDocRefID
                    Else
                        txt_CorDocRefID.Hidden = True
                    End If
                    txt_DocRefIDGroup.Value = .DocSpec_DocRefID
                    chk_IsIndividulGroup.Checked = .IsIndividualYN
                    If .IsIndividualYN = True Then
                        If Not IsNothing(.INDV_ResCountryCode) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_ResCountryCode)
                            If drTemp IsNot Nothing Then
                                cmb_ResCountryCode.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                    Else
                        If Not IsNothing(.ORG_ResCountryCode) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .ORG_ResCountryCode)
                            If drTemp IsNot Nothing Then
                                cmb_ResCountryCode.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                    End If


                    'Account Infromation Group
                    txt_accountnumber_group.Value = .AccountNumber
                    If Not IsNothing(.Account_AcctNumberType) Then
                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_ACCOUNT_NUMBER_TYPE", "FK_CRS_ACCOUNT_NUMBER_TYPE_CODE", .Account_AcctNumberType)
                        If drTemp IsNot Nothing Then
                            cmb_AccountType_Group.SetTextWithTextValue(drTemp("FK_CRS_ACCOUNT_NUMBER_TYPE_CODE"), drTemp("ACCOUNT_NUMBER_TYPE_NAME"))
                        End If
                    End If
                    chk_undocument_Group.Checked = .Account_UndocumentedAccount
                    chk_closedaccount_Group.Checked = .Account_ClosedAccount
                    chk_dormantaccount_Group.Checked = .Account_DormantAccount
                    If Not IsNothing(.AccountBalance_CurrencyCode) Then
                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_CURRENCY", "FK_CURRENCY_CODE", .AccountBalance_CurrencyCode)
                        If drTemp IsNot Nothing Then
                            cmb_AccountCurrency_Group.SetTextWithTextValue(drTemp("FK_CURRENCY_CODE"), drTemp("CURRENCY_NAME"))
                        End If
                    End If

                    txt_accountbalance_Group.Value = .AccountBalance

                    'Individual Information Group
                    If .IsIndividualYN = True Then
                        cmb_AccountHolderType.IsHidden = True
                        pnlOIGP.Hidden = True
                        pnl_individualinformation_Group.Hidden = False
                        txt_name_Group.Value = .INDV_Name
                        If Not IsNothing(.INDV_Name_Type) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_NAME_TYPE", "FK_CRS_NAME_TYPE_CODE", .INDV_Name_Type)
                            If drTemp IsNot Nothing Then
                                cmb_NameType_Group.SetTextWithTextValue(drTemp("FK_CRS_NAME_TYPE_CODE"), drTemp("NAME_TYPE_NAME"))
                            End If
                        End If
                        txt_precedingtitle_group.Value = .INDV_Name_PrecedingTitle
                        txt_title_group.Value = .INDV_Name_Title
                        txt_firstname_group.Value = .INDV_Name_FirstName
                        'txt_firstname_type_group.Value = .INDV_Name_FirstName_Type
                        txt_middlename_group.Value = .INDV_Name_MiddleName
                        'txt_middlename_type_group.Value = .INDV_Name_MiddleName_Type
                        txt_lastname_group.Value = .INDV_Name_LastName
                        'txt_lastname_type_group.Value = .INDV_Name_LastName_Type
                        txt_nameprefix_group.Value = .INDV_Name_NamePrefix
                        txt_generationidentifier_group.Value = .INDV_Name_GenerationIdentifier
                        txt_namesufix_group.Value = .INDV_Name_Suffix
                        txt_generalsufix_group.Value = .INDV_Name_GeneralSuffix
                        datebirthdate_group.Value = .INDV_BirthInfo_BirthDate
                        txt_birthcity_group.Value = .INDV_BirthInfo_City
                        txt_birthcitysubentity_group.Value = .INDV_BirthInfo_CitySubEntity
                        If Not IsNothing(.INDV_BirthInfo_CountryInfo_CountryCode) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_BirthInfo_CountryInfo_CountryCode)
                            If drTemp IsNot Nothing Then
                                cmb_birthcountrycode_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Nationality1) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality1)
                            If drTemp IsNot Nothing Then
                                cmb_nationality1_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Nationality2) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality2)
                            If drTemp IsNot Nothing Then
                                cmb_nationality2_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Nationality3) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality3)
                            If drTemp IsNot Nothing Then
                                cmb_nationality3_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Nationality4) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality4)
                            If drTemp IsNot Nothing Then
                                cmb_nationality4_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                        If Not IsNothing(.INDV_Nationality5) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality5)
                            If drTemp IsNot Nothing Then
                                cmb_nationality5_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                    Else
                        cmb_AccountHolderType.IsHidden = False
                        pnlOIGP.Hidden = False
                        pnl_individualinformation_Group.Hidden = True
                        If Not IsNothing(.ORG_AcctHolderType) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_HOLDER_TYPE", "FK_CRS_ACCOUNT_HOLDER_TYPE_CODE", .ORG_AcctHolderType)
                            If drTemp IsNot Nothing Then
                                cmb_AccountHolderType.SetTextWithTextValue(drTemp("FK_CRS_ACCOUNT_HOLDER_TYPE_CODE"), drTemp("ACCOUNT_HOLDER_TYPE_NAME"))
                            End If
                        End If

                        txt_org_Name.Value = .ORG_Name
                        If Not IsNothing(.ORG_Name_NameType) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_NAME_TYPE", "FK_CRS_NAME_TYPE_CODE", .ORG_Name_NameType)
                            If drTemp IsNot Nothing Then
                                cmb_org_nametype.SetTextWithTextValue(drTemp("FK_CRS_NAME_TYPE_CODE"), drTemp("NAME_TYPE_NAME"))
                            End If
                        End If
                        txt_precedingtitle_group.Value = Nothing
                        txt_title_group.Value = Nothing
                        txt_firstname_group.Value = Nothing
                        'txt_firstname_type_group.Value = Nothing
                        txt_middlename_group.Value = Nothing
                        'txt_middlename_type_group.Value = Nothing
                        txt_lastname_group.Value = Nothing
                        'txt_lastname_type_group.Value = Nothing
                        txt_nameprefix_group.Value = Nothing
                        txt_generationidentifier_group.Value = Nothing
                        txt_namesufix_group.Value = Nothing
                        txt_generalsufix_group.Value = Nothing
                        datebirthdate_group.Value = Nothing
                        txt_birthcity_group.Value = Nothing
                        txt_birthcitysubentity_group.Value = Nothing
                        cmb_birthcountrycode_group.SetTextValue("")

                        cmb_nationality1_group.SetTextValue("")
                        cmb_nationality2_group.SetTextValue("")
                        cmb_nationality3_group.SetTextValue("")
                        cmb_nationality4_group.SetTextValue("")
                        cmb_nationality5_group.SetTextValue("")

                    End If

                    'TIN Information Group
                    If .IsIndividualYN = True Then
                        cmb_org_tinissuedby_group.IsHidden = True
                        cmb_tinissuedby_group.IsHidden = False
                        cmb_org_tintype.IsHidden = True
                        txt_tin_group.Value = .INDV_TIN
                        txt_tin_group.FieldLabel = "TIN"
                        If Not IsNothing(.INDV_TIN_ISSUEDBY) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_TIN_ISSUEDBY)
                            If drTemp IsNot Nothing Then
                                cmb_tinissuedby_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If
                    Else
                        cmb_org_tintype.IsHidden = False
                        cmb_org_tinissuedby_group.IsHidden = False
                        cmb_tinissuedby_group.IsHidden = True
                        'TIN Information Group
                        txt_tin_group.Value = .ORG_IN
                        txt_tin_group.FieldLabel = "IN"
                        If Not IsNothing(.ORG_IN_INType) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_IN_TYPE", "FK_CRS_IN_TYPE_CODE", .ORG_IN_INType)
                            If drTemp IsNot Nothing Then
                                cmb_org_tintype.SetTextWithTextValue(drTemp("FK_CRS_IN_TYPE_CODE"), drTemp("IN_TYPE_NAME"))
                            End If
                        End If
                        If Not IsNothing(.ORG_IN_IssuedBy) Then
                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .ORG_IN_IssuedBy)
                            If drTemp IsNot Nothing Then
                                cmb_org_tinissuedby_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                            End If
                        End If

                    End If


                    'Address Information Group
                    If Not IsNothing(.Address_LegalAddressType) Then
                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_LEGAL_ADDRESS", "FK_CRS_LEGAL_ADDRESS_TYPE_CODE", .Address_LegalAddressType)
                        If drTemp IsNot Nothing Then
                            cmb_addresslegaltype_group.SetTextWithTextValue(drTemp("FK_CRS_LEGAL_ADDRESS_TYPE_CODE"), drTemp("LEGAL_ADDRESS_TYPE_NAME"))
                        End If
                    End If
                    If Not IsNothing(.Address_CountryCode) Then
                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .Address_CountryCode)
                        If drTemp IsNot Nothing Then
                            cmb_countrycode_Group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                        End If
                    End If
                    txt_addressfree_group.Value = .Address_AddressFree
                    txt_street_group.Value = .Address_AddressFix_Street
                    Dim adaisistreetfixnya As Integer = 0
                    If .Address_AddressFix_Street IsNot Nothing Then
                        adaisistreetfixnya = adaisistreetfixnya + 1
                    End If
                    If .Address_AddressFix_BuildingIdentifier IsNot Nothing Then
                        adaisistreetfixnya = adaisistreetfixnya + 1
                    End If
                    If .Address_AddressFix_SuiteIdentifier IsNot Nothing Then
                        adaisistreetfixnya = adaisistreetfixnya + 1
                    End If
                    If .Address_AddressFix_FloorIdentifier IsNot Nothing Then
                        adaisistreetfixnya = adaisistreetfixnya + 1
                    End If
                    If .Address_AddressFix_DistrictName IsNot Nothing Then
                        adaisistreetfixnya = adaisistreetfixnya + 1
                    End If
                    If .Address_AddressFix_POB IsNot Nothing Then
                        adaisistreetfixnya = adaisistreetfixnya + 1
                    End If
                    If .Address_AddressFix_PostCode IsNot Nothing Then
                        adaisistreetfixnya = adaisistreetfixnya + 1
                    End If
                    If .Address_AddressFix_CountrySubentity IsNot Nothing Then
                        adaisistreetfixnya = adaisistreetfixnya + 1
                    End If

                    If adaisistreetfixnya > 0 Then
                        txt_city_group.AllowBlank = False
                        txt_city_group.FieldStyle = "background-color:#FFE4C4"
                    Else
                        txt_city_group.AllowBlank = True
                        txt_city_group.FieldStyle = "background-color:#FFFFFF"
                    End If
                    txt_buildingindentifier_Group.Value = .Address_AddressFix_BuildingIdentifier
                    txt_suiteidentifier_group.Value = .Address_AddressFix_SuiteIdentifier
                    txt_flooridentifier_group.Value = .Address_AddressFix_FloorIdentifier
                    txt_districtname_Group.Value = .Address_AddressFix_DistrictName
                    txt_pob_group.Value = .Address_AddressFix_POB
                    txt_postcode_group.Value = .Address_AddressFix_PostCode
                    txt_city_group.Value = .Address_AddressFix_City
                    txt_countrysubentity_group.Value = .Address_AddressFix_CountrySubentity



                    If .ORG_AcctHolderType = "CRS101" Then
                        panelcpdetail.Hidden = False
                        windowcp.Visible = True
                    Else
                        panelcpdetail.Hidden = True
                        windowcp.Visible = False
                    End If




                End With



                'Load Validation Result
                Dim strValidationResult As String = CRS_Report_BLL.getValidationReportByReportGroupID(CRS_Report_Class.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, CRS_Report_Group_Class.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID)
                If Not String.IsNullOrWhiteSpace(strValidationResult) Then
                    NawaDevBLL.NawaFramework.extInfoPanelupdate(fpMain, strValidationResult, "info_ValidationResultReportingGroup")
                    info_ValidationResultReportingGroup.Hidden = False
                Else
                    info_ValidationResultReportingGroup.Hidden = True
                End If

                ' Tambahan 02/03/2022

                cmb_DocTypeIndicator.StringFieldStyle = "background-color:#FFE4C4"
                cmb_AccountType_Group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_AccountCurrency_Group.StringFieldStyle = "background-color:#FFE4C4"
                cmb_AccountHolderType.StringFieldStyle = "background-color:#FFE4C4"
                cmb_NameType_Group.StringFieldStyle = "background-color:#FFE4C4"
                cmb_birthcountrycode_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality1_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality2_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality3_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality4_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_nationality5_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_tinissuedby_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_addresslegaltype_group.StringFieldStyle = "background-color:#FFFFFF"
                cmb_countrycode_Group.StringFieldStyle = "background-color:#FFE4C4"
                cmb_controllingpersontype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nametype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nationality_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_tinissuedby_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_legaladdresstype_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_countrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_paymenttype.StringFieldStyle = "background-color:#FFE4C4"
                cmb_paymentcurrencycode.StringFieldStyle = "background-color:#FFE4C4"
                cmb_org_nametype.StringFieldStyle = "background-color:#FFE4C4"
                cmb_ResCountryCode.StringFieldStyle = "background-color:#FFE4C4"
                cmb_org_tintype.StringFieldStyle = "background-color:#FFFFFF"
                cmb_org_tinissuedby_group.StringFieldStyle = "background-color:#FFFFFF"

                'Show window_ Reporting Group
                window_reporting_group.Hidden = False
                window_reporting_group.Body.ScrollTo(Direction.Top, 0)

            Else
                Throw New ApplicationException("Reporting Group cannot be loaded")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


#End Region

#Region "Reporting Payment"

    Public Property IDPayment() As Long
        Get
            Return Session("CRS_International_ApprovalDetail.IDPayment")
        End Get
        Set(ByVal value As Long)
            Session("CRS_International_ApprovalDetail.IDPayment") = value
        End Set
    End Property

    Public Property obj_Reporting_Payment_Detail() As CRS_INTERNATIONAL_RG_AR_PAYMENT
        Get
            Return Session("CRS_International_ApprovalDetail.obj_Reporting_Payment_Detail")
        End Get
        Set(ByVal value As CRS_INTERNATIONAL_RG_AR_PAYMENT)
            Session("CRS_International_ApprovalDetail.obj_Reporting_Payment_Detail") = value
        End Set
    End Property

    Protected Sub gc_payment_group(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Detail" Then
                    obj_Reporting_Payment_Detail = CRS_Report_Group_Class.list_Reporting_RGARPayment.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID = strID)
                    Load_Window_Report_Payment(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_Payment_Cancel_Click()
        Try
            'Hide window pop up
            Window_Payment.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_Reporting_Payment()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Group_Class.list_Reporting_RGARPayment)


        gp_payment.GetStore().DataSource = objtable
        gp_payment.GetStore().DataBind()

    End Sub


    Protected Sub Clean_Window_Payment()
        'Clean fields
        txt_paymentamount.Value = Nothing
        cmb_paymentcurrencycode.SetTextValue("")
        cmb_paymenttype.SetTextValue("")

    End Sub

    Protected Sub Load_Window_Report_Payment(strAction As String)
        'Clean window pop up
        Clean_Window_Payment()

        If obj_Reporting_Payment_Detail IsNot Nothing Then
            'Populate fields
            With obj_Reporting_Payment_Detail
                txt_paymentamount.Value = .PaymentAmnt

                If .Payment_Type IsNot Nothing Then
                    Dim objPaymentType = CRS_Report_BLL.GetPaymentTypeByCode(.Payment_Type)
                    If Not objPaymentType Is Nothing Then
                        cmb_paymenttype.SetTextWithTextValue(objPaymentType.FK_CRS_PAYMENT_TYPE_CODE, objPaymentType.PAYMENT_TYPE_NAME)
                    End If
                End If


                If .Payment_PaymentAmnt_currCode IsNot Nothing Then
                    Dim objPaymentCurrency = CRS_Report_BLL.GetPaymentCurrencyByCode(.Payment_PaymentAmnt_currCode)
                    If Not objPaymentCurrency Is Nothing Then
                        cmb_paymentcurrencycode.SetTextWithTextValue(objPaymentCurrency.FK_CURRENCY_CODE, objPaymentCurrency.CURRENCY_NAME)
                    End If
                End If
            End With
        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            txt_paymentamount.ReadOnly = False
            cmb_paymenttype.IsReadOnly = False
            cmb_paymentcurrencycode.IsReadOnly = False
        Else
            txt_paymentamount.ReadOnly = True
            cmb_paymenttype.IsReadOnly = True
            cmb_paymentcurrencycode.IsReadOnly = True

        End If

        cmb_paymenttype.StringFieldStyle = "background-color:#FFE4C4"
        cmb_paymentcurrencycode.StringFieldStyle = "background-color:#FFE4C4"

        'Bind ATM
        IDPayment = obj_Reporting_Payment_Detail.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID

        'Show window pop up
        Window_Payment.Title = "Reporting Payment - " & strAction
        Window_Payment.Hidden = False
    End Sub
#End Region

#Region "Controlling Person"

    Public Property IDCP() As Long
        Get
            Return Session("CRS_International_Add.IDCP")
        End Get
        Set(ByVal value As Long)
            Session("CRS_International_Add.IDCP") = value
        End Set
    End Property

    Public Property obj_Reporting_cp_Edit() As CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON
        Get
            Return Session("CRS_International_Add.obj_Reporting_cp_Edit")
        End Get
        Set(ByVal value As CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON)
            Session("CRS_International_Add.obj_Reporting_cp_Edit") = value
        End Set
    End Property

    Protected Sub btn_cp_Add_Click()
        Try
            'Kosongkan object edit & windows pop up
            Clean_Window_cp()

            'Show window pop up
            windowcp.Title = "Reporting Controlling Person - Add"
            windowcp.Hidden = False

            'Set IDCP to 0
            IDCP = 0

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub gc_cp_group(sender As Object, e As DirectEventArgs)
        Try
            Dim strID As String = e.ExtraParams(0).Value
            Dim strAction As String = e.ExtraParams(1).Value

            If strID IsNot Nothing Then
                If strAction = "Delete" Then
                    Dim objToDelete = CRS_Report_Group_Class.list_Reporting_RGARPerson.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID = strID)
                    If objToDelete IsNot Nothing Then
                        CRS_Report_Group_Class.list_Reporting_RGARPerson.Remove(objToDelete)
                    End If
                    Bind_Reporting_cp()

                ElseIf strAction = "Edit" Or strAction = "Detail" Then
                    obj_Reporting_cp_Edit = CRS_Report_Group_Class.list_Reporting_RGARPerson.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID = strID)
                    Load_Window_Report_cp(strAction)
                End If
            Else
                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btn_cp_Cancel_Click()
        Try
            'Hide window pop up
            windowcp.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Bind_Reporting_cp()
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Group_Class.list_Reporting_RGARPerson)

        gp_cp_Detail.GetStore().DataSource = objtable
        gp_cp_Detail.GetStore().DataBind()

    End Sub


    Protected Sub Clean_Window_cp()
        'Controlling Person Information
        cmb_restcountrycode_cp.SetTextValue("")
        cmb_controllingpersontype_cp.SetTextValue("")
        txt_firstname_cp.Value = Nothing
        'txt_firstnametype_cp.Value = Nothing
        txt_middlename_cp.Value = Nothing
        'txt_middlenametype_cp.Value = Nothing
        txt_lastname_cp.Value = Nothing
        'txt_lastnametype_cp.Value = Nothing
        txt_nameprefix_cp.Value = Nothing
        txt_generationidentifier_cp.Value = Nothing
        txt_namesufix_cp.Value = Nothing
        txt_generalsufix_cp.Value = Nothing
        cmb_nametype_cp.SetTextValue("")
        txt_precedingtitle_cp.Value = Nothing
        txt_title_cp.Value = Nothing
        DateBirthDate_cp.Value = Nothing
        cmb_nationality_cp.SetTextValue("")


        'TIN Controlling Person Information
        txt_tin_cp.Value = Nothing
        cmb_tinissuedby_cp.SetTextValue("")

        'Address Controlling Person Information
        cmb_legaladdresstype_cp.SetTextValue("")
        cmb_countrycode_cp.SetTextValue("")
        txt_addressfree_cp.Value = Nothing
        txt_street_cp.Value = Nothing
        txt_buildingidentifier_cp.Value = Nothing
        txt_suiteidentifier_cp.Value = Nothing
        txt_flooridentifier_cp.Value = Nothing
        txt_districtname_cp.Value = Nothing
        txt_pob_cp.Value = Nothing
        txt_postcode_cp.Value = Nothing
        txt_city_cp.Value = Nothing
        txt_countrysubentity_cp.Value = Nothing
    End Sub


    Protected Sub Load_Window_Report_cp(strAction As String)
        'Clean window pop up
        Clean_Window_cp()

        If obj_Reporting_cp_Edit IsNot Nothing Then
            'Populate fields
            With obj_Reporting_cp_Edit
                If Not IsNothing(.ResCountryCode) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .ResCountryCode)
                    If drTemp IsNot Nothing Then
                        cmb_restcountrycode_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                    End If
                End If
                If Not IsNothing(.CtrlgPersonType) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_CP_TYPE", "FK_CRS_CONTROLLING_PERSON_TYPE_CODE", .CtrlgPersonType)
                    If drTemp IsNot Nothing Then
                        cmb_controllingpersontype_cp.SetTextWithTextValue(drTemp("FK_CRS_CONTROLLING_PERSON_TYPE_CODE"), drTemp("CONTROLLING_PERSON_TYPE_NAME"))
                    End If
                End If
                txt_firstname_cp.Value = .FirstName
                'txt_firstnametype_cp.Value = .FirstNameType
                txt_middlename_cp.Value = .MiddleName
                'txt_middlenametype_cp.Value = .MiddleNameType
                txt_lastname_cp.Value = .LastName
                'txt_lastnametype_cp.Value = .LastNameType
                txt_nameprefix_cp.Value = .Name_NamePrefix
                txt_generationidentifier_cp.Value = .Name_GenerationIdentifier
                txt_namesufix_cp.Value = .Name_Suffix
                txt_generalsufix_cp.Value = .Name_GeneralSuffix
                If Not IsNothing(.NameType) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_NAME_TYPE", "FK_CRS_NAME_TYPE_CODE", .NameType)
                    If drTemp IsNot Nothing Then
                        cmb_nametype_cp.SetTextWithTextValue(drTemp("FK_CRS_NAME_TYPE_CODE"), drTemp("NAME_TYPE_NAME"))
                    End If
                End If
                txt_precedingtitle_cp.Value = .Name_PrecedingTitle
                txt_title_cp.Value = .Name_Title
                DateBirthDate_cp.Value = .BirthDate
                If Not IsNothing(.Nationality) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .Nationality)
                    If drTemp IsNot Nothing Then
                        cmb_nationality_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                    End If
                End If


                'TIN Controlling Person Information
                txt_tin_cp.Value = .TIN
                If Not IsNothing(.IssuedBy) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .IssuedBy)
                    If drTemp IsNot Nothing Then
                        cmb_tinissuedby_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                    End If
                End If

                'Address Controlling Person Information
                If Not IsNothing(.legalAddressType) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_LEGAL_ADDRESS", "FK_CRS_LEGAL_ADDRESS_TYPE_CODE", .legalAddressType)
                    If drTemp IsNot Nothing Then
                        cmb_legaladdresstype_cp.SetTextWithTextValue(drTemp("FK_CRS_LEGAL_ADDRESS_TYPE_CODE"), drTemp("LEGAL_ADDRESS_TYPE_NAME"))
                    End If
                End If
                If Not IsNothing(.CountryCode) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .CountryCode)
                    If drTemp IsNot Nothing Then
                        cmb_countrycode_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                    End If
                End If
                txt_addressfree_cp.Value = .AddressFree
                txt_street_cp.Value = .AddressFix_Street
                Dim adaisistreetfixnya As Integer = 0
                If .AddressFix_Street IsNot Nothing Then
                    adaisistreetfixnya = adaisistreetfixnya + 1
                End If
                If .AddressFix_BuildingIdentifier IsNot Nothing Then
                    adaisistreetfixnya = adaisistreetfixnya + 1
                End If
                If .AddressFix_SuiteIdentifier IsNot Nothing Then
                    adaisistreetfixnya = adaisistreetfixnya + 1
                End If
                If .AddressFix_FloorIdentifier IsNot Nothing Then
                    adaisistreetfixnya = adaisistreetfixnya + 1
                End If
                If .AddressFix_DistrictName IsNot Nothing Then
                    adaisistreetfixnya = adaisistreetfixnya + 1
                End If
                If .AddressFix_POB IsNot Nothing Then
                    adaisistreetfixnya = adaisistreetfixnya + 1
                End If
                If .AddressFix_PostCode IsNot Nothing Then
                    adaisistreetfixnya = adaisistreetfixnya + 1
                End If
                If .AddressFix_CountrySubentity IsNot Nothing Then
                    adaisistreetfixnya = adaisistreetfixnya + 1
                End If

                If adaisistreetfixnya > 0 Then
                    txt_city_cp.AllowBlank = False
                    txt_city_cp.FieldStyle = "background-color:#FFE4C4"
                Else
                    txt_city_cp.AllowBlank = True
                    txt_city_cp.FieldStyle = "background-color:#FFFFFF"
                End If
                txt_buildingidentifier_cp.Value = .AddressFix_BuildingIdentifier
                txt_suiteidentifier_cp.Value = .AddressFix_SuiteIdentifier
                txt_flooridentifier_cp.Value = .AddressFix_FloorIdentifier
                txt_districtname_cp.Value = .AddressFix_DistrictName
                txt_pob_cp.Value = .AddressFix_POB
                txt_postcode_cp.Value = .AddressFix_PostCode
                txt_city_cp.Value = .AddressFix_City
                txt_countrysubentity_cp.Value = .AddressFix_CountrySubentity
                txt_birthcity_cp.Value = .BirthInfo_City
                txt_birthcitysubentity_cp.Value = .BirthInfo_SubEntity
                If Not IsNothing(.BirthInfo_FormerCountryName) Then
                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .BirthInfo_FormerCountryName)
                    If drTemp IsNot Nothing Then
                        cmb_birthcountrycode_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
                    End If
                End If
            End With

        End If

        'Set fields' ReadOnly
        If strAction = "Edit" Then
            SetReadOnlyWindowReportingCP(False)
        Else
            SetReadOnlyWindowReportingCP(True)
        End If

        'Bind ATM
        IDCP = obj_Reporting_cp_Edit.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID

        'Show window pop up
        windowcp.Title = "Reporting Controlling Person - " & strAction
        windowcp.Hidden = False
    End Sub

    Protected Sub SetReadOnlyWindowReportingCP(isReadOnly As Boolean)
        Try


            'Controlling Person Information
            cmb_restcountrycode_cp.IsReadOnly = isReadOnly
            cmb_controllingpersontype_cp.IsReadOnly = isReadOnly
            txt_firstname_cp.ReadOnly = isReadOnly
            'txt_firstnametype_cp.ReadOnly = isReadOnly
            txt_middlename_cp.ReadOnly = isReadOnly
            'txt_middlenametype_cp.ReadOnly = isReadOnly
            txt_lastname_cp.ReadOnly = isReadOnly
            'txt_lastnametype_cp.ReadOnly = isReadOnly
            txt_nameprefix_cp.ReadOnly = isReadOnly
            txt_generationidentifier_cp.ReadOnly = isReadOnly
            txt_namesufix_cp.ReadOnly = isReadOnly
            txt_generalsufix_cp.ReadOnly = isReadOnly
            cmb_nametype_cp.IsReadOnly = isReadOnly
            txt_precedingtitle_cp.ReadOnly = isReadOnly
            txt_title_cp.ReadOnly = isReadOnly
            DateBirthDate_cp.ReadOnly = isReadOnly
            txt_birthcity_group.ReadOnly = isReadOnly
            txt_birthcitysubentity_group.ReadOnly = isReadOnly
            cmb_birthcountrycode_cp.IsReadOnly = isReadOnly
            cmb_nationality_cp.IsReadOnly = isReadOnly

            'TIN Controlling Person Information
            txt_tin_cp.ReadOnly = isReadOnly
            cmb_tinissuedby_cp.IsReadOnly = isReadOnly

            'Address Controlling Person Information
            cmb_legaladdresstype_cp.IsReadOnly = isReadOnly
            cmb_countrycode_cp.IsReadOnly = isReadOnly
            txt_addressfree_cp.ReadOnly = isReadOnly
            txt_street_cp.ReadOnly = isReadOnly
            txt_buildingidentifier_cp.ReadOnly = isReadOnly
            txt_suiteidentifier_cp.ReadOnly = isReadOnly
            txt_flooridentifier_cp.ReadOnly = isReadOnly
            txt_districtname_cp.ReadOnly = isReadOnly
            txt_pob_cp.ReadOnly = isReadOnly
            txt_postcode_cp.ReadOnly = isReadOnly
            txt_city_cp.ReadOnly = isReadOnly
            txt_countrysubentity_cp.ReadOnly = isReadOnly

            If isReadOnly = False Then
                cmb_controllingpersontype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nametype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nationality_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_tinissuedby_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_legaladdresstype_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_countrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_restcountrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_birthcountrycode_cp.StringFieldStyle = "background-color:#FFFFFF"
            Else
                cmb_controllingpersontype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nametype_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_nationality_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_tinissuedby_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_legaladdresstype_cp.StringFieldStyle = "background-color:#FFFFFF"
                cmb_countrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_restcountrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
                cmb_birthcountrycode_cp.StringFieldStyle = "background-color:#FFFFFF"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class
