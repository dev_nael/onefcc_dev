﻿Imports System.IO
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Entity
Imports CRSBLL
Imports CRSDAL
Imports System.Reflection
Partial Class CRS_CRS_International_CRS_International_GenerateAdd
    Inherits ParentPage
    Dim drTemp As DataRow
    Dim strSQL As String
    Public objFormModuleAdd As NawaBLL.FormModuleAdd
    Public Structure Years
        Dim Id As Integer
        Dim year As String
    End Structure
    Public Structure Countrys
        Dim Id As Integer
        Dim formatdata As String
    End Structure

    Public Structure Formats
        Dim Id As Integer
        Dim format As String
    End Structure


    Public Property IDUnik() As Long
        Get
            Return Session("CRS_International_Detail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("CRS_International_Detail.IDUnik") = value
        End Set
    End Property

    Public Property CRS_Report_Class() As CRSBLL.CRS_Report_BLL.CRS_Reporting_Class
        Get
            Return Session("CRS_International_GenerateAdd.CRS_Report_Class")
        End Get
        Set(ByVal value As CRSBLL.CRS_Report_BLL.CRS_Reporting_Class)
            Session("CRS_International_GenerateAdd.CRS_Report_Class") = value
        End Set
    End Property

    Public Property CRS_GenerateValid() As List(Of CRS_International_Generate_Bulk_CountryValid)
        Get
            Return Session("CRS_International_GenerateAdd.CRS_GenerateValid")
        End Get
        Set(ByVal value As List(Of CRS_International_Generate_Bulk_CountryValid))
            Session("CRS_International_GenerateAdd.CRS_GenerateValid") = value
        End Set
    End Property

    Public Property CRS_GenerateNotValid() As List(Of CRS_International_Generate_Bulk_CountryInValid)
        Get
            Return Session("CRS_International_GenerateAdd.CRS_GenerateNotValid")
        End Get
        Set(ByVal value As List(Of CRS_International_Generate_Bulk_CountryInValid))
            Session("CRS_International_GenerateAdd.CRS_GenerateNotValid") = value
        End Set
    End Property

    Public Property CRS_Report_Group_Class() As CRSBLL.CRS_Report_BLL.CRS_Reporting_Group_Class
        Get
            Return Session("CRS_International_GenerateAdd.CRS_Report_Group_Class")
        End Get
        Set(ByVal value As CRSBLL.CRS_Report_BLL.CRS_Reporting_Group_Class)
            Session("CRS_International_GenerateAdd.CRS_Report_Group_Class") = value
        End Set
    End Property

    Private Sub CRS_International_GenerateAdd_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()
                LoadDataGenerateBulk()
                SetCommandColumnLocation()

            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub ClearSession()
        Session("pkselectedreport") = Nothing
        Session("This_Selected_Report_Data") = Nothing
        'ObjModule = Nothing
    End Sub



    Protected Sub gp_country_Gridcommand(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                'Clear_WindowReport()
                IDUnik = id
                'windowreportheader.Hidden = False
                'SetReportHeader(id)
                'CRS_Report_Class = CRS_Report_BLL.GetCRSINTReportClassByID(id)
                ''Load Reporting Group By ID
                'Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Class.list_CRS_Reporting_Group)


                'gp_Report_Group.GetStore().DataSource = objtable
                'gp_Report_Group.GetStore().DataBind()
            Else

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Protected Sub gp_country_invalid_Gridcommand(sender As Object, e As Ext.Net.DirectEventArgs)
    '    Try
    '        Dim id As String = e.ExtraParams(0).Value
    '        If e.ExtraParams(1).Value = "Detail" Then
    '            'Clear_WindowScreeningDetail()
    '            'WindowScreeningDetail.Hidden = False
    '            'LoadData_WindowScreeningDetail(id)
    '        Else

    '        End If
    '    Catch ex As Exception
    '        ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    Sub SetCommandColumnLocation()
        Try
            Dim objParamSettingbutton As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(32)
            Dim buttonPosition As Integer = 1
            If Not objParamSettingbutton Is Nothing Then
                buttonPosition = objParamSettingbutton.SettingValue
            End If

            'ColumnActionLocation(gp_country, gp_country_CommandColumn, buttonPosition)
            'ColumnActionLocation(gp_country_All, gp_country_All_CommandColumn, buttonPosition)
            'ColumnActionLocation(gp_country_invalid, gp_country_invalid_CommandColumn, buttonPosition)
            ''ColumnActionLocation(gp_Report_Group, cc_Reporting_Group, buttonPosition)
            ''ColumnActionLocation(gp_payment, cc_payment, buttonPosition)
            ''ColumnActionLocation(gp_cp_Detail, cc_cp_detail, buttonPosition)
        Catch ex As Exception

        End Try
    End Sub

    Sub ColumnActionLocation(gridpanel As GridPanel, commandColumn As ColumnBase, buttonPosition As Integer)
        If buttonPosition = 2 Then
            gridpanel.ColumnModel.Columns.RemoveAt(gridpanel.ColumnModel.Columns.Count - 1)
            gridpanel.ColumnModel.Columns.Insert(1, commandColumn)
        End If
    End Sub
    Protected Sub LoadDataGenerateBulk()
        Try
            ' Year
            Dim listyear As New List(Of Years)
            Dim year As New Years
            Dim years As New DataTable
            Dim ids As Integer = 0
            Using obdjb As New CRSEntities
                years = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select distinct year(reportingperiod) as year from CRS_INTERNATIONAL_REPORT_Header")
                For Each items As DataRow In years.Rows
                    ids = ids + 1
                    year.Id = ids
                    year.year = items("year")
                    listyear.Add(year)
                Next
            End Using

            Dim table As New DataTable()
            Dim fields() As FieldInfo = GetType(Years).GetFields()
            For Each field As FieldInfo In fields
                table.Columns.Add(field.Name, field.FieldType)
            Next
            For Each item As Years In listyear
                Dim row As DataRow = table.NewRow()
                For Each field As FieldInfo In fields
                    row(field.Name) = field.GetValue(item)
                Next
                table.Rows.Add(row)
            Next

            'Country
            Dim listcountry As New List(Of Countrys)
            Dim country As New Countrys
            country.Id = 1
            country.formatdata = "ALL"
            listcountry.Add(country)
            Dim country2 As New Countrys
            country2.Id = 2
            country2.formatdata = "Select Country"
            listcountry.Add(country2)

            Dim tablecountry As New DataTable()
            Dim fieldscountry() As FieldInfo = GetType(Countrys).GetFields()
            For Each field As FieldInfo In fieldscountry
                tablecountry.Columns.Add(field.Name, field.FieldType)
            Next
            For Each item As Countrys In listcountry
                Dim row As DataRow = tablecountry.NewRow()
                For Each field As FieldInfo In fieldscountry
                    row(field.Name) = field.GetValue(item)
                Next
                tablecountry.Rows.Add(row)
            Next


            storecountry.DataSource = tablecountry
            storecountry.DataBind()

            'Format
            Dim listformat As New List(Of Formats)
            Dim Formats As New Formats
            Formats.Id = 1
            Formats.format = "Excel"
            listformat.Add(Formats)
            Dim Formats2 As New Formats
            Formats2.Id = 2
            Formats2.format = "XML"
            listformat.Add(Formats2)

            Dim tableformat As New DataTable()
            Dim fieldsformat() As FieldInfo = GetType(Formats).GetFields()
            For Each field As FieldInfo In fieldsformat
                tableformat.Columns.Add(field.Name, field.FieldType)
            Next
            For Each item As Formats In listformat
                Dim row As DataRow = tableformat.NewRow()
                For Each field As FieldInfo In fieldsformat
                    row(field.Name) = field.GetValue(item)
                Next
                tableformat.Rows.Add(row)
            Next


            storeformat.DataSource = tableformat
            storeformat.DataBind()

            storeyear.DataSource = table
            storeyear.DataBind()

            If Session("GenerateBULK") = 1 Then
                Session("This_Selected_Report_Data") = 1
                display_year.Hidden = False
                display_year.Value = Session("Reporting_Periode_CRS_International_Selected_Row").ToString
                cboyear.Hidden = True
                display_country.Hidden = False
                cbocountry.Hidden = True
                gp_country_invalid.Hidden = True
                btnSearch.Hidden = True
                gp_country.Hidden = True
                gp_country_All.Hidden = False
                btnSave.Hidden = False
                Session("GenerateBULK") = 0
                Dim pkselectedreport As String = Session("PK_CRS_International_Selected_Row").ToString
                Session("PK_CRS_International_Selected_Row") = Nothing
                Session("pkselectedreport") = pkselectedreport
                CRS_GenerateValid = New List(Of CRS_International_Generate_Bulk_CountryValid)
                CRS_GenerateNotValid = New List(Of CRS_International_Generate_Bulk_CountryInValid)


                Dim nilaiformat = cbopjenisformat.RawValue
                Dim nilaiyear = cboyear.RawValue

                Dim test = nilaiyear
                Dim query1 As String = ""
                Dim query2 As String = ""

                query1 = "select max(a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
                  " max(country.CONTRY_NAME) as ReceivingCountry  , a.Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
                  " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
                   " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
                  " where PK_CRS_INTERNATIONAL_REPORT_HEADER_ID in(" & pkselectedreport & ") " &
                  " Group by country.CONTRY_NAME,a.Status,a.MarkedAsDelete "

                Dim datacountryvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query1)

                For Each row As DataRow In datacountryvalid.Rows


                    Dim status As Integer = row("Status")
                    Dim markasdelete As String = ""
                    If Not IsDBNull(row("MarkedAsDelete")) Then
                        markasdelete = row("MarkedAsDelete")
                    Else
                        markasdelete = "0"
                    End If

                    Dim displayStatus As String = ""
                    If status = 0 Then
                        displayStatus = "0 - Draft"
                    ElseIf status = 1 Then
                        displayStatus = "1 - Waiting For Generate"
                    ElseIf status = 2 Then
                        displayStatus = "2 - Waiting For Upload Reference No"
                    ElseIf status = 3 Then
                        displayStatus = "3 - Reference No Uploaded"
                    ElseIf status = 4 Then
                        If markasdelete Is Nothing Then
                            displayStatus = "4 - Waiting For Approval : Add/Edit Data"
                        Else
                            If markasdelete = "1" Then
                                displayStatus = "4 - Waiting For Approval : Delete Data"
                            Else
                                displayStatus = "4 - Waiting For Approval : Add/Edit Data"
                            End If
                        End If

                    ElseIf status = 5 Then
                        displayStatus = "5 - Need To Correction"
                    ElseIf status = 6 Then
                        displayStatus = "6 - Waiting Report Generated"
                    ElseIf status = 7 Then
                        displayStatus = "7 - Need Report Correction"
                    ElseIf status = 8 Then
                        displayStatus = "8 - XML Generated"
                    ElseIf status = 9 Then
                        displayStatus = "9 - Excel Generated"
                    ElseIf status = 10 Then
                        displayStatus = "10 - In Progress Editing"

                    End If

                    row("Status") = displayStatus


                Next
                gp_country_All.GetStore.DataSource = datacountryvalid
                gp_country_All.GetStore.DataBind()

            Else
                display_year.Hidden = True
                display_year.Value = Nothing
                cboyear.Hidden = False
                display_country.Hidden = True
                cbocountry.Hidden = False
                gp_country_invalid.Hidden = True
                btnSearch.Hidden = False
                gp_country.Hidden = True
                gp_country_All.Hidden = True
                btnSave.Hidden = True
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub isDataValid()
        If cbopjenisformat.SelectedItem.Value Is Nothing Then
            Throw New ApplicationException(cbopjenisformat.FieldLabel + " Is required.")
        End If
        If cboyear.SelectedItem.Value Is Nothing Then
            Throw New ApplicationException(cboyear.FieldLabel + " Is required.")
        End If
        If cbocountry.SelectedItem.Value Is Nothing Then
            Throw New ApplicationException(cbocountry.FieldLabel + " Is required.")
        End If
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As DirectEventArgs)
        Try
            If cbopjenisformat.SelectedItem.Value Is Nothing Then
                Throw New ApplicationException(cbopjenisformat.FieldLabel + " Is required.")
            End If


            CRS_GenerateValid = New List(Of CRS_International_Generate_Bulk_CountryValid)
            CRS_GenerateNotValid = New List(Of CRS_International_Generate_Bulk_CountryInValid)

            Using objdb As New CRSEntities
                Dim generateheader As New CRS_International_Generate_Bulk
                Dim query1 As String = ""
                Dim nilaiyear As String = cboyear.RawValue
                Dim nilaiyears As String = ""
                If Session("Reporting_Periode_CRS_International_Selected_Row") IsNot Nothing Then
                    nilaiyears = Session("Reporting_Periode_CRS_International_Selected_Row").ToString
                End If

                Session("Reporting_Periode_CRS_International_Selected_Row") = Nothing
                Dim query2 As String = ""

                If Session("This_Selected_Report_Data") = 1 Then
                    Dim pkselectcrs As String = Session("pkselectedreport").ToString
                    query1 = "select max(a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
                  " max(country.CONTRY_NAME) as ReceivingCountry  , a.Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
                  " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
                   " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
                  " where PK_CRS_INTERNATIONAL_REPORT_HEADER_ID in(" & pkselectcrs & ") " &
                  " Group by country.CONTRY_NAME,a.Status,a.MarkedAsDelete "

                    Dim datacountryvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query1)
                    With generateheader
                        .Report_ID = "Bulk"
                        .YearPeriode = nilaiyears
                        .Format = cbopjenisformat.RawValue
                        .Total_Report_Ready_to_Generate = datacountryvalid.Rows.Count
                        .CreatedDate = DateTime.Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID.ToString
                        objdb.Entry(generateheader).State = Entity.EntityState.Added
                        objdb.SaveChanges()
                        Dim strQuery1 As String = "INSERT INTO CRS_International_Generate_Bulk_CountryValid ("
                        strQuery1 += "FK_Header_Generate_ID, "
                        strQuery1 += "PK_report_ID, "
                        strQuery1 += "Country, "
                        strQuery1 += "Status, "
                        strQuery1 += "CreatedDate, "
                        strQuery1 += "CreatedBy) "
                        strQuery1 += " SELECT "
                        strQuery1 += generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID.ToString
                        strQuery1 += " , pk_crs_international_report_header_id ,"
                        strQuery1 += " ReceivingCountry, "
                        strQuery1 += " Status, "
                        strQuery1 += " getdate()"
                        strQuery1 += ", '"
                        strQuery1 += NawaBLL.Common.SessionCurrentUser.UserID.ToString
                        strQuery1 += "' "
                        strQuery1 += " from crs_international_report_header where PK_CRS_INTERNATIONAL_REPORT_HEADER_ID in(" & pkselectcrs & ") "
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery1, Nothing)

                    End With
                    Session("This_Selected_Report_Data") = 0
                Else
                    Dim allowneedtocorrection As Integer = CRS_Global_BLL.getGlobalParameterValueByID(14)
                    If allowneedtocorrection = 0 Then
                        '      query1 = "select max(a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as PK_CRS_INTERNATIONAL_REPORT_HEADER_ID ,a.MarkedAsDelete, " &
                        '" max(country.CONTRY_NAME) as ReceivingCountry  , a.Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
                        '" From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
                        ' " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
                        '" where a.MessageTypeIndic = 'CRS701' and a.ReceivingCountry <> 'US' and a.status in(1,2,3,4,6,7,8,9,10)  and year(reportingperiod) = '" & nilaiyear & "'" &
                        '" Group by country.CONTRY_NAME,a.Status,a.MarkedAsDelete "
                        query1 = "select max(a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as PK_CRS_INTERNATIONAL_REPORT_HEADER_ID ,a.MarkedAsDelete, " &
                        " max(country.CONTRY_NAME) as ReceivingCountry  , max(a.Status) as Status " &
                        " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
                        " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
                        " where a.MessageTypeIndic = 'CRS701' and a.ReceivingCountry <> 'US' and a.status in(1,2,3,4,6,7,8,9,10)  and year(reportingperiod) = '" & nilaiyear & "'" &
                        " Group by country.CONTRY_NAME,a.MarkedAsDelete "
                        query2 = "select a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
                    " country.CONTRY_NAME as ReceivingCountry, a.Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
                    " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
                 " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
                " where a.MessageTypeIndic = 'CRS701' and a.ReceivingCountry <> 'US' and a.status in(0,5)  and year(reportingperiod) = '" & nilaiyear & "'" &
                    " Group by a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,country.CONTRY_NAME,a.Status,a.MarkedAsDelete "
                    Else
                        query1 = "select max(a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
                    " max(country.CONTRY_NAME) as ReceivingCountry  , max(a.Status) as Status " &
                    " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
                     " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
                    " where a.MessageTypeIndic = 'CRS701' and a.ReceivingCountry <> 'US' and a.status <> 0  and year(reportingperiod) = '" & nilaiyear & "'" &
                    " Group by country.CONTRY_NAME,a.MarkedAsDelete "
                        query2 = "select a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
                    " country.CONTRY_NAME as ReceivingCountry, a.Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
                    " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
                 " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
                " where a.MessageTypeIndic = 'CRS701' and a.ReceivingCountry <> 'US' and a.status = 0  and year(reportingperiod) = '" & nilaiyear & "'" &
                    " Group by a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,country.CONTRY_NAME,a.Status,a.MarkedAsDelete "

                    End If
                    Dim datacountryvalidpk As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query1)

                    Dim pkselectcountrys As String = ""
                    Dim listcountryresults As New List(Of CRS_INTERNATIONAL_REPORT_HEADER)
                    For Each items As DataRow In datacountryvalidpk.Rows

                        pkselectcountrys = pkselectcountrys & "," & items("PK_CRS_INTERNATIONAL_REPORT_HEADER_ID")
                    Next
                    pkselectcountrys = pkselectcountrys.Remove(0, 1)
                    Dim query11 As String = ""
                    query11 = "select max(a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
                  " max(country.CONTRY_NAME) as ReceivingCountry  , a.Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
                  " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
                   " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
                  " where PK_CRS_INTERNATIONAL_REPORT_HEADER_ID in(" & pkselectcountrys & ") " &
                  " Group by country.CONTRY_NAME,a.Status,a.MarkedAsDelete "
                    Dim datacountryvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query11)
                    Dim datacountrynotvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query2)
                    If datacountryvalid.Rows.Count = 0 Then
                        Throw New Exception("Empty Data Country Valid, Can't Save Data")
                    End If
                    With generateheader
                        If Session("Vw_CRS_INTERNATIONAL_GenerateXMLParalel_PK") Is Nothing Then
                            If allowneedtocorrection = 0 Then
                                If cbocountry.SelectedItem.Index = 0 Then
                                    .Report_ID = "Bulk"
                                    .YearPeriode = cboyear.RawValue
                                    .Format = cbopjenisformat.RawValue
                                    .Total_Report_Ready_to_Generate = datacountryvalid.Rows.Count
                                    .CreatedDate = DateTime.Now
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID.ToString
                                    objdb.Entry(generateheader).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    Dim strQuery1 As String = "INSERT INTO CRS_International_Generate_Bulk_CountryValid ("
                                    strQuery1 += "FK_Header_Generate_ID, "
                                    strQuery1 += "PK_report_ID, "
                                    strQuery1 += "Country, "
                                    strQuery1 += "Status, "
                                    strQuery1 += "CreatedDate, "
                                    strQuery1 += "CreatedBy) "
                                    strQuery1 += " SELECT DISTINCT "
                                    strQuery1 += generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID.ToString
                                    strQuery1 += " , max(pk_crs_international_report_header_id) as pk_crs_international_report_header_id ,"
                                    strQuery1 += " max(ReceivingCountry) as ReceivingCountry, "
                                    strQuery1 += " max(Status) as Status, "
                                    strQuery1 += " getdate()"
                                    strQuery1 += ", '"
                                    strQuery1 += NawaBLL.Common.SessionCurrentUser.UserID.ToString
                                    strQuery1 += "' "
                                    strQuery1 += " from crs_international_report_header where messagetypeindic = 'CRS701' and status in(1,2,3,4,6,7,8,9,10) and ReceivingCountry <> 'US' and year(reportingperiod) = '" & cboyear.RawValue & "' group by ReceivingCountry"
                                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery1, Nothing)
                                    Dim strQuery2 As String = "INSERT INTO CRS_International_Generate_Bulk_CountryInValid ("
                                    strQuery2 += "FK_Header_Generate_ID, "
                                    strQuery2 += "PK_report_ID, "
                                    strQuery2 += "Country, "
                                    strQuery2 += "Status, "
                                    strQuery2 += "CreatedDate, "
                                    strQuery2 += "CreatedBy) "
                                    strQuery2 += " SELECT "
                                    strQuery2 += generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID.ToString
                                    strQuery2 += " , pk_crs_international_report_header_id ,"
                                    strQuery2 += " ReceivingCountry, "
                                    strQuery2 += " Status, "
                                    strQuery2 += " getdate()"
                                    strQuery2 += ", '"
                                    strQuery2 += NawaBLL.Common.SessionCurrentUser.UserID.ToString
                                    strQuery2 += "' "
                                    strQuery2 += " from crs_international_report_header where messagetypeindic = 'CRS701' and status in(0,5) and ReceivingCountry <> 'US' and year(reportingperiod) = '" & cboyear.RawValue & "' "
                                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                                Else

                                    Dim smCountry As RowSelectionModel = TryCast(gp_country.GetSelectionModel(), RowSelectionModel)
                                    Dim selected As RowSelectionModel = gp_country.SelectionModel.Primary
                                    If selected.SelectedRows.Count = 0 Then

                                        Throw New Exception("Minimal 1 data harus dipilih jika Selected Country!")
                                    End If
                                    .Report_ID = "Bulk"
                                    .YearPeriode = cboyear.RawValue
                                    .Format = cbopjenisformat.RawValue
                                    .Total_Report_Ready_to_Generate = selected.SelectedRows.Count
                                    .CreatedDate = DateTime.Now
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID.ToString
                                    objdb.Entry(generateheader).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    Dim listCountryPK As New List(Of CRS_INTERNATIONAL_REPORT_HEADER)
                                    For Each item As SelectedRow In smCountry.SelectedRows
                                        Dim recordID = item.RecordID.ToString
                                        Dim objNew = New CRSDAL.CRS_INTERNATIONAL_REPORT_HEADER
                                        With objNew
                                            .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = recordID
                                        End With

                                        listCountryPK.Add(objNew)
                                    Next
                                    Dim pkselectcountry As String = ""
                                    Dim listcountryresult As New List(Of CRS_INTERNATIONAL_REPORT_HEADER)
                                    For Each item In listCountryPK

                                        pkselectcountry = pkselectcountry & "," & item.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID
                                    Next
                                    pkselectcountry = pkselectcountry.Remove(0, 1)

                                    Dim strQuery1 As String = "INSERT INTO CRS_International_Generate_Bulk_CountryValid ("
                                    strQuery1 += "FK_Header_Generate_ID, "
                                    strQuery1 += "PK_report_ID, "
                                    strQuery1 += "Country, "
                                    strQuery1 += "Status, "
                                    strQuery1 += "CreatedDate, "
                                    strQuery1 += "CreatedBy) "
                                    strQuery1 += " SELECT "
                                    strQuery1 += generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID.ToString
                                    strQuery1 += " , pk_crs_international_report_header_id ,"
                                    strQuery1 += " ReceivingCountry, "
                                    strQuery1 += " Status, "
                                    strQuery1 += " getdate()"
                                    strQuery1 += ", '"
                                    strQuery1 += NawaBLL.Common.SessionCurrentUser.UserID.ToString
                                    strQuery1 += "' "
                                    strQuery1 += " from CRS_INTERNATIONAL_REPORT_HEADER WHERE PK_CRS_INTERNATIONAL_REPORT_HEADER_ID in(" & pkselectcountry & ") "
                                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery1, Nothing)
                                    Dim strQuery2 As String = "INSERT INTO CRS_International_Generate_Bulk_CountryInValid ("
                                    strQuery2 += "FK_Header_Generate_ID, "
                                    strQuery2 += "PK_report_ID, "
                                    strQuery2 += "Country, "
                                    strQuery2 += "Status, "
                                    strQuery2 += "CreatedDate, "
                                    strQuery2 += "CreatedBy) "
                                    strQuery2 += " SELECT "
                                    strQuery2 += generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID.ToString
                                    strQuery2 += " , pk_crs_international_report_header_id ,"
                                    strQuery2 += " ReceivingCountry, "
                                    strQuery2 += " Status, "
                                    strQuery2 += " getdate()"
                                    strQuery2 += ", '"
                                    strQuery2 += NawaBLL.Common.SessionCurrentUser.UserID.ToString
                                    strQuery2 += "' "
                                    strQuery2 += " from crs_international_report_header where messagetypeindic = 'CRS701' and status in(0,5) and ReceivingCountry <> 'US' and year(reportingperiod) = '" & cboyear.RawValue & "' "
                                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                                End If
                            Else
                                If cbocountry.SelectedItem.Index = 0 Then
                                    .Report_ID = "Bulk"
                                    .YearPeriode = cboyear.RawValue
                                    .Format = cbopjenisformat.RawValue
                                    .Total_Report_Ready_to_Generate = datacountryvalid.Rows.Count
                                    .CreatedDate = DateTime.Now
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID.ToString
                                    objdb.Entry(generateheader).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    Dim strQuery1 As String = "INSERT INTO CRS_International_Generate_Bulk_CountryValid ("
                                    strQuery1 += "FK_Header_Generate_ID, "
                                    strQuery1 += "PK_report_ID, "
                                    strQuery1 += "Country, "
                                    strQuery1 += "Status, "
                                    strQuery1 += "CreatedDate, "
                                    strQuery1 += "CreatedBy) "
                                    strQuery1 += " SELECT DISTINCT "
                                    strQuery1 += generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID.ToString
                                    strQuery1 += " , max(pk_crs_international_report_header_id) as pk_crs_international_report_header_id ,"
                                    strQuery1 += " max(ReceivingCountry) as ReceivingCountry, "
                                    strQuery1 += " max(Status) as Status, "
                                    strQuery1 += " getdate()"
                                    strQuery1 += ", '"
                                    strQuery1 += NawaBLL.Common.SessionCurrentUser.UserID.ToString
                                    strQuery1 += "' "
                                    strQuery1 += " from crs_international_report_header where messagetypeindic = 'CRS701' and status <> 0 and ReceivingCountry <> 'US' and year(reportingperiod) = '" & cboyear.RawValue & "' group by ReceivingCountry"
                                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery1, Nothing)
                                    Dim strQuery2 As String = "INSERT INTO CRS_International_Generate_Bulk_CountryInValid ("
                                    strQuery2 += "FK_Header_Generate_ID, "
                                    strQuery2 += "PK_report_ID, "
                                    strQuery2 += "Country, "
                                    strQuery2 += "Status, "
                                    strQuery2 += "CreatedDate, "
                                    strQuery2 += "CreatedBy) "
                                    strQuery2 += " SELECT "
                                    strQuery2 += generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID.ToString
                                    strQuery2 += " , pk_crs_international_report_header_id ,"
                                    strQuery2 += " ReceivingCountry, "
                                    strQuery2 += " Status, "
                                    strQuery2 += " getdate()"
                                    strQuery2 += ", '"
                                    strQuery2 += NawaBLL.Common.SessionCurrentUser.UserID.ToString
                                    strQuery2 += "' "
                                    strQuery2 += " from crs_international_report_header where messagetypeindic = 'CRS701' and status = 0 and ReceivingCountry <> 'US' and year(reportingperiod) = '" & cboyear.RawValue & "' "
                                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                                Else

                                    Dim smCountry As RowSelectionModel = TryCast(gp_country.GetSelectionModel(), RowSelectionModel)
                                    Dim selected As RowSelectionModel = gp_country.SelectionModel.Primary
                                    If selected.SelectedRows.Count = 0 Then

                                        Throw New Exception("Minimal 1 data harus dipilih jika Selected Country!")
                                    End If
                                    .Report_ID = "Bulk"
                                    .YearPeriode = cboyear.RawValue
                                    .Format = cbopjenisformat.RawValue
                                    .Total_Report_Ready_to_Generate = selected.SelectedRows.Count
                                    .CreatedDate = DateTime.Now
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID.ToString
                                    objdb.Entry(generateheader).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    Dim listCountryPK As New List(Of CRS_INTERNATIONAL_REPORT_HEADER)
                                    For Each item As SelectedRow In smCountry.SelectedRows
                                        Dim recordID = item.RecordID.ToString
                                        Dim objNew = New CRSDAL.CRS_INTERNATIONAL_REPORT_HEADER
                                        With objNew
                                            .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = recordID
                                        End With

                                        listCountryPK.Add(objNew)
                                    Next
                                    Dim pkselectcountry As String = ""
                                    Dim listcountryresult As New List(Of CRS_INTERNATIONAL_REPORT_HEADER)
                                    For Each item In listCountryPK

                                        pkselectcountry = pkselectcountry & "," & item.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID
                                    Next
                                    pkselectcountry = pkselectcountry.Remove(0, 1)

                                    Dim strQuery1 As String = "INSERT INTO CRS_International_Generate_Bulk_CountryValid ("
                                    strQuery1 += "FK_Header_Generate_ID, "
                                    strQuery1 += "PK_report_ID, "
                                    strQuery1 += "Country, "
                                    strQuery1 += "Status, "
                                    strQuery1 += "CreatedDate, "
                                    strQuery1 += "CreatedBy) "
                                    strQuery1 += " SELECT "
                                    strQuery1 += generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID.ToString
                                    strQuery1 += " , pk_crs_international_report_header_id ,"
                                    strQuery1 += " ReceivingCountry, "
                                    strQuery1 += " Status, "
                                    strQuery1 += " getdate()"
                                    strQuery1 += ", '"
                                    strQuery1 += NawaBLL.Common.SessionCurrentUser.UserID.ToString
                                    strQuery1 += "' "
                                    strQuery1 += " from CRS_INTERNATIONAL_REPORT_HEADER WHERE PK_CRS_INTERNATIONAL_REPORT_HEADER_ID in(" & pkselectcountry & ") "
                                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery1, Nothing)
                                    Dim strQuery2 As String = "INSERT INTO CRS_International_Generate_Bulk_CountryInValid ("
                                    strQuery2 += "FK_Header_Generate_ID, "
                                    strQuery2 += "PK_report_ID, "
                                    strQuery2 += "Country, "
                                    strQuery2 += "Status, "
                                    strQuery2 += "CreatedDate, "
                                    strQuery2 += "CreatedBy) "
                                    strQuery2 += " SELECT "
                                    strQuery2 += generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID.ToString
                                    strQuery2 += " , pk_crs_international_report_header_id ,"
                                    strQuery2 += " ReceivingCountry, "
                                    strQuery2 += " Status, "
                                    strQuery2 += " getdate()"
                                    strQuery2 += ", '"
                                    strQuery2 += NawaBLL.Common.SessionCurrentUser.UserID.ToString
                                    strQuery2 += "' "
                                    strQuery2 += " from crs_international_report_header where messagetypeindic = 'CRS701' and status = 0 and ReceivingCountry <> 'US' and year(reportingperiod) = '" & cboyear.RawValue & "' "
                                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                                End If
                            End If



                        End If
                    End With
                End If

            End Using
            fpMain.Hidden = True
            lbl_Confirmation.Text = "Data Saved into Database"
            pnl_Confirmation.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub btnCancel_DirectEvent(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub CRS_International_GenerateAdd_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Insert
    End Sub

    Private Sub CRS_International_GenerateAdd_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleAdd = New NawaBLL.FormModuleAdd(fpMain, pnl_Confirmation, lbl_Confirmation)
    End Sub

    Protected Sub btnSearch_Click()
        Try
            CRS_GenerateValid = New List(Of CRS_International_Generate_Bulk_CountryValid)
            CRS_GenerateNotValid = New List(Of CRS_International_Generate_Bulk_CountryInValid)
            If cbopjenisformat.SelectedItem.Value Is Nothing Then
                Throw New ApplicationException(cbopjenisformat.FieldLabel + " Is required.")
            End If
            If cboyear.SelectedItem.Value Is Nothing Then
                Throw New ApplicationException(cboyear.FieldLabel + " Is required.")
            End If
            If cbocountry.SelectedItem.Value Is Nothing Then
                Throw New ApplicationException(cbocountry.FieldLabel + " Is required.")
            End If


            'Hide Grid Screening Result
            gp_country_invalid.Hidden = False
            If cbocountry.SelectedItem.Index = 0 Then
                gp_country_All.Hidden = False
                gp_country.Hidden = True
            Else
                gp_country_All.Hidden = True
                gp_country.Hidden = False

            End If

            Dim nilaiformat = cbopjenisformat.RawValue
            Dim nilaiyear = cboyear.RawValue

            Dim test = nilaiyear
            Dim query1 As String = ""
            Dim query2 As String = ""
            Dim allowneedtocorrection As Integer = CRS_Global_BLL.getGlobalParameterValueByID(14)
            If allowneedtocorrection = 0 Then
                query1 = "select max(a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
              " max(country.CONTRY_NAME) as ReceivingCountry  , max(a.Status) as Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
              " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
               " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
              " where a.MessageTypeIndic = 'CRS701' and a.ReceivingCountry <> 'US' and a.status in(1,2,3,4,6,7,8,9,10)  and year(reportingperiod) = '" & nilaiyear & "'" &
              " Group by country.CONTRY_NAME,a.MarkedAsDelete "
                query2 = "select a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
                " country.CONTRY_NAME as ReceivingCountry, a.Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
                " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
             " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
            " where a.MessageTypeIndic = 'CRS701' and a.ReceivingCountry <> 'US' and a.status in(0,5)  and year(reportingperiod) = '" & nilaiyear & "'" &
                " Group by a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,country.CONTRY_NAME,a.Status,a.MarkedAsDelete "
            Else
                query1 = "select max(a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
                " max(country.CONTRY_NAME) as ReceivingCountry  , max(a.Status) as Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
                " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
                 " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
                " where a.MessageTypeIndic = 'CRS701' and a.ReceivingCountry <> 'US' and a.status <> 0  and year(reportingperiod) = '" & nilaiyear & "'" &
                " Group by country.CONTRY_NAME,a.MarkedAsDelete "

                query2 = "select a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
                " country.CONTRY_NAME as ReceivingCountry, a.Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
                " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
             " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
            " where a.MessageTypeIndic = 'CRS701' and a.ReceivingCountry <> 'US' and a.status = 0  and year(reportingperiod) = '" & nilaiyear & "'" &
                " Group by a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,country.CONTRY_NAME,a.Status,a.MarkedAsDelete "

            End If
            Dim datacountryvalidpk As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query1)

            Dim pkselectcountrys As String = ""
            Dim listcountryresults As New List(Of CRS_INTERNATIONAL_REPORT_HEADER)
            For Each items As DataRow In datacountryvalidpk.Rows

                pkselectcountrys = pkselectcountrys & "," & items("PK_CRS_INTERNATIONAL_REPORT_HEADER_ID")
            Next
            pkselectcountrys = pkselectcountrys.Remove(0, 1)
            Dim query11 As String = ""
            query11 = "select max(a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
                  " max(country.CONTRY_NAME) as ReceivingCountry  , a.Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
                  " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
                   " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
                  " where PK_CRS_INTERNATIONAL_REPORT_HEADER_ID in(" & pkselectcountrys & ") " &
                  " Group by country.CONTRY_NAME,a.Status,a.MarkedAsDelete "
            Dim datacountryvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query11)
            Dim datacountrynotvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query2)


            For Each row As DataRow In datacountryvalid.Rows


                Dim status As Integer = row("Status")
                Dim markasdelete As String = ""
                If Not IsDBNull(row("MarkedAsDelete")) Then
                    markasdelete = row("MarkedAsDelete")
                Else
                    markasdelete = "0"
                End If

                Dim displayStatus As String = ""
                If status = 0 Then
                    displayStatus = "0 - Draft"
                ElseIf status = 1 Then
                    displayStatus = "1 - Waiting For Generate"
                ElseIf status = 2 Then
                    displayStatus = "2 - Waiting For Upload Reference No"
                ElseIf status = 3 Then
                    displayStatus = "3 - Reference No Uploaded"
                ElseIf status = 4 Then
                    If markasdelete Is Nothing Then
                        displayStatus = "4 - Waiting For Approval : Add/Edit Data"
                    Else
                        If markasdelete = "1" Then
                            displayStatus = "4 - Waiting For Approval : Delete Data"
                        Else
                            displayStatus = "4 - Waiting For Approval : Add/Edit Data"
                        End If
                    End If

                ElseIf status = 5 Then
                    displayStatus = "5 - Need To Correction"
                ElseIf status = 6 Then
                    displayStatus = "6 - Waiting Report Generated"
                ElseIf status = 7 Then
                    displayStatus = "7 - Need Report Correction"
                ElseIf status = 8 Then
                    displayStatus = "8 - XML Generated"
                ElseIf status = 9 Then
                    displayStatus = "9 - Excel Generated"
                ElseIf status = 10 Then
                    displayStatus = "10 - In Progress Editing"

                End If

                row("Status") = displayStatus


            Next

            For Each row As DataRow In datacountrynotvalid.Rows
                Dim status As Integer = row("Status")
                Dim markasdelete As String = ""
                If Not IsDBNull(row("MarkedAsDelete")) Then
                    markasdelete = row("MarkedAsDelete")
                Else
                    markasdelete = "0"
                End If

                Dim displayStatus As String = ""
                If status = 0 Then
                    displayStatus = "0 - Draft"
                ElseIf status = 1 Then
                    displayStatus = "1 - Waiting For Generate"
                ElseIf status = 2 Then
                    displayStatus = "2 - Waiting For Upload Reference No"
                ElseIf status = 3 Then
                    displayStatus = "3 - Reference No Uploaded"
                ElseIf status = 4 Then
                    If markasdelete Is Nothing Then
                        displayStatus = "4 - Waiting For Approval : Add/Edit Data"
                    Else
                        If markasdelete = "1" Then
                            displayStatus = "4 - Waiting For Approval : Delete Data"
                        Else
                            displayStatus = "4 - Waiting For Approval : Add/Edit Data"
                        End If
                    End If

                ElseIf status = 5 Then
                    displayStatus = "5 - Need To Correction"
                ElseIf status = 6 Then
                    displayStatus = "6 - Waiting Report Generated"
                ElseIf status = 7 Then
                    displayStatus = "7 - Need Report Correction"
                ElseIf status = 8 Then
                    displayStatus = "8 - XML Generated"
                ElseIf status = 9 Then
                    displayStatus = "9 - Excel Generated"
                ElseIf status = 10 Then
                    displayStatus = "10 - In Progress Editing"

                End If
                row("Status") = displayStatus
            Next

            gp_country_invalid.GetStore.DataSource = datacountrynotvalid
            gp_country_invalid.GetStore.DataBind()
            If cbocountry.SelectedItem.Index = 0 Then
                gp_country_All.GetStore.DataSource = datacountryvalid
                gp_country_All.GetStore.DataBind()
            Else
                gp_country.GetStore.DataSource = datacountryvalid
                gp_country.GetStore.DataBind()
            End If


            ''Save to EOD Scheduler and get the PK_EODTaskDetailLog_ID for check the status
            'Dim objParamRequest(1) As SqlParameter
            'objParamRequest(0) = New SqlParameter
            'objParamRequest(0).ParameterName = "@PK_Request_ID"
            'objParamRequest(0).Value = PK_Request_ID

            'objParamRequest(1) = New SqlParameter
            'objParamRequest(1).ParameterName = "@User_ID"
            'objParamRequest(1).Value = NawaBLL.Common.SessionCurrentUser.UserID

            'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_SCREENING_REQUEST_SaveEOD", objParamRequest) '' Edited on 27 Aug 2021

            ''Show status
            ''df_ScreeningStatus.Hidden = False
            ''df_ScreeningStatus.Text = "Screening is in progress. Please wait..."

            ''Show Confirmation
            ''LblConfirmation.Text = "Screening Request has been saved to Database and will be processed by Scheduler."
            ''FormPanelInput.Hidden = True
            ''Panelconfirmation.Hidden = False

            ''Check status periodically
            'btn_Screening.Hidden = True
            'btn_Save.Hidden = True
            'df_ScreeningStart.Text = DateTime.Now.ToString("dd-MMM-yyyy, HH:mm:ss")
            'panel_ScreeningStatus.Hidden = False
            'Dim cfg As New MaskConfig
            'cfg.Msg = "Screening in Progress..."
            'cfg.El = "Ext.getBody()"
            'Ext.Net.X.Mask.Show(cfg)
            'TaskManager1.StartTask("refreshScreeningStatus")

            btnSave.Hidden = False

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Protected Sub btnCancelReportHeader_DirectEvent(sender As Object, e As DirectEventArgs)
    '    Try
    '        windowreportheader.Hidden = True
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub

    '#Region "Reporting Group & CP"
    '    Protected Sub BindReportingGroup()
    '        Try
    '            If CRS_Report_Class.list_CRS_Reporting_Group IsNot Nothing Then
    '                Dim dtReportingGroup As DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Class.list_CRS_Reporting_Group.OrderBy(Function(x) x.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID).ToList)


    '                'Get Module ID by Module Name
    '                Dim intModuleID As Integer = 50053
    '                Dim objModuleCek As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("CRS_INTERNATIONAL_REPORTING_GROUP")
    '                If objModuleCek IsNot Nothing Then
    '                    intModuleID = objModuleCek.PK_Module_ID
    '                End If

    '                'Get status from count of Validation Report
    '                strSQL = "SELECT KeyFieldValue FROM CRS_International_ValidationReport"
    '                strSQL += " WHERE ModuleID = " & intModuleID     '13243 adalah module ID untuk CRS_INTERNATIONAL_REPORTING_GROUP
    '                strSQL += " And RecordID = " & IDUnik

    '                Dim dtCRS_International_ValidationReport As DataTable = CRS_Global_BLL.getDataTableByQuery(strSQL)

    '                'For Each row As DataRow In dtReportingGroup.Rows
    '                '    If dtCRS_International_ValidationReport IsNot Nothing Then

    '                '        If row("IsIndividualYN") = "1" Then
    '                '            row("IsIndividualYN") = "Yes"
    '                '        Else
    '                '            row("IsIndividualYN") = "No"
    '                '        End If
    '                '    End If
    '                'Next

    '                'Bind to gridpanel
    '                gp_Report_Group.GetStore.DataSource = dtReportingGroup
    '                gp_Report_Group.GetStore.DataBind()

    '            End If
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub

    '    Protected Sub btn_ReportingGroup_Back_Click(sender As Object, e As DirectEventArgs)
    '        Try
    '            CRS_Report_Group_Class.list_Reporting_RGARPayment = Nothing
    '            window_reporting_group.Hidden = True
    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub


    '    Protected Sub gc_Reporting_Group(sender As Object, e As DirectEventArgs)
    '        Try

    '            Dim ID As String = e.ExtraParams(0).Value
    '            Dim strCommandName As String = e.ExtraParams(1).Value
    '            If strCommandName = "Detail" Then
    '                'SetReadOnlyWindowReportingGroup(True)
    '                'ValidateReport()
    '                LoadReportingGroup("Detail", ID)
    '                Bind_Reporting_Payment()
    '                Bind_Reporting_cp()
    '            End If
    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub LoadReportingGroup(strCommandName As String, strID As String)
    '        Try


    '            'Change window_s title
    '            window_reporting_group.Title = "Reporting Group - " & strCommandName

    '            'Load Reporting Group By ID
    '            CRS_Report_Group_Class = CRS_Report_BLL.GetReportingGroupClassByID(strID)

    '            'Load Reporting Group Header
    '            If CRS_Report_Group_Class IsNot Nothing Then
    '                With CRS_Report_Group_Class.obj_Reporting_Group
    '                    'General Information
    '                    If Not IsNothing(.DocSpec_DocTypeIndic) Then
    '                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_International_Doc_Type_Indic", "FK_CRS_DOC_TYPE_INDIC_CODE", .DocSpec_DocTypeIndic)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_DocTypeIndicator.SetTextWithTextValue(drTemp("FK_CRS_DOC_TYPE_INDIC_CODE"), drTemp("DOC_TYPE_INDIC_NAME"))
    '                        End If
    '                    End If
    '                    If .DocSpec_CorrDocRefID IsNot Nothing Then
    '                        txt_CorDocRefID.Hidden = False
    '                        txt_CorDocRefID.Value = .DocSpec_CorrDocRefID
    '                    Else
    '                        txt_CorDocRefID.Hidden = True
    '                    End If

    '                    txt_DocRefIDGroup.Value = .DocSpec_DocRefID
    '                    chk_IsIndividulGroup.Checked = .IsIndividualYN
    '                    If .IsIndividualYN = True Then
    '                        If Not IsNothing(.INDV_ResCountryCode) Then
    '                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_ResCountryCode)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_ResCountryCode.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                            End If
    '                        End If
    '                    Else
    '                        If Not IsNothing(.ORG_ResCountryCode) Then
    '                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .ORG_ResCountryCode)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_ResCountryCode.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                            End If
    '                        End If
    '                    End If


    '                    'Account Infromation Group
    '                    txt_accountnumber_group.Value = .AccountNumber
    '                    If Not IsNothing(.Account_AcctNumberType) Then
    '                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_ACCOUNT_NUMBER_TYPE", "FK_CRS_ACCOUNT_NUMBER_TYPE_CODE", .Account_AcctNumberType)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_AccountType_Group.SetTextWithTextValue(drTemp("FK_CRS_ACCOUNT_NUMBER_TYPE_CODE"), drTemp("ACCOUNT_NUMBER_TYPE_NAME"))
    '                        End If
    '                    End If
    '                    chk_undocument_Group.Checked = .Account_UndocumentedAccount
    '                    chk_closedaccount_Group.Checked = .Account_ClosedAccount
    '                    chk_dormantaccount_Group.Checked = .Account_DormantAccount
    '                    If Not IsNothing(.AccountBalance_CurrencyCode) Then
    '                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_CURRENCY", "FK_CURRENCY_CODE", .AccountBalance_CurrencyCode)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_AccountCurrency_Group.SetTextWithTextValue(drTemp("FK_CURRENCY_CODE"), drTemp("CURRENCY_NAME"))
    '                        End If
    '                    End If

    '                    txt_accountbalance_Group.Value = .AccountBalance

    '                    'Individual Information Group
    '                    If .IsIndividualYN = True Then
    '                        cmb_AccountHolderType.IsHidden = True
    '                        pnlOIGP.Hidden = True
    '                        pnl_individualinformation_Group.Hidden = False
    '                        txt_name_Group.Value = .INDV_Name
    '                        If Not IsNothing(.INDV_Name_Type) Then
    '                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_NAME_TYPE", "FK_CRS_NAME_TYPE_CODE", .INDV_Name_Type)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_NameType_Group.SetTextWithTextValue(drTemp("FK_CRS_NAME_TYPE_CODE"), drTemp("NAME_TYPE_NAME"))
    '                            End If
    '                        End If
    '                        txt_precedingtitle_group.Value = .INDV_Name_PrecedingTitle
    '                        txt_title_group.Value = .INDV_Name_Title
    '                        txt_firstname_group.Value = .INDV_Name_FirstName
    '                        'txt_firstname_type_group.Value = .INDV_Name_FirstName_Type
    '                        txt_middlename_group.Value = .INDV_Name_MiddleName
    '                        'txt_middlename_type_group.Value = .INDV_Name_MiddleName_Type
    '                        txt_lastname_group.Value = .INDV_Name_LastName
    '                        'txt_lastname_type_group.Value = .INDV_Name_LastName_Type
    '                        txt_nameprefix_group.Value = .INDV_Name_NamePrefix
    '                        txt_generationidentifier_group.Value = .INDV_Name_GenerationIdentifier
    '                        txt_namesufix_group.Value = .INDV_Name_Suffix
    '                        txt_generalsufix_group.Value = .INDV_Name_GeneralSuffix
    '                        datebirthdate_group.Value = .INDV_BirthInfo_BirthDate
    '                        txt_birthcity_group.Value = .INDV_BirthInfo_City
    '                        txt_birthcitysubentity_group.Value = .INDV_BirthInfo_CitySubEntity
    '                        If Not IsNothing(.INDV_BirthInfo_CountryInfo_CountryCode) Then
    '                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_BirthInfo_CountryInfo_CountryCode)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_birthcountrycode_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                            End If
    '                        End If
    '                        If Not IsNothing(.INDV_Nationality1) Then
    '                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality1)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_nationality1_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                            End If
    '                        End If
    '                        If Not IsNothing(.INDV_Nationality2) Then
    '                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality1)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_nationality1_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                            End If
    '                        End If
    '                        If Not IsNothing(.INDV_Nationality3) Then
    '                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality3)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_nationality1_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                            End If
    '                        End If
    '                        If Not IsNothing(.INDV_Nationality4) Then
    '                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality4)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_nationality1_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                            End If
    '                        End If
    '                        If Not IsNothing(.INDV_Nationality5) Then
    '                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_Nationality5)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_nationality1_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                            End If
    '                        End If
    '                    Else
    '                        cmb_AccountHolderType.IsHidden = False
    '                        pnlOIGP.Hidden = False
    '                        pnl_individualinformation_Group.Hidden = True
    '                        If Not IsNothing(.ORG_AcctHolderType) Then
    '                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_HOLDER_TYPE", "FK_CRS_ACCOUNT_HOLDER_TYPE_CODE", .ORG_AcctHolderType)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_AccountHolderType.SetTextWithTextValue(drTemp("FK_CRS_ACCOUNT_HOLDER_TYPE_CODE"), drTemp("ACCOUNT_HOLDER_TYPE_NAME"))
    '                            End If
    '                        End If

    '                        txt_org_Name.Value = .ORG_Name
    '                        If Not IsNothing(.ORG_Name_NameType) Then
    '                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_NAME_TYPE", "FK_CRS_NAME_TYPE_CODE", .ORG_Name_NameType)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_org_nametype.SetTextWithTextValue(drTemp("FK_CRS_NAME_TYPE_CODE"), drTemp("NAME_TYPE_NAME"))
    '                            End If
    '                        End If
    '                        txt_precedingtitle_group.Value = Nothing
    '                        txt_title_group.Value = Nothing
    '                        txt_firstname_group.Value = Nothing
    '                        'txt_firstname_type_group.Value = Nothing
    '                        txt_middlename_group.Value = Nothing
    '                        'txt_middlename_type_group.Value = Nothing
    '                        txt_lastname_group.Value = Nothing
    '                        'txt_lastname_type_group.Value = Nothing
    '                        txt_nameprefix_group.Value = Nothing
    '                        txt_generationidentifier_group.Value = Nothing
    '                        txt_namesufix_group.Value = Nothing
    '                        txt_generalsufix_group.Value = Nothing
    '                        datebirthdate_group.Value = Nothing
    '                        txt_birthcity_group.Value = Nothing
    '                        txt_birthcitysubentity_group.Value = Nothing
    '                        cmb_birthcountrycode_group.SetTextValue("")

    '                        cmb_nationality1_group.SetTextValue("")
    '                        cmb_nationality2_group.SetTextValue("")
    '                        cmb_nationality3_group.SetTextValue("")
    '                        cmb_nationality4_group.SetTextValue("")
    '                        cmb_nationality5_group.SetTextValue("")

    '                    End If

    '                    'TIN Information Group
    '                    If .IsIndividualYN = True Then

    '                        txt_tin_group.Value = .INDV_TIN
    '                        If Not IsNothing(.INDV_TIN_ISSUEDBY) Then
    '                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .INDV_TIN_ISSUEDBY)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_tinissuedby_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                            End If
    '                        End If
    '                    Else
    '                        'TIN Information Group
    '                        txt_tin_group.Value = .ORG_IN
    '                        If Not IsNothing(.ORG_IN_IssuedBy) Then
    '                            drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .ORG_IN_IssuedBy)
    '                            If drTemp IsNot Nothing Then
    '                                cmb_tinissuedby_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                            End If
    '                        End If

    '                    End If


    '                    'Address Information Group
    '                    If Not IsNothing(.Address_LegalAddressType) Then
    '                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_LEGAL_ADDRESS", "FK_CRS_LEGAL_ADDRESS_TYPE_CODE", .Address_LegalAddressType)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_addresslegaltype_group.SetTextWithTextValue(drTemp("FK_CRS_LEGAL_ADDRESS_TYPE_CODE"), drTemp("LEGAL_ADDRESS_TYPE_NAME"))
    '                        End If
    '                    End If
    '                    If Not IsNothing(.Address_CountryCode) Then
    '                        drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .Address_CountryCode)
    '                        If drTemp IsNot Nothing Then
    '                            cmb_countrycode_Group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                        End If
    '                    End If
    '                    txt_addressfree_group.Value = .Address_AddressFree
    '                    txt_street_group.Value = .Address_AddressFix_Street
    '                    If .Address_AddressFix_Street IsNot Nothing Then
    '                        txt_city_group.AllowBlank = False
    '                        txt_city_group.FieldStyle = "background-color:#FFE4C4"
    '                    End If
    '                    txt_buildingindentifier_Group.Value = .Address_AddressFix_BuildingIdentifier
    '                    txt_suiteidentifier_group.Value = .Address_AddressFix_SuiteIdentifier
    '                    txt_flooridentifier_group.Value = .Address_AddressFix_FloorIdentifier
    '                    txt_districtname_Group.Value = .Address_AddressFix_DistrictName
    '                    txt_pob_group.Value = .Address_AddressFix_POB
    '                    txt_postcode_group.Value = .Address_AddressFix_PostCode
    '                    txt_city_group.Value = .Address_AddressFix_City
    '                    txt_countrysubentity_group.Value = .Address_AddressFix_CountrySubentity



    '                    If .ORG_AcctHolderType = "CRS101" Then
    '                        panelcpdetail.Hidden = False
    '                        windowcp.Visible = True
    '                    Else
    '                        panelcpdetail.Hidden = True
    '                        windowcp.Visible = False
    '                    End If




    '                End With



    '                'Load Validation Result



    '                cmb_DocTypeIndicator.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_AccountType_Group.StringFieldStyle = "background-color:#FFFFFF"
    '                cmb_AccountCurrency_Group.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_AccountHolderType.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_NameType_Group.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_birthcountrycode_group.StringFieldStyle = "background-color:#FFFFFF"
    '                cmb_nationality1_group.StringFieldStyle = "background-color:#FFFFFF"
    '                cmb_nationality2_group.StringFieldStyle = "background-color:#FFFFFF"
    '                cmb_nationality3_group.StringFieldStyle = "background-color:#FFFFFF"
    '                cmb_nationality4_group.StringFieldStyle = "background-color:#FFFFFF"
    '                cmb_nationality5_group.StringFieldStyle = "background-color:#FFFFFF"
    '                cmb_tinissuedby_group.StringFieldStyle = "background-color:#FFFFFF"
    '                cmb_addresslegaltype_group.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_countrycode_Group.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_controllingpersontype_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_nametype_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_nationality_cp.StringFieldStyle = "background-color:#FFFFFF"
    '                cmb_tinissuedby_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_legaladdresstype_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_countrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_paymenttype.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_paymentcurrencycode.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_org_nametype.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_ResCountryCode.StringFieldStyle = "background-color:#FFE4C4"

    '                'Show window_ Reporting Group
    '                window_reporting_group.Hidden = False
    '                window_reporting_group.Body.ScrollTo(Direction.Top, 0)

    '            Else
    '                Throw New ApplicationException("Reporting Group cannot be loaded")
    '            End If
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub
    '#End Region


    '#End Region
    '#Region "Reporting Payment"

    '    Public Property IDPayment() As Long
    '        Get
    '            Return Session("CRS_International_Detail.IDPayment")
    '        End Get
    '        Set(ByVal value As Long)
    '            Session("CRS_International_Detail.IDPayment") = value
    '        End Set
    '    End Property

    '    Public Property obj_Reporting_Payment_Detail() As CRS_INTERNATIONAL_RG_AR_PAYMENT
    '        Get
    '            Return Session("CRS_International_Detail.obj_Reporting_Payment_Detail")
    '        End Get
    '        Set(ByVal value As CRS_INTERNATIONAL_RG_AR_PAYMENT)
    '            Session("CRS_International_Detail.obj_Reporting_Payment_Detail") = value
    '        End Set
    '    End Property

    '    Protected Sub gc_payment_group(sender As Object, e As DirectEventArgs)
    '        Try
    '            Dim strID As String = e.ExtraParams(0).Value
    '            Dim strAction As String = e.ExtraParams(1).Value

    '            If strID IsNot Nothing Then
    '                If strAction = "Detail" Then
    '                    obj_Reporting_Payment_Detail = CRS_Report_Group_Class.list_Reporting_RGARPayment.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID = strID)
    '                    Load_Window_Report_Payment(strAction)
    '                End If
    '            Else
    '                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
    '            End If
    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub btn_Payment_Cancel_Click()
    '        Try
    '            'Hide window pop up
    '            Window_Payment.Hidden = True
    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub Bind_Reporting_Payment()
    '        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Group_Class.list_Reporting_RGARPayment)


    '        gp_payment.GetStore().DataSource = objtable
    '        gp_payment.GetStore().DataBind()

    '    End Sub


    '    Protected Sub Clean_Window_Payment()
    '        'Clean fields
    '        txt_paymentamount.Value = Nothing
    '        cmb_paymentcurrencycode.SetTextValue("")
    '        cmb_paymenttype.SetTextValue("")

    '    End Sub

    '    Protected Sub Load_Window_Report_Payment(strAction As String)
    '        'Clean window pop up
    '        Clean_Window_Payment()

    '        If obj_Reporting_Payment_Detail IsNot Nothing Then
    '            'Populate fields
    '            With obj_Reporting_Payment_Detail
    '                txt_paymentamount.Value = .PaymentAmnt

    '                If .Payment_Type IsNot Nothing Then
    '                    Dim objPaymentType = CRS_Report_BLL.GetPaymentTypeByCode(.Payment_Type)
    '                    If Not objPaymentType Is Nothing Then
    '                        cmb_paymenttype.SetTextWithTextValue(objPaymentType.FK_CRS_PAYMENT_TYPE_CODE, objPaymentType.PAYMENT_TYPE_NAME)
    '                    End If
    '                End If


    '                If .Payment_PaymentAmnt_currCode IsNot Nothing Then
    '                    Dim objPaymentCurrency = CRS_Report_BLL.GetPaymentCurrencyByCode(.Payment_PaymentAmnt_currCode)
    '                    If Not objPaymentCurrency Is Nothing Then
    '                        cmb_paymentcurrencycode.SetTextWithTextValue(objPaymentCurrency.FK_CURRENCY_CODE, objPaymentCurrency.CURRENCY_NAME)
    '                    End If
    '                End If
    '            End With
    '        End If

    '        'Set fields' ReadOnly
    '        If strAction = "Edit" Then
    '            txt_paymentamount.ReadOnly = False
    '            cmb_paymenttype.IsReadOnly = False
    '            cmb_paymentcurrencycode.IsReadOnly = False
    '        Else
    '            txt_paymentamount.ReadOnly = True
    '            cmb_paymenttype.IsReadOnly = True
    '            cmb_paymentcurrencycode.IsReadOnly = True

    '        End If

    '        cmb_paymenttype.StringFieldStyle = "background-color:#FFE4C4"
    '        cmb_paymentcurrencycode.StringFieldStyle = "background-color:#FFE4C4"

    '        'Bind ATM
    '        IDPayment = obj_Reporting_Payment_Detail.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID

    '        'Show window pop up
    '        Window_Payment.Title = "Reporting Payment - " & strAction
    '        Window_Payment.Hidden = False
    '    End Sub
    '#End Region

    '#Region "Controlling Person"

    '    Public Property IDCP() As Long
    '        Get
    '            Return Session("CRS_International_Add.IDCP")
    '        End Get
    '        Set(ByVal value As Long)
    '            Session("CRS_International_Add.IDCP") = value
    '        End Set
    '    End Property

    '    Public Property obj_Reporting_cp_Edit() As CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON
    '        Get
    '            Return Session("CRS_International_Add.obj_Reporting_cp_Edit")
    '        End Get
    '        Set(ByVal value As CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON)
    '            Session("CRS_International_Add.obj_Reporting_cp_Edit") = value
    '        End Set
    '    End Property

    '    Protected Sub btn_cp_Add_Click()
    '        Try
    '            'Kosongkan object edit & windows pop up
    '            Clean_Window_cp()

    '            'Show window pop up
    '            windowcp.Title = "Reporting Controlling Person - Add"
    '            windowcp.Hidden = False

    '            'Set IDCP to 0
    '            IDCP = 0

    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub gc_cp_group(sender As Object, e As DirectEventArgs)
    '        Try
    '            Dim strID As String = e.ExtraParams(0).Value
    '            Dim strAction As String = e.ExtraParams(1).Value

    '            If strID IsNot Nothing Then
    '                If strAction = "Delete" Then
    '                    Dim objToDelete = CRS_Report_Group_Class.list_Reporting_RGARPerson.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID = strID)
    '                    If objToDelete IsNot Nothing Then
    '                        CRS_Report_Group_Class.list_Reporting_RGARPerson.Remove(objToDelete)
    '                    End If
    '                    Bind_Reporting_cp()

    '                ElseIf strAction = "Edit" Or strAction = "Detail" Then
    '                    obj_Reporting_cp_Edit = CRS_Report_Group_Class.list_Reporting_RGARPerson.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID = strID)
    '                    Load_Window_Report_cp(strAction)
    '                End If
    '            Else
    '                Ext.Net.X.Msg.Alert("Information", "Some Data For This Process Might be Missing, Please Report to Admin OR TRY Again.").Show()
    '            End If
    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub btn_cp_Cancel_Click()
    '        Try
    '            'Hide window pop up
    '            windowcp.Hidden = True
    '        Catch ex As Exception When TypeOf ex Is ApplicationException
    '            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '        Catch ex As Exception
    '            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '        End Try
    '    End Sub

    '    Protected Sub Bind_Reporting_cp()
    '        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Group_Class.list_Reporting_RGARPerson)

    '        gp_cp_Detail.GetStore().DataSource = objtable
    '        gp_cp_Detail.GetStore().DataBind()

    '    End Sub


    '    Protected Sub Clean_Window_cp()
    '        'Controlling Person Information
    '        cmb_restcountrycode_cp.SetTextValue("")
    '        cmb_controllingpersontype_cp.SetTextValue("")
    '        txt_firstname_cp.Value = Nothing
    '        'txt_firstnametype_cp.Value = Nothing
    '        txt_middlename_cp.Value = Nothing
    '        'txt_middlenametype_cp.Value = Nothing
    '        txt_lastname_cp.Value = Nothing
    '        'txt_lastnametype_cp.Value = Nothing
    '        txt_nameprefix_cp.Value = Nothing
    '        txt_generationidentifier_cp.Value = Nothing
    '        txt_namesufix_cp.Value = Nothing
    '        txt_generalsufix_cp.Value = Nothing
    '        cmb_nametype_cp.SetTextValue("")
    '        txt_precedingtitle_cp.Value = Nothing
    '        txt_title_cp.Value = Nothing
    '        DateBirthDate_cp.Value = Nothing
    '        cmb_nationality_cp.SetTextValue("")


    '        'TIN Controlling Person Information
    '        txt_tin_cp.Value = Nothing
    '        cmb_tinissuedby_cp.SetTextValue("")

    '        'Address Controlling Person Information
    '        cmb_legaladdresstype_cp.SetTextValue("")
    '        cmb_countrycode_cp.SetTextValue("")
    '        txt_addressfree_cp.Value = Nothing
    '        txt_street_cp.Value = Nothing
    '        txt_buildingidentifier_cp.Value = Nothing
    '        txt_suiteidentifier_cp.Value = Nothing
    '        txt_flooridentifier_cp.Value = Nothing
    '        txt_districtname_cp.Value = Nothing
    '        txt_pob_cp.Value = Nothing
    '        txt_postcode_cp.Value = Nothing
    '        txt_city_cp.Value = Nothing
    '        txt_countrysubentity_cp.Value = Nothing
    '    End Sub


    '    Protected Sub Load_Window_Report_cp(strAction As String)
    '        'Clean window pop up
    '        Clean_Window_cp()

    '        If obj_Reporting_cp_Edit IsNot Nothing Then
    '            'Populate fields
    '            With obj_Reporting_cp_Edit
    '                If Not IsNothing(.ResCountryCode) Then
    '                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .ResCountryCode)
    '                    If drTemp IsNot Nothing Then
    '                        cmb_restcountrycode_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                    End If
    '                End If

    '                If Not IsNothing(.CtrlgPersonType) Then
    '                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_CP_TYPE", "FK_CRS_CONTROLLING_PERSON_TYPE_CODE", .CtrlgPersonType)
    '                    If drTemp IsNot Nothing Then
    '                        cmb_controllingpersontype_cp.SetTextWithTextValue(drTemp("FK_CRS_CONTROLLING_PERSON_TYPE_CODE"), drTemp("CONTROLLING_PERSON_TYPE_NAME"))
    '                    End If
    '                End If
    '                txt_firstname_cp.Value = .FirstName
    '                'txt_firstnametype_cp.Value = .FirstNameType
    '                txt_middlename_cp.Value = .MiddleName
    '                'txt_middlenametype_cp.Value = .MiddleNameType
    '                txt_lastname_cp.Value = .LastName
    '                'txt_lastnametype_cp.Value = .LastNameType
    '                txt_nameprefix_cp.Value = .Name_NamePrefix
    '                txt_generationidentifier_cp.Value = .Name_GenerationIdentifier
    '                txt_namesufix_cp.Value = .Name_Suffix
    '                txt_generalsufix_cp.Value = .Name_GeneralSuffix
    '                If Not IsNothing(.NameType) Then
    '                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_NAME_TYPE", "FK_CRS_NAME_TYPE_CODE", .NameType)
    '                    If drTemp IsNot Nothing Then
    '                        cmb_nametype_cp.SetTextWithTextValue(drTemp("FK_CRS_NAME_TYPE_CODE"), drTemp("NAME_TYPE_NAME"))
    '                    End If
    '                End If
    '                txt_precedingtitle_cp.Value = .Name_PrecedingTitle
    '                txt_title_cp.Value = .Name_Title
    '                DateBirthDate_cp.Value = .BirthDate
    '                If Not IsNothing(.Nationality) Then
    '                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .Nationality)
    '                    If drTemp IsNot Nothing Then
    '                        cmb_nationality_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                    End If
    '                End If


    '                'TIN Controlling Person Information
    '                txt_tin_cp.Value = .TIN
    '                If Not IsNothing(.IssuedBy) Then
    '                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .IssuedBy)
    '                    If drTemp IsNot Nothing Then
    '                        cmb_tinissuedby_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                    End If
    '                End If

    '                'Address Controlling Person Information
    '                If Not IsNothing(.legalAddressType) Then
    '                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_LEGAL_ADDRESS", "FK_CRS_LEGAL_ADDRESS_TYPE_CODE", .legalAddressType)
    '                    If drTemp IsNot Nothing Then
    '                        cmb_legaladdresstype_cp.SetTextWithTextValue(drTemp("FK_CRS_LEGAL_ADDRESS_TYPE_CODE"), drTemp("LEGAL_ADDRESS_TYPE_NAME"))
    '                    End If
    '                End If
    '                If Not IsNothing(.CountryCode) Then
    '                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .CountryCode)
    '                    If drTemp IsNot Nothing Then
    '                        cmb_countrycode_cp.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                    End If
    '                End If
    '                txt_addressfree_cp.Value = .AddressFree
    '                txt_street_cp.Value = .AddressFix_Street
    '                If .AddressFix_Street IsNot Nothing Then
    '                    txt_city_cp.AllowBlank = False
    '                    txt_city_cp.FieldStyle = "background-color:#FFE4C4"
    '                End If
    '                txt_buildingidentifier_cp.Value = .AddressFix_BuildingIdentifier
    '                txt_suiteidentifier_cp.Value = .AddressFix_SuiteIdentifier
    '                txt_flooridentifier_cp.Value = .AddressFix_FloorIdentifier
    '                txt_districtname_cp.Value = .AddressFix_DistrictName
    '                txt_pob_cp.Value = .AddressFix_POB
    '                txt_postcode_cp.Value = .AddressFix_PostCode
    '                txt_city_cp.Value = .AddressFix_City
    '                txt_countrysubentity_cp.Value = .AddressFix_CountrySubentity
    '                txt_birthcity_group.Value = .BirthInfo_City
    '                txt_birthcitysubentity_group.Value = .BirthInfo_SubEntity
    '                If Not IsNothing(.BirthInfo_FormerCountryName) Then
    '                    drTemp = CRS_Global_BLL.getDataRowByID("vw_CRS_INTERNATIONAL_COUNTRY", "FK_COUNTRY_CODE", .BirthInfo_FormerCountryName)
    '                    If drTemp IsNot Nothing Then
    '                        cmb_birthcountrycode_group.SetTextWithTextValue(drTemp("FK_COUNTRY_CODE"), drTemp("CONTRY_NAME"))
    '                    End If
    '                End If
    '            End With

    '        End If

    '        'Set fields' ReadOnly
    '        If strAction = "Edit" Then
    '            SetReadOnlyWindowReportingCP(False)
    '        Else
    '            SetReadOnlyWindowReportingCP(True)
    '        End If

    '        'Bind ATM
    '        IDCP = obj_Reporting_cp_Edit.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID

    '        'Show window pop up
    '        windowcp.Title = "Reporting Controlling Person - " & strAction
    '        windowcp.Hidden = False
    '    End Sub

    '    Protected Sub SetReadOnlyWindowReportingCP(isReadOnly As Boolean)
    '        Try


    '            'Controlling Person Information
    '            cmb_restcountrycode_cp.IsReadOnly = isReadOnly
    '            cmb_controllingpersontype_cp.IsReadOnly = isReadOnly
    '            txt_firstname_cp.ReadOnly = isReadOnly
    '            'txt_firstnametype_cp.ReadOnly = isReadOnly
    '            txt_middlename_cp.ReadOnly = isReadOnly
    '            'txt_middlenametype_cp.ReadOnly = isReadOnly
    '            txt_lastname_cp.ReadOnly = isReadOnly
    '            'txt_lastnametype_cp.ReadOnly = isReadOnly
    '            txt_nameprefix_cp.ReadOnly = isReadOnly
    '            txt_generationidentifier_cp.ReadOnly = isReadOnly
    '            txt_namesufix_cp.ReadOnly = isReadOnly
    '            txt_generalsufix_cp.ReadOnly = isReadOnly
    '            cmb_nametype_cp.IsReadOnly = isReadOnly
    '            txt_precedingtitle_cp.ReadOnly = isReadOnly
    '            txt_title_cp.ReadOnly = isReadOnly
    '            DateBirthDate_cp.ReadOnly = isReadOnly
    '            txt_birthcity_group.ReadOnly = isReadOnly
    '            txt_birthcitysubentity_group.ReadOnly = isReadOnly
    '            cmb_birthcountrycode_cp.IsReadOnly = isReadOnly
    '            cmb_nationality_cp.IsReadOnly = isReadOnly

    '            'TIN Controlling Person Information
    '            txt_tin_cp.ReadOnly = isReadOnly
    '            cmb_tinissuedby_cp.IsReadOnly = isReadOnly

    '            'Address Controlling Person Information
    '            cmb_legaladdresstype_cp.IsReadOnly = isReadOnly
    '            cmb_countrycode_cp.IsReadOnly = isReadOnly
    '            txt_addressfree_cp.ReadOnly = isReadOnly
    '            txt_street_cp.ReadOnly = isReadOnly
    '            txt_buildingidentifier_cp.ReadOnly = isReadOnly
    '            txt_suiteidentifier_cp.ReadOnly = isReadOnly
    '            txt_flooridentifier_cp.ReadOnly = isReadOnly
    '            txt_districtname_cp.ReadOnly = isReadOnly
    '            txt_pob_cp.ReadOnly = isReadOnly
    '            txt_postcode_cp.ReadOnly = isReadOnly
    '            txt_city_cp.ReadOnly = isReadOnly
    '            txt_countrysubentity_cp.ReadOnly = isReadOnly


    '            If isReadOnly = False Then
    '                cmb_controllingpersontype_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_nametype_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_nationality_cp.StringFieldStyle = "background-color:#FFFFFF"
    '                cmb_tinissuedby_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_legaladdresstype_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_countrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_restcountrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_birthcountrycode_cp.StringFieldStyle = "background-color:#FFFFFF"
    '            Else
    '                cmb_controllingpersontype_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_nametype_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_nationality_cp.StringFieldStyle = "background-color:#FFFFFF"
    '                cmb_tinissuedby_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_legaladdresstype_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_countrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_restcountrycode_cp.StringFieldStyle = "background-color:#FFE4C4"
    '                cmb_birthcountrycode_cp.StringFieldStyle = "background-color:#FFFFFF"
    '            End If
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub
    '#End Region

    Protected Sub btn_Confirmation_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim strEncryptedModuleID = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & strEncryptedModuleID)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & strEncryptedModuleID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

End Class
