﻿Imports Ext.Net
Imports NawaBLL
Imports OfficeOpenXml
Partial Class CRS_CRS_International_CRS_International_Approval
    Inherits Parent
    Public objFormModuleApproval As NawaBLL.FormModuleView

    Public Property ObjModule As NawaDAL.Module
        Get
            Return Session("CRS_International_Approval.ObjModule")
        End Get
        Set(ByVal value As NawaDAL.Module)
            Session("CRS_International_Approval.ObjModule") = value
        End Set
    End Property
    Public Property strWhereClause() As String
        Get
            Return Session("CRS_International_Approval.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_Approval.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("CRS_International_Approval.strSort")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_Approval.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("CRS_International_Approval.indexStart")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_Approval.indexStart") = value
        End Set
    End Property
    Public Property QueryTable() As String
        Get
            Return Session("CRS_International_Approval.Table")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_Approval.Table") = value
        End Set
    End Property
    Public Property QueryField() As String
        Get
            Return Session("CRS_International_Approval.Field")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_Approval.Field") = value
        End Set
    End Property

    Private Sub Parameterview_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleApproval = New NawaBLL.FormModuleView(Me.GridpanelView, Me.Button1)
    End Sub

    Private Sub CRS_International_Approval_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try


            Dim Moduleid As String = Request.Params("ModuleID")
                Dim intModuleid As Integer

                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.Approval) Then
                    Dim strIDCode As String = 1
                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If

                objFormModuleApproval.ModuleID = objmodule.PK_Module_ID
                objFormModuleApproval.ModuleName = objmodule.ModuleName

                objFormModuleApproval.AddField("PK_CRS_INTERNATIONAL_REPORT_HEADER_ID", "Report ID", 1, True, True, NawaBLL.Common.MFieldType.BIGIDENTITY)
                objFormModuleApproval.AddField("TransmittingCountry", "Transmitting Country", 2, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("ReceivingCountry", "Receiving Country", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("MessageType", "Message Type", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("StatusName", "Status", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("AGING", "Aging", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("Warning", "Warning", 7, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("MessageRefID", "Message Ref ID", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("MessageTypeIndic", "Message Type Indic", 9, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("CorrMessageRefID", "Corr Message Ref ID", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("ReportingPeriod", "Reporting Period", 11, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                objFormModuleApproval.AddField("Timestamp", "Timestamp", 12, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                objFormModuleApproval.AddField("isvalid", "Valid?", 13, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("ErrorMessage", "Error Message", 14, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("createdby", "Created By", 15, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleApproval.AddField("createddate", "Created Date", 16, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                objFormModuleApproval.AddField("LastUpdateDate", "LastUpdateDate", 17, False, True, NawaBLL.Common.MFieldType.VARCHARValue)

                objFormModuleApproval.SettingFormView()





            'Remove default form View buttons
            Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
            GridpanelView.ColumnModel.Columns.Remove(objcommandcol)

            'Add button Approval Detail
            Using objCustomCommandColumn As New CommandColumn
                objCustomCommandColumn.ID = "columncrudapproval"
                objCustomCommandColumn.ClientIDMode = Web.UI.ClientIDMode.Static
                objCustomCommandColumn.Width = 100

                Dim extparam As New Ext.Net.Parameter
                extparam.Name = "unikkey"
                extparam.Value = "record.data.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID"
                extparam.Mode = ParameterMode.Raw

                Dim extparamcommand As New Ext.Net.Parameter
                extparamcommand.Name = "command"
                extparamcommand.Value = "command"
                extparamcommand.Mode = ParameterMode.Raw

                objCustomCommandColumn.DirectEvents.Command.ExtraParams.Add(extparam)
                objCustomCommandColumn.DirectEvents.Command.ExtraParams.Add(extparamcommand)
                ''objCustomCommandColumn.DirectEvents.Command.Confirmation.Title = "Delete"
                ''objCustomCommandColumn.DirectEvents.Command.Confirmation.BeforeConfirm = "if (command='Edit') return false;"
                ''objCustomCommandColumn.DirectEvents.Command.Confirmation.ConfirmRequest = True
                ''objCustomCommandColumn.DirectEvents.Command.Confirmation.Message = "Are You Sure To Delete This Record ?"

                Dim gcApprovalDetail As New GridCommand
                gcApprovalDetail.CommandName = "ApprovalDetail"
                gcApprovalDetail.Icon = Icon.ApplicationForm
                gcApprovalDetail.Text = "Detail"
                gcApprovalDetail.ToolTip.Text = "Detail"
                objCustomCommandColumn.Commands.Add(gcApprovalDetail)

                AddHandler objCustomCommandColumn.DirectEvents.Command.Event, AddressOf GridCommandCustom

                GridpanelView.ColumnModel.Columns.Insert(1, objCustomCommandColumn)


            End Using

            GridpanelView.Title = objmodule.ModuleLabel + " - Request Approval"

            If Not Ext.Net.X.IsAjaxRequest Then
                Dim objcommandcolumnrefid As Ext.Net.Column = GridpanelView.ColumnModel.Columns.Find(Function(x) x.DataIndex = "MessageRefID")
                objcommandcolumnrefid.Width = 250

                Dim objcommandcolumnrefidcorr As Ext.Net.Column = GridpanelView.ColumnModel.Columns.Find(Function(x) x.DataIndex = "CorrMessageRefID")
                objcommandcolumnrefidcorr.Width = 250

                cboExportExcel.SelectedItem.Text = "Excel"
                Dim objcommandcolumn As Ext.Net.Column = GridpanelView.ColumnModel.Columns.Find(Function(x) x.DataIndex = "Warning")
                objcommandcolumn.Width = 200

                Dim objcommandcolumn2 As Ext.Net.Column = GridpanelView.ColumnModel.Columns.Find(Function(x) x.DataIndex = "ErrorMessage")
                objcommandcolumn2.Width = 700

            End If



        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleApproval.GetWhereClauseHeader(e)

            strfilter = strfilter.Replace("Active", objFormModuleApproval.ModuleName & ".Active")
            If String.IsNullOrEmpty(strfilter) Then
                strfilter += "StatusName = 'Waiting For Approval'"
            Else
                strfilter += " AND StatusName = 'Waiting For Approval'"
            End If

            Dim strsort As String = "LastUpdateDate DESC"
            For Each item As DataSorter In e.Sort
                'strsort += ", " & item.Property & " " & item.Direction.ToString
                strsort = item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter
            'Begin Penambahan Advanced Filter
            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            End If
            'END Penambahan Advanced Filter

            Me.strOrder = strsort

            'QueryTable = "vw_SIPENDAR_PROFILE"
            'QueryField = "*"
            'Dim DataPaging As DataTable = SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)

            Dim DataPaging As Data.DataTable = objFormModuleApproval.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)

            'Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = DLL.SQLHelper.ExecuteTabelPaging("CustomerInformation_WebTempTable", "CIFNo, Name, DateOfBirth, AccountOwnerId, OpeningDate, WorkingUnitName, IsCustomerInList", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleApproval.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleApproval.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CRS Inter Report Header")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=CRS_INTERNATIONAL_Report_Header.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleApproval.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleApproval.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=CRS_INTERNATIONAL_Report_Header.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleApproval.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleApproval.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CRS Inter Report Header")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=CRS_INTERNATIONAL_Report_Header.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using

                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleApproval.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleApproval.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=CRS_INTERNATIONAL_Report_Header.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    'Custom CommandColumn for Approval Detail
    Protected Sub GridCommandCustom(sender As Object, e As Ext.Net.DirectEventArgs)
        Try

            Dim ModuleID As String = Request.Params("ModuleID")
            Dim intModuleID As Integer

            intModuleID = NawaBLL.Common.DecryptQueryString(ModuleID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim ObjModule = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleID)

            Dim ID As String = e.ExtraParams(0).Value
            ID = NawaBLL.Common.EncryptQueryString(ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

            If e.ExtraParams(1).Value = "ApprovalDetail" Then
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApprovalDetail & "?ID={0}&ModuleID={1}", ID, ModuleID), "Loading...")
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
End Class
