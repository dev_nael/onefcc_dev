﻿Imports System.IO
Imports Elmah
Imports Ext
Imports NawaBLL
Imports NawaDevBLL
Imports NawaDevDAL
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Entity
Imports CRSBLL
Imports CRSDAL
Imports System.Reflection
Partial Class CRS_CRS_International_CRS_International_GenerateDetail
    Inherits ParentPage
    Dim drTemp As DataRow
    Dim strSQL As String
    Public objFormModuleAdd As NawaBLL.FormModuleAdd

    Public Property Filetodownload() As String
        Get
            Return Session("CRS_International_GenerateDetail.Filetodownload")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_GenerateDetail.Filetodownload") = value
        End Set
    End Property
    Public Structure Years
        Dim Id As Integer
        Dim year As String
    End Structure
    Public Structure Countrys
        Dim Id As Integer
        Dim formatdata As String
    End Structure

    Public Structure Formats
        Dim Id As Integer
        Dim format As String
    End Structure


    Public Property IDUnik() As Long
        Get
            Return Session("CRS_International_Detail.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("CRS_International_Detail.IDUnik") = value
        End Set
    End Property


    Public Property CRS_Report_Class() As CRSBLL.CRS_Report_BLL.CRS_Reporting_Class
        Get
            Return Session("CRS_International_GenerateDetail.CRS_Report_Class")
        End Get
        Set(ByVal value As CRSBLL.CRS_Report_BLL.CRS_Reporting_Class)
            Session("CRS_International_GenerateDetail.CRS_Report_Class") = value
        End Set
    End Property

    Public Property ObjGeneratedCRSInternational() As CRS_International_Generate_Bulk
        Get
            Return Session("CRS_International_GenerateDetail.ObjGeneratedCRSInternational")
        End Get
        Set(ByVal value As CRS_International_Generate_Bulk)
            Session("CRS_International_GenerateDetail.ObjGeneratedCRSInternational") = value
        End Set
    End Property

    Public Property CRS_GenerateValid() As List(Of CRS_International_Generate_Bulk_CountryValid)
        Get
            Return Session("CRS_International_GenerateDetail.CRS_GenerateValid")
        End Get
        Set(ByVal value As List(Of CRS_International_Generate_Bulk_CountryValid))
            Session("CRS_International_GenerateDetail.CRS_GenerateValid") = value
        End Set
    End Property

    Public Property CRS_GenerateNotValid() As List(Of CRS_International_Generate_Bulk_CountryInValid)
        Get
            Return Session("CRS_International_GenerateDetail.CRS_GenerateNotValid")
        End Get
        Set(ByVal value As List(Of CRS_International_Generate_Bulk_CountryInValid))
            Session("CRS_International_GenerateDetail.CRS_GenerateNotValid") = value
        End Set
    End Property

    Public Property CRS_Report_Group_Class() As CRSBLL.CRS_Report_BLL.CRS_Reporting_Group_Class
        Get
            Return Session("CRS_International_GenerateDetail.CRS_Report_Group_Class")
        End Get
        Set(ByVal value As CRSBLL.CRS_Report_BLL.CRS_Reporting_Group_Class)
            Session("CRS_International_GenerateDetail.CRS_Report_Group_Class") = value
        End Set
    End Property

    Private Sub CRS_International_GenerateDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If
                ClearSession()
                LoadData()
                TaskManager1.StartTask("refreshScreeningStatus") '' add 07-Jan-2022
                'SetCommandColumnLocation()
                'btnSave.Hidden = True
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub ClearSession()
        'ObjModule = Nothing
        ObjGeneratedCRSInternational = Nothing

        '5-Aug-2021 Adi : kosongkan agar tidak ke-download saat invalid
        Filetodownload = ""
        Session("TotalRow") = 0

        '' Add 31-Dec-2021 Felix
        display_totalxmlgenerated.Text = ""
        display_totalxmlvalidate.Text = ""
        display_statusAPI.Text = ""
        display_errormassageapi.Text = ""
    End Sub
    Protected Sub gp_country_Gridcommand(sender As Object, e As Ext.Net.DirectEventArgs)
        Try
            Dim id As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Detail" Then
                'Clear_WindowReport()
                IDUnik = id
                'windowreportheader.Hidden = False
                'SetReportHeader(id)
                'CRS_Report_Class = CRS_Report_BLL.GetCRSINTReportClassByID(id)
                ''Load Reporting Group By ID
                'Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(CRS_Report_Class.list_CRS_Reporting_Group)


                'gp_Report_Group.GetStore().DataSource = objtable
                'gp_Report_Group.GetStore().DataBind()
            Else

            End If
        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadData()
        Try
            Session("TotalRow") = 0
            CRS_GenerateNotValid = New List(Of CRS_International_Generate_Bulk_CountryInValid)
            Using objdb As New CRSEntities
                Dim format As String = ""
                If Session("vw_CRS_International_Generate_Bulk_GenerateXML") = 1 Then
                    format = "XML"
                Else
                    format = "Excel"
                End If
                Dim generateheader As New CRS_International_Generate_Bulk

                With generateheader
                    If Session("Vw_CRS_INTERNATIONAL_GenerateXMLParalel_PK") IsNot Nothing Then
                        Dim idpk As Integer = Session("Vw_CRS_INTERNATIONAL_GenerateXMLParalel_PK")
                        Dim objreport = objdb.CRS_INTERNATIONAL_REPORT_HEADER.Where(Function(x) x.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = idpk).FirstOrDefault
                        .Report_ID = idpk
                        .YearPeriode = objreport.ReportingPeriod.Value.Year.ToString
                        .Format = format
                        .Total_Report_Ready_to_Generate = 1
                        .CreatedDate = DateTime.Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID.ToString
                        objdb.Entry(generateheader).State = Entity.EntityState.Added
                        objdb.SaveChanges()
                        Dim strQuery1 As String = "INSERT INTO CRS_International_Generate_Bulk_CountryValid ("
                        strQuery1 += "FK_Header_Generate_ID, "
                        strQuery1 += "PK_report_ID, "
                        strQuery1 += "Country, "
                        strQuery1 += "Status, "
                        strQuery1 += "CreatedDate, "
                        strQuery1 += "CreatedBy) "
                        strQuery1 += " SELECT "
                        strQuery1 += generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID.ToString
                        strQuery1 += " , "
                        strQuery1 += idpk.ToString
                        strQuery1 += " , "
                        strQuery1 += " ReceivingCountry, "
                        strQuery1 += " Status, "
                        strQuery1 += " getdate()"
                        strQuery1 += ", '"
                        strQuery1 += NawaBLL.Common.SessionCurrentUser.UserID.ToString
                        strQuery1 += "' "
                        strQuery1 += " from crs_international_report_header where PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & idpk.ToString
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery1, Nothing)
                        'Dim strQuery2 As String = "INSERT INTO CRS_International_Generate_Bulk_CountryInValid ("
                        'strQuery2 += "FK_Header_Generate_ID, "
                        'strQuery2 += "PK_report_ID, "
                        'strQuery2 += "Country, "
                        'strQuery2 += "Status, "
                        'strQuery2 += "CreatedDate, "
                        'strQuery2 += "CreatedBy) "
                        'strQuery2 += " SELECT "
                        'strQuery2 += generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID.ToString
                        'strQuery2 += " , "
                        'strQuery2 += idpk.ToString
                        'strQuery2 += " , "
                        'strQuery2 += " ReceivingCountry, "
                        'strQuery2 += " Status, "
                        'strQuery2 += " getdate()"
                        'strQuery2 += ", '"
                        'strQuery2 += NawaBLL.Common.SessionCurrentUser.UserID.ToString
                        'strQuery2 += "' "
                        'strQuery2 += " from crs_international_report_header where messagetypeindic = 'CRS701' and status in(0,4,5,10) and isvalid = 0 and year(reportingperiod) = '" & objreport.ReportingPeriod.Value.Year & "' "
                        'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                        Session("Vw_CRS_INTERNATIONAL_GenerateXMLParalel_PK") = Nothing

                        Dim objreport2 As CRS_International_Generate_Bulk = objdb.CRS_International_Generate_Bulk.Where(Function(x) x.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID = generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID).FirstOrDefault
                        display_format.Value = objreport2.Format
                        display_year.Value = objreport2.YearPeriode
                        display_errormassageapi.Value = objreport2.Error_API
                        display_statusAPI.Value = objreport2.Status_API

                        display_totalxmlgenerated.Value = 0
                        display_totalxmlvalidate.Value = 0
                        ' Update 9 Juni 2022 : Tambah Param untuk grid country valid
                        Dim query1 As String = "select max(a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
                        " max(country.CONTRY_NAME) as Country, a.Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
                        " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
                        " inner join CRS_International_Generate_Bulk_CountryValid cv on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = cv.PK_report_ID " &
                        " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
                         " where cv.PK_report_ID = " & idpk & " and cv.FK_Header_Generate_ID = " & generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID &
                        " Group by country.CONTRY_NAME,a.Status,a.MarkedAsDelete "

                        Dim datacountryvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query1)
                        'Dim datacountrynotvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from CRS_International_Generate_Bulk_CountryInValid where PK_report_ID = " & idpk)

                        datacountryvalid.Columns.Add(New DataColumn("StatusName", GetType(String)))
                        For Each row As DataRow In datacountryvalid.Rows


                            Dim status As Integer = row("Status")


                            Dim displayStatus As String = ""
                            If status = 0 Then
                                displayStatus = "0 - Draft"
                            ElseIf status = 1 Then
                                displayStatus = "1 - Waiting For Generate"
                            ElseIf status = 2 Then
                                displayStatus = "2 - Waiting For Upload Reference No"
                            ElseIf status = 3 Then
                                displayStatus = "3 - Reference No Uploaded"
                            ElseIf status = 4 Then

                                displayStatus = "4 - Waiting For Approval "

                            ElseIf status = 5 Then
                                displayStatus = "5 - Need To Correction"
                            ElseIf status = 6 Then
                                displayStatus = "6 - Waiting Report Generated"
                            ElseIf status = 7 Then
                                displayStatus = "7 - Need Report Correction"
                            ElseIf status = 8 Then
                                displayStatus = "8 - XML Generated"
                            ElseIf status = 9 Then
                                displayStatus = "9 - Excel Generated"
                            ElseIf status = 10 Then
                                displayStatus = "10 - In Progress Editing"

                            End If

                            row("StatusName") = displayStatus


                        Next

                        'For Each row As DataRow In datacountrynotvalid.Rows
                        '    Dim status As Integer = row("Status")


                        '    Dim displayStatus As String = ""
                        '    If status = 0 Then
                        '        displayStatus = "0 - Draft"
                        '    ElseIf status = 1 Then
                        '        displayStatus = "1 - Waiting For Generate"
                        '    ElseIf status = 2 Then
                        '        displayStatus = "2 - Waiting For Upload Reference No"
                        '    ElseIf status = 3 Then
                        '        displayStatus = "3 - Reference No Uploaded"
                        '    ElseIf status = 4 Then

                        '        displayStatus = "4 - Waiting For Approval"

                        '    ElseIf status = 5 Then
                        '        displayStatus = "5 - Need To Correction"
                        '    ElseIf status = 6 Then
                        '        displayStatus = "6 - Waiting Report Generated"
                        '    ElseIf status = 7 Then
                        '        displayStatus = "7 - Need Report Correction"
                        '    ElseIf status = 8 Then
                        '        displayStatus = "8 - XML Generated"
                        '    ElseIf status = 9 Then
                        '        displayStatus = "9 - Excel Generated"
                        '    ElseIf status = 10 Then
                        '        displayStatus = "10 - In Progress Editing"

                        '    End If
                        '    row("Status") = displayStatus
                        'Next
                        Session("JumlahData") = datacountryvalid.Rows.Count
                        Store2.DataSource = datacountryvalid
                        Store2.DataBind()
                        'Store3.DataSource = datacountrynotvalid
                        'Store3.DataBind

                        'gp_country_invalid.Hidden = True
                        ObjGeneratedCRSInternational = objreport2
                        IDUnik = generateheader.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID
                        If objreport2.Filezip IsNot Nothing And objreport2.FilezipName IsNot Nothing Then
                            btnDownloadXML.Hidden = False
                            btnSaveUpload.Disabled = True
                        Else
                            btnDownloadXML.Hidden = True
                            btnSaveUpload.Disabled = False
                        End If
                    Else

                        ObjGeneratedCRSInternational = CRSBLL.GenerateCRSReportBLL.GetGenerateCRSInternationalByID(IDUnik)
                        If ObjGeneratedCRSInternational IsNot Nothing Then
                            'If ObjGeneratedCRSInternational.Report_ID = "Bulk" Then
                            'Else
                            '    gp_country_invalid.Hidden = True
                            'End If
                            'Dim objreport As CRS_International_Generate_Bulk = objdb.CRS_International_Generate_Bulk.Where(Function(x) x.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID = IDUnik).FirstOrDefault
                            'Dim datacountryvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from CRS_International_Generate_Bulk_CountryValid where FK_Header_Generate_ID = " & IDUnik)
                            Dim query1 As String = "select max(a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
                        " max(country.CONTRY_NAME) as Country, a.Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
                        " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
                        " inner join CRS_International_Generate_Bulk_CountryValid cv on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = cv.PK_report_ID " &
                          " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
                         " where cv.FK_Header_Generate_ID = " & IDUnik & "" &
                        " Group by country.CONTRY_NAME,a.Status,a.MarkedAsDelete "
                            Dim datacountryvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query1)
                            'Dim datacountrynotvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from CRS_International_Generate_Bulk_CountryInValid where FK_Header_Generate_ID = " & IDUnik)
                            display_format.Value = ObjGeneratedCRSInternational.Format
                            display_year.Value = ObjGeneratedCRSInternational.YearPeriode
                            display_errormassageapi.Value = ObjGeneratedCRSInternational.Error_API
                            display_statusAPI.Value = ObjGeneratedCRSInternational.Status_API
                            display_totalxmlgenerated.Value = 0
                            display_totalxmlvalidate.Value = 0
                            datacountryvalid.Columns.Add(New DataColumn("StatusName", GetType(String)))
                            For Each row As DataRow In datacountryvalid.Rows


                                Dim status As Integer = row("Status")


                                Dim displayStatus As String = ""
                                If status = 0 Then
                                    displayStatus = "0 - Draft"
                                ElseIf status = 1 Then
                                    displayStatus = "1 - Waiting For Generate"
                                ElseIf status = 2 Then
                                    displayStatus = "2 - Waiting For Upload Reference No"
                                ElseIf status = 3 Then
                                    displayStatus = "3 - Reference No Uploaded"
                                ElseIf status = 4 Then

                                    displayStatus = "4 - Waiting For Approval "

                                ElseIf status = 5 Then
                                    displayStatus = "5 - Need To Correction"
                                ElseIf status = 6 Then
                                    displayStatus = "6 - Waiting Report Generated"
                                ElseIf status = 7 Then
                                    displayStatus = "7 - Need Report Correction"
                                ElseIf status = 8 Then
                                    displayStatus = "8 - XML Generated"
                                ElseIf status = 9 Then
                                    displayStatus = "9 - Excel Generated"
                                ElseIf status = 10 Then
                                    displayStatus = "10 - In Progress Editing"

                                End If

                                row("StatusName") = displayStatus


                            Next
                            'datacountrynotvalid.Columns.Add(New DataColumn("StatusName", GetType(String)))
                            'For Each row As DataRow In datacountrynotvalid.Rows
                            '    Dim status As Integer = row("Status")


                            '    Dim displayStatus As String = ""
                            '    If status = 0 Then
                            '        displayStatus = "0 - Draft"
                            '    ElseIf status = 1 Then
                            '        displayStatus = "1 - Waiting For Generate"
                            '    ElseIf status = 2 Then
                            '        displayStatus = "2 - Waiting For Upload Reference No"
                            '    ElseIf status = 3 Then
                            '        displayStatus = "3 - Reference No Uploaded"
                            '    ElseIf status = 4 Then

                            '        displayStatus = "4 - Waiting For Approval "


                            '    ElseIf status = 5 Then
                            '        displayStatus = "5 - Need To Correction"
                            '    ElseIf status = 6 Then
                            '        displayStatus = "6 - Waiting Report Generated"
                            '    ElseIf status = 7 Then
                            '        displayStatus = "7 - Need Report Correction"
                            '    ElseIf status = 8 Then
                            '        displayStatus = "8 - XML Generated"
                            '    ElseIf status = 9 Then
                            '        displayStatus = "9 - Excel Generated"
                            '    ElseIf status = 10 Then
                            '        displayStatus = "10 - In Progress Editing"

                            '    End If
                            '    row("StatusName") = displayStatus
                            'Next

                            Session("JumlahData") = datacountryvalid.Rows.Count
                            Store2.DataSource = datacountryvalid
                            Store2.DataBind()
                            'Store3.DataSource = datacountrynotvalid
                            'Store3.DataBind()
                        End If
                        Dim objGenerateParalelStatus(0) As SqlParameter
                        objGenerateParalelStatus(0) = New SqlParameter
                        objGenerateParalelStatus(0).ParameterName = "@PK_ListOfGenerate_ID"
                        objGenerateParalelStatus(0).Value = IDUnik

                        Dim dtGenerateParalelStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_International_GET_GenerateParalelStatus", objGenerateParalelStatus)

                        If dtGenerateParalelStatus IsNot Nothing And dtGenerateParalelStatus.Rows.Count > 0 Then
                            For Each row As DataRow In dtGenerateParalelStatus.Rows

                                If row.Item("KodeStatusGenerate") = "4" Or row.Item("KodeStatusGenerate") = "6" Then
                                    display_totalxmlgenerated.Value = 0
                                    display_totalxmlvalidate.Value = row.Item("CounterValidate")
                                Else


                                    display_totalxmlgenerated.Value = row.Item("Counter")
                                    display_totalxmlvalidate.Value = Session("JumlahData")

                                End If
                                'display_totalxmlvalidate.Text = row.Item("CounterValidate")

                                'display_totalxmlgenerated.Text = row.Item("Counter")
                                display_statusAPI.Text = row.Item("StatusGenerate")

                                display_errormassageapi.Text = row.Item("Error_API")

                                If ObjGeneratedCRSInternational.Filezip IsNot Nothing And ObjGeneratedCRSInternational.FilezipName IsNot Nothing Then
                                    btnDownloadXML.Hidden = False
                                    btnSaveUpload.Disabled = True
                                Else
                                    btnDownloadXML.Hidden = True
                                    btnSaveUpload.Disabled = False
                                End If
                            Next
                        End If


                    End If
                End With
            End Using



        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub btnSave_Click(sender As Object, e As DirectEventArgs)
    '    Try

    '        CRS_GenerateValid = New List(Of CRS_International_Generate_Bulk_CountryValid)
    '        CRS_GenerateNotValid = New List(Of CRS_International_Generate_Bulk_CountryInValid)
    '        Using objdb As New CRSEntities
    '            Dim generateheader As New CRS_International_Generate_Bulk
    '            'Dim datacountryvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from crs_international_report_header where messagetypeindic = 'CRS701' and status in(1,2,3) and isvalid = 1 and year(reportingperiod) = '" & cboyear.RawValue & "'")
    '            'Dim datacountrynotvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * from crs_international_report_header where messagetypeindic = 'CRS701' and status in(0,4,5,10) and isvalid = 0 and year(reportingperiod) = '" & cboyear.RawValue & "'")
    '            With generateheader

    '            End With
    '        End Using
    '        fpMain.Hidden = True
    '        lbl_Confirmation.Text = "Data Saved into Database"
    '        pnl_Confirmation.Hidden = False
    '    Catch ex As Exception When TypeOf ex Is ApplicationException
    '        Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
    '    Catch ex As Exception
    '        Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
    '        Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
    '    End Try
    'End Sub


    Protected Sub btnCancelUpload_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim Moduleid As String = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID={0}", Moduleid), "Loading...")
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub CRS_International_GenerateDetail_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        ActionType = NawaBLL.Common.ModuleActionEnum.Insert
    End Sub

    Private Sub CRS_International_GenerateDetail_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleAdd = New NawaBLL.FormModuleAdd(fpMain, pnl_Confirmation, lbl_Confirmation)
    End Sub

    Protected Sub btn_Confirmation_Click(sender As Object, e As DirectEventArgs)
        Try
            Dim strEncryptedModuleID = NawaBLL.Common.EncryptQueryString(ObjModule.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & strEncryptedModuleID)
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & strEncryptedModuleID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()

        End Try
    End Sub

    Protected Sub StoreValidation_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            StoreView.PageSize = SystemParameterBLL.GetPageSize
            Dim intStart As Integer = e.Start
            Dim intTotalRowCount As Long = 0

            Dim intLimit As Int16 = e.Limit
            Dim strfilter As String = Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)
            Dim strsort As String = ""

            For Each item As DataSorter In e.Sort
                strsort += item.Property & " " & item.Direction.ToString
            Next


            'Dim strQuerySchema As String = "Select * from (Select gavsxr.UnikReference, gavsxr.ErrorMessage FROM goAML_ValidateSchemaXSD_Report As gavsxr WHERE gavsxr.TransactionDate='" & Convert.ToDateTime(ObjGeneratedgoAML.Transaction_Date).ToString("yyyy-MM-dd") & "' AND gavsxr.Jenis_Laporan='" & ObjGeneratedgoAML.Jenis_Laporan & "' AND gavsxr.LastUpdateDate='" & Convert.ToDateTime(ObjGeneratedgoAML.Last_Update_Date).ToString("yyyy-MM-dd") & "')xx "
            '' Pakai DateDiff
            Dim strQuerySchema As String = "select * from (SELECT gavsxr.UnikReference,gavsxr.ErrorMessage FROM CRS_International_ValidateSchemaXSD_Report AS gavsxr WHERE gavsxr.FK_CRS_INTERNATIONAL_GENERATE_BULK_ID='" & IDUnik.ToString & "' )xx"
            If strfilter.Length > 0 Then
                strQuerySchema &= " where " & strfilter
            End If
            Dim objListParam(3) As SqlParameter
            objListParam(0) = New SqlParameter
            objListParam(0).ParameterName = "@querydata"
            objListParam(0).SqlDbType = SqlDbType.VarChar
            objListParam(0).Value = strQuerySchema
            objListParam(1) = New SqlParameter
            objListParam(1).ParameterName = "@orderby"
            objListParam(1).SqlDbType = SqlDbType.VarChar
            objListParam(1).Value = strsort
            objListParam(2) = New SqlParameter
            objListParam(2).ParameterName = "@PageNum"
            objListParam(2).SqlDbType = SqlDbType.Int
            objListParam(2).Value = intStart
            objListParam(3) = New SqlParameter
            objListParam(3).ParameterName = "@PageSize"
            objListParam(3).SqlDbType = SqlDbType.Int
            objListParam(3).Value = intLimit
            'query hanya ambil 1 record untuk simpen schemanya saja(pagesize=1) biar enteng
            Dim DataPaging As DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew(strQuerySchema, strsort, intStart, intLimit, intTotalRowCount)
            e.Total = intTotalRowCount
            Session("TotalRow") = intTotalRowCount
            GridPanelValidation.GetStore.DataSource = DataPaging
            GridPanelValidation.GetStore.DataBind()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BtnValidate_DirectClick(sender As Object, e As EventArgs)
        Try
            If Not ObjGeneratedCRSInternational Is Nothing Then

                Dim objGenerateParalelStatus(0) As SqlParameter
                objGenerateParalelStatus(0) = New SqlParameter
                objGenerateParalelStatus(0).ParameterName = "@PK_ListOfGenerate_ID"
                objGenerateParalelStatus(0).Value = IDUnik

                Dim dtGenerateParalelStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_International_GET_GenerateParalelStatus", objGenerateParalelStatus)

                If dtGenerateParalelStatus IsNot Nothing And dtGenerateParalelStatus.Rows.Count > 0 Then
                    display_totalxmlgenerated.Text = 0
                    For Each row As DataRow In dtGenerateParalelStatus.Rows
                        display_totalxmlvalidate.Text = row.Item("CounterValidate")

                        'display_totalxmlgenerated.Text = row.Item("Counter")

                        display_statusAPI.Text = row.Item("StatusGenerate")

                        display_errormassageapi.Text = row.Item("Error_API")

                        If row.Item("KodeStatusGenerate") <> "1" And row.Item("KodeStatusGenerate") <> "4" And row.Item("KodeStatusGenerate") <> "8" Then
                            Dim objParam(1) As SqlParameter
                            objParam(0) = New SqlParameter
                            objParam(0).ParameterName = "@PK_ListOfGenerate_ID"
                            objParam(0).Value = IDUnik
                            objParam(0).DbType = SqlDbType.BigInt

                            objParam(1) = New SqlParameter
                            objParam(1).ParameterName = "@User_ID"
                            objParam(1).Value = Common.SessionCurrentUser.UserID
                            objParam(1).DbType = SqlDbType.VarChar

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_International_VALIDATE_XML_SaveEOD", objParam)

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update CRS_International_Generate_Bulk " &
                                " set Status_API = 4 " &
                                " ,LastUpdateDate = getdate() " &
                            " where PK_CRS_INTERNATIONAL_GENERATE_BULK_ID =  '" & IDUnik & "'", Nothing)

                            display_statusAPI.Text = "Process Validate XML"
                        ElseIf row.Item("KodeStatusGenerate") = "4" Then
                            Throw New Exception("Process Validate XML by API is in progress. Please check again later.")
                        ElseIf row.Item("KodeStatusGenerate") = "8" Then
                            Throw New Exception("Process Generate Excel by API is in progress. Please check again later.")
                        ElseIf row.Item("KodeStatusGenerate") = "1" Then
                            Throw New Exception("Process Generate XML by API is in progress. Please check again later.")

                        End If
                    Next
                Else
                    Dim objParam(1) As SqlParameter
                    objParam(0) = New SqlParameter
                    objParam(0).ParameterName = "@PK_ListOfGenerate_ID"
                    objParam(0).Value = IDUnik
                    objParam(0).DbType = SqlDbType.BigInt

                    objParam(1) = New SqlParameter
                    objParam(1).ParameterName = "@User_ID"
                    objParam(1).Value = Common.SessionCurrentUser.UserID
                    objParam(1).DbType = SqlDbType.VarChar

                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_International_VALIDATE_XML_SaveEOD", objParam)

                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update CRS_International_Generate_Bulk " &
                                " set Status_API = 4 " &
                                " ,LastUpdateDate = getdate() " &
                            " where PK_CRS_INTERNATIONAL_GENERATE_BULK_ID =  '" & IDUnik & "'", Nothing)
                    display_statusAPI.Text = "Process Validate XML"
                End If
                '' End 31-Dec-2021


                LoadData()
                StoreView.Reload()
            End If
            '' Edit 31-Dec-2021
            'NawaDevBLL.GeneratePPATKBLL.ValidateSchema(ObjGeneratedgoAML.Transaction_Date, ObjGeneratedgoAML.Jenis_Laporan, ObjGeneratedgoAML.Last_Update_Date)



        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub BtnSave_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)


        '' Added by Felix 27 Aug 2020
        'Dim isHarusValid As String = getParameterGlobalByPK(12) '' Download harus Valid XSD
        '' End of Felix

        Try
            If display_totalxmlvalidate.Text = "0" Then
                'Throw New Exception("There is not data valid")
                Throw New Exception("Can't generate XML if there is still invalid data.")
            End If

            If ObjGeneratedCRSInternational.IsAlreadyValidateSchema Is Nothing And ObjGeneratedCRSInternational.Format = "XML" Then
                Throw New Exception("Please Validate Schema first before Generate XML")
            ElseIf ObjGeneratedCRSInternational.IsAlreadyValidateSchema Is Nothing And ObjGeneratedCRSInternational.Format = "Excel" Then
                Throw New Exception("Please Validate Schema first before Generate XML")
            Else
                If ObjGeneratedCRSInternational.Valid_XSD IsNot Nothing Then
                    If Convert.ToString(ObjGeneratedCRSInternational.IsAlreadyValidateSchema).ToLower = "false" Or Convert.ToString(ObjGeneratedCRSInternational.Valid_XSD).ToLower = "" Then
                        Throw New Exception("Please Validate Schema first before Generate XML")
                    End If
                End If

            End If

            If ObjGeneratedCRSInternational.Valid_XSD IsNot Nothing Then
                If (Convert.ToString(ObjGeneratedCRSInternational.Valid_XSD).ToLower = "false" Or Convert.ToString(ObjGeneratedCRSInternational.Valid_XSD).ToLower = "") Then
                    Throw New Exception("There is Validate Schema result Fail. Please fix the fail result first before generate xml")
                End If
            End If
            ''If Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "false" Or Convert.ToString(ObjGeneratedgoAML.ValidateSchemaResultValid).ToLower = "" Then '' Edited By Felix 27 Aug 2020


            ''5-Aug-2021 Adi : Menambahkan validasi jika masih ada report yang akan di generate statusnya 4 (Waiting for Approval) atau 5 (Need Correction)
            'Using objdb As New NawaDatadevEntities
            '    Dim objCekApproval = objdb.goAML_Report.Where(Function(x) x.Report_Code = ObjGeneratedgoAML.Jenis_Laporan And DbFunctions.TruncateTime(x.Transaction_Date) = DbFunctions.TruncateTime(ObjGeneratedgoAML.Transaction_Date) And
            '                                                    DbFunctions.TruncateTime(x.LastUpdateDate) = DbFunctions.TruncateTime(ObjGeneratedgoAML.Last_Update_Date) And (x.Status = 4 Or x.Status = 5)).ToList
            '    If objCekApproval IsNot Nothing AndAlso objCekApproval.Count > 0 Then
            '        Throw New Exception("Can't generate XML. There is still " & objCekApproval.Count & " invalid reports or are in Approval.")
            '    End If
            'End Using
            'End of 5-Aug-2021 Adi : Menambahkan validasi jika masih ada report yang akan di generate statusnya 4 (Waiting for Approval) atau 5 (Need Correction)


            Dim path As String = Server.MapPath("~\goaml\FolderExport\")
            '  Dim path As String = SystemParameterBLL.GetSystemParameterByPk(9017).SettingValue

            Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
            If Not Directory.Exists(Dirpath) Then
                Directory.CreateDirectory(Dirpath)
            End If

            '' Edit 31-Dec-2021
            'NawaDevBLL.GeneratePPATKBLL.ValidateSchema(ObjGeneratedgoAML.Transaction_Date, ObjGeneratedgoAML.Jenis_Laporan, ObjGeneratedgoAML.Last_Update_Date)

            Dim objGenerateParalelStatus(0) As SqlParameter
            objGenerateParalelStatus(0) = New SqlParameter
            objGenerateParalelStatus(0).ParameterName = "@PK_ListOfGenerate_ID"
            objGenerateParalelStatus(0).Value = IDUnik

            Dim dtGenerateParalelStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_International_GET_GenerateParalelStatus", objGenerateParalelStatus)

            If dtGenerateParalelStatus IsNot Nothing And dtGenerateParalelStatus.Rows.Count > 0 Then
                Dim query1 As String = "select max(a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as PK_CRS_INTERNATIONAL_REPORT_HEADER_ID,a.MarkedAsDelete, " &
                        " max(country.CONTRY_NAME) as Country, a.Status, count(b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID) as TotalAccount " &
                        " From CRS_INTERNATIONAL_REPORT_HEADER a left join CRS_INTERNATIONAL_REPORTING_GROUP b on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = b.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID " &
                        " inner join CRS_International_Generate_Bulk_CountryValid cv on a.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = cv.PK_report_ID " &
                          " LEFT JOIN vw_CRS_INTERNATIONAL_COUNTRY as country on a.ReceivingCountry = country.FK_COUNTRY_CODE " &
                         " where cv.FK_Header_Generate_ID = " & IDUnik & "" &
                        " Group by country.CONTRY_NAME,a.Status,a.MarkedAsDelete "
                Dim datacountryvalid As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query1)
                display_totalxmlgenerated.Text = 0
                display_totalxmlvalidate.Text = datacountryvalid.Rows.Count
                For Each row As DataRow In dtGenerateParalelStatus.Rows

                    display_totalxmlgenerated.Text = Row.Item("Counter")
                display_statusAPI.Text = Row.Item("StatusGenerate")

                display_errormassageapi.Text = Row.Item("Error_API")

                If Row.Item("FileZip") IsNot Nothing And Not (IsDBNull(Row.Item("FileZip"))) And Not (IsDBNull(Row.Item("FileZipName"))) Then

                    'Dim listZip As New List(Of String)
                    'Dim dtZip As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text,
                    '                                                        " select FileZipName,FileZip " &
                    '                                                        " FROM goAML_GenerateXMLParalel " &
                    '                                                        " where FK_goAML_Generate_XML_NOID = " & IDListOfGenerated, Nothing)
                    'listZip.Clear() '' Added on 02 Feb 2021, untuk hapus attachemnt report sebelumnya, Thanks to Pak Bom

                    'If dtZip.Rows.Count > 0 Then
                    '    For Each itemattachment As Data.DataRow In dtZip.Rows
                    '        Dim fileattachmentname As String = Dirpath & itemattachment(0)
                    '        IO.File.WriteAllBytes(fileattachmentname, itemattachment(1))
                    '        listZip.Add(fileattachmentname)

                    '    Next
                    'End If
                    'Filetodownload = listZip.Item(0).ToString

                    'If Filetodownload Is Nothing Then
                    '    Throw New Exception("Not Exists Report")

                    'End If

                    'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update goAML_GenerateXMLParalel " &
                    '    " set StatusGenerate = 7 " &
                    '    " ,LastUpdateDate = getdate() " &
                    '" where FK_goAML_Generate_XML_NOID =  '" & IDListOfGenerated & "'", Nothing)

                    'lblStatusAPI.Text = "XML Generated"

                    'Throw New Exception("API has sucessfully Generate XML. If you want to regenerate, please do Validate XML first.")
                ElseIf Row.Item("KodeStatusGenerate") = "6" Then
                    If Row.Item("Format") = "Excel" Then
                        Dim objParam(1) As SqlParameter
                        objParam(0) = New SqlParameter
                        objParam(0).ParameterName = "@PK_CRS_INTERNATIONAL_GENERATE_BULK_ID"
                        objParam(0).Value = IDUnik
                        objParam(0).DbType = SqlDbType.BigInt

                        objParam(1) = New SqlParameter
                        objParam(1).ParameterName = "@User_ID"
                        objParam(1).Value = Common.SessionCurrentUser.UserID
                        objParam(1).DbType = SqlDbType.VarChar

                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_INTERNATIONAL_GenerateExcel_SaveEOD", objParam)

                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update CRS_International_Generate_Bulk " &
                            " set Status_API = 8 " &
                            " ,LastUpdateDate = getdate() " &
                        " where PK_CRS_INTERNATIONAL_GENERATE_BULK_ID =  '" & IDUnik & "'", Nothing)
                        display_statusAPI.Text = "Process Generate Excel"
                    ElseIf Row.Item("Format") = "XML" Then
                        Dim objParam(1) As SqlParameter
                        objParam(0) = New SqlParameter
                        objParam(0).ParameterName = "@PK_ListOfGenerate_ID"
                        objParam(0).Value = IDUnik
                        objParam(0).DbType = SqlDbType.BigInt

                        objParam(1) = New SqlParameter
                        objParam(1).ParameterName = "@User_ID"
                        objParam(1).Value = Common.SessionCurrentUser.UserID
                        objParam(1).DbType = SqlDbType.VarChar

                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_International_GENERATE_XML_SaveEOD", objParam)

                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update CRS_International_Generate_Bulk " &
                            " set Status_API = 1 " &
                            " ,LastUpdateDate = getdate() " &
                        " where PK_CRS_INTERNATIONAL_GENERATE_BULK_ID =  '" & IDUnik & "'", Nothing)
                        display_statusAPI.Text = "Process Generate XML"
                    End If

                ElseIf Row.Item("KodeStatusGenerate") = "1" Then
                    Throw New Exception("Process Generate XML by API is in progress. Please check again later.")
                ElseIf Row.Item("KodeStatusGenerate") = "4" Then
                    Throw New Exception("Process Validate XML by API is in progress. Please check again later.")
                ElseIf Row.Item("KodeStatusGenerate") = "8" Then
                    Throw New Exception("Process Generate Excel by API is in progress. Please check again later.")
                End If

                Next

            End If


            '' remarked 31-Dec-2021 , ini cara lama yg berhasil
            'If Not ObjGeneratedgoAML Is Nothing Then
            '    'FileReturn = GeneratePPATKBLL.GenerateReportXML(ObjGeneratedgoAML.Transaction_Date, ObjGeneratedgoAML.Jenis_Laporan, Common.SessionCurrentUser.UserID, Dirpath, connection, filenamexml, filenamezip, formatdate, ObjGeneratedgoAML.Status)

            '    Filetodownload = GeneratePPATKBLL.GenerateReportXMLNew(ObjGeneratedgoAML.Transaction_Date, ObjGeneratedgoAML.Jenis_Laporan, ObjGeneratedgoAML.Last_Update_Date, Dirpath)
            '    If Filetodownload Is Nothing Then
            '        Throw New Exception("Not Exists Report")

            '    End If

            'End If
            '' end remark

            '' End 31-Dec-2021


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Protected Sub btnDownloadXML_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)

        '' Added by Felix 27 Aug 2020
        'Dim isHarusValid As String = getParameterGlobalByPK(12) '' Download harus Valid XSD
        '' End of Felix

        Try


            If ObjGeneratedCRSInternational.Valid_XSD = 0 Then
                Dim valid As Integer = CRS_Global_BLL.getGlobalParameterValueByID(9)
                If valid = 1 Then
                    Throw New Exception("Download Harus Valid XSD")
                End If
            End If

            Dim path As String = Server.MapPath("~\goaml\FolderExport\")
            '  Dim path As String = SystemParameterBLL.GetSystemParameterByPk(9017).SettingValue

            Dim Dirpath = path & Common.SessionCurrentUser.UserID & "\"
            If Not Directory.Exists(Dirpath) Then
                Directory.CreateDirectory(Dirpath)
            End If

            '' Edit 31-Dec-2021
            'NawaDevBLL.GeneratePPATKBLL.ValidateSchema(ObjGeneratedgoAML.Transaction_Date, ObjGeneratedgoAML.Jenis_Laporan, ObjGeneratedgoAML.Last_Update_Date)

            Dim objGenerateParalelStatus(0) As SqlParameter
            objGenerateParalelStatus(0) = New SqlParameter
            objGenerateParalelStatus(0).ParameterName = "@PK_ListOfGenerate_ID"
            objGenerateParalelStatus(0).Value = IDUnik

            Dim dtGenerateParalelStatus As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_International_GET_GenerateParalelStatus", objGenerateParalelStatus)

            If dtGenerateParalelStatus IsNot Nothing And dtGenerateParalelStatus.Rows.Count > 0 Then
                For Each row As DataRow In dtGenerateParalelStatus.Rows
                    If row.Item("KodeStatusGenerate") = "4" Or row.Item("KodeStatusGenerate") = "6" Then
                        display_totalxmlgenerated.Value = 0
                        display_totalxmlvalidate.Value = row.Item("CounterValidate")
                    Else


                        display_totalxmlgenerated.Value = row.Item("Counter")
                            display_totalxmlvalidate.Value = Session("JumlahData")

                    End If
                    'display_totalxmlvalidate.Text = row.Item("CounterValidate")

                    'display_totalxmlgenerated.Text = row.Item("Counter")
                    display_statusAPI.Text = row.Item("StatusGenerate")

                    display_errormassageapi.Text = row.Item("Error_API")



                    If row.Item("FileZip") IsNot Nothing And Not (IsDBNull(row.Item("FileZip"))) And Not (IsDBNull(row.Item("FileZipName"))) Then

                        Dim listZip As New List(Of String)
                        Dim dtZip As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text,
                                                                                " select FileZipName,FileZip " &
                                                                                " FROM CRS_International_Generate_Bulk " &
                                                                                " where PK_CRS_INTERNATIONAL_GENERATE_BULK_ID = " & IDUnik, Nothing)
                        listZip.Clear() '' Added on 02 Feb 2021, untuk hapus attachemnt report sebelumnya, Thanks to Pak Bom

                        If dtZip.Rows.Count > 0 Then
                            For Each itemattachment As Data.DataRow In dtZip.Rows
                                Dim fileattachmentname As String = Dirpath & itemattachment(0)
                                IO.File.WriteAllBytes(fileattachmentname, itemattachment(1))
                                listZip.Add(fileattachmentname)

                            Next
                        End If
                        Filetodownload = listZip.Item(0).ToString

                        If Filetodownload Is Nothing Then
                            Throw New Exception("Not Exists Report")

                        End If
                        If ObjGeneratedCRSInternational.Format = "Excel" Then
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update CRS_International_Generate_Bulk " &
                          " set Status_API = 11 " &
                          " ,LastUpdateDate = getdate() " &
                           " ,Last_Download_Date = getdate() " &
                      " where PK_CRS_INTERNATIONAL_GENERATE_BULK_ID =  '" & IDUnik & "'", Nothing)

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update header " &
                          " set header.status = 2 " &
                          " ,header.LastUpdateDate = getdate() " &
                          " From CRS_International_Generate_Bulk_CountryValid CV " &
                          " inner join CRS_International_Generate_Bulk bulks " &
                          " on bulks.PK_CRS_International_Generate_Bulk_ID = CV.FK_Header_Generate_ID " &
                          " inner join CRS_INTERNATIONAL_REPORT_HEADER header " &
                          " on header.pk_CRS_INTERNATIONAL_REPORT_HEADER_id = CV.PK_report_ID " &
                          " where PK_CRS_International_Generate_Bulk_ID =  '" & IDUnik & "'", Nothing)

                            display_statusAPI.Text = "Excel Generated"
                        Else
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update CRS_International_Generate_Bulk " &
                          " set Status_API = 7 " &
                          " ,LastUpdateDate = getdate() " &
                          " ,Last_Download_Date = getdate() " &
                      " where PK_CRS_INTERNATIONAL_GENERATE_BULK_ID =  '" & IDUnik & "'", Nothing)

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update header " &
                          " set header.status = 2 " &
                          " ,header.LastUpdateDate = getdate() " &
                          " From CRS_International_Generate_Bulk_CountryValid CV " &
                          " inner join CRS_International_Generate_Bulk bulks " &
                          " on bulks.PK_CRS_International_Generate_Bulk_ID = CV.FK_Header_Generate_ID " &
                          " inner join CRS_INTERNATIONAL_REPORT_HEADER header " &
                          " on header.pk_CRS_INTERNATIONAL_REPORT_HEADER_id = CV.PK_report_ID " &
                          " where PK_CRS_International_Generate_Bulk_ID =  '" & IDUnik & "'", Nothing)

                            display_statusAPI.Text = "XML Generated"
                        End If

                    ElseIf row.Item("KodeStatusGenerate") = "6" Then
                        'Dim objParam(1) As SqlParameter
                        'objParam(0) = New SqlParameter
                        'objParam(0).ParameterName = "@PK_ListOfGenerate_ID"
                        'objParam(0).Value = IDListOfGenerated
                        'objParam(0).DbType = SqlDbType.BigInt

                        'objParam(1) = New SqlParameter
                        'objParam(1).ParameterName = "@User_ID"
                        'objParam(1).Value = Common.SessionCurrentUser.UserID
                        'objParam(1).DbType = SqlDbType.VarChar

                        'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_GENERATE_XML_SaveEOD", objParam)

                        'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update goAML_GenerateXMLParalel " &
                        '    " set StatusGenerate = 1 " &
                        '    " ,LastUpdateDate = getdate() " &
                        '" where FK_goAML_Generate_XML_NOID =  '" & IDListOfGenerated & "'", Nothing)
                        'lblStatusAPI.Text = "Process Generate XML"
                    ElseIf row.Item("KodeStatusGenerate") = "1" Then
                        Throw New Exception("Process Generate XML by API is in progress. Please check again later.")
                    ElseIf row.Item("KodeStatusGenerate") = "8" Then
                        Throw New Exception("Process Generate Excel by API is in progress. Please check again later.")
                    ElseIf row.Item("KodeStatusGenerate") = "4" Then
                        Throw New Exception("Process Validate XML by API is in progress. Please check again later.")
                    End If

                Next

            End If


        Catch ex As Exception
            ErrorSignal.FromCurrentContext.Raise(ex)
            Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    <DirectMethod>
    Sub Download()
        If Filetodownload <> "" Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(Filetodownload))
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "application/zip"
            Response.BinaryWrite(IO.File.ReadAllBytes(Filetodownload))
            Response.End()

            ''06-Jan-2022 Felix , hapus setelah download
            If IO.File.Exists(Filetodownload) Then
                IO.File.Delete(Filetodownload)
            End If

            '5-Aug-2021 Adi : kosongkan agar tidak ke-download saat invalid
            Filetodownload = ""
        End If
    End Sub
End Class
