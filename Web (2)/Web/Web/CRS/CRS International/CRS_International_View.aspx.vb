﻿Imports Ext.Net
Imports OfficeOpenXml
Imports NawaBLL
Imports System.Data
Imports NawaDAL
Imports System.Xml
Imports System.Data.SqlClient
Imports System.IO
Imports CRSBLL
Imports System.Web.Services
Imports CRSDAL

Partial Class CRS_CRS_International_CRS_International_View
    Inherits Parent
    Public objFormModuleView As NawaBLL.FormModuleView
    Protected NeedToCorrection As Integer
    Protected UserCRS As String
    Public Property strWhereClause() As String
        Get
            Return Session("CRS_International_View.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_View.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("CRS_International_View.strSort")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_View.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("CRS_International_View.indexStart")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_View.indexStart") = value
        End Set
    End Property
    Public Property QueryTable() As String
        Get
            Return Session("CRS_International_View.Table")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_View.Table") = value
        End Set
    End Property
    Public Property QueryField() As String
        Get
            Return Session("CRS_International_View.Field")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_View.Field") = value
        End Set
    End Property

    '28-Sep-2021 Adi : show event mask on generate XML
    Public Property Filetodownload() As String
        Get
            Return Session("CRS_International_View.Filetodownload")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_View.Filetodownload") = value
        End Set
    End Property

    Public Property FiletodownloadXML() As String
        Get
            Return Session("CRS_International_View.FiletodownloadXML")
        End Get
        Set(ByVal value As String)
            Session("CRS_International_View.FiletodownloadXML") = value
        End Set
    End Property
    '-------------------------------------------------

    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CRS Inter Report Header")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=CRS_INTERNATIONAL_Report_Header.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=CRS_INTERNATIONAL_Report_Header.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add("CRS Inter Report Header")
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=CRS_INTERNATIONAL_Report_Header.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using

                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=CRS_INTERNATIONAL_Report_Header.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub

    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            'Begin Penambahan Advanced Filter
            If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                Toolbar2.Hidden = True
            Else
                Toolbar2.Hidden = False
            End If
            LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")
            'END Penambahan Advanced Filter


            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

            strfilter = strfilter.Replace("Active", "CRS Inter Report Header" & ".Active")


            Dim strsort As String = "PK_CRS_INTERNATIONAL_REPORT_HEADER_ID DESC"
            For Each item As DataSorter In e.Sort
                'strsort += ", " & item.Property & " " & item.Direction.ToString
                strsort = item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter
            'Begin Penambahan Advanced Filter
            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            End If
            'END Penambahan Advanced Filter

            Me.strOrder = strsort

            'QueryTable = "vw_SIPENDAR_PROFILE"
            'QueryField = "*"
            'Dim DataPaging As DataTable = SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)

            Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)

            'Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = DLL.SQLHelper.ExecuteTabelPaging("CustomerInformation_WebTempTable", "CIFNo, Name, DateOfBirth, AccountOwnerId, OpeningDate, WorkingUnitName, IsCustomerInList", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Begin Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdd_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim sm As RowSelectionModel = TryCast(Me.GridpanelView.GetSelectionModel(), RowSelectionModel)
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    Private Sub CRS_International_View_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub

    Private Sub CRS_International_View_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer
            Try
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)


                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                    Dim strIDCode As String = 1
                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If



                objFormModuleView.ModuleID = objmodule.PK_Module_ID
                objFormModuleView.ModuleName = objmodule.ModuleName
                objFormModuleView.AddField("PK_CRS_INTERNATIONAL_REPORT_HEADER_ID", "Report ID", 1, True, True, NawaBLL.Common.MFieldType.BIGIDENTITY)
                objFormModuleView.AddField("TransmittingCountry", "Transmitting Country", 2, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("ReceivingCountry", "Receiving Country", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("MessageType", "Message Type", 4, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("StatusName", "Status", 5, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("AGING", "Aging", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Warning", "Warning", 7, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("MessageRefID", "Message Ref ID", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("MessageTypeIndic", "Message Type Indic", 9, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("CorrMessageRefID", "Corr Message Ref ID", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("ReportingPeriod", "Reporting Period", 11, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                objFormModuleView.AddField("Timestamp", "Timestamp", 12, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                objFormModuleView.AddField("isvalid", "Valid?", 13, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("ErrorMessage", "Error Message", 14, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("createdby", "Created By", 15, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("createddate", "Created Date", 16, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                objFormModuleView.AddField("LastUpdateDate", "LastUpdateDate", 17, False, True, NawaBLL.Common.MFieldType.VARCHARValue)


                objFormModuleView.SettingFormView()
                GridpanelView.Width = 200
                UserCRS = NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID.ToString
                If objmodule.ModuleName <> "Vw_CRS_International_Report_Fatca" Then
                    'Custom GridCommand
                    If UserCRS <> "57" Then
                        Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
                        Dim gridCommandResponse As New GridCommand

                        objcommandcol.Width = 450
                        Dim objGeneratexml As New GridCommand
                        objGeneratexml.CommandName = "GenerateXML"
                        objGeneratexml.Icon = Icon.DiskDownload
                        objGeneratexml.Text = "Generate XML"
                        objGeneratexml.ToolTip.Text = ""
                        objcommandcol.Commands.Add(objGeneratexml)

                        Dim objGenerateexcel As New GridCommand
                        objGenerateexcel.CommandName = "GenerateExcel"
                        objGenerateexcel.Icon = Icon.DiskDownload
                        objGenerateexcel.Text = "Generate Excel"
                        objGenerateexcel.ToolTip.Text = ""
                        objcommandcol.Commands.Add(objGenerateexcel)

                        Dim extparam As New Ext.Net.Parameter
                        extparam.Name = "unikkey"
                        extparam.Value = "record.data.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID"
                        extparam.Mode = ParameterMode.Raw
                        objcommandcol.DirectEvents.Command.ExtraParams.Add(extparam)

                        objcommandcol.DirectEvents.Command.IsUpload = True
                        objcommandcol.DirectEvents.Command.EventMask.ShowMask = True
                        objcommandcol.DirectEvents.Command.EventMask.Msg = "Loading..."
                        objcommandcol.DirectEvents.Command.Success = "NawadataDirect.Download_XML({isUpload : true});"

                        Dim extParamCommandName As New Ext.Net.Parameter
                        extparam.Name = "command"
                        extparam.Value = "command"
                        extparam.Mode = ParameterMode.Raw
                        objcommandcol.DirectEvents.Command.ExtraParams.Add(extparam)


                        AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf RedirectToSiPendarReport


                        Dim stringcommand As String = "prepareCommandCollection"
                        objcommandcol.PrepareToolbar.Fn = stringcommand
                    Else
                        Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
                        Dim gridCommandResponse As New GridCommand

                        objcommandcol.Width = 300
                        Dim objGeneratexml As New GridCommand
                        objGeneratexml.CommandName = "GenerateXML"
                        objGeneratexml.Icon = Icon.DiskDownload
                        objGeneratexml.Text = "Generate XML"
                        objGeneratexml.ToolTip.Text = ""
                        objcommandcol.Commands.Add(objGeneratexml)

                        Dim objGenerateexcel As New GridCommand
                        objGenerateexcel.CommandName = "GenerateExcel"
                        objGenerateexcel.Icon = Icon.DiskDownload
                        objGenerateexcel.Text = "Generate Excel"
                        objGenerateexcel.ToolTip.Text = ""
                        objcommandcol.Commands.Add(objGenerateexcel)

                        Dim extparam As New Ext.Net.Parameter
                        extparam.Name = "unikkey"
                        extparam.Value = "record.data.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID"
                        extparam.Mode = ParameterMode.Raw
                        objcommandcol.DirectEvents.Command.ExtraParams.Add(extparam)

                        objcommandcol.DirectEvents.Command.IsUpload = True
                        objcommandcol.DirectEvents.Command.EventMask.ShowMask = True
                        objcommandcol.DirectEvents.Command.EventMask.Msg = "Loading..."
                        objcommandcol.DirectEvents.Command.Success = "NawadataDirect.Download_XML({isUpload : true});"

                        Dim extParamCommandName As New Ext.Net.Parameter
                        extparam.Name = "command"
                        extparam.Value = "command"
                        extparam.Mode = ParameterMode.Raw
                        objcommandcol.DirectEvents.Command.ExtraParams.Add(extparam)


                        AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf RedirectToSiPendarReport


                        Dim stringcommand As String = "prepareCommandCollection"
                        objcommandcol.PrepareToolbar.Fn = stringcommand
                    End If

                End If




            Catch ex As Exception

            End Try

            If Not Ext.Net.X.IsAjaxRequest Then
                Dim objcommandcolumnrefid As Ext.Net.Column = GridpanelView.ColumnModel.Columns.Find(Function(x) x.DataIndex = "MessageRefID")
                objcommandcolumnrefid.Width = 250

                Dim objcommandcolumnrefidcorr As Ext.Net.Column = GridpanelView.ColumnModel.Columns.Find(Function(x) x.DataIndex = "CorrMessageRefID")
                objcommandcolumnrefidcorr.Width = 250

                Dim objcommandcolumn As Ext.Net.Column = GridpanelView.ColumnModel.Columns.Find(Function(x) x.DataIndex = "Warning")
                objcommandcolumn.Width = 200

                Dim objcommandcolumn2 As Ext.Net.Column = GridpanelView.ColumnModel.Columns.Find(Function(x) x.DataIndex = "ErrorMessage")
                objcommandcolumn2.Width = 700

                cboExportExcel.SelectedItem.Text = "Excel"
            End If
            NeedToCorrection = CRS_Global_BLL.getGlobalParameterValueByID(14)


        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub GenerateXML_Bulk()
        Try
            Session("GenerateBULK") = 0
            Session("GenerateBULK") = 1
            'Cek minimal 1 profile hasil screening dipilih
            Dim smCRSInter As RowSelectionModel = TryCast(GridpanelView.GetSelectionModel(), RowSelectionModel)
            If smCRSInter.SelectedRows.Count = 0 Then
                Throw New Exception("Minimal 1 data harus dipilih!")
            End If

            ' Cek apakah yang dipilih ada yang draft atau in progress editing
            Dim listCRSInter As New List(Of CRS_INTERNATIONAL_REPORT_HEADER)
            Dim reportingperiode As String = ""
            For Each item As SelectedRow In smCRSInter.SelectedRows
                Dim recordID = item.RecordID.ToString
                Dim objNew = New CRSDAL.CRS_INTERNATIONAL_REPORT_HEADER
                With objNew
                    .PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = recordID

                End With
                Using objdb As New CRSEntities
                    Dim objreport = objdb.CRS_INTERNATIONAL_REPORT_HEADER.Where(Function(x) x.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = recordID).FirstOrDefault
                    If objreport IsNot Nothing Then
                        reportingperiode = objreport.ReportingPeriod.Value.Year
                    End If

                End Using
                listCRSInter.Add(objNew)
            Next
            Dim pkcrsinter As String = ""

            Dim listcrsinterresult As New List(Of CRS_INTERNATIONAL_REPORT_HEADER)
            For Each item In listCRSInter

                pkcrsinter = pkcrsinter & "," & item.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID

            Next
            pkcrsinter = pkcrsinter.Remove(0, 1)

            Dim jumlahdraftdanprogessediting As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select count(*) from CRS_INTERNATIONAL_REPORT_HEADER WHERE PK_CRS_INTERNATIONAL_REPORT_HEADER_ID in(" & pkcrsinter & ")  and status in(0,10)", Nothing)

            If jumlahdraftdanprogessediting > 0 Then
                Throw New Exception("Ada Salah Satu Data Yang Statusnya Masih Draft atau In Progress Editing!")
            End If


            ' cek juga di global param apakah allow need to correction di generate
            Dim allowneedtocorrection As Integer = CRS_Global_BLL.getGlobalParameterValueByID(14)
            If allowneedtocorrection = 0 Then
                Dim jumlahdraftneedtocorrection As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select count(*) from CRS_INTERNATIONAL_REPORT_HEADER WHERE PK_CRS_INTERNATIONAL_REPORT_HEADER_ID in(" & pkcrsinter & ")  and status in(5) ", Nothing)
                If jumlahdraftneedtocorrection > 0 Then
                    Throw New Exception("Tidak Bisa Generate Data Dengan Status Need To Correction")
                End If
            End If

            ' cek apakah ada data dengan receiving country yang sama lebih dari 1
            Dim jumlahddatacountryduplikat As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select top 1 count(ReceivingCountry) from CRS_INTERNATIONAL_REPORT_HEADER WHERE PK_CRS_INTERNATIONAL_REPORT_HEADER_ID in(" & pkcrsinter & ")  GROUP BY ReceivingCountry ORDER BY ReceivingCountry DESC", Nothing)
            If jumlahddatacountryduplikat > 1 Then
                Throw New Exception("Tidak Bisa Generate Data Karena Salah Satu Data Yang Di Select Ada Yang Sama Receiving Country nya")
            End If

            ' cek apakah ada reporting period yang sama
            Dim jumlahddatareportingperiod As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select count(distinct ReportingPeriod) from vw_crs_international_Report WHERE PK_CRS_INTERNATIONAL_REPORT_HEADER_ID in(" & pkcrsinter & ") ", Nothing)
            If jumlahddatareportingperiod > 1 Then
                Throw New Exception("Tidak Bisa Generate Data Karena Reporting Periode Ada Yang Berbeda")
            End If

            Session("PK_CRS_International_Selected_Row") = pkcrsinter
            Session("Reporting_Periode_CRS_International_Selected_Row") = reportingperiode
            Dim objModuleCRSListOfGenerate As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_CRS_International_Generate_Bulk")
            If objModuleCRSListOfGenerate IsNot Nothing Then
                Dim strEncModuleID As String = NawaBLL.Common.EncryptQueryString(objModuleCRSListOfGenerate.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objModuleCRSListOfGenerate.UrlAdd & "?ModuleID={0}", strEncModuleID), "Loading")
            Else
                Throw New ApplicationException("Module vw_CRS_International_Generate_Bulk is not exists!")
            End If

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub


    Protected Sub RedirectToSiPendarReport(sender As Object, e As DirectEventArgs)
        Try
            Session("GenerateBULK") = 0
            Session("Vw_CRS_INTERNATIONAL_GenerateXMLParalel_PK") = Nothing
            If e.ExtraParams("command") = "GenerateXML" Then
                Dim objModuleSiPendarReport As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_CRS_International_Generate_Bulk")
                If objModuleSiPendarReport IsNot Nothing Then
                    Dim strEncModuleID As String = NawaBLL.Common.EncryptQueryString(objModuleSiPendarReport.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                    'Get Parameter Profile ID
                    Dim pkProfileID As String = e.ExtraParams("unikkey")
                    Session("Vw_CRS_INTERNATIONAL_GenerateXMLParalel_PK") = pkProfileID
                    Session("vw_CRS_International_Generate_Bulk_GenerateXML") = 1

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objModuleSiPendarReport.UrlDetail & "?ModuleID={0}", strEncModuleID), "Loading")
                Else
                    Throw New ApplicationException("Module vw_CRS_International_Generate_Bulk is not exists!")
                End If
            ElseIf e.ExtraParams("command") = "GenerateExcel" Then
                Dim objModuleSiPendarReport As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_CRS_International_Generate_Bulk")
                If objModuleSiPendarReport IsNot Nothing Then
                    Dim strEncModuleID As String = NawaBLL.Common.EncryptQueryString(objModuleSiPendarReport.PK_Module_ID, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                    'Get Parameter Profile ID
                    Dim pkProfileID As String = e.ExtraParams("unikkey")
                    Session("Vw_CRS_INTERNATIONAL_GenerateXMLParalel_PK") = pkProfileID
                    Session("vw_CRS_International_Generate_Bulk_GenerateXML") = 0

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objModuleSiPendarReport.UrlDetail & "?ModuleID={0}", strEncModuleID), "Loading")
                Else
                    Throw New ApplicationException("Module vw_CRS_International_Generate_Bulk is not exists!")
                End If
            End If

        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub



End Class
