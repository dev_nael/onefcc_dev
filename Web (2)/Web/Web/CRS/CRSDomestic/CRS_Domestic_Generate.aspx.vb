﻿Imports Ext
Imports Elmah
Imports System.Data
Imports SiPendarBLL
Imports SiPendarDAL
Imports System.Data.SqlClient
Imports System.Data.Entity
Imports NawaBLL
Imports System.IO
Imports NawaDAL

Partial Class CRS_Domestic_Generate
    Inherits ParentPage

    Public Property IDModule() As String
        Get
            Return Session("CRS_Domestic_Generate.IDModule")
        End Get
        Set(ByVal value As String)
            Session("CRS_Domestic_Generate.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("CRS_Domestic_Generate.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("CRS_Domestic_Generate.IDUnik") = value
        End Set
    End Property

    Public Property IDParalel() As Long
        Get
            Return Session("CRS_Domestic_Generate.IDParalel")
        End Get
        Set(ByVal value As Long)
            Session("CRS_Domestic_Generate.IDParalel") = value
        End Set
    End Property

    Public Property Filetodownload() As String
        Get
            Return Session("CRS_Domestic_Generate.Filetodownload")
        End Get
        Set(ByVal value As String)
            Session("CRS_Domestic_Generate.Filetodownload") = value
        End Set
    End Property

    Sub ClearSession()
        IDUnik = Nothing
        'cb_generate_export.SelectedItem.Text = "Excel"
        'cb_generate_export.SelectedItem.Value = "Excel"
        cb_generate_export.SetTextWithTextValue("Excel", "Excel")
        IDUnik = Nothing
        IDParalel = Nothing
        Filetodownload = ""
    End Sub

    Private Sub CRS_Domestic_Generate_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ActionType = NawaBLL.Common.ModuleActionEnum.view
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()
                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If

                IDModule = Request.Params("ModuleID")
                Dim intModuleID As Integer = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                Dim objHeader As New CRSDAL.CRS_DOMESTIC_HEADER
                Using objdb As New CRSDAL.CRSEntities
                    objHeader = objdb.CRS_DOMESTIC_HEADER.Where(Function(x) x.PK_CRS_DOMESTIC_HEADER_ID = IDUnik).FirstOrDefault
                    If objHeader IsNot Nothing Then
                        Df_generate_id.Value = objHeader.PK_CRS_DOMESTIC_HEADER_ID
                        Df_generate_year.Value = objHeader.Periode
                    End If
                End Using

                LoadData()

                TaskManager1.StartTask("refreshStatus")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_generate_Validate_Click()
        Try
            If IDParalel = Nothing Then
                Dim strQuery As String = " insert into CRS_DOMESTIC_GenerateXMLParalel(FK_CRS_DOMESTIC_HEADER_ID, YearPeriod, StatusGenerate, CreatedBy, CreatedDate) "
                strQuery = strQuery & " select PK_CRS_DOMESTIC_HEADER_ID, Periode, 0, '" & NawaBLL.Common.SessionCurrentUser.UserID & "', GETDATE() from CRS_DOMESTIC_HEADER "
                strQuery = strQuery & " where PK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                strQuery = strQuery & " select SCOPE_IDENTITY() "
                Dim drTemp As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString(strQuery)
                'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                If drTemp IsNot Nothing AndAlso Not IsDBNull(drTemp(0)) Then
                    IDParalel = drTemp(0)
                Else
                    Throw New ApplicationException("an error occurred while processing data, please report it to your admin.")
                End If
            Else
                'Dim FK_EODTaskDetailLog_ID As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select top 1 FK_EODTaskDetailLog_ID from CRS_DOMESTIC_GenerateXMLParalel where PK_CRS_DOMESTIC_GenerateXMLParalel_ID = " & IDParalel, Nothing)
                'Dim intStatus As Integer = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select FK_MsEODStatus_ID from EODTaskDetailLog where PK_EODTaskDetailLog_ID = " & FK_EODTaskDetailLog_ID, Nothing)
                'If intStatus = 1 OrElse intStatus = 2 Then
                '    Dim MsEODStatusName As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select top 1 MsEODStatusName from MsEODStatus where PK_MsEODStatus_ID = " & intStatus, Nothing)
                '    Throw New ApplicationException("Please Try Again After Some Time, the Process is " & MsEODStatusName & ".")
                'End If

                Dim StrQuery As String = " select detaillog.FK_MsEODStatus_ID as TaskDetailStatus, schedulerlog.FK_MsEODStatus_ID as SchedulerStatus "
                StrQuery = StrQuery & " from CRS_DOMESTIC_GenerateXMLParalel paralel "
                StrQuery = StrQuery & " join EODTaskDetailLog detaillog "
                StrQuery = StrQuery & " on paralel.FK_EODTaskDetailLog_ID = detaillog.PK_EODTaskDetailLog_ID "
                StrQuery = StrQuery & " join EODSchedulerLog schedulerlog "
                StrQuery = StrQuery & " on detaillog.EODSchedulerLogID = schedulerlog.PK_EODSchedulerLog_ID "
                StrQuery = StrQuery & " where PK_CRS_DOMESTIC_GenerateXMLParalel_ID = " & IDParalel
                StrQuery = StrQuery & " and (detaillog.FK_MsEODStatus_ID = 1 or detaillog.FK_MsEODStatus_ID = 2) "
                StrQuery = StrQuery & " and (schedulerlog.FK_MsEODStatus_ID = 1 or schedulerlog.FK_MsEODStatus_ID = 2) "
                Dim CheckRunProcess As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString(StrQuery)
                If CheckRunProcess IsNot Nothing AndAlso CheckRunProcess.Rows.Count > 0 Then
                    Dim DrRunProcess As DataRow = CheckRunProcess.Rows(0)
                    If Not IsDBNull(DrRunProcess("TaskDetailStatus")) Then
                        StrQuery = "select top 1 MsEODStatusName from MsEODStatus where PK_MsEODStatus_ID = " & DrRunProcess("TaskDetailStatus")
                        Dim MsEODStatusName As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, StrQuery, Nothing)
                        Throw New ApplicationException("Please Try Again After Some Time, the Process is " & MsEODStatusName & ".")
                    End If
                End If
            End If
            Dim objParam(1) As SqlParameter
            objParam(0) = New SqlParameter
            objParam(0).ParameterName = "@PK_CRS_DOMESTIC_GenerateXMLParalel_ID"
            objParam(0).Value = IDParalel
            objParam(0).DbType = SqlDbType.BigInt

            objParam(1) = New SqlParameter
            objParam(1).ParameterName = "@User_ID"
            objParam(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
            objParam(1).DbType = SqlDbType.VarChar

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Domestic_ValidateXML_SaveEOD", objParam)

            Df_generate_statusAPI.Text = "Process Validate XML"

            Btn_generate_Download.Hidden = True

            cb_generate_export.IsReadOnly = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_generate_Generate_Click()
        Try
            If IDParalel = Nothing Then
                Throw New ApplicationException("Please Run the Validation Process First.")
            Else
                Dim ParameterValue As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, " select ParameterValue from CRS_DOMESTIC_GLOBAL_PARAMETER where PK_CRS_DOMESTIC_GLOBAL_PARAMETER_ID = 5 ", Nothing)
                If ParameterValue = "1" Then
                    Dim ErrorMessage As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, " select isnull(ErrorMessage,'') from CRS_DOMESTIC_GenerateXMLParalel where PK_CRS_DOMESTIC_GenerateXMLParalel_ID = " & IDParalel, Nothing)
                    If Not String.IsNullOrEmpty(ErrorMessage) Then
                        Throw New ApplicationException("Please Fix Invalid Data First Before Generate.")
                    End If
                End If

                'Dim FK_EODTaskDetailLog_ID As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select top 1 FK_EODTaskDetailLog_ID from CRS_DOMESTIC_GenerateXMLParalel where PK_CRS_DOMESTIC_GenerateXMLParalel_ID = " & IDParalel, Nothing)
                ''Dim intStatus As Integer = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select FK_MsEODStatus_ID from EODTaskDetailLog where PK_EODTaskDetailLog_ID = " & FK_EODTaskDetailLog_ID, Nothing)
                'Dim intStatus As Integer = 0
                'Dim ScedulerID As Long = 0
                'Dim StrQuery As String = "select top 1 FK_MsEODStatus_ID, EODSchedulerLogID from EODTaskDetailLog where PK_EODTaskDetailLog_ID = " & FK_EODTaskDetailLog_ID
                'Dim DataEODTaskDetailLog As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString(StrQuery)
                'If DataEODTaskDetailLog IsNot Nothing Then
                '    If Not IsDBNull(DataEODTaskDetailLog("FK_MsEODStatus_ID")) Then
                '        intStatus = DataEODTaskDetailLog("FK_MsEODStatus_ID")
                '    Else
                '        Throw New ApplicationException("There is Some issue When getting data from EOD Task Detail Log ID on : " & FK_EODTaskDetailLog_ID & ".")
                '    End If
                '    If Not IsDBNull(DataEODTaskDetailLog("EODSchedulerLogID")) Then
                '        ScedulerID = DataEODTaskDetailLog("EODSchedulerLogID")
                '    Else
                '        Throw New ApplicationException("There is Some issue When getting data from EOD Task Detail Log ID on : " & FK_EODTaskDetailLog_ID & ".")
                '    End If
                'End If
                'Dim intStatusScheduler As Integer = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select FK_MsEODStatus_ID from EODSchedulerLog where PK_EODSchedulerLog_ID = " & ScedulerID, Nothing)
                'If (intStatus = 1 OrElse intStatus = 2) AndAlso (intStatusScheduler = 1 OrElse intStatusScheduler = 2) Then
                '    Dim MsEODStatusName As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select top 1 MsEODStatusName from MsEODStatus where PK_MsEODStatus_ID = " & intStatus, Nothing)
                '    Throw New ApplicationException("Please Try Again After Some Time, the Process is " & MsEODStatusName & ".")
                'End If

                Dim StrQuery As String = " select detaillog.FK_MsEODStatus_ID as TaskDetailStatus, schedulerlog.FK_MsEODStatus_ID as SchedulerStatus "
                StrQuery = StrQuery & " from CRS_DOMESTIC_GenerateXMLParalel paralel "
                StrQuery = StrQuery & " join EODTaskDetailLog detaillog "
                StrQuery = StrQuery & " on paralel.FK_EODTaskDetailLog_ID = detaillog.PK_EODTaskDetailLog_ID "
                StrQuery = StrQuery & " join EODSchedulerLog schedulerlog "
                StrQuery = StrQuery & " on detaillog.EODSchedulerLogID = schedulerlog.PK_EODSchedulerLog_ID "
                StrQuery = StrQuery & " where PK_CRS_DOMESTIC_GenerateXMLParalel_ID = " & IDParalel
                StrQuery = StrQuery & " and (detaillog.FK_MsEODStatus_ID = 1 or detaillog.FK_MsEODStatus_ID = 2) "
                StrQuery = StrQuery & " and (schedulerlog.FK_MsEODStatus_ID = 1 or schedulerlog.FK_MsEODStatus_ID = 2) "
                Dim CheckRunProcess As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString(StrQuery)
                If CheckRunProcess IsNot Nothing AndAlso CheckRunProcess.Rows.Count > 0 Then
                    Dim DrRunProcess As DataRow = CheckRunProcess.Rows(0)
                    If Not IsDBNull(DrRunProcess("TaskDetailStatus")) Then
                        StrQuery = "select top 1 MsEODStatusName from MsEODStatus where PK_MsEODStatus_ID = " & DrRunProcess("TaskDetailStatus")
                        Dim MsEODStatusName As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, StrQuery, Nothing)
                        Throw New ApplicationException("Please Try Again After Some Time, the Process is " & MsEODStatusName & ".")
                    End If
                End If
            End If

            If cb_generate_export.SelectedItemValue IsNot Nothing Then
                If cb_generate_export.SelectedItemValue = "Excel" Then
                    Df_generate_statusAPI.Text = "Process Generate Excel"
                    Dim objParam(1) As SqlParameter
                    objParam(0) = New SqlParameter
                    objParam(0).ParameterName = "@PK_CRS_DOMESTIC_GenerateXMLParalel_ID"
                    objParam(0).Value = IDParalel
                    objParam(0).DbType = SqlDbType.BigInt

                    objParam(1) = New SqlParameter
                    objParam(1).ParameterName = "@User_ID"
                    objParam(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
                    objParam(1).DbType = SqlDbType.VarChar

                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Domestic_GenerateExcel_SaveEOD", objParam)
                ElseIf cb_generate_export.SelectedItemValue = "XML" Then
                    Df_generate_statusAPI.Text = "Process Generate XML"
                    Dim objParam(1) As SqlParameter
                    objParam(0) = New SqlParameter
                    objParam(0).ParameterName = "@PK_CRS_DOMESTIC_GenerateXMLParalel_ID"
                    objParam(0).Value = IDParalel
                    objParam(0).DbType = SqlDbType.BigInt

                    objParam(1) = New SqlParameter
                    objParam(1).ParameterName = "@User_ID"
                    objParam(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
                    objParam(1).DbType = SqlDbType.VarChar

                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Domestic_GenerateXML_SaveEOD", objParam)
                Else
                    Throw New ApplicationException("an error occurred while Checking " & cb_generate_export.Label & ", please report it to your admin.")
                End If
            Else
                Throw New ApplicationException(cb_generate_export.Label & "Is Nothing, please report it to your admin.")
            End If

            Btn_generate_Download.Hidden = True
            cb_generate_export.IsReadOnly = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_generate_Download_Click()
        Try
            If IDParalel = Nothing Then
                Throw New ApplicationException("Please Run the Validation Process First.")
            End If

            Dim pathGenerateXML As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue FROM CRS_DOMESTIC_GLOBAL_PARAMETER where PK_CRS_DOMESTIC_GLOBAL_PARAMETER_ID = 7", Nothing)
            'pathGenerateXML = "G:\GoAML\Development\Web\TEMP"
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim Dirpath = pathGenerateXML & "\" & strUserID & "\"

            If Not Directory.Exists(pathGenerateXML & "\" & strUserID) Then
                Directory.CreateDirectory(pathGenerateXML & "\" & strUserID)
            End If

            If Not cb_generate_export.SelectedItemValue Is Nothing Then
                If cb_generate_export.SelectedItemValue = "Excel" Then
                    Dim strQuery As String = " select FileExcelZIP, FileExcelZIPName "
                    strQuery = strQuery & " from CRS_DOMESTIC_GenerateXMLParalel "
                    strQuery = strQuery & " where PK_CRS_DOMESTIC_GenerateXMLParalel_ID = " & IDParalel
                    Dim drGenerateParalel As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString(strQuery)
                    If drGenerateParalel IsNot Nothing Then
                        If Not IsDBNull(drGenerateParalel("FileExcelZIP")) AndAlso Not IsDBNull(drGenerateParalel("FileExcelZIPName")) Then
                            Dim fileattachmentname As String = Dirpath & drGenerateParalel("FileExcelZIPName")
                            IO.File.WriteAllBytes(fileattachmentname, drGenerateParalel("FileExcelZIP"))
                            Filetodownload = fileattachmentname
                            'If Filetodownload Is Nothing Then
                            '    Throw New Exception("Not Exists Report")
                            'End If
                            Df_generate_statusAPI.Text = "Excel Downloaded"
                            SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "UPDATE CRS_DOMESTIC_HEADER SET Status_Report = 9 where PK_CRS_DOMESTIC_HEADER_ID = " & IDUnik, Nothing)
                        Else
                            Throw New ApplicationException("There is no Generated Excel Data Saved, please generate data first.")
                        End If
                    Else
                        Throw New ApplicationException("an error occurred while Getting Generated Excel Data, please report it to your admin.")
                    End If
                ElseIf cb_generate_export.SelectedItemValue = "XML" Then
                    Dim strQuery As String = " select FileXMLZIP, FileXMLZIPName "
                    strQuery = strQuery & " from CRS_DOMESTIC_GenerateXMLParalel "
                    strQuery = strQuery & " where PK_CRS_DOMESTIC_GenerateXMLParalel_ID = " & IDParalel
                    Dim drGenerateParalel As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString(strQuery)
                    If drGenerateParalel IsNot Nothing Then
                        If Not IsDBNull(drGenerateParalel("FileXMLZIP")) AndAlso Not IsDBNull(drGenerateParalel("FileXMLZIPName")) Then
                            Dim fileattachmentname As String = Dirpath & drGenerateParalel("FileXMLZIPName")
                            IO.File.WriteAllBytes(fileattachmentname, drGenerateParalel("FileXMLZIP"))
                            Filetodownload = fileattachmentname
                            Df_generate_statusAPI.Text = "XML Downloaded"
                            SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "UPDATE CRS_DOMESTIC_HEADER SET Status_Report = 8 where PK_CRS_DOMESTIC_HEADER_ID = " & IDUnik, Nothing)
                        Else
                            Throw New ApplicationException("There is no Generated XML Data Saved, please generate data first.")
                        End If
                    Else
                        Throw New ApplicationException("an error occurred while Getting Generated Excel Data, please report it to your admin.")
                    End If
                Else
                    Throw New ApplicationException("an error occurred while Checking " & cb_generate_export.Label & ", please report it to your admin.")
                End If
            Else
                Throw New ApplicationException(cb_generate_export.Label & "Is Nothing, please report it to your admin.")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_generate_Close_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule, "Loading")
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule, "Loading")
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadData()
        Try
            Dim strQuery As String = " select top 1 paralel.*, statusapi.KETERANGAN from CRS_DOMESTIC_GenerateXMLParalel paralel "
            strQuery = strQuery & " left join CRS_STATUS_GENERATE_API statusapi "
            strQuery = strQuery & " on paralel.StatusGenerate = statusapi.KODE "
            strQuery = strQuery & " where FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
            Dim dtGenerateParalel As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString(strQuery)
            If dtGenerateParalel IsNot Nothing AndAlso dtGenerateParalel.Rows.Count > 0 Then
                Dim drTemp As DataRow = dtGenerateParalel(0)
                If Not IsDBNull(drTemp("KETERANGAN")) Then
                    Df_generate_statusAPI.Value = drTemp("KETERANGAN")
                End If
                If Not IsDBNull(drTemp("StatusGenerate")) Then
                    If drTemp("StatusGenerate") = 2 Then
                        Df_generate_ErrorAPI.Value = "ERROR Occurred When Generating XML"
                    ElseIf drTemp("StatusGenerate") = 5 Then
                        Df_generate_ErrorAPI.Value = "ERROR Occurred When Validating Data"
                    ElseIf drTemp("StatusGenerate") = 9 Then
                        Df_generate_ErrorAPI.Value = "ERROR Occurred When Generating Excel"
                    Else
                        Df_generate_ErrorAPI.Value = ""
                    End If

                    If drTemp("StatusGenerate") = 1 OrElse drTemp("StatusGenerate") = 2 OrElse drTemp("StatusGenerate") = 3 OrElse drTemp("StatusGenerate") = 7 Then
                        cb_generate_export.SetTextWithTextValue("XML", "XML")
                        cb_generate_export.IsReadOnly = True
                    ElseIf drTemp("StatusGenerate") = 8 OrElse drTemp("StatusGenerate") = 9 OrElse drTemp("StatusGenerate") = 10 OrElse drTemp("StatusGenerate") = 11 Then
                        cb_generate_export.SetTextWithTextValue("Excel", "Excel")
                        cb_generate_export.IsReadOnly = True
                    Else
                        cb_generate_export.IsReadOnly = False
                    End If

                    If drTemp("StatusGenerate") = 3 OrElse drTemp("StatusGenerate") = 7 OrElse drTemp("StatusGenerate") = 10 OrElse drTemp("StatusGenerate") = 11 Then
                        Btn_generate_Download.Hidden = False
                    Else
                        Btn_generate_Download.Hidden = True
                    End If
                End If
                If Not IsDBNull(drTemp("PK_CRS_DOMESTIC_GenerateXMLParalel_ID")) Then
                    IDParalel = drTemp("PK_CRS_DOMESTIC_GenerateXMLParalel_ID")
                End If
            End If
            If IDParalel <> Nothing Then
                Dim strQueryError As String = " select FK_CRS_DOMESTIC_HEADER_ID as PK_CRS_DOMESTIC_HEADER_ID, ErrorMessage as ErrorValidation from CRS_DOMESTIC_GenerateXMLParalel "
                strQueryError = strQueryError & " where ErrorMessage is not null and PK_CRS_DOMESTIC_GenerateXMLParalel_ID = " & IDParalel
                Dim dtDataError As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString(strQueryError)
                StoreValidateXSD.DataSource = dtDataError
                StoreValidateXSD.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    <DirectMethod>
    Sub Download()
        If Filetodownload <> "" Then
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(Filetodownload))
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "application/zip"
            Response.BinaryWrite(IO.File.ReadAllBytes(Filetodownload))
            Response.End()

            ''06-Jan-2022 Felix , hapus setelah download
            If IO.File.Exists(Filetodownload) Then
                IO.File.Delete(Filetodownload)
            End If

            '5-Aug-2021 Adi : kosongkan agar tidak ke-download saat invalid
            Filetodownload = ""
        End If
    End Sub
End Class