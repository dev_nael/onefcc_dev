﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CRS_Domestic_AllForm_BU.aspx.vb" Inherits="CRS_CRSDomestic_CRS_Domestic_AllForm_BU" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };
        
        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
        //var prepareCommandCollectionPengendaliEntitas_Disable = function (grid, toolbar, rowIndex, record) {
        //    var ButtonPengendaliEntitasEdit = toolbar.items.get(0);
        //    var ButtonPengendaliEntitasDelete = toolbar.items.get(2);
        //    ButtonPengendaliEntitasEdit.setDisabled(true)
        //    ButtonPengendaliEntitasDelete.setDisabled(true)
        //}

        //var prepareCommandCollectionPengendaliEntitas_Enable = function (grid, toolbar, rowIndex, record) {
        //    var ButtonPengendaliEntitasEdit = toolbar.items.get(0);
        //    var ButtonPengendaliEntitasDelete = toolbar.items.get(2);
        //    ButtonPengendaliEntitasEdit.setDisabled(false)
        //    ButtonPengendaliEntitasDelete.setDisabled(false)
        //}
    </script>
   <%-- <style type="text/css">
        .x-form-item-label-text { width : 230px !important; }
        .x-form-item-label.x-form-item-label-default { width : 230px !important; }
    </style>--%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="" Hidden="false">
        <DockedItems>
            <ext:Toolbar ID="ToolbarInput" ClientIDMode="Static" runat="server" EnableOverflow="true" Dock="Top" Layout="FitLayout" MaxHeight="200"  >                
                <Items>
                      <ext:InfoPanel ID="infoValidationResultPanelHeader" ClientIDMode="Static" runat="server" UI="Warning" Layout="AnchorLayout"  Title="Validation Result" AutoScroll="true" IconCls="#Error" MarginSpec="10 0" Border="True" Hidden="true" Collapsible="true"></ext:InfoPanel>
                </Items>
                </ext:Toolbar>                                                     
        </DockedItems>
        <Items>
            <ext:Panel runat="server" Title="Report Header" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="PanelReportHeader" Border="true" BodyPadding="10">
                <Content>
                    <ext:DisplayField ID="Report_Header_ID" LabelWidth="250" runat="server" FieldLabel="ID" AnchorHorizontal="80%" />
                    <ext:DisplayField ID="Report_Header_BU" LabelWidth="250" runat="server" FieldLabel="Business Unit Code" AnchorHorizontal="80%" />
                    <ext:TextField ID="Report_Header_BU_Input" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Business Code" AnchorHorizontal="80%" MaxLength="50" EnforceMaxLength="true" Hidden="true"/>
                    <ext:DisplayField ID="Report_Header_Status" LabelWidth="250" runat="server" FieldLabel="Status" AnchorHorizontal="80%" />
                    <ext:TextField ID="Report_Header_NPWP" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="NPWP Lembaga Keuangan" AnchorHorizontal="80%" MaxLength="16" EnforceMaxLength="true" MaskRe="/[0-9]/" Editable="false" FieldStyle="background-color:#ddd;"></ext:TextField>
                    <ext:NumberField ID="Report_Header_JumlahRekening" LabelWidth="250" runat="server" FieldLabel="Jumlah Data Rekening" AnchorHorizontal="80%" Editable="false" FormatText="#,##0" FieldStyle="background-color:#ddd;"></ext:NumberField>
                    <ext:NumberField ID="Report_Header_JumlahPengendaliEntitas" LabelWidth="250" runat="server" FieldLabel="Jumlah Data Pengendali Entitas" AnchorHorizontal="80%" Editable="false" FormatText="#,##0" FieldStyle="background-color:#ddd;"></ext:NumberField>
                    <%--<ext:NumberField ID="Report_Header_JumlahNilaiSaldo" LabelWidth="250" runat="server" FieldLabel="Jumlah Nilai Saldo" AnchorHorizontal="80%" Editable="false" FormatText="#,##0"></ext:NumberField>--%>
                    <ext:NumberField ID="Report_Header_JumlahNilaiSaldo" LabelWidth="250" runat="server" FieldLabel="Jumlah Nilai Saldo" AnchorHorizontal="80%" Editable="false" FormatText="#,##0" FieldStyle="background-color:#ddd;"></ext:NumberField>
                    <ext:TextField ID="Report_Header_PeriodeLaporan" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Periode Laporan" AnchorHorizontal="80%" MaxLength="4" EnforceMaxLength="true" MaskRe="/[0-9]/" Editable="false" FieldStyle="background-color:#ddd;"></ext:TextField>
                    <%--<ext:DateField ID="Report_Header_TanggalPenyampaian" LabelWidth="250" runat="server" FieldLabel="Tanggal Penyampaian" AnchorHorizontal="80%" Format="dd-MMM-yyyy" Editable="false" FieldStyle="background-color:#ddd;"></ext:DateField>--%>
                    <%--<ext:TextField ID="Report_Header_NomorTandaTerimaElektronik" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Nomor Tanda Terima Eliktronik" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>--%>
                    <ext:Panel runat="server" Title="Nomor Tanda Terima Elektronik" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="PanelRefNum" Border="true" BodyPadding="10" MarginSpec="10 0 0 0">
                        <Content>
                            <ext:GridPanel ID="GridPanelRefNum" runat="server" EmptyText="No Available Data" Border="false" >
                                <View>
                                    <ext:GridView runat="server" EnableTextSelection="true" />
                                </View>
                                <Store>
                                    <ext:Store ID="StoreRefNum" runat="server" PageSize="10" IsPagingStore="true">
                                        <Model>
                                            <ext:Model runat="server" ID="Model2" IDProperty="PK_CRS_DOMESTIC_REF_NUM_ID">
                                                <Fields>
                                                    <ext:ModelField Name="PK_CRS_DOMESTIC_REF_NUM_ID" Type="Int"></ext:ModelField>
                                                    <ext:ModelField Name="YearPeriod" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="RefNum" Type="String"></ext:ModelField>
                                                    <ext:ModelField Name="TanggalPelaporan" Type="Date"></ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Text="No"></ext:RowNumbererColumn>
                                        <ext:Column ID="Column8" runat="server" DataIndex="YearPeriod" Text="Periode" Flex="1"></ext:Column>
                                        <ext:Column ID="Column9" runat="server" DataIndex="RefNum" Text="Nomor Referensi" Flex="1"></ext:Column>
                                        <ext:DateColumn ID="Column10" runat="server" DataIndex="TanggalPelaporan" Text="Tanggal Penyampaian" Flex="1"></ext:DateColumn>
                                    </Columns>
                                </ColumnModel>
                                <Plugins>
                                    <%--<ext:FilterHeader ID="FilterHeader3" runat="server" Remote="true"></ext:FilterHeader>--%>
                                    <ext:FilterHeader runat="server" ></ext:FilterHeader>
                                </Plugins>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                                </BottomBar>
                            </ext:GridPanel>
                        </Content>
                    </ext:Panel>
                </Content>
            </ext:Panel>
            <ext:Panel runat="server" Title="Data Rekening" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="PanelDataRekening" Border="true" BodyPadding="10" MarginSpec="10 0 0 0">
                <Content>
                    <ext:GridPanel ID="GridPanelDataRekening" runat="server" ClientIDMode="Static" AnchorHorizontal="100%" EmptyText="No Available Data" Border="false">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_adddatarekening" Text="Add New Record" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="Btn_adddatarekening_click">
                                                <EventMask ShowMask="true" Msg="Processing..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StoreDataRekening" runat="server" IsPagingStore="true" RemoteFilter="true" RemoteSort="true" PageSize="10" 
			                    OnReadData="StoreDataRekening_ReadData" RemotePaging="true" ClientIDMode="Static">
                                <Model>
                                    <ext:Model runat="server" ID="Model355" IDProperty="PK_CRS_DOMESTIC_DATA_REKENING_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_DOMESTIC_DATA_REKENING_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="NomorCIF" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NomorRekening" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="JnsPemegangRekening" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="StsRekening" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NamaPemegangRek" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="SaldoAtauNilai" Type="Float"></ext:ModelField>
                                            <ext:ModelField Name="IsValid" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
	                        <Sorters>
	                        </Sorters>
	                        <Proxy>
	                            <ext:PageProxy />
	                        </Proxy>
                            </ext:Store>
                        </Store>
                        <Plugins>
                            <%--<ext:FilterHeader ID="grdiheaderfilter" runat="server" Remote="true"></ext:FilterHeader>--%>
                            <ext:FilterHeader ID="Filter_DataRekening" runat="server" Remote="true"></ext:FilterHeader>
                        </Plugins>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn88" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="columncrudDataRekening" runat="server" Text="Action" Flex="1">
                                    <Commands>
                                        <%--<ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>--%>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridcommandDataRekening">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record?After Delete This Record, Data Will be Deleted From Database" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_DOMESTIC_DATA_REKENING_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column312" runat="server" DataIndex="NomorCIF" Text="Nomor CIF" Flex="1"></ext:Column>
                                <ext:Column ID="Column313" runat="server" DataIndex="NomorRekening" Text="Nomor Rekening" Flex="1"></ext:Column>
                                 <ext:Column ID="Column14" runat="server" DataIndex="NamaPemegangRek" Text="Nama" Flex="1"></ext:Column>
                                <ext:Column ID="Column1" runat="server" DataIndex="JnsPemegangRekening" Text="Jenis Pemegang Rekening" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" DataIndex="StsRekening" Text="Status Rekening" Flex="1"></ext:Column>
                                <ext:Column ID="Column3" runat="server" DataIndex="SaldoAtauNilai" Text="Saldo atau Nilai" Flex="1"></ext:Column>
                                <ext:Column ID="Column4" runat="server" DataIndex="IsValid" Text="Is Valid" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" HideRefresh="false" />
                        </BottomBar>
                    </ext:GridPanel>
                </Content>
            </ext:Panel>
            <ext:panel runat="server" ID="pnl_changeslog"  BodyPadding="10" ButtonAlign="Center"  Title="Changes Log" Layout="AnchorLayout" Border="true"  StyleSpec="border-color: #157fcc !important;margin-top:10px" Hidden="true">
                <Items>
                     <ext:GridPanel ID="gp_changeslog" runat="server">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                    
                        <Store>
                            <ext:Store ID="store2" runat="server" IsPagingStore="true" PageSize="10">
                                <Model>
                                    <ext:Model runat="server" ID="Model4" IDProperty="PK_CHANGES_LOG_APPROVAL_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CHANGES_LOG_APPROVAL_ID" Type="Auto"></ext:ModelField>
                                            <ext:ModelField Name="REPORT_ID" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="IdentitasUnik" Type="String"></ext:ModelField>
                                             <ext:ModelField Name="Account_No" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="CIF_NO" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="REKENING_ENTITAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_TBL_REKENING" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_TBL_PENGENDALI_ENTITAS" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FK_Business_Unit_Code" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="FIELD_NAME" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="OLD_VALUE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NEW_VALUE" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="ACTION" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No"></ext:RowNumbererColumn>
                                 <ext:Column ID="Column15" runat="server" DataIndex="REPORT_ID" Text="Report ID" MinWidth="200"></ext:Column>
                                 <ext:Column ID="Column16" runat="server" DataIndex="IdentitasUnik" Text="Identitas Unik" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column17" runat="server" DataIndex="CIF_NO" Text="CIF No" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column26" runat="server" DataIndex="Account_No" Text="Nomor Rekening" MinWidth="200"></ext:Column>
                                <ext:Column ID="Column22" runat="server" DataIndex="REKENING_ENTITAS" Text="Rekening/PengendaliEntitas" MinWidth="150" ></ext:Column>
                               <ext:Column ID="Column18" runat="server" DataIndex="FK_TBL_REKENING" Text="Fk Data Rekening" MinWidth="150"></ext:Column>
                               <ext:Column ID="Column19" runat="server" DataIndex="FK_TBL_PENGENDALI_ENTITAS" Text="Fk Pengendali Entitas" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column20" runat="server" DataIndex="FK_Business_Unit_Code" Text="Business Unit" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column21" runat="server" DataIndex="FIELD_NAME" Text="Field Name" MinWidth="150"></ext:Column>
                                <ext:Column ID="Column23" runat="server" DataIndex="OLD_VALUE" Text="Old Value" MinWidth="350"></ext:Column>
                                <ext:Column ID="Column24" runat="server" DataIndex="NEW_VALUE" Text="New Value" MinWidth="350"></ext:Column>
                                <ext:Column ID="Column25" runat="server" DataIndex="ACTION" Text="Action" MinWidth="150"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:FilterHeader runat="server"></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar5" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                  
                </Items>
            </ext:panel>
            <ext:Panel runat="server" ID="Report_Header_Edit_Action_Panel" Hidden="true" MarginSpec="10 0 0 0">
                <Content>
                    <NDS:NDSDropDownField ID="Report_Header_Edit_Action" LabelWidth="260" ValueField="Value" DisplayField="Text" runat="server" StringField="ValueCB as [Value], TextCB as [Text]" StringTable="VW_CRS_DOMESTIC_Header_Edit_Action" Label="Action" />
                </Content>
            </ext:Panel>
            <%--<ext:ComboBox runat="server" ID="Report_Header_Edit_Action" Editable="false" FieldLabel="Action" LabelWidth="260" AllowBlank="false" MarginSpec="10 0 0 0" Hidden="true" >
                <Items>
                    <ext:ListItem Text="Update Data" Value="Update" Index="1"></ext:ListItem>
                    <ext:ListItem Text="Insert As New Report" Value="Insert" Index="2"></ext:ListItem>
                </Items>
            </ext:ComboBox>--%>
        </Items>
        <Buttons>
            <ext:Button ID="btn_Header_Submit" runat="server" Icon="Disk" Text="Submit">
                <DirectEvents>
                    <Click OnEvent="Btn_Header_Submit_Click">
                        <EventMask ShowMask="true" Msg="Saving Data...">
                        </EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Header_Reject" runat="server" Icon="Decline" Text="Reject" Hidden="true" >
                <DirectEvents>
                    <Click OnEvent="Btn_Header_Reject_Click">
                        <Confirmation Message="Are you sure to Reject this Data ?" ConfirmRequest="true" Title="Delete"></Confirmation>
                        <EventMask ShowMask="true" Msg="Reject Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_Header_Back" runat="server" Icon="PageBack" Text="Back">
                <DirectEvents>
                    <Click OnEvent="Btn_Header_Back_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="Panelconfirmation" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="HBoxLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true">
        <Defaults>
            <ext:Parameter Name="margins" Value="0 5 0 0" Mode="Value" />
        </Defaults>
        <LayoutConfig>
            <ext:HBoxLayoutConfig Padding="5" Align="Middle" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Label ID="LblConfirmation" runat="server" Align="center" Cls="NawaLabel"></ext:Label>
        </Items>
        <Buttons>
            <ext:Button ID="BtnConfirmation" runat="server" Text="OK" Icon="ApplicationGo">
                <DirectEvents>
                    <Click OnEvent="Btn_Confirmation_Click"></Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:FormPanel ID="FormPanelDataRekening" BodyPadding="20" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" DefaultAnchor="100%" Hidden="true" AutoScroll="true" Title="">
        <DockedItems>
            <ext:Toolbar ID="Toolbar1" ClientIDMode="Static" runat="server" EnableOverflow="true" Dock="Top" Layout="FitLayout" MaxHeight="200"  >                
                <Items>
                      <ext:InfoPanel ID="infoValidationResultPanelRekening" ClientIDMode="Static" runat="server" UI="Warning" Layout="AnchorLayout"  Title="Validation Result" AutoScroll="true" IconCls="#Error" MarginSpec="10 0" Border="True" Hidden="true" Collapsible="true"></ext:InfoPanel>
                </Items>
                </ext:Toolbar>                                                     
        </DockedItems>
        <Items>
            <ext:Panel runat="server" Title="Data Rekening" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="FormPanelDataRekening_PanelDataRekening" Border="true" BodyPadding="10">
                <Content>
                    <ext:TextField ID="FormPanelDataRekening_PanelDataRekening_BusinessUnitCode" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Business Code" AnchorHorizontal="80%" MaxLength="50" EnforceMaxLength="true" ReadOnly="true" />
                    <ext:TextField ID="FormPanelDataRekening_PanelDataRekening_NPWPPengirim" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="NPWP Pengirim" AnchorHorizontal="80%" MaxLength="15" EnforceMaxLength="true" MaskRe="/[0-9]/" Editable="false" FieldStyle="background-color:#ddd;"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelDataRekening_LembagaKeuanganPelapor" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="NPWP Lembaga Keuangan Pelapor" AnchorHorizontal="80%" MaxLength="15" EnforceMaxLength="true" MaskRe="/[0-9]/" Editable="false" FieldStyle="background-color:#ddd;"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelDataRekening_IdentitasUnik" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Identitas Unik" AnchorHorizontal="80%" MaxLength="27" EnforceMaxLength="true" MaskRe="/[0-9]/" Editable="false" FieldStyle="background-color:#ddd;"></ext:TextField>
                    <NDS:NDSDropDownField ID="FormPanelDataRekening_PanelDataRekening_JenisData" LabelWidth="250" ValueField="code" DisplayField="DataName" runat="server" StringField="code, DataName" StringTable="VW_CRS_DOMESTIC_JENIS_DATA" Label="Jenis Data" AnchorHorizontal="80%" AllowBlank="false" OnOnValueChanged="onJenisDataChange" />
                    <ext:TextField ID="FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Identitas Unik Koreksi" AnchorHorizontal="80%" MaxLength="27" EnforceMaxLength="true" MaskRe="/[0-9]/" Editable="false" FieldStyle="background-color:#ddd;" Hidden="true" ></ext:TextField>
                    <NDS:NDSDropDownField ID="FormPanelDataRekening_PanelDataRekening_JenisLembagaKeuangan" LabelWidth="250" ValueField="code" DisplayField="DataName" runat="server" StringField="code, DataName" StringTable="VW_CRS_DOMESTIC_LJK_TYPE" Label="Jenis Lembaga Keuangan" AnchorHorizontal="80%" AllowBlank="false"/>
                    <ext:TextField ID="FormPanelDataRekening_PanelDataRekening_NomorCIF" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Nomor CIF" AnchorHorizontal="80%" MaxLength="50" EnforceMaxLength="true">
                        <DirectEvents>
                            <FocusLeave OnEvent="onFocusLeaveCIFandRekening"/>
                        </DirectEvents>
                    </ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelDataRekening_NomorRekening" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Nomor Rekening" AnchorHorizontal="80%" MaxLength="50" EnforceMaxLength="true">
                        <DirectEvents>
                            <FocusLeave OnEvent="onFocusLeaveCIFandRekening"/>
                        </DirectEvents>
                    </ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelDataRekening_NomorCIFandRekening" LabelWidth="250" runat="server" FieldLabel="Nomor CIF & Nomor Rekening" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true" Editable="false" Enabled="false" FieldStyle="background-color:#ddd;"></ext:TextField>
                    <NDS:NDSDropDownField ID="FormPanelDataRekening_PanelDataRekening_StatusRekening" LabelWidth="250" ValueField="code" DisplayField="DataName" runat="server" StringField="code, DataName" StringTable="VW_CRS_DOMESTIC_STATUS_REKENING" Label="Status Rekening" AnchorHorizontal="80%" AllowBlank="false"/>
                    <NDS:NDSDropDownField ID="FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening" LabelWidth="250" ValueField="code" DisplayField="DataName" runat="server" StringField="code, DataName" StringTable="VW_CRS_DOMESTIC_JENIS_PEMEGANG_REKENING" Label="Jenis Pemegang Rekening" AnchorHorizontal="80%" AllowBlank="false" OnOnValueChanged="FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening_OnValueChange"/>
                    <NDS:NDSDropDownField ID="FormPanelDataRekening_PanelDataRekening_MataUang" LabelWidth="250" ValueField="code" DisplayField="DataName" runat="server" StringField="code, DataName" StringTable="VW_CRS_DOMESTIC_CURRENCY" Label="Mata Uang" AnchorHorizontal="80%" AllowBlank="false"/>
                    <ext:NumberField  ID="FormPanelDataRekening_PanelDataRekening_SaldoatauNilai" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Saldo atau Nilai" AnchorHorizontal="80%" FormatText="#,##0" FieldStyle="text-align: left;"></ext:NumberField>
                    <ext:NumberField ID="FormPanelDataRekening_PanelDataRekening_Deviden" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Deviden" AnchorHorizontal="80%" FieldStyle="text-align: left;"></ext:NumberField>
                    <ext:NumberField ID="FormPanelDataRekening_PanelDataRekening_Bunga" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Bunga" AnchorHorizontal="80%" FieldStyle="text-align: left;"></ext:NumberField>
                    <ext:NumberField ID="FormPanelDataRekening_PanelDataRekening_PenghasilanBruto" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Penghasilan Bruto" AnchorHorizontal="80%" FieldStyle="text-align: left;"></ext:NumberField>
                    <ext:NumberField ID="FormPanelDataRekening_PanelDataRekening_PenghasilanLainnya" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Penghasilan Lainnya" AnchorHorizontal="80%" FieldStyle="text-align: left;"></ext:NumberField>
                    <%--<ext:TextField ID="FormPanelDataRekening_PanelDataRekening_SaldoatauNilai" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Saldo atau Nilai" AnchorHorizontal="80%" FormatText="#,##0"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelDataRekening_Deviden" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Deviden" AnchorHorizontal="80%"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelDataRekening_Bunga" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Bunga" AnchorHorizontal="80%"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelDataRekening_PenghasilanBruto" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Penghasilan Bruto" AnchorHorizontal="80%"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelDataRekening_PenghasilanLainnya" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Penghasilan Lainnya" AnchorHorizontal="80%"></ext:TextField>--%>
                </Content>
            </ext:Panel>
            <ext:Panel runat="server" Title="Informasi Pemegang Rekening " Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="FormPanelDataRekening_PanelInformasi" Border="true" BodyPadding="10" MarginSpec="10 0 0 0" Hidden="true">
                <Content>
                    <ext:TextField ID="FormPanelDataRekening_PanelInformasi_Nama" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Nama" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelInformasi_NamaLainnya" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Nama Lainnya" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelInformasi_NPWP" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="NPWP" AnchorHorizontal="80%" MaskRe="/[0-9]/" MaxLength="15" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelInformasi_SIUP" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="SIUP" Hidden="true" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelInformasi_SITU" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="SITU" Hidden="true" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelInformasi_AKTA" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="AKTA" Hidden="true" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelInformasi_NIK" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="NIK" AnchorHorizontal="80%" MaxLength="16" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelInformasi_SIM" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="SIM" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="FormPanelDataRekening_PanelInformasi_Paspor" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Paspor" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <NDS:NDSDropDownField ID="FormPanelDataRekening_PanelInformasi_Kewarganegaraan" LabelWidth="250" ValueField="code" DisplayField="DataName" runat="server" StringField="code, DataName" StringTable="VW_CRS_DOMESTIC_COUNTRY" Label="Kewarganegaraan" AnchorHorizontal="80%" AllowBlank="true"/>
                    <%--<ext:TextField ID="FormPanelDataRekening_PanelInformasi_Kewarganegaraan" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Kewarganegaraan" AnchorHorizontal="80%" MaxLength="50" EnforceMaxLength="true"></ext:TextField>--%>
                    <ext:TextField ID="FormPanelDataRekening_PanelInformasi_TempatLahir" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <ext:DateField ID="FormPanelDataRekening_PanelInformasi_TanggalLahir" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Tanggal Lahir" AnchorHorizontal="80%" Format="dd-MMM-yyyy" MinDate="01-01-1753"></ext:DateField>
                    <ext:TextArea ID="FormPanelDataRekening_PanelInformasi_AlamatDomisili" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Alamat Domisili" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"/>
                    <ext:TextArea ID="FormPanelDataRekening_PanelInformasi_AlamatUsaha" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Alamat Usaha" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"/>
                    <ext:TextArea ID="FormPanelDataRekening_PanelInformasi_AlamatKorespondensi" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Alamat Korespondensi" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"/>
                </Content>
            </ext:Panel>
            <ext:Panel runat="server" Title="Data Pengendali Entitas" Collapsible="true" Layout="AnchorLayout" AnchorHorizontal="100%" ID="FormPanelDataRekening_PanelDataPengendaliEntitas" Border="true" BodyPadding="10" MarginSpec="10 0 0 0" Hidden="true" >
                <Content>
                    <ext:GridPanel ID="GridPanelPengendaliEntitas" runat="server" EmptyText="No Available Data" Border="false">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true"/>
                        </View>
                        <TopBar>
                            <ext:Toolbar runat="server">
                                <Items>
                                    <ext:Button runat="server" ID="btn_adddatapengendalientitas" Text="Add New Record" Icon="Add">
                                        <DirectEvents>
                                            <Click OnEvent="Btn_adddatapengendalientitas_click">
                                                <EventMask ShowMask="true" Msg="Processing..."></EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="StorePengendaliEntitas" runat="server" PageSize="10" IsPagingStore="true">
                                <Model>
                                    <ext:Model runat="server" ID="Model1" IDProperty="PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="NamaCP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Kewarganegaraan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NIKCP" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="columncrudPengendaliEntitas" runat="server" Text="Action" Flex="1">
                                    <Commands>
                                        <%--<ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <ext:GridCommand Text="Delete" CommandName="Delete" Icon="Delete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>--%>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridcommandPengendaliEntitas">
                                            <EventMask ShowMask="true"></EventMask>
                                            <Confirmation BeforeConfirm="if (command=='Edit'|| command=='Detail') return false;" ConfirmRequest="true" Message="Are You sure to Delete This Record?" Title="Delete"></Confirmation>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column5" runat="server" DataIndex="NamaCP" Text="Nama" Flex="1"></ext:Column>
                                <ext:Column ID="Column6" runat="server" DataIndex="Kewarganegaraan" Text="Kewarganegaraan" Flex="1"></ext:Column>
                                <ext:Column ID="Column7" runat="server" DataIndex="NIKCP" Text="NIK" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <%--<ext:FilterHeader ID="FilterHeader1" runat="server" Remote="true"></ext:FilterHeader>--%>
                            <ext:FilterHeader runat="server" ></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel ID="GridPanelPengendaliEntitas_KhususDetail" runat="server" EmptyText="No Available Data" Border="false" Hidden="true" >
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true"/>
                        </View>
                        <Store>
                            <ext:Store ID="StorePengendaliEntitas_KhususDetail" runat="server" PageSize="10" IsPagingStore="true">
                                <Model>
                                    <ext:Model runat="server" ID="Model3" IDProperty="PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="NamaCP" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="Kewarganegaraan" Type="String"></ext:ModelField>
                                            <ext:ModelField Name="NIKCP" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Text="No"></ext:RowNumbererColumn>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Action" Flex="1">
                                    <Commands>
                                        <%--<ext:GridCommand Text="Edit" CommandName="Edit" Icon="ApplicationEdit" MinWidth="70">
                                            <ToolTip Text="Edit"></ToolTip>
                                        </ext:GridCommand>--%>
                                        <ext:GridCommand Text="Detail" CommandName="Detail" Icon="ApplicationViewDetail" MinWidth="70">
                                            <ToolTip Text="Detail"></ToolTip>
                                        </ext:GridCommand>
                                        <%--<ext:GridCommand Text="Delete" CommandName="Delete" Icon="Delete" MinWidth="70">
                                            <ToolTip Text="Delete"></ToolTip>
                                        </ext:GridCommand>--%>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridcommandPengendaliEntitas">
                                            <EventMask ShowMask="true"></EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="unikkey" Value="record.data.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:Column ID="Column11" runat="server" DataIndex="NamaCP" Text="Nama" Flex="1"></ext:Column>
                                <ext:Column ID="Column12" runat="server" DataIndex="Kewarganegaraan" Text="Kewarganegaraan" Flex="1"></ext:Column>
                                <ext:Column ID="Column13" runat="server" DataIndex="NIKCP" Text="NIK" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <%--<ext:FilterHeader ID="FilterHeader2" runat="server" Remote="true"></ext:FilterHeader>--%>
                            <ext:FilterHeader runat="server" ></ext:FilterHeader>
                        </Plugins>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar3" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="btn_DataRekening_save" runat="server" Text="Save" Icon="Disk">
                <DirectEvents>
                    <Click OnEvent="Btn_DataRekening_save_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_DataRekening_cancel" runat="server" Text="Cancel" Icon="Cancel">
                <DirectEvents>
                    <Click OnEvent="Btn_DataRekening_cancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
    <ext:Window ID="WindowPengendaliEntitas" Layout="AnchorLayout" Title="Pengendali Entitas" runat="server" Maximizable="true" Modal="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center" Hidden="true" MinWidth="1000" MinHeight="500">
        <Content>
            <ext:Panel runat="server" Layout="AnchorLayout" AnchorHorizontal="100%" ID="WindowPengendaliEntitas_PanelInformasi" Border="false">
                <Content>
                    <ext:TextField ID="WindowPengendaliEntitas_Nama" LabelSeparator="" LabelWidth="250" AllowBlank="false" runat="server" FieldLabel="Nama" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <NDS:NDSDropDownField ID="WindowPengendaliEntitas_Kewarganegaraan" LabelWidth="250" ValueField="code" DisplayField="DataName" runat="server" StringField="code, DataName" StringTable="VW_CRS_DOMESTIC_COUNTRY" Label="Kewarganegaraan" AnchorHorizontal="80%" AllowBlank="false"/>
                    <ext:TextField ID="WindowPengendaliEntitas_TempatLahir" LabelSeparator="" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Tempat Lahir" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <ext:DateField ID="WindowPengendaliEntitas_TanggalLahir" LabelSeparator="" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Tanggal Lahir" AnchorHorizontal="80%" Format="dd-MMM-yyyy" MinDate="01-01-1753"></ext:DateField>
                    <ext:TextField ID="WindowPengendaliEntitas_NPWPorTIN" LabelSeparator="" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="NPWP atau TIN" AnchorHorizontal="80%" MaxLength="16" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="WindowPengendaliEntitas_NIK" LabelSeparator="" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="NIK" AnchorHorizontal="80%" MaxLength="16" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="WindowPengendaliEntitas_SIM" LabelSeparator="" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="SIM" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextField ID="WindowPengendaliEntitas_Paspor" LabelSeparator="" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Paspor" AnchorHorizontal="80%" MaxLength="100" EnforceMaxLength="true"></ext:TextField>
                    <ext:TextArea ID="WindowPengendaliEntitas_AlamatDomisili" LabelSeparator="" AllowBlank="false" LabelWidth="250" runat="server" FieldLabel="Alamat Domisili" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"/>
                    <ext:TextArea ID="WindowPengendaliEntitas_AlamatKorespondensi" LabelSeparator="" AllowBlank="true" LabelWidth="250" runat="server" FieldLabel="Alamat Korespondensi" AnchorHorizontal="80%" MaxLength="255" EnforceMaxLength="true"/>
                </Content>
            </ext:Panel>
        </Content>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.8, height: size.height * 0.8});" />
            <Resize Handler="#{WindowPengendaliEntitas}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="btn_PengendaliEntitas_save" runat="server" Icon="Disk" Text="Save">
                <DirectEvents>
                    <Click OnEvent="Btn_PengendaliEntitas_save_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="btn_PengendaliEntitas_cancel" runat="server" Icon="Cancel" Text="Cancel">
                <DirectEvents>
                    <Click OnEvent="Btn_PengendaliEntitas_cancel_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:Window ID="WindowBackConfirmation" Layout="AnchorLayout" Title="Confirmation" runat="server" Modal="true" BodyStyle="padding:20px" AutoScroll="true" ButtonAlign="Center" Hidden="true">
        <Content>
            <ext:Label ID="WindowBackConfirmation_Label" runat="server" Align="center" Html="Do You Want to Keep Changed Data ?<br/>(No, All Changed Data will be Returned)" Anchor="100%"></ext:Label>
        </Content>
        <Listeners>
            <BeforeShow Handler="var size = Ext.getBody().getSize(); this.setSize({ width: size.width * 0.3, height: size.height * 0.3});" />
            <Resize Handler="#{WindowPengendaliEntitas}.center()" />
        </Listeners>
        <Buttons>
            <ext:Button ID="WindowBackConfirmation_Yes" runat="server" Icon="Disk" Text="Yes">
                <DirectEvents>
                    <Click OnEvent="WindowBackConfirmation_Yes_Click">
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="WindowBackConfirmation_No" runat="server" Icon="Cancel" Text="No">
                <DirectEvents>
                    <Click OnEvent="WindowBackConfirmation_No_Click">
                        <EventMask ShowMask="true" Msg="Loading Data..."></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Window>
</asp:Content>

