﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeFile="CRS_Domestic_Generate.aspx.vb" Inherits="CRS_Domestic_Generate" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="~/NDSDropDownField.ascx" TagPrefix="nds" TagName="NDSDropDownField" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        Ext.net.FilterHeader.behaviour.string[0].match = function (recordValue, matchValue) {
            return (Ext.net.FilterHeader.behaviour.getStrValue(recordValue) || "").indexOf(matchValue) > -1;
        };
        
        Ext.net.FilterHeader.behaviour.string[0].serialize = function (value) {
            return {
                type: "string",
                op: "*",
                value: value
            };
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ext:FormPanel ID="FormPanelInput" BodyPadding="10" runat="server" ClientIDMode="Static" Border="false" Frame="false" Layout="AnchorLayout" ButtonAlign="Center" AutoScroll="true" Title="CRS Domestic Report - Generate Report" Hidden="false">
        <Items>
           <%-- <ext:ComboBox LabelWidth="250" runat="server" ID="cb_generate_export" Editable="false" FieldLabel="Format" AllowBlank="false">
                <Items>
                    <ext:ListItem Text="Excel" Value="Excel"></ext:ListItem>
                    <ext:ListItem Text="XML" Value="XML"></ext:ListItem>
                </Items>
            </ext:ComboBox>--%>
            <ext:Panel runat="server" ID="cb_generate_export_Panel" Hidden="false" MarginSpec="10 10 10 10" Layout="AnchorLayout" AnchorHorizontal="100%">
                <Content>
                    <NDS:NDSDropDownField ID="cb_generate_export" LabelWidth="250" ValueField="Value" DisplayField="Text" runat="server" StringField="ValueCB as [Value], TextCB as [Text]" StringTable="VW_CRS_Domestic_Excel_Or_XML" Label="Format" AllowBlank="false" AnchorHorizontal="40%" />
                    <ext:DisplayField ID="Df_generate_id" LabelWidth="250" runat="server" FieldLabel="ID" AnchorHorizontal="80%"  />
                    <ext:DisplayField ID="Df_generate_year" LabelWidth="250" runat="server" FieldLabel="Year" AnchorHorizontal="80%"  />
                    <ext:DisplayField ID="Df_generate_statusAPI" LabelWidth="250" runat="server" FieldLabel="Status API" AnchorHorizontal="80%"  />
                    <ext:DisplayField ID="Df_generate_ErrorAPI" LabelWidth="250" runat="server" FieldLabel="Error Message API" AnchorHorizontal="80%"  />
                </Content>
            </ext:Panel>
            <ext:FormPanel ID="Fp_validateXSD" runat="server" AnchorHorizontal="100%" BodyPadding="10" DefaultAlign="center" AutoScroll="true" Title="Validation Schema">
                <Content>
                    <ext:GridPanel ID="Gp_ValidateXSD_ErrorMessage" ClientIDMode="Static" runat="server" Title="" AutoScroll="true" Border="true" MinHeight="200">
                        <View>
                            <ext:GridView runat="server" EnableTextSelection="true" />
                        </View>
                        <Store>
                            <ext:Store ID="StoreValidateXSD" runat="server" PageSize="10" IsPagingStore="true">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PK_CRS_DOMESTIC_HEADER_ID" Type="Int"></ext:ModelField>
                                            <ext:ModelField Name="ErrorValidation" Type="String"></ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn4" runat="server" Text="No" MinWidth="30"></ext:RowNumbererColumn>
                                <ext:Column ID="Column2" runat="server" DataIndex="ErrorValidation" Text="Error Message" MinWidth="300" CellWrap="true" Flex="1"></ext:Column>
                            </Columns>
                        </ColumnModel>
                       <%-- <Plugins>
                            <ext:FilterHeader ID="FilterHeader3" runat="server" Remote="true"></ext:FilterHeader>
                            <ext:FilterHeader runat="server" ></ext:FilterHeader>
                        </Plugins>--%>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar4" runat="server" HideRefresh="True" />
                        </BottomBar>
                    </ext:GridPanel>
                </Content>
            </ext:FormPanel>
            
            <ext:Panel ID="panel_TaskManager" runat="server" MarginSpec="10 0" Border="true" BodyPadding="10" Hidden="true">
                <Content>
                    <ext:TaskManager ID="TaskManager1" runat="server">
                        <Tasks>
                            <ext:Task
                                TaskID="refreshStatus"
                                Interval="20000"
                                AutoRun="false"
                                >
                                <DirectEvents>
                                    <Update OnEvent="LoadData">
                                        <%--<EventMask Msg="Loading..." ShowMask="true" MinDelay="200"></EventMask>--%>
                                    </Update>
                                </DirectEvents>
                            </ext:Task>
                        </Tasks>
                    </ext:TaskManager>
                </Content>
            </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button ID="Btn_generate_Close_ClickBbtn_generate_Validate" runat="server" Icon="Disk" Text="Validate Schema">
                <DirectEvents>
                    <Click OnEvent="Btn_generate_Validate_Click">
                       <%-- <EventMask Msg="Loading..." ShowMask="true"></EventMask>--%>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="Btn_generate_Generate" runat="server" Icon="Disk" Text="Generate">
                <DirectEvents>
                    <Click OnEvent="Btn_generate_Generate_Click">
                       <%-- <EventMask Msg="Loading..." ShowMask="true"></EventMask>--%>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="Btn_generate_Download" runat="server" Icon="Disk" Text="Download" Hidden="true">
                <DirectEvents>
                    <Click OnEvent="Btn_generate_Download_Click" Success="NawadataDirect.Download({isUpload : true});">
                       <%-- <EventMask Msg="Loading..." ShowMask="true"></EventMask>--%>
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="Btn_generate_Close" runat="server" Icon="Cancel" Text="Close">
                <DirectEvents>
                    <Click OnEvent="Btn_generate_Close_Click">
                        <EventMask Msg="Loading..." ShowMask="true"></EventMask>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</asp:Content>

