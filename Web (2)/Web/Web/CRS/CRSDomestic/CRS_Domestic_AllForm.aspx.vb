﻿Imports Ext
Imports Elmah
Imports System.Data
Imports SiPendarBLL
Imports SiPendarDAL
Imports System.Data.SqlClient
Imports System.Data.Entity

Partial Class CRS_Domestic_AllForm
    Inherits ParentPage

    Public Property IDModule() As String
        Get
            Return Session("CRS_Domestic_AllForm.IDModule")
        End Get
        Set(ByVal value As String)
            Session("CRS_Domestic_AllForm.IDModule") = value
        End Set
    End Property

    Public Property IDUnik() As Long
        Get
            Return Session("CRS_Domestic_AllForm.IDUnik")
        End Get
        Set(ByVal value As Long)
            Session("CRS_Domestic_AllForm.IDUnik") = value
        End Set
    End Property

    Public Property IDUnikAditional() As Long
        Get
            Return Session("CRS_Domestic_AllForm.IDUnikAditional")
        End Get
        Set(ByVal value As Long)
            Session("CRS_Domestic_AllForm.IDUnikAditional") = value
        End Set
    End Property

    Public Property FormAction() As String
        Get
            Return Session("CRS_Domestic_AllForm.FormAction")
        End Get
        Set(ByVal value As String)
            Session("CRS_Domestic_AllForm.FormAction") = value
        End Set
    End Property

    Public Property ParameterNPWP() As String
        Get
            Return Session("CRS_Domestic_AllForm.ParameterNPWP")
        End Get
        Set(ByVal value As String)
            Session("CRS_Domestic_AllForm.ParameterNPWP") = value
        End Set
    End Property

    Public Property StatusReportOld() As String
        Get
            Return Session("CRS_Domestic_AllForm.StatusReportOld")
        End Get
        Set(ByVal value As String)
            Session("CRS_Domestic_AllForm.StatusReportOld") = value
        End Set
    End Property

    Public Property JenisDataBeforeChange() As String
        Get
            Return Session("CRS_Domestic_AllForm.JenisDataBeforeChange")
        End Get
        Set(ByVal value As String)
            Session("CRS_Domestic_AllForm.JenisDataBeforeChange") = value
        End Set
    End Property

    Public Property IsJenisDataChange() As Boolean
        Get
            Return Session("CRS_Domestic_AllForm.IsJenisDataChange")
        End Get
        Set(ByVal value As Boolean)
            Session("CRS_Domestic_AllForm.IsJenisDataChange") = value
        End Set
    End Property

    Public Property HeaderClass() As CRSBLL.CRS_Domestic_Report_Class
        Get
            Return Session("CRS_Domestic_AllForm.HeaderClass")
        End Get
        Set(ByVal value As CRSBLL.CRS_Domestic_Report_Class)
            Session("CRS_Domestic_AllForm.HeaderClass") = value
        End Set
    End Property

    Public Property RekeningClass() As CRSBLL.CRS_Data_Rekening_Class
        Get
            Return Session("CRS_Domestic_AllForm.RekeningClass")
        End Get
        Set(ByVal value As CRSBLL.CRS_Data_Rekening_Class)
            Session("CRS_Domestic_AllForm.RekeningClass") = value
        End Set
    End Property

    Public Property DataPengendaliEntitas() As CRSDAL.CRS_DOMESTIC_PENGENDALI_ENTITAS
        Get
            Return Session("CRS_Domestic_AllForm.DataPengendaliEntitas")
        End Get
        Set(ByVal value As CRSDAL.CRS_DOMESTIC_PENGENDALI_ENTITAS)
            Session("CRS_Domestic_AllForm.DataPengendaliEntitas") = value
        End Set
    End Property

    Public Property RekeningAction() As CRSBLL.CRSDomesticBLL.enumActionForm
        Get
            Return Session("CRS_Domestic_AllForm.RekeningAction")
        End Get
        Set(ByVal value As CRSBLL.CRSDomesticBLL.enumActionForm)
            Session("CRS_Domestic_AllForm.RekeningAction") = value
        End Set
    End Property

    Public Property PengendaliEntitasAction() As CRSBLL.CRSDomesticBLL.enumActionForm
        Get
            Return Session("CRS_Domestic_AllForm.PengendaliEntitasAction")
        End Get
        Set(ByVal value As CRSBLL.CRSDomesticBLL.enumActionForm)
            Session("CRS_Domestic_AllForm.PengendaliEntitasAction") = value
        End Set
    End Property

    Public Property PengendaliEntitasPKCounter() As Integer
        Get
            Return Session("CRS_Domestic_AllForm.PengendaliEntitasPKCounter")
        End Get
        Set(ByVal value As Integer)
            Session("CRS_Domestic_AllForm.PengendaliEntitasPKCounter") = value
        End Set
    End Property

    Public Property IsReportAlreadyEdited() As Integer
        Get
            Return Session("CRS_Domestic_AllForm.IsReportAlreadyEdited")
        End Get
        Set(ByVal value As Integer)
            Session("CRS_Domestic_AllForm.IsReportAlreadyEdited") = value
        End Set
    End Property

    Public Property DataModuleApproval() As CRSDAL.ModuleApproval
        Get
            Return Session("CRS_Domestic_AllForm.DataModuleApproval")
        End Get
        Set(ByVal value As CRSDAL.ModuleApproval)
            Session("CRS_Domestic_AllForm.DataModuleApproval") = value
        End Set
    End Property

    'Public Property IsPengendaliEntitasEdited() As Boolean
    '    Get
    '        Return Session("CRS_Domestic_AllForm.IsPengendaliEntitasEdited")
    '    End Get
    '    Set(ByVal value As Boolean)
    '        Session("CRS_Domestic_AllForm.IsPengendaliEntitasEdited") = value
    '    End Set
    'End Property

    Public Property IsDataRekeningEdited() As Boolean
        Get
            Return Session("CRS_Domestic_AllForm.IsDataRekeningEdited")
        End Get
        Set(ByVal value As Boolean)
            Session("CRS_Domestic_AllForm.IsDataRekeningEdited") = value
        End Set
    End Property

    Sub ClearSession()
        HeaderClass = New CRSBLL.CRS_Domestic_Report_Class
        RekeningClass = New CRSBLL.CRS_Data_Rekening_Class
        DataPengendaliEntitas = New CRSDAL.CRS_DOMESTIC_PENGENDALI_ENTITAS
        DataModuleApproval = New CRSDAL.ModuleApproval
        ParameterNPWP = ""
        'Report_Header_Edit_Action.SelectedItem.Text = "Update Data"
        'Report_Header_Edit_Action.SelectedItem.Value = "Update"
        'Report_Header_Edit_Action.SelectedItem.Index = 1
        Report_Header_Edit_Action.SetTextWithTextValue("Update", "Update Data")
        IDUnik = Nothing
        StatusReportOld = Nothing
        IsReportAlreadyEdited = 0
        IsJenisDataChange = False
        IsDataRekeningEdited = False
    End Sub

    Private Sub CRS_Domestic_AllForm_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                FormAction = Request.Params("FormAction")
                If FormAction IsNot Nothing Then
                    Dim modelaction As New NawaBLL.Common.ModuleActionEnum
                    If FormAction = "Add" Then
                        modelaction = NawaBLL.Common.ModuleActionEnum.Insert
                    ElseIf FormAction = "Edit" Then
                        modelaction = NawaBLL.Common.ModuleActionEnum.Update
                    ElseIf FormAction = "Detail" Then
                        modelaction = NawaBLL.Common.ModuleActionEnum.Detail
                    ElseIf FormAction = "Delete" Then
                        modelaction = NawaBLL.Common.ModuleActionEnum.Delete
                    ElseIf FormAction = "Approval" Then
                        modelaction = NawaBLL.Common.ModuleActionEnum.Approval
                    End If
                    ActionType = modelaction
                    'If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, ObjModule.PK_Module_ID, modelaction) Then
                    '    Dim strIDCode As String = 1
                    '    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    '    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    '    Exit Sub
                    'End If
                Else
                    'FormAction = "Add"
                    'ActionType = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim strIDCode As String = 1
                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Ext.Net.X.IsAjaxRequest Then
                ClearSession()
                Dim IDData As String = Request.Params("ID")
                If IDData IsNot Nothing Then
                    IDUnik = NawaBLL.Common.DecryptQueryString(IDData, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                End If

                IDModule = Request.Params("ModuleID")
                Dim intModuleID As Integer = NawaBLL.Common.DecryptQueryString(IDModule, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                LoadGridComponentDataHeader()

                FormPanelInput.Title = "CRS Domestic Report - " & FormAction
                FormPanelDataRekening.Title = "CRS Domestic Report - " & FormAction

                ClearFieldHeader()

                Dim devaultnpwp As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRow("CRS_DOMESTIC_GLOBAL_PARAMETER", "PK_CRS_DOMESTIC_GLOBAL_PARAMETER_ID", "3")
                If devaultnpwp IsNot Nothing Then
                    If Not IsDBNull(devaultnpwp("ParameterValue")) Then
                        ParameterNPWP = devaultnpwp("ParameterValue")
                    End If
                End If

                'SetControlVisibility(True)
                'Report_Header_NPWP.Value = ParameterNPWP

                If FormAction = "Add" Then
                    'SetControlEditableHeader(True)
                    FormPanelDataRekening_PanelDataRekening_JenisData.StringFilter = " code = 'DJP1' "
                    'prepare data header
                    Report_Header_NPWP.Value = ParameterNPWP
                    Report_Header_JumlahRekening.Value = "0"
                    Report_Header_JumlahNilaiSaldo.Value = 0
                    Report_Header_JumlahPengendaliEntitas.Value = 0
                    Report_Header_PeriodeLaporan.Value = (Date.Today.Year - 1).ToString()
                    PanelRefNum.Hidden = True
                Else
                    If FormAction = "Approval" Then
                        DataModuleApproval = CRSBLL.CRSDomesticBLL.PullDataModuleApprovalByID(IDUnik)
                        IDUnik = DataModuleApproval.ModuleKey
                        If DataModuleApproval.PK_ModuleAction_ID = 2 Then
                            'Report_Header_Edit_Action.Hidden = False
                            'Report_Header_Edit_Action.Selectable = False
                            'Report_Header_Edit_Action.FieldStyle = "background-color:#ddd;"
                            Report_Header_Edit_Action_Panel.Hidden = False
                            Report_Header_Edit_Action.IsReadOnly = True
                            Report_Header_Edit_Action.StringFieldStyle = "background-color:#ddd;"
                            If DataModuleApproval.ModuleField IsNot Nothing Then
                                'Report_Header_Edit_Action.SelectedItem.Text = "Insert As New Report"
                                'Report_Header_Edit_Action.SelectedItem.Value = "Insert"
                                'Report_Header_Edit_Action.SelectedItem.Index = 2
                                Report_Header_Edit_Action.SetTextWithTextValue("Insert", "Insert As New Report")
                            Else
                                'Report_Header_Edit_Action.SelectedItem.Text = "Update Data"
                                'Report_Header_Edit_Action.SelectedItem.Value = "Update"
                                'Report_Header_Edit_Action.SelectedItem.Index = 1
                                Report_Header_Edit_Action.SetTextWithTextValue("Update", "Update Data")
                            End If
                        End If
                    End If
                    If FormAction = "Edit" Then
                        Dim CheckDataInSaveState As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString("select top 1 * from CRS_Domestic_XML_SavedState where FK_Report_ID = " & IDUnik)
                        If CheckDataInSaveState IsNot Nothing AndAlso CheckDataInSaveState.Rows.Count > 0 Then
                            IsReportAlreadyEdited = 1
                        End If
                        Dim CheckAlreadyInApproval As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString("select top 1 * from ModuleApproval where ModuleName = '" & ObjModule.ModuleName & "' and ModuleKey = '" & IDUnik & "'")
                        If CheckAlreadyInApproval IsNot Nothing AndAlso CheckAlreadyInApproval.Rows.Count > 0 Then
                            LblConfirmation.Text = "Data Already in Pending Approval, Please Accept Or Reject First"
                            FormPanelInput.Hidden = True
                            Panelconfirmation.Hidden = False
                        Else
                            LoadDataHeader()
                            Dim strQuery As String = " select top 10 IdentitasUnik from CRS_DOMESTIC_DATA_REKENING "
                            strQuery = strQuery & " where isValid = 0 and FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                            Dim dtinvalidDataRekening As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString(strQuery)
                            If dtinvalidDataRekening IsNot Nothing AndAlso dtinvalidDataRekening.Rows.Count > 0 Then
                                Dim strValidationResult As String = "TOP 10 Invalid Data Rekening"
                                For Each item In dtinvalidDataRekening.Rows
                                    strValidationResult = strValidationResult & "<br/>InValid Data Rekening With IdentitasUnik : " & item("IdentitasUnik")
                                Next
                                infoValidationResultPanelHeader.Html = strValidationResult
                                infoValidationResultPanelHeader.Hidden = False
                            Else
                                infoValidationResultPanelHeader.Hidden = True
                            End If
                            'Dim listCRSDataRekeningInValid As List(Of CRSDAL.CRS_DOMESTIC_DATA_REKENING) = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.isValid <> True).ToList
                            'If listCRSDataRekeningInValid.Count > 0 Then
                            '    Dim strValidationResult As String = ""
                            '    For Each item In listCRSDataRekeningInValid
                            '        strValidationResult = strValidationResult & "InValid Data Rekening With IdentitasUnik : " & item.IdentitasUnik & "<br/>"
                            '    Next
                            '    infoValidationResultPanelHeader.Html = strValidationResult
                            '    infoValidationResultPanelHeader.Hidden = False
                            'Else
                            '    infoValidationResultPanelHeader.Hidden = True
                            'End If
                            'SetControlEditableHeader(True)
                            StatusReportOld = HeaderClass.CRS_Domestic_Header.Status_Report
                            If StatusReportOld = "0" Then
                                FormPanelDataRekening_PanelDataRekening_JenisData.StringFilter = " code = 'DJP1' "
                                FormAction = "Add"
                            Else
                                FormPanelDataRekening_PanelDataRekening_JenisData.StringFilter = " code <> 'DJP1' "
                                'If IsReportAlreadyEdited = 0 AndAlso FormAction = "Edit" AndAlso StatusReportOld <> "0" Then
                                '    CRSBLL.CRSDomesticBLL.SaveStateByReportID(IDUnik)
                                '    IsReportAlreadyEdited = 1
                                'End If
                                'SaveHeaderStatus("10")
                                'If HeaderClass.list_CRS_Domestic_Data_Rekening.Count > 1 Then
                                '    Dim intcounterDJP1 As Integer = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData = "DJP1").Count()
                                '    Dim intcounterDJPOther As Integer = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").Count()
                                '    If intcounterDJP1 > 0 AndAlso intcounterDJPOther > 0 Then
                                '        IsJenisDataChange = True
                                '    End If
                                'End If
                                CheckDataHeaderEditAction()
                            End If
                        End If
                    ElseIf FormAction = "Delete" Then
                        Dim CheckAlreadyInApproval As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString("select top 1 * from ModuleApproval where ModuleName = '" & ObjModule.ModuleName & "' and ModuleKey = '" & IDUnik & "'")
                        If CheckAlreadyInApproval IsNot Nothing AndAlso CheckAlreadyInApproval.Rows.Count > 0 Then
                            LblConfirmation.Text = "Data Already in Pending Approval, Please Accept Or Reject First"
                            FormPanelInput.Hidden = True
                            Panelconfirmation.Hidden = False
                        Else
                            LoadDataHeader()
                            'SetControlEditableHeader(False)
                            StatusReportOld = HeaderClass.CRS_Domestic_Header.Status_Report
                        End If
                    Else
                        LoadDataHeader()
                        'SetControlEditableHeader(False)
                    End If
                    FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Hidden = False

                End If

                'If FormAction = "Edit" Then
                '    Report_Header_Edit_Action.Hidden = False
                '    If IsJenisDataChange Then
                '        Report_Header_Edit_Action.SelectedItem.Text = "Insert As New Report"
                '        Report_Header_Edit_Action.SelectedItem.Value = "Insert"
                '        Report_Header_Edit_Action.Selectable = False
                '        Report_Header_Edit_Action.FieldStyle = "background-color:#ddd;"
                '    End If
                'End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub LoadGridComponentDataHeader()
        Try
            Dim objcommandcolDataRekening As Ext.Net.CommandColumn = columncrudDataRekening

            Dim objDetailDataRekening As New GridCommand With {
                .CommandName = "Detail",
                .Icon = Icon.ApplicationViewDetail,
                .Text = "Detail"
            }
            objDetailDataRekening.ToolTip.Text = "Detail"
            objcommandcolDataRekening.Commands.Add(objDetailDataRekening)

            Dim objcommandcolPengendaliEntitas As Ext.Net.CommandColumn = columncrudPengendaliEntitas

            Dim objDetailPengendaliEntitas As New GridCommand With {
                .CommandName = "Detail",
                .Icon = Icon.ApplicationViewDetail,
                .Text = "Detail"
            }
            objDetailPengendaliEntitas.ToolTip.Text = "Detail"
            objcommandcolPengendaliEntitas.Commands.Add(objDetailPengendaliEntitas)

            If FormAction = "Add" OrElse FormAction = "Edit" Then
                Dim objEditDataRekening As New GridCommand With {
                    .CommandName = "Edit",
                    .Icon = Icon.ApplicationEdit,
                    .Text = "Edit"
                }
                objEditDataRekening.ToolTip.Text = "Edit"
                objcommandcolDataRekening.Commands.Add(objEditDataRekening)

                Dim objDeleteDataRekening As New GridCommand With {
                    .CommandName = "Delete",
                    .Icon = Icon.Delete,
                    .Text = "Delete"
                }
                objDeleteDataRekening.ToolTip.Text = "Delete"
                objcommandcolDataRekening.Commands.Add(objDeleteDataRekening)

                Dim objEditPengendaliEntitas As New GridCommand With {
                    .CommandName = "Edit",
                    .Icon = Icon.ApplicationEdit,
                    .Text = "Edit"
                }
                objEditPengendaliEntitas.ToolTip.Text = "Edit"
                objcommandcolPengendaliEntitas.Commands.Add(objEditPengendaliEntitas)

                Dim objDeletePengendaliEntitas As New GridCommand With {
                    .CommandName = "Delete",
                    .Icon = Icon.Delete,
                    .Text = "Delete"
                }
                objDeletePengendaliEntitas.ToolTip.Text = "Delete"
                objcommandcolPengendaliEntitas.Commands.Add(objDeletePengendaliEntitas)

                objcommandcolDataRekening.MinWidth = 200
                objcommandcolPengendaliEntitas.MinWidth = 200
            Else
                If FormAction = "Delete" Then
                    btn_Header_Submit.Text = "Delete"
                    btn_Header_Submit.Icon = Icon.ApplicationDelete
                ElseIf FormAction = "Approval" Then
                    btn_Header_Submit.Text = "Approve"
                    btn_Header_Submit.Icon = Icon.Accept
                    btn_Header_Reject.Hidden = False
                Else
                    btn_Header_Submit.Hidden = True
                End If
                btn_adddatarekening.Hidden = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadGridComponentDataRekening()
        Try
            'Dim objcommandcolPengendaliEntitas As Ext.Net.CommandColumn = columncrudPengendaliEntitas
            'objcommandcolPengendaliEntitas.Commands.Clear()

            'Dim objDetailPengendaliEntitas As New GridCommand With {
            '    .CommandName = "Detail",
            '    .Icon = Icon.ApplicationViewDetail,
            '    .Text = "Detail"
            '}
            'objDetailPengendaliEntitas.ToolTip.Text = "Detail"
            'objcommandcolPengendaliEntitas.Commands.Add(objDetailPengendaliEntitas)

            'If RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add OrElse RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Edit Then
            '    Dim objEditPengendaliEntitas As New GridCommand With {
            '        .CommandName = "Edit",
            '        .Icon = Icon.ApplicationEdit,
            '        .Text = "Edit"
            '    }
            '    objEditPengendaliEntitas.ToolTip.Text = "Edit"
            '    objcommandcolPengendaliEntitas.Commands.Add(objEditPengendaliEntitas)

            '    Dim objDeletePengendaliEntitas As New GridCommand With {
            '        .CommandName = "Delete",
            '        .Icon = Icon.Delete,
            '        .Text = "Delete"
            '    }
            '    objDeletePengendaliEntitas.ToolTip.Text = "Delete"
            '    objcommandcolPengendaliEntitas.Commands.Add(objDeletePengendaliEntitas)

            '    objcommandcolPengendaliEntitas.MinWidth = 200
            '    btn_DataRekening_save.Hidden = False
            '    btn_PengendaliEntitas_save.Hidden = False
            '    btn_adddatapengendalientitas.Hidden = False
            'Else
            '    btn_DataRekening_save.Hidden = True
            '    btn_PengendaliEntitas_save.Hidden = True
            '    btn_adddatapengendalientitas.Hidden = True
            'End If

            'For Each item In columncrudPengendaliEntitas.Columns
            '    item.Visible = True
            'Next
            Dim objcommandcolPengendaliEntitas As Ext.Net.CommandColumn = columncrudPengendaliEntitas
            If RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add OrElse RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Edit Then

                'objcommandcolPengendaliEntitas.PrepareToolbar.Fn = "prepareCommandCollectionPengendaliEntitas_Enable"
                'For Each item In columncrudPengendaliEntitas.Commands
                '    Dim tempindex As Integer = columncrudPengendaliEntitas.Commands.IndexOf(item)
                '    Dim tempCommandbase = columncrudPengendaliEntitas.Commands.Item(tempindex)
                '    Dim tempCommands As GridCommand = columncrudPengendaliEntitas.Commands.Item(tempindex)
                '    'tempCommandbase.
                '    If tempCommands.CommandName = "Edit" OrElse tempCommands.CommandName = "Delete" Then
                '        tempCommands.Hidden = False

                '    End If
                '    'columncrudPengendaliEntitas.Commands.Item(tempindex) = tempCommands
                'Next
                'objcommandcolPengendaliEntitas.MinWidth = 200
                btn_DataRekening_save.Hidden = False
                'btn_PengendaliEntitas_save.Hidden = False
                btn_adddatapengendalientitas.Hidden = False
            Else
                'objcommandcolPengendaliEntitas.PrepareToolbar.Fn = "prepareCommandCollectionPengendaliEntitas_Disable"
                'For Each item In columncrudPengendaliEntitas.Commands
                '    Dim tempindex As Integer = columncrudPengendaliEntitas.Commands.IndexOf(item)
                '    Dim tempCommands As GridCommand = columncrudPengendaliEntitas.Commands.Item(tempindex)
                '    If tempCommands.CommandName = "Edit" OrElse tempCommands.CommandName = "Delete" Then
                '        tempCommands.Hidden = True
                '    End If
                '    'columncrudPengendaliEntitas.Commands.Item(tempindex) = tempCommands
                'Next
                btn_DataRekening_save.Hidden = True
                'btn_PengendaliEntitas_save.Hidden = True
                btn_adddatapengendalientitas.Hidden = True
            End If
            'columncrudPengendaliEntitas = objcommandcolPengendaliEntitas
            'GridPanelPengendaliEntitas.Refresh()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadDataHeader()
        Try
            'If FormAction = "Add" Then
            '    Exit Sub
            'End If
            HeaderClass = CRSBLL.CRSDomesticBLL.PullDataReportByID(IDUnik)
            With HeaderClass.CRS_Domestic_Header
                Report_Header_ID.Text = .PK_CRS_DOMESTIC_HEADER_ID
                Report_Header_NPWP.Value = .NPWPLKPengirim
                'Report_Header_JumlahRekening.Value = .JumlahDataRekening
                'Report_Header_JumlahNilaiSaldo.Value = .JumlahNilaiSaldo
                'Report_Header_JumlahPengendaliEntitas.Value = .JumlahDataPengendaliEntitas
                Report_Header_PeriodeLaporan.Value = .Periode
                'If .TanggalPenyampaian.HasValue Then
                '    Report_Header_TanggalPenyampaian.Value = .TanggalPenyampaian
                'End If
                'Report_Header_NomorTandaTerimaElektronik.Value = .NomorTandaTerimaElektronik
            End With
            BindRefNum(HeaderClass.CRS_Domestic_Header.Periode)
            'BindDataRekening(HeaderClass.list_CRS_Domestic_Data_Rekening)
            BindDataRekening()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub RealoadDataCounterHeader()
        Try
            Report_Header_Status.Text = HeaderClass.CRS_Domestic_Header.Status_Report & " - " & NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select Description from CRS_Domestic_Ref_Status_Report where PK_Status_Report_ID = " & HeaderClass.CRS_Domestic_Header.Status_Report, Nothing)
            Dim inttotalRekening As Integer = 0
            If FormAction = "Approval" AndAlso DataModuleApproval IsNot Nothing AndAlso DataModuleApproval.PK_ModuleAction_ID = 2 AndAlso DataModuleApproval.ModuleField IsNot Nothing Then
                Dim strQuery As String = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                inttotalRekening = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                'Report_Header_JumlahRekening.Value = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").Count
                Report_Header_JumlahRekening.Value = inttotalRekening.ToString()
                strQuery = " select ISNULL(SUM(SaldoAtauNilai), 0) from CRS_DOMESTIC_DATA_REKENING "
                strQuery = strQuery & " where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                'Report_Header_JumlahNilaiSaldo.Value = HeaderClass.list_CRS_Domestic_Data_Rekening.Sum(Function(x) x.SaldoAtauNilai And x.JenisData <> "DJP1")
                Dim sumSaldo As Double = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Report_Header_JumlahNilaiSaldo.Value = sumSaldo.ToString()
                strQuery = " select COUNT(1) as CountDataPengendaliEntitas "
                strQuery = strQuery & " From CRS_DOMESTIC_PENGENDALI_ENTITAS entitas "
                strQuery = strQuery & " Join CRS_DOMESTIC_DATA_REKENING rekening "
                strQuery = strQuery & " On entitas.FK_CRS_DOMESTIC_DATA_REKENING_ID = rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID "
                strQuery = strQuery & " where rekening.JenisData <> 'DJP1' and "
                strQuery = strQuery & " entitas.FK_CRS_DOMESTIC_HEADER_ID = " & HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                Dim getCountPengendaliEntitas As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString(strQuery)
                If getCountPengendaliEntitas IsNot Nothing Then
                    Report_Header_JumlahPengendaliEntitas.Value = getCountPengendaliEntitas("CountDataPengendaliEntitas")
                End If
            Else
                Dim strQuery As String = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                inttotalRekening = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                'Report_Header_JumlahRekening.Value = HeaderClass.list_CRS_Domestic_Data_Rekening.Count()
                Report_Header_JumlahRekening.Value = inttotalRekening.ToString()
                strQuery = " select ISNULL(SUM(SaldoAtauNilai), 0) from CRS_DOMESTIC_DATA_REKENING "
                strQuery = strQuery & " where FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                'Report_Header_JumlahNilaiSaldo.Value = HeaderClass.list_CRS_Domestic_Data_Rekening.Sum(Function(x) x.SaldoAtauNilai)
                Dim sumSaldo As Double = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Report_Header_JumlahNilaiSaldo.Value = sumSaldo
                strQuery = " select COUNT(1) as CountDataPengendaliEntitas "
                strQuery = strQuery & " from CRS_DOMESTIC_PENGENDALI_ENTITAS "
                strQuery = strQuery & " where FK_CRS_DOMESTIC_HEADER_ID = " & HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                Dim getCountPengendaliEntitas As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString(strQuery)
                If getCountPengendaliEntitas IsNot Nothing Then
                    Report_Header_JumlahPengendaliEntitas.Value = getCountPengendaliEntitas("CountDataPengendaliEntitas")
                End If
            End If
            'Dim intLimit As Integer = 10
            'If intLimit > inttotalRekening Then
            '    intLimit = inttotalRekening
            'End If
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("VW_CRS_DOMESTIC_DATA_REKENING", "*", " FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik, " PK_CRS_DOMESTIC_DATA_REKENING_ID asc", 0, intLimit, inttotalRekening)
            'StoreDataRekening.DataSource = DataPaging
            'StoreDataRekening.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadDataRekening(IDRekening As Long)
        Try
            ClearFieldRekening()
            RekeningClass = CRSBLL.CRSDomesticBLL.PullDataRekeningByID(IDRekening)
            With RekeningClass.CRS_Domestic_Data_Rekening
                FormPanelDataRekening_PanelDataRekening_NPWPPengirim.Value = .NPWPLembagaKeuanganPengirim
                FormPanelDataRekening_PanelDataRekening_LembagaKeuanganPelapor.Value = .NPWPLembagaKeuanganPelapor
                'FormPanelDataRekening_PanelDataRekening_NPWPPengirim.Value = ParameterNPWP
                'FormPanelDataRekening_PanelDataRekening_LembagaKeuanganPelapor.Value = ParameterNPWP
                'If RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add Then

                'Else
                '    FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Value = .IdentitasUnik
                'End If
                FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Value = .IdentitasUnik
                If .JenisData IsNot Nothing Then
                    Dim DrTemp As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRow("VW_CRS_DOMESTIC_JENIS_DATA", "code", .JenisData)
                    If DrTemp IsNot Nothing Then
                        FormPanelDataRekening_PanelDataRekening_JenisData.SetTextWithTextValue(DrTemp("code"), DrTemp("DataName"))
                        JenisDataBeforeChange = DrTemp("code")
                    End If
                End If
                FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Value = .IdentitasUnikKoreksi
                If .JenisLembagaKeuangan IsNot Nothing Then
                    Dim DrTemp As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRow("VW_CRS_DOMESTIC_LJK_TYPE", "code", .JenisLembagaKeuangan)
                    If DrTemp IsNot Nothing Then
                        FormPanelDataRekening_PanelDataRekening_JenisLembagaKeuangan.SetTextWithTextValue(DrTemp("code"), DrTemp("DataName"))
                    End If
                End If
                FormPanelDataRekening_PanelDataRekening_NomorCIF.Value = .NomorCIF
                FormPanelDataRekening_PanelDataRekening_NomorRekening.Value = .NomorRekening
                FormPanelDataRekening_PanelDataRekening_NomorCIFandRekening.Value = .NomorCifNomorRekening
                If .StsRekening IsNot Nothing Then
                    Dim DrTemp As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRow("VW_CRS_DOMESTIC_STATUS_REKENING", "code", .StsRekening)
                    If DrTemp IsNot Nothing Then
                        FormPanelDataRekening_PanelDataRekening_StatusRekening.SetTextWithTextValue(DrTemp("code"), DrTemp("DataName"))
                    End If
                End If
                If .JnsPemegangRekening IsNot Nothing Then
                    Dim DrTemp As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRow("VW_CRS_DOMESTIC_JENIS_PEMEGANG_REKENING", "code", .JnsPemegangRekening)
                    If DrTemp IsNot Nothing Then
                        FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.SetTextWithTextValue(DrTemp("code"), DrTemp("DataName"))
                        'FormPanelDataRekening_PanelInformasi.Title = FormPanelDataRekening_PanelInformasi.Title & DrTemp("DataName")
                        'FormPanelDataRekening_PanelInformasi.Title = "Informasi Pemegang Rekening " & DrTemp("DataName")
                        FormPanelDataRekening_PanelInformasi.Title = FormPanelDataRekening_PanelInformasi.Title & DrTemp("code")
                    Else
                        'FormPanelDataRekening_PanelInformasi.Title = FormPanelDataRekening_PanelInformasi.Title & "Individual"
                        FormPanelDataRekening_PanelInformasi.Title = "Informasi Pemegang Rekening INDIVIDUAL"
                    End If
                    HiddenFieldIndividuOrEntitas(.JnsPemegangRekening)
                End If
                If .MataUang IsNot Nothing Then
                    Dim DrTemp As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRow("VW_CRS_DOMESTIC_CURRENCY", "code", .MataUang)
                    If DrTemp IsNot Nothing Then
                        FormPanelDataRekening_PanelDataRekening_MataUang.SetTextWithTextValue(DrTemp("code"), DrTemp("DataName"))
                    End If
                End If
                FormPanelDataRekening_PanelDataRekening_SaldoatauNilai.Value = .SaldoAtauNilai
                FormPanelDataRekening_PanelDataRekening_Deviden.Value = .Deviden
                FormPanelDataRekening_PanelDataRekening_Bunga.Value = .Bunga
                FormPanelDataRekening_PanelDataRekening_PenghasilanBruto.Value = .PhBruto
                FormPanelDataRekening_PanelDataRekening_PenghasilanLainnya.Value = .PhLainnya

                FormPanelDataRekening_PanelInformasi_Nama.Value = .NamaPemegangRek
                FormPanelDataRekening_PanelInformasi_NamaLainnya.Value = .NamaLainPemegangRek
                FormPanelDataRekening_PanelInformasi_NPWP.Value = .NPWPPemegangRek
                FormPanelDataRekening_PanelInformasi_SIUP.Value = .SIUPPemegangRek
                FormPanelDataRekening_PanelInformasi_SITU.Value = .SITUPemegangRek
                FormPanelDataRekening_PanelInformasi_AKTA.Value = .AktaPemegangRek
                FormPanelDataRekening_PanelInformasi_NIK.Value = .NIKPemegangRek
                FormPanelDataRekening_PanelInformasi_SIM.Value = .SIMPemegangRek
                FormPanelDataRekening_PanelInformasi_Paspor.Value = .PasporPemegangRek
                'FormPanelDataRekening_PanelInformasi_Kewarganegaraan.Value = .KewarganegaraanPemegangRek
                If .KewarganegaraanPemegangRek IsNot Nothing Then
                    Dim DrTemp As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRow("VW_CRS_DOMESTIC_COUNTRY", "code", .KewarganegaraanPemegangRek)
                    If DrTemp IsNot Nothing Then
                        FormPanelDataRekening_PanelInformasi_Kewarganegaraan.SetTextWithTextValue(DrTemp("code"), DrTemp("DataName"))
                    End If
                End If
                FormPanelDataRekening_PanelInformasi_TempatLahir.Value = .TempatLahirPemegangRek
                If .TglLahirPemegangRek.HasValue Then
                    FormPanelDataRekening_PanelInformasi_TanggalLahir.Value = .TglLahirPemegangRek
                End If
                FormPanelDataRekening_PanelInformasi_AlamatDomisili.Value = .AlamatDomPemegangRek
                FormPanelDataRekening_PanelInformasi_AlamatUsaha.Value = .AlamatUsahaPemegangRek
                FormPanelDataRekening_PanelInformasi_AlamatKorespondensi.Value = .AlamatKorespondensiPemegangRek
            End With
            HiddenFieldIndividuOrEntitas(FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.StringValue)
            BindPengendaliEntitas(RekeningClass.list_CRS_Domestic_Pengendali_Entitas)
            If FormAction <> "Approval" AndAlso RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Edit Then
                Dim strValidationResult As String = CRSBLL.CRSDomesticBLL.ValidateDataRekening(RekeningClass)
                If Not String.IsNullOrWhiteSpace(strValidationResult) Then
                    infoValidationResultPanelRekening.Html = strValidationResult
                    FormPanelDataRekening.Body.ScrollTo(Direction.Top, 0)
                    infoValidationResultPanelRekening.Hidden = False
                Else
                    infoValidationResultPanelRekening.Hidden = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LoadDataPengendaliEntitas(IDPengendaliEntitas As Long)
        Try
            ClearFieldPengendaliEntitas()
            DataPengendaliEntitas = New CRSDAL.CRS_DOMESTIC_PENGENDALI_ENTITAS
            DataPengendaliEntitas = RekeningClass.list_CRS_Domestic_Pengendali_Entitas.Where(Function(x) x.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID = IDPengendaliEntitas).FirstOrDefault
            With DataPengendaliEntitas
                WindowPengendaliEntitas_Nama.Value = .NamaCP
                If .KodeNegaraCP IsNot Nothing Then
                    Dim DrTemp As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRow("VW_CRS_DOMESTIC_COUNTRY", "code", .KodeNegaraCP)
                    If DrTemp IsNot Nothing Then
                        WindowPengendaliEntitas_Kewarganegaraan.SetTextWithTextValue(DrTemp("code"), DrTemp("DataName"))
                    End If
                End If
                WindowPengendaliEntitas_TempatLahir.Value = .TempatLahirCP
                If .TglLahirCP.HasValue Then
                    WindowPengendaliEntitas_TanggalLahir.Value = .TglLahirCP
                End If
                WindowPengendaliEntitas_NPWPorTIN.Value = .NPWPTINCP
                WindowPengendaliEntitas_NIK.Value = .NIKCP
                WindowPengendaliEntitas_SIM.Value = .SIMCP
                WindowPengendaliEntitas_Paspor.Value = .PasporCP
                WindowPengendaliEntitas_AlamatDomisili.Value = .AlamatDomCP
                WindowPengendaliEntitas_AlamatKorespondensi.Value = .AlamatKorespondensiCP
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening_OnValueChange(sender As Object, e As EventArgs)
        FormPanelDataRekening_PanelInformasi.Title = FormPanelDataRekening_PanelInformasi.Title & FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.StringValue
        'FormPanelDataRekening_PanelInformasi.Title = "Informasi Pemegang Rekening " & FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.StringValue
        FormPanelDataRekening_PanelInformasi.Hidden = False
        HiddenFieldIndividuOrEntitas(FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.StringValue)
    End Sub

    Protected Sub HiddenFieldIndividuOrEntitas(JenisPemegangRekening As String)
        Try
            If JenisPemegangRekening = "INDIVIDUAL" Then
                FormPanelDataRekening_PanelInformasi.Hidden = False

                FormPanelDataRekening_PanelInformasi_SIUP.Hidden = True
                FormPanelDataRekening_PanelInformasi_SITU.Hidden = True
                FormPanelDataRekening_PanelInformasi_AKTA.Hidden = True
                FormPanelDataRekening_PanelDataPengendaliEntitas.Hidden = True

                FormPanelDataRekening_PanelInformasi_NIK.Hidden = False
                FormPanelDataRekening_PanelInformasi_SIM.Hidden = False
                FormPanelDataRekening_PanelInformasi_Paspor.Hidden = False
                FormPanelDataRekening_PanelInformasi_Kewarganegaraan.IsHidden = False
                FormPanelDataRekening_PanelInformasi_TempatLahir.Hidden = False
                FormPanelDataRekening_PanelInformasi_TanggalLahir.Hidden = False
            ElseIf JenisPemegangRekening = "ENTITAS" Then
                FormPanelDataRekening_PanelInformasi.Hidden = False

                FormPanelDataRekening_PanelInformasi_SIUP.Hidden = False
                FormPanelDataRekening_PanelInformasi_SITU.Hidden = False
                FormPanelDataRekening_PanelInformasi_AKTA.Hidden = False
                FormPanelDataRekening_PanelDataPengendaliEntitas.Hidden = False

                FormPanelDataRekening_PanelInformasi_NIK.Hidden = True
                FormPanelDataRekening_PanelInformasi_SIM.Hidden = True
                FormPanelDataRekening_PanelInformasi_Paspor.Hidden = True
                FormPanelDataRekening_PanelInformasi_Kewarganegaraan.IsHidden = True
                FormPanelDataRekening_PanelInformasi_TempatLahir.Hidden = True
                FormPanelDataRekening_PanelInformasi_TanggalLahir.Hidden = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub CheckDataHeaderEditAction()
        Try
            If FormAction = "Edit" Then
                Dim strQuery As String = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                'Dim intcounterDJPOther As Integer = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").Count()
                Dim intcounterDJPOther As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                'If HeaderClass.list_CRS_Domestic_Data_Rekening.Count > 1 And Not IsJenisDataChange Then
                '    Dim intcounterDJP1 As Integer = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData = "DJP1").Count()
                '    'intcounterDJPOther = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").Count()
                '    If intcounterDJP1 > 0 AndAlso intcounterDJPOther > 0 Then
                '        IsJenisDataChange = True
                '    End If
                'End If
                'If HeaderClass.list_CRS_Domestic_Data_Rekening.Count > 0 And Not IsJenisDataChange Then
                '    Dim intcounterDJPChange As Integer = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.TempJenisData <> Nothing And x.JenisData <> x.TempJenisData).Count()
                '    If intcounterDJPChange > 0 Then
                '        IsJenisDataChange = True
                '    End If
                '    'intcounterDJPOther = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").Count()
                'End If
                If Not IsJenisDataChange Then
                    strQuery = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING  "
                    strQuery = strQuery & " where TempJenisData is not null and JenisData <> TempJenisData "
                    strQuery = strQuery & " and FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                    'Dim intcounterDJPChange As Integer = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.TempJenisData <> Nothing And x.JenisData <> x.TempJenisData).Count()
                    Dim intcounterDJPChange As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                    If intcounterDJPChange > 0 Then
                        IsJenisDataChange = True
                    Else
                        IsJenisDataChange = False
                    End If
                    strQuery = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where JenisData = 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                    'Dim intcounterDJP1 As Integer = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData = "DJP1").Count()
                    Dim intcounterDJP1 As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                    If intcounterDJP1 > 0 AndAlso intcounterDJPOther > 0 Then
                        IsJenisDataChange = True
                    End If
                End If
                'Report_Header_Edit_Action.Hidden = False
                Report_Header_Edit_Action_Panel.Hidden = False
                If HeaderClass.CRS_Domestic_Header.IsReportStateCode = 3 AndAlso IsJenisDataChange Then
                    'Report_Header_Edit_Action.SelectedItem.Text = "Insert As New Report"
                    'Report_Header_Edit_Action.SelectedItem.Value = "Insert"
                    'Report_Header_Edit_Action.SelectedItem.Index = 2
                    'Report_Header_Edit_Action.Selectable = False
                    'Report_Header_Edit_Action.FieldStyle = "background-color:#ddd;"
                    Report_Header_Edit_Action.SetTextWithTextValue("Insert", "Insert As New Report")
                    Report_Header_Edit_Action.IsReadOnly = True
                    Report_Header_Edit_Action.StringFieldStyle = "background-color:#ddd;"
                ElseIf intcounterDJPOther = 0 Then
                    Report_Header_Edit_Action.SetTextWithTextValue("Update", "Update Data")
                    Report_Header_Edit_Action.IsReadOnly = True
                    Report_Header_Edit_Action.StringFieldStyle = "background-color:#ddd;"
                Else
                    'Report_Header_Edit_Action.Selectable = True
                    'Report_Header_Edit_Action.FieldStyle = "background-color:#ffe4c4;"
                    Report_Header_Edit_Action.IsReadOnly = False
                    Report_Header_Edit_Action.StringFieldStyle = "background-color:#ffe4c4;"
                End If

                Dim drTemp As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString("select count(1) as CountData from CRS_DOMESTIC_REF_NUM where YearPeriod = '" & HeaderClass.CRS_Domestic_Header.Periode & "'")
                If drTemp("CountData") > 0 Then
                    Report_Header_Edit_Action.SetTextWithTextValue("Insert", "Insert As New Report")
                    Report_Header_Edit_Action.IsReadOnly = True
                    Report_Header_Edit_Action.StringFieldStyle = "background-color:#ddd;"
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Btn_Header_Submit_Click()
        Try
            If FormAction = "Approval" Then

                With HeaderClass.CRS_Domestic_Header
                    If Not String.IsNullOrEmpty(Report_Header_JumlahRekening.Text) Then
                        .JumlahDataRekening = CInt(Report_Header_JumlahRekening.Value)
                    Else
                        .JumlahDataRekening = 0
                    End If
                    If Not String.IsNullOrEmpty(Report_Header_JumlahPengendaliEntitas.Text) Then
                        .JumlahDataPengendaliEntitas = CInt(Report_Header_JumlahPengendaliEntitas.Value)
                    Else
                        .JumlahDataPengendaliEntitas = 0
                    End If
                    If Not String.IsNullOrEmpty(Report_Header_JumlahNilaiSaldo.Text) Then
                        .JumlahNilaiSaldo = CDbl(Report_Header_JumlahNilaiSaldo.Value)
                    Else
                        .JumlahNilaiSaldo = 0
                    End If
                End With
                If DataModuleApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update Then
                    With HeaderClass.CRS_Domestic_Header
                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                        .Status_Report = "1"
                        .IsReportStateCode = 2
                    End With
                    CRSBLL.CRSDomesticBLL.SaveDataHeader(HeaderClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Edit)
                    If DataModuleApproval.ModuleField IsNot Nothing Then
                        'CRSBLL.CRSDomesticBLL.SaveDataIntoNewRow(IDUnik)
                        Dim NewPKHeader As Long = CRSBLL.CRSDomesticBLL.SaveDataIntoNewRow(IDUnik)
                        UpdateDataErrorValidationMessage(NewPKHeader)
                        CRSBLL.CRSDomesticBLL.RestoreStateByReportID(IDUnik)
                        UpdateDataErrorValidationMessage(IDUnik)
                        LblConfirmation.Text = "Request Data Updating(Insert As New Report) Approved, and Saved into Database"
                    Else
                        UpdateDataErrorValidationMessage(IDUnik)
                        LblConfirmation.Text = "Request Data Updating Approved, and Saved into Database"
                    End If
                    Dim strquerydeletebackup As String = "DELETE from CRS_Domestic_XML_SavedState where FK_Report_ID = " & IDUnik
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquerydeletebackup, Nothing)
                ElseIf DataModuleApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert Then
                    With HeaderClass.CRS_Domestic_Header
                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                        .Status_Report = "1"
                    End With
                    CRSBLL.CRSDomesticBLL.SaveDataHeader(HeaderClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Edit)
                    UpdateDataErrorValidationMessage(IDUnik)
                    LblConfirmation.Text = "Request Data Adding Approved, and Added into Database"
                ElseIf DataModuleApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete Then
                    CRSBLL.CRSDomesticBLL.SaveDataHeader(HeaderClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Delete)
                    LblConfirmation.Text = "Request Data Deletion Approved, and Deleted from Database"
                End If
                Dim strquery As String = "DELETE from ModuleApproval where ModuleName = '" & ObjModule.ModuleName & "' and ModuleKey = '" & IDUnik & "'"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquery, Nothing)
            Else
                Dim strQuery As String = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                Dim intcountDataRekening As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                If HeaderClass Is Nothing OrElse intcountDataRekening = 0 AndAlso (FormAction = "Edit" OrElse FormAction = "Add") Then
                    Throw New ApplicationException("Tidak Terdapat Data Rekening, mohon untuk memastikan telah mengisikan data rekening terlebih dahulu.")
                End If
                If FormAction <> "Delete" Then
                    strQuery = " select top 10 IdentitasUnik from CRS_DOMESTIC_DATA_REKENING "
                    strQuery = strQuery & " where isValid = 0 and FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                    Dim dtinvalidDataRekening As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString(strQuery)
                    If dtinvalidDataRekening IsNot Nothing AndAlso dtinvalidDataRekening.Rows.Count > 0 Then
                        Dim strValidationResult As String = "TOP 10 Invalid Data Rekening"
                        For Each item In dtinvalidDataRekening.Rows
                            strValidationResult = strValidationResult & "<br/>InValid Data Rekening With IdentitasUnik : " & item("IdentitasUnik")
                        Next
                        infoValidationResultPanelHeader.Html = strValidationResult
                        FormPanelInput.Body.ScrollTo(Direction.Top, 0)
                        infoValidationResultPanelHeader.Hidden = False
                        Throw New ApplicationException("There is still have some invalid Data Rekening, please Fix Invalid Data.")
                        Exit Sub
                    Else
                        infoValidationResultPanelHeader.Hidden = True
                    End If
                End If
                'Dim listCRSDataRekeningInValid As List(Of CRSDAL.CRS_DOMESTIC_DATA_REKENING) = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.isValid <> True).ToList
                'If listCRSDataRekeningInValid.Count > 0 Then
                '    Dim strValidationResult As String = ""
                '    'Dim firstLoop As Boolean = True
                '    For Each item In listCRSDataRekeningInValid
                '        strValidationResult = strValidationResult & "InValid Data Rekening With IdentitasUnik : " & item.IdentitasUnik & "<br/>"
                '    Next
                '    infoValidationResultPanelHeader.Html = strValidationResult
                '    FormPanelInput.Body.ScrollTo(Direction.Top, 0)
                '    infoValidationResultPanelHeader.Hidden = False
                '    Throw New ApplicationException("There is still have some invalid Data Rekening, please Fix Invalid Data.")
                '    Exit Sub
                'Else
                '    infoValidationResultPanelHeader.Hidden = True
                'End If
                If String.IsNullOrWhiteSpace(Report_Header_Edit_Action.SelectedItemText) Then
                    Throw New ApplicationException("Mohon untuk memilih " & Report_Header_Edit_Action.Label & " terlebih dahulu.")
                End If

                With HeaderClass.CRS_Domestic_Header
                    If Not String.IsNullOrEmpty(Report_Header_NPWP.Value) Then
                        .NPWPLKPengirim = Report_Header_NPWP.Value
                    Else
                        .NPWPLKPengirim = ""
                    End If
                    If Not String.IsNullOrEmpty(Report_Header_JumlahRekening.Text) Then
                        .JumlahDataRekening = CInt(Report_Header_JumlahRekening.Value)
                    Else
                        .JumlahDataRekening = 0
                    End If
                    If Not String.IsNullOrEmpty(Report_Header_JumlahPengendaliEntitas.Text) Then
                        .JumlahDataPengendaliEntitas = CInt(Report_Header_JumlahPengendaliEntitas.Value)
                    Else
                        .JumlahDataPengendaliEntitas = 0
                    End If
                    If Not String.IsNullOrEmpty(Report_Header_JumlahNilaiSaldo.Text) Then
                        .JumlahNilaiSaldo = CDbl(Report_Header_JumlahNilaiSaldo.Value)
                    Else
                        .JumlahNilaiSaldo = 0
                    End If
                    'If Not CDate(Report_Header_TanggalPenyampaian.Value) = DateTime.MinValue Then
                    '    .TanggalPenyampaian = Report_Header_TanggalPenyampaian.Value
                    'End If
                    '.NomorTandaTerimaElektronik = Report_Header_NomorTandaTerimaElektronik.Value
                    If Not String.IsNullOrEmpty(Report_Header_PeriodeLaporan.Value) Then
                        .Periode = Report_Header_PeriodeLaporan.Value
                    Else
                        .Periode = "0"
                    End If
                    .Status_Report = "1"
                    .IsValid = True
                    .IsReportStateCode = 2

                    If .InitialOrCorrection = "Initial" AndAlso Report_Header_Edit_Action.SelectedItemValue = "Insert" Then
                        If IsJenisDataChange Then
                            .InitialOrCorrection = "Correction"
                        Else
                            strQuery = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                            'Dim intcounterDJPOther As Integer = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").Count()
                            Dim intcounterDJPOther As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            If Not IsJenisDataChange Then
                                strQuery = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING  "
                                strQuery = strQuery & " where TempJenisData is not null and JenisData <> TempJenisData "
                                strQuery = strQuery & " and FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                                'Dim intcounterDJPChange As Integer = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.TempJenisData <> Nothing And x.JenisData <> x.TempJenisData).Count()
                                Dim intcounterDJPChange As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                If intcounterDJPChange > 0 Then
                                    IsJenisDataChange = True
                                Else
                                    IsJenisDataChange = False
                                End If
                                strQuery = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where JenisData = 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                                'Dim intcounterDJP1 As Integer = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData = "DJP1").Count()
                                Dim intcounterDJP1 As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                                If intcounterDJP1 > 0 AndAlso intcounterDJPOther > 0 Then
                                    IsJenisDataChange = True
                                End If
                            End If

                            If IsJenisDataChange Then
                                .InitialOrCorrection = "Correction"
                            End If
                        End If
                    ElseIf FormAction = "Add" Then
                        .InitialOrCorrection = "Initial"
                    End If
                End With

                'Dim strqueryUpdateTempData As String = "update CRS_DOMESTIC_DATA_REKENING set TempIdentitasUnik = null, TempIdentitasUnikKoreksi = null, TempJenisData = null, IsNewData = null where FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                Dim strqueryUpdateTempData As String = "update CRS_DOMESTIC_DATA_REKENING set TempIdentitasUnik = null, TempIdentitasUnikKoreksi = null, TempJenisData = null where FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strqueryUpdateTempData, Nothing)
                If NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse ObjModule.IsUseApproval = False Then
                    If FormAction = "Delete" Then
                        CRSBLL.CRSDomesticBLL.SaveDataHeader(HeaderClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Delete)
                    Else
                        CRSBLL.CRSDomesticBLL.SaveDataHeader(HeaderClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Edit)
                        If FormAction = "Edit" AndAlso Report_Header_Edit_Action.SelectedItemValue = "Insert" Then
                            'Dim intcounterDJPOther As Integer = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").Count()
                            Dim intcounterDJPOther As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik, Nothing)

                            If intcounterDJPOther = 0 Then
                                Throw New ApplicationException("No Data Found with Jenis Data other than DJP1.")
                            End If
                            'CRSBLL.CRSDomesticBLL.SaveDataIntoNewRow(IDUnik)
                            Dim NewPKHeader As Long = CRSBLL.CRSDomesticBLL.SaveDataIntoNewRow(IDUnik)
                            UpdateDataErrorValidationMessage(NewPKHeader)
                            CRSBLL.CRSDomesticBLL.RestoreStateByReportID(IDUnik)
                            UpdateDataErrorValidationMessage(IDUnik)
                        Else
                            UpdateDataErrorValidationMessage(IDUnik)
                        End If
                    End If
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "DELETE from CRS_Domestic_XML_SavedState where FK_Report_ID = " & IDUnik, Nothing)
                    'HeaderClass.CRS_Domestic_Header.Status_Report = "1"
                    LblConfirmation.Text = "Data Saved into Database"
                Else
                    If IsReportAlreadyEdited = 0 AndAlso FormAction = "Edit" AndAlso StatusReportOld <> "0" Then
                        CRSBLL.CRSDomesticBLL.SaveStateByReportID(IDUnik)
                        IsReportAlreadyEdited = 1
                    End If
                    'If IDUnik = Nothing Then
                    '    With HeaderClass.CRS_Domestic_Header
                    '        .PK_CRS_DOMESTIC_HEADER_ID = -1
                    '        .NPWPLKPengirim = ParameterNPWP
                    '        .JumlahDataRekening = 0
                    '        .JumlahDataPengendaliEntitas = 0
                    '        .JumlahNilaiSaldo = 0
                    '        .Periode = (Date.Today.Year - 1).ToString()
                    '        .Status_Report = "0"
                    '    End With
                    '    CRSBLL.CRSDomesticBLL.SaveDataHeader(HeaderClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Add)
                    '    IDUnik = HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                    '    Report_Header_ID.Text = IDUnik
                    'End If
                    HeaderClass.CRS_Domestic_Header.Status_Report = "4"
                    CRSBLL.CRSDomesticBLL.SaveDataHeader(HeaderClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Edit)
                    UpdateDataErrorValidationMessage(IDUnik)
                    If FormAction = "Add" Then
                        CRSBLL.CRSDomesticBLL.SaveWithApproval(ObjModule, IDUnik, NawaBLL.Common.ModuleActionEnum.Insert)
                    ElseIf FormAction = "Edit" Then
                        If Report_Header_Edit_Action.SelectedItemValue = "Insert" Then
                            Dim intcounterDJPOther As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik, Nothing)
                            'Dim intcounterDJPOther As Integer = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").Count()
                            If intcounterDJPOther = 0 Then
                                Throw New ApplicationException("No Data Found with Jenis Data other than DJP1.")
                            End If
                            CRSBLL.CRSDomesticBLL.SaveWithApproval(ObjModule, IDUnik, NawaBLL.Common.ModuleActionEnum.Update, True)
                        Else
                            CRSBLL.CRSDomesticBLL.SaveWithApproval(ObjModule, IDUnik, NawaBLL.Common.ModuleActionEnum.Update)
                        End If
                    ElseIf FormAction = "Delete" Then
                        CRSBLL.CRSDomesticBLL.SaveWithApproval(ObjModule, IDUnik, NawaBLL.Common.ModuleActionEnum.Delete, , StatusReportOld)
                    End If
                    LblConfirmation.Text = "Data Saved into Pending Approval"
                End If
            End If
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Header_Reject_Click()
        Try
            If DataModuleApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update Then
                CRSBLL.CRSDomesticBLL.RestoreStateByReportID(IDUnik)
                UpdateDataErrorValidationMessage(IDUnik)
                LblConfirmation.Text = "Request Data Updating Rejected"
            ElseIf DataModuleApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert Then
                CRSBLL.CRSDomesticBLL.SaveDataHeader(HeaderClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Delete)
                LblConfirmation.Text = "Request Data Adding Rejected"
            ElseIf DataModuleApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete Then
                SaveHeaderStatus(DataModuleApproval.ModuleField)
                LblConfirmation.Text = "Request Data Deletion Rejected"
            End If
            Dim strquery As String = "DELETE from ModuleApproval where ModuleName = '" & ObjModule.ModuleName & "' and ModuleKey = '" & IDUnik & "'"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquery, Nothing)
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_Header_Back_Click()
        Try
            If FormAction = "Approval" Then
                If InStr(ObjModule.UrlApproval, "?") > 0 Then
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "&ModuleID=" & IDModule, "Loading")
                Else
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & IDModule, "Loading")
                End If
            ElseIf (FormAction = "Add" OrElse FormAction = "Edit") AndAlso IsReportAlreadyEdited = 1 Then
                WindowBackConfirmation.Hidden = False
                'ElseIf FormAction = "Add" Then
                '    'WindowBackConfirmation_Label.Html = "Do You Want to Keep Changed Data ?<br/>(No, All Changed Data will be Returned)"
                '    WindowBackConfirmation.Hidden = False
                'ElseIf FormAction = "Edit" Then
                '    'WindowBackConfirmation_Label.Html = "Do You Want to Keep Changed Data ?<br/>(No, All Changed Data will be Returned)"
                '    WindowBackConfirmation.Hidden = False
            Else
                If InStr(ObjModule.UrlView, "?") > 0 Then
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule, "Loading")
                Else
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule, "Loading")
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub WindowBackConfirmation_Yes_Click()
        Try
            If InStr(ObjModule.UrlView, "?") > 0 Then
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule, "Loading")
            Else
                Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule, "Loading")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub WindowBackConfirmation_No_Click()
        Try
            'ExtNet.Msg.Confirm("Confirmation Data", "Do You Want to keep saved Data as Draft ?").Show()
            If FormAction = "Add" AndAlso HeaderClass IsNot Nothing AndAlso HeaderClass.CRS_Domestic_Header IsNot Nothing Then
                CRSBLL.CRSDomesticBLL.SaveDataHeader(HeaderClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Delete)
                LblConfirmation.Text = "Data Deleted from system"
            ElseIf FormAction = "Edit" AndAlso IsReportAlreadyEdited = 1 Then
                CRSBLL.CRSDomesticBLL.RestoreStateByReportID(IDUnik)
                LblConfirmation.Text = "Data Before Edit Restored"
                UpdateDataErrorValidationMessage(IDUnik)
            End If
            FormPanelInput.Hidden = True
            Panelconfirmation.Hidden = False
            WindowBackConfirmation.Hidden = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub GridcommandDataRekening(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                If IsReportAlreadyEdited = 0 AndAlso FormAction = "Edit" AndAlso StatusReportOld <> "0" Then
                    CRSBLL.CRSDomesticBLL.SaveStateByReportID(IDUnik)
                    IsReportAlreadyEdited = 1
                    SaveHeaderStatus("10")
                End If
                RekeningClass = CRSBLL.CRSDomesticBLL.PullDataRekeningByID(Convert.ToInt64(ID))
                CRSBLL.CRSDomesticBLL.SaveDataRekening(RekeningClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Delete)
                'Dim datarekening As CRSDAL.CRS_DOMESTIC_DATA_REKENING = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.PK_CRS_DOMESTIC_DATA_REKENING_ID = ID).FirstOrDefault
                'HeaderClass.list_CRS_Domestic_Data_Rekening.Remove(datarekening)
                BindDataRekening()
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                If FormAction = "Add" Then
                    FormPanelDataRekening_PanelDataRekening_JenisData.StringFilter = " code = 'DJP1' "
                Else
                    FormPanelDataRekening_PanelDataRekening_JenisData.StringFilter = " code <> 'DJP1' "
                End If
                PengendaliEntitasPKCounter = -1
                RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Edit
                LoadGridComponentDataRekening()
                LoadDataRekening(Convert.ToInt64(ID))
                SetControlEditableRekening(True)
                FormPanelInput.Hidden = True
                FormPanelDataRekening.Hidden = False
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Detail
                LoadGridComponentDataRekening()
                LoadDataRekening(Convert.ToInt64(ID))
                SetControlEditableRekening(False)
                FormPanelInput.Hidden = True
                FormPanelDataRekening.Hidden = False
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    'Protected Sub BindDataRekening(listDataRekening As List(Of CRSDAL.CRS_DOMESTIC_DATA_REKENING))
    Protected Sub BindDataRekening()
        'Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listDataRekening)
        'Dim strQuery As String = ""
        'If FormAction = "Approval" AndAlso DataModuleApproval IsNot Nothing AndAlso DataModuleApproval.PK_ModuleAction_ID = 2 AndAlso DataModuleApproval.ModuleField IsNot Nothing Then
        '    strQuery = "select * from VW_CRS_DOMESTIC_DATA_REKENING where JenisData <> 'DJP1' AND FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
        'Else
        '    strQuery = "select * from VW_CRS_DOMESTIC_DATA_REKENING where FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
        'End If
        'Dim objtable As Data.DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString(strQuery)
        'StoreDataRekening.DataSource = objtable
        'StoreDataRekening.DataBind()
        GridPanelDataRekening.Refresh()
        GridPanelDataRekening.GetStore().Reload()
        RealoadDataCounterHeader()
    End Sub

    Protected Sub Btn_adddatarekening_click(sender As Object, e As DirectEventArgs)
        Try
            'RekeningClass = New CRSBLL.CRS_Data_Rekening_Class
            'With RekeningClass.CRS_Domestic_Data_Rekening
            '    .PK_CRS_DOMESTIC_DATA_REKENING_ID = -1
            '    .FK_CRS_DOMESTIC_HEADER_ID = HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
            'End With
            'CRSBLL.CRSDomesticBLL.SaveDataRekening(RekeningClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Add)
            If HeaderClass.CRS_Domestic_Header.InitialOrCorrection = "Initial" Then
                FormPanelDataRekening_PanelDataRekening_JenisData.StringFilter = ""
            Else
                FormPanelDataRekening_PanelDataRekening_JenisData.StringFilter = " code <> 'DJP1' "
            End If

            ClearFieldRekening()
            FormPanelDataRekening_PanelInformasi.Hidden = True
            FormPanelDataRekening_PanelDataPengendaliEntitas.Hidden = True
            If FormAction = "Add" Then
                Dim DrVW_CRS_DOMESTIC_JENIS_DATA As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRow("VW_CRS_DOMESTIC_JENIS_DATA", "code", "DJP1")
                If DrVW_CRS_DOMESTIC_JENIS_DATA IsNot Nothing Then
                    FormPanelDataRekening_PanelDataRekening_JenisData.SetTextWithTextValue(DrVW_CRS_DOMESTIC_JENIS_DATA("code"), DrVW_CRS_DOMESTIC_JENIS_DATA("DataName"))
                    JenisDataBeforeChange = DrVW_CRS_DOMESTIC_JENIS_DATA("code")
                End If
                'FormPanelDataRekening_PanelDataRekening_JenisData.SetTextValue("DJP1")
                'JenisDataBeforeChange = "DJP1"
            Else
                JenisDataBeforeChange = ""
            End If
            Dim DrTemp As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRow("VW_CRS_DOMESTIC_LJK_TYPE", "code", "DI")
            If DrTemp IsNot Nothing Then
                FormPanelDataRekening_PanelDataRekening_JenisLembagaKeuangan.SetTextWithTextValue(DrTemp("code"), DrTemp("DataName"))
            End If
            'FormPanelDataRekening_PanelDataRekening_JenisLembagaKeuangan.SetTextValue("DI")
            FormPanelDataRekening_PanelDataRekening_NPWPPengirim.Value = ParameterNPWP
            FormPanelDataRekening_PanelDataRekening_LembagaKeuanganPelapor.Value = ParameterNPWP

            'Dim headlike As String = Report_Header_PeriodeLaporan.Value & ParameterNPWP
            'Dim counterString As String = ""
            'Dim identitascounter As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString("select max(IdentitasUnik) as IdentitasUnik from CRS_DOMESTIC_DATA_REKENING where IdentitasUnik like '" & headlike & "%'")
            'If identitascounter IsNot Nothing AndAlso Not IsDBNull(identitascounter("IdentitasUnik")) Then
            '    counterString = identitascounter("IdentitasUnik")
            '    Dim counter As Integer = Convert.ToInt32((counterString.Substring(counterString.Length - 7))) + 1
            '    counterString = counter.ToString()
            'Else
            '    counterString = "1"
            'End If
            'FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Value = (headlike & counterString.PadLeft(7, "0"))

            PengendaliEntitasPKCounter = -1
            RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add
            SetControlEditableRekening(True)
            LoadGridComponentDataRekening()
            FormPanelInput.Hidden = True
            FormPanelDataRekening.Hidden = False
            RekeningClass = New CRSBLL.CRS_Data_Rekening_Class
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_DataRekening_save_Click()
        Try
            ValidationDataRekening()
            If IDUnik = Nothing Then
                With HeaderClass.CRS_Domestic_Header
                    .PK_CRS_DOMESTIC_HEADER_ID = -1
                    .NPWPLKPengirim = ParameterNPWP
                    .JumlahDataRekening = 0
                    .JumlahDataPengendaliEntitas = 0
                    .JumlahNilaiSaldo = 0
                    .Periode = (Date.Today.Year - 1).ToString()
                    .Status_Report = "0"
                End With
                CRSBLL.CRSDomesticBLL.SaveDataHeader(HeaderClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Add)
                IDUnik = HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                Report_Header_ID.Text = IDUnik
            End If

            With RekeningClass.CRS_Domestic_Data_Rekening
                If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelDataRekening_NPWPPengirim.Value) Then
                    .NPWPLembagaKeuanganPengirim = FormPanelDataRekening_PanelDataRekening_NPWPPengirim.Value
                Else
                    .NPWPLembagaKeuanganPengirim = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelDataRekening_LembagaKeuanganPelapor.Value) Then
                    .NPWPLembagaKeuanganPelapor = FormPanelDataRekening_PanelDataRekening_LembagaKeuanganPelapor.Value
                Else
                    .NPWPLembagaKeuanganPelapor = Nothing
                End If
                If RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add OrElse (FormAction = "Edit" AndAlso RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Edit AndAlso JenisDataBeforeChange = "DJP1" AndAlso JenisDataBeforeChange <> FormPanelDataRekening_PanelDataRekening_JenisData.SelectedItemValue) Then
                    .TempJenisData = .JenisData
                    .TempIdentitasUnik = .IdentitasUnik
                    'Dim headlike As String = Report_Header_PeriodeLaporan.Value & ParameterNPWP
                    Dim headlike As String = Report_Header_PeriodeLaporan.Value & .NPWPLembagaKeuanganPengirim
                    Dim counterString As String = ""
                    Dim identitascounter As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString("select max(IdentitasUnik) as IdentitasUnik from CRS_DOMESTIC_DATA_REKENING where IdentitasUnik like '" & headlike & "%'")
                    If identitascounter IsNot Nothing AndAlso Not IsDBNull(identitascounter("IdentitasUnik")) Then
                        counterString = identitascounter("IdentitasUnik")
                        Dim counter As Integer = Convert.ToInt32((counterString.Substring(counterString.Length - 7))) + 1
                        counterString = counter.ToString()
                    Else
                        counterString = "1"
                    End If
                    .IdentitasUnik = (headlike & counterString.PadLeft(7, "0"))
                    'HeaderClass.CRS_Domestic_Header.IsReportStateCode = 3
                    'IsJenisDataChange = True
                ElseIf JenisDataBeforeChange <> FormPanelDataRekening_PanelDataRekening_JenisData.SelectedItemValue AndAlso (JenisDataBeforeChange = "DJP2" OrElse JenisDataBeforeChange = "DJP3") AndAlso .TempJenisData Is Nothing Then
                    .TempJenisData = .JenisData
                    .TempIdentitasUnik = .IdentitasUnik
                    .TempIdentitasUnikKoreksi = .IdentitasUnikKoreksi
                    'Dim headlike As String = Report_Header_PeriodeLaporan.Value & ParameterNPWP
                    Dim headlike As String = Report_Header_PeriodeLaporan.Value & .NPWPLembagaKeuanganPengirim
                    Dim counterString As String = ""
                    Dim identitascounter As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString("select max(IdentitasUnik) as IdentitasUnik from CRS_DOMESTIC_DATA_REKENING where IdentitasUnik like '" & headlike & "%'")
                    If identitascounter IsNot Nothing AndAlso Not IsDBNull(identitascounter("IdentitasUnik")) Then
                        counterString = identitascounter("IdentitasUnik")
                        Dim counter As Integer = Convert.ToInt32((counterString.Substring(counterString.Length - 7))) + 1
                        counterString = counter.ToString()
                    Else
                        counterString = "1"
                    End If
                    .IdentitasUnik = (headlike & counterString.PadLeft(7, "0"))
                    'HeaderClass.CRS_Domestic_Header.IsReportStateCode = 3
                Else
                    .IdentitasUnik = FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Value
                End If
                If Not String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_JenisData.SelectedItemValue) Then
                    .JenisData = FormPanelDataRekening_PanelDataRekening_JenisData.SelectedItemValue
                Else
                    .JenisData = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Value) Then
                    .IdentitasUnikKoreksi = FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Value
                Else
                    .IdentitasUnikKoreksi = Nothing
                End If
                If Not String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_JenisLembagaKeuangan.SelectedItemValue) Then
                    .JenisLembagaKeuangan = FormPanelDataRekening_PanelDataRekening_JenisLembagaKeuangan.SelectedItemValue
                Else
                    .JenisLembagaKeuangan = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelDataRekening_NomorCIF.Value) Then
                    .NomorCIF = FormPanelDataRekening_PanelDataRekening_NomorCIF.Value
                Else
                    .NomorCIF = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelDataRekening_NomorRekening.Value) Then
                    .NomorRekening = FormPanelDataRekening_PanelDataRekening_NomorRekening.Value
                Else
                    .NomorRekening = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelDataRekening_NomorCIFandRekening.Value) Then
                    .NomorCifNomorRekening = FormPanelDataRekening_PanelDataRekening_NomorCIFandRekening.Value
                Else
                    .NomorCifNomorRekening = Nothing
                End If
                If Not String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_StatusRekening.SelectedItemValue) Then
                    .StsRekening = FormPanelDataRekening_PanelDataRekening_StatusRekening.SelectedItemValue
                Else
                    .StsRekening = Nothing
                End If
                If Not String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.SelectedItemValue) Then
                    .JnsPemegangRekening = FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.SelectedItemValue
                Else
                    .JnsPemegangRekening = Nothing
                End If
                If Not String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_MataUang.SelectedItemValue) Then
                    .MataUang = FormPanelDataRekening_PanelDataRekening_MataUang.SelectedItemValue
                Else
                    .MataUang = Nothing
                End If
                If Not String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_SaldoatauNilai.Text) Then
                    .SaldoAtauNilai = CDbl(FormPanelDataRekening_PanelDataRekening_SaldoatauNilai.Value)
                Else
                    .SaldoAtauNilai = Nothing
                End If
                If Not String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_Deviden.Text) Then
                    .Deviden = CDbl(FormPanelDataRekening_PanelDataRekening_Deviden.Value)
                Else
                    .Deviden = 0
                End If
                If Not String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_Bunga.Text) Then
                    .Bunga = CDbl(FormPanelDataRekening_PanelDataRekening_Bunga.Value)
                Else
                    .Bunga = 0
                End If
                If Not String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_PenghasilanBruto.Text) Then
                    .PhBruto = CDbl(FormPanelDataRekening_PanelDataRekening_PenghasilanBruto.Value)
                Else
                    .PhBruto = 0
                End If
                If Not String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_PenghasilanLainnya.Text) Then
                    .PhLainnya = CDbl(FormPanelDataRekening_PanelDataRekening_PenghasilanLainnya.Value)
                Else
                    .PhLainnya = 0
                End If

                If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_Nama.Value) Then
                    .NamaPemegangRek = FormPanelDataRekening_PanelInformasi_Nama.Value
                Else
                    .NamaPemegangRek = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_NamaLainnya.Value) Then
                    .NamaLainPemegangRek = FormPanelDataRekening_PanelInformasi_NamaLainnya.Value
                Else
                    .NamaLainPemegangRek = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_NPWP.Value) Then
                    .NPWPPemegangRek = FormPanelDataRekening_PanelInformasi_NPWP.Value
                Else
                    .NPWPPemegangRek = Nothing
                End If
                If .JnsPemegangRekening = "INDIVIDUAL" Then
                    If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_NIK.Value) Then
                        .NIKPemegangRek = FormPanelDataRekening_PanelInformasi_NIK.Value
                    Else
                        .NIKPemegangRek = Nothing
                    End If
                    If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_SIM.Value) Then
                        .SIMPemegangRek = FormPanelDataRekening_PanelInformasi_SIM.Value
                    Else
                        .SIMPemegangRek = Nothing
                    End If
                    If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_Paspor.Value) Then
                        .PasporPemegangRek = FormPanelDataRekening_PanelInformasi_Paspor.Value
                    Else
                        .PasporPemegangRek = Nothing
                    End If
                    'If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_Kewarganegaraan.Value) Then
                    '    .KewarganegaraanPemegangRek = FormPanelDataRekening_PanelInformasi_Kewarganegaraan.Value
                    'End If
                    If Not String.IsNullOrEmpty(FormPanelDataRekening_PanelInformasi_Kewarganegaraan.SelectedItemValue) Then
                        .KewarganegaraanPemegangRek = FormPanelDataRekening_PanelInformasi_Kewarganegaraan.SelectedItemValue
                    Else
                        .KewarganegaraanPemegangRek = Nothing
                    End If
                    If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_TempatLahir.Value) Then
                        .TempatLahirPemegangRek = FormPanelDataRekening_PanelInformasi_TempatLahir.Value
                    Else
                        .TempatLahirPemegangRek = Nothing
                    End If
                    If Not CDate(FormPanelDataRekening_PanelInformasi_TanggalLahir.Value) = DateTime.MinValue AndAlso CDate(FormPanelDataRekening_PanelInformasi_TanggalLahir.Value) >= CDate("01-01-1753") Then
                        .TglLahirPemegangRek = FormPanelDataRekening_PanelInformasi_TanggalLahir.Value
                    Else
                        .TglLahirPemegangRek = Nothing
                    End If
                ElseIf .JnsPemegangRekening = "ENTITAS" Then
                    If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_SIUP.Value) Then
                        .SIUPPemegangRek = FormPanelDataRekening_PanelInformasi_SIUP.Value
                    Else
                        .SIUPPemegangRek = Nothing
                    End If
                    If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_SITU.Value) Then
                        .SITUPemegangRek = FormPanelDataRekening_PanelInformasi_SITU.Value
                    Else
                        .SITUPemegangRek = Nothing
                    End If
                    If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_AKTA.Value) Then
                        .AktaPemegangRek = FormPanelDataRekening_PanelInformasi_AKTA.Value
                    Else
                        .AktaPemegangRek = Nothing
                    End If
                End If
                If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_AlamatDomisili.Value) Then
                    .AlamatDomPemegangRek = FormPanelDataRekening_PanelInformasi_AlamatDomisili.Value
                Else
                    .AlamatDomPemegangRek = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_AlamatUsaha.Value) Then
                    .AlamatUsahaPemegangRek = FormPanelDataRekening_PanelInformasi_AlamatUsaha.Value
                Else
                    .AlamatUsahaPemegangRek = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_AlamatKorespondensi.Value) Then
                    .AlamatKorespondensiPemegangRek = FormPanelDataRekening_PanelInformasi_AlamatKorespondensi.Value
                Else
                    .AlamatKorespondensiPemegangRek = Nothing
                End If
                .FK_CRS_DOMESTIC_HEADER_ID = HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                If RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add Then
                    .PK_CRS_DOMESTIC_DATA_REKENING_ID = -1
                    '.IsNewData = True
                End If
                .isValid = True
            End With

            For Each item In RekeningClass.list_CRS_Domestic_Pengendali_Entitas
                item.IdentitasUnik = RekeningClass.CRS_Domestic_Data_Rekening.IdentitasUnik
                item.FK_CRS_DOMESTIC_HEADER_ID = RekeningClass.CRS_Domestic_Data_Rekening.FK_CRS_DOMESTIC_HEADER_ID
                item.FK_CRS_DOMESTIC_DATA_REKENING_ID = RekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID
            Next

            Dim strValidationResult As String = CRSBLL.CRSDomesticBLL.ValidateDataRekening(RekeningClass)
            If Not String.IsNullOrWhiteSpace(strValidationResult) Then
                infoValidationResultPanelRekening.Html = strValidationResult
                FormPanelDataRekening.Body.ScrollTo(Direction.Top, 0)
                infoValidationResultPanelRekening.Hidden = False
                Throw New ApplicationException("The Data Rekening still have some invalid data.")
                Exit Sub
            Else
                infoValidationResultPanelRekening.Hidden = True
            End If

            If IsReportAlreadyEdited = 0 AndAlso FormAction = "Edit" AndAlso StatusReportOld <> "0" Then
                CRSBLL.CRSDomesticBLL.SaveStateByReportID(IDUnik)
                IsReportAlreadyEdited = 1
                SaveHeaderStatus("10")
            End If

            If RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add Then
                CRSBLL.CRSDomesticBLL.SaveDataRekening(RekeningClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Add)
                'HeaderClass.list_CRS_Domestic_Data_Rekening.Add(RekeningClass.CRS_Domestic_Data_Rekening)
            ElseIf RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Edit Then
                CRSBLL.CRSDomesticBLL.SaveDataRekening(RekeningClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Edit)
                'Dim objEdit = HeaderClass.list_CRS_Domestic_Data_Rekening.Find(Function(x) x.PK_CRS_DOMESTIC_DATA_REKENING_ID = RekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID)
                'Dim indexObjectEdit As Integer = HeaderClass.list_CRS_Domestic_Data_Rekening.IndexOf(objEdit)
                'HeaderClass.list_CRS_Domestic_Data_Rekening(indexObjectEdit) = RekeningClass.CRS_Domestic_Data_Rekening
            End If
            BindDataRekening()

            With HeaderClass.CRS_Domestic_Header
                If Not String.IsNullOrEmpty(Report_Header_NPWP.Value) Then
                    .NPWPLKPengirim = Report_Header_NPWP.Value
                Else
                    .NPWPLKPengirim = ""
                End If
                If Not String.IsNullOrEmpty(Report_Header_JumlahRekening.Text) Then
                    .JumlahDataRekening = CInt(Report_Header_JumlahRekening.Value)
                Else
                    .JumlahDataRekening = 0
                End If
                If Not String.IsNullOrEmpty(Report_Header_JumlahPengendaliEntitas.Text) Then
                    .JumlahDataPengendaliEntitas = CInt(Report_Header_JumlahPengendaliEntitas.Value)
                Else
                    .JumlahDataPengendaliEntitas = 0
                End If
                If Not String.IsNullOrEmpty(Report_Header_JumlahNilaiSaldo.Text) Then
                    .JumlahNilaiSaldo = CDbl(Report_Header_JumlahNilaiSaldo.Value)
                Else
                    .JumlahNilaiSaldo = 0
                End If
                If Not String.IsNullOrEmpty(Report_Header_PeriodeLaporan.Value) Then
                    .Periode = Report_Header_PeriodeLaporan.Value
                Else
                    .Periode = "0"
                End If
                .IsReportStateCode = 3
            End With
            CRSBLL.CRSDomesticBLL.SaveDataHeader(HeaderClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Edit)

            'UpdateDataErrorValidationMessage(IDUnik)
            'With HeaderClass.CRS_Domestic_Header
            '    .NPWPLKPengirim = Report_Header_NPWP.Value
            '    .JumlahDataRekening = Report_Header_JumlahRekening.Value
            '    .JumlahNilaiSaldo = Report_Header_JumlahNilaiSaldo.Value
            '    .JumlahDataPengendaliEntitas = Report_Header_JumlahPengendaliEntitas.Value
            '    .Periode = Report_Header_PeriodeLaporan.Value
            '    .Status_Report = "1"
            'End With
            'CRSBLL.CRSDomesticBLL.SaveDataHeader(HeaderClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Edit)
            'If FormAction = "Edit" AndAlso IsJenisDataChange Then
            '    Report_Header_Edit_Action.SelectedItem.Text = "Insert As New Report"
            '    Report_Header_Edit_Action.SelectedItem.Value = "Insert"
            '    Report_Header_Edit_Action.Selectable = False
            '    Report_Header_Edit_Action.FieldStyle = "background-color:#ddd;"
            'End If
            Dim strQuery As String = " select top 10 IdentitasUnik from CRS_DOMESTIC_DATA_REKENING "
            strQuery = strQuery & " where isValid = 0 and FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
            Dim dtinvalidDataRekening As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString(strQuery)
            If dtinvalidDataRekening IsNot Nothing AndAlso dtinvalidDataRekening.Rows.Count > 0 Then
                Dim strValidationResultHeader As String = "TOP 10 Invalid Data Rekening"
                For Each item In dtinvalidDataRekening.Rows
                    strValidationResultHeader = strValidationResultHeader & "<br/>InValid Data Rekening With IdentitasUnik : " & item("IdentitasUnik")
                Next
                infoValidationResultPanelHeader.Html = strValidationResultHeader
                infoValidationResultPanelHeader.Hidden = False
            Else
                infoValidationResultPanelHeader.Hidden = True
            End If

            'Dim listCRSDataRekeningInValid As List(Of CRSDAL.CRS_DOMESTIC_DATA_REKENING) = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.isValid <> True).ToList
            'If listCRSDataRekeningInValid.Count > 0 Then
            '    Dim strValidationResultHeader As String = ""
            '    For Each item In listCRSDataRekeningInValid
            '        strValidationResultHeader = strValidationResultHeader & "InValid Data Rekening With IdentitasUnik : " & item.IdentitasUnik & "<br/>"
            '    Next
            '    infoValidationResultPanelHeader.Html = strValidationResultHeader
            '    infoValidationResultPanelHeader.Hidden = False
            'Else
            '    infoValidationResultPanelHeader.Hidden = True
            'End If
            CheckDataHeaderEditAction()
            FormPanelInput.Hidden = False
            FormPanelDataRekening.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_DataRekening_cancel_Click()
        Try
            'If RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add Then
            '    Dim strquery As String = "DELETE from CRS_DOMESTIC_DATA_REKENING WHERE PK_CRS_DOMESTIC_DATA_REKENING_ID = " & RekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID
            '    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquery, Nothing)

            '    strquery = "DELETE from CRS_DOMESTIC_PENGENDALI_ENTITAS WHERE FK_CRS_DOMESTIC_DATA_REKENING_ID = " & HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
            '    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquery, Nothing)
            'End If
            FormPanelInput.Hidden = False
            FormPanelDataRekening.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub GridcommandPengendaliEntitas(sender As Object, e As DirectEventArgs)
        Try
            Dim ID As String = e.ExtraParams(0).Value
            If e.ExtraParams(1).Value = "Delete" Then
                RekeningClass.list_CRS_Domestic_Pengendali_Entitas.RemoveAll(Function(x) x.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID = ID)
                BindPengendaliEntitas(RekeningClass.list_CRS_Domestic_Pengendali_Entitas)
                IsDataRekeningEdited = True
            ElseIf e.ExtraParams(1).Value = "Edit" Then
                SetControlEditablePengendaliEntitas(True)
                PengendaliEntitasAction = CRSBLL.CRSDomesticBLL.enumActionForm.Edit
                WindowPengendaliEntitas.Hidden = False
                LoadDataPengendaliEntitas(ID)
            ElseIf e.ExtraParams(1).Value = "Detail" Then
                SetControlEditablePengendaliEntitas(False)
                PengendaliEntitasAction = CRSBLL.CRSDomesticBLL.enumActionForm.Detail
                WindowPengendaliEntitas.Hidden = False
                LoadDataPengendaliEntitas(ID)
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub BindPengendaliEntitas(listPengendaliEntitas As List(Of CRSDAL.CRS_DOMESTIC_PENGENDALI_ENTITAS))
        Dim objtable As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(listPengendaliEntitas)
        objtable.Columns.Add(New DataColumn("Kewarganegaraan", GetType(String)))
        If objtable.Rows.Count > 0 Then
            For Each item As Data.DataRow In objtable.Rows
                If Not IsDBNull(item("KodeNegaraCP")) Then
                    Dim temDataRow As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRow("VW_CRS_DOMESTIC_COUNTRY", "code", item("KodeNegaraCP"))
                    If Not IsDBNull(temDataRow("DataName")) Then
                        item("Kewarganegaraan") = temDataRow("DataName")
                    End If
                End If
            Next
        End If

        If FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.StringValue = "ENTITAS" Then
            If RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Detail AndAlso (FormAction = "Add" OrElse FormAction = "Edit") Then
                StorePengendaliEntitas_KhususDetail.DataSource = objtable
                StorePengendaliEntitas_KhususDetail.DataBind()
                GridPanelPengendaliEntitas.Hidden = True
                GridPanelPengendaliEntitas_KhususDetail.Hidden = False
            Else
                StorePengendaliEntitas.DataSource = objtable
                StorePengendaliEntitas.DataBind()
                GridPanelPengendaliEntitas.Hidden = False
                GridPanelPengendaliEntitas_KhususDetail.Hidden = True
            End If
        End If
    End Sub

    Protected Sub Btn_adddatapengendalientitas_click(sender As Object, e As DirectEventArgs)
        Try
            ClearFieldPengendaliEntitas()
            SetControlEditablePengendaliEntitas(True)
            PengendaliEntitasAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add
            WindowPengendaliEntitas.Hidden = False
            DataPengendaliEntitas = New CRSDAL.CRS_DOMESTIC_PENGENDALI_ENTITAS
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_PengendaliEntitas_save_Click()
        Try
            ValidationDataPengendaliEntitas()
            'CheckEditedPengendaliEntitas()
            With DataPengendaliEntitas
                If Not String.IsNullOrWhiteSpace(WindowPengendaliEntitas_Nama.Value) Then
                    .NamaCP = WindowPengendaliEntitas_Nama.Value
                Else
                    .NamaCP = Nothing
                End If
                If Not String.IsNullOrEmpty(WindowPengendaliEntitas_Kewarganegaraan.SelectedItemValue) Then
                    .KodeNegaraCP = WindowPengendaliEntitas_Kewarganegaraan.SelectedItemValue
                Else
                    .KodeNegaraCP = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(WindowPengendaliEntitas_TempatLahir.Value) Then
                    .TempatLahirCP = WindowPengendaliEntitas_TempatLahir.Value
                Else
                    .TempatLahirCP = Nothing
                End If
                If Not CDate(WindowPengendaliEntitas_TanggalLahir.Value) = DateTime.MinValue AndAlso CDate(WindowPengendaliEntitas_TanggalLahir.Value) >= CDate("01-01-1753") Then
                    .TglLahirCP = WindowPengendaliEntitas_TanggalLahir.Value
                Else
                    .TglLahirCP = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(WindowPengendaliEntitas_NPWPorTIN.Value) Then
                    .NPWPTINCP = WindowPengendaliEntitas_NPWPorTIN.Value
                Else
                    .NPWPTINCP = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(WindowPengendaliEntitas_NIK.Value) Then
                    .NIKCP = WindowPengendaliEntitas_NIK.Value
                Else
                    .NIKCP = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(WindowPengendaliEntitas_SIM.Value) Then
                    .SIMCP = WindowPengendaliEntitas_SIM.Value
                Else
                    .SIMCP = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(WindowPengendaliEntitas_Paspor.Value) Then
                    .PasporCP = WindowPengendaliEntitas_Paspor.Value
                Else
                    .PasporCP = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(WindowPengendaliEntitas_AlamatDomisili.Value) Then
                    .AlamatDomCP = WindowPengendaliEntitas_AlamatDomisili.Value
                Else
                    .AlamatDomCP = Nothing
                End If
                If Not String.IsNullOrWhiteSpace(WindowPengendaliEntitas_AlamatKorespondensi.Value) Then
                    .AlamatKorespondensiCP = WindowPengendaliEntitas_AlamatKorespondensi.Value
                Else
                    .AlamatKorespondensiCP = Nothing
                End If
                .FK_CRS_DOMESTIC_HEADER_ID = HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
            End With
            Select Case PengendaliEntitasAction
                Case CRSBLL.CRSDomesticBLL.enumActionForm.Add
                    DataPengendaliEntitas.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID = PengendaliEntitasPKCounter
                    PengendaliEntitasPKCounter = PengendaliEntitasPKCounter - 1
                    RekeningClass.list_CRS_Domestic_Pengendali_Entitas.Add(DataPengendaliEntitas)
                Case CRSBLL.CRSDomesticBLL.enumActionForm.Edit
                    Dim objedit = RekeningClass.list_CRS_Domestic_Pengendali_Entitas.Find(Function(x) x.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID = DataPengendaliEntitas.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID)
                    Dim indexObjectEdit As Integer = RekeningClass.list_CRS_Domestic_Pengendali_Entitas.IndexOf(objEdit)
                    RekeningClass.list_CRS_Domestic_Pengendali_Entitas(indexObjectEdit) = DataPengendaliEntitas
                    'objedit = DataPengendaliEntitas
            End Select
            'CRSBLL.CRSDomesticBLL.SaveDataPengendaliEntitas(DataPengendaliEntitas(), CRSBLL.CRSDomesticBLL.enumActionForm.Add)
            'CRSBLL.CRSDomesticBLL.SaveDataPengendaliEntitas(DataPengendaliEntitas(), CRSBLL.CRSDomesticBLL.enumActionForm.Edit)
            'CRSBLL.CRSDomesticBLL.SaveDataPengendaliEntitas(DataPengendaliEntitas(), CRSBLL.CRSDomesticBLL.enumActionForm.Delete)
            BindPengendaliEntitas(RekeningClass.list_CRS_Domestic_Pengendali_Entitas)
            WindowPengendaliEntitas.Hidden = True
            IsDataRekeningEdited = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Btn_PengendaliEntitas_cancel_Click()
        Try
            WindowPengendaliEntitas.Hidden = True
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub SetControlEditableHeader(isEditable As Boolean)
        Try
            Report_Header_PeriodeLaporan.Editable = isEditable
            'Report_Header_TanggalPenyampaian.Editable = isEditable
            If Not isEditable Then
                Report_Header_PeriodeLaporan.FieldStyle = "background-color:#ddd;"
                'Report_Header_TanggalPenyampaian.FieldStyle = "background-color:#ddd;"
            Else
                Report_Header_PeriodeLaporan.FieldStyle = "background-color:#fff;"
                'Report_Header_TanggalPenyampaian.FieldStyle = "background-color:#fff;"
            End If
            'Report_Header_NomorTandaTerimaElektronik.Editable = isEditable
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SetControlEditableRekening(isEditable As Boolean)
        Try
            FormPanelDataRekening_PanelDataRekening_JenisData.IsReadOnly = Not isEditable
            FormPanelDataRekening_PanelDataRekening_JenisLembagaKeuangan.IsReadOnly = Not isEditable
            FormPanelDataRekening_PanelDataRekening_NomorCIF.Editable = isEditable
            FormPanelDataRekening_PanelDataRekening_NomorRekening.Editable = isEditable
            'FormPanelDataRekening_PanelDataRekening_NomorCIFandRekening.Editable = isEditable
            FormPanelDataRekening_PanelDataRekening_StatusRekening.IsReadOnly = Not isEditable
            FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.IsReadOnly = Not isEditable
            FormPanelDataRekening_PanelDataRekening_MataUang.IsReadOnly = Not isEditable
            FormPanelDataRekening_PanelDataRekening_SaldoatauNilai.Editable = isEditable
            FormPanelDataRekening_PanelDataRekening_Deviden.Editable = isEditable
            FormPanelDataRekening_PanelDataRekening_Bunga.Editable = isEditable
            FormPanelDataRekening_PanelDataRekening_PenghasilanBruto.Editable = isEditable
            FormPanelDataRekening_PanelDataRekening_PenghasilanLainnya.Editable = isEditable

            FormPanelDataRekening_PanelInformasi_Nama.Editable = isEditable
            FormPanelDataRekening_PanelInformasi_NamaLainnya.Editable = isEditable
            FormPanelDataRekening_PanelInformasi_NPWP.Editable = isEditable
            FormPanelDataRekening_PanelInformasi_SIUP.Editable = isEditable
            FormPanelDataRekening_PanelInformasi_SITU.Editable = isEditable
            FormPanelDataRekening_PanelInformasi_AKTA.Editable = isEditable
            FormPanelDataRekening_PanelInformasi_NIK.Editable = isEditable
            FormPanelDataRekening_PanelInformasi_SIM.Editable = isEditable
            FormPanelDataRekening_PanelInformasi_Paspor.Editable = isEditable
            FormPanelDataRekening_PanelInformasi_Kewarganegaraan.IsReadOnly = Not isEditable
            FormPanelDataRekening_PanelInformasi_TempatLahir.Editable = isEditable
            FormPanelDataRekening_PanelInformasi_TanggalLahir.Editable = isEditable
            FormPanelDataRekening_PanelInformasi_AlamatDomisili.Editable = isEditable
            FormPanelDataRekening_PanelInformasi_AlamatUsaha.Editable = isEditable
            FormPanelDataRekening_PanelInformasi_AlamatKorespondensi.Editable = isEditable


            If FormAction = "Add" Then
                FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Hidden = True
                If RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add Then
                    FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Hidden = True
                Else
                    FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Hidden = False
                End If
            ElseIf FormAction = "Edit" Then
                FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Hidden = False
                If RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add Then
                    FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Hidden = True
                    FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Editable = True
                    FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.FieldStyle = "background-color:#ffe4c4;"
                    FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.AllowBlank = False
                Else
                    FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Hidden = False
                    FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Editable = False
                    FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.FieldStyle = "background-color:#ddd;"
                    FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.AllowBlank = True
                End If
            Else
                FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Editable = False
                FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.AllowBlank = True
                FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Hidden = False
                FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Hidden = False
            End If
            'If RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add Then
            '    If FormAction = "Edit" Then
            '        FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Editable = True
            '        FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.FieldStyle = "background-color:#ffe4c4;"
            '        FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.AllowBlank = False
            '    End If
            '    FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Hidden = True
            'Else
            '    FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Editable = False
            '    FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.FieldStyle = "background-color:#ddd;"
            '    FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.AllowBlank = True
            '    FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Hidden = False
            'End If
            If isEditable Then
                FormPanelDataRekening_PanelDataRekening_JenisData.StringFieldStyle = "background-color:#ffe4c4;"
                FormPanelDataRekening_PanelDataRekening_JenisLembagaKeuangan.StringFieldStyle = "background-color:#ffe4c4;"
                FormPanelDataRekening_PanelDataRekening_NomorCIF.FieldStyle = "background-color:#ffe4c4;"
                FormPanelDataRekening_PanelDataRekening_NomorRekening.FieldStyle = "background-color:#ffe4c4;"
                FormPanelDataRekening_PanelDataRekening_StatusRekening.StringFieldStyle = "background-color:#ffe4c4;"
                FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.StringFieldStyle = "background-color:#ffe4c4;"
                FormPanelDataRekening_PanelDataRekening_MataUang.StringFieldStyle = "background-color:#ffe4c4;"
                FormPanelDataRekening_PanelDataRekening_SaldoatauNilai.FieldStyle = "background-color:#ffe4c4;"
                FormPanelDataRekening_PanelDataRekening_Deviden.FieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelDataRekening_Bunga.FieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelDataRekening_PenghasilanBruto.FieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelDataRekening_PenghasilanLainnya.FieldStyle = "background-color:#fff;"

                FormPanelDataRekening_PanelInformasi_Nama.FieldStyle = "background-color:#ffe4c4;"
                FormPanelDataRekening_PanelInformasi_NamaLainnya.FieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelInformasi_NPWP.FieldStyle = "background-color:#ffe4c4;"
                FormPanelDataRekening_PanelInformasi_SIUP.FieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelInformasi_SITU.FieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelInformasi_AKTA.FieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelInformasi_NIK.FieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelInformasi_SIM.FieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelInformasi_Paspor.FieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelInformasi_Kewarganegaraan.StringFieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelInformasi_TempatLahir.FieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelInformasi_TanggalLahir.FieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelInformasi_AlamatDomisili.FieldStyle = "background-color:#ffe4c4;"
                FormPanelDataRekening_PanelInformasi_AlamatUsaha.FieldStyle = "background-color:#fff;"
                FormPanelDataRekening_PanelInformasi_AlamatKorespondensi.FieldStyle = "background-color:#fff;"
            Else
                FormPanelDataRekening_PanelDataRekening_JenisData.StringFieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelDataRekening_JenisLembagaKeuangan.StringFieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelDataRekening_NomorCIF.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelDataRekening_NomorRekening.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelDataRekening_StatusRekening.StringFieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.StringFieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelDataRekening_MataUang.StringFieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelDataRekening_SaldoatauNilai.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelDataRekening_Deviden.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelDataRekening_Bunga.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelDataRekening_PenghasilanBruto.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelDataRekening_PenghasilanLainnya.FieldStyle = "background-color:#ddd;"

                FormPanelDataRekening_PanelInformasi_Nama.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_NamaLainnya.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_NPWP.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_SIUP.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_SITU.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_AKTA.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_NIK.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_SIM.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_Paspor.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_Kewarganegaraan.StringFieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_TempatLahir.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_TanggalLahir.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_AlamatDomisili.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_AlamatUsaha.FieldStyle = "background-color:#ddd;"
                FormPanelDataRekening_PanelInformasi_AlamatKorespondensi.FieldStyle = "background-color:#ddd;"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub SetControlEditablePengendaliEntitas(isEditable As Boolean)
        Try
            WindowPengendaliEntitas_Nama.Editable = isEditable
            WindowPengendaliEntitas_Kewarganegaraan.IsReadOnly = Not isEditable
            WindowPengendaliEntitas_TempatLahir.Editable = isEditable
            WindowPengendaliEntitas_TanggalLahir.Editable = isEditable
            WindowPengendaliEntitas_NPWPorTIN.Editable = isEditable
            WindowPengendaliEntitas_NIK.Editable = isEditable
            WindowPengendaliEntitas_SIM.Editable = isEditable
            WindowPengendaliEntitas_Paspor.Editable = isEditable
            WindowPengendaliEntitas_AlamatDomisili.Editable = isEditable
            WindowPengendaliEntitas_AlamatKorespondensi.Editable = isEditable
            btn_PengendaliEntitas_save.Hidden = Not isEditable
            If isEditable Then
                WindowPengendaliEntitas_Nama.FieldStyle = "background-color:#ffe4c4;"
                WindowPengendaliEntitas_Kewarganegaraan.StringFieldStyle = "background-color:#ffe4c4;"
                WindowPengendaliEntitas_TempatLahir.FieldStyle = "background-color:#fff;"
                WindowPengendaliEntitas_TanggalLahir.FieldStyle = "background-color:#fff;"
                WindowPengendaliEntitas_NPWPorTIN.FieldStyle = "background-color:#fff;"
                WindowPengendaliEntitas_NIK.FieldStyle = "background-color:#fff;"
                WindowPengendaliEntitas_SIM.FieldStyle = "background-color:#fff;"
                WindowPengendaliEntitas_Paspor.FieldStyle = "background-color:#fff;"
                WindowPengendaliEntitas_AlamatDomisili.FieldStyle = "background-color:#ffe4c4;"
                WindowPengendaliEntitas_AlamatKorespondensi.FieldStyle = "background-color:#fff;"
            Else
                WindowPengendaliEntitas_Nama.FieldStyle = "background-color:#ddd;"
                WindowPengendaliEntitas_Kewarganegaraan.StringFieldStyle = "background-color:#ddd;"
                WindowPengendaliEntitas_TempatLahir.FieldStyle = "background-color:#ddd;"
                WindowPengendaliEntitas_TanggalLahir.FieldStyle = "background-color:#ddd;"
                WindowPengendaliEntitas_NPWPorTIN.FieldStyle = "background-color:#ddd;"
                WindowPengendaliEntitas_NIK.FieldStyle = "background-color:#ddd;"
                WindowPengendaliEntitas_SIM.FieldStyle = "background-color:#ddd;"
                WindowPengendaliEntitas_Paspor.FieldStyle = "background-color:#ddd;"
                WindowPengendaliEntitas_AlamatDomisili.FieldStyle = "background-color:#ddd;"
                WindowPengendaliEntitas_AlamatKorespondensi.FieldStyle = "background-color:#ddd;"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ClearFieldHeader()
        Try
            Report_Header_NPWP.Value = Nothing
            Report_Header_JumlahRekening.Value = Nothing
            Report_Header_JumlahPengendaliEntitas.Value = Nothing
            Report_Header_JumlahNilaiSaldo.Value = Nothing
            Report_Header_PeriodeLaporan.Value = Nothing
            'Report_Header_TanggalPenyampaian.Value = Nothing
            'Report_Header_NomorTandaTerimaElektronik.Value = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ClearFieldRekening()
        Try
            FormPanelDataRekening_PanelDataRekening_NPWPPengirim.Value = Nothing
            FormPanelDataRekening_PanelDataRekening_LembagaKeuanganPelapor.Value = Nothing
            FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Value = Nothing
            FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Value = Nothing
            FormPanelDataRekening_PanelDataRekening_NomorCIF.Value = Nothing
            FormPanelDataRekening_PanelDataRekening_NomorRekening.Value = Nothing
            FormPanelDataRekening_PanelDataRekening_NomorCIFandRekening.Value = Nothing
            FormPanelDataRekening_PanelDataRekening_SaldoatauNilai.Value = Nothing

            FormPanelDataRekening_PanelDataRekening_Deviden.Value = 0
            FormPanelDataRekening_PanelDataRekening_Bunga.Value = 0
            FormPanelDataRekening_PanelDataRekening_PenghasilanBruto.Value = 0
            FormPanelDataRekening_PanelDataRekening_PenghasilanLainnya.Value = 0

            FormPanelDataRekening_PanelInformasi_Nama.Value = Nothing
            FormPanelDataRekening_PanelInformasi_NamaLainnya.Value = Nothing
            FormPanelDataRekening_PanelInformasi_NPWP.Value = Nothing
            FormPanelDataRekening_PanelInformasi_SIUP.Value = Nothing
            FormPanelDataRekening_PanelInformasi_SITU.Value = Nothing
            FormPanelDataRekening_PanelInformasi_AKTA.Value = Nothing
            FormPanelDataRekening_PanelInformasi_NIK.Value = Nothing
            FormPanelDataRekening_PanelInformasi_SIM.Value = Nothing
            FormPanelDataRekening_PanelInformasi_Paspor.Value = Nothing
            'FormPanelDataRekening_PanelInformasi_Kewarganegaraan.Value = Nothing
            FormPanelDataRekening_PanelInformasi_TempatLahir.Value = Nothing
            FormPanelDataRekening_PanelInformasi_TanggalLahir.Value = Nothing
            FormPanelDataRekening_PanelInformasi_AlamatDomisili.Value = Nothing
            FormPanelDataRekening_PanelInformasi_AlamatUsaha.Value = Nothing
            FormPanelDataRekening_PanelInformasi_AlamatKorespondensi.Value = Nothing

            FormPanelDataRekening_PanelDataRekening_JenisData.SetTextValue("")
            FormPanelDataRekening_PanelDataRekening_JenisLembagaKeuangan.SetTextValue("")
            FormPanelDataRekening_PanelDataRekening_StatusRekening.SetTextValue("")
            FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.SetTextValue("")
            FormPanelDataRekening_PanelDataRekening_MataUang.SetTextValue("")
            FormPanelDataRekening_PanelInformasi_Kewarganegaraan.SetTextValue("")

            'Dim EmptyList As New List(Of CRSDAL.CRS_DOMESTIC_PENGENDALI_ENTITAS)
            'BindPengendaliEntitas(EmptyList)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ClearFieldPengendaliEntitas()
        Try
            WindowPengendaliEntitas_Kewarganegaraan.SetTextValue("")
            WindowPengendaliEntitas_Nama.Value = Nothing
            WindowPengendaliEntitas_TempatLahir.Value = Nothing
            WindowPengendaliEntitas_TanggalLahir.Value = Nothing
            WindowPengendaliEntitas_NPWPorTIN.Value = Nothing
            WindowPengendaliEntitas_NIK.Value = Nothing
            WindowPengendaliEntitas_SIM.Value = Nothing
            WindowPengendaliEntitas_Paspor.Value = Nothing
            WindowPengendaliEntitas_AlamatDomisili.Value = Nothing
            WindowPengendaliEntitas_AlamatKorespondensi.Value = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub Btn_Confirmation_Click()
        Try
            If FormAction = "Approval" Then
                If InStr(ObjModule.UrlApproval, "?") > 0 Then
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "&ModuleID=" & IDModule, "Loading")
                Else
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlApproval & "?ModuleID=" & IDModule, "Loading")
                End If
            Else
                If InStr(ObjModule.UrlView, "?") > 0 Then
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "&ModuleID=" & IDModule, "Loading")
                Else
                    Ext.Net.X.Redirect(NawaBLL.Common.GetApplicationPath & ObjModule.UrlView & "?ModuleID=" & IDModule, "Loading")
                End If
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub SaveHeaderStatus(status As String)
        Try
            HeaderClass.CRS_Domestic_Header.Status_Report = status
            CRSBLL.CRSDomesticBLL.SaveDataHeader(HeaderClass(), CRSBLL.CRSDomesticBLL.enumActionForm.Edit)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub onJenisDataChange()
        If FormAction = "Edit" Then
            If JenisDataBeforeChange = "DJP1" Then
                FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Value = FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Value
                'Dim headlike As String = Report_Header_PeriodeLaporan.Value & ParameterNPWP
                'Dim counterString As String = ""
                'Dim identitascounter As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString("select max(IdentitasUnik) as IdentitasUnik from CRS_DOMESTIC_DATA_REKENING where IdentitasUnik like '" & headlike & "%'")
                'If identitascounter IsNot Nothing AndAlso Not IsDBNull(identitascounter("IdentitasUnik")) Then
                '    counterString = identitascounter("IdentitasUnik")
                '    Dim counter As Integer = Convert.ToInt32((counterString.Substring(counterString.Length - 7))) + 1
                '    counterString = counter.ToString()
                'Else
                '    counterString = "1"
                'End If
                'FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Value = (headlike & counterString.PadLeft(7, "0"))
                FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Hidden = True
                'ElseIf JenisDataBeforeChange <> FormPanelDataRekening_PanelDataRekening_JenisData.SelectedItemValue AndAlso (JenisDataBeforeChange = "DJP2" OrElse JenisDataBeforeChange = "DJP3") Then
            ElseIf (JenisDataBeforeChange = "DJP2" OrElse JenisDataBeforeChange = "DJP3") Then
                FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Hidden = False
                If RekeningClass.CRS_Domestic_Data_Rekening.TempJenisData Is Nothing Then
                    FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Value = FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Value
                    FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Hidden = True
                Else
                    If RekeningClass.CRS_Domestic_Data_Rekening.TempJenisData = FormPanelDataRekening_PanelDataRekening_JenisData.SelectedItemValue Then
                        FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Value = RekeningClass.CRS_Domestic_Data_Rekening.TempIdentitasUnikKoreksi
                        FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Value = RekeningClass.CRS_Domestic_Data_Rekening.TempIdentitasUnik
                        FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Hidden = False
                    ElseIf RekeningClass.CRS_Domestic_Data_Rekening.JenisData = FormPanelDataRekening_PanelDataRekening_JenisData.SelectedItemValue Then
                        FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Value = RekeningClass.CRS_Domestic_Data_Rekening.IdentitasUnikKoreksi
                        FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Value = RekeningClass.CRS_Domestic_Data_Rekening.IdentitasUnik
                        FormPanelDataRekening_PanelDataRekening_IdentitasUnik.Hidden = False
                    End If
                End If

            End If

        End If
    End Sub

    Protected Sub onFocusLeaveCIFandRekening(sender As Object, e As DirectEventArgs)
        FormPanelDataRekening_PanelDataRekening_NomorCIFandRekening.Value = FormPanelDataRekening_PanelDataRekening_NomorCIF.Value & FormPanelDataRekening_PanelDataRekening_NomorRekening.Value
    End Sub

    Protected Sub BindRefNum(stringPeriode As String)
        Dim sqrQuery As String = "select * from CRS_DOMESTIC_REF_NUM where YearPeriod = '" & stringPeriode & "'"
        Dim objTable As Data.DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString(sqrQuery)
        StoreRefNum.DataSource = objTable
        StoreRefNum.DataBind()
        If objTable.Rows.Count = 0 Then
            PanelRefNum.Hidden = True
        Else
            PanelRefNum.Hidden = False
        End If
    End Sub

    Protected Sub ValidationDataPengendaliEntitas()
        Try
            If String.IsNullOrWhiteSpace(WindowPengendaliEntitas_Nama.Value) Then
                Throw New ApplicationException(WindowPengendaliEntitas_Nama.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(WindowPengendaliEntitas_Kewarganegaraan.SelectedItemValue) Then
                Throw New ApplicationException(WindowPengendaliEntitas_Kewarganegaraan.Label & " harus diisi.")
            End If
            If String.IsNullOrWhiteSpace(WindowPengendaliEntitas_AlamatDomisili.Value) Then
                Throw New ApplicationException(WindowPengendaliEntitas_AlamatDomisili.FieldLabel & " harus diisi.")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ValidationDataRekening()
        Try
            If String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_JenisData.SelectedItemValue) Then
                Throw New ApplicationException(FormPanelDataRekening_PanelDataRekening_JenisData.Label & " harus diisi.")
            End If

            If FormAction = "Edit" AndAlso RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add Then
                If String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Value) Then
                    Throw New ApplicationException(FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.FieldLabel & " harus diisi.")
                Else
                    Dim drTemp As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString("select count(1) as CountData from CRS_DOMESTIC_DATA_REKENING where IdentitasUnik = '" & FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Value & "' and FK_CRS_DOMESTIC_HEADER_ID <> " & IDUnik)
                    If drTemp("CountData") = 0 Then
                        Throw New ApplicationException("Identitas Unik Koreksi yang dimasukan tidak ditemukan pada Report lain yang ada pada DataBase.")
                    End If
                End If
            End If
            'If FormAction = "Edit" AndAlso RekeningAction = CRSBLL.CRSDomesticBLL.enumActionForm.Add AndAlso String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.Value) Then
            '    Throw New ApplicationException(FormPanelDataRekening_PanelDataRekening_IdentitasUnikkoreksi.FieldLabel & " harus diisi.")
            'End If

            If String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_JenisLembagaKeuangan.SelectedItemValue) Then
                Throw New ApplicationException(FormPanelDataRekening_PanelDataRekening_JenisLembagaKeuangan.Label & " harus diisi.")
            End If
            If String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelDataRekening_NomorCIF.Value) Then
                Throw New ApplicationException(FormPanelDataRekening_PanelDataRekening_NomorCIF.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelDataRekening_NomorRekening.Value) Then
                Throw New ApplicationException(FormPanelDataRekening_PanelDataRekening_NomorRekening.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_StatusRekening.SelectedItemValue) Then
                Throw New ApplicationException(FormPanelDataRekening_PanelDataRekening_StatusRekening.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.SelectedItemValue) Then
                Throw New ApplicationException(FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.Label & " harus diisi.")
            End If
            If String.IsNullOrEmpty(FormPanelDataRekening_PanelDataRekening_MataUang.SelectedItemValue) Then
                Throw New ApplicationException(FormPanelDataRekening_PanelDataRekening_MataUang.Label & " harus diisi.")
            End If
            If String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelDataRekening_SaldoatauNilai.Text) Then
                Throw New ApplicationException(FormPanelDataRekening_PanelDataRekening_SaldoatauNilai.FieldLabel & " harus diisi.")
            End If

            If String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_Nama.Value) Then
                Throw New ApplicationException(FormPanelDataRekening_PanelInformasi_Nama.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_NPWP.Value) Then
                Throw New ApplicationException(FormPanelDataRekening_PanelInformasi_NPWP.FieldLabel & " harus diisi.")
            End If
            If String.IsNullOrWhiteSpace(FormPanelDataRekening_PanelInformasi_AlamatDomisili.Value) Then
                Throw New ApplicationException(FormPanelDataRekening_PanelInformasi_AlamatDomisili.FieldLabel & " harus diisi.")
            End If
            'If FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.SelectedItemValue = "INDIVIDUAL" Then
            'ElseIf FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.SelectedItemValue = "ENTITAS" Then
            'End If
            If FormPanelDataRekening_PanelDataRekening_JenisPemegangRekening.SelectedItemValue = "ENTITAS" Then
                If RekeningClass.list_CRS_Domestic_Pengendali_Entitas.Count = 0 Then
                    Throw New ApplicationException("Data Pengendali Entitas harus diisi apabila Jenis Pemegang Rekening merupakan 'ENTITAS'.")
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub UpdateDataErrorValidationMessage(ID As Long)
        Try
            Dim strQuery As String
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Validation Satuan', 'Validate Data Satuan started (Header ID = " & ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_DataHeader_ID"
            param(0).Value = ID
            param(0).DbType = SqlDbType.BigInt

            Dim strValidationResult As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_DOMESTIC_UpdateErrorMessage", param)
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Validation Satuan', 'Validate Data Satuan finished (Header ID = " & ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub CheckEditedDataRekening()
        Try

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub CheckEditedPengendaliEntitas()
        Try
            If PengendaliEntitasAction = CRSBLL.CRSDomesticBLL.enumActionForm.Edit Then

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub StoreDataRekening_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try
            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = NawaBLL.Nawa.BLL.NawaFramework.GetWhereClauseHeader(e)

            If IDUnik <> Nothing Then
                Dim strQuery As String = ""
                If FormAction = "Approval" AndAlso DataModuleApproval IsNot Nothing AndAlso DataModuleApproval.PK_ModuleAction_ID = 2 AndAlso DataModuleApproval.ModuleField IsNot Nothing Then
                    strQuery = " JenisData <> 'DJP1' AND FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                Else
                    strQuery = " FK_CRS_DOMESTIC_HEADER_ID = " & IDUnik
                End If

                If String.IsNullOrEmpty(strfilter) Then
                    strfilter = strQuery
                Else
                    strfilter = strfilter & " and ( " & strQuery & " ) "
                End If

                intLimit = 10

                Dim strsort As String = ""
                For Each item As DataSorter In e.Sort
                    strsort += item.Property & " " & item.Direction.ToString
                Next
                'Me.indexStart = intStart
                'Me.strWhereClause = strfilter
                'Me.strOrder = strsort
                If strsort = "" Then
                    strsort = " PK_CRS_DOMESTIC_DATA_REKENING_ID asc"
                End If

                'Dim DataPaging As Data.DataTable = objTransactor.getDataPaging(strfilter, strsort, intStart, intLimit, inttotalRecord)
                Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging("VW_CRS_DOMESTIC_DATA_REKENING", "*", strfilter, strsort, intStart, intLimit, inttotalRecord)
                ''-- start paging ------------------------------------------------------------
                'Dim rangeData As List(Of Object) = If((e.Start <0 OrElse limit <0), data, data.GetRange(e.Start, limit))
                ''-- end paging ------------------------------------------------------------
                Dim limit As Integer = e.Limit
                If (e.Start + e.Limit) > inttotalRecord Then
                    limit = inttotalRecord - e.Start
                End If
                e.Total = inttotalRecord
                StoreDataRekening.DataSource = DataPaging
                Session("filterstring") = strfilter

                StoreDataRekening.DataBind()
                'Ari 15-09-2021 Inisiasi total watchlist dari store read data
                'TotalWatchlist = inttotalRecord
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

End Class