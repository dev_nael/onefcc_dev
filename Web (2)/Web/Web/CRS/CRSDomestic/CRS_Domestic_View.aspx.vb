﻿Imports Ext.Net
Imports OfficeOpenXml
Imports NawaBLL
Imports System.Data
Imports NawaDAL
Imports System.Xml
Imports System.Data.SqlClient
Imports System.IO

Partial Class CRS_Domestic_View

    Inherits Parent

    Public objFormModuleView As NawaBLL.FormModuleView

    Public Property strWhereClause() As String
        Get
            Return Session("CRS_Domestic_View.strWhereClause")
        End Get
        Set(ByVal value As String)
            Session("CRS_Domestic_View.strWhereClause") = value
        End Set
    End Property
    Public Property strOrder() As String
        Get
            Return Session("CRS_Domestic_View.strSort")
        End Get
        Set(ByVal value As String)
            Session("CRS_Domestic_View.strSort") = value
        End Set
    End Property
    Public Property indexStart() As String
        Get
            Return Session("CRS_Domestic_View.indexStart")
        End Get
        Set(ByVal value As String)
            Session("CRS_Domestic_View.indexStart") = value
        End Set
    End Property
    Public Property QueryTable() As String
        Get
            Return Session("CRS_Domestic_View.Table")
        End Get
        Set(ByVal value As String)
            Session("CRS_Domestic_View.Table") = value
        End Set
    End Property
    Public Property QueryField() As String
        Get
            Return Session("CRS_Domestic_View.Field")
        End Get
        Set(ByVal value As String)
            Session("CRS_Domestic_View.Field") = value
        End Set
    End Property

    Protected Sub ExportAllExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, 0, Integer.MaxValue, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next

                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        Dim objfileinfo As IO.FileInfo = Nothing
        Try
            If Not cboExportExcel.SelectedItem Is Nothing Then
                If cboExportExcel.SelectedItem.Value = "Excel" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".xlsx"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next


                        Using resource As New ExcelPackage(objfileinfo)
                            Dim ws As ExcelWorksheet = resource.Workbook.Worksheets.Add(objFormModuleView.ModuleName)
                            ws.Cells("A1").LoadFromDataTable(objtbl, True)
                            Dim dateformat As String = NawaBLL.SystemParameterBLL.GetDateFormat
                            Dim intcolnumber As Integer = 1
                            For Each item As System.Data.DataColumn In objtbl.Columns
                                If item.DataType = GetType(Date) Then
                                    ws.Column(intcolnumber).Style.Numberformat.Format = dateformat
                                End If
                                intcolnumber = intcolnumber + 1
                            Next
                            ws.Cells(ws.Dimension.Address).AutoFitColumns()
                            resource.Save()
                            Response.Clear()
                            Response.ClearHeaders()
                            Response.ContentType = "application/vnd.ms-excel"
                            Response.AddHeader("content-disposition", "attachment;filename=downloaddataxls.xlsx")
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "ContentType"
                            Response.BinaryWrite(IO.File.ReadAllBytes(objfileinfo.FullName))
                            Response.End()
                        End Using
                    End Using
                    'Dim json As String = Me.Hidden1.Value.ToString()
                    'Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
                    'Dim xml As XmlNode = eSubmit.Xml
                    'Me.Response.Clear()
                    'Me.Response.ContentType = "application/vnd.ms-excel"
                    'Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
                    'Dim xtExcel As New Xsl.XslCompiledTransform()
                    'xtExcel.Load(Server.MapPath("Excel.xsl"))
                    'xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
                    'Me.Response.[End]()
                ElseIf cboExportExcel.SelectedItem.Value = "CSV" Then
                    Dim tempfilexls As String = Guid.NewGuid.ToString & ".csv"
                    objfileinfo = New IO.FileInfo(Server.MapPath("~\temp\" & tempfilexls))
                    Dim stringWriter_Temp = New IO.StreamWriter(objfileinfo.FullName)
                    Using objtbl As Data.DataTable = objFormModuleView.getDataPaging(Me.strWhereClause, Me.strOrder, Me.indexStart, NawaBLL.SystemParameterBLL.GetPageSize, 0)

                        objFormModuleView.changeHeader(objtbl)
                        For Each item As Ext.Net.ColumnBase In GridpanelView.ColumnModel.Columns

                            If item.Hidden Then
                                If objtbl.Columns.Contains(item.DataIndex) Then
                                    objtbl.Columns.Remove(item.DataIndex)
                                End If

                            End If
                        Next
                        For k As Integer = 0 To objtbl.Columns.Count - 1
                            'add separator
                            stringWriter_Temp.Write(objtbl.Columns(k).ColumnName + ","c)
                        Next
                        'append new line
                        stringWriter_Temp.Write(vbCr & vbLf)
                        For i As Integer = 0 To objtbl.Rows.Count - 1
                            For k As Integer = 0 To objtbl.Columns.Count - 1
                                'add separator
                                stringWriter_Temp.Write("""" & objtbl.Rows(i).Item(k).ToString & """" + ","c)
                            Next
                            'append new line
                            stringWriter_Temp.Write(vbCr & vbLf)
                        Next
                        stringWriter_Temp.Close()
                        Response.Clear()
                        Response.AddHeader("content-disposition", "attachment;filename=downloaddatacsv.csv")
                        Response.Charset = ""
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Me.EnableViewState = False
                        'Response.ContentType = "application/ms-excel"
                        Response.ContentType = "text/csv"
                        Response.BinaryWrite(System.IO.File.ReadAllBytes(objfileinfo.FullName))
                        Response.End()
                    End Using
                Else
                    Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
                End If
            Else
                Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
            End If
        Catch ex As Exception
            Ext.Net.X.MessageBox.Alert("error", "Please Choose Format").Show()
        Finally
            If Not objfileinfo Is Nothing Then
                objfileinfo.Delete()
            End If
        End Try
    End Sub


    Protected Sub Store_ReadData(sender As Object, e As StoreReadDataEventArgs)
        Try

            'Begin Penambahan Advanced Filter
            If Session("Component_AdvancedFilter.AdvancedFilterData") = "" Then
                Toolbar2.Hidden = True
            Else
                Toolbar2.Hidden = False
            End If
            LblAdvancedFilter.Text = Session("Component_AdvancedFilter.AdvancedFilterData")
            'END Penambahan Advanced Filter


            Dim intStart As Integer = e.Start
            Dim intLimit As Int16 = e.Limit
            Dim inttotalRecord As Integer
            Dim strfilter As String = objFormModuleView.GetWhereClauseHeader(e)

            strfilter = strfilter.Replace("Active", objFormModuleView.ModuleName & ".Active")


            Dim strsort As String = "PK_CRS_DOMESTIC_HEADER_ID DESC"
            For Each item As DataSorter In e.Sort
                strsort += ", " & item.Property & " " & item.Direction.ToString
            Next
            Me.indexStart = intStart
            Me.strWhereClause = strfilter
            'Begin Penambahan Advanced Filter
            If strWhereClause.Length > 0 Then
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= "and " & Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            Else
                If Not Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = "" Then
                    strWhereClause &= Session("Component_AdvancedFilter.AdvancedFilterDataQuery")
                    intStart = 0
                End If
            End If
            'END Penambahan Advanced Filter

            Me.strOrder = strsort

            'QueryTable = "vw_SIPENDAR_PROFILE"
            'QueryField = "*"
            'Dim DataPaging As DataTable = SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)

            Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)

            'Dim DataPaging As Data.DataTable = objFormModuleView.getDataPaging(strWhereClause, strsort, intStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = NawaDAL.SQLHelper.ExecuteTabelPaging(QueryTable, QueryField, strWhereClause, strOrder, indexStart, intLimit, inttotalRecord)
            'Dim DataPaging As Data.DataTable = DLL.SQLHelper.ExecuteTabelPaging("CustomerInformation_WebTempTable", "CIFNo, Name, DateOfBirth, AccountOwnerId, OpeningDate, WorkingUnitName, IsCustomerInList", strfilter, strsort, intStart, intLimit, inttotalRecord)
            ''-- start paging ------------------------------------------------------------
            Dim limit As Integer = e.Limit
            If (e.Start + e.Limit) > inttotalRecord Then
                limit = inttotalRecord - e.Start
            End If
            'Dim rangeData As List(Of Object) = If((e.Start < 0 OrElse limit < 0), data, data.GetRange(e.Start, limit))
            ''-- end paging ------------------------------------------------------------
            e.Total = inttotalRecord
            GridpanelView.GetStore.DataSource = DataPaging
            GridpanelView.GetStore.DataBind()

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'Begin Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdvancedFilter_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            Dim intModuleid As Integer = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
            Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)

            AdvancedFilter1.TableName = Nothing
            AdvancedFilter1.objListModuleField = objFormModuleView.objSchemaModuleField

            Dim objwindow As Ext.Net.Window = Ext.Net.X.GetCmp("WindowFilter")
            objwindow.Hidden = False
            AdvancedFilter1.BindData()
            'AdvancedFilter1.ClearFilter()
            AdvancedFilter1.StoreToReleoad = "StoreView"

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As DirectEventArgs)
        Try
            Session("Component_AdvancedFilter.AdvancedFilterData") = Nothing
            Session("Component_AdvancedFilter.AdvancedFilterDataQuery") = Nothing
            Session("Component_AdvancedFilter.objTableFilter") = Nothing
            Toolbar2.Hidden = True
            StoreView.Reload()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub
    'End Update Penambahan Advance Filter
    <DirectMethod>
    Public Sub BtnAdd_Click()
        Try

            Dim Moduleid As String = Request.Params("ModuleID")

            If objFormModuleView.objSchemaModule.UrlAdd.ToString.Contains("?") Then
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "&ModuleID={0}", Moduleid), "Loading")
            Else
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")
            End If

            'Dim sm As RowSelectionModel = TryCast(Me.GridpanelView.GetSelectionModel(), RowSelectionModel)
            'Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlAdd & "?ModuleID={0}", Moduleid), "Loading")

        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Private Sub CRS_Domestic_View_Init(sender As Object, e As EventArgs) Handles Me.Init
        objFormModuleView = New NawaBLL.FormModuleView(Me.GridpanelView, Me.BtnAdd)
    End Sub

    Private Sub CRS_Domestic_View_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            Dim Moduleid As String = Request.Params("ModuleID")
            Dim intModuleid As Integer
            Try
                intModuleid = NawaBLL.Common.DecryptQueryString(Moduleid, NawaBLL.SystemParameterBLL.GetEncriptionKey)
                Dim objmodule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleID(intModuleid)


                If Not NawaBLL.ModuleBLL.GetHakAkses(NawaBLL.Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, NawaBLL.Common.ModuleActionEnum.view) Then
                    Dim strIDCode As String = 1
                    strIDCode = NawaBLL.Common.EncryptQueryString(strIDCode, NawaBLL.SystemParameterBLL.GetEncriptionKey)

                    Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/UnAuthorizeAccess.aspx?ID={0}", strIDCode), "Loading...")
                    Exit Sub
                End If

                objFormModuleView.ModuleID = objmodule.PK_Module_ID
                objFormModuleView.ModuleName = objmodule.ModuleName

                objFormModuleView.AddField("PK_CRS_DOMESTIC_HEADER_ID", "CRS Domestic ID", 1, True, True, NawaBLL.Common.MFieldType.BIGIDENTITY)
                objFormModuleView.AddField("NPWPLKPengirim", "NPWP Lembaga Keuangan", 2, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("JumlahDataRekening", "Jumlah Data Rekening", 3, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("JumlahDataPengendaliEntitas", "Jumlah Data Pengendali Entitas", 4, False, True, NawaBLL.Common.MFieldType.BIGINTValue)
                objFormModuleView.AddField("JumlahNilaiSaldo", "Jumlah Nilai Saldo", 5, False, True, NawaBLL.Common.MFieldType.NUMERICDECIMALValue,,,,, 2)
                objFormModuleView.AddField("Periode", "Periode Laporan", 6, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("TanggalPenyampaian", "Tanggal Penyampaian", 7, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                objFormModuleView.AddField("NomorTandaTerimaElektronik", "Nomor Tanda Terima Elektronik", 8, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                'objFormModuleView.AddField("Status_Report", "Status Report", 9, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("Status_Report", "Status Report", 9, False, True, NawaBLL.Common.MFieldType.ReferenceTable, "CRS_Domestic_Ref_Status_Report", "PK_Status_Report_ID", "Description")
                'objFormModuleView.AddField("Reporting_Date", "Reporting date", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                'objFormModuleView.AddField("StatusGenerateExcel", "Status Generate Excel", 11, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                'objFormModuleView.AddField("StatusGenerateXML", "Status Generate XML", 12, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                'objFormModuleView.AddField("DateGenerateExcel", "Tanggal Generate Excel", 13, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                'objFormModuleView.AddField("DateGenerateXML", "Tanggal Generate XML", 14, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                'objFormModuleView.AddField("LJK_Type", "Jenis Lembaga Keuangan", 15, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                objFormModuleView.AddField("ErrorMsg", "Error Message", 10, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("IsValid", "Is Valid", 11, False, True, NawaBLL.Common.MFieldType.BooleanValue)
                objFormModuleView.AddField("StatusGenerate", "Status API", 12, False, True, NawaBLL.Common.MFieldType.ReferenceTable, "CRS_STATUS_GENERATE_API", "KODE", "KETERANGAN")
                objFormModuleView.AddField("ErrorMessage", "Error API", 13, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("InitialOrCorrection", "Initial Or Correction", 14, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("LastUpdateBy", "Last Update By", 15, False, True, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("LastUpdateDate", "Last Update Date", 16, False, True, NawaBLL.Common.MFieldType.DATETIMEValue)
                objFormModuleView.AddField("ParameterValue", "ParameterValue", 17, False, False, NawaBLL.Common.MFieldType.VARCHARValue)
                objFormModuleView.AddField("StatusReportPrep", "StatusReportPrep", 18, False, False, NawaBLL.Common.MFieldType.VARCHARValue)

                objFormModuleView.SettingFormView()
                'Custom GridCommand
                Dim GlobalParameterValue As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select ParameterValue from CRS_DOMESTIC_GLOBAL_PARAMETER where PK_CRS_DOMESTIC_GLOBAL_PARAMETER_ID = 10", Nothing)
                If GlobalParameterValue <> "1" Then
                    GlobalParameterValue = "0"
                End If
                Dim objcommandcol As Ext.Net.CommandColumn = GridpanelView.ColumnModel.Columns.Find(Function(x) x.ID = "columncrud")
                objcommandcol.Commands.Clear()
                Dim intCountAkses As Integer = 0
                Dim intDeletePosition As Integer = 0
                Dim intEditPosition As Integer = 0
                'GridpanelView.ColumnModel.Columns.Remove(objcommandcol)
                Dim objGenerate As New GridCommand With {
                    .CommandName = "Generate",
                    .Icon = Icon.DiskDownload,
                    .Text = "Generate Report"
                }
                objGenerate.ToolTip.Text = "Generate Report"
                objcommandcol.Commands.Add(objGenerate)
                If objmodule IsNot Nothing Then
                    If Not ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, Common.ModuleActionEnum.Insert) Then
                        BtnAdd.Hidden = True
                    End If
                    If ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, Common.ModuleActionEnum.Detail) Then
                        Dim objDetail As New GridCommand With {
                            .CommandName = "Detail",
                            .Icon = Icon.ApplicationViewDetail,
                            .Text = "Detail"
                        }
                        objDetail.ToolTip.Text = "Detail"
                        objcommandcol.Commands.Add(objDetail)
                        intCountAkses = intCountAkses + 1
                    End If
                    If ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, Common.ModuleActionEnum.Update) Then
                        Dim objEdit As New GridCommand With {
                            .CommandName = "Edit",
                            .Icon = Icon.BulletEdit,
                            .Text = "Edit"
                        }
                        objEdit.ToolTip.Text = "Edit"
                        objcommandcol.Commands.Add(objEdit)
                        intCountAkses = intCountAkses + 1
                        intEditPosition = intCountAkses
                    End If
                    If ModuleBLL.GetHakAkses(Common.SessionCurrentUser.FK_MGroupMenu_ID, objmodule.PK_Module_ID, Common.ModuleActionEnum.Delete) Then
                        Dim objDelete As New GridCommand With {
                            .CommandName = "Delete",
                            .Icon = Icon.PencilDelete,
                            .Text = "Delete"
                        }
                        objDelete.ToolTip.Text = "Delete"
                        objcommandcol.Commands.Add(objDelete)
                        intCountAkses = intCountAkses + 1
                        intDeletePosition = intCountAkses
                    End If
                End If
                objcommandcol.Width = 350
                'Dim objGenerate As New GridCommand With {
                '    .CommandName = "Generate",
                '    .Icon = Icon.DiskDownload,
                '    .Text = "Generate Report"
                '}
                'objGenerate.ToolTip.Text = "Generate Report"
                'objcommandcol.Commands.Add(objGenerate)

                Dim extparam As New Ext.Net.Parameter
                extparam.Name = "unikkey"
                extparam.Value = "record.data.PK_CRS_DOMESTIC_HEADER_ID"
                extparam.Mode = ParameterMode.Raw
                objcommandcol.DirectEvents.Command.ExtraParams.Add(extparam)

                AddHandler objcommandcol.DirectEvents.Command.Event, AddressOf Generate_satuan

                'objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection" & intCountAkses & GlobalParameterValue
                objcommandcol.PrepareToolbar.Fn = "prepareCommandCollection"
                If intEditPosition <> 0 Then
                    objcommandcol.PrepareToolbar.Fn = "prepareCommandEdit" & intEditPosition
                ElseIf intDeletePosition <> 0 Then
                    objcommandcol.PrepareToolbar.Fn = "prepareCommandDelete" & intDeletePosition
                End If
            Catch ex As Exception
                Throw ex
            End Try

            If Not Ext.Net.X.IsAjaxRequest Then
                cboExportExcel.SelectedItem.Text = "Excel"
                cb_generate_export.SelectedItem.Text = "Excel"
            End If
        Catch ex As Exception When TypeOf ex Is ApplicationException
            Ext.Net.X.Msg.Alert("Information", ex.Message).Show()
        Catch ex As Exception
            Elmah.ErrorSignal.FromCurrentContext.Raise(ex)
            Ext.Net.X.Msg.Alert("Error", ex.Message).Show()
        End Try
    End Sub

    Protected Sub Generate_Bulk()
        Window_Generate.Hidden = False
        Window_Generate.Title = "Generate Report Domestic Bulk"
    End Sub

    Protected Sub btn_generate_Close_Click()
        Window_Generate.Hidden = True
    End Sub

    Protected Sub Generate_satuan(sender As Object, e As DirectEventArgs)
        Dim Moduleid As String = Request.Params("ModuleID")
        Dim PKHeaderID As Long = e.ExtraParams("unikkey")
        Dim PKID As String = NawaBLL.Common.EncryptQueryString(PKHeaderID, NawaBLL.SystemParameterBLL.GetEncriptionKey)

        If e.ExtraParams("command") = "Generate" Then
            'Dim CountRekening As Integer = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select count(1) as CountGenerate FROM CRS_DOMESTIC_GenerateXMLParalel where FK_CRS_DOMESTIC_HEADER_ID = " & PKHeaderID, Nothing)
            'If CountRekening = 0 Then
            '    Dim strQuery As String = " insert into CRS_DOMESTIC_GenerateXMLParalel(FK_CRS_DOMESTIC_HEADER_ID, YearPeriod, StatusGenerate) "
            '    strQuery = strQuery & " select PK_CRS_DOMESTIC_HEADER_ID, Periode, 0 from CRS_DOMESTIC_HEADER "
            '    strQuery = strQuery & " where PK_CRS_DOMESTIC_HEADER_ID = " & PKHeaderID
            '    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            'End If
            'Window_Generate.Hidden = False
            'Window_Generate.Title = "Generate Report Domestic Satuan"
            Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & "/CRS/CRSDomestic/CRS_Domestic_Generate.aspx" & "?ModuleID={0}&ID={1}", Moduleid, PKID), "Loading")
        ElseIf e.ExtraParams("command") = "Edit" Then
            If objFormModuleView.objSchemaModule.UrlAdd.ToString.Contains("?") Then
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlEdit & "&ModuleID={0}&ID={1}", Moduleid, PKID), "Loading")
            Else
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlEdit & "?ModuleID={0}&ID={1}", Moduleid, PKID), "Loading")
            End If
        ElseIf e.ExtraParams("command") = "Detail" Then
            If objFormModuleView.objSchemaModule.UrlAdd.ToString.Contains("?") Then
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlDetail & "&ModuleID={0}&ID={1}", Moduleid, PKID), "Loading")
            Else
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlDetail & "?ModuleID={0}&ID={1}", Moduleid, PKID), "Loading")
            End If
        ElseIf e.ExtraParams("command") = "Delete" Then
            If objFormModuleView.objSchemaModule.UrlAdd.ToString.Contains("?") Then
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlDelete & "&ModuleID={0}&ID={1}", Moduleid, PKID), "Loading")
            Else
                Ext.Net.X.Redirect(String.Format(NawaBLL.Common.GetApplicationPath & objFormModuleView.objSchemaModule.UrlDelete & "?ModuleID={0}&ID={1}", Moduleid, PKID), "Loading")
            End If
        End If
    End Sub
End Class
