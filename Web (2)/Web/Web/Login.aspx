﻿    <%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" ValidateRequest="true" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   
    <title>Log in</title>



    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="styles/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="styles/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="styles/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="styles/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="styles/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <style>
        .btn-disabled {color:red !important;}
        .button-text .x-btn-inner-default-small {
    color: darkred !important;
    font-size:15px !important
}
     </style>
</head>
<%--<body class="hold-transition login-page" onload="document.forms[0]['txtUsername'].focus();">--%>
<body class="hold-transition">
                <form id="form2" runat="server" autocomplete="off">

    <%--<div class="login-box">--%>
    <div class="col-md-7 offset-md-2 no-float" style="padding-left:100px;padding-right:50px;background-color:white;min-height:560px">

     
        <!-- /.login-logo -->
        <div class="login-box-body no-float" style="background-image: linear-gradient(to bottom, rgba(242, 245, 248, 1), white);/*background-color:#F2F5F8;*/display: flex;flex-direction: column;min-height:560px;padding-left:40px;padding-right:40px;" >
           
            <div style="text-align: center;padding-top:50px;text-align:left;margin-left:-20px">
                <img src="images/nawadata_logo.png" style="max-width: 450px; max-height: 370px; width: 100%; height: 100%;background-image: none;margin-left:10px" />
            </div>
            <br/>
<%--            <form id="form1" runat="server" autocomplete="off">--%>
                <ext:ResourceManager ID="ResourceManager1" runat="server" Theme="Crisp" />
                <div class="form-group has-feedback">
                    <%--   <input type="email" id="txtEmail" runat="server" class="form-control" placeholder="User ID" >--%>
                    <asp:TextBox ID="txtUsername" runat="server" class="form-control" placeholder="User ID" style="padding:20px !important; border-radius:20px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="help-block" ControlToValidate="txtUsername" ErrorMessage="User ID is required"></asp:RequiredFieldValidator>
                    <span class="glyphicon glyphicon-user form-control-feedback" style="padding-top:5px;padding-right:20px;"></span>
                </div>
                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" class="form-control" placeholder="Password" onkeydown="if (event.keyCode == 13)
 {document.getElementById('btnLogin').click(); return false;}"  style="padding:20px !important; border-radius:20px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="help-block" ControlToValidate="txtPassword" ErrorMessage="Password is required"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage=""></asp:CustomValidator>
                    <span class="glyphicon glyphicon-lock form-control-feedback" style="padding-top:5px;padding-right:20px;"></span>
                </div>
                <div class="row" style="text-align: center" >

                    <!-- /.col -->
                    <div class="col-xs-12">
                        <button type="button" id="btnLogin" runat="server" style="background-color: #82171d; padding:10px; border-radius:20px;" class="btn btn-primary btn-block ">Sign In</button>

                        <div class="col-xs-12">
                            <label runat="server" id="lblOR">Or</label>
                        </div>

                        <button type="button" id="btnSSO" runat="server" CausesValidation="false" style="background-color: #82171d; padding:10px; border-radius:20px;" class="btn btn-primary btn-block ">Login With SSO</button>
                        <%--<ext:Button ID="btnSSO" runat="server" Text="Login With SSO" Icon="Accept" Flex="1">
                            <DirectEvents>
                                <Click OnEvent="btnSSO_Click">
                                    <EventMask ShowMask="true" Msg="Verifying..." MinDelay="500" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>--%>
                    </div>
                    <!-- /.col -->
                </div>

            <%--Add 20-Sep-2022 , Tambah Login by SSO--%>
           <%-- <div class="row" style="text-align: center" >

                <!-- /.col -->
                <div class="col-xs-12">
                    <label runat="server" >Or</label>
                </div>
                <!-- /.col -->
            </div>--%>

            <%--<div class="row" style="text-align: center" >
                <!-- /.col -->
                <div class="col-xs-12">
                    <button type="button" id="btnSSO" runat="server" style="background-color: #82171d; padding:10px; border-radius:20px;" class="btn btn-primary btn-block ">Login With SSO</button>
                </div>
                <!-- /.col -->
            </div>--%>

            

        <%--    <ext:panel runat="server" ID="PanelBtnSSO" Layout="FormLayout" ButtonAlign="Center">
                    <Buttons>
                    <ext:Button ID="btnSSO" runat="server" Text="Login With SSO" Icon="Accept" Flex="1">
                        <DirectEvents>
                            <Click OnEvent="btnSSO_Click">
                                <EventMask ShowMask="true" Msg="Verifying..." MinDelay="500" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:panel>--%>
             <%--End 20-Sep-2022 , Tambah Login by SSO--%>

                <div id="divMsg" style="display:none;">
                    <img src="images/loading.gif" alt="Please wait.." />
                </div>
             <div style="text-align:justify;color:red;margin-top:20px">PERINGATAN!<br />
                Sistem ini hanya boleh diakses oleh pengguna yang berhak dan digunakan untuk kepentingan Nawa Data.
                Penyalahgunaan dapat dikategorikan sebagai pelanggaran dan/atau tindakan melawan hukum dan dapat berakibat terhadap sanksi
                sesuai dengan peraturan/ketentuan yang berlaku
                <br /><br />
            </div>
            <%--</form>--%>
            <!-- /.social-auth-links -->

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

    <!-- news-box -->
    <div class="col-md-5 offset-md-1" style="margin-left:-20px;padding-right:50px; border-image: linear-gradient(to bottom, lightgray, rgba(0, 0, 0, 0)) 1 100%; border-width:1px;margin-right:-10px">
        <div class="row" style="padding:10px;">
            <div class="col-lg-12 col-sm-12">
                <%--<h3 style="color:#465672"><i class="fa fa-newspaper-o"></i> &nbsp;AML News</h3>--%>
                <h3 style="color:#465672; margin-top:10px;">AML News</h3>
            </div>
        </div>
        <div class="row" runat="server" id="rowNews">
            <!-- begin post -->
        
            <asp:Repeater ID="rptNews" runat="server">
                <ItemTemplate>
                 <%--   <div class="col-lg-12 col-sm-12" style="width:200px" runat="server" visible='<%# Eval("News_Image").ToString.Trim.Length > 0 %>'>
                                        <div class="card-block">
        <img style="width:190px;height:174px;margin-left:-40px;margin-right:0px" src="<%# Eval("News_Image") %>" alt="News Picture">
                </div>
                            </div>--%>
                        <div class="col-lg-12 col-sm-12" runat="server">
                                <div class="box box-solid" style="margin-bottom:5px;">
                    <div class="box-body">
                  
                        <div class="media">
                            <div class="media-left" runat="server" visible='<%# Not IsDBNull(Eval("News_Image_URL"))%>'>
                               <img src="<%# Eval("News_Image_URL") %>" class="media-object" style="position: static;width: 75px;height: 75px;left: 0px;top: 0px;flex: none;order: 0;flex-grow: 0;margin: 0px 10px;border-radius: 50%;">
                            </div>
                            <div class="media-left" runat="server" visible='<%# IsDBNull(Eval("News_Image_URL"))%>'>
                                <span class="info-box-icon bg-grey" style="width: 75px;height: 75px;left: 0px;top: 0px;flex: none;order: 0;flex-grow: 0;margin: 0px 10px;border-radius: 50%; font-size:30px; color:white;"><i class="fa fa-picture-o" style="padding-top:-10px;"></i></span>                            
                            </div>
                            <div class="media-body">
                                <div class="clearfix">
                                    <h4 style="margin-top:0;">  <asp:LinkButton ID="LinkButton2" runat="server"  CausesValidation="false" CommandName="remove" CommandArgument='<%#Eval("PK_AML_News_ID") %>' Style="color:#465672 !important;"><%# Eval("News_Title") %></asp:LinkButton></h4>
                                   
                                    <span style="margin-bottom:0; color:grey;">
                                        <%--<%# Eval("CreatedBy") %> - <small class="text-muted"><%# Eval("Dates") %></small>--%>
                                        <%# Eval("Dates") %>
                                    </span>
                                  <%--   <p class="pull-right">
                                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Read more"  style="background-color:darkred" CausesValidation="false" CssClass="btn btn-success btn-xs" CommandName="remove" CommandArgument='<%#Eval("PK_AML_News_ID") %>'></asp:LinkButton>
  
                                    </p>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                      
                   
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="row" runat="server" id="rowMoreNews" style="margin-bottom:50px;margin-right:5px">
            <div class="col-lg-12 col-md-12 col-sm-12 text-right button-text">
                <ext:Button runat="server" ID="btnMoreNews" Text="More News ➜" style="height:30px; width:150px;background-color:white;border:none">
                    <DirectEvents>
                        <Click OnEvent="btnMoreNews_Click">
                            <EventMask ShowMask="true" Msg="Loading..."></EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </div>
        </div>

        <%-- No News available --%>
        <div class="row" runat="server" id="rowNoNews" >
              <div class="callout callout-info">
                <h4><i class="icon fa fa-info-circle"></i> Information</h4>
                <p>There is no news available for this moment.</p>
              </div>
        </div>
        <%-- /.No News available --%>
    </div>
    <!-- /.news-box -->
    
    </form>

    <!-- jQuery 2.2.3 -->
    <script src="styles/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="styles/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="styles/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });

        $('#btnLogin').click(function () {
            $(this).attr('disabled', 'disabled');
            $(this).html("Please Wait....");
            //   $('#divMsg').show();
            //your client side validation here

            if (valid)
                return true;
            else {
                $(this).removeAttr('disabled');
                $('#divMsg').hide();
                return false;
            }
        });

        $('#btnSSO').click(function () {
            //$(this).attr('disabled', 'disabled');
            $(this).html("Verifying....");
            //   $('#divMsg').show();
            //your client side validation here

            //if (valid)
            //    return true;
            //else {
            //    $(this).removeAttr('disabled');
            //    $('#divMsg').hide();
            //    return false;
            //}
        });
    </script>
</body>
</html>
