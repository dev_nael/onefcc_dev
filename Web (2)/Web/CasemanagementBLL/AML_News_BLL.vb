﻿Imports Ext.Net
Imports CasemanagementDAL
Imports NawaDAL

Public Class AML_News_BLL

    Shared Function getNewsClassByID(id As Integer) As AML_News_Class
        Dim objNewsClass As New AML_News_Class
        Using objdb As New CasemanagementEntities
            Dim objNews As AML_News = objdb.AML_News.Where(Function(x) x.PK_AML_News_ID = id).FirstOrDefault
            Dim listNewsAccess As List(Of AML_News_Access) = objdb.AML_News_Access.Where(Function(x) x.FK_AML_News_ID = id).ToList
            If objNews IsNot Nothing Then
                objNewsClass.objNews = objNews
            End If
            If listNewsAccess IsNot Nothing Then
                objNewsClass.listNewsAccess = listNewsAccess
            End If
        End Using
        Return objNewsClass
    End Function

    Shared Function getRoleByID(id As Integer) As NawaDAL.MRole
        Dim objRole As New NawaDAL.MRole
        Using objdb As New NawaDAL.NawaDataEntities
            Dim Role As NawaDAL.MRole = objdb.MRoles.Where(Function(x) x.PK_MRole_ID = id).FirstOrDefault
            If Role IsNot Nothing Then
                objRole = Role
            End If
        End Using
        Return objRole
    End Function

    'Ari 23 Sept 2021 penambahan untuk get data mrole by userid
    Shared Function getRoleByUserID(userID As String) As Integer
        Using objdb As New NawaDAL.NawaDataEntities
            Try
                Dim Role As NawaDAL.MUser = objdb.MUsers.Where(Function(x) x.UserID = userID).FirstOrDefault
                If Role IsNot Nothing Then
                    Return Role.FK_MRole_ID
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Shared Sub DeleteTanpaApproval(objNewsClass As AML_News_Class, objModule As NawaDAL.Module)
        Using objDb As New CasemanagementEntities
            Dim roles As String = ""
            Dim pkaml As Integer = 0
            Dim jumlahrow As Integer = 0
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try
                    With objNewsClass.objNews
                        roles = .IsAllRoles
                        pkaml = .PK_AML_News_ID
                    End With

                    'Delete News
                    objDb.Entry(objNewsClass.objNews).State = Entity.EntityState.Deleted
                    objDb.SaveChanges()

                    'Delete News Access
                    For Each item In objNewsClass.listNewsAccess
                        objDb.Entry(item).State = Entity.EntityState.Deleted
                        objDb.SaveChanges()
                    Next

                    'AuditTrail
                    Dim header As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Delete, objModule.ModuleLabel)

                    'AuditTrailDetail
                    NawaFramework.CreateAuditTrailDetailDelete(objDb, header.PK_AuditTrail_ID, objNewsClass.objNews)

                    If objNewsClass.listNewsAccess IsNot Nothing AndAlso objNewsClass.listNewsAccess.Count > 0 Then
                        For Each item In objNewsClass.listNewsAccess
                            NawaFramework.CreateAuditTrailDetailDelete(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If
                    jumlahrow = objNewsClass.listNewsAccess.Count
                    objDb.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
            If jumlahrow > 0 Then
                If roles = "True" Then
                    SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "delete from aml_news_access where fk_aml_news_id =  " & pkaml, Nothing)

                End If
            End If

        End Using
    End Sub

    Public Shared Sub DeleteWithApproval(objNewsClass As AML_News_Class, objModule As NawaDAL.Module)
        Using objDb As New CasemanagementEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try

                    Dim xmldata As String = NawaBLL.Common.Serialize(objNewsClass)
                    Dim objModuleApproval As New CasemanagementDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = objNewsClass.objNews.PK_AML_News_ID
                        .ModuleField = xmldata
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objDb.Entry(objModuleApproval).State = Entity.EntityState.Added

                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub SettingColor(formPanelOld As FormPanel, formPanelNew As FormPanel, unikkeyold As String, unikkeynew As String)
        Try
            Dim objdisplayOld As DisplayField
            Dim objdisplayNew As DisplayField

            Dim listfieldnews As List(Of String) = New List(Of String)
            listfieldnews.Add("PK_AML_NEWS_ID")
            listfieldnews.Add("Title")
            listfieldnews.Add("News_Content")
            listfieldnews.Add("News_Content_Short")
            listfieldnews.Add("News_Image")
            listfieldnews.Add("Attachment_Name")
            listfieldnews.Add("Valid_From")
            listfieldnews.Add("Valid_To")
            listfieldnews.Add("CreatedBy")

            For Each item As String In listfieldnews
                objdisplayOld = formPanelOld.FindControl(item & unikkeynew)
                objdisplayNew = formPanelNew.FindControl(item & unikkeyold)
                If Not objdisplayOld Is Nothing And Not objdisplayNew Is Nothing Then

                    If objdisplayOld.Text <> objdisplayNew.Text Then
                        objdisplayOld.FieldStyle = "Color:red"
                        objdisplayNew.FieldStyle = "Color:red"
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'ari 23 Sept 2021 penambahan untuk menandai perubahaan di grid
    Public Shared Sub SettingColorGrid(formPanelOld As GridPanel, formPanelNew As GridPanel)
        Try
            Dim objdisplayOld As Column
            Dim objdisplayNew As Column

            Dim listfieldnews As List(Of String) = New List(Of String)
            listfieldnews.Add("PK_MRole_ID")
            listfieldnews.Add("RoleName")


            For Each item As String In listfieldnews
                objdisplayOld = formPanelOld.FindControl(item)
                objdisplayNew = formPanelNew.FindControl(item)
                If Not objdisplayOld Is Nothing And Not objdisplayNew Is Nothing Then

                    If objdisplayOld.Text <> objdisplayNew.Text Then
                        objdisplayOld.StyleSpec = "Color:red"
                        objdisplayNew.StyleSpec = "Color:red"
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Shared Sub SaveAddTanpaApproval(objNewsClass As AML_News_Class, objModule As NawaDAL.Module)
        Using objDb As New CasemanagementEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try

                    With objNewsClass.objNews
                        If String.IsNullOrEmpty(.CreatedBy) Then
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = Now
                        End If

                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With

                    objDb.Entry(objNewsClass.objNews).State = Entity.EntityState.Added
                    objDb.SaveChanges()

                    'Add News Access
                    For Each item In objNewsClass.listNewsAccess
                        objDb.Entry(item).State = Entity.EntityState.Added
                        objDb.SaveChanges()
                    Next

                    'AuditTrail
                    Dim header As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                    'AuditTrailDetail
                    NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, objNewsClass.objNews)

                    If objNewsClass.listNewsAccess IsNot Nothing AndAlso objNewsClass.listNewsAccess.Count > 0 Then
                        For Each item In objNewsClass.listNewsAccess
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()

                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub SaveAddWithApproval(objNewsClass As AML_News_Class, objModule As NawaDAL.Module)
        Using objDb As New CasemanagementEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try


                    Dim xmldata As String = NawaBLL.Common.Serialize(objNewsClass)

                    Dim objModuleApproval As New CasemanagementDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = 0 '---- Module Key ????' -----
                        .ModuleField = xmldata
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objDb.Entry(objModuleApproval).State = Entity.EntityState.Added

                    'AuditTrail
                    Dim header As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                    'AuditTrailDetail
                    NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, objNewsClass.objNews)
                    If objNewsClass.listNewsAccess IsNot Nothing AndAlso objNewsClass.listNewsAccess.Count > 0 Then
                        For Each item In objNewsClass.listNewsAccess
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub SaveEditTanpaApproval(objNewsClass As AML_News_Class, objModule As NawaDAL.Module)
        Dim objNewsClass_Old = getNewsClassByID(objNewsClass.objNews.PK_AML_News_ID)
        Dim roles As String = ""
        Dim pkaml As Integer = 0
        Dim jumlahrow As Integer = 0
        Using objDb As New CasemanagementEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try

                    With objNewsClass.objNews
                        If String.IsNullOrEmpty(.LastUpdateBy) Then
                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .LastUpdateDate = Now
                        End If
                        roles = .IsAllRoles
                        pkaml = .PK_AML_News_ID
                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With

                    objDb.Entry(objNewsClass.objNews).State = Entity.EntityState.Modified
                    objDb.SaveChanges()

                    'Ari 21 Sept 2021 menambahakan kondisi jika ada perubahaan list roles yang ditambahakan atau di hapus


                    'Update News Access
                    For Each item In objNewsClass.listNewsAccess
                        If item.PK_AML_News_Access_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next
                    objDb.SaveChanges()
                    For Each itemx As CasemanagementDAL.AML_News_Access In (From x In objDb.AML_News_Access Where x.FK_AML_News_ID = objNewsClass.objNews.PK_AML_News_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.AML_News_Access = objNewsClass.listNewsAccess.Find(Function(x) x.PK_AML_News_Access_ID = itemx.PK_AML_News_Access_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    objDb.SaveChanges()
                    'AuditTrail
                    Dim header As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)
                    NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, objNewsClass.objNews, objNewsClass_Old.objNews)

                    If objNewsClass.listNewsAccess IsNot Nothing AndAlso objNewsClass.listNewsAccess.Count > 0 Then
                        For Each item In objNewsClass.listNewsAccess
                            Dim item_old = objNewsClass_Old.listNewsAccess.Find(Function(x) x.PK_AML_News_Access_ID = item.PK_AML_News_Access_ID)
                            NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, item, item_old)
                        Next
                    End If
                    jumlahrow = objNewsClass.listNewsAccess.Count
                    objDb.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
            If jumlahrow > 0 Then
                If roles = "True" Then
                    SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "delete from aml_news_access where fk_aml_news_id =  " & pkaml, Nothing)

                End If
            End If

        End Using
    End Sub
    Public Shared Sub SaveEditWithApproval(objNewsClass As AML_News_Class, objModule As NawaDAL.Module)
        Dim objNewsClass_Old = getNewsClassByID(objNewsClass.objNews.PK_AML_News_ID)
        Dim roles As String = ""
        Dim pkaml As Integer = 0
        Using objdb As New CasemanagementEntities
            Using objtrans As Entity.DbContextTransaction = objdb.Database.BeginTransaction
                Try
                    Dim alternateby As String = ""
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If


                    Dim xmldata As String = NawaBLL.Common.Serialize(objNewsClass)
                    Dim xmldataOld As String = NawaBLL.Common.Serialize(objNewsClass_Old)

                    Dim objModuleApproval As New CasemanagementDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = objNewsClass.objNews.PK_AML_News_ID
                        .ModuleField = xmldata
                        .ModuleFieldBefore = xmldataOld
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objdb.Entry(objModuleApproval).State = Entity.EntityState.Added

                    objdb.SaveChanges()
                    'Ari 21 Sept 2021 menambahakan kondisi jika ada perubahaan list roles yang ditambahakan atau di hapus
                    With objNewsClass.objNews
                        roles = .IsAllRoles
                        pkaml = .PK_AML_News_ID
                    End With

                    'Update News Access
                    For Each item In objNewsClass.listNewsAccess
                        If item.PK_AML_News_Access_ID > 0 Then
                            objdb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            objdb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next
                    objdb.SaveChanges()
                    For Each itemx As CasemanagementDAL.AML_News_Access In (From x In objdb.AML_News_Access Where x.FK_AML_News_ID = objNewsClass.objNews.PK_AML_News_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.AML_News_Access = objNewsClass.listNewsAccess.Find(Function(x) x.PK_AML_News_Access_ID = itemx.PK_AML_News_Access_ID)
                        If objcek Is Nothing Then
                            objdb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next

                    objdb.SaveChanges()

                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using


        End Using
    End Sub

    Shared Function Accept(ID As String) As Boolean
        Using objdb As New CasemanagementEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As CasemanagementDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = NawaBLL.ModuleBLL.GetModuleByModuleName(objApproval.ModuleName)
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            Dim objData As AML_News_Class = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AML_News_Class))
                            SaveAddTanpaApproval(objData, objModule)
                        Case NawaBLL.Common.ModuleActionEnum.Update
                            ''Update News Access
                            'Dim objModuledata As AML_News_Class = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AML_News_Class))
                            'Dim objModuledataOld As AML_News_Class = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(AML_News_Class))

                            'objModuledata.objNews.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            'objModuledataOld.objNews.ApprovedDate = Now
                            'objdb.AML_News.Attach(objModuledata.objNews)
                            'objdb.Entry(objModuledata.objNews).State = Entity.EntityState.Modified
                            'For Each item In objModuledata.listNewsAccess
                            '    If item.PK_AML_News_Access_ID > 0 Then
                            '        objdb.Entry(item).State = Entity.EntityState.Modified

                            '    Else
                            '        objdb.Entry(item).State = Entity.EntityState.Added

                            '    End If

                            'Next
                            'objdb.SaveChanges()
                            'For Each itemx As CasemanagementDAL.AML_News_Access In (From x In objdb.AML_News_Access Where x.FK_AML_News_ID = 34 Select x).ToList
                            '    Dim objcek As CasemanagementDAL.AML_News_Access = objModuledata.listNewsAccess.Find(Function(x) x.PK_AML_News_Access_ID = itemx.PK_AML_News_Access_ID)
                            '    If objcek Is Nothing Then
                            '        objdb.Entry(itemx).State = Entity.EntityState.Deleted

                            '    End If
                            'Next
                            'objdb.SaveChanges()
                            Dim objData As AML_News_Class = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AML_News_Class))
                            SaveEditTanpaApproval(objData, objModule)
                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objData As AML_News_Class = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AML_News_Class))
                            DeleteTanpaApproval(objData, objModule)
                    End Select

                    'Hapus data di Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted

                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function
    Shared Function Reject(ID As String) As Boolean

        Using objdb As New CasemanagementEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As CasemanagementDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As CasemanagementDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    'Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()

                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Public Shared Sub LoadPanel(objPanel As FormPanel, objdata As String, strModulename As String, unikkey As String)
        Dim objNewsClass As AML_News_Class = NawaBLL.Common.Deserialize(objdata, GetType(AML_News_Class))
        Dim objNews As AML_News = objNewsClass.objNews
        Using db As CasemanagementEntities = New CasemanagementEntities
            If Not objNews Is Nothing Then
                Dim strunik As String = unikkey
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ID", "PK_AML_NEWS_ID" & strunik, objNews.PK_AML_News_ID)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "News Title", "Title" & strunik, objNews.News_Title)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "News Content", "News_Content" & strunik, objNews.News_Content)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "News Content Short", "News_Content_Short" & strunik, objNews.News_Content_Short)
                If objNews.IsPublic = False Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Is Public", "Is_Public" & strunik, "False")
                ElseIf objNews.IsPublic = True Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Is Public", "Is_Public" & strunik, "True")
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Is Public", "Is_Public" & strunik, Nothing)
                End If
                If objNews.IsAllRoles = False Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Is All Roles", "IsAllRoles" & strunik, "False")
                ElseIf objNews.IsAllRoles = True Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Is All Roles", "IsAllRoles" & strunik, "True")
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Is All Roles", "IsAllRoles" & strunik, Nothing)
                End If

                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Valid From", "Valid_From" & strunik, objNews.ValidFrom)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Valid To", "Valid_To" & strunik, objNews.ValidTo)
                If objNews.News_Image IsNot Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "News Image", "News_Image" & strunik, objNews.News_ImageName)
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "News Image", "News_Image" & strunik, Nothing)
                End If
                If objNews.News_Attachment IsNot Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Attachment Name", "Attachment_Name" & strunik, objNews.News_AttachmentName)
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Attachment Name", "Attachment_Name" & strunik, Nothing)
                End If

                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CreatedBy", "CreatedBy" & strunik, objNews.CreatedBy)
            End If
        End Using
    End Sub

    Public Shared Function IsExistsInApproval(strModuleName As String, strID As String) As Boolean
        Try
            Dim isExists As Boolean = False

            Using objdb As New CasemanagementEntities
                Dim objCek = objdb.ModuleApprovals.Where(Function(x) x.ModuleName = strModuleName And x.ModuleKey = strID).FirstOrDefault
                If objCek IsNot Nothing Then
                    isExists = True
                End If
            End Using

            Return isExists

        Catch ex As Exception
            Throw ex
            Return True
        End Try
    End Function


    Public Class AML_News_Class
        Public objNews As New AML_News
        Public listNewsAccess As New List(Of AML_News_Access)
    End Class


End Class
