﻿Imports Ext.Net
Imports CasemanagementDAL
Imports NawaDAL
Public Class STRMemo_BLL

    Shared Function getSTRMemoClassByID(id As Integer) As STRMemoClass
        Dim objSTRMemoClass As New STRMemoClass
        Using objdb As New CasemanagementEntities
            Dim objSTRMemo As OneFCC_CaseManagement_STRMemo = objdb.OneFCC_CaseManagement_STRMemo.Where(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_ID = id).FirstOrDefault
            Dim listCaseID As List(Of OneFCC_CaseManagement_STRMemo_CaseID) = objdb.OneFCC_CaseManagement_STRMemo_CaseID.Where(Function(x) x.FK_OneFCC_CaseManagement_STRMemo_ID = id).ToList
            Dim listIndikator As List(Of OneFCC_CaseManagement_STRMemo_Indikator) = objdb.OneFCC_CaseManagement_STRMemo_Indikator.Where(Function(x) x.FK_OneFCC_CaseManagement_STRMemo_ID = id).ToList
            Dim listPihakTerkair As List(Of OneFCC_CaseManagement_STRMemo_Pihak_Terkait) = objdb.OneFCC_CaseManagement_STRMemo_Pihak_Terkait.Where(Function(x) x.FK_OneFCC_CaseManagement_STRMemo_ID = id).ToList
            Dim listTindakLanjut As List(Of OneFCC_CaseManagement_STRMemo_TindakLanjut) = objdb.OneFCC_CaseManagement_STRMemo_TindakLanjut.Where(Function(x) x.FK_OneFCC_CaseManagement_STRMemo_ID = id).ToList
            Dim listTransaction As List(Of OneFCC_CaseManagement_STRMemo_Transaction) = objdb.OneFCC_CaseManagement_STRMemo_Transaction.Where(Function(x) x.FK_OneFCC_CaseManagement_STRMemo_ID = id).ToList
            Dim listAttachment As List(Of OneFCC_CaseManagement_STRMemo_Attachment) = objdb.OneFCC_CaseManagement_STRMemo_Attachment.Where(Function(x) x.FK_OneFCC_CaseManagement_STRMemo_ID = id).ToList
            If objSTRMemo IsNot Nothing Then
                objSTRMemoClass.objSTRMemo = objSTRMemo
            End If
            If listIndikator IsNot Nothing Then
                objSTRMemoClass.listIndikator = listIndikator
            End If
            If listPihakTerkair IsNot Nothing Then
                objSTRMemoClass.listPihakTerkait = listPihakTerkair
            End If
            If listTindakLanjut IsNot Nothing Then
                objSTRMemoClass.listTindakLanjut = listTindakLanjut
            End If
            If listCaseID IsNot Nothing Then
                objSTRMemoClass.listCaseID = listCaseID
            End If
            If listTransaction IsNot Nothing Then
                objSTRMemoClass.listTransaction = listTransaction
            End If
            If listAttachment IsNot Nothing Then
                objSTRMemoClass.listAttachment = listAttachment
            End If
        End Using
        Return objSTRMemoClass
    End Function

    Public Shared Sub SaveAddTanpaApproval(objSTRMemoClass As STRMemoClass, objModule As NawaDAL.Module)
        Using objDb As New CasemanagementEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try

                    With objSTRMemoClass.objSTRMemo
                        If String.IsNullOrEmpty(.CreatedBy) Then
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = Now
                        End If

                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With

                    objDb.Entry(objSTRMemoClass.objSTRMemo).State = Entity.EntityState.Added
                    objDb.SaveChanges()


                    'Add Indikator
                    For Each item In objSTRMemoClass.listIndikator
                        item.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                        objDb.Entry(item).State = Entity.EntityState.Added
                        objDb.SaveChanges()
                    Next


                    'Add Pihak Terkait
                    For Each item In objSTRMemoClass.listPihakTerkait
                        item.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                        objDb.Entry(item).State = Entity.EntityState.Added
                        objDb.SaveChanges()
                    Next

                    'Add Tindak Lanjut
                    For Each item In objSTRMemoClass.listTindakLanjut
                        item.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                        objDb.Entry(item).State = Entity.EntityState.Added
                        objDb.SaveChanges()
                    Next

                    'Add Case ID
                    For Each item In objSTRMemoClass.listCaseID
                        item.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                        objDb.Entry(item).State = Entity.EntityState.Added
                        objDb.SaveChanges()
                    Next

                    'Add Attachment
                    For Each item In objSTRMemoClass.listAttachment
                        item.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                        objDb.Entry(item).State = Entity.EntityState.Added
                        objDb.SaveChanges()
                    Next

                    'Add Transaction
                    For Each item In objSTRMemoClass.listTransaction
                        item.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                        objDb.Entry(item).State = Entity.EntityState.Added
                        objDb.SaveChanges()
                    Next

                    Dim WorkHistory As New OneFCC_CaseManagement_WorkflowHistory_STRMemo
                    With WorkHistory
                        .FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                        .Workflow_Step = 1
                        .UserID = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedDate = Now
                    End With

                    objDb.Entry(WorkHistory).State = Entity.EntityState.Added
                    objDb.SaveChanges()

                    'AuditTrail
                    Dim header As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                    'AuditTrailDetail
                    NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, objSTRMemoClass.objSTRMemo)

                    If objSTRMemoClass.listIndikator IsNot Nothing AndAlso objSTRMemoClass.listIndikator.Count > 0 Then
                        For Each item In objSTRMemoClass.listIndikator
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listPihakTerkait IsNot Nothing AndAlso objSTRMemoClass.listPihakTerkait.Count > 0 Then
                        For Each item In objSTRMemoClass.listPihakTerkait
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listTindakLanjut IsNot Nothing AndAlso objSTRMemoClass.listTindakLanjut.Count > 0 Then
                        For Each item In objSTRMemoClass.listTindakLanjut
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listCaseID IsNot Nothing AndAlso objSTRMemoClass.listCaseID.Count > 0 Then
                        For Each item In objSTRMemoClass.listCaseID
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listAttachment IsNot Nothing AndAlso objSTRMemoClass.listAttachment.Count > 0 Then
                        For Each item In objSTRMemoClass.listAttachment
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listTransaction IsNot Nothing AndAlso objSTRMemoClass.listTransaction.Count > 0 Then
                        For Each item In objSTRMemoClass.listTransaction
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If
                    'Audit Trail Header
                    ''Define local variables
                    'Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    'Dim strAlternateID As String = Nothing
                    'Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    'Dim strModuleName As String = objModule.ModuleLabel
                    'Dim strQuery As String
                    'Dim intModuleAction As Integer = 1

                    'objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("OneFCC_CaseManagement_STRMemo")

                    'strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'STR Memo Add', 'Save Audit Trail started')"
                    'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    'Dim objAuditTrailheader As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)

                    ''Save Audit Trail Detail by XML
                    'Dim lngPKAuditTrail As Long = header.PK_AuditTrail_ID

                    'Dim ListDataSTRMemo As New List(Of OneFCC_CaseManagement_STRMemo)
                    'Dim ListDataSTRMemoOld As New List(Of OneFCC_CaseManagement_STRMemo)
                    'Dim ListDataIndikatorOld As New List(Of OneFCC_CaseManagement_STRMemo_Indikator)
                    'Dim ListDataPihakTerkaitOld As New List(Of OneFCC_CaseManagement_STRMemo_Pihak_Terkait)
                    'Dim ListDataTindakLanjutOld As New List(Of OneFCC_CaseManagement_STRMemo_TindakLanjut)
                    'Dim ListDataTransactionOld As New List(Of OneFCC_CaseManagement_STRMemo_Transaction)
                    'Dim ListDataAttachmentOld As New List(Of OneFCC_CaseManagement_STRMemo_Attachment)
                    'Dim ListDataCaseIDOld As New List(Of OneFCC_CaseManagement_STRMemo_CaseID)


                    'ListDataSTRMemo.Add(objSTRMemoClass.objSTRMemo)
                    'NawaFramework.CreateAuditTrailDetailXML(header.PK_AuditTrail_ID, objModule.ModuleName, ListDataSTRMemo, ListDataSTRMemoOld)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, objSTRMemoClass.listIndikator, ListDataIndikatorOld)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, objSTRMemoClass.listPihakTerkait, ListDataPihakTerkaitOld)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, objSTRMemoClass.listTindakLanjut, ListDataTindakLanjutOld)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, objSTRMemoClass.listTransaction, ListDataTransactionOld)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, objSTRMemoClass.listAttachment, ListDataAttachmentOld)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, objSTRMemoClass.listCaseID, ListDataCaseIDOld)

                    'strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'STR Memo Add', 'Save Audit Trail finished')"
                    'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                    ''End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML

                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()

                    Throw ex
                End Try
            End Using
        End Using
    End Sub


    Public Shared Sub SaveEditTanpaApproval(objSTRMemoClass As STRMemoClass, objModule As NawaDAL.Module)
        Dim objSTRMemoClass_Old = getSTRMemoClassByID(objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID)
        Dim jumlahrow As Integer = 0
        Using objDb As New CasemanagementEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try

                    With objSTRMemoClass.objSTRMemo
                        If String.IsNullOrEmpty(.LastUpdateBy) Then
                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .LastUpdateDate = Now
                        End If
                        .StatusID = 1
                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With

                    objDb.Entry(objSTRMemoClass.objSTRMemo).State = Entity.EntityState.Modified
                    objDb.SaveChanges()

                    'Ari 21 Sept 2021 menambahakan kondisi jika ada perubahaan list roles yang ditambahakan atau di hapus


                    'Update Indikator
                    For Each item In objSTRMemoClass.listIndikator
                        If item.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            item.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next



                    'Update Pihak Terkait
                    For Each item In objSTRMemoClass.listPihakTerkait
                        If item.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            item.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next

                    'Update Tindak Lanjut
                    For Each item In objSTRMemoClass.listTindakLanjut
                        If item.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            item.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next

                    'Update Transaction
                    For Each item In objSTRMemoClass.listTransaction
                        If item.Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            item.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next

                    ' Update Attachment
                    If objSTRMemoClass.listAttachment IsNot Nothing Then
                        For Each item In objSTRMemoClass.listAttachment
                            If item.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID > 0 Then
                                objDb.Entry(item).State = Entity.EntityState.Modified

                            Else
                                item.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                                objDb.Entry(item).State = Entity.EntityState.Added

                            End If

                        Next
                    End If

                    objDb.SaveChanges()
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Indikator In (From x In objDb.OneFCC_CaseManagement_STRMemo_Indikator Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Indikator = objSTRMemoClass.listIndikator.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID = itemx.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait In (From x In objDb.OneFCC_CaseManagement_STRMemo_Pihak_Terkait Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait = objSTRMemoClass.listPihakTerkait.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID = itemx.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut In (From x In objDb.OneFCC_CaseManagement_STRMemo_TindakLanjut Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut = objSTRMemoClass.listTindakLanjut.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID = itemx.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Transaction In (From x In objDb.OneFCC_CaseManagement_STRMemo_Transaction Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Transaction = objSTRMemoClass.listTransaction.Find(Function(x) x.Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID = itemx.Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment In (From x In objDb.OneFCC_CaseManagement_STRMemo_Attachment Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment = objSTRMemoClass.listAttachment.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = itemx.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    objDb.SaveChanges()
                    ''AuditTrail
                    'Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    'Dim strAlternateID As String = Nothing
                    'Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    'Dim strModuleName As String = objModule.ModuleLabel
                    'Dim strQuery As String
                    'Dim intModuleAction As Integer = 2

                    'objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("OneFCC_CaseManagement_STRMemo")

                    'strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'STR Memo Edit', 'Save Audit Trail started')"
                    'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    'Dim objAuditTrailheader As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    ''AuditTrail
                    'Dim header As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)


                    ''Save Audit Trail Detail by XML
                    'Dim lngPKAuditTrail As Long = header.PK_AuditTrail_ID

                    'Dim ListDataSTRMemo As New List(Of OneFCC_CaseManagement_STRMemo)
                    'Dim ListDataSTRMemoOld As New List(Of OneFCC_CaseManagement_STRMemo)

                    'ListDataSTRMemo.Add(objSTRMemoClass.objSTRMemo)
                    'ListDataSTRMemoOld.Add(objSTRMemoClass_Old.objSTRMemo)
                    'NawaFramework.CreateAuditTrailDetailXML(header.PK_AuditTrail_ID, objModule.ModuleName, ListDataSTRMemo, ListDataSTRMemoOld)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, objSTRMemoClass.listIndikator, objSTRMemoClass_Old.listIndikator)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, objSTRMemoClass.listPihakTerkait, objSTRMemoClass_Old.listPihakTerkait)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, objSTRMemoClass.listTindakLanjut, objSTRMemoClass_Old.listTindakLanjut)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, objSTRMemoClass.listTransaction, objSTRMemoClass_Old.listTransaction)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, objSTRMemoClass.listAttachment, objSTRMemoClass_Old.listAttachment)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, objSTRMemoClass.listCaseID, objSTRMemoClass_Old.listCaseID)

                    'strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'STR Memo Edit', 'Save Audit Trail finished')"
                    'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                    ''End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML
                    'AuditTrail
                    Dim header As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                    'AuditTrailDetail
                    NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, objSTRMemoClass.objSTRMemo)

                    If objSTRMemoClass.listIndikator IsNot Nothing AndAlso objSTRMemoClass.listIndikator.Count > 0 Then
                        For Each item In objSTRMemoClass.listIndikator
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listPihakTerkait IsNot Nothing AndAlso objSTRMemoClass.listPihakTerkait.Count > 0 Then
                        For Each item In objSTRMemoClass.listPihakTerkait
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listTindakLanjut IsNot Nothing AndAlso objSTRMemoClass.listTindakLanjut.Count > 0 Then
                        For Each item In objSTRMemoClass.listTindakLanjut
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listCaseID IsNot Nothing AndAlso objSTRMemoClass.listCaseID.Count > 0 Then
                        For Each item In objSTRMemoClass.listCaseID
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listAttachment IsNot Nothing AndAlso objSTRMemoClass.listAttachment.Count > 0 Then
                        For Each item In objSTRMemoClass.listAttachment
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listTransaction IsNot Nothing AndAlso objSTRMemoClass.listTransaction.Count > 0 Then
                        For Each item In objSTRMemoClass.listTransaction
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If
                    objDb.SaveChanges()
                    Dim WorkHistory As New OneFCC_CaseManagement_WorkflowHistory_STRMemo
                    With WorkHistory
                        .FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                        .Workflow_Step = 1
                        .UserID = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .CreatedDate = Now
                    End With

                    objDb.Entry(WorkHistory).State = Entity.EntityState.Added
                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using


        End Using
    End Sub

    Public Shared Sub Approve(objSTRMemoClass As STRMemoClass, objModule As NawaDAL.Module)
        Dim objSTRMemoClass_Old = getSTRMemoClassByID(objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID)
        Dim jumlahrow As Integer = 0
        Using objDb As New CasemanagementEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try

                    With objSTRMemoClass.objSTRMemo
                        If String.IsNullOrEmpty(.LastUpdateBy) Then
                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .LastUpdateDate = Now
                        End If

                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With

                    objDb.Entry(objSTRMemoClass.objSTRMemo).State = Entity.EntityState.Modified
                    objDb.SaveChanges()

                    'Ari 21 Sept 2021 menambahakan kondisi jika ada perubahaan list roles yang ditambahakan atau di hapus


                    'Update Indikator
                    For Each item In objSTRMemoClass.listIndikator
                        If item.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next



                    'Update Pihak Terkait
                    For Each item In objSTRMemoClass.listPihakTerkait
                        If item.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next

                    'Update Tindak Lanjut
                    For Each item In objSTRMemoClass.listTindakLanjut
                        If item.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next

                    'Update Transaction
                    For Each item In objSTRMemoClass.listTransaction
                        If item.Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next

                    ' Update Attachment
                    If objSTRMemoClass.listAttachment IsNot Nothing Then
                        For Each item In objSTRMemoClass.listAttachment
                            If item.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID > 0 Then
                                objDb.Entry(item).State = Entity.EntityState.Modified

                            Else
                                objDb.Entry(item).State = Entity.EntityState.Added

                            End If

                        Next
                    End If

                    objDb.SaveChanges()
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Indikator In (From x In objDb.OneFCC_CaseManagement_STRMemo_Indikator Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Indikator = objSTRMemoClass.listIndikator.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID = itemx.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait In (From x In objDb.OneFCC_CaseManagement_STRMemo_Pihak_Terkait Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait = objSTRMemoClass.listPihakTerkait.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID = itemx.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut In (From x In objDb.OneFCC_CaseManagement_STRMemo_TindakLanjut Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut = objSTRMemoClass.listTindakLanjut.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID = itemx.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Transaction In (From x In objDb.OneFCC_CaseManagement_STRMemo_Transaction Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Transaction = objSTRMemoClass.listTransaction.Find(Function(x) x.Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID = itemx.Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment In (From x In objDb.OneFCC_CaseManagement_STRMemo_Attachment Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment = objSTRMemoClass.listAttachment.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = itemx.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    objDb.SaveChanges()
                    'AuditTrail
                    Dim header As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)
                    NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, objSTRMemoClass.objSTRMemo, objSTRMemoClass_Old.objSTRMemo)

                    If objSTRMemoClass.listIndikator IsNot Nothing AndAlso objSTRMemoClass.listIndikator.Count > 0 Then
                        For Each item In objSTRMemoClass.listIndikator
                            Dim item_old = objSTRMemoClass_Old.listIndikator.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID = item.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID)
                            NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, item, item_old)
                        Next
                    End If
                    If objSTRMemoClass.listPihakTerkait IsNot Nothing AndAlso objSTRMemoClass.listPihakTerkait.Count > 0 Then
                        For Each item In objSTRMemoClass.listPihakTerkait
                            Dim item_old = objSTRMemoClass_Old.listPihakTerkait.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID = item.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID)
                            NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, item, item_old)
                        Next
                    End If
                    If objSTRMemoClass.listTindakLanjut IsNot Nothing AndAlso objSTRMemoClass.listTindakLanjut.Count > 0 Then
                        For Each item In objSTRMemoClass.listTindakLanjut
                            Dim item_old = objSTRMemoClass_Old.listTindakLanjut.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID = item.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID)
                            NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, item, item_old)
                        Next
                    End If
                    If objSTRMemoClass.listTransaction IsNot Nothing AndAlso objSTRMemoClass.listTransaction.Count > 0 Then
                        For Each item In objSTRMemoClass.listTransaction
                            Dim item_old = objSTRMemoClass_Old.listTransaction.Find(Function(x) x.Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID = item.Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID)
                            NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, item, item_old)
                        Next
                    End If
                    If objSTRMemoClass.listAttachment IsNot Nothing AndAlso objSTRMemoClass.listAttachment.Count > 0 Then
                        For Each item In objSTRMemoClass.listAttachment
                            Dim item_old = objSTRMemoClass_Old.listAttachment.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = item.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID)
                            NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, item, item_old)
                        Next
                    End If

                    objDb.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using


        End Using
    End Sub

    Public Shared Sub Reject(objSTRMemoClass As STRMemoClass, objModule As NawaDAL.Module)
        Dim objSTRMemoClass_Old = getSTRMemoClassByID(objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID)
        Dim jumlahrow As Integer = 0
        Using objDb As New CasemanagementEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try

                    With objSTRMemoClass.objSTRMemo
                        If String.IsNullOrEmpty(.LastUpdateBy) Then
                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .LastUpdateDate = Now
                        End If

                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With

                    objDb.Entry(objSTRMemoClass.objSTRMemo).State = Entity.EntityState.Modified
                    objDb.SaveChanges()

                    'Ari 21 Sept 2021 menambahakan kondisi jika ada perubahaan list roles yang ditambahakan atau di hapus


                    'Update Indikator
                    For Each item In objSTRMemoClass.listIndikator
                        If item.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next



                    'Update Pihak Terkait
                    For Each item In objSTRMemoClass.listPihakTerkait
                        If item.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next

                    'Update Tindak Lanjut
                    For Each item In objSTRMemoClass.listTindakLanjut
                        If item.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next

                    'Update Transaction
                    For Each item In objSTRMemoClass.listTransaction
                        If item.Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next

                    ' Update Attachment
                    For Each item In objSTRMemoClass.listAttachment
                        If item.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID > 0 Then
                            objDb.Entry(item).State = Entity.EntityState.Modified

                        Else
                            objDb.Entry(item).State = Entity.EntityState.Added

                        End If

                    Next
                    objDb.SaveChanges()
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Indikator In (From x In objDb.OneFCC_CaseManagement_STRMemo_Indikator Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Indikator = objSTRMemoClass.listIndikator.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID = itemx.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait In (From x In objDb.OneFCC_CaseManagement_STRMemo_Pihak_Terkait Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Pihak_Terkait = objSTRMemoClass.listPihakTerkait.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID = itemx.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut In (From x In objDb.OneFCC_CaseManagement_STRMemo_TindakLanjut Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_TindakLanjut = objSTRMemoClass.listTindakLanjut.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID = itemx.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Transaction In (From x In objDb.OneFCC_CaseManagement_STRMemo_Transaction Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Transaction = objSTRMemoClass.listTransaction.Find(Function(x) x.Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID = itemx.Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    For Each itemx As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment In (From x In objDb.OneFCC_CaseManagement_STRMemo_Attachment Where x.FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID Select x).ToList
                        Dim objcek As CasemanagementDAL.OneFCC_CaseManagement_STRMemo_Attachment = objSTRMemoClass.listAttachment.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = itemx.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID)
                        If objcek Is Nothing Then
                            objDb.Entry(itemx).State = Entity.EntityState.Deleted

                        End If
                    Next
                    objDb.SaveChanges()
                    'AuditTrail
                    Dim header As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)
                    NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, objSTRMemoClass.objSTRMemo, objSTRMemoClass_Old.objSTRMemo)

                    If objSTRMemoClass.listIndikator IsNot Nothing AndAlso objSTRMemoClass.listIndikator.Count > 0 Then
                        For Each item In objSTRMemoClass.listIndikator
                            Dim item_old = objSTRMemoClass_Old.listIndikator.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID = item.PK_OneFCC_CaseManagement_STRMemo_Indikator_ID)
                            NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, item, item_old)
                        Next
                    End If
                    If objSTRMemoClass.listPihakTerkait IsNot Nothing AndAlso objSTRMemoClass.listPihakTerkait.Count > 0 Then
                        For Each item In objSTRMemoClass.listPihakTerkait
                            Dim item_old = objSTRMemoClass_Old.listPihakTerkait.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID = item.PK_OneFCC_CaseManagement_STRMemo_Pihak_Terkait_ID)
                            NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, item, item_old)
                        Next
                    End If
                    If objSTRMemoClass.listTindakLanjut IsNot Nothing AndAlso objSTRMemoClass.listTindakLanjut.Count > 0 Then
                        For Each item In objSTRMemoClass.listTindakLanjut
                            Dim item_old = objSTRMemoClass_Old.listTindakLanjut.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID = item.PK_OneFCC_CaseManagement_STRMemo_TindakLanjut_ID)
                            NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, item, item_old)
                        Next
                    End If
                    If objSTRMemoClass.listTransaction IsNot Nothing AndAlso objSTRMemoClass.listTransaction.Count > 0 Then
                        For Each item In objSTRMemoClass.listTransaction
                            Dim item_old = objSTRMemoClass_Old.listTransaction.Find(Function(x) x.Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID = item.Pk_OneFCC_CaseManagement_STRMemo_Transaction_ID)
                            NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, item, item_old)
                        Next
                    End If
                    If objSTRMemoClass.listAttachment IsNot Nothing AndAlso objSTRMemoClass.listAttachment.Count > 0 Then
                        For Each item In objSTRMemoClass.listAttachment
                            Dim item_old = objSTRMemoClass_Old.listAttachment.Find(Function(x) x.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID = item.PK_OneFCC_CaseManagement_STRMemo_Attachment_ID)
                            NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, item, item_old)
                        Next
                    End If

                    objDb.SaveChanges()
                    'Dim WorkHistory As New OneFCC_CaseManagement_WorkflowHistory_STRMemo
                    'With WorkHistory
                    '    .FK_OneFCC_CaseManagement_STRMemo_ID = objSTRMemoClass.objSTRMemo.PK_OneFCC_CaseManagement_STRMemo_ID
                    '    .Workflow_Step = 1
                    '    .UserID = NawaBLL.Common.SessionCurrentUser.UserID
                    '    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    '    .CreatedDate = Now
                    'End With

                    'objDb.Entry(WorkHistory).State = Entity.EntityState.Added
                    'objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using


        End Using
    End Sub


    Public Shared Sub DeleteTanpaApproval(objSTRMemoClass As STRMemoClass, objModule As NawaDAL.Module)
        Using objDb As New CasemanagementEntities
            Dim roles As String = ""
            Dim pkaml As Integer = 0
            Dim jumlahrow As Integer = 0
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try


                    'Delete News
                    objDb.Entry(objSTRMemoClass.objSTRMemo).State = Entity.EntityState.Deleted
                    objDb.SaveChanges()

                    'Delete Indicator
                    For Each item In objSTRMemoClass.listIndikator
                        objDb.Entry(item).State = Entity.EntityState.Deleted
                        objDb.SaveChanges()
                    Next
                    'Delete Pihak Terkait
                    For Each item In objSTRMemoClass.listPihakTerkait
                        objDb.Entry(item).State = Entity.EntityState.Deleted
                        objDb.SaveChanges()
                    Next
                    'Delete Tindak Lanjut
                    For Each item In objSTRMemoClass.listTindakLanjut
                        objDb.Entry(item).State = Entity.EntityState.Deleted
                        objDb.SaveChanges()
                    Next
                    'Delete Attachment
                    For Each item In objSTRMemoClass.listAttachment
                        objDb.Entry(item).State = Entity.EntityState.Deleted
                        objDb.SaveChanges()
                    Next
                    'Delete Transaction
                    For Each item In objSTRMemoClass.listTransaction
                        objDb.Entry(item).State = Entity.EntityState.Deleted
                        objDb.SaveChanges()
                    Next
                    'Delete Alert
                    For Each item In objSTRMemoClass.listCaseID
                        objDb.Entry(item).State = Entity.EntityState.Deleted
                        objDb.SaveChanges()
                    Next

                    ''AuditTrail
                    ' 'Define local variables
                    'Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    'Dim strAlternateID As String = Nothing
                    'Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    'Dim strModuleName As String = objModule.ModuleLabel
                    'Dim strQuery As String
                    'Dim intModuleAction As Integer = 3

                    'objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("OneFCC_CaseManagement_STRMemo")

                    'strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'STR Memo Delete', 'Save Audit Trail started')"
                    'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    'Dim objAuditTrailheader As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)

                    ''AuditTrail
                    'Dim header As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)


                    ''Save Audit Trail Detail by XML
                    'Dim lngPKAuditTrail As Long = header.PK_AuditTrail_ID

                    'Dim ListDataSTRMemo As New List(Of OneFCC_CaseManagement_STRMemo)
                    'Dim ListDataSTRMemoOld As New List(Of OneFCC_CaseManagement_STRMemo)
                    'Dim ListDataIndikatorOld As New List(Of OneFCC_CaseManagement_STRMemo_Indikator)
                    'Dim ListDataPihakTerkaitOld As New List(Of OneFCC_CaseManagement_STRMemo_Pihak_Terkait)
                    'Dim ListDataTindakLanjutOld As New List(Of OneFCC_CaseManagement_STRMemo_TindakLanjut)
                    'Dim ListDataTransactionOld As New List(Of OneFCC_CaseManagement_STRMemo_Transaction)
                    'Dim ListDataAttachmentOld As New List(Of OneFCC_CaseManagement_STRMemo_Attachment)
                    'Dim ListDataCaseIDOld As New List(Of OneFCC_CaseManagement_STRMemo_CaseID)


                    'ListDataSTRMemo.Add(objSTRMemoClass.objSTRMemo)
                    'NawaFramework.CreateAuditTrailDetailXML(header.PK_AuditTrail_ID, objModule.ModuleName, ListDataSTRMemoOld, ListDataSTRMemo)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, ListDataIndikatorOld, objSTRMemoClass.listIndikator)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, ListDataPihakTerkaitOld, objSTRMemoClass.listPihakTerkait)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, ListDataTindakLanjutOld, objSTRMemoClass.listTindakLanjut)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, ListDataTransactionOld, objSTRMemoClass.listTransaction)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, ListDataAttachmentOld, objSTRMemoClass.listAttachment)
                    'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objModule.ModuleName, ListDataCaseIDOld, objSTRMemoClass.listCaseID)

                    'strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'STR Memo Delete', 'Save Audit Trail finished')"
                    'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                    ''End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML

                    'AuditTrail
                    Dim header As CasemanagementDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                    'AuditTrailDetail
                    NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, objSTRMemoClass.objSTRMemo)

                    If objSTRMemoClass.listIndikator IsNot Nothing AndAlso objSTRMemoClass.listIndikator.Count > 0 Then
                        For Each item In objSTRMemoClass.listIndikator
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listPihakTerkait IsNot Nothing AndAlso objSTRMemoClass.listPihakTerkait.Count > 0 Then
                        For Each item In objSTRMemoClass.listPihakTerkait
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listTindakLanjut IsNot Nothing AndAlso objSTRMemoClass.listTindakLanjut.Count > 0 Then
                        For Each item In objSTRMemoClass.listTindakLanjut
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listCaseID IsNot Nothing AndAlso objSTRMemoClass.listCaseID.Count > 0 Then
                        For Each item In objSTRMemoClass.listCaseID
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listAttachment IsNot Nothing AndAlso objSTRMemoClass.listAttachment.Count > 0 Then
                        For Each item In objSTRMemoClass.listAttachment
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If objSTRMemoClass.listTransaction IsNot Nothing AndAlso objSTRMemoClass.listTransaction.Count > 0 Then
                        For Each item In objSTRMemoClass.listTransaction
                            NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, item)
                        Next
                    End If
                    objDb.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using


        End Using
    End Sub

    Public Class STRMemoClass
        Public objSTRMemo As New OneFCC_CaseManagement_STRMemo
        Public listCaseID As New List(Of OneFCC_CaseManagement_STRMemo_CaseID)
        Public listIndikator As New List(Of OneFCC_CaseManagement_STRMemo_Indikator)
        Public listPihakTerkait As New List(Of OneFCC_CaseManagement_STRMemo_Pihak_Terkait)
        Public listTindakLanjut As New List(Of OneFCC_CaseManagement_STRMemo_TindakLanjut)
        Public listTransaction As New List(Of OneFCC_CaseManagement_STRMemo_Transaction)
        Public listAttachment As New List(Of OneFCC_CaseManagement_STRMemo_Attachment)
    End Class
End Class
