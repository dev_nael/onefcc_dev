﻿'--------------------------------------------
'Classname      : OneFCC EDD
'Description    : Maintain OneFCC EDD Data
'Created By     : Adi Darmawan
'Created Date   : 25-Aug-2021
'--------------------------------------------

Imports System.ComponentModel
Imports System.Data.SqlClient
Imports CasemanagementDAL
Imports Ionic.Zip
Imports Ionic.Zlib
Imports System.Xml

Public Class OneFCC_EDD_BLL

    Shared Function GetEDDClassByID(ID As Long) As OneFCC_EDD_CLASS
        Using objDb As New CasemanagementEntities

            Dim objEDD = objDb.OneFCC_EDD.Where(Function(x) x.PK_OneFCC_EDD_ID = ID).FirstOrDefault
            Dim listEDD_Detail = objDb.OneFCC_EDD_Detail.Where(Function(x) x.FK_OneFCC_EDD_ID = ID).ToList

            Dim objProfileClass = New OneFCC_EDD_CLASS
            With objProfileClass
                .objEDD = objEDD
                .objListEDD_Detail = listEDD_Detail
            End With

            Return objProfileClass
        End Using
    End Function

    Shared Function GetEDDTypeByID(ID As String) As OneFCC_EDD_Type
        Using objDb As CasemanagementEntities = New CasemanagementEntities
            Return objDb.OneFCC_EDD_Type.Where(Function(x) x.PK_OneFCC_EDD_Type_ID = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetEDDTypeByCode(Code As String) As OneFCC_EDD_Type
        Using objDb As CasemanagementEntities = New CasemanagementEntities
            Return objDb.OneFCC_EDD_Type.Where(Function(x) x.EDD_Type_Code = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetEDDCustomerSegmentByCode(Code As String) As OneFCC_Question_Customer_Segment
        Using objDb As CasemanagementEntities = New CasemanagementEntities
            Return objDb.OneFCC_Question_Customer_Segment.Where(Function(x) x.Customer_Segment_Code = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetEDDCustomerTypeByCode(Code As String) As OneFCC_Question_Customer_Type
        Using objDb As CasemanagementEntities = New CasemanagementEntities
            Return objDb.OneFCC_Question_Customer_Type.Where(Function(x) x.Customer_Type_Code = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetAMLCustomer(CIF As String) As AML_CUSTOMER
        Using objDb As CasemanagementEntities = New CasemanagementEntities
            Return objDb.AML_CUSTOMER.Where(Function(x) x.CIFNo = CIF).FirstOrDefault
        End Using
    End Function

    Shared Function GetModuleApprovalByPKID(ID As Long) As CasemanagementDAL.ModuleApproval
        Using objDb As CasemanagementEntities = New CasemanagementEntities
            Return objDb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault
        End Using
    End Function

    Shared Sub SaveEditWithApproval(objModule As NawaDAL.Module, objData As OneFCC_EDD_CLASS)
        'Get Old Data for Audit Trail
        Dim objData_Old = GetEDDClassByID(objData.objEDD.PK_OneFCC_EDD_ID)

        Using objDB As New CasemanagementEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objEDD
                        .LastUpdateBy = strUserID
                        .LastUpdateDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objXMLData_Old As String = NawaBLL.Common.Serialize(objData_Old)

                    Dim objModuleApproval As New ModuleApproval
                    With objModuleApproval
                        .FK_MRole_ID = NawaBLL.Common.SessionCurrentUser.FK_MRole_ID
                        .ModuleKey = objData.objEDD.PK_OneFCC_EDD_ID
                        .ModuleName = objModule.ModuleName
                        .ModuleField = objXMLData
                        .ModuleFieldBefore = objXMLData_Old
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub SaveEditWithoutApproval(objModule As NawaDAL.Module, objData As OneFCC_EDD_CLASS)
        'Get Old Data for Audit Trail
        Dim objData_Old = GetEDDClassByID(objData.objEDD.PK_OneFCC_EDD_ID)

        Dim strQuery As String = " SELECT ParameterValue FROM AML_Global_Parameter WHERE PK_GlobalReportParameter_ID = 6 "
        Dim IsUsingScorePointAffectingAMLCustomer As String = Nothing
        Dim dataRowGetParameterAffectingCustomer As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQuery)
        If dataRowGetParameterAffectingCustomer IsNot Nothing AndAlso Not IsDBNull(dataRowGetParameterAffectingCustomer("ParameterValue")) Then
            IsUsingScorePointAffectingAMLCustomer = dataRowGetParameterAffectingCustomer("ParameterValue")
        End If

        Using objDB As New CasemanagementEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objEDD
                        .ApprovedBy = strUserID
                        .ApprovedDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData
                    objDB.Entry(objData.objEDD).State = Entity.EntityState.Modified
                    objDB.SaveChanges()

                    'Audit Trail Header
                    Dim objAuditTrailheader As AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.objEDD, objData_Old.objEDD)


                    '===================== NEW ADDED DATA (PK untuk masing2 data < 0)
                    'Added Detail
                    For Each item In objData.objListEDD_Detail.Where(Function(x) x.PK_OneFCC_EDD_Detail_ID < 0).ToList
                        With item
                            .FK_OneFCC_EDD_ID = objData.objEDD.PK_OneFCC_EDD_ID
                            .ApprovedBy = strUserID
                            .ApprovedDate = Now
                            .Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next
                    '===================== END OF NEW ADDED DATA


                    '===================== UPDATED DATA (PK untuk masing2 data > 0)
                    'Updated Detail
                    For Each item In objData.objListEDD_Detail.Where(Function(x) x.PK_OneFCC_EDD_Detail_ID > 0).ToList
                        Dim item_old = objData_Old.objListEDD_Detail.Find(Function(x) x.PK_OneFCC_EDD_Detail_ID = item.PK_OneFCC_EDD_Detail_ID)

                        With item
                            .ApprovedBy = strUserID
                            .ApprovedDate = Now
                            .Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    Next
                    '===================== END OF UPDATED DATA


                    '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                    'Deleted Detail
                    For Each item_old In objData_Old.objListEDD_Detail
                        Dim objCek = objData.objListEDD_Detail.Find(Function(x) x.PK_OneFCC_EDD_Detail_ID = item_old.PK_OneFCC_EDD_Detail_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next
                    '===================== END OF DELETED DATA

                    If objData.objEDD.CIFNo IsNot Nothing AndAlso objData.objEDD.FK_RISK_RATING_CODE IsNot Nothing AndAlso objData.objEDD.FK_OneFCC_EDD_Type_Code = "C" AndAlso IsUsingScorePointAffectingAMLCustomer = "1" Then
                        Dim amlcust As AML_CUSTOMER = GetAMLCustomer(objData.objEDD.CIFNo)
                        Dim amlcustOld As AML_CUSTOMER = GetAMLCustomer(objData.objEDD.CIFNo)
                        If amlcust IsNot Nothing Then
                            amlcust.FK_AML_RISK_CODE = objData.objEDD.FK_RISK_RATING_CODE
                            amlcust.DATEOFLASTDUEDILIGENCE = objData.objEDD.LastUpdateDate
                            amlcust.LastUpdateBy = objData.objEDD.LastUpdateBy
                            amlcust.LastUpdateDate = objData.objEDD.LastUpdateDate
                            amlcust.ApprovedBy = strUserID
                            amlcust.ApprovedDate = Now
                            amlcust.Alternateby = strAlternateID

                            objDB.Entry(amlcust).State = Entity.EntityState.Modified
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, amlcust, amlcustOld)
                        End If
                    End If

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using

    End Sub

    Shared Sub SaveAddWithApproval(objModule As NawaDAL.Module, objData As OneFCC_EDD_CLASS, unikID As String)

        Using objDB As New CasemanagementEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objEDD
                        .Alternateby = strAlternateID
                    End With
                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objModuleApproval As New ModuleApproval
                    With objModuleApproval
                        .FK_MRole_ID = NawaBLL.Common.SessionCurrentUser.FK_MRole_ID
                        .ModuleKey = unikID
                        .ModuleName = objModule.ModuleName
                        .ModuleField = objXMLData
                        .ModuleFieldBefore = Nothing
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With
                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Function SaveAddWithoutApproval(objModule As NawaDAL.Module, objData As OneFCC_EDD_CLASS)
        Dim IDUnik As Long

        Dim strQuery As String = " SELECT ParameterValue FROM AML_Global_Parameter WHERE PK_GlobalReportParameter_ID = 6 "
        Dim IsUsingScorePointAffectingAMLCustomer As String = Nothing
        Dim dataRowGetParameterAffectingCustomer As DataRow = CasemanagementBLL.OneFCC_EDD_BLL.getDataRowFromWithStringQuery(strQuery)
        If dataRowGetParameterAffectingCustomer IsNot Nothing AndAlso Not IsDBNull(dataRowGetParameterAffectingCustomer("ParameterValue")) Then
            IsUsingScorePointAffectingAMLCustomer = dataRowGetParameterAffectingCustomer("ParameterValue")
        End If

        Using objDB As New CasemanagementEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objEDD
                        .Active = True
                        .ApprovedBy = strUserID
                        .ApprovedDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData
                    objDB.Entry(objData.objEDD).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    '25-May-2021
                    IDUnik = objData.objEDD.PK_OneFCC_EDD_ID
                    '-----------

                    'Audit Trail Header
                    Dim objAuditTrailheader As AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.objEDD)

                    'Added Detail
                    For Each item In objData.objListEDD_Detail
                        With item
                            .FK_OneFCC_EDD_ID = objData.objEDD.PK_OneFCC_EDD_ID
                            .ApprovedBy = strUserID
                            .ApprovedDate = Now
                            .Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next


                    If objData.objEDD.CIFNo IsNot Nothing AndAlso objData.objEDD.FK_RISK_RATING_CODE IsNot Nothing AndAlso objData.objEDD.FK_OneFCC_EDD_Type_Code = "C" AndAlso IsUsingScorePointAffectingAMLCustomer = "1" Then
                        Dim amlcust As AML_CUSTOMER = GetAMLCustomer(objData.objEDD.CIFNo)
                        Dim amlcustOld As AML_CUSTOMER = GetAMLCustomer(objData.objEDD.CIFNo)
                        If amlcust IsNot Nothing Then
                            amlcust.FK_AML_RISK_CODE = objData.objEDD.FK_RISK_RATING_CODE
                            amlcust.DATEOFLASTDUEDILIGENCE = objData.objEDD.LastUpdateDate
                            amlcust.LastUpdateBy = objData.objEDD.LastUpdateBy
                            amlcust.LastUpdateDate = objData.objEDD.LastUpdateDate
                            amlcust.ApprovedBy = strUserID
                            amlcust.ApprovedDate = Now
                            amlcust.Alternateby = strAlternateID

                            objDB.Entry(amlcust).State = Entity.EntityState.Modified
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, amlcust, amlcustOld)
                        End If
                    End If

                    objTrans.Commit()

                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex

                    Return Nothing
                End Try
            End Using
        End Using

        Return IDUnik
    End Function

    Shared Sub SaveDeleteWithApproval(objModule As NawaDAL.Module, objData As OneFCC_EDD_CLASS)

        Using objDB As New CasemanagementEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objModuleApproval As New NawaDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleKey = objData.objEDD.PK_OneFCC_EDD_ID
                        .ModuleName = objModule.ModuleName
                        .ModuleField = Nothing
                        .ModuleFieldBefore = objXMLData
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub SaveDeleteWithoutApproval(objModule As NawaDAL.Module, objData As OneFCC_EDD_CLASS)
        Using objDB As New CasemanagementEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Delete objData
                    objDB.Entry(objData.objEDD).State = Entity.EntityState.Deleted
                    objDB.SaveChanges()

                    'Audit Trail Header
                    Dim objAuditTrailheader As AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.objEDD)

                    'Delete Detail
                    For Each item In objData.objListEDD_Detail
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub Accept(ID As Long)

        Dim IDUnik As Long

        Using objdb As New CasemanagementEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = NawaBLL.ModuleBLL.GetModuleByModuleName(objApproval.ModuleName)
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            Dim objData As OneFCC_EDD_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(OneFCC_EDD_CLASS))
                            IDUnik = SaveAddWithoutApproval(objModule, objData)

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objData As OneFCC_EDD_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(OneFCC_EDD_CLASS))

                            'Save data dan Audit Trail sama dengan Without Approval
                            SaveEditWithoutApproval(objModule, objData)

                            '25-May-2021 IDUnik ambil dati objData
                            IDUnik = objData.objEDD.PK_OneFCC_EDD_ID

                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objData_Old As OneFCC_EDD_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(OneFCC_EDD_CLASS))

                            'Save data dan Audit Trail sama dengan Without Approval
                            SaveDeleteWithoutApproval(objModule, objData_Old)

                            '25-May-2021 IDUnik ambil dati objData
                            IDUnik = objData_Old.objEDD.PK_OneFCC_EDD_ID

                    End Select

                    'Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()

                    objtrans.Commit()

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Shared Sub Reject(ID As Long)
        Using objdb As New CasemanagementEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As CasemanagementDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                    Dim intModuleAction As Integer = objApproval.PK_ModuleAction_ID
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    Dim objData As OneFCC_EDD_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(OneFCC_EDD_CLASS))

                    'Audit Trail Header
                    Dim objAuditTrailheader As AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    With objAuditTrailheader
                        .PK_ModuleApproval_ID = objApproval.PK_ModuleApproval_ID
                    End With
                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, objData.objEDD)

                    'Save Detail
                    For Each item In objData.objListEDD_Detail
                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()

                    objtrans.Commit()

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using

    End Sub

    Public Shared Function CopyGenericToDataTable(Of T)(list As IList(Of T)) As DataTable
        Dim props As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
        Dim table As New DataTable()
        For i As Integer = 0 To props.Count - 1
            Dim prop As PropertyDescriptor = props(i)
            table.Columns.Add(prop.Name, If(Nullable.GetUnderlyingType(prop.PropertyType), prop.PropertyType))
        Next
        Dim values As Object() = New Object(props.Count - 1) {}
        For Each item As T In list
            For i As Integer = 0 To values.Length - 1
                values(i) = If(props(i).GetValue(item), DBNull.Value)
            Next
            table.Rows.Add(values)
        Next
        Return table
    End Function

    Public Shared Function IsExistsInApproval(strModuleName As String, strID As String) As Boolean
        Try
            Dim isExists As Boolean = False

            Using objdb As New CasemanagementEntities
                Dim objCek = objdb.ModuleApprovals.Where(Function(x) x.ModuleName = strModuleName And x.ModuleKey = strID).FirstOrDefault
                If objCek IsNot Nothing Then
                    isExists = True
                End If
            End Using

            Return isExists

        Catch ex As Exception
            Throw ex
            Return True
        End Try
    End Function

    Public Shared Function GetNextLevelRole(objModule As NawaDAL.Module, intLvl As Integer) As Integer
        Try
            Using objdb As New CasemanagementEntities
                Dim introle As Integer = (From mwflow In objdb.MWorkFlows
                                          Join
                                               mwflowdetail In objdb.MWorkFlowDetails On
                                               mwflow.PK_WorkFlow Equals mwflowdetail.FK_WorkFlow
                                          Join
                                               mwflowmoduleTR In objdb.Module_TR_WorkFlow On
                                               mwflow.PK_WorkFlow Equals mwflowmoduleTR.FK_WorkFlow_ID
                                          Where
                                               (mwflowdetail.Level = (intLvl + 1)) And
                                               (mwflowmoduleTR.FK_ModuleID = objModule.PK_Module_ID)
                                          Select mwflowdetail.RoleID
                                               ).FirstOrDefault()
                Return introle
            End Using
        Catch ex As Exception
            Return 0
        End Try
    End Function

    ''' <summary>
    ''' Function For Check If User Role Using WorkFlow For This Module
    ''' </summary>
    ''' <param name="objModule">objModule = Module</param>
    ''' <param name="intLvl">User WorkFlow Level (usually 1)</param>
    ''' <returns></returns>
    Public Shared Function IsUserRoleUsingWorkFlow(objModule As NawaDAL.Module, intLvl As Integer) As Boolean
        Try
            Dim valueResponse As Boolean = False
            Dim intRole As Integer = NawaBLL.Common.SessionCurrentUser.FK_MRole_ID
            Using objdb As New CasemanagementEntities
                Dim dataTRWorkflow As Module_TR_WorkFlow = objdb.Module_TR_WorkFlow.Where(Function(x) x.FK_ModuleID = objModule.PK_Module_ID).FirstOrDefault
                If dataTRWorkflow IsNot Nothing AndAlso dataTRWorkflow.FK_WorkFlow_ID <> Nothing Then
                    Dim dataWorkFlowDetail As MWorkFlowDetail = objdb.MWorkFlowDetails.Where(Function(x) x.FK_WorkFlow = dataTRWorkflow.FK_WorkFlow_ID And x.Level = intLvl And x.RoleID = intRole).FirstOrDefault
                    If dataWorkFlowDetail IsNot Nothing Then
                        valueResponse = True
                    End If
                End If
                'Dim intcount As Integer = 0
                'Dim intcount As Integer = (From mwflow In objdb.MWorkFlow
                '                           Join mwflowdetail In objdb.MWorkFlowDetail
                '                                On mwflow.PK_WorkFlow Equals mwflowdetail.FK_WorkFlow
                '                           Join mwflowmoduleTR In objdb.Module_TR_WorkFlow
                '                                On mwflow.PK_WorkFlow Equals mwflowmoduleTR.FK_WorkFlow_ID
                '                           Where
                '                                 (mwflowdetail.RoleID = intRole) And
                '                                (mwflowdetail.Level = intLvl) And
                '                                (mwflowmoduleTR.FK_ModuleID = objModule.PK_Module_ID)
                '                                ).Count()
                'If intcount = 0 Then
                '    valueResponse = False
                'End If
            End Using
            Return valueResponse
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Function For Check If Data Approval From WorkFlow
    ''' </summary>
    ''' <param name="objModule">objModule = Module</param>
    ''' <param name="unikID">unikID = ModuleKey From ModuleApproval</param>
    ''' <param name="status">1 = (Draft), 2 = (Submit), 3 = (InProgress), 4 = (Approved), 5 = (Rejected), 6 = (Revise)</param>
    ''' <returns></returns>
    'Public Shared Function IsFromWorkFlowByStatus(objModule As NawaDAL.Module, unikID As String, status As Integer) As Boolean
    '    Try
    '        Dim valueResponse As Boolean = False
    '        Using objdb As New CasemanagementEntities
    '            Dim workFlowProgress As MWorkFlow_Progress = objdb.MWorkFlow_Progress.Where(Function(x) x.FK_Module_ID = objModule.PK_Module_ID And x.FK_Unik_ID = unikID And x.FK_MWorkflow_Status_ID = status).FirstOrDefault
    '            If workFlowProgress IsNot Nothing Then
    '                valueResponse = True
    '            End If
    '        End Using
    '        Return valueResponse
    '    Catch ex As Exception
    '        Return False
    '    End Try
    'End Function
    Public Shared Function IsFromWorkFlowByStatus(objModule As NawaDAL.Module, unikID As String, status As Integer) As MWorkFlow_Progress
        Try
            Using objdb As New CasemanagementEntities
                Return objdb.MWorkFlow_Progress.Where(Function(x) x.FK_Module_ID = objModule.PK_Module_ID And x.FK_Unik_ID = unikID And x.FK_MWorkflow_Status_ID = status).FirstOrDefault
            End Using
        Catch ex As Exception
            Return New MWorkFlow_Progress
        End Try
    End Function

    ''' <summary>
    ''' Function For Get Data WorkFlow History
    ''' </summary>
    ''' <param name="objModule">objModule = Module</param>
    ''' <param name="unikID">unikID = ModuleKey From ModuleApproval</param>
    ''' <param name="PKApproval">PKApproval = Primary Key From ModuleApproval</param>
    ''' <returns></returns>
    Public Shared Function GetDataWorkFlowHistory(objModule As NawaDAL.Module, unikID As String, PKApproval As Long) As List(Of MWorkFlow_History)
        Try
            Dim valueResponse As New List(Of MWorkFlow_History)
            Using objdb As New CasemanagementEntities
                Dim workFlowProgress As List(Of MWorkFlow_History) = objdb.MWorkFlow_History.Where(Function(x) x.FK_Module_ID = objModule.PK_Module_ID And x.FK_Unik_ID = unikID And x.FK_ModuleApproval_ID = PKApproval).ToList
                If workFlowProgress IsNot Nothing Then
                    valueResponse = workFlowProgress
                End If
            End Using
            Return valueResponse
        Catch ex As Exception
            Return New List(Of MWorkFlow_History)
        End Try
    End Function

    Shared Sub SaveAddWithApprovalWorkFlow(objModule As NawaDAL.Module, objData As OneFCC_EDD_CLASS, note As String, unikID As String)

        Using objDB As New CasemanagementEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    With objData.objEDD
                        .Alternateby = strAlternateID
                    End With

                    Dim nextRoleId As Integer = GetNextLevelRole(objModule, 1)

                    'Dim guid As String = System.Guid.NewGuid().ToString()
                    'Dim unikID As String = DateTime.Now.ToString("yyyyMMddHHmmssfff") & strUserID & "NewData"

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objModuleApproval As New ModuleApproval
                    With objModuleApproval
                        '.FK_MRole_ID = nextRoleId
                        .FK_MRole_ID = NawaBLL.Common.SessionCurrentUser.FK_MRole_ID
                        .ModuleKey = unikID
                        .ModuleName = objModule.ModuleName
                        .ModuleField = objXMLData
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With
                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    ValidateUserAndInitWorkflow(objDB, objModule.PK_Module_ID, NawaBLL.Common.SessionCurrentUser.UserID, unikID, objModuleApproval.PK_ModuleApproval_ID, note)
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub SaveEditWithApprovalWorkFLow(objModule As NawaDAL.Module, objData As OneFCC_EDD_CLASS, note As String)
        'Get Old Data for Audit Trail
        Dim objData_Old = GetEDDClassByID(objData.objEDD.PK_OneFCC_EDD_ID)

        Using objDB As New CasemanagementEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objEDD
                        .LastUpdateBy = strUserID
                        .LastUpdateDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objXMLData_Old As String = NawaBLL.Common.Serialize(objData_Old)

                    Dim objModuleApproval As New ModuleApproval
                    With objModuleApproval
                        .FK_MRole_ID = NawaBLL.Common.SessionCurrentUser.FK_MRole_ID
                        .ModuleKey = objData.objEDD.PK_OneFCC_EDD_ID
                        .ModuleName = objModule.ModuleName
                        .ModuleField = objXMLData
                        .ModuleFieldBefore = objXMLData_Old
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    ValidateUserAndInitWorkflow(objDB, objModule.PK_Module_ID, NawaBLL.Common.SessionCurrentUser.UserID, objData.objEDD.PK_OneFCC_EDD_ID, objModuleApproval.PK_ModuleApproval_ID, note)

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub SaveDataCorrection(objModule As NawaDAL.Module, objData As OneFCC_EDD_CLASS, note As String, objApproval As CasemanagementDAL.ModuleApproval)
        Using objDB As New CasemanagementDAL.CasemanagementEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objEDD
                        .LastUpdateBy = strUserID
                        .LastUpdateDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)

                    With objApproval
                        .ModuleField = objXMLData
                    End With


                    objDB.Entry(objApproval).State = Entity.EntityState.Modified
                    objDB.SaveChanges()
                    SaveWorkflow(objDB, objTrans, objApproval.PK_ModuleApproval_ID, objModule.PK_Module_ID, objApproval.ModuleKey, note, 1)

                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Function ValidateUserAndInitWorkflow(objDb As CasemanagementEntities, intModule As Integer, struserid As String, strunik As String, pkmoduleapprovalid As Long, Optional ByVal strReviewNotes As String = "") As Boolean

        Dim objListParam(2) As System.Data.SqlClient.SqlParameter

        objListParam(0) = New System.Data.SqlClient.SqlParameter
        objListParam(0).ParameterName = "@pkmoduleid"
        objListParam(0).Value = intModule
        'di isi karena pas mau save, pasti sudah ada isinya
        objListParam(1) = New System.Data.SqlClient.SqlParameter
        objListParam(1).ParameterName = "@pkunikid"
        objListParam(1).Value = strunik

        objListParam(2) = New System.Data.SqlClient.SqlParameter
        objListParam(2).ParameterName = "@userId"
        objListParam(2).Value = struserid

        'Dim strresult As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_ValidateMWorkflow", objListParam)
        'Dim pkworkflowid As Integer = objDb.Database.ExecuteSqlCommand("exec usp_ValidateMWorkflow @pkmoduleid,@pkunikid,@userId", objListParam)
        Dim pkworkflowid As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_ValidateMWorkflow", objListParam)

        If pkworkflowid > 0 Then

            objDb.Database.ExecuteSqlCommand("exec usp_CreateInitWorkflowModuleDesigner '" & pkworkflowid & "','" & strunik & "','" & NawaBLL.Common.SessionCurrentUser.PK_MUser_ID & "','" & intModule & "','" & NawaBLL.Common.SessionCurrentUser.PK_MUser_ID & "','" & strReviewNotes.Replace("'", "''") & "','3'," & pkmoduleapprovalid & ", '4'")

        End If


    End Function

    Shared Function SaveWorkflow(objdb As CasemanagementEntities, objtrans As Entity.DbContextTransaction, pkmoduleapprovalid As Long, pkmoduleid As Integer, pkunik As String, strnotes As String, intworkflowdecision As Integer) As Boolean
        Try
            Dim bFinalresult As Boolean = False
            Dim objListParam(6) As SqlParameter
            objListParam(0) = New SqlParameter
            objListParam(1) = New SqlParameter
            objListParam(2) = New SqlParameter
            objListParam(3) = New SqlParameter
            objListParam(4) = New SqlParameter
            objListParam(5) = New SqlParameter
            objListParam(6) = New SqlParameter

            objListParam(0).ParameterName = "@FK_ModuleApproval_ID"
            objListParam(0).Value = pkmoduleapprovalid

            objListParam(1).ParameterName = "@pkmoduleid"
            objListParam(1).Value = pkmoduleid

            objListParam(2).ParameterName = "@pkunik"
            objListParam(2).Value = pkunik

            If Not NawaBLL.Common.SessionAlternateUser Is Nothing Then
                objListParam(3).ParameterName = "@pkuseridexecute"
                objListParam(3).Value = NawaBLL.Common.SessionAlternateUser.PK_MUser_ID
            Else
                objListParam(3).ParameterName = "@pkuseridexecute"
                objListParam(3).Value = NawaBLL.Common.SessionCurrentUser.PK_MUser_ID
            End If

            objListParam(4).ParameterName = "@notes"
            objListParam(4).Value = strnotes

            objListParam(5).ParameterName = "@intworkflowstatus"
            objListParam(5).Value = intworkflowdecision

            objListParam(6).ParameterName = "@finalAccept"
            objListParam(6).Direction = ParameterDirection.InputOutput
            objListParam(6).Value = bFinalresult


            NawaDAL.SQLHelper.ExecuteScalar(objdb.Database.Connection, objtrans, CommandType.StoredProcedure, "usp_SaveWorkflowModuleDesigner", objListParam)

            objdb.SaveChanges()
            objtrans.Commit()

            Return objListParam(6).Value

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Function getDataRowFromWithStringQuery(strQuery As String) As DataRow
        Try
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Return drResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Function getDataTableFromWithStringQuery(strQuery As String) As DataTable
        Try
            Dim drResult As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Return drResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class


Public Class OneFCC_EDD_CLASS

    Public objEDD As New OneFCC_EDD
    Public objListEDD_Detail As New List(Of OneFCC_EDD_Detail)
    'Public objEDDExtendInformation As New OneFCC_EDD_Extend_Information

End Class
