﻿Imports Ext.Net
Imports CasemanagementDAL
Imports NawaDAL
Imports System.Data.SqlClient

Public Class SARMemo_BLL
    Public Sub New()

    End Sub

    Shared Function getSARMemoClassByID(id As Integer) As SARMemoClass
        Dim objSARMemoClass As New SARMemoClass
        Using objdb As New CasemanagementEntities
            Dim objSARMemo As New OneFCC_CaseManagement_SARMemo
            Dim ObjSARMemoDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_SARMemo where pk_OneFCC_CaseManagement_SARMemo_ID = " & id, Nothing)
            For Each item As DataRow In ObjSARMemoDT.Rows
                objSARMemo.PK_OneFCC_CaseManagement_SARMemoID = item("PK_OneFCC_CaseManagement_SARMemo_ID")
                objSARMemo.Cabang = item("Cabang")
                objSARMemo.CIF = item("CIF")
                objSARMemo.Consumer_Banking = item("Consumer_Banking")
                objSARMemo.CreatedBy = item("CreatedBy")
                objSARMemo.CreatedDate = item("CreatedDate")
                objSARMemo.Dari = item("Dari")
                objSARMemo.Detail_Kesimpulan = item("Detail_Kesimpulan")
                objSARMemo.Direktorat = item("Direktorat")
                objSARMemo.FK_CaseManagement_Workflow_SARMemo_ID = item("FK_CaseManagement_Workflow_SARMemo_ID")
                objSARMemo.Hasil_Analisis = item("Hasil_Analisis")
                objSARMemo.IsCustomer = item("IsCustomer")
                objSARMemo.Kateri_Pihak_Terlapor = item("Kateri_Pihak_Terlapor")
                objSARMemo.Kepada = item("Kepada")
                objSARMemo.Unit_Bisnis_Directorat = item("Unit_Bisnis_Directorat")
                objSARMemo.Direktorat = item("Direktorat")
                objSARMemo.Cabang = item("Cabang")
                objSARMemo.Lama_menjadi_Nasabah = item("Lama_menjadi_Nasabah")
                objSARMemo.Kesimpulan = item("Kesimpulan")
                objSARMemo.Lama_menjadi_Nasabah = item("Lama_menjadi_Nasabah")
                objSARMemo.Nama_Pihak_Terlapor = item("Nama_Pihak_Terlapor")
                objSARMemo.No = item("No")
                objSARMemo.Pekerjaan_Bidang_Usaha = item("Pekerjaan_Bidang_Usaha")
                objSARMemo.Penghasilan_Tahun = item("Penghasilan_Tahun")
                objSARMemo.Perihal = item("Perihal")
                objSARMemo.PK_OneFCC_CaseManagement_SARMemoID = item("PK_OneFCC_CaseManagement_SARMemo_ID")
                objSARMemo.Portofolio_Calon_Pihak_Terlapor = item("Portofolio_Calon_Pihak_Terlapor")
                objSARMemo.Profil_Lainnya = item("Profil_Lainnya")
                objSARMemo.StatusID = item("StatusID")
                objSARMemo.Sumber_Pelaporan = item("Sumber_Pelaporan")
                objSARMemo.Tanggal = item("Tanggal")
                objSARMemo.Tindak_lanjut = item("Tindak_lanjut")
                objSARMemo.Tipologi = item("Tipologi")
                objSARMemo.Unit_Bisnis_Directorat = item("Unit_Bisnis_Directorat")
                objSARMemo.WIC = item("WIC")
                objSARMemo.Workflow_Step = item("Workflow_Step")
                objSARMemo.Jenis_Laporan = item("Jenis_Laporan")
                objSARMemo.Tanggal_Laporan = item("Tanggal_Laporan")
                objSARMemo.NoRefPPATK = item("NoRefPPATK")
                objSARMemo.Alasan = item("Alasan")
            Next

            Dim listCaseID As New List(Of OneFCC_CaseManagement_SARMemo_CaseID)
            Dim listCaseIDDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_SARMemo_CaseID where FK_OneFCC_CaseManagement_SARMemo_ID = " & id, Nothing)
            For Each item As DataRow In listCaseIDDT.Rows
                Dim CaseID As New OneFCC_CaseManagement_SARMemo_CaseID
                CaseID.PK_OneFCC_CaseManagement_SARMemo_CaseIDID = item("PK_OneFCC_CaseManagement_SARMemo_CaseID_ID")
                CaseID.FK_OneFCC_CaseManagement_SARMemoID = item("FK_OneFCC_CaseManagement_SARMemo_ID")
                CaseID.CaseID = item("CaseID")
                CaseID.Active = item("Active")
                CaseID.CreatedBy = item("CreatedBy")
                CaseID.LastUpdateBy = item("LastUpdateBy")
                CaseID.ApprovedBy = item("ApprovedBy")
                CaseID.CreatedDate = item("CreatedDate")
                CaseID.LastUpdateDate = item("LastUpdateDate")
                CaseID.ApprovedDate = item("ApprovedDate")
                CaseID.Alternateby = item("Alternateby")
                listCaseID.Add(CaseID)
            Next


            Dim listIndikator As New List(Of OneFCC_CaseManagement_SARMemo_Indikator)
            Dim listIndikatorDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_SARMemo_Indikator where FK_OneFCC_CaseManagement_SARMemo_ID = " & id, Nothing)
            For Each item As DataRow In listIndikatorDT.Rows
                Dim Indikator As New OneFCC_CaseManagement_SARMemo_Indikator
                Indikator.PK_OneFCC_CaseManagement_SARMemo_IndikatorID = item("PK_OneFCC_CaseManagement_SARMemo_Indikator_ID")
                Indikator.FK_OneFCC_CaseManagement_SARMemoID = item("FK_OneFCC_CaseManagement_SARMemo_ID")
                Indikator.FK_Indicator = item("FK_Indicator")
                Indikator.Active = item("Active")
                Indikator.CreatedBy = item("CreatedBy")
                Indikator.LastUpdateBy = item("LastUpdateBy")
                Indikator.ApprovedBy = item("ApprovedBy")
                Indikator.CreatedDate = item("CreatedDate")
                Indikator.LastUpdateDate = item("LastUpdateDate")
                Indikator.ApprovedDate = item("ApprovedDate")
                Indikator.Alternateby = item("Alternateby")
                listIndikator.Add(Indikator)
            Next


            Dim listPihakTerkait As New List(Of OneFCC_CaseManagement_SARMemo_Pihak_Terkait)
            Dim listPihakTerkaitDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_SARMemo_Pihak_Terkait where FK_OneFCC_CaseManagement_SARMemo_ID = " & id, Nothing)
            For Each item As DataRow In listPihakTerkaitDT.Rows
                Dim PihakTerkait As New OneFCC_CaseManagement_SARMemo_Pihak_Terkait
                PihakTerkait.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID = item("PK_OneFCC_CaseManagement_SARMemo_Pihak_Terkait_ID")
                PihakTerkait.Nama_PihakTerkait = item("Nama_PihakTerkait")
                PihakTerkait.Hubungan_PihakTerkait = item("Hubungan_PihakTerkait")
                PihakTerkait.Dilaporkan_PihakTerkait = item("Dilaporkan_PihakTerkait")
                PihakTerkait.RefGrips_PihakTerkait = item("RefGrips_PihakTerkait")
                PihakTerkait.FK_OneFCC_CaseManagement_SARMemoID = item("FK_OneFCC_CaseManagement_SARMemo_ID")
                PihakTerkait.CIF = item("CIF")
                PihakTerkait.Active = item("Active")
                PihakTerkait.CreatedBy = item("CreatedBy")
                PihakTerkait.LastUpdateBy = item("LastUpdateBy")
                PihakTerkait.ApprovedBy = item("ApprovedBy")
                PihakTerkait.CreatedDate = item("CreatedDate")
                PihakTerkait.LastUpdateDate = item("LastUpdateDate")
                PihakTerkait.ApprovedDate = item("ApprovedDate")
                PihakTerkait.Alternateby = item("Alternateby")
                listPihakTerkait.Add(PihakTerkait)
            Next


            Dim listTindakLanjut As New List(Of OneFCC_CaseManagement_SARMemo_TindakLanjut)
            Dim listTindakLanjutDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_SARMemo_TindakLanjut where FK_OneFCC_CaseManagement_SARMemo_ID = " & id, Nothing)
            For Each item As DataRow In listTindakLanjutDT.Rows
                Dim TindakLanjut As New OneFCC_CaseManagement_SARMemo_TindakLanjut
                TindakLanjut.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID = item("PK_OneFCC_CaseManagement_SARMemo_TindakLanjut_ID")
                TindakLanjut.TindakLanjutDescription = item("TindakLanjutDescription")
                TindakLanjut.FK_OneFCC_CaseManagement_TindakLanjut_ID = item("FK_OneFCC_CaseManagement_TindakLanjut_ID")
                TindakLanjut.FK_OneFCC_CaseManagement_SARMemoID = item("FK_OneFCC_CaseManagement_SARMemo_ID")
                TindakLanjut.Active = item("Active")
                TindakLanjut.CreatedBy = item("CreatedBy")
                TindakLanjut.LastUpdateBy = item("LastUpdateBy")
                TindakLanjut.ApprovedBy = item("ApprovedBy")
                TindakLanjut.CreatedDate = item("CreatedDate")
                TindakLanjut.LastUpdateDate = item("LastUpdateDate")
                TindakLanjut.ApprovedDate = item("ApprovedDate")
                TindakLanjut.Alternateby = item("Alternateby")
                listTindakLanjut.Add(TindakLanjut)
            Next


            Dim listAttachment As New List(Of OneFCC_CaseManagement_SARMemo_Attachment)
            Dim listAttachmentDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_SARMemo_Attachment where FK_OneFCC_CaseManagement_SARMemo_ID = " & id, Nothing)
            For Each item As DataRow In listAttachmentDT.Rows
                Dim Attachment As New OneFCC_CaseManagement_SARMemo_Attachment
                Attachment.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = item("PK_OneFCC_CaseManagement_SARMemo_Attachment_ID")
                Attachment.Fk_goAML_ODM_Generate_STR_SAR = item("Fk_goAML_ODM_Generate_STR_SAR")
                Attachment.File_Name = item("File_Name")
                Attachment.File_Doc = item("File_Doc")
                Attachment.FK_OneFCC_CaseManagement_SARMemoID = item("FK_OneFCC_CaseManagement_SARMemo_ID")
                Attachment.Remarks = item("Remarks")
                Attachment.Active = item("Active")
                Attachment.CreatedBy = item("CreatedBy")
                Attachment.LastUpdateBy = item("LastUpdateBy")
                Attachment.ApprovedBy = item("ApprovedBy")
                Attachment.CreatedDate = item("CreatedDate")
                Attachment.LastUpdateDate = item("LastUpdateDate")
                Attachment.ApprovedDate = item("ApprovedDate")
                Attachment.Alternateby = item("Alternateby")
                listAttachment.Add(Attachment)
            Next

            Dim listActivity As New List(Of OneFCC_CaseManagement_SARMemo_Activity)
            Dim listActivityDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_SARMemo_Activity where FK_OneFCC_CaseManagement_SARMemo_ID = " & id, Nothing)
            For Each item As DataRow In listActivityDT.Rows
                Dim Activity As New OneFCC_CaseManagement_SARMemo_Activity
                Activity.Pk_OneFCC_CaseManagement_SARMemo_ActivityID = item("Pk_OneFCC_CaseManagement_SARMemo_Activity_ID")
                Activity.Account_NO = item("Account_NO")
                Activity.FK_OneFCC_CaseManagement_SARMemoID = item("FK_OneFCC_CaseManagement_SARMemo_ID")
                Activity.Date_Activity = item("Date_Activity")
                Activity.Significance = Convert.ToInt32(item("Significance"))
                Activity.Reason = item("Reason")
                Activity.Comments = item("Comments")
                Activity.Active = item("Active")
                Activity.CreatedBy = item("CreatedBy")
                Activity.LastUpdateBy = item("LastUpdateBy")
                Activity.ApprovedBy = item("ApprovedBy")
                Activity.CreatedDate = item("CreatedDate")
                Activity.LastUpdateDate = item("LastUpdateDate")
                Activity.ApprovedDate = item("ApprovedDate")
                Activity.Alternateby = item("Alternateby")
                listActivity.Add(Activity)
            Next

            If objSARMemo IsNot Nothing Then
                objSARMemoClass.ObjSARMemo = objSARMemo
            End If
            If listIndikator IsNot Nothing Then
                objSARMemoClass.listIndikator = listIndikator
            End If
            If listPihakTerkait IsNot Nothing Then
                objSARMemoClass.listPihakTerkait = listPihakTerkait
            End If
            If listTindakLanjut IsNot Nothing Then
                objSARMemoClass.listTindakLanjut = listTindakLanjut
            End If
            If listCaseID IsNot Nothing Then
                objSARMemoClass.listCaseID = listCaseID
            End If
            If listActivity IsNot Nothing Then
                objSARMemoClass.listActivity = listActivity
            End If
            If listAttachment IsNot Nothing Then
                objSARMemoClass.listAttachment = listAttachment
            End If
        End Using
        Return objSARMemoClass
    End Function

    Public Shared Sub SaveAddTanpaApproval(objSARMemoClass As SARMemoClass, objModule As NawaDAL.Module)
        Try
            Dim strQuery As String = ""
            With objSARMemoClass.ObjSARMemo
                If String.IsNullOrEmpty(.CreatedBy) Then
                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .CreatedDate = Now
                End If

                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                .ApprovedDate = Now
            End With
            '14-Des-2021 Penambahan Field Submission_Type
            'Save objData to Table SIPENDAR_WATCHLIST_Upload
            strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo "
            strQuery += "("
            strQuery += "No,"
            strQuery += "Kepada,"
            strQuery += "Dari,"
            strQuery += "Tanggal,"
            strQuery += "Perihal,"
            strQuery += "Sumber_Pelaporan,"
            strQuery += "Tipologi,"
            strQuery += "Nama_Pihak_Terlapor,"
            strQuery += "Kateri_Pihak_Terlapor,"
            strQuery += "Unit_Bisnis_Directorat,"
            strQuery += "Consumer_Banking,"
            strQuery += "Lama_menjadi_Nasabah,"
            strQuery += "Pekerjaan_Bidang_Usaha,"
            strQuery += "Penghasilan_Tahun,"
            strQuery += "Profil_Lainnya,"
            strQuery += "Portofolio_Calon_Pihak_Terlapor,"
            strQuery += "Hasil_Analisis,"
            strQuery += "Kesimpulan,"
            strQuery += "Detail_Kesimpulan,"
            strQuery += "Tindak_lanjut,"
            strQuery += "StatusID,"
            strQuery += "CIF,"
            strQuery += "WIC,"
            strQuery += "FK_CaseManagement_Workflow_SARMemo_ID,"
            strQuery += "Workflow_Step,"
            strQuery += "Direktorat,"
            strQuery += "Cabang,"
            strQuery += "IsCustomer,"
            strQuery += "Active,"
            strQuery += "CreatedBy,"
            strQuery += "LastUpdateBy,"
            strQuery += "ApprovedBy,"
            strQuery += "CreatedDate,"
            strQuery += "LastUpdateDate,"
            strQuery += "ApprovedDate,"
            strQuery += "Jenis_Laporan,"
            strQuery += "Tanggal_Laporan,"
            strQuery += "NoRefPPATK,"
            strQuery += "Alasan,"
            strQuery += "Alternateby"
            strQuery += ") VALUES ( "
            strQuery += "'" & objSARMemoClass.ObjSARMemo.No & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Kepada & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Dari & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Tanggal & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Perihal & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Sumber_Pelaporan.ToString & "'"
            strQuery += ", " & objSARMemoClass.ObjSARMemo.Tipologi & ""
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Nama_Pihak_Terlapor & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Kateri_Pihak_Terlapor & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Unit_Bisnis_Directorat & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Consumer_Banking & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Lama_menjadi_Nasabah & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Pekerjaan_Bidang_Usaha & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Penghasilan_Tahun & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Profil_Lainnya & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Portofolio_Calon_Pihak_Terlapor & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Hasil_Analisis & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Kesimpulan & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Detail_Kesimpulan & "'"
            strQuery += ", " & objSARMemoClass.ObjSARMemo.Tindak_lanjut & ""
            strQuery += ", " & objSARMemoClass.ObjSARMemo.StatusID & ""
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.CIF & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.WIC & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.FK_CaseManagement_Workflow_SARMemo_ID & "'"
            strQuery += ", " & objSARMemoClass.ObjSARMemo.Workflow_Step & ""
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Direktorat & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Cabang & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.IsCustomer & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Active & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.CreatedBy & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.LastUpdateBy & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.ApprovedBy & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.CreatedDate & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.LastUpdateDate & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.ApprovedDate & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Jenis_Laporan & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Tanggal_Laporan & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.NoRefPPATK & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Alasan & "'"
            strQuery += ", '" & objSARMemoClass.ObjSARMemo.Alternateby & "')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            strQuery = ""

            Dim PKSARMemoLatest As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_OneFCC_CaseManagement_SARMemo_ID FROM OneFCC_CaseManagement_SARMemo order by pk_OneFCC_CaseManagement_SARMemo_id desc", Nothing)

            'Add Indikator
            For Each item In objSARMemoClass.listIndikator
                strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Indikator ( "
                strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                strQuery += "FK_Indicator,"
                strQuery += "Active,"
                strQuery += "CreatedBy,"
                strQuery += "LastUpdateBy,"
                strQuery += "ApprovedBy,"
                strQuery += "CreatedDate,"
                strQuery += "LastUpdateDate,"
                strQuery += "ApprovedDate,"
                strQuery += "Alternateby"
                strQuery += ") VALUES ( "
                strQuery += " " & PKSARMemoLatest & ""
                strQuery += " , '" & item.FK_Indicator & "'"
                strQuery += " , '" & item.Active & "'"
                strQuery += " , '" & item.CreatedBy & "'"
                strQuery += " , '" & item.LastUpdateBy & "'"
                strQuery += " , '" & item.ApprovedBy & "'"
                strQuery += " , '" & item.CreatedDate & "'"
                strQuery += " , '" & item.LastUpdateDate & "'"
                strQuery += " , '" & item.ApprovedDate & "'"
                strQuery += " , '" & item.Alternateby & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
            strQuery = ""

            'Add Pihak Terkait
            For Each item In objSARMemoClass.listPihakTerkait
                strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Pihak_Terkait ( "
                strQuery += "Nama_PihakTerkait,"
                strQuery += "Hubungan_PihakTerkait,"
                strQuery += "Dilaporkan_PihakTerkait,"
                strQuery += "RefGrips_PihakTerkait,"
                strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                strQuery += "CIF,"
                strQuery += "Active,"
                strQuery += "CreatedBy,"
                strQuery += "LastUpdateBy,"
                strQuery += "ApprovedBy,"
                strQuery += "CreatedDate,"
                strQuery += "LastUpdateDate,"
                strQuery += "ApprovedDate,"
                strQuery += "Alternateby"
                strQuery += " ) VALUES ( "
                strQuery += " '" & item.Nama_PihakTerkait & "'"
                strQuery += " , '" & item.Hubungan_PihakTerkait & "'"
                strQuery += " , '" & item.Dilaporkan_PihakTerkait & "'"
                strQuery += " , '" & item.RefGrips_PihakTerkait & "'"
                strQuery += " , " & PKSARMemoLatest & ""
                strQuery += " , '" & item.CIF & "'"
                strQuery += " , '" & item.Active & "'"
                strQuery += " , '" & item.CreatedBy & "'"
                strQuery += " , '" & item.LastUpdateBy & "'"
                strQuery += " , '" & item.ApprovedBy & "'"
                strQuery += " , '" & item.CreatedDate & "'"
                strQuery += " , '" & item.LastUpdateDate & "'"
                strQuery += " , '" & item.ApprovedDate & "'"
                strQuery += " , '" & item.Alternateby & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Next
            strQuery = ""
            'Add Tindak Lanjut
            For Each item In objSARMemoClass.listTindakLanjut
                strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_TindakLanjut ( "
                strQuery += "TindakLanjutDescription,"
                strQuery += "FK_OneFCC_CaseManagement_TindakLanjut_ID,"
                strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                strQuery += "Active,"
                strQuery += "CreatedBy,"
                strQuery += "LastUpdateBy,"
                strQuery += "ApprovedBy,"
                strQuery += "CreatedDate,"
                strQuery += "LastUpdateDate,"
                strQuery += "ApprovedDate,"
                strQuery += "Alternateby"
                strQuery += " ) VALUES ( "
                strQuery += " '" & item.TindakLanjutDescription & "'"
                strQuery += " ," & item.FK_OneFCC_CaseManagement_TindakLanjut_ID & ""
                strQuery += " ," & PKSARMemoLatest & ""
                strQuery += " ,'" & item.Active & "'"
                strQuery += " ,'" & item.CreatedBy & "'"
                strQuery += " ,'" & item.LastUpdateBy & "'"
                strQuery += " ,'" & item.ApprovedBy & "'"
                strQuery += " ,'" & item.CreatedDate & "'"
                strQuery += " ,'" & item.LastUpdateDate & "'"
                strQuery += " ,'" & item.ApprovedDate & "'"
                strQuery += " ,'" & item.Alternateby & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
            strQuery = ""
            'Add Case ID
            For Each item In objSARMemoClass.listCaseID
                strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_CaseID ( "
                strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                strQuery += "CaseID,"
                strQuery += "Active,"
                strQuery += "CreatedBy,"
                strQuery += "LastUpdateBy,"
                strQuery += "ApprovedBy,"
                strQuery += "CreatedDate,"
                strQuery += "LastUpdateDate,"
                strQuery += "ApprovedDate,"
                strQuery += "Alternateby"
                strQuery += " ) VALUES ( "
                strQuery += "" & PKSARMemoLatest & ""
                strQuery += "," & item.CaseID & ""
                strQuery += ",'" & item.Active & "'"
                strQuery += ",'" & item.CreatedBy & "'"
                strQuery += ",'" & item.LastUpdateBy & "'"
                strQuery += ",'" & item.ApprovedBy & "'"
                strQuery += ",'" & item.CreatedDate & "'"
                strQuery += ",'" & item.LastUpdateDate & "'"
                strQuery += ",'" & item.ApprovedDate & "'"
                strQuery += ",'" & item.Alternateby & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
            strQuery = ""


            'Add Attachment
            For Each item In objSARMemoClass.listAttachment
                Dim objParam(0) As SqlParameter

                objParam(0) = New SqlParameter
                objParam(0).ParameterName = "@FileBinary"
                objParam(0).Value = item.File_Doc
                objParam(0).DbType = SqlDbType.Binary

                strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Attachment ( "
                strQuery += "Fk_goAML_ODM_Generate_STR_SAR,"
                strQuery += "File_Name,"
                strQuery += "File_Doc,"
                strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                strQuery += "Remarks,"
                strQuery += "Active,"
                strQuery += "CreatedBy,"
                strQuery += "LastUpdateBy,"
                strQuery += "ApprovedBy,"
                strQuery += "CreatedDate,"
                strQuery += "LastUpdateDate,"
                strQuery += "ApprovedDate,"
                strQuery += "Alternateby"
                strQuery += " ) VALUES ( "
                strQuery += "'" & item.Fk_goAML_ODM_Generate_STR_SAR & "'"
                strQuery += ",'" & item.File_Name & "'"
                strQuery += ", CONVERT(varbinary(max),@FileBinary)"
                strQuery += ",'" & PKSARMemoLatest & "'"
                strQuery += ",'" & item.Remarks & "'"
                strQuery += ",'" & item.Active & "'"
                strQuery += ",'" & item.CreatedBy & "'"
                strQuery += ",'" & item.LastUpdateBy & "'"
                strQuery += ",'" & item.ApprovedBy & "'"
                strQuery += ",'" & item.CreatedDate & "'"
                strQuery += ",'" & item.LastUpdateDate & "'"
                strQuery += ",'" & item.ApprovedDate & "'"
                strQuery += ",'" & item.Alternateby & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, objParam)
            Next
            strQuery = ""
            'Add Activity
            For Each item In objSARMemoClass.listActivity
                strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Activity ( "
                strQuery += "Account_NO,"
                strQuery += "Date_Activity,"
                strQuery += "Significance,"
                strQuery += "Reason,"
                strQuery += "Comments,"
                strQuery += "Active,"
                strQuery += "CreatedBy,"
                strQuery += "LastUpdateBy,"
                strQuery += "ApprovedBy,"
                strQuery += "CreatedDate,"
                strQuery += "LastUpdateDate,"
                strQuery += "ApprovedDate,"
                strQuery += "Alternateby,"
                strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID"
                strQuery += " ) VALUES ( "
                strQuery += " '" & item.Account_NO & "'"
                strQuery += " , '" & item.Date_Activity & "'"
                strQuery += " , '" & item.Significance & "'"
                strQuery += " , '" & item.Reason & "'"
                strQuery += " , '" & item.Comments & "'"
                strQuery += " ,'" & item.Active & "'"
                strQuery += " , '" & item.CreatedBy & "'"
                strQuery += " , '" & item.LastUpdateBy & "'"
                strQuery += " , '" & item.ApprovedBy & "'"
                strQuery += " , '" & item.CreatedDate & "'"
                strQuery += " , '" & item.LastUpdateDate & "'"
                strQuery += " , '" & item.ApprovedDate & "'"
                strQuery += " , '" & item.Alternateby & "'"
                strQuery += " , '" & PKSARMemoLatest & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next

            Dim WorkHistory As New OneFCC_CaseManagement_WorkflowHistory_SARMemo
            With WorkHistory
                .FK_OneFCC_CaseManagement_SARMemo_ID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID
                .Workflow_Step = 1
                .UserID = NawaBLL.Common.SessionCurrentUser.UserID
                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                .CreatedDate = Now
            End With
            strQuery = ""
            strQuery = "INSERT INTO OneFCC_CaseManagement_WorkflowHistory_SARMemo( "
            strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
            strQuery += "Workflow_Step,"
            strQuery += "UserID,"
            strQuery += "CreatedBy,"
            strQuery += "CreatedDate"
            strQuery += " ) VALUES ( "
            strQuery += " " & PKSARMemoLatest & ""
            strQuery += " ," & WorkHistory.Workflow_Step & ""
            strQuery += " , '" & WorkHistory.UserID & "'"
            strQuery += " , '" & WorkHistory.CreatedBy & "'"
            strQuery += " , '" & WorkHistory.CreatedDate & "')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            strQuery = ""

            ' Audit Trail
            strQuery = "INSERT INTO AuditTrailHeader ( "
            strQuery += "ApproveBy,"
            strQuery += "CreatedBy,"
            strQuery += "CreatedDate,"
            strQuery += "FK_AuditTrailStatus_ID,"
            strQuery += "FK_ModuleAction_ID,"
            strQuery += "ModuleLabel"
            strQuery += " ) VALUES ( "
            strQuery += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            strQuery += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
            strQuery += " , '" & NawaBLL.Common.ModuleActionEnum.Insert & "'"
            strQuery += " , '" & objModule.ModuleLabel & "')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim PKAudit As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)

            ' Audit SAR Memo
            strQuery = ""
            Dim objectdata As Object
            objectdata = objSARMemoClass.ObjSARMemo
            Dim objtype As Type = objectdata.GetType
            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
            For Each item As System.Reflection.PropertyInfo In properties
                Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                If Not item.GetValue(objectdata, Nothing) Is Nothing Then
                    If item.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                        objaudittraildetail.NewValue = item.GetValue(objectdata, Nothing)
                    Else
                        objaudittraildetail.NewValue = ""
                    End If
                Else
                    objaudittraildetail.NewValue = ""
                End If
                strQuery = "INSERT INTO AuditTrailDetail(  "
                strQuery += "FK_AuditTrailHeader_ID,"
                strQuery += "FieldName,"
                strQuery += "OldValue,"
                strQuery += "NewValue"
                strQuery += " ) VALUES ( "
                strQuery += " " & PKAudit & ""
                strQuery += " ,'" & item.Name & "'"
                strQuery += " , ''"
                strQuery += " , '" & objaudittraildetail.NewValue & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Next


            If objSARMemoClass.listIndikator IsNot Nothing AndAlso objSARMemoClass.listIndikator.Count > 0 Then
                For Each item In objSARMemoClass.listIndikator
                    strQuery = ""
                    Dim ObjectdataIndikator As Object
                    ObjectdataIndikator = item
                    Dim ObjtypeIndikator As Type = ObjectdataIndikator.GetType
                    Dim PropertiesIndikator() As System.Reflection.PropertyInfo = ObjtypeIndikator.GetProperties
                    For Each itemIndikator As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemIndikator.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemIndikator.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemIndikator.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemIndikator.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listPihakTerkait IsNot Nothing AndAlso objSARMemoClass.listPihakTerkait.Count > 0 Then
                For Each item In objSARMemoClass.listPihakTerkait
                    strQuery = ""
                    Dim ObjectdataPihakTerkait As Object
                    ObjectdataPihakTerkait = item
                    Dim ObjtypePihakTerkait As Type = ObjectdataPihakTerkait.GetType
                    Dim PropertiesPihakTerkait() As System.Reflection.PropertyInfo = ObjtypePihakTerkait.GetProperties
                    For Each itemPihakTerkait As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemPihakTerkait.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemPihakTerkait.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemPihakTerkait.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemPihakTerkait.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listTindakLanjut IsNot Nothing AndAlso objSARMemoClass.listTindakLanjut.Count > 0 Then
                For Each item In objSARMemoClass.listTindakLanjut
                    strQuery = ""
                    Dim ObjectdataTindakLanjut As Object
                    ObjectdataTindakLanjut = item
                    Dim ObjtypeTindakLanjut As Type = ObjectdataTindakLanjut.GetType
                    Dim PropertiesTindakLanjut() As System.Reflection.PropertyInfo = ObjtypeTindakLanjut.GetProperties
                    For Each itemTindakLanjut As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemTindakLanjut.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemTindakLanjut.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemTindakLanjut.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemTindakLanjut.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listCaseID IsNot Nothing AndAlso objSARMemoClass.listCaseID.Count > 0 Then
                For Each item In objSARMemoClass.listCaseID
                    strQuery = ""
                    Dim ObjectdataCaseID As Object
                    ObjectdataCaseID = item
                    Dim ObjtypeCaseID As Type = ObjectdataCaseID.GetType
                    Dim PropertiesCaseID() As System.Reflection.PropertyInfo = ObjtypeCaseID.GetProperties
                    For Each itemCaseID As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemCaseID.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemCaseID.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemCaseID.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemCaseID.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

            If objSARMemoClass.listAttachment IsNot Nothing AndAlso objSARMemoClass.listAttachment.Count > 0 Then
                For Each item In objSARMemoClass.listAttachment
                    strQuery = ""
                    Dim ObjectdataAttachment As Object
                    ObjectdataAttachment = item
                    Dim ObjtypeAttachment As Type = ObjectdataAttachment.GetType
                    Dim PropertiesAttachment() As System.Reflection.PropertyInfo = ObjtypeAttachment.GetProperties
                    For Each itemAttachment As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemAttachment.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemAttachment.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemAttachment.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemAttachment.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

            If objSARMemoClass.listActivity IsNot Nothing AndAlso objSARMemoClass.listActivity.Count > 0 Then
                For Each item In objSARMemoClass.listActivity
                    strQuery = ""
                    Dim ObjectdataActivity As Object
                    ObjectdataActivity = item
                    Dim ObjtypeActivity As Type = ObjectdataActivity.GetType
                    Dim PropertiesActivity() As System.Reflection.PropertyInfo = ObjtypeActivity.GetProperties
                    For Each itemActivity As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemActivity.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemActivity.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemActivity.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemActivity.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

        Catch ex As Exception

        End Try

    End Sub


    Public Shared Sub SaveEditTanpaApproval(objSARMemoClass As SARMemoClass, objModule As NawaDAL.Module)
        Dim objSARMemoClass_Old = getSARMemoClassByID(objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID)
        Dim jumlahrow As Integer = 0
        Try
            With objSARMemoClass.ObjSARMemo
                If String.IsNullOrEmpty(.LastUpdateBy) Then
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateDate = Now
                End If
                .StatusID = 1
                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                .ApprovedDate = Now
            End With
            Dim strQuery As String = ""
            strQuery = "UPDATE OneFCC_CaseManagement_SARMemo  "
            strQuery += "SET                                   "
            strQuery += "No                                     = '" & objSARMemoClass.ObjSARMemo.No & "'"
            strQuery += ",Kepada                                = '" & objSARMemoClass.ObjSARMemo.Kepada & "'"
            strQuery += ",Dari                                  = '" & objSARMemoClass.ObjSARMemo.Dari & "'"
            strQuery += ",Tanggal                               = '" & objSARMemoClass.ObjSARMemo.Tanggal & "'"
            strQuery += ",Perihal                               = '" & objSARMemoClass.ObjSARMemo.Perihal & "'"
            strQuery += ",Sumber_Pelaporan                      = '" & objSARMemoClass.ObjSARMemo.Sumber_Pelaporan.ToString & "'"
            strQuery += ",Tipologi                              = '" & objSARMemoClass.ObjSARMemo.Tipologi & "'"
            strQuery += ",Nama_Pihak_Terlapor                   = '" & objSARMemoClass.ObjSARMemo.Nama_Pihak_Terlapor & "'"
            strQuery += ",Kateri_Pihak_Terlapor                 = '" & objSARMemoClass.ObjSARMemo.Kateri_Pihak_Terlapor & "'"
            strQuery += ",Unit_Bisnis_Directorat                = '" & objSARMemoClass.ObjSARMemo.Unit_Bisnis_Directorat & "'"
            strQuery += ",Consumer_Banking                      = '" & objSARMemoClass.ObjSARMemo.Consumer_Banking & "'"
            strQuery += ",Lama_menjadi_Nasabah                  = '" & objSARMemoClass.ObjSARMemo.Lama_menjadi_Nasabah & "'"
            strQuery += ",Pekerjaan_Bidang_Usaha                = '" & objSARMemoClass.ObjSARMemo.Pekerjaan_Bidang_Usaha & "'"
            strQuery += ",Penghasilan_Tahun                     = '" & objSARMemoClass.ObjSARMemo.Penghasilan_Tahun & "'"
            strQuery += ",Profil_Lainnya                        = '" & objSARMemoClass.ObjSARMemo.Profil_Lainnya & "'"
            strQuery += ",Portofolio_Calon_Pihak_Terlapor       = '" & objSARMemoClass.ObjSARMemo.Portofolio_Calon_Pihak_Terlapor & "'"
            strQuery += ",Hasil_Analisis                        = '" & objSARMemoClass.ObjSARMemo.Hasil_Analisis & "'"
            strQuery += ",Kesimpulan                            = '" & objSARMemoClass.ObjSARMemo.Kesimpulan & "'"
            strQuery += ",Detail_Kesimpulan                     = '" & objSARMemoClass.ObjSARMemo.Detail_Kesimpulan & "'"
            strQuery += ",Tindak_lanjut                         = " & objSARMemoClass.ObjSARMemo.Tindak_lanjut & ""
            strQuery += ",StatusID                              = " & objSARMemoClass.ObjSARMemo.StatusID & ""
            strQuery += ",CIF                                   = '" & objSARMemoClass.ObjSARMemo.CIF & "'"
            strQuery += ",WIC                                   = '" & objSARMemoClass.ObjSARMemo.WIC & "'"
            strQuery += ",FK_CaseManagement_Workflow_SARMemo_ID = " & objSARMemoClass.ObjSARMemo.FK_CaseManagement_Workflow_SARMemo_ID & ""
            strQuery += ",Workflow_Step                         = " & objSARMemoClass.ObjSARMemo.Workflow_Step & ""
            strQuery += ",Direktorat                            = '" & objSARMemoClass.ObjSARMemo.Direktorat & "'"
            strQuery += ",Cabang                                = '" & objSARMemoClass.ObjSARMemo.Cabang & "'"
            strQuery += ",IsCustomer                            = '" & objSARMemoClass.ObjSARMemo.IsCustomer & "'"
            strQuery += ",Active                                = '" & objSARMemoClass.ObjSARMemo.Active & "'"
            strQuery += ",CreatedBy                             = '" & objSARMemoClass.ObjSARMemo.CreatedBy & "'"
            strQuery += ",LastUpdateBy                          = '" & objSARMemoClass.ObjSARMemo.LastUpdateBy & "'"
            strQuery += ",ApprovedBy                            = '" & objSARMemoClass.ObjSARMemo.ApprovedBy & "'"
            strQuery += ",CreatedDate                           = '" & objSARMemoClass.ObjSARMemo.CreatedDate & "'"
            strQuery += ",LastUpdateDate                        = '" & objSARMemoClass.ObjSARMemo.LastUpdateDate & "'"
            strQuery += ",ApprovedDate                          = '" & objSARMemoClass.ObjSARMemo.ApprovedDate & "'"
            strQuery += ",Jenis_Laporan                          = '" & objSARMemoClass.ObjSARMemo.Jenis_Laporan & "'"
            strQuery += ",Tanggal_Laporan                          = '" & objSARMemoClass.ObjSARMemo.Tanggal_Laporan & "'"
            strQuery += ",NoRefPPATK                          = '" & objSARMemoClass.ObjSARMemo.NoRefPPATK & "'"
            strQuery += ",Alasan                          = '" & objSARMemoClass.ObjSARMemo.Alasan & "'"
            strQuery += "from OneFCC_CaseManagement_SARMemo  where PK_OneFCC_CaseManagement_SARMemo_ID = " & objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            strQuery = ""

            Dim PKSARMemoLatest As Integer = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID
            'Update Indikator
            For Each item In objSARMemoClass.listIndikator
                If item.PK_OneFCC_CaseManagement_SARMemo_IndikatorID > 0 Then
                    strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_Indikator  "
                    strQuery += "SET                                   "
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID = " & PKSARMemoLatest & ""
                    strQuery += ",FK_Indicator                       = '" & item.FK_Indicator & "'"
                    strQuery += ",Active                             = '" & item.Active & "'"
                    strQuery += ",CreatedBy                          = '" & item.CreatedBy & "'"
                    strQuery += ",LastUpdateBy                       = '" & item.LastUpdateBy & "'"
                    strQuery += ",ApprovedBy                         = '" & item.ApprovedBy & "'"
                    strQuery += ",CreatedDate                        = '" & item.CreatedDate & "'"
                    strQuery += ",LastUpdateDate                     = '" & item.LastUpdateDate & "'"
                    strQuery += ",ApprovedDate                       = '" & item.ApprovedDate & "'"
                    strQuery += ",Alternateby                        = '" & item.Alternateby & "'"
                    strQuery += "from OneFCC_CaseManagement_SARMemo_Indikator  where PK_OneFCC_CaseManagement_SARMemo_Indikator_ID = " & item.PK_OneFCC_CaseManagement_SARMemo_IndikatorID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                Else
                    strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Indikator ( "
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                    strQuery += "FK_Indicator,"
                    strQuery += "Active,"
                    strQuery += "CreatedBy,"
                    strQuery += "LastUpdateBy,"
                    strQuery += "ApprovedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "LastUpdateDate,"
                    strQuery += "ApprovedDate,"
                    strQuery += "Alternateby"
                    strQuery += ") VALUES ( "
                    strQuery += " " & PKSARMemoLatest & ""
                    strQuery += " , " & item.FK_Indicator & ""
                    strQuery += " , '" & item.Active & "'"
                    strQuery += " , " & item.CreatedBy & "'"
                    strQuery += " , '" & item.LastUpdateBy & "'"
                    strQuery += " , '" & item.ApprovedBy & "'"
                    strQuery += " , '" & item.CreatedDate & "'"
                    strQuery += " , '" & item.LastUpdateDate & "'"
                    strQuery += " , '" & item.ApprovedDate & "'"
                    strQuery += " , '" & item.Alternateby & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                End If

            Next
            strQuery = ""


            'Update Pihak Terkait
            For Each item In objSARMemoClass.listPihakTerkait
                If item.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID > 0 Then
                    strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_Pihak_Terkait  "
                    strQuery += "SET                                   "
                    strQuery += "Nama_PihakTerkait						= '" & item.Nama_PihakTerkait & "'"
                    strQuery += ",Hubungan_PihakTerkait                =  '" & item.Hubungan_PihakTerkait & "'"
                    strQuery += ",Dilaporkan_PihakTerkait              =  '" & item.Dilaporkan_PihakTerkait & "'"
                    strQuery += ",RefGrips_PihakTerkait                =  '" & item.RefGrips_PihakTerkait & "'"
                    strQuery += ",FK_OneFCC_CaseManagement_SARMemo_ID  =  " & PKSARMemoLatest & ""
                    strQuery += ",CIF                                  =  '" & item.CIF & "'"
                    strQuery += ",Active                               =  '" & item.Active & "'"
                    strQuery += ",CreatedBy                            =  '" & item.CreatedBy & "'"
                    strQuery += ",LastUpdateBy                         =  '" & item.LastUpdateBy & "'"
                    strQuery += ",ApprovedBy                           =  '" & item.ApprovedBy & "'"
                    strQuery += ",CreatedDate                          =  '" & item.CreatedDate & "'"
                    strQuery += ",LastUpdateDate                       =  '" & item.LastUpdateDate & "'"
                    strQuery += ",ApprovedDate                         =  '" & item.ApprovedDate & "'"
                    strQuery += ",Alternateby                          =  '" & item.Alternateby & "'"
                    strQuery += "from OneFCC_CaseManagement_SARMemo_Pihak_Terkait  where PK_OneFCC_CaseManagement_SARMemo_Pihak_Terkait_ID = " & item.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                Else
                    strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Pihak_Terkait ( "
                    strQuery += "Nama_PihakTerkait,"
                    strQuery += "Hubungan_PihakTerkait,"
                    strQuery += "Dilaporkan_PihakTerkait,"
                    strQuery += "RefGrips_PihakTerkait,"
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                    strQuery += "CIF,"
                    strQuery += "Active,"
                    strQuery += "CreatedBy,"
                    strQuery += "LastUpdateBy,"
                    strQuery += "ApprovedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "LastUpdateDate,"
                    strQuery += "ApprovedDate,"
                    strQuery += "Alternateby"
                    strQuery += " ) VALUES ( "
                    strQuery += " '" & item.Nama_PihakTerkait & "'"
                    strQuery += " , '" & item.Hubungan_PihakTerkait & "'"
                    strQuery += " , '" & item.Dilaporkan_PihakTerkait & "'"
                    strQuery += " , '" & item.RefGrips_PihakTerkait & "'"
                    strQuery += " , " & PKSARMemoLatest & ""
                    strQuery += " , '" & item.CIF & "'"
                    strQuery += " , '" & item.Active & "'"
                    strQuery += " , '" & item.CreatedBy & "'"
                    strQuery += " , '" & item.LastUpdateBy & "'"
                    strQuery += " , '" & item.ApprovedBy & "'"
                    strQuery += " , '" & item.CreatedDate & "'"
                    strQuery += " , '" & item.LastUpdateDate & "'"
                    strQuery += " , '" & item.ApprovedDate & "'"
                    strQuery += " , '" & item.Alternateby & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)



                End If

            Next
            strQuery = ""

            'Update Tindak Lanjut
            For Each item In objSARMemoClass.listTindakLanjut
                If item.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID > 0 Then
                    strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_TindakLanjut  "
                    strQuery += "SET                                   "
                    strQuery += "TindakLanjutDescription					='" & item.TindakLanjutDescription & "'"
                    strQuery += ",FK_OneFCC_CaseManagement_TindakLanjut_ID = " & item.FK_OneFCC_CaseManagement_TindakLanjut_ID & ""
                    strQuery += ",FK_OneFCC_CaseManagement_SARMemo_ID      = " & PKSARMemoLatest & ""
                    strQuery += ",Active                                   = '" & item.Active & "'"
                    strQuery += ",CreatedBy                                = '" & item.CreatedBy & "'"
                    strQuery += ",LastUpdateBy                             = '" & item.LastUpdateBy & "'"
                    strQuery += ",ApprovedBy                               = '" & item.ApprovedBy & "'"
                    strQuery += ",CreatedDate                              = '" & item.CreatedDate & "'"
                    strQuery += ",LastUpdateDate                           = '" & item.LastUpdateDate & "'"
                    strQuery += ",ApprovedDate                             = '" & item.ApprovedDate & "'"
                    strQuery += ",Alternateby                              = '" & item.Alternateby & "'"
                    strQuery += "from OneFCC_CaseManagement_SARMemo_TindakLanjut  where PK_OneFCC_CaseManagement_SARMemo_TindakLanjut_ID = " & item.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                Else
                    strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_TindakLanjut ( "
                    strQuery += "TindakLanjutDescription,"
                    strQuery += "FK_OneFCC_CaseManagement_TindakLanjut_ID,"
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                    strQuery += "Active,"
                    strQuery += "CreatedBy,"
                    strQuery += "LastUpdateBy,"
                    strQuery += "ApprovedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "LastUpdateDate,"
                    strQuery += "ApprovedDate,"
                    strQuery += "Alternateby"
                    strQuery += " ) VALUES ( "
                    strQuery += " '" & item.TindakLanjutDescription & "'"
                    strQuery += " ," & item.FK_OneFCC_CaseManagement_TindakLanjut_ID & ""
                    strQuery += " ," & PKSARMemoLatest & ""
                    strQuery += " ,'" & item.Active & "'"
                    strQuery += " ,'" & item.CreatedBy & "'"
                    strQuery += " ,'" & item.LastUpdateBy & "'"
                    strQuery += " ,'" & item.ApprovedBy & "'"
                    strQuery += " ,'" & item.CreatedDate & "'"
                    strQuery += " ,'" & item.LastUpdateDate & "'"
                    strQuery += " ,'" & item.ApprovedDate & "'"
                    strQuery += " ,'" & item.Alternateby & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                End If

            Next
            strQuery = ""

            'Update Activity
            For Each item In objSARMemoClass.listActivity
                If item.Pk_OneFCC_CaseManagement_SARMemo_ActivityID > 0 Then
                    strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_Activity  "
                    strQuery += "SET                                   "
                    strQuery += "Account_NO		= '" & item.Account_NO & "'"
                    strQuery += ",Date_Activity    = '" & item.Date_Activity & "'"
                    strQuery += ",Significance     = '" & item.Significance & "'"
                    strQuery += ",Reason           = '" & item.Reason & "'"
                    strQuery += ",Comments         = '" & item.Comments & "'"
                    strQuery += ",Active           = '" & item.Active & "'"
                    strQuery += ",CreatedBy        = '" & item.CreatedBy & "'"
                    strQuery += ",LastUpdateBy     = '" & item.LastUpdateBy & "'"
                    strQuery += ",ApprovedBy       = '" & item.ApprovedBy & "'"
                    strQuery += ",CreatedDate      = '" & item.CreatedDate & "'"
                    strQuery += ",LastUpdateDate   = '" & item.LastUpdateDate & "'"
                    strQuery += ",ApprovedDate     = '" & item.ApprovedDate & "'"
                    strQuery += ",Alternateby      = '" & item.Alternateby & "'"
                    strQuery += ",FK_OneFCC_CaseManagement_SARMemo_ID      = " & PKSARMemoLatest & ""
                    strQuery += "from OneFCC_CaseManagement_SARMemo_Activity  where PK_OneFCC_CaseManagement_SARMemo_Activity_ID = " & item.Pk_OneFCC_CaseManagement_SARMemo_ActivityID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                Else
                    strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Activity ( "
                    strQuery += "Account_NO,"
                    strQuery += "Date_Activity,"
                    strQuery += "Significance,"
                    strQuery += "Reason,"
                    strQuery += "Comments,"
                    strQuery += "Active,"
                    strQuery += "CreatedBy,"
                    strQuery += "LastUpdateBy,"
                    strQuery += "ApprovedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "LastUpdateDate,"
                    strQuery += "ApprovedDate,"
                    strQuery += "Alternateby,"
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID"
                    strQuery += " ) VALUES ( "
                    strQuery += " '" & item.Account_NO & "'"
                    strQuery += " , '" & item.Date_Activity & "'"
                    strQuery += " , '" & item.Significance & "'"
                    strQuery += " , '" & item.Reason & "'"
                    strQuery += " , '" & item.Comments & "'"
                    strQuery += " ,'" & item.Active & "'"
                    strQuery += " , '" & item.CreatedBy & "'"
                    strQuery += " , '" & item.LastUpdateBy & "'"
                    strQuery += " , '" & item.ApprovedBy & "'"
                    strQuery += " , '" & item.CreatedDate & "'"
                    strQuery += " , '" & item.LastUpdateDate & "'"
                    strQuery += " , '" & item.ApprovedDate & "'"
                    strQuery += " , '" & item.Alternateby & "'"
                    strQuery += " , '" & PKSARMemoLatest & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                End If

            Next
            strQuery = ""

            ' Update Attachment
            If objSARMemoClass.listAttachment IsNot Nothing Then
                For Each item In objSARMemoClass.listAttachment
                    Dim objParam(0) As SqlParameter

                    objParam(0) = New SqlParameter
                    objParam(0).ParameterName = "@FileBinary"
                    objParam(0).Value = item.File_Doc
                    objParam(0).DbType = SqlDbType.Binary
                    If item.PK_OneFCC_CaseManagement_SARMemo_AttachmentID > 0 Then
                        strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_Attachment  "
                        strQuery += "SET                                   "
                        strQuery += "Fk_goAML_ODM_Generate_STR_SAR        = '" & item.Fk_goAML_ODM_Generate_STR_SAR & "'"
                        strQuery += ",File_Name                           = '" & item.File_Name & "'"
                        strQuery += ",File_Doc                            = CONVERT(varbinary(max),@FileBinary)"
                        strQuery += ",FK_OneFCC_CaseManagement_SARMemo_ID = " & PKSARMemoLatest & ""
                        strQuery += ",Remarks                             = '" & item.Remarks & "'"
                        strQuery += ",Active                              = '" & item.Active & "'"
                        strQuery += ",CreatedBy                           = '" & item.CreatedBy & "'"
                        strQuery += ",LastUpdateBy                        = '" & item.LastUpdateBy & "'"
                        strQuery += ",ApprovedBy                          = '" & item.ApprovedBy & "'"
                        strQuery += ",CreatedDate                         = '" & item.CreatedDate & "'"
                        strQuery += ",LastUpdateDate                      = '" & item.LastUpdateDate & "'"
                        strQuery += ",ApprovedDate                        = '" & item.ApprovedDate & "'"
                        strQuery += ",Alternateby                         = '" & item.Alternateby & "'"
                        strQuery += "from OneFCC_CaseManagement_SARMemo_Attachment  where PK_OneFCC_CaseManagement_SARMemo_Attachment_ID = " & item.PK_OneFCC_CaseManagement_SARMemo_AttachmentID
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, objParam)

                    Else



                        strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Attachment ( "
                        strQuery += "Fk_goAML_ODM_Generate_STR_SAR,"
                        strQuery += "File_Name,"
                        strQuery += "File_Doc,"
                        strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                        strQuery += "Remarks,"
                        strQuery += "Active,"
                        strQuery += "CreatedBy,"
                        strQuery += "LastUpdateBy,"
                        strQuery += "ApprovedBy,"
                        strQuery += "CreatedDate,"
                        strQuery += "LastUpdateDate,"
                        strQuery += "ApprovedDate,"
                        strQuery += "Alternateby"
                        strQuery += " ) VALUES ( "
                        strQuery += "'" & item.Fk_goAML_ODM_Generate_STR_SAR & "'"
                        strQuery += ",'" & item.File_Name & "'"
                        strQuery += ", CONVERT(varbinary(max),@FileBinary)"
                        strQuery += ",'" & PKSARMemoLatest & "'"
                        strQuery += ",'" & item.Remarks & "'"
                        strQuery += ",'" & item.Active & "'"
                        strQuery += ",'" & item.CreatedBy & "'"
                        strQuery += ",'" & item.LastUpdateBy & "'"
                        strQuery += ",'" & item.ApprovedBy & "'"
                        strQuery += ",'" & item.CreatedDate & "'"
                        strQuery += ",'" & item.LastUpdateDate & "'"
                        strQuery += ",'" & item.ApprovedDate & "'"
                        strQuery += ",'" & item.Alternateby & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, objParam)

                    End If

                Next
            End If
            strQuery = ""

            Dim listIndikatorOld As New List(Of OneFCC_CaseManagement_SARMemo_Indikator)
            Dim listIndikatorDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_SARMemo_Indikator where FK_OneFCC_CaseManagement_SARMemo_ID = " & objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID, Nothing)
            For Each item As DataRow In listIndikatorDT.Rows
                Dim Indikator As New OneFCC_CaseManagement_SARMemo_Indikator
                Indikator.PK_OneFCC_CaseManagement_SARMemo_IndikatorID = item("PK_OneFCC_CaseManagement_SARMemo_Indikator_ID")
                Indikator.FK_OneFCC_CaseManagement_SARMemoID = item("FK_OneFCC_CaseManagement_SARMemo_ID")
                Indikator.FK_Indicator = item("FK_Indicator")
                Indikator.Active = item("Active")
                Indikator.CreatedBy = item("CreatedBy")
                Indikator.LastUpdateBy = item("LastUpdateBy")
                Indikator.ApprovedBy = item("ApprovedBy")
                Indikator.CreatedDate = item("CreatedDate")
                Indikator.LastUpdateDate = item("LastUpdateDate")
                Indikator.ApprovedDate = item("ApprovedDate")
                Indikator.Alternateby = item("Alternateby")
                listIndikatorOld.Add(Indikator)
            Next
            For Each itemx As OneFCC_CaseManagement_SARMemo_Indikator In listIndikatorOld
                Dim objcek As OneFCC_CaseManagement_SARMemo_Indikator = objSARMemoClass.listIndikator.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_IndikatorID = itemx.PK_OneFCC_CaseManagement_SARMemo_IndikatorID)
                If objcek Is Nothing Then
                    strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Indikator		"
                    strQuery += "where pk_OneFCC_CaseManagement_SARMemo_Indikator_ID =  " & itemx.PK_OneFCC_CaseManagement_SARMemo_IndikatorID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                End If
            Next

            If objSARMemoClass_Old.listPihakTerkait.Count > 0 Then
                For Each itemx As OneFCC_CaseManagement_SARMemo_Pihak_Terkait In (From x In objSARMemoClass_Old.listPihakTerkait Where x.FK_OneFCC_CaseManagement_SARMemoID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID Select x).ToList
                    Dim objcek As OneFCC_CaseManagement_SARMemo_Pihak_Terkait = objSARMemoClass.listPihakTerkait.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID = itemx.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID)
                    If objcek Is Nothing Then
                        strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Pihak_Terkait		"
                        strQuery += "where PK_OneFCC_CaseManagement_SARMemo_Pihak_Terkait_ID =  " & itemx.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                    End If
                Next
            End If

            For Each itemx As OneFCC_CaseManagement_SARMemo_TindakLanjut In (From x In objSARMemoClass_Old.listTindakLanjut Where x.FK_OneFCC_CaseManagement_SARMemoID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID Select x).ToList
                Dim objcek As OneFCC_CaseManagement_SARMemo_TindakLanjut = objSARMemoClass.listTindakLanjut.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID = itemx.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID)
                If objcek Is Nothing Then
                    strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_TindakLanjut		"
                    strQuery += "where PK_OneFCC_CaseManagement_SARMemo_TindakLanjut_ID =  " & itemx.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                End If
            Next
            For Each itemx As OneFCC_CaseManagement_SARMemo_Activity In (From x In objSARMemoClass_Old.listActivity Where x.FK_OneFCC_CaseManagement_SARMemoID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID Select x).ToList
                Dim objcek As OneFCC_CaseManagement_SARMemo_Activity = objSARMemoClass.listActivity.Find(Function(x) x.Pk_OneFCC_CaseManagement_SARMemo_ActivityID = itemx.Pk_OneFCC_CaseManagement_SARMemo_ActivityID)
                If objcek Is Nothing Then
                    strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Activity		"
                    strQuery += "where PK_OneFCC_CaseManagement_SARMemo_Activity_ID =  " & itemx.Pk_OneFCC_CaseManagement_SARMemo_ActivityID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                End If
            Next
            For Each itemx As OneFCC_CaseManagement_SARMemo_Attachment In (From x In objSARMemoClass_Old.listAttachment Where x.FK_OneFCC_CaseManagement_SARMemoID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID Select x).ToList
                Dim objcek As OneFCC_CaseManagement_SARMemo_Attachment = objSARMemoClass.listAttachment.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = itemx.PK_OneFCC_CaseManagement_SARMemo_AttachmentID)
                If objcek Is Nothing Then
                    strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Attachment		"
                    strQuery += "where PK_OneFCC_CaseManagement_SARMemo_Attachment_ID =  " & itemx.PK_OneFCC_CaseManagement_SARMemo_AttachmentID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                End If
            Next

            ' Audit Trail
            strQuery = "INSERT INTO AuditTrailHeader ( "
            strQuery += "ApproveBy,"
            strQuery += "CreatedBy,"
            strQuery += "CreatedDate,"
            strQuery += "FK_AuditTrailStatus_ID,"
            strQuery += "FK_ModuleAction_ID,"
            strQuery += "ModuleLabel"
            strQuery += " ) VALUES ( "
            strQuery += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            strQuery += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
            strQuery += " , '" & NawaBLL.Common.ModuleActionEnum.Insert & "'"
            strQuery += " , '" & objModule.ModuleLabel & "')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Dim PKAudit As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)

            ' Audit SAR Memo
            strQuery = ""
            Dim objectdata As Object
            objectdata = objSARMemoClass.ObjSARMemo
            Dim objtype As Type = objectdata.GetType
            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
            For Each item As System.Reflection.PropertyInfo In properties
                Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                If Not item.GetValue(objectdata, Nothing) Is Nothing Then
                    If item.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                        objaudittraildetail.NewValue = item.GetValue(objectdata, Nothing)
                    Else
                        objaudittraildetail.NewValue = ""
                    End If
                Else
                    objaudittraildetail.NewValue = ""
                End If
                strQuery = "INSERT INTO AuditTrailDetail(  "
                strQuery += "FK_AuditTrailHeader_ID,"
                strQuery += "FieldName,"
                strQuery += "OldValue,"
                strQuery += "NewValue"
                strQuery += " ) VALUES ( "
                strQuery += " " & PKAudit & ""
                strQuery += " ,'" & item.Name & "'"
                strQuery += " , ''"
                strQuery += " , '" & objaudittraildetail.NewValue & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Next


            If objSARMemoClass.listIndikator IsNot Nothing AndAlso objSARMemoClass.listIndikator.Count > 0 Then
                For Each item In objSARMemoClass.listIndikator
                    strQuery = ""
                    Dim ObjectdataIndikator As Object
                    ObjectdataIndikator = item
                    Dim ObjtypeIndikator As Type = ObjectdataIndikator.GetType
                    Dim PropertiesIndikator() As System.Reflection.PropertyInfo = ObjtypeIndikator.GetProperties
                    For Each itemIndikator As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemIndikator.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemIndikator.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemIndikator.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemIndikator.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listPihakTerkait IsNot Nothing AndAlso objSARMemoClass.listPihakTerkait.Count > 0 Then
                For Each item In objSARMemoClass.listPihakTerkait
                    strQuery = ""
                    Dim ObjectdataPihakTerkait As Object
                    ObjectdataPihakTerkait = item
                    Dim ObjtypePihakTerkait As Type = ObjectdataPihakTerkait.GetType
                    Dim PropertiesPihakTerkait() As System.Reflection.PropertyInfo = ObjtypePihakTerkait.GetProperties
                    For Each itemPihakTerkait As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemPihakTerkait.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemPihakTerkait.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemPihakTerkait.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemPihakTerkait.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listTindakLanjut IsNot Nothing AndAlso objSARMemoClass.listTindakLanjut.Count > 0 Then
                For Each item In objSARMemoClass.listTindakLanjut
                    strQuery = ""
                    Dim ObjectdataTindakLanjut As Object
                    ObjectdataTindakLanjut = item
                    Dim ObjtypeTindakLanjut As Type = ObjectdataTindakLanjut.GetType
                    Dim PropertiesTindakLanjut() As System.Reflection.PropertyInfo = ObjtypeTindakLanjut.GetProperties
                    For Each itemTindakLanjut As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemTindakLanjut.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemTindakLanjut.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemTindakLanjut.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemTindakLanjut.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listCaseID IsNot Nothing AndAlso objSARMemoClass.listCaseID.Count > 0 Then
                For Each item In objSARMemoClass.listCaseID
                    strQuery = ""
                    Dim ObjectdataCaseID As Object
                    ObjectdataCaseID = item
                    Dim ObjtypeCaseID As Type = ObjectdataCaseID.GetType
                    Dim PropertiesCaseID() As System.Reflection.PropertyInfo = ObjtypeCaseID.GetProperties
                    For Each itemCaseID As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemCaseID.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemCaseID.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemCaseID.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemCaseID.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

            If objSARMemoClass.listAttachment IsNot Nothing AndAlso objSARMemoClass.listAttachment.Count > 0 Then
                For Each item In objSARMemoClass.listAttachment
                    strQuery = ""
                    Dim ObjectdataAttachment As Object
                    ObjectdataAttachment = item
                    Dim ObjtypeAttachment As Type = ObjectdataAttachment.GetType
                    Dim PropertiesAttachment() As System.Reflection.PropertyInfo = ObjtypeAttachment.GetProperties
                    For Each itemAttachment As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemAttachment.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemAttachment.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemAttachment.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemAttachment.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

            If objSARMemoClass.listActivity IsNot Nothing AndAlso objSARMemoClass.listActivity.Count > 0 Then
                For Each item In objSARMemoClass.listActivity
                    strQuery = ""
                    Dim ObjectdataActivity As Object
                    ObjectdataActivity = item
                    Dim ObjtypeActivity As Type = ObjectdataActivity.GetType
                    Dim PropertiesActivity() As System.Reflection.PropertyInfo = ObjtypeActivity.GetProperties
                    For Each itemActivity As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemActivity.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemActivity.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemActivity.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemActivity.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

            Dim WorkHistory As New OneFCC_CaseManagement_WorkflowHistory_SARMemo
            With WorkHistory
                .FK_OneFCC_CaseManagement_SARMemo_ID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID
                .Workflow_Step = 1
                .UserID = NawaBLL.Common.SessionCurrentUser.UserID
                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                .CreatedDate = Now
            End With
            strQuery = ""
            strQuery = "INSERT INTO OneFCC_CaseManagement_WorkflowHistory_SARMemo( "
            strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
            strQuery += "Workflow_Step,"
            strQuery += "UserID,"
            strQuery += "CreatedBy,"
            strQuery += "CreatedDate"
            strQuery += " ) VALUES ( "
            strQuery += " " & PKSARMemoLatest & ""
            strQuery += " ," & WorkHistory.Workflow_Step & ""
            strQuery += " , '" & WorkHistory.UserID & "'"
            strQuery += " , '" & WorkHistory.CreatedBy & "'"
            strQuery += " , '" & WorkHistory.CreatedDate & "')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            strQuery = ""



        Catch ex As Exception

        End Try


    End Sub

    Public Shared Sub Approve(objSARMemoClass As SARMemoClass, objModule As NawaDAL.Module)
        Dim objSARMemoClass_Old = getSARMemoClassByID(objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID)
        Dim jumlahrow As Integer = 0
        Try
            With objSARMemoClass.ObjSARMemo
                If String.IsNullOrEmpty(.LastUpdateBy) Then
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateDate = Now
                End If
                '.StatusID = 1
                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                .ApprovedDate = Now
            End With
            Dim strQuery As String = ""
            strQuery = "UPDATE OneFCC_CaseManagement_SARMemo  "
            strQuery += "SET                                   "
            strQuery += "No                                     = '" & objSARMemoClass.ObjSARMemo.No & "'"
            strQuery += ",Kepada                                = '" & objSARMemoClass.ObjSARMemo.Kepada & "'"
            strQuery += ",Dari                                  = '" & objSARMemoClass.ObjSARMemo.Dari & "'"
            strQuery += ",Tanggal                               = '" & objSARMemoClass.ObjSARMemo.Tanggal & "'"
            strQuery += ",Perihal                               = '" & objSARMemoClass.ObjSARMemo.Perihal & "'"
            strQuery += ",Sumber_Pelaporan                      = '" & objSARMemoClass.ObjSARMemo.Sumber_Pelaporan.ToString & "'"
            strQuery += ",Tipologi                              = '" & objSARMemoClass.ObjSARMemo.Tipologi & "'"
            strQuery += ",Nama_Pihak_Terlapor                   = '" & objSARMemoClass.ObjSARMemo.Nama_Pihak_Terlapor & "'"
            strQuery += ",Kateri_Pihak_Terlapor                 = '" & objSARMemoClass.ObjSARMemo.Kateri_Pihak_Terlapor & "'"
            strQuery += ",Unit_Bisnis_Directorat                = '" & objSARMemoClass.ObjSARMemo.Unit_Bisnis_Directorat & "'"
            strQuery += ",Consumer_Banking                      = '" & objSARMemoClass.ObjSARMemo.Consumer_Banking & "'"
            strQuery += ",Lama_menjadi_Nasabah                  = '" & objSARMemoClass.ObjSARMemo.Lama_menjadi_Nasabah & "'"
            strQuery += ",Pekerjaan_Bidang_Usaha                = '" & objSARMemoClass.ObjSARMemo.Pekerjaan_Bidang_Usaha & "'"
            strQuery += ",Penghasilan_Tahun                     = '" & objSARMemoClass.ObjSARMemo.Penghasilan_Tahun & "'"
            strQuery += ",Profil_Lainnya                        = '" & objSARMemoClass.ObjSARMemo.Profil_Lainnya & "'"
            strQuery += ",Portofolio_Calon_Pihak_Terlapor       = '" & objSARMemoClass.ObjSARMemo.Portofolio_Calon_Pihak_Terlapor & "'"
            strQuery += ",Hasil_Analisis                        = '" & objSARMemoClass.ObjSARMemo.Hasil_Analisis & "'"
            strQuery += ",Kesimpulan                            = '" & objSARMemoClass.ObjSARMemo.Kesimpulan & "'"
            strQuery += ",Detail_Kesimpulan                     = '" & objSARMemoClass.ObjSARMemo.Detail_Kesimpulan & "'"
            strQuery += ",Tindak_lanjut                         = " & objSARMemoClass.ObjSARMemo.Tindak_lanjut & ""
            strQuery += ",StatusID                              = " & objSARMemoClass.ObjSARMemo.StatusID & ""
            strQuery += ",CIF                                   = '" & objSARMemoClass.ObjSARMemo.CIF & "'"
            strQuery += ",WIC                                   = '" & objSARMemoClass.ObjSARMemo.WIC & "'"
            strQuery += ",FK_CaseManagement_Workflow_SARMemo_ID = " & objSARMemoClass.ObjSARMemo.FK_CaseManagement_Workflow_SARMemo_ID & ""
            strQuery += ",Workflow_Step                         = " & objSARMemoClass.ObjSARMemo.Workflow_Step & ""
            strQuery += ",Direktorat                            = '" & objSARMemoClass.ObjSARMemo.Direktorat & "'"
            strQuery += ",Cabang                                = '" & objSARMemoClass.ObjSARMemo.Cabang & "'"
            strQuery += ",IsCustomer                            = '" & objSARMemoClass.ObjSARMemo.IsCustomer & "'"
            strQuery += ",Active                                = '" & objSARMemoClass.ObjSARMemo.Active & "'"
            strQuery += ",CreatedBy                             = '" & objSARMemoClass.ObjSARMemo.CreatedBy & "'"
            strQuery += ",LastUpdateBy                          = '" & objSARMemoClass.ObjSARMemo.LastUpdateBy & "'"
            strQuery += ",ApprovedBy                            = '" & objSARMemoClass.ObjSARMemo.ApprovedBy & "'"
            strQuery += ",CreatedDate                           = '" & objSARMemoClass.ObjSARMemo.CreatedDate & "'"
            strQuery += ",LastUpdateDate                        = '" & objSARMemoClass.ObjSARMemo.LastUpdateDate & "'"
            strQuery += ",ApprovedDate                          = '" & objSARMemoClass.ObjSARMemo.ApprovedDate & "'"
            strQuery += ",Jenis_Laporan                          = '" & objSARMemoClass.ObjSARMemo.Jenis_Laporan & "'"
            strQuery += ",Tanggal_Laporan                          = '" & objSARMemoClass.ObjSARMemo.Tanggal_Laporan & "'"
            strQuery += ",NoRefPPATK                          = '" & objSARMemoClass.ObjSARMemo.NoRefPPATK & "'"
            strQuery += ",Alasan                          = '" & objSARMemoClass.ObjSARMemo.Alasan & "'"
            strQuery += "from OneFCC_CaseManagement_SARMemo  where PK_OneFCC_CaseManagement_SARMemo_ID = " & objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            strQuery = ""

            Dim PKSARMemoLatest As Integer = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID
            'Update Indikator
            For Each item In objSARMemoClass.listIndikator
                If item.PK_OneFCC_CaseManagement_SARMemo_IndikatorID > 0 Then
                    strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_Indikator  "
                    strQuery += "SET                                   "
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID = " & PKSARMemoLatest & ""
                    strQuery += ",FK_Indicator                       = '" & item.FK_Indicator & "'"
                    strQuery += ",Active                             = '" & item.Active & "'"
                    strQuery += ",CreatedBy                          = '" & item.CreatedBy & "'"
                    strQuery += ",LastUpdateBy                       = '" & item.LastUpdateBy & "'"
                    strQuery += ",ApprovedBy                         = '" & item.ApprovedBy & "'"
                    strQuery += ",CreatedDate                        = '" & item.CreatedDate & "'"
                    strQuery += ",LastUpdateDate                     = '" & item.LastUpdateDate & "'"
                    strQuery += ",ApprovedDate                       = '" & item.ApprovedDate & "'"
                    strQuery += ",Alternateby                        = '" & item.Alternateby & "'"
                    strQuery += "from OneFCC_CaseManagement_SARMemo_Indikator  where PK_OneFCC_CaseManagement_SARMemo_Indikator_ID = " & item.PK_OneFCC_CaseManagement_SARMemo_IndikatorID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                Else
                    strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Indikator ( "
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                    strQuery += "FK_Indicator,"
                    strQuery += "Active,"
                    strQuery += "CreatedBy,"
                    strQuery += "LastUpdateBy,"
                    strQuery += "ApprovedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "LastUpdateDate,"
                    strQuery += "ApprovedDate,"
                    strQuery += "Alternateby"
                    strQuery += ") VALUES ( "
                    strQuery += " " & PKSARMemoLatest & ""
                    strQuery += " , " & item.FK_Indicator & ""
                    strQuery += " , '" & item.Active & "'"
                    strQuery += " , " & item.CreatedBy & "'"
                    strQuery += " , '" & item.LastUpdateBy & "'"
                    strQuery += " , '" & item.ApprovedBy & "'"
                    strQuery += " , '" & item.CreatedDate & "'"
                    strQuery += " , '" & item.LastUpdateDate & "'"
                    strQuery += " , '" & item.ApprovedDate & "'"
                    strQuery += " , '" & item.Alternateby & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                End If

            Next
            strQuery = ""


            'Update Pihak Terkait
            For Each item In objSARMemoClass.listPihakTerkait
                If item.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID > 0 Then
                    strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_Pihak_Terkait  "
                    strQuery += "SET                                   "
                    strQuery += "Nama_PihakTerkait						= '" & item.Nama_PihakTerkait & "'"
                    strQuery += ",Hubungan_PihakTerkait                =  '" & item.Hubungan_PihakTerkait & "'"
                    strQuery += ",Dilaporkan_PihakTerkait              =  '" & item.Dilaporkan_PihakTerkait & "'"
                    strQuery += ",RefGrips_PihakTerkait                =  '" & item.RefGrips_PihakTerkait & "'"
                    strQuery += ",FK_OneFCC_CaseManagement_SARMemo_ID  =  " & PKSARMemoLatest & ""
                    strQuery += ",CIF                                  =  '" & item.CIF & "'"
                    strQuery += ",Active                               =  '" & item.Active & "'"
                    strQuery += ",CreatedBy                            =  '" & item.CreatedBy & "'"
                    strQuery += ",LastUpdateBy                         =  '" & item.LastUpdateBy & "'"
                    strQuery += ",ApprovedBy                           =  '" & item.ApprovedBy & "'"
                    strQuery += ",CreatedDate                          =  '" & item.CreatedDate & "'"
                    strQuery += ",LastUpdateDate                       =  '" & item.LastUpdateDate & "'"
                    strQuery += ",ApprovedDate                         =  '" & item.ApprovedDate & "'"
                    strQuery += ",Alternateby                          =  '" & item.Alternateby & "'"
                    strQuery += "from OneFCC_CaseManagement_SARMemo_Pihak_Terkait  where PK_OneFCC_CaseManagement_SARMemo_Pihak_Terkait_ID = " & item.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                Else
                    strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Pihak_Terkait ( "
                    strQuery += "Nama_PihakTerkait,"
                    strQuery += "Hubungan_PihakTerkait,"
                    strQuery += "Dilaporkan_PihakTerkait,"
                    strQuery += "RefGrips_PihakTerkait,"
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                    strQuery += "CIF,"
                    strQuery += "Active,"
                    strQuery += "CreatedBy,"
                    strQuery += "LastUpdateBy,"
                    strQuery += "ApprovedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "LastUpdateDate,"
                    strQuery += "ApprovedDate,"
                    strQuery += "Alternateby"
                    strQuery += " ) VALUES ( "
                    strQuery += " '" & item.Nama_PihakTerkait & "'"
                    strQuery += " , '" & item.Hubungan_PihakTerkait & "'"
                    strQuery += " , '" & item.Dilaporkan_PihakTerkait & "'"
                    strQuery += " , '" & item.RefGrips_PihakTerkait & "'"
                    strQuery += " , " & PKSARMemoLatest & ""
                    strQuery += " , '" & item.CIF & "'"
                    strQuery += " , '" & item.Active & "'"
                    strQuery += " , '" & item.CreatedBy & "'"
                    strQuery += " , '" & item.LastUpdateBy & "'"
                    strQuery += " , '" & item.ApprovedBy & "'"
                    strQuery += " , '" & item.CreatedDate & "'"
                    strQuery += " , '" & item.LastUpdateDate & "'"
                    strQuery += " , '" & item.ApprovedDate & "'"
                    strQuery += " , '" & item.Alternateby & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)



                End If

            Next
            strQuery = ""

            'Update Tindak Lanjut
            For Each item In objSARMemoClass.listTindakLanjut
                If item.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID > 0 Then
                    strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_TindakLanjut  "
                    strQuery += "SET                                   "
                    strQuery += "TindakLanjutDescription					='" & item.TindakLanjutDescription & "'"
                    strQuery += ",FK_OneFCC_CaseManagement_TindakLanjut_ID = " & item.FK_OneFCC_CaseManagement_TindakLanjut_ID & ""
                    strQuery += ",FK_OneFCC_CaseManagement_SARMemo_ID      = " & PKSARMemoLatest & ""
                    strQuery += ",Active                                   = '" & item.Active & "'"
                    strQuery += ",CreatedBy                                = '" & item.CreatedBy & "'"
                    strQuery += ",LastUpdateBy                             = '" & item.LastUpdateBy & "'"
                    strQuery += ",ApprovedBy                               = '" & item.ApprovedBy & "'"
                    strQuery += ",CreatedDate                              = '" & item.CreatedDate & "'"
                    strQuery += ",LastUpdateDate                           = '" & item.LastUpdateDate & "'"
                    strQuery += ",ApprovedDate                             = '" & item.ApprovedDate & "'"
                    strQuery += ",Alternateby                              = '" & item.Alternateby & "'"
                    strQuery += "from OneFCC_CaseManagement_SARMemo_TindakLanjut  where PK_OneFCC_CaseManagement_SARMemo_TindakLanjut_ID = " & item.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                Else
                    strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_TindakLanjut ( "
                    strQuery += "TindakLanjutDescription,"
                    strQuery += "FK_OneFCC_CaseManagement_TindakLanjut_ID,"
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                    strQuery += "Active,"
                    strQuery += "CreatedBy,"
                    strQuery += "LastUpdateBy,"
                    strQuery += "ApprovedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "LastUpdateDate,"
                    strQuery += "ApprovedDate,"
                    strQuery += "Alternateby"
                    strQuery += " ) VALUES ( "
                    strQuery += " '" & item.TindakLanjutDescription & "'"
                    strQuery += " ," & item.FK_OneFCC_CaseManagement_TindakLanjut_ID & ""
                    strQuery += " ," & PKSARMemoLatest & ""
                    strQuery += " ,'" & item.Active & "'"
                    strQuery += " ,'" & item.CreatedBy & "'"
                    strQuery += " ,'" & item.LastUpdateBy & "'"
                    strQuery += " ,'" & item.ApprovedBy & "'"
                    strQuery += " ,'" & item.CreatedDate & "'"
                    strQuery += " ,'" & item.LastUpdateDate & "'"
                    strQuery += " ,'" & item.ApprovedDate & "'"
                    strQuery += " ,'" & item.Alternateby & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                End If

            Next
            strQuery = ""

            'Update Activity
            For Each item In objSARMemoClass.listActivity
                If item.Pk_OneFCC_CaseManagement_SARMemo_ActivityID > 0 Then
                    strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_Activity  "
                    strQuery += "SET                                   "
                    strQuery += "Account_NO		= '" & item.Account_NO & "'"
                    strQuery += ",Date_Activity    = '" & item.Date_Activity & "'"
                    strQuery += ",Significance     = '" & item.Significance & "'"
                    strQuery += ",Reason           = '" & item.Reason & "'"
                    strQuery += ",Comments         = '" & item.Comments & "'"
                    strQuery += ",Active           = '" & item.Active & "'"
                    strQuery += ",CreatedBy        = '" & item.CreatedBy & "'"
                    strQuery += ",LastUpdateBy     = '" & item.LastUpdateBy & "'"
                    strQuery += ",ApprovedBy       = '" & item.ApprovedBy & "'"
                    strQuery += ",CreatedDate      = '" & item.CreatedDate & "'"
                    strQuery += ",LastUpdateDate   = '" & item.LastUpdateDate & "'"
                    strQuery += ",ApprovedDate     = '" & item.ApprovedDate & "'"
                    strQuery += ",Alternateby      = '" & item.Alternateby & "'"
                    strQuery += ",FK_OneFCC_CaseManagement_SARMemo_ID      = " & PKSARMemoLatest & ""
                    strQuery += "from OneFCC_CaseManagement_SARMemo_Activity  where PK_OneFCC_CaseManagement_SARMemo_Activity_ID = " & item.Pk_OneFCC_CaseManagement_SARMemo_ActivityID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                Else
                    strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Activity ( "
                    strQuery += "Account_NO,"
                    strQuery += "Date_Activity,"
                    strQuery += "Significance,"
                    strQuery += "Reason,"
                    strQuery += "Comments,"
                    strQuery += "Active,"
                    strQuery += "CreatedBy,"
                    strQuery += "LastUpdateBy,"
                    strQuery += "ApprovedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "LastUpdateDate,"
                    strQuery += "ApprovedDate,"
                    strQuery += "Alternateby,"
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID"
                    strQuery += " ) VALUES ( "
                    strQuery += " '" & item.Account_NO & "'"
                    strQuery += " , '" & item.Date_Activity & "'"
                    strQuery += " , '" & item.Significance & "'"
                    strQuery += " , '" & item.Reason & "'"
                    strQuery += " , '" & item.Comments & "'"
                    strQuery += " ,'" & item.Active & "'"
                    strQuery += " , '" & item.CreatedBy & "'"
                    strQuery += " , '" & item.LastUpdateBy & "'"
                    strQuery += " , '" & item.ApprovedBy & "'"
                    strQuery += " , '" & item.CreatedDate & "'"
                    strQuery += " , '" & item.LastUpdateDate & "'"
                    strQuery += " , '" & item.ApprovedDate & "'"
                    strQuery += " , '" & item.Alternateby & "'"
                    strQuery += " , '" & PKSARMemoLatest & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                End If

            Next
            strQuery = ""

            ' Update Attachment
            If objSARMemoClass.listAttachment IsNot Nothing Then
                For Each item In objSARMemoClass.listAttachment
                    Dim objParam(0) As SqlParameter

                    objParam(0) = New SqlParameter
                    objParam(0).ParameterName = "@FileBinary"
                    objParam(0).Value = item.File_Doc
                    objParam(0).DbType = SqlDbType.Binary
                    If item.PK_OneFCC_CaseManagement_SARMemo_AttachmentID > 0 Then
                        strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_Attachment  "
                        strQuery += "SET                                   "
                        strQuery += "Fk_goAML_ODM_Generate_STR_SAR        = '" & item.Fk_goAML_ODM_Generate_STR_SAR & "'"
                        strQuery += ",File_Name                           = '" & item.File_Name & "'"
                        strQuery += ",File_Doc                            = CONVERT(varbinary(max),@FileBinary)"
                        strQuery += ",FK_OneFCC_CaseManagement_SARMemo_ID = " & PKSARMemoLatest & ""
                        strQuery += ",Remarks                             = '" & item.Remarks & "'"
                        strQuery += ",Active                              = '" & item.Active & "'"
                        strQuery += ",CreatedBy                           = '" & item.CreatedBy & "'"
                        strQuery += ",LastUpdateBy                        = '" & item.LastUpdateBy & "'"
                        strQuery += ",ApprovedBy                          = '" & item.ApprovedBy & "'"
                        strQuery += ",CreatedDate                         = '" & item.CreatedDate & "'"
                        strQuery += ",LastUpdateDate                      = '" & item.LastUpdateDate & "'"
                        strQuery += ",ApprovedDate                        = '" & item.ApprovedDate & "'"
                        strQuery += ",Alternateby                         = '" & item.Alternateby & "'"
                        strQuery += "from OneFCC_CaseManagement_SARMemo_Attachment  where PK_OneFCC_CaseManagement_SARMemo_Attachment_ID = " & item.PK_OneFCC_CaseManagement_SARMemo_AttachmentID
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, objParam)

                    Else



                        strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Attachment ( "
                        strQuery += "Fk_goAML_ODM_Generate_STR_SAR,"
                        strQuery += "File_Name,"
                        strQuery += "File_Doc,"
                        strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                        strQuery += "Remarks,"
                        strQuery += "Active,"
                        strQuery += "CreatedBy,"
                        strQuery += "LastUpdateBy,"
                        strQuery += "ApprovedBy,"
                        strQuery += "CreatedDate,"
                        strQuery += "LastUpdateDate,"
                        strQuery += "ApprovedDate,"
                        strQuery += "Alternateby"
                        strQuery += " ) VALUES ( "
                        strQuery += "'" & item.Fk_goAML_ODM_Generate_STR_SAR & "'"
                        strQuery += ",'" & item.File_Name & "'"
                        strQuery += ", CONVERT(varbinary(max),@FileBinary)"
                        strQuery += ",'" & PKSARMemoLatest & "'"
                        strQuery += ",'" & item.Remarks & "'"
                        strQuery += ",'" & item.Active & "'"
                        strQuery += ",'" & item.CreatedBy & "'"
                        strQuery += ",'" & item.LastUpdateBy & "'"
                        strQuery += ",'" & item.ApprovedBy & "'"
                        strQuery += ",'" & item.CreatedDate & "'"
                        strQuery += ",'" & item.LastUpdateDate & "'"
                        strQuery += ",'" & item.ApprovedDate & "'"
                        strQuery += ",'" & item.Alternateby & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, objParam)

                    End If

                Next
            End If
            strQuery = ""

            Dim listIndikatorOld As New List(Of OneFCC_CaseManagement_SARMemo_Indikator)
            Dim listIndikatorDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_SARMemo_Indikator where FK_OneFCC_CaseManagement_SARMemo_ID = " & objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID, Nothing)
            For Each item As DataRow In listIndikatorDT.Rows
                Dim Indikator As New OneFCC_CaseManagement_SARMemo_Indikator
                Indikator.PK_OneFCC_CaseManagement_SARMemo_IndikatorID = item("PK_OneFCC_CaseManagement_SARMemo_Indikator_ID")
                Indikator.FK_OneFCC_CaseManagement_SARMemoID = item("FK_OneFCC_CaseManagement_SARMemo_ID")
                Indikator.FK_Indicator = item("FK_Indicator")
                Indikator.Active = item("Active")
                Indikator.CreatedBy = item("CreatedBy")
                Indikator.LastUpdateBy = item("LastUpdateBy")
                Indikator.ApprovedBy = item("ApprovedBy")
                Indikator.CreatedDate = item("CreatedDate")
                Indikator.LastUpdateDate = item("LastUpdateDate")
                Indikator.ApprovedDate = item("ApprovedDate")
                Indikator.Alternateby = item("Alternateby")
                listIndikatorOld.Add(Indikator)
            Next
            For Each itemx As OneFCC_CaseManagement_SARMemo_Indikator In listIndikatorOld
                Dim objcek As OneFCC_CaseManagement_SARMemo_Indikator = objSARMemoClass.listIndikator.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_IndikatorID = itemx.PK_OneFCC_CaseManagement_SARMemo_IndikatorID)
                If objcek Is Nothing Then
                    strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Indikator		"
                    strQuery += "where pk_OneFCC_CaseManagement_SARMemo_Indikator_ID =  " & itemx.PK_OneFCC_CaseManagement_SARMemo_IndikatorID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                End If
            Next

            If objSARMemoClass_Old.listPihakTerkait.Count > 0 Then
                For Each itemx As OneFCC_CaseManagement_SARMemo_Pihak_Terkait In (From x In objSARMemoClass_Old.listPihakTerkait Where x.FK_OneFCC_CaseManagement_SARMemoID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID Select x).ToList
                    Dim objcek As OneFCC_CaseManagement_SARMemo_Pihak_Terkait = objSARMemoClass.listPihakTerkait.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID = itemx.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID)
                    If objcek Is Nothing Then
                        strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Pihak_Terkait		"
                        strQuery += "where PK_OneFCC_CaseManagement_SARMemo_Pihak_Terkait_ID =  " & itemx.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                    End If
                Next
            End If
            For Each itemx As OneFCC_CaseManagement_SARMemo_TindakLanjut In (From x In objSARMemoClass_Old.listTindakLanjut Where x.FK_OneFCC_CaseManagement_SARMemoID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID Select x).ToList
                Dim objcek As OneFCC_CaseManagement_SARMemo_TindakLanjut = objSARMemoClass.listTindakLanjut.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID = itemx.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID)
                If objcek Is Nothing Then
                    strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_TindakLanjut		"
                    strQuery += "where PK_OneFCC_CaseManagement_SARMemo_TindakLanjut_ID =  " & itemx.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                End If
            Next
            For Each itemx As OneFCC_CaseManagement_SARMemo_Activity In (From x In objSARMemoClass_Old.listActivity Where x.FK_OneFCC_CaseManagement_SARMemoID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID Select x).ToList
                Dim objcek As OneFCC_CaseManagement_SARMemo_Activity = objSARMemoClass.listActivity.Find(Function(x) x.Pk_OneFCC_CaseManagement_SARMemo_ActivityID = itemx.Pk_OneFCC_CaseManagement_SARMemo_ActivityID)
                If objcek Is Nothing Then
                    strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Activity		"
                    strQuery += "where PK_OneFCC_CaseManagement_SARMemo_Activity_ID =  " & itemx.Pk_OneFCC_CaseManagement_SARMemo_ActivityID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                End If
            Next
            For Each itemx As OneFCC_CaseManagement_SARMemo_Attachment In (From x In objSARMemoClass_Old.listAttachment Where x.FK_OneFCC_CaseManagement_SARMemoID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID Select x).ToList
                Dim objcek As OneFCC_CaseManagement_SARMemo_Attachment = objSARMemoClass.listAttachment.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = itemx.PK_OneFCC_CaseManagement_SARMemo_AttachmentID)
                If objcek Is Nothing Then
                    strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Attachment		"
                    strQuery += "where PK_OneFCC_CaseManagement_SARMemo_Attachment_ID =  " & itemx.PK_OneFCC_CaseManagement_SARMemo_AttachmentID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                End If
            Next

            ' Audit Trail
            strQuery = "INSERT INTO AuditTrailHeader ( "
            strQuery += "ApproveBy,"
            strQuery += "CreatedBy,"
            strQuery += "CreatedDate,"
            strQuery += "FK_AuditTrailStatus_ID,"
            strQuery += "FK_ModuleAction_ID,"
            strQuery += "ModuleLabel"
            strQuery += " ) VALUES ( "
            strQuery += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            strQuery += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
            strQuery += " , '" & NawaBLL.Common.ModuleActionEnum.Insert & "'"
            strQuery += " , '" & objModule.ModuleLabel & "')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Dim PKAudit As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)

            ' Audit SAR Memo
            strQuery = ""
            Dim objectdata As Object
            objectdata = objSARMemoClass.ObjSARMemo
            Dim objtype As Type = objectdata.GetType
            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
            For Each item As System.Reflection.PropertyInfo In properties
                Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                If Not item.GetValue(objectdata, Nothing) Is Nothing Then
                    If item.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                        objaudittraildetail.NewValue = item.GetValue(objectdata, Nothing)
                    Else
                        objaudittraildetail.NewValue = ""
                    End If
                Else
                    objaudittraildetail.NewValue = ""
                End If
                strQuery = "INSERT INTO AuditTrailDetail(  "
                strQuery += "FK_AuditTrailHeader_ID,"
                strQuery += "FieldName,"
                strQuery += "OldValue,"
                strQuery += "NewValue"
                strQuery += " ) VALUES ( "
                strQuery += " " & PKAudit & ""
                strQuery += " ,'" & item.Name & "'"
                strQuery += " , ''"
                strQuery += " , '" & objaudittraildetail.NewValue & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Next


            If objSARMemoClass.listIndikator IsNot Nothing AndAlso objSARMemoClass.listIndikator.Count > 0 Then
                For Each item In objSARMemoClass.listIndikator
                    strQuery = ""
                    Dim ObjectdataIndikator As Object
                    ObjectdataIndikator = item
                    Dim ObjtypeIndikator As Type = ObjectdataIndikator.GetType
                    Dim PropertiesIndikator() As System.Reflection.PropertyInfo = ObjtypeIndikator.GetProperties
                    For Each itemIndikator As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemIndikator.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemIndikator.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemIndikator.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemIndikator.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listPihakTerkait IsNot Nothing AndAlso objSARMemoClass.listPihakTerkait.Count > 0 Then
                For Each item In objSARMemoClass.listPihakTerkait
                    strQuery = ""
                    Dim ObjectdataPihakTerkait As Object
                    ObjectdataPihakTerkait = item
                    Dim ObjtypePihakTerkait As Type = ObjectdataPihakTerkait.GetType
                    Dim PropertiesPihakTerkait() As System.Reflection.PropertyInfo = ObjtypePihakTerkait.GetProperties
                    For Each itemPihakTerkait As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemPihakTerkait.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemPihakTerkait.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemPihakTerkait.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemPihakTerkait.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listTindakLanjut IsNot Nothing AndAlso objSARMemoClass.listTindakLanjut.Count > 0 Then
                For Each item In objSARMemoClass.listTindakLanjut
                    strQuery = ""
                    Dim ObjectdataTindakLanjut As Object
                    ObjectdataTindakLanjut = item
                    Dim ObjtypeTindakLanjut As Type = ObjectdataTindakLanjut.GetType
                    Dim PropertiesTindakLanjut() As System.Reflection.PropertyInfo = ObjtypeTindakLanjut.GetProperties
                    For Each itemTindakLanjut As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemTindakLanjut.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemTindakLanjut.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemTindakLanjut.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemTindakLanjut.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listCaseID IsNot Nothing AndAlso objSARMemoClass.listCaseID.Count > 0 Then
                For Each item In objSARMemoClass.listCaseID
                    strQuery = ""
                    Dim ObjectdataCaseID As Object
                    ObjectdataCaseID = item
                    Dim ObjtypeCaseID As Type = ObjectdataCaseID.GetType
                    Dim PropertiesCaseID() As System.Reflection.PropertyInfo = ObjtypeCaseID.GetProperties
                    For Each itemCaseID As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemCaseID.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemCaseID.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemCaseID.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemCaseID.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

            If objSARMemoClass.listAttachment IsNot Nothing AndAlso objSARMemoClass.listAttachment.Count > 0 Then
                For Each item In objSARMemoClass.listAttachment
                    strQuery = ""
                    Dim ObjectdataAttachment As Object
                    ObjectdataAttachment = item
                    Dim ObjtypeAttachment As Type = ObjectdataAttachment.GetType
                    Dim PropertiesAttachment() As System.Reflection.PropertyInfo = ObjtypeAttachment.GetProperties
                    For Each itemAttachment As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemAttachment.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemAttachment.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemAttachment.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemAttachment.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

            If objSARMemoClass.listActivity IsNot Nothing AndAlso objSARMemoClass.listActivity.Count > 0 Then
                For Each item In objSARMemoClass.listActivity
                    strQuery = ""
                    Dim ObjectdataActivity As Object
                    ObjectdataActivity = item
                    Dim ObjtypeActivity As Type = ObjectdataActivity.GetType
                    Dim PropertiesActivity() As System.Reflection.PropertyInfo = ObjtypeActivity.GetProperties
                    For Each itemActivity As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemActivity.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemActivity.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemActivity.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemActivity.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

            'Dim WorkHistory As New OneFCC_CaseManagement_WorkflowHistory_SARMemo
            'With WorkHistory
            '    .FK_OneFCC_CaseManagement_SARMemo_ID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID
            '    .Workflow_Step = 1
            '    .UserID = NawaBLL.Common.SessionCurrentUser.UserID
            '    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            '    .CreatedDate = Now
            'End With
            'strQuery = ""
            'strQuery = "INSERT INTO OneFCC_CaseManagement_WorkflowHistory_SARMemo( "
            'strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
            'strQuery += "Workflow_Step,"
            'strQuery += "UserID,"
            'strQuery += "CreatedBy,"
            'strQuery += "CreatedDate"
            'strQuery += " ) VALUES ( "
            'strQuery += " " & PKSARMemoLatest & ""
            'strQuery += " ," & WorkHistory.Workflow_Step & ""
            'strQuery += " , '" & WorkHistory.UserID & "'"
            'strQuery += " , '" & WorkHistory.CreatedBy & "'"
            'strQuery += " , '" & WorkHistory.CreatedDate & "')"
            'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            'strQuery = ""



        Catch ex As Exception

        End Try


    End Sub

    Public Shared Sub Reject(objSARMemoClass As SARMemoClass, objModule As NawaDAL.Module)
        Dim objSARMemoClass_Old = getSARMemoClassByID(objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID)
        Dim jumlahrow As Integer = 0
        Try
            With objSARMemoClass.ObjSARMemo
                If String.IsNullOrEmpty(.LastUpdateBy) Then
                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    .LastUpdateDate = Now
                End If
                '.StatusID = 1
                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                .ApprovedDate = Now
            End With
            Dim strQuery As String = ""
            strQuery = "UPDATE OneFCC_CaseManagement_SARMemo  "
            strQuery += "SET                                   "
            strQuery += "No                                     = '" & objSARMemoClass.ObjSARMemo.No & "'"
            strQuery += ",Kepada                                = '" & objSARMemoClass.ObjSARMemo.Kepada & "'"
            strQuery += ",Dari                                  = '" & objSARMemoClass.ObjSARMemo.Dari & "'"
            strQuery += ",Tanggal                               = '" & objSARMemoClass.ObjSARMemo.Tanggal & "'"
            strQuery += ",Perihal                               = '" & objSARMemoClass.ObjSARMemo.Perihal & "'"
            strQuery += ",Sumber_Pelaporan                      = '" & objSARMemoClass.ObjSARMemo.Sumber_Pelaporan & "'"
            strQuery += ",Tipologi                              = '" & objSARMemoClass.ObjSARMemo.Tipologi & "'"
            strQuery += ",Nama_Pihak_Terlapor                   = '" & objSARMemoClass.ObjSARMemo.Nama_Pihak_Terlapor & "'"
            strQuery += ",Kateri_Pihak_Terlapor                 = '" & objSARMemoClass.ObjSARMemo.Kateri_Pihak_Terlapor & "'"
            strQuery += ",Unit_Bisnis_Directorat                = '" & objSARMemoClass.ObjSARMemo.Unit_Bisnis_Directorat & "'"
            strQuery += ",Consumer_Banking                      = '" & objSARMemoClass.ObjSARMemo.Consumer_Banking & "'"
            strQuery += ",Lama_menjadi_Nasabah                  = '" & objSARMemoClass.ObjSARMemo.Lama_menjadi_Nasabah & "'"
            strQuery += ",Pekerjaan_Bidang_Usaha                = '" & objSARMemoClass.ObjSARMemo.Pekerjaan_Bidang_Usaha & "'"
            strQuery += ",Penghasilan_Tahun                     = '" & objSARMemoClass.ObjSARMemo.Penghasilan_Tahun & "'"
            strQuery += ",Profil_Lainnya                        = '" & objSARMemoClass.ObjSARMemo.Profil_Lainnya & "'"
            strQuery += ",Portofolio_Calon_Pihak_Terlapor       = '" & objSARMemoClass.ObjSARMemo.Portofolio_Calon_Pihak_Terlapor & "'"
            strQuery += ",Hasil_Analisis                        = '" & objSARMemoClass.ObjSARMemo.Hasil_Analisis & "'"
            strQuery += ",Kesimpulan                            = '" & objSARMemoClass.ObjSARMemo.Kesimpulan & "'"
            strQuery += ",Detail_Kesimpulan                     = '" & objSARMemoClass.ObjSARMemo.Detail_Kesimpulan & "'"
            strQuery += ",Tindak_lanjut                         = " & objSARMemoClass.ObjSARMemo.Tindak_lanjut & ""
            strQuery += ",StatusID                              = " & objSARMemoClass.ObjSARMemo.StatusID & ""
            strQuery += ",CIF                                   = '" & objSARMemoClass.ObjSARMemo.CIF & "'"
            strQuery += ",WIC                                   = '" & objSARMemoClass.ObjSARMemo.WIC & "'"
            strQuery += ",FK_CaseManagement_Workflow_SARMemo_ID = " & objSARMemoClass.ObjSARMemo.FK_CaseManagement_Workflow_SARMemo_ID & ""
            strQuery += ",Workflow_Step                         = " & objSARMemoClass.ObjSARMemo.Workflow_Step & ""
            strQuery += ",Direktorat                            = '" & objSARMemoClass.ObjSARMemo.Direktorat & "'"
            strQuery += ",Cabang                                = '" & objSARMemoClass.ObjSARMemo.Cabang & "'"
            strQuery += ",IsCustomer                            = '" & objSARMemoClass.ObjSARMemo.IsCustomer & "'"
            strQuery += ",Active                                = '" & objSARMemoClass.ObjSARMemo.Active & "'"
            strQuery += ",CreatedBy                             = '" & objSARMemoClass.ObjSARMemo.CreatedBy & "'"
            strQuery += ",LastUpdateBy                          = '" & objSARMemoClass.ObjSARMemo.LastUpdateBy & "'"
            strQuery += ",ApprovedBy                            = '" & objSARMemoClass.ObjSARMemo.ApprovedBy & "'"
            strQuery += ",CreatedDate                           = '" & objSARMemoClass.ObjSARMemo.CreatedDate & "'"
            strQuery += ",LastUpdateDate                        = '" & objSARMemoClass.ObjSARMemo.LastUpdateDate & "'"
            strQuery += ",ApprovedDate                          = '" & objSARMemoClass.ObjSARMemo.ApprovedDate & "'"
            strQuery += ",Jenis_Laporan                          = '" & objSARMemoClass.ObjSARMemo.Jenis_Laporan & "'"
            strQuery += ",Tanggal_Laporan                          = '" & objSARMemoClass.ObjSARMemo.Tanggal_Laporan & "'"
            strQuery += ",NoRefPPATK                          = '" & objSARMemoClass.ObjSARMemo.NoRefPPATK & "'"
            strQuery += ",Alasan                          = '" & objSARMemoClass.ObjSARMemo.Alasan & "'"
            strQuery += "from OneFCC_CaseManagement_SARMemo  where PK_OneFCC_CaseManagement_SARMemo_ID = " & objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            strQuery = ""

            Dim PKSARMemoLatest As Integer = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID
            'Update Indikator
            For Each item In objSARMemoClass.listIndikator
                If item.PK_OneFCC_CaseManagement_SARMemo_IndikatorID > 0 Then
                    strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_Indikator  "
                    strQuery += "SET                                   "
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID = " & PKSARMemoLatest & ""
                    strQuery += ",FK_Indicator                       = '" & item.FK_Indicator & "'"
                    strQuery += ",Active                             = '" & item.Active & "'"
                    strQuery += ",CreatedBy                          = '" & item.CreatedBy & "'"
                    strQuery += ",LastUpdateBy                       = '" & item.LastUpdateBy & "'"
                    strQuery += ",ApprovedBy                         = '" & item.ApprovedBy & "'"
                    strQuery += ",CreatedDate                        = '" & item.CreatedDate & "'"
                    strQuery += ",LastUpdateDate                     = '" & item.LastUpdateDate & "'"
                    strQuery += ",ApprovedDate                       = '" & item.ApprovedDate & "'"
                    strQuery += ",Alternateby                        = '" & item.Alternateby & "'"
                    strQuery += "from OneFCC_CaseManagement_SARMemo_Indikator  where PK_OneFCC_CaseManagement_SARMemo_Indikator_ID = " & item.PK_OneFCC_CaseManagement_SARMemo_IndikatorID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                Else
                    strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Indikator ( "
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                    strQuery += "FK_Indicator,"
                    strQuery += "Active,"
                    strQuery += "CreatedBy,"
                    strQuery += "LastUpdateBy,"
                    strQuery += "ApprovedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "LastUpdateDate,"
                    strQuery += "ApprovedDate,"
                    strQuery += "Alternateby"
                    strQuery += ") VALUES ( "
                    strQuery += " " & PKSARMemoLatest & ""
                    strQuery += " , " & item.FK_Indicator & ""
                    strQuery += " , '" & item.Active & "'"
                    strQuery += " , " & item.CreatedBy & "'"
                    strQuery += " , '" & item.LastUpdateBy & "'"
                    strQuery += " , '" & item.ApprovedBy & "'"
                    strQuery += " , '" & item.CreatedDate & "'"
                    strQuery += " , '" & item.LastUpdateDate & "'"
                    strQuery += " , '" & item.ApprovedDate & "'"
                    strQuery += " , '" & item.Alternateby & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                End If

            Next
            strQuery = ""


            'Update Pihak Terkait
            For Each item In objSARMemoClass.listPihakTerkait
                If item.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID > 0 Then
                    strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_Pihak_Terkait  "
                    strQuery += "SET                                   "
                    strQuery += "Nama_PihakTerkait						= '" & item.Nama_PihakTerkait & "'"
                    strQuery += ",Hubungan_PihakTerkait                =  '" & item.Hubungan_PihakTerkait & "'"
                    strQuery += ",Dilaporkan_PihakTerkait              =  '" & item.Dilaporkan_PihakTerkait & "'"
                    strQuery += ",RefGrips_PihakTerkait                =  '" & item.RefGrips_PihakTerkait & "'"
                    strQuery += ",FK_OneFCC_CaseManagement_SARMemo_ID  =  " & PKSARMemoLatest & ""
                    strQuery += ",CIF                                  =  '" & item.CIF & "'"
                    strQuery += ",Active                               =  '" & item.Active & "'"
                    strQuery += ",CreatedBy                            =  '" & item.CreatedBy & "'"
                    strQuery += ",LastUpdateBy                         =  '" & item.LastUpdateBy & "'"
                    strQuery += ",ApprovedBy                           =  '" & item.ApprovedBy & "'"
                    strQuery += ",CreatedDate                          =  '" & item.CreatedDate & "'"
                    strQuery += ",LastUpdateDate                       =  '" & item.LastUpdateDate & "'"
                    strQuery += ",ApprovedDate                         =  '" & item.ApprovedDate & "'"
                    strQuery += ",Alternateby                          =  '" & item.Alternateby & "'"
                    strQuery += "from OneFCC_CaseManagement_SARMemo_Pihak_Terkait  where PK_OneFCC_CaseManagement_SARMemo_Pihak_Terkait_ID = " & item.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                Else
                    strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Pihak_Terkait ( "
                    strQuery += "Nama_PihakTerkait,"
                    strQuery += "Hubungan_PihakTerkait,"
                    strQuery += "Dilaporkan_PihakTerkait,"
                    strQuery += "RefGrips_PihakTerkait,"
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                    strQuery += "CIF,"
                    strQuery += "Active,"
                    strQuery += "CreatedBy,"
                    strQuery += "LastUpdateBy,"
                    strQuery += "ApprovedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "LastUpdateDate,"
                    strQuery += "ApprovedDate,"
                    strQuery += "Alternateby"
                    strQuery += " ) VALUES ( "
                    strQuery += " '" & item.Nama_PihakTerkait & "'"
                    strQuery += " , '" & item.Hubungan_PihakTerkait & "'"
                    strQuery += " , '" & item.Dilaporkan_PihakTerkait & "'"
                    strQuery += " , '" & item.RefGrips_PihakTerkait & "'"
                    strQuery += " , " & PKSARMemoLatest & ""
                    strQuery += " , '" & item.CIF & "'"
                    strQuery += " , '" & item.Active & "'"
                    strQuery += " , '" & item.CreatedBy & "'"
                    strQuery += " , '" & item.LastUpdateBy & "'"
                    strQuery += " , '" & item.ApprovedBy & "'"
                    strQuery += " , '" & item.CreatedDate & "'"
                    strQuery += " , '" & item.LastUpdateDate & "'"
                    strQuery += " , '" & item.ApprovedDate & "'"
                    strQuery += " , '" & item.Alternateby & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)



                End If

            Next
            strQuery = ""

            'Update Tindak Lanjut
            For Each item In objSARMemoClass.listTindakLanjut
                If item.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID > 0 Then
                    strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_TindakLanjut  "
                    strQuery += "SET                                   "
                    strQuery += "TindakLanjutDescription					='" & item.TindakLanjutDescription & "'"
                    strQuery += ",FK_OneFCC_CaseManagement_TindakLanjut_ID = " & item.FK_OneFCC_CaseManagement_TindakLanjut_ID & ""
                    strQuery += ",FK_OneFCC_CaseManagement_SARMemo_ID      = " & PKSARMemoLatest & ""
                    strQuery += ",Active                                   = '" & item.Active & "'"
                    strQuery += ",CreatedBy                                = '" & item.CreatedBy & "'"
                    strQuery += ",LastUpdateBy                             = '" & item.LastUpdateBy & "'"
                    strQuery += ",ApprovedBy                               = '" & item.ApprovedBy & "'"
                    strQuery += ",CreatedDate                              = '" & item.CreatedDate & "'"
                    strQuery += ",LastUpdateDate                           = '" & item.LastUpdateDate & "'"
                    strQuery += ",ApprovedDate                             = '" & item.ApprovedDate & "'"
                    strQuery += ",Alternateby                              = '" & item.Alternateby & "'"
                    strQuery += "from OneFCC_CaseManagement_SARMemo_TindakLanjut  where PK_OneFCC_CaseManagement_SARMemo_TindakLanjut_ID = " & item.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                Else
                    strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_TindakLanjut ( "
                    strQuery += "TindakLanjutDescription,"
                    strQuery += "FK_OneFCC_CaseManagement_TindakLanjut_ID,"
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                    strQuery += "Active,"
                    strQuery += "CreatedBy,"
                    strQuery += "LastUpdateBy,"
                    strQuery += "ApprovedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "LastUpdateDate,"
                    strQuery += "ApprovedDate,"
                    strQuery += "Alternateby"
                    strQuery += " ) VALUES ( "
                    strQuery += " '" & item.TindakLanjutDescription & "'"
                    strQuery += " ," & item.FK_OneFCC_CaseManagement_TindakLanjut_ID & ""
                    strQuery += " ," & PKSARMemoLatest & ""
                    strQuery += " ,'" & item.Active & "'"
                    strQuery += " ,'" & item.CreatedBy & "'"
                    strQuery += " ,'" & item.LastUpdateBy & "'"
                    strQuery += " ,'" & item.ApprovedBy & "'"
                    strQuery += " ,'" & item.CreatedDate & "'"
                    strQuery += " ,'" & item.LastUpdateDate & "'"
                    strQuery += " ,'" & item.ApprovedDate & "'"
                    strQuery += " ,'" & item.Alternateby & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                End If

            Next
            strQuery = ""

            'Update Activity
            For Each item In objSARMemoClass.listActivity
                If item.Pk_OneFCC_CaseManagement_SARMemo_ActivityID > 0 Then
                    strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_Activity  "
                    strQuery += "SET                                   "
                    strQuery += "Account_NO		= '" & item.Account_NO & "'"
                    strQuery += ",Date_Activity    = '" & item.Date_Activity & "'"
                    strQuery += ",Significance     = '" & item.Significance & "'"
                    strQuery += ",Reason           = '" & item.Reason & "'"
                    strQuery += ",Comments         = '" & item.Comments & "'"
                    strQuery += ",Active           = '" & item.Active & "'"
                    strQuery += ",CreatedBy        = '" & item.CreatedBy & "'"
                    strQuery += ",LastUpdateBy     = '" & item.LastUpdateBy & "'"
                    strQuery += ",ApprovedBy       = '" & item.ApprovedBy & "'"
                    strQuery += ",CreatedDate      = '" & item.CreatedDate & "'"
                    strQuery += ",LastUpdateDate   = '" & item.LastUpdateDate & "'"
                    strQuery += ",ApprovedDate     = '" & item.ApprovedDate & "'"
                    strQuery += ",Alternateby      = '" & item.Alternateby & "'"
                    strQuery += ",FK_OneFCC_CaseManagement_SARMemo_ID      = " & PKSARMemoLatest & ""
                    strQuery += "from OneFCC_CaseManagement_SARMemo_Activity  where PK_OneFCC_CaseManagement_SARMemo_Activity_ID = " & item.Pk_OneFCC_CaseManagement_SARMemo_ActivityID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                Else
                    strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Activity ( "
                    strQuery += "Account_NO,"
                    strQuery += "Date_Activity,"
                    strQuery += "Significance,"
                    strQuery += "Reason,"
                    strQuery += "Comments,"
                    strQuery += "Active,"
                    strQuery += "CreatedBy,"
                    strQuery += "LastUpdateBy,"
                    strQuery += "ApprovedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "LastUpdateDate,"
                    strQuery += "ApprovedDate,"
                    strQuery += "Alternateby,"
                    strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID"
                    strQuery += " ) VALUES ( "
                    strQuery += " '" & item.Account_NO & "'"
                    strQuery += " , '" & item.Date_Activity & "'"
                    strQuery += " , '" & item.Significance & "'"
                    strQuery += " , '" & item.Reason & "'"
                    strQuery += " , '" & item.Comments & "'"
                    strQuery += " ,'" & item.Active & "'"
                    strQuery += " , '" & item.CreatedBy & "'"
                    strQuery += " , '" & item.LastUpdateBy & "'"
                    strQuery += " , '" & item.ApprovedBy & "'"
                    strQuery += " , '" & item.CreatedDate & "'"
                    strQuery += " , '" & item.LastUpdateDate & "'"
                    strQuery += " , '" & item.ApprovedDate & "'"
                    strQuery += " , '" & item.Alternateby & "'"
                    strQuery += " , '" & PKSARMemoLatest & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                End If

            Next
            strQuery = ""

            ' Update Attachment
            If objSARMemoClass.listAttachment IsNot Nothing Then
                For Each item In objSARMemoClass.listAttachment
                    Dim objParam(0) As SqlParameter

                    objParam(0) = New SqlParameter
                    objParam(0).ParameterName = "@FileBinary"
                    objParam(0).Value = item.File_Doc
                    objParam(0).DbType = SqlDbType.Binary
                    If item.PK_OneFCC_CaseManagement_SARMemo_AttachmentID > 0 Then
                        strQuery = "UPDATE OneFCC_CaseManagement_SARMemo_Attachment  "
                        strQuery += "SET                                   "
                        strQuery += "Fk_goAML_ODM_Generate_STR_SAR        = '" & item.Fk_goAML_ODM_Generate_STR_SAR & "'"
                        strQuery += ",File_Name                           = '" & item.File_Name & "'"
                        strQuery += ",File_Doc                            = CONVERT(varbinary(max),@FileBinary)"
                        strQuery += ",FK_OneFCC_CaseManagement_SARMemo_ID = " & PKSARMemoLatest & ""
                        strQuery += ",Remarks                             = '" & item.Remarks & "'"
                        strQuery += ",Active                              = '" & item.Active & "'"
                        strQuery += ",CreatedBy                           = '" & item.CreatedBy & "'"
                        strQuery += ",LastUpdateBy                        = '" & item.LastUpdateBy & "'"
                        strQuery += ",ApprovedBy                          = '" & item.ApprovedBy & "'"
                        strQuery += ",CreatedDate                         = '" & item.CreatedDate & "'"
                        strQuery += ",LastUpdateDate                      = '" & item.LastUpdateDate & "'"
                        strQuery += ",ApprovedDate                        = '" & item.ApprovedDate & "'"
                        strQuery += ",Alternateby                         = '" & item.Alternateby & "'"
                        strQuery += "from OneFCC_CaseManagement_SARMemo_Attachment  where PK_OneFCC_CaseManagement_SARMemo_Attachment_ID = " & item.PK_OneFCC_CaseManagement_SARMemo_AttachmentID
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, objParam)

                    Else



                        strQuery = "INSERT INTO OneFCC_CaseManagement_SARMemo_Attachment ( "
                        strQuery += "Fk_goAML_ODM_Generate_STR_SAR,"
                        strQuery += "File_Name,"
                        strQuery += "File_Doc,"
                        strQuery += "FK_OneFCC_CaseManagement_SARMemo_ID,"
                        strQuery += "Remarks,"
                        strQuery += "Active,"
                        strQuery += "CreatedBy,"
                        strQuery += "LastUpdateBy,"
                        strQuery += "ApprovedBy,"
                        strQuery += "CreatedDate,"
                        strQuery += "LastUpdateDate,"
                        strQuery += "ApprovedDate,"
                        strQuery += "Alternateby"
                        strQuery += " ) VALUES ( "
                        strQuery += "'" & item.Fk_goAML_ODM_Generate_STR_SAR & "'"
                        strQuery += ",'" & item.File_Name & "'"
                        strQuery += ", CONVERT(varbinary(max),@FileBinary)"
                        strQuery += ",'" & PKSARMemoLatest & "'"
                        strQuery += ",'" & item.Remarks & "'"
                        strQuery += ",'" & item.Active & "'"
                        strQuery += ",'" & item.CreatedBy & "'"
                        strQuery += ",'" & item.LastUpdateBy & "'"
                        strQuery += ",'" & item.ApprovedBy & "'"
                        strQuery += ",'" & item.CreatedDate & "'"
                        strQuery += ",'" & item.LastUpdateDate & "'"
                        strQuery += ",'" & item.ApprovedDate & "'"
                        strQuery += ",'" & item.Alternateby & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, objParam)

                    End If

                Next
            End If
            strQuery = ""

            Dim listIndikatorOld As New List(Of OneFCC_CaseManagement_SARMemo_Indikator)
            Dim listIndikatorDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM OneFCC_CaseManagement_SARMemo_Indikator where FK_OneFCC_CaseManagement_SARMemo_ID = " & objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID, Nothing)
            For Each item As DataRow In listIndikatorDT.Rows
                Dim Indikator As New OneFCC_CaseManagement_SARMemo_Indikator
                Indikator.PK_OneFCC_CaseManagement_SARMemo_IndikatorID = item("PK_OneFCC_CaseManagement_SARMemo_Indikator_ID")
                Indikator.FK_OneFCC_CaseManagement_SARMemoID = item("FK_OneFCC_CaseManagement_SARMemo_ID")
                Indikator.FK_Indicator = item("FK_Indicator")
                Indikator.Active = item("Active")
                Indikator.CreatedBy = item("CreatedBy")
                Indikator.LastUpdateBy = item("LastUpdateBy")
                Indikator.ApprovedBy = item("ApprovedBy")
                Indikator.CreatedDate = item("CreatedDate")
                Indikator.LastUpdateDate = item("LastUpdateDate")
                Indikator.ApprovedDate = item("ApprovedDate")
                Indikator.Alternateby = item("Alternateby")
                listIndikatorOld.Add(Indikator)
            Next
            For Each itemx As OneFCC_CaseManagement_SARMemo_Indikator In listIndikatorOld
                Dim objcek As OneFCC_CaseManagement_SARMemo_Indikator = objSARMemoClass.listIndikator.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_IndikatorID = itemx.PK_OneFCC_CaseManagement_SARMemo_IndikatorID)
                If objcek Is Nothing Then
                    strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Indikator		"
                    strQuery += "where pk_OneFCC_CaseManagement_SARMemo_Indikator_ID =  " & itemx.PK_OneFCC_CaseManagement_SARMemo_IndikatorID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                End If
            Next


            If objSARMemoClass_Old.listPihakTerkait.Count > 0 Then
                For Each itemx As OneFCC_CaseManagement_SARMemo_Pihak_Terkait In (From x In objSARMemoClass_Old.listPihakTerkait Where x.FK_OneFCC_CaseManagement_SARMemoID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID Select x).ToList
                    Dim objcek As OneFCC_CaseManagement_SARMemo_Pihak_Terkait = objSARMemoClass.listPihakTerkait.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID = itemx.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID)
                    If objcek Is Nothing Then
                        strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Pihak_Terkait		"
                        strQuery += "where PK_OneFCC_CaseManagement_SARMemo_Pihak_Terkait_ID =  " & itemx.PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                    End If
                Next
            End If
            For Each itemx As OneFCC_CaseManagement_SARMemo_TindakLanjut In (From x In objSARMemoClass_Old.listTindakLanjut Where x.FK_OneFCC_CaseManagement_SARMemoID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID Select x).ToList
                Dim objcek As OneFCC_CaseManagement_SARMemo_TindakLanjut = objSARMemoClass.listTindakLanjut.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID = itemx.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID)
                If objcek Is Nothing Then
                    strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_TindakLanjut		"
                    strQuery += "where PK_OneFCC_CaseManagement_SARMemo_TindakLanjut_ID =  " & itemx.PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                End If
            Next
            For Each itemx As OneFCC_CaseManagement_SARMemo_Activity In (From x In objSARMemoClass_Old.listActivity Where x.FK_OneFCC_CaseManagement_SARMemoID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID Select x).ToList
                Dim objcek As OneFCC_CaseManagement_SARMemo_Activity = objSARMemoClass.listActivity.Find(Function(x) x.Pk_OneFCC_CaseManagement_SARMemo_ActivityID = itemx.Pk_OneFCC_CaseManagement_SARMemo_ActivityID)
                If objcek Is Nothing Then
                    strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Activity		"
                    strQuery += "where PK_OneFCC_CaseManagement_SARMemo_Activity_ID =  " & itemx.Pk_OneFCC_CaseManagement_SARMemo_ActivityID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                End If
            Next
            For Each itemx As OneFCC_CaseManagement_SARMemo_Attachment In (From x In objSARMemoClass_Old.listAttachment Where x.FK_OneFCC_CaseManagement_SARMemoID = objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID Select x).ToList
                Dim objcek As OneFCC_CaseManagement_SARMemo_Attachment = objSARMemoClass.listAttachment.Find(Function(x) x.PK_OneFCC_CaseManagement_SARMemo_AttachmentID = itemx.PK_OneFCC_CaseManagement_SARMemo_AttachmentID)
                If objcek Is Nothing Then
                    strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Attachment		"
                    strQuery += "where PK_OneFCC_CaseManagement_SARMemo_Attachment_ID =  " & itemx.PK_OneFCC_CaseManagement_SARMemo_AttachmentID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                End If
            Next

            ' Audit Trail
            strQuery = "INSERT INTO AuditTrailHeader ( "
            strQuery += "ApproveBy,"
            strQuery += "CreatedBy,"
            strQuery += "CreatedDate,"
            strQuery += "FK_AuditTrailStatus_ID,"
            strQuery += "FK_ModuleAction_ID,"
            strQuery += "ModuleLabel"
            strQuery += " ) VALUES ( "
            strQuery += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            strQuery += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
            strQuery += " , '" & NawaBLL.Common.ModuleActionEnum.Insert & "'"
            strQuery += " , '" & objModule.ModuleLabel & "')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Dim PKAudit As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)

            ' Audit SAR Memo
            strQuery = ""
            Dim objectdata As Object
            objectdata = objSARMemoClass.ObjSARMemo
            Dim objtype As Type = objectdata.GetType
            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
            For Each item As System.Reflection.PropertyInfo In properties
                Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                If Not item.GetValue(objectdata, Nothing) Is Nothing Then
                    If item.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                        objaudittraildetail.NewValue = item.GetValue(objectdata, Nothing)
                    Else
                        objaudittraildetail.NewValue = ""
                    End If
                Else
                    objaudittraildetail.NewValue = ""
                End If
                strQuery = "INSERT INTO AuditTrailDetail(  "
                strQuery += "FK_AuditTrailHeader_ID,"
                strQuery += "FieldName,"
                strQuery += "OldValue,"
                strQuery += "NewValue"
                strQuery += " ) VALUES ( "
                strQuery += " " & PKAudit & ""
                strQuery += " ,'" & item.Name & "'"
                strQuery += " , ''"
                strQuery += " , '" & objaudittraildetail.NewValue & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Next


            If objSARMemoClass.listIndikator IsNot Nothing AndAlso objSARMemoClass.listIndikator.Count > 0 Then
                For Each item In objSARMemoClass.listIndikator
                    strQuery = ""
                    Dim ObjectdataIndikator As Object
                    ObjectdataIndikator = item
                    Dim ObjtypeIndikator As Type = ObjectdataIndikator.GetType
                    Dim PropertiesIndikator() As System.Reflection.PropertyInfo = ObjtypeIndikator.GetProperties
                    For Each itemIndikator As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemIndikator.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemIndikator.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemIndikator.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemIndikator.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listPihakTerkait IsNot Nothing AndAlso objSARMemoClass.listPihakTerkait.Count > 0 Then
                For Each item In objSARMemoClass.listPihakTerkait
                    strQuery = ""
                    Dim ObjectdataPihakTerkait As Object
                    ObjectdataPihakTerkait = item
                    Dim ObjtypePihakTerkait As Type = ObjectdataPihakTerkait.GetType
                    Dim PropertiesPihakTerkait() As System.Reflection.PropertyInfo = ObjtypePihakTerkait.GetProperties
                    For Each itemPihakTerkait As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemPihakTerkait.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemPihakTerkait.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemPihakTerkait.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemPihakTerkait.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listTindakLanjut IsNot Nothing AndAlso objSARMemoClass.listTindakLanjut.Count > 0 Then
                For Each item In objSARMemoClass.listTindakLanjut
                    strQuery = ""
                    Dim ObjectdataTindakLanjut As Object
                    ObjectdataTindakLanjut = item
                    Dim ObjtypeTindakLanjut As Type = ObjectdataTindakLanjut.GetType
                    Dim PropertiesTindakLanjut() As System.Reflection.PropertyInfo = ObjtypeTindakLanjut.GetProperties
                    For Each itemTindakLanjut As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemTindakLanjut.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemTindakLanjut.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemTindakLanjut.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemTindakLanjut.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listCaseID IsNot Nothing AndAlso objSARMemoClass.listCaseID.Count > 0 Then
                For Each item In objSARMemoClass.listCaseID
                    strQuery = ""
                    Dim ObjectdataCaseID As Object
                    ObjectdataCaseID = item
                    Dim ObjtypeCaseID As Type = ObjectdataCaseID.GetType
                    Dim PropertiesCaseID() As System.Reflection.PropertyInfo = ObjtypeCaseID.GetProperties
                    For Each itemCaseID As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemCaseID.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemCaseID.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemCaseID.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemCaseID.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

            If objSARMemoClass.listAttachment IsNot Nothing AndAlso objSARMemoClass.listAttachment.Count > 0 Then
                For Each item In objSARMemoClass.listAttachment
                    strQuery = ""
                    Dim ObjectdataAttachment As Object
                    ObjectdataAttachment = item
                    Dim ObjtypeAttachment As Type = ObjectdataAttachment.GetType
                    Dim PropertiesAttachment() As System.Reflection.PropertyInfo = ObjtypeAttachment.GetProperties
                    For Each itemAttachment As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemAttachment.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemAttachment.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemAttachment.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemAttachment.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

            If objSARMemoClass.listActivity IsNot Nothing AndAlso objSARMemoClass.listActivity.Count > 0 Then
                For Each item In objSARMemoClass.listActivity
                    strQuery = ""
                    Dim ObjectdataActivity As Object
                    ObjectdataActivity = item
                    Dim ObjtypeActivity As Type = ObjectdataActivity.GetType
                    Dim PropertiesActivity() As System.Reflection.PropertyInfo = ObjtypeActivity.GetProperties
                    For Each itemActivity As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemActivity.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemActivity.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemActivity.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemActivity.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If





        Catch ex As Exception

        End Try

    End Sub


    Public Shared Sub DeleteTanpaApproval(objSARMemoClass As SARMemoClass, objModule As NawaDAL.Module)
        Try
            Dim strQuery As String = ""
            strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo		"
            strQuery += "where pk_OneFCC_CaseManagement_SARMemo_id =  " & objSARMemoClass.ObjSARMemo.PK_OneFCC_CaseManagement_SARMemoID
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            strQuery = ""
            'Delete Indicator
            For Each item In objSARMemoClass.listIndikator
                strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Indikator		"
                strQuery += "where fk_OneFCC_CaseManagement_SARMemo_id =  " & item.FK_OneFCC_CaseManagement_SARMemoID
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Next
            strQuery = ""
            'Delete Pihak Terkait
            For Each item In objSARMemoClass.listPihakTerkait
                strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Pihak_Terkait		"
                strQuery += "where fk_OneFCC_CaseManagement_SARMemo_id =  " & item.FK_OneFCC_CaseManagement_SARMemoID
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Next
            strQuery = ""
            'Delete Tindak Lanjut
            For Each item In objSARMemoClass.listTindakLanjut
                strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_TindakLanjut		"
                strQuery += "where fk_OneFCC_CaseManagement_SARMemo_id =  " & item.FK_OneFCC_CaseManagement_SARMemoID
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Next
            strQuery = ""
            'Delete Attachment
            For Each item In objSARMemoClass.listAttachment
                strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Attachment		"
                strQuery += "where fk_OneFCC_CaseManagement_SARMemo_id =  " & item.FK_OneFCC_CaseManagement_SARMemoID
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Next
            strQuery = ""
            'Delete Activity
            For Each item In objSARMemoClass.listActivity
                strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_Activity		"
                strQuery += "where fk_OneFCC_CaseManagement_SARMemo_id =  " & item.FK_OneFCC_CaseManagement_SARMemoID
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Next
            strQuery = ""
            'Delete Alert
            For Each item In objSARMemoClass.listCaseID
                strQuery += "DELETE FROM OneFCC_CaseManagement_SARMemo_CaseID		"
                strQuery += "where fk_OneFCC_CaseManagement_SARMemo_id =  " & item.FK_OneFCC_CaseManagement_SARMemoID
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Next

            ' Audit Trail
            strQuery = "INSERT INTO AuditTrailHeader ( "
            strQuery += "ApproveBy,"
            strQuery += "CreatedBy,"
            strQuery += "CreatedDate,"
            strQuery += "FK_AuditTrailStatus_ID,"
            strQuery += "FK_ModuleAction_ID,"
            strQuery += "ModuleLabel"
            strQuery += " ) VALUES ( "
            strQuery += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQuery += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            strQuery += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
            strQuery += " , '" & NawaBLL.Common.ModuleActionEnum.Insert & "'"
            strQuery += " , '" & objModule.ModuleLabel & "')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Dim PKAudit As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)

            ' Audit SAR Memo
            strQuery = ""
            Dim objectdata As Object
            objectdata = objSARMemoClass.ObjSARMemo
            Dim objtype As Type = objectdata.GetType
            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
            For Each item As System.Reflection.PropertyInfo In properties
                Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                If Not item.GetValue(objectdata, Nothing) Is Nothing Then
                    If item.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                        objaudittraildetail.NewValue = item.GetValue(objectdata, Nothing)
                    Else
                        objaudittraildetail.NewValue = ""
                    End If
                Else
                    objaudittraildetail.NewValue = ""
                End If
                strQuery = "INSERT INTO AuditTrailDetail(  "
                strQuery += "FK_AuditTrailHeader_ID,"
                strQuery += "FieldName,"
                strQuery += "OldValue,"
                strQuery += "NewValue"
                strQuery += " ) VALUES ( "
                strQuery += " " & PKAudit & ""
                strQuery += " ,'" & item.Name & "'"
                strQuery += " , ''"
                strQuery += " , '" & objaudittraildetail.NewValue & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Next


            If objSARMemoClass.listIndikator IsNot Nothing AndAlso objSARMemoClass.listIndikator.Count > 0 Then
                For Each item In objSARMemoClass.listIndikator
                    strQuery = ""
                    Dim ObjectdataIndikator As Object
                    ObjectdataIndikator = item
                    Dim ObjtypeIndikator As Type = ObjectdataIndikator.GetType
                    Dim PropertiesIndikator() As System.Reflection.PropertyInfo = ObjtypeIndikator.GetProperties
                    For Each itemIndikator As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemIndikator.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemIndikator.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemIndikator.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemIndikator.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listPihakTerkait IsNot Nothing AndAlso objSARMemoClass.listPihakTerkait.Count > 0 Then
                For Each item In objSARMemoClass.listPihakTerkait
                    strQuery = ""
                    Dim ObjectdataPihakTerkait As Object
                    ObjectdataPihakTerkait = item
                    Dim ObjtypePihakTerkait As Type = ObjectdataPihakTerkait.GetType
                    Dim PropertiesPihakTerkait() As System.Reflection.PropertyInfo = ObjtypePihakTerkait.GetProperties
                    For Each itemPihakTerkait As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemPihakTerkait.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemPihakTerkait.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemPihakTerkait.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemPihakTerkait.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listTindakLanjut IsNot Nothing AndAlso objSARMemoClass.listTindakLanjut.Count > 0 Then
                For Each item In objSARMemoClass.listTindakLanjut
                    strQuery = ""
                    Dim ObjectdataTindakLanjut As Object
                    ObjectdataTindakLanjut = item
                    Dim ObjtypeTindakLanjut As Type = ObjectdataTindakLanjut.GetType
                    Dim PropertiesTindakLanjut() As System.Reflection.PropertyInfo = ObjtypeTindakLanjut.GetProperties
                    For Each itemTindakLanjut As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemTindakLanjut.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemTindakLanjut.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemTindakLanjut.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemTindakLanjut.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next

                Next
            End If

            If objSARMemoClass.listCaseID IsNot Nothing AndAlso objSARMemoClass.listCaseID.Count > 0 Then
                For Each item In objSARMemoClass.listCaseID
                    strQuery = ""
                    Dim ObjectdataCaseID As Object
                    ObjectdataCaseID = item
                    Dim ObjtypeCaseID As Type = ObjectdataCaseID.GetType
                    Dim PropertiesCaseID() As System.Reflection.PropertyInfo = ObjtypeCaseID.GetProperties
                    For Each itemCaseID As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemCaseID.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemCaseID.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemCaseID.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemCaseID.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

            If objSARMemoClass.listAttachment IsNot Nothing AndAlso objSARMemoClass.listAttachment.Count > 0 Then
                For Each item In objSARMemoClass.listAttachment
                    strQuery = ""
                    Dim ObjectdataAttachment As Object
                    ObjectdataAttachment = item
                    Dim ObjtypeAttachment As Type = ObjectdataAttachment.GetType
                    Dim PropertiesAttachment() As System.Reflection.PropertyInfo = ObjtypeAttachment.GetProperties
                    For Each itemAttachment As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemAttachment.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemAttachment.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemAttachment.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemAttachment.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

            If objSARMemoClass.listActivity IsNot Nothing AndAlso objSARMemoClass.listActivity.Count > 0 Then
                For Each item In objSARMemoClass.listActivity
                    strQuery = ""
                    Dim ObjectdataActivity As Object
                    ObjectdataActivity = item
                    Dim ObjtypeActivity As Type = ObjectdataActivity.GetType
                    Dim PropertiesActivity() As System.Reflection.PropertyInfo = ObjtypeActivity.GetProperties
                    For Each itemActivity As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemActivity.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemActivity.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemActivity.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemActivity.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

            If objSARMemoClass.listCaseID IsNot Nothing AndAlso objSARMemoClass.listCaseID.Count > 0 Then
                For Each item In objSARMemoClass.listCaseID
                    strQuery = ""
                    Dim ObjectdataCaseID As Object
                    ObjectdataCaseID = item
                    Dim ObjtypeCaseID As Type = ObjectdataCaseID.GetType
                    Dim PropertiesCaseID() As System.Reflection.PropertyInfo = ObjtypeCaseID.GetProperties
                    For Each itemCaseID As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New CasemanagementDAL.AuditTrailDetail
                        If Not itemCaseID.GetValue(objectdata, Nothing) Is Nothing Then
                            If itemCaseID.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                objaudittraildetail.NewValue = itemCaseID.GetValue(objectdata, Nothing)
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        strQuery = "INSERT INTO AuditTrailDetail(  "
                        strQuery += "FK_AuditTrailHeader_ID,"
                        strQuery += "FieldName,"
                        strQuery += "OldValue,"
                        strQuery += "NewValue"
                        strQuery += " ) VALUES ( "
                        strQuery += " " & PKAudit & ""
                        strQuery += " ,'" & itemCaseID.Name & "'"
                        strQuery += " , ''"
                        strQuery += " , '" & objaudittraildetail.NewValue & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Next
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Class OneFCC_CaseManagement_SARMemo
        Public PK_OneFCC_CaseManagement_SARMemoID As Long
        Public No As String
        Public Kepada As String
        Public Dari As String
        Public Tanggal As DateTime
        Public Perihal As String
        Public Sumber_Pelaporan As String
        Public Tipologi As Integer
        Public Nama_Pihak_Terlapor As String
        Public Kateri_Pihak_Terlapor As String
        Public Unit_Bisnis_Directorat As String
        Public Consumer_Banking As String
        Public Lama_menjadi_Nasabah As String
        Public Pekerjaan_Bidang_Usaha As String
        Public Penghasilan_Tahun As String
        Public Profil_Lainnya As String
        Public Portofolio_Calon_Pihak_Terlapor As String
        Public Hasil_Analisis As String
        Public Kesimpulan As String
        Public Detail_Kesimpulan As String
        Public Tindak_lanjut As Integer
        Public StatusID As Integer
        Public CIF As String
        Public WIC As String
        Public FK_CaseManagement_Workflow_SARMemo_ID As String
        Public Workflow_Step As Integer
        Public Direktorat As String
        Public Cabang As String
        Public IsCustomer As Nullable(Of Boolean)
        Public Active As Nullable(Of Boolean)
        Public CreatedBy As String
        Public LastUpdateBy As String
        Public ApprovedBy As String
        Public CreatedDate As Nullable(Of Date)
        Public LastUpdateDate As Nullable(Of Date)
        Public ApprovedDate As Nullable(Of Date)
        Public Alternateby As String
        Public Jenis_Laporan As String
        Public Tanggal_Laporan As Nullable(Of Date)
        Public NoRefPPATK As String
        Public Alasan As String
    End Class

    Public Class OneFCC_CaseManagement_SARMemo_CaseID
        Public Property PK_OneFCC_CaseManagement_SARMemo_CaseIDID As Long

        Public Property FK_OneFCC_CaseManagement_SARMemoID As Nullable(Of Long)

        Public Property CaseID As Nullable(Of Long)

        Public Property Active As Nullable(Of Boolean)

        Public Property CreatedBy As String

        Public Property LastUpdateBy As String

        Public Property ApprovedBy As String

        Public Property CreatedDate As Nullable(Of Date)

        Public Property LastUpdateDate As Nullable(Of Date)

        Public Property ApprovedDate As Nullable(Of Date)

        Public Property Alternateby As String
    End Class

    Public Class OneFCC_CaseManagement_SARMemo_Indikator

        Public Property PK_OneFCC_CaseManagement_SARMemo_IndikatorID As Long

        Public Property FK_OneFCC_CaseManagement_SARMemoID As Nullable(Of Long)

        Public Property FK_Indicator As String

        Public Property Active As Nullable(Of Boolean)

        Public Property CreatedBy As String

        Public Property LastUpdateBy As String

        Public Property ApprovedBy As String

        Public Property CreatedDate As Nullable(Of Date)

        Public Property LastUpdateDate As Nullable(Of Date)

        Public Property ApprovedDate As Nullable(Of Date)

        Public Property Alternateby As String


    End Class

    Public Class OneFCC_CaseManagement_SARMemo_Pihak_Terkait

        Public Property PK_OneFCC_CaseManagement_SARMemo_Pihak_TerkaitID As Integer

        Public Property Nama_PihakTerkait As String

        Public Property Hubungan_PihakTerkait As String

        Public Property Dilaporkan_PihakTerkait As String

        Public Property RefGrips_PihakTerkait As String

        Public Property FK_OneFCC_CaseManagement_SARMemoID As Nullable(Of Integer)

        Public Property CIF As String

        Public Property Active As Nullable(Of Boolean)

        Public Property CreatedBy As String

        Public Property LastUpdateBy As String

        Public Property ApprovedBy As String

        Public Property CreatedDate As Nullable(Of Date)

        Public Property LastUpdateDate As Nullable(Of Date)

        Public Property ApprovedDate As Nullable(Of Date)

        Public Property Alternateby As String


    End Class

    Public Class OneFCC_CaseManagement_SARMemo_TindakLanjut

        Public Property PK_OneFCC_CaseManagement_SARMemo_TindakLanjutID As Integer

        Public Property TindakLanjutDescription As String

        Public Property FK_OneFCC_CaseManagement_TindakLanjut_ID As Nullable(Of Integer)

        Public Property FK_OneFCC_CaseManagement_SARMemoID As Nullable(Of Long)

        Public Property Active As Nullable(Of Boolean)

        Public Property CreatedBy As String

        Public Property LastUpdateBy As String

        Public Property ApprovedBy As String

        Public Property CreatedDate As Nullable(Of Date)

        Public Property LastUpdateDate As Nullable(Of Date)

        Public Property ApprovedDate As Nullable(Of Date)

        Public Property Alternateby As String


    End Class

    Public Class OneFCC_CaseManagement_SARMemo_Attachment

        Public Property PK_OneFCC_CaseManagement_SARMemo_AttachmentID As Integer

        Public Property Fk_goAML_ODM_Generate_STR_SAR As Nullable(Of Integer)

        Public Property File_Name As String

        Public Property File_Doc As Byte()

        Public Property FK_OneFCC_CaseManagement_SARMemoID As Nullable(Of Integer)

        Public Property Active As Nullable(Of Boolean)

        Public Property CreatedBy As String

        Public Property LastUpdateBy As String

        Public Property ApprovedBy As String

        Public Property CreatedDate As Nullable(Of Date)

        Public Property LastUpdateDate As Nullable(Of Date)

        Public Property ApprovedDate As Nullable(Of Date)

        Public Property Alternateby As String

        Public Property Remarks As String


    End Class

    Public Class OneFCC_CaseManagement_SARMemo_Activity
        Public Property Pk_OneFCC_CaseManagement_SARMemo_ActivityID As Long
        Public Property FK_OneFCC_CaseManagement_SARMemoID As Nullable(Of Integer)
        Public Property Account_NO As String
        Public Property Date_Activity As DateTime
        Public Property Significance As Nullable(Of Long)
        Public Property Reason As String
        Public Property Comments As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
    End Class

    Public Class OneFCC_CaseManagement_WorkflowHistory_SARMemo
        Public Property PK_CaseManagement_WorkflowHistory_SARMemo_ID As Long
        Public Property FK_OneFCC_CaseManagement_SARMemo_ID As Nullable(Of Long)
        Public Property Workflow_Step As Nullable(Of Long)
        Public Property UserID As String
        Public Property Investigation_Notes As String
        Public Property Request_Change As String
        Public Property FK_Proposed_Status_ID As Nullable(Of Long)
        Public Property Approval_Status As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class


    Public Class SARMemoClass
        Public ObjSARMemo As New OneFCC_CaseManagement_SARMemo
        Public listCaseID As New List(Of OneFCC_CaseManagement_SARMemo_CaseID)
        Public listIndikator As New List(Of OneFCC_CaseManagement_SARMemo_Indikator)
        Public listPihakTerkait As New List(Of OneFCC_CaseManagement_SARMemo_Pihak_Terkait)
        Public listTindakLanjut As New List(Of OneFCC_CaseManagement_SARMemo_TindakLanjut)
        Public listActivity As New List(Of OneFCC_CaseManagement_SARMemo_Activity)
        Public listAttachment As New List(Of OneFCC_CaseManagement_SARMemo_Attachment)
    End Class
End Class
