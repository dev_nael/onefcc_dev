﻿Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Xml
Imports Ionic.Zip
Imports Ionic.Zlib
Imports NawaBLL
Imports NawaDAL
Imports NawaDevDAL
Imports System.Xml.Schema
Imports System.IO

Public Class GeneratePPATKBLL

    Public Shared Function GetGeneratedgoAMLByID(obj As Integer) As goAML_Generate_XML
        Using objdb As NawaDatadevEntities = New NawaDatadevEntities
            Return objdb.goAML_Generate_XML.Where(Function(x) x.NO_ID = obj).FirstOrDefault
        End Using
    End Function

    Public Sub UploadReportPPATK(obj As goAML_Generate_XML, submit As Date, noreferensi As String, objmodule As String)


        Dim sqlparam(2) As SqlParameter
        sqlparam(0) = New SqlParameter
        sqlparam(0).ParameterName = "@pkgoaml_generate_xml_ID"
        sqlparam(0).Value = obj.NO_ID
        sqlparam(1) = New SqlParameter
        sqlparam(1).ParameterName = "@submitdate"
        sqlparam(1).Value = submit
        sqlparam(2) = New SqlParameter
        sqlparam(2).ParameterName = "@refno"
        sqlparam(2).Value = noreferensi
        SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_UpdateRefNo", sqlparam)

        'Using objdb As NawaDatadevEntities = New NawaDatadevEntities
        '    Try
        '        Dim datetransaction As String = obj.Transaction_Date.Value.ToString("yyyy-MM-dd")
        '        Dim datelast As String = obj.LastUpdateDate.Value.ToString("yyyy-MM-dd")
        '        Dim status As Integer = GlobalReportFunctionBLL.getStatusReportByCodeInt(obj.Status)
        '        Dim listObjgoAMLReport As List(Of GoAML_Report)
        '        listObjgoAMLReport = GlobalReportFunctionBLL.getListReportGenerateXML(datetransaction, obj.Jenis_Laporan, status, datelast)

        '        Dim user As String = Common.SessionCurrentUser.UserID
        '        Dim act As Integer = Common.AuditTrailStatusEnum.AffectedToDatabase
        '        Dim acts As Integer = Common.ModuleActionEnum.Update
        '        Dim modulename As String = objmodule
        '        Dim objATHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, acts, modulename)
        '        Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
        '            Try
        '                For Each item In listObjgoAMLReport
        '                    Dim itemOld As GoAML_Report = item
        '                    With item
        '                        .submit_date = submit
        '                        .fiu_ref_number = noreferensi
        '                        '.LastUpdateDate = Now.Date
        '                        .status = 3
        '                    End With
        '                    objdb.Entry(item).State = EntityState.Modified
        '                    NawaFramework.CreateAuditTrailDetailEdit(objdb, objATHeader.PK_AuditTrail_ID, item, itemOld)
        '                    objdb.SaveChanges()
        '                Next
        '                objTrans.Commit()
        '            Catch ex As Exception
        '                objTrans.Rollback()
        '                Throw ex
        '            End Try
        '        End Using
        '        Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
        '            Try
        '                Dim objOld As goAML_Generate_XML = obj
        '                With obj
        '                    .Status = GlobalReportFunctionBLL.getStatusReportByCode(3)
        '                    .Report_PPATK_Date = submit
        '                    '.LastUpdateDate = Now.Date
        '                    '.Last_Update_Date = Now.Date
        '                End With
        '                objdb.Entry(obj).State = EntityState.Modified
        '                NawaFramework.CreateAuditTrailDetailEdit(objdb, objATHeader.PK_AuditTrail_ID, obj, objOld)
        '                objdb.SaveChanges()
        '                objTrans.Commit()
        '            Catch ex As Exception
        '                objTrans.Rollback()
        '                Throw ex
        '            End Try
        '        End Using
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Using
    End Sub

    Public Shared Function GenerateReportXMLALLNew(ListgoamlGeneratexml As DataTable, strdirpath As String) As String

        Dim lstFile As New List(Of String)
        Dim lstFileZip As New List(Of String)

        Dim intjmlFiledalam1zip As Integer = 1000
        intjmlFiledalam1zip = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT garrgp.ParameterValue FROM goAML_Ref_ReportGlobalParameter AS garrgp WHERE garrgp.PK_GlobalReportParameter_ID=11", Nothing)

        For Each item As DataRow In ListgoamlGeneratexml.Rows
            Dim FileReturn As String = ""
            Dim datetransaction As Date = Convert.ToDateTime(item("transactiondate")).ToString("yyyy-MM-dd")
            Dim jenislaporan As String = item("jenislaporan")
            Dim datelastupdate As Date = Convert.ToDateTime(item("lastupdatedate")).ToString("yyyy-MM-dd")
            FileReturn = GenerateReportXMLNew(datetransaction, jenislaporan, datelastupdate, strdirpath)
            lstFileZip.Add(FileReturn)
        Next

        Dim fileNamezip As String = "CombineAll.zip"
        If lstFileZip.Count > 1 Then
            Using zip As New ZipFile()
                zip.CompressionLevel = CompressionLevel.Level9
                zip.AddFiles(lstFileZip, "")
                zip.Save(fileNamezip)

            End Using
        End If
        If lstFileZip.Count = 1 Then
            Return lstFileZip(0)
        ElseIf lstFileZip.Count > 1 Then
            Return fileNamezip
        End If
    End Function

    Public Shared Function GenerateReportXMLAll(DStartDate As Date, kode As String, session As String, path As String, con As String, filenamexml As String, filenamezip As String, formatdate As String, list As List(Of goAML_Report)) As String
        Using objdb As NawaDatadevEntities = New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try
                    Dim lstFIle As New List(Of String)
                    Dim lstFIleAttachment As New List(Of String)
                    Dim lstFileZip As New List(Of String)
                    Dim lstFileZipAttachment As New List(Of String)
                    Dim lstFileZipZip As New List(Of String)
                    Dim intjmlFiledalam1zip As Integer
                    Dim intcounter As Integer
                    Dim FileNameZipReturn As String = ""
                    Dim filezipzip As String = ""
                    Dim pathAttachment As String = ""
                    Dim pathAttachmentzip As String = ""
                    Dim file As String
                    Dim filezip As String

                    intjmlFiledalam1zip = 1000
                    intcounter = 0
                    lstFIle.Clear()

                    Dim datetransaction As String = DStartDate.Date.ToString("yyyy-MM-dd")
                    Dim objgoAML As List(Of goAML_Report)
                    If list.Count > 0 Then
                        objgoAML = GlobalReportFunctionBLL.getListReportGenerateXML(datetransaction, kode, list)
                    Else
                        objgoAML = GlobalReportFunctionBLL.getListReportGenerateXML(datetransaction, kode)
                    End If

                    For Each item As goAML_Report In objgoAML
                        If item.isValid Then
                            intcounter += 1
                            Dim objgoAMLTransaction As goAML_Transaction = objdb.goAML_Transaction.Where(Function(x) x.FK_Report_ID = item.PK_Report_ID).FirstOrDefault

                            If item.Status = 1 Then
                                Try
                                    With item
                                        .Submission_Date = Now.Date
                                    End With
                                    objdb.Entry(item).State = EntityState.Modified
                                    objdb.SaveChanges()
                                Catch ex As Exception
                                    objTrans.Rollback()
                                    Throw ex
                                End Try
                            End If

                            If GlobalReportFunctionBLL.getJenisLaporanAttachment(item.Report_Code) = "LTKM/STR" Then
                                If lstFIleAttachment.Count > 0 Then
                                    lstFIleAttachment.Clear()
                                End If
                                'For Each files As String In My.Computer.FileSystem.GetFiles("C:\Users\NAWADATA\Source\Repos\goaml\Development\Web\Web\goAML\DikumenSAR\" & item.PK_Report_ID, FileIO.SearchOption.SearchAllSubDirectories, "*")
                                '    lstFIleAttachment.Add(files)
                                'Next
                                For Each files As String In My.Computer.FileSystem.GetFiles(SystemParameterBLL.GetSystemParameterByPk(9018).SettingValue & item.PK_Report_ID, FileIO.SearchOption.SearchAllSubDirectories, "*")
                                    lstFIleAttachment.Add(files)
                                Next
                                'pathAttachmentzip = "C:\Users\NAWADATA\Source\Repos\goaml\Development\Web\Web\goAML\DikumenSAR\" & DateTime.Today.Date.ToString(formatdate) & "-Attachment-" & item.PK_Report_ID & ".zip"     'sementara
                                pathAttachmentzip = SystemParameterBLL.GetSystemParameterByPk(9018).SettingValue & DateTime.Today.Date.ToString(formatdate) & "-Attachment-" & item.PK_Report_ID & ".zip"
                                If pathAttachment IsNot Nothing Then
                                    Using zip As New ZipFile()

                                        zip.CompressionLevel = CompressionLevel.Level9
                                        zip.AddFiles(lstFIleAttachment, "")

                                        zip.Save(pathAttachmentzip)
                                        lstFileZipAttachment.Add(pathAttachmentzip)
                                    End Using
                                End If
                            End If
                            file = path & DateTime.Today.Date.ToString(formatdate) & "-Report-" & intcounter & "-" & item.PK_Report_ID & ".xml"
                            filezip = path & DateTime.Today.Date.ToString(formatdate) & "-Report-" & filenamezip

                            If lstFIleAttachment.Count > 0 Then
                                filezipzip = path & DateTime.Today.Date.ToString(formatdate) & "-" & "All.zip"
                            End If

                            lstFIle.Add(file)
                            Dim sqlCon As SqlConnection = New SqlConnection(con)
                            Dim result As String = ""
                            Using zip As New ZipFile()

                                Using (sqlCon)
                                    Dim sqlCom As New SqlCommand()
                                    sqlCom.Connection = sqlCon

                                    If objgoAMLTransaction.FK_Transaction_Type = 1 Then
                                        sqlCom.CommandText = "usp_Generate_XML"
                                    ElseIf objgoAMLTransaction.FK_Transaction_Type = 2 Then
                                        sqlCom.CommandText = "usp_Generate_XML2"
                                    Else
                                        Throw New Exception("Type Transaction Not Valid")
                                    End If
                                    sqlCom.CommandType = CommandType.StoredProcedure
                                    sqlCom.Parameters.AddWithValue("@PK", item.PK_Report_ID)
                                    sqlCom.Parameters.AddWithValue("@TransactionDate", datetransaction)
                                    sqlCom.Parameters.AddWithValue("@Kode", kode)
                                    sqlCom.Parameters.AddWithValue("@Session", session)

                                    sqlCon.Open()

                                    Dim sda As SqlDataAdapter = New SqlDataAdapter(sqlCom)
                                    Dim ds As DataSet = New DataSet("xml")
                                    sda.FillSchema(ds, SchemaType.Source, "xml")
                                    sda.Fill(ds, "xml")
                                    Dim da As DataTable = ds.Tables(0)
                                    result = da.Rows(0)(0).ToString()
                                    sqlCon.Close()

                                    If result = "" Then
                                        Throw New Exception("Data Report Kosong")
                                    End If

                                    UpdateReport(item, objgoAML, intcounter)

                                    Dim objdoc As New XmlDocument
                                    objdoc.LoadXml(result)
                                    objdoc.PreserveWhitespace = False
                                    objdoc.Save(file)

                                    zip.CompressionLevel = CompressionLevel.Level9
                                    If lstFIleAttachment.Count > 0 Then
                                        If lstFIle.Count > 1 Then
                                            zip.AddFiles(lstFIle, "")

                                            zip.Save(filezip)
                                            lstFileZip.Add(filezip)
                                        End If
                                    Else
                                        zip.AddFiles(lstFIle, "")

                                        zip.Save(filezip)
                                        lstFileZip.Add(filezip)
                                    End If

                                End Using
                            End Using
                        End If
                    Next
                    If lstFIleAttachment.Count > 0 Then
                        Using zip As New ZipFile()
                            zip.CompressionLevel = CompressionLevel.Level9
                            zip.AddFiles(lstFileZip, "")
                            zip.AddFiles(lstFileZipAttachment, "")

                            zip.Save(filezipzip)
                            lstFileZipZip.Add(filezipzip)
                        End Using
                    End If

                    If lstFileZipAttachment.Count > 0 Then
                        FileNameZipReturn = lstFileZipZip.Item(0).ToString
                    Else
                        FileNameZipReturn = lstFileZip.Item(0).ToString
                    End If
                    objTrans.Commit()
                    Return FileNameZipReturn
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Function

    Public Shared Function ValidateSchemaALL(objdt As DataTable) As String
        Dim dTransaksi As Date
        Dim dLastupdate As Date
        Dim strJenisLaporan As String
        For Each item As Data.DataRow In objdt.Rows
            dTransaksi = item("transactiondate")
            strJenisLaporan = item("jenislaporan")
            dLastupdate = item("lastupdatedate")
            ValidateSchema(dTransaksi, strJenisLaporan, dLastupdate)

        Next
    End Function

    Public Shared Function ValidateSchema(dtanggalTransaksi As Date, strJenisLaporan As String, LastUpdateDate As Date) As String

        Dim dtreport As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT gar.PK_Report_ID,gar.submission_date,gar.report_code,gar.unikReference,gar.transaction_date FROM GoAML_Report AS gar WHERE  DATEDIFF(DAY, gar.transaction_date,'" & dtanggalTransaksi.ToString("yyyy-MM-dd") & "')=0 AND gar.isValid=1 and report_code='" & strJenisLaporan & "' AND DATEDIFF(DAY, gar.LastUpdateDate,'" & LastUpdateDate.ToString("yyyy-MM-dd") & "')=0 and [status] not in (4,5)", Nothing)

        Dim strxsd As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT TOP 1 gatsx.XsdXMLData FROM goAML_Template_Schema_Xsd AS gatsx WHERE gatsx.[Active]=1", Nothing)
        Dim ListOfError As New Data.DataTable
        ListOfError.Columns.Add(New DataColumn("TransactionDate", GetType(DateTime)))
        ListOfError.Columns.Add(New DataColumn("JenisLaporan", GetType(String)))
        ListOfError.Columns.Add(New DataColumn("LastUpdateDate", GetType(DateTime)))
        ListOfError.Columns.Add(New DataColumn("FK_Report_ID", GetType(Long)))
        ListOfError.Columns.Add(New DataColumn("ErrorMessage", GetType(String)))




        For Each item As DataRow In dtreport.Rows
            Dim pkreportid As Long = item(0)
            'create xmlnya
            Dim objParam(1) As SqlParameter
            objParam(0) = New SqlParameter
            objParam(0).ParameterName = "@PK"
            objParam(0).Value = pkreportid

            objParam(1) = New SqlParameter
            objParam(1).ParameterName = "@session"
            objParam(1).Value = NawaBLL.Common.SessionCurrentUser.UserID

            Dim strresult As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_generate_XMLNew", objParam)

            LoadValidatedXmlDocumentfromstring(strresult, strxsd, ListOfError, dtanggalTransaksi, strJenisLaporan, LastUpdateDate, pkreportid)
            'validate

        Next

        If ListOfError.Rows.Count >= 0 Then
            Dim objParam(3) As SqlClient.SqlParameter

            objParam(0) = New SqlClient.SqlParameter
            objParam(0).ParameterName = "@tanggaltransaksi"
            objParam(0).Value = dtanggalTransaksi

            objParam(1) = New SqlClient.SqlParameter
            objParam(1).ParameterName = "@jenislaporan"
            objParam(1).Value = strJenisLaporan

            objParam(2) = New SqlClient.SqlParameter
            objParam(2).ParameterName = "@lastupdatedate"
            objParam(2).Value = LastUpdateDate



            objParam(3) = New SqlClient.SqlParameter
            objParam(3).ParameterName = "@tblValidationXSD"
            objParam(3).Value = ListOfError

            NawaDAL.SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SaveValidateXSD", objParam)


        End If

    End Function

    Public Shared Function LoadValidatedXmlDocumentfromstring(stringxml As String, stringxsd As String, ByRef ListofError As Data.DataTable, dtanggalTransaksi As Date, strJenisLaporan As String, LastUpdateDate As Date, pkreportid As Long) As XmlDocument
        Dim doc As New XmlDocument()
        Dim objxsdreader As XmlReader = XmlReader.Create(New StringReader(stringxsd))
        doc.LoadXml(stringxml)
        doc.Schemas.Add(Nothing, objxsdreader)

        Dim errorBuilder As New XmlValidationErrorBuilder()
        doc.Validate(New ValidationEventHandler(AddressOf errorBuilder.ValidationEventHandler))
        Dim listerror As List(Of String) = errorBuilder.GetErrors()

        If listerror.Count > 0 Then
            For Each item As String In listerror
                Dim objnewError As Data.DataRow = ListofError.NewRow
                With objnewError
                    objnewError.Item("TransactionDate") = dtanggalTransaksi
                    objnewError.Item("JenisLaporan") = strJenisLaporan
                    objnewError.Item("LastUpdateDate") = LastUpdateDate
                    objnewError.Item("FK_Report_ID") = pkreportid
                    objnewError.Item("ErrorMessage") = item
                End With
                ListofError.Rows.Add(objnewError)
            Next

        End If
        Return doc
    End Function

    Public Shared Function GenerateReportXMLNew(dtanggalTransaksi As Date, strJenisLaporan As String, LastUpdateDate As Date, strdirpath As String) As String

        Dim retzipfile As String = ""

        ''update tanggallaporan jadi tanggal hari ini
        SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "UPDATE GoAML_Report SET submission_date = FORMAT(GETDATE(),'yyyyMMdd'), Status = 2 WHERE DATEDIFF(DAY, transaction_date,'" & dtanggalTransaksi.ToString("yyyy-MM-dd") & "')=0 AND report_code='" & strJenisLaporan & "' AND DATEDIFF(DAY, LastUpdateDate,'" & LastUpdateDate & "')=0 AND isValid=1 and [status] <> 4", Nothing)

        SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "UPDATE goAML_Generate_XML SET Last_Generate_Date = GETDATE(), Status = 'Waiting For Upload Reference No' WHERE Transaction_Date='" & dtanggalTransaksi.ToString("yyyy-MM-dd") & "' AND Jenis_Laporan='" & strJenisLaporan & "' AND Last_Update_Date='" & LastUpdateDate & "'", Nothing)


        ''filter based on tanggaltransaksi,jenislaporan dan isvalid=1
        Dim dtreport As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT gar.PK_Report_ID,gar.submission_date,gar.report_code,gar.unikReference,gar.transaction_date,isnull(b.ReportType,'') ReportType FROM GoAML_Report AS gar LEFT JOIN goAML_Ref_Jenis_Laporan b ON gar.Report_Code=b.Kode WHERE  DATEDIFF(DAY, gar.transaction_date,'" & dtanggalTransaksi.ToString("yyyy-MM-dd") & "')=0 AND gar.isValid=1 and report_code='" & strJenisLaporan & "' AND DATEDIFF(DAY, gar.LastUpdateDate,'" & LastUpdateDate.ToString("yyyy-MM-dd") & "')=0 and [status] not in (4,5)", Nothing)



        Dim lstFile As New List(Of String)
        Dim lstFileZip As New List(Of String)

        Dim intjmlFiledalam1zip As Integer = 1000
        intjmlFiledalam1zip = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "SELECT garrgp.ParameterValue FROM goAML_Ref_ReportGlobalParameter AS garrgp WHERE garrgp.PK_GlobalReportParameter_ID=11", Nothing)

        Dim intcounter As Integer = 0

        Dim listAttachment As New List(Of String)
        For Each item As DataRow In dtreport.Rows

            Dim pkreportid As Long = item(0)
            Dim strtanggallaporan As String = Convert.ToDateTime(item(1)).ToString("yyyyMMdd")
            Dim strreportcode As String = item(2)
            Dim strunikreference As String = item(3)
            Dim strtanggaltransaksi As String = Convert.ToDateTime(item(4)).ToString("yyyyMMdd")
            Dim strreporttype As String = item(5)
            Dim strTanggalGenerate As String = Convert.ToDateTime(Now()).ToString("yyyyMMddHHmmtt") '' Added By Felix 28 Dec 2020

            Dim objParam(1) As SqlParameter
            objParam(0) = New SqlParameter
            objParam(0).ParameterName = "@PK"
            objParam(0).Value = pkreportid

            objParam(1) = New SqlParameter
            objParam(1).ParameterName = "@session"
            objParam(1).Value = NawaBLL.Common.SessionCurrentUser.UserID

            Dim incrementRow As Int32 ''edited by Felix 16 Nov 2020
            incrementRow = 0
            'Dim strresult As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_generate_XMLNew", objParam)
            Dim xmlDataTable As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_generate_XMLNew", objParam) ''edited by Felix 16 Nov 2020

            For Each itemXML As DataRow In xmlDataTable.Rows ''added by Felix 16 Nov 2020
                Dim strresult As String = itemXML(0) ''added by Felix 16 Nov 2020
                Dim filexml As String ''added by Felix 16 Nov 2020

                If xmlDataTable.Rows.Count > 1 Then
                    incrementRow = incrementRow + 1
                    'filexml = strdirpath & strtanggallaporan & "-" & strtanggaltransaksi & "-" & strreportcode & "-" & strunikreference.Replace("/", "_") & "-[" & pkreportid & "]-" & incrementRow & ".xml"
                    filexml = strdirpath & strtanggallaporan & "-" & strtanggaltransaksi & "-" & strTanggalGenerate & "-" & strreportcode & "-" & strunikreference.Replace("/", "_") & "-[" & pkreportid & "]-" & incrementRow & ".xml" '' Edited by FElix 28 Dec 2020, tambah tanggal generate
                Else
                    'filexml = strdirpath & strtanggallaporan & "-" & strtanggaltransaksi & "-" & strreportcode & "-" & strunikreference.Replace("/", "_") & "-[" & pkreportid & "].xml"
                    filexml = strdirpath & strtanggallaporan & "-" & strtanggaltransaksi & "-" & strTanggalGenerate & "-" & strreportcode & "-" & strunikreference.Replace("/", "_") & "-[" & pkreportid & "].xml" '' Edited by FElix 28 Dec 2020, tambah tanggal generate
                End If


                If strresult = "" Then
                    Throw New Exception("Data Report Empty for pk_goaml_report_Id =" & pkreportid)
                End If

                ''delete kalau sudah pernah ada
                If IO.File.Exists(filexml) Then
                    IO.File.Delete(filexml)
                End If


                Dim objdoc As New XmlDocument
                objdoc.LoadXml(strresult)
                objdoc.PreserveWhitespace = False
                objdoc.Save(filexml)


                '''  uncomment ini supaya kalau str yg ada attachment masuk ke dalam zipnya juga
                '''
                '''

                If strreporttype = "LTKM/STR" Then

                    Dim dtattachment As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT a.[File_Name],a.File_Doc FROM  goAML_ODM_Generate_STR_SAR_Attachment AS a INNER JOIN goAML_ODM_Generate_STR_SAR AS b ON  a.Fk_goAML_ODM_Generate_STR_SAR = b.PK_goAML_ODM_Generate_STR_SAR WHERE b.Fk_Report_ID=" & pkreportid, Nothing)

                    If dtattachment.Rows.Count > 0 Then
                        For Each itemattachment As Data.DataRow In dtattachment.Rows
                            Dim fileattachmentname As String = strdirpath & strtanggallaporan & "-" & strtanggaltransaksi & "-" & strreportcode & "-" & strunikreference & "-[" & pkreportid & "]-" & itemattachment(0)
                            IO.File.WriteAllBytes(fileattachmentname, itemattachment(1))
                            listAttachment.Add(fileattachmentname)

                        Next
                        lstFile.AddRange(listAttachment)
                        lstFile.Add(filexml)
                    Else
                        'gak ada attachment,
                        lstFile.Add(filexml)

                    End If
                Else
                    lstFile.Add(filexml)
                End If

                'lstFile.Add(filexml)
            Next ''added by Felix 16 Nov 2020

            If lstFile.Count = intjmlFiledalam1zip And lstFile.Count > 0 Then
                Using zip As New ZipFile()
                    intcounter += 1
                    Dim fileNamezip As String = strdirpath & strtanggallaporan & "-" & strtanggaltransaksi & "-" & Right("00" & intcounter, 3) & "-" & strreportcode & ".zip"

                    If IO.File.Exists(fileNamezip) Then
                        IO.File.Delete(fileNamezip)
                    End If
                    zip.CompressionLevel = CompressionLevel.Level9
                    zip.AddFiles(lstFile, "")

                    zip.Save(fileNamezip)
                    lstFileZip.Add(fileNamezip)
                End Using

                'buang karena sudah di zip
                For Each Item1 As String In lstFile
                    IO.File.Delete(Item1)
                Next



                lstFile.Clear()
            End If

        Next
        ' ini kalau masih ada sisa
        If lstFile.Count > 0 Then

            Using zip As New ZipFile()
                intcounter += 1
                Dim fileNamezip As String = strdirpath & Now.ToString("yyyyMMdd") & "-" & dtanggalTransaksi.ToString("yyyyMMdd") & "-" & Right("00" & intcounter, 3) & "-" & strJenisLaporan & ".zip"

                If IO.File.Exists(fileNamezip) Then
                    IO.File.Delete(fileNamezip)
                End If
                zip.CompressionLevel = CompressionLevel.Level9
                zip.AddFiles(lstFile, "")

                zip.Save(fileNamezip)
                lstFileZip.Add(fileNamezip)

            End Using

            'buang karena sudah di zip
            For Each Item1 As String In lstFile
                IO.File.Delete(Item1)
            Next
            lstFile.Clear()

        End If

        'Update BSIM-Ryn-11Nov2020
        'ganti kondisi lstFileZip yang count > 1 menjadi > 0 
        '------------------------
        'update by hendra 25 nov 2020
        'diubah balik  jadi >1, tujuan nya kalau list file zipnya ada >1 baru di zip lagi dengan nama ALL
        'jadi harusnya kalau cuma 1 gak akan masuk if bawah ini
        '------------------------
        If lstFileZip.Count > 1 Then
            Using zip As New ZipFile()

                Dim dfiledatename As String
                dfiledatename = Now.ToString("yyyyMMdd")


                Dim fileNamezip As String = strdirpath & dfiledatename & "-" & dtanggalTransaksi.ToString("yyyyMMdd") & "-All-" & strJenisLaporan & ".zip"
                If IO.File.Exists(fileNamezip) Then
                    IO.File.Delete(fileNamezip)
                End If
                zip.CompressionLevel = CompressionLevel.Level9
                zip.AddFiles(lstFileZip, "")
                zip.Save(fileNamezip)

                For Each Item1 As String In lstFileZip
                    IO.File.Delete(Item1)
                Next

                retzipfile = fileNamezip
            End Using
        ElseIf lstFileZip.Count = 1 Then

            retzipfile = lstFileZip.Item(0).ToString

        Else

            'Update BSIM-Ryn-09Nov2020 
            'Remark bagian array karena akan error sql diganti dengan error message dari query dtreport terdapat filter status not in 4 dan 5

            Throw New Exception("Report status Waiting for Approval or Need to Correction")
        End If
        Return retzipfile

    End Function

    ''calon di buang kalau yg atas sudah done
    Public Shared Function GenerateReportXML(DStartDate As Date, kode As String, session As String, path As String, con As String, filenamexml As String, filenamezip As String, formatdate As String, statusReport As String) As String
        Using objdb As NawaDatadevEntities = New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try
                    Dim lstFIle As New List(Of String)
                    Dim lstFIleAttachment As New List(Of String)
                    Dim lstFileZip As New List(Of String)
                    Dim lstFileZipAttachment As New List(Of String)
                    Dim lstFileZipZip As New List(Of String)
                    Dim intjmlFiledalam1zip As Integer
                    Dim intcounter As Integer
                    Dim FileNameZipReturn As String = ""
                    Dim filezipzip As String = ""
                    Dim pathAttachment As String = ""
                    Dim pathAttachmentzip As String = ""
                    Dim file As String
                    Dim filezip As String
                    Dim a As Boolean = False

                    intjmlFiledalam1zip = 1000
                    intcounter = 0
                    lstFIle.Clear()

                    Dim datetransaction As String = DStartDate.Date.ToString("yyyy-MM-dd")

                    Dim status As Integer = GlobalReportFunctionBLL.getStatusReportByCodeInt(statusReport)
                    Dim objgoAML As List(Of goAML_Report) = GlobalReportFunctionBLL.getListReportGenerateXML(datetransaction, kode, status)

                    For Each item As goAML_Report In objgoAML
                        If item.isValid Then
                            intcounter += 1
                            Dim objgoAMLTransaction As goAML_Transaction = objdb.goAML_Transaction.Where(Function(x) x.FK_Report_ID = item.PK_Report_ID).FirstOrDefault

                            If item.Status = 1 And a = False Then
                                Using objdbo As NawaDatadevEntities = New NawaDatadevEntities
                                    Using objTranso As DbContextTransaction = objdbo.Database.BeginTransaction
                                        Try
                                            For Each items As goAML_Report In objgoAML
                                                With items
                                                    .Submission_Date = Now.Date
                                                End With
                                                objdbo.Entry(items).State = EntityState.Modified
                                            Next
                                            a = True
                                            objdbo.SaveChanges()
                                            objTranso.Commit()
                                            Dim obj As goAML_Generate_XML = GlobalReportFunctionBLL.getListGenerateXMLNew(datetransaction, kode)
                                            With obj
                                                .Total_Valid = .Total_Valid - objgoAML.Count
                                            End With
                                            objdb.Entry(obj).State = EntityState.Modified
                                            objdb.SaveChanges()
                                            objTrans.Commit()
                                        Catch ex As Exception
                                            objTrans.Rollback()
                                            Throw ex
                                        End Try
                                    End Using
                                End Using

                            End If

                            If GlobalReportFunctionBLL.getJenisLaporanAttachment(item.Report_Code) = "LTKM/STR" Then
                                If lstFIleAttachment.Count > 0 Then
                                    lstFIleAttachment.Clear()
                                End If
                                'For Each files As String In My.Computer.FileSystem.GetFiles("C:\Users\NAWADATA\Source\Repos\goaml\Development\Web\Web\goAML\DikumenSAR\" & item.PK_Report_ID, FileIO.SearchOption.SearchAllSubDirectories, "*")
                                '    lstFIleAttachment.Add(files)
                                'Next
                                For Each files As String In My.Computer.FileSystem.GetFiles(SystemParameterBLL.GetSystemParameterByPk(9018).SettingValue & item.PK_Report_ID, FileIO.SearchOption.SearchAllSubDirectories, "*")
                                    lstFIleAttachment.Add(files)
                                Next
                                'pathAttachmentzip = "C:\Users\NAWADATA\Source\Repos\goaml\Development\Web\Web\goAML\DikumenSAR\" & DateTime.Today.Date.ToString(formatdate) & "-Attachment-" & item.PK_Report_ID & ".zip"     'sementara
                                pathAttachmentzip = SystemParameterBLL.GetSystemParameterByPk(9018).SettingValue & DateTime.Today.Date.ToString(formatdate) & "-Attachment-" & item.PK_Report_ID & ".zip"
                                If pathAttachment IsNot Nothing Then
                                    Using zip As New ZipFile()

                                        zip.CompressionLevel = CompressionLevel.Level9
                                        zip.AddFiles(lstFIleAttachment, "")

                                        zip.Save(pathAttachmentzip)
                                        lstFileZipAttachment.Add(pathAttachmentzip)
                                    End Using
                                End If
                            End If
                            file = path & DateTime.Today.Date.ToString(formatdate) & "-Report-" & intcounter & "-" & item.PK_Report_ID & ".xml"
                            filezip = path & DateTime.Today.Date.ToString(formatdate) & "-Report-" & filenamezip

                            If lstFIleAttachment.Count > 0 Then
                                filezipzip = path & DateTime.Today.Date.ToString(formatdate) & "-" & "All.zip"
                            End If

                            lstFIle.Add(file)
                            Dim sqlCon As SqlConnection = New SqlConnection(con)
                            Dim result As String = ""
                            Using zip As New ZipFile()

                                Using (sqlCon)
                                    Dim sqlCom As New SqlCommand()
                                    sqlCom.Connection = sqlCon

                                    If objgoAMLTransaction.FK_Transaction_Type = 1 Then
                                        sqlCom.CommandText = "usp_Generate_XML"
                                    ElseIf objgoAMLTransaction.FK_Transaction_Type = 2 Then
                                        sqlCom.CommandText = "usp_Generate_XML2"
                                    Else
                                        Throw New Exception("Type Transaction Not Valid")
                                    End If
                                    sqlCom.CommandType = CommandType.StoredProcedure
                                    sqlCom.Parameters.AddWithValue("@PK", item.PK_Report_ID)
                                    sqlCom.Parameters.AddWithValue("@TransactionDate", datetransaction)
                                    sqlCom.Parameters.AddWithValue("@Kode", kode)
                                    sqlCom.Parameters.AddWithValue("@Session", session)

                                    sqlCon.Open()

                                    Dim sda As SqlDataAdapter = New SqlDataAdapter(sqlCom)
                                    Dim ds As DataSet = New DataSet("xml")
                                    sda.FillSchema(ds, SchemaType.Source, "xml")
                                    sda.Fill(ds, "xml")
                                    Dim da As DataTable = ds.Tables(0)
                                    result = da.Rows(0)(0).ToString()
                                    sqlCon.Close()

                                    If result = "" Then
                                        Throw New Exception("Data Report Kosong")
                                    End If

                                    UpdateReport(item, objgoAML, intcounter)

                                    Dim objdoc As New XmlDocument
                                    objdoc.LoadXml(result)
                                    objdoc.PreserveWhitespace = False
                                    objdoc.Save(file)

                                    zip.CompressionLevel = CompressionLevel.Level9
                                    If lstFIleAttachment.Count > 0 Then
                                        If lstFIle.Count > 1 Then
                                            zip.AddFiles(lstFIle, "")

                                            zip.Save(filezip)
                                            lstFileZip.Add(filezip)
                                        End If
                                    Else
                                        zip.AddFiles(lstFIle, "")

                                        zip.Save(filezip)
                                        lstFileZip.Add(filezip)
                                    End If

                                End Using
                            End Using
                        End If
                    Next
                    If lstFIleAttachment.Count > 0 Then
                        Using zip As New ZipFile()
                            zip.CompressionLevel = CompressionLevel.Level9
                            zip.AddFiles(lstFileZip, "")
                            zip.AddFiles(lstFileZipAttachment, "")

                            zip.Save(filezipzip)
                            lstFileZipZip.Add(filezipzip)
                        End Using
                    End If

                    'If lstFIle.Count > 0 Then

                    '    Using zip As New ZipFile()
                    '        intcounter += 1
                    '        Dim filename = path & DateTime.Today.Date.ToString(formatdate) & filenamezip

                    '        If IO.File.Exists(filename) Then
                    '            IO.File.Delete(filename)
                    '        End If

                    '        zip.CompressionLevel = CompressionLevel.Level9
                    '        zip.AddFiles(lstFIle, "")
                    '        zip.Save(filename)
                    '        lstFileZip.Add(filename)
                    '        FileNameZipReturn = filename
                    '    End Using

                    '    Return FileNameZipReturn
                    'Else
                    If lstFileZipAttachment.Count > 0 Then
                        FileNameZipReturn = lstFileZipZip.Item(0).ToString
                    Else
                        FileNameZipReturn = lstFileZip.Item(0).ToString
                    End If
                    'objTrans.Commit()
                    'End If
                    Return FileNameZipReturn
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Function

    Private Shared Sub UpdateReport(obj As goAML_Report, listobj As List(Of goAML_Report), counter As Integer)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objGenerateXML As List(Of goAML_Generate_XML) = GlobalReportFunctionBLL.getListGenerateXML(obj.Transaction_Date, obj.Report_Code, Now.Date.ToString())
                    If objGenerateXML.Count > 1 Then

                        With obj
                            .Status = 2
                            .LastUpdateDate = Now.Date
                        End With
                        objDB.Entry(obj).State = EntityState.Modified
                        objDB.SaveChanges()

                        Dim newobjxmlvalid As goAML_Generate_XML = GlobalReportFunctionBLL.getListGenerateXMLNew(obj.Transaction_Date, obj.Report_Code)
                        If newobjxmlvalid.NO_ID <> 0 Then
                            With newobjxmlvalid
                                .Status = GlobalReportFunctionBLL.getStatusReportByCode(2).ToString
                                .Last_Generate_Date = Now.Date
                                .Last_Update_Date = Now.Date
                                .LastUpdateDate = Now.Date
                            End With
                            objDB.Entry(newobjxmlvalid).State = EntityState.Modified
                            objDB.SaveChanges()
                            objTrans.Commit()
                        End If

                        Dim oldobjxmlvalid As goAML_Generate_XML = GlobalReportFunctionBLL.getListGenerateXMLOld(obj.Transaction_Date, obj.Report_Code)
                        If newobjxmlvalid.Last_Update_Date = oldobjxmlvalid.Last_Update_Date Then
                            With oldobjxmlvalid
                                .Total_Valid = .Total_Valid + 1
                                .Last_Generate_Date = Now.Date
                            End With
                            objDB.Entry(oldobjxmlvalid).State = EntityState.Modified
                            objDB.SaveChanges()

                            With newobjxmlvalid
                                .Total_Valid = .Total_Valid - 1
                            End With
                            objDB.Entry(newobjxmlvalid).State = EntityState.Modified
                            objDB.SaveChanges()

                            If newobjxmlvalid.Total_Valid = 0 And newobjxmlvalid.Total_Invalid = 0 Then
                                objDB.Entry(newobjxmlvalid).State = EntityState.Deleted
                                objDB.SaveChanges()
                            End If
                            objTrans.Commit()
                        End If
                    Else
                        Dim newobjxmlvalid As goAML_Generate_XML = GlobalReportFunctionBLL.getListGenerateXMLNew(obj.Transaction_Date, obj.Report_Code)
                        If newobjxmlvalid.NO_ID <> 0 Then
                            If obj.Status = 1 Then
                                With obj
                                    .Status = 2
                                    .LastUpdateDate = Now.Date
                                End With
                                objDB.Entry(obj).State = EntityState.Modified
                                objDB.SaveChanges()
                            End If

                            If newobjxmlvalid.Total_Invalid > 0 Then
                                Dim objxml As New goAML_Generate_XML
                                With objxml
                                    .Jenis_Laporan = obj.Report_Code
                                    .Report_Type = GlobalReportFunctionBLL.getReportTypebyPK(obj.FK_Report_Type_ID).ToString
                                    .Transaction_Date = obj.Transaction_Date
                                    .Total_Invalid = 0
                                    .Last_Update_Date = Now.Date
                                    .LastUpdateDate = Now.Date
                                    .Total_Valid = 1
                                    .Last_Generate_Date = Now.Date
                                    .Status = GlobalReportFunctionBLL.getStatusReportByCode(2).ToString
                                End With
                                objDB.Entry(objxml).State = EntityState.Added
                                objDB.SaveChanges()
                            Else
                                With newobjxmlvalid
                                    '.Total_Valid = .Total_Valid - 1
                                    .Status = GlobalReportFunctionBLL.getStatusReportByCode(2).ToString
                                    .Last_Generate_Date = Now.Date
                                    If listobj.Count = counter Then
                                        If .Last_Update_Date Is Nothing Then
                                            .Last_Update_Date = Date.Now
                                            .LastUpdateDate = Date.Now
                                            .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        End If
                                    End If
                                End With
                                objDB.Entry(newobjxmlvalid).State = EntityState.Modified
                                objDB.SaveChanges()
                            End If
                        Else
                            Dim oldobjxmlvalid As goAML_Generate_XML = GlobalReportFunctionBLL.getListGenerateXMLOld(obj.Transaction_Date, obj.Report_Code)
                            With oldobjxmlvalid
                                .Last_Generate_Date = Now.Date
                            End With
                            objDB.Entry(oldobjxmlvalid).State = EntityState.Modified
                            objDB.SaveChanges()
                        End If
                        objTrans.Commit()
                    End If
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try

            End Using
        End Using

    End Sub

End Class
