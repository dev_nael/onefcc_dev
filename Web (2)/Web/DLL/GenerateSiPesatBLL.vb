﻿Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Xml
Imports Ionic.Zip
Imports Ionic.Zlib
Imports NawaBLL
Imports NawaDAL
Imports NawaDevDAL
Imports System.Xml.Schema
Imports System.IO

Public Class GenerateSiPesatBLL
    Public Shared Function GetGeneratedSiPesatByID(id As Integer) As AML_Sipesat_GeneratedFileList
        Using objdb As NawaDatadevEntities = New NawaDatadevEntities
            Return objdb.AML_Sipesat_GeneratedFileList.Where(Function(x) x.PK_Sipesat_GeneratedFileList_ID = id).FirstOrDefault
        End Using
    End Function

    Shared Function getStatusReportByPK(ID As String) As String
        Dim statusName As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim status As NawaDevDAL.goAML_Ref_Status_Report = objdb.goAML_Ref_Status_Report.Where(Function(x) x.PK_Status_Report_ID = ID).FirstOrDefault
            If status IsNot Nothing Then
                statusName = status.Description
            End If
        End Using
        Return statusName
    End Function
    Public Shared Function GenerateReportXMLNew(idpjk As String, Periode As String, JenisInformasi As String, strdirpath As String) As String
        Dim retzipfile As String = ""
        Dim lstFile As New List(Of String)
        Dim lstFileZip As New List(Of String)
        Dim namefile As String = ""

        If JenisInformasi = 1 Then
            namefile = "SIPESAT_" + idpjk + "_IN_" + Date.Now.ToString("ddMMyyyy")
        ElseIf JenisInformasi = 2 Then
            namefile = "SIPESAT_" + idpjk + "_TW_" + Date.Now.ToString("ddMMyyyy")
        Else
            namefile = "SIPESAT_" + idpjk + "_" + Date.Now.ToString("ddMMyyyy")
        End If

        Dim objParam(1) As SqlParameter
        objParam(0) = New SqlParameter
        objParam(0).ParameterName = "@Periode"
        objParam(0).Value = Periode

        objParam(1) = New SqlParameter
        objParam(1).ParameterName = "@Tipe"
        objParam(1).Value = JenisInformasi

        Dim strresult As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GenerateXMLSiPesat", objParam)

        Dim filexml As String = strdirpath & namefile & ".xml"

        If strresult = "" Then
            Throw New Exception("Data Report Kosong Untuk Periode = " & Periode & " Dan Jenis Informasi = " & JenisInformasi)
        End If

        ''delete kalau sudah pernah ada
        If IO.File.Exists(filexml) Then
            IO.File.Delete(filexml)
        End If


        Dim objdoc As New XmlDocument
        objdoc.LoadXml(strresult)
        objdoc.PreserveWhitespace = False
        objdoc.Save(filexml)

        lstFile.Add(filexml)

        If lstFile.Count > 0 Then
            Using zip As New ZipFile()
                Dim fileNamezip As String = strdirpath & namefile & ".zip"

                If IO.File.Exists(fileNamezip) Then
                    IO.File.Delete(fileNamezip)
                End If
                zip.CompressionLevel = CompressionLevel.Level9
                zip.AddFiles(lstFile, "")

                zip.Save(fileNamezip)
                lstFileZip.Add(fileNamezip)
            End Using

            'buang karena sudah di zip
            For Each Item1 As String In lstFile
                IO.File.Delete(Item1)
            Next

            lstFile.Clear()
        End If

        retzipfile = lstFileZip.Item(0).ToString

        Return retzipfile

    End Function

    Shared Function GenerateData(objmodule As NawaDAL.Module, id As String)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim actsUpdate As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim modulename As String = objmodule.ModuleName
                    Dim objGeneratedList As New AML_Sipesat_GeneratedFileList
                    Dim objGeneratedListOld As New AML_Sipesat_GeneratedFileList

                    objGeneratedListOld = GetGeneratedSiPesatByID(id)
                    objGeneratedList = GetGeneratedSiPesatByID(id)

                    With objGeneratedList
                        .Status_Report = 2
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                        .LastUpdateDate = Now
                    End With
                    objdb.Entry(objGeneratedList).State = EntityState.Modified

                    Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, actsUpdate, modulename)

                    NawaFramework.CreateAuditTrailDetailEdit(objdb, objaudittrailheader.PK_AuditTrail_ID, objGeneratedList, objGeneratedListOld)

                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Function


    Shared Function SubmitionData(objmodule As NawaDAL.Module, id As String, no_ref As String, no_ref_date As Date)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim actsUpdate As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim modulename As String = objmodule.ModuleName
                    Dim objGeneratedList As New AML_Sipesat_GeneratedFileList
                    Dim objGeneratedListOld As New AML_Sipesat_GeneratedFileList

                    objGeneratedListOld = GetGeneratedSiPesatByID(id)
                    objGeneratedList = GetGeneratedSiPesatByID(id)

                    With objGeneratedList
                        .Status_Report = 3
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                        .LastUpdateDate = Now
                        .Submit_Date = Now
                        .No_Referensi = no_ref
                        .Report_PPATK_Date = no_ref_date
                    End With
                    objdb.Entry(objGeneratedList).State = EntityState.Modified

                    Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, actsUpdate, modulename)


                    NawaFramework.CreateAuditTrailDetailEdit(objdb, objaudittrailheader.PK_AuditTrail_ID, objGeneratedList, objGeneratedListOld)

                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Function

End Class
