﻿Imports System.Data.SqlClient
Imports CRSDAL

Public Class CRSDomesticBLL

    Enum enumActionForm
        Detail = 0
        Add = 1
        Edit = 2
        Delete = 3
        Accept = 4
        Reject = 5
    End Enum

    ' 13 Juli 2022 Ari : Tambah untuk get data old BU 
    Shared Function GetCRSDOMReportClassByID(ID As Long) As CRS_Domestic_Report_BU_Class
        Try
            Using objDb As New CRSDAL.CRSEntities

                Dim objCRSDOMReportClass = New CRS_Domestic_Report_BU_Class
                With objCRSDOMReportClass
                    'Reporting Header
                    Dim ObjCRSDomesticBU As New CRS_DOMESTIC_HEADER_BUSINESSUNIT
                    ObjCRSDomesticBU = objDb.CRS_DOMESTIC_HEADER_BUSINESSUNIT.Where(Function(x) x.PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID = ID).FirstOrDefault

                    .CRS_Domestic_Header_BusinessUnit = ObjCRSDomesticBU
                    Dim IDTransaction As Integer = 0
                    Dim strQuerypk2 As String = "select FK_CRS_DOMESTIC_HEADER_ID from CRS_DOMESTIC_HEADER_BUSINESSUNIT where PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID = " & ID.ToString
                    Dim pk2 As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerypk2, Nothing)
                    With .CRS_Domestic_Header_BusinessUnit
                        If .JumlahNilaiSaldo Is Nothing Then
                            .JumlahNilaiSaldo = 0
                        End If

                    End With

                    Dim strQuerycode As String = "select FK_BusinessUnit_Code from CRS_DOMESTIC_HEADER_BUSINESSUNIT where PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID = " & ID.ToString
                    Dim code As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerycode, Nothing)

                    'Data Rekening
                    Dim listrekening = objDb.CRS_DOMESTIC_DATA_REKENING.Where(Function(x) x.FK_BusinessUnit_Code = code And x.FK_CRS_DOMESTIC_HEADER_ID = pk2).ToList
                    If listrekening IsNot Nothing Then
                        .list_CRS_Domestic_Data_Rekening_BusinessUnit = listrekening
                        For Each item In listrekening

                        Next
                    End If


                End With

                Return objCRSDOMReportClass
            End Using
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Shared Function GetDataRekeningClassByID(ID As Long) As CRS_Data_Rekening_Class
        Using objDb As New CRSDAL.CRSEntities

            Dim objCRSDomesticDataRekeningClass = New CRS_Data_Rekening_Class
            With objCRSDomesticDataRekeningClass
                'Reporting Group
                .CRS_Domestic_Data_Rekening = objDb.CRS_DOMESTIC_DATA_REKENING.Where(Function(x) x.PK_CRS_DOMESTIC_DATA_REKENING_ID = ID).FirstOrDefault

                If .CRS_Domestic_Data_Rekening IsNot Nothing Then

                    'Pengendali Entitas
                    If .list_CRS_Domestic_Pengendali_Entitas IsNot Nothing AndAlso .list_CRS_Domestic_Pengendali_Entitas.Count > 0 Then
                        For Each item In .list_CRS_Domestic_Pengendali_Entitas
                            Dim listpengendalientitas = objDb.CRS_DOMESTIC_PENGENDALI_ENTITAS.Where(Function(x) x.FK_CRS_DOMESTIC_DATA_REKENING_ID = item.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID).ToList
                            If listpengendalientitas IsNot Nothing Then .list_CRS_Domestic_Pengendali_Entitas.AddRange(listpengendalientitas)
                        Next
                    Else
                        Dim listpengendalientitas = objDb.CRS_DOMESTIC_PENGENDALI_ENTITAS.Where(Function(x) x.FK_CRS_DOMESTIC_DATA_REKENING_ID = ID).ToList
                        If listpengendalientitas IsNot Nothing Then .list_CRS_Domestic_Pengendali_Entitas.AddRange(listpengendalientitas)
                    End If


                End If

            End With

            Return objCRSDomesticDataRekeningClass
        End Using
    End Function

    Shared Sub DeleteDataRekeningByDataRekeningID_AuditTrail(objData_Old As CRS_Data_Rekening_Class, objDB As CRSEntities, objModule As NawaDAL.Module)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("CRS_DOMESTIC_DATA_REKENING")

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Data Rekening ', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim objAuditTrailheader As CRSDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)

            'Save Audit Trail Detail by XML
            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

            Dim listdatarekeningtodelete As New List(Of CRS_DOMESTIC_DATA_REKENING)
            listdatarekeningtodelete.Add(objData_Old.CRS_Domestic_Data_Rekening)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, listdatarekeningtodelete)


            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_CRS_Domestic_Pengendali_Entitas)


            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Data Rekening', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveDataRekening_AuditTrail(objData As CRS_Data_Rekening_Class, objData_Old As CRS_Data_Rekening_Class, objDB As CRSEntities, objModule As NawaDAL.Module, intModuleAction As Integer)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("CRS_DOMESTIC_DATA_REKENING")

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Data Rekening', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim objAuditTrailheader As CRSDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)

            'Save Audit Trail Detail by XML
            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

            Dim listDataRekening As New List(Of CRS_DOMESTIC_DATA_REKENING)
            Dim listDataRekening_Old As New List(Of CRS_DOMESTIC_DATA_REKENING)


            listDataRekening.Add(objData.CRS_Domestic_Data_Rekening)
            listDataRekening_Old.Add(objData_Old.CRS_Domestic_Data_Rekening)
            NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, listDataRekening, listDataRekening_Old)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_CRS_Domestic_Pengendali_Entitas, objData_Old.list_CRS_Domestic_Pengendali_Entitas)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Data Rekening', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            'End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Shared Function PullDataReportByID(ID As Long) As CRS_Domestic_Report_Class
        Try
            Dim ReportClass As New CRS_Domestic_Report_Class
            With ReportClass
                Using objdb As New CRSDAL.CRSEntities
                    .CRS_Domestic_Header = objdb.CRS_DOMESTIC_HEADER.Where(Function(x) x.PK_CRS_DOMESTIC_HEADER_ID = ID).FirstOrDefault
                    '.list_CRS_Domestic_Data_Rekening = objdb.CRS_DOMESTIC_DATA_REKENING.Where(Function(x) x.FK_CRS_DOMESTIC_HEADER_ID = ID).ToList
                End Using
            End With
            Return ReportClass
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ' 30 Jun 2022 Ari : Pull Data CRS Domestic BU
    Shared Function PullDataReportBUByID(ID As Long) As CRS_Domestic_Report_BU_Class
        Try
            Dim ReportClassBU As New CRS_Domestic_Report_BU_Class
            With ReportClassBU
                Using objdb As New CRSDAL.CRSEntities
                    .CRS_Domestic_Header_BusinessUnit = objdb.CRS_DOMESTIC_HEADER_BUSINESSUNIT.Where(Function(x) x.PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID = ID).FirstOrDefault
                    '.list_CRS_Domestic_Data_Rekening = objdb.CRS_DOMESTIC_DATA_REKENING.Where(Function(x) x.FK_CRS_DOMESTIC_HEADER_ID = ID).ToList
                End Using
            End With
            Return ReportClassBU
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Function PullDataRekeningByID(ID As Long) As CRS_Data_Rekening_Class
        Try
            Dim DataRekeningClass As New CRS_Data_Rekening_Class
            With DataRekeningClass
                Using objdb As New CRSDAL.CRSEntities
                    .CRS_Domestic_Data_Rekening = objdb.CRS_DOMESTIC_DATA_REKENING.Where(Function(x) x.PK_CRS_DOMESTIC_DATA_REKENING_ID = ID).FirstOrDefault
                    .list_CRS_Domestic_Pengendali_Entitas = objdb.CRS_DOMESTIC_PENGENDALI_ENTITAS.Where(Function(x) x.FK_CRS_DOMESTIC_DATA_REKENING_ID = ID).ToList
                End Using
            End With
            Return DataRekeningClass
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Function PullDataModuleApprovalByID(ID As Long) As CRSDAL.ModuleApproval
        Try
            Dim DataModuleApproval As New CRSDAL.ModuleApproval
            Using objdb As New CRSDAL.CRSEntities
                DataModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault
            End Using
            Return DataModuleApproval
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Function PullDataAsDataRow(strTable As String, strTableKeyField As String, strKey As String) As DataRow
        Try
            Dim strSQL As String = "SELECT TOP 1 * FROM " & strTable & " WHERE " & strTableKeyField & "='" & strKey & "'"
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)
            Return drResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Function PullDataAsDataRowSingleString(strQuery As String) As DataRow
        Try
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            Return drResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Function PullDataAsDataTableSingleString(strQuery As String) As DataTable
        Try
            Dim drResult As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Return drResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Sub SaveDataHeader(DataHeaderClass As CRS_Domestic_Report_Class, Action As enumActionForm)
        Using objdb As New CRSDAL.CRSEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    With DataHeaderClass.CRS_Domestic_Header
                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .LastUpdateDate = Now
                        .LJK_Type = "DI"
                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                            .Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                        End If
                    End With
                    Select Case Action
                        Case enumActionForm.Add
                            With DataHeaderClass.CRS_Domestic_Header
                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .CreatedDate = Now
                            End With
                            objdb.Entry(DataHeaderClass.CRS_Domestic_Header).State = Entity.EntityState.Added
                        Case enumActionForm.Edit
                            objdb.Entry(DataHeaderClass.CRS_Domestic_Header).State = Entity.EntityState.Modified
                        Case enumActionForm.Delete
                            Dim strQuery As String = "DELETE from CRS_DOMESTIC_HEADER WHERE PK_CRS_DOMESTIC_HEADER_ID = " & DataHeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                            strQuery = "DELETE from CRS_DOMESTIC_DATA_REKENING WHERE FK_CRS_DOMESTIC_HEADER_ID = " & DataHeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                            strQuery = "DELETE from CRS_DOMESTIC_PENGENDALI_ENTITAS WHERE FK_CRS_DOMESTIC_HEADER_ID = " & DataHeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                            strQuery = "DELETE from CRS_DOMESTIC_GenerateXMLParalel where FK_CRS_DOMESTIC_HEADER_ID = " & DataHeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            'objdb.Entry(DataHeaderClass.CRS_Domestic_Header).State = Entity.EntityState.Deleted
                    End Select
                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    '30 Jun 2022 Ari : Save Data Header BU
    Shared Sub SaveDataHeaderBU(DataHeaderClass As CRS_Domestic_Report_BU_Class, Action As enumActionForm)
        Using objdb As New CRSDAL.CRSEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    '13 Juli 2022 Tambah Audit
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase

                    Dim strQuery As String
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim strModuleName As String = "CRS DOMESTIC HEADER BUSINESS UNIT"
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID

                    strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Report', 'Save Report Started')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                    With DataHeaderClass.CRS_Domestic_Header_BusinessUnit
                        If .JumlahNilaiSaldo Is Nothing Then
                            .JumlahNilaiSaldo = 0
                        End If
                    End With
                    'Get Old Data for Audit Trail
                    Dim objData_Old As New CRS_Domestic_Report_BU_Class
                    With DataHeaderClass.CRS_Domestic_Header_BusinessUnit
                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .LastUpdateDate = Now
                        .LJK_Type = "DI"
                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                            .Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                        End If
                    End With
                    Select Case Action
                        Case enumActionForm.Add
                            With DataHeaderClass.CRS_Domestic_Header_BusinessUnit
                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .CreatedDate = Now
                            End With
                            objdb.Entry(DataHeaderClass.CRS_Domestic_Header_BusinessUnit).State = Entity.EntityState.Added

                        Case enumActionForm.Edit
                            Dim strQueryPKHeader As String = " select isnull(FK_BusinessUnit_Code,'') from CRS_DOMESTIC_HEADER_BUSINESSUNIT WHERE PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID = " & DataHeaderClass.CRS_Domestic_Header_BusinessUnit.PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID
                            Dim codes As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryPKHeader, Nothing)

                            If codes Is Nothing Or String.IsNullOrEmpty(codes) Then
                            Else
                                objData_Old = GetCRSDOMReportClassByID(DataHeaderClass.CRS_Domestic_Header_BusinessUnit.PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID)

                            End If

                            objdb.Entry(DataHeaderClass.CRS_Domestic_Header_BusinessUnit).State = Entity.EntityState.Modified


                        Case enumActionForm.Delete
                            Dim strQuerydelete2 As String = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Header', 'Delete Report started (Report ID = " & DataHeaderClass.CRS_Domestic_Header_BusinessUnit.PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID & ")')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerydelete2, Nothing)

                            Dim strQuerydelete As String = "DELETE from CRS_DOMESTIC_HEADER_BUSINESSUNIT WHERE PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID = " & DataHeaderClass.CRS_Domestic_Header_BusinessUnit.PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerydelete, Nothing)

                            strQuerydelete = "DELETE from CRS_DOMESTIC_DATA_REKENING WHERE FK_CRS_DOMESTIC_HEADER_ID = " & DataHeaderClass.CRS_Domestic_Header_BusinessUnit.FK_CRS_DOMESTIC_HEADER_ID & " AND FK_BusinessUnit_Code = '" & DataHeaderClass.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "' "
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerydelete, Nothing)

                            strQuerydelete = "DELETE from CRS_DOMESTIC_PENGENDALI_ENTITAS WHERE FK_CRS_DOMESTIC_HEADER_ID = " & DataHeaderClass.CRS_Domestic_Header_BusinessUnit.FK_CRS_DOMESTIC_HEADER_ID & " AND FK_BusinessUnit_Code = '" & DataHeaderClass.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "' "
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerydelete, Nothing)

                            strQuerydelete = "DELETE from CRS_DOMESTIC_GenerateXMLParalel where FK_CRS_DOMESTIC_HEADER_ID = " & DataHeaderClass.CRS_Domestic_Header_BusinessUnit.FK_CRS_DOMESTIC_HEADER_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerydelete, Nothing)
                            'objdb.Entry(DataHeaderClass.CRS_Domestic_Header).State = Entity.EntityState.Deleted


                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Header' , 'Delete Report finished (Report ID = " & DataHeaderClass.CRS_Domestic_Header_BusinessUnit.PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID & ")')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    End Select
                    objdb.SaveChanges()
                    objTrans.Commit()

                    Select Case Action
                        Case enumActionForm.Add
                            'Save Audit Trail
                            Dim objAuditTrailheader As New CRSDAL.AuditTrailHeader
                            objAuditTrailheader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)

                            'Save Audit Trail Detail by XML
                            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID
                            Dim listReport As New List(Of CRS_DOMESTIC_HEADER_BUSINESSUNIT)

                            listReport.Add(DataHeaderClass.CRS_Domestic_Header_BusinessUnit)
                            NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, listReport, Nothing)
                        Case enumActionForm.Edit
                            'Save Audit Trail
                            Dim objAuditTrailheader As CRSDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)

                            'Save Audit Trail Detail by XML
                            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

                            Dim listReport As New List(Of CRS_DOMESTIC_HEADER_BUSINESSUNIT)
                            Dim listReport_Old As New List(Of CRS_DOMESTIC_HEADER_BUSINESSUNIT)

                            listReport.Add(DataHeaderClass.CRS_Domestic_Header_BusinessUnit)
                            listReport_Old.Add(objData_Old.CRS_Domestic_Header_BusinessUnit)
                            NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, listReport, listReport_Old)
                    End Select
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub


    Shared Sub SaveDataRekening(DataRekeningClass As CRS_Data_Rekening_Class, Action As enumActionForm)
        Dim oldData As New CRS_Data_Rekening_Class
        Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
        Dim strAlternateID As String = Nothing
        Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
        Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
        Dim strModuleName As String = "CRS DOMESTIC DATA REKENING"
        Dim objModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("CRS_Domestic_Data_Rekening")
        'Get Old Data to Delete
        Dim objData_Old As New CRS_Data_Rekening_Class

        Try

            If Action = enumActionForm.Edit Then
                objData_Old = GetDataRekeningClassByID(DataRekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID)
                If objData_Old Is Nothing Then
                    objData_Old = New CRS_Data_Rekening_Class
                End If
                oldData = PullDataRekeningByID(DataRekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID)
            ElseIf Action = enumActionForm.Delete Then
                objData_Old = GetDataRekeningClassByID(DataRekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID)
                If objData_Old Is Nothing Then
                    objData_Old = New CRS_Data_Rekening_Class
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Using objdb As New CRSDAL.CRSEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    With DataRekeningClass.CRS_Domestic_Data_Rekening
                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .LastUpdateDate = Now
                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                            .Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                        End If
                    End With
                    Select Case Action
                        Case enumActionForm.Add

                            'Insert ke EODLogSP untuk ukur performance
                            Dim strQueryadd As String = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Data Rekening', 'Save Add Data started')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryadd, Nothing)


                            With DataRekeningClass.CRS_Domestic_Data_Rekening
                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .CreatedDate = Now
                            End With
                            objdb.Entry(DataRekeningClass.CRS_Domestic_Data_Rekening).State = Entity.EntityState.Added




                        Case enumActionForm.Edit

                            'Insert ke EODLogSP untuk ukur performance
                            Dim strQueryedit As String = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Data Rekening', 'Save Edit Data started')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryedit, Nothing)


                            objdb.Entry(DataRekeningClass.CRS_Domestic_Data_Rekening).State = Entity.EntityState.Modified
                        Case enumActionForm.Delete


                            Dim strQuerydelete As String = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Activity', 'Delete Domestic Data Rekening started (Data Rekening ID = " & DataRekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID & ")')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerydelete, Nothing)



                            objdb.Entry(DataRekeningClass.CRS_Domestic_Data_Rekening).State = Entity.EntityState.Deleted

                    End Select
                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
        Using objdb As New CRSDAL.CRSEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Select Case Action
                        Case enumActionForm.Add
                            If DataRekeningClass.list_CRS_Domestic_Pengendali_Entitas IsNot Nothing Then
                                For Each item In DataRekeningClass.list_CRS_Domestic_Pengendali_Entitas
                                    item.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.LastUpdateDate = Now
                                    item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.CreatedDate = Now
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    End If
                                    item.IdentitasUnik = DataRekeningClass.CRS_Domestic_Data_Rekening.IdentitasUnik
                                    item.FK_CRS_DOMESTIC_HEADER_ID = DataRekeningClass.CRS_Domestic_Data_Rekening.FK_CRS_DOMESTIC_HEADER_ID
                                    item.FK_CRS_DOMESTIC_DATA_REKENING_ID = DataRekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                Next
                            End If

                            Dim strQueryadd As String = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Data Rekening', 'Save Add Data finished')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryadd, Nothing)
                            '===================== END OF NEW ADDED DATA


                        Case enumActionForm.Edit
                            If DataRekeningClass.list_CRS_Domestic_Pengendali_Entitas IsNot Nothing Then
                                For Each item In DataRekeningClass.list_CRS_Domestic_Pengendali_Entitas
                                    item.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.LastUpdateDate = Now
                                    item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.CreatedDate = Now
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    End If
                                    item.IdentitasUnik = DataRekeningClass.CRS_Domestic_Data_Rekening.IdentitasUnik
                                    item.FK_CRS_DOMESTIC_HEADER_ID = DataRekeningClass.CRS_Domestic_Data_Rekening.FK_CRS_DOMESTIC_HEADER_ID
                                    item.FK_CRS_DOMESTIC_DATA_REKENING_ID = DataRekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID
                                    If item.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID > 0 Then
                                        objdb.Entry(item).State = Entity.EntityState.Modified
                                        oldData.list_CRS_Domestic_Pengendali_Entitas.RemoveAll(Function(x) x.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID = item.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID)
                                    Else
                                        objdb.Entry(item).State = Entity.EntityState.Added
                                    End If
                                Next

                            End If
                            For Each item In oldData.list_CRS_Domestic_Pengendali_Entitas
                                objdb.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            Dim strQueryadd As String = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Data Rekening', 'Save Edit Data finished')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryadd, Nothing)
                            '===================== END OF NEW EDITED DATA

                        Case enumActionForm.Delete
                            If DataRekeningClass.list_CRS_Domestic_Pengendali_Entitas IsNot Nothing Then
                                For Each item In DataRekeningClass.list_CRS_Domestic_Pengendali_Entitas
                                    objdb.Entry(item).State = Entity.EntityState.Deleted
                                Next
                            End If
                            Dim strQuerydelete As String = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Activity', 'Delete Domestic Data Rekening finished (Data Rekening ID = " & DataRekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID & ")')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerydelete, Nothing)



                    End Select
                    objdb.SaveChanges()
                    objTrans.Commit()

                    Select Case Action
                        Case enumActionForm.Add
                            'Save Audit Trail
                            SaveDataRekening_AuditTrail(DataRekeningClass, objData_Old, objdb, objModule, intModuleAction)
                        Case enumActionForm.Edit
                            'Save Audit Trail
                            SaveDataRekening_AuditTrail(DataRekeningClass, objData_Old, objdb, objModule, intModuleAction)
                        Case enumActionForm.Delete
                            'Save Audit Trail
                            DeleteDataRekeningByDataRekeningID_AuditTrail(objData_Old, objdb, objModule)
                    End Select

                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Function SaveDataIntoNewRow(ID As Long) As Long
        Try
            Dim HeaderClass As New CRS_Domestic_Report_Class
            Dim strQuery As String = ""
            Dim inttotalRekening As Integer = 0
            HeaderClass = PullDataReportByID(ID)
            Dim IDOld As String = ID
            Dim StrQueryCount As String = " select COUNT(1) from CRS_DOMESTIC_HEADER WHERE Periode = '" & (Date.Today.Year - 1).ToString() & "' and InitialOrCorrection = 'Correction' "
            Dim TotalHeader As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCount, Nothing)
            If TotalHeader > 0 Then
                HeaderClass = New CRSBLL.CRS_Domestic_Report_Class
                Dim StrQueryPKHeader As String = " select PK_CRS_DOMESTIC_HEADER_ID from CRS_DOMESTIC_HEADER WHERE Periode = '" & (Date.Today.Year - 1).ToString() & "' and InitialOrCorrection = 'Correction' "
                Dim PKID As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryPKHeader, Nothing)
                HeaderClass = PullDataReportByID(PKID)

                'HeaderClass.CRS_Domestic_Header.JumlahDataRekening = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").Count
                'HeaderClass.CRS_Domestic_Header.JumlahNilaiSaldo = HeaderClass.list_CRS_Domestic_Data_Rekening.Sum(Function(x) x.SaldoAtauNilai And x.JenisData <> "DJP1")
                strQuery = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & IDOld
                inttotalRekening = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                HeaderClass.CRS_Domestic_Header.JumlahDataRekening = HeaderClass.CRS_Domestic_Header.JumlahDataRekening + inttotalRekening
                strQuery = " select ISNULL(SUM(SaldoAtauNilai), 0) from CRS_DOMESTIC_DATA_REKENING "
                strQuery = strQuery & " where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & IDOld
                Dim sumSaldo As Double = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                HeaderClass.CRS_Domestic_Header.JumlahNilaiSaldo = HeaderClass.CRS_Domestic_Header.JumlahNilaiSaldo + sumSaldo
                strQuery = " select COUNT(1) as CountDataPengendaliEntitas "
                strQuery = strQuery & " From CRS_DOMESTIC_PENGENDALI_ENTITAS entitas "
                strQuery = strQuery & " Join CRS_DOMESTIC_DATA_REKENING rekening "
                strQuery = strQuery & " On entitas.FK_CRS_DOMESTIC_DATA_REKENING_ID = rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID "
                strQuery = strQuery & " where rekening.JenisData <> 'DJP1' and "
                strQuery = strQuery & " entitas.FK_CRS_DOMESTIC_HEADER_ID = " & IDOld
                Dim getCountPengendaliEntitas As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString(strQuery)
                If getCountPengendaliEntitas IsNot Nothing Then
                    HeaderClass.CRS_Domestic_Header.JumlahDataPengendaliEntitas = HeaderClass.CRS_Domestic_Header.JumlahDataPengendaliEntitas + getCountPengendaliEntitas("CountDataPengendaliEntitas")
                End If
                strQuery = " select PK_CRS_DOMESTIC_DATA_REKENING_ID from CRS_DOMESTIC_DATA_REKENING  "
                strQuery = strQuery & " where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                Dim datarekening As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString(strQuery)
                SaveDataHeader(HeaderClass, enumActionForm.Edit)
                'Dim pkcounter As Integer = -1
                'Dim pkcounterEntitas As Integer = -1
                ''For Each item In HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").ToList
                'For Each item In datarekening.Rows
                '    'If item.JenisData <> "DJP1" Then
                '    '    Dim RekeningClass As New CRS_Data_Rekening_Class
                '    '    RekeningClass = PullDataRekeningByID(item.PK_CRS_DOMESTIC_DATA_REKENING_ID)
                '    '    RekeningClass.CRS_Domestic_Data_Rekening.FK_CRS_DOMESTIC_HEADER_ID = HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                '    '    RekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID = pkcounter
                '    '    pkcounter = pkcounter - 1
                '    '    For Each itementity In RekeningClass.list_CRS_Domestic_Pengendali_Entitas
                '    '        itementity.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID = pkcounterEntitas
                '    '        pkcounterEntitas = pkcounterEntitas - 1
                '    '    Next
                '    '    SaveDataRekening(RekeningClass, enumActionForm.Add)
                '    'End If
                '    Dim RekeningClass As New CRS_Data_Rekening_Class
                '    RekeningClass = PullDataRekeningByID(item("PK_CRS_DOMESTIC_DATA_REKENING_ID"))
                '    RekeningClass.CRS_Domestic_Data_Rekening.FK_CRS_DOMESTIC_HEADER_ID = HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                '    RekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID = pkcounter
                '    pkcounter = pkcounter - 1
                '    If RekeningClass.list_CRS_Domestic_Pengendali_Entitas IsNot Nothing Then
                '        For Each itementity In RekeningClass.list_CRS_Domestic_Pengendali_Entitas
                '            itementity.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID = pkcounterEntitas
                '            pkcounterEntitas = pkcounterEntitas - 1
                '        Next
                '    End If

                '    SaveDataRekening(RekeningClass, enumActionForm.Add)
                'Next
            Else
                'HeaderClass.CRS_Domestic_Header.JumlahDataRekening = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").Count
                'HeaderClass.CRS_Domestic_Header.JumlahNilaiSaldo = HeaderClass.list_CRS_Domestic_Data_Rekening.Sum(Function(x) x.SaldoAtauNilai And x.JenisData <> "DJP1")
                strQuery = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                inttotalRekening = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                HeaderClass.CRS_Domestic_Header.JumlahDataRekening = inttotalRekening.ToString()
                strQuery = " select ISNULL(SUM(SaldoAtauNilai), 0) from CRS_DOMESTIC_DATA_REKENING "
                strQuery = strQuery & " where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                Dim sumSaldo As Double = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                HeaderClass.CRS_Domestic_Header.JumlahNilaiSaldo = 0
                HeaderClass.CRS_Domestic_Header.InitialOrCorrection = "Correction"
                strQuery = " select COUNT(1) as CountDataPengendaliEntitas "
                strQuery = strQuery & " From CRS_DOMESTIC_PENGENDALI_ENTITAS entitas "
                strQuery = strQuery & " Join CRS_DOMESTIC_DATA_REKENING rekening "
                strQuery = strQuery & " On entitas.FK_CRS_DOMESTIC_DATA_REKENING_ID = rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID "
                strQuery = strQuery & " where rekening.JenisData <> 'DJP1' and "
                strQuery = strQuery & " entitas.FK_CRS_DOMESTIC_HEADER_ID = " & HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                Dim getCountPengendaliEntitas As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString(strQuery)
                If getCountPengendaliEntitas IsNot Nothing Then
                    HeaderClass.CRS_Domestic_Header.JumlahDataPengendaliEntitas = getCountPengendaliEntitas("CountDataPengendaliEntitas")
                End If
                strQuery = " select PK_CRS_DOMESTIC_DATA_REKENING_ID from CRS_DOMESTIC_DATA_REKENING  "
                strQuery = strQuery & " where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                Dim datarekening As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString(strQuery)
                HeaderClass.CRS_Domestic_Header.Status_Report = 1
                HeaderClass.CRS_Domestic_Header.IsValid = 1
                HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID = -1
                SaveDataHeader(HeaderClass, enumActionForm.Add)
                'Dim pkcounter As Integer = -1
                'Dim pkcounterEntitas As Integer = -1
                ''For Each item In HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").ToList
                'For Each item In datarekening.Rows
                '    'If item.JenisData <> "DJP1" Then
                '    '    Dim RekeningClass As New CRS_Data_Rekening_Class
                '    '    RekeningClass = PullDataRekeningByID(item.PK_CRS_DOMESTIC_DATA_REKENING_ID)
                '    '    RekeningClass.CRS_Domestic_Data_Rekening.FK_CRS_DOMESTIC_HEADER_ID = HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                '    '    RekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID = pkcounter
                '    '    pkcounter = pkcounter - 1
                '    '    For Each itementity In RekeningClass.list_CRS_Domestic_Pengendali_Entitas
                '    '        itementity.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID = pkcounterEntitas
                '    '        pkcounterEntitas = pkcounterEntitas - 1
                '    '    Next
                '    '    SaveDataRekening(RekeningClass, enumActionForm.Add)
                '    'End If
                '    Dim RekeningClass As New CRS_Data_Rekening_Class
                '    RekeningClass = PullDataRekeningByID(item("PK_CRS_DOMESTIC_DATA_REKENING_ID"))
                '    RekeningClass.CRS_Domestic_Data_Rekening.FK_CRS_DOMESTIC_HEADER_ID = HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                '    RekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID = pkcounter
                '    pkcounter = pkcounter - 1
                '    If RekeningClass.list_CRS_Domestic_Pengendali_Entitas IsNot Nothing Then
                '        For Each itementity In RekeningClass.list_CRS_Domestic_Pengendali_Entitas
                '            itementity.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID = pkcounterEntitas
                '            pkcounterEntitas = pkcounterEntitas - 1
                '        Next
                '    End If

                '    SaveDataRekening(RekeningClass, enumActionForm.Add)
                'Next
            End If

            Return HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    '30 Jun 2022 Ari : Save Data Into New Row Header BU
    Shared Function SaveDataIntoNewRowBU(ID As Long) As Long
        Try
            Dim HeaderClassBU As New CRS_Domestic_Report_BU_Class
            Dim strQuery As String = ""
            Dim inttotalRekening As Integer = 0
            Dim IDIfCorrectionExist As Long = 0
            HeaderClassBU = PullDataReportBUByID(ID)
            Dim IDBUOld As String = ID
            Dim StrQueryCount As String = " select COUNT(1) from CRS_DOMESTIC_HEADER_BUSINESSUNIT WHERE Periode = '" & (Date.Today.Year - 1).ToString() & "' and InitialOrCorrection = 'Correction' And fk_businessunit_code = '" & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "'"
            Dim TotalHeaderBU As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryCount, Nothing)
            If TotalHeaderBU > 0 Then
                Dim StrQueryPKHeaderID As String = " select PK_CRS_DOMESTIC_HEADER_ID from CRS_DOMESTIC_HEADER WHERE Periode = '" & (Date.Today.Year - 1).ToString() & "' and InitialOrCorrection = 'Correction' "
                Dim PKID As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryPKHeaderID, Nothing)
                Dim StrQueryPKHeaderIDInitial As String = " select PK_CRS_DOMESTIC_HEADER_ID from CRS_DOMESTIC_HEADER WHERE Periode = '" & (Date.Today.Year - 1).ToString() & "' and InitialOrCorrection = 'Initial' "
                Dim PKIDInitial As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryPKHeaderIDInitial, Nothing)

                Dim BUCode As String = HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code
                HeaderClassBU = New CRSBLL.CRS_Domestic_Report_BU_Class
                Dim StrQueryPKHeader As String = " select PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID from CRS_DOMESTIC_HEADER_BUSINESSUNIT WHERE Periode = '" & (Date.Today.Year - 1).ToString() & "' and InitialOrCorrection = 'Correction' And fk_businessunit_code = '" & BUCode & "'"
                Dim PKBU As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryPKHeader, Nothing)
                HeaderClassBU = PullDataReportBUByID(PKBU)
                'HeaderClass.CRS_Domestic_Header.JumlahDataRekening = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").Count
                'HeaderClass.CRS_Domestic_Header.JumlahNilaiSaldo = HeaderClass.list_CRS_Domestic_Data_Rekening.Sum(Function(x) x.SaldoAtauNilai And x.JenisData <> "DJP1")
                strQuery = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & PKIDInitial & " And fk_businessunit_code = '" & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "' AND Periode = '" & (Date.Today.Year - 1).ToString() & "'"
                inttotalRekening = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                HeaderClassBU.CRS_Domestic_Header_BusinessUnit.JumlahDataRekening = HeaderClassBU.CRS_Domestic_Header_BusinessUnit.JumlahDataRekening + inttotalRekening
                strQuery = " select ISNULL(SUM(SaldoAtauNilai), 0) from CRS_DOMESTIC_DATA_REKENING "
                strQuery = strQuery & " where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & PKIDInitial & " And fk_businessunit_code = '" & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "' AND Periode = '" & (Date.Today.Year - 1).ToString() & "'"
                Dim sumSaldo As Double = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                HeaderClassBU.CRS_Domestic_Header_BusinessUnit.JumlahNilaiSaldo = HeaderClassBU.CRS_Domestic_Header_BusinessUnit.JumlahNilaiSaldo + sumSaldo
                strQuery = " select COUNT(1) as CountDataPengendaliEntitas "
                strQuery = strQuery & " From CRS_DOMESTIC_PENGENDALI_ENTITAS entitas "
                strQuery = strQuery & " Join CRS_DOMESTIC_DATA_REKENING rekening "
                strQuery = strQuery & " On entitas.FK_CRS_DOMESTIC_DATA_REKENING_ID = rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID "
                strQuery = strQuery & " where rekening.JenisData <> 'DJP1' and "
                strQuery = strQuery & " entitas.FK_CRS_DOMESTIC_HEADER_ID = " & PKIDInitial & " And entitas.fk_businessunit_code = '" & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "' AND entitas.Periode = '" & (Date.Today.Year - 1).ToString() & "'"
                Dim getCountPengendaliEntitas As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString(strQuery)
                If getCountPengendaliEntitas IsNot Nothing Then
                    HeaderClassBU.CRS_Domestic_Header_BusinessUnit.JumlahDataPengendaliEntitas = HeaderClassBU.CRS_Domestic_Header_BusinessUnit.JumlahDataPengendaliEntitas + getCountPengendaliEntitas("CountDataPengendaliEntitas")
                End If
                strQuery = " select PK_CRS_DOMESTIC_DATA_REKENING_ID from CRS_DOMESTIC_DATA_REKENING  "
                strQuery = strQuery & " where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & PKIDInitial & " And fk_businessunit_code = '" & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "' AND Periode = '" & (Date.Today.Year - 1).ToString() & "'"
                Dim datarekening As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString(strQuery)
                SaveDataHeaderBU(HeaderClassBU, enumActionForm.Edit)
                Dim pkcounter As Integer = -1
                Dim pkcounterEntitas As Integer = -1
                'For Each item In HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").ToList
                For Each item In datarekening.Rows
                    'If item.JenisData <> "DJP1" Then
                    '    Dim RekeningClass As New CRS_Data_Rekening_Class
                    '    RekeningClass = PullDataRekeningByID(item.PK_CRS_DOMESTIC_DATA_REKENING_ID)
                    '    RekeningClass.CRS_Domestic_Data_Rekening.FK_CRS_DOMESTIC_HEADER_ID = HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                    '    RekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID = pkcounter
                    '    pkcounter = pkcounter - 1
                    '    For Each itementity In RekeningClass.list_CRS_Domestic_Pengendali_Entitas
                    '        itementity.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID = pkcounterEntitas
                    '        pkcounterEntitas = pkcounterEntitas - 1
                    '    Next
                    '    SaveDataRekening(RekeningClass, enumActionForm.Add)
                    'End If
                    Dim RekeningClass As New CRS_Data_Rekening_Class
                    RekeningClass = PullDataRekeningByID(item("PK_CRS_DOMESTIC_DATA_REKENING_ID"))
                    RekeningClass.CRS_Domestic_Data_Rekening.FK_CRS_DOMESTIC_HEADER_ID = HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_CRS_DOMESTIC_HEADER_ID
                    RekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID = pkcounter
                    RekeningClass.CRS_Domestic_Data_Rekening.FK_BusinessUnit_Code = HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code
                    pkcounter = pkcounter - 1
                    If RekeningClass.list_CRS_Domestic_Pengendali_Entitas IsNot Nothing Then
                        For Each itementity In RekeningClass.list_CRS_Domestic_Pengendali_Entitas
                            itementity.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID = pkcounterEntitas
                            itementity.FK_CRS_DOMESTIC_HEADER_ID = HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_CRS_DOMESTIC_HEADER_ID
                            itementity.FK_BusinessUnit_Code = HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code
                            pkcounterEntitas = pkcounterEntitas - 1
                        Next
                    End If

                    SaveDataRekening(RekeningClass, enumActionForm.Add)
                Next
            Else
                Dim StrQueryPKHeader As String = " select PK_CRS_DOMESTIC_HEADER_ID from CRS_DOMESTIC_HEADER WHERE Periode = '" & (Date.Today.Year - 1).ToString() & "' and InitialOrCorrection = 'Correction' "
                Dim PKID As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryPKHeader, Nothing)
                Dim PKHeaderld As Integer = HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_CRS_DOMESTIC_HEADER_ID
                'HeaderClass.CRS_Domestic_Header.JumlahDataRekening = HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").Count
                'HeaderClass.CRS_Domestic_Header.JumlahNilaiSaldo = HeaderClass.list_CRS_Domestic_Data_Rekening.Sum(Function(x) x.SaldoAtauNilai And x.JenisData <> "DJP1")
                strQuery = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_CRS_DOMESTIC_HEADER_ID & " And fk_businessunit_code = '" & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "' AND Periode = '" & (Date.Today.Year - 1).ToString() & "'"
                inttotalRekening = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                HeaderClassBU.CRS_Domestic_Header_BusinessUnit.JumlahDataRekening = inttotalRekening.ToString()
                HeaderClassBU.CRS_Domestic_Header_BusinessUnit.InitialOrCorrection = "Correction"
                strQuery = " select ISNULL(SUM(SaldoAtauNilai), 0) from CRS_DOMESTIC_DATA_REKENING "
                strQuery = strQuery & " where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_CRS_DOMESTIC_HEADER_ID & " And fk_businessunit_code = '" & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "' AND Periode = '" & (Date.Today.Year - 1).ToString() & "'"
                Dim sumSaldo As Double = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                HeaderClassBU.CRS_Domestic_Header_BusinessUnit.JumlahNilaiSaldo = sumSaldo
                strQuery = " select COUNT(1) as CountDataPengendaliEntitas "
                strQuery = strQuery & " From CRS_DOMESTIC_PENGENDALI_ENTITAS entitas "
                strQuery = strQuery & " Join CRS_DOMESTIC_DATA_REKENING rekening "
                strQuery = strQuery & " On entitas.FK_CRS_DOMESTIC_DATA_REKENING_ID = rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID "
                strQuery = strQuery & " where rekening.JenisData <> 'DJP1' and "
                strQuery = strQuery & " entitas.FK_CRS_DOMESTIC_HEADER_ID = " & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_CRS_DOMESTIC_HEADER_ID & " And entitas.fk_businessunit_code = '" & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "' AND entitas.Periode = '" & (Date.Today.Year - 1).ToString() & "'"
                Dim getCountPengendaliEntitas As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString(strQuery)
                If getCountPengendaliEntitas IsNot Nothing Then
                    HeaderClassBU.CRS_Domestic_Header_BusinessUnit.JumlahDataPengendaliEntitas = getCountPengendaliEntitas("CountDataPengendaliEntitas")
                End If
                strQuery = " select PK_CRS_DOMESTIC_DATA_REKENING_ID from CRS_DOMESTIC_DATA_REKENING  "
                strQuery = strQuery & " where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_CRS_DOMESTIC_HEADER_ID & " And fk_businessunit_code = '" & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "' AND Periode = '" & (Date.Today.Year - 1).ToString() & "'"
                Dim datarekening As DataTable = CRSBLL.CRSDomesticBLL.PullDataAsDataTableSingleString(strQuery)
                HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_CRS_DOMESTIC_HEADER_ID = PKID
                HeaderClassBU.CRS_Domestic_Header_BusinessUnit.Status_Report = 1
                HeaderClassBU.CRS_Domestic_Header_BusinessUnit.PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID = -1
                SaveDataHeaderBU(HeaderClassBU, enumActionForm.Add)
                Dim HeaderClass As New CRS_Domestic_Report_Class
                Dim strQueryHeader As String = ""
                Dim inttotalRekeningHeader As Integer = 0
                HeaderClass = PullDataReportByID(PKID)
                If HeaderClass.CRS_Domestic_Header.JumlahNilaiSaldo = 0 Then
                    strQueryHeader = " select COUNT(1) from CRS_DOMESTIC_DATA_REKENING where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & PKHeaderld & " And fk_businessunit_code = '" & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "' AND Periode = '" & (Date.Today.Year - 1).ToString() & "'"
                    inttotalRekeningHeader = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryHeader, Nothing)
                    HeaderClass.CRS_Domestic_Header.JumlahDataRekening = inttotalRekeningHeader.ToString()
                    strQueryHeader = " select ISNULL(SUM(SaldoAtauNilai), 0) from CRS_DOMESTIC_DATA_REKENING "
                    strQueryHeader = strQueryHeader & " where JenisData <> 'DJP1' and FK_CRS_DOMESTIC_HEADER_ID = " & PKHeaderld & " And fk_businessunit_code = '" & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "' AND Periode = '" & (Date.Today.Year - 1).ToString() & "'"
                    Dim sumSaldoHeader As Double = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryHeader, Nothing)
                    HeaderClass.CRS_Domestic_Header.JumlahNilaiSaldo = sumSaldoHeader
                    strQueryHeader = " select COUNT(1) as CountDataPengendaliEntitas "
                    strQueryHeader = strQueryHeader & " From CRS_DOMESTIC_PENGENDALI_ENTITAS entitas "
                    strQueryHeader = strQueryHeader & " Join CRS_DOMESTIC_DATA_REKENING rekening "
                    strQueryHeader = strQueryHeader & " On entitas.FK_CRS_DOMESTIC_DATA_REKENING_ID = rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID "
                    strQueryHeader = strQueryHeader & " where rekening.JenisData <> 'DJP1' and "
                    strQueryHeader = strQueryHeader & " entitas.FK_CRS_DOMESTIC_HEADER_ID = " & PKHeaderld & " And entitas.fk_businessunit_code = '" & HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code & "' AND entitas.Periode = '" & (Date.Today.Year - 1).ToString() & "'"
                    Dim getCountPengendaliEntitasHeader As DataRow = CRSBLL.CRSDomesticBLL.PullDataAsDataRowSingleString(strQueryHeader)
                    If getCountPengendaliEntitasHeader IsNot Nothing Then
                        HeaderClass.CRS_Domestic_Header.JumlahDataPengendaliEntitas = getCountPengendaliEntitas("CountDataPengendaliEntitas")
                    End If
                    SaveDataHeader(HeaderClass, enumActionForm.Edit)
                End If

                Dim pkcounter As Integer = -1
                Dim pkcounterEntitas As Integer = -1
                'For Each item In HeaderClass.list_CRS_Domestic_Data_Rekening.Where(Function(x) x.JenisData <> "DJP1").ToList
                For Each item In datarekening.Rows
                    'If item.JenisData <> "DJP1" Then
                    '    Dim RekeningClass As New CRS_Data_Rekening_Class
                    '    RekeningClass = PullDataRekeningByID(item.PK_CRS_DOMESTIC_DATA_REKENING_ID)
                    '    RekeningClass.CRS_Domestic_Data_Rekening.FK_CRS_DOMESTIC_HEADER_ID = HeaderClass.CRS_Domestic_Header.PK_CRS_DOMESTIC_HEADER_ID
                    '    RekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID = pkcounter
                    '    pkcounter = pkcounter - 1
                    '    For Each itementity In RekeningClass.list_CRS_Domestic_Pengendali_Entitas
                    '        itementity.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID = pkcounterEntitas
                    '        pkcounterEntitas = pkcounterEntitas - 1
                    '    Next
                    '    SaveDataRekening(RekeningClass, enumActionForm.Add)
                    'End If
                    Dim RekeningClass As New CRS_Data_Rekening_Class
                    RekeningClass = PullDataRekeningByID(item("PK_CRS_DOMESTIC_DATA_REKENING_ID"))
                    RekeningClass.CRS_Domestic_Data_Rekening.FK_CRS_DOMESTIC_HEADER_ID = PKID
                    RekeningClass.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID = pkcounter
                    RekeningClass.CRS_Domestic_Data_Rekening.FK_BusinessUnit_Code = HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code
                    pkcounter = pkcounter - 1
                    If RekeningClass.list_CRS_Domestic_Pengendali_Entitas IsNot Nothing Then
                        For Each itementity In RekeningClass.list_CRS_Domestic_Pengendali_Entitas
                            itementity.PK_CRS_DOMESTIC_PENGENDALI_ENTITAS_ID = pkcounterEntitas
                            itementity.FK_CRS_DOMESTIC_HEADER_ID = PKID
                            itementity.FK_BusinessUnit_Code = HeaderClassBU.CRS_Domestic_Header_BusinessUnit.FK_BusinessUnit_Code
                            pkcounterEntitas = pkcounterEntitas - 1
                        Next
                    End If

                    SaveDataRekening(RekeningClass, enumActionForm.Add)
                Next
            End If


            Return HeaderClassBU.CRS_Domestic_Header_BusinessUnit.PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Shared Sub SaveDataPengendaliEntitas(DataPengendaliEntitas As CRSDAL.CRS_DOMESTIC_PENGENDALI_ENTITAS, Action As enumActionForm)
        Using objdb As New CRSDAL.CRSEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    With DataPengendaliEntitas
                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .LastUpdateDate = Now
                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                            .Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                        End If
                    End With
                    Select Case Action
                        Case enumActionForm.Add
                            With DataPengendaliEntitas
                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .CreatedDate = Now
                            End With
                            objdb.Entry(DataPengendaliEntitas).State = Entity.EntityState.Added
                        Case enumActionForm.Edit
                            objdb.Entry(DataPengendaliEntitas).State = Entity.EntityState.Modified
                        Case enumActionForm.Delete
                            objdb.Entry(DataPengendaliEntitas).State = Entity.EntityState.Deleted
                    End Select
                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub SaveStateByReportID(strID As String)
        Try
            Dim strQuery As String = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic', 'Save Report State for later Restore if Rejected by Checker started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(1) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_ID"
            param(0).Value = strID
            param(0).DbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@UserID"
            param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(1).DbType = SqlDbType.VarChar


            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Domestic_XML_SaveState", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic', 'Save Report State for later Restore if Rejected by Checker finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '28 Jun 2022 Ari: Penambahan Save State Domestic BU
    Shared Sub SaveStateBUByReportID(strID As String)
        Try
            Dim strQuerypk2 As String = "select FK_CRS_DOMESTIC_HEADER_ID from CRS_DOMESTIC_HEADER_BUSINESSUNIT where PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID = " & strID.ToString
            Dim pk2 As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerypk2, Nothing)


            Dim strQuery As String = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic', 'Save Report State for later Restore if Rejected by Checker started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_ID"
            param(0).Value = pk2
            param(0).DbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@UserID"
            param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(1).DbType = SqlDbType.VarChar

            Dim strQuerycode As String = "select FK_BusinessUnit_Code from CRS_DOMESTIC_HEADER_BUSINESSUNIT where PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID = " & strID.ToString
            Dim code As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerycode, Nothing)

            param(2) = New SqlParameter
            param(2).ParameterName = "@BuCode"
            param(2).Value = code
            param(2).DbType = SqlDbType.VarChar


            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Domestic_BU_XML_SaveState", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Business Unit', 'Save Report State for later Restore if Rejected by Checker finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Shared Sub RestoreStateByReportID(strID As String)
        Try
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_ID"
            param(0).Value = strID
            param(0).DbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@UserID"
            param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(1).DbType = SqlDbType.VarChar
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Domestic_XML_RestoreState", param)


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '28 Jun 2022 Ari: Penambahan Restore State Domestic BU
    Shared Sub RestoreStateBUByReportID(strID As String)
        Try
            Dim strQuerypk2 As String = "select FK_CRS_DOMESTIC_HEADER_ID from CRS_DOMESTIC_HEADER_BUSINESSUNIT where PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID = " & strID.ToString
            Dim pk2 As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerypk2, Nothing)

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_ID"
            param(0).Value = pk2
            param(0).DbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@UserID"
            param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(1).DbType = SqlDbType.VarChar

            Dim strQuerycode As String = "select FK_BusinessUnit_Code from CRS_DOMESTIC_HEADER_BUSINESSUNIT where PK_CRS_DOMESTIC_HEADER_BUSINESSUNIT_ID = " & strID.ToString
            Dim code As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerycode, Nothing)


            param(2) = New SqlParameter
            param(2).ParameterName = "@BuCode"
            param(2).Value = code
            param(2).DbType = SqlDbType.VarChar

            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Domestic__BU_XML_RestoreState", param)

            'Dim strQuerycount As String = "select COUNT(1) from CRS_Domestic_BU_XML_SavedState WHERE FK_Report_ID=" & pk2 & " and ModuleName = 'CRS_DOMESTIC_HEADER'"

            'Dim total As Integer = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuerycount, Nothing)

            'If total = 
            ''Hapus GoAML_Report_XML_SavedState by PK Report ID
            '    Dim strQuery As String = "DELETE FROM CRS_Domestic_BU_XML_SavedState WHERE FK_Report_ID=" & pk2 & " and ModuleName <> 'CRS_DOMESTIC_HEADER'"
            'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveWithApproval(objModule As NawaDAL.Module, PK_ID As Long, moduleAction As NawaBLL.Common.ModuleActionEnum, Optional isEditInsertNewRow As Boolean = False, Optional strStatusOld As String = Nothing)
        Using objDB As New CRSDAL.CRSEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = moduleAction
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    Dim objModuleApproval As New CRSDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleKey = PK_ID
                        .ModuleName = objModule.ModuleName
                        If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update AndAlso isEditInsertNewRow Then
                            .ModuleField = "Insert New Row"
                        ElseIf intModuleAction = NawaBLL.Common.ModuleActionEnum.Delete AndAlso strStatusOld IsNot Nothing Then
                            .ModuleField = strStatusOld
                        End If
                        '.ModuleField = objXMLData
                        '.ModuleFieldBefore = objXMLData_Old
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Function ValidateDataRekening(DataRekening As CRS_Data_Rekening_Class) As String
        Try
            Dim strQuery As String
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Data Rekening', 'Save Data to Validate by XML started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim listDataRekening As New List(Of CRSDAL.CRS_DOMESTIC_DATA_REKENING)
            listDataRekening.Add(DataRekening.CRS_Domestic_Data_Rekening)
            SaveDataToValidateWithXML(listDataRekening)
            If DataRekening.list_CRS_Domestic_Pengendali_Entitas IsNot Nothing Then
                SaveDataToValidateWithXML(DataRekening.list_CRS_Domestic_Pengendali_Entitas)
            End If


            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Data Rekening', 'Save Data to Validate by XML finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Data Rekening', 'Validate Data Rekening started (Transaction ID = " & DataRekening.CRS_Domestic_Data_Rekening.FK_CRS_DOMESTIC_HEADER_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(1) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_DataRekening_ID"
            param(0).Value = DataRekening.CRS_Domestic_Data_Rekening.PK_CRS_DOMESTIC_DATA_REKENING_ID
            param(0).DbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@UserID"
            param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(1).DbType = SqlDbType.VarChar

            Dim strValidationResult As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Domestic_Validate_DataRekening", param)
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Domestic Data Rekening', 'Validate Data Rekening finished (Transaction ID = " & DataRekening.CRS_Domestic_Data_Rekening.FK_CRS_DOMESTIC_HEADER_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Return strValidationResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Sub SaveDataToValidateWithXML(objData As Object)
        Try
            Dim objtype As Type = objData.GetType
            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
            Dim tablename As String = properties(2).PropertyType.Name

            Dim objdt As New DataTable
            Dim objdataset = New DataSet()
            Dim objXMLData As String = ""
            Dim byteData As Byte() = Nothing

            If objData IsNot Nothing Then
                objdt = NawaBLL.Common.CopyGenericToDataTable(objData)
                objdataset.Tables.Add(objdt)
                objXMLData = objdataset.GetXml()
                byteData = System.Text.Encoding.Default.GetBytes(objXMLData)
            End If

            Dim param(2) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@TableName"
            param(0).Value = tablename
            param(0).SqlDbType = SqlDbType.VarChar

            param(1) = New SqlParameter
            param(1).ParameterName = "@XmlDocument"
            param(1).Value = byteData
            param(1).SqlDbType = SqlDbType.VarBinary

            param(2) = New SqlParameter
            param(2).ParameterName = "@UserID"
            param(2).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(2).SqlDbType = SqlDbType.VarChar

            Dim dtPK As DataTable = Nothing
            dtPK = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Report_XML_ToValidate_Save", param)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class

Public Class CRS_Domestic_Report_Class
    Public CRS_Domestic_Header As New CRSDAL.CRS_DOMESTIC_HEADER
    Public list_CRS_Domestic_Data_Rekening As New List(Of CRSDAL.CRS_DOMESTIC_DATA_REKENING)
End Class

Public Class CRS_Data_Rekening_Class
    Public CRS_Domestic_Data_Rekening As New CRSDAL.CRS_DOMESTIC_DATA_REKENING
    Public list_CRS_Domestic_Pengendali_Entitas As New List(Of CRSDAL.CRS_DOMESTIC_PENGENDALI_ENTITAS)
End Class

'29 June 2022 Ari : Tambah class BU
Public Class CRS_Domestic_Report_BU_Class
    Public CRS_Domestic_Header_BusinessUnit As New CRSDAL.CRS_DOMESTIC_HEADER_BUSINESSUNIT
    Public list_CRS_Domestic_Data_Rekening_BusinessUnit As New List(Of CRSDAL.CRS_DOMESTIC_DATA_REKENING)
End Class


