﻿
Imports System.Data.SqlClient
Imports CRSDAL

Public Class CRS_Report_BLL
#Region "CRS International"
    Shared Sub SaveDataToValidateWithXML(objData As Object)
        Try
            Dim objtype As Type = objData.GetType
            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
            Dim tablename As String = properties(2).PropertyType.Name

            Dim objdt As New DataTable
            Dim objdataset = New DataSet()
            Dim objXMLData As String = ""
            Dim byteData As Byte() = Nothing

            If objData IsNot Nothing Then
                objdt = NawaBLL.Common.CopyGenericToDataTable(objData)
                objdataset.Tables.Add(objdt)
                objXMLData = objdataset.GetXml()
                byteData = System.Text.Encoding.Default.GetBytes(objXMLData)
            End If

            Dim param(2) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@TableName"
            param(0).Value = tablename
            param(0).SqlDbType = SqlDbType.VarChar

            param(1) = New SqlParameter
            param(1).ParameterName = "@XmlDocument"
            param(1).Value = byteData
            param(1).SqlDbType = SqlDbType.VarBinary

            param(2) = New SqlParameter
            param(2).ParameterName = "@UserID"
            param(2).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(2).SqlDbType = SqlDbType.VarChar

            Dim dtPK As DataTable = Nothing
            dtPK = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Report_XML_ToValidate_Save", param)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "Report"
#Region "Action Report"
#Region "Save Report"
    Shared Sub SaveStateByReportID(strReportID As String)
        Try
            Dim strQuery As String

            'Dim strRestoreState As String = CRSBLL.CRS_Global_BLL.getGlobalParameterValueByID(3002)
            'If String.IsNullOrWhiteSpace(strRestoreState) Then
            '    strRestoreState = "0"
            'End If
            Dim strRestoreState As String = ""
            strRestoreState = "1"
            'Save State if RestoreState feature set to ON
            If strRestoreState = "1" Then
                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report', 'Save Report State for later Restore if Rejected by Checker started')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                Dim param(1) As SqlParameter
                param(0) = New SqlParameter
                param(0).ParameterName = "@PK_Report_ID"
                param(0).Value = strReportID
                param(0).DbType = SqlDbType.BigInt

                param(1) = New SqlParameter
                param(1).ParameterName = "@UserID"
                param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
                param(1).DbType = SqlDbType.VarChar
                NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_International_Report_XML_SaveState", param)

                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report', 'Save Report State for later Restore if Rejected by Checker finished')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveReportEdit(objModule As NawaDAL.Module, objData As CRS_Reporting_Class)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report', 'Save Report Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Get Old Data for Audit Trail
            Dim objData_Old = GetCRSINTReportClassByID(objData.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID)

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Jalankan save state saat status berubah ke 9 untuk pertama kali
            'Hanya dijalankan jika membutuhkan Approval
            If Not (NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not objModule.IsUseApproval) Then
                If objData_Old.obj_CRS_Reporting_Header.Status <> 9 And objData.obj_CRS_Reporting_Header.Status = 9 Then
                    SaveStateByReportID(objData.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID)
                ElseIf objData_Old.obj_CRS_Reporting_Header.Status <> 9 And objData.obj_CRS_Reporting_Header.Status = 4 Then
                    SaveStateByReportID(objData.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID)
                End If
            End If

            'Save New Data
            Using objDB As New CRSDAL.CRSEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_CRS_Reporting_Header
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        objDB.Entry(objData.obj_CRS_Reporting_Header).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        objTrans.Commit()

                        '' Add 27-Apr-2022
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update crs_international_report_header set timestamp = getdate() where PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & objData.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, Nothing)
                        '' End 27-Apr-2022
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                'Save Audit Trail
                Dim objAuditTrailheader As CRSDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)

                'Save Audit Trail Detail by XML
                Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

                Dim listReport As New List(Of CRS_INTERNATIONAL_REPORT_HEADER)
                Dim listReport_Old As New List(Of CRS_INTERNATIONAL_REPORT_HEADER)

                listReport.Add(objData.obj_CRS_Reporting_Header)
                listReport_Old.Add(objData_Old.obj_CRS_Reporting_Header)
                NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, listReport, listReport_Old)

                'Update Entity Reference
                objDB.Database.ExecuteSqlCommand("exec usp_updateEntityReference")

            End Using




            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report', 'Save Report Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveReportEditApproval(objModule As NawaDAL.Module, objData As CRS_Reporting_Class)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report', 'Save Report Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Get Old Data for Audit Trail
            Dim objData_Old = GetCRSINTReportClassByID(objData.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID)

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If



            'Save New Data
            Using objDB As New CRSDAL.CRSEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_CRS_Reporting_Header
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        objDB.Entry(objData.obj_CRS_Reporting_Header).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        objTrans.Commit()
                        '' Add 27-Apr-2022
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update crs_international_report_header set timestamp = getdate() where PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & objData.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, Nothing)
                        '' End 27-Apr-2022
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                'Save Audit Trail
                Dim objAuditTrailheader As CRSDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)

                'Save Audit Trail Detail by XML
                Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

                Dim listReport As New List(Of CRS_INTERNATIONAL_REPORT_HEADER)
                Dim listReport_Old As New List(Of CRS_INTERNATIONAL_REPORT_HEADER)

                listReport.Add(objData.obj_CRS_Reporting_Header)
                listReport_Old.Add(objData_Old.obj_CRS_Reporting_Header)
                NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, listReport, listReport_Old)

                'Update Entity Reference
                objDB.Database.ExecuteSqlCommand("exec usp_updateEntityReference")

            End Using




            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report', 'Save Report Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Shared Function SaveReportAdd(objModule As NawaDAL.Module, objData As CRS_Reporting_Class) As String
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report', 'Save Report Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Get Old Data (Karena saat save transaction dia metodenya langsung simpan, maka harus dikondisikan logicnya).
            'Jika Old data exists, Secara intAction tetap 1 (Add) tapi secara data dia Modified.
            Dim objData_Old = GetCRSINTReportClassByID(objData.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID)

            Using objDB As New CRSDAL.CRSEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_CRS_Reporting_Header
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        If objData_Old Is Nothing OrElse objData_Old.obj_CRS_Reporting_Header Is Nothing Then
                            objDB.Entry(objData.obj_CRS_Reporting_Header).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(objData.obj_CRS_Reporting_Header).State = Entity.EntityState.Modified
                        End If

                        objDB.SaveChanges()

                        'Save Report FI
                        If objData_Old Is Nothing OrElse objData_Old.obj_CRS_Reporting_FI Is Nothing Then
                            objData.obj_CRS_Reporting_FI.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = objData.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID
                            objDB.Entry(objData.obj_CRS_Reporting_FI).State = Entity.EntityState.Added
                        Else
                            objData.obj_CRS_Reporting_FI.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = objData.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID
                            objDB.Entry(objData.obj_CRS_Reporting_FI).State = Entity.EntityState.Modified
                        End If
                        objDB.SaveChanges()
                        objTrans.Commit()

                        '' Add 27-Apr-2022
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "update crs_international_report_header set timestamp = getdate() where PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = " & objData.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID, Nothing)
                        '' End 27-Apr-2022

                        Return objData.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                        Return Nothing
                    End Try

                    'objDB.Database.ExecuteSqlCommand("exec usp_updateEntityReference")
                End Using

                'Save Audit Trail
                Dim objAuditTrailheader As CRSDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)

                'Save Audit Trail Detail by XML
                Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID
                Dim listReport As New List(Of CRS_INTERNATIONAL_REPORT_HEADER)

                listReport.Add(objData.obj_CRS_Reporting_Header)
                NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, listReport, Nothing)

            End Using



            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report', 'Save Report Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Function validateReportAndReportFI(objReport As CRS_Reporting_Class) As String
        Try
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report Header & FI', 'Save Data to Validate by XML started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim listGoAML_Report As New List(Of CRSDAL.CRS_INTERNATIONAL_REPORT_HEADER)
            Dim listGoAML_ReportFI As New List(Of CRSDAL.CRS_INTERNATIONAL_REPORTING_FI)
            listGoAML_Report.Add(objReport.obj_CRS_Reporting_Header)
            listGoAML_ReportFI.Add(objReport.obj_CRS_Reporting_FI)
            SaveDataToValidateWithXML(listGoAML_Report)
            SaveDataToValidateWithXML(listGoAML_ReportFI)
            If objReport.list_CRS_Reporting_Group IsNot Nothing Then
                SaveDataToValidateWithXML(objReport.list_CRS_Reporting_Group)
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report Header & FI', 'Save Data to Validate by XML finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Jalankan SP usp_GOAML_Report_Validate_ReportAndIndicator untuk Validasi hanya level Report dan Indicator
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report Header & FI', 'Validate Report & Indicator started (Report ID = " & objReport.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(1) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = objReport.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID
            param(0).DbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@UserID"
            param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(1).DbType = SqlDbType.VarChar

            Dim strValidationResult As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_INTERNATIONAL_REPORT_HEADER_Validate_ReportAndReeportFI", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report Header & FI', 'Validate Report & Indicator finished (Report ID = " & objReport.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Ambil lagi semua ValidationReport by ReportID
            strValidationResult = getValidationReportByReportID(objReport.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID)
            Return strValidationResult

        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

#End Region
#Region "Delete Report"

    Shared Sub DeleteReportByReportID(strReportID As String, strUserMaker As String, strUserChecker As String)
        Try

            Dim strQuery As String = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report', 'Delete Report started (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Jalankan semua fungsi Delete, Audit Trail, Update Status dan List of Generated lewat SP
            Dim param(2) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).SqlDbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@UserID_Maker"
            param(1).Value = strUserMaker
            param(1).SqlDbType = SqlDbType.VarChar

            param(2) = New SqlParameter
            param(2).ParameterName = "@UserID_Checker"
            param(2).Value = strUserChecker
            param(2).SqlDbType = SqlDbType.VarChar

            NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Report_DeleteReportByReportID", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Report', 'Delete Report finished (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteReportGroupByReportID(strReportID As String)
        Try
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Activity', 'Delete Transaction started (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.BigInt
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_DeleteReportingGroup_ByReportID", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Activity', 'Delete Reporting Group finished (Reporting Group ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region
    Shared Sub RestoreStateByReportID(strReportID As String)
        Try
            Dim strQuery As String

            'Dim strRestoreState As String = CRS_Global_BLL.getGlobalParameterValueByID(3002)
            'If String.IsNullOrWhiteSpace(strRestoreState) Then
            '    strRestoreState = "0"
            'End If
            Dim strRestoreState As String = ""
            strRestoreState = "1"
            'Restore State
            If strRestoreState = "1" Then       'Restore State On
                Dim param(1) As SqlParameter
                param(0) = New SqlParameter
                param(0).ParameterName = "@PK_Report_ID"
                param(0).Value = strReportID
                param(0).DbType = SqlDbType.BigInt

                param(1) = New SqlParameter
                param(1).ParameterName = "@UserID"
                param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
                param(1).DbType = SqlDbType.VarChar
                NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_International_Report_Header_XML_RestoreState", param)
            End If

            'Hapus GoAML_Report_XML_SavedState by PK Report ID
            strQuery = "DELETE FROM CRS_INTERNATIONAL_Report_XML_SavedState WHERE FK_Report_ID=" & strReportID
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region
#Region "Validate Report"
#End Region
#Region "Get Data Report"
    Shared Function GetMessageTypeIndic(Code As String) As vw_CRS_INTERNATIONAL_MESSAGE_TYPE_INDICATOR
        Using objDb As CRSDAL.CRSEntities = New CRSDAL.CRSEntities
            Return objDb.vw_CRS_INTERNATIONAL_MESSAGE_TYPE_INDICATOR.Where(Function(x) x.FK_MESSAGE_TYPE_INDICATOR_CODE = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetDocTypeIndic(Code As String) As vw_CRS_International_Doc_Type_Indic
        Using objDb As CRSDAL.CRSEntities = New CRSDAL.CRSEntities
            Return objDb.vw_CRS_International_Doc_Type_Indic.Where(Function(x) x.FK_CRS_DOC_TYPE_INDIC_CODE = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetDocTypeIndicCorrection(Code As String) As Vw_CRS_International_ReportFI_DocTypeIndic
        Using objDb As CRSDAL.CRSEntities = New CRSDAL.CRSEntities
            Return objDb.Vw_CRS_International_ReportFI_DocTypeIndic.Where(Function(x) x.FK_CRS_DOC_TYPE_INDIC_CODE = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetLJKType(Code As String) As vw_crs_international_ljk_type
        Using objDb As CRSDAL.CRSEntities = New CRSDAL.CRSEntities
            Return objDb.vw_crs_international_ljk_type.Where(Function(x) x.FK_CRS_LJK_TYPE_CODE = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetCountry(Code As String) As vw_CRS_INTERNATIONAL_COUNTRY
        Using objDb As CRSDAL.CRSEntities = New CRSDAL.CRSEntities
            Return objDb.vw_CRS_INTERNATIONAL_COUNTRY.Where(Function(x) x.FK_COUNTRY_CODE = Code).FirstOrDefault
        End Using
    End Function

    Shared Function getValidationReportByReportID(strReportID As String)
        Try
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@reportId"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.Int

            Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_ResponseValidateReportByID", param)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Function GetCRSINTReportClassByID(ID As Long) As CRS_Reporting_Class
        Using objDb As New CRSDAL.CRSEntities

            Dim objCRSINTReportClass = New CRS_Reporting_Class
            With objCRSINTReportClass
                'Reporting Header
                .obj_CRS_Reporting_Header = objDb.CRS_INTERNATIONAL_REPORT_HEADER.Where(Function(x) x.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID = ID).FirstOrDefault
                .obj_CRS_Reporting_FI = objDb.CRS_INTERNATIONAL_REPORTING_FI.Where(Function(x) x.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = ID).FirstOrDefault
                Dim IDTransaction As Integer = 0

                'Reporting Group
                Dim listReportingGroup = objDb.CRS_INTERNATIONAL_REPORTING_GROUP.Where(Function(x) x.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = ID).ToList
                If listReportingGroup IsNot Nothing Then
                    .list_CRS_Reporting_Group = listReportingGroup
                    For Each item In listReportingGroup

                    Next
                End If

                'Reporting FI
                Dim objReportFI = objDb.CRS_INTERNATIONAL_REPORTING_FI.Where(Function(x) x.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = ID).FirstOrDefault
                If objReportFI IsNot Nothing Then
                    .obj_CRS_Reporting_FI = objReportFI
                End If


            End With

            Return objCRSINTReportClass
        End Using
    End Function
#End Region
#End Region

#Region "Report Group"
#Region "Action Report Group"
#Region "Save Report Group"
    Shared Sub SaveReportingGroup(objModule As NawaDAL.Module, objData As CRS_Reporting_Group_Class, strReportID As String)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            If objData.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID < 0 Then
                intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert
            End If

            'Get Old Data for Audit Trail
            Dim objData_Old = New CRS_Reporting_Group_Class
            If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                objData_Old = GetReportingGroupClassByID(objData.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID)
            End If

            Using objDB As New CRSDAL.CRSEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_Reporting_Group
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        If objData.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID < 0 Then
                            objDB.Entry(objData.obj_Reporting_Group).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(objData.obj_Reporting_Group).State = Entity.EntityState.Modified
                        End If
                        objDB.SaveChanges()

                        '===================== NEW ADDED/EDITED DATA
                        'Insert ke EODLogSP untuk ukur performance
                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Internation Reporting Group', 'Save Add/Edit Data started')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                        Dim OldPK As Long = 0
                        Dim OldPKSub As Long = 0
                        Dim OldPKSubSubID As Long = 0

                        'Controlling Person
                        If objData.list_Reporting_RGARPerson IsNot Nothing Then
                            If objData.list_Reporting_RGARPerson.Count > 0 Then
                                For Each objReportCP In objData.list_Reporting_RGARPerson
                                    OldPK = objReportCP.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID
                                    objReportCP.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = objData.obj_Reporting_Group.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID
                                    objReportCP.FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = objData.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID

                                    If objReportCP.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID < 0 Then
                                        objDB.Entry(objReportCP).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objReportCP).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()


                                Next
                            End If

                        End If


                        'Payment
                        If objData.list_Reporting_RGARPayment IsNot Nothing Then
                            If objData.list_Reporting_RGARPayment.Count > 0 Then
                                For Each ObjReportPayment In objData.list_Reporting_RGARPayment
                                    OldPK = ObjReportPayment.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID
                                    ObjReportPayment.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID = objData.obj_Reporting_Group.FK_CRS_INTERNATIONAL_REPORT_HEADER_ID
                                    ObjReportPayment.FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = objData.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID

                                    If ObjReportPayment.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID < 0 Then
                                        objDB.Entry(ObjReportPayment).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(ObjReportPayment).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()


                                Next
                            End If
                        End If







                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Reporting Group', 'Save Add/Edit Data finished')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        '===================== END OF NEW ADDED/EDITED DATA

                        '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                        If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Reporting Group', 'Save Deleted Data started')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                            'Controlling Person
                            For Each item_old In objData_Old.list_Reporting_RGARPerson
                                Dim objCek = objData.list_Reporting_RGARPerson.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID = item_old.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Payment
                            For Each item_old In objData_Old.list_Reporting_RGARPayment
                                Dim objCek = objData.list_Reporting_RGARPayment.Find(Function(x) x.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID = item_old.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next



                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Reporting Group', 'Save Deleted Data finished')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            '===================== END OF _DeleteD DATA
                        End If

                        objDB.SaveChanges()
                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                'Save Audit Trail
                SaveReportingGroup_AuditTrail(objData, objData_Old, objDB, objModule, intModuleAction)

            End Using

            '2021-11-24 : Coba jalankan validasi satuan per Transaction ID dari Form Reportnya untuk tuning performance
            'Jalankan validasi satuan untuk replace IsValid dan Error Message
            'strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Validate Satuan Started (Report ID = " & strReportID & ")')"
            'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Dim param(0) As SqlParameter
            'param(0) = New SqlParameter
            'param(0).ParameterName = "@PK_Report_ID"
            'param(0).Value = strReportID
            'param(0).DbType = SqlDbType.BigInt
            'NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_ValidasiSatuan", param)

            'strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Validate Satuan Finished (Report ID = " & strReportID & ")')"
            'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveReportingGroup_AuditTrail(objData As CRS_Reporting_Group_Class, objData_Old As CRS_Reporting_Group_Class, objDB As CRSEntities, objModule As NawaDAL.Module, intModuleAction As Integer)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("CRS_INTERNATIONAL_REPORTING_GROUP")

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Reporting Group', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim objAuditTrailheader As CRSDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)

            'Save Audit Trail Detail by XML
            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

            Dim listReportingGroup As New List(Of CRS_INTERNATIONAL_REPORTING_GROUP)
            Dim listReportingGroup_Old As New List(Of CRS_INTERNATIONAL_REPORTING_GROUP)


            listReportingGroup.Add(objData.obj_Reporting_Group)
            listReportingGroup_Old.Add(objData_Old.obj_Reporting_Group)
            NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, listReportingGroup, listReportingGroup_Old)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_Reporting_RGARPerson, objData_Old.list_Reporting_RGARPerson)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_Reporting_RGARPayment, objData_Old.list_Reporting_RGARPayment)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Reporting Group', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            'End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
#Region "Delete Reporting Group"

    Shared Sub DeleteReportGroupByReportGroupID(strReportGroupID As String)
        Try
            'Define local variables
            Dim objModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("CRS_INTERNATIONAL_REPORTING_GROUP")
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Get Old Data to Delete
            Dim objData_Old = GetReportingGroupClassByID(strReportGroupID)
            If objData_Old Is Nothing Then
                objData_Old = New CRS_Reporting_Group_Class
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Activity', 'Delete International Reporting Group started (Reporting Group ID = " & strReportGroupID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Using objDB As New CRSDAL.CRSEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Delete Reporting Group
                        objDB.Entry(objData_Old.obj_Reporting_Group).State = Entity.EntityState.Deleted

                        'Controlling Person
                        For Each item_old In objData_Old.list_Reporting_RGARPerson
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Payment
                        For Each item_old In objData_Old.list_Reporting_RGARPayment
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        objDB.SaveChanges()
                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Activity', 'Delete Reporting Group finished (Reporting Group ID = " & strReportGroupID & ")')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                'Save Audit Trail
                DeleteReportingGroupByReportGroupID_AuditTrail(objData_Old, objDB, objModule)

            End Using

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteReportingGroupByReportGroupID_AuditTrail(objData_Old As CRS_Reporting_Group_Class, objDB As CRSEntities, objModule As NawaDAL.Module)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("CRS_INTERNATIONAL_REPORTING_GROUP")

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Reporting Group T', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim objAuditTrailheader As CRSDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)

            'Save Audit Trail Detail by XML
            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

            Dim listreportgrouptodelete As New List(Of CRS_INTERNATIONAL_REPORTING_GROUP)
            listreportgrouptodelete.Add(objData_Old.obj_Reporting_Group)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, listreportgrouptodelete)


            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_Reporting_RGARPerson)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_Reporting_RGARPayment)


            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Reporting Group', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region
#End Region
#Region "Validate Report Group"
    Shared Function validateReportingGroup(objReport As CRS_Reporting_Class, objReportingGroup As CRS_Reporting_Group_Class) As String
        Try
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS Inernational Reporting Group', 'Save Data to Validate by XML started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim listCRS_Report As New List(Of CRSDAL.CRS_INTERNATIONAL_REPORT_HEADER)
            Dim listCRS_ReportGroup As New List(Of CRSDAL.CRS_INTERNATIONAL_REPORTING_GROUP)
            Dim listCRS_ReportFI As New List(Of CRSDAL.CRS_INTERNATIONAL_REPORTING_FI)

            listCRS_Report.Add(objReport.obj_CRS_Reporting_Header)
            listCRS_ReportGroup.Add(objReportingGroup.obj_Reporting_Group)
            listCRS_ReportFI.Add(objReport.obj_CRS_Reporting_FI)

            SaveDataToValidateWithXML(listCRS_Report)
            SaveDataToValidateWithXML(listCRS_ReportFI)
            SaveDataToValidateWithXML(listCRS_ReportGroup)

            If objReportingGroup.list_Reporting_RGARPayment IsNot Nothing Then
                If objReportingGroup.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID = -1 Then
                    For Each item In objReportingGroup.list_Reporting_RGARPayment
                        item.FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = -1
                    Next

                    SaveDataToValidateWithXML(objReportingGroup.list_Reporting_RGARPayment)
                Else
                    For Each item In objReportingGroup.list_Reporting_RGARPayment
                        item.FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = objReportingGroup.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID
                    Next
                    SaveDataToValidateWithXML(objReportingGroup.list_Reporting_RGARPayment)
                End If

            End If

            If objReportingGroup.list_Reporting_RGARPerson IsNot Nothing Then
                If objReportingGroup.list_Reporting_RGARPerson.Count > 0 Then
                    If objReportingGroup.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID = -1 Then
                        For Each item In objReportingGroup.list_Reporting_RGARPerson
                            item.FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = -1

                        Next
                        SaveDataToValidateWithXML(objReportingGroup.list_Reporting_RGARPerson)
                    Else
                        For Each item In objReportingGroup.list_Reporting_RGARPerson
                            item.FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = objReportingGroup.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID

                        Next
                        SaveDataToValidateWithXML(objReportingGroup.list_Reporting_RGARPerson)
                    End If
                Else
                    objReportingGroup.list_Reporting_RGARPerson = Nothing
                End If


            End If


            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Reporting Group', 'Save Data to Validate by XML finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Jalankan SP usp_GOAML_Report_Validate_PerTransactionID
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Reporting Group', 'Validate Transaction Group started (Reporting Group ID = " & objReportingGroup.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = objReport.obj_CRS_Reporting_Header.PK_CRS_INTERNATIONAL_REPORT_HEADER_ID
            param(0).DbType = SqlDbType.BigInt

            'Dim pkreportinggroup As Integer = 0
            'If objReportingGroup.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID = -1 Then
            '    pkreportinggroup = Nothing
            'Else
            '    pkreportinggroup = objReportingGroup.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID
            'End If
            param(1) = New SqlParameter
            param(1).ParameterName = "@PK_ReportGroup_ID"
            param(1).Value = objReportingGroup.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID
            param(1).DbType = SqlDbType.BigInt

            param(2) = New SqlParameter
            param(2).ParameterName = "@UserID"
            param(2).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(2).DbType = SqlDbType.VarChar


            Dim strValidationResult As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_Report_Validate_PerReportingGroupID", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'CRS International Reporting Group', 'Validate Reporting Group finished (Reporting Group ID = " & objReportingGroup.obj_Reporting_Group.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Return strValidationResult

        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function
#End Region
#Region "Get Data Report Group"
    Shared Function GetReportingGroupClassByID(ID As Long) As CRS_Reporting_Group_Class
        Using objDb As New CRSDAL.CRSEntities

            Dim objCRSReportingGroupClass = New CRS_Reporting_Group_Class
            With objCRSReportingGroupClass
                'Reporting Group
                .obj_Reporting_Group = objDb.CRS_INTERNATIONAL_REPORTING_GROUP.Where(Function(x) x.PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID = ID).FirstOrDefault

                If .obj_Reporting_Group IsNot Nothing Then

                    'Controlling Person
                    If .list_Reporting_RGARPerson IsNot Nothing AndAlso .list_Reporting_RGARPerson.Count > 0 Then
                        For Each item In .list_Reporting_RGARPerson
                            Dim listcontrollingperson = objDb.CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON.Where(Function(x) x.FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = item.PK_CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON_ID).ToList
                            If listcontrollingperson IsNot Nothing Then .list_Reporting_RGARPerson.AddRange(listcontrollingperson)
                        Next
                    Else
                        Dim listcontrollingperson = objDb.CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON.Where(Function(x) x.FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = ID).ToList
                        If listcontrollingperson IsNot Nothing Then .list_Reporting_RGARPerson.AddRange(listcontrollingperson)
                    End If

                    'Payment
                    If .list_Reporting_RGARPayment IsNot Nothing AndAlso .list_Reporting_RGARPayment.Count > 0 Then
                        For Each item In .list_Reporting_RGARPayment
                            Dim listpayment = objDb.CRS_INTERNATIONAL_RG_AR_PAYMENT.Where(Function(x) x.FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = item.PK_CRS_INTERNATIONAL_RG_AR_PAYMENT_ID).ToList
                            If listpayment IsNot Nothing Then .list_Reporting_RGARPayment.AddRange(listpayment)
                        Next
                    Else
                        Dim listpayment = objDb.CRS_INTERNATIONAL_RG_AR_PAYMENT.Where(Function(x) x.FK_CRS_INTERNATIONAL_REPORTING_GRP_ID = ID).ToList
                        If listpayment IsNot Nothing Then .list_Reporting_RGARPayment.AddRange(listpayment)
                    End If


                End If

            End With

            Return objCRSReportingGroupClass
        End Using
    End Function

    Shared Function getValidationReportByReportGroupID(strReportID As String, strID As String) As String
        Try
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.Int

            param(1) = New SqlParameter
            param(1).ParameterName = "@PK_ReportGroup_ID"
            param(1).Value = strID
            param(1).DbType = SqlDbType.Int


            Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_CRS_GetValidationReport_PerReportingGroupID", param)

        Catch ex As Exception
            Throw ex
        End Try
    End Function


#End Region
#End Region

#Region "Report Payment"
#Region "Action Report Payment"
#End Region
#Region "Validate Report Payment"
#End Region
#Region "Get Data Report Payment"
    Shared Function GetPaymentTypeByCode(Code As String) As vw_CRS_INTERNATIONAL_PAYMENT_TYPE
        Using objDb As CRSDAL.CRSEntities = New CRSDAL.CRSEntities
            Return objDb.vw_CRS_INTERNATIONAL_PAYMENT_TYPE.Where(Function(x) x.FK_CRS_PAYMENT_TYPE_CODE = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetPaymentCurrencyByCode(Code As String) As vw_CRS_INTERNATIONAL_CURRENCY
        Using objDb As CRSDAL.CRSEntities = New CRSDAL.CRSEntities
            Return objDb.vw_CRS_INTERNATIONAL_CURRENCY.Where(Function(x) x.FK_CURRENCY_CODE = Code).FirstOrDefault
        End Using
    End Function
#End Region
#End Region

#Region "Class"

    Public Class CRS_Reporting_Class

        'Reporting Header
        Public obj_CRS_Reporting_Header As New CRSDAL.CRS_INTERNATIONAL_REPORT_HEADER

        'Reporting FI
        Public obj_CRS_Reporting_FI As New CRSDAL.CRS_INTERNATIONAL_REPORTING_FI

        'Reporting Group
        Public list_CRS_Reporting_Group As New List(Of CRSDAL.CRS_INTERNATIONAL_REPORTING_GROUP)


    End Class


    Public Class CRS_Reporting_Group_Class
        'Reporting Group
        Public obj_Reporting_Group As New CRS_INTERNATIONAL_REPORTING_GROUP

        'Reporting RG AR Person
        Public list_Reporting_RGARPerson As New List(Of CRSDAL.CRS_INTERNATIONAL_RG_AR_CTRLG_PERSON)

        'Reporting RG AR Payment
        Public list_Reporting_RGARPayment As New List(Of CRSDAL.CRS_INTERNATIONAL_RG_AR_PAYMENT)

    End Class
#End Region
#End Region
End Class
