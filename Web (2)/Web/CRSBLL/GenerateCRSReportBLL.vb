﻿Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Xml
Imports Ionic.Zip
Imports Ionic.Zlib
Imports System.Xml.Schema
Imports System.IO
Imports CRSBLL
Imports CRSDAL
Public Class GenerateCRSReportBLL
#Region "CRS International"

    Public Shared Function GetGenerateCRSInternationalByID(obj As Integer) As CRS_International_Generate_Bulk
        Using objdb As CRSEntities = New CRSEntities
            Return objdb.CRS_International_Generate_Bulk.Where(Function(x) x.PK_CRS_INTERNATIONAL_GENERATE_BULK_ID = obj).FirstOrDefault
        End Using
    End Function
#End Region
End Class
