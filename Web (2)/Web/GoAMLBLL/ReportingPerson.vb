﻿Imports NawaBLL
Imports GoAMLDAL

Imports Ext.Net
Imports System.Text
Imports System.Data.SqlClient

Public Class ReportingPerson

#Region "CRUD"
    Sub SaveAddTanpaApproval(Person As ReportingPersonData, objSchemaModule As NawaDAL.Module)
        'Sub SaveAddTanpaApproval(objData As goAML_ODM_Reporting_Person, listIdentification As List(Of goAML_Person_Identification), listPhone As List(Of goAML_Ref_Phone), listAddress As List(Of goAML_Ref_Address), listEmpPhone As List(Of goAML_Ref_Phone), listEmpAddress As List(Of goAML_Ref_Address), objSchemaModule As NawaDAL.Module)
        'not done: code SaveAddTanpaApproval

        Using objdb As New GoAMLDAL.GoAMLEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Person.objPerson.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    Person.objPerson.ApprovedDate = DateTime.Now
                    objdb.Entry(Person.objPerson).State = Entity.EntityState.Added

                    Dim objaudittrailheader As New GoAMLDAL.AuditTrailHeader
                    objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objaudittrailheader.CreatedDate = Date.Now '.ToString("yyyy-MM-dd HH:mm:ss.fffffff")
                    objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                    objaudittrailheader.ModuleLabel = objSchemaModule.ModuleLabel
                    objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                    objdb.SaveChanges()

                    For Each item As goAML_Person_Identification In Person.ObjIdent
                        item.FK_Person_ID = Person.objPerson.PK_ODM_Reporting_Person_ID
                        item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        item.ApprovedDate = DateTime.Now
                        item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        item.CreatedDate = Date.Now

                        objdb.Entry(item).State = Entity.EntityState.Added
                    Next

                    For Each item As goAML_Ref_Address In Person.ObjAddress
                        item.FK_To_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID
                        item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        item.ApprovedDate = DateTime.Now
                        item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        item.CreatedDate = Date.Now

                        objdb.Entry(item).State = Entity.EntityState.Added
                    Next

                    For Each item As goAML_Ref_Address In Person.ObjEmpAddress
                        item.FK_To_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID
                        item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        item.ApprovedDate = DateTime.Now
                        item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        item.CreatedDate = Date.Now

                        objdb.Entry(item).State = Entity.EntityState.Added
                    Next

                    For Each item As goAML_Ref_Phone In Person.ObjPhone
                        item.FK_for_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID

                        objdb.Entry(item).State = Entity.EntityState.Added
                    Next

                    For Each item As goAML_Ref_Phone In Person.ObjEmpPhone
                        item.FK_for_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID

                        objdb.Entry(item).State = Entity.EntityState.Added
                    Next

                    Dim objtype As Type = Person.objPerson.GetType
                    Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                    For Each item As System.Reflection.PropertyInfo In properties
                        Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        objaudittraildetail.FieldName = item.Name
                        objaudittraildetail.OldValue = ""

                        If Not item.GetValue(Person.objPerson, Nothing) Is Nothing Then
                            objaudittraildetail.NewValue = item.GetValue(Person.objPerson, Nothing)

                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                    Next

                    For Each itemDetail As GoAMLDAL.goAML_Person_Identification In Person.ObjIdent
                        objtype = itemDetail.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemDetail, Nothing) Is Nothing Then
                                If item.GetValue(itemDetail, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemDetail, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If

                            objdb.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemDetail As GoAMLDAL.goAML_Ref_Phone In Person.ObjPhone
                        objtype = itemDetail.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemDetail, Nothing) Is Nothing Then
                                If item.GetValue(itemDetail, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemDetail, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If

                            objdb.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemDetail As GoAMLDAL.goAML_Ref_Phone In Person.ObjEmpPhone
                        objtype = itemDetail.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemDetail, Nothing) Is Nothing Then
                                If item.GetValue(itemDetail, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemDetail, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If

                            objdb.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemDetail As GoAMLDAL.goAML_Ref_Address In Person.ObjAddress
                        objtype = itemDetail.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemDetail, Nothing) Is Nothing Then
                                If item.GetValue(itemDetail, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemDetail, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If

                            objdb.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemDetail As GoAMLDAL.goAML_Ref_Address In Person.ObjEmpAddress
                        objtype = itemDetail.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemDetail, Nothing) Is Nothing Then
                                If item.GetValue(itemDetail, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemDetail, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If

                            objdb.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    objdb.SaveChanges()

                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub


    Sub SaveAddApproval(Person As ReportingPersonData, objModule As NawaDAL.Module)
        Using objDB As New GoAMLDAL.GoAMLEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objData As New GoAMLBLL.ReportingPersonData
                    objData.objPerson = Person.objPerson
                    objData.ObjIdent = Person.ObjIdent
                    objData.ObjAddress = Person.ObjAddress
                    objData.ObjPhone = Person.ObjPhone
                    objData.ObjEmpAddress = Person.ObjEmpAddress
                    objData.ObjEmpPhone = Person.ObjEmpPhone

                    Dim dataXML As String = NawaBLL.Common.Serialize(objData)

                    Dim objModuleApproval As New GoAMLDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = 0
                        .ModuleField = dataXML
                        .ModuleFieldBefore = ""
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added

                    Dim objATHeader As New GoAMLDAL.AuditTrailHeader
                    objATHeader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objATHeader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objATHeader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                    objATHeader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    objATHeader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                    objATHeader.ModuleLabel = objModule.ModuleLabel
                    objDB.Entry(objATHeader).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    Dim objtype As Type = objData.GetType
                    Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                    For Each item As System.Reflection.PropertyInfo In properties
                        Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                        objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                        objATDetail.FieldName = item.Name
                        objATDetail.OldValue = ""

                        If Not item.GetValue(objData, Nothing) Is Nothing Then
                            objATDetail.NewValue = item.GetValue(objData, Nothing)
                        Else
                            objATDetail.NewValue = ""
                        End If
                        objDB.Entry(objATDetail).State = Entity.EntityState.Added
                    Next

                    For Each itemDetail As GoAMLDAL.goAML_Person_Identification In Person.ObjIdent
                        objtype = itemDetail.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                            objaudittraildetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objaudittraildetail.FieldName = item.Name
                            objaudittraildetail.OldValue = ""

                            If Not item.GetValue(itemDetail, Nothing) Is Nothing Then
                                If item.GetValue(itemDetail, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objaudittraildetail.NewValue = item.GetValue(itemDetail, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                            objDB.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemDetail As GoAMLDAL.goAML_Ref_Address In Person.ObjAddress
                        objtype = itemDetail.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                            objaudittraildetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objaudittraildetail.FieldName = item.Name
                            objaudittraildetail.OldValue = ""

                            If Not item.GetValue(itemDetail, Nothing) Is Nothing Then
                                If item.GetValue(itemDetail, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objaudittraildetail.NewValue = item.GetValue(itemDetail, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                            objDB.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemDetail As GoAMLDAL.goAML_Ref_Address In Person.ObjEmpAddress
                        objtype = itemDetail.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                            objaudittraildetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objaudittraildetail.FieldName = item.Name
                            objaudittraildetail.OldValue = ""

                            If Not item.GetValue(itemDetail, Nothing) Is Nothing Then
                                If item.GetValue(itemDetail, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objaudittraildetail.NewValue = item.GetValue(itemDetail, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                            objDB.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemDetail As GoAMLDAL.goAML_Ref_Phone In Person.ObjPhone
                        objtype = itemDetail.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                            objaudittraildetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objaudittraildetail.FieldName = item.Name
                            objaudittraildetail.OldValue = ""

                            If Not item.GetValue(itemDetail, Nothing) Is Nothing Then
                                If item.GetValue(itemDetail, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objaudittraildetail.NewValue = item.GetValue(itemDetail, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                            objDB.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemDetail As GoAMLDAL.goAML_Ref_Phone In Person.ObjEmpPhone
                        objtype = itemDetail.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                            objaudittraildetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objaudittraildetail.FieldName = item.Name
                            objaudittraildetail.OldValue = ""

                            If Not item.GetValue(itemDetail, Nothing) Is Nothing Then
                                If item.GetValue(itemDetail, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objaudittraildetail.NewValue = item.GetValue(itemDetail, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                            objDB.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        Next
                    Next

                    objDB.SaveChanges()
                    objTrans.Commit()
                    Nawa.BLL.NawaFramework.SendEmailModuleApproval(objModuleApproval.PK_ModuleApproval_ID)
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub


    Sub SaveEditTanpaApproval(Person As ReportingPersonData, objModule As NawaDAL.Module)
        Using objDB As New GoAMLDAL.GoAMLEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    Person.objPerson.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    Person.objPerson.ApprovedDate = Now
                    objDB.Entry(Person.objPerson).State = Entity.EntityState.Modified

                    Dim objATHeader As New GoAMLDAL.AuditTrailHeader
                    objATHeader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objATHeader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objATHeader.CreatedDate = Date.Now 'Now.ToString("yyyy-MM-dd HH:mm:ss")
                    objATHeader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    objATHeader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                    objATHeader.ModuleLabel = objModule.ModuleLabel
                    objDB.Entry(objATHeader).State = Entity.EntityState.Added
                    objDB.SaveChanges()

#Region "Identification"
                    For Each itemx As GoAMLDAL.goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = Person.objPerson.PK_ODM_Reporting_Person_ID Select x).ToList
                        Dim objcek As GoAMLDAL.goAML_Person_Identification = Person.ObjIdent.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID)
                        If objcek Is Nothing Then

                            objDB.Entry(itemx).State = Entity.EntityState.Deleted
                        End If
                    Next

                    For Each item As GoAMLDAL.goAML_Person_Identification In Person.ObjIdent
                        Dim obcek As GoAMLDAL.goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = item.PK_Person_Identification_ID Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            item.FK_Person_ID = Person.objPerson.PK_ODM_Reporting_Person_ID
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = DateTime.Now
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Date.Now

                            objDB.Entry(item).State = Entity.EntityState.Added
                        Else
                            obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.LastUpdateDate = Date.Now

                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = Entity.EntityState.Modified
                        End If
                    Next
#End Region

#Region "Phone"
                    For Each itemx As GoAMLDAL.goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 2 Select x).ToList
                        Dim objcek As GoAMLDAL.goAML_Ref_Phone = Person.ObjPhone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = Entity.EntityState.Deleted
                        End If
                    Next

                    For Each item As GoAMLDAL.goAML_Ref_Phone In Person.ObjPhone
                        Dim obcek As GoAMLDAL.goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 2 And x.FK_for_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            item.FK_for_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = Entity.EntityState.Modified
                        End If
                    Next
#End Region

#Region "Employer Phone"
                    For Each itemx As GoAMLDAL.goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 11 Select x).ToList
                        Dim objcek As GoAMLDAL.goAML_Ref_Phone = Person.ObjEmpPhone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = Entity.EntityState.Deleted
                        End If
                    Next

                    For Each item As GoAMLDAL.goAML_Ref_Phone In Person.ObjEmpPhone
                        Dim obcek As GoAMLDAL.goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 11 And x.FK_for_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            item.FK_for_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = Entity.EntityState.Modified
                        End If
                    Next
#End Region

#Region "Address"
                    For Each itemx As GoAMLDAL.goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 2 Select x).ToList
                        Dim objcek As GoAMLDAL.goAML_Ref_Address = Person.ObjAddress.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = Entity.EntityState.Deleted
                        End If
                    Next

                    For Each item As GoAMLDAL.goAML_Ref_Address In Person.ObjAddress
                        Dim obcek As GoAMLDAL.goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 2 And x.FK_To_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            item.FK_To_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = DateTime.Now
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Date.Now

                            objDB.Entry(item).State = Entity.EntityState.Added
                        Else
                            obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.LastUpdateDate = Date.Now

                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = Entity.EntityState.Modified
                        End If
                    Next
#End Region

#Region "Employer Address"
                    For Each itemx As GoAMLDAL.goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 11 Select x).ToList
                        Dim objcek As GoAMLDAL.goAML_Ref_Address = Person.ObjEmpAddress.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = Entity.EntityState.Deleted
                        End If
                    Next

                    For Each item As GoAMLDAL.goAML_Ref_Address In Person.ObjEmpAddress
                        Dim obcek As GoAMLDAL.goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 11 And x.FK_To_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            item.FK_To_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = DateTime.Now
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Date.Now

                            objDB.Entry(item).State = Entity.EntityState.Added
                        Else
                            obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.LastUpdateDate = Date.Now

                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = Entity.EntityState.Modified
                        End If
                    Next
#End Region

                    Dim objtype As Type = Person.objPerson.GetType
                    Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                    For Each item As System.Reflection.PropertyInfo In properties
                        Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                        objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                        objATDetail.FieldName = item.Name
                        objATDetail.OldValue = ""

                        If Not item.GetValue(Person.objPerson, Nothing) Is Nothing Then
                            objATDetail.NewValue = item.GetValue(Person.objPerson, Nothing)
                        Else
                            objATDetail.NewValue = ""
                        End If
                        objDB.Entry(objATDetail).State = Entity.EntityState.Added
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Person_Identification In Person.ObjIdent
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If

                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Ref_Phone In Person.ObjPhone
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If

                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Ref_Phone In Person.ObjEmpPhone
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If

                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Ref_Address In Person.ObjAddress
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If

                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Ref_Address In Person.ObjEmpAddress
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If

                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub



    Sub SaveEditApproval(Person As ReportingPersonData, objModule As NawaDAL.Module)
        Using objDB As New GoAMLDAL.GoAMLEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objData As New GoAMLBLL.ReportingPersonData
                    objData.objPerson = Person.objPerson
                    objData.ObjIdent = Person.ObjIdent
                    objData.ObjAddress = Person.ObjAddress
                    objData.ObjPhone = Person.ObjPhone
                    objData.ObjEmpAddress = Person.ObjEmpAddress
                    objData.ObjEmpPhone = Person.ObjEmpPhone

                    Dim dataXML As String = NawaBLL.Common.Serialize(objData)

                    Dim objRPBefore As goAML_ODM_Reporting_Person = objDB.goAML_ODM_Reporting_Person.Where(Function(x) x.PK_ODM_Reporting_Person_ID = Person.objPerson.PK_ODM_Reporting_Person_ID).FirstOrDefault
                    Dim objIdentBefore As List(Of goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = Person.objPerson.PK_ODM_Reporting_Person_ID And x.isReportingPerson = True).ToList
                    Dim objAddressBefore As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 2).ToList
                    Dim objPhoneBefore As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 2).ToList
                    Dim objEmpAddressBefore As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 11).ToList
                    Dim objEmpPhoneBefore As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = Person.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 11).ToList

                    Dim objDataBefore As New GoAMLBLL.ReportingPersonData
                    objDataBefore.objPerson = objRPBefore
                    objDataBefore.ObjIdent = objIdentBefore
                    objDataBefore.ObjAddress = objAddressBefore
                    objDataBefore.ObjPhone = objPhoneBefore
                    objDataBefore.ObjEmpAddress = objEmpAddressBefore
                    objDataBefore.ObjEmpPhone = objEmpPhoneBefore
                    Dim dataXMLBefore As String = NawaBLL.Common.Serialize(objDataBefore)

                    Dim objModuleApproval As New GoAMLDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = Person.objPerson.PK_ODM_Reporting_Person_ID
                        .ModuleField = dataXML
                        .ModuleFieldBefore = dataXMLBefore
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added

                    Dim objATHeader As New GoAMLDAL.AuditTrailHeader
                    objATHeader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objATHeader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objATHeader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                    objATHeader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    objATHeader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                    objATHeader.ModuleLabel = objModule.ModuleLabel
                    objDB.Entry(objATHeader).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    Dim objtype As Type = Person.objPerson.GetType
                    Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                    For Each item As System.Reflection.PropertyInfo In properties
                        Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                        objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                        objATDetail.FieldName = item.Name

                        If Not item.GetValue(objRPBefore, Nothing) Is Nothing Then
                            objATDetail.OldValue = item.GetValue(objRPBefore, Nothing)
                        Else
                            objATDetail.OldValue = ""
                        End If

                        If Not item.GetValue(Person.objPerson, Nothing) Is Nothing Then
                            objATDetail.NewValue = item.GetValue(Person.objPerson, Nothing)
                        Else
                            objATDetail.NewValue = ""
                        End If
                        objDB.Entry(objATDetail).State = Entity.EntityState.Added
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Person_Identification In Person.ObjIdent
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If
                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Ref_Address In Person.ObjAddress
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If
                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Ref_Address In Person.ObjEmpAddress
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If
                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Ref_Phone In Person.ObjPhone
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If
                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Ref_Phone In Person.ObjEmpPhone
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If
                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    objDB.SaveChanges()
                    objTrans.Commit()
                    Nawa.BLL.NawaFramework.SendEmailModuleApproval(objModuleApproval.PK_ModuleApproval_ID)
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub



    Sub DeleteTanpaapproval(ID As String, objSchemaModule As NawaDAL.Module)
        Using objDB As New GoAMLDAL.GoAMLEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objTask As GoAMLDAL.goAML_ODM_Reporting_Person = objDB.goAML_ODM_Reporting_Person.Where(Function(x) x.PK_ODM_Reporting_Person_ID = ID).FirstOrDefault
                    Dim listIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = ID).ToList
                    Dim listPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID).ToList
                    Dim listAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID).ToList

                    objDB.Entry(objTask).State = Entity.EntityState.Deleted

                    For Each item As GoAMLDAL.goAML_Person_Identification In listIdentification
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                    Next
                    For Each item As GoAMLDAL.goAML_Ref_Phone In listPhone
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                    Next
                    For Each item As GoAMLDAL.goAML_Ref_Address In listAddress
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                    Next

                    Dim objATHeader As New GoAMLDAL.AuditTrailHeader
                    objATHeader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objATHeader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objATHeader.CreatedDate = Date.Now 'Now.ToString("yyyy-MM-dd HH:mm:ss")
                    objATHeader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    objATHeader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                    objATHeader.ModuleLabel = objSchemaModule.ModuleLabel
                    objDB.Entry(objATHeader).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    Dim objtype As Type = objTask.GetType
                    Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                    For Each item As System.Reflection.PropertyInfo In properties
                        Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                        objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                        objATDetail.FieldName = item.Name
                        objATDetail.OldValue = ""

                        If Not item.GetValue(objTask, Nothing) Is Nothing Then
                            objATDetail.NewValue = item.GetValue(objTask, Nothing)
                        Else
                            objATDetail.NewValue = ""
                        End If
                        objDB.Entry(objATDetail).State = Entity.EntityState.Added
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Person_Identification In listIdentification
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If
                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Ref_Phone In listPhone
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If
                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Ref_Address In listAddress
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If
                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub



    Sub DeleteDenganapproval(ID As String, objSchemaModule As NawaDAL.Module)
        Using objDB As New GoAMLDAL.GoAMLEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objRP As GoAMLDAL.goAML_ODM_Reporting_Person = objDB.goAML_ODM_Reporting_Person.Where(Function(x) x.PK_ODM_Reporting_Person_ID = ID).FirstOrDefault
                    Dim listIdent As List(Of GoAMLDAL.goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = ID And x.isReportingPerson = True).ToList
                    Dim listAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID And (x.FK_Ref_Detail_Of = 2 Or x.FK_Ref_Detail_Of = 5)).ToList
                    Dim listPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID And (x.FK_Ref_Detail_Of = 2 Or x.FK_Ref_Detail_Of = 5)).ToList

                    Dim objRPData As New GoAMLBLL.ReportingPersonData
                    objRPData.objPerson = objRP
                    objRPData.ObjIdent = listIdent
                    objRPData.ObjAddress = listAddress
                    objRPData.ObjPhone = listPhone

                    Dim dataXML As String = NawaBLL.Common.Serialize(objRPData)
                    Dim objModuleApproval As New GoAMLDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objSchemaModule.ModuleName
                        .ModuleKey = ID
                        .ModuleField = dataXML
                        .ModuleFieldBefore = ""
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added

                    Dim objATHeader As New GoAMLDAL.AuditTrailHeader
                    objATHeader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objATHeader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objATHeader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                    objATHeader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    objATHeader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                    objATHeader.ModuleLabel = objSchemaModule.ModuleLabel
                    objDB.Entry(objATHeader).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    Dim objtype As Type = objRP.GetType
                    Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                    For Each item As System.Reflection.PropertyInfo In properties
                        Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                        objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                        objATDetail.FieldName = item.Name
                        objATDetail.OldValue = ""

                        If Not item.GetValue(objRP, Nothing) Is Nothing Then
                            objATDetail.NewValue = item.GetValue(objRP, Nothing)
                        Else
                            objATDetail.NewValue = ""
                        End If
                        objDB.Entry(objATDetail).State = Entity.EntityState.Added
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Person_Identification In listIdent
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If
                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Ref_Address In listAddress
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If
                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    For Each itemheader As GoAMLDAL.goAML_Ref_Phone In listPhone
                        objtype = itemheader.GetType
                        properties = objtype.GetProperties
                        For Each item As System.Reflection.PropertyInfo In properties
                            Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                            objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                            objATDetail.FieldName = item.Name
                            objATDetail.OldValue = ""

                            If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                            Else
                                objATDetail.NewValue = ""
                            End If
                            objDB.Entry(objATDetail).State = Entity.EntityState.Added
                        Next
                    Next

                    objDB.SaveChanges()
                    objTrans.Commit()
                    Nawa.BLL.NawaFramework.SendEmailModuleApproval(objModuleApproval.PK_ModuleApproval_ID)
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

#End Region
#Region "Get Data"

    'Get Last Pk Reporting Person
    Private Function getLastReportingPersonID() As Integer
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim RP_ID As Integer
            RP_ID = objDb.goAML_ODM_Reporting_Person.OrderByDescending(Function(x) x.PK_ODM_Reporting_Person_ID).Select(Function(x) x.PK_ODM_Reporting_Person_ID).FirstOrDefault()
            Return RP_ID
        End Using
    End Function


    'Get Data By Id
    Shared Function GetReportingPersonByID(id As Integer) As GoAMLDAL.goAML_ODM_Reporting_Person
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_ODM_Reporting_Person.Where(Function(x) x.PK_ODM_Reporting_Person_ID = id).FirstOrDefault
        End Using
    End Function
    Shared Function GetListIdentificationByID(personID As Long) As List(Of GoAMLDAL.goAML_Person_Identification)
        Dim objIdent As New List(Of GoAMLDAL.goAML_Person_Identification)
        Using objdb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim Ident As New List(Of GoAMLDAL.goAML_Person_Identification)
            Ident = objdb.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = personID And x.isReportingPerson = True).ToList
            If Ident IsNot Nothing Then
                objIdent = Ident
            End If
        End Using
        Return objIdent
    End Function
    Shared Function GetListPhoneByID(personID As Long, fk As Integer) As List(Of GoAMLDAL.goAML_Ref_Phone)
        Dim objPhone As New List(Of GoAMLDAL.goAML_Ref_Phone)
        Using objdb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim Phone As New List(Of GoAMLDAL.goAML_Ref_Phone)
            Phone = objdb.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = personID And x.FK_Ref_Detail_Of = fk).ToList
            If Phone IsNot Nothing Then
                objPhone = Phone
            End If
        End Using
        Return objPhone
    End Function
    Shared Function GetListAddressByID(personID As Long, fk As Integer) As List(Of GoAMLDAL.goAML_Ref_Address)
        Dim objAddress As New List(Of GoAMLDAL.goAML_Ref_Address)
        Using objdb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim Address As New List(Of GoAMLDAL.goAML_Ref_Address)
            Address = objdb.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = personID And x.FK_Ref_Detail_Of = fk).ToList
            If Address IsNot Nothing Then
                objAddress = Address
            End If
        End Using
        Return objAddress
    End Function
    Shared Function GetListVwIdentificationByID(personID As Long) As List(Of GoAMLDAL.Vw_Temp_Identification)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Temp_Identification Where x.FK_Person_ID = personID And x.isReportingPerson = True Select x).ToList
        End Using
    End Function
    Shared Function GetListVwPhoneByID(personID As Long, fk As Integer) As List(Of GoAMLDAL.Vw_Temp_Phone)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Temp_Phone Where x.FK_for_Table_ID = personID And x.FK_Ref_Detail_Of = fk Select x).ToList
        End Using
    End Function
    Shared Function GetListVwAddressByID(personID As Long, fk As Integer) As List(Of GoAMLDAL.Vw_Temp_Address)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Temp_Address Where x.FK_To_Table_ID = personID And x.FK_Ref_Detail_Of = fk Select x).ToList
        End Using
    End Function
    Shared Function GetCountryName(Code As String) As String
        Dim keterangan As String = ""
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim negara As GoAMLDAL.goAML_Ref_Nama_Negara = objDb.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = Code).FirstOrDefault
            If negara IsNot Nothing Then
                keterangan = negara.Keterangan
            End If
            Return keterangan
        End Using
    End Function
    Shared Function GetKodeLaporan(Code As String) As String
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Jenis_Laporan Where x.Kode = Code Select x.Keterangan).FirstOrDefault
        End Using
    End Function
    Shared Function GetKategoriKontak(Code As String) As String
        Dim keterangan As String = ""
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim alamat As GoAMLDAL.goAML_Ref_Kategori_Kontak = objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = Code).FirstOrDefault
            If alamat IsNot Nothing Then
                keterangan = alamat.Keterangan
            End If
            Return keterangan
        End Using
    End Function

    Shared Function GetJenisAlatKomunikasi(Code As String) As String
        Dim keterangan As String = ""
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim jenisAlat As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = objDb.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Kode = Code).FirstOrDefault
            If jenisAlat IsNot Nothing Then
                keterangan = jenisAlat.Keterangan
            End If
            Return keterangan
        End Using
    End Function

    Shared Function GetJenisIdentitas(Code As String) As String
        Dim keterangan As String = ""
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim jenisIdentitas As GoAMLDAL.goAML_Ref_Jenis_Dokumen_Identitas = objDb.goAML_Ref_Jenis_Dokumen_Identitas.Where(Function(x) x.Kode = Code).FirstOrDefault
            If jenisIdentitas IsNot Nothing Then
                keterangan = jenisIdentitas.Keterangan
            End If
            Return keterangan
        End Using
    End Function

#End Region
#Region "Approval"
    Sub LoadPanel(objPanel As FormPanel, objData As String, unikID As String)
        Dim objPerson As GoAMLBLL.ReportingPersonData = NawaBLL.Common.Deserialize(objData, GetType(GoAMLBLL.ReportingPersonData))
        If Not objPerson Is Nothing Then
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ID", "PK_ODM_Reporting_Person_ID" & unikID, objPerson.objPerson.PK_ODM_Reporting_Person_ID)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kode Laporan", "Kode_Laporan" & unikID, GetKodeLaporan(objPerson.objPerson.Kode_Laporan))
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gelar", "Title" & unikID, objPerson.objPerson.Title)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Lengkap", "Last_Name" & unikID, objPerson.objPerson.Last_Name)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Lahir", "BirthDate" & unikID, objPerson.objPerson.BirthDate)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Lahir", "Birth_Place" & unikID, objPerson.objPerson.Birth_Place)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Jenis Kelamin", "Gender" & unikID, objPerson.objPerson.Gender)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Ibu Kandung", "Mothers_Name" & unikID, objPerson.objPerson.Mothers_Name)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Alias", "Alias" & unikID, objPerson.objPerson.Alias)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NIK", "SSN" & unikID, objPerson.objPerson.SSN)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Paspor", "Passport_Number" & unikID, objPerson.objPerson.Passport_Number)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Penerbit Paspor", "Passport_Country" & unikID, objPerson.objPerson.Passport_Country)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Identitas Lainnya", "Id_Number" & unikID, objPerson.objPerson.Id_Number)


            'Phone
            Dim objStorePhone As New Ext.Net.Store
            objStorePhone.ID = unikID & "StoreGridPhone"
            objStorePhone.ClientIDMode = Web.UI.ClientIDMode.Static

            Dim objModelPhone As New Ext.Net.Model
            Dim objFieldPhone As Ext.Net.ModelField

            objFieldPhone = New Ext.Net.ModelField
            objFieldPhone.Name = "PK_goAML_Ref_Phone"
            objFieldPhone.Type = ModelFieldType.Auto
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New Ext.Net.ModelField
            objFieldPhone.Name = "Tph_Contact_Type"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New Ext.Net.ModelField
            objFieldPhone.Name = "Tph_Communication_Type"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New Ext.Net.ModelField
            objFieldPhone.Name = "tph_country_prefix"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New Ext.Net.ModelField
            objFieldPhone.Name = "tph_number"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New Ext.Net.ModelField
            objFieldPhone.Name = "tph_extension"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New Ext.Net.ModelField
            objFieldPhone.Name = "comments"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objStorePhone.Model.Add(objModelPhone)

            Dim objListcolumnPhone As New List(Of ColumnBase)

            Using objcolumnNo As New Ext.Net.RowNumbererColumn
                objcolumnNo.Text = "No."
                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                objListcolumnPhone.Add(objcolumnNo)
            End Using

            Dim objColumnPhone As Ext.Net.Column

            objColumnPhone = New Ext.Net.Column
            objColumnPhone.Text = "Kategori Kontak"
            objColumnPhone.DataIndex = "Tph_Contact_Type"
            objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListcolumnPhone.Add(objColumnPhone)

            objColumnPhone = New Ext.Net.Column
            objColumnPhone.Text = "Jenis Alat Komunikasi"
            objColumnPhone.DataIndex = "Tph_Communication_Type"
            objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListcolumnPhone.Add(objColumnPhone)

            objColumnPhone = New Ext.Net.Column
            objColumnPhone.Text = "Kode Area Telepon"
            objColumnPhone.DataIndex = "tph_country_prefix"
            objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListcolumnPhone.Add(objColumnPhone)

            objColumnPhone = New Ext.Net.Column
            objColumnPhone.Text = "Nomor Telepon"
            objColumnPhone.DataIndex = "tph_number"
            objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListcolumnPhone.Add(objColumnPhone)

            objColumnPhone = New Ext.Net.Column
            objColumnPhone.Text = "Nomor Ektensi"
            objColumnPhone.DataIndex = "tph_extension"
            objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListcolumnPhone.Add(objColumnPhone)

            objColumnPhone = New Ext.Net.Column
            objColumnPhone.Text = "Catatan"
            objColumnPhone.DataIndex = "comments"
            objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListcolumnPhone.Add(objColumnPhone)

            'Dim objparamPhone(0) As System.Data.SqlClient.SqlParameter
            'objparamPhone(0) = New SqlClient.SqlParameter
            'objparamPhone(0).ParameterName = "@ReportingPersonPhoneID"
            'objparamPhone(0).SqlDbType = SqlDbType.BigInt
            'objparamPhone(0).Value = objPerson.objPerson.PK_ODM_Reporting_Person_ID

            Dim objdtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(objPerson.ObjPhone)

            For Each item As DataRow In objdtPhone.Rows
                item("Tph_Contact_Type") = GetKategoriKontak(item("Tph_Contact_Type"))
            Next

            For Each item As DataRow In objdtPhone.Rows
                item("Tph_Communication_Type") = GetJenisAlatKomunikasi(item("Tph_Communication_Type"))
            Next

            Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Telepon", objStorePhone, objListcolumnPhone, objdtPhone)



            'Address
            Dim objStoreAddress As New Ext.Net.Store
            objStoreAddress.ID = unikID & "StoreGridAddress"
            objStoreAddress.ClientIDMode = Web.UI.ClientIDMode.Static

            Dim objModelAddress As New Ext.Net.Model
            Dim objFieldAddress As Ext.Net.ModelField

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "PK_Customer_Address_ID"
            objFieldAddress.Type = ModelFieldType.Auto
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "Address_Type"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "Address"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "Town"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "City"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "Zip"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "Country_Code"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "State"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "Comments"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objStoreAddress.Model.Add(objModelAddress)

            Dim objListcolumnAddress As New List(Of ColumnBase)

            Using objcolumnNo As New Ext.Net.RowNumbererColumn
                objcolumnNo.Text = "No."
                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                objListcolumnAddress.Add(objcolumnNo)
            End Using

            Dim objColumnAddress As Ext.Net.Column

            objColumnAddress = New Ext.Net.Column
            objColumnAddress.Text = "Tipe Alamat"
            objColumnAddress.DataIndex = "Address_Type"
            objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListcolumnAddress.Add(objColumnAddress)

            objColumnAddress = New Ext.Net.Column
            objColumnAddress.Text = "Alamat"
            objColumnAddress.DataIndex = "Address"
            objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListcolumnAddress.Add(objColumnAddress)

            objColumnAddress = New Ext.Net.Column
            objColumnAddress.Text = "Kecamatan"
            objColumnAddress.DataIndex = "Town"
            objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListcolumnAddress.Add(objColumnAddress)

            objColumnAddress = New Ext.Net.Column
            objColumnAddress.Text = "Kota/Kabupaten"
            objColumnAddress.DataIndex = "City"
            objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListcolumnAddress.Add(objColumnAddress)

            objColumnAddress = New Ext.Net.Column
            objColumnAddress.Text = "Kode Pos"
            objColumnAddress.DataIndex = "Zip"
            objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListcolumnAddress.Add(objColumnAddress)

            objColumnAddress = New Ext.Net.Column
            objColumnAddress.Text = "Negara"

            objColumnAddress.DataIndex = "Country_Code" 'GetCountryName("Country_Code")
            objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListcolumnAddress.Add(objColumnAddress)

            objColumnAddress = New Ext.Net.Column
            objColumnAddress.Text = "Provinsi"
            objColumnAddress.DataIndex = "State"
            objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListcolumnAddress.Add(objColumnAddress)

            objColumnAddress = New Ext.Net.Column
            objColumnAddress.Text = "Catatan"
            objColumnAddress.DataIndex = "Comments"
            objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListcolumnAddress.Add(objColumnAddress)

            Dim objparamAddress(0) As System.Data.SqlClient.SqlParameter
            objparamAddress(0) = New SqlClient.SqlParameter
            objparamAddress(0).ParameterName = "@ReportingPersonAddressID"
            objparamAddress(0).SqlDbType = SqlDbType.BigInt
            objparamAddress(0).Value = objPerson.objPerson.PK_ODM_Reporting_Person_ID

            Dim objdtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(objPerson.ObjAddress)

            For Each item As DataRow In objdtAddress.Rows
                item("Address_Type") = GetKategoriKontak(item("Address_Type"))
            Next

            For Each item As DataRow In objdtAddress.Rows
                item("Country_Code") = GetCountryName(item("Country_Code"))
            Next

            Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Alamat", objStoreAddress, objListcolumnAddress, objdtAddress)


            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 1", "Nationality1" & unikID, GetCountryName(objPerson.objPerson.Nationality1))
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 2", "Nationality2" & unikID, GetCountryName(objPerson.objPerson.Nationality2))
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 3", "Nationality3" & unikID, GetCountryName(objPerson.objPerson.Nationality3))
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Domisili", "Residence" & unikID, GetCountryName(objPerson.objPerson.Residence))
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 1", "Email1" & unikID, objPerson.objPerson.Email1)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 2", "Email2" & unikID, objPerson.objPerson.Email2)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 3", "Email3" & unikID, objPerson.objPerson.Email3)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 4", "Email4" & unikID, objPerson.objPerson.Email4)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 5", "Email5" & unikID, objPerson.objPerson.Email5)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Pekerjaan", "Occupation" & unikID, objPerson.objPerson.Occupation)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Bekerja", "Employer_Name" & unikID, objPerson.objPerson.Employer_Name)

            'Employer Phone
            Dim objStorePhoneEmployer As New Ext.Net.Store
            objStorePhoneEmployer.ID = unikID & "StoreGridPhoneEmployer"
            objStorePhoneEmployer.ClientIDMode = Web.UI.ClientIDMode.Static

            Dim objModelPhoneEmployer As New Ext.Net.Model
            Dim objFieldPhoneEmployer As Ext.Net.ModelField

            objFieldPhoneEmployer = New Ext.Net.ModelField
            objFieldPhoneEmployer.Name = "PK_goAML_Ref_Phone"
            objFieldPhoneEmployer.Type = ModelFieldType.Auto
            objModelPhoneEmployer.Fields.Add(objFieldPhoneEmployer)

            objFieldPhoneEmployer = New Ext.Net.ModelField
            objFieldPhoneEmployer.Name = "Tph_Contact_Type"
            objFieldPhoneEmployer.Type = ModelFieldType.String
            objModelPhoneEmployer.Fields.Add(objFieldPhoneEmployer)

            objFieldPhoneEmployer = New Ext.Net.ModelField
            objFieldPhoneEmployer.Name = "Tph_Communication_Type"
            objFieldPhoneEmployer.Type = ModelFieldType.String
            objModelPhoneEmployer.Fields.Add(objFieldPhoneEmployer)

            objFieldPhoneEmployer = New Ext.Net.ModelField
            objFieldPhoneEmployer.Name = "tph_country_prefix"
            objFieldPhoneEmployer.Type = ModelFieldType.String
            objModelPhoneEmployer.Fields.Add(objFieldPhoneEmployer)

            objFieldPhoneEmployer = New Ext.Net.ModelField
            objFieldPhoneEmployer.Name = "tph_number"
            objFieldPhoneEmployer.Type = ModelFieldType.String
            objModelPhoneEmployer.Fields.Add(objFieldPhoneEmployer)

            objFieldPhoneEmployer = New Ext.Net.ModelField
            objFieldPhoneEmployer.Name = "tph_extension"
            objFieldPhoneEmployer.Type = ModelFieldType.String
            objModelPhoneEmployer.Fields.Add(objFieldPhoneEmployer)

            objFieldPhoneEmployer = New Ext.Net.ModelField
            objFieldPhoneEmployer.Name = "comments"
            objFieldPhoneEmployer.Type = ModelFieldType.String
            objModelPhoneEmployer.Fields.Add(objFieldPhoneEmployer)

            objStorePhoneEmployer.Model.Add(objModelPhoneEmployer)

            Dim objListcolumnPhoneEmployer As New List(Of ColumnBase)

            Using objcolumnNo As New Ext.Net.RowNumbererColumn
                objcolumnNo.Text = "No."
                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                objListcolumnPhoneEmployer.Add(objcolumnNo)
            End Using

            Dim objColumnPhoneEmployer As Ext.Net.Column

            objColumnPhoneEmployer = New Ext.Net.Column
            objColumnPhoneEmployer.Text = "Kategori Kontak"
            objColumnPhoneEmployer.DataIndex = "Tph_Contact_Type"
            objColumnPhoneEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhoneEmployer.Flex = 1
            objListcolumnPhoneEmployer.Add(objColumnPhoneEmployer)

            objColumnPhoneEmployer = New Ext.Net.Column
            objColumnPhoneEmployer.Text = "Jenis Alat Komunikasi"
            objColumnPhoneEmployer.DataIndex = "Tph_Communication_Type"
            objColumnPhoneEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhoneEmployer.Flex = 1
            objListcolumnPhoneEmployer.Add(objColumnPhoneEmployer)

            objColumnPhoneEmployer = New Ext.Net.Column
            objColumnPhoneEmployer.Text = "Kode Area Telepon"
            objColumnPhoneEmployer.DataIndex = "tph_country_prefix"
            objColumnPhoneEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhoneEmployer.Flex = 1
            objListcolumnPhoneEmployer.Add(objColumnPhoneEmployer)

            objColumnPhoneEmployer = New Ext.Net.Column
            objColumnPhoneEmployer.Text = "Nomor Telepon"
            objColumnPhoneEmployer.DataIndex = "tph_number"
            objColumnPhoneEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhoneEmployer.Flex = 1
            objListcolumnPhoneEmployer.Add(objColumnPhoneEmployer)

            objColumnPhoneEmployer = New Ext.Net.Column
            objColumnPhoneEmployer.Text = "Nomor Ektensi"
            objColumnPhoneEmployer.DataIndex = "tph_extension"
            objColumnPhoneEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhoneEmployer.Flex = 1
            objListcolumnPhoneEmployer.Add(objColumnPhoneEmployer)

            objColumnPhoneEmployer = New Ext.Net.Column
            objColumnPhoneEmployer.Text = "Catatan"
            objColumnPhoneEmployer.DataIndex = "comments"
            objColumnPhoneEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhoneEmployer.Flex = 1
            objListcolumnPhoneEmployer.Add(objColumnPhoneEmployer)

            'Dim objparamPhone(0) As System.Data.SqlClient.SqlParameter
            'objparamPhone(0) = New SqlClient.SqlParameter
            'objparamPhone(0).ParameterName = "@ReportingPersonPhoneID"
            'objparamPhone(0).SqlDbType = SqlDbType.BigInt
            'objparamPhone(0).Value = objPerson.objPerson.PK_ODM_Reporting_Person_ID

            Dim objdtPhoneEmployer As DataTable = NawaBLL.Common.CopyGenericToDataTable(objPerson.ObjEmpPhone)

            For Each item As DataRow In objdtPhoneEmployer.Rows
                item("Tph_Contact_Type") = GetKategoriKontak(item("Tph_Contact_Type"))
            Next

            For Each item As DataRow In objdtPhoneEmployer.Rows
                item("Tph_Communication_Type") = GetJenisAlatKomunikasi(item("Tph_Communication_Type"))
            Next

            Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Telepon Tempat Bekerja", objStorePhoneEmployer, objListcolumnPhoneEmployer, objdtPhoneEmployer)


            'Employer Address
            Dim objStoreAddressEmployer As New Ext.Net.Store
            objStoreAddressEmployer.ID = unikID & "StoreGridAddressEmployer"
            objStoreAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static

            Dim objModelAddressEmployer As New Ext.Net.Model
            Dim objFieldAddressEmployer As Ext.Net.ModelField

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "PK_Customer_Address_ID"
            objFieldAddressEmployer.Type = ModelFieldType.Auto
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "Address_Type"
            objFieldAddressEmployer.Type = ModelFieldType.String
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "Address"
            objFieldAddressEmployer.Type = ModelFieldType.String
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "Town"
            objFieldAddressEmployer.Type = ModelFieldType.String
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "City"
            objFieldAddressEmployer.Type = ModelFieldType.String
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "Zip"
            objFieldAddressEmployer.Type = ModelFieldType.String
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "Country_Code"
            objFieldAddressEmployer.Type = ModelFieldType.String
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "State"
            objFieldAddressEmployer.Type = ModelFieldType.String
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "Comments"
            objFieldAddressEmployer.Type = ModelFieldType.String
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objStoreAddressEmployer.Model.Add(objModelAddressEmployer)

            Dim objListcolumnAddressEmployer As New List(Of ColumnBase)

            Using objcolumnNo As New Ext.Net.RowNumbererColumn
                objcolumnNo.Text = "No."
                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                objListcolumnAddressEmployer.Add(objcolumnNo)
            End Using

            Dim objColumnAddressEmployer As Ext.Net.Column

            objColumnAddressEmployer = New Ext.Net.Column
            objColumnAddressEmployer.Text = "Tipe Alamat"
            objColumnAddressEmployer.DataIndex = "Address_Type"
            objColumnAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListcolumnAddressEmployer.Add(objColumnAddressEmployer)

            objColumnAddressEmployer = New Ext.Net.Column
            objColumnAddressEmployer.Text = "Alamat"
            objColumnAddressEmployer.DataIndex = "Address"
            objColumnAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddressEmployer.Flex = 1
            objListcolumnAddressEmployer.Add(objColumnAddressEmployer)

            objColumnAddressEmployer = New Ext.Net.Column
            objColumnAddressEmployer.Text = "Kecamatan"
            objColumnAddressEmployer.DataIndex = "Town"
            objColumnAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddressEmployer.Flex = 1
            objListcolumnAddressEmployer.Add(objColumnAddressEmployer)

            objColumnAddressEmployer = New Ext.Net.Column
            objColumnAddressEmployer.Text = "Kota/Kabupaten"
            objColumnAddressEmployer.DataIndex = "City"
            objColumnAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddressEmployer.Flex = 1
            objListcolumnAddressEmployer.Add(objColumnAddressEmployer)

            objColumnAddressEmployer = New Ext.Net.Column
            objColumnAddressEmployer.Text = "Kode Pos"
            objColumnAddressEmployer.DataIndex = "Zip"
            objColumnAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddressEmployer.Flex = 1
            objListcolumnAddressEmployer.Add(objColumnAddressEmployer)

            objColumnAddressEmployer = New Ext.Net.Column
            objColumnAddressEmployer.Text = "Negara"

            objColumnAddressEmployer.DataIndex = "Country_Code" 'GetCountryName("Country_Code")
            objColumnAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddressEmployer.Flex = 1
            objListcolumnAddressEmployer.Add(objColumnAddressEmployer)

            objColumnAddressEmployer = New Ext.Net.Column
            objColumnAddressEmployer.Text = "Provinsi"
            objColumnAddressEmployer.DataIndex = "State"
            objColumnAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddressEmployer.Flex = 1
            objListcolumnAddressEmployer.Add(objColumnAddressEmployer)

            objColumnAddressEmployer = New Ext.Net.Column
            objColumnAddressEmployer.Text = "Catatan"
            objColumnAddressEmployer.DataIndex = "Comments"
            objColumnAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddressEmployer.Flex = 1
            objListcolumnAddressEmployer.Add(objColumnAddressEmployer)

            Dim objparamAddressEmployer(0) As System.Data.SqlClient.SqlParameter
            objparamAddressEmployer(0) = New SqlClient.SqlParameter
            objparamAddressEmployer(0).ParameterName = "@ReportingPersonAddressID"
            objparamAddressEmployer(0).SqlDbType = SqlDbType.BigInt
            objparamAddressEmployer(0).Value = objPerson.objPerson.PK_ODM_Reporting_Person_ID

            Dim objdtAddressEmployer As DataTable = NawaBLL.Common.CopyGenericToDataTable(objPerson.ObjEmpAddress)

            For Each item As DataRow In objdtAddressEmployer.Rows
                item("Address_Type") = GetKategoriKontak(item("Address_Type"))
            Next

            For Each item As DataRow In objdtAddressEmployer.Rows
                item("Country_Code") = GetCountryName(item("Country_Code"))
            Next

            Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Alamat Tempat Bekerja", objStoreAddressEmployer, objListcolumnAddressEmployer, objdtAddressEmployer)


            'Identification
            Dim objStoreIdent As New Ext.Net.Store
            objStoreIdent.ID = unikID & "StoreGridIdentification"
            objStoreIdent.ClientIDMode = Web.UI.ClientIDMode.Static

            Dim objModelIdent As New Ext.Net.Model
            Dim objFieldIdent As Ext.Net.ModelField

            objFieldIdent = New Ext.Net.ModelField
            objFieldIdent.Name = "PK_Person_Identification_ID"
            objFieldIdent.Type = ModelFieldType.Auto
            objModelIdent.Fields.Add(objFieldIdent)

            objFieldIdent = New Ext.Net.ModelField
            objFieldIdent.Name = "Type"
            objFieldIdent.Type = ModelFieldType.String
            objModelIdent.Fields.Add(objFieldIdent)

            objFieldIdent = New Ext.Net.ModelField
            objFieldIdent.Name = "Number"
            objFieldIdent.Type = ModelFieldType.String
            objModelIdent.Fields.Add(objFieldIdent)

            objFieldIdent = New Ext.Net.ModelField
            objFieldIdent.Name = "Issue_Date"
            objFieldIdent.Type = ModelFieldType.Date
            objModelIdent.Fields.Add(objFieldIdent)

            objFieldIdent = New Ext.Net.ModelField
            objFieldIdent.Name = "Expiry_Date"
            objFieldIdent.Type = ModelFieldType.Date
            objModelIdent.Fields.Add(objFieldIdent)

            objFieldIdent = New Ext.Net.ModelField
            objFieldIdent.Name = "Issued_By"
            objFieldIdent.Type = ModelFieldType.String
            objModelIdent.Fields.Add(objFieldIdent)

            objFieldIdent = New Ext.Net.ModelField
            objFieldIdent.Name = "Issued_Country"
            objFieldIdent.Type = ModelFieldType.String
            objModelIdent.Fields.Add(objFieldIdent)

            objFieldIdent = New Ext.Net.ModelField
            objFieldIdent.Name = "Identification_Comment"
            objFieldIdent.Type = ModelFieldType.String
            objModelIdent.Fields.Add(objFieldIdent)

            objStoreIdent.Model.Add(objModelIdent)

            Dim objListcolumnIdentification As New List(Of ColumnBase)

            Using objcolumnNo As New Ext.Net.RowNumbererColumn
                objcolumnNo.Text = "No."
                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                objListcolumnIdentification.Add(objcolumnNo)
            End Using

            Dim objColumnIdent As Ext.Net.Column

            objColumnIdent = New Ext.Net.Column
            objColumnIdent.Text = "Jenis Identitas"
            objColumnIdent.DataIndex = "Type"
            objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnIdent.Flex = 1
            objListcolumnIdentification.Add(objColumnIdent)

            objColumnIdent = New Ext.Net.Column
            objColumnIdent.Text = "Nomor Identitas"
            objColumnIdent.DataIndex = "Number"
            objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnIdent.Flex = 1
            objListcolumnIdentification.Add(objColumnIdent)

            objColumnIdent = New Ext.Net.Column
            objColumnIdent.Text = "Tanggal Diterbitkan"
            objColumnIdent.DataIndex = "Issue_Date"
            objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnIdent.Flex = 1
            objListcolumnIdentification.Add(objColumnIdent)

            objColumnIdent = New Ext.Net.Column
            objColumnIdent.Text = "Tanggal Kadaluarsa"
            objColumnIdent.DataIndex = "Expiry_Date"
            objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnIdent.Flex = 1
            objListcolumnIdentification.Add(objColumnIdent)

            objColumnIdent = New Ext.Net.Column
            objColumnIdent.Text = "Diterbitkan Oleh"
            objColumnIdent.DataIndex = "Issued_By"
            objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnIdent.Flex = 1
            objListcolumnIdentification.Add(objColumnIdent)

            objColumnIdent = New Ext.Net.Column
            objColumnIdent.Text = "Negara Penerbit Identitas"
            objColumnIdent.DataIndex = "Issued_Country"
            objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnIdent.Flex = 1
            objListcolumnIdentification.Add(objColumnIdent)

            objColumnIdent = New Ext.Net.Column
            objColumnIdent.Text = "Catatan"
            objColumnIdent.DataIndex = "Identification_Comment"
            objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnIdent.Flex = 1
            objListcolumnIdentification.Add(objColumnIdent)

            Dim objparamIdent(0) As System.Data.SqlClient.SqlParameter
            objparamIdent(0) = New SqlClient.SqlParameter
            objparamIdent(0).ParameterName = "@ReportingPersonIdentificationID"
            objparamIdent(0).SqlDbType = SqlDbType.BigInt
            objparamIdent(0).Value = objPerson.objPerson.PK_ODM_Reporting_Person_ID

            Dim objdtIdent As DataTable = NawaBLL.Common.CopyGenericToDataTable(objPerson.ObjIdent)

            For Each item As DataRow In objdtIdent.Rows
                item("Type") = GetJenisIdentitas(item("Type"))
            Next

            For Each item As DataRow In objdtIdent.Rows
                item("Issued_Country") = GetCountryName(item("Issued_Country"))
            Next

            Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Dokumen Identitas", objStoreIdent, objListcolumnIdentification, objdtIdent)


            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Sudah Meninggal?", "Deceased" & unikID, objPerson.objPerson.Deceased)
            If objPerson.objPerson.Deceased = True Then
                Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Meninggal", "Deceased_Date" & unikID, objPerson.objPerson.Deceased_Date)
            End If
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NPWP", "Tax_Number" & unikID, objPerson.objPerson.Tax_Number)
            If objPerson.objPerson.Tax_Reg_Number = True Then
                Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "PEPs?", "Tax_Reg_Number" & unikID, "Y")
            Else
                Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "PEPs?", "Tax_Reg_Number" & unikID, "T")
            End If
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Sumber Dana", "Source_Of_Wealth" & unikID, objPerson.objPerson.Source_Of_Wealth)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan", "Comment" & unikID, objPerson.objPerson.Comment)

        End If
    End Sub

    Sub LoadPanelDeleteAndDetail(objPanel As FormPanel, strModulename As String, unikkey As String)
        Dim objReportingPerson As GoAMLDAL.goAML_ODM_Reporting_Person = GetReportingPersonByID(unikkey)
        If Not objReportingPerson Is Nothing Then
            Dim strunik As String = Guid.NewGuid.ToString

            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ID", "PK_ODM_Reporting_Person_ID" & strunik, objReportingPerson.PK_ODM_Reporting_Person_ID)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kode Laporan", "Kode_Laporan" & strunik, GetKodeLaporan(objReportingPerson.Kode_Laporan))
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gelar", "Title" & strunik, objReportingPerson.Title)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Lengkap", "Last_Name" & strunik, objReportingPerson.Last_Name)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Lahir", "BirthDate" & strunik, objReportingPerson.BirthDate)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Lahir", "Birth_Place" & strunik, objReportingPerson.Birth_Place)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Jenis Kelamin", "Gender" & strunik, objReportingPerson.Gender)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Ibu Kandung", "Mothers_Name" & strunik, objReportingPerson.Mothers_Name)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Alias", "Alias" & strunik, objReportingPerson.Alias)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NIK", "SSN" & strunik, objReportingPerson.SSN)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Paspor", "Passport_Number" & strunik, objReportingPerson.Passport_Number)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Penerbit Paspor", "Passport_Country" & strunik, objReportingPerson.Passport_Country)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Identitas Lainnya", "Id_Number" & strunik, objReportingPerson.Id_Number)


            'Phone
            Dim objStorePhone As New Ext.Net.Store
            objStorePhone.ID = strunik & "StoreGridPhone"
            objStorePhone.ClientIDMode = Web.UI.ClientIDMode.Static

            Dim objModelPhone As New Ext.Net.Model
            Dim objFieldPhone As Ext.Net.ModelField

            objFieldPhone = New Ext.Net.ModelField
            objFieldPhone.Name = "PK_goAML_Ref_Phone"
            objFieldPhone.Type = ModelFieldType.Auto
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New Ext.Net.ModelField
            objFieldPhone.Name = "Tph_Contact_Type"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New Ext.Net.ModelField
            objFieldPhone.Name = "Tph_Communication_Type"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New Ext.Net.ModelField
            objFieldPhone.Name = "tph_number"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objStorePhone.Model.Add(objModelPhone)

            Dim objListcolumnPhone As New List(Of ColumnBase)

            Using objcolumnNo As New Ext.Net.RowNumbererColumn
                objcolumnNo.Text = "No."
                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                objListcolumnPhone.Add(objcolumnNo)
            End Using

            Dim objColumnPhone As Ext.Net.Column

            objColumnPhone = New Ext.Net.Column
            objColumnPhone.Text = "Kategori Kontak"
            objColumnPhone.DataIndex = "Tph_Contact_Type"
            objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListcolumnPhone.Add(objColumnPhone)

            objColumnPhone = New Ext.Net.Column
            objColumnPhone.Text = "Jenis Alat Komunikasi"
            objColumnPhone.DataIndex = "Tph_Communication_Type"
            objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListcolumnPhone.Add(objColumnPhone)

            objColumnPhone = New Ext.Net.Column
            objColumnPhone.Text = "Nomor Telepon"
            objColumnPhone.DataIndex = "tph_number"
            objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListcolumnPhone.Add(objColumnPhone)

            Dim objparamPhone(0) As System.Data.SqlClient.SqlParameter
            objparamPhone(0) = New SqlClient.SqlParameter
            objparamPhone(0).ParameterName = "@PK_ODM_Reporting_Person_ID"
            objparamPhone(0).SqlDbType = SqlDbType.BigInt
            objparamPhone(0).Value = objReportingPerson.PK_ODM_Reporting_Person_ID

            Using objdata As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GetPhoneDetailByReportingPerson", objparamPhone)
                Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Telepon", objStorePhone, objListcolumnPhone, objdata)
            End Using

            'Address
            Dim objStoreAddress As New Ext.Net.Store
            objStoreAddress.ID = strunik & "StoreGridAddress"
            objStoreAddress.ClientIDMode = Web.UI.ClientIDMode.Static

            Dim objModelAddress As New Ext.Net.Model
            Dim objFieldAddress As Ext.Net.ModelField

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "PK_Customer_Address_ID"
            objFieldAddress.Type = ModelFieldType.Auto
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "Address_Type"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "Address"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "City"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New Ext.Net.ModelField
            objFieldAddress.Name = "Country_Code"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objStoreAddress.Model.Add(objModelAddress)

            Dim objListcolumnAddress As New List(Of ColumnBase)

            Using objcolumnNo As New Ext.Net.RowNumbererColumn
                objcolumnNo.Text = "No."
                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                objListcolumnAddress.Add(objcolumnNo)
            End Using

            Dim objColumnAddress As Ext.Net.Column

            objColumnAddress = New Ext.Net.Column
            objColumnAddress.Text = "Tipe Alamat"
            objColumnAddress.DataIndex = "Address_Type"
            objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListcolumnAddress.Add(objColumnAddress)

            objColumnAddress = New Ext.Net.Column
            objColumnAddress.Text = "Alamat"
            objColumnAddress.DataIndex = "Address"
            objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListcolumnAddress.Add(objColumnAddress)

            objColumnAddress = New Ext.Net.Column
            objColumnAddress.Text = "Kota/Kabupaten"
            objColumnAddress.DataIndex = "City"
            objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListcolumnAddress.Add(objColumnAddress)

            objColumnAddress = New Ext.Net.Column
            objColumnAddress.Text = "Negara"
            objColumnAddress.DataIndex = "Country_Code"
            objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListcolumnAddress.Add(objColumnAddress)

            Dim objparamAddress(0) As System.Data.SqlClient.SqlParameter
            objparamAddress(0) = New SqlClient.SqlParameter
            objparamAddress(0).ParameterName = "@PK_ODM_Reporting_Person_ID"
            objparamAddress(0).SqlDbType = SqlDbType.BigInt
            objparamAddress(0).Value = objReportingPerson.PK_ODM_Reporting_Person_ID

            Using objdata As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GetAddressDetailByReportingPerson", objparamAddress)
                Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Alamat", objStoreAddress, objListcolumnAddress, objdata)
            End Using

            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 1", "Nationality1" & strunik, GetCountryName(objReportingPerson.Nationality1))
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 2", "Nationality2" & strunik, GetCountryName(objReportingPerson.Nationality2))
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 3", "Nationality3" & strunik, GetCountryName(objReportingPerson.Nationality3))
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Domisili", "Residence" & strunik, GetCountryName(objReportingPerson.Residence))
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 1", "Email1" & strunik, objReportingPerson.Email1)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 2", "Email2" & strunik, objReportingPerson.Email2)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 3", "Email3" & strunik, objReportingPerson.Email3)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 4", "Email4" & strunik, objReportingPerson.Email4)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 5", "Email5" & strunik, objReportingPerson.Email5)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Pekerjaan", "Occupation" & strunik, objReportingPerson.Occupation)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Bekerja", "Employer_Name" & strunik, objReportingPerson.Employer_Name)

            'Employer Address
            Dim objStoreAddressEmployer As New Ext.Net.Store
            objStoreAddressEmployer.ID = strunik & "StoreGridAddressEmployer"
            objStoreAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static

            Dim objModelAddressEmployer As New Ext.Net.Model
            Dim objFieldAddressEmployer As Ext.Net.ModelField

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "PK_Customer_Address_ID"
            objFieldAddressEmployer.Type = ModelFieldType.Auto
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "Address_Type"
            objFieldAddressEmployer.Type = ModelFieldType.String
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "Address"
            objFieldAddressEmployer.Type = ModelFieldType.String
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "City"
            objFieldAddressEmployer.Type = ModelFieldType.String
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objFieldAddressEmployer = New Ext.Net.ModelField
            objFieldAddressEmployer.Name = "Country_Code"
            objFieldAddressEmployer.Type = ModelFieldType.String
            objModelAddressEmployer.Fields.Add(objFieldAddressEmployer)

            objStoreAddressEmployer.Model.Add(objModelAddressEmployer)

            Dim objListcolumnAddressEmployer As New List(Of ColumnBase)

            Using objcolumnNo As New Ext.Net.RowNumbererColumn
                objcolumnNo.Text = "No."
                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                objListcolumnAddressEmployer.Add(objcolumnNo)
            End Using

            Dim objColumnAddressEmployer As Ext.Net.Column

            objColumnAddressEmployer = New Ext.Net.Column
            objColumnAddressEmployer.Text = "Tipe Alamat"
            objColumnAddressEmployer.DataIndex = "Address_Type"
            objColumnAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddressEmployer.Flex = 1
            objListcolumnAddressEmployer.Add(objColumnAddressEmployer)

            objColumnAddressEmployer = New Ext.Net.Column
            objColumnAddressEmployer.Text = "Alamat"
            objColumnAddressEmployer.DataIndex = "Address"
            objColumnAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddressEmployer.Flex = 1
            objListcolumnAddressEmployer.Add(objColumnAddressEmployer)

            objColumnAddressEmployer = New Ext.Net.Column
            objColumnAddressEmployer.Text = "Kota/Kabupaten"
            objColumnAddressEmployer.DataIndex = "City"
            objColumnAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddressEmployer.Flex = 1
            objListcolumnAddressEmployer.Add(objColumnAddressEmployer)

            objColumnAddressEmployer = New Ext.Net.Column
            objColumnAddressEmployer.Text = "Negara"
            objColumnAddressEmployer.DataIndex = "Country_Code"
            objColumnAddressEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnAddressEmployer.Flex = 1
            objListcolumnAddressEmployer.Add(objColumnAddressEmployer)

            Dim objparamAddressEmployer(0) As System.Data.SqlClient.SqlParameter
            objparamAddressEmployer(0) = New SqlClient.SqlParameter
            objparamAddressEmployer(0).ParameterName = "@PK_ODM_Reporting_Person_ID"
            objparamAddressEmployer(0).SqlDbType = SqlDbType.BigInt
            objparamAddressEmployer(0).Value = objReportingPerson.PK_ODM_Reporting_Person_ID

            Using objdata As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GetAddressEmpDetailByReportingPerson", objparamAddressEmployer)
                Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Alamat Tempat Bekerja", objStoreAddressEmployer, objListcolumnAddressEmployer, objdata)
            End Using


            'Employer Phone
            Dim objStorePhoneEmployer As New Ext.Net.Store
            objStorePhoneEmployer.ID = strunik & "StoreGridPhoneEmployer"
            objStorePhoneEmployer.ClientIDMode = Web.UI.ClientIDMode.Static

            Dim objModelPhoneEmployer As New Ext.Net.Model
            Dim objFieldPhoneEmployer As Ext.Net.ModelField

            objFieldPhoneEmployer = New Ext.Net.ModelField
            objFieldPhoneEmployer.Name = "PK_goAML_Ref_Phone"
            objFieldPhoneEmployer.Type = ModelFieldType.Auto
            objModelPhoneEmployer.Fields.Add(objFieldPhoneEmployer)

            objFieldPhoneEmployer = New Ext.Net.ModelField
            objFieldPhoneEmployer.Name = "Tph_Contact_Type"
            objFieldPhoneEmployer.Type = ModelFieldType.String
            objModelPhoneEmployer.Fields.Add(objFieldPhoneEmployer)

            objFieldPhoneEmployer = New Ext.Net.ModelField
            objFieldPhoneEmployer.Name = "Tph_Communication_Type"
            objFieldPhoneEmployer.Type = ModelFieldType.String
            objModelPhoneEmployer.Fields.Add(objFieldPhoneEmployer)

            objFieldPhoneEmployer = New Ext.Net.ModelField
            objFieldPhoneEmployer.Name = "tph_number"
            objFieldPhoneEmployer.Type = ModelFieldType.String
            objModelPhoneEmployer.Fields.Add(objFieldPhoneEmployer)

            objStorePhoneEmployer.Model.Add(objModelPhoneEmployer)

            Dim objListcolumnPhoneEmployer As New List(Of ColumnBase)

            Using objcolumnNo As New Ext.Net.RowNumbererColumn
                objcolumnNo.Text = "No."
                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                objListcolumnPhoneEmployer.Add(objcolumnNo)
            End Using

            Dim objColumnPhoneEmployer As Ext.Net.Column

            objColumnPhoneEmployer = New Ext.Net.Column
            objColumnPhoneEmployer.Text = "Kategori Kontak"
            objColumnPhoneEmployer.DataIndex = "Tph_Contact_Type"
            objColumnPhoneEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhoneEmployer.Flex = 1
            objListcolumnPhoneEmployer.Add(objColumnPhoneEmployer)

            objColumnPhoneEmployer = New Ext.Net.Column
            objColumnPhoneEmployer.Text = "Jenis Alat Komunikasi"
            objColumnPhoneEmployer.DataIndex = "Tph_Communication_Type"
            objColumnPhoneEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhoneEmployer.Flex = 1
            objListcolumnPhoneEmployer.Add(objColumnPhoneEmployer)

            objColumnPhoneEmployer = New Ext.Net.Column
            objColumnPhoneEmployer.Text = "Nomor Telepon"
            objColumnPhoneEmployer.DataIndex = "tph_number"
            objColumnPhoneEmployer.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnPhoneEmployer.Flex = 1
            objListcolumnPhoneEmployer.Add(objColumnPhoneEmployer)

            Dim objparamPhoneEmployer(0) As System.Data.SqlClient.SqlParameter
            objparamPhoneEmployer(0) = New SqlClient.SqlParameter
            objparamPhoneEmployer(0).ParameterName = "@PK_ODM_Reporting_Person_ID"
            objparamPhoneEmployer(0).SqlDbType = SqlDbType.BigInt
            objparamPhoneEmployer(0).Value = objReportingPerson.PK_ODM_Reporting_Person_ID

            Using objdata As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GetPhoneEmpDetailByReportingPerson", objparamPhoneEmployer)
                Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Telepon Tempat Bekerja", objStorePhoneEmployer, objListcolumnPhoneEmployer, objdata)
            End Using

            'Identification
            Dim objStoreIdent As New Ext.Net.Store
            objStoreIdent.ID = strunik & "StoreGridIdentification"
            objStoreIdent.ClientIDMode = Web.UI.ClientIDMode.Static

            Dim objModelIdent As New Ext.Net.Model
            Dim objFieldIdent As Ext.Net.ModelField

            objFieldIdent = New Ext.Net.ModelField
            objFieldIdent.Name = "PK_Person_Identification_ID"
            objFieldIdent.Type = ModelFieldType.Auto
            objModelIdent.Fields.Add(objFieldIdent)

            objFieldIdent = New Ext.Net.ModelField
            objFieldIdent.Name = "Type"
            objFieldIdent.Type = ModelFieldType.String
            objModelIdent.Fields.Add(objFieldIdent)

            objFieldIdent = New Ext.Net.ModelField
            objFieldIdent.Name = "Number"
            objFieldIdent.Type = ModelFieldType.String
            objModelIdent.Fields.Add(objFieldIdent)

            objFieldIdent = New Ext.Net.ModelField
            objFieldIdent.Name = "Issued_By"
            objFieldIdent.Type = ModelFieldType.String
            objModelIdent.Fields.Add(objFieldIdent)

            objFieldIdent = New Ext.Net.ModelField
            objFieldIdent.Name = "Issued_Country"
            objFieldIdent.Type = ModelFieldType.String
            objModelIdent.Fields.Add(objFieldIdent)

            objStoreIdent.Model.Add(objModelIdent)

            Dim objListcolumnIdentification As New List(Of ColumnBase)

            Using objcolumnNo As New Ext.Net.RowNumbererColumn
                objcolumnNo.Text = "No."
                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                objListcolumnIdentification.Add(objcolumnNo)
            End Using

            Dim objColumnIdent As Ext.Net.Column

            objColumnIdent = New Ext.Net.Column
            objColumnIdent.Text = "Jenis Identitas"
            objColumnIdent.DataIndex = "Type"
            objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnIdent.Flex = 1
            objListcolumnIdentification.Add(objColumnIdent)

            objColumnIdent = New Ext.Net.Column
            objColumnIdent.Text = "Nomor Identitas"
            objColumnIdent.DataIndex = "Number"
            objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnIdent.Flex = 1
            objListcolumnIdentification.Add(objColumnIdent)

            objColumnIdent = New Ext.Net.Column
            objColumnIdent.Text = "Negara Penerbit Identitas"
            objColumnIdent.DataIndex = "Issued_Country"
            objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            objColumnIdent.Flex = 1
            objListcolumnIdentification.Add(objColumnIdent)

            Dim objparamIdent(0) As System.Data.SqlClient.SqlParameter
            objparamIdent(0) = New SqlClient.SqlParameter
            objparamIdent(0).ParameterName = "@PK_ODM_Reporting_Person_ID"
            objparamIdent(0).SqlDbType = SqlDbType.BigInt
            objparamIdent(0).Value = objReportingPerson.PK_ODM_Reporting_Person_ID

            Using objdata As Data.DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GetIdentificationDetailByReportingPerson", objparamIdent)
                Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Dokumen Identitas", objStoreIdent, objListcolumnIdentification, objdata)
            End Using

            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Sudah Meninggal?", "Deceased" & strunik, objReportingPerson.Deceased)
            If objReportingPerson.Deceased = True Then
                Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Meninggal", "Deceased_Date" & strunik, objReportingPerson.Deceased_Date)
            End If
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NPWP", "Tax_Number" & strunik, objReportingPerson.Tax_Number)
            If objReportingPerson.Tax_Reg_Number = True Then
                Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "PEPs?", "Tax_Reg_Number" & strunik, "Y")
            Else
                Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "PEPs?", "Tax_Reg_Number" & strunik, "T")
            End If
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Sumber Dana", "Source_Of_Wealth" & strunik, objReportingPerson.Source_Of_Wealth)
            Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan", "Comment" & strunik, objReportingPerson.Comment)

        End If
    End Sub

    Sub Accept(ID As String)
        Using objDB As New GoAMLDAL.GoAMLEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objApproval As GoAMLDAL.ModuleApproval = objDB.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As GoAMLDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objDB.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            Dim objModuledata As ReportingPersonData = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(ReportingPersonData))
                            objModuledata.objPerson.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objModuledata.objPerson.ApprovedDate = Now
                            objDB.Entry(objModuledata.objPerson).State = Entity.EntityState.Added

                            Dim objATHeader As New GoAMLDAL.AuditTrailHeader
                            objATHeader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objATHeader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objATHeader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objATHeader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            objATHeader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                            objATHeader.ModuleLabel = objModule.ModuleLabel
                            objDB.Entry(objATHeader).State = Entity.EntityState.Added
                            objDB.SaveChanges()

                            For Each item As GoAMLDAL.goAML_Person_Identification In objModuledata.ObjIdent
                                item.FK_Person_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID
                                objDB.Entry(item).State = Entity.EntityState.Added
                            Next

                            For Each item As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjPhone
                                item.FK_for_Table_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID
                                objDB.Entry(item).State = Entity.EntityState.Added
                            Next

                            For Each item As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjAddress
                                item.FK_To_Table_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID
                                objDB.Entry(item).State = Entity.EntityState.Added
                            Next

                            For Each item As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjEmpPhone
                                item.FK_for_Table_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID
                                objDB.Entry(item).State = Entity.EntityState.Added
                            Next

                            For Each item As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjEmpAddress
                                item.FK_To_Table_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID
                                objDB.Entry(item).State = Entity.EntityState.Added
                            Next

                            Dim objtype As Type = objModuledata.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                objATDetail.FieldName = item.Name
                                objATDetail.OldValue = ""
                                If Not item.GetValue(objModuledata, Nothing) Is Nothing Then
                                    objATDetail.NewValue = item.GetValue(objModuledata, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                                objDB.Entry(objATDetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Person_Identification In objModuledata.ObjIdent
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjEmpPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjEmpAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objModuledata As ReportingPersonData = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(ReportingPersonData))
                            Dim objModuledataOld As ReportingPersonData = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(ReportingPersonData))

                            objModuledata.objPerson.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objModuledata.objPerson.ApprovedDate = Now
                            objDB.goAML_ODM_Reporting_Person.Attach(objModuledata.objPerson)
                            objDB.Entry(objModuledata.objPerson).State = Entity.EntityState.Modified

                            For Each itemx As GoAMLDAL.goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID And x.isReportingPerson = True Select x).ToList
                                Dim objcek As GoAMLDAL.goAML_Person_Identification = objModuledata.ObjIdent.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID)
                                If objcek Is Nothing Then
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 2 Select x).ToList
                                Dim objcek As GoAMLDAL.goAML_Ref_Phone = objModuledata.ObjPhone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                If objcek Is Nothing Then
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 11 Select x).ToList
                                Dim objcek As GoAMLDAL.goAML_Ref_Phone = objModuledata.ObjEmpPhone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                If objcek Is Nothing Then
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 2 Select x).ToList
                                Dim objcek As GoAMLDAL.goAML_Ref_Address = objModuledata.ObjAddress.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                If objcek Is Nothing Then
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 11 Select x).ToList
                                Dim objcek As GoAMLDAL.goAML_Ref_Address = objModuledata.ObjEmpAddress.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                If objcek Is Nothing Then
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each item As GoAMLDAL.goAML_Person_Identification In objModuledata.ObjIdent
                                Dim obcek As GoAMLDAL.goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = item.PK_Person_Identification_ID And x.isReportingPerson = True Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    objDB.Entry(item).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = Entity.EntityState.Modified
                                End If
                            Next

                            For Each item As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjPhone
                                Dim obcek As GoAMLDAL.goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 2 Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    objDB.Entry(item).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = Entity.EntityState.Modified
                                End If
                            Next

                            For Each item As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjEmpPhone
                                Dim obcek As GoAMLDAL.goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 11 Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    objDB.Entry(item).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = Entity.EntityState.Modified
                                End If
                            Next

                            For Each item As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjAddress
                                Dim obcek As GoAMLDAL.goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 2 Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    objDB.Entry(item).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = Entity.EntityState.Modified
                                End If
                            Next

                            For Each item As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjEmpAddress
                                Dim obcek As GoAMLDAL.goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 11 Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    objDB.Entry(item).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = Entity.EntityState.Modified
                                End If
                            Next

                            Dim objATHeader As New GoAMLDAL.AuditTrailHeader
                            objATHeader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objATHeader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objATHeader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objATHeader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            objATHeader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                            objATHeader.ModuleLabel = objModule.ModuleLabel
                            objDB.Entry(objATHeader).State = Entity.EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                objATDetail.FieldName = item.Name
                                objATDetail.OldValue = ""
                                If Not item.GetValue(objModuledataOld, Nothing) Is Nothing Then
                                    objATDetail.OldValue = item.GetValue(objModuledataOld, Nothing)
                                Else
                                    objATDetail.OldValue = ""
                                End If
                                If Not item.GetValue(objModuledata, Nothing) Is Nothing Then
                                    objATDetail.NewValue = item.GetValue(objModuledata, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                                objDB.Entry(objATDetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Person_Identification In objModuledata.ObjIdent
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjEmpPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjEmpAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objModuledata As ReportingPersonData = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(ReportingPersonData))
                            objDB.Entry(objModuledata.objPerson).State = Entity.EntityState.Deleted

                            Dim listIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID And x.isReportingPerson = True).ToList
                            For Each item As GoAMLDAL.goAML_Person_Identification In listIdentification
                                objDB.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            Dim listPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 2).ToList
                            For Each item As GoAMLDAL.goAML_Ref_Phone In listPhone
                                objDB.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            Dim listEmpPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 11).ToList
                            For Each item As GoAMLDAL.goAML_Ref_Phone In listEmpPhone
                                objDB.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            Dim listAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 2).ToList
                            For Each item As GoAMLDAL.goAML_Ref_Address In listAddress
                                objDB.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            Dim listEmpAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objModuledata.objPerson.PK_ODM_Reporting_Person_ID And x.FK_Ref_Detail_Of = 11).ToList
                            For Each item As GoAMLDAL.goAML_Ref_Address In listEmpAddress
                                objDB.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            Dim objATHeader As New GoAMLDAL.AuditTrailHeader
                            objATHeader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objATHeader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objATHeader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objATHeader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            objATHeader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                            objATHeader.ModuleLabel = objModule.ModuleLabel
                            objDB.Entry(objATHeader).State = Entity.EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                objATDetail.FieldName = item.Name
                                objATDetail.OldValue = ""
                                If Not item.GetValue(objModuledata, Nothing) Is Nothing Then
                                    objATDetail.NewValue = item.GetValue(objModuledata, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                                objDB.Entry(objATDetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Person_Identification In objModuledata.ObjIdent
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjEmpPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjEmpAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next
                    End Select
                    objDB.Entry(objApproval).State = Entity.EntityState.Deleted
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Sub Reject(ID As String)
        Using objDB As New GoAMLDAL.GoAMLEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objApproval As GoAMLDAL.ModuleApproval = objDB.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As GoAMLDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objDB.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            Dim objModuledata As ReportingPersonData = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(ReportingPersonData))

                            Dim objATHeader As New GoAMLDAL.AuditTrailHeader
                            objATHeader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objATHeader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objATHeader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objATHeader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objATHeader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                            objATHeader.ModuleLabel = objModule.ModuleLabel
                            objDB.Entry(objATHeader).State = Entity.EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.objPerson.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                objATDetail.FieldName = item.Name
                                objATDetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objPerson, Nothing) Is Nothing Then
                                    objATDetail.NewValue = item.GetValue(objModuledata.objPerson, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                                objDB.Entry(objATDetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Person_Identification In objModuledata.ObjIdent
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjEmpPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjEmpAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objModuledata As ReportingPersonData = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(ReportingPersonData))
                            Dim objModuledataOld As ReportingPersonData = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(ReportingPersonData))

                            Dim objATHeader As New GoAMLDAL.AuditTrailHeader
                            objATHeader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objATHeader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objATHeader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objATHeader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objATHeader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                            objATHeader.ModuleLabel = objModule.ModuleLabel
                            objDB.Entry(objATHeader).State = Entity.EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.objPerson.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                objATDetail.FieldName = item.Name
                                If Not item.GetValue(objModuledataOld.objPerson, Nothing) Is Nothing Then
                                    objATDetail.OldValue = item.GetValue(objModuledataOld.objPerson, Nothing)
                                Else
                                    objATDetail.OldValue = ""
                                End If
                                If Not item.GetValue(objModuledata.objPerson, Nothing) Is Nothing Then
                                    objATDetail.NewValue = item.GetValue(objModuledata.objPerson, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                                objDB.Entry(objATDetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Person_Identification In objModuledata.ObjIdent
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjEmpPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjEmpAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objModuledata As ReportingPersonData = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(ReportingPersonData))

                            Dim objATHeader As New GoAMLDAL.AuditTrailHeader
                            objATHeader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objATHeader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objATHeader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objATHeader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objATHeader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                            objATHeader.ModuleLabel = objModule.ModuleLabel
                            objDB.Entry(objATHeader).State = Entity.EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.objPerson.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                objATDetail.FieldName = item.Name
                                objATDetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objPerson, Nothing) Is Nothing Then
                                    objATDetail.NewValue = item.GetValue(objModuledata.objPerson, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                                objDB.Entry(objATDetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Person_Identification In objModuledata.ObjIdent
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.ObjEmpPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.ObjEmpAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                    objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                    objATDetail.FieldName = item.Name
                                    objATDetail.OldValue = ""

                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objATDetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objATDetail.NewValue = ""
                                        End If
                                    Else
                                        objATDetail.NewValue = ""
                                    End If

                                    objDB.Entry(objATDetail).State = Entity.EntityState.Added
                                Next
                            Next
                    End Select

                    objDB.Entry(objApproval).State = Entity.EntityState.Deleted
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub
#End Region

    Sub LoadPanelDiff(objPanel As FormPanel, objDataOld As String, objDataNew As String, unikID As String, unikNewID As String, DataUpdate As String)
        Dim objPerson As GoAMLBLL.ReportingPersonData = NawaBLL.Common.Deserialize(objDataOld, GetType(GoAMLBLL.ReportingPersonData))
        Dim objPersonNew As GoAMLBLL.ReportingPersonData = NawaBLL.Common.Deserialize(objDataNew, GetType(GoAMLBLL.ReportingPersonData))
        If Not objPerson Is Nothing And Not objPersonNew Is Nothing Then
            If DataUpdate = "old" Then
#Region "Old Data"
                Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ID", "PK_ODM_Reporting_Person_ID" & unikID, objPerson.objPerson.PK_ODM_Reporting_Person_ID)
                If Not objPerson.objPerson.Kode_Laporan = objPersonNew.objPerson.Kode_Laporan Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kode Laporan", "Kode_Laporan" & unikID, GetKodeLaporan(objPerson.objPerson.Kode_Laporan))
                End If

                If Not objPerson.objPerson.Title = objPersonNew.objPerson.Title Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gelar", "Title" & unikID, objPerson.objPerson.Title)
                End If

                If Not objPerson.objPerson.Last_Name = objPersonNew.objPerson.Last_Name Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Lengkap", "Last_Name" & unikID, objPerson.objPerson.Last_Name)
                End If

                If Not objPerson.objPerson.BirthDate = objPersonNew.objPerson.BirthDate Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Lahir", "BirthDate" & unikID, objPerson.objPerson.BirthDate)
                End If

                If Not objPerson.objPerson.Birth_Place = objPersonNew.objPerson.Birth_Place Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Lahir", "Birth_Place" & unikID, objPerson.objPerson.Birth_Place)
                End If

                If Not objPerson.objPerson.Gender = objPersonNew.objPerson.Gender Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Jenis Kelamin", "Gender" & unikID, objPerson.objPerson.Gender)
                End If

                If Not objPerson.objPerson.Mothers_Name = objPersonNew.objPerson.Mothers_Name Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Ibu Kandung", "Mothers_Name" & unikID, objPerson.objPerson.Mothers_Name)
                End If

                If Not objPerson.objPerson.Alias = objPersonNew.objPerson.Alias Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Alias", "Alias" & unikID, objPerson.objPerson.Alias)
                End If

                If Not objPerson.objPerson.SSN = objPersonNew.objPerson.SSN Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NIK", "SSN" & unikID, objPerson.objPerson.SSN)
                End If

                If Not objPerson.objPerson.Passport_Number = objPersonNew.objPerson.Passport_Number Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Paspor", "Passport_Number" & unikID, objPerson.objPerson.Passport_Number)
                End If

                If Not objPerson.objPerson.Passport_Country = objPersonNew.objPerson.Passport_Country Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Penerbit Paspor", "Passport_Country" & unikID, objPerson.objPerson.Passport_Country)
                End If

                If Not objPerson.objPerson.Id_Number = objPersonNew.objPerson.Id_Number Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Identitas Lainnya", "Id_Number" & unikID, objPerson.objPerson.Id_Number)
                End If

                If Not objPerson.objPerson.Nationality1 = objPersonNew.objPerson.Nationality1 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 1", "Nationality1" & unikID, GetCountryName(objPerson.objPerson.Nationality1))
                End If

                If Not objPerson.objPerson.Nationality2 = objPersonNew.objPerson.Nationality2 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 2", "Nationality2" & unikID, GetCountryName(objPerson.objPerson.Nationality2))
                End If

                If Not objPerson.objPerson.Nationality3 = objPersonNew.objPerson.Nationality3 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 3", "Nationality3" & unikID, GetCountryName(objPerson.objPerson.Nationality3))
                End If

                If Not objPerson.objPerson.Residence = objPersonNew.objPerson.Residence Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Domisili", "Residence" & unikID, GetCountryName(objPerson.objPerson.Residence))
                End If

                If Not objPerson.objPerson.Email1 = objPersonNew.objPerson.Email1 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 1", "Email1" & unikID, objPerson.objPerson.Email1)
                End If

                If Not objPerson.objPerson.Email2 = objPersonNew.objPerson.Email2 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 2", "Email2" & unikID, objPerson.objPerson.Email2)
                End If

                If Not objPerson.objPerson.Email3 = objPersonNew.objPerson.Email3 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 3", "Email3" & unikID, objPerson.objPerson.Email3)
                End If

                If Not objPerson.objPerson.Email4 = objPersonNew.objPerson.Email4 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 4", "Email4" & unikID, objPerson.objPerson.Email4)
                End If

                If Not objPerson.objPerson.Email5 = objPersonNew.objPerson.Email5 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 5", "Email5" & unikID, objPerson.objPerson.Email5)
                End If

                If Not objPerson.objPerson.Occupation = objPersonNew.objPerson.Occupation Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Pekerjaan", "Occupation" & unikID, objPerson.objPerson.Occupation)
                End If

                If Not objPerson.objPerson.Employer_Name = objPersonNew.objPerson.Employer_Name Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Bekerja", "Employer_Name" & unikID, objPerson.objPerson.Employer_Name)
                End If

                If Not objPerson.objPerson.Deceased = objPersonNew.objPerson.Deceased Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Sudah Meninggal?", "Deceased" & unikID, objPerson.objPerson.Deceased)
                End If

                If objPerson.objPerson.Deceased = True Then
                    If Not objPerson.objPerson.Deceased_Date = objPersonNew.objPerson.Deceased_Date Then
                        Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Meninggal", "Deceased_Date" & unikID, objPerson.objPerson.Deceased_Date)
                    End If
                End If

                If Not objPerson.objPerson.Tax_Number = objPersonNew.objPerson.Tax_Number Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NPWP", "Tax_Number" & unikID, objPerson.objPerson.Tax_Number)
                End If

                If Not objPerson.objPerson.Tax_Reg_Number = objPersonNew.objPerson.Tax_Reg_Number Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "PEPs?", "Tax_Reg_Number" & unikID, objPerson.objPerson.Tax_Reg_Number)
                End If

                If Not objPerson.objPerson.Source_Of_Wealth = objPersonNew.objPerson.Source_Of_Wealth Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Sumber Dana", "Source_Of_Wealth" & unikID, objPerson.objPerson.Source_Of_Wealth)
                End If

                If Not objPerson.objPerson.Comment = objPersonNew.objPerson.Comment Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan", "Comment" & unikID, objPerson.objPerson.Comment)
                End If
#End Region
            ElseIf DataUpdate = "new" Then
#Region "New Data"
                Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ID", "PK_ODM_Reporting_Person_ID" & unikNewID, objPersonNew.objPerson.PK_ODM_Reporting_Person_ID)
                If Not objPerson.objPerson.Kode_Laporan = objPersonNew.objPerson.Kode_Laporan Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kode Laporan", "Kode_Laporan" & unikNewID, GetKodeLaporan(objPersonNew.objPerson.Kode_Laporan))
                End If

                If Not objPerson.objPerson.Title = objPersonNew.objPerson.Title Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gelar", "Title" & unikNewID, objPersonNew.objPerson.Title)
                End If

                If Not objPerson.objPerson.Last_Name = objPersonNew.objPerson.Last_Name Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Lengkap", "Last_Name" & unikNewID, objPersonNew.objPerson.Last_Name)
                End If

                If Not objPerson.objPerson.BirthDate = objPersonNew.objPerson.BirthDate Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Lahir", "BirthDate" & unikNewID, objPersonNew.objPerson.BirthDate)
                End If

                If Not objPerson.objPerson.Birth_Place = objPersonNew.objPerson.Birth_Place Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Lahir", "Birth_Place" & unikNewID, objPersonNew.objPerson.Birth_Place)
                End If

                If Not objPerson.objPerson.Gender = objPersonNew.objPerson.Gender Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Jenis Kelamin", "Gender" & unikNewID, objPersonNew.objPerson.Gender)
                End If

                If Not objPerson.objPerson.Mothers_Name = objPersonNew.objPerson.Mothers_Name Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Ibu Kandung", "Mothers_Name" & unikNewID, objPersonNew.objPerson.Mothers_Name)
                End If

                If Not objPerson.objPerson.Alias = objPersonNew.objPerson.Alias Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Alias", "Alias" & unikNewID, objPersonNew.objPerson.Alias)
                End If

                If Not objPerson.objPerson.SSN = objPersonNew.objPerson.SSN Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NIK", "SSN" & unikNewID, objPersonNew.objPerson.SSN)
                End If

                If Not objPerson.objPerson.Passport_Number = objPersonNew.objPerson.Passport_Number Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Paspor", "Passport_Number" & unikNewID, objPersonNew.objPerson.Passport_Number)
                End If

                If Not objPerson.objPerson.Passport_Country = objPersonNew.objPerson.Passport_Country Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Penerbit Paspor", "Passport_Country" & unikNewID, objPersonNew.objPerson.Passport_Country)
                End If

                If Not objPerson.objPerson.Id_Number = objPersonNew.objPerson.Id_Number Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Identitas Lainnya", "Id_Number" & unikNewID, objPersonNew.objPerson.Id_Number)
                End If

                If Not objPerson.objPerson.Nationality1 = objPersonNew.objPerson.Nationality1 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 1", "Nationality1" & unikNewID, GetCountryName(objPersonNew.objPerson.Nationality1))
                End If

                If Not objPerson.objPerson.Nationality2 = objPersonNew.objPerson.Nationality2 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 2", "Nationality2" & unikNewID, GetCountryName(objPersonNew.objPerson.Nationality2))
                End If

                If Not objPerson.objPerson.Nationality3 = objPersonNew.objPerson.Nationality3 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 3", "Nationality3" & unikNewID, GetCountryName(objPersonNew.objPerson.Nationality3))
                End If

                If Not objPerson.objPerson.Residence = objPersonNew.objPerson.Residence Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Domisili", "Residence" & unikNewID, GetCountryName(objPersonNew.objPerson.Residence))
                End If

                If Not objPerson.objPerson.Email1 = objPersonNew.objPerson.Email1 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 1", "Email1" & unikNewID, objPersonNew.objPerson.Email1)
                End If

                If Not objPerson.objPerson.Email2 = objPersonNew.objPerson.Email2 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 2", "Email2" & unikNewID, objPersonNew.objPerson.Email2)
                End If

                If Not objPerson.objPerson.Email3 = objPersonNew.objPerson.Email3 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 3", "Email3" & unikNewID, objPersonNew.objPerson.Email3)
                End If

                If Not objPerson.objPerson.Email4 = objPersonNew.objPerson.Email4 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 4", "Email4" & unikNewID, objPersonNew.objPerson.Email4)
                End If

                If Not objPerson.objPerson.Email5 = objPersonNew.objPerson.Email5 Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 5", "Email5" & unikNewID, objPersonNew.objPerson.Email5)
                End If

                If Not objPerson.objPerson.Occupation = objPersonNew.objPerson.Occupation Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Pekerjaan", "Occupation" & unikNewID, objPersonNew.objPerson.Occupation)
                End If

                If Not objPerson.objPerson.Employer_Name = objPersonNew.objPerson.Employer_Name Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Bekerja", "Employer_Name" & unikNewID, objPersonNew.objPerson.Employer_Name)
                End If

                If Not objPerson.objPerson.Deceased = objPersonNew.objPerson.Deceased Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Sudah Meninggal?", "Deceased" & unikNewID, objPersonNew.objPerson.Deceased)
                End If

                If objPerson.objPerson.Deceased = True Then
                    If Not objPerson.objPerson.Deceased_Date = objPersonNew.objPerson.Deceased_Date Then
                        Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Meninggal", "Deceased_Date" & unikNewID, objPersonNew.objPerson.Deceased_Date)
                    End If
                End If

                If Not objPerson.objPerson.Tax_Number = objPersonNew.objPerson.Tax_Number Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NPWP", "Tax_Number" & unikNewID, objPersonNew.objPerson.Tax_Number)
                End If

                If Not objPerson.objPerson.Tax_Reg_Number = objPersonNew.objPerson.Tax_Reg_Number Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "PEPs?", "Tax_Reg_Number" & unikNewID, objPersonNew.objPerson.Tax_Reg_Number)
                End If

                If Not objPerson.objPerson.Source_Of_Wealth = objPersonNew.objPerson.Source_Of_Wealth Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Sumber Dana", "Source_Of_Wealth" & unikNewID, objPersonNew.objPerson.Source_Of_Wealth)
                End If

                If Not objPerson.objPerson.Comment = objPersonNew.objPerson.Comment Then
                    Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan", "Comment" & unikNewID, objPersonNew.objPerson.Comment)
                End If
#End Region
            End If

            'Identification
            'Dim objStoreIdent As New Ext.Net.Store
            'objStoreIdent.ID = unikID & "StoreGridIdentification"
            'objStoreIdent.ClientIDMode = Web.UI.ClientIDMode.Static

            'Dim objModelIdent As New Ext.Net.Model
            'Dim objFieldIdent As Ext.Net.ModelField

            'objFieldIdent = New Ext.Net.ModelField
            'objFieldIdent.Name = "PK_Person_Identification_ID"
            'objFieldIdent.Type = ModelFieldType.Auto
            'objModelIdent.Fields.Add(objFieldIdent)

            'objFieldIdent = New Ext.Net.ModelField
            'objFieldIdent.Name = "Type"
            'objFieldIdent.Type = ModelFieldType.String
            'objModelIdent.Fields.Add(objFieldIdent)

            'objFieldIdent = New Ext.Net.ModelField
            'objFieldIdent.Name = "Number"
            'objFieldIdent.Type = ModelFieldType.String
            'objModelIdent.Fields.Add(objFieldIdent)

            'objFieldIdent = New Ext.Net.ModelField
            'objFieldIdent.Name = "Issue_Date"
            'objFieldIdent.Type = ModelFieldType.Date
            'objModelIdent.Fields.Add(objFieldIdent)

            'objFieldIdent = New Ext.Net.ModelField
            'objFieldIdent.Name = "Expiry_Date"
            'objFieldIdent.Type = ModelFieldType.Date
            'objModelIdent.Fields.Add(objFieldIdent)

            'objFieldIdent = New Ext.Net.ModelField
            'objFieldIdent.Name = "Issued_By"
            'objFieldIdent.Type = ModelFieldType.String
            'objModelIdent.Fields.Add(objFieldIdent)

            'objFieldIdent = New Ext.Net.ModelField
            'objFieldIdent.Name = "Issued_Country"
            'objFieldIdent.Type = ModelFieldType.String
            'objModelIdent.Fields.Add(objFieldIdent)

            'objFieldIdent = New Ext.Net.ModelField
            'objFieldIdent.Name = "Identification_Comment"
            'objFieldIdent.Type = ModelFieldType.String
            'objModelIdent.Fields.Add(objFieldIdent)

            'objStoreIdent.Model.Add(objModelIdent)

            'Dim objListcolumnIdentification As New List(Of ColumnBase)

            'Using objcolumnNo As New Ext.Net.RowNumbererColumn
            '    objcolumnNo.Text = "No."
            '    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
            '    objListcolumnIdentification.Add(objcolumnNo)
            'End Using

            'Dim objColumnIdent As Ext.Net.Column

            'objColumnIdent = New Ext.Net.Column
            'objColumnIdent.Text = "Jenis Identitas"
            'objColumnIdent.DataIndex = "Type"
            'objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnIdent.Flex = 1
            'objListcolumnIdentification.Add(objColumnIdent)

            'objColumnIdent = New Ext.Net.Column
            'objColumnIdent.Text = "Nomor Identitas"
            'objColumnIdent.DataIndex = "Number"
            'objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnIdent.Flex = 1
            'objListcolumnIdentification.Add(objColumnIdent)

            'objColumnIdent = New Ext.Net.Column
            'objColumnIdent.Text = "Tanggal Diterbitkan"
            'objColumnIdent.DataIndex = "Issue_Date"
            'objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnIdent.Flex = 1
            'objListcolumnIdentification.Add(objColumnIdent)

            'objColumnIdent = New Ext.Net.Column
            'objColumnIdent.Text = "Tanggal Kadaluarsa"
            'objColumnIdent.DataIndex = "Expiry_Date"
            'objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnIdent.Flex = 1
            'objListcolumnIdentification.Add(objColumnIdent)

            'objColumnIdent = New Ext.Net.Column
            'objColumnIdent.Text = "Diterbitkan Oleh"
            'objColumnIdent.DataIndex = "Issued_By"
            'objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnIdent.Flex = 1
            'objListcolumnIdentification.Add(objColumnIdent)

            'objColumnIdent = New Ext.Net.Column
            'objColumnIdent.Text = "Negara Penerbit Identitas"
            'objColumnIdent.DataIndex = "Issued_Country"
            'objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnIdent.Flex = 1
            'objListcolumnIdentification.Add(objColumnIdent)

            'objColumnIdent = New Ext.Net.Column
            'objColumnIdent.Text = "Catatan"
            'objColumnIdent.DataIndex = "Identification_Comment"
            'objColumnIdent.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnIdent.Flex = 1
            'objListcolumnIdentification.Add(objColumnIdent)

            'Dim objparamIdent(0) As System.Data.SqlClient.SqlParameter
            'objparamIdent(0) = New SqlClient.SqlParameter
            'objparamIdent(0).ParameterName = "@ReportingPersonIdentificationID"
            'objparamIdent(0).SqlDbType = SqlDbType.BigInt
            'objparamIdent(0).Value = objPerson.objPerson.PK_ODM_Reporting_Person_ID

            'Dim objdtIdent As DataTable = NawaBLL.Common.CopyGenericToDataTable(objPerson.ObjIdent)

            'For Each item As DataRow In objdtIdent.Rows
            '    item("Type") = GetJenisIdentitas(item("Type"))
            'Next

            'For Each item As DataRow In objdtIdent.Rows
            '    item("Issued_Country") = GetCountryName(item("Issued_Country"))
            'Next

            'Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Identification", objStoreIdent, objListcolumnIdentification, objdtIdent)


            ''Phone
            'Dim objStorePhone As New Ext.Net.Store
            'objStorePhone.ID = unikID & "StoreGridPhone"
            'objStorePhone.ClientIDMode = Web.UI.ClientIDMode.Static

            'Dim objModelPhone As New Ext.Net.Model
            'Dim objFieldPhone As Ext.Net.ModelField

            'objFieldPhone = New Ext.Net.ModelField
            'objFieldPhone.Name = "PK_goAML_Ref_Phone"
            'objFieldPhone.Type = ModelFieldType.Auto
            'objModelPhone.Fields.Add(objFieldPhone)

            'objFieldPhone = New Ext.Net.ModelField
            'objFieldPhone.Name = "Tph_Contact_Type"
            'objFieldPhone.Type = ModelFieldType.String
            'objModelPhone.Fields.Add(objFieldPhone)

            'objFieldPhone = New Ext.Net.ModelField
            'objFieldPhone.Name = "Tph_Communication_Type"
            'objFieldPhone.Type = ModelFieldType.String
            'objModelPhone.Fields.Add(objFieldPhone)

            'objFieldPhone = New Ext.Net.ModelField
            'objFieldPhone.Name = "tph_country_prefix"
            'objFieldPhone.Type = ModelFieldType.String
            'objModelPhone.Fields.Add(objFieldPhone)

            'objFieldPhone = New Ext.Net.ModelField
            'objFieldPhone.Name = "tph_number"
            'objFieldPhone.Type = ModelFieldType.String
            'objModelPhone.Fields.Add(objFieldPhone)

            'objFieldPhone = New Ext.Net.ModelField
            'objFieldPhone.Name = "tph_extension"
            'objFieldPhone.Type = ModelFieldType.String
            'objModelPhone.Fields.Add(objFieldPhone)

            'objFieldPhone = New Ext.Net.ModelField
            'objFieldPhone.Name = "comments"
            'objFieldPhone.Type = ModelFieldType.String
            'objModelPhone.Fields.Add(objFieldPhone)

            'objStorePhone.Model.Add(objModelPhone)

            'Dim objListcolumnPhone As New List(Of ColumnBase)

            'Using objcolumnNo As New Ext.Net.RowNumbererColumn
            '    objcolumnNo.Text = "No."
            '    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
            '    objListcolumnPhone.Add(objcolumnNo)
            'End Using

            'Dim objColumnPhone As Ext.Net.Column

            'objColumnPhone = New Ext.Net.Column
            'objColumnPhone.Text = "Kategori Kontak"
            'objColumnPhone.DataIndex = "Tph_Contact_Type"
            'objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnPhone.Flex = 1
            'objListcolumnPhone.Add(objColumnPhone)

            'objColumnPhone = New Ext.Net.Column
            'objColumnPhone.Text = "Jenis Alat Komunikasi"
            'objColumnPhone.DataIndex = "Tph_Communication_Type"
            'objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnPhone.Flex = 1
            'objListcolumnPhone.Add(objColumnPhone)

            'objColumnPhone = New Ext.Net.Column
            'objColumnPhone.Text = "Kode Area Telepon"
            'objColumnPhone.DataIndex = "tph_country_prefix"
            'objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnPhone.Flex = 1
            'objListcolumnPhone.Add(objColumnPhone)

            'objColumnPhone = New Ext.Net.Column
            'objColumnPhone.Text = "Nomor Telepon"
            'objColumnPhone.DataIndex = "tph_number"
            'objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnPhone.Flex = 1
            'objListcolumnPhone.Add(objColumnPhone)

            'objColumnPhone = New Ext.Net.Column
            'objColumnPhone.Text = "Nomor Ektensi"
            'objColumnPhone.DataIndex = "tph_extension"
            'objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnPhone.Flex = 1
            'objListcolumnPhone.Add(objColumnPhone)

            'objColumnPhone = New Ext.Net.Column
            'objColumnPhone.Text = "Catatan"
            'objColumnPhone.DataIndex = "comments"
            'objColumnPhone.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnPhone.Flex = 1
            'objListcolumnPhone.Add(objColumnPhone)


            'Dim objdtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(objPerson.ObjPhone)

            'For Each item As DataRow In objdtPhone.Rows
            '    item("Tph_Contact_Type") = GetKategoriKontak(item("Tph_Contact_Type"))
            'Next

            'For Each item As DataRow In objdtPhone.Rows
            '    item("Tph_Communication_Type") = GetJenisAlatKomunikasi(item("Tph_Communication_Type"))
            'Next

            'Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Phone", objStorePhone, objListcolumnPhone, objdtPhone)



            ''Address
            'Dim objStoreAddress As New Ext.Net.Store
            'objStoreAddress.ID = unikID & "StoreGridAddress"
            'objStoreAddress.ClientIDMode = Web.UI.ClientIDMode.Static

            'Dim objModelAddress As New Ext.Net.Model
            'Dim objFieldAddress As Ext.Net.ModelField

            'objFieldAddress = New Ext.Net.ModelField
            'objFieldAddress.Name = "PK_Customer_Address_ID"
            'objFieldAddress.Type = ModelFieldType.Auto
            'objModelAddress.Fields.Add(objFieldAddress)

            'objFieldAddress = New Ext.Net.ModelField
            'objFieldAddress.Name = "Address_Type"
            'objFieldAddress.Type = ModelFieldType.String
            'objModelAddress.Fields.Add(objFieldAddress)

            'objFieldAddress = New Ext.Net.ModelField
            'objFieldAddress.Name = "Address"
            'objFieldAddress.Type = ModelFieldType.String
            'objModelAddress.Fields.Add(objFieldAddress)

            'objFieldAddress = New Ext.Net.ModelField
            'objFieldAddress.Name = "Town"
            'objFieldAddress.Type = ModelFieldType.String
            'objModelAddress.Fields.Add(objFieldAddress)

            'objFieldAddress = New Ext.Net.ModelField
            'objFieldAddress.Name = "City"
            'objFieldAddress.Type = ModelFieldType.String
            'objModelAddress.Fields.Add(objFieldAddress)

            'objFieldAddress = New Ext.Net.ModelField
            'objFieldAddress.Name = "Zip"
            'objFieldAddress.Type = ModelFieldType.String
            'objModelAddress.Fields.Add(objFieldAddress)

            'objFieldAddress = New Ext.Net.ModelField
            'objFieldAddress.Name = "Country_Code"
            'objFieldAddress.Type = ModelFieldType.String
            'objModelAddress.Fields.Add(objFieldAddress)

            'objFieldAddress = New Ext.Net.ModelField
            'objFieldAddress.Name = "State"
            'objFieldAddress.Type = ModelFieldType.String
            'objModelAddress.Fields.Add(objFieldAddress)

            'objFieldAddress = New Ext.Net.ModelField
            'objFieldAddress.Name = "Comments"
            'objFieldAddress.Type = ModelFieldType.String
            'objModelAddress.Fields.Add(objFieldAddress)

            'objStoreAddress.Model.Add(objModelAddress)

            'Dim objListcolumnAddress As New List(Of ColumnBase)

            'Using objcolumnNo As New Ext.Net.RowNumbererColumn
            '    objcolumnNo.Text = "No."
            '    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
            '    objListcolumnAddress.Add(objcolumnNo)
            'End Using

            'Dim objColumnAddress As Ext.Net.Column

            'objColumnAddress = New Ext.Net.Column
            'objColumnAddress.Text = "Tipe Alamat"
            'objColumnAddress.DataIndex = "Address_Type"
            'objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnAddress.Flex = 1
            'objListcolumnAddress.Add(objColumnAddress)

            'objColumnAddress = New Ext.Net.Column
            'objColumnAddress.Text = "Alamat"
            'objColumnAddress.DataIndex = "Address"
            'objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnAddress.Flex = 1
            'objListcolumnAddress.Add(objColumnAddress)

            'objColumnAddress = New Ext.Net.Column
            'objColumnAddress.Text = "Kecamatan"
            'objColumnAddress.DataIndex = "Town"
            'objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnAddress.Flex = 1
            'objListcolumnAddress.Add(objColumnAddress)

            'objColumnAddress = New Ext.Net.Column
            'objColumnAddress.Text = "Kota/Kabupaten"
            'objColumnAddress.DataIndex = "City"
            'objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnAddress.Flex = 1
            'objListcolumnAddress.Add(objColumnAddress)

            'objColumnAddress = New Ext.Net.Column
            'objColumnAddress.Text = "Kode Pos"
            'objColumnAddress.DataIndex = "Zip"
            'objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnAddress.Flex = 1
            'objListcolumnAddress.Add(objColumnAddress)

            'objColumnAddress = New Ext.Net.Column
            'objColumnAddress.Text = "Negara"

            'objColumnAddress.DataIndex = "Country_Code" 'GetCountryName("Country_Code")
            'objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnAddress.Flex = 1
            'objListcolumnAddress.Add(objColumnAddress)

            'objColumnAddress = New Ext.Net.Column
            'objColumnAddress.Text = "Provinsi"
            'objColumnAddress.DataIndex = "State"
            'objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnAddress.Flex = 1
            'objListcolumnAddress.Add(objColumnAddress)

            'objColumnAddress = New Ext.Net.Column
            'objColumnAddress.Text = "Catatan"
            'objColumnAddress.DataIndex = "Comments"
            'objColumnAddress.ClientIDMode = Web.UI.ClientIDMode.Static
            'objColumnAddress.Flex = 1
            'objListcolumnAddress.Add(objColumnAddress)

            'Dim objparamAddress(0) As System.Data.SqlClient.SqlParameter
            'objparamAddress(0) = New SqlClient.SqlParameter
            'objparamAddress(0).ParameterName = "@ReportingPersonAddressID"
            'objparamAddress(0).SqlDbType = SqlDbType.BigInt
            'objparamAddress(0).Value = objPerson.objPerson.PK_ODM_Reporting_Person_ID

            'Dim objdtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(objPerson.ObjAddress)

            'For Each item As DataRow In objdtAddress.Rows
            '    item("Address_Type") = GetKategoriKontak(item("Address_Type"))
            'Next

            'For Each item As DataRow In objdtAddress.Rows
            '    item("Country_Code") = GetCountryName(item("Country_Code"))
            'Next

            'Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Address", objStoreAddress, objListcolumnAddress, objdtAddress)

        End If
    End Sub
End Class
