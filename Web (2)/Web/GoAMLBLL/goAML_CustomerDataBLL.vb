﻿Imports GoAMLBLL.goAML

<Serializable()>
Public Class goAML_CustomerDataBLL
    Public objgoAML_Ref_Customer As New GoAMLDAL.goAML_Ref_Customer
    Public objListgoAML_Ref_Director As New List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director)
    Public objListgoAML_Ref_Phone As New List(Of GoAMLDAL.goAML_Ref_Phone)
    Public objlistgoaml_Ref_Address As New List(Of GoAMLDAL.goAML_Ref_Address)
    Public objListgoAML_Ref_Identification As New List(Of GoAMLDAL.goAML_Person_Identification)
    Public objListgoAML_Ref_Identification_Director As New List(Of GoAMLDAL.goAML_Person_Identification)
    Public objListgoAML_Ref_Employer_Phone As New List(Of GoAMLDAL.goAML_Ref_Phone)
    Public objlistgoaml_Ref_Employer_Address As New List(Of GoAMLDAL.goAML_Ref_Address)
    Public objListgoAML_Ref_Director_Address As New List(Of GoAMLDAL.goAML_Ref_Address)
    Public objListgoAML_Ref_Director_Phone As New List(Of GoAMLDAL.goAML_Ref_Phone)
    Public objListgoAML_Ref_Director_Employer_Phone As New List(Of GoAMLDAL.goAML_Ref_Phone)
    Public objlistgoaml_Ref_Director_Employer_Address As New List(Of GoAMLDAL.goAML_Ref_Address)

#Region "goAML 5.0.1, Add by Septian, 27 Januari 2023"
    Public objgoAML_Ref_Customer2 As New goAML_Ref_Customer2
    Public ObjList_GoAML_Ref_Customer_PreviousName As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
    Public ObjList_GoAML_Ref_Customer_Email As List(Of DataModel.goAML_Ref_Customer_Email)
    Public ObjList_GoAML_Ref_Customer_EmploymentHistory As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
    Public ObjList_GoAML_Ref_Customer_PEP As List(Of DataModel.goAML_Ref_Customer_PEP)
    Public ObjList_GoAML_Ref_Customer_NetworkDevice As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
    Public ObjList_GoAML_Ref_Customer_SocialMedia As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
    Public ObjList_GoAML_Ref_Customer_Sanction As List(Of DataModel.goAML_Ref_Customer_Sanction)
    Public ObjList_GoAML_Ref_Customer_RelatedPerson As List(Of DataModel.goAML_Ref_Customer_Related_Person)
    Public ObjList_GoAML_Ref_Customer_AdditionalInformation As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
    Public ObjList_GoAML_Ref_Customer_URL As List(Of DataModel.goAML_Ref_Customer_URL)
    Public ObjList_GoAML_Ref_Customer_EntityIdentification As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
    Public ObjList_GoAML_Ref_Customer_RelatedEntities As List(Of DataModel.goAML_Ref_Customer_Related_Entities)
    Public ObjListgoAML_Ref_Director2 As New List(Of DataModel.goAML_Ref_Entity_Director)
#End Region

    Public Class goAML_Ref_Customer2
        'Add By Septian, go AML v5.0.1, 2023-01-12
        Public Property PK_Customer_ID As Long
        Public Property FK_Customer_Type_ID As Nullable(Of Integer)
        Public Property CIF As String
        Public Property Country_Of_Birth As String
        Public Property Full_Name_Frn As String
        Public Property Residence_Since As Nullable(Of Date)
        Public Property Is_Protected As Nullable(Of Boolean)
        Public Property Entity_Status As String
        Public Property Entity_Status_Date As Nullable(Of Date)
        'End Add go AML v5.0.1
    End Class

    Public Class goAML_Ref_Customer_Previous_Name
        Public Property PK_goAML_Ref_Customer_Previous_Name_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property CIF As String
        Public Property FIRST_NAME As String
        Public Property LAST_NAME As String
        Public Property COMMENTS As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    Public Class goAML_Ref_Customer_Employment_History
        'Core
        Public Property PK_goAML_Ref_Customer_Employment_History_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property CIF As String
        Public Property EMPLOYER_NAME As String
        Public Property EMPLOYER_BUSINESS As String
        Public Property EMPLOYER_IDENTIFIER As String
        Public Property EMPLOYMENT_PERIOD_VALID_FROM As Nullable(Of Date)
        Public Property EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE As Boolean
        Public Property EMPLOYMENT_PERIOD_VALID_TO As Nullable(Of Date)
        Public Property EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE As Boolean

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    Public Class goAML_Ref_Customer_Network_Device
        'Core
        Public Property PK_goAML_Ref_Customer_Network_Device_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property CIF As String
        Public Property DEVICE_NUMBER As String
        Public Property OPERATING_SYSTEM As String
        Public Property SERVICE_PROVIDER As String
        Public Property FK_GOAML_REPORT_ID As Long
        Public Property IPV6 As String
        Public Property IPV4 As String
        Public Property CGN_PORT As Integer
        Public Property IPV4_IPV6 As String
        Public Property NAT As String
        Public Property FIRST_SEEN_DATE As Nullable(Of Date)
        Public Property LAST_SEEN_DATE As Nullable(Of Date)
        Public Property USING_PROXY As Boolean
        Public Property CITY As String
        Public Property COUNTRY As String
        Public Property COMMENTS As String

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    Public Class goAML_Ref_Customer_Social_Media
        'Core
        Public Property PK_goAML_Ref_Customer_Social_Media_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property CIF As String
        Public Property PLATFORM As String
        Public Property USER_NAME As String
        Public Property COMMENTS As String

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
    End Class

    Public Class goAML_Ref_Customer_Additional_Information
        'Core
        Public Property PK_goAML_Ref_Customer_Additional_Information_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property CIF As String
        Public Property INFO_TYPE As String
        Public Property INFO_SUBJECT As String
        Public Property INFO_TEXT As String
        Public Property INFO_NUMERIC As Decimal
        Public Property INFO_DATE As Nullable(Of Date)
        Public Property INFO_BOOLEAN As Boolean

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
    End Class

    Public Class goAML_Grid_Customer
        Public Property ObjList_GoAML_PreviousName As List(Of goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name)
        Public Property ObjList_GoAML_Email As List(Of DataModel.goAML_Ref_Customer_Email)
        Public Property ObjList_GoAML_EmploymentHistory As List(Of goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History)
        Public Property ObjList_GoAML_PEP As List(Of DataModel.goAML_Ref_Customer_PEP)
        Public Property ObjList_GoAML_NetworkDevice As List(Of goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device)
        Public Property ObjList_GoAML_SocialMedia As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media)
        Public Property ObjList_GoAML_Sanction As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Public Property ObjList_GoAML_RelatedPerson As List(Of DataModel.goAML_Ref_Customer_Related_Person)
        Public Property ObjList_GoAML_AdditionalInformation As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information)
        Public Property ObjList_GoAML_URL As List(Of DataModel.goAML_Ref_Customer_URL)
        Public Property ObjList_GoAML_EntityIdentification As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Public Property ObjList_GoAML_RelatedEntities As List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        Public Property objList_GoAML_Ref_Director As New List(Of DataModel.goAML_Ref_Entity_Director)
    End Class

    Public Class General_Reference
        Public Property Kode As String
        Public Property Keterangan As String
    End Class

    Public Class VWgoAML_Customer

    End Class

End Class
