﻿Public Class DirectorEntityActivityClass
    Public objDirectorEntityActivity As New GoAMLDAL.goAML_Act_Director
    Public listAddress As New List(Of GoAMLDAL.goAML_Act_Director_Address)
    Public listPhone As New List(Of GoAMLDAL.goAML_Act_Director_Phone)
    Public listAddressEmployer As New List(Of GoAMLDAL.goAML_Act_Director_Address)
    Public listPhoneEmployer As New List(Of GoAMLDAL.goAML_Act_Director_Phone)
    Public listIdentification As New List(Of GoAMLDAL.goAML_Activity_Person_Identification)
End Class
