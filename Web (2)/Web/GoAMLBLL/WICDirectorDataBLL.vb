﻿Imports GoAMLBLL.goAML
Imports GoAMLDAL

Public Class WICDirectorDataBLL
    Public ObjDirector As goAML_Ref_Walk_In_Customer_Director
    Public ListDirectorPhone As List(Of goAML_Ref_Phone)
    Public ListDirectorPhoneEmployer As List(Of goAML_Ref_Phone)
    Public ListDirectorAddress As List(Of goAML_Ref_Address)
    Public ListDirectorAddressEmployer As List(Of goAML_Ref_Address)
    Public ListDirectorIdentification As List(Of goAML_Person_Identification)
    Public ListDirectorEmail As List(Of DataModel.goAML_Ref_Customer_Email)
    Public ListDirectorSanction As List(Of DataModel.goAML_Ref_Customer_Sanction)
    Public ListDirectorPEP As List(Of DataModel.goAML_Ref_Customer_PEP)
    Public Sub New()

    End Sub
End Class
