﻿<Serializable()>
Public Class goAML_AccountDataBLL
    Public objgoAML_Ref_Account As New GoAMLDAL.goAML_Ref_Account
    Public objListgoAML_Ref_Account_Signatory As New List(Of GoAMLDAL.goAML_Ref_Account_Signatory)
    Public objListgoAML_RelatedEntities As New List(Of GoAMLBLL.goAML_AccountBLL.goAML_Ref_Account_Related_Entity)
    Public objListgoAML_RelatedPerson As New List(Of GoAMLBLL.goAML_AccountBLL.goAML_Ref_Account_Related_Person)
    Public objListgoAML_RelatedAccount As New List(Of GoAMLBLL.goAML_AccountBLL.goAML_Ref_Account_Related_Account)
    Public objListgoAML_Sanction As New List(Of GoAMLBLL.goAML_AccountBLL.goAML_Ref_Account_Sanction)
End Class
