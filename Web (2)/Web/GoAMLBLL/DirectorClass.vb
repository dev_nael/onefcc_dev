﻿Public Class DirectorClass
    Public objDirector As New GoAMLDAL.goAML_Trn_Director
    Public listAddressDirector As New List(Of GoAMLDAL.goAML_Trn_Director_Address)
    Public listPhoneDirector As New List(Of GoAMLDAL.goAML_trn_Director_Phone)
    Public listAddressEmployerDirector As New List(Of GoAMLDAL.goAML_Trn_Director_Address)
    Public listPhoneEmployerDirector As New List(Of GoAMLDAL.goAML_trn_Director_Phone)
    Public listIdentification As New List(Of GoAMLDAL.goAML_Transaction_Person_Identification)

End Class
