﻿Public Class Transaction
    Public objTransaction As New GoAMLDAL.goAML_Transaction
    Public action As String
    'Pengirim
    Public objPersonFrom As New GoAMLDAL.goAML_Transaction_Person
    Public objEntityFrom As New GoAMLDAL.goAML_Transaction_Entity
    Public objAccountFrom As New GoAMLDAL.goAML_Transaction_Account
    Public LisAccountSignatoryFrom As New List(Of SignatoryClass)

    'Conductor
    Public ObjConductor As New GoAMLDAL.goAML_Trn_Conductor
    Public listObjAddressConductor As New List(Of GoAMLDAL.goAML_Trn_Conductor_Address)
    Public listObjPhoneConductor As New List(Of GoAMLDAL.goAML_trn_Conductor_Phone)
    Public listObjAddressEmployerConductor As New List(Of GoAMLDAL.goAML_Trn_Conductor_Address)
    Public listObjPhoneEmployerConductor As New List(Of GoAMLDAL.goAML_trn_Conductor_Phone)
    Public listObjIdentificationConductor As New List(Of GoAMLDAL.goAML_Transaction_Person_Identification)

    'PersonFrom
    Public listObjAddressPersonFrom As New List(Of GoAMLDAL.goAML_Trn_Person_Address)
    Public listObjPhonePersonFrom As New List(Of GoAMLDAL.goAML_trn_Person_Phone)
    Public listObjAddressEmployerPersonFrom As New List(Of GoAMLDAL.goAML_Trn_Person_Address)
    Public listObjPhoneEmployerPersonFrom As New List(Of GoAMLDAL.goAML_trn_Person_Phone)
    Public listObjIdentificationPersonFrom As New List(Of GoAMLDAL.goAML_Transaction_Person_Identification)

    'Account
    Public objAccountEntityFrom As New GoAMLDAL.goAML_Trn_Entity_account
    Public listObjAddressAccountEntityFrom As New List(Of GoAMLDAL.goAML_Trn_Acc_Entity_Address)
    Public listObjPhoneAccountEntityFrom As New List(Of GoAMLDAL.goAML_Trn_Acc_Entity_Phone)

    Public listDirectorAccountEntityFrom As New List(Of DirectorClass)

    'Entity
    Public ListObjAddressEntityFrom As New List(Of GoAMLDAL.goAML_Trn_Entity_Address)
    Public listObjPhoneEntityFrom As New List(Of GoAMLDAL.goAML_Trn_Entity_Phone)

    Public listDirectorEntityFrom As New List(Of DirectorClass)

    'Penerima-------------------------------------------------------------------
    Public objPersonTo As New GoAMLDAL.goAML_Transaction_Person
    Public objEntityTo As New GoAMLDAL.goAML_Transaction_Entity
    Public objAccountTo As New GoAMLDAL.goAML_Transaction_Account
    Public LisAccountSignatoryTo As New List(Of SignatoryClass)

    'Person
    Public listObjAddressPersonTo As New List(Of GoAMLDAL.goAML_Trn_Person_Address)
    Public listObjPhonePersonTo As New List(Of GoAMLDAL.goAML_trn_Person_Phone)
    Public listObjAddressEmployerPersonTo As New List(Of GoAMLDAL.goAML_Trn_Person_Address)
    Public listObjPhoneEmployerPersonTo As New List(Of GoAMLDAL.goAML_trn_Person_Phone)
    Public listObjIdentificationPersonTo As New List(Of GoAMLDAL.goAML_Transaction_Person_Identification)

    'Account
    Public objAccountEntityTo As New GoAMLDAL.goAML_Trn_Entity_account
    Public listObjAddressAccountEntityto As New List(Of GoAMLDAL.goAML_Trn_Acc_Entity_Address)
    Public listObjPhoneAccountEntityto As New List(Of GoAMLDAL.goAML_Trn_Acc_Entity_Phone)
    Public listDirectorAccountEntityto As New List(Of DirectorClass)

    'Entity
    Public ListObjAddressEntityto As New List(Of GoAMLDAL.goAML_Trn_Entity_Address)
    Public listObjPhoneEntityto As New List(Of GoAMLDAL.goAML_Trn_Entity_Phone)
    Public listDirectorEntityto As New List(Of DirectorClass)

    'Multiparty
    Public objTransactionParty As New GoAMLDAL.goAML_Transaction_Party
    Public listDirectorEntytAccountparty As New List(Of DirectorEntityAccountPartyClass)
    Public listDirectorEntityParty As New List(Of DirectorEntityPartyClass)
    Public listSignatoryAccountParty As New List(Of SignatoryAccountPartyClass)

    ' multiparty account
    Public objAccountParty As New GoAMLDAL.goAML_Trn_Party_Account
    Public objAccountEntityParty As New GoAMLDAL.goAML_Trn_Par_Acc_Entity
    Public listAddresAccountEntityParty As New List(Of GoAMLDAL.goAML_Trn_par_Acc_Entity_Address)
    Public listPhoneAccountEntityParty As New List(Of GoAMLDAL.goAML_trn_par_acc_Entity_Phone)

    ' multiparty person
    Public objPersonParty As New GoAMLDAL.goAML_Trn_Party_Person
    Public listAddressPersonParty As New List(Of GoAMLDAL.goAML_Trn_Party_Person_Address)
    Public listPhonePersonParty As New List(Of GoAMLDAL.goAML_Trn_Party_Person_Phone)
    Public listAddressEmployerPersonParty As New List(Of GoAMLDAL.goAML_Trn_Party_Person_Address)
    Public listPhoneEmployerPersonParty As New List(Of GoAMLDAL.goAML_Trn_Party_Person_Phone)
    Public listIdentificationPersonParty As New List(Of GoAMLDAL.goAML_Transaction_Party_Identification)

    ' multiparty entity
    Public objentityParty As New GoAMLDAL.goAML_Trn_Party_Entity
    Public listAddressEntityparty As New List(Of GoAMLDAL.goAML_Trn_Party_Entity_Address)
    Public listPhoneEntityParty As New List(Of GoAMLDAL.goAML_Trn_Party_Entity_Phone)

End Class
