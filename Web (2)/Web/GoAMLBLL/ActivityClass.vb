﻿Public Class ActivityClass
    Public action As String
    'Activity
    Public objActivity As New GoAMLDAL.goAML_Act_ReportPartyType
    Public listDirectorEntytAccountActivity As New List(Of DirectorEntityAccountActivityClass)
    Public listDirectorEntityActivity As New List(Of DirectorEntityActivityClass)
    Public listSignatoryAccountActivity As New List(Of SignatoryAccountActivityClass)

    ' Activity account
    Public objAccountActivity As New GoAMLDAL.goAML_Act_Account
    Public objAccountEntityActivity As New GoAMLDAL.goAML_Act_Entity_Account
    Public listAddresAccountEntityActivity As New List(Of GoAMLDAL.goAML_Act_Acc_Entity_Address)
    Public listPhoneAccountEntityActivity As New List(Of GoAMLDAL.goAML_Act_Acc_Entity_Phone)

    ' Activity person
    Public objPersonActivity As New GoAMLDAL.goAML_Act_Person
    Public listAddressPersonActivity As New List(Of GoAMLDAL.goAML_Act_Person_Address)
    Public listPhonePersonActivity As New List(Of GoAMLDAL.goAML_Act_Person_Phone)
    Public listAddressEmployerPersonActivity As New List(Of GoAMLDAL.goAML_Act_Person_Address)
    Public listPhoneEmployerPersonActivity As New List(Of GoAMLDAL.goAML_Act_Person_Phone)
    Public listIdentificationPersonActivity As New List(Of GoAMLDAL.goAML_Activity_Person_Identification)

    ' Activity entity
    Public objentityActivity As New GoAMLDAL.goAML_Act_Entity
    Public listAddressEntityActivity As New List(Of GoAMLDAL.goAML_Act_Entity_Address)
    Public listPhoneEntityActivity As New List(Of GoAMLDAL.goAML_Act_Entity_Phone)
End Class
