﻿Imports System.Text.RegularExpressions
Imports GoAMLBLL.Base.Model

Namespace Helper
    Public Class CommonHelper
        Public Shared Function GetReferenceByCode(Table As String, Kode As String) As GoAMLBLL.Base.Model.General_Reference

            Dim query = "SELECT TOP 1 * From " & Table & " WHERE Kode = '" & Kode & "'"
            Dim result = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)

            If result IsNot Nothing Then
                Dim objResult = New General_Reference
                objResult.Kode = result("Kode").ToString()
                objResult.Keterangan = result("Keterangan").ToString()

                Return objResult
            End If

            Return Nothing
        End Function

        Public Shared Function WrapSqlVariable(variable As Object) As String
            Dim ret = ""
            If TypeOf variable Is Boolean Then
                ret = If(variable, "1", "0")
            ElseIf TypeOf variable Is Integer Then
                ret = "" & variable & ""
            ElseIf variable Is Nothing Then ' 2023-11-20, Nael: Jika variable Nothing maka ret adalah Null
                ret = "NULL"
            ElseIf TypeOf variable Is String AndAlso String.IsNullOrEmpty(variable) Then ' 2023-11-21, Nael: Jika empty string maka ret adalah Null
                ret = "NULL"
            Else
                ret = "'" & variable & "'"
            End If

            Return ret
        End Function

        Shared Function IsFieldValid(text As String, fieldName As String) As Boolean
            Try
                Dim regex As New Regex("^[0-9 ]+$")

                Select Case fieldName
                    Case "incorporation_state"
                        regex = New Regex("[ a-zA-Z]*")
                    Case "tax_number"
                        regex = New Regex("[0-9]{15}")
                    Case "last_name"
                        regex = New Regex("[\-'. a-zA-Z]*")
                    Case "birth_place"
                        regex = New Regex("[ a-zA-Z]*")
                    Case "mothers_name"
                        regex = New Regex("[\-'. a-zA-Z]*")
                    Case "alias"
                        regex = New Regex("[\-'. a-zA-Z]*")
                    Case "ssn"
                        regex = New Regex("[0-9]{22}")
                    Case "city"
                        regex = New Regex("[ a-zA-Z]*")
                    Case "state"
                        regex = New Regex("[ a-zA-Z]*")
                    Case "email_address"
                        regex = New Regex("[_a-zA-Z0-9-+]+(\.[_a-zA-Z0-9-\+]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})")
                    Case "ipv4_address_type"
                        regex = New Regex("((1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])")
                    Case "ipv6_address_type"
                        regex = New Regex("([A-Fa-f0-9]{1,4}:){7}[A-Fa-f0-9]{1,4}")
                    Case Else
                        Return True
                End Select

                Return regex.IsMatch(text)
            Catch ex As Exception
                Return False
            End Try
        End Function

        Shared Function IsNumber(text As String) As Boolean
            Try

                Dim number As New Regex("^[0-9 ]+$")
                Return number.IsMatch(text)

            Catch ex As Exception
                Return False
            End Try
        End Function
    End Class
End Namespace

