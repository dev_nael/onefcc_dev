﻿Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Reflection
Imports System.Runtime.Remoting.Messaging
Imports System.Web.UI
Imports Ext.Net
Imports Microsoft.VisualBasic.CompilerServices
Imports NawaBLL
Imports NawaBLL.Nawa
Imports NawaDAL
Imports GoAMLBLL.Base.Constant
Imports GoAMLBLL.goAML
Imports GoAMLBLL.goAML.Enum
Imports GoAMLBLL.goAML.Services
Imports GoAMLBLL.goAML_CustomerDataBLL
Imports GoAMLBLL.WICDataBLL
Imports GoAMLDAL
Imports OfficeOpenXml.FormulaParsing.Excel.Functions.Logical

<Serializable()>
Public Class WICBLL
    Public oPanelinput As FormPanel
    Sub New(ObjPanel As FormPanel)
        oPanelinput = ObjPanel
    End Sub
    Sub New()

    End Sub
    'saad 17112020
    Shared Function getDataApproval(id As String, moduleName As String) As GoAMLDAL.ModuleApproval
        Dim objModuleApproval As New GoAMLDAL.ModuleApproval
        Using objdb As New GoAMLEntities
            Dim Approval As GoAMLDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.ModuleKey = id And x.ModuleName = moduleName).FirstOrDefault
            'If Approval IsNot Nothing Then

            'End If
            objModuleApproval = Approval
        End Using
        Return objModuleApproval
    End Function
    Sub SaveAddTanpaApproval(objData As goAML_Ref_WIC, listDirector As List(Of WICDirectorDataBLL), listAddressDetail As List(Of goAML_Ref_Address), listPhoneDetail As List(Of goAML_Ref_Phone), listAddressEmployerDetail As List(Of goAML_Ref_Address), listPhoneEmployerDetail As List(Of goAML_Ref_Phone), listIdentificationDetail As List(Of goAML_Person_Identification), objModule As NawaDAL.Module, wicData As WICDataBLL)
        Using objdb As New GoAMLEntities
            Using objtrans As DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    With objData
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = DateTime.Now
                    End With
                    objdb.Entry(objData).State = EntityState.Added

                    objdb.SaveChanges()

                    wicData.ObjWIC = objData
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim modulename As String = objModule.ModuleLabel
                    Dim ObjAuditTrailHeader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, objData)

                    If listDirector.Count > 0 Then

                        For Each DirectorBLL As WICDirectorDataBLL In listDirector
                            Dim objDirector As goAML_Ref_Walk_In_Customer_Director = DirectorBLL.ObjDirector
                            objDirector.FK_Entity_ID = objData.PK_Customer_ID
                            objdb.Entry(objDirector).State = EntityState.Added

                            objdb.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, objDirector)
                            If DirectorBLL.ListDirectorAddress.Count > 0 Then

                                For Each itemAddress As goAML_Ref_Address In DirectorBLL.ListDirectorAddress
                                    With itemAddress
                                        .FK_To_Table_ID = objDirector.NO_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With

                                    objdb.Entry(itemAddress).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, itemAddress)
                                Next
                            End If

                            If DirectorBLL.ListDirectorAddressEmployer.Count > 0 Then

                                For Each itemAddress As goAML_Ref_Address In DirectorBLL.ListDirectorAddressEmployer
                                    With itemAddress
                                        .FK_To_Table_ID = objDirector.NO_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With

                                    objdb.Entry(itemAddress).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, itemAddress)
                                Next

                            End If

                            If DirectorBLL.ListDirectorPhone.Count > 0 Then

                                For Each itemPhone As goAML_Ref_Phone In DirectorBLL.ListDirectorPhone
                                    With itemPhone
                                        .FK_for_Table_ID = objDirector.NO_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objdb.Entry(itemPhone).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, itemPhone)
                                Next

                            End If

                            If DirectorBLL.ListDirectorPhoneEmployer.Count > 0 Then

                                For Each itemPhone As goAML_Ref_Phone In DirectorBLL.ListDirectorPhoneEmployer
                                    With itemPhone
                                        .FK_for_Table_ID = objDirector.NO_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objdb.Entry(itemPhone).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, itemPhone)
                                Next

                            End If

                            If DirectorBLL.ListDirectorIdentification.Count > 0 Then

                                For Each itemIdentification As goAML_Person_Identification In DirectorBLL.ListDirectorIdentification
                                    With itemIdentification
                                        .FK_Person_ID = objDirector.NO_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objdb.Entry(itemIdentification).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, itemIdentification)
                                Next

                            End If
                        Next
                    End If

                    If listAddressDetail.Count > 0 Then

                        For Each item As goAML_Ref_Address In listAddressDetail
                            With item
                                .FK_To_Table_ID = objData.PK_Customer_ID
                                .CreatedBy = Common.SessionCurrentUser.UserID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                                .CreatedDate = DateTime.Now
                            End With
                            objdb.Entry(item).State = EntityState.Added
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next

                    End If

                    If listPhoneDetail.Count > 0 Then

                        For Each item As goAML_Ref_Phone In listPhoneDetail
                            With item
                                .FK_for_Table_ID = objData.PK_Customer_ID
                                .CreatedBy = Common.SessionCurrentUser.UserID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                                .CreatedDate = DateTime.Now
                            End With
                            objdb.Entry(item).State = EntityState.Added
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next

                    End If

                    If listAddressEmployerDetail.Count > 0 Then

                        For Each item As goAML_Ref_Address In listAddressEmployerDetail
                            With item
                                .FK_To_Table_ID = objData.PK_Customer_ID
                                .CreatedBy = Common.SessionCurrentUser.UserID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                                .CreatedDate = DateTime.Now
                            End With
                            objdb.Entry(item).State = EntityState.Added
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next

                    End If

                    If listPhoneEmployerDetail.Count > 0 Then

                        For Each item As goAML_Ref_Phone In listPhoneEmployerDetail
                            With item
                                .FK_for_Table_ID = objData.PK_Customer_ID
                                .CreatedBy = Common.SessionCurrentUser.UserID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                                .CreatedDate = DateTime.Now
                            End With
                            objdb.Entry(item).State = EntityState.Added
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next

                    End If

                    If listIdentificationDetail.Count > 0 Then

                        For Each item As goAML_Person_Identification In listIdentificationDetail
                            With item
                                .FK_Person_ID = objData.PK_Customer_ID
                                .CreatedBy = Common.SessionCurrentUser.UserID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                                .CreatedDate = DateTime.Now
                            End With
                            objdb.Entry(item).State = EntityState.Added
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next

                    End If

                    objdb.SaveChanges()
                    objtrans.Commit()

                    ' AuditTrail, goAML 5.0.1, Add by Septian, 2023-2-21
                    SaveGoAmlWic501(wicData)
                    CreateAuditTrail(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, wicData.ObjGrid)
                    ' End Audit Trail
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
    Public Sub SaveAddApproval(objSave As goAML_Ref_WIC, listDirector As List(Of WICDirectorDataBLL), listAddressDetail As List(Of goAML_Ref_Address), listPhoneDetail As List(Of goAML_Ref_Phone), listAddressEmployerDetail As List(Of goAML_Ref_Address), listPhoneEmployerDetail As List(Of goAML_Ref_Phone), listIdentificationDetail As List(Of goAML_Person_Identification), objModule As NawaDAL.Module, wicData As WICDataBLL)
        Using objDB As New GoAMLEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objData As New WICDataBLL With
                    {
                        .ObjWIC = objSave,
                        .ObjWICAddress = listAddressDetail,
                        .ObjWICPhone = listPhoneDetail,
                        .ObjWICAddressEmployer = listAddressEmployerDetail,
                        .OBjWICPhoneEmployer = listPhoneEmployerDetail,
                        .ObjWICIdentification = listIdentificationDetail,
                        .ObjDirector = listDirector,
                        .ObjGrid = wicData.ObjGrid,
                        .ObjWIC2 = wicData.ObjWIC2
                    }
                    Dim dataXML As String = Common.Serialize(objData)

                    Dim objModuleApproval As New GoAMLDAL.ModuleApproval With
                    {
                        .ModuleName = objModule.ModuleName,
                        .ModuleKey = 0,
                        .ModuleField = dataXML,
                        .ModuleFieldBefore = "",
                        .PK_ModuleAction_ID = Common.ModuleActionEnum.Insert,
                        .CreatedBy = Common.SessionCurrentUser.UserID,
                        .CreatedDate = Now
                    }

                    objDB.Entry(objModuleApproval).State = EntityState.Added

                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim modulename As String = objModule.ModuleLabel
                    Dim objATHeader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, objData)

                    objDB.SaveChanges()

                    If listDirector.Count > 0 Then
                        For Each itemDetail As WICDirectorDataBLL In listDirector
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemDetail.ObjDirector)
                            If itemDetail.ListDirectorAddress.Count > 0 Then
                                For Each item In itemDetail.ListDirectorAddress
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If
                            If itemDetail.ListDirectorAddressEmployer.Count > 0 Then
                                For Each item In itemDetail.ListDirectorAddressEmployer
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If itemDetail.ListDirectorPhone.Count > 0 Then
                                For Each item In itemDetail.ListDirectorPhone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If itemDetail.ListDirectorPhoneEmployer.Count > 0 Then
                                For Each item In itemDetail.ListDirectorPhoneEmployer
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If itemDetail.ListDirectorIdentification.Count > 0 Then
                                For Each item In itemDetail.ListDirectorIdentification
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If
                        Next
                    End If

                    If listAddressDetail.Count > 0 Then
                        For Each item In listAddressDetail
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listAddressEmployerDetail.Count > 0 Then
                        For Each item In listAddressEmployerDetail
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listPhoneDetail.Count > 0 Then
                        For Each item In listPhoneDetail
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listPhoneEmployerDetail.Count > 0 Then
                        For Each item In listPhoneEmployerDetail
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listIdentificationDetail.Count > 0 Then
                        For Each item In listIdentificationDetail
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    ' AuditTrail, goAML 5.0.1, Add by Septian, 2023-2-21
                    CreateAuditTrail(objDB, objATHeader.PK_AuditTrail_ID, wicData.ObjGrid)
                    ' End Audit Trail

                    objDB.SaveChanges()
                    objTrans.Commit()

                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Sub Accept(pK_ModuleApproval_ID As String)
        Using objDB As New GoAMLEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objApproval As GoAMLDAL.ModuleApproval = objDB.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = pK_ModuleApproval_ID).FirstOrDefault()
                    Dim objModule As GoAMLDAL.Module
                    Dim objModuledata As WICDataBLL
                    If objApproval IsNot Nothing Then
                        objModule = objDB.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    objModuledata = Common.Deserialize(objApproval.ModuleField, GetType(WICDataBLL))

                    Select Case objApproval.PK_ModuleAction_ID
                        Case Common.ModuleActionEnum.Insert

                            'objModuledata = Common.Deserialize(objApproval.ModuleField, GetType(WICDataBLL))
                            With objModuledata.ObjWIC
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                                .Corp_Comments = IIf(String.IsNullOrEmpty(.Corp_Comments), Nothing, .Corp_Comments)
                                .INDV_Comment = IIf(String.IsNullOrEmpty(.INDV_Comment), Nothing, .INDV_Comment)
                                .INDV_Birth_Place = IIf(String.IsNullOrEmpty(.INDV_Birth_Place), Nothing, .INDV_Birth_Place)
                                .INDV_Mothers_Name = IIf(String.IsNullOrEmpty(.INDV_Mothers_Name), Nothing, .INDV_Mothers_Name)
                                .INDV_Alias = IIf(String.IsNullOrEmpty(.INDV_Alias), Nothing, .INDV_Alias)
                                .INDV_SSN = IIf(String.IsNullOrEmpty(.INDV_SSN), Nothing, .INDV_SSN)
                                .INDV_Passport_Number = IIf(String.IsNullOrEmpty(.INDV_Passport_Number), Nothing, .INDV_Passport_Number)
                                .INDV_ID_Number = IIf(String.IsNullOrEmpty(.INDV_ID_Number), Nothing, .INDV_ID_Number)
                                .INDV_Occupation = IIf(String.IsNullOrEmpty(.INDV_Occupation), Nothing, .INDV_Occupation)
                                .INDV_Employer_Name = IIf(String.IsNullOrEmpty(.INDV_Employer_Name), Nothing, .INDV_Employer_Name)
                                .INDV_Gender = IIf(String.IsNullOrEmpty(.INDV_Gender), Nothing, .INDV_Gender)
                                .INDV_SumberDana = IIf(String.IsNullOrEmpty(.INDV_SumberDana), Nothing, .INDV_SumberDana)
                                .INDV_Tax_Number = IIf(String.IsNullOrEmpty(.INDV_Tax_Number), Nothing, .INDV_Tax_Number)
                            End With
                            objDB.Entry(objModuledata.ObjWIC).State = EntityState.Added

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, objModuledata)
                            objDB.SaveChanges()

                            If objModuledata.ObjDirector.Count > 0 Then

                                For Each objDirector As WICDirectorDataBLL In objModuledata.ObjDirector
                                    Dim item As goAML_Ref_Walk_In_Customer_Director = objDirector.ObjDirector
                                    With item
                                        .FK_Entity_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                        .Comments = IIf(String.IsNullOrEmpty(.Comments), Nothing, .Comments)
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                    objDB.SaveChanges()

                                    If objDirector.ListDirectorAddress.Count > 0 Then
                                        For Each itemAddress As goAML_Ref_Address In objDirector.ListDirectorAddress
                                            With itemAddress
                                                .FK_To_Table_ID = item.NO_ID
                                                .FK_Ref_Detail_Of = 8
                                                .CreatedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .CreatedDate = DateTime.Now
                                                .Comments = IIf(String.IsNullOrEmpty(.Comments), Nothing, .Comments) ' 2023-11-21, Nael
                                                .State = IIf(String.IsNullOrEmpty(.State), Nothing, .State) ' 2023-11-21, Nael
                                                .IsCustomer = 0
                                            End With
                                            objDB.Entry(itemAddress).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemAddress) ' 2023-11-21, Nael
                                        Next

                                    End If

                                    If objDirector.ListDirectorAddressEmployer.Count > 0 Then

                                        For Each itemAddress As goAML_Ref_Address In objDirector.ListDirectorAddressEmployer
                                            With itemAddress
                                                .FK_To_Table_ID = item.NO_ID
                                                .FK_Ref_Detail_Of = 4
                                                .CreatedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .CreatedDate = DateTime.Now
                                                .Comments = IIf(String.IsNullOrEmpty(.Comments), Nothing, .Comments) ' 2023-11-21, Nael
                                                .State = IIf(String.IsNullOrEmpty(.State), Nothing, .State) ' 2023-11-21, Nael
                                                .Zip = IIf(String.IsNullOrEmpty(.Zip), Nothing, .Zip)
                                                .Town = IIf(String.IsNullOrEmpty(.Town), Nothing, .Town)
                                                .IsCustomer = 0
                                            End With
                                            objDB.Entry(itemAddress).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemAddress)
                                        Next

                                    End If

                                    If objDirector.ListDirectorPhone.Count > 0 Then
                                        For Each itemPhone As goAML_Ref_Phone In objDirector.ListDirectorPhone
                                            With itemPhone
                                                .FK_for_Table_ID = item.NO_ID
                                                .FK_Ref_Detail_Of = 8
                                                .CreatedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .CreatedDate = DateTime.Now
                                                .comments = IIf(String.IsNullOrEmpty(.comments), Nothing, .comments) ' 2023-11-21, Nael
                                                .tph_country_prefix = IIf(String.IsNullOrEmpty(.tph_country_prefix), Nothing, .tph_country_prefix) ' 2023-11-21, Nael
                                                .IsCustomer = 0
                                            End With
                                            objDB.Entry(itemPhone).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemPhone)
                                        Next

                                    End If

                                    If objDirector.ListDirectorPhoneEmployer.Count > 0 Then

                                        For Each itemPhone As goAML_Ref_Phone In objDirector.ListDirectorPhoneEmployer
                                            With itemPhone
                                                .FK_for_Table_ID = item.NO_ID
                                                .FK_Ref_Detail_Of = 4
                                                .CreatedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .CreatedDate = DateTime.Now
                                                .comments = IIf(String.IsNullOrEmpty(.comments), Nothing, .comments) ' 2023-11-21, Nael
                                                .tph_country_prefix = IIf(String.IsNullOrEmpty(.tph_country_prefix), Nothing, .tph_country_prefix) ' 2023-11-21, Nael
                                                .IsCustomer = 0
                                            End With
                                            objDB.Entry(itemPhone).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemPhone)
                                        Next

                                    End If

                                    If objDirector.ListDirectorIdentification.Count > 0 Then

                                        For Each itemIdentification As goAML_Person_Identification In objDirector.ListDirectorIdentification
                                            With itemIdentification
                                                .FK_Person_ID = item.NO_ID
                                                .FK_Person_Type = 7
                                                .CreatedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .CreatedDate = DateTime.Now
                                                .Identification_Comment = IIf(String.IsNullOrEmpty(.Identification_Comment), Nothing, .Identification_Comment) ' 2023-11-21, Nael
                                                .IsCustomer = 0
                                            End With
                                            objDB.Entry(itemIdentification).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemIdentification)
                                        Next

                                    End If
                                Next
                            End If

                            If objModuledata.ObjWICAddress.Count > 0 Then

                                For Each item As goAML_Ref_Address In objModuledata.ObjWICAddress
                                    With item
                                        .FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .FK_Ref_Detail_Of = 3 ' 2023-11-14, Nael: WIC  -> https://nawadata0-my.sharepoint.com/:x:/g/personal/felix_d_nawadata_com/ETLNlr0hjTNIufViJU_8G54Bp2QRghSxXKfhAb9AlAa9Sw?e=GPWeMj, Sheet: Address, Phone
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                        .Comments = IIf(String.IsNullOrEmpty(.Comments), Nothing, .Comments) ' 2023-11-21, Nael
                                        .State = IIf(String.IsNullOrEmpty(.State), Nothing, .State) ' 2023-11-21, Nael
                                        .Zip = IIf(String.IsNullOrEmpty(.Zip), Nothing, .Zip)
                                        .Town = IIf(String.IsNullOrEmpty(.Town), Nothing, .Town)
                                        .IsCustomer = 0
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.ObjWICPhone.Count > 0 Then

                                For Each item As goAML_Ref_Phone In objModuledata.ObjWICPhone
                                    With item
                                        .FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .FK_Ref_Detail_Of = 3 ' 2023-11-14, Nael: WIC  -> https://nawadata0-my.sharepoint.com/:x:/g/personal/felix_d_nawadata_com/ETLNlr0hjTNIufViJU_8G54Bp2QRghSxXKfhAb9AlAa9Sw?e=GPWeMj, Sheet: Address, Phone
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                        .comments = IIf(String.IsNullOrEmpty(.comments), Nothing, .comments) ' 2023-11-21, Nael
                                        .tph_country_prefix = IIf(String.IsNullOrEmpty(.tph_country_prefix), Nothing, .tph_country_prefix) ' 2023-11-21, Nael
                                        .IsCustomer = 0
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.ObjWICAddressEmployer.Count > 0 Then

                                For Each item As goAML_Ref_Address In objModuledata.ObjWICAddressEmployer
                                    With item
                                        .FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .FK_Ref_Detail_Of = 10 ' 2023-11-14, Nael: Employer WIC  -> https://nawadata0-my.sharepoint.com/:x:/g/personal/felix_d_nawadata_com/ETLNlr0hjTNIufViJU_8G54Bp2QRghSxXKfhAb9AlAa9Sw?e=GPWeMj, Sheet: Address, Phone
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                        .Comments = IIf(String.IsNullOrEmpty(.Comments), Nothing, .Comments) ' 2023-11-21, Nael
                                        .State = IIf(String.IsNullOrEmpty(.State), Nothing, .State) ' 2023-11-21, Nael
                                        .Zip = IIf(String.IsNullOrEmpty(.Zip), Nothing, .Zip)
                                        .Town = IIf(String.IsNullOrEmpty(.Town), Nothing, .Town)
                                        .IsCustomer = 0
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.OBjWICPhoneEmployer.Count > 0 Then
                                For Each item As goAML_Ref_Phone In objModuledata.OBjWICPhoneEmployer
                                    With item
                                        .FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .FK_Ref_Detail_Of = 10 ' 2023-11-14, Nael: Employer WIC -> https://nawadata0-my.sharepoint.com/:x:/g/personal/felix_d_nawadata_com/ETLNlr0hjTNIufViJU_8G54Bp2QRghSxXKfhAb9AlAa9Sw?e=GPWeMj, Sheet: Address, Phone
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                        .comments = IIf(String.IsNullOrEmpty(.comments), Nothing, .comments) ' 2023-11-21, Nael
                                        .tph_country_prefix = IIf(String.IsNullOrEmpty(.tph_country_prefix), Nothing, .tph_country_prefix) ' 2023-11-21, Nael
                                        .IsCustomer = 0
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.ObjWICIdentification.Count > 0 Then
                                For Each item As goAML_Person_Identification In objModuledata.ObjWICIdentification
                                    With item
                                        .FK_Person_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .FK_Person_Type = 8 ' 2023-11-14, Nael: Identification WIC -> https://nawadata0-my.sharepoint.com/:x:/g/personal/felix_d_nawadata_com/ETLNlr0hjTNIufViJU_8G54Bp2QRghSxXKfhAb9AlAa9Sw?e=GPWeMj, Sheet: Identification
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                        .Identification_Comment = IIf(String.IsNullOrEmpty(.Identification_Comment), Nothing, .Identification_Comment) ' 2023-11-21, Nael
                                        .IsCustomer = 0
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            'SaveGoAmlWic501(objModuledata)
                        Case Common.ModuleActionEnum.Update

                            objModuledata = Common.Deserialize(objApproval.ModuleField, GetType(WICDataBLL))
                            Dim objModuledataOld As WICDataBLL = Common.Deserialize(objApproval.ModuleFieldBefore, GetType(WICDataBLL))

                            objModuledata.ObjWIC.ApprovedBy = Common.SessionCurrentUser.UserID
                            objModuledata.ObjWIC.ApprovedDate = Now
                            objDB.Entry(objModuledata.ObjWIC).State = EntityState.Modified

                            Dim user As String = Common.SessionCurrentUser.UserID
                            Dim act As Integer = Common.AuditTrailStatusEnum.AffectedToDatabase
                            Dim acts As Integer = Common.ModuleActionEnum.Update
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, objModuledata, objModuledataOld)
                            objDB.SaveChanges()
                            Dim findDirectors As List(Of goAML_Ref_Walk_In_Customer_Director) = (From x In objDB.goAML_Ref_Walk_In_Customer_Director Where x.FK_Entity_ID = objModuledata.ObjWIC.PK_Customer_ID Select x).ToList()

                            For Each objDirFromDatabase As goAML_Ref_Walk_In_Customer_Director In findDirectors
                                Dim itemx As WICDirectorDataBLL = objModuledata.ObjDirector.FirstOrDefault(Function(x) x.ObjDirector.NO_ID.Equals(objDirFromDatabase.NO_ID))
                                If itemx Is Nothing Then
                                    objDB.Entry(objDirFromDatabase).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, objDirFromDatabase)
                                    objDB.SaveChanges()

                                    For Each itemAddressx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objDirFromDatabase.NO_ID And x.FK_Ref_Detail_Of = 8 Select x).ToList()
                                        objDB.Entry(itemAddressx).State = EntityState.Deleted
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemAddressx)
                                        objDB.SaveChanges()
                                    Next

                                    For Each itemPhonex As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objDirFromDatabase.NO_ID And x.FK_Ref_Detail_Of = 8 Select x).ToList()
                                        objDB.Entry(itemPhonex).State = EntityState.Deleted
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemPhonex)
                                        objDB.SaveChanges()
                                    Next

                                    For Each itemAddressx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objDirFromDatabase.NO_ID And x.FK_Ref_Detail_Of = 4 Select x).ToList()
                                        objDB.Entry(itemAddressx).State = EntityState.Deleted
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemAddressx)
                                        objDB.SaveChanges()
                                    Next

                                    For Each itemPhonex As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objDirFromDatabase.NO_ID And x.FK_Ref_Detail_Of = 4 Select x).ToList()
                                        objDB.Entry(itemPhonex).State = EntityState.Deleted
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemPhonex)
                                        objDB.SaveChanges()
                                    Next

                                    For Each itemIdentificationx As goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = objDirFromDatabase.NO_ID And x.FK_Person_Type = 7 Select x).ToList()
                                        objDB.Entry(itemIdentificationx).State = EntityState.Deleted
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemIdentificationx)
                                        objDB.SaveChanges()
                                    Next
                                End If
                            Next

                            For Each objDirectors As WICDirectorDataBLL In objModuledata.ObjDirector
                                Dim item As goAML_Ref_Walk_In_Customer_Director = objDirectors.ObjDirector
                                Dim obcek As goAML_Ref_Walk_In_Customer_Director = (From x In objDB.goAML_Ref_Walk_In_Customer_Director Where x.NO_ID = item.NO_ID Select x).FirstOrDefault

                                If obcek Is Nothing Then
                                    With item
                                        'agam 23102020
                                        .FK_Entity_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .Comments = IIf(String.IsNullOrEmpty(.Comments), Nothing, .Comments) ' 2023-11-21, Nael
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                Else
                                    With item
                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                        .Comments = IIf(String.IsNullOrEmpty(.Comments), Nothing, .Comments) ' 2023-11-21, Nael
                                    End With
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = EntityState.Modified
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                End If
                                objDB.SaveChanges()

                                If objDirectors.ListDirectorPhone.Count > 0 Then
                                    ' 2023-10-23, Nael: Delete
                                    Dim listDirPhoneInDb = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objDirectors.ObjDirector.NO_ID And x.FK_Ref_Detail_Of = 8).ToList()

                                    For Each phone As goAML_Ref_Phone In listDirPhoneInDb
                                        Dim isDeleted = Not objDirectors.ListDirectorPhone.Exists(Function(x) x.PK_goAML_Ref_Phone = phone.PK_goAML_Ref_Phone)
                                        If isDeleted Then
                                            objDB.Entry(phone).State = Entity.EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, phone)
                                            objDB.SaveChanges()
                                        End If
                                    Next

                                    ' 2023-10-23, Nael: Insert dan Update
                                    For Each itemP As goAML_Ref_Phone In objDirectors.ListDirectorPhone
                                        Dim obcekP As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemP.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 8 Select x).FirstOrDefault
                                        If obcekP Is Nothing Then
                                            With itemP
                                                'agam 23102020
                                                .FK_for_Table_ID = item.NO_ID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemP).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        Else
                                            With itemP
                                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                            End With
                                            objDB.Entry(obcekP).CurrentValues.SetValues(itemP)
                                            objDB.Entry(obcekP).State = EntityState.Modified
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                If objDirectors.ListDirectorAddress.Count > 0 Then
                                    ' 2023-10-23, Nael: Delete
                                    Dim listDirAddressInDb = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objDirectors.ObjDirector.NO_ID And x.FK_Ref_Detail_Of = 8).ToList()

                                    For Each address As goAML_Ref_Address In listDirAddressInDb
                                        Dim isDeleted = Not objDirectors.ListDirectorAddress.Exists(Function(x) x.PK_Customer_Address_ID = address.PK_Customer_Address_ID)
                                        If isDeleted Then
                                            objDB.Entry(address).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, address)
                                            objDB.SaveChanges()
                                        End If
                                    Next

                                    ' 2023-10-23, Nael: Insert and Update
                                    For Each itemP As goAML_Ref_Address In objDirectors.ListDirectorAddress
                                        Dim obcekP As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemP.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 8 Select x).FirstOrDefault
                                        If obcekP Is Nothing Then
                                            With itemP
                                                'agam 23102020
                                                .FK_To_Table_ID = item.NO_ID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemP).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        Else
                                            With itemP
                                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                            End With
                                            objDB.Entry(obcekP).CurrentValues.SetValues(itemP)
                                            objDB.Entry(obcekP).State = EntityState.Modified
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                If objDirectors.ListDirectorPhoneEmployer.Count > 0 Then
                                    ' 2023-10-23, Nael: Delete
                                    Dim listDirPhoneEmpInDb = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objDirectors.ObjDirector.NO_ID And x.FK_Ref_Detail_Of = 4).ToList()

                                    For Each phone As goAML_Ref_Phone In listDirPhoneEmpInDb
                                        Dim isDeleted = Not objDirectors.ListDirectorPhoneEmployer.Exists(Function(x) x.PK_goAML_Ref_Phone = phone.PK_goAML_Ref_Phone)
                                        If isDeleted Then
                                            objDB.Entry(phone).State = Entity.EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, phone)
                                            objDB.SaveChanges()
                                        End If
                                    Next

                                    ' 2023-10-23, Nael: Insert and Update
                                    For Each itemP As goAML_Ref_Phone In objDirectors.ListDirectorPhoneEmployer
                                        Dim obcekP As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemP.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 4 Select x).FirstOrDefault
                                        If obcekP Is Nothing Then
                                            With itemP
                                                'agam 23102020
                                                .FK_for_Table_ID = item.NO_ID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemP).State = EntityState.Added
                                            objDB.SaveChanges() ' 2023-10-19, Nael
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        Else
                                            With itemP
                                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                            End With
                                            objDB.Entry(obcekP).CurrentValues.SetValues(itemP)
                                            objDB.Entry(obcekP).State = EntityState.Modified
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        End If
                                        objDB.SaveChanges() ' 2023-10-19, Nael
                                    Next
                                End If

                                If objDirectors.ListDirectorAddressEmployer.Count > 0 Then
                                    ' 2023-10-23, Nael: Delete
                                    Dim listDirAddressEmpInDb = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objDirectors.ObjDirector.NO_ID And x.FK_Ref_Detail_Of = 4).ToList()

                                    For Each address As goAML_Ref_Address In listDirAddressEmpInDb
                                        Dim isDeleted = Not objDirectors.ListDirectorAddressEmployer.Exists(Function(x) x.PK_Customer_Address_ID = address.PK_Customer_Address_ID)
                                        If isDeleted Then
                                            objDB.Entry(address).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, address)
                                            objDB.SaveChanges()
                                        End If
                                    Next

                                    ' 2023-10-23, Nael: Insert and Update
                                    For Each itemP As goAML_Ref_Address In objDirectors.ListDirectorAddressEmployer
                                        Dim obcekP As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemP.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 4 Select x).FirstOrDefault
                                        If obcekP Is Nothing Then
                                            With itemP
                                                'agam 23102020
                                                .FK_To_Table_ID = item.NO_ID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemP).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        Else
                                            With itemP
                                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                            End With
                                            objDB.Entry(obcekP).CurrentValues.SetValues(itemP)
                                            objDB.Entry(obcekP).State = EntityState.Modified
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        End If
                                        objDB.SaveChanges() ' 2023-10-19, Nael
                                    Next
                                End If

                                If objDirectors.ListDirectorIdentification.Count > 0 Then
                                    ' 2023-10-23, Nael: Delete
                                    Dim listDirIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = objDirectors.ObjDirector.NO_ID And x.FK_REF_DETAIL_OF = 7).ToList()

                                    For Each identification As goAML_Person_Identification In listDirIdentification
                                        Dim isDeleted = Not objDirectors.ListDirectorIdentification.Exists(Function(x) x.PK_Person_Identification_ID = identification.PK_Person_Identification_ID)
                                        If isDeleted Then
                                            objDB.Entry(identification).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, identification)
                                            objDB.SaveChanges()
                                        End If
                                    Next

                                    For Each itemP As goAML_Person_Identification In objDirectors.ListDirectorIdentification
                                        Dim obcekP As goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = itemP.PK_Person_Identification_ID And x.FK_Person_Type = 7 Select x).FirstOrDefault
                                        If obcekP Is Nothing Then
                                            With itemP
                                                'agam 23102020
                                                .FK_Person_ID = item.NO_ID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemP).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        Else
                                            With itemP
                                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                            End With
                                            objDB.Entry(obcekP).CurrentValues.SetValues(itemP)
                                            objDB.Entry(obcekP).State = EntityState.Modified
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                            Next

                            For Each itemx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 3 Select x).ToList()
                                Dim objCek As goAML_Ref_Address = objModuledata.ObjWICAddress.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 3)
                                If objCek Is Nothing Then
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemx, objCek)
                                End If
                                objDB.SaveChanges()
                            Next

                            For Each item As goAML_Ref_Address In objModuledata.ObjWICAddress
                                Dim obcek As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 3 Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    With item
                                        'agam 23102020
                                        .FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                Else
                                    With item
                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                    End With
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = EntityState.Modified
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                End If
                                objDB.SaveChanges()
                            Next

                            For Each itemx As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 3 Select x).ToList()
                                Dim objCek As goAML_Ref_Phone = objModuledata.ObjWICPhone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 3)
                                If objCek Is Nothing Then
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemx, objCek)
                                End If
                                objDB.SaveChanges()
                            Next

                            For Each item As goAML_Ref_Phone In objModuledata.ObjWICPhone
                                Dim obcek As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    With item
                                        'agam 23102020
                                        .FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                Else
                                    With item
                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                    End With
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = EntityState.Modified
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                End If
                                objDB.SaveChanges()
                            Next

                            For Each itemx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 10 Select x).ToList()
                                Dim objCek As goAML_Ref_Address = objModuledata.ObjWICAddressEmployer.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 10)
                                If objCek Is Nothing Then
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemx, objCek)
                                End If
                                objDB.SaveChanges()
                            Next

                            For Each item As goAML_Ref_Address In objModuledata.ObjWICAddressEmployer
                                Dim obcek As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 10 Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    With item
                                        'agam 23102020
                                        .FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                Else
                                    With item
                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                    End With
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = EntityState.Modified
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                End If
                                objDB.SaveChanges()
                            Next

                            For Each itemx As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 10 Select x).ToList()
                                Dim objCek As goAML_Ref_Phone = objModuledata.OBjWICPhoneEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 10)
                                If objCek Is Nothing Then
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemx, objCek)
                                End If
                                objDB.SaveChanges()
                            Next

                            For Each item As goAML_Ref_Phone In objModuledata.OBjWICPhoneEmployer
                                Dim obcek As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 10 Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    With item
                                        'agam 23102020
                                        .FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                Else
                                    With item
                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                    End With
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = EntityState.Modified
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                End If
                                objDB.SaveChanges()
                            Next

                            For Each itemx As goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Person_Type = 8 Select x).ToList()
                                Dim objCek As goAML_Person_Identification = objModuledata.ObjWICIdentification.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID And x.FK_Person_Type = 8)
                                If objCek Is Nothing Then
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemx, objCek)
                                End If
                                objDB.SaveChanges()
                            Next

                            For Each item As goAML_Person_Identification In objModuledata.ObjWICIdentification
                                Dim obcek As goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = item.PK_Person_Identification_ID And x.FK_Person_Type = 8 Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    With item
                                        'agam 23102020
                                        .FK_Person_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                Else
                                    With item
                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                    End With
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = EntityState.Modified
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                End If
                                objDB.SaveChanges()
                            Next

                        Case Common.ModuleActionEnum.Delete
                            objModuledata = Common.Deserialize(objApproval.ModuleField, GetType(WICDataBLL))
                            objDB.Entry(objModuledata.ObjWIC).State = EntityState.Deleted

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, objModuledata)

                            If objModuledata.ObjDirector.Count > 0 Then

                                For Each item As WICDirectorDataBLL In objModuledata.ObjDirector
                                    Dim items As goAML_Ref_Walk_In_Customer_Director = item.ObjDirector
                                    objDB.Entry(items).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)

                                    If item.ListDirectorAddress.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In item.ListDirectorAddress
                                            objDB.Entry(itemD).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If item.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In item.ListDirectorAddressEmployer
                                            objDB.Entry(itemD).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If item.ListDirectorPhone.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In item.ListDirectorPhone
                                            objDB.Entry(itemD).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If item.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In item.ListDirectorPhoneEmployer
                                            objDB.Entry(itemD).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If item.ListDirectorIdentification.Count > 0 Then
                                        For Each itemD As goAML_Person_Identification In item.ListDirectorIdentification
                                            objDB.Entry(itemD).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                Next
                            End If

                            If objModuledata.ObjWICAddress.Count > 0 Then
                                For Each item As goAML_Ref_Address In objModuledata.ObjWICAddress
                                    objDB.Entry(item).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.ObjWICPhone.Count > 0 Then
                                For Each item As goAML_Ref_Phone In objModuledata.ObjWICPhone
                                    objDB.Entry(item).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.ObjWICAddressEmployer.Count > 0 Then
                                For Each item As goAML_Ref_Address In objModuledata.ObjWICAddressEmployer
                                    objDB.Entry(item).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.OBjWICPhoneEmployer.Count > 0 Then
                                For Each item As goAML_Ref_Phone In objModuledata.OBjWICPhoneEmployer
                                    objDB.Entry(item).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.ObjWICIdentification.Count > 0 Then
                                For Each item As goAML_Person_Identification In objModuledata.ObjWICIdentification
                                    objDB.Entry(item).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            objDB.SaveChanges()

                    End Select

                    objDB.Entry(objApproval).State = EntityState.Deleted
                    objDB.SaveChanges()
                    objTrans.Commit()

                    Delete_Grid(objModuledata.ObjWIC.WIC_No, objModuledata.ObjGrid)
                    SaveGoAmlWic501(objModuledata)
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Public Sub Reject(pK_ModuleApproval_ID As Long)
        Using objDB As New GoAMLEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objApproval As GoAMLDAL.ModuleApproval = objDB.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = pK_ModuleApproval_ID).FirstOrDefault()
                    Dim objModule As GoAMLDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objDB.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case Common.ModuleActionEnum.Insert
                            Dim objModuledata As WICDataBLL = Common.Deserialize(objApproval.ModuleField, GetType(WICDataBLL))

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, objModuledata)

                            If objModuledata.ObjDirector.Count > 0 Then

                                For Each itemDetail As WICDirectorDataBLL In objModuledata.ObjDirector
                                    Dim item As goAML_Ref_Walk_In_Customer_Director = itemDetail.ObjDirector
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)

                                    If itemDetail.ListDirectorAddress.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddress
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddressEmployer
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorPhone.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhone
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhoneEmployer
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorIdentification.Count > 0 Then
                                        For Each itemD As goAML_Person_Identification In itemDetail.ListDirectorIdentification
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If
                                Next

                            End If

                            If objModuledata.ObjWICAddress.Count > 0 Then
                                For Each itemDetail As goAML_Ref_Address In objModuledata.ObjWICAddress
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next
                            End If

                            If objModuledata.ObjWICPhone.Count > 0 Then
                                For Each itemDetail As goAML_Ref_Phone In objModuledata.ObjWICPhone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next

                            End If

                            If objModuledata.ObjWICAddressEmployer.Count > 0 Then
                                For Each itemDetail As goAML_Ref_Address In objModuledata.ObjWICAddressEmployer
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next
                            End If

                            If objModuledata.OBjWICPhoneEmployer.Count > 0 Then
                                For Each itemDetail As goAML_Ref_Phone In objModuledata.OBjWICPhoneEmployer
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next

                            End If

                            If objModuledata.ObjWICIdentification.Count > 0 Then
                                For Each itemDetail As goAML_Person_Identification In objModuledata.ObjWICIdentification
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next

                            End If

                        Case Common.ModuleActionEnum.Update
                            Dim objModuledata As WICDataBLL = Common.Deserialize(objApproval.ModuleField, GetType(WICDataBLL))
                            Dim objModuledataOld As WICDataBLL = Common.Deserialize(objApproval.ModuleFieldBefore, GetType(WICDataBLL))

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, objModuledata, objModuledataOld)
                            objDB.SaveChanges()

                            If objModuledata.ObjDirector.Count > 0 Then

                                For Each itemDetail As WICDirectorDataBLL In objModuledata.ObjDirector
                                    Dim item As goAML_Ref_Walk_In_Customer_Director = itemDetail.ObjDirector
                                    Dim tempObjDirector As WICDirectorDataBLL = objModuledataOld.ObjDirector.Where(Function(x) x.ObjDirector.NO_ID = item.NO_ID).FirstOrDefault()
                                    If tempObjDirector IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, tempObjDirector.ObjDirector)
                                    End If

                                    If itemDetail.ListDirectorAddress.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddress
                                            Dim itemm As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = item.NO_ID And x.FK_Ref_Detail_Of = 8)).FirstOrDefault
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddressEmployer
                                            Dim itemm As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = item.NO_ID And x.FK_Ref_Detail_Of = 4)).FirstOrDefault
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorPhone.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhone
                                            Dim itemm As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = item.NO_ID And x.FK_Ref_Detail_Of = 8)).FirstOrDefault
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhoneEmployer
                                            Dim itemm As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = item.NO_ID And x.FK_Ref_Detail_Of = 4)).FirstOrDefault
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorIdentification.Count > 0 Then
                                        For Each itemD As goAML_Person_Identification In itemDetail.ListDirectorIdentification
                                            Dim itemm As goAML_Person_Identification = (From x In objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = item.NO_ID And x.FK_Person_Type = 7)).FirstOrDefault
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                        Next
                                    End If
                                Next

                            End If

                            If objModuledata.ObjWICAddress.Count > 0 Then

                                For Each itemD As goAML_Ref_Address In objModuledata.ObjWICAddress
                                    Dim itemm As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 3)).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                Next

                            End If

                            If objModuledata.ObjWICPhone.Count > 0 Then

                                For Each itemD As goAML_Ref_Phone In objModuledata.ObjWICPhone
                                    Dim itemm As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 3)).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                Next

                            End If

                            If objModuledata.ObjWICAddressEmployer.Count > 0 Then

                                For Each itemD As goAML_Ref_Address In objModuledata.ObjWICAddressEmployer
                                    Dim itemm As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 10)).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                Next

                            End If

                            If objModuledata.OBjWICPhoneEmployer.Count > 0 Then

                                For Each itemD As goAML_Ref_Phone In objModuledata.OBjWICPhoneEmployer
                                    Dim itemm As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 10)).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                Next

                            End If

                            If objModuledata.ObjWICIdentification.Count > 0 Then

                                For Each itemD As goAML_Person_Identification In objModuledata.ObjWICIdentification
                                    Dim itemm As goAML_Person_Identification = (From x In objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Person_Type = 8)).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                Next

                            End If

                        Case Common.ModuleActionEnum.Delete
                            Dim objModuledata As WICDataBLL = Common.Deserialize(objApproval.ModuleField, GetType(WICDataBLL))

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, objModuledata)
                            objDB.SaveChanges()

                            If objModuledata.ObjDirector.Count > 0 Then

                                For Each itemDetail As WICDirectorDataBLL In objModuledata.ObjDirector
                                    Dim item As goAML_Ref_Walk_In_Customer_Director = itemDetail.ObjDirector
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)

                                    If itemDetail.ListDirectorAddress.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddress
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddressEmployer
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorPhone.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhone
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhoneEmployer
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorIdentification.Count > 0 Then
                                        For Each itemD As goAML_Person_Identification In itemDetail.ListDirectorIdentification
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                Next
                            End If

                            If objModuledata.ObjWICAddress.Count > 0 Then

                                For Each itemDetail As goAML_Ref_Address In objModuledata.ObjWICAddress
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next
                            End If

                            If objModuledata.ObjWICPhone.Count > 0 Then

                                For Each itemDetail As goAML_Ref_Phone In objModuledata.ObjWICPhone
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next

                            End If

                            If objModuledata.ObjWICAddressEmployer.Count > 0 Then

                                For Each itemDetail As goAML_Ref_Address In objModuledata.ObjWICAddressEmployer
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next
                            End If

                            If objModuledata.OBjWICPhoneEmployer.Count > 0 Then

                                For Each itemDetail As goAML_Ref_Phone In objModuledata.OBjWICPhoneEmployer
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next

                            End If

                            If objModuledata.ObjWICIdentification.Count > 0 Then

                                For Each itemDetail As goAML_Person_Identification In objModuledata.ObjWICIdentification
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next

                            End If

                    End Select

                    objDB.Entry(objApproval).State = EntityState.Deleted
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Public Sub LoadPanel(formPanelNew As FormPanel, moduleField As String, unikID As String)
        Dim objWICBLL As WICDataBLL = Common.Deserialize(moduleField, GetType(WICDataBLL))
        If Not objWICBLL Is Nothing Then
            If objWICBLL.ObjWIC.FK_Customer_Type_ID = 1 Then
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "ID", "PK_Customer_ID" & unikID, objWICBLL.ObjWIC.PK_Customer_ID)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Gelar", "INDV_Title" & unikID, objWICBLL.ObjWIC.INDV_Title)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nama Lengkap", "INDV_last_name" & unikID, objWICBLL.ObjWIC.INDV_Last_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tanggal Lahir", "INDV_Birthdate" & unikID, objWICBLL.ObjWIC.INDV_BirthDate)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tempat Lahir", "INDV_Birth_place" & unikID, objWICBLL.ObjWIC.INDV_Birth_Place)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nama Ibu Kandung", "INDV_Mothers_name" & unikID, objWICBLL.ObjWIC.INDV_Mothers_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nama Alias", "INDV_Alias" & unikID, objWICBLL.ObjWIC.INDV_Alias)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "NIK", "INDV_SSN" & unikID, objWICBLL.ObjWIC.INDV_SSN)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "No. Passport", "INDV_Passport_number" & unikID, objWICBLL.ObjWIC.INDV_Passport_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Negara Penerbit Passport", "INDV_Passport_country" & unikID, objWICBLL.ObjWIC.INDV_Passport_Country)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "No. Identitas Lain", "INDV_ID_Number" & unikID, objWICBLL.ObjWIC.INDV_ID_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Kewarganegaraan 1", "INDV_Nationality1 " & unikID, objWICBLL.ObjWIC.INDV_Nationality1)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Kewarganegaraan 2", "INDV_Nationality2" & unikID, objWICBLL.ObjWIC.INDV_Nationality2)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Kewarganegaraan 3", "INDV_Nationality3" & unikID, objWICBLL.ObjWIC.INDV_Nationality3)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Negara Domisili", "INDV_residence" & unikID, objWICBLL.ObjWIC.INDV_Residence)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Email", "INDV_Email" & unikID, objWICBLL.ObjWIC.INDV_Email)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Pekerjaan", "INDV_Occupation" & unikID, objWICBLL.ObjWIC.INDV_Occupation)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tempat Bekerja", "INDV_employer_name" & unikID, objWICBLL.ObjWIC.INDV_Employer_Name)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "ID", "PK_Customer_ID" & unikID, objWICBLL.ObjWIC.PK_Customer_ID)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nama Korporasi", "Corp_Name" & unikID, objWICBLL.ObjWIC.Corp_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nama Komersial", "Corp_Commercial_name" & unikID, objWICBLL.ObjWIC.Corp_Commercial_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Bentuk Korporasi", "Corp_Incorporation_legal_form" & unikID, objWICBLL.ObjWIC.Corp_Incorporation_Legal_Form)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nomor Induk Berusaha", "Corp_Incorporation_number" & unikID, objWICBLL.ObjWIC.Corp_Incorporation_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Bidang Usaha", "Corp_Business" & unikID, objWICBLL.ObjWIC.Corp_Business)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Email Korporasi", "Corp_Email" & unikID, objWICBLL.ObjWIC.Corp_Email)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Website Korporasi", "Corp_url" & unikID, objWICBLL.ObjWIC.Corp_Url)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Provinsi", "Corp_incorporation_state" & unikID, objWICBLL.ObjWIC.Corp_Incorporation_State)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Negara", "Corp_incorporation_country_code" & unikID, objWICBLL.ObjWIC.Corp_Incorporation_Country_Code)
                'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Pemilik/Pengurus/Orang yang diberikan Otorisasi Melakukan Transaksi", "Corp_director_id" & unikID, objWICBLL.ObjWIC.Corp_Director_ID)
                'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Peran", "Corp_Role" & unikID, objWICBLL.ObjWIC.Corp_Role)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tanggal Pendirian", "Corp_incorporation_date" & unikID, objWICBLL.ObjWIC.Corp_Incorporation_Date)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tutup?", "Corp_business_closed" & unikID, objWICBLL.ObjWIC.Corp_Business_Closed)
                If objWICBLL.ObjWIC.Corp_Business_Closed Then
                    BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tanggal Tutup", "Corp_date_business_closed" & unikID, objWICBLL.ObjWIC.Corp_Date_Business_Closed)
                End If
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "NPWP", "Corp_tax_number" & unikID, objWICBLL.ObjWIC.Corp_Tax_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Catatan", "Corp_Comments" & unikID, objWICBLL.ObjWIC.Corp_Comments)
            End If



            '--------------------- Phone -------------------
            Dim objStorePhone As New Store
            objStorePhone.ID = unikID & "StoreGridPhone"
            objStorePhone.ClientIDMode = ClientIDMode.Static

            Dim objModelPhone = New Model
            Dim objFieldPhone = New ModelField

            objFieldPhone = New ModelField
            objFieldPhone.Name = "PK_goAML_Ref_Phone"
            objFieldPhone.Type = ModelFieldType.Auto
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "FK_Ref_Detail_Of"
            objFieldPhone.Type = ModelFieldType.Int
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "FK_for_Table_ID"
            objFieldPhone.Type = ModelFieldType.Int
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "Tph_Contact_Type"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "Tph_Communication_Type"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "tph_country_prefix"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "tph_number"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "tph_extension"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "comments"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objStorePhone.Model.Add(objModelPhone)

            Dim objListColumnPhone As New List(Of ColumnBase)
            Using objColumnNo As New RowNumbererColumn
                objColumnNo.Text = "No."
                objColumnNo.ClientIDMode = ClientIDMode.Static
                objListColumnPhone.Add(objColumnNo)
            End Using

            Dim objColumnPhone As Column

            objColumnPhone = New Column
            objColumnPhone.Text = "Kategori Kontak"
            objColumnPhone.DataIndex = "Tph_Contact_Type"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            objColumnPhone = New Column
            objColumnPhone.Text = "Jenis Alat Komunikasi"
            objColumnPhone.DataIndex = "Tph_Communication_Type"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            objColumnPhone = New Column
            objColumnPhone.Text = "Kode Area Telp"
            objColumnPhone.DataIndex = "tph_country_prefix"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            objColumnPhone = New Column
            objColumnPhone.Text = "Nomor Telepon"
            objColumnPhone.DataIndex = "tph_number"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            objColumnPhone = New Column
            objColumnPhone.Text = "Nomor Extensi"
            objColumnPhone.DataIndex = "tph_extension"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            Dim objParamPhone(0) As SqlParameter
            objParamPhone(0) = New SqlParameter
            objParamPhone(0).ParameterName = "@PK_Customer_ID"
            objParamPhone(0).SqlDbType = SqlDbType.BigInt
            objParamPhone(0).Value = objWICBLL.ObjWIC.PK_Customer_ID

            Dim objdtPhone As DataTable = Common.CopyGenericToDataTable(objWICBLL.ObjWICPhone)
            BLL.NawaFramework.ExtGridPanel(formPanelNew, "Phone", objStorePhone, objListColumnPhone, objdtPhone)




            '---------------- Address ----------------
            Dim objStoreAddress As New Store
            objStoreAddress.ID = unikID & "StoreGridAddress"
            objStoreAddress.ClientIDMode = ClientIDMode.Static

            Dim objModelAddress As New Model
            Dim objFieldAddress As ModelField

            objFieldAddress = New ModelField
            objFieldAddress.Name = "PK_Customer_Address_ID"
            objFieldAddress.Type = ModelFieldType.Auto
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "FK_Ref_detail_of"
            objFieldAddress.Type = ModelFieldType.Int
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "FK_to_Table_ID"
            objFieldAddress.Type = ModelFieldType.Int
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Address_Type"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Address"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Town"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "City"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Zip"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Country_Code"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "State"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "comments"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objStoreAddress.Model.Add(objModelAddress)

            Dim objListColumnAddress As New List(Of ColumnBase)
            Using objColumnNo As New RowNumbererColumn
                objColumnNo.Text = "No."
                objColumnNo.ClientIDMode = ClientIDMode.Static
                objListColumnAddress.Add(objColumnNo)
            End Using

            Dim objColumnAddress As Column

            objColumnAddress = New Column
            objColumnAddress.Text = "Tipe Alamat"
            objColumnAddress.DataIndex = "Address_Type"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Alamat"
            objColumnAddress.DataIndex = "Address"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Kecamatan"
            objColumnAddress.DataIndex = "Town"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Kota/Kabupaten"
            objColumnAddress.DataIndex = "City"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Kode Pos"
            objColumnAddress.DataIndex = "Zip"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Negara"
            objColumnAddress.DataIndex = "Country_Code"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Provinsi"
            objColumnAddress.DataIndex = "State"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            Dim objParamAddress(0) As SqlParameter
            objParamAddress(0) = New SqlParameter
            objParamAddress(0).ParameterName = "@PK_Customer_ID"
            objParamAddress(0).SqlDbType = SqlDbType.BigInt
            objParamAddress(0).Value = objWICBLL.ObjWIC.PK_Customer_ID

            Dim objdtAddress As DataTable = Common.CopyGenericToDataTable(objWICBLL.ObjWICAddress)
            BLL.NawaFramework.ExtGridPanel(formPanelNew, "Address", objStoreAddress, objListColumnAddress, objdtAddress)

        End If
    End Sub

    Sub LoadPanelDelete(formPanelWICDelete As FormPanel, moduleName As String, strUnikKey As String)
        Dim objWIC As goAML_Ref_WIC = GetWICByID(strUnikKey)
        Dim negara, direktur, badan, role As String
        If Not objWIC Is Nothing Then
            Dim strUnik As String = Guid.NewGuid.ToString()
            If objWIC.FK_Customer_Type_ID = 1 Then
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "ID", "PK_Customer_ID" & strUnik, objWIC.PK_Customer_ID)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Gelar", "INDV_Title" & strUnik, objWIC.INDV_Title)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Nama Lengkap", "INDV_last_name" & strUnik, objWIC.INDV_Last_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Tanggal Lahir", "INDV_Birthdate" & strUnik, objWIC.INDV_BirthDate.Value.ToString("dd-MMM-yy"))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Tempat Lahir", "INDV_Birth_place" & strUnik, objWIC.INDV_Birth_Place)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Nama Ibu Kandung", "INDV_Mothers_name" & strUnik, objWIC.INDV_Mothers_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Nama Alias", "INDV_Alias" & strUnik, objWIC.INDV_Alias)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "NIK", "INDV_SSN" & strUnik, objWIC.INDV_SSN)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "No. Passport", "INDV_Passport_number" & strUnik, objWIC.INDV_Passport_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Negara Penerbit Passport", "INDV_Passport_country" & strUnik, objWIC.INDV_Passport_Country)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "No. Identitas Lain", "INDV_ID_Number" & strUnik, objWIC.INDV_ID_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Kewarganegaraan 1", "INDV_Nationality1" & strUnik, GlobalReportFunctionBLL.getCountryByCode(objWIC.INDV_Nationality1))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Kewarganegaraan 2", "INDV_Nationality2" & strUnik, GlobalReportFunctionBLL.getCountryByCode(objWIC.INDV_Nationality2))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Kewarganegaraan 3", "INDV_Nationality3" & strUnik, GlobalReportFunctionBLL.getCountryByCode(objWIC.INDV_Nationality3))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Negara Domisili", "INDV_residence" & strUnik, GlobalReportFunctionBLL.getCountryByCode(objWIC.INDV_Residence))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Email", "INDV_Email" & strUnik, objWIC.INDV_Email)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Pekerjaan", "INDV_Occupation" & strUnik, objWIC.INDV_Occupation)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Tempat Bekerja", "INDV_employer_name" & strUnik, objWIC.INDV_Employer_Name)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "ID", "PK_Customer_ID" & strUnik, objWIC.PK_Customer_ID)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Nama Korporasi", "Corp_Name" & strUnik, objWIC.Corp_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Nama Komersial", "Corp_Commercial_name" & strUnik, objWIC.Corp_Commercial_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Bentuk Korporasi", "Corp_Incorporation_legal_form" & strUnik, GlobalReportFunctionBLL.getBentukKorporasiByKode(objWIC.Corp_Incorporation_Legal_Form))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Nomor Induk Berusaha", "Corp_Incorporation_number" & strUnik, objWIC.Corp_Incorporation_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Bidang Usaha", "Corp_Business" & strUnik, objWIC.Corp_Business)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Email Korporasi", "Corp_Email" & strUnik, objWIC.Corp_Email)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Website Korporasi", "Corp_url" & strUnik, objWIC.Corp_Url)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Provinsi", "Corp_incorporation_state" & strUnik, objWIC.Corp_Incorporation_State)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Negara", "Corp_incorporation_country_code" & strUnik, GlobalReportFunctionBLL.getCountryByCode(objWIC.Corp_Incorporation_Country_Code))
                'direktur = GetBentukBadanUsaha(objWIC.Corp_Director_ID)
                'BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Pemilik/Pengurus/Orang yang diberikan Otorisasi Melakukan Transaksi", "Corp_director_id" & strUnik, direktur)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Peran", "Corp_Role" & strUnik, GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(objWIC.Corp_Role))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Tanggal Pendirian", "Corp_incorporation_date" & strUnik, objWIC.Corp_Incorporation_Date)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Tutup?", "Corp_business_closed" & strUnik, objWIC.Corp_Business_Closed)
                If objWIC.Corp_Business_Closed Then
                    BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Tanggal Tutup", "Corp_date_business_closed" & strUnik, objWIC.Corp_Date_Business_Closed)
                End If
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "NPWP", "Corp_tax_number" & strUnik, objWIC.Corp_Tax_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Catatan", "Corp_Comments" & strUnik, objWIC.Corp_Comments)
            End If


            '--------------------- Director -------------------
            Dim objStoreDirector As New Store
            objStoreDirector.ID = strUnik & "StoreGridDirector"
            objStoreDirector.ClientIDMode = ClientIDMode.Static

            Dim objModelDirector = New Model
            Dim objFieldDirector = New ModelField

            objFieldDirector = New ModelField
            objFieldDirector.Name = "NO_ID"
            objFieldDirector.Type = ModelFieldType.Auto
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "Role"
            objFieldDirector.Type = ModelFieldType.Int
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "FK_Entity_ID"
            objFieldDirector.Type = ModelFieldType.Int
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "Tph_Contact_Type"
            objFieldDirector.Type = ModelFieldType.String
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "Tph_Communication_Type"
            objFieldDirector.Type = ModelFieldType.String
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "tph_country_prefix"
            objFieldDirector.Type = ModelFieldType.String
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "tph_number"
            objFieldDirector.Type = ModelFieldType.String
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "tph_extension"
            objFieldDirector.Type = ModelFieldType.String
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "comments"
            objFieldDirector.Type = ModelFieldType.String
            objModelDirector.Fields.Add(objFieldDirector)

            objStoreDirector.Model.Add(objModelDirector)

            Dim objListColumnDirector As New List(Of ColumnBase)
            Using objColumnNo As New RowNumbererColumn
                objColumnNo.Text = "No."
                objColumnNo.ClientIDMode = ClientIDMode.Static
                objListColumnDirector.Add(objColumnNo)
            End Using

            Dim objColumnDirector As Column

            objColumnDirector = New Column
            objColumnDirector.Text = "Kategori Kontak"
            objColumnDirector.DataIndex = "Tph_Contact_Type"
            objColumnDirector.ClientIDMode = ClientIDMode.Static
            objColumnDirector.Flex = 1
            objListColumnDirector.Add(objColumnDirector)

            objColumnDirector = New Column
            objColumnDirector.Text = "Jenis Alat Komunikasi"
            objColumnDirector.DataIndex = "Tph_Communication_Type"
            objColumnDirector.ClientIDMode = ClientIDMode.Static
            objColumnDirector.Flex = 1
            objListColumnDirector.Add(objColumnDirector)

            objColumnDirector = New Column
            objColumnDirector.Text = "Nomor Telepon"
            objColumnDirector.DataIndex = "tph_number"
            objColumnDirector.ClientIDMode = ClientIDMode.Static
            objColumnDirector.Flex = 1
            objListColumnDirector.Add(objColumnDirector)

            Dim objParamDirector(0) As SqlParameter
            objParamDirector(0) = New SqlParameter
            objParamDirector(0).ParameterName = "@PK_Customer_ID"
            objParamDirector(0).SqlDbType = SqlDbType.BigInt
            objParamDirector(0).Value = objWIC.PK_Customer_ID

            Using objData As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GetPhoneDetailByWIC", objParamDirector)
                BLL.NawaFramework.ExtGridPanel(formPanelWICDelete, "Director", objStoreDirector, objListColumnDirector, objData)
            End Using


            '--------------------- Phone -------------------
            Dim objStorePhone As New Store
            objStorePhone.ID = strUnik & "StoreGridPhone"
            objStorePhone.ClientIDMode = ClientIDMode.Static

            Dim objModelPhone = New Model
            Dim objFieldPhone = New ModelField

            objFieldPhone = New ModelField
            objFieldPhone.Name = "PK_goAML_Ref_Phone"
            objFieldPhone.Type = ModelFieldType.Auto
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "FK_Ref_Detail_Of"
            objFieldPhone.Type = ModelFieldType.Int
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "FK_for_Table_ID"
            objFieldPhone.Type = ModelFieldType.Int
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "Tph_Contact_Type"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "Tph_Communication_Type"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "tph_country_prefix"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "tph_number"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "tph_extension"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "comments"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objStorePhone.Model.Add(objModelPhone)

            Dim objListColumnPhone As New List(Of ColumnBase)
            Using objColumnNo As New RowNumbererColumn
                objColumnNo.Text = "No."
                objColumnNo.ClientIDMode = ClientIDMode.Static
                objListColumnPhone.Add(objColumnNo)
            End Using

            Dim objColumnPhone As Column

            objColumnPhone = New Column
            objColumnPhone.Text = "Kategori Kontak"
            objColumnPhone.DataIndex = "Tph_Contact_Type"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            objColumnPhone = New Column
            objColumnPhone.Text = "Jenis Alat Komunikasi"
            objColumnPhone.DataIndex = "Tph_Communication_Type"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            objColumnPhone = New Column
            objColumnPhone.Text = "Nomor Telepon"
            objColumnPhone.DataIndex = "tph_number"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            Dim objParamPhone(0) As SqlParameter
            objParamPhone(0) = New SqlParameter
            objParamPhone(0).ParameterName = "@PK_Customer_ID"
            objParamPhone(0).SqlDbType = SqlDbType.BigInt
            objParamPhone(0).Value = objWIC.PK_Customer_ID

            Using objData As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GetPhoneDetailByWIC", objParamPhone)
                BLL.NawaFramework.ExtGridPanel(formPanelWICDelete, "Phone", objStorePhone, objListColumnPhone, objData)
            End Using



            '---------------- Address ----------------
            Dim objStoreAddress As New Store
            objStoreAddress.ID = strUnik & "StoreGridAddress"
            objStoreAddress.ClientIDMode = ClientIDMode.Static

            Dim objModelAddress As New Model
            Dim objFieldAddress As ModelField

            objFieldAddress = New ModelField
            objFieldAddress.Name = "PK_Customer_Address_ID"
            objFieldAddress.Type = ModelFieldType.Auto
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "FK_Ref_detail_of"
            objFieldAddress.Type = ModelFieldType.Int
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "FK_to_Table_ID"
            objFieldAddress.Type = ModelFieldType.Int
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Address_Type"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Address"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Town"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "City"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Zip"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Country_Code"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "State"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "comments"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objStoreAddress.Model.Add(objModelAddress)

            Dim objListColumnAddress As New List(Of ColumnBase)
            Using objColumnNo As New RowNumbererColumn
                objColumnNo.Text = "No."
                objColumnNo.ClientIDMode = ClientIDMode.Static
                objListColumnAddress.Add(objColumnNo)
            End Using

            Dim objColumnAddress As Column

            objColumnAddress = New Column
            objColumnAddress.Text = "Tipe Alamat"
            objColumnAddress.DataIndex = "Address_Type"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Alamat"
            objColumnAddress.DataIndex = "Address"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Kota/Kabupaten"
            objColumnAddress.DataIndex = "City"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Negara"
            objColumnAddress.DataIndex = "Country_Code"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            Dim objParamAddress(0) As SqlParameter
            objParamAddress(0) = New SqlParameter
            objParamAddress(0).ParameterName = "@PK_Customer_ID"
            objParamAddress(0).SqlDbType = SqlDbType.BigInt
            objParamAddress(0).Value = objWIC.PK_Customer_ID

            Using objData As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GetAddressDetailByWIC", objParamAddress)
                BLL.NawaFramework.ExtGridPanel(formPanelWICDelete, "Address", objStoreAddress, objListColumnAddress, objData)
            End Using

        End If
    End Sub
    Public Shared Function GetWICByID(strUnikKey As String) As goAML_Ref_WIC
        Using objDb As GoAMLEntities = New GoAMLEntities
            Return (From x In objDb.goAML_Ref_WIC Where x.PK_Customer_ID = strUnikKey Select x).FirstOrDefault
        End Using
    End Function
    Public Shared Function GetWICByWICNO(strUnikKey As String) As goAML_Ref_WIC
        Using objDb As GoAMLEntities = New GoAMLEntities
            Return (From x In objDb.goAML_Ref_WIC Where x.WIC_No = strUnikKey Select x).FirstOrDefault
        End Using
    End Function

    Function GetWICByIDD(strUnikKey As String) As goAML_Ref_WIC
        Using objDb As GoAMLEntities = New GoAMLEntities
            Return (From x In objDb.goAML_Ref_WIC
                    Join a In objDb.goAML_Ref_Nama_Negara On x.Corp_Incorporation_Country_Code Equals a.Kode
                    Where x.PK_Customer_ID = strUnikKey Select x).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetWICByPKID(ByVal id As Long) As goAML_Ref_WIC
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_WIC Where X.PK_Customer_ID = id Select X).FirstOrDefault
        End Using
    End Function
    Function GetWICByPKIDDetailAddress(id As Long) As List(Of goAML_Ref_Address)
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Address Where X.FK_To_Table_ID = id And X.FK_Ref_Detail_Of = 3 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailEmployerAddress(id As Long) As List(Of goAML_Ref_Address)
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Address Where X.FK_To_Table_ID = id And X.FK_Ref_Detail_Of = 10 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailPhone(id As Long) As List(Of goAML_Ref_Phone)
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Phone Where X.FK_for_Table_ID = id And X.FK_Ref_Detail_Of = 3 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailEmployerPhone(id As Long) As List(Of goAML_Ref_Phone)
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Phone Where X.FK_for_Table_ID = id And X.FK_Ref_Detail_Of = 10 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailDirector(id As Long) As List(Of goAML_Ref_Walk_In_Customer_Director)
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Walk_In_Customer_Director Where X.FK_Entity_ID = id Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailDirectorDetail(id As Long) As goAML_Ref_Walk_In_Customer_Director
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Walk_In_Customer_Director Where X.NO_ID = id Select X).FirstOrDefault
        End Using
    End Function
    Function GetWICByPKIDDetailDirectorPhone(id As Long) As List(Of goAML_Ref_Phone)
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Phone Where X.FK_for_Table_ID = id And X.FK_Ref_Detail_Of = 8 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailDirectorAddress(id As Long) As List(Of goAML_Ref_Address)
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Address Where X.FK_To_Table_ID = id And X.FK_Ref_Detail_Of = 8 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailDirectorPhoneEmployer(id As Long) As List(Of goAML_Ref_Phone)
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Phone Where X.FK_for_Table_ID = id And X.FK_Ref_Detail_Of = 4 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailDirectorAddressEmployer(id As Long) As List(Of goAML_Ref_Address)
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Address Where X.FK_To_Table_ID = id And X.FK_Ref_Detail_Of = 4 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailIdentification(id As Long) As List(Of goAML_Person_Identification)
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.goAML_Person_Identification Where X.FK_Person_ID = id And X.FK_Person_Type = 8 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailDirectorIdentification(id As Long) As List(Of goAML_Person_Identification)
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.goAML_Person_Identification Where X.FK_Person_ID = id And X.FK_Person_Type = 7 Select X).ToList
        End Using
    End Function
    Function IsExistInPendingApproval(id As Long, objModule As NawaDAL.[Module]) As Boolean
        Using NawaDataEntitiy As GoAMLEntities = New GoAMLEntities
            Return (From X In NawaDataEntitiy.ModuleApprovals Where X.ModuleKey = id And X.ModuleName = objModule.ModuleName).Count > 0
        End Using
    End Function

    Public Sub SaveEditTanpaApproval(objSave As goAML_Ref_WIC, listDirector As List(Of WICDirectorDataBLL), listAddressDetail As List(Of goAML_Ref_Address), listPhoneDetail As List(Of goAML_Ref_Phone), listAddressEmployerDetail As List(Of goAML_Ref_Address), listPhoneEmployerDetail As List(Of goAML_Ref_Phone), listIdentificationDetail As List(Of goAML_Person_Identification), objModule As NawaDAL.Module, wicData As WICDataBLL)
        Using objDB As New GoAMLEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    wicData.ObjWIC = objSave
                    objSave.ApprovedBy = Common.SessionCurrentUser.UserID
                    objSave.ApprovedDate = Now
                    objDB.Entry(objSave).State = EntityState.Modified

                    objDB.SaveChanges()
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim modulename As String = objModule.ModuleLabel
                    Dim ObjAuditTrailHeader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, objSave)
                    objDB.SaveChanges()


                    For Each objDirectors As WICDirectorDataBLL In listDirector
                        Dim itemx As New goAML_Ref_Walk_In_Customer_Director
                        itemx = (From x In objDB.goAML_Ref_Walk_In_Customer_Director Where x.FK_Entity_ID = objSave.PK_Customer_ID Select x).FirstOrDefault
                        Dim objDir As New WICDirectorDataBLL
                        If itemx IsNot Nothing Then
                            objDir = listDirector.Find(Function(x) x.ObjDirector.NO_ID = itemx.NO_ID)
                        End If
                        'agam 22102020
                        'Dim objcek As New goAML_Ref_Walk_In_Customer_Director
                        'objcek = objDir.ObjDirector
                        If objDir.ObjDirector Is Nothing And itemx IsNot Nothing Then
                            objDB.Entry(itemx).State = EntityState.Deleted
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                        End If

                        If itemx IsNot Nothing Then

                            For Each itemxx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = itemx.NO_ID And x.FK_Ref_Detail_Of = 8 Select x).ToList
                                Dim objcekk As goAML_Ref_Address = objDirectors.ListDirectorAddress.Find(Function(x) x.PK_Customer_Address_ID = itemxx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 8)
                                If objcekk Is Nothing Then
                                    objDB.Entry(itemxx).State = EntityState.Deleted
                                    'agam 20102020
                                    objDB.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemxx)
                                End If
                            Next

                            For Each itemxx As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = itemx.NO_ID And x.FK_Ref_Detail_Of = 8 Select x).ToList
                                Dim objcekk As goAML_Ref_Phone = objDirectors.ListDirectorPhone.Find(Function(x) x.PK_goAML_Ref_Phone = itemxx.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 8)
                                If objcekk Is Nothing Then
                                    objDB.Entry(itemxx).State = EntityState.Deleted
                                    'agam 20102020
                                    objDB.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemxx)
                                End If
                            Next

                            For Each itemxx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = itemx.NO_ID And x.FK_Ref_Detail_Of = 4 Select x).ToList
                                Dim objcekk As goAML_Ref_Address = objDirectors.ListDirectorAddressEmployer.Find(Function(x) x.PK_Customer_Address_ID = itemxx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 4)
                                If objcekk Is Nothing Then
                                    objDB.Entry(itemxx).State = EntityState.Deleted
                                    'agam 20102020
                                    objDB.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemxx)
                                End If
                            Next

                            For Each itemxx As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = itemx.NO_ID And x.FK_Ref_Detail_Of = 4 Select x).ToList
                                Dim objcekk As goAML_Ref_Phone = objDirectors.ListDirectorPhoneEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = itemxx.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 4)
                                If objcekk Is Nothing Then
                                    objDB.Entry(itemxx).State = EntityState.Deleted
                                    'agam 20102020
                                    objDB.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemxx)
                                End If
                            Next

                            For Each itemxx As goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = itemx.NO_ID And x.FK_Person_Type = 7 Select x).ToList
                                Dim objcekk As New goAML_Person_Identification
                                'objcekk = objDirectors.ListDirectorIdentification.Find(Function(x) x.PK_Person_Identification_ID = itemxx.PK_Person_Identification_ID)
                                objcekk = objDirectors.ListDirectorIdentification.Where(Function(x) x.PK_Person_Identification_ID = itemxx.PK_Person_Identification_ID).FirstOrDefault
                                If objcekk Is Nothing Then
                                    objDB.Entry(itemxx).State = EntityState.Deleted
                                    'agam 20102020
                                    objDB.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemxx)
                                End If
                            Next

                        End If

                    Next

                    For Each objDirectors As WICDirectorDataBLL In listDirector
                        Dim item As goAML_Ref_Walk_In_Customer_Director = objDirectors.ObjDirector
                        Dim obcek As goAML_Ref_Walk_In_Customer_Director = (From x In objDB.goAML_Ref_Walk_In_Customer_Director Where x.NO_ID = item.NO_ID Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            With item
                                .FK_Entity_ID = objSave.PK_Customer_ID
                                .CreatedBy = Common.SessionCurrentUser.UserID
                                .CreatedDate = DateTime.Now
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                            End With
                            objDB.Entry(item).State = EntityState.Added
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                            objDB.SaveChanges()
                        Else
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = EntityState.Modified
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        End If

                        For Each itemDetail As goAML_Ref_Address In objDirectors.ListDirectorAddress
                            Dim obcekDetail As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemDetail.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 8 And x.FK_To_Table_ID = item.NO_ID Select x).FirstOrDefault
                            If obcekDetail Is Nothing Then
                                'agam 22102020
                                ' With item
                                With itemDetail
                                    .FK_To_Table_ID = item.NO_ID
                                    .CreatedBy = Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                End With
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                                objDB.Entry(itemDetail).State = EntityState.Added
                            Else
                                'agam 22102020
                                ' With item
                                With obcekDetail
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                End With
                                objDB.Entry(obcekDetail).CurrentValues.SetValues(itemDetail)
                                objDB.Entry(obcekDetail).State = EntityState.Modified
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            End If
                        Next

                        For Each itemDetail As goAML_Ref_Address In objDirectors.ListDirectorAddressEmployer
                            Dim obcekDetail As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemDetail.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 4 And x.FK_To_Table_ID = item.NO_ID Select x).FirstOrDefault
                            If obcekDetail Is Nothing Then
                                'agam 22102020
                                ' With item
                                With itemDetail
                                    .FK_To_Table_ID = item.NO_ID
                                    .CreatedBy = Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                End With
                                objDB.Entry(itemDetail).State = EntityState.Added
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            Else
                                With itemDetail
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                End With
                                objDB.Entry(obcekDetail).CurrentValues.SetValues(itemDetail)
                                objDB.Entry(obcekDetail).State = EntityState.Modified
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            End If
                        Next

                        For Each itemDetail As goAML_Ref_Phone In objDirectors.ListDirectorPhone
                            Dim obcekDetail As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemDetail.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 8 And x.FK_for_Table_ID = item.NO_ID Select x).FirstOrDefault
                            If obcekDetail Is Nothing Then
                                'agam 22102020
                                ' With item
                                With itemDetail
                                    .FK_for_Table_ID = item.NO_ID
                                    .CreatedBy = Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                End With
                                objDB.Entry(itemDetail).State = EntityState.Added
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            Else
                                With itemDetail
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                End With
                                objDB.Entry(obcekDetail).CurrentValues.SetValues(itemDetail)
                                objDB.Entry(obcekDetail).State = EntityState.Modified
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            End If
                        Next

                        For Each itemDetail As goAML_Ref_Phone In objDirectors.ListDirectorPhoneEmployer
                            Dim obcekDetail As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemDetail.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 4 And x.FK_for_Table_ID = item.NO_ID Select x).FirstOrDefault
                            If obcekDetail Is Nothing Then
                                'agam 22102020
                                ' With item
                                With itemDetail
                                    .FK_for_Table_ID = item.NO_ID
                                    .CreatedBy = Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                End With
                                objDB.Entry(itemDetail).State = EntityState.Added
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            Else
                                With itemDetail
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                End With
                                objDB.Entry(obcekDetail).CurrentValues.SetValues(itemDetail)
                                objDB.Entry(obcekDetail).State = EntityState.Modified
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            End If
                        Next

                        For Each itemDetail As goAML_Person_Identification In objDirectors.ListDirectorIdentification
                            Dim obcekDetail As goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = itemDetail.PK_Person_Identification_ID And x.FK_Person_Type = 7 And x.FK_Person_ID = item.NO_ID Select x).FirstOrDefault
                            If obcekDetail Is Nothing Then
                                'agam 22102020
                                ' With item
                                With itemDetail
                                    .FK_Person_ID = item.NO_ID
                                    .CreatedBy = Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                End With
                                objDB.Entry(itemDetail).State = EntityState.Added
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            Else
                                With itemDetail
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                End With
                                objDB.Entry(obcekDetail).CurrentValues.SetValues(itemDetail)
                                objDB.Entry(obcekDetail).State = EntityState.Modified
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            End If
                        Next

                    Next



                    For Each itemx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 3 Select x).ToList
                        Dim objcek As goAML_Ref_Address = listAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 3)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = EntityState.Deleted
                            'agam 20102020

                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                        End If
                    Next
                    For Each item As goAML_Ref_Address In listAddressDetail
                        ' Dim obcek As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 3 Select x).FirstOrDefault
                        Dim obcek As goAML_Ref_Address = objDB.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                        If obcek Is Nothing Then
                            objDB.Entry(item).State = EntityState.Added
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        Else
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = EntityState.Modified
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        End If
                    Next

                    For Each itemx As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 3 Select x).ToList
                        Dim objcek As goAML_Ref_Phone = listPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 3)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = EntityState.Deleted
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                        End If
                    Next
                    For Each item As goAML_Ref_Phone In listPhoneDetail
                        Dim obcek As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 3 Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            objDB.Entry(item).State = EntityState.Added
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        Else
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = EntityState.Modified
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        End If
                    Next

                    For Each itemx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 10 Select x).ToList
                        Dim objcek As goAML_Ref_Address = listAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 10)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = EntityState.Deleted
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                        End If
                    Next
                    For Each item As goAML_Ref_Address In listAddressEmployerDetail
                        Dim obcek As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 10 Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            objDB.Entry(item).State = EntityState.Added
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        Else
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = EntityState.Modified
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        End If
                    Next

                    For Each itemx As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 10 Select x).ToList
                        Dim objcek As goAML_Ref_Phone = listPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 10)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = EntityState.Deleted
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                        End If
                    Next
                    For Each item As goAML_Ref_Phone In listPhoneEmployerDetail
                        Dim obcek As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 10 Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            objDB.Entry(item).State = EntityState.Added
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        Else
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = EntityState.Modified
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        End If
                    Next

                    For Each itemx As goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = objSave.PK_Customer_ID And x.FK_Person_Type = 8 Select x).ToList
                        Dim objcek As goAML_Person_Identification = listIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID And x.FK_Person_Type = 8)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = EntityState.Deleted
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                        End If
                    Next

                    For Each item As goAML_Person_Identification In listIdentificationDetail
                        Dim obcek As goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = item.PK_Person_Identification_ID And x.FK_Person_Type = 8 Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            objDB.Entry(item).State = EntityState.Added
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        Else
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = EntityState.Modified
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        End If
                    Next

                    objDB.SaveChanges()
                    objTrans.Commit()

                    Delete_Grid(wicData.ObjWIC2.WIC_No, wicData.ObjGrid)
                    SaveGoAmlWic501(wicData)
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
    Public Sub SaveEditApproval(objSave As goAML_Ref_WIC, listDirector As List(Of WICDirectorDataBLL), listAddressDetail As List(Of goAML_Ref_Address), listPhoneDetail As List(Of goAML_Ref_Phone), listAddressEmployerDetail As List(Of goAML_Ref_Address), listPhoneEmployerDetail As List(Of goAML_Ref_Phone), listIdentificationDetail As List(Of goAML_Person_Identification), objModule As NawaDAL.Module, wicData As WICDataBLL)
        Using objDB As New GoAMLEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objData As New WICDataBLL With
                    {
                        .ObjWIC = objSave,
                        .ObjWICAddress = listAddressDetail,
                        .ObjWICPhone = listPhoneDetail,
                        .ObjWICAddressEmployer = listAddressEmployerDetail,
                        .OBjWICPhoneEmployer = listPhoneEmployerDetail,
                        .ObjWICIdentification = listIdentificationDetail,
                        .ObjDirector = listDirector,
                        .ObjWIC2 = wicData.ObjWIC2,
                        .ObjGrid = wicData.ObjGrid
                    }
                    Dim dataXML As String = Common.Serialize(objData)

                    Dim objWICBefore As goAML_Ref_WIC = objDB.goAML_Ref_WIC.Where(Function(x) x.PK_Customer_ID = objSave.PK_Customer_ID).FirstOrDefault
                    Dim objWICAddressBefore As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 3).ToList
                    Dim objWICPhoneBefore As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 3).ToList
                    Dim objWICAddressEmployerBefore As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 10).ToList
                    Dim objWICPhoneEmployerBefore As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 10).ToList
                    Dim objWICIdentificationBefore As List(Of goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = objSave.PK_Customer_ID And x.FK_Person_Type = 8).ToList
                    Dim objWICDataBefore = GetWICData(objSave.WIC_No)

                    Dim objDirectorClass As New List(Of WICDirectorDataBLL)
                    Dim objWICDirectorBefore As New goAML_Ref_Walk_In_Customer_Director
                    Dim objWICDirectorAddressBefore As List(Of goAML_Ref_Address) = New List(Of goAML_Ref_Address)
                    Dim objWICDirectorPhoneBefore As New List(Of goAML_Ref_Phone)
                    Dim objWICDirectorEmployerAddressBefore As List(Of goAML_Ref_Address) = New List(Of goAML_Ref_Address)
                    Dim objWICDirectorEmployerPhoneBefore As List(Of goAML_Ref_Phone) = New List(Of goAML_Ref_Phone)
                    Dim objWICDirectorEmployerIdentificationBefore As List(Of goAML_Person_Identification) = New List(Of goAML_Person_Identification)
                    Dim objWICDirectorEmailBefore As List(Of DataModel.goAML_Ref_Customer_Email) = New List(Of DataModel.goAML_Ref_Customer_Email)
                    Dim objWICDirectorSanctionBefore As List(Of DataModel.goAML_Ref_Customer_Sanction) = New List(Of DataModel.goAML_Ref_Customer_Sanction)
                    Dim objWICDirectorPEPBefore As List(Of DataModel.goAML_Ref_Customer_PEP) = New List(Of DataModel.goAML_Ref_Customer_PEP)

                    If listDirector.Count > 0 Then

                        ' START: 2023-10-18, Nael
                        Dim tempDirector = objDB.goAML_Ref_Walk_In_Customer_Director.Where(Function(x) x.FK_Entity_ID = objSave.PK_Customer_ID).ToList()
                        If tempDirector IsNot Nothing AndAlso tempDirector.Count > 0 Then
                            For Each directorClass In tempDirector
                                objWICDirectorBefore = directorClass
                                objWICDirectorAddressBefore = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = directorClass.NO_ID And x.FK_Ref_Detail_Of = 8).ToList
                                objWICDirectorPhoneBefore = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = directorClass.NO_ID And x.FK_Ref_Detail_Of = 8).ToList
                                objWICDirectorEmployerAddressBefore = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = directorClass.NO_ID And x.FK_Ref_Detail_Of = 4).ToList
                                objWICDirectorEmployerPhoneBefore = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = directorClass.NO_ID And x.FK_Ref_Detail_Of = 4).ToList
                                objWICDirectorEmployerIdentificationBefore = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = directorClass.NO_ID And x.FK_Person_Type = 7).ToList
                                objWICDirectorEmailBefore = goAML_Customer_Service.GetEmailByPkFk(directorClass.NO_ID, RefDetail.WIC_DIRECTOR)
                                objWICDirectorSanctionBefore = goAML_Customer_Service.GetSanctionByPkFk(directorClass.NO_ID, RefDetail.WIC_DIRECTOR)
                                objWICDirectorPEPBefore = goAML_Customer_Service.GetPEPByPkFk(directorClass.NO_ID, RefDetail.WIC_DIRECTOR)

                                ' 2023-10-19, Nael
                                Dim Director As New WICDirectorDataBLL() With {
                                    .ObjDirector = objWICDirectorBefore,
                                    .ListDirectorAddress = objWICDirectorAddressBefore,
                                    .ListDirectorPhone = objWICDirectorPhoneBefore,
                                    .ListDirectorAddressEmployer = objWICDirectorEmployerAddressBefore,
                                    .ListDirectorPhoneEmployer = objWICDirectorEmployerPhoneBefore,
                                    .ListDirectorIdentification = objWICDirectorEmployerIdentificationBefore,
                                    .ListDirectorEmail = objWICDirectorEmailBefore,
                                    .ListDirectorSanction = objWICDirectorSanctionBefore,
                                    .ListDirectorPEP = objWICDirectorPEPBefore
                                }
                                objDirectorClass.Add(Director)
                            Next
                        End If
                        ' END: 2023-10-18, Nael
                    End If

                    Dim objDataBefore As New WICDataBLL With
                    {
                        .ObjWIC = objWICBefore,
                        .ObjWICAddress = objWICAddressBefore,
                        .ObjWICPhone = objWICPhoneBefore,
                        .ObjWICAddressEmployer = objWICAddressEmployerBefore,
                        .OBjWICPhoneEmployer = objWICPhoneEmployerBefore,
                        .ObjDirector = objDirectorClass,
                        .ObjWICIdentification = objWICIdentificationBefore,
                        .ObjWIC2 = objWICDataBefore.ObjWIC2,
                        .ObjGrid = objWICDataBefore.ObjGrid
                    }
                    Dim dataXMLBefore As String = Common.Serialize(objDataBefore)

                    Dim objModuleApproval As New GoAMLDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = objSave.PK_Customer_ID
                        .ModuleField = dataXML
                        .ModuleFieldBefore = dataXMLBefore
                        .PK_ModuleAction_ID = Common.ModuleActionEnum.Update
                        .CreatedBy = Common.SessionCurrentUser.UserID
                        .CreatedDate = Now
                    End With

                    objDB.Entry(objModuleApproval).State = EntityState.Added

                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim modulename As String = objModule.ModuleLabel
                    Dim ObjAuditTrailHeader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, objSave, objWICBefore)
                    objDB.SaveChanges()

                    If listDirector.Count > 0 Then

                        For Each itemDetail As WICDirectorDataBLL In listDirector
                            Dim item As goAML_Ref_Walk_In_Customer_Director = itemDetail.ObjDirector
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, objWICDirectorBefore)

                            If itemDetail.ListDirectorAddress.Count > 0 Then
                                For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddress
                                    Dim items As goAML_Ref_Address = objDB.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = itemD.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 8 And x.FK_To_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                                Next
                            End If

                            If itemDetail.ListDirectorAddressEmployer.Count > 0 Then
                                For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddressEmployer
                                    Dim items As goAML_Ref_Address = objDB.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = itemD.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 4 And x.FK_To_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                                Next
                            End If

                            If itemDetail.ListDirectorPhone.Count > 0 Then
                                For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhone
                                    Dim items As goAML_Ref_Phone = objDB.goAML_Ref_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = itemD.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 8 And x.FK_for_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                                Next
                            End If

                            If itemDetail.ListDirectorPhoneEmployer.Count > 0 Then
                                For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhoneEmployer
                                    Dim items As goAML_Ref_Phone = objDB.goAML_Ref_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = itemD.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 4 And x.FK_for_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                                Next
                            End If

                            If itemDetail.ListDirectorIdentification.Count > 0 Then
                                For Each itemD As goAML_Person_Identification In itemDetail.ListDirectorIdentification
                                    Dim items As goAML_Person_Identification = objDB.goAML_Person_Identification.Where(Function(x) x.PK_Person_Identification_ID = itemD.PK_Person_Identification_ID And x.FK_Person_Type = 7 And x.FK_Person_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                                Next
                            End If
                        Next

                    End If

                    If listAddressDetail.Count > 0 Then
                        For Each itemD As goAML_Ref_Address In listAddressDetail
                            Dim items As goAML_Ref_Address = objDB.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = itemD.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 3 And x.FK_To_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                        Next
                    End If

                    If listPhoneDetail.Count > 0 Then
                        For Each itemD As goAML_Ref_Phone In listPhoneDetail
                            Dim items As goAML_Ref_Phone = objDB.goAML_Ref_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = itemD.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 3 And x.FK_for_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                        Next
                    End If

                    If listAddressEmployerDetail.Count > 0 Then
                        For Each itemD As goAML_Ref_Address In listAddressEmployerDetail
                            Dim items As goAML_Ref_Address = objDB.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = itemD.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 10 And x.FK_To_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                        Next
                    End If

                    If listPhoneEmployerDetail.Count > 0 Then
                        For Each itemD As goAML_Ref_Phone In listPhoneEmployerDetail
                            Dim items As goAML_Ref_Phone = objDB.goAML_Ref_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = itemD.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 10 And x.FK_for_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                        Next
                    End If

                    If listIdentificationDetail.Count > 0 Then
                        For Each itemD As goAML_Person_Identification In listIdentificationDetail
                            Dim items As goAML_Person_Identification = objDB.goAML_Person_Identification.Where(Function(x) x.PK_Person_Identification_ID = itemD.PK_Person_Identification_ID And x.FK_Person_Type = 8 And x.FK_Person_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                        Next
                    End If

                    objDB.SaveChanges()
                    objTrans.Commit()

                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
    Sub DeleteTanpaapproval(ID As String, objModule As NawaDAL.Module)
        Using objDB As New GoAMLEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objWIC As goAML_Ref_WIC = objDB.goAML_Ref_WIC.Where(Function(x) x.PK_Customer_ID = ID).FirstOrDefault
                    Dim listWICAddress As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 3).ToList
                    Dim listWICPhone As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 3).ToList
                    Dim listWICAddressEmployer As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 10).ToList
                    Dim listWICPhoneEmployer As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 10).ToList
                    Dim listWICIdentification As List(Of goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = ID And x.FK_Person_Type = 8).ToList
                    Dim listWICDirector As List(Of goAML_Ref_Walk_In_Customer_Director) = objDB.goAML_Ref_Walk_In_Customer_Director.Where(Function(x) x.FK_Entity_ID = ID).ToList

                    Dim objDirectorClass As New List(Of WICDirectorDataBLL)
                    Dim objWICDirector As goAML_Ref_Walk_In_Customer_Director = New goAML_Ref_Walk_In_Customer_Director
                    Dim listWICDirectorAddress As List(Of goAML_Ref_Address) = New List(Of goAML_Ref_Address)
                    Dim listWICDirectorPhone As List(Of goAML_Ref_Phone) = New List(Of goAML_Ref_Phone)
                    Dim listWICDirectorEmployerAddress As List(Of goAML_Ref_Address) = New List(Of goAML_Ref_Address)
                    Dim listWICDirectorEmployerPhone As List(Of goAML_Ref_Phone) = New List(Of goAML_Ref_Phone)
                    Dim listWICDirectorIdentification As List(Of goAML_Person_Identification) = New List(Of goAML_Person_Identification)

                    If listWICDirector.Count > 0 Then

                        Dim DirectorClass As New WICDirectorDataBLL
                        For Each director In listWICDirector

                            listWICDirectorAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = director.NO_ID And x.FK_Ref_Detail_Of = 8).ToList
                            listWICDirectorEmployerAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = director.NO_ID And x.FK_Ref_Detail_Of = 4).ToList
                            listWICDirectorPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = director.NO_ID And x.FK_Ref_Detail_Of = 8).ToList
                            listWICDirectorEmployerPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = director.NO_ID And x.FK_Ref_Detail_Of = 4).ToList
                            listWICDirectorIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = director.NO_ID And x.FK_Person_Type = 7).ToList

                            With DirectorClass
                                .ObjDirector = director
                                .ListDirectorAddress = listWICDirectorAddress
                                .ListDirectorPhone = listWICDirectorPhone
                                .ListDirectorAddressEmployer = listWICDirectorEmployerAddress
                                .ListDirectorPhoneEmployer = listWICDirectorEmployerPhone
                                .ListDirectorIdentification = listWICDirectorIdentification
                            End With
                            objDirectorClass.Add(DirectorClass)
                        Next

                    End If
                    objDB.Entry(objWIC).State = EntityState.Deleted

                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim modulename As String = objModule.ModuleLabel
                    Dim ObjAuditTrailHeader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, objWIC)

                    If objDirectorClass.Count > 0 Then

                        For Each objDir As WICDirectorDataBLL In objDirectorClass
                            Dim item As goAML_Ref_Walk_In_Customer_Director = objDir.ObjDirector
                            objDB.Entry(item).State = EntityState.Deleted
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)

                            If objDir.ListDirectorAddress.Count > 0 Then
                                For Each itemx As goAML_Ref_Address In objDir.ListDirectorAddress
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorAddressEmployer.Count > 0 Then
                                For Each itemx As goAML_Ref_Address In objDir.ListDirectorAddressEmployer
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorPhone.Count > 0 Then
                                For Each itemx As goAML_Ref_Phone In objDir.ListDirectorPhone
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorPhoneEmployer.Count > 0 Then
                                For Each itemx As goAML_Ref_Phone In objDir.ListDirectorPhoneEmployer
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorIdentification.Count > 0 Then
                                For Each itemx As goAML_Person_Identification In objDir.ListDirectorIdentification
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                        Next
                    End If

                    If listWICAddress.Count > 0 Then
                        For Each item As goAML_Ref_Address In listWICAddress
                            objDB.Entry(item).State = EntityState.Deleted
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICPhone.Count > 0 Then
                        For Each item As goAML_Ref_Phone In listWICPhone
                            objDB.Entry(item).State = EntityState.Deleted
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICAddressEmployer.Count > 0 Then
                        For Each item As goAML_Ref_Address In listWICAddressEmployer
                            objDB.Entry(item).State = EntityState.Deleted
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICPhoneEmployer.Count > 0 Then
                        For Each item As goAML_Ref_Phone In listWICPhoneEmployer
                            objDB.Entry(item).State = EntityState.Deleted
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICIdentification.Count > 0 Then
                        For Each item As goAML_Person_Identification In listWICIdentification
                            objDB.Entry(item).State = EntityState.Deleted
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    objDB.SaveChanges()
                    objTrans.Commit()

                    Delete_Grid(objWIC.WIC_No, Nothing, True)
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
    Sub DeleteDenganapproval(ID As String, objModule As NawaDAL.Module)
        Using objDB As New GoAMLEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objWIC As goAML_Ref_WIC = objDB.goAML_Ref_WIC.Where(Function(x) x.PK_Customer_ID = ID).FirstOrDefault
                    Dim listWICDirector As List(Of goAML_Ref_Walk_In_Customer_Director) = objDB.goAML_Ref_Walk_In_Customer_Director.Where(Function(x) x.FK_Entity_ID = ID).ToList
                    Dim listWICAddress As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 3).ToList
                    Dim listWICPhone As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 3).ToList
                    Dim listWICAddressEmployer As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 10).ToList
                    Dim listWICPhoneEmployer As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 10).ToList
                    Dim listWICIdentification As List(Of goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = ID And x.FK_Person_Type = 8).ToList

                    Dim objDirectorClass As New List(Of WICDirectorDataBLL)
                    Dim objWICDirector As goAML_Ref_Walk_In_Customer_Director = New goAML_Ref_Walk_In_Customer_Director
                    Dim listWICAddressDirector As List(Of goAML_Ref_Address) = New List(Of goAML_Ref_Address)
                    Dim listWICPhoneDirector As List(Of goAML_Ref_Phone) = New List(Of goAML_Ref_Phone)
                    Dim listWICAddressDirectorEmployer As List(Of goAML_Ref_Address) = New List(Of goAML_Ref_Address)
                    Dim listWICPhoneDirectorEmployer As List(Of goAML_Ref_Phone) = New List(Of goAML_Ref_Phone)
                    Dim listWICIdentificationDirector As List(Of goAML_Person_Identification) = New List(Of goAML_Person_Identification)

                    If listWICDirector.Count > 0 Then

                        Dim DirectorClass As New WICDirectorDataBLL
                        For Each Director In listWICDirector

                            listWICAddressDirector = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = Director.NO_ID And x.FK_Ref_Detail_Of = 8).ToList
                            listWICPhoneDirector = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = Director.NO_ID And x.FK_Ref_Detail_Of = 8).ToList
                            listWICAddressDirectorEmployer = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = Director.NO_ID And x.FK_Ref_Detail_Of = 4).ToList
                            listWICPhoneDirectorEmployer = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = Director.NO_ID And x.FK_Ref_Detail_Of = 4).ToList
                            listWICIdentificationDirector = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = Director.NO_ID And x.FK_Person_Type = 7).ToList

                            With DirectorClass
                                .ObjDirector = Director
                                .ListDirectorAddress = listWICAddressDirector
                                .ListDirectorPhone = listWICPhoneDirector
                                .ListDirectorAddressEmployer = listWICAddressDirectorEmployer
                                .ListDirectorPhoneEmployer = listWICPhoneDirectorEmployer
                                .ListDirectorIdentification = listWICIdentificationDirector
                            End With
                            objDirectorClass.Add(DirectorClass)

                        Next

                    End If
                    Dim objWICDataBLL As New WICDataBLL With
                    {
                        .ObjWIC = objWIC,
                        .ObjWICAddress = listWICAddress,
                        .ObjWICPhone = listWICPhone,
                        .ObjWICAddressEmployer = listWICAddressEmployer,
                        .OBjWICPhoneEmployer = listWICPhoneEmployer,
                        .ObjWICIdentification = listWICIdentification,
                        .ObjDirector = objDirectorClass
                    }

                    Dim dataXML As String = Common.Serialize(objWICDataBLL)
                    Dim objModuleApproval As New GoAMLDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = ID
                        .ModuleField = dataXML
                        .ModuleFieldBefore = ""
                        .PK_ModuleAction_ID = Common.ModuleActionEnum.Delete
                        .CreatedDate = Now
                        .CreatedBy = Common.SessionCurrentUser.UserID
                    End With

                    objDB.Entry(objModuleApproval).State = EntityState.Added

                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim modulename As String = objModule.ModuleLabel
                    Dim ObjAuditTrailHeader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, objWIC)

                    If objDirectorClass.Count > 0 Then

                        For Each objDir As WICDirectorDataBLL In objDirectorClass
                            Dim item As goAML_Ref_Walk_In_Customer_Director = objDir.ObjDirector
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)

                            If objDir.ListDirectorAddress.Count > 0 Then
                                For Each itemx As goAML_Ref_Address In objDir.ListDirectorAddress
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorAddressEmployer.Count > 0 Then
                                For Each itemx As goAML_Ref_Address In objDir.ListDirectorAddressEmployer
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorPhone.Count > 0 Then
                                For Each itemx As goAML_Ref_Phone In objDir.ListDirectorPhone
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorPhoneEmployer.Count > 0 Then
                                For Each itemx As goAML_Ref_Phone In objDir.ListDirectorPhoneEmployer
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorIdentification.Count > 0 Then
                                For Each itemx As goAML_Person_Identification In objDir.ListDirectorIdentification
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                        Next
                    End If

                    If listWICAddress.Count > 0 Then
                        For Each item As goAML_Ref_Address In listWICAddress
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICPhone.Count > 0 Then
                        For Each item As goAML_Ref_Phone In listWICPhone
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICAddressEmployer.Count > 0 Then
                        For Each item As goAML_Ref_Address In listWICAddressEmployer
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICPhoneEmployer.Count > 0 Then
                        For Each item As goAML_Ref_Phone In listWICPhoneEmployer
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICIdentification.Count > 0 Then
                        For Each item As goAML_Person_Identification In listWICIdentification
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    objDB.SaveChanges()
                    objTrans.Commit()

                    'Delete_Grid(objWIC.WIC_No, Nothing, True)
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub



#Region "Go AML 5.0.1 - WIC"
    'add by Septian, goAML WIC 5.0.1
    Sub Delete_Grid(wicNo As String, gridWIC As WICDataBLL.goAML_Grid_WIC, Optional isDeleteAll As Boolean = False)
        Dim strQuery As String = "BEGIN TRAN "
        Try
            'strQuery += $"DELETE FROM goAML_Ref_WIC_Social_Media WHERE WIC_No = '{WIC_No}';"
            'strQuery += $"DELETE FROM goAML_Ref_WIC_Previous_Name WHERE WIC_No = '{WIC_No}';"
            'strQuery += $"DELETE FROM goAML_Ref_WIC_Email WHERE WIC_No = '{WIC_No}';"
            'strQuery += $"DELETE FROM goAML_Ref_WIC_Employment_History WHERE WIC_No = '{WIC_No}';"
            'strQuery += $"DELETE FROM goAML_Ref_WIC_Person_PEP WHERE WIC_No = '{WIC_No}';"
            'strQuery += $"DELETE FROM goAML_Ref_WIC_Network_Devices WHERE WIC_No = '{WIC_No}';"
            'strQuery += $"DELETE FROM goAML_Ref_WIC_Sanction WHERE WIC_No = '{WIC_No}';"
            'strQuery += $"DELETE FROM goAML_Ref_WIC_Related_Person WHERE WIC_No = '{WIC_No}';"
            'strQuery += $"DELETE FROM goAML_Ref_WIC_Additional_Information WHERE WIC_No = '{WIC_No}';"

            strQuery = strQuery & GetQuery_DeleteGrid_Email(wicNo, If(gridWIC Is Nothing, Nothing, gridWIC.ObjList_GoAML_Email), isDeleteAll) & " "
            strQuery = strQuery & GetQuery_DeleteGrid_PersonPEP(wicNo, If(gridWIC Is Nothing, Nothing, gridWIC.ObjList_GoAML_PersonPEP), isDeleteAll) & " "
            strQuery = strQuery & GetQuery_DeleteGrid_Sanction(wicNo, If(gridWIC Is Nothing, Nothing, gridWIC.ObjList_GoAML_Sanction), isDeleteAll) & " "
            strQuery = strQuery & GetQuery_DeleteGrid_RelatedPerson(wicNo, If(gridWIC Is Nothing, Nothing, gridWIC.ObjList_GoAML_RelatedPerson), isDeleteAll) & " "
            strQuery = strQuery & GetQuery_DeleteGrid_URL(wicNo, If(gridWIC Is Nothing, Nothing, gridWIC.ObjList_GoAML_URL), isDeleteAll) & " "
            strQuery = strQuery & GetQuery_DeleteGrid_EntityIdentification(wicNo, If(gridWIC Is Nothing, Nothing, gridWIC.ObjList_GoAML_EntityIdentification), isDeleteAll) & " "
            strQuery = strQuery & GetQuery_DeleteGrid_RelatedEntity(wicNo, If(gridWIC Is Nothing, Nothing, gridWIC.ObjList_GoAML_RelatedEntity), isDeleteAll) & " "

            strQuery += $"COMMIT TRAN "
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        Catch ex As Exception
            strQuery += $"ROLLBACK TRAN"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        End Try
    End Sub
    'Edit Ripan 03-August-2023 ( ambigues objectname, jadi dikasih initial object di depannya )
    'Function GetQuery_DeleteGrid_Email(wicNo As String, newGrid As List(goAML_Ref_WIC_Email), Optional isDeleteAll As Boolean = False) As String
    Function GetQuery_DeleteGrid_Email(wicNo As String, newGrid As List(Of WICDataBLL.goAML_Ref_WIC_Email), Optional isDeleteAll As Boolean = False) As String
        Dim strQuery = ""

        If isDeleteAll Then
            strQuery = $"DELETE FROM goAML_Ref_WIC_Email WHERE WIC_No = {wicNo} AND FK_REF_DETAIL_OF = 3;"
            Return strQuery
        End If

        Dim oldGrid = GetEmail(wicNo, 3)
        For Each oldItemGrid In oldGrid
            Dim checkItemNew = newGrid.Find(Function(x) x.PK_goAML_Ref_WIC_Email_ID = oldItemGrid.PK_goAML_Ref_WIC_Email_ID)

            If checkItemNew Is Nothing Then
                strQuery = strQuery & $"DELETE FROM goAML_Ref_WIC_Email WHERE PK_goAML_Ref_WIC_Email_ID = {oldItemGrid.PK_goAML_Ref_WIC_Email_ID};"
            End If
        Next

        Return strQuery
    End Function
    Function GetQuery_DeleteGrid_PersonPEP(wicNo As String, newGrid As List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP), Optional isDeleteAll As Boolean = False) As String
        Dim strQuery = ""

        If isDeleteAll Then
            strQuery = $"DELETE FROM goAML_Ref_WIC_Person_PEP WHERE WIC_No = {wicNo};"
            Return strQuery
        End If

        Dim oldGrid = GetPEP(wicNo)
        For Each oldItemGrid In oldGrid
            Dim checkItemNew = newGrid.Find(Function(x) x.PK_goAML_Ref_WIC_PEP_ID = oldItemGrid.PK_goAML_Ref_WIC_PEP_ID)

            If checkItemNew Is Nothing Then
                strQuery = strQuery & $"DELETE FROM goAML_Ref_WIC_Person_PEP WHERE PK_goAML_Ref_WIC_PEP_ID = {oldItemGrid.PK_goAML_Ref_WIC_PEP_ID};"
            End If
        Next

        Return strQuery
    End Function
    'Edit Ripan 03-August-2023 ( ambigues objectname, jadi dikasih initial object di depannya )
    'Function GetQuery_DeleteGrid_Sanction(wicNo As String, newGrid As List(Of goAML_Ref_WIC_Sanction), Optional isDeleteAll As Boolean = False) As String
    Function GetQuery_DeleteGrid_Sanction(wicNo As String, newGrid As List(Of WICDataBLL.goAML_Ref_WIC_Sanction), Optional isDeleteAll As Boolean = False) As String
        Dim strQuery = ""

        If isDeleteAll Then
            strQuery = $"DELETE FROM goAML_Ref_WIC_Sanction WHERE WIC_No = {wicNo};"
            Return strQuery
        End If

        Dim oldGrid = GetSanction(wicNo)
        For Each oldItemGrid In oldGrid
            Dim checkItemNew = newGrid.Find(Function(x) x.PK_goAML_Ref_WIC_Sanction_ID = oldItemGrid.PK_goAML_Ref_WIC_Sanction_ID)

            If checkItemNew Is Nothing Then
                strQuery = strQuery & $"DELETE FROM goAML_Ref_WIC_Sanction WHERE PK_goAML_Ref_WIC_Sanction_ID = {oldItemGrid.PK_goAML_Ref_WIC_Sanction_ID};"
            End If
        Next
        Return strQuery
    End Function
    Function GetQuery_DeleteGrid_RelatedPerson(wicNo As String, newGrid As List(Of WICDataBLL.goAML_Ref_WIC_Related_Person), Optional isDeleteAll As Boolean = False) As String
        Dim strQuery = ""

        If isDeleteAll Then
            strQuery = $"DELETE FROM goAML_Ref_WIC_Related_Person WHERE WIC_No = {wicNo};"
            Return strQuery
        End If

        Dim oldGrid = GetRelatedPerson(wicNo)
        For Each oldItemGrid In oldGrid
            Dim checkItemNew = newGrid.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Person_ID = oldItemGrid.PK_goAML_Ref_WIC_Related_Person_ID)

            If checkItemNew Is Nothing Then
                strQuery = strQuery & $"DELETE FROM goAML_Ref_WIC_Related_Person WHERE PK_goAML_Ref_WIC_Related_Person_ID = {oldItemGrid.PK_goAML_Ref_WIC_Related_Person_ID};"
            End If
        Next
        Return strQuery
    End Function
    'Edit Ripan 03-August-2023 ( ambigues objectname, jadi dikasih initial object di depannya )
    'Function GetQuery_DeleteGrid_URL(wicNo As String, newGrid As List(Of goAML_Ref_WIC_URL), Optional isDeleteAll As Boolean = False) As String
    Function GetQuery_DeleteGrid_URL(wicNo As String, newGrid As List(Of WICDataBLL.goAML_Ref_WIC_URL), Optional isDeleteAll As Boolean = False) As String
        Dim strQuery = ""

        If isDeleteAll Then
            strQuery = $"DELETE FROM goAML_Ref_WIC_URL WHERE WIC_No = {wicNo};"
            Return strQuery
        End If

        Dim oldGrid = GetURL(wicNo)
        For Each oldItemGrid In oldGrid
            Dim checkItemNew = newGrid.Find(Function(x) x.PK_goAML_Ref_WIC_URL_ID = oldItemGrid.PK_goAML_Ref_WIC_URL_ID)

            If checkItemNew Is Nothing Then
                strQuery = strQuery & $"DELETE FROM goAML_Ref_WIC_URL WHERE PK_goAML_Ref_WIC_URL_ID = {oldItemGrid.PK_goAML_Ref_WIC_URL_ID};"
            End If
        Next
        Return strQuery
    End Function
    'Edit Ripan 03-August-2023 ( ambigues objectname, jadi dikasih initial object di depannya )
    'Function GetQuery_DeleteGrid_EntityIdentification(wicNo As String, newGrid As List(Of goAML_Ref_WIC_Entity_Identification), Optional isDeleteAll As Boolean = False) As String
    Function GetQuery_DeleteGrid_EntityIdentification(wicNo As String, newGrid As List(Of WICDataBLL.goAML_Ref_WIC_Entity_Identification), Optional isDeleteAll As Boolean = False) As String
        Dim strQuery = ""

        If isDeleteAll Then
            strQuery = $"DELETE FROM goAML_Ref_WIC_Entity_Identification WHERE WIC_No = {wicNo};"
            Return strQuery
        End If

        Dim oldGrid = GetEntityIdentification(wicNo)
        For Each oldItemGrid In oldGrid
            Dim checkItemNew = newGrid.Find(Function(x) x.PK_goAML_Ref_WIC_Entity_Identifications_ID = oldItemGrid.PK_goAML_Ref_WIC_Entity_Identifications_ID)

            If checkItemNew Is Nothing Then
                strQuery = strQuery & $"DELETE FROM goAML_Ref_WIC_Entity_Identification WHERE PK_goAML_Ref_WIC_Entity_Identifications_ID = {oldItemGrid.PK_goAML_Ref_WIC_Entity_Identifications_ID};"
            End If
        Next
        Return strQuery
    End Function
    Function GetQuery_DeleteGrid_RelatedEntity(wicNo As String, newGrid As List(Of WICDataBLL.goAML_Ref_WIC_Related_Entity), Optional isDeleteAll As Boolean = False) As String
        Dim strQuery = ""

        If isDeleteAll Then
            strQuery = $"DELETE FROM goAML_Ref_WIC_Related_Entity WHERE WIC_No = {wicNo};"
            Return strQuery
        End If

        Dim oldGrid = GetRelatedEntity(wicNo)
        For Each oldItemGrid In oldGrid
            Dim checkItemNew = newGrid.Find(Function(x) x.PK_goAML_Ref_WIC_Related_Entities_ID = oldItemGrid.PK_goAML_Ref_WIC_Related_Entities_ID)

            If checkItemNew Is Nothing Then
                strQuery = strQuery & $"DELETE FROM goAML_Ref_WIC_Related_Entity WHERE PK_goAML_Ref_WIC_Related_Entities_ID = {oldItemGrid.PK_goAML_Ref_WIC_Related_Entities_ID};"
            End If
        Next
        Return strQuery
    End Function

    Sub SaveGoAmlWic501(wic As WICDataBLL)
        Try
            'Individu
            SaveWIC(wic.ObjWIC2)
            'SaveSocialMedia(wic.ObjGrid.ObjList_GoAML_SocialMedia)
            'SavePreviousName(wic.ObjGrid.ObjList_GoAML_PreviousName)
            If wic.ObjGrid IsNot Nothing Then
                Dim PK_Customer_ID = IIf(wic.ObjWIC Is Nothing, 0, wic.ObjWIC.PK_Customer_ID)
                Dim wicNo = IIf(wic.ObjWIC Is Nothing, "NULL", wic.ObjWIC.WIC_No)
                SaveEmail(wic.ObjGrid.ObjList_GoAML_Email, wicNo, PK_Customer_ID)
                'SaveEmploymentHistory(wic.ObjGrid.ObjList_GoAML_EmploymentHistory)
                SavePersonPEP(wic.ObjGrid.ObjList_GoAML_PersonPEP, wicNo, PK_Customer_ID)
                'SaveNetworkDevice(wic.ObjGrid.ObjList_GoAML_NetworkDevice)
                SaveSanction(wic.ObjGrid.ObjList_GoAML_Sanction, wicNo, PK_Customer_ID)
                SaveRelatedPerson(wic.ObjGrid.ObjList_GoAML_RelatedPerson, wicNo)
                'SaveAdditionalInformation(wic.ObjGrid.ObjList_GoAML_AdditionalInformation)

                'Corp
                Save_URL(wic.ObjGrid.ObjList_GoAML_URL, wicNo, PK_Customer_ID) ' 2023-11-14, Nael: Menambahkan Parameter PK_Customer_ID
                Save_EntityIdentification(wic.ObjGrid.ObjList_GoAML_EntityIdentification, wicNo, PK_Customer_ID)
                Save_RelatedEntities(wic.ObjGrid.ObjList_GoAML_RelatedEntity, wicNo)
                Save_Director(wic.ObjDirector)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Sub Save_Director(objList As List(Of WICDirectorDataBLL))
        Try
            If objList IsNot Nothing Then
                Dim context = New DataContext(AmlModule.Customer)

                For Each objData In objList
                    Dim where_director = $"FK_FOR_TABLE_ID = {objData.ObjDirector.NO_ID} AND FK_REF_DETAIL_OF = {RefDetail.WIC_DIRECTOR}"

                    If objData.ListDirectorEmail IsNot Nothing Then
                        context.goAML_Ref_WIC_Email.Delete(where_director)
                        For Each objDataDetail In objData.ListDirectorEmail
                            objDataDetail.FK_REF_DETAIL_OF = RefDetail.WIC_DIRECTOR
                            objDataDetail.CIF = Nothing
                            objDataDetail.FK_FOR_TABLE_ID = objData.ObjDirector.NO_ID
                            context.goAML_Ref_WIC_Email.Save(objDataDetail)
                        Next
                    End If

                    If objData.ListDirectorSanction IsNot Nothing Then
                        context.goAML_Ref_WIC_Sanction.Delete(where_director)
                        For Each objDataDetail In objData.ListDirectorSanction
                            objDataDetail.CIF = Nothing
                            objDataDetail.FK_REF_DETAIL_OF = RefDetail.WIC_DIRECTOR
                            objDataDetail.FK_FOR_TABLE_ID = objData.ObjDirector.NO_ID
                            context.goAML_Ref_WIC_Sanction.Save(objDataDetail)
                        Next
                    End If

                    If objData.ListDirectorPEP IsNot Nothing Then
                        context.goAML_Ref_WIC_Person_PEP.Delete(where_director)
                        For Each objDataDetail In objData.ListDirectorPEP
                            objDataDetail.FK_REF_DETAIL_OF = RefDetail.WIC_DIRECTOR
                            objDataDetail.CIF = Nothing
                            objDataDetail.FK_FOR_TABLE_ID = objData.ObjDirector.NO_ID
                            context.goAML_Ref_WIC_Person_PEP.Save(objDataDetail)
                        Next
                    End If
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub

    Sub SaveWIC(objData As goAML_Ref_WIC2)
        Try
            If (objData IsNot Nothing) Then
                Dim strQuery As String = ""
                strQuery = $"UPDATE [dbo].[goAML_Ref_WIC]"
                strQuery += $"   SET"
                strQuery += $"      [Is_Protected] = {If(objData.IS_PROTECTED, 1, 0)}"
                strQuery += $"      ,[Is_RealWIC] = {If(objData.IS_REAL_WIC, 1, 0)}"
                strQuery += $" WHERE WIC_No = '{objData.WIC_No}'"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            End If

        Catch ex As Exception

        End Try
    End Sub

    ' 2023-11-15, Nael: Proses Update diubah menjadi delete old, insert new
    Sub SaveEmail(obj_ListEmail_Edit As List(Of WICDataBLL.goAML_Ref_WIC_Email), wic As String, pkParent As Integer)
        Try
            If obj_ListEmail_Edit IsNot Nothing Then
                Dim context = New DataContext(AmlModule.WIC)
                Dim filter = $"WIC_No = '{wic}' AND FK_REF_DETAIL_OF = 3"
                context.goAML_Ref_WIC_Email.Delete(filter)
                For Each objData In obj_ListEmail_Edit
                    objData.FK_FOR_TABLE_ID = Nothing
                    objData.FK_REF_DETAIL_OF = 3
                    Dim convertedObj = New DataModel.goAML_Ref_Customer_Email()
                    convertedObj.PK_goAML_Ref_Customer_Email_ID = objData.PK_goAML_Ref_WIC_Email_ID
                    convertedObj.CIF = objData.WIC_No
                    convertedObj.EMAIL_ADDRESS = objData.email_address
                    convertedObj.Active = objData.Active
                    convertedObj.LastUpdateBy = objData.LastUpdateBy
                    convertedObj.ApprovedBy = objData.ApprovedBy
                    convertedObj.CreatedDate = objData.CreatedDate
                    convertedObj.LastUpdateDate = objData.LastUpdateDate
                    convertedObj.ApprovedDate = objData.ApprovedDate
                    convertedObj.Alternateby = objData.Alternateby
                    convertedObj.FK_REF_DETAIL_OF = 3
                    convertedObj.FK_FOR_TABLE_ID = pkParent

                    context.goAML_Ref_WIC_Email.Save(convertedObj)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    ' 2023-11-15, Nael: Proses Update diubah menjadi delete old, insert new
    Sub SavePersonPEP(obj_ListPersonPEP_Edit As List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP), wic As String, pkParent As Integer)
        Try
            If obj_ListPersonPEP_Edit IsNot Nothing Then
                Dim context = New DataContext(AmlModule.WIC)
                Dim filter = $"WIC_No = '{wic}' AND FK_REF_DETAIL_OF = 3"
                context.goAML_Ref_WIC_Person_PEP.Delete(filter)
                For Each objData In obj_ListPersonPEP_Edit
                    Dim convertedObj = New DataModel.goAML_Ref_Customer_PEP()
                    convertedObj.PK_goAML_Ref_Customer_PEP_ID = objData.PK_goAML_Ref_WIC_PEP_ID
                    convertedObj.CIF = objData.WIC_No
                    convertedObj.PEP_COUNTRY = objData.pep_country
                    convertedObj.FUNCTION_NAME = objData.function_name
                    convertedObj.FUNCTION_DESCRIPTION = objData.function_description
                    convertedObj.COMMENTS = objData.comments
                    'convertedObj.Active = objData.Active
                    convertedObj.CreatedBy = objData.CreatedBy
                    convertedObj.LastUpdateBy = objData.LastUpdateBy
                    convertedObj.ApprovedBy = objData.ApprovedBy
                    convertedObj.CreatedDate = objData.CreatedDate
                    convertedObj.LastUpdateDate = objData.LastUpdateDate
                    convertedObj.ApprovedDate = objData.ApprovedDate
                    convertedObj.Alternateby = objData.Alternateby
                    convertedObj.FK_REF_DETAIL_OF = 3
                    convertedObj.FK_FOR_TABLE_ID = pkParent

                    context.goAML_Ref_WIC_Person_PEP.Save(convertedObj)
                Next

            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    ' 2023-11-15, Nael: Proses Update diubah menjadi delete old, insert new
    Sub SaveSanction(obj_ListSanction_Edit As List(Of WICDataBLL.goAML_Ref_WIC_Sanction), wic As String, Optional headerID As Integer = -1)
        Try
            If obj_ListSanction_Edit IsNot Nothing Then
                Dim context = New DataContext(AmlModule.WIC)
                Dim filter = $"WIC_No = '{wic}' AND FK_REF_DETAIL_OF = 3"
                context.goAML_Ref_WIC_Sanction.Delete(filter)
                For Each objData In obj_ListSanction_Edit
                    Dim convertedObj = New DataModel.goAML_Ref_Customer_Sanction()

                    convertedObj.PK_goAML_Ref_Customer_Sanction_ID = objData.PK_goAML_Ref_WIC_Sanction_ID
                    convertedObj.CIF = objData.WIC_No
                    convertedObj.PROVIDER = objData.Provider
                    convertedObj.SANCTION_LIST_NAME = objData.Sanction_List_Name
                    convertedObj.MATCH_CRITERIA = objData.Match_Criteria
                    convertedObj.LINK_TO_SOURCE = objData.Link_To_Source
                    convertedObj.SANCTION_LIST_ATTRIBUTES = objData.Sanction_List_Attributes
                    convertedObj.COMMENTS = objData.Comments
                    convertedObj.Active = objData.Active
                    convertedObj.CreatedBy = objData.CreatedBy
                    convertedObj.LastUpdateBy = objData.LastUpdateBy
                    convertedObj.ApprovedBy = objData.ApprovedBy
                    convertedObj.CreatedDate = objData.CreatedDate
                    convertedObj.LastUpdateDate = objData.LastUpdateDate
                    convertedObj.ApprovedDate = objData.ApprovedDate
                    convertedObj.Alternateby = objData.Alternateby
                    convertedObj.FK_REF_DETAIL_OF = 3
                    convertedObj.FK_FOR_TABLE_ID = headerID

                    context.goAML_Ref_WIC_Sanction.Save(convertedObj)
                Next

            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    ' 2023-11-15, Nael: Mengubah fungsi related person
    Sub SaveRelatedPerson(obj_ListRelatedPerson_Edit As List(Of WICDataBLL.goAML_Ref_WIC_Related_Person), WIC As String)
        Try
            If obj_ListRelatedPerson_Edit IsNot Nothing Then
                Dim _context = New DataContext(AmlModule.WIC)

                Dim listOldRelatedPerson = _context.goAML_Ref_WIC_Related_Person.GetData($"WIC_No = '{WIC}'")

                For Each objOld In listOldRelatedPerson
                    Dim pk = objOld.PK_goAML_Ref_Customer_Related_Person_ID
                    Dim fk_ref_detail_of = RefDetail.CUSTOMER_RELATED_PERSON
                    Dim fk_ref_detail_work = RefDetail.WIC_RELATED_PERSON_WORK
                    _context.goAML_Person_Identification.Delete($"FK_Person_ID = {pk} AND FK_Person_Type = {fk_ref_detail_of}")
                    _context.goAML_Ref_Address.Delete($"FK_To_Table_ID = {pk} AND FK_REF_DETAIL_OF = {fk_ref_detail_of}")
                    _context.goAML_Ref_Phone.Delete($"FK_for_Table_ID = {pk} AND FK_REF_DETAIL_OF = {fk_ref_detail_of}")
                    _context.goAML_Ref_Address.Delete($"FK_To_Table_ID = {pk} AND FK_REF_DETAIL_OF = {fk_ref_detail_work}")
                    _context.goAML_Ref_Phone.Delete($"FK_for_Table_ID = {pk} AND FK_REF_DETAIL_OF = {fk_ref_detail_work}")
                    _context.goAML_Ref_WIC_Email.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = {RefDetail.WIC_RELATED_PERSON}")
                    _context.goAML_Ref_WIC_Sanction.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = {RefDetail.WIC_RELATED_PERSON}")
                    _context.goAML_Ref_WIC_Person_PEP.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = {RefDetail.WIC_RELATED_PERSON}")
                Next

                _context.goAML_Ref_WIC_Related_Person.Delete($"WIC_No = '{WIC}'")

                For Each objData In obj_ListRelatedPerson_Edit
                    Dim convertedObj = New DataModel.goAML_Ref_Customer_Related_Person
                    convertedObj.PK_goAML_Ref_Customer_Related_Person_ID = objData.PK_goAML_Ref_WIC_Related_Person_ID
                    convertedObj.CIF = objData.WIC_No
                    convertedObj.PERSON_PERSON_RELATION = objData.Person_Person_Relation
                    'convertedObj.RELATION_DATE_RANGE_VALID_FROM = objData.Relation_Date_Range_Valid_From
                    'convertedObj.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = objData.Relation_Date_Range_Is_Approx_From_Date
                    'convertedObj.RELATION_DATE_RANGE_VALID_TO = objData.Relation_Date_Range_Valid_To
                    'convertedObj.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = objData.Relation_Date_Range_Is_Approx_To_Date
                    convertedObj.COMMENTS = objData.Comments
                    convertedObj.GENDER = objData.Gender
                    convertedObj.TITLE = objData.Title
                    convertedObj.FIRST_NAME = objData.First_Name
                    convertedObj.MIDDLE_NAME = objData.Middle_Name
                    convertedObj.PREFIX = objData.Prefix
                    convertedObj.LAST_NAME = objData.Last_Name
                    convertedObj.BIRTHDATE = IIf(objData.Birth_Date Is Nothing, Nothing, objData.Birth_Date.ToString())
                    convertedObj.BIRTH_PLACE = objData.Birth_Place
                    convertedObj.COUNTRY_OF_BIRTH = objData.Country_Of_Birth
                    convertedObj.MOTHERS_NAME = objData.Mother_Name
                    convertedObj.ALIAS = objData._Alias
                    convertedObj.FULL_NAME_FRN = objData.Full_Name_Frn
                    convertedObj.SSN = objData.SSN
                    convertedObj.PASSPORT_NUMBER = objData.Passport_Number
                    convertedObj.PASSPORT_COUNTRY = objData.Passport_Country
                    convertedObj.ID_NUMBER = objData.ID_Number
                    convertedObj.NATIONALITY1 = objData.Nationality1
                    convertedObj.NATIONALITY2 = objData.Nationality2
                    convertedObj.NATIONALITY3 = objData.Nationality3
                    convertedObj.RESIDENCE = objData.Residence
                    convertedObj.RESIDENCE_SINCE = objData.Residence_Since
                    convertedObj.OCCUPATION = objData.Occupation
                    convertedObj.DECEASED = objData.Deceased
                    convertedObj.DATE_DECEASED = objData.Date_Deceased
                    convertedObj.TAX_NUMBER = objData.Tax_Number
                    convertedObj.TAX_REG_NUMBER = objData.Is_PEP
                    convertedObj.SOURCE_OF_WEALTH = objData.Source_Of_Wealth
                    convertedObj.IS_PROTECTED = objData.Is_Protected
                    'convertedObj.Active = objData.Active
                    convertedObj.CreatedBy = objData.CreatedBy
                    convertedObj.LastUpdateBy = objData.LastUpdateBy
                    convertedObj.ApprovedBy = objData.ApprovedBy
                    convertedObj.CreatedDate = objData.CreatedDate
                    convertedObj.LastUpdateDate = objData.LastUpdateDate
                    convertedObj.ApprovedDate = objData.ApprovedDate
                    convertedObj.Alternateby = objData.Alternateby
                    convertedObj.EMPLOYER_NAME = objData.EMPLOYER_NAME
                    convertedObj.RELATION_COMMENTS = objData.relation_comments

                    Dim newPK = _context.goAML_Ref_WIC_Related_Person.Save(convertedObj)
                    goAML_Customer_Service.WICRelatedPersonService.SaveChild(newPK, objData)
                Next

            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    ' 2023-11-14, Nael: Menambahkan parentPK
    ' 2023-11-15, Nael: Proses Update diubah menjadi delete old, insert new
    Sub Save_URL(objList As List(Of WICDataBLL.goAML_Ref_WIC_URL), wic As String, parentPK As Integer)
        Try
            Dim strQuery As String = ""
            If objList IsNot Nothing Then
                Dim context = New DataContext(AmlModule.WIC)
                Dim filter = $"WIC_No = '{wic}' AND FK_REF_DETAIL_OF = 3"

                context.goAML_Ref_WIC_URL.Delete(filter)
                For Each objData In objList
                    Dim convertedObj = New DataModel.goAML_Ref_Customer_URL
                    convertedObj.PK_goAML_Ref_Customer_URL_ID = objData.PK_goAML_Ref_WIC_URL_ID
                    convertedObj.CIF = objData.WIC_NO
                    convertedObj.URL = objData.URL
                    convertedObj.Active = objData.Active
                    convertedObj.CreatedBy = objData.CreatedBy
                    convertedObj.LastUpdateBy = objData.LastUpdateBy
                    convertedObj.ApprovedBy = objData.ApprovedBy
                    convertedObj.CreatedDate = objData.CreatedDate
                    convertedObj.LastUpdateDate = objData.LastUpdateDate
                    convertedObj.ApprovedDate = objData.ApprovedDate
                    convertedObj.Alternateby = objData.Alternateby
                    convertedObj.FK_FOR_TABLE_ID = parentPK
                    convertedObj.FK_REF_DETAIL_OF = 3

                    context.goAML_Ref_WIC_URL.Save(convertedObj)
                Next

            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    ' 2023-11-14, Nael: Menambahkan fkparent
    ' 2023-11-15, Mengganti proses update jadi delete old, insert new
    Sub Save_EntityIdentification(objList As List(Of WICDataBLL.goAML_Ref_WIC_Entity_Identification), wic As String, pkParent As Integer)
        Try
            Dim context = New DataContext(AmlModule.WIC)
            Dim filter = $"WIC_No = '{wic}' AND FK_REF_DETAIL_OF = 3"
            context.goAML_Ref_WIC_Entity_Identification.Delete(filter)
            For Each objData In objList
                Dim convertedObj = New DataModel.goAML_Ref_Customer_Entity_Identification
                convertedObj.PK_goAML_Ref_Customer_Entity_Identification_ID = objData.PK_goAML_Ref_WIC_Entity_Identifications_ID
                convertedObj.CIF = objData.WIC_NO
                convertedObj.TYPE = objData.TYPE
                convertedObj.NUMBER = objData.NUMBER
                convertedObj.ISSUE_DATE = objData.ISSUE_DATE
                convertedObj.EXPIRY_DATE = objData.EXPIRY_DATE
                convertedObj.ISSUED_BY = objData.ISSUED_BY
                convertedObj.ISSUE_COUNTRY = objData.ISSUE_COUNTRY
                convertedObj.COMMENTS = objData.COMMENTS
                convertedObj.Active = objData.Active
                convertedObj.CreatedBy = objData.CreatedBy
                convertedObj.LastUpdateBy = objData.LastUpdateBy
                convertedObj.ApprovedBy = objData.ApprovedBy
                convertedObj.CreatedDate = objData.CreatedDate
                convertedObj.LastUpdateDate = objData.LastUpdateDate
                convertedObj.ApprovedDate = objData.ApprovedDate
                convertedObj.Alternateby = objData.Alternateby
                convertedObj.FK_FOR_TABLE_ID = pkParent
                convertedObj.FK_REF_DETAIL_OF = 3

                context.goAML_Ref_WIC_Entity_Identification.Save(convertedObj)
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub Save_RelatedEntities(objList As List(Of WICDataBLL.goAML_Ref_WIC_Related_Entity), WIC As String)
        Try
            Dim strQuery As String = ""
            If objList IsNot Nothing Then
                Dim context = New DataContext(AmlModule.WIC)

                Dim listOldRelatedEntity = context.goAML_Ref_WIC_Related_Entity.GetData($"WIC_No = '{WIC}'")

                ' delete data dari grid grid
                For Each objOld In listOldRelatedEntity
                    Dim pk = objOld.PK_goAML_Ref_Customer_Related_Entities_ID
                    Dim fk_ref_detail_of = RefDetail.WIC_RELATED_ENTITY
                    Dim fk_ref_detail_work = RefDetail.WIC_RELATED_PERSON_WORK
                    context.goAML_Ref_WIC_Entity_Identification.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = 36")
                    context.goAML_Ref_Address.Delete($"FK_To_Table_ID = {pk} AND FK_REF_DETAIL_OF = {fk_ref_detail_of}")
                    context.goAML_Ref_Phone.Delete($"FK_for_Table_ID = {pk} AND FK_REF_DETAIL_OF = {fk_ref_detail_of}")
                    context.goAML_Ref_Address.Delete($"FK_To_Table_ID = {pk} AND FK_REF_DETAIL_OF = {fk_ref_detail_work}")
                    context.goAML_Ref_Phone.Delete($"FK_for_Table_ID = {pk} AND FK_REF_DETAIL_OF = {fk_ref_detail_work}")
                    context.goAML_Ref_WIC_Email.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = {fk_ref_detail_of}")
                    context.goAML_Ref_WIC_Sanction.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = {fk_ref_detail_of}")
                    context.goAML_Ref_WIC_Person_PEP.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = {fk_ref_detail_of}")
                Next
                ' delete data related entity lama
                context.goAML_Ref_WIC_Related_Entity.Delete($"WIC_No = '{WIC}'")

                ' insert data baru
                For Each objData In objList
                    Dim convertedObj = New DataModel.goAML_Ref_Customer_Related_Entities
                    convertedObj.PK_goAML_Ref_Customer_Related_Entities_ID = objData.PK_goAML_Ref_WIC_Related_Entities_ID
                    convertedObj.CIF = objData.WIC_NO
                    convertedObj.ENTITY_ENTITY_RELATION = objData.ENTITY_ENTITY_RELATION
                    'convertedObj.RELATION_DATE_RANGE_VALID_FROM = objData.RELATION_DATE_RANGE_VALID_FROM
                    'convertedObj.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = objData.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                    'convertedObj.RELATION_DATE_RANGE_VALID_TO = objData.RELATION_DATE_RANGE_VALID_TO
                    'convertedObj.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = objData.RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                    convertedObj.SHARE_PERCENTAGE = objData.SHARE_PERCENTAGE
                    convertedObj.COMMENTS = objData.COMMENTS
                    convertedObj.NAME = objData.NAME
                    convertedObj.COMMERCIAL_NAME = objData.COMMERCIAL_NAME
                    convertedObj.INCORPORATION_LEGAL_FORM = objData.INCORPORATION_LEGAL_FORM
                    convertedObj.INCORPORATION_NUMBER = objData.INCORPORATION_NUMBER
                    convertedObj.BUSINESS = objData.BUSINESS
                    convertedObj.ENTITY_STATUS = objData.ENTITY_STATUS
                    convertedObj.ENTITY_STATUS_DATE = objData.ENTITY_STATUS_DATE
                    convertedObj.INCORPORATION_STATE = objData.INCORPORATION_STATE
                    convertedObj.INCORPORATION_COUNTRY_CODE = objData.INCORPORATION_COUNTRY_CODE
                    convertedObj.BUSINESS_CLOSED = objData.BUSINESS_CLOSED
                    convertedObj.DATE_BUSINESS_CLOSED = objData.DATE_BUSINESS_CLOSED
                    convertedObj.TAX_NUMBER = objData.TAX_NUMBER
                    convertedObj.TAX_REG_NUMBER = objData.TAX_REG_NUMBER
                    convertedObj.RELATION_COMMENTS = objData.RELATION_COMMENTS
                    convertedObj.CreatedBy = objData.CreatedBy
                    convertedObj.LastUpdateBy = objData.LastUpdateBy
                    convertedObj.ApprovedBy = objData.ApprovedBy
                    convertedObj.CreatedDate = objData.CreatedDate
                    convertedObj.LastUpdateDate = objData.LastUpdateDate
                    convertedObj.ApprovedDate = objData.ApprovedDate
                    convertedObj.Alternateby = objData.Alternateby

                    Dim newPK = context.goAML_Ref_WIC_Related_Entity.Save(convertedObj)
                    goAML_Customer_Service.WICRelatedEntityService.SaveChild(newPK, objData)
                Next



                'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub Save_Director(objList As List(Of DataModel.goAML_Ref_Entity_Director))
        Try
            Dim context = New DataContext(AmlModule.Customer)
            'Dim emailDAL = New goAML_Ref_Customer_Email_DAL
            'Dim sanctionDAL = New goAML_Ref_Customer_Sanction_DAL
            'Dim pepDAL = New goAML_Ref_Customer_PEP_DAL

            For Each objData In objList
                Dim where_director = $"WIC_No = '{objData.PK_goAML_Ref_Customer_Entity_Director_ID}' AND FK_REF_DETAIL_OF = 8"
                context.goAML_Ref_Customer_Email.Delete(where_director)
                For Each objDataDetail In objData.ObjList_GoAML_Ref_Customer_Email
                    context.goAML_Ref_WIC_Email.Save(objDataDetail)
                Next

                context.goAML_Ref_Customer_Sanction.Delete(where_director)
                For Each objDataDetail In objData.ObjList_GoAML_Ref_Customer_Sanction
                    context.goAML_Ref_WIC_Sanction.Save(objDataDetail)
                Next

                context.goAML_Ref_Customer_PEP.Delete(where_director)
                For Each objDataDetail In objData.ObjList_GoAML_Ref_Customer_PEP
                    context.goAML_Ref_WIC_Person_PEP.Save(objDataDetail)
                Next
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub CreateAuditTrail(objDB As GoAMLEntities, pkAuditTrailId As Long, gridWIC As goAML_Grid_WIC)
        If gridWIC.ObjList_GoAML_SocialMedia IsNot Nothing Then
            For Each itemheader In gridWIC.ObjList_GoAML_SocialMedia
                NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, itemheader)
            Next
        End If

        If gridWIC.ObjList_GoAML_PreviousName IsNot Nothing Then
            For Each itemheader In gridWIC.ObjList_GoAML_PreviousName
                NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, itemheader)
            Next
        End If

        If gridWIC.ObjList_GoAML_Email IsNot Nothing Then
            For Each itemheader In gridWIC.ObjList_GoAML_Email
                NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, itemheader)
            Next
        End If

        If gridWIC.ObjList_GoAML_EmploymentHistory IsNot Nothing Then
            For Each itemheader In gridWIC.ObjList_GoAML_EmploymentHistory
                NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, itemheader)
            Next
        End If

        If gridWIC.ObjList_GoAML_PersonPEP IsNot Nothing Then
            For Each itemheader In gridWIC.ObjList_GoAML_PersonPEP
                NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, itemheader)
            Next
        End If

        If gridWIC.ObjList_GoAML_NetworkDevice IsNot Nothing Then
            For Each itemheader In gridWIC.ObjList_GoAML_NetworkDevice
                NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, itemheader)
            Next
        End If

        If gridWIC.ObjList_GoAML_Sanction IsNot Nothing Then
            For Each itemheader In gridWIC.ObjList_GoAML_Sanction
                NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, itemheader)
            Next
        End If

        If gridWIC.ObjList_GoAML_RelatedPerson IsNot Nothing Then
            Dim itemSerialize = gridWIC.ObjList_GoAML_RelatedPerson.Select(Function(x) x)
            For Each itemheader In itemSerialize
                If itemheader.ObjList_GoAML_Ref_Phone IsNot Nothing Then
                    For Each item In itemheader.ObjList_GoAML_Ref_Phone
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, item)
                    Next
                    itemheader.ObjList_GoAML_Ref_Phone = Nothing
                End If

                If itemheader.ObjList_GoAML_Ref_Phone_Work IsNot Nothing Then
                    For Each item In itemheader.ObjList_GoAML_Ref_Phone_Work
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, item)
                    Next
                    itemheader.ObjList_GoAML_Ref_Phone_Work = Nothing
                End If

                If itemheader.ObjList_GoAML_Ref_Address IsNot Nothing Then
                    For Each item In itemheader.ObjList_GoAML_Ref_Address
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, item)
                    Next
                    itemheader.ObjList_GoAML_Ref_Address = Nothing
                End If

                If itemheader.ObjList_GoAML_Ref_Address_Work IsNot Nothing Then
                    For Each item In itemheader.ObjList_GoAML_Ref_Address_Work
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, item)
                    Next
                    itemheader.ObjList_GoAML_Ref_Address_Work = Nothing
                End If

                If itemheader.ObjList_GoAML_Ref_Customer_Email IsNot Nothing Then
                    For Each item In itemheader.ObjList_GoAML_Ref_Customer_Email
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, item)
                    Next
                    itemheader.ObjList_GoAML_Ref_Customer_Email = Nothing
                End If

                If itemheader.ObjList_GoAML_Ref_Customer_PEP IsNot Nothing Then
                    For Each item In itemheader.ObjList_GoAML_Ref_Customer_PEP
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, item)
                    Next
                    itemheader.ObjList_GoAML_Ref_Customer_PEP = Nothing
                End If

                If itemheader.ObjList_GoAML_Ref_Customer_Sanction IsNot Nothing Then
                    For Each item In itemheader.ObjList_GoAML_Ref_Customer_Sanction
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, item)
                    Next
                    itemheader.ObjList_GoAML_Ref_Customer_Sanction = Nothing
                End If

                If itemheader.ObjList_GoAML_Person_Identification IsNot Nothing Then
                    For Each item In itemheader.ObjList_GoAML_Person_Identification
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, item)
                    Next
                    itemheader.ObjList_GoAML_Person_Identification = Nothing
                End If

                NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, itemheader)
            Next
        End If

        If gridWIC.ObjList_GoAML_AdditionalInformation IsNot Nothing Then
            For Each itemheader In gridWIC.ObjList_GoAML_AdditionalInformation
                NawaFramework.CreateAuditTrailDetailAdd(objDB, pkAuditTrailId, itemheader)
            Next
        End If
    End Sub
    Shared Function GetReferenceByCode(Table As String, Kode As String) As WICDataBLL.General_Reference

        Dim query = "SELECT TOP 1 * From " & Table & " WHERE Kode = '" & Kode & "'"
        Dim result = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)

        If result IsNot Nothing Then
            Dim objResult = New WICDataBLL.General_Reference
            objResult.Kode = result("Kode").ToString()
            objResult.Keterangan = result("Keterangan").ToString()

            Return objResult
        End If

        Return Nothing
    End Function

    Function GetWICData(wicNo As String) As WICDataBLL
        Dim wicData = New WICDataBLL
        wicData.ObjGrid = New WICDataBLL.goAML_Grid_WIC

        wicData.ObjWIC2 = GetWIC(wicNo)
        wicData.ObjGrid.ObjList_GoAML_Email = GetEmail(wicNo, 3)
        wicData.ObjGrid.ObjList_GoAML_PersonPEP = GetPEP(wicNo, 3)
        wicData.ObjGrid.ObjList_GoAML_Sanction = GetSanction(wicNo, 3)
        wicData.ObjGrid.ObjList_GoAML_RelatedPerson = GetRelatedPerson(wicNo)
        wicData.ObjGrid.ObjList_GoAML_URL = GetURL(wicNo, 3)
        wicData.ObjGrid.ObjList_GoAML_EntityIdentification = GetEntityIdentification(wicNo, 3)
        wicData.ObjGrid.ObjList_GoAML_RelatedEntity = GetRelatedEntity(wicNo)

        Return wicData
    End Function

    Function GetWIC(wicNo As String) As WICDataBLL.goAML_Ref_WIC2
        Dim query = "select * FROM goAML_Ref_WIC where WIC_No = '" & wicNo & "'"

        Dim objDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
        Dim objRet = New goAML_Ref_WIC2
        For Each itemAddress As DataRow In objDataTable.Rows
            If Not IsDBNull(itemAddress("PK_Customer_ID")) Then
                objRet.PK_Customer_ID = itemAddress("PK_Customer_ID")
            End If
            If Not IsDBNull(itemAddress("WIC_No")) Then
                objRet.WIC_No = itemAddress("WIC_No")
            End If
            If Not IsDBNull(itemAddress("IS_PROTECTED")) Then
                objRet.IS_PROTECTED = itemAddress("IS_PROTECTED")
            End If
            If Not IsDBNull(itemAddress("Is_RealWIC")) Then
                objRet.IS_REAL_WIC = itemAddress("Is_RealWIC")
            End If
        Next
        Return objRet
    End Function

    ' 2023-11-14, Nael: Menambahkan fkRefDetail
    Function GetEmail(wicNo As String, Optional fkRefDetail As Integer = 0) As List(Of WICDataBLL.goAML_Ref_WIC_Email)
        Dim query = "select * FROM goAML_Ref_WIC_Email where WIC_No = '" & wicNo & "' AND FK_REF_DETAIL_OF = " & fkRefDetail ' 2023-11-14, Nael

        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
        Dim listReturn As New List(Of WICDataBLL.goAML_Ref_WIC_Email) 'Edit Ripan 03-August-2023 ( efek penambahakan di GoAMLDAL, harus dikasih intial object )

        For Each data As DataRow In dataDT.Rows
            Dim objReturn = New WICDataBLL.goAML_Ref_WIC_Email 'Edit Ripan 03-August-2023 ( efek penambahakan di GoAMLDAL, harus dikasih intial object )
            If Not IsDBNull(data("PK_goAML_Ref_WIC_Email_ID")) Then
                objReturn.PK_goAML_Ref_WIC_Email_ID = data("PK_goAML_Ref_WIC_Email_ID")
            End If
            If Not IsDBNull(data("WIC_No")) Then
                objReturn.WIC_No = data("WIC_No")
            End If
            If Not IsDBNull(data("EMAIL_ADDRESS")) Then
                objReturn.email_address = data("EMAIL_ADDRESS")
            End If

            listReturn.Add(objReturn)
        Next
        Return listReturn
    End Function

    ' 2023-11-14, Nael: Menambahkan parameter fkRefDetail
    Function GetPEP(wicNo As String, Optional fkRefDetail As Integer = 0) As List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP)
        Dim query = "select * FROM goAML_Ref_WIC_Person_PEP where WIC_No = '" & wicNo & "'"

        ' 2023-11-14, Nael: Menambahkan filter FK_REF_DETAIL_OF
        query += " AND FK_REF_DETAIL_OF = " & fkRefDetail

        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
        Dim listReturn As New List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP)

        For Each data As DataRow In dataDT.Rows
            Dim objReturn = New WICDataBLL.goAML_Ref_WIC_Person_PEP
            If Not data.IsNull("PK_goAML_Ref_WIC_PEP_ID") Then
                objReturn.PK_goAML_Ref_WIC_PEP_ID = data("PK_goAML_Ref_WIC_PEP_ID")
            End If
            If Not data.IsNull("WIC_No") Then
                objReturn.WIC_No = data("WIC_No")
            End If
            If Not data.IsNull("PEP_COUNTRY") Then
                objReturn.pep_country = data("PEP_COUNTRY")
            End If
            If Not data.IsNull("FUNCTION_NAME") Then
                objReturn.function_name = data("FUNCTION_NAME")
            End If
            If Not data.IsNull("FUNCTION_DESCRIPTION") Then
                objReturn.function_description = data("FUNCTION_DESCRIPTION")
            End If
            'objReturn.pep_date_range_valid_from = data("PEP_DATE_RANGE_VALID_FROM")
            'objReturn.pep_date_range_is_approx_from_date = data("PEP_DATE_RANGE_IS_APPROX_FROM_DATE")
            'objReturn.pep_date_range_valid_to = data("PEP_DATE_RANGE_VALID_TO")
            'objReturn.pep_date_range_is_approx_to_date = data("PEP_DATE_RANGE_IS_APPROX_TO_DATE")
            If Not data.IsNull("COMMENTS") Then
                objReturn.comments = data("COMMENTS")
            End If

            listReturn.Add(objReturn)
        Next
        Return listReturn
    End Function

    ' 2023-11-14, Nael: Menambahkan optional parameter fkRefDetail
    Function GetSanction(wicNo As String, Optional fkRefDetail As Integer = 0) As List(Of WICDataBLL.goAML_Ref_WIC_Sanction)
        Dim query = "select * FROM goAML_Ref_WIC_Sanction where WIC_No = '" & wicNo & "' AND FK_REF_DETAIL_OF = " & fkRefDetail ' 2023-11-14, Nael
        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
        Dim listReturn As New List(Of WICDataBLL.goAML_Ref_WIC_Sanction) 'Edit Ripan 03-August-2023 ( efek penambahakan di GoAMLDAL, harus dikasih intial object )



        For Each data As DataRow In dataDT.Rows
            Dim objReturn = New WICDataBLL.goAML_Ref_WIC_Sanction 'Edit Ripan 03-August-2023 ( efek penambahakan di GoAMLDAL, harus dikasih intial object )
            If Not IsDBNull(data("PK_goAML_Ref_WIC_Sanction_ID")) Then
                objReturn.PK_goAML_Ref_WIC_Sanction_ID = data("PK_goAML_Ref_WIC_Sanction_ID")
            End If
            If Not IsDBNull(data("WIC_No")) Then
                objReturn.WIC_No = data("WIC_No")
            End If
            If Not IsDBNull(data("PK_goAML_Ref_WIC_Sanction_ID")) Then
                objReturn.PK_goAML_Ref_WIC_Sanction_ID = data("PK_goAML_Ref_WIC_Sanction_ID")
            End If
            If Not IsDBNull(data("PROVIDER")) Then
                objReturn.Provider = data("PROVIDER")
            End If
            If Not IsDBNull(data("SANCTION_LIST_NAME")) Then
                objReturn.Sanction_List_Name = data("SANCTION_LIST_NAME")
            End If
            If Not IsDBNull(data("MATCH_CRITERIA")) Then
                objReturn.Match_Criteria = data("MATCH_CRITERIA")
            End If
            If Not IsDBNull(data("LINK_TO_SOURCE")) Then
                objReturn.Link_To_Source = data("LINK_TO_SOURCE")
            End If
            If Not IsDBNull(data("SANCTION_LIST_ATTRIBUTES")) Then
                objReturn.Sanction_List_Attributes = data("SANCTION_LIST_ATTRIBUTES")
            End If
            If Not IsDBNull(data("SANCTION_LIST_DATE_RANGE_VALID_FROM")) Then
                objReturn.Sanction_List_Date_Range_Valid_From = data("SANCTION_LIST_DATE_RANGE_VALID_FROM")
            End If
            If Not IsDBNull(data("SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE")) Then
                objReturn.Sanction_List_Date_Range_Is_Approx_From_Date = data("SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE")
            End If
            If Not IsDBNull(data("SANCTION_LIST_DATE_RANGE_VALID_TO")) Then
                objReturn.Sanction_List_Date_Range_Valid_To = data("SANCTION_LIST_DATE_RANGE_VALID_TO")
            End If
            If Not IsDBNull(data("SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE")) Then
                objReturn.Sanction_List_Date_Range_Is_Approx_To_Date = data("SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE")
            End If
            If Not IsDBNull(data("COMMENTS")) Then
                objReturn.Comments = data("COMMENTS")
            End If
            ' 2023-11-13, Nael: Menambahkan data ini
            If Not IsDBNull(data("FK_REF_DETAIL_OF")) Then
                objReturn.FK_REF_DETAIL_OF = data("FK_REF_DETAIL_OF")
            End If
            If Not IsDBNull(data("FK_FOR_TABLE_ID")) Then
                objReturn.FK_FOR_TABLE_ID = data("FK_FOR_TABLE_ID")
            End If

            listReturn.Add(objReturn)
        Next
        Return listReturn
    End Function

    Function GetRelatedPerson(wicNo As String) As List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)
        Dim query = "select * FROM goAML_Ref_WIC_Related_Person where WIC_No = '" & wicNo & "'"
        Dim DataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
        Dim listReturn As New List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)

        For Each Data As DataRow In DataDT.Rows
            Dim objReturn = New WICDataBLL.goAML_Ref_WIC_Related_Person

            If Not IsDBNull(Data("PK_goAML_Ref_WIC_Related_Person_ID")) Then
                objReturn.PK_goAML_Ref_WIC_Related_Person_ID = Data("PK_goAML_Ref_WIC_Related_Person_ID")
            End If
            If Not IsDBNull(Data("WIC_No")) Then
                objReturn.WIC_No = Data("WIC_No")
            End If
            If Not IsDBNull(Data("PERSON_PERSON_RELATION")) Then
                objReturn.Person_Person_Relation = Data("PERSON_PERSON_RELATION")
            End If
            If Not IsDBNull(Data("RELATION_DATE_RANGE_VALID_FROM")) Then
                objReturn.Relation_Date_Range_Valid_From = Data("RELATION_DATE_RANGE_VALID_FROM")
            End If
            If Not IsDBNull(Data("RELATION_DATE_RANGE_IS_APPROX_FROM_DATE")) Then
                objReturn.Relation_Date_Range_Is_Approx_From_Date = Data("RELATION_DATE_RANGE_IS_APPROX_FROM_DATE")
            End If
            If Not IsDBNull(Data("RELATION_DATE_RANGE_VALID_TO")) Then
                objReturn.Relation_Date_Range_Valid_To = Data("RELATION_DATE_RANGE_VALID_TO")
            End If
            If Not IsDBNull(Data("RELATION_DATE_RANGE_IS_APPROX_TO_DATE")) Then
                objReturn.Relation_Date_Range_Is_Approx_To_Date = Data("RELATION_DATE_RANGE_IS_APPROX_TO_DATE")
            End If
            If Not IsDBNull(Data("COMMENTS")) Then
                objReturn.Comments = Data("COMMENTS")
            End If
            If Not IsDBNull(Data("GENDER")) Then
                objReturn.Gender = Data("GENDER")
            End If
            If Not IsDBNull(Data("TITLE")) Then
                objReturn.Title = Data("TITLE")
            End If
            If Not IsDBNull(Data("FIRST_NAME")) Then
                objReturn.First_Name = Data("FIRST_NAME")
            End If
            If Not IsDBNull(Data("MIDDLE_NAME")) Then
                objReturn.Middle_Name = Data("MIDDLE_NAME")
            End If
            If Not IsDBNull(Data("PREFIX")) Then
                objReturn.Prefix = Data("PREFIX")
            End If
            If Not IsDBNull(Data("LAST_NAME")) Then
                objReturn.Last_Name = Data("LAST_NAME")
            End If
            'If Not IsDBNull(Data("BIRTHDATE")) Then
            '    objReturn.BIRTHDATE = Data("BIRTHDATE")
            'End If
            If Not IsDBNull(Data("BIRTH_PLACE")) Then
                objReturn.Birth_Place = Data("BIRTH_PLACE")
            End If
            If Not IsDBNull(Data("COUNTRY_OF_BIRTH")) Then
                objReturn.Country_Of_Birth = Data("COUNTRY_OF_BIRTH")
            End If
            'If Not IsDBNull(Data("MOTHERS_NAME")) Then
            '    objReturn.MOTHERS_NAME = Data("MOTHERS_NAME")
            'End If
            'If Not IsDBNull(Data("ALIAS")) Then
            '    objReturn.ALIAS = Data("ALIAS")
            'End If
            If Not IsDBNull(Data("FULL_NAME_FRN")) Then
                objReturn.Full_Name_Frn = Data("FULL_NAME_FRN")
            End If
            If Not IsDBNull(Data("SSN")) Then
                objReturn.SSN = Data("SSN")
            End If
            If Not IsDBNull(Data("PASSPORT_NUMBER")) Then
                objReturn.Passport_Number = Data("PASSPORT_NUMBER")
            End If
            If Not IsDBNull(Data("PASSPORT_COUNTRY")) Then
                objReturn.Passport_Country = Data("PASSPORT_COUNTRY")
            End If
            If Not IsDBNull(Data("ID_NUMBER")) Then
                objReturn.ID_Number = Data("ID_NUMBER")
            End If
            If Not IsDBNull(Data("NATIONALITY1")) Then
                objReturn.Nationality1 = Data("NATIONALITY1")
            End If
            If Not IsDBNull(Data("NATIONALITY2")) Then
                objReturn.Nationality2 = Data("NATIONALITY2")
            End If
            If Not IsDBNull(Data("NATIONALITY3")) Then
                objReturn.Nationality3 = Data("NATIONALITY3")
            End If
            If Not IsDBNull(Data("RESIDENCE")) Then
                objReturn.Residence = Data("RESIDENCE")
            End If
            If Not IsDBNull(Data("RESIDENCE_SINCE")) Then
                objReturn.Residence_Since = Data("RESIDENCE_SINCE")
            End If
            If Not IsDBNull(Data("OCCUPATION")) Then
                objReturn.Occupation = Data("OCCUPATION")
            End If
            If Not IsDBNull(Data("DECEASED")) Then
                objReturn.Deceased = Data("DECEASED")
            End If
            If Not IsDBNull(Data("DATE_DECEASED")) Then
                objReturn.Date_Deceased = Data("DATE_DECEASED")
            End If
            If Not IsDBNull(Data("TAX_NUMBER")) Then
                objReturn.Tax_Number = Data("TAX_NUMBER")
            End If
            'If Not IsDBNull(Data("TAX_REG_NUMBER")) Then    
            '    objReturn.TAX_REG_NUMBER = Data("TAX_REG_NUMBER")
            'End If
            If Not IsDBNull(Data("SOURCE_OF_WEALTH")) Then
                objReturn.Source_Of_Wealth = Data("SOURCE_OF_WEALTH")
            End If
            If Not IsDBNull(Data("IS_PROTECTED")) Then
                objReturn.Is_Protected = Data("IS_PROTECTED")
            End If

            If Not IsDBNull(Data("relation_comments")) Then
                objReturn.relation_comments = Data("relation_comments")
            End If

            If Not IsDBNull(Data("EMPLOYER_NAME")) Then
                objReturn.EMPLOYER_NAME = Data("EMPLOYER_NAME")
            End If

            Dim pk_id = Data("PK_goAML_Ref_WIC_Related_Person_ID")
            'goAML_WIC
            objReturn.ObjList_GoAML_Ref_Address = goAML_Customer_Service.GetAddressByPkFk(pk_id, 31)
            objReturn.ObjList_GoAML_Ref_Address_Work = goAML_Customer_Service.GetAddressByPkFk(pk_id, 32)
            objReturn.ObjList_GoAML_Ref_Phone = goAML_Customer_Service.GetPhoneByPkFk(pk_id, 31)
            objReturn.ObjList_GoAML_Ref_Phone_Work = goAML_Customer_Service.GetPhoneByPkFk(pk_id, 32)
            objReturn.ObjList_GoAML_Person_Identification = goAML_Customer_Service.GetIdentificationPkFk(pk_id, 31)
            objReturn.ObjList_GoAML_Ref_Customer_Email = goAML_Customer_Service.WICEmailService.GetByPkFk(pk_id, 31)
            objReturn.ObjList_GoAML_Ref_Customer_Sanction = goAML_Customer_Service.WICSanctionService.GetByPkFk(pk_id, 31)
            objReturn.ObjList_GoAML_Ref_Customer_PEP = goAML_Customer_Service.WICPersonPEPService.GetByPkFk(pk_id, 31)

            listReturn.Add(objReturn)
        Next
        Return listReturn
    End Function
    ' 2023-11-14, Nael: Menambahkan optional parameter fkRefDetail
    Function GetURL(wicNo As String, Optional fkRefDetail As Integer = 0) As List(Of WICDataBLL.goAML_Ref_WIC_URL)
        Dim query = "select * FROM goAML_Ref_WIC_URL where WIC_No = '" & wicNo & "'"

        If fkRefDetail > -1 Then
            query += " AND FK_REF_DETAIL_OF = " & fkRefDetail
        End If

        Dim DataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
        Dim listReturn As New List(Of WICDataBLL.goAML_Ref_WIC_URL) 'Edit Ripan 03-August-2023 ( efek penambahakan di GoAMLDAL, harus dikasih intial object )

        For Each Data As DataRow In DataDT.Rows
            Dim objReturn = New WICDataBLL.goAML_Ref_WIC_URL 'Edit Ripan 03-August-2023 ( efek penambahakan di GoAMLDAL, harus dikasih intial object )
            If Not IsDBNull(Data("PK_goAML_Ref_WIC_URL_ID")) Then
                objReturn.PK_goAML_Ref_WIC_URL_ID = Data("PK_goAML_Ref_WIC_URL_ID")
            End If
            If Not IsDBNull(Data("WIC_No")) Then
                objReturn.WIC_NO = Data("WIC_No")
            End If
            If Not IsDBNull(Data("URL")) Then
                objReturn.URL = Data("URL")
            End If

            listReturn.Add(objReturn)
        Next
        Return listReturn
    End Function

    ' 2023-11-14, Nael: menambahkan optional parameter fkRefDetailOf
    Function GetEntityIdentification(wicNo As String, Optional fkRefDetailOf As Integer = 0) As List(Of WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        Dim query = "select * FROM goAML_Ref_WIC_Entity_Identification where WIC_No = '" & wicNo & "'"
        query += " AND FK_REF_DETAIL_OF = " & fkRefDetailOf

        Dim DataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
        Dim listReturn As New List(Of WICDataBLL.goAML_Ref_WIC_Entity_Identification) 'Edit Ripan 03-August-2023 ( efek penambahakan di GoAMLDAL, harus dikasih intial object )


        For Each Data As DataRow In DataDT.Rows
            Dim objReturn = New WICDataBLL.goAML_Ref_WIC_Entity_Identification 'Edit Ripan 03-August-2023 ( efek penambahakan di GoAMLDAL, harus dikasih intial object )

            If Not IsDBNull(Data("PK_goAML_Ref_WIC_Entity_Identifications_ID")) Then
                objReturn.PK_goAML_Ref_WIC_Entity_Identifications_ID = Data("PK_goAML_Ref_WIC_Entity_Identifications_ID")
            End If
            If Not IsDBNull(Data("WIC_No")) Then
                objReturn.WIC_NO = Data("WIC_No")
            End If
            If Not IsDBNull(Data("TYPE")) Then
                objReturn.TYPE = Data("TYPE")
            End If
            If Not IsDBNull(Data("NUMBER")) Then
                objReturn.NUMBER = Data("NUMBER")
            End If
            If Not IsDBNull(Data("ISSUE_DATE")) Then
                objReturn.ISSUE_DATE = Data("ISSUE_DATE")
            End If
            If Not IsDBNull(Data("EXPIRY_DATE")) Then
                objReturn.EXPIRY_DATE = Data("EXPIRY_DATE")
            End If
            If Not IsDBNull(Data("ISSUE_DATE")) Then
                objReturn.ISSUE_DATE = Data("ISSUE_DATE")
            End If
            If Not IsDBNull(Data("ISSUE_COUNTRY")) Then
                objReturn.ISSUE_COUNTRY = Data("ISSUE_COUNTRY")
            End If
            If Not IsDBNull(Data("ISSUED_BY")) Then
                objReturn.ISSUED_BY = Data("ISSUED_BY")
            End If
            If Not IsDBNull(Data("COMMENTS")) Then
                objReturn.COMMENTS = Data("COMMENTS")
            End If

            listReturn.Add(objReturn)
        Next
        Return listReturn
    End Function

    Function GetRelatedEntity(wicNo As String) As List(Of WICDataBLL.goAML_Ref_WIC_Related_Entity)
        Dim query = "select * FROM goAML_Ref_WIC_Related_Entity where WIC_No = '" & wicNo & "'"
        Dim DataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query, Nothing)
        Dim listReturn As New List(Of WICDataBLL.goAML_Ref_WIC_Related_Entity)

        For Each Data As DataRow In DataDT.Rows
            Dim objReturn = New WICDataBLL.goAML_Ref_WIC_Related_Entity

            If Not IsDBNull(Data("PK_goAML_Ref_WIC_Related_Entities_ID")) Then
                objReturn.PK_goAML_Ref_WIC_Related_Entities_ID = Data("PK_goAML_Ref_WIC_Related_Entities_ID")
            End If
            If Not IsDBNull(Data("WIC_No")) Then
                objReturn.WIC_NO = Data("WIC_No")
            End If
            If Not IsDBNull(Data("ENTITY_ENTITY_RELATION")) Then
                objReturn.ENTITY_ENTITY_RELATION = Data("ENTITY_ENTITY_RELATION")
            End If
            If Not IsDBNull(Data("RELATION_DATE_RANGE_VALID_FROM")) Then
                objReturn.RELATION_DATE_RANGE_VALID_FROM = Data("RELATION_DATE_RANGE_VALID_FROM")
            End If
            If Not IsDBNull(Data("RELATION_DATE_RANGE_IS_APPROX_FROM_DATE")) Then
                objReturn.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = Data("RELATION_DATE_RANGE_IS_APPROX_FROM_DATE")
            End If
            If Not IsDBNull(Data("RELATION_DATE_RANGE_VALID_TO")) Then
                objReturn.RELATION_DATE_RANGE_VALID_TO = Data("RELATION_DATE_RANGE_VALID_TO")
            End If
            If Not IsDBNull(Data("RELATION_DATE_RANGE_IS_APPROX_TO_DATE")) Then
                objReturn.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = Data("RELATION_DATE_RANGE_IS_APPROX_TO_DATE")
            End If
            If Not IsDBNull(Data("SHARE_PERCENTAGE")) Then
                objReturn.SHARE_PERCENTAGE = Data("SHARE_PERCENTAGE")
            End If
            If Not IsDBNull(Data("COMMENTS")) Then
                objReturn.COMMENTS = Data("COMMENTS")
            End If
            If Not IsDBNull(Data("NAME")) Then
                objReturn.NAME = Data("NAME")
            End If
            If Not IsDBNull(Data("COMMERCIAL_NAME")) Then
                objReturn.COMMERCIAL_NAME = Data("COMMERCIAL_NAME")
            End If
            If Not IsDBNull(Data("INCORPORATION_LEGAL_FORM")) Then
                objReturn.INCORPORATION_LEGAL_FORM = Data("INCORPORATION_LEGAL_FORM")
            End If
            If Not IsDBNull(Data("INCORPORATION_NUMBER")) Then
                objReturn.INCORPORATION_NUMBER = Data("INCORPORATION_NUMBER")
            End If
            If Not IsDBNull(Data("BUSINESS")) Then
                objReturn.BUSINESS = Data("BUSINESS")
            End If
            If Not IsDBNull(Data("ENTITY_STATUS")) Then
                objReturn.ENTITY_STATUS = Data("ENTITY_STATUS")
            End If
            If Not IsDBNull(Data("ENTITY_STATUS_DATE")) Then
                objReturn.ENTITY_STATUS_DATE = Data("ENTITY_STATUS_DATE")
            End If
            If Not IsDBNull(Data("INCORPORATION_STATE")) Then
                objReturn.INCORPORATION_STATE = Data("INCORPORATION_STATE")
            End If
            If Not IsDBNull(Data("INCORPORATION_COUNTRY_CODE")) Then
                objReturn.INCORPORATION_COUNTRY_CODE = Data("INCORPORATION_COUNTRY_CODE")
            End If
            If Not IsDBNull(Data("INCORPORATION_DATE")) Then
                objReturn.INCORPORATION_DATE = Data("INCORPORATION_DATE")
            End If
            If Not IsDBNull(Data("BUSINESS_CLOSED")) Then
                objReturn.BUSINESS_CLOSED = Data("BUSINESS_CLOSED")
            End If
            If Not IsDBNull(Data("DATE_BUSINESS_CLOSED")) Then
                objReturn.DATE_BUSINESS_CLOSED = Data("DATE_BUSINESS_CLOSED")
            End If
            If Not IsDBNull(Data("TAX_NUMBER")) Then
                objReturn.TAX_NUMBER = Data("TAX_NUMBER")
            End If
            If Not IsDBNull(Data("TAX_REG_NUMBER")) Then
                objReturn.TAX_REG_NUMBER = Data("TAX_REG_NUMBER")
            End If
            ' 2023-11-15, Nael:Add
            If Not IsDBNull(Data("RELATION_COMMENTS")) Then
                objReturn.RELATION_COMMENTS = Data("RELATION_COMMENTS")
            End If

            objReturn.ObjList_GoAML_Ref_Customer_Phone = goAML_Customer_Service.GetPhoneByPkFk(objReturn.PK_goAML_Ref_WIC_Related_Entities_ID, 36)
            objReturn.ObjList_GoAML_Ref_Customer_Address = goAML_Customer_Service.GetAddressByPkFk(objReturn.PK_goAML_Ref_WIC_Related_Entities_ID, 36)
            objReturn.ObjList_GoAML_Ref_Customer_Email = goAML_Customer_Service.WICEmailService.GetByPkFk(objReturn.PK_goAML_Ref_WIC_Related_Entities_ID, 36)
            objReturn.ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Customer_Service.WICEntityIdentificationService.GetByPkFk(objReturn.PK_goAML_Ref_WIC_Related_Entities_ID, 36)
            objReturn.ObjList_GoAML_Ref_Customer_URL = goAML_Customer_Service.WICUrlService.GetByPkFk(objReturn.PK_goAML_Ref_WIC_Related_Entities_ID, 36)
            objReturn.ObjList_GoAML_Ref_Customer_Sanction = goAML_Customer_Service.WICSanctionService.GetByPkFk(objReturn.PK_goAML_Ref_WIC_Related_Entities_ID, 36)

            listReturn.Add(objReturn)
        Next
        Return listReturn
    End Function

    Function WrapSqlVariable(variable As Object) As String
        Dim ret = ""
        If variable Is Nothing Then
            ret = "NULL"
        Else
            If TypeOf variable Is Boolean Then
                ret = If(variable, "1", "0")
            ElseIf TypeOf variable Is Integer Then
                ret = "" & variable & ""
            Else
                ret = "'" & variable & "'"
            End If
        End If

        Return ret
    End Function
    'end add by Septian
#End Region
End Class
