﻿'==========================================
'Created Date   : 22 Sep 2021
'Created By     : NawaData
'Description    : Library for goAML Report
'                 Coba buat tanpa DAL (Entity)
'==========================================

Imports GoAMLDAL

Public Class GoAML_Parameter_BLL

    Shared Function GetgoAML_Ref_Submission_TypeByCode(strCode As String) As goAML_Ref_Submission_Type
        Try
            Dim objResult As goAML_Ref_Submission_Type = Nothing
            Using objDb As New GoAMLEntities
                objResult = objDb.goAML_Ref_Submission_Type.Where(Function(x) x.Kode = strCode).FirstOrDefault
            End Using
            Return objResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function GetgoAML_Ref_Jenis_LaporanByCode(strCode As String) As goAML_Ref_Jenis_Laporan
        Try
            Dim objResult As goAML_Ref_Jenis_Laporan = Nothing
            Using objDb As New GoAMLEntities
                objResult = objDb.goAML_Ref_Jenis_Laporan.Where(Function(x) x.Kode = strCode).FirstOrDefault
            End Using
            Return objResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function GetGoAML_Laporan_Req_PPATKByCode(strCode As String) As GoAML_Laporan_Req_PPATK
        Try
            Dim objResult As GoAML_Laporan_Req_PPATK = Nothing
            Using objDb As New GoAMLEntities
                objResult = objDb.GoAML_Laporan_Req_PPATK.Where(Function(x) x.Kode = strCode).FirstOrDefault
            End Using
            Return objResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function GetgoAML_odm_ref_Report_TRNorACTByCode(strCode As String) As goAML_odm_ref_Report_TRNorACT
        Try
            Dim objResult As goAML_odm_ref_Report_TRNorACT = Nothing
            Using objDb As New GoAMLEntities
                objResult = objDb.goAML_odm_ref_Report_TRNorACT.Where(Function(x) x.ReportType = strCode).FirstOrDefault
            End Using
            Return objResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function GetGoAML_Laporan_With_IndicatorByCode(strCode As String) As GoAML_Laporan_With_Indicator
        Try
            Dim objResult As GoAML_Laporan_With_Indicator = Nothing
            Using objDb As New GoAMLEntities
                objResult = objDb.GoAML_Laporan_With_Indicator.Where(Function(x) x.ReportType = strCode).FirstOrDefault
            End Using
            Return objResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function
End Class
