﻿Imports System.ComponentModel
Imports System.Data.Entity.Core.Mapping
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net.Mail
Imports System.Runtime.CompilerServices
Imports System.Text
Imports Ext.Net
Imports NawaBLL
Imports NawaDAL
Imports GoAMLBLL.goAML
Imports GoAMLBLL.goAML.DataAccess
Imports GoAMLBLL.goAML.Enum
Imports GoAMLBLL.goAML.Services
Imports GoAMLBLL.goAML_Customer.DataAccess
Imports GoAMLBLL.goAML_CustomerDataBLL
Imports GoAMLDAL
Imports OfficeOpenXml.ExcelErrorValue
Imports OfficeOpenXml.FormulaParsing.Excel.Functions.Logical


Public Class goAML_CustomerBLL
    Implements IDisposable

    Shared Function GetVWCustomerPhones(ID As String) As List(Of GoAMLDAL.Vw_Customer_Phones)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Customer_Phones Where x.FK_for_Table_ID = ID Select x).ToList()
        End Using
    End Function
    '' Edited on 04 Dec 2020
    Shared Function getDataApproval(id As String, moduleName As String) As GoAMLDAL.ModuleApproval
        Dim objModuleApproval As New GoAMLDAL.ModuleApproval
        Using objdb As New GoAMLEntities
            Dim Approval As GoAMLDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.ModuleKey = id And x.ModuleName = moduleName).FirstOrDefault
            'If Approval IsNot Nothing Then

            'End If
            objModuleApproval = Approval
        End Using
        Return objModuleApproval
    End Function

    Shared Function getPKCustomer(id As String) As String
        Dim PKCustomer = ""
        Using objdb As New GoAMLEntities
            Dim PK As String = objdb.goAML_Ref_Customer.Where(Function(x) x.CIF = id).FirstOrDefault.PK_Customer_ID
            If PK IsNot Nothing Then
                PKCustomer = PK
            End If
        End Using
        Return PKCustomer
    End Function
    '' End of edit on 04 Dec 2020

    Shared Function GetVWCustomerAddresses(ID As String) As List(Of GoAMLDAL.Vw_Customer_Addresses)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Customer_Addresses Where x.FK_To_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetVWCustomerAddressesbyID(ID As String) As GoAMLDAL.Vw_Customer_Addresses
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Customer_Addresses Where x.FK_To_Table_ID = ID Select x).FirstOrDefault
        End Using
    End Function

    Shared Function GetVWCustomerAddressesbyPK(ID As String) As GoAMLDAL.Vw_Customer_Addresses
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Customer_Addresses Where x.PK_Customer_Address_ID = ID Select x).FirstOrDefault
        End Using
    End Function

    Shared Function GetVWDirectorPhones(ID As String) As List(Of GoAMLDAL.Vw_Director_Phones)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Director_Phones Where x.FK_for_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetVWDirectorAddresses(ID As String) As List(Of GoAMLDAL.Vw_Director_Addresses)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Director_Addresses Where x.FK_To_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetDirectorAddresses(ID As String) As List(Of GoAMLDAL.goAML_Ref_Address)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Address Where x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 6 Select x).ToList()
        End Using
    End Function

    Shared Function GetDirectorPhone(ID As String) As List(Of GoAMLDAL.goAML_Ref_Phone)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Phone Where x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 6 Select x).ToList()
        End Using
    End Function

    Shared Function GetCustomerAddresses(ID As String) As List(Of GoAMLDAL.goAML_Ref_Address)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Address Where x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 1 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWCustomerDirector(ID As String) As List(Of GoAMLDAL.Vw_Customer_Director)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Customer_Director Where x.FK_Entity_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetCustomerDirector(ID As String) As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Customer_Entity_Director Where x.FK_Entity_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetCustomerDirectorByPK(ID As String) As GoAMLDAL.goAML_Ref_Customer_Entity_Director
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Customer_Entity_Director Where x.FK_Entity_ID = ID Select x).FirstOrDefault
        End Using
    End Function

    Shared Function GetCustomerDirectorByPKDirector(ID As String) As GoAMLDAL.goAML_Ref_Customer_Entity_Director
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Customer_Entity_Director Where x.PK_goAML_Ref_Customer_Entity_Director_ID = ID Select x).FirstOrDefault
        End Using
    End Function

    Shared Function GetVWEmployerAddresses(ID As String) As List(Of GoAMLDAL.Vw_Employer_Addresses)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Employer_Addresses Where x.FK_To_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetEmployerAddresses(ID As String) As List(Of GoAMLDAL.goAML_Ref_Address)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Address Where x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 5 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWEmployerPhones(ID As String) As List(Of GoAMLDAL.Vw_Employer_Phones)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Employer_Phones Where x.FK_for_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetEmployerPhones(ID As String) As List(Of GoAMLDAL.goAML_Ref_Phone)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Phone Where x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 5 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWDirectorEmployerAddresses(ID As String) As List(Of GoAMLDAL.Vw_Director_Employer_Addresses)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Director_Employer_Addresses Where x.FK_To_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetDirectorEmployerAddresses(ID As String) As List(Of GoAMLDAL.goAML_Ref_Address)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Address Where x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 7 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWDirectorEmployerPhones(ID As String) As List(Of GoAMLDAL.Vw_Director_Employer_Phones)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Director_Employer_Phones Where x.FK_for_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetDirectorEmployerPhones(ID As String) As List(Of GoAMLDAL.goAML_Ref_Phone)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Phone Where x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 7 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWCustomerIdentifications(ID As String) As List(Of GoAMLDAL.Vw_Person_Identification)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Person_Identification Where x.FK_Person_ID = ID And x.FK_Person_Type = 1 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWDirectorIdentifications(ID As String) As List(Of GoAMLDAL.Vw_Person_Identification)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Person_Identification Where x.FK_Person_ID = ID And x.FK_Person_Type = 4 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWDirectorIdentificationsbypk(ID As String) As List(Of GoAMLDAL.Vw_Person_Identification)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Person_Identification Where x.PK_Person_Identification_ID = ID And x.FK_Person_Type = 4 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWCustomerIdentificationsbyPK(ID As String) As GoAMLDAL.Vw_Person_Identification
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Person_Identification Where x.FK_Person_ID = ID And x.FK_Person_Type = 1 Select x).FirstOrDefault()
        End Using
    End Function

    Shared Function GetVWCustomerIdentificationsbyPKPersonIdentification(ID As String) As GoAMLDAL.Vw_Person_Identification
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Person_Identification Where x.PK_Person_Identification_ID = ID And x.FK_Person_Type = 1 Select x).FirstOrDefault()
        End Using
    End Function

    Shared Function GetCustomerIdentifications(ID As String) As List(Of GoAMLDAL.goAML_Person_Identification)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Person_Identification Where x.FK_Person_ID = ID And x.FK_Person_Type = 1 Select x).ToList()
        End Using
    End Function

    Shared Function GetDirectorIdentifications(ID As String) As List(Of GoAMLDAL.goAML_Person_Identification)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Person_Identification Where x.FK_Person_ID = ID And x.FK_Person_Type = 4 Select x).ToList()
        End Using
    End Function

    Shared Function GetCustomerIdentificationbyPkID(ID As String) As GoAMLDAL.goAML_Person_Identification
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Person_Identification Where x.PK_Person_Identification_ID = ID Select x).FirstOrDefault()
        End Using
    End Function

    Shared Function GetCustomerPhones(ID As String) As List(Of GoAMLDAL.goAML_Ref_Phone)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Phone Where x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 1 Select x).ToList()
        End Using
    End Function

    Shared Function GetCustomer(ID As Integer) As GoAMLDAL.goAML_Ref_Customer

        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Customer.Where(Function(x) x.PK_Customer_ID = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetCustomerbyCIF(CIF As String) As GoAMLDAL.goAML_Ref_Customer

        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = CIF).FirstOrDefault
        End Using
    End Function

    Shared Function GetListgoAML_Ref_PhoneByPKID(ID As Integer) As List(Of GoAMLDAL.goAML_Ref_Phone)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 1).ToList
        End Using
    End Function

    Shared Function GetListgoAML_Ref_AddressByPKID(ID As Integer) As List(Of GoAMLDAL.goAML_Ref_Address)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 1).ToList
        End Using
    End Function
    Shared Function GetTabelTypeForByID(id As Integer) As GoAMLDAL.goAML_For_Table
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_For_Table.Where(Function(x) x.PK_For_Table_ID = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetContactTypeByID(id As String) As GoAMLDAL.goAML_Ref_Kategori_Kontak
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetCommunicationTypeByID(id As String) As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetAddressTypeByID(id As String) As GoAMLDAL.goAML_Ref_Kategori_Kontak
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetAddressByID(id As String) As GoAMLDAL.goAML_Ref_Address
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetPhonesByID(id As String) As GoAMLDAL.goAML_Ref_Phone
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetNamaNegaraByID(id As String) As GoAMLDAL.goAML_Ref_Nama_Negara
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetNamaNegaraByKeterangan(id As String) As GoAMLDAL.goAML_Ref_Nama_Negara
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Nama_Negara.Where(Function(x) x.Keterangan = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetJenisDokumenByID(id As String) As GoAMLDAL.goAML_Ref_Jenis_Dokumen_Identitas
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Jenis_Dokumen_Identitas.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetNameDirectorByID(id As String) As GoAMLDAL.goAML_Ref_Customer
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = id And x.FK_Customer_Type_ID = 1).FirstOrDefault
        End Using
    End Function

    Shared Function GetRoleDirectorByID(id As String) As GoAMLDAL.goAML_Ref_Peran_orang_dalam_Korporasi
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Peran_orang_dalam_Korporasi.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetBentukBadanUsahaByID(id As String) As GoAMLDAL.goAML_Ref_Bentuk_Badan_Usaha
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Bentuk_Badan_Usaha.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetListKategoriKontak() As List(Of GoAMLDAL.goAML_Ref_Kategori_Kontak)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Kategori_Kontak.ToList
        End Using
    End Function
    'Shared Function GetListNamaNegara() As List(Of GoAMLDAL.goAML_Ref_Nama_Negara)
    '    Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
    '        Return objDb.goAML_Ref_Nama_Negara.ToList
    '    End Using
    'End Function


    Shared Function GetListTypeKomunikasi() As List(Of GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Jenis_Alat_Komunikasi.ToList
        End Using
    End Function

    Shared Function GetListCustomerType() As List(Of GoAMLDAL.goAML_Ref_Customer_Type)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Customer_Type.ToList
        End Using
    End Function

    Shared Function GetListStatusRekening() As List(Of GoAMLDAL.goAML_Ref_Status_Rekening)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Status_Rekening.ToList
        End Using
    End Function

    Shared Function GetStatusRekeningbyID(ID As String) As GoAMLDAL.goAML_Ref_Status_Rekening
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetPartyRolebyID(ID As String) As GoAMLDAL.goAML_Ref_Party_Role
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Party_Role.Where(Function(x) x.Kode = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetListJenisKelamin() As List(Of GoAMLDAL.goAML_Ref_Jenis_Kelamin)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Jenis_Kelamin.ToList
        End Using
    End Function

    Shared Function GetJenisKelamin(Kode As String) As GoAMLDAL.goAML_Ref_Jenis_Kelamin
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Jenis_Kelamin.Where(Function(x) x.Kode = Kode).FirstOrDefault
        End Using
    End Function


    Shared Function GetNamaNegara(Kode As String) As GoAMLDAL.goAML_Ref_Nama_Negara
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = Kode).FirstOrDefault
        End Using
    End Function

    Shared Function GetListNamaNegara() As List(Of GoAMLDAL.goAML_Ref_Nama_Negara)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Nama_Negara.ToList
        End Using
    End Function
    'Update: Zikri_14092020 Add Validation GCN
    Shared Function GetCheckGCN(id As String) As GoAMLDAL.goAML_Ref_Customer
        Using ObjDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim result As GoAMLDAL.goAML_Ref_Customer = ObjDb.goAML_Ref_Customer.Where(Function(x) x.GCN = id And x.isGCNPrimary = True).FirstOrDefault
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    'End Update
    Shared Function GetListBentukBadanUsaha() As List(Of GoAMLDAL.goAML_Ref_Bentuk_Badan_Usaha)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Bentuk_Badan_Usaha.ToList
        End Using
    End Function

    Shared Function GetListPartyRole() As List(Of GoAMLDAL.goAML_Ref_Party_Role)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Party_Role.ToList
        End Using
    End Function

    Shared Function getlistjenisdokumenidentitas() As List(Of GoAMLDAL.goAML_Ref_Jenis_Dokumen_Identitas)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Jenis_Dokumen_Identitas.ToList
        End Using
    End Function

    Shared Function GetListPeranDalamKorporasi() As List(Of GoAMLDAL.goAML_Ref_Peran_orang_dalam_Korporasi)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Peran_orang_dalam_Korporasi.ToList
        End Using
    End Function

    'Shared Function ifticopy(pkiftiid As Integer, userid As String)

    '    Dim objListParam(1) As SqlParameter
    '    objListParam(0) = New SqlParameter
    '    objListParam(0).ParameterName = "@Pkiftiid"
    '    objListParam(0).Value = pkiftiid

    '    objListParam(1) = New SqlParameter
    '    objListParam(1).ParameterName = "@userid"
    '    objListParam(1).Value = userid


    '    Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_IFTI_Copy", objListParam)
    'End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    Function SaveEditApproval(objData As GoAMLDAL.goAML_Ref_Customer, objListgoAML_Ref_Director As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director), objModule As NawaDAL.Module, objListgoAML_Ref_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification As List(Of GoAMLDAL.goAML_Person_Identification), objListgoAML_Ref_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Employer_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Employer_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification_Director As List(Of GoAMLDAL.goAML_Person_Identification), objData2 As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2, objList As GoAMLBLL.goAML_CustomerDataBLL.goAML_Grid_Customer) As Boolean
        'done:code  SaveAddApproval
        Using objdb As New NawaDAL.NawaDataEntities
            Using objdbDev As New GoAMLDAL.GoAMLEntities
                Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                    Try
                        Dim objxData As New GoAMLBLL.goAML_CustomerDataBLL
                        objxData.objgoAML_Ref_Customer = objData
                        objxData.objListgoAML_Ref_Director = objListgoAML_Ref_Director
                        objxData.objListgoAML_Ref_Phone = objListgoAML_Ref_Phone
                        objxData.objlistgoaml_Ref_Address = objListgoAML_Ref_Address
                        objxData.objListgoAML_Ref_Identification = objListgoAML_Ref_Identification
                        objxData.objListgoAML_Ref_Employer_Phone = objListgoAML_Ref_Employer_Phone
                        objxData.objlistgoaml_Ref_Employer_Address = objListgoAML_Ref_Employer_Address
                        objxData.objListgoAML_Ref_Director_Address = objListgoAML_Ref_Director_Address
                        objxData.objListgoAML_Ref_Director_Phone = objListgoAML_Ref_Director_Phone
                        objxData.objListgoAML_Ref_Director_Employer_Phone = objListgoAML_Ref_Director_Employer_Phone
                        objxData.objlistgoaml_Ref_Director_Employer_Address = objListgoAML_Ref_Director_Employer_Address
                        objxData.objListgoAML_Ref_Identification_Director = objListgoAML_Ref_Identification_Director

                        '' Add by Septian, goAML 5.0.1, 2021-01-30
                        objxData.objgoAML_Ref_Customer2 = objData2
                        objxData.ObjList_GoAML_Ref_Customer_PreviousName = objList.ObjList_GoAML_PreviousName
                        objxData.ObjList_GoAML_Ref_Customer_Email = objList.ObjList_GoAML_Email
                        objxData.ObjList_GoAML_Ref_Customer_EmploymentHistory = objList.ObjList_GoAML_EmploymentHistory
                        objxData.ObjList_GoAML_Ref_Customer_PEP = objList.ObjList_GoAML_PEP
                        objxData.ObjList_GoAML_Ref_Customer_NetworkDevice = objList.ObjList_GoAML_NetworkDevice
                        objxData.ObjList_GoAML_Ref_Customer_SocialMedia = objList.ObjList_GoAML_SocialMedia
                        objxData.ObjList_GoAML_Ref_Customer_Sanction = objList.ObjList_GoAML_Sanction
                        objxData.ObjList_GoAML_Ref_Customer_RelatedPerson = objList.ObjList_GoAML_RelatedPerson
                        objxData.ObjList_GoAML_Ref_Customer_AdditionalInformation = objList.ObjList_GoAML_AdditionalInformation
                        objxData.ObjList_GoAML_Ref_Customer_URL = objList.ObjList_GoAML_URL
                        objxData.ObjList_GoAML_Ref_Customer_EntityIdentification = objList.ObjList_GoAML_EntityIdentification
                        objxData.ObjList_GoAML_Ref_Customer_RelatedEntities = objList.ObjList_GoAML_RelatedEntities
                        objxData.ObjListgoAML_Ref_Director2 = objList.objList_GoAML_Ref_Director
                        '' End Add

                        Dim xmldata As String = NawaBLL.Common.Serialize(objxData)

                        Dim objgoAML_CustomerBefore As GoAMLDAL.goAML_Ref_Customer = objdbDev.goAML_Ref_Customer.Where(Function(x) x.PK_Customer_ID = objxData.objgoAML_Ref_Customer.PK_Customer_ID).FirstOrDefault
                        Dim objListgoAML_Ref_DirectorBefore As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director) = objdbDev.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.FK_Entity_ID = objxData.objgoAML_Ref_Customer.CIF).ToList
                        Dim objListgoAML_Ref_IdentificationBefore As List(Of GoAMLDAL.goAML_Person_Identification) = objdbDev.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = objxData.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Person_Type = 1).ToList
                        Dim objListgoAML_Ref_PhoneBefore As List(Of GoAMLDAL.goAML_Ref_Phone) = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objxData.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Ref_Detail_Of = "1").ToList
                        Dim objListgoAML_Ref_AddressBefore As List(Of GoAMLDAL.goAML_Ref_Address) = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objxData.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Ref_Detail_Of = "1").ToList
                        Dim objListgoAML_Ref_Employer_PhoneBefore As List(Of GoAMLDAL.goAML_Ref_Phone) = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objxData.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Ref_Detail_Of = "5").ToList
                        Dim objListgoAML_Ref_Employer_AddressBefore As List(Of GoAMLDAL.goAML_Ref_Address) = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objxData.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Ref_Detail_Of = "5").ToList


                        Dim objListgoAML_Ref_Director_PhoneBefore As New List(Of GoAMLDAL.goAML_Ref_Phone)
                        Dim objListgoAML_Ref_Director_AddressBefore As New List(Of GoAMLDAL.goAML_Ref_Address)
                        Dim objListgoAML_Ref_Director_Employer_PhoneBefore As New List(Of GoAMLDAL.goAML_Ref_Phone)
                        Dim objListgoAML_Ref_Director_Employer_AddressBefore As New List(Of GoAMLDAL.goAML_Ref_Address)
                        Dim objListgoAML_Ref_IdentificationDirectorBefore As New List(Of GoAMLDAL.goAML_Person_Identification)

                        '' Add by Septian, goAML 5.0.1, 2021-01-30
                        Dim obj_goAML_Customer2Before As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2 = GetCustomerbyCIF2(objData.CIF)
                        Dim objList_goAML_PreviousNameBefore = GetPreviousName(objData.CIF)
                        Dim objList_goAML_EmailBefore = GetEmail(objData.CIF)
                        Dim objList_goAML_EmploymentHistoryBefore = GetEmploymentHistory(objData.CIF)
                        Dim objList_goAML_PEPBefore = GetPEP(objData.CIF)
                        Dim objList_goAML_NetworkDeviceBefore = GetNetworkDevice(objData.CIF)
                        Dim objList_goAML_SocialMediaBefore = GetSocialMedia(objData.CIF)
                        Dim objList_goAML_SanctionBefore = GetSanction(objData.CIF)
                        Dim objList_goAML_RelatedPersonBefore = goAML_Customer_Service.RelatedPersonService.GetCompleteByCIF(objData.CIF) 'GetRelatedPerson(objData.CIF)
                        Dim objList_goAML_AdditionalInformationBefore = GetAdditionalInformation(objData.CIF)
                        Dim objList_goAML_URLBefore = GetURL(objData.CIF)
                        Dim objList_goAML_EntityIdentificationBefore = GetEntityIdentification(objData.CIF)
                        Dim objList_goAML_RelatedEntitiesBefore = goAML_Customer_Service.RelatedEntityService.GetCompleteByCIF(objData.CIF) 'GetRelatedEntities(objData.CIF)
                        Dim objList_goAML_DirectorBefore = goAML_Customer_Service.DirectorService.GetCompleteByCIF(objData.CIF) 'GetRelatedEntities(objData.CIF)

                        '' End Add

                        If objData.FK_Customer_Type_ID = 2 Then
                            'For Each item As GoAMLDAL.goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_DirectorBefore
                            '    objListgoAML_Ref_Director_PhoneBefore = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "6").ToList
                            '    objListgoAML_Ref_Director_AddressBefore = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "6").ToList
                            '    objListgoAML_Ref_Director_Employer_PhoneBefore = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "7").ToList
                            '    objListgoAML_Ref_Director_Employer_AddressBefore = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "7").ToList
                            '    objListgoAML_Ref_IdentificationDirectorBefore = objdbDev.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Person_Type = "4").ToList
                            'Next
                            'Update: Try_28092020
                            For Each item As GoAMLDAL.goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_DirectorBefore

                                Dim objphonenew As New List(Of goAML_Ref_Phone)

                                Dim objaddressnew As New List(Of goAML_Ref_Address)

                                Dim objempaddressnew As New List(Of goAML_Ref_Address)

                                Dim objempphonenew As New List(Of goAML_Ref_Phone)

                                Dim objidentificationew As New List(Of goAML_Person_Identification)

                                objphonenew = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "6").ToList

                                objaddressnew = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "6").ToList

                                objempaddressnew = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "7").ToList

                                objempphonenew = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "7").ToList

                                objidentificationew = objdbDev.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Person_Type = "4").ToList

                                For Each item1 In objphonenew

                                    objListgoAML_Ref_Director_PhoneBefore.Add(item1)

                                Next

                                For Each item2 In objaddressnew

                                    objListgoAML_Ref_Director_AddressBefore.Add(item2)

                                Next

                                For Each item3 In objempaddressnew
                                    objListgoAML_Ref_Director_Employer_AddressBefore.Add(item3)
                                Next

                                For Each item4 In objempphonenew
                                    objListgoAML_Ref_Director_Employer_PhoneBefore.Add(item4)
                                Next

                                For Each item5 In objidentificationew
                                    objListgoAML_Ref_IdentificationDirectorBefore.Add(item5)
                                Next
                            Next
                            'End Update
                        Else
                            objListgoAML_Ref_Director_PhoneBefore = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList
                            objListgoAML_Ref_Director_AddressBefore = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList
                            objListgoAML_Ref_Director_Employer_PhoneBefore = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList
                            objListgoAML_Ref_Director_Employer_AddressBefore = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList
                            objListgoAML_Ref_IdentificationDirectorBefore = objdbDev.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = 0).ToList
                        End If

                        Dim objxDatabefore As New GoAMLBLL.goAML_CustomerDataBLL
                        objxDatabefore.objgoAML_Ref_Customer = objgoAML_CustomerBefore
                        objxDatabefore.objListgoAML_Ref_Director = objListgoAML_Ref_DirectorBefore
                        objxDatabefore.objListgoAML_Ref_Identification = objListgoAML_Ref_IdentificationBefore
                        objxDatabefore.objListgoAML_Ref_Phone = objListgoAML_Ref_PhoneBefore
                        objxDatabefore.objlistgoaml_Ref_Address = objListgoAML_Ref_AddressBefore
                        objxDatabefore.objListgoAML_Ref_Employer_Phone = objListgoAML_Ref_Employer_PhoneBefore
                        objxDatabefore.objlistgoaml_Ref_Employer_Address = objListgoAML_Ref_Employer_AddressBefore
                        objxDatabefore.objListgoAML_Ref_Director_Phone = objListgoAML_Ref_Director_PhoneBefore
                        objxDatabefore.objListgoAML_Ref_Director_Address = objListgoAML_Ref_Director_AddressBefore
                        objxDatabefore.objListgoAML_Ref_Director_Employer_Phone = objListgoAML_Ref_Director_Employer_PhoneBefore
                        objxDatabefore.objlistgoaml_Ref_Director_Employer_Address = objListgoAML_Ref_Director_Employer_AddressBefore
                        objxDatabefore.objListgoAML_Ref_Identification_Director = objListgoAML_Ref_IdentificationDirectorBefore

                        '' Add by Septian, goAML 5.0.1, 2021-01-30
                        objxDatabefore.objgoAML_Ref_Customer2 = obj_goAML_Customer2Before
                        objxDatabefore.ObjList_GoAML_Ref_Customer_PreviousName = objList_goAML_PreviousNameBefore
                        objxDatabefore.ObjList_GoAML_Ref_Customer_Email = objList_goAML_EmailBefore
                        objxDatabefore.ObjList_GoAML_Ref_Customer_EmploymentHistory = objList_goAML_EmploymentHistoryBefore
                        objxDatabefore.ObjList_GoAML_Ref_Customer_PEP = objList_goAML_PEPBefore
                        objxDatabefore.ObjList_GoAML_Ref_Customer_NetworkDevice = objList_goAML_NetworkDeviceBefore
                        objxDatabefore.ObjList_GoAML_Ref_Customer_SocialMedia = objList_goAML_SocialMediaBefore
                        objxDatabefore.ObjList_GoAML_Ref_Customer_Sanction = objList_goAML_SanctionBefore
                        objxDatabefore.ObjList_GoAML_Ref_Customer_RelatedPerson = objList_goAML_RelatedPersonBefore
                        objxDatabefore.ObjList_GoAML_Ref_Customer_AdditionalInformation = objList_goAML_AdditionalInformationBefore
                        objxDatabefore.ObjList_GoAML_Ref_Customer_URL = objList_goAML_URLBefore
                        objxDatabefore.ObjList_GoAML_Ref_Customer_EntityIdentification = objList_goAML_EntityIdentificationBefore
                        objxDatabefore.ObjList_GoAML_Ref_Customer_RelatedEntities = objList_goAML_RelatedEntitiesBefore
                        objxDatabefore.ObjListgoAML_Ref_Director2 = objList_goAML_DirectorBefore
                        '' End Add

                        Dim xmlbefore As String = NawaBLL.Common.Serialize(objxDatabefore)

                        Dim objModuleApproval As New NawaDAL.ModuleApproval
                        With objModuleApproval
                            .ModuleName = objModule.ModuleName
                            .ModuleKey = objxData.objgoAML_Ref_Customer.PK_Customer_ID
                            .ModuleField = xmldata
                            .ModuleFieldBefore = xmlbefore
                            .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                            .CreatedDate = Now
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        End With

                        objdb.Entry(objModuleApproval).State = Entity.EntityState.Added

                        ' ======================= START: AUDIT TRAIL ================================
                        Using objDba As New GoAMLEntities
                            Dim header As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)

                            'AuditTrailDetail, NaelEdit
                            ' 2023-09-29, Nael: Menambahkan Audit Trail
                            ' CreateAuditTrailDetailEdit
                            'objData
                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, objData, objgoAML_CustomerBefore)
                            objDba.SaveChanges()

                            ' ============ Customer Phones ============
                            If objListgoAML_Ref_Phone.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newUserPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                                    Dim oldUserPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newUserPhone.PK_goAML_Ref_Phone))
                                    If oldUserPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserPhone, oldUserPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldUserPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                                    Dim newUserPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldUserPhone.PK_goAML_Ref_Phone))
                                    If newUserPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserPhone, oldUserPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            ' ========== Customer Address ===========
                            If objListgoAML_Ref_Address.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newUserAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                                    Dim oldUserAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newUserAddress.PK_Customer_Address_ID))
                                    If oldUserAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserAddress, oldUserAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldUserAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                                    Dim newUserAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldUserAddress.PK_Customer_Address_ID))
                                    If newUserAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserAddress, oldUserAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objData.FK_Customer_Type_ID = 1 Then
                                ' ======== Person Identification =========
                                If objListgoAML_Ref_Identification.Count > 0 Then
                                    ' 2023-10-02, Nael: Cek data new di old
                                    For Each newUserIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                                        Dim oldUserIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(newUserIdentification.PK_Person_Identification_ID))
                                        If oldUserIdentification IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserIdentification, oldUserIdentification)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserIdentification)
                                        End If
                                        objDba.SaveChanges()
                                    Next

                                    ' 2023-10-02, Nael: Cek data old di new
                                    For Each oldUserIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                                        Dim newUserIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(oldUserIdentification.PK_Person_Identification_ID))
                                        If newUserIdentification IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserIdentification, oldUserIdentification)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserIdentification)
                                        End If
                                        objDba.SaveChanges()
                                    Next
                                End If

                                ' ======== Employer Address ==========
                                If objListgoAML_Ref_Employer_Address.Count > 0 Then
                                    ' 2023-10-02, Nael: Cek data new di old
                                    For Each newEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                                        Dim oldEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newEmployerAddress.PK_Customer_Address_ID))
                                        If oldEmployerAddress IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerAddress, oldEmployerAddress)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newEmployerAddress)
                                        End If
                                        objDba.SaveChanges()
                                    Next

                                    ' 2023-10-02, Nael: Cek data old di new
                                    For Each oldEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                                        Dim newEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldEmployerAddress.PK_Customer_Address_ID))
                                        If newEmployerAddress IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerAddress, oldEmployerAddress)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldEmployerAddress)
                                        End If
                                        objDba.SaveChanges()
                                    Next
                                End If

                                ' ======= Employer Phone =======
                                If objListgoAML_Ref_Employer_Phone.Count > 0 Then
                                    ' 2023-10-02, Nael: Cek data new di old
                                    For Each newEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                                        Dim oldEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newEmployerPhone.PK_goAML_Ref_Phone))
                                        If oldEmployerPhone IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerPhone, oldEmployerPhone)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newEmployerPhone)
                                        End If
                                        objDba.SaveChanges()
                                    Next

                                    ' 2023-10-02, Nael: Cek data old di new
                                    For Each oldEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                                        Dim newEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldEmployerPhone.PK_goAML_Ref_Phone))
                                        If newEmployerPhone IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerPhone, oldEmployerPhone)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldEmployerPhone)
                                        End If
                                        objDba.SaveChanges()
                                    Next
                                End If
                            End If

                            If objData.FK_Customer_Type_ID = 2 Then
                                If objListgoAML_Ref_Director_Employer_Address.Count > 0 Then
                                    ' 2023-10-02, Nael: Cek data new di old
                                    For Each newDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                                        Dim oldDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorEmployerAddress.PK_Customer_Address_ID))
                                        If newDirectorEmployerAddress IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress, oldDirectorEmployerAddress)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress)
                                        End If
                                        objDba.SaveChanges()
                                    Next

                                    ' 2023-10-02, Nael: Cek data old di new
                                    For Each oldDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                                        Dim newDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldDirectorEmployerAddress.PK_Customer_Address_ID))
                                        If newDirectorEmployerAddress IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress, oldDirectorEmployerAddress)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorEmployerAddress)
                                        End If
                                        objDba.SaveChanges()
                                    Next
                                End If

                                If objListgoAML_Ref_Director_Employer_Phone.Count > 0 Then
                                    ' 2023-10-02, Nael: Cek data new di old
                                    For Each newDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                                        Dim oldDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorEmployerPhone.PK_goAML_Ref_Phone))
                                        If newDirectorEmployerPhone IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone, oldDirectorEmployerPhone)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone)
                                        End If
                                        objDba.SaveChanges()
                                    Next

                                    ' 2023-10-02, Nael: Cek data old di new
                                    For Each oldDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                                        Dim newDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldDirectorEmployerPhone.PK_goAML_Ref_Phone))
                                        If newDirectorEmployerPhone IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone, oldDirectorEmployerPhone)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorEmployerPhone)
                                        End If
                                        objDba.SaveChanges()
                                    Next
                                End If

                                If objListgoAML_Ref_Director_Address.Count > 0 Then
                                    ' 2023-10-02, Nael: Cek data new di old
                                    For Each newDirectorAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                                        Dim oldDirectorAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorAddress.PK_Customer_Address_ID))
                                        If newDirectorAddress IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorAddress, oldDirectorAddress)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorAddress)
                                        End If
                                        objDba.SaveChanges()
                                    Next

                                    ' 2023-10-02, Nael: Cek data old di new
                                    For Each oldDirectorAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                                        Dim newDirectorAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldDirectorAddress.PK_Customer_Address_ID))
                                        If newDirectorAddress IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorAddress, oldDirectorAddress)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorAddress)
                                        End If
                                        objDba.SaveChanges()
                                    Next
                                End If

                                If objListgoAML_Ref_Director_Phone.Count > 0 Then
                                    For Each newDirectorPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                                        Dim oldDirectorPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorPhone.PK_goAML_Ref_Phone))
                                        If newDirectorPhone IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorPhone, oldDirectorPhone)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorPhone)
                                        End If
                                        objDba.SaveChanges()
                                    Next

                                    ' 2023-10-02, Nael: Cek data old di new
                                    For Each oldDirectorPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                                        Dim newDirectorPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldDirectorPhone.PK_goAML_Ref_Phone))
                                        If newDirectorPhone IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorPhone, oldDirectorPhone)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorPhone)
                                        End If
                                        objDba.SaveChanges()
                                    Next
                                End If

                                If objListgoAML_Ref_Identification_Director.Count > 0 Then
                                    ' 2023-10-02, Nael: Cek data new di old
                                    For Each newDirectorIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                                        Dim oldDirectorIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification_Director.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(newDirectorIdentification.PK_Person_Identification_ID))
                                        If newDirectorIdentification IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorIdentification, oldDirectorIdentification)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorIdentification)
                                        End If
                                        objDba.SaveChanges()
                                    Next

                                    ' 2023-10-02, Nael: Cek data old di new
                                    For Each oldDirectorIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                                        Dim newDirectorIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification_Director.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(oldDirectorIdentification.PK_Person_Identification_ID))
                                        If newDirectorIdentification IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorIdentification, oldDirectorIdentification)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorIdentification)
                                        End If
                                        objDba.SaveChanges()
                                    Next
                                End If

                            End If

                            objDba.SaveChanges() ' 2023-09-29, Nael: SaveChanges
                        End Using
                        ' ======================= END: AUDIT TRAIL ================================

                        objdb.SaveChanges()
                        objtrans.Commit()
                        NawaBLL.Nawa.BLL.NawaFramework.SendEmailModuleApproval(objModuleApproval.PK_ModuleApproval_ID)

                    Catch ex As Exception
                        objtrans.Rollback()
                        Throw
                    End Try
                End Using
            End Using
        End Using
    End Function

    Sub SaveAddApproval(objData As GoAMLDAL.goAML_Ref_Customer, objListgoAML_Ref_Director As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director), objModule As NawaDAL.Module, objListgoAML_Ref_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification As List(Of GoAMLDAL.goAML_Person_Identification), objListgoAML_Ref_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Employer_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Employer_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), Objlistgoaml_Ref_Identification_Director As List(Of GoAMLDAL.goAML_Person_Identification), objData2 As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2, objList As GoAMLBLL.goAML_CustomerDataBLL.goAML_Grid_Customer)
        Using objDB As New GoAMLDAL.GoAMLEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objdatabll As New GoAMLBLL.goAML_CustomerDataBLL
                    objdatabll.objgoAML_Ref_Customer = objData
                    objdatabll.objListgoAML_Ref_Director = objListgoAML_Ref_Director
                    objdatabll.objlistgoaml_Ref_Address = objListgoAML_Ref_Address
                    objdatabll.objListgoAML_Ref_Phone = objListgoAML_Ref_Phone
                    objdatabll.objListgoAML_Ref_Employer_Phone = objListgoAML_Ref_Employer_Phone
                    objdatabll.objlistgoaml_Ref_Employer_Address = objListgoAML_Ref_Employer_Address
                    objdatabll.objListgoAML_Ref_Identification = objListgoAML_Ref_Identification
                    objdatabll.objListgoAML_Ref_Director_Address = objListgoAML_Ref_Director_Address
                    objdatabll.objListgoAML_Ref_Director_Phone = objListgoAML_Ref_Director_Phone
                    objdatabll.objListgoAML_Ref_Director_Employer_Phone = objListgoAML_Ref_Director_Employer_Phone
                    objdatabll.objlistgoaml_Ref_Director_Employer_Address = objListgoAML_Ref_Director_Employer_Address
                    objdatabll.objListgoAML_Ref_Identification_Director = Objlistgoaml_Ref_Identification_Director

                    'Add by Septian, goAML 5.0.1, 2023-01-30
                    objdatabll.objgoAML_Ref_Customer2 = objData2
                    objdatabll.ObjList_GoAML_Ref_Customer_PreviousName = objList.ObjList_GoAML_PreviousName
                    objdatabll.ObjList_GoAML_Ref_Customer_Email = objList.ObjList_GoAML_Email
                    objdatabll.ObjList_GoAML_Ref_Customer_EmploymentHistory = objList.ObjList_GoAML_EmploymentHistory
                    objdatabll.ObjList_GoAML_Ref_Customer_PEP = objList.ObjList_GoAML_PEP
                    objdatabll.ObjList_GoAML_Ref_Customer_NetworkDevice = objList.ObjList_GoAML_NetworkDevice
                    objdatabll.ObjList_GoAML_Ref_Customer_SocialMedia = objList.ObjList_GoAML_SocialMedia
                    objdatabll.ObjList_GoAML_Ref_Customer_Sanction = objList.ObjList_GoAML_Sanction
                    objdatabll.ObjList_GoAML_Ref_Customer_RelatedPerson = objList.ObjList_GoAML_RelatedPerson
                    objdatabll.ObjList_GoAML_Ref_Customer_AdditionalInformation = objList.ObjList_GoAML_AdditionalInformation
                    objdatabll.ObjList_GoAML_Ref_Customer_URL = objList.ObjList_GoAML_URL
                    objdatabll.ObjList_GoAML_Ref_Customer_EntityIdentification = objList.ObjList_GoAML_EntityIdentification
                    objdatabll.ObjList_GoAML_Ref_Customer_RelatedEntities = objList.ObjList_GoAML_RelatedEntities
                    objdatabll.ObjListgoAML_Ref_Director2 = objList.objList_GoAML_Ref_Director
                    'End Add

                    Dim dataXML As String = NawaBLL.Common.Serialize(objdatabll)

                    Dim objModuleApproval As New GoAMLDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = 0
                        .ModuleField = dataXML
                        .ModuleFieldBefore = ""
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added

                    ' ======================= START: AUDIT TRAIL ================================
                    Using objDba As New GoAMLEntities
                        Dim header As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                        'AuditTrailDetail
                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, objData)
                        objDba.SaveChanges()
                        If objListgoAML_Ref_Phone.Count > 0 Then
                            For Each userPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, userPhone)
                                objDba.SaveChanges()
                            Next
                        End If

                        If objListgoAML_Ref_Address.Count > 0 Then
                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                objDba.SaveChanges()
                            Next
                        End If

                        If objData.FK_Customer_Type_ID = 1 Then

                            If objListgoAML_Ref_Identification.Count > 0 Then
                                For Each itemheader As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objListgoAML_Ref_Employer_Address.Count > 0 Then
                                For Each itemheader As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objListgoAML_Ref_Employer_Phone.Count > 0 Then
                                For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    objDba.SaveChanges()
                                Next
                            End If
                        End If

                        If objData.FK_Customer_Type_ID = 2 Then
                            If objListgoAML_Ref_Director_Employer_Address.Count > 0 Then
                                For Each itemheader As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objListgoAML_Ref_Director_Employer_Phone.Count > 0 Then
                                For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objListgoAML_Ref_Director_Address.Count > 0 Then
                                For Each itemheader As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objListgoAML_Ref_Director_Phone.Count > 0 Then
                                For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    objDba.SaveChanges()
                                Next
                            End If

                            If Objlistgoaml_Ref_Identification_Director.Count > 0 Then
                                For Each itemheader As GoAMLDAL.goAML_Person_Identification In Objlistgoaml_Ref_Identification_Director
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    objDba.SaveChanges()
                                Next
                            End If

                        End If


                        objDba.SaveChanges()
                    End Using
                    ' ======================= END: AUDIT TRAIL ================================

                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Sub SaveEditTanpaApproval(objData As GoAMLDAL.goAML_Ref_Customer, objListgoAML_Ref_Director As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director), objModule As NawaDAL.Module, objListgoAML_Ref_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification As List(Of GoAMLDAL.goAML_Person_Identification), objListgoAML_Ref_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Employer_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Employer_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification_Director As List(Of GoAMLDAL.goAML_Person_Identification), objData2 As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2, objList As GoAMLBLL.goAML_CustomerDataBLL.goAML_Grid_Customer)
        Using objDB As New GoAMLDAL.GoAMLEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    'Update: Zikri_14092020 Add Validation GCN
                    'Dim objModelOld As GoAMLDAL.goAML_Ref_Customer = objDB.goAML_Ref_Customer.FirstOrDefault(Function(x) x.PK_Customer_ID.Equals(objData.PK_Customer_ID))
                    If objData.GCN IsNot Nothing Then
                        If objData.isGCNPrimary = True Then
                            Dim checkGCN As goAML_Ref_Customer = GoAMLBLL.goAML_CustomerBLL.GetCheckGCN(objData.GCN)
                            If checkGCN IsNot Nothing Then
                                If checkGCN.CIF <> objData.CIF Then
                                    checkGCN.isGCNPrimary = 0
                                    checkGCN.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    checkGCN.ApprovedDate = Now
                                    checkGCN.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    checkGCN.LastUpdateDate = Now
                                    objDB.Entry(checkGCN).State = Entity.EntityState.Modified
                                    objDB.SaveChanges()
                                End If
                            End If
                        End If
                    End If
                    'End Update
                    Dim objDataBefore As GoAMLDAL.goAML_Ref_Customer = objDB.goAML_Ref_Customer.FirstOrDefault(Function(x) x.PK_Customer_ID.Equals(objData.PK_Customer_ID))
                    objData.Active = 1
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        objData.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    Else
                        objData.Alternateby = ""
                    End If
                    objData.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.ApprovedDate = Now
                    objData.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.LastUpdateDate = Now
                    objDB.Entry(objData).State = Entity.EntityState.Modified
                    objDB.SaveChanges()

                    If objData.FK_Customer_Type_ID = 2 Then
                        For Each itemx As GoAMLDAL.goAML_Ref_Customer_Entity_Director In (From x In objDB.goAML_Ref_Customer_Entity_Director Where x.FK_Entity_ID = objData.CIF Select x).ToList
                            Dim objcek As GoAMLDAL.goAML_Ref_Customer_Entity_Director = objListgoAML_Ref_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = itemx.PK_goAML_Ref_Customer_Entity_Director_ID)
                            If objcek Is Nothing Then
                                objDB.Entry(itemx).State = Entity.EntityState.Deleted
                            End If
                        Next
                        objDB.SaveChanges()

                        For Each item As GoAMLDAL.goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                            Dim item2 = objList.objList_GoAML_Ref_Director.FirstOrDefault(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID)
                            Dim obcek As GoAMLDAL.goAML_Ref_Customer_Entity_Director = (From x In objDB.goAML_Ref_Customer_Entity_Director Where x.PK_goAML_Ref_Customer_Entity_Director_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID Select x).FirstOrDefault
                            If obcek Is Nothing Then
                                item.FK_Entity_ID = objData.CIF
                                objDB.Entry(item).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(obcek).CurrentValues.SetValues(item)
                                objDB.Entry(obcek).State = Entity.EntityState.Modified
                            End If
                            objDB.SaveChanges()

                            For Each itemx As GoAMLDAL.goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 6 Select x).ToList
                                Dim objcek As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.LastUpdateDate = Now
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 7 Select x).ToList
                                Dim objcek As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.LastUpdateDate = Now
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 6 Select x).ToList
                                Dim objcek As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.LastUpdateDate = Now
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 7 Select x).ToList
                                Dim objcek As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.LastUpdateDate = Now
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Person_Type = 4 Select x).ToList
                                Dim objcek As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID)
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.LastUpdateDate = Now
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                                Dim objcek As GoAMLDAL.goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID Select x).FirstOrDefault
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.CreatedDate = Now
                                    itemx.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID '' Edited on 04 Dec 2020
                                    objDB.Entry(itemx).State = Entity.EntityState.Added
                                Else
                                    Dim objcekdirector = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_Person_ID).ToList()
                                    If objcekdirector Is Nothing Then
                                        objDB.Entry(objcek).State = Entity.EntityState.Deleted
                                    Else
                                        objDB.Entry(objcek).CurrentValues.SetValues(itemx)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            objcek.Alternateby = ""
                                        End If
                                        objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.ApprovedDate = Now
                                        objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.LastUpdateDate = Now
                                        objDB.Entry(objcek).State = Entity.EntityState.Modified
                                    End If
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                                Dim objcek As GoAMLDAL.goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.CreatedDate = Now
                                    itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID '' Edited on 04 Dec 2020
                                    objDB.Entry(itemx).State = Entity.EntityState.Added
                                Else
                                    Dim objcekdirector = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_for_Table_ID).ToList()
                                    If objcekdirector Is Nothing Then
                                        objDB.Entry(objcek).State = Entity.EntityState.Deleted
                                    Else
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            objcek.Alternateby = ""
                                        End If
                                        objDB.Entry(objcek).CurrentValues.SetValues(itemx)
                                        objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.ApprovedDate = Now
                                        objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.LastUpdateDate = Now
                                        objDB.Entry(objcek).State = Entity.EntityState.Modified
                                    End If
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                                Dim objcek As GoAMLDAL.goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.CreatedDate = Now
                                    itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID '' Edited on 04 Dec 2020
                                    objDB.Entry(itemx).State = Entity.EntityState.Added
                                Else
                                    Dim objcekdirector = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_for_Table_ID).ToList()
                                    If objcekdirector Is Nothing Then
                                        objDB.Entry(objcek).State = Entity.EntityState.Deleted
                                    Else
                                        objDB.Entry(objcek).CurrentValues.SetValues(itemx)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            objcek.Alternateby = ""
                                        End If
                                        objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.ApprovedDate = Now
                                        objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.LastUpdateDate = Now
                                        objDB.Entry(objcek).State = Entity.EntityState.Modified
                                    End If
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                                Dim objcek As GoAMLDAL.goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID Select x).FirstOrDefault
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.CreatedDate = Now
                                    itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID '' edited on 04 Dec 2020
                                    objDB.Entry(itemx).State = Entity.EntityState.Added
                                Else
                                    Dim objcekdirector = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.PK_Customer_Address_ID).ToList()
                                    If objcekdirector Is Nothing Then
                                        objDB.Entry(objcek).State = Entity.EntityState.Deleted
                                    Else
                                        objDB.Entry(objcek).CurrentValues.SetValues(itemx)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            objcek.Alternateby = ""
                                        End If
                                        objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.ApprovedDate = Now
                                        objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.LastUpdateDate = Now
                                        objDB.Entry(objcek).State = Entity.EntityState.Modified
                                    End If
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                                Dim objcek As GoAMLDAL.goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID Select x).FirstOrDefault
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.CreatedDate = Now
                                    itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID '' Edited on 04 Dec 2020
                                    objDB.Entry(itemx).State = Entity.EntityState.Added
                                Else
                                    Dim objcekdirector = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_To_Table_ID).ToList()
                                    If objcekdirector Is Nothing Then
                                        objDB.Entry(objcek).State = Entity.EntityState.Deleted
                                    Else
                                        objDB.Entry(objcek).CurrentValues.SetValues(itemx)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            objcek.Alternateby = ""
                                        End If
                                        objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.ApprovedDate = Now
                                        objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.LastUpdateDate = Now
                                        objDB.Entry(objcek).State = Entity.EntityState.Modified
                                    End If
                                End If
                            Next

                            item2.PK_goAML_Ref_Customer_Entity_Director_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                            item2.ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(item2.PK_goAML_Ref_Customer_Entity_Director_ID, item2.ObjList_GoAML_Ref_Customer_Email)
                            item2.ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(item2.PK_goAML_Ref_Customer_Entity_Director_ID, item2.ObjList_GoAML_Ref_Customer_Sanction)
                            item2.ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(item2.PK_goAML_Ref_Customer_Entity_Director_ID, item2.ObjList_GoAML_Ref_Customer_PEP)
                        Next
                    End If

                    If objData.FK_Customer_Type_ID = 1 Then
                        For Each itemx As GoAMLDAL.goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = objData.PK_Customer_ID And x.FK_Person_Type = 1 Select x).ToList
                            Dim objcek As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID)
                            If objcek Is Nothing Then
                                objDB.Entry(itemx).State = Entity.EntityState.Deleted
                            End If
                        Next
                        For Each item As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                            Dim obcek As GoAMLDAL.goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = item.PK_Person_Identification_ID And x.FK_Person_Type = 1 Select x).FirstOrDefault
                            If obcek Is Nothing Then
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    item.Alternateby = ""
                                End If
                                item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.ApprovedDate = Now
                                item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.CreatedDate = Now
                                objDB.Entry(item).State = Entity.EntityState.Added
                            Else

                                objDB.Entry(obcek).CurrentValues.SetValues(item)
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    obcek.Alternateby = ""
                                End If
                                obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                obcek.ApprovedDate = Now
                                obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                obcek.LastUpdateDate = Now
                                objDB.Entry(obcek).State = Entity.EntityState.Modified
                            End If
                        Next

                        For Each itemx As GoAMLDAL.goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objData.PK_Customer_ID And x.FK_Ref_Detail_Of = 5 Select x).ToList
                            Dim objcek As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                            If objcek Is Nothing Then
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    itemx.Alternateby = ""
                                End If
                                itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.ApprovedDate = Now
                                itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.CreatedDate = Now
                                objDB.Entry(itemx).State = Entity.EntityState.Deleted
                            End If
                        Next
                        For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                            Dim obcek As GoAMLDAL.goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone Select x).FirstOrDefault
                            If obcek Is Nothing Then
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    item.Alternateby = ""
                                End If
                                item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.ApprovedDate = Now
                                item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.CreatedDate = Now
                                objDB.Entry(item).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(obcek).CurrentValues.SetValues(item)
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    obcek.Alternateby = ""
                                End If
                                obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                obcek.ApprovedDate = Now
                                obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                obcek.LastUpdateDate = Now
                                objDB.Entry(obcek).State = Entity.EntityState.Modified
                            End If
                        Next

                        For Each itemx As GoAMLDAL.goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objData.PK_Customer_ID And x.FK_Ref_Detail_Of = 5 Select x).ToList
                            Dim objcek As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                            If objcek Is Nothing Then
                                objDB.Entry(itemx).State = Entity.EntityState.Deleted
                            End If
                        Next
                        For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                            Dim obcek As GoAMLDAL.goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID Select x).FirstOrDefault
                            If obcek Is Nothing Then
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    item.Alternateby = ""
                                End If
                                item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.ApprovedDate = Now
                                item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.CreatedDate = Now
                                objDB.Entry(item).State = Entity.EntityState.Added
                            Else

                                objDB.Entry(obcek).CurrentValues.SetValues(item)
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    obcek.Alternateby = ""
                                End If
                                obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                obcek.ApprovedDate = Now
                                obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                obcek.LastUpdateDate = Now
                                objDB.Entry(obcek).State = Entity.EntityState.Modified
                            End If
                        Next
                    End If

                    For Each itemx As GoAMLDAL.goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objData.PK_Customer_ID And x.FK_Ref_Detail_Of = 1 Select x).ToList
                        Dim objcek As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = Entity.EntityState.Deleted
                        End If
                    Next

                    For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                        Dim obcek As GoAMLDAL.goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                obcek.Alternateby = ""
                            End If
                            obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.ApprovedDate = Now
                            obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.LastUpdateDate = Now
                            objDB.Entry(obcek).State = Entity.EntityState.Modified
                        End If
                    Next

                    For Each itemx As GoAMLDAL.goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objData.PK_Customer_ID And x.FK_Ref_Detail_Of = 1 Select x).ToList
                        Dim objcek As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = Entity.EntityState.Deleted
                        End If
                    Next

                    For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                        Dim obcek As GoAMLDAL.goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Else

                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                obcek.Alternateby = ""
                            End If
                            obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.ApprovedDate = Now
                            obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.LastUpdateDate = Now
                            objDB.Entry(obcek).State = Entity.EntityState.Modified
                        End If
                    Next

                    ' ======================= START: AUDIT TRAIL ================================
                    Using objDba As New GoAMLEntities
                        Dim header As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)

                        'AuditTrailDetail, NaelEdit
                        ' 2023-09-29, Nael: Menambahkan Audit Trail
                        ' CreateAuditTrailDetailEdit
                        'objData
                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, objData, objDataBefore)
                        objDba.SaveChanges()

                        ' ============ Customer Phones ============
                        If objListgoAML_Ref_Phone.Count > 0 Then
                            ' 2023-10-02, Nael: Cek data new di old
                            For Each newUserPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                                Dim oldUserPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newUserPhone.PK_goAML_Ref_Phone))
                                If oldUserPhone IsNot Nothing Then
                                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserPhone, oldUserPhone)
                                Else
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserPhone)
                                End If
                                objDba.SaveChanges()
                            Next

                            ' 2023-10-02, Nael: Cek data old di new
                            For Each oldUserPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                                Dim newUserPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldUserPhone.PK_goAML_Ref_Phone))
                                If newUserPhone IsNot Nothing Then
                                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserPhone, oldUserPhone)
                                Else
                                    NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserPhone)
                                End If
                                objDba.SaveChanges()
                            Next
                        End If

                        ' ========== Customer Address ===========
                        If objListgoAML_Ref_Address.Count > 0 Then
                            ' 2023-10-02, Nael: Cek data new di old
                            For Each newUserAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                                Dim oldUserAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newUserAddress.PK_Customer_Address_ID))
                                If oldUserAddress IsNot Nothing Then
                                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserAddress, oldUserAddress)
                                Else
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserAddress)
                                End If
                                objDba.SaveChanges()
                            Next

                            ' 2023-10-02, Nael: Cek data old di new
                            For Each oldUserAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                                Dim newUserAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldUserAddress.PK_Customer_Address_ID))
                                If newUserAddress IsNot Nothing Then
                                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserAddress, oldUserAddress)
                                Else
                                    NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserAddress)
                                End If
                                objDba.SaveChanges()
                            Next
                        End If

                        If objData.FK_Customer_Type_ID = 1 Then
                            ' ======== Person Identification =========
                            If objListgoAML_Ref_Identification.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newUserIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                                    Dim oldUserIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(newUserIdentification.PK_Person_Identification_ID))
                                    If oldUserIdentification IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserIdentification, oldUserIdentification)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserIdentification)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldUserIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                                    Dim newUserIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(oldUserIdentification.PK_Person_Identification_ID))
                                    If newUserIdentification IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserIdentification, oldUserIdentification)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserIdentification)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            ' ======== Employer Address ==========
                            If objListgoAML_Ref_Employer_Address.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                                    Dim oldEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newEmployerAddress.PK_Customer_Address_ID))
                                    If oldEmployerAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerAddress, oldEmployerAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newEmployerAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                                    Dim newEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldEmployerAddress.PK_Customer_Address_ID))
                                    If newEmployerAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerAddress, oldEmployerAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldEmployerAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            ' ======= Employer Phone =======
                            If objListgoAML_Ref_Employer_Phone.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                                    Dim oldEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newEmployerPhone.PK_goAML_Ref_Phone))
                                    If oldEmployerPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerPhone, oldEmployerPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newEmployerPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                                    Dim newEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldEmployerPhone.PK_goAML_Ref_Phone))
                                    If newEmployerPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerPhone, oldEmployerPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldEmployerPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If
                        End If

                        If objData.FK_Customer_Type_ID = 2 Then
                            If objListgoAML_Ref_Director_Employer_Address.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                                    Dim oldDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorEmployerAddress.PK_Customer_Address_ID))
                                    If newDirectorEmployerAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress, oldDirectorEmployerAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                                    Dim newDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldDirectorEmployerAddress.PK_Customer_Address_ID))
                                    If newDirectorEmployerAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress, oldDirectorEmployerAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorEmployerAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objListgoAML_Ref_Director_Employer_Phone.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                                    Dim oldDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorEmployerPhone.PK_goAML_Ref_Phone))
                                    If newDirectorEmployerPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone, oldDirectorEmployerPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                                    Dim newDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldDirectorEmployerPhone.PK_goAML_Ref_Phone))
                                    If newDirectorEmployerPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone, oldDirectorEmployerPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorEmployerPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objListgoAML_Ref_Director_Address.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newDirectorAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                                    Dim oldDirectorAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorAddress.PK_Customer_Address_ID))
                                    If newDirectorAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorAddress, oldDirectorAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldDirectorAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                                    Dim newDirectorAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldDirectorAddress.PK_Customer_Address_ID))
                                    If newDirectorAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorAddress, oldDirectorAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objListgoAML_Ref_Director_Phone.Count > 0 Then
                                For Each newDirectorPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                                    Dim oldDirectorPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorPhone.PK_goAML_Ref_Phone))
                                    If newDirectorPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorPhone, oldDirectorPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldDirectorPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                                    Dim newDirectorPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldDirectorPhone.PK_goAML_Ref_Phone))
                                    If newDirectorPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorPhone, oldDirectorPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objListgoAML_Ref_Identification_Director.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newDirectorIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                                    Dim oldDirectorIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification_Director.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(newDirectorIdentification.PK_Person_Identification_ID))
                                    If newDirectorIdentification IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorIdentification, oldDirectorIdentification)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorIdentification)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldDirectorIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                                    Dim newDirectorIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification_Director.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(oldDirectorIdentification.PK_Person_Identification_ID))
                                    If newDirectorIdentification IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorIdentification, oldDirectorIdentification)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorIdentification)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                        End If

                        objDba.SaveChanges() ' 2023-09-29, Nael: SaveChanges
                    End Using
                    ' ======================= END: AUDIT TRAIL ================================



                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using

        'add by Septian, goAML 5.0.1, 2022-02-13
        objData2.PK_Customer_ID = objData.PK_Customer_ID
        objData2.CIF = objData.CIF
        Delete_Grid(objData2.CIF)
        SaveTanpaApproval(objData2, objList)
        'end add
    End Sub

    Sub SaveAddTanpaApproval(objData As GoAMLDAL.goAML_Ref_Customer, objListgoAML_Ref_Director As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director), objModule As NawaDAL.Module, objListgoAML_Ref_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification As List(Of GoAMLDAL.goAML_Person_Identification), objListgoAML_Ref_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Employer_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Employer_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Address As List(Of GoAMLDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Phone As List(Of GoAMLDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification_Director As List(Of GoAMLDAL.goAML_Person_Identification), objData2 As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2, objList As GoAMLBLL.goAML_CustomerDataBLL.goAML_Grid_Customer)
        'Dim int_PK_goAML_Ref_Customer_Entity_Director_ID As Integer

        Using objDB As New GoAMLDAL.GoAMLEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    'Update: Zikri_14092020 Add Validation GCN
                    If objData.GCN IsNot Nothing Then
                        Dim checkGCN As goAML_Ref_Customer = GoAMLBLL.goAML_CustomerBLL.GetCheckGCN(objData.GCN)
                        If checkGCN IsNot Nothing Then
                            If objData.isGCNPrimary = True Then
                                checkGCN.isGCNPrimary = 0
                                checkGCN.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                checkGCN.ApprovedDate = Now
                                checkGCN.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                checkGCN.LastUpdateDate = Now
                                objDB.Entry(checkGCN).State = Entity.EntityState.Modified
                                objDB.SaveChanges()
                            End If
                        End If
                    End If
                    'End Update
                    objData.Active = 1
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        objData.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    Else
                        objData.Alternateby = ""
                    End If
                    objData.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.CreatedDate = Now
                    objData.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.ApprovedDate = Now
                    objDB.Entry(objData).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    If objData.FK_Customer_Type_ID = 2 Then
                        For Each item As GoAMLDAL.goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                            item.FK_Entity_ID = objData.CIF
                            'add by septian, goAML enhancement, 2023-04-17
                            Dim item2 = objList.objList_GoAML_Ref_Director.FirstOrDefault(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID)
                            'end add
                            Dim addressdirector = objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                            Dim phonedirector = objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                            Dim addressempdirector = objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                            Dim phoneempdirector = objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                            Dim identificationdirector = objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                            objDB.Entry(item).State = Entity.EntityState.Added
                            objDB.SaveChanges()

                            For Each itemx As GoAMLDAL.goAML_Ref_Address In addressdirector
                                itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                'int_PK_goAML_Ref_Customer_Entity_Director_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID

                                itemx.Active = 1
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    itemx.Alternateby = ""
                                End If
                                itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.CreatedDate = Now
                                itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.ApprovedDate = Now
                                objDB.Entry(itemx).State = Entity.EntityState.Added
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Phone In phonedirector

                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    itemx.Alternateby = ""
                                End If
                                itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.CreatedDate = Now
                                itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.ApprovedDate = Now
                                objDB.Entry(itemx).State = Entity.EntityState.Added
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Address In addressempdirector
                                itemx.Active = 1
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    itemx.Alternateby = ""
                                End If
                                itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.CreatedDate = Now
                                itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.ApprovedDate = Now
                                objDB.Entry(itemx).State = Entity.EntityState.Added
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Phone In phoneempdirector
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    itemx.Alternateby = ""
                                End If
                                itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.CreatedDate = Now
                                itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.ApprovedDate = Now
                                objDB.Entry(itemx).State = Entity.EntityState.Added
                            Next

                            For Each itemx As GoAMLDAL.goAML_Person_Identification In identificationdirector
                                itemx.Active = 1
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    itemx.Alternateby = ""
                                End If
                                itemx.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.CreatedDate = Now
                                itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.ApprovedDate = Now
                                objDB.Entry(itemx).State = Entity.EntityState.Added
                            Next
                            'add by septian, 2023-04-17

                            'For Each item2 As GoAMLBLL.goAML_Customer.DataModel.goAML_Ref_Customer_Entity_Director In objList.objList_GoAML_Ref_Director
                            item2.PK_goAML_Ref_Customer_Entity_Director_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                            item2.ObjList_GoAML_Ref_Customer_Email = goAML_Ref_Customer_Email_Mapper.Serialize(item2.PK_goAML_Ref_Customer_Entity_Director_ID, item2.ObjList_GoAML_Ref_Customer_Email)
                            item2.ObjList_GoAML_Ref_Customer_Sanction = goAML_Ref_Customer_Sanction_Mapper.Serialize(item2.PK_goAML_Ref_Customer_Entity_Director_ID, item2.ObjList_GoAML_Ref_Customer_Sanction)
                            item2.ObjList_GoAML_Ref_Customer_PEP = goAML_Ref_Customer_PEP_Mapper.Serialize(item2.PK_goAML_Ref_Customer_Entity_Director_ID, item2.ObjList_GoAML_Ref_Customer_PEP)

                            'end add
                            'Next
                        Next
                    End If

                    If objData.FK_Customer_Type_ID = 1 Then
                        For Each item As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                            item.Active = 1
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.FK_Person_ID = objData.PK_Customer_ID
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Now
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Next

                        For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                            item.Active = 1
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.FK_To_Table_ID = objData.PK_Customer_ID
                            item.FK_Ref_Detail_Of = 5
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Now
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Next

                        For Each item As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.FK_for_Table_ID = objData.PK_Customer_ID
                            item.FK_Ref_Detail_Of = 5
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Now
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Next

                    End If

                    For Each item As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                        item.Active = 1
                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                            item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                        Else
                            item.Alternateby = ""
                        End If
                        item.FK_To_Table_ID = objData.PK_Customer_ID
                        item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        item.CreatedDate = Now
                        item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        item.ApprovedDate = Now
                        objDB.Entry(item).State = Entity.EntityState.Added
                    Next

                    For Each itemx As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                        Else
                            itemx.Alternateby = ""
                        End If
                        itemx.FK_for_Table_ID = objData.PK_Customer_ID
                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        itemx.CreatedDate = Now
                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        itemx.ApprovedDate = Now
                        objDB.Entry(itemx).State = Entity.EntityState.Added
                    Next

                    ' ======================= START: AUDIT TRAIL ================================
                    Using objDba As New GoAMLEntities
                        Dim header As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)

                        'AuditTrailDetail, NaelEdit
                        ' 2023-09-29, Nael: Menambahkan Audit Trail
                        ' CreateAuditTrailDetailEdit
                        'objData
                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, objData, objData2)
                        objDba.SaveChanges()

                        ' ============ Customer Phones ============
                        If objListgoAML_Ref_Phone.Count > 0 Then
                            ' 2023-10-02, Nael: Cek data new di old
                            For Each newUserPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                                Dim oldUserPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newUserPhone.PK_goAML_Ref_Phone))
                                If oldUserPhone IsNot Nothing Then
                                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserPhone, oldUserPhone)
                                Else
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserPhone)
                                End If
                                objDba.SaveChanges()
                            Next

                            ' 2023-10-02, Nael: Cek data old di new
                            For Each oldUserPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                                Dim newUserPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldUserPhone.PK_goAML_Ref_Phone))
                                If newUserPhone IsNot Nothing Then
                                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserPhone, oldUserPhone)
                                Else
                                    NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserPhone)
                                End If
                                objDba.SaveChanges()
                            Next
                        End If

                        ' ========== Customer Address ===========
                        If objListgoAML_Ref_Address.Count > 0 Then
                            ' 2023-10-02, Nael: Cek data new di old
                            For Each newUserAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                                Dim oldUserAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newUserAddress.PK_Customer_Address_ID))
                                If oldUserAddress IsNot Nothing Then
                                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserAddress, oldUserAddress)
                                Else
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserAddress)
                                End If
                                objDba.SaveChanges()
                            Next

                            ' 2023-10-02, Nael: Cek data old di new
                            For Each oldUserAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                                Dim newUserAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldUserAddress.PK_Customer_Address_ID))
                                If newUserAddress IsNot Nothing Then
                                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserAddress, oldUserAddress)
                                Else
                                    NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserAddress)
                                End If
                                objDba.SaveChanges()
                            Next
                        End If

                        If objData.FK_Customer_Type_ID = 1 Then
                            ' ======== Person Identification =========
                            If objListgoAML_Ref_Identification.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newUserIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                                    Dim oldUserIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(newUserIdentification.PK_Person_Identification_ID))
                                    If oldUserIdentification IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserIdentification, oldUserIdentification)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserIdentification)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldUserIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                                    Dim newUserIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(oldUserIdentification.PK_Person_Identification_ID))
                                    If newUserIdentification IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserIdentification, oldUserIdentification)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserIdentification)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            ' ======== Employer Address ==========
                            If objListgoAML_Ref_Employer_Address.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                                    Dim oldEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newEmployerAddress.PK_Customer_Address_ID))
                                    If oldEmployerAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerAddress, oldEmployerAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newEmployerAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                                    Dim newEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldEmployerAddress.PK_Customer_Address_ID))
                                    If newEmployerAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerAddress, oldEmployerAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldEmployerAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            ' ======= Employer Phone =======
                            If objListgoAML_Ref_Employer_Phone.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                                    Dim oldEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newEmployerPhone.PK_goAML_Ref_Phone))
                                    If oldEmployerPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerPhone, oldEmployerPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newEmployerPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                                    Dim newEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldEmployerPhone.PK_goAML_Ref_Phone))
                                    If newEmployerPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerPhone, oldEmployerPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldEmployerPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If
                        End If

                        If objData.FK_Customer_Type_ID = 2 Then
                            If objListgoAML_Ref_Director_Employer_Address.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                                    Dim oldDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorEmployerAddress.PK_Customer_Address_ID))
                                    If newDirectorEmployerAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress, oldDirectorEmployerAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                                    Dim newDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorEmployerAddress.PK_Customer_Address_ID))
                                    If newDirectorEmployerAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress, oldDirectorEmployerAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorEmployerAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objListgoAML_Ref_Director_Employer_Phone.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                                    Dim oldDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorEmployerPhone.PK_goAML_Ref_Phone))
                                    If newDirectorEmployerPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone, oldDirectorEmployerPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                                    Dim newDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorEmployerPhone.PK_goAML_Ref_Phone))
                                    If newDirectorEmployerPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone, oldDirectorEmployerPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorEmployerPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objListgoAML_Ref_Director_Address.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newDirectorAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                                    Dim oldDirectorAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorAddress.PK_Customer_Address_ID))
                                    If newDirectorAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorAddress, oldDirectorAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldDirectorAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                                    Dim newDirectorAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorAddress.PK_Customer_Address_ID))
                                    If newDirectorAddress IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorAddress, oldDirectorAddress)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorAddress)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objListgoAML_Ref_Director_Phone.Count > 0 Then
                                For Each newDirectorPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                                    Dim oldDirectorPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorPhone.PK_goAML_Ref_Phone))
                                    If newDirectorPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorPhone, oldDirectorPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldDirectorPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                                    Dim newDirectorPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorPhone.PK_goAML_Ref_Phone))
                                    If newDirectorPhone IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorPhone, oldDirectorPhone)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorPhone)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                            If objListgoAML_Ref_Identification_Director.Count > 0 Then
                                ' 2023-10-02, Nael: Cek data new di old
                                For Each newDirectorIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                                    Dim oldDirectorIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification_Director.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(newDirectorIdentification.PK_Person_Identification_ID))
                                    If newDirectorIdentification IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorIdentification, oldDirectorIdentification)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorIdentification)
                                    End If
                                    objDba.SaveChanges()
                                Next

                                ' 2023-10-02, Nael: Cek data old di new
                                For Each oldDirectorIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                                    Dim newDirectorIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification_Director.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(newDirectorIdentification.PK_Person_Identification_ID))
                                    If newDirectorIdentification IsNot Nothing Then
                                        NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorIdentification, oldDirectorIdentification)
                                    Else
                                        NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorIdentification)
                                    End If
                                    objDba.SaveChanges()
                                Next
                            End If

                        End If

                        objDba.SaveChanges() ' 2023-09-29, Nael: SaveChanges
                    End Using
                    ' ======================= END: AUDIT TRAIL ================================

                    objDB.SaveChanges()
                    objTrans.Commit()


                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
        objData2.PK_Customer_ID = objData.PK_Customer_ID
        SaveTanpaApproval(objData2, objList)
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        ' TODO: uncomment the following line if Finalize() is overridden above.
        ' GC.SuppressFinalize(Me)
    End Sub

    Shared Sub LoadPanel(objPanel As FormPanel, objdata As String, strModulename As String, unikkey As String)

        'done: loadPanel

        Dim objgoAML_CustomerDataBLL As GoAMLBLL.goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objdata, GetType(GoAMLBLL.goAML_CustomerDataBLL))
        Dim objCustomer As GoAMLDAL.goAML_Ref_Customer = objgoAML_CustomerDataBLL.objgoAML_Ref_Customer
        Dim objCustomer2 As goAML_Ref_Customer2 = objgoAML_CustomerDataBLL.objgoAML_Ref_Customer2

        'Dim objListEmailTemplatedetail As List(Of NawaDAL.EmailTemplateDetail) = GetListEmailTemplateDetailByPKID(unikkey)
        'Dim objListEmailTemplateAction As List(Of NawaDAL.EmailTemplateAction) = NawaBLL.EmailTemplateBLL.GetListEmailTemplateActionByPKID(unikkey)
        'Dim objListEmailTemplateAttachment As List(Of NawaDAL.EmailTemplateAttachment) = NawaBLL.EmailTemplateBLL.GetListEmailTemplateAttachmentByPKID(unikkey)

        Dim INDV_gender As GoAMLDAL.goAML_Ref_Jenis_Kelamin = Nothing
        Dim INDV_statusCode As GoAMLDAL.goAML_Ref_Status_Rekening = Nothing
        Dim INDV_nationality1 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
        Dim INDV_nationality2 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
        Dim INDV_nationality3 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
        Dim INDV_residence As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
        Dim CORP_Incorporation_legal_form As GoAMLDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
        Dim CORP_incorporation_country_code As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
        Dim CORP_Role As GoAMLDAL.goAML_Ref_Party_Role = Nothing



        Using db As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            If Not objCustomer Is Nothing Then
                Dim strunik As String = unikkey

#Region "goAML 5.0.1, Grid, Add By Septian, 27 Jan 2023"
                '' Previous Name, Add By Septian, 2023-01-27
                Dim objStore_PreviousName As New Ext.Net.Store
                objStore_PreviousName.ID = strunik & "StoreGrid_PreviousName"
                objStore_PreviousName.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModel_PreviousName As New Ext.Net.Model
                Dim objField_PreviousName As Ext.Net.ModelField

                objField_PreviousName = New Ext.Net.ModelField
                objField_PreviousName.Name = "PK_goAML_Ref_Customer_Previous_Name_ID"
                objField_PreviousName.Type = ModelFieldType.Auto
                objModel_PreviousName.Fields.Add(objField_PreviousName)

                objField_PreviousName = New Ext.Net.ModelField
                objField_PreviousName.Name = "CIF"
                objField_PreviousName.Type = ModelFieldType.String
                objModel_PreviousName.Fields.Add(objField_PreviousName)

                objField_PreviousName = New Ext.Net.ModelField
                objField_PreviousName.Name = "FIRST_NAME"
                objField_PreviousName.Type = ModelFieldType.String
                objModel_PreviousName.Fields.Add(objField_PreviousName)

                objField_PreviousName = New Ext.Net.ModelField
                objField_PreviousName.Name = "LAST_NAME"
                objField_PreviousName.Type = ModelFieldType.String
                objModel_PreviousName.Fields.Add(objField_PreviousName)

                objField_PreviousName = New Ext.Net.ModelField
                objField_PreviousName.Name = "COMMENTS"
                objField_PreviousName.Type = ModelFieldType.String
                objModel_PreviousName.Fields.Add(objField_PreviousName)

                objStore_PreviousName.Model.Add(objModel_PreviousName)

                Dim objListColumn_PreviousName As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListColumn_PreviousName.Add(objcolumnNo)
                End Using

                Dim objColumn_PreviousName As Ext.Net.Column

                objColumn_PreviousName = New Ext.Net.Column
                objColumn_PreviousName.Text = "First Name"
                objColumn_PreviousName.DataIndex = "FIRST_NAME"
                objColumn_PreviousName.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_PreviousName.Flex = 1
                objListColumn_PreviousName.Add(objColumn_PreviousName)

                objColumn_PreviousName = New Ext.Net.Column
                objColumn_PreviousName.Text = "Last Name"
                objColumn_PreviousName.DataIndex = "LAST_NAME"
                objColumn_PreviousName.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_PreviousName.Flex = 1
                objListColumn_PreviousName.Add(objColumn_PreviousName)

                objColumn_PreviousName = New Ext.Net.Column
                objColumn_PreviousName.Text = "Comments"
                objColumn_PreviousName.DataIndex = "COMMENTS"
                objColumn_PreviousName.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_PreviousName.Flex = 1
                objListColumn_PreviousName.Add(objColumn_PreviousName)

                Dim objDt_PreviousName As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_PreviousName)
                '' End Previous Name

                '' Email, Add By Septian, 2023-01-27
                Dim objStore_Email As New Ext.Net.Store
                objStore_Email.ID = strunik & "StoreGrid_Email"
                objStore_Email.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModel_Email As New Ext.Net.Model
                Dim objField_Email As Ext.Net.ModelField

                objField_Email = New Ext.Net.ModelField
                objField_Email.Name = "PK_goAML_Ref_Customer_Previous_Name_ID"
                objField_Email.Type = ModelFieldType.Auto
                objModel_Email.Fields.Add(objField_Email)

                objField_Email = New Ext.Net.ModelField
                objField_Email.Name = "CIF"
                objField_Email.Type = ModelFieldType.String
                objModel_Email.Fields.Add(objField_Email)

                objField_Email = New Ext.Net.ModelField
                objField_Email.Name = "EMAIL_ADDRESS"
                objField_Email.Type = ModelFieldType.String
                objModel_Email.Fields.Add(objField_Email)

                objStore_Email.Model.Add(objModel_Email)

                Dim objListColumn_Email As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListColumn_Email.Add(objcolumnNo)
                End Using

                Dim objColumn_Email As Ext.Net.Column

                objColumn_Email = New Ext.Net.Column
                objColumn_Email.Text = "Email Address"
                objColumn_Email.DataIndex = "EMAIL_ADDRESS"
                objColumn_Email.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_Email.Flex = 1
                objListColumn_Email.Add(objColumn_Email)

                Dim objDt_Email As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Email)
                '' End Email

                '' Network Device, Add By Septian, 2023-01-27
                Dim objStore_NetworkDevice As New Ext.Net.Store
                objStore_NetworkDevice.ID = strunik & "StoreGrid_NetworkDevice"
                objStore_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModel_NetworkDevice As New Ext.Net.Model
                Dim objField_NetworkDevice As Ext.Net.ModelField

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "PK_goAML_Ref_Customer_Network_Device_ID"
                objField_NetworkDevice.Type = ModelFieldType.Auto
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "CIF"
                objField_NetworkDevice.Type = ModelFieldType.String
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "DEVICE_NUMBER"
                objField_NetworkDevice.Type = ModelFieldType.String
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "OPERATING_SYSTEM"
                objField_NetworkDevice.Type = ModelFieldType.String
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "SERVICE_PROVIDER"
                objField_NetworkDevice.Type = ModelFieldType.String
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "FK_GOAML_REPORT_ID"
                objField_NetworkDevice.Type = ModelFieldType.Int
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "IPV6"
                objField_NetworkDevice.Type = ModelFieldType.String
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "IPV4"
                objField_NetworkDevice.Type = ModelFieldType.String
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "CGN_PORT"
                objField_NetworkDevice.Type = ModelFieldType.Int
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "IPV4_IPV6"
                objField_NetworkDevice.Type = ModelFieldType.String
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "NAT"
                objField_NetworkDevice.Type = ModelFieldType.String
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "FIRST_SEEN_DATE"
                objField_NetworkDevice.Type = ModelFieldType.Date
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "LAST_SEEN_DATE"
                objField_NetworkDevice.Type = ModelFieldType.Date
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "USING_PROXY"
                objField_NetworkDevice.Type = ModelFieldType.Boolean
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "CITY"
                objField_NetworkDevice.Type = ModelFieldType.String
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "COUNTRY"
                objField_NetworkDevice.Type = ModelFieldType.String
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objField_NetworkDevice = New Ext.Net.ModelField
                objField_NetworkDevice.Name = "COMMENTS"
                objField_NetworkDevice.Type = ModelFieldType.String
                objModel_NetworkDevice.Fields.Add(objField_NetworkDevice)

                objStore_NetworkDevice.Model.Add(objModel_NetworkDevice)

                Dim objListColumn_NetworkDevice As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListColumn_NetworkDevice.Add(objcolumnNo)
                End Using

                Dim objColumn_NetworkDevice As Ext.Net.Column

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "Device Number"
                objColumn_NetworkDevice.DataIndex = "DEVICE_NUMBER"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "Operating System"
                objColumn_NetworkDevice.DataIndex = "OPERATING_SYSTEM"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "Service Provider"
                objColumn_NetworkDevice.DataIndex = "SERVICE_PROVIDER"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "IP v6"
                objColumn_NetworkDevice.DataIndex = "IPV6"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "IP v4"
                objColumn_NetworkDevice.DataIndex = "IPV4"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "CGN Port"
                objColumn_NetworkDevice.DataIndex = "CGN_PORT"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "IP v6 with IP v4"
                objColumn_NetworkDevice.DataIndex = "IPV4_IPV6"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "Nat"
                objColumn_NetworkDevice.DataIndex = "NAT"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "First Seen Date"
                objColumn_NetworkDevice.DataIndex = "FIRST_SEEN_DATE"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "Last Seen Date"
                objColumn_NetworkDevice.DataIndex = "LAST_SEEN_DATE"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "Using Proxy"
                objColumn_NetworkDevice.DataIndex = "USING_PROXY"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "City"
                objColumn_NetworkDevice.DataIndex = "CITY"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "Country"
                objColumn_NetworkDevice.DataIndex = "COUNTRY"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                objColumn_NetworkDevice = New Ext.Net.Column
                objColumn_NetworkDevice.Text = "Comments"
                objColumn_NetworkDevice.DataIndex = "COMMENTS"
                objColumn_NetworkDevice.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_NetworkDevice.Flex = 1
                objListColumn_NetworkDevice.Add(objColumn_NetworkDevice)

                Dim objDt_NetworkDevice As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_NetworkDevice)

                'End Network Device

                '' Social Media, Add By Septian, 2023-01-28
                Dim objStore_SocialMedia As New Ext.Net.Store
                objStore_SocialMedia.ID = strunik & "StoreGrid_SocialMedia"
                objStore_SocialMedia.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModel_SocialMedia As New Ext.Net.Model
                Dim objField_SocialMedia As Ext.Net.ModelField

                objField_SocialMedia = New Ext.Net.ModelField
                objField_SocialMedia.Name = "PK_goAML_Ref_Customer_Network_Device_ID"
                objField_SocialMedia.Type = ModelFieldType.Auto
                objModel_SocialMedia.Fields.Add(objField_SocialMedia)

                objField_SocialMedia = New Ext.Net.ModelField
                objField_SocialMedia.Name = "CIF"
                objField_SocialMedia.Type = ModelFieldType.String
                objModel_SocialMedia.Fields.Add(objField_SocialMedia)

                objField_SocialMedia = New Ext.Net.ModelField
                objField_SocialMedia.Name = "PLATFORM"
                objField_SocialMedia.Type = ModelFieldType.String
                objModel_SocialMedia.Fields.Add(objField_SocialMedia)

                objField_SocialMedia = New Ext.Net.ModelField
                objField_SocialMedia.Name = "USER_NAME"
                objField_SocialMedia.Type = ModelFieldType.String
                objModel_SocialMedia.Fields.Add(objField_SocialMedia)

                objField_SocialMedia = New Ext.Net.ModelField
                objField_SocialMedia.Name = "COMMENTS"
                objField_SocialMedia.Type = ModelFieldType.String
                objModel_SocialMedia.Fields.Add(objField_SocialMedia)

                objStore_SocialMedia.Model.Add(objModel_SocialMedia)

                Dim objListColumn_SocialMedia As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListColumn_SocialMedia.Add(objcolumnNo)
                End Using

                Dim objColumn_SocialMedia As Ext.Net.Column

                objColumn_SocialMedia = New Ext.Net.Column
                objColumn_SocialMedia.Text = "Platform"
                objColumn_SocialMedia.DataIndex = "PLATFORM"
                objColumn_SocialMedia.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_SocialMedia.Flex = 1
                objListColumn_SocialMedia.Add(objColumn_SocialMedia)

                objColumn_SocialMedia = New Ext.Net.Column
                objColumn_SocialMedia.Text = "User Name"
                objColumn_SocialMedia.DataIndex = "USER_NAME"
                objColumn_SocialMedia.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_SocialMedia.Flex = 1
                objListColumn_SocialMedia.Add(objColumn_SocialMedia)

                objColumn_SocialMedia = New Ext.Net.Column
                objColumn_SocialMedia.Text = "Comments"
                objColumn_SocialMedia.DataIndex = "COMMENTS"
                objColumn_SocialMedia.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_SocialMedia.Flex = 1
                objListColumn_SocialMedia.Add(objColumn_SocialMedia)

                Dim objDt_SocialMedia As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_SocialMedia)
                '' End Social Media

                '' Employment History, Add By Septian, 2023-01-28
                Dim objStore_EmploymentHistory As New Ext.Net.Store
                objStore_EmploymentHistory.ID = strunik & "StoreGrid_EmploymentHistory"
                objStore_EmploymentHistory.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModel_EmploymentHistory As New Ext.Net.Model
                Dim objField_EmploymentHistory As Ext.Net.ModelField

                objField_EmploymentHistory = New Ext.Net.ModelField
                objField_EmploymentHistory.Name = "PK_goAML_Ref_Customer_Employment_History_ID"
                objField_EmploymentHistory.Type = ModelFieldType.Auto
                objModel_EmploymentHistory.Fields.Add(objField_EmploymentHistory)

                objField_EmploymentHistory = New Ext.Net.ModelField
                objField_EmploymentHistory.Name = "CIF"
                objField_EmploymentHistory.Type = ModelFieldType.String
                objModel_EmploymentHistory.Fields.Add(objField_EmploymentHistory)

                objField_EmploymentHistory = New Ext.Net.ModelField
                objField_EmploymentHistory.Name = "EMPLOYER_NAME"
                objField_EmploymentHistory.Type = ModelFieldType.String
                objModel_EmploymentHistory.Fields.Add(objField_EmploymentHistory)

                objField_EmploymentHistory = New Ext.Net.ModelField
                objField_EmploymentHistory.Name = "EMPLOYER_BUSINESS"
                objField_EmploymentHistory.Type = ModelFieldType.String
                objModel_EmploymentHistory.Fields.Add(objField_EmploymentHistory)

                objField_EmploymentHistory = New Ext.Net.ModelField
                objField_EmploymentHistory.Name = "EMPLOYER_IDENTIFIER"
                objField_EmploymentHistory.Type = ModelFieldType.String
                objModel_EmploymentHistory.Fields.Add(objField_EmploymentHistory)

                objField_EmploymentHistory = New Ext.Net.ModelField
                objField_EmploymentHistory.Name = "EMPLOYMENT_PERIOD_VALID_FROM"
                objField_EmploymentHistory.Type = ModelFieldType.Date
                objModel_EmploymentHistory.Fields.Add(objField_EmploymentHistory)

                objField_EmploymentHistory = New Ext.Net.ModelField
                objField_EmploymentHistory.Name = "EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE"
                objField_EmploymentHistory.Type = ModelFieldType.Boolean
                objModel_EmploymentHistory.Fields.Add(objField_EmploymentHistory)

                objField_EmploymentHistory = New Ext.Net.ModelField
                objField_EmploymentHistory.Name = "EMPLOYMENT_PERIOD_VALID_TO"
                objField_EmploymentHistory.Type = ModelFieldType.Date
                objModel_EmploymentHistory.Fields.Add(objField_EmploymentHistory)

                objField_EmploymentHistory = New Ext.Net.ModelField
                objField_EmploymentHistory.Name = "EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE"
                objField_EmploymentHistory.Type = ModelFieldType.Boolean
                objModel_EmploymentHistory.Fields.Add(objField_EmploymentHistory)

                objStore_EmploymentHistory.Model.Add(objModel_EmploymentHistory)

                Dim objListColumn_EmploymentHistory As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListColumn_EmploymentHistory.Add(objcolumnNo)
                End Using

                Dim objColumn_EmploymentHistory As Ext.Net.Column

                objColumn_EmploymentHistory = New Ext.Net.Column
                objColumn_EmploymentHistory.Text = "Employer Name"
                objColumn_EmploymentHistory.DataIndex = "EMPLOYER_NAME"
                objColumn_EmploymentHistory.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EmploymentHistory.Flex = 1
                objListColumn_EmploymentHistory.Add(objColumn_EmploymentHistory)

                objColumn_EmploymentHistory = New Ext.Net.Column
                objColumn_EmploymentHistory.Text = "Employer Business"
                objColumn_EmploymentHistory.DataIndex = "EMPLOYER_BUSINESS"
                objColumn_EmploymentHistory.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EmploymentHistory.Flex = 1
                objListColumn_EmploymentHistory.Add(objColumn_EmploymentHistory)

                objColumn_EmploymentHistory = New Ext.Net.Column
                objColumn_EmploymentHistory.Text = "Employer Identifier"
                objColumn_EmploymentHistory.DataIndex = "EMPLOYER_IDENTIFIER"
                objColumn_EmploymentHistory.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EmploymentHistory.Flex = 1
                objListColumn_EmploymentHistory.Add(objColumn_EmploymentHistory)

                objColumn_EmploymentHistory = New Ext.Net.Column
                objColumn_EmploymentHistory.Text = "Employment Period Valid From"
                objColumn_EmploymentHistory.DataIndex = "EMPLOYMENT_PERIOD_VALID_FROM"
                objColumn_EmploymentHistory.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EmploymentHistory.Flex = 1
                objListColumn_EmploymentHistory.Add(objColumn_EmploymentHistory)

                objColumn_EmploymentHistory = New Ext.Net.Column
                objColumn_EmploymentHistory.Text = "Employment Perios Is Approx From Date"
                objColumn_EmploymentHistory.DataIndex = "EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE"
                objColumn_EmploymentHistory.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EmploymentHistory.Flex = 1
                objListColumn_EmploymentHistory.Add(objColumn_EmploymentHistory)

                objColumn_EmploymentHistory = New Ext.Net.Column
                objColumn_EmploymentHistory.Text = "Employment Period Valid To"
                objColumn_EmploymentHistory.DataIndex = "EMPLOYMENT_PERIOD_VALID_TO"
                objColumn_EmploymentHistory.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EmploymentHistory.Flex = 1
                objListColumn_EmploymentHistory.Add(objColumn_EmploymentHistory)

                objColumn_EmploymentHistory = New Ext.Net.Column
                objColumn_EmploymentHistory.Text = "Employment Period Is Approx To Date"
                objColumn_EmploymentHistory.DataIndex = "EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE"
                objColumn_EmploymentHistory.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EmploymentHistory.Flex = 1
                objListColumn_EmploymentHistory.Add(objColumn_EmploymentHistory)

                Dim objDt_EmploymentHistory As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_EmploymentHistory)
                '' End Employment History

                '' Entity Identification, Add By Septian, 2023-01-28
                Dim objStore_EntityIdentification As New Ext.Net.Store
                objStore_EntityIdentification.ID = strunik & "StoreGrid_EntityIdentification"
                objStore_EntityIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModel_EntityIdentification As New Ext.Net.Model
                Dim objField_EntityIdentification As Ext.Net.ModelField

                objField_EntityIdentification = New Ext.Net.ModelField
                objField_EntityIdentification.Name = "PK_goAML_Ref_Customer_Entity_Identifications_ID"
                objField_EntityIdentification.Type = ModelFieldType.Auto
                objModel_EntityIdentification.Fields.Add(objField_EntityIdentification)

                objField_EntityIdentification = New Ext.Net.ModelField
                objField_EntityIdentification.Name = "CIF"
                objField_EntityIdentification.Type = ModelFieldType.String
                objModel_EntityIdentification.Fields.Add(objField_EntityIdentification)

                objField_EntityIdentification = New Ext.Net.ModelField
                objField_EntityIdentification.Name = "TYPE"
                objField_EntityIdentification.Type = ModelFieldType.String
                objModel_EntityIdentification.Fields.Add(objField_EntityIdentification)

                objField_EntityIdentification = New Ext.Net.ModelField
                objField_EntityIdentification.Name = "NUMBER"
                objField_EntityIdentification.Type = ModelFieldType.String
                objModel_EntityIdentification.Fields.Add(objField_EntityIdentification)

                objField_EntityIdentification = New Ext.Net.ModelField
                objField_EntityIdentification.Name = "ISSUE_DATE"
                objField_EntityIdentification.Type = ModelFieldType.Date
                objModel_EntityIdentification.Fields.Add(objField_EntityIdentification)

                objField_EntityIdentification = New Ext.Net.ModelField
                objField_EntityIdentification.Name = "EXPIRY_DATE"
                objField_EntityIdentification.Type = ModelFieldType.Date
                objModel_EntityIdentification.Fields.Add(objField_EntityIdentification)

                objField_EntityIdentification = New Ext.Net.ModelField
                objField_EntityIdentification.Name = "ISSUED_BY"
                objField_EntityIdentification.Type = ModelFieldType.String
                objModel_EntityIdentification.Fields.Add(objField_EntityIdentification)

                objField_EntityIdentification = New Ext.Net.ModelField
                objField_EntityIdentification.Name = "ISSUE_COUNTRY"
                objField_EntityIdentification.Type = ModelFieldType.String
                objModel_EntityIdentification.Fields.Add(objField_EntityIdentification)

                objField_EntityIdentification = New Ext.Net.ModelField
                objField_EntityIdentification.Name = "COMMENTS"
                objField_EntityIdentification.Type = ModelFieldType.String
                objModel_EntityIdentification.Fields.Add(objField_EntityIdentification)

                objStore_EntityIdentification.Model.Add(objModel_EntityIdentification)

                Dim objListColumn_EntityIdentification As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListColumn_EntityIdentification.Add(objcolumnNo)
                End Using

                Dim objColumn_EntityIdentification As Ext.Net.Column

                objColumn_EntityIdentification = New Ext.Net.Column
                objColumn_EntityIdentification.Text = "Type"
                objColumn_EntityIdentification.DataIndex = "TYPE"
                objColumn_EntityIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EntityIdentification.Flex = 1
                objListColumn_EntityIdentification.Add(objColumn_EntityIdentification)

                objColumn_EntityIdentification = New Ext.Net.Column
                objColumn_EntityIdentification.Text = "Number"
                objColumn_EntityIdentification.DataIndex = "NUMBER"
                objColumn_EntityIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EntityIdentification.Flex = 1
                objListColumn_EntityIdentification.Add(objColumn_EntityIdentification)

                objColumn_EntityIdentification = New Ext.Net.Column
                objColumn_EntityIdentification.Text = "Issue Date"
                objColumn_EntityIdentification.DataIndex = "ISSUE_DATE"
                objColumn_EntityIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EntityIdentification.Flex = 1
                objListColumn_EntityIdentification.Add(objColumn_EntityIdentification)


                objColumn_EntityIdentification = New Ext.Net.Column
                objColumn_EntityIdentification.Text = "Expiry Date"
                objColumn_EntityIdentification.DataIndex = "EXPIRY_DATE"
                objColumn_EntityIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EntityIdentification.Flex = 1
                objListColumn_EntityIdentification.Add(objColumn_EntityIdentification)

                objColumn_EntityIdentification = New Ext.Net.Column
                objColumn_EntityIdentification.Text = "Issued By"
                objColumn_EntityIdentification.DataIndex = "ISSUED_BY"
                objColumn_EntityIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EntityIdentification.Flex = 1
                objListColumn_EntityIdentification.Add(objColumn_EntityIdentification)

                objColumn_EntityIdentification = New Ext.Net.Column
                objColumn_EntityIdentification.Text = "Issue Country"
                objColumn_EntityIdentification.DataIndex = "ISSUE_COUNTRY"
                objColumn_EntityIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EntityIdentification.Flex = 1
                objListColumn_EntityIdentification.Add(objColumn_EntityIdentification)

                objColumn_EntityIdentification = New Ext.Net.Column
                objColumn_EntityIdentification.Text = "Comments"
                objColumn_EntityIdentification.DataIndex = "COMMENTS"
                objColumn_EntityIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_EntityIdentification.Flex = 1
                objListColumn_EntityIdentification.Add(objColumn_EntityIdentification)

                Dim objDt_EntityIdentification As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_EntityIdentification)
                '' End Entity Identification

                '' PEP, Add By Septian, 2023-01-28
                Dim objStore_PEP As New Ext.Net.Store
                objStore_PEP.ID = strunik & "StoreGrid_PEP"
                objStore_PEP.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModel_PEP As New Ext.Net.Model
                Dim objField_PEP As Ext.Net.ModelField

                objField_PEP = New Ext.Net.ModelField
                objField_PEP.Name = "PK_goAML_Ref_Customer_Network_Device_ID"
                objField_PEP.Type = ModelFieldType.Auto
                objModel_PEP.Fields.Add(objField_PEP)

                objField_PEP = New Ext.Net.ModelField
                objField_PEP.Name = "CIF"
                objField_PEP.Type = ModelFieldType.String
                objModel_PEP.Fields.Add(objField_PEP)

                objField_PEP = New Ext.Net.ModelField
                objField_PEP.Name = "PEP_COUNTRY"
                objField_PEP.Type = ModelFieldType.String
                objModel_PEP.Fields.Add(objField_PEP)

                objField_PEP = New Ext.Net.ModelField
                objField_PEP.Name = "FUNCTION_NAME"
                objField_PEP.Type = ModelFieldType.String
                objModel_PEP.Fields.Add(objField_PEP)

                objField_PEP = New Ext.Net.ModelField
                objField_PEP.Name = "FUNCTION_DESCRIPTION"
                objField_PEP.Type = ModelFieldType.String
                objModel_PEP.Fields.Add(objField_PEP)

                objField_PEP = New Ext.Net.ModelField
                objField_PEP.Name = "PEP_DATE_RANGE_VALID_FROM"
                objField_PEP.Type = ModelFieldType.Date
                objModel_PEP.Fields.Add(objField_PEP)

                objField_PEP = New Ext.Net.ModelField
                objField_PEP.Name = "PEP_DATE_RANGE_IS_APPROX_FROM_DATE"
                objField_PEP.Type = ModelFieldType.Boolean
                objModel_PEP.Fields.Add(objField_PEP)

                objField_PEP = New Ext.Net.ModelField
                objField_PEP.Name = "PEP_DATE_RANGE_VALID_TO"
                objField_PEP.Type = ModelFieldType.Date
                objModel_PEP.Fields.Add(objField_PEP)

                objField_PEP = New Ext.Net.ModelField
                objField_PEP.Name = "PEP_DATE_RANGE_IS_APPROX_TO_DATE"
                objField_PEP.Type = ModelFieldType.Boolean
                objModel_PEP.Fields.Add(objField_PEP)

                objField_PEP = New Ext.Net.ModelField
                objField_PEP.Name = "COMMENTS"
                objField_PEP.Type = ModelFieldType.String
                objModel_PEP.Fields.Add(objField_PEP)

                objStore_PEP.Model.Add(objModel_PEP)

                Dim objListColumn_PEP As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListColumn_PEP.Add(objcolumnNo)
                End Using

                Dim objColumn_PEP As Ext.Net.Column

                objColumn_PEP = New Ext.Net.Column
                objColumn_PEP.Text = "Country"
                objColumn_PEP.DataIndex = "PEP_COUNTRY"
                objColumn_PEP.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_PEP.Flex = 1
                objListColumn_PEP.Add(objColumn_PEP)

                objColumn_PEP = New Ext.Net.Column
                objColumn_PEP.Text = "Function Name"
                objColumn_PEP.DataIndex = "FUNCTION_NAME"
                objColumn_PEP.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_PEP.Flex = 1
                objListColumn_PEP.Add(objColumn_PEP)

                objColumn_PEP = New Ext.Net.Column
                objColumn_PEP.Text = "Function Description"
                objColumn_PEP.DataIndex = "FUNCTION_DESCRIPTION"
                objColumn_PEP.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_PEP.Flex = 1
                objListColumn_PEP.Add(objColumn_PEP)

                objColumn_PEP = New Ext.Net.Column
                objColumn_PEP.Text = "Valid From"
                objColumn_PEP.DataIndex = "PEP_DATE_RANGE_VALID_FROM"
                objColumn_PEP.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_PEP.Flex = 1
                objListColumn_PEP.Add(objColumn_PEP)

                objColumn_PEP = New Ext.Net.Column
                objColumn_PEP.Text = "Is Approx From Date"
                objColumn_PEP.DataIndex = "PEP_DATE_RANGE_IS_APPROX_FROM_DATE"
                objColumn_PEP.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_PEP.Flex = 1
                objListColumn_PEP.Add(objColumn_PEP)

                objColumn_PEP = New Ext.Net.Column
                objColumn_PEP.Text = "Valid To"
                objColumn_PEP.DataIndex = "PEP_DATE_RANGE_VALID_TO"
                objColumn_PEP.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_PEP.Flex = 1
                objListColumn_PEP.Add(objColumn_PEP)

                objColumn_PEP = New Ext.Net.Column
                objColumn_PEP.Text = "Is Approx To Date"
                objColumn_PEP.DataIndex = "PEP_DATE_RANGE_IS_APPROX_TO_DATE"
                objColumn_PEP.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_PEP.Flex = 1
                objListColumn_PEP.Add(objColumn_PEP)

                objColumn_PEP = New Ext.Net.Column
                objColumn_PEP.Text = "Comments"
                objColumn_PEP.DataIndex = "COMMENTS"
                objColumn_PEP.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_PEP.Flex = 1
                objListColumn_PEP.Add(objColumn_PEP)

                Dim objDt_PEP As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_PEP)
                '' End PEP

                '' Sanction, Add By Septian, 2023-01-28
                Dim objStore_Sanction As New Ext.Net.Store
                objStore_Sanction.ID = strunik & "StoreGrid_Sanction"
                objStore_Sanction.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModel_Sanction As New Ext.Net.Model
                Dim objField_Sanction As Ext.Net.ModelField

                objField_Sanction = New Ext.Net.ModelField
                objField_Sanction.Name = "PK_goAML_Ref_Customer_Sanction_ID"
                objField_Sanction.Type = ModelFieldType.Auto
                objModel_Sanction.Fields.Add(objField_Sanction)

                objField_Sanction = New Ext.Net.ModelField
                objField_Sanction.Name = "CIF"
                objField_Sanction.Type = ModelFieldType.String
                objModel_Sanction.Fields.Add(objField_Sanction)

                objField_Sanction = New Ext.Net.ModelField
                objField_Sanction.Name = "PROVIDER"
                objField_Sanction.Type = ModelFieldType.String
                objModel_Sanction.Fields.Add(objField_Sanction)

                objField_Sanction = New Ext.Net.ModelField
                objField_Sanction.Name = "SANCTION_LIST_NAME"
                objField_Sanction.Type = ModelFieldType.String
                objModel_Sanction.Fields.Add(objField_Sanction)

                objField_Sanction = New Ext.Net.ModelField
                objField_Sanction.Name = "MATCH_CRITERIA"
                objField_Sanction.Type = ModelFieldType.String
                objModel_Sanction.Fields.Add(objField_Sanction)

                objField_Sanction = New Ext.Net.ModelField
                objField_Sanction.Name = "LINK_TO_SOURCE"
                objField_Sanction.Type = ModelFieldType.String
                objModel_Sanction.Fields.Add(objField_Sanction)

                objField_Sanction = New Ext.Net.ModelField
                objField_Sanction.Name = "SANCTION_LIST_ATTRIBUTES"
                objField_Sanction.Type = ModelFieldType.String
                objModel_Sanction.Fields.Add(objField_Sanction)

                objField_Sanction = New Ext.Net.ModelField
                objField_Sanction.Name = "SANCTION_LIST_DATE_RANGE_VALID_FROM"
                objField_Sanction.Type = ModelFieldType.Date
                objModel_Sanction.Fields.Add(objField_Sanction)

                objField_Sanction = New Ext.Net.ModelField
                objField_Sanction.Name = "SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE"
                objField_Sanction.Type = ModelFieldType.Boolean
                objModel_Sanction.Fields.Add(objField_Sanction)

                objField_Sanction = New Ext.Net.ModelField
                objField_Sanction.Name = "SANCTION_LIST_DATE_RANGE_VALID_TO"
                objField_Sanction.Type = ModelFieldType.Date
                objModel_Sanction.Fields.Add(objField_Sanction)

                objField_Sanction = New Ext.Net.ModelField
                objField_Sanction.Name = "SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE"
                objField_Sanction.Type = ModelFieldType.Boolean
                objModel_Sanction.Fields.Add(objField_Sanction)

                objField_Sanction = New Ext.Net.ModelField
                objField_Sanction.Name = "COMMENTS"
                objField_Sanction.Type = ModelFieldType.String
                objModel_Sanction.Fields.Add(objField_Sanction)

                objStore_Sanction.Model.Add(objModel_Sanction)

                Dim objListColumn_Sanction As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListColumn_Sanction.Add(objcolumnNo)
                End Using

                Dim objColumn_Sanction As Ext.Net.Column

                objColumn_Sanction = New Ext.Net.Column
                objColumn_Sanction.Text = "Provider"
                objColumn_Sanction.DataIndex = "PROVIDER"
                objColumn_Sanction.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_Sanction.Flex = 1
                objListColumn_Sanction.Add(objColumn_Sanction)

                objColumn_Sanction = New Ext.Net.Column
                objColumn_Sanction.Text = "Sanction List Name"
                objColumn_Sanction.DataIndex = "SANCTION_LIST_NAME"
                objColumn_Sanction.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_Sanction.Flex = 1
                objListColumn_Sanction.Add(objColumn_Sanction)

                objColumn_Sanction = New Ext.Net.Column
                objColumn_Sanction.Text = "Match Criteria"
                objColumn_Sanction.DataIndex = "MATCH_CRITERIA"
                objColumn_Sanction.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_Sanction.Flex = 1
                objListColumn_Sanction.Add(objColumn_Sanction)

                objColumn_Sanction = New Ext.Net.Column
                objColumn_Sanction.Text = "Link To Source"
                objColumn_Sanction.DataIndex = "LINK_TO_SOURCE"
                objColumn_Sanction.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_Sanction.Flex = 1
                objListColumn_Sanction.Add(objColumn_Sanction)

                objColumn_Sanction = New Ext.Net.Column
                objColumn_Sanction.Text = "Attributes"
                objColumn_Sanction.DataIndex = "SANCTION_LIST_ATTRIBUTES"
                objColumn_Sanction.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_Sanction.Flex = 1
                objListColumn_Sanction.Add(objColumn_Sanction)

                objColumn_Sanction = New Ext.Net.Column
                objColumn_Sanction.Text = "Valid From"
                objColumn_Sanction.DataIndex = "SANCTION_LIST_DATE_RANGE_VALID_FROM"
                objColumn_Sanction.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_Sanction.Flex = 1
                objListColumn_Sanction.Add(objColumn_Sanction)

                objColumn_Sanction = New Ext.Net.Column
                objColumn_Sanction.Text = "Is Approx From Date ?"
                objColumn_Sanction.DataIndex = "SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE"
                objColumn_Sanction.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_Sanction.Flex = 1
                objListColumn_Sanction.Add(objColumn_Sanction)

                objColumn_Sanction = New Ext.Net.Column
                objColumn_Sanction.Text = "Valid To"
                objColumn_Sanction.DataIndex = "SANCTION_LIST_DATE_RANGE_VALID_TO"
                objColumn_Sanction.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_Sanction.Flex = 1
                objListColumn_Sanction.Add(objColumn_Sanction)

                objColumn_Sanction = New Ext.Net.Column
                objColumn_Sanction.Text = "Is Approx To Date ?"
                objColumn_Sanction.DataIndex = "SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE"
                objColumn_Sanction.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_Sanction.Flex = 1
                objListColumn_Sanction.Add(objColumn_Sanction)

                objColumn_Sanction = New Ext.Net.Column
                objColumn_Sanction.Text = "Comments"
                objColumn_Sanction.DataIndex = "COMMENTS"
                objColumn_Sanction.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_Sanction.Flex = 1
                objListColumn_Sanction.Add(objColumn_Sanction)

                Dim objDt_Sanction As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_Sanction)
                '' End Sanction

                '' Related Person, Add By Septian, 2023-01-28
                Dim objStore_RelatedPerson As New Ext.Net.Store
                objStore_RelatedPerson.ID = strunik & "StoreGrid_RelatedPerson"
                objStore_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModel_RelatedPerson As New Ext.Net.Model
                Dim objField_RelatedPerson As Ext.Net.ModelField

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "PK_goAML_Ref_Customer_Related_Person_ID"
                objField_RelatedPerson.Type = ModelFieldType.Auto
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "CIF"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "PERSON_PERSON_RELATION"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "RELATION_DATE_RANGE_VALID_FROM"
                objField_RelatedPerson.Type = ModelFieldType.Date
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "RELATION_DATE_RANGE_IS_APPROX_FROM_DATE"
                objField_RelatedPerson.Type = ModelFieldType.Boolean
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "RELATION_DATE_RANGE_VALID_TO"
                objField_RelatedPerson.Type = ModelFieldType.Date
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "RELATION_DATE_RANGE_IS_APPROX_TO_DATE"
                objField_RelatedPerson.Type = ModelFieldType.Boolean
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "COMMENTS"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "GENDER"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "TITLE"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "FIRST_NAME"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "MIDDLE_NAME"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "PREFIX"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "LAST_NAME"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "BIRTHDATE"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "BIRTH_PLACE"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "COUNTRY_OF_BIRTH"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "MOTHERS_NAME"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "ALIAS"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "FULL_NAME_FRN"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "SSN"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "PASSPORT_NUMBER"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "PASSPORT_COUNTRY"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "ID_NUMBER"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "NATIONALITY1"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "NATIONALITY2"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "NATIONALITY3"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "RESIDENCE"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "RESIDENCE_SINCE"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "OCCUPATION"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "DECEASED"
                objField_RelatedPerson.Type = ModelFieldType.Boolean
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "DATE_DECEASED"
                objField_RelatedPerson.Type = ModelFieldType.Date
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "TAX_NUMBER"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "TAX_REG_NUMBER"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "SOURCE_OF_WEALTH"
                objField_RelatedPerson.Type = ModelFieldType.String
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objField_RelatedPerson = New Ext.Net.ModelField
                objField_RelatedPerson.Name = "IS_PROTECTED"
                objField_RelatedPerson.Type = ModelFieldType.Boolean
                objModel_RelatedPerson.Fields.Add(objField_RelatedPerson)

                objStore_RelatedPerson.Model.Add(objModel_RelatedPerson)

                Dim objListColumn_RelatedPerson As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListColumn_RelatedPerson.Add(objcolumnNo)
                End Using

                Dim objColumn_RelatedPerson As Ext.Net.Column

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Person Relation"
                objColumn_RelatedPerson.DataIndex = "PERSON_PERSON_RELATION"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                'objColumn_RelatedPerson = New Ext.Net.Column
                'objColumn_RelatedPerson.Text = "Valid From"
                'objColumn_RelatedPerson.DataIndex = "RELATION_DATE_RANGE_VALID_FROM"
                'objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColumn_RelatedPerson.Flex = 1
                'objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                'objColumn_RelatedPerson = New Ext.Net.Column
                'objColumn_RelatedPerson.Text = "Is Approx From Date?"
                'objColumn_RelatedPerson.DataIndex = "RELATION_DATE_RANGE_IS_APPROX_FROM_DATE"
                'objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColumn_RelatedPerson.Flex = 1
                'objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                'objColumn_RelatedPerson = New Ext.Net.Column
                'objColumn_RelatedPerson.Text = "Valid To"
                'objColumn_RelatedPerson.DataIndex = "RELATION_DATE_RANGE_VALID_TO"
                'objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColumn_RelatedPerson.Flex = 1
                'objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                'objColumn_RelatedPerson = New Ext.Net.Column
                'objColumn_RelatedPerson.Text = "Is Approx To Date"
                'objColumn_RelatedPerson.DataIndex = "RELATION_DATE_RANGE_IS_APPROX_TO_DATE"
                'objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColumn_RelatedPerson.Flex = 1
                'objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Comments"
                objColumn_RelatedPerson.DataIndex = "COMMENTS"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Gender"
                objColumn_RelatedPerson.DataIndex = "GENDER"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Title"
                objColumn_RelatedPerson.DataIndex = "TITLE"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "First Name"
                objColumn_RelatedPerson.DataIndex = "FIRST_NAME"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Middle Name"
                objColumn_RelatedPerson.DataIndex = "MIDDLE_NAME"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Prefix"
                objColumn_RelatedPerson.DataIndex = "PREFIX"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Last Name"
                objColumn_RelatedPerson.DataIndex = "LAST_NAME"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Birth Date"
                objColumn_RelatedPerson.DataIndex = "BIRTHDATE"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Birth Place"
                objColumn_RelatedPerson.DataIndex = "BIRTH_PLACE"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Country Of Birth"
                objColumn_RelatedPerson.DataIndex = "COUNTRY_OF_BIRTH"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Mothers Name"
                objColumn_RelatedPerson.DataIndex = "MOTHERS_NAME"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Alias"
                objColumn_RelatedPerson.DataIndex = "ALIAS"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                'objColumn_RelatedPerson = New Ext.Net.Column
                'objColumn_RelatedPerson.Text = "Foreign Name"
                'objColumn_RelatedPerson.DataIndex = "FULL_NAME_FRN"
                'objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColumn_RelatedPerson.Flex = 1
                'objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "SSN"
                objColumn_RelatedPerson.DataIndex = "SSN"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Passport Number"
                objColumn_RelatedPerson.DataIndex = "PASSPORT_NUMBER"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Passport Country"
                objColumn_RelatedPerson.DataIndex = "PASSPORT_COUNTRY"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Id Number"
                objColumn_RelatedPerson.DataIndex = "ID_NUMBER"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Nationality 1"
                objColumn_RelatedPerson.DataIndex = "NATIONALITY1"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Nationality 2"
                objColumn_RelatedPerson.DataIndex = "NATIONALITY2"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Nationality 3"
                objColumn_RelatedPerson.DataIndex = "NATIONALITY3"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Residence"
                objColumn_RelatedPerson.DataIndex = "RESIDENCE"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                'objColumn_RelatedPerson = New Ext.Net.Column
                'objColumn_RelatedPerson.Text = "Residence Since"
                'objColumn_RelatedPerson.DataIndex = "RESIDENCE_SINCE"
                'objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColumn_RelatedPerson.Flex = 1
                'objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Occupation"
                objColumn_RelatedPerson.DataIndex = "OCCUPATION"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Deceased"
                objColumn_RelatedPerson.DataIndex = "DECEASED"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Date Deceased"
                objColumn_RelatedPerson.DataIndex = "DATE_DECEASED"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Tax Number"
                objColumn_RelatedPerson.DataIndex = "TAX_NUMBER"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Tax Reg Number"
                objColumn_RelatedPerson.DataIndex = "TAX_REG_NUMBER"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Source Of Wealth"
                objColumn_RelatedPerson.DataIndex = "SOURCE_OF_WEALTH"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                objColumn_RelatedPerson = New Ext.Net.Column
                objColumn_RelatedPerson.Text = "Is Protected"
                objColumn_RelatedPerson.DataIndex = "IS_PROTECTED"
                objColumn_RelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedPerson.Flex = 1
                objListColumn_RelatedPerson.Add(objColumn_RelatedPerson)

                Dim objDt_RelatedPerson As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedPerson)
                '' End Related Person

                '' Additional Information, Add By Septian, 2023-01-28
                Dim objStore_AdditionalInfo As New Ext.Net.Store
                objStore_AdditionalInfo.ID = strunik & "StoreGrid_AdditionalInfo"
                objStore_AdditionalInfo.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModel_AdditionalInfo As New Ext.Net.Model
                Dim objField_AdditionalInfo As Ext.Net.ModelField

                objField_AdditionalInfo = New Ext.Net.ModelField
                objField_AdditionalInfo.Name = "PK_goAML_Ref_Customer_Additional_Information_ID"
                objField_AdditionalInfo.Type = ModelFieldType.Auto
                objModel_AdditionalInfo.Fields.Add(objField_AdditionalInfo)

                objField_AdditionalInfo = New Ext.Net.ModelField
                objField_AdditionalInfo.Name = "CIF"
                objField_AdditionalInfo.Type = ModelFieldType.String
                objModel_AdditionalInfo.Fields.Add(objField_AdditionalInfo)

                objField_AdditionalInfo = New Ext.Net.ModelField
                objField_AdditionalInfo.Name = "INFO_TYPE"
                objField_AdditionalInfo.Type = ModelFieldType.String
                objModel_AdditionalInfo.Fields.Add(objField_AdditionalInfo)

                objField_AdditionalInfo = New Ext.Net.ModelField
                objField_AdditionalInfo.Name = "INFO_SUBJECT"
                objField_AdditionalInfo.Type = ModelFieldType.String
                objModel_AdditionalInfo.Fields.Add(objField_AdditionalInfo)

                objField_AdditionalInfo = New Ext.Net.ModelField
                objField_AdditionalInfo.Name = "INFO_TEXT"
                objField_AdditionalInfo.Type = ModelFieldType.String
                objModel_AdditionalInfo.Fields.Add(objField_AdditionalInfo)

                objField_AdditionalInfo = New Ext.Net.ModelField
                objField_AdditionalInfo.Name = "INFO_NUMERIC"
                objField_AdditionalInfo.Type = ModelFieldType.Float
                objModel_AdditionalInfo.Fields.Add(objField_AdditionalInfo)

                objField_AdditionalInfo = New Ext.Net.ModelField
                objField_AdditionalInfo.Name = "INFO_DATE"
                objField_AdditionalInfo.Type = ModelFieldType.Date
                objModel_AdditionalInfo.Fields.Add(objField_AdditionalInfo)

                objField_AdditionalInfo = New Ext.Net.ModelField
                objField_AdditionalInfo.Name = "INFO_BOOLEAN"
                objField_AdditionalInfo.Type = ModelFieldType.Boolean
                objModel_AdditionalInfo.Fields.Add(objField_AdditionalInfo)

                objStore_AdditionalInfo.Model.Add(objModel_AdditionalInfo)

                Dim objListColumn_AdditionalInfo As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListColumn_AdditionalInfo.Add(objcolumnNo)
                End Using

                Dim objColumn_AdditionalInfo As Ext.Net.Column

                objColumn_AdditionalInfo = New Ext.Net.Column
                objColumn_AdditionalInfo.Text = "Info Type"
                objColumn_AdditionalInfo.DataIndex = "INFO_TYPE"
                objColumn_AdditionalInfo.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_AdditionalInfo.Flex = 1
                objListColumn_AdditionalInfo.Add(objColumn_AdditionalInfo)

                objColumn_AdditionalInfo = New Ext.Net.Column
                objColumn_AdditionalInfo.Text = "Info Subject"
                objColumn_AdditionalInfo.DataIndex = "INFO_SUBJECT"
                objColumn_AdditionalInfo.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_AdditionalInfo.Flex = 1
                objListColumn_AdditionalInfo.Add(objColumn_AdditionalInfo)

                objColumn_AdditionalInfo = New Ext.Net.Column
                objColumn_AdditionalInfo.Text = "Info Text"
                objColumn_AdditionalInfo.DataIndex = "INFO_TEXT"
                objColumn_AdditionalInfo.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_AdditionalInfo.Flex = 1
                objListColumn_AdditionalInfo.Add(objColumn_AdditionalInfo)

                objColumn_AdditionalInfo = New Ext.Net.Column
                objColumn_AdditionalInfo.Text = "Info Numeric"
                objColumn_AdditionalInfo.DataIndex = "INFO_NUMERIC"
                objColumn_AdditionalInfo.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_AdditionalInfo.Flex = 1
                objListColumn_AdditionalInfo.Add(objColumn_AdditionalInfo)

                objColumn_AdditionalInfo = New Ext.Net.Column
                objColumn_AdditionalInfo.Text = "Info Date"
                objColumn_AdditionalInfo.DataIndex = "INFO_DATE"
                objColumn_AdditionalInfo.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_AdditionalInfo.Flex = 1
                objListColumn_AdditionalInfo.Add(objColumn_AdditionalInfo)

                objColumn_AdditionalInfo = New Ext.Net.Column
                objColumn_AdditionalInfo.Text = "Info Boolean"
                objColumn_AdditionalInfo.DataIndex = "INFO_BOOLEAN"
                objColumn_AdditionalInfo.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_AdditionalInfo.Flex = 1
                objListColumn_AdditionalInfo.Add(objColumn_AdditionalInfo)

                Dim objDt_AdditionalInfo As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_AdditionalInformation)
                '' End Additional Information

                '' URL, Add By Septian, 2023-01-28
                Dim objStore_URL As New Ext.Net.Store
                objStore_URL.ID = strunik & "StoreGrid_URL"
                objStore_URL.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModel_URL As New Ext.Net.Model
                Dim objField_URL As Ext.Net.ModelField

                objField_URL = New Ext.Net.ModelField
                objField_URL.Name = "PK_goAML_Ref_Customer_URL_ID"
                objField_URL.Type = ModelFieldType.Auto
                objModel_URL.Fields.Add(objField_URL)

                objField_URL = New Ext.Net.ModelField
                objField_URL.Name = "CIF"
                objField_URL.Type = ModelFieldType.String
                objModel_URL.Fields.Add(objField_URL)

                objField_URL = New Ext.Net.ModelField
                objField_URL.Name = "URL"
                objField_URL.Type = ModelFieldType.String
                objModel_URL.Fields.Add(objField_URL)

                objStore_URL.Model.Add(objModel_URL)

                Dim objListColumn_URL As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListColumn_URL.Add(objcolumnNo)
                End Using

                Dim objColumn_URL As Ext.Net.Column

                objColumn_URL = New Ext.Net.Column
                objColumn_URL.Text = "Url"
                objColumn_URL.DataIndex = "URL"
                objColumn_URL.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_URL.Flex = 1
                objListColumn_URL.Add(objColumn_URL)

                Dim objDt_URL As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_URL)
                '' End URL

                '' Related Entities, Add By Septian, 2023-01-28
                Dim objStore_RelatedEntities As New Ext.Net.Store
                objStore_RelatedEntities.ID = strunik & "StoreGrid_RelatedEntities"
                objStore_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModel_RelatedEntities As New Ext.Net.Model
                Dim objField_RelatedEntities As Ext.Net.ModelField

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "PK_goAML_Ref_Customer_Related_Entities_ID"
                objField_RelatedEntities.Type = ModelFieldType.Auto
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "CIF"
                objField_RelatedEntities.Type = ModelFieldType.String
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "ENTITY_ENTITY_RELATION"
                objField_RelatedEntities.Type = ModelFieldType.String
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                'objField_RelatedEntities = New Ext.Net.ModelField
                'objField_RelatedEntities.Name = "RELATION_DATE_RANGE_VALID_FROM"
                'objField_RelatedEntities.Type = ModelFieldType.Date
                'objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                'objField_RelatedEntities = New Ext.Net.ModelField
                'objField_RelatedEntities.Name = "RELATION_DATE_RANGE_IS_APPROX_FROM_DATE"
                'objField_RelatedEntities.Type = ModelFieldType.Boolean
                'objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                'objField_RelatedEntities = New Ext.Net.ModelField
                'objField_RelatedEntities.Name = "RELATION_DATE_RANGE_VALID_TO"
                'objField_RelatedEntities.Type = ModelFieldType.Date
                'objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                'objField_RelatedEntities = New Ext.Net.ModelField
                'objField_RelatedEntities.Name = "RELATION_DATE_RANGE_IS_APPROX_TO_DATE"
                'objField_RelatedEntities.Type = ModelFieldType.Boolean
                'objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                'objField_RelatedEntities = New Ext.Net.ModelField
                'objField_RelatedEntities.Name = "SHARE_PERCENTAGE"
                'objField_RelatedEntities.Type = ModelFieldType.Float
                'objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "COMMENTS"
                objField_RelatedEntities.Type = ModelFieldType.String
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "NAME"
                objField_RelatedEntities.Type = ModelFieldType.String
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "COMMERCIAL_NAME"
                objField_RelatedEntities.Type = ModelFieldType.String
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "INCORPORATION_LEGAL_FORM"
                objField_RelatedEntities.Type = ModelFieldType.String
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "INCORPORATION_NUMBER"
                objField_RelatedEntities.Type = ModelFieldType.String
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "BUSINESS"
                objField_RelatedEntities.Type = ModelFieldType.String
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                'objField_RelatedEntities = New Ext.Net.ModelField
                'objField_RelatedEntities.Name = "ENTITY_STATUS"
                'objField_RelatedEntities.Type = ModelFieldType.String
                'objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                'objField_RelatedEntities = New Ext.Net.ModelField
                'objField_RelatedEntities.Name = "ENTITY_STATUS_DATE"
                'objField_RelatedEntities.Type = ModelFieldType.Date
                'objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "INCORPORATION_STATE"
                objField_RelatedEntities.Type = ModelFieldType.String
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "INCORPORATION_COUNTRY_CODE"
                objField_RelatedEntities.Type = ModelFieldType.String
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "INCORPORATION_DATE"
                objField_RelatedEntities.Type = ModelFieldType.Date
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "BUSINESS_CLOSED"
                objField_RelatedEntities.Type = ModelFieldType.Boolean
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "DATE_BUSINESS_CLOSED"
                objField_RelatedEntities.Type = ModelFieldType.String
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "TAX_NUMBER"
                objField_RelatedEntities.Type = ModelFieldType.String
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objField_RelatedEntities = New Ext.Net.ModelField
                objField_RelatedEntities.Name = "TAX_REG_NUMBER"
                objField_RelatedEntities.Type = ModelFieldType.String
                objModel_RelatedEntities.Fields.Add(objField_RelatedEntities)

                objStore_RelatedEntities.Model.Add(objModel_RelatedEntities)

                Dim objListColumn_RelatedEntities As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListColumn_RelatedEntities.Add(objcolumnNo)
                End Using

                Dim objColumn_RelatedEntities As Ext.Net.Column

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Entity Relation"
                objColumn_RelatedEntities.DataIndex = "ENTITY_ENTITY_RELATION"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Valid From"
                objColumn_RelatedEntities.DataIndex = "RELATION_DATE_RANGE_VALID_FROM"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Is Approx From Date"
                objColumn_RelatedEntities.DataIndex = "RELATION_DATE_RANGE_IS_APPROX_FROM_DATE"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Valid To"
                objColumn_RelatedEntities.DataIndex = "RELATION_DATE_RANGE_VALID_TO"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Is Approx To Date"
                objColumn_RelatedEntities.DataIndex = "RELATION_DATE_RANGE_IS_APPROX_TO_DATE"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Share Percentage"
                objColumn_RelatedEntities.DataIndex = "SHARE_PERCENTAGE"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Comments"
                objColumn_RelatedEntities.DataIndex = "COMMENTS"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Name"
                objColumn_RelatedEntities.DataIndex = "NAME"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Commercial Name"
                objColumn_RelatedEntities.DataIndex = "COMMERCIAL_NAME"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Incorporation Legal Form"
                objColumn_RelatedEntities.DataIndex = "INCORPORATION_LEGAL_FORM"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Incorporation Number"
                objColumn_RelatedEntities.DataIndex = "INCORPORATION_NUMBER"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Business"
                objColumn_RelatedEntities.DataIndex = "BUSINESS"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Entity Status"
                objColumn_RelatedEntities.DataIndex = "ENTITY_STATUS"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Entity Status Date"
                objColumn_RelatedEntities.DataIndex = "ENTITY_STATUS_DATE"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Incorporation State"
                objColumn_RelatedEntities.DataIndex = "INCORPORATION_STATE"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Incorporation Country Code"
                objColumn_RelatedEntities.DataIndex = "INCORPORATION_COUNTRY_CODE"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Incorporation Date"
                objColumn_RelatedEntities.DataIndex = "INCORPORATION_DATE"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Business Closed"
                objColumn_RelatedEntities.DataIndex = "BUSINESS_CLOSED"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Date Business Closed"
                objColumn_RelatedEntities.DataIndex = "DATE_BUSINESS_CLOSED"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Tax Number"
                objColumn_RelatedEntities.DataIndex = "TAX_NUMBER"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                objColumn_RelatedEntities = New Ext.Net.Column
                objColumn_RelatedEntities.Text = "Tax Reg Number"
                objColumn_RelatedEntities.DataIndex = "TAX_REG_NUMBER"
                objColumn_RelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumn_RelatedEntities.Flex = 1
                objListColumn_RelatedEntities.Add(objColumn_RelatedEntities)

                Dim objDt_RelatedEntities As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.ObjList_GoAML_Ref_Customer_RelatedEntities)
                '' End Related Entities
#End Region

                If objCustomer.FK_Customer_Type_ID = 1 Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, objCustomer.CIF)
                    'dedy added 26082020
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "GCN", "GCN" & strunik, objCustomer.GCN)
                    If objCustomer.isGCNPrimary = True Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "GCN Primary ?", "isGCNPrimary" & strunik, "YES")
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "GCN Primary ?", "isGCNPrimary" & strunik, "NO")
                    End If
                    'dedy end added 26082020
                    If objCustomer.isUpdateFromDataSource = True Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Update From Data Source ?", "isUpdateFromDataSource" & strunik, "YES")
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Update From Data Source ?", "isUpdateFromDataSource" & strunik, "NO")
                    End If

                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Title", "INDV_Title" & strunik, objCustomer.INDV_Title)
                    If objCustomer.INDV_Title IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Title", "INDV_Title" & strunik, objCustomer.INDV_Title)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Title", "INDV_Title" & strunik, "")
                    End If
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Last Name", "INDV_Last_Name" & strunik, objCustomer.INDV_Last_Name)
                    If objCustomer.INDV_Last_Name IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Last Name", "INDV_Last_Name" & strunik, objCustomer.INDV_Last_Name)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Last Name", "INDV_Last_Name" & strunik, "")
                    End If

                    'If objCustomer2.Full_Name_Frn IsNot Nothing Then
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Foreign Full Name", "INDV_Frn_Name" & strunik, objCustomer2.Full_Name_Frn)
                    'Else
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Last Name", "INDV_Frn_Name" & strunik, "")
                    'End If

                    Dim gender = db.goAML_Ref_Jenis_Kelamin.Where(Function(x) x.Kode = objCustomer.INDV_Gender).FirstOrDefault()
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gender", "INDV_Gender" & strunik, gender.Keterangan)
                    If objCustomer.INDV_Gender IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gender", "INDV_Gender" & strunik, gender.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gender", "INDV_Gender" & strunik, "")
                    End If

                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Previous Name", objStore_PreviousName, objListColumn_PreviousName, objDt_PreviousName)

                    If objCustomer.INDV_BirthDate IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Birth Date", "INDV_BirthDate" & strunik, objCustomer.INDV_BirthDate)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Birth Date", "INDV_BirthDate" & strunik, "")
                    End If
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Birth Place", "INDV_Birth_Place" & strunik, objCustomer.INDV_Birth_Place)
                    If objCustomer.INDV_Birth_Place IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Birth Place", "INDV_Birth_Place" & strunik, objCustomer.INDV_Birth_Place)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Birth Place", "INDV_Birth_Place" & strunik, "")
                    End If

                    'Dim countryOfBirth = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objCustomer2.Country_Of_Birth).FirstOrDefault()
                    'If countryOfBirth IsNot Nothing Then
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Country Of Birth", "INDV_CountryOfBirth" & strunik, countryOfBirth.Keterangan)
                    'Else
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Country Of Birth", "INDV_CountryOfBirth" & strunik, "")
                    'End If

                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Mother Name", "INDV_Mothers_Name" & strunik, objCustomer.INDV_Mothers_Name)
                    If objCustomer.INDV_Mothers_Name IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Mother Name", "INDV_Mothers_Name" & strunik, objCustomer.INDV_Mothers_Name)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Mother Name", "INDV_Mothers_Name" & strunik, objCustomer.INDV_Mothers_Name)
                    End If
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Alias", "INDV_Alias" & strunik, objCustomer.INDV_Alias)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "SSN", "INDV_SSN" & strunik, objCustomer.INDV_SSN)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Passport Number", "INDV_Passport_Number" & strunik, objCustomer.INDV_Passport_Number)
                    If objCustomer.INDV_Passport_Country IsNot Nothing And Not objCustomer.INDV_Passport_Country = "" Then
                        Dim passportcountry = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objCustomer.INDV_Passport_Country).FirstOrDefault()
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Passport Country", "INDV_Passport_Country" & strunik, passportcountry.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Passport Country", "INDV_Passport_Country" & strunik, "")
                    End If
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ID_Number", "INDV_ID_Number" & strunik, objCustomer.INDV_ID_Number)
                    If objCustomer.INDV_Nationality1 IsNot Nothing And Not objCustomer.INDV_Nationality1 = "" Then
                        Dim nasionality1 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objCustomer.INDV_Nationality1).FirstOrDefault()
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nasionality 1", "INDV_Nationality1" & strunik, nasionality1.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nasionality 1", "INDV_Nationality1" & strunik, "")
                    End If
                    If objCustomer.INDV_Nationality2 IsNot Nothing And Not objCustomer.INDV_Nationality2 = "" Then
                        Dim nasionality2 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objCustomer.INDV_Nationality2).FirstOrDefault()
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nasionality 2", "INDV_Nationality2" & strunik, nasionality2.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nasionality 2", "INDV_Nationality2" & strunik, "")
                    End If
                    If objCustomer.INDV_Nationality3 IsNot Nothing And Not objCustomer.INDV_Nationality3 = "" Then
                        Dim nasionality3 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objCustomer.INDV_Nationality3).FirstOrDefault()
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nasionality 3", "INDV_Nationality3" & strunik, nasionality3.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nasionality 3", "INDV_Nationality3" & strunik, "")
                    End If
                    Dim residence = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objCustomer.INDV_Residence).FirstOrDefault()
                    If residence IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Residence", "INDV_Residence" & strunik, residence.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Residence", "INDV_Residence" & strunik, "")
                    End If

                    'If objCustomer2.Residence_Since IsNot Nothing Then
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Residence Since", "INDV_Residence_Since" & strunik, objCustomer2.Residence_Since.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat))
                    'Else
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Residence Since", "INDV_Residence_Since" & strunik, "")
                    'End If

                    If objCustomer2.Is_Protected = True Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Is Protected ?", "isProtected" & strunik, "YES")
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Is Protected ?", "isProtected" & strunik, "NO")
                    End If

                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Email", objStore_Email, objListColumn_Email, objDt_Email)

                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email", "INDV_Email" & strunik, objCustomer.INDV_Email)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 2", "INDV_Email2" & strunik, objCustomer.INDV_Email2)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 3", "INDV_Email3" & strunik, objCustomer.INDV_Email3)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 4", "INDV_Email4" & strunik, objCustomer.INDV_Email4)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 5", "INDV_Email5" & strunik, objCustomer.INDV_Email5)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NPWP", "INDV_Tax_Number" & strunik, objCustomer.INDV_Tax_Number)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Source of Wealth", "INDV_Source_of_Wealth" & strunik, objCustomer.INDV_Source_of_Wealth)

                    If objCustomer.INDV_Deceased = True Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Deceased", "INDV_Deceased" & strunik, "YES")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Deceased Date", "INDV_Deceased_Date" & strunik, objCustomer.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat))
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Deceased", "INDV_Deceased" & strunik, "No")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Deceased Date", "INDV_Deceased_Date" & strunik, "")

                    End If
                    If objCustomer.INDV_Tax_Reg_Number = True Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "PEP ?", "INDV_Tax_Reg_Number" & strunik, "YES")
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "PEP ?", "INDV_Tax_Reg_Number" & strunik, "No")
                    End If

                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Occupation", "INDV_Occupation" & strunik, objCustomer.INDV_Occupation)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Employer Name", "INDV_Employer_Name" & strunik, objCustomer.INDV_Employer_Name)

                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, objCustomer.CIF)
                    'dedy added 26082020
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "GCN", "GCN" & strunik, objCustomer.GCN)
                    If objCustomer.isGCNPrimary = True Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "GCN Primary ?", "isGCNPrimary" & strunik, "YES")
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "GCN Primary ?", "isGCNPrimary" & strunik, "NO")
                    End If
                    'dedy end added 26082020
                    If objCustomer.isUpdateFromDataSource = True Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Update From Data Source ?", "isUpdateFromDataSource" & strunik, "YES")
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Update From Data Source ?", "isUpdateFromDataSource" & strunik, "NO")
                    End If
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Corp Name", "Corp_Name" & strunik, objCustomer.Corp_Name)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Commercial Name", "Corp_Commercial_Name" & strunik, objCustomer.Corp_Commercial_Name)
                    If objCustomer.Corp_Incorporation_Legal_Form IsNot Nothing And Not objCustomer.Corp_Incorporation_Legal_Form = "" Then
                        Dim Corp_Legal_Form = db.goAML_Ref_Bentuk_Badan_Usaha.Where(Function(x) x.Kode = objCustomer.Corp_Incorporation_Legal_Form).FirstOrDefault()
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Incorporation Legal Form", "Corp_Incorporation_Legal_Form" & strunik, Corp_Legal_Form.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Incorporation Legal Form", "Corp_Incorporation_Legal_Form" & strunik, "")
                    End If
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Incorporation Number", "Corp_Incorporation_Number" & strunik, objCustomer.Corp_Incorporation_Number)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Corp Bussiness", "Corp_Business" & strunik, objCustomer.Corp_Business)

                    'Dim qEntityStatus = "SELECT TOP 1 * FROM goAML_Ref_Entity_Status WHERE Kode = '" + objCustomer2.Entity_Status + "'"
                    'Dim refEntityStatus As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, qEntityStatus)
                    'If refEntityStatus IsNot Nothing Then
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Entity Status", "Corp_EntityStatus" & strunik, refEntityStatus("Keterangan").ToString())
                    'Else
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Entity Status", "Corp_EntityStatus" & strunik, "")
                    'End If


                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Email", objStore_Email, objListColumn_Email, objDt_Email)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Entity URL", objStore_URL, objListColumn_URL, objDt_URL)

                    'If objCustomer2.Entity_Status_Date IsNot Nothing Then
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Entity Status Date", "Corp_EntityStatusDate" & strunik, objCustomer2.Entity_Status_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat))
                    'Else
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Entity Status Date", "Corp_EntityStatusDate" & strunik, "")
                    'End If

                    'comment by Septian, goAML 5.0.1
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email", "Corp_Email" & strunik, objCustomer.Corp_Email)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "URL", "Corp_Url" & strunik, objCustomer.Corp_Url)
                    'end comment
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "State", "Corp_Incorporation_State" & strunik, objCustomer.Corp_Incorporation_State)
                    Dim countrycode = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objCustomer.Corp_Incorporation_Country_Code).FirstOrDefault()
                    If countrycode IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Country Code", "Corp_Incorporation_Country_Code" & strunik, countrycode.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Country Code", "Corp_Incorporation_Country_Code" & strunik, "")
                    End If
                    If objCustomer.Corp_Incorporation_Date IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Incorporation Date", "Corp_Incorporation_Date" & strunik, objCustomer.Corp_Incorporation_Date)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Incorporation Date", "Corp_Incorporation_Date" & strunik, "")
                    End If
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Closed ?", "Corp_Business_Closed" & strunik, objCustomer.Corp_Business_Closed)
                    If Not objCustomer.Corp_Business_Closed = 0 Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Date Bussiness Closed", "Corp_Date_Business_Closed" & strunik, objCustomer.Corp_Date_Business_Closed)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Date Bussiness Closed", "Corp_Date_Business_Closed" & strunik, "")
                    End If
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tax Number", "Corp_Tax_Number" & strunik, objCustomer.Corp_Tax_Number)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Comments", "Corp_Comments" & strunik, objCustomer.Corp_Comments)
                End If

                '''Phone
                Dim objStore As New Ext.Net.Store
                objStore.ID = strunik & "StoreGrid"
                objStore.ClientIDMode = Web.UI.ClientIDMode.Static

                Dim objModel As New Ext.Net.Model
                Dim objField As Ext.Net.ModelField

                objField = New Ext.Net.ModelField
                objField.Name = "PK_goAML_Ref_Phone"
                objField.Type = ModelFieldType.Auto
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "FK_Ref_Detail_Of"
                objField.Type = ModelFieldType.Int
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "FK_for_Table_ID"
                objField.Type = ModelFieldType.Int
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "Tph_Contact_Type"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "Tph_Communication_Type"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "tph_country_prefix"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "tph_number"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "tph_extension"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "comments"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objStore.Model.Add(objModel)

                Dim objListcolumn As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumn.Add(objcolumnNo)
                End Using

                Dim objColum As Ext.Net.Column

                objColum = New Ext.Net.Column
                objColum.Text = "Contact Type"
                objColum.DataIndex = "Tph_Contact_Type"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                objColum = New Ext.Net.Column
                objColum.Text = "Communication Type"
                objColum.DataIndex = "Tph_Communication_Type"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                objColum = New Ext.Net.Column
                objColum.Text = "Country Prefix"
                objColum.DataIndex = "tph_country_prefix"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                objColum = New Ext.Net.Column
                objColum.Text = "Number"
                objColum.DataIndex = "tph_number"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                objColum = New Ext.Net.Column
                objColum.Text = "Extension"
                objColum.DataIndex = "tph_extension"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                objColum = New Ext.Net.Column
                objColum.Text = "Comments"
                objColum.DataIndex = "comments"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                Dim objdt As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone)

                For Each item As DataRow In objdt.Rows
                    item("Tph_Contact_Type") = GetContactTypeByID(item("Tph_Contact_Type")).Keterangan
                Next

                For Each item As DataRow In objdt.Rows
                    item("Tph_Communication_Type") = GetCommunicationTypeByID(item("Tph_Communication_Type")).Keterangan
                Next

                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Phone", objStore, objListcolumn, objdt)
                NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Informasi Telepon", objStore, objListcolumn, objdt)


                '' Address
                Dim objStoreAddress As New Ext.Net.Store
                objStoreAddress.ID = strunik & "StoreGridAddress"
                objStoreAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelAddress As New Ext.Net.Model
                Dim objFieldAddress As Ext.Net.ModelField

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "PK_Customer_Address_ID"
                objFieldAddress.Type = ModelFieldType.Auto
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "FK_Ref_Detail_Of"
                objFieldAddress.Type = ModelFieldType.Auto
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "FK_To_Table_ID"
                objFieldAddress.Type = ModelFieldType.Auto
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Address_Type"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Address"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Town"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "City"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Zip"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Country_Code"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "State"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Comments"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objStoreAddress.Model.Add(objModelAddress)


                Dim objListcolumnAddress As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnAddress.Add(objcolumnNo)
                End Using


                Dim objColumAddress As Ext.Net.Column

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Address Type"
                objColumAddress.DataIndex = "Address_Type"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Address"
                objColumAddress.DataIndex = "Address"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Town"
                objColumAddress.DataIndex = "Town"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "City"
                objColumAddress.DataIndex = "City"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Zip"
                objColumAddress.DataIndex = "Zip"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Country Code"
                objColumAddress.DataIndex = "Country_Code"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "State"
                objColumAddress.DataIndex = "State"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Comment"
                objColumAddress.DataIndex = "Comments"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                Dim objdtAddress As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objlistgoaml_Ref_Address)

                For Each item As DataRow In objdtAddress.Rows
                    item("Address_Type") = GetAddressTypeByID(item("Address_Type")).Keterangan
                Next

                For Each item As DataRow In objdtAddress.Rows
                    item("Country_Code") = GetNamaNegaraByID(item("Country_Code")).Keterangan
                Next


                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Address", objStoreAddress, objListcolumnAddress, objdtAddress)
                NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Informasi Alamat", objStoreAddress, objListcolumnAddress, objdtAddress)

                'If objCustomer.FK_Customer_Type_ID = 1 Then
                '    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Employment History", objStore_EmploymentHistory, objListColumn_EmploymentHistory, objDt_EmploymentHistory)
                'End If

                If objCustomer.FK_Customer_Type_ID = 2 Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Entity Identification", objStore_EntityIdentification, objListColumn_EntityIdentification, objDt_EntityIdentification)
                End If


                '' Identification
                Dim objStoreIdentification As New Ext.Net.Store
                objStoreIdentification.ID = strunik & "StoreGridIdentification"
                objStoreIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelIdentification As New Ext.Net.Model
                Dim objFieldIdentification As Ext.Net.ModelField

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "PK_Person_Identification_ID"
                objFieldIdentification.Type = ModelFieldType.Auto
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "isReportingPerson"
                objFieldIdentification.Type = ModelFieldType.Auto
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "FK_Person_ID"
                objFieldIdentification.Type = ModelFieldType.Auto
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Type"
                objFieldIdentification.Type = ModelFieldType.String
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Number"
                objFieldIdentification.Type = ModelFieldType.String
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Issue_Date"
                objFieldIdentification.Type = ModelFieldType.Date
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Expiry_Date"
                objFieldIdentification.Type = ModelFieldType.Date
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Issued_By"
                objFieldIdentification.Type = ModelFieldType.String
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Issued_Country"
                objFieldIdentification.Type = ModelFieldType.String
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Identification_Comment"
                objFieldIdentification.Type = ModelFieldType.String
                objModelIdentification.Fields.Add(objFieldIdentification)

                objStoreIdentification.Model.Add(objModelIdentification)


                Dim objListcolumnIdentification As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnIdentification.Add(objcolumnNo)
                End Using


                Dim objColumIdentification As Ext.Net.Column
                Dim objColumIdentificationDate As Ext.Net.DateColumn


                objColumIdentification = New Ext.Net.Column
                objColumIdentification.Text = "Identification Type"
                objColumIdentification.DataIndex = "Type"
                objColumIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentification.Flex = 1
                objListcolumnIdentification.Add(objColumIdentification)

                objColumIdentification = New Ext.Net.Column
                objColumIdentification.Text = "Number"
                objColumIdentification.DataIndex = "Number"
                objColumIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentification.Flex = 1
                objListcolumnIdentification.Add(objColumIdentification)

                objColumIdentificationDate = New Ext.Net.DateColumn
                objColumIdentificationDate.Text = "Issue Date"
                objColumIdentificationDate.DataIndex = "Issue_Date"
                objColumIdentificationDate.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDate.Flex = 1
                objListcolumnIdentification.Add(objColumIdentificationDate)

                objColumIdentificationDate = New Ext.Net.DateColumn
                objColumIdentificationDate.Text = "Expiry Date"
                objColumIdentificationDate.DataIndex = "Expiry_Date"
                objColumIdentificationDate.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDate.Flex = 1
                objListcolumnIdentification.Add(objColumIdentificationDate)

                objColumIdentification = New Ext.Net.Column
                objColumIdentification.Text = "Issued By"
                objColumIdentification.DataIndex = "Issued_By"
                objColumIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentification.Flex = 1
                objListcolumnIdentification.Add(objColumIdentification)

                objColumIdentification = New Ext.Net.Column
                objColumIdentification.Text = "Issued Country"
                objColumIdentification.DataIndex = "Issued_Country"
                objColumIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentification.Flex = 1
                objListcolumnIdentification.Add(objColumIdentification)

                objColumIdentification = New Ext.Net.Column
                objColumIdentification.Text = "Comment"
                objColumIdentification.DataIndex = "Identification_Comment"
                objColumIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentification.Flex = 1
                objListcolumnIdentification.Add(objColumIdentification)

                If objCustomer.FK_Customer_Type_ID = 1 Then
                    Dim objdtIdentification As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Identification)

                    For Each item As DataRow In objdtIdentification.Rows
                        item("Type") = GetJenisDokumenByID(item("Type")).Keterangan
                    Next

                    For Each item As DataRow In objdtIdentification.Rows
                        item("Issued_Country") = GetNamaNegaraByID(item("Issued_Country")).Keterangan
                    Next

                    'For Each item As DataRow In objdtIdentification.Rows
                    '    item("Issue_Date") = item("Issue_Date").ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    'Next

                    'For Each item As DataRow In objdtIdentification.Rows
                    '    item("Expiry_Date") = item("Expiry_Date").ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    'Next



                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "identification", objStoreIdentification, objListcolumnIdentification, objdtIdentification)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Person PEP", objStore_PEP, objListColumn_PEP, objDt_PEP)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Network Device", objStore_NetworkDevice, objListColumn_NetworkDevice, objDt_NetworkDevice)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Social Media", objStore_SocialMedia, objListColumn_SocialMedia, objDt_SocialMedia)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Sanction", objStore_Sanction, objListColumn_Sanction, objDt_Sanction)
                    'ExtGridPanel_Internal(objPanel, "Sanction", objStore_Sanction, objListColumn_Sanction, objDt_Sanction)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Sanction", objStore_Sanction, objListColumn_Sanction, objDt_Sanction)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Related Person", objStore_RelatedPerson, objListColumn_RelatedPerson, objDt_RelatedPerson)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Additional Information", objStore_AdditionalInfo, objListColumn_AdditionalInfo, objDt_AdditionalInfo)
                End If



                '' Director
                Dim objStoreDirector As New Ext.Net.Store
                objStoreDirector.ID = strunik & "StoreGridDirectorq"
                objStoreDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelDirector As New Ext.Net.Model
                Dim objFieldDirector As Ext.Net.ModelField

                objFieldDirector = New Ext.Net.ModelField
                objFieldDirector.Name = "PK_goAML_Ref_Customer_Entity_Director_ID"
                objFieldDirector.Type = ModelFieldType.Auto
                objModelDirector.Fields.Add(objFieldDirector)

                objFieldDirector = New Ext.Net.ModelField
                objFieldDirector.Name = "Last_Name"
                objFieldDirector.Type = ModelFieldType.Auto
                objModelDirector.Fields.Add(objFieldDirector)

                objFieldDirector = New Ext.Net.ModelField
                objFieldDirector.Name = "Role"
                objFieldDirector.Type = ModelFieldType.Auto
                objModelDirector.Fields.Add(objFieldDirector)

                objFieldDirector = New Ext.Net.ModelField
                objFieldDirector.Name = "FK_Entity_ID"
                objFieldDirector.Type = ModelFieldType.String
                objModelDirector.Fields.Add(objFieldDirector)


                objStoreDirector.Model.Add(objModelDirector)


                Dim objListcolumnDirector As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnDirector.Add(objcolumnNo)
                End Using


                Dim objColumDirector As Ext.Net.Column

                objColumDirector = New Ext.Net.Column
                objColumDirector.Text = "Name"
                objColumDirector.DataIndex = "Last_Name"
                objColumDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirector.Flex = 1
                objListcolumnDirector.Add(objColumDirector)

                objColumDirector = New Ext.Net.Column
                objColumDirector.Text = "Role"
                objColumDirector.DataIndex = "Role"
                objColumDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirector.Flex = 1
                objListcolumnDirector.Add(objColumDirector)

                If objCustomer.FK_Customer_Type_ID = 2 Then
                    Dim objdtDirector As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Director)

                    For Each item As DataRow In objdtDirector.Rows
                        item("Role") = GetRoleDirectorByID(item("Role")).Keterangan
                    Next


                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Director", objStoreDirector, objListcolumnDirector, objdtDirector)
                End If

                '' Identification
                Dim objStoreIdentificationDirector As New Ext.Net.Store
                objStoreIdentification.ID = strunik & "StoreGridIdentificationDirector"
                objStoreIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelIdentificationDirector As New Ext.Net.Model
                Dim objFieldIdentificationDirector As Ext.Net.ModelField

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "PK_Person_Identification_ID"
                objFieldIdentificationDirector.Type = ModelFieldType.Auto
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "isReportingPerson"
                objFieldIdentificationDirector.Type = ModelFieldType.Auto
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "FK_Person_ID"
                objFieldIdentificationDirector.Type = ModelFieldType.Auto
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Type"
                objFieldIdentificationDirector.Type = ModelFieldType.String
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Number"
                objFieldIdentificationDirector.Type = ModelFieldType.String
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Issue_Date"
                objFieldIdentificationDirector.Type = ModelFieldType.Date
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Expiry_Date"
                objFieldIdentificationDirector.Type = ModelFieldType.Date
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Issued_By"
                objFieldIdentificationDirector.Type = ModelFieldType.String
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Issued_Country"
                objFieldIdentificationDirector.Type = ModelFieldType.String
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Identification_Comment"
                objFieldIdentificationDirector.Type = ModelFieldType.String
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objStoreIdentificationDirector.Model.Add(objModelIdentificationDirector)


                Dim objListcolumnIdentificationDirector As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnIdentificationDirector.Add(objcolumnNo)
                End Using


                Dim objColumIdentificationDirector As Ext.Net.Column
                Dim objColumIdentificationDirectorDate As Ext.Net.DateColumn

                objColumIdentificationDirector = New Ext.Net.Column
                objColumIdentificationDirector.Text = "Type"
                objColumIdentificationDirector.DataIndex = "Type"
                objColumIdentificationDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirector.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirector)

                objColumIdentificationDirector = New Ext.Net.Column
                objColumIdentificationDirector.Text = "Number Document"
                objColumIdentificationDirector.DataIndex = "Number"
                objColumIdentificationDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirector.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirector)

                objColumIdentificationDirectorDate = New Ext.Net.DateColumn
                objColumIdentificationDirectorDate.Text = "Date"
                objColumIdentificationDirectorDate.DataIndex = "Issue_Date"
                objColumIdentificationDirectorDate.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirectorDate.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirectorDate)

                objColumIdentificationDirectorDate = New Ext.Net.DateColumn
                objColumIdentificationDirectorDate.Text = "Tanggal Expire"
                objColumIdentificationDirectorDate.DataIndex = "Expiry_Date"
                objColumIdentificationDirectorDate.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirectorDate.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirectorDate)

                objColumIdentificationDirector = New Ext.Net.Column
                objColumIdentificationDirector.Text = "By"
                objColumIdentificationDirector.DataIndex = "Issued_By"
                objColumIdentificationDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirector.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirector)

                objColumIdentificationDirector = New Ext.Net.Column
                objColumIdentificationDirector.Text = "Country"
                objColumIdentificationDirector.DataIndex = "Issued_Country"
                objColumIdentificationDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirector.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirector)

                objColumIdentificationDirector = New Ext.Net.Column
                objColumIdentificationDirector.Text = "Catatan"
                objColumIdentificationDirector.DataIndex = "Identification_Comment"
                objColumIdentificationDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirector.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirector)

                If objCustomer.FK_Customer_Type_ID = 2 Then
                    Dim objdtIdentificationDirector As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Identification_Director)

                    For Each item As DataRow In objdtIdentificationDirector.Rows
                        item("Type") = GetJenisDokumenByID(item("Type")).Keterangan
                    Next

                    For Each item As DataRow In objdtIdentificationDirector.Rows
                        item("Issued_Country") = GetNamaNegaraByID(item("Issued_Country")).Keterangan
                    Next

                    'For Each item As DataRow In objdtIdentificationDirector.Rows
                    '    item("Issue_Date") = item("Issue_Date").ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    'Next

                    'For Each item As DataRow In objdtIdentificationDirector.Rows
                    '    item("Expiry_Date") = item("Expiry_Date").ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    'Next


                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Identification", objStoreIdentificationDirector, objListcolumnIdentificationDirector, objdtIdentificationDirector)

                End If


                '''Phone
                Dim objStoreEmployerPhone As New Ext.Net.Store
                objStoreEmployerPhone.ID = strunik & "StoreGridPhoneIndividuEmployer"
                objStoreEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static

                Dim objModelEmployerPhone As New Ext.Net.Model
                Dim objFieldEmployerPhone As Ext.Net.ModelField

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "PK_goAML_Ref_Phone"
                objFieldEmployerPhone.Type = ModelFieldType.Auto
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "FK_Ref_Detail_Of"
                objFieldEmployerPhone.Type = ModelFieldType.Int
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "FK_for_Table_ID"
                objFieldEmployerPhone.Type = ModelFieldType.Int
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "Tph_Contact_Type"
                objFieldEmployerPhone.Type = ModelFieldType.String
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "Tph_Communication_Type"
                objFieldEmployerPhone.Type = ModelFieldType.String
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "tph_country_prefix"
                objFieldEmployerPhone.Type = ModelFieldType.String
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "tph_number"
                objFieldEmployerPhone.Type = ModelFieldType.String
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "tph_extension"
                objFieldEmployerPhone.Type = ModelFieldType.String
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "comments"
                objFieldEmployerPhone.Type = ModelFieldType.String
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objStoreEmployerPhone.Model.Add(objModelEmployerPhone)

                Dim objListcolumnEmployerPhone As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnEmployerPhone.Add(objcolumnNo)
                End Using

                Dim objColumEmployerPhone As Ext.Net.Column

                objColumEmployerPhone = New Ext.Net.Column
                objColumEmployerPhone.Text = "Kategori Kontak"
                objColumEmployerPhone.DataIndex = "Tph_Contact_Type"
                objColumEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerPhone.Flex = 1
                objListcolumnEmployerPhone.Add(objColumEmployerPhone)

                objColumEmployerPhone = New Ext.Net.Column
                objColumEmployerPhone.Text = "Jenis Alat Komunikasi"
                objColumEmployerPhone.DataIndex = "Tph_Communication_Type"
                objColumEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerPhone.Flex = 1
                objListcolumnEmployerPhone.Add(objColumEmployerPhone)


                objColumEmployerPhone = New Ext.Net.Column
                objColumEmployerPhone.Text = "Kode Area Telepon"
                objColumEmployerPhone.DataIndex = "tph_country_prefix"
                objColumEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerPhone.Flex = 1
                objListcolumnEmployerPhone.Add(objColumEmployerPhone)

                objColumEmployerPhone = New Ext.Net.Column
                objColumEmployerPhone.Text = "Nomor Telepon"
                objColumEmployerPhone.DataIndex = "tph_number"
                objColumEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerPhone.Flex = 1
                objListcolumnEmployerPhone.Add(objColumEmployerPhone)

                objColumEmployerPhone = New Ext.Net.Column
                objColumEmployerPhone.Text = "Nomor Ekstensi"
                objColumEmployerPhone.DataIndex = "tph_extension"
                objColumEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerPhone.Flex = 1
                objListcolumnEmployerPhone.Add(objColumEmployerPhone)

                objColumEmployerPhone = New Ext.Net.Column
                objColumEmployerPhone.Text = "Catatan Telepon"
                objColumEmployerPhone.DataIndex = "comments"
                objColumEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerPhone.Flex = 1
                objListcolumnEmployerPhone.Add(objColumEmployerPhone)

                Dim objdtEmployerPhone As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Employer_Phone)

                For Each item As DataRow In objdtEmployerPhone.Rows
                    item("Tph_Contact_Type") = GetContactTypeByID(item("Tph_Contact_Type")).Keterangan
                Next

                'For Each item As DataRow In objdtEmployerPhone.Rows
                '    item("Tph_Communication_Type") = GetCommunicationTypeByID(item("Tph_Communication_Type")).Keterangan
                'Next

                '' Address
                Dim objStoreEmployerAddress As New Ext.Net.Store
                objStoreEmployerAddress.ID = strunik & "StoreGridAddressIndividuEmployer"
                objStoreEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelEmployerAddress As New Ext.Net.Model
                Dim objFieldEmployerAddress As Ext.Net.ModelField

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "PK_Customer_Address_ID"
                objFieldEmployerAddress.Type = ModelFieldType.Auto
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "FK_Ref_Detail_Of"
                objFieldEmployerAddress.Type = ModelFieldType.Auto
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "FK_To_Table_ID"
                objFieldEmployerAddress.Type = ModelFieldType.Auto
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "Address_Type"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "Address"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "Town"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "City"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "Zip"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "Country_Code"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "State"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "Comments"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objStoreEmployerAddress.Model.Add(objModelEmployerAddress)


                Dim objListcolumnEmployerAddress As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnEmployerAddress.Add(objcolumnNo)
                End Using


                Dim objColumEmployerAddress As Ext.Net.Column

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Kategori Alamat"
                objColumEmployerAddress.DataIndex = "Address_Type"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Alamat"
                objColumEmployerAddress.DataIndex = "Address"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Kabupaten"
                objColumEmployerAddress.DataIndex = "Town"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Kota"
                objColumEmployerAddress.DataIndex = "City"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Kode Pos"
                objColumEmployerAddress.DataIndex = "Zip"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Negara"
                objColumEmployerAddress.DataIndex = "Country_Code"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Ibu Kota"
                objColumEmployerAddress.DataIndex = "State"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Catatan Address"
                objColumEmployerAddress.DataIndex = "Comments"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                Dim objdtEmployerAddress As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objlistgoaml_Ref_Employer_Address)

                For Each item As DataRow In objdtEmployerAddress.Rows
                    item("Address_Type") = GetAddressTypeByID(item("Address_Type")).Keterangan
                Next

                For Each item As DataRow In objdtEmployerAddress.Rows
                    item("Country_Code") = GetNamaNegaraByID(item("Country_Code")).Keterangan
                Next

                '''INI Buat Employer
                If objCustomer.FK_Customer_Type_ID = 1 Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Employer Phone", objStoreEmployerPhone, objListcolumnEmployerPhone, objdtEmployerPhone)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Employer Address", objStoreEmployerAddress, objListcolumnEmployerAddress, objdtEmployerAddress)

                End If

                '''Phone
                Dim objStoreDirectorPhone As New Ext.Net.Store
                objStoreDirectorPhone.ID = strunik & "StoreGridDirectorPhone"
                objStoreDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static

                Dim objModelDirectorPhone As New Ext.Net.Model
                Dim objFieldDirectorPhone As Ext.Net.ModelField

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "PK_goAML_Ref_Phone"
                objFieldDirectorPhone.Type = ModelFieldType.Auto
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "FK_Ref_Detail_Of"
                objFieldDirectorPhone.Type = ModelFieldType.Int
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "FK_for_Table_ID"
                objFieldDirectorPhone.Type = ModelFieldType.Int
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "Tph_Contact_Type"
                objFieldDirectorPhone.Type = ModelFieldType.String
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "Tph_Communication_Type"
                objFieldDirectorPhone.Type = ModelFieldType.String
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "tph_country_prefix"
                objFieldDirectorPhone.Type = ModelFieldType.String
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "tph_number"
                objFieldDirectorPhone.Type = ModelFieldType.String
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "tph_extension"
                objFieldDirectorPhone.Type = ModelFieldType.String
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "comments"
                objFieldDirectorPhone.Type = ModelFieldType.String
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objStoreDirectorPhone.Model.Add(objModelDirectorPhone)

                Dim objListcolumnDirectorPhone As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnDirectorPhone.Add(objcolumnNo)
                End Using

                Dim objColumDirectorPhone As Ext.Net.Column

                objColumDirectorPhone = New Ext.Net.Column
                objColumDirectorPhone.Text = "Director Contact Type"
                objColumDirectorPhone.DataIndex = "Tph_Contact_Type"
                objColumDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorPhone.Flex = 1
                objListcolumnDirectorPhone.Add(objColumDirectorPhone)

                objColumDirectorPhone = New Ext.Net.Column
                objColumDirectorPhone.Text = "Director Communication Type"
                objColumDirectorPhone.DataIndex = "Tph_Communication_Type"
                objColumDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorPhone.Flex = 1
                objListcolumnDirectorPhone.Add(objColumDirectorPhone)

                objColumDirectorPhone = New Ext.Net.Column
                objColumDirectorPhone.Text = "Director Country Prefix"
                objColumDirectorPhone.DataIndex = "tph_country_prefix"
                objColumDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorPhone.Flex = 1
                objListcolumnDirectorPhone.Add(objColumDirectorPhone)

                objColumDirectorPhone = New Ext.Net.Column
                objColumDirectorPhone.Text = "Director Number"
                objColumDirectorPhone.DataIndex = "tph_number"
                objColumDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorPhone.Flex = 1
                objListcolumnDirectorPhone.Add(objColumDirectorPhone)

                objColumDirectorPhone = New Ext.Net.Column
                objColumDirectorPhone.Text = "Director Extension"
                objColumDirectorPhone.DataIndex = "tph_extension"
                objColumDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorPhone.Flex = 1
                objListcolumnDirectorPhone.Add(objColumDirectorPhone)

                objColumDirectorPhone = New Ext.Net.Column
                objColumDirectorPhone.Text = "Director Comments Phone"
                objColumDirectorPhone.DataIndex = "comments"
                objColumDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorPhone.Flex = 1
                objListcolumnDirectorPhone.Add(objColumDirectorPhone)
                'Update: Zikri_28092020
                Dim objdtDirectorPhone As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Director_Phone)
                If objCustomer.FK_Customer_Type_ID = 2 Then
                    For Each item As DataRow In objdtDirectorPhone.Rows
                        item("Tph_Contact_Type") = GetContactTypeByID(item("Tph_Contact_Type")).Keterangan
                    Next

                    For Each item As DataRow In objdtDirectorPhone.Rows
                        item("Tph_Communication_Type") = GetCommunicationTypeByID(item("Tph_Communication_Type")).Keterangan
                    Next
                End If
                'End Update

                '' Address
                Dim objStoreDirectorAddress As New Ext.Net.Store
                objStoreDirectorAddress.ID = strunik & "StoreGridDirectorAddress"
                objStoreDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelDirectorAddress As New Ext.Net.Model
                Dim objFieldDirectorAddress As Ext.Net.ModelField

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "PK_Customer_Address_ID"
                objFieldDirectorAddress.Type = ModelFieldType.Auto
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "FK_Ref_Detail_Of"
                objFieldDirectorAddress.Type = ModelFieldType.Auto
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "FK_To_Table_ID"
                objFieldDirectorAddress.Type = ModelFieldType.Auto
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "Address_Type"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "Address"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "Town"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "City"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "Zip"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "Country_Code"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "State"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "Comments"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objStoreDirectorAddress.Model.Add(objModelDirectorAddress)


                Dim objListcolumnDirectorAddress As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnDirectorAddress.Add(objcolumnNo)
                End Using


                Dim objColumDirectorAddress As Ext.Net.Column

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director Address Type"
                objColumDirectorAddress.DataIndex = "Address_Type"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director Address"
                objColumDirectorAddress.DataIndex = "Address"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director Town"
                objColumDirectorAddress.DataIndex = "Town"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director City"
                objColumDirectorAddress.DataIndex = "City"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director Zip"
                objColumDirectorAddress.DataIndex = "Zip"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director Country Code"
                objColumDirectorAddress.DataIndex = "Country_Code"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director State"
                objColumDirectorAddress.DataIndex = "State"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director Comment Address"
                objColumDirectorAddress.DataIndex = "Comments"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)
                'Update: Zikri_28-09-2020
                Dim objdtDirectorAddress As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Director_Address)
                If objCustomer.FK_Customer_Type_ID = 2 Then

                    For Each item As DataRow In objdtDirectorAddress.Rows
                        item("Address_Type") = GetAddressTypeByID(item("Address_Type")).Keterangan
                    Next

                    For Each item As DataRow In objdtDirectorAddress.Rows
                        item("Country_Code") = GetNamaNegaraByID(item("Country_Code")).Keterangan
                    Next

                End If
                'End Update

                '''Phone
                Dim objStoreDirectorEmpPhone As New Ext.Net.Store
                objStoreDirectorEmpPhone.ID = strunik & "StoreGridDirectorEmpPhone"
                objStoreDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static

                Dim objModelDirectorEmpPhone As New Ext.Net.Model
                Dim objFieldDirectorEmpPhone As Ext.Net.ModelField

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "PK_goAML_Ref_Phone"
                objFieldDirectorEmpPhone.Type = ModelFieldType.Auto
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "FK_Ref_Detail_Of"
                objFieldDirectorEmpPhone.Type = ModelFieldType.Int
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "FK_for_Table_ID"
                objFieldDirectorEmpPhone.Type = ModelFieldType.Int
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "Tph_Contact_Type"
                objFieldDirectorEmpPhone.Type = ModelFieldType.String
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "Tph_Communication_Type"
                objFieldDirectorEmpPhone.Type = ModelFieldType.String
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "tph_country_prefix"
                objFieldDirectorEmpPhone.Type = ModelFieldType.String
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "tph_number"
                objFieldDirectorEmpPhone.Type = ModelFieldType.String
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "tph_extension"
                objFieldDirectorEmpPhone.Type = ModelFieldType.String
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "comments"
                objFieldDirectorEmpPhone.Type = ModelFieldType.String
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objStoreDirectorEmpPhone.Model.Add(objModelDirectorEmpPhone)

                Dim objListcolumnDirectorEmpPhone As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnDirectorEmpPhone.Add(objcolumnNo)
                End Using

                Dim objColumDirectorEmpPhone As Ext.Net.Column

                objColumDirectorEmpPhone = New Ext.Net.Column
                objColumDirectorEmpPhone.Text = "Director Emp Contact Type"
                objColumDirectorEmpPhone.DataIndex = "Tph_Contact_Type"
                objColumDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpPhone.Flex = 1
                objListcolumnDirectorEmpPhone.Add(objColumDirectorEmpPhone)

                objColumDirectorEmpPhone = New Ext.Net.Column
                objColumDirectorEmpPhone.Text = "Director Emp Communication Type"
                objColumDirectorEmpPhone.DataIndex = "Tph_Communication_Type"
                objColumDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpPhone.Flex = 1
                objListcolumnDirectorEmpPhone.Add(objColumDirectorEmpPhone)

                objColumDirectorEmpPhone = New Ext.Net.Column
                objColumDirectorEmpPhone.Text = "Director Emp Country Prefix"
                objColumDirectorEmpPhone.DataIndex = "tph_country_prefix"
                objColumDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpPhone.Flex = 1
                objListcolumnDirectorEmpPhone.Add(objColumDirectorEmpPhone)

                objColumDirectorEmpPhone = New Ext.Net.Column
                objColumDirectorEmpPhone.Text = "Director Emp Number"
                objColumDirectorEmpPhone.DataIndex = "tph_number"
                objColumDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpPhone.Flex = 1
                objListcolumnDirectorEmpPhone.Add(objColumDirectorEmpPhone)

                objColumDirectorEmpPhone = New Ext.Net.Column
                objColumDirectorEmpPhone.Text = "Director Emp Extension"
                objColumDirectorEmpPhone.DataIndex = "tph_extension"
                objColumDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpPhone.Flex = 1
                objListcolumnDirectorEmpPhone.Add(objColumDirectorEmpPhone)

                objColumDirectorEmpPhone = New Ext.Net.Column
                objColumDirectorEmpPhone.Text = "Director Emp Comments Phone"
                objColumDirectorEmpPhone.DataIndex = "comments"
                objColumDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpPhone.Flex = 1
                objListcolumnDirectorEmpPhone.Add(objColumDirectorEmpPhone)
                'Update: Zikri_28092020
                Dim objdtDirectorEmpPhone As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Director_Employer_Phone)
                If objCustomer.FK_Customer_Type_ID = 2 Then

                    For Each item As DataRow In objdtDirectorEmpPhone.Rows
                        item("Tph_Contact_Type") = GetContactTypeByID(item("Tph_Contact_Type")).Keterangan
                    Next

                    For Each item As DataRow In objdtDirectorEmpPhone.Rows
                        item("Tph_Communication_Type") = GetCommunicationTypeByID(item("Tph_Communication_Type")).Keterangan
                    Next

                End If
                'End Udpate
                '' Address
                Dim objStoreDirectorEmpAddress As New Ext.Net.Store
                objStoreDirectorEmpAddress.ID = strunik & "StoreGridDirectorEmpAddress"
                objStoreDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelDirectorEmpAddress As New Ext.Net.Model
                Dim objFieldDirectorEmpAddress As Ext.Net.ModelField

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "PK_Customer_Address_ID"
                objFieldDirectorEmpAddress.Type = ModelFieldType.Auto
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "FK_Ref_Detail_Of"
                objFieldDirectorEmpAddress.Type = ModelFieldType.Auto
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "FK_To_Table_ID"
                objFieldDirectorEmpAddress.Type = ModelFieldType.Auto
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "Address_Type"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "Address"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "Town"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "City"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "Zip"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "Country_Code"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "State"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "Comments"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objStoreDirectorEmpAddress.Model.Add(objModelDirectorEmpAddress)


                Dim objListcolumnDirectorEmpAddress As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnDirectorEmpAddress.Add(objcolumnNo)
                End Using


                Dim objColumDirectorEmpAddress As Ext.Net.Column

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp Address Type"
                objColumDirectorEmpAddress.DataIndex = "Address_Type"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp Address"
                objColumDirectorEmpAddress.DataIndex = "Address"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp Town"
                objColumDirectorEmpAddress.DataIndex = "Town"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp City"
                objColumDirectorEmpAddress.DataIndex = "City"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp Zip"
                objColumDirectorEmpAddress.DataIndex = "Zip"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp Country Code"
                objColumDirectorEmpAddress.DataIndex = "Country_Code"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp State"
                objColumDirectorEmpAddress.DataIndex = "State"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp Comment Address"
                objColumDirectorEmpAddress.DataIndex = "Comments"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)
                'Update: Zikri_28092020
                Dim objdtDirectorEmpAddress As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objlistgoaml_Ref_Director_Employer_Address)
                If objCustomer.FK_Customer_Type_ID = 2 Then

                    For Each item As DataRow In objdtDirectorEmpAddress.Rows
                        item("Address_Type") = GetAddressTypeByID(item("Address_Type")).Keterangan
                    Next

                    For Each item As DataRow In objdtDirectorEmpAddress.Rows
                        item("Country_Code") = GetNamaNegaraByID(item("Country_Code")).Keterangan
                    Next

                End If
                'End Update

                If objCustomer.FK_Customer_Type_ID = 2 Then
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Director Address", objStoreDirectorAddress, objListcolumnDirectorAddress, objdtDirectorAddress)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Director Phone", objStoreDirectorPhone, objListcolumnDirectorPhone, objdtDirectorPhone)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Director Empolyer Address", objStoreDirectorEmpAddress, objListcolumnDirectorEmpAddress, objdtDirectorEmpAddress)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Director Employer Phone", objStoreDirectorEmpPhone, objListcolumnDirectorEmpPhone, objdtDirectorEmpPhone)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Alamat Director", objStoreDirectorAddress, objListcolumnDirectorAddress, objdtDirectorAddress)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Telepon Director", objStoreDirectorPhone, objListcolumnDirectorPhone, objdtDirectorPhone)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Alamat Tempat Bekerja Director", objStoreDirectorEmpAddress, objListcolumnDirectorEmpAddress, objdtDirectorEmpAddress)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Telepon Tempat Bekerja Director", objStoreDirectorEmpPhone, objListcolumnDirectorEmpPhone, objdtDirectorEmpPhone)

                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Employment History", objStore_NetworkDevice, objListColumn_NetworkDevice, objDt_NetworkDevice)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Sanction", objStore_Sanction, objListColumn_Sanction, objDt_Sanction)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Related Person", objStore_RelatedPerson, objListColumn_RelatedPerson, objDt_RelatedPerson)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Related Entities", objStore_RelatedEntities, objListColumn_RelatedEntities, objDt_RelatedEntities)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Additonal Information", objStore_AdditionalInfo, objListColumn_AdditionalInfo, objDt_AdditionalInfo)
                End If

            End If
        End Using

    End Sub

    'Sub ExtGridPanel(pn As FormPanel, String strLabel, Store objStore, List<ColumnBase> objColumnModelBase, Object objdata) As GridPanel
    '        Dim gridPanel = New GridPanel()
    '    gridPanel gridPanel() = New GridPanel();
    '        PagingToolbar item = New PagingToolbar();
    '        FilterHeader FilterHeader = New FilterHeader();
    '        FilterHeader.Remote = False;
    '        gridPanel.Title = strLabel;
    '        gridPanel.BottomBar.Add(item);
    '        gridPanel.Plugins.Add(FilterHeader);
    '        gridPanel.Store.Add(objStore);
    '        objStore.PageSize = SystemParameterBLL.GetPageSize();
    '        foreach(ColumnBase item2 In objColumnModelBase)
    '        {
    '            gridPanel.ColumnModel.Columns.Add(item2);
    '        }

    '        pn.Add(gridPanel);
    '        objStore.DataSource = RuntimeHelpers.GetObjectValue(objdata);
    '        objStore.DataBind();
    '        Return gridPanel();
    'End Sub

    Shared Function ExtGridPanel_Internal(pn As FormPanel, strLabel As String, objStore As Store, objColumnModelBase As List(Of ColumnBase), objdata As Object) As GridPanel
        Dim gridPanel = New GridPanel()
        Dim item = New PagingToolbar()
        Dim filterHeader = New FilterHeader()
        filterHeader.Remote = False
        gridPanel.Title = strLabel
        gridPanel.BottomBar.Add(item)
        gridPanel.Plugins.Add(filterHeader)
        gridPanel.Store.Add(objStore)
        objStore.PageSize = SystemParameterBLL.GetPageSize()

        For Each item2 As ColumnBase In objColumnModelBase
            gridPanel.ColumnModel.Columns.Add(item2)
        Next

        pn.Add(gridPanel)
        objStore.DataSource = RuntimeHelpers.GetObjectValue(objdata)
        objStore.DataBind()

        Return gridPanel
    End Function

    Shared Sub SettingColor(objPanelOld As FormPanel, objPanelNew As FormPanel, objdata As String, objdatabefore As String, unikkeyold As String, unikkeynew As String)

        If objdata.Length > 0 And objdatabefore.Length > 0 Then
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("PK_Customer_ID", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("CIF", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("AccountNo", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("Opened", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("Closed", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("Gender", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("balance", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("date_balance", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("status_code", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("beneficiary", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("beneficiary_comment", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("comments", objPanelOld, objPanelNew, unikkeyold, unikkeynew)

        End If
    End Sub

    Shared Function Accept(ID As String) As Boolean
        'done: accept

        Dim obj_customer_grid = New goAML_Grid_Customer
        Dim objCustomer As GoAMLDAL.goAML_Ref_Customer
        Dim objCustomer2 As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2
        Dim objApproval As GoAMLDAL.ModuleApproval

        Using objdb As New GoAMLDAL.GoAMLEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    objApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As GoAMLDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If
                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            'Dim objModuleDataBefore As goAML_CustomerDataBLL = Nothing

                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            'If Not String.IsNullOrEmpty(objApproval.ModuleFieldBefore) Then
                            '    objModuleDataBefore = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(goAML_CustomerDataBLL))
                            'End If

                            objCustomer = objModuledata.objgoAML_Ref_Customer
                            Dim objDirector As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director) = objModuledata.objListgoAML_Ref_Director
                            Dim objPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objModuledata.objListgoAML_Ref_Phone
                            Dim objAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objModuledata.objlistgoaml_Ref_Address
                            Dim objEmployerPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objModuledata.objListgoAML_Ref_Employer_Phone
                            Dim objEmployerAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objModuledata.objlistgoaml_Ref_Employer_Address
                            Dim objIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objModuledata.objListgoAML_Ref_Identification
                            Dim objDirectorPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objModuledata.objListgoAML_Ref_Director_Phone
                            Dim objDirectorAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objModuledata.objListgoAML_Ref_Director_Address
                            Dim objDirectorEmployerPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objModuledata.objListgoAML_Ref_Director_Employer_Phone
                            Dim objDirectorEmployerAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objModuledata.objlistgoaml_Ref_Director_Employer_Address
                            Dim objIdentificationDirector As List(Of GoAMLDAL.goAML_Person_Identification) = objModuledata.objListgoAML_Ref_Identification_Director

                            'goAML 5.0.1, Add By Septian
                            objCustomer2 = objModuledata.objgoAML_Ref_Customer2
                            Dim objPreviousName As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name) = objModuledata.ObjList_GoAML_Ref_Customer_PreviousName
                            Dim objNetworkDevice As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device) = objModuledata.ObjList_GoAML_Ref_Customer_NetworkDevice
                            Dim objEmail As List(Of DataModel.goAML_Ref_Customer_Email) = objModuledata.ObjList_GoAML_Ref_Customer_Email
                            Dim objSocialMedia As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media) = objModuledata.ObjList_GoAML_Ref_Customer_SocialMedia
                            Dim objEmploymentHistory As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History) = objModuledata.ObjList_GoAML_Ref_Customer_EmploymentHistory
                            Dim objEntityIdentification As List(Of DataModel.goAML_Ref_Customer_Entity_Identification) = objModuledata.ObjList_GoAML_Ref_Customer_EntityIdentification
                            Dim objPEP As List(Of DataModel.goAML_Ref_Customer_PEP) = objModuledata.ObjList_GoAML_Ref_Customer_PEP
                            Dim objSanction As List(Of DataModel.goAML_Ref_Customer_Sanction) = objModuledata.ObjList_GoAML_Ref_Customer_Sanction
                            Dim objRelatedPerson As List(Of DataModel.goAML_Ref_Customer_Related_Person) = objModuledata.ObjList_GoAML_Ref_Customer_RelatedPerson
                            Dim objAdditionalInformation As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information) = objModuledata.ObjList_GoAML_Ref_Customer_AdditionalInformation
                            Dim objURL As List(Of DataModel.goAML_Ref_Customer_URL) = objModuledata.ObjList_GoAML_Ref_Customer_URL
                            Dim objRelatedEntities As List(Of DataModel.goAML_Ref_Customer_Related_Entities) = objModuledata.ObjList_GoAML_Ref_Customer_RelatedEntities
                            Dim objDirector2 As List(Of DataModel.goAML_Ref_Entity_Director) = objModuledata.ObjListgoAML_Ref_Director2

                            'Dim objDirectorBefore = If(objModuleDataBefore Is Nothing, Nothing, objModuleDataBefore.ObjListgoAML_Ref_Director2)
                            'end add

                            With obj_customer_grid
                                .ObjList_GoAML_PreviousName = objPreviousName
                                .ObjList_GoAML_Email = objEmail
                                .ObjList_GoAML_EmploymentHistory = objEmploymentHistory
                                .ObjList_GoAML_PEP = objPEP
                                .ObjList_GoAML_NetworkDevice = objNetworkDevice
                                .ObjList_GoAML_SocialMedia = objSocialMedia
                                .ObjList_GoAML_Sanction = objSanction
                                .ObjList_GoAML_RelatedPerson = objRelatedPerson
                                .ObjList_GoAML_AdditionalInformation = objAdditionalInformation
                                .ObjList_GoAML_URL = objURL
                                .ObjList_GoAML_EntityIdentification = objEntityIdentification
                                .ObjList_GoAML_RelatedEntities = objRelatedEntities
                                .objList_GoAML_Ref_Director = objDirector2
                            End With
                            ' end add

                            'Update: Zikri_14092020 Add Validation GCN
                            If objCustomer.GCN IsNot Nothing Then
                                If objCustomer.isGCNPrimary = True Then
                                    Dim checkGCN As goAML_Ref_Customer = GoAMLBLL.goAML_CustomerBLL.GetCheckGCN(objCustomer.GCN)
                                    If checkGCN IsNot Nothing Then
                                        If checkGCN.CIF <> objCustomer.CIF Then
                                            checkGCN.isGCNPrimary = 0
                                            checkGCN.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            checkGCN.ApprovedDate = Now
                                            checkGCN.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            checkGCN.LastUpdateDate = Now
                                            objdb.Entry(checkGCN).State = Entity.EntityState.Modified
                                            objdb.SaveChanges()
                                        End If
                                    End If
                                End If
                            End If
                            'End Update
                            objCustomer.Active = 1
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                objCustomer.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                objCustomer.Alternateby = ""
                            End If
                            objCustomer.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objCustomer.ApprovedDate = Now
                            objdb.Entry(objCustomer).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            ' 2023100611
                            If objCustomer.FK_Customer_Type_ID = 2 Then
                                For Each item As GoAMLDAL.goAML_Ref_Customer_Entity_Director In objDirector
                                    item.FK_Entity_ID = objCustomer.CIF
                                    Dim addressdirector = objDirectorAddress.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                                    Dim phonedirector = objDirectorPhone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                                    Dim addressempdirector = objDirectorEmployerAddress.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                                    Dim phoneempdirector = objDirectorEmployerPhone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                                    Dim identificationdirector = objIdentificationDirector.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Person_Type = 4).ToList()
                                    ' 2023-10-06, Nael: Menambahkan email, sanction, dan pep pada entity director
                                    Dim entityemail = objEmail.Where(Function(x) x.FK_FOR_TABLE_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_REF_DETAIL_OF = 6).ToList()
                                    Dim entitySanction = objSanction.Where(Function(x) x.FK_FOR_TABLE_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_REF_DETAIL_OF = 6).ToList()
                                    Dim entityPEP = objPEP.Where(Function(x) x.FK_FOR_TABLE_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_REF_DETAIL_OF = 6).ToList()

                                    'objdb.Entry(item).State = Entity.EntityState.Added
                                    objdb.goAML_Ref_Customer_Entity_Director.Add(item)
                                    objdb.SaveChanges()

                                    For Each itemx As GoAMLDAL.goAML_Ref_Address In addressdirector
                                        itemx.Active = 1
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            itemx.Alternateby = ""
                                        End If
                                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.ApprovedDate = Now
                                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.CreatedDate = Now
                                        itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                        objdb.Entry(itemx).State = Entity.EntityState.Added
                                    Next
                                    objdb.SaveChanges()
                                    For Each itemx As GoAMLDAL.goAML_Ref_Phone In phonedirector
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            itemx.Alternateby = ""
                                        End If
                                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.ApprovedDate = Now
                                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.CreatedDate = Now
                                        itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                        objdb.Entry(itemx).State = Entity.EntityState.Added
                                    Next
                                    objdb.SaveChanges()
                                    For Each itemx As GoAMLDAL.goAML_Ref_Address In addressempdirector
                                        itemx.Active = 1
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            itemx.Alternateby = ""
                                        End If
                                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.ApprovedDate = Now
                                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.CreatedDate = Now
                                        itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                        objdb.Entry(itemx).State = Entity.EntityState.Added
                                    Next

                                    For Each itemx As GoAMLDAL.goAML_Person_Identification In identificationdirector
                                        itemx.Active = 1
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            itemx.Alternateby = ""
                                        End If
                                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.ApprovedDate = Now
                                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.CreatedDate = Now
                                        itemx.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                        objdb.Entry(itemx).State = Entity.EntityState.Added
                                    Next
                                    objdb.SaveChanges()

                                    For Each itemx As GoAMLDAL.goAML_Ref_Phone In phoneempdirector
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            itemx.Alternateby = ""
                                        End If
                                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.ApprovedDate = Now
                                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.CreatedDate = Now
                                        itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                        objdb.Entry(itemx).State = Entity.EntityState.Added
                                    Next
                                    objdb.SaveChanges()

                                    ' 2023-10-6, Nael =========================================================
                                    For Each itemx As DataModel.goAML_Ref_Customer_Email In entityemail
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            itemx.Alternateby = ""
                                        End If
                                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.ApprovedDate = Now
                                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.CreatedDate = Now
                                        itemx.FK_FOR_TABLE_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                        objdb.Entry(itemx).State = Entity.EntityState.Added
                                    Next
                                    objdb.SaveChanges()

                                    For Each itemx As DataModel.goAML_Ref_Customer_Sanction In entitySanction
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            itemx.Alternateby = ""
                                        End If
                                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.ApprovedDate = Now
                                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.CreatedDate = Now
                                        itemx.FK_FOR_TABLE_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                        objdb.Entry(itemx).State = Entity.EntityState.Added
                                    Next
                                    objdb.SaveChanges()

                                    For Each itemx As DataModel.goAML_Ref_Customer_PEP In entityPEP
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            itemx.Alternateby = ""
                                        End If
                                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.ApprovedDate = Now
                                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.CreatedDate = Now
                                        itemx.FK_FOR_TABLE_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                        objdb.Entry(itemx).State = Entity.EntityState.Added
                                    Next
                                    objdb.SaveChanges()
                                    ' =========================================================================
                                Next
                            End If

                            For Each item As GoAMLDAL.goAML_Ref_Phone In objPhone
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    item.Alternateby = ""
                                End If
                                item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.ApprovedDate = Now
                                item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.CreatedDate = Now
                                item.FK_for_Table_ID = objCustomer.PK_Customer_ID
                                objdb.Entry(item).State = Entity.EntityState.Added
                            Next
                            objdb.SaveChanges()

                            For Each item As GoAMLDAL.goAML_Ref_Address In objAddress
                                item.Active = 1
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    item.Alternateby = ""
                                End If
                                item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.ApprovedDate = Now
                                item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.CreatedDate = Now
                                item.FK_To_Table_ID = objCustomer.PK_Customer_ID
                                objdb.Entry(item).State = Entity.EntityState.Added
                            Next
                            objdb.SaveChanges()

                            If objCustomer.FK_Customer_Type_ID = 1 Then
                                For Each item As GoAMLDAL.goAML_Person_Identification In objIdentification
                                    item.Active = 1
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.CreatedDate = Now
                                    item.FK_Person_ID = objCustomer.PK_Customer_ID
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                Next
                                objdb.SaveChanges()
                                For Each item As GoAMLDAL.goAML_Ref_Address In objEmployerAddress
                                    item.Active = 1
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.CreatedDate = Now
                                    item.FK_To_Table_ID = objCustomer.PK_Customer_ID
                                    item.FK_Ref_Detail_Of = 5
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                Next
                                objdb.SaveChanges()
                                For Each item As GoAMLDAL.goAML_Ref_Phone In objEmployerPhone
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.CreatedDate = Now
                                    item.FK_for_Table_ID = objCustomer.PK_Customer_ID
                                    item.FK_Ref_Detail_Of = 5
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                Next
                                objdb.SaveChanges()
                            End If
                            ' 2023-09-29, Nael: Update
                            ' ======================= START: AUDIT TRAIL ================================
                            Using objDba As New GoAMLEntities
                                Dim header As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                                'AuditTrailDetail, NaelEdit
                                ' 2023-09-29, Nael: Menambahkan Audit Trail untuk add
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, objCustomer)
                                objDba.SaveChanges()

                                If objPhone.Count > 0 Then
                                    For Each userPhone As GoAMLDAL.goAML_Ref_Phone In objPhone
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, userPhone)
                                        objDba.SaveChanges() ' 2023-09-29, Nael: SaveChange
                                    Next
                                End If

                                If objAddress.Count > 0 Then
                                    For Each itemheader As GoAMLDAL.goAML_Ref_Address In objAddress
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        objDba.SaveChanges() ' 2023-09-29, Nael: SaveChange
                                    Next
                                End If

                                If objCustomer.FK_Customer_Type_ID = 1 Then

                                    If objIdentification.Count > 0 Then
                                        For Each itemheader As GoAMLDAL.goAML_Person_Identification In objIdentification
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                            objDba.SaveChanges() ' 2023-09-29, Nael: SaveChange
                                        Next
                                    End If

                                    If objEmployerAddress.Count > 0 Then
                                        For Each itemheader As GoAMLDAL.goAML_Ref_Address In objEmployerAddress
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                            objDba.SaveChanges() ' 2023-09-29, Nael: SaveChange
                                        Next
                                    End If

                                    If objEmployerPhone.Count > 0 Then
                                        For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objEmployerPhone
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                            objDba.SaveChanges() ' 2023-09-29, Nael: SaveChange
                                        Next
                                    End If
                                End If

                                If objCustomer.FK_Customer_Type_ID = 2 Then
                                    If objDirectorEmployerAddress.Count > 0 Then
                                        For Each itemheader As GoAMLDAL.goAML_Ref_Address In objDirectorEmployerAddress
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                            objDba.SaveChanges() ' 2023-09-29, Nael: SaveChange
                                        Next
                                    End If

                                    If objDirectorEmployerPhone.Count > 0 Then
                                        For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objDirectorEmployerPhone
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                            objDba.SaveChanges() ' 2023-09-29, Nael: SaveChange
                                        Next
                                    End If

                                    If objDirectorAddress.Count > 0 Then
                                        For Each itemheader As GoAMLDAL.goAML_Ref_Address In objDirectorAddress
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                            objDba.SaveChanges() ' 2023-09-29, Nael: SaveChange
                                        Next
                                    End If

                                    If objDirectorPhone.Count > 0 Then
                                        For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objDirectorPhone
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                            objDba.SaveChanges() ' 2023-09-29, Nael: SaveChange
                                        Next
                                    End If

                                    If objIdentificationDirector.Count > 0 Then
                                        For Each itemheader As GoAMLDAL.goAML_Person_Identification In objIdentificationDirector
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                            objDba.SaveChanges() ' 2023-09-29, Nael: SaveChange
                                        Next
                                    End If

                                End If

                                objDba.SaveChanges()
                            End Using
                        ' ======================= END: AUDIT TRAIL ================================

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))
                            Dim objModuledataOld As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(goAML_CustomerDataBLL))

                            objCustomer = objModuledata.objgoAML_Ref_Customer

                            'goAML 5.0.1, Add By Septian
                            objCustomer2 = objModuledata.objgoAML_Ref_Customer2
                            Dim objPreviousName As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name) = objModuledata.ObjList_GoAML_Ref_Customer_PreviousName
                            Dim objNetworkDevice As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device) = objModuledata.ObjList_GoAML_Ref_Customer_NetworkDevice
                            Dim objEmail As List(Of DataModel.goAML_Ref_Customer_Email) = objModuledata.ObjList_GoAML_Ref_Customer_Email
                            Dim objSocialMedia As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media) = objModuledata.ObjList_GoAML_Ref_Customer_SocialMedia
                            Dim objEmploymentHistory As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History) = objModuledata.ObjList_GoAML_Ref_Customer_EmploymentHistory
                            Dim objEntityIdentification As List(Of DataModel.goAML_Ref_Customer_Entity_Identification) = objModuledata.ObjList_GoAML_Ref_Customer_EntityIdentification
                            Dim objPEP As List(Of DataModel.goAML_Ref_Customer_PEP) = objModuledata.ObjList_GoAML_Ref_Customer_PEP
                            Dim objSanction As List(Of DataModel.goAML_Ref_Customer_Sanction) = objModuledata.ObjList_GoAML_Ref_Customer_Sanction
                            Dim objRelatedPerson As List(Of DataModel.goAML_Ref_Customer_Related_Person) = objModuledata.ObjList_GoAML_Ref_Customer_RelatedPerson
                            Dim objAdditionalInformation As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information) = objModuledata.ObjList_GoAML_Ref_Customer_AdditionalInformation
                            Dim objURL As List(Of DataModel.goAML_Ref_Customer_URL) = objModuledata.ObjList_GoAML_Ref_Customer_URL
                            Dim objRelatedEntities As List(Of DataModel.goAML_Ref_Customer_Related_Entities) = objModuledata.ObjList_GoAML_Ref_Customer_RelatedEntities
                            Dim objDirector As List(Of DataModel.goAML_Ref_Entity_Director) = objModuledata.ObjListgoAML_Ref_Director2

                            With obj_customer_grid
                                .ObjList_GoAML_PreviousName = objPreviousName
                                .ObjList_GoAML_Email = objEmail
                                .ObjList_GoAML_EmploymentHistory = objEmploymentHistory
                                .ObjList_GoAML_PEP = objPEP
                                .ObjList_GoAML_NetworkDevice = objNetworkDevice
                                .ObjList_GoAML_SocialMedia = objSocialMedia
                                .ObjList_GoAML_Sanction = objSanction
                                .ObjList_GoAML_RelatedPerson = objRelatedPerson
                                .ObjList_GoAML_AdditionalInformation = objAdditionalInformation
                                .ObjList_GoAML_URL = objURL
                                .ObjList_GoAML_EntityIdentification = objEntityIdentification
                                .ObjList_GoAML_RelatedEntities = objRelatedEntities
                                .objList_GoAML_Ref_Director = objDirector
                            End With
                            'end add
                            'Update: Zikri_14092020 Add Validation GCN
                            If objCustomer.GCN IsNot Nothing Then
                                If objCustomer.isGCNPrimary = True Then
                                    Dim checkGCN As goAML_Ref_Customer = GoAMLBLL.goAML_CustomerBLL.GetCheckGCN(objCustomer.GCN)
                                    If checkGCN IsNot Nothing Then
                                        If checkGCN.CIF <> objCustomer.CIF Then
                                            checkGCN.isGCNPrimary = 0
                                            checkGCN.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            checkGCN.ApprovedDate = Now
                                            checkGCN.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            checkGCN.LastUpdateDate = Now
                                            objdb.Entry(checkGCN).State = Entity.EntityState.Modified
                                            objdb.SaveChanges()
                                        End If
                                    End If
                                End If
                            End If
                            'End Update

                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                objCustomer.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                objCustomer.Alternateby = ""
                            End If
                            objCustomer.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objCustomer.LastUpdateDate = Now
                            objCustomer.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objCustomer.ApprovedDate = Now
                            objdb.goAML_Ref_Customer.Attach(objCustomer)
                            objdb.Entry(objCustomer).State = Entity.EntityState.Modified

                            If objCustomer.FK_Customer_Type_ID = 2 Then
                                For Each itemx As GoAMLDAL.goAML_Ref_Customer_Entity_Director In (From x In objdb.goAML_Ref_Customer_Entity_Director Where x.FK_Entity_ID = objCustomer.CIF Select x).ToList
                                    Dim objcek As GoAMLDAL.goAML_Ref_Customer_Entity_Director = objModuledata.objListgoAML_Ref_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = itemx.PK_goAML_Ref_Customer_Entity_Director_ID)
                                    If objcek Is Nothing Then
                                        objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                    End If
                                Next
                                objdb.SaveChanges()

                                For Each item As GoAMLDAL.goAML_Ref_Customer_Entity_Director In objModuledata.objListgoAML_Ref_Director
                                    Dim obcek As GoAMLDAL.goAML_Ref_Customer_Entity_Director = (From x In objdb.goAML_Ref_Customer_Entity_Director Where x.PK_goAML_Ref_Customer_Entity_Director_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID Select x).FirstOrDefault
                                    If obcek Is Nothing Then
                                        item.FK_Entity_ID = objCustomer.CIF
                                        objdb.Entry(item).State = Entity.EntityState.Added
                                    Else
                                        objdb.Entry(obcek).CurrentValues.SetValues(item)
                                        objdb.Entry(obcek).State = Entity.EntityState.Modified
                                    End If
                                    objdb.SaveChanges()

                                    For Each itemx As GoAMLDAL.goAML_Ref_Phone In (From x In objdb.goAML_Ref_Phone Where x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 6 Select x).ToList
                                        Dim objcek As GoAMLDAL.goAML_Ref_Phone = objModuledata.objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                        If objcek Is Nothing Then
                                            objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                        End If
                                    Next

                                    For Each itemx As GoAMLDAL.goAML_Ref_Phone In (From x In objdb.goAML_Ref_Phone Where x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 7 Select x).ToList
                                        Dim objcek As GoAMLDAL.goAML_Ref_Phone = objModuledata.objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                        If objcek Is Nothing Then
                                            objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                        End If
                                    Next

                                    For Each itemx As GoAMLDAL.goAML_Ref_Address In (From x In objdb.goAML_Ref_Address Where x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 6 Select x).ToList
                                        Dim objcek As GoAMLDAL.goAML_Ref_Address = objModuledata.objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                        If objcek Is Nothing Then
                                            objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                        End If
                                    Next

                                    For Each itemx As GoAMLDAL.goAML_Ref_Address In (From x In objdb.goAML_Ref_Address Where x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 7 Select x).ToList
                                        Dim objcek As GoAMLDAL.goAML_Ref_Address = objModuledata.objlistgoaml_Ref_Director_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                        If objcek Is Nothing Then
                                            objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                        End If
                                    Next

                                    For Each itemx As GoAMLDAL.goAML_Person_Identification In (From x In objdb.goAML_Person_Identification Where x.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Person_Type = 4 Select x).ToList
                                        Dim objcek As GoAMLDAL.goAML_Person_Identification = objModuledata.objListgoAML_Ref_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID)
                                        If objcek Is Nothing Then
                                            objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                        End If
                                    Next

                                    For Each itemx As GoAMLDAL.goAML_Person_Identification In objModuledata.objListgoAML_Ref_Identification_Director
                                        Dim objcek As GoAMLDAL.goAML_Person_Identification = (From x In objdb.goAML_Person_Identification Where x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID Select x).FirstOrDefault
                                        If objcek Is Nothing Then
                                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                            Else
                                                itemx.Alternateby = ""
                                            End If
                                            itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.ApprovedDate = Now
                                            itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.CreatedDate = Now
                                            itemx.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                            objdb.Entry(itemx).State = Entity.EntityState.Added
                                        Else
                                            Dim objcekdirector = objdb.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_Person_ID).ToList()
                                            If objcekdirector Is Nothing Then
                                                objdb.Entry(objcek).State = Entity.EntityState.Deleted
                                            Else
                                                objdb.Entry(objcek).CurrentValues.SetValues(itemx)
                                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                    objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                                Else
                                                    objcek.Alternateby = ""
                                                End If
                                                objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.LastUpdateDate = Now
                                                objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.ApprovedDate = Now
                                                objdb.Entry(objcek).State = Entity.EntityState.Modified
                                            End If
                                        End If
                                    Next

                                    For Each itemx As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Director_Employer_Phone
                                        Dim objcek As GoAMLDAL.goAML_Ref_Phone = (From x In objdb.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                        If objcek Is Nothing Then
                                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                            Else
                                                itemx.Alternateby = ""
                                            End If
                                            itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                            itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.ApprovedDate = Now
                                            itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.CreatedDate = Now
                                            objdb.Entry(itemx).State = Entity.EntityState.Added
                                        Else
                                            Dim objcekdirector = objdb.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_for_Table_ID).ToList()
                                            If objcekdirector Is Nothing Then
                                                objdb.Entry(objcek).State = Entity.EntityState.Deleted
                                            Else

                                                objdb.Entry(objcek).CurrentValues.SetValues(itemx)
                                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                    objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                                Else
                                                    objcek.Alternateby = ""
                                                End If
                                                objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.LastUpdateDate = Now
                                                objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.ApprovedDate = Now
                                                objdb.Entry(objcek).State = Entity.EntityState.Modified
                                            End If
                                        End If
                                    Next

                                    For Each itemx As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Director_Phone
                                        Dim objcek As GoAMLDAL.goAML_Ref_Phone = (From x In objdb.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                        If objcek Is Nothing Then
                                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                            Else
                                                itemx.Alternateby = ""
                                            End If
                                            itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.ApprovedDate = Now
                                            itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.CreatedDate = Now
                                            itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                            objdb.Entry(itemx).State = Entity.EntityState.Added
                                        Else
                                            Dim objcekdirector = objdb.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_for_Table_ID).ToList()
                                            If objcekdirector Is Nothing Then
                                                objdb.Entry(objcek).State = Entity.EntityState.Deleted
                                            Else
                                                objdb.Entry(objcek).CurrentValues.SetValues(itemx)
                                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                    objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                                Else
                                                    objcek.Alternateby = ""
                                                End If
                                                objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.LastUpdateDate = Now
                                                objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.ApprovedDate = Now
                                                objdb.Entry(objcek).State = Entity.EntityState.Modified
                                            End If
                                        End If
                                    Next

                                    For Each itemx As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Director_Employer_Address
                                        Dim objcek As GoAMLDAL.goAML_Ref_Address = (From x In objdb.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID Select x).FirstOrDefault
                                        If objcek Is Nothing Then
                                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                            Else
                                                itemx.Alternateby = ""
                                            End If
                                            itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.ApprovedDate = Now
                                            itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.CreatedDate = Now
                                            itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                            objdb.Entry(itemx).State = Entity.EntityState.Added
                                        Else
                                            Dim objcekdirector = objdb.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.PK_Customer_Address_ID).ToList()
                                            If objcekdirector Is Nothing Then
                                                objdb.Entry(objcek).State = Entity.EntityState.Deleted
                                            Else
                                                objdb.Entry(objcek).CurrentValues.SetValues(itemx)
                                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                    objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                                Else
                                                    objcek.Alternateby = ""
                                                End If
                                                objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.LastUpdateDate = Now
                                                objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.ApprovedDate = Now
                                                objdb.Entry(objcek).State = Entity.EntityState.Modified
                                            End If
                                        End If
                                    Next

                                    For Each itemx As GoAMLDAL.goAML_Ref_Address In objModuledata.objListgoAML_Ref_Director_Address
                                        Dim objcek As GoAMLDAL.goAML_Ref_Address = (From x In objdb.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID Select x).FirstOrDefault
                                        If objcek Is Nothing Then
                                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                            Else
                                                itemx.Alternateby = ""
                                            End If
                                            itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.ApprovedDate = Now
                                            itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.CreatedDate = Now
                                            itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                            objdb.Entry(itemx).State = Entity.EntityState.Added
                                        Else
                                            Dim objcekdirector = objdb.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_To_Table_ID).ToList()
                                            If objcekdirector Is Nothing Then
                                                objdb.Entry(objcek).State = Entity.EntityState.Deleted
                                            Else
                                                objdb.Entry(objcek).CurrentValues.SetValues(itemx)
                                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                    objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                                Else
                                                    objcek.Alternateby = ""
                                                End If
                                                objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.LastUpdateDate = Now
                                                objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.ApprovedDate = Now
                                                objdb.Entry(objcek).State = Entity.EntityState.Modified
                                            End If
                                        End If
                                    Next
                                Next
                            End If


                            For Each itemx As GoAMLDAL.goAML_Ref_Phone In (From x In objdb.goAML_Ref_Phone Where x.FK_for_Table_ID = objCustomer.PK_Customer_ID Select x).ToList
                                Dim objcek As GoAMLDAL.goAML_Ref_Phone = objModuledata.objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                If objcek Is Nothing Then
                                    objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next
                            For Each item As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                Dim obcek As GoAMLDAL.goAML_Ref_Phone = (From x In objdb.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.CreatedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                Else
                                    objdb.Entry(obcek).CurrentValues.SetValues(item)
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        obcek.Alternateby = ""
                                    End If
                                    obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    obcek.LastUpdateDate = Now
                                    obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    obcek.ApprovedDate = Now
                                    objdb.Entry(obcek).State = Entity.EntityState.Modified
                                End If
                            Next

                            For Each itemx As GoAMLDAL.goAML_Ref_Address In (From x In objdb.goAML_Ref_Address Where x.FK_To_Table_ID = objModuledata.objgoAML_Ref_Customer.PK_Customer_ID Select x).ToList
                                Dim objcek As GoAMLDAL.goAML_Ref_Address = objModuledata.objlistgoaml_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                If objcek Is Nothing Then
                                    objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next
                            For Each item As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                Dim obcek As GoAMLDAL.goAML_Ref_Address = (From x In objdb.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.CreatedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                Else
                                    objdb.Entry(obcek).CurrentValues.SetValues(item)
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        obcek.Alternateby = ""
                                    End If
                                    obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    obcek.LastUpdateDate = Now
                                    obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    obcek.ApprovedDate = Now
                                    objdb.Entry(obcek).State = Entity.EntityState.Modified
                                End If
                            Next

                            If objCustomer.FK_Customer_Type_ID = 1 Then
                                For Each itemx As GoAMLDAL.goAML_Person_Identification In (From x In objdb.goAML_Person_Identification Where x.FK_Person_ID = objModuledata.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Person_Type = 1 Select x).ToList
                                    'Fix fk_personID diubah menjadi pk Saad  22102020
                                    'Dim objcek As GoAMLDAL.goAML_Person_Identification = objModuledata.objListgoAML_Ref_Identification.Find(Function(x) x.FK_Person_ID = itemx.PK_Person_Identification_ID)
                                    Dim objcek As GoAMLDAL.goAML_Person_Identification = objModuledata.objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID)
                                    If objcek Is Nothing Then
                                        objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                    End If
                                Next
                                For Each item As GoAMLDAL.goAML_Person_Identification In objModuledata.objListgoAML_Ref_Identification
                                    'Fix fk_personID diubah menjadi pk Saad  22102020
                                    'Dim obcek As GoAMLDAL.goAML_Person_Identification = (From x In objdb.goAML_Person_Identification Where x.FK_Person_ID = item.PK_Person_Identification_ID Select x).FirstOrDefault
                                    Dim obcek As GoAMLDAL.goAML_Person_Identification = (From x In objdb.goAML_Person_Identification Where x.PK_Person_Identification_ID = item.PK_Person_Identification_ID Select x).FirstOrDefault
                                    If obcek Is Nothing Then
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            item.Alternateby = ""
                                        End If
                                        item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        item.ApprovedDate = Now
                                        item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        item.CreatedDate = Now
                                        objdb.Entry(item).State = Entity.EntityState.Added
                                    Else
                                        objdb.Entry(obcek).CurrentValues.SetValues(item)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            obcek.Alternateby = ""
                                        End If
                                        obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        obcek.LastUpdateDate = Now
                                        obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        obcek.ApprovedDate = Now
                                        objdb.Entry(obcek).State = Entity.EntityState.Modified
                                    End If
                                Next
                                objdb.SaveChanges()
                                For Each itemx As GoAMLDAL.goAML_Ref_Phone In (From x In objdb.goAML_Ref_Phone Where x.FK_for_Table_ID = objModuledata.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Ref_Detail_Of = 5 Select x).ToList
                                    Dim objcek As GoAMLDAL.goAML_Ref_Phone = objModuledata.objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                    If objcek Is Nothing Then
                                        objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                    End If
                                Next
                                For Each item As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Employer_Phone
                                    Dim obcek As GoAMLDAL.goAML_Ref_Phone = (From x In objdb.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                    If obcek Is Nothing Then
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            item.Alternateby = ""
                                        End If
                                        item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        item.ApprovedDate = Now
                                        item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        item.CreatedDate = Now
                                        objdb.Entry(item).State = Entity.EntityState.Added
                                    Else
                                        objdb.Entry(obcek).CurrentValues.SetValues(item)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            obcek.Alternateby = ""
                                        End If
                                        obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        obcek.LastUpdateDate = Now
                                        obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        obcek.ApprovedDate = Now
                                        objdb.Entry(obcek).State = Entity.EntityState.Modified
                                    End If
                                Next
                                objdb.SaveChanges()
                                For Each itemx As GoAMLDAL.goAML_Ref_Address In (From x In objdb.goAML_Ref_Address Where x.FK_To_Table_ID = objModuledata.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Ref_Detail_Of = 5 Select x).ToList
                                    Dim objcek As GoAMLDAL.goAML_Ref_Address = objModuledata.objlistgoaml_Ref_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                    If objcek Is Nothing Then
                                        objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                    End If
                                Next
                                For Each item As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Employer_Address
                                    Dim obcek As GoAMLDAL.goAML_Ref_Address = (From x In objdb.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID Select x).FirstOrDefault
                                    If obcek Is Nothing Then
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            item.Alternateby = ""
                                        End If
                                        item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        item.ApprovedDate = Now
                                        item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        item.CreatedDate = Now
                                        objdb.Entry(item).State = Entity.EntityState.Added
                                    Else
                                        objdb.Entry(obcek).CurrentValues.SetValues(item)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            obcek.Alternateby = ""
                                        End If
                                        obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        obcek.LastUpdateDate = Now
                                        obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        obcek.ApprovedDate = Now
                                        objdb.Entry(obcek).State = Entity.EntityState.Modified
                                    End If
                                Next
                                objdb.SaveChanges()
                            End If

                            ' ======================= START: AUDIT TRAIL ================================
                            Using objDba As New GoAMLEntities
                                Dim header As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)

                                'AuditTrailDetail, NaelEdit
                                ' 2023-09-29, Nael: Menambahkan Audit Trail
                                ' CreateAuditTrailDetailEdit
                                'objModuledata
                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, objCustomer, objModuledataOld.objgoAML_Ref_Customer)
                                objDba.SaveChanges()

                                ' ============ Customer Phones ============
                                If objModuledata.objListgoAML_Ref_Phone.Count > 0 Then
                                    ' 2023-10-02, Nael: Cek data new di old
                                    For Each newUserPhone As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                        Dim oldUserPhone As GoAMLDAL.goAML_Ref_Phone = objModuledataOld.objListgoAML_Ref_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newUserPhone.PK_goAML_Ref_Phone))
                                        If oldUserPhone IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserPhone, oldUserPhone)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserPhone)
                                        End If
                                        objDba.SaveChanges()
                                    Next

                                    ' 2023-10-02, Nael: Cek data old di new
                                    For Each oldUserPhone As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                        Dim newUserPhone As GoAMLDAL.goAML_Ref_Phone = objModuledata.objListgoAML_Ref_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldUserPhone.PK_goAML_Ref_Phone))
                                        If newUserPhone IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserPhone, oldUserPhone)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserPhone)
                                        End If
                                        objDba.SaveChanges()
                                    Next
                                End If

                                ' ========== Customer Address ===========
                                If objModuledata.objlistgoaml_Ref_Address.Count > 0 Then
                                    ' 2023-10-02, Nael: Cek data new di old
                                    For Each newUserAddress As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                        Dim oldUserAddress As GoAMLDAL.goAML_Ref_Address = objModuledataOld.objlistgoaml_Ref_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newUserAddress.PK_Customer_Address_ID))
                                        If oldUserAddress IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserAddress, oldUserAddress)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserAddress)
                                        End If
                                        objDba.SaveChanges()
                                    Next

                                    ' 2023-10-02, Nael: Cek data old di new
                                    For Each oldUserAddress As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                        Dim newUserAddress As GoAMLDAL.goAML_Ref_Address = objModuledata.objlistgoaml_Ref_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldUserAddress.PK_Customer_Address_ID))
                                        If newUserAddress IsNot Nothing Then
                                            NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserAddress, oldUserAddress)
                                        Else
                                            NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserAddress)
                                        End If
                                        objDba.SaveChanges()
                                    Next
                                End If

                                If objModuledata.objgoAML_Ref_Customer.FK_Customer_Type_ID = 1 Then
                                    ' ======== Person Identification =========
                                    If objModuledata.objListgoAML_Ref_Identification.Count > 0 Then
                                        ' 2023-10-02, Nael: Cek data new di old
                                        For Each newUserIdentification As GoAMLDAL.goAML_Person_Identification In objModuledata.objListgoAML_Ref_Identification
                                            Dim oldUserIdentification As GoAMLDAL.goAML_Person_Identification = objModuledataOld.objListgoAML_Ref_Identification.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(newUserIdentification.PK_Person_Identification_ID))
                                            If oldUserIdentification IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserIdentification, oldUserIdentification)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserIdentification)
                                            End If
                                            objDba.SaveChanges()
                                        Next

                                        ' 2023-10-02, Nael: Cek data old di new
                                        For Each oldUserIdentification As GoAMLDAL.goAML_Person_Identification In objModuledata.objListgoAML_Ref_Identification
                                            Dim newUserIdentification As GoAMLDAL.goAML_Person_Identification = objModuledata.objListgoAML_Ref_Identification.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(oldUserIdentification.PK_Person_Identification_ID))
                                            If newUserIdentification IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserIdentification, oldUserIdentification)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserIdentification)
                                            End If
                                            objDba.SaveChanges()
                                        Next
                                    End If

                                    ' ======== Employer Address ==========
                                    If objModuledata.objlistgoaml_Ref_Employer_Address.Count > 0 Then
                                        ' 2023-10-02, Nael: Cek data new di old
                                        For Each newEmployerAddress As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Employer_Address
                                            Dim oldEmployerAddress As GoAMLDAL.goAML_Ref_Address = objModuledataOld.objlistgoaml_Ref_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newEmployerAddress.PK_Customer_Address_ID))
                                            If oldEmployerAddress IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerAddress, oldEmployerAddress)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newEmployerAddress)
                                            End If
                                            objDba.SaveChanges()
                                        Next

                                        ' 2023-10-02, Nael: Cek data old di new
                                        For Each oldEmployerAddress As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Employer_Address
                                            Dim newEmployerAddress As GoAMLDAL.goAML_Ref_Address = objModuledata.objlistgoaml_Ref_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldEmployerAddress.PK_Customer_Address_ID))
                                            If newEmployerAddress IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerAddress, oldEmployerAddress)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldEmployerAddress)
                                            End If
                                            objDba.SaveChanges()
                                        Next
                                    End If

                                    ' ======= Employer Phone =======
                                    If objModuledata.objListgoAML_Ref_Employer_Phone.Count > 0 Then
                                        ' 2023-10-02, Nael: Cek data new di old
                                        For Each newEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Employer_Phone
                                            Dim oldEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objModuledataOld.objListgoAML_Ref_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newEmployerPhone.PK_goAML_Ref_Phone))
                                            If oldEmployerPhone IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerPhone, oldEmployerPhone)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newEmployerPhone)
                                            End If
                                            objDba.SaveChanges()
                                        Next

                                        ' 2023-10-02, Nael: Cek data old di new
                                        For Each oldEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Employer_Phone
                                            Dim newEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objModuledata.objListgoAML_Ref_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldEmployerPhone.PK_goAML_Ref_Phone))
                                            If newEmployerPhone IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerPhone, oldEmployerPhone)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldEmployerPhone)
                                            End If
                                            objDba.SaveChanges()
                                        Next
                                    End If
                                End If

                                If objModuledata.objgoAML_Ref_Customer.FK_Customer_Type_ID = 2 Then
                                    If objModuledata.objlistgoaml_Ref_Director_Employer_Address.Count > 0 Then
                                        ' 2023-10-02, Nael: Cek data new di old
                                        For Each newDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Director_Employer_Address
                                            Dim oldDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address = objModuledataOld.objlistgoaml_Ref_Director_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorEmployerAddress.PK_Customer_Address_ID))
                                            If newDirectorEmployerAddress IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress, oldDirectorEmployerAddress)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress)
                                            End If
                                            objDba.SaveChanges()
                                        Next

                                        ' 2023-10-02, Nael: Cek data old di new
                                        For Each oldDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address In objModuledataOld.objlistgoaml_Ref_Director_Employer_Address
                                            Dim newDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address = objModuledata.objlistgoaml_Ref_Director_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldDirectorEmployerAddress.PK_Customer_Address_ID))
                                            If newDirectorEmployerAddress IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress, oldDirectorEmployerAddress)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorEmployerAddress)
                                            End If
                                            objDba.SaveChanges()
                                        Next
                                    End If

                                    If objModuledata.objListgoAML_Ref_Director_Employer_Phone.Count > 0 Then
                                        ' 2023-10-02, Nael: Cek data new di old
                                        For Each newDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Director_Employer_Phone
                                            Dim oldDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objModuledataOld.objListgoAML_Ref_Director_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorEmployerPhone.PK_goAML_Ref_Phone))
                                            If newDirectorEmployerPhone IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone, oldDirectorEmployerPhone)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone)
                                            End If
                                            objDba.SaveChanges()
                                        Next

                                        ' 2023-10-02, Nael: Cek data old di new
                                        For Each oldDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objModuledataOld.objListgoAML_Ref_Director_Employer_Phone
                                            Dim newDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objModuledata.objListgoAML_Ref_Director_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldDirectorEmployerPhone.PK_goAML_Ref_Phone))
                                            If newDirectorEmployerPhone IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone, oldDirectorEmployerPhone)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorEmployerPhone)
                                            End If
                                            objDba.SaveChanges()
                                        Next
                                    End If

                                    If objModuledata.objListgoAML_Ref_Director_Address.Count > 0 Then
                                        ' 2023-10-02, Nael: Cek data new di old
                                        For Each newDirectorAddress As GoAMLDAL.goAML_Ref_Address In objModuledata.objListgoAML_Ref_Director_Address
                                            Dim oldDirectorAddress As GoAMLDAL.goAML_Ref_Address = objModuledataOld.objListgoAML_Ref_Director_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorAddress.PK_Customer_Address_ID))
                                            If newDirectorAddress IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorAddress, oldDirectorAddress)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorAddress)
                                            End If
                                            objDba.SaveChanges()
                                        Next

                                        ' 2023-10-02, Nael: Cek data old di new
                                        For Each oldDirectorAddress As GoAMLDAL.goAML_Ref_Address In objModuledataOld.objListgoAML_Ref_Director_Address
                                            Dim newDirectorAddress As GoAMLDAL.goAML_Ref_Address = objModuledata.objListgoAML_Ref_Director_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldDirectorAddress.PK_Customer_Address_ID))
                                            If newDirectorAddress IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorAddress, oldDirectorAddress)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorAddress)
                                            End If
                                            objDba.SaveChanges()
                                        Next
                                    End If

                                    If objModuledata.objListgoAML_Ref_Director_Phone.Count > 0 Then
                                        For Each newDirectorPhone As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Director_Phone
                                            Dim oldDirectorPhone As GoAMLDAL.goAML_Ref_Phone = objModuledataOld.objListgoAML_Ref_Director_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorPhone.PK_goAML_Ref_Phone))
                                            If newDirectorPhone IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorPhone, oldDirectorPhone)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorPhone)
                                            End If
                                            objDba.SaveChanges()
                                        Next

                                        ' 2023-10-02, Nael: Cek data old di new
                                        For Each oldDirectorPhone As GoAMLDAL.goAML_Ref_Phone In objModuledataOld.objListgoAML_Ref_Director_Phone
                                            Dim newDirectorPhone As GoAMLDAL.goAML_Ref_Phone = objModuledata.objListgoAML_Ref_Director_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldDirectorPhone.PK_goAML_Ref_Phone))
                                            If newDirectorPhone IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorPhone, oldDirectorPhone)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorPhone)
                                            End If
                                            objDba.SaveChanges()
                                        Next
                                    End If

                                    If objModuledata.objListgoAML_Ref_Identification_Director.Count > 0 Then
                                        ' 2023-10-02, Nael: Cek data new di old
                                        For Each newDirectorIdentification As GoAMLDAL.goAML_Person_Identification In objModuledata.objListgoAML_Ref_Identification_Director
                                            Dim oldDirectorIdentification As GoAMLDAL.goAML_Person_Identification = objModuledataOld.objListgoAML_Ref_Identification_Director.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(newDirectorIdentification.PK_Person_Identification_ID))
                                            If newDirectorIdentification IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorIdentification, oldDirectorIdentification)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorIdentification)
                                            End If
                                            objDba.SaveChanges()
                                        Next

                                        ' 2023-10-02, Nael: Cek data old di new
                                        For Each oldDirectorIdentification As GoAMLDAL.goAML_Person_Identification In objModuledataOld.objListgoAML_Ref_Identification_Director
                                            Dim newDirectorIdentification As GoAMLDAL.goAML_Person_Identification = objModuledata.objListgoAML_Ref_Identification_Director.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(oldDirectorIdentification.PK_Person_Identification_ID))
                                            If newDirectorIdentification IsNot Nothing Then
                                                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorIdentification, oldDirectorIdentification)
                                            Else
                                                NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorIdentification)
                                            End If
                                            objDba.SaveChanges()
                                        Next
                                    End If

                                End If

                                objDba.SaveChanges() ' 2023-09-29, Nael: SaveChanges
                            End Using
                        ' ======================= END: AUDIT TRAIL ================================


                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            objdb.Entry(objModuledata.objgoAML_Ref_Customer).State = Entity.EntityState.Deleted

                            For Each item As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objdb.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            For Each item As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objdb.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            'For Each item As EmailTemplateAction In objModuledata.objListEmailTemplateAction
                            '    objdb.Entry(item).State = Entity.EntityState.Deleted
                            'Next

                            'For Each item As EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                            '    objdb.Entry(item).State = Entity.EntityState.Deleted
                            'Next

                            'audittrail
                            Dim objaudittrailheader As New GoAMLDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next
                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next


                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next



                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Activation
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            objdb.Entry(objModuledata.objgoAML_Ref_Customer).State = Entity.EntityState.Modified

                            'audittrail
                            Dim objaudittrailheader As New GoAMLDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Activation
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                    End Select
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()

                    'add by septian, goAML 5.0.1
                    objCustomer2.PK_Customer_ID = objCustomer.PK_Customer_ID
                    objCustomer2.CIF = objCustomer.CIF

                    If objApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update Then
                        Delete_Grid(objCustomer2.CIF)
                    End If


                    If objApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update Or objApproval.PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert Then
                        SaveTanpaApproval(objCustomer2, obj_customer_grid)
                    End If


                    'end add
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    'Shared Sub LoadPanelActivation(objPanel As FormPanel, objmodulename As String, unikkey As String)
    '    'done: code anelActivation


    '    'Dim objEmailTemplateDataBLL As NawaBLL.EmailTemplateDataBLL = NawaBLL.Common.Deserialize(objdata, GetType(NawaBLL.EmailTemplateDataBLL))
    '    Dim objgoAML_Customer As GoAMLDAL.goAML_Ref_Customer = GetCustomer(unikkey)
    '    Dim objListgoAML_Ref_Phone As List(Of GoAMLDAL.goAML_Ref_Phone) = GetListgoAML_Ref_PhoneByPKID(unikkey)
    '    Dim objListgoAML_Ref_Address As List(Of GoAMLDAL.goAML_Ref_Address) = GetListgoAML_Ref_AddressByPKID(unikkey)

    '    Dim INDV_gender As GoAMLDAL.goAML_Ref_Jenis_Kelamin = Nothing
    '    Dim INDV_statusCode As GoAMLDAL.goAML_Ref_Status_Rekening = Nothing
    '    Dim INDV_nationality1 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim INDV_nationality2 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim INDV_nationality3 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim INDV_residence As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim CORP_Incorporation_legal_form As GoAMLDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
    '    Dim CORP_incorporation_country_code As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim CORP_Role As GoAMLDAL.goAML_Ref_Party_Role = Nothing

    '    Using db As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
    '        If Not objgoAML_Customer Is Nothing Then
    '            Dim strunik As String = unikkey
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, objgoAML_Customer.CIF)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Account No", "AccountNo" & strunik, objgoAML_Customer.AccountNo)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Pembukaan Rekening", "Opened" & strunik, CDate(objgoAML_Customer.Opened).ToString("dd-MMM-yyyy"))
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Penutupan Rekening", "Closed" & strunik, CDate(objgoAML_Customer.Closed).ToString("dd-MMM-yyyy"))

    '            INDV_gender = db.goAML_Ref_Jenis_Kelamin.Where(Function(x) x.Kode = objgoAML_Customer.Gender).FirstOrDefault
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gender", "Gender" & strunik, INDV_gender.Keterangan)

    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Saldo Akhir", "balance" & strunik, objgoAML_Customer.Balance)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Saldo", "date_balance" & strunik, CDate(objgoAML_Customer.Date_Balance).ToString("dd-MMM-yyyy"))

    '            INDV_statusCode = db.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = objgoAML_Customer.Status_Code).FirstOrDefault
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Status Rekening", "status_code" & strunik, INDV_statusCode.Keterangan)

    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Penerima Manfaat Utama", "beneficiary" & strunik, objgoAML_Customer.Beneficiary)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan Terkait Penerima Manfaat Utama", "beneficiary_comment" & strunik, objgoAML_Customer.Beneficiary_Comment)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan", "comments" & strunik, objgoAML_Customer.Comments)

    '            If objgoAML_Customer.FK_Customer_Type_ID = "1" Then
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Title", "beneficiary_comment" & strunik, objgoAML_Customer.Beneficiary_Comment)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Lengkap", "INDV_last_name" & strunik, objgoAML_Customer.INDV_Last_Name)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Lahir", "INDV_Birthdate" & strunik, CDate(objgoAML_Customer.INDV_BirthDate).ToString("dd-MMM-yyyy"))
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Lahir", "INDV_Birth_place" & strunik, objgoAML_Customer.INDV_Birth_Place)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Ibu Kandung", "INDV_Mothers_name" & strunik, objgoAML_Customer.INDV_Mothers_Name)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Alias", "INDV_Alias" & strunik, objgoAML_Customer.INDV_Alias)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NIK", "INDV_SSN" & strunik, objgoAML_Customer.INDV_Alias)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Passport", "INDV_Passport_number" & strunik, objgoAML_Customer.INDV_Passport_Number)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Penerbit Passport", "INDV_Passport_country" & strunik, objgoAML_Customer.INDV_Passport_Country)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Identitas Lain", "INDV_ID_Number" & strunik, objgoAML_Customer.INDV_ID_Number)

    '                INDV_nationality1 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Nationality1).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 1", "INDV_Nationality1" & strunik, INDV_nationality1.Keterangan)

    '                INDV_nationality2 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Nationality2).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 2", "INDV_Nationality2" & strunik, INDV_nationality2.Keterangan)

    '                INDV_nationality3 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Nationality3).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 3", "INDV_Nationality3" & strunik, INDV_nationality3.Keterangan)

    '                INDV_residence = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Residence).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Domisili", "INDV_residence" & strunik, INDV_residence.Keterangan)

    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email", "INDV_Email" & strunik, objgoAML_Customer.INDV_Email)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Pekerjaan", "INDV_Occupation" & strunik, objgoAML_Customer.INDV_Occupation)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Bekerja", "INDV_employer_name" & strunik, objgoAML_Customer.INDV_Occupation)

    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartDate", "StartDate" & strunik, CDate(objEmailTemplate.StartDate).ToString("dd-MMM-yyyy"))
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartTime", "StartTime" & strunik, objEmailTemplate.StartTime))
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ExcludeHoliday", "ExcludeHoliday" & strunik, objEmailTemplate.ExcludeHoliday.GetValueOrDefault(False).ToString)

    '            Else
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartDate", "StartDate" & strunik, "")
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartTime", "StartTime" & strunik, "")
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ExcludeHoliday", "ExcludeHoliday" & strunik, "")

    '            End If

    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Active", "Active" & strunik, objgoAML_Customer.Active.ToString & " -->" & (Not objgoAML_Customer.Active).ToString)

    '            Dim objStore As New Ext.Net.Store
    '            objStore.ID = strunik & "StoreGrid"
    '            objStore.ClientIDMode = Web.UI.ClientIDMode.Static

    '            Dim objModel As New Ext.Net.Model
    '            Dim objField As Ext.Net.ModelField

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "PK_goAML_ref_phone"
    '            objField.Type = ModelFieldType.Auto
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "FK_Ref_detail_of"
    '            objField.Type = ModelFieldType.Auto
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "FK_for_Table_ID"
    '            objField.Type = ModelFieldType.Int
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_contact_type"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_communication_type"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_country_prefix"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_number"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_extension"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "comments"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objStore.Model.Add(objModel)



    '            Dim objListcolumn As New List(Of ColumnBase)
    '            Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '                objcolumnNo.Text = "No."
    '                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '                objListcolumn.Add(objcolumnNo)
    '            End Using


    '            Dim objColum As Ext.Net.Column


    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Detail Dari"
    '            objColum.DataIndex = "FK_Ref_detail_of"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)



    '            objColum = New Ext.Net.Column
    '            objColum.Text = "PK"
    '            objColum.DataIndex = "FK_for_Table_ID"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)


    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Kategori Kontak"
    '            objColum.DataIndex = "tph_contact_type"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Jenis Alat Komunikasi"
    '            objColum.DataIndex = "tph_communication_type"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Kode Area Telp"
    '            objColum.DataIndex = "tph_country_prefix"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Nomor Telepon"
    '            objColum.DataIndex = "tph_number"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Nomor Extensi"
    '            objColum.DataIndex = "tph_extension"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Catatan"
    '            objColum.DataIndex = "comments"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            Dim objdt As Data.DataTable = CopyGenericToDataTable(objListgoAML_Ref_Phone)
    '            Dim objcol As New Data.DataColumn
    '            objcol.ColumnName = "Detail of"
    '            objcol.DataType = GetType(String)
    '            objdt.Columns.Add(objcol)

    '            For Each item As DataRow In objdt.Rows
    '                Dim objtask As GoAMLDAL.goAML_For_Table = GetTabelTypeForByID(item("FK_Ref_Detail_Of"))
    '                If Not objtask Is Nothing Then
    '                    item("FK_Ref_Detail_Of") = objtask.Description
    '                End If
    '            Next

    '            '' ini
    '            Dim objStoreAddress As New Ext.Net.Store
    '            objStoreAddress.ID = strunik & "StoreGridReplacer"
    '            objStoreAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            Dim objModelAddress As New Ext.Net.Model
    '            Dim objFieldAddress As Ext.Net.ModelField




    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "PK_Customer_Address_ID"
    '            objFieldAddress.Type = ModelFieldType.Auto
    '            objModelAddress.Fields.Add(objFieldAddress)


    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "FK_Ref_Detail_Of"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "FK_To_Table_ID"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Address_Type"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Address"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Town"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "City"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Zip"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Country_Code"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "State"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Comments"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objStoreAddress.Model.Add(objModelAddress)


    '            Dim objListcolumnAddress As New List(Of ColumnBase)
    '            Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '                objcolumnNo.Text = "No."
    '                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '                objListcolumnAddress.Add(objcolumnNo)
    '            End Using

    '            '' Address
    '            Dim objColumAddress As Ext.Net.Column

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Detail dari"
    '            objColumAddress.DataIndex = "FK_Ref_Detail_Of"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "PK"
    '            objColumAddress.DataIndex = "FK_To_Table_ID"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Address Type"
    '            objColumAddress.DataIndex = "Address_Type"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Address"
    '            objColumAddress.DataIndex = "Address"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Town"
    '            objColumAddress.DataIndex = "Town"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "City"
    '            objColumAddress.DataIndex = "City"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Zip"
    '            objColumAddress.DataIndex = "Zip"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Country Code"
    '            objColumAddress.DataIndex = "Country_Code"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "State"
    '            objColumAddress.DataIndex = "State"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Comments"
    '            objColumAddress.DataIndex = "Comments"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)
    '            '' Address


    '            'Dim objStoreAction As New Store
    '            'objStoreAction.ID = strunik & "StoreGridAction"
    '            'objStoreAction.ClientIDMode = Web.UI.ClientIDMode.Static
    '            'Dim ObjModelAction As New Ext.Net.Model

    '            'ObjModelAction.Fields.Add(New ModelField("EmailActionType", ModelFieldType.String))
    '            'ObjModelAction.Fields.Add(New ModelField("TSQLtoExecute", ModelFieldType.String))

    '            'objStoreAction.Model.Add(ObjModelAction)

    '            'Dim objListcolumnAction As New List(Of ColumnBase)
    '            'Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '            '    objcolumnNo.Text = "No."
    '            '    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '            '    objListcolumnAction.Add(objcolumnNo)
    '            'End Using

    '            'objListcolumnAction.Add(New Ext.Net.Column() With {.Text = "EmailActionType", .DataIndex = "EmailActionType", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAction.Add(New Ext.Net.Column() With {.Text = "TSQLtoExecute", .DataIndex = "TSQLtoExecute", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})


    '            'Dim objdtaction As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objListEmailTemplateAction)
    '            'Dim objcolaction As New Data.DataColumn
    '            'objcolaction.ColumnName = "EmailActionType"
    '            'objcolaction.DataType = GetType(String)
    '            'objdtaction.Columns.Add(objcolaction)


    '            'For Each item As DataRow In objdtaction.Rows
    '            '    Dim objtask As NawaDAL.EmailActionType = NawaBLL.EmailTemplateBLL.GetEmailActionTypeByID(item("FK_EmailActionType_ID"))
    '            '    If Not objtask Is Nothing Then
    '            '        item("EmailActionType") = objtask.EmailActionTypeName
    '            '    End If

    '            'Next

    '            'Dim objStoreAttachment As New Store With {.ID = "StoreGridAttachment", .ClientIDMode = Web.UI.ClientIDMode.Static}
    '            'Dim ObjModelAttachment As New Ext.Net.Model

    '            'ObjModelAttachment.Fields.Add(New ModelField("EmailAttachmentType", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("NamaReport", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("ParameterReport", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("NamaFile", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("EmailRenderAsName", ModelFieldType.String))
    '            'objStoreAttachment.Model.Add(ObjModelAttachment)



    '            'Dim objListcolumnAttachment As New List(Of ColumnBase)
    '            'Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '            '    objcolumnNo.Text = "No."
    '            '    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '            '    objListcolumnAttachment.Add(objcolumnNo)
    '            'End Using

    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Email Attachment Type", .DataIndex = "EmailAttachmentType", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Report Name", .DataIndex = "NamaReport", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Report Parameter", .DataIndex = "ParameterReport", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Report File Name", .DataIndex = "NamaFile", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Render As", .DataIndex = "EmailRenderAsName", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})



    '            'Dim objdtattachment As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objListEmailTemplateAttachment)

    '            'Dim objcolAttachment As New Data.DataColumn
    '            'objcolAttachment.ColumnName = "EmailAttachmentType"
    '            'objcolAttachment.DataType = GetType(String)
    '            'objdtattachment.Columns.Add(objcolAttachment)

    '            'Dim objcolAttachment1 As New Data.DataColumn
    '            'objcolAttachment1.ColumnName = "EmailRenderAsName"
    '            'objcolAttachment1.DataType = GetType(String)
    '            'objdtattachment.Columns.Add(objcolAttachment1)

    '            'For Each item As DataRow In objdtattachment.Rows
    '            '    Dim objtask As NawaDAL.EmailAttachmentType = NawaBLL.EmailTemplateBLL.GetEMailAttachmentTypebypk(item("FK_EmailAttachmentType_ID"))
    '            '    If Not objtask Is Nothing Then
    '            '        item("EmailAttachmentType") = objtask.EmailAttachmentType1
    '            '    End If
    '            '    If item("FK_EmailRenderAs_Id").ToString <> "" Then
    '            '        Dim objtask1 As NawaDAL.EmailRenderA = NawaBLL.EmailTemplateBLL.GetEmailRenderAsbypk(item("FK_EmailRenderAs_Id"))
    '            '        If Not objtask1 Is Nothing Then
    '            '            item("EmailRenderAsName") = objtask1.EmailRenderAsName
    '            '        End If
    '            '    End If

    '            'Next


    '            NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Phone", objStore, objListcolumn, objdt)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Address", objStoreAddress, objListcolumnAddress, objListgoAML_Ref_Address)
    '            'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Action", objStoreAction, objListcolumnAction, objdtaction)
    '            'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Attachment", objStoreAttachment, objListcolumnAttachment, objdtattachment)

    '        End If
    '    End Using

    'End Sub

    Shared Function Reject(ID As String) As Boolean
        'done:reject
        Using objdb As New NawaDAL.NawaDataEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim objApproval As NawaDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If
                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert

                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            'audittrail
                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next




                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Update

                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))
                            Dim objModuledataOld As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(goAML_CustomerDataBLL))

                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledataOld.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                objATDetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objATDetail.FieldName = item.Name
                                objATDetail.OldValue = ""
                                If Not item.GetValue(objModuledataOld, Nothing) Is Nothing Then
                                    objATDetail.NewValue = item.GetValue(objModuledataOld, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                                objdb.Entry(objATDetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next
                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            'audittrail
                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next


                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Activation
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))


                            'audittrail
                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Activation
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next


                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next
                            'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                            '    objtype = itemheader.GetType
                            '    properties = objtype.GetProperties
                            '    For Each item As System.Reflection.PropertyInfo In properties
                            '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                            '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            '        objaudittraildetail.FieldName = item.Name
                            '        objaudittraildetail.OldValue = ""
                            '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                            '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                            '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                            '            Else
                            '                objaudittraildetail.NewValue = ""
                            '            End If
                            '        Else
                            '            objaudittraildetail.NewValue = ""
                            '        End If
                            '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            '    Next
                            'Next

                            'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                            '    objtype = itemheader.GetType
                            '    properties = objtype.GetProperties
                            '    For Each item As System.Reflection.PropertyInfo In properties
                            '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                            '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            '        objaudittraildetail.FieldName = item.Name
                            '        objaudittraildetail.OldValue = ""
                            '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                            '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                            '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                            '            Else
                            '                objaudittraildetail.NewValue = ""
                            '            End If
                            '        Else
                            '            objaudittraildetail.NewValue = ""
                            '        End If
                            '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            '    Next
                            'Next
                    End Select
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Public Shared Function CopyGenericToDataTable(Of T)(list As IList(Of T)) As DataTable
        Dim props As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
        Dim table As New DataTable()
        For i As Integer = 0 To props.Count - 1
            Dim prop As PropertyDescriptor = props(i)
            table.Columns.Add(prop.Name, If(Nullable.GetUnderlyingType(prop.PropertyType), prop.PropertyType))
        Next
        Dim values As Object() = New Object(props.Count - 1) {}
        For Each item As T In list
            For i As Integer = 0 To values.Length - 1
                values(i) = If(props(i).GetValue(item), DBNull.Value)
            Next
            table.Rows.Add(values)
        Next
        Return table
    End Function
#End Region

#Region "Save Edit Approval, By Septian, 12 Jan 2023"
    Shared Function GetCustomerbyCIF2(CIF As String) As goAML_CustomerDataBLL.goAML_Ref_Customer2
        Dim data As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT TOP 1 * FROM goAML_Ref_Customer WHERE CIF = '{CIF}'")

        Dim objReturn = New goAML_CustomerDataBLL.goAML_Ref_Customer2
        objReturn.Country_Of_Birth = data("Country_Of_Birth").ToString()
        objReturn.Full_Name_Frn = data("Full_Name_Frn").ToString()
        objReturn.Residence_Since = If(IsDBNull(data("Residence_Since")), Date.MinValue, data("Residence_Since"))
        objReturn.Is_Protected = If(IsDBNull(data("Is_Protected")), False, True)
        objReturn.Entity_Status = data("Entity_Status").ToString()
        objReturn.Entity_Status_Date = If(IsDBNull(data("Entity_Status_Date")), Date.MinValue, data("Entity_Status_Date"))

        Return objReturn
    End Function

    Shared Function GetPreviousName(CIF As String) As List(Of goAML_Ref_Customer_Previous_Name)
        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_Previous_Name WHERE CIF = '{CIF}'")
        Dim listReturn As New List(Of goAML_Ref_Customer_Previous_Name)

        For Each data As DataRow In dataDT.Rows
            Dim objReturn = New goAML_Ref_Customer_Previous_Name
            objReturn.CIF = data("CIF")
            objReturn.PK_goAML_Ref_Customer_Previous_Name_ID = data("PK_goAML_Ref_Customer_Previous_Name_ID")
            objReturn.FIRST_NAME = data("FIRST_NAME")
            objReturn.LAST_NAME = data("LAST_NAME")
            objReturn.COMMENTS = data("COMMENTS")

            listReturn.Add(objReturn)
        Next

        Return listReturn
    End Function

    Shared Function GetEmail(CIF As String) As List(Of DataModel.goAML_Ref_Customer_Email)
        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_Email WHERE CIF = '{CIF}'")
        Dim listReturn As New List(Of DataModel.goAML_Ref_Customer_Email)

        For Each data As DataRow In dataDT.Rows
            Dim objReturn = New DataModel.goAML_Ref_Customer_Email
            objReturn.PK_goAML_Ref_Customer_Email_ID = data("PK_goAML_Ref_Customer_Email_ID")
            objReturn.CIF = data("CIF")
            objReturn.EMAIL_ADDRESS = data("EMAIL_ADDRESS")

            listReturn.Add(objReturn)
        Next
        Return listReturn
    End Function

    Shared Function GetNetworkDevice(CIF As String) As List(Of goAML_Ref_Customer_Network_Device)
        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_Network_Device WHERE CIF = '{CIF}'")
        Dim listReturn As New List(Of goAML_Ref_Customer_Network_Device)

        For Each Data As DataRow In dataDT.Rows
            Dim objReturn = New goAML_Ref_Customer_Network_Device
            objReturn.PK_goAML_Ref_Customer_Network_Device_ID = Data("PK_goAML_Ref_Customer_Network_Device_ID")
            objReturn.CIF = Data("CIF")
            objReturn.DEVICE_NUMBER = Data("DEVICE_NUMBER")
            objReturn.OPERATING_SYSTEM = Data("OPERATING_SYSTEM")
            objReturn.SERVICE_PROVIDER = Data("SERVICE_PROVIDER")
            objReturn.IPV6 = Data("IPV6")
            objReturn.IPV4 = Data("IPV4")
            objReturn.CGN_PORT = Data("CGN_PORT")
            objReturn.IPV4_IPV6 = Data("IPV4_IPV6")
            objReturn.NAT = Data("NAT")
            objReturn.FIRST_SEEN_DATE = Data("FIRST_SEEN_DATE")
            objReturn.LAST_SEEN_DATE = Data("LAST_SEEN_DATE")
            objReturn.USING_PROXY = Data("USING_PROXY")
            objReturn.CITY = Data("CITY")
            objReturn.COUNTRY = Data("COUNTRY")
            objReturn.COMMENTS = Data("COMMENTS")

            listReturn.Add(objReturn)
        Next

        Return listReturn
    End Function

    Shared Function GetSocialMedia(CIF As String) As List(Of goAML_Ref_Customer_Social_Media)
        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_Social_Media WHERE CIF = '{CIF}'")
        Dim listReturn As New List(Of goAML_Ref_Customer_Social_Media)

        For Each Data As DataRow In dataDT.Rows
            Dim objReturn = New goAML_Ref_Customer_Social_Media
            objReturn.PK_goAML_Ref_Customer_Social_Media_ID = Data("PK_goAML_Ref_Customer_Social_Media_ID")
            objReturn.CIF = Data("CIF")
            objReturn.PK_goAML_Ref_Customer_Social_Media_ID = Data("PK_goAML_Ref_Customer_Social_Media_ID")
            objReturn.PLATFORM = Data("PLATFORM")
            objReturn.USER_NAME = Data("USER_NAME")
            objReturn.COMMENTS = Data("COMMENTS")

            listReturn.Add(objReturn)
        Next

        Return listReturn
    End Function

    Shared Function GetEmploymentHistory(CIF As String) As List(Of goAML_Ref_Customer_Employment_History)
        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_Employment_History WHERE CIF = '{CIF}'")
        Dim listReturn As New List(Of goAML_Ref_Customer_Employment_History)

        For Each Data As DataRow In dataDT.Rows
            Dim objReturn = New goAML_Ref_Customer_Employment_History
            objReturn.PK_goAML_Ref_Customer_Employment_History_ID = Data("PK_goAML_Ref_Customer_Employment_History_ID")
            objReturn.CIF = Data("CIF")
            objReturn.EMPLOYER_NAME = Data("EMPLOYER_NAME")
            objReturn.EMPLOYER_BUSINESS = Data("EMPLOYER_BUSINESS")
            objReturn.EMPLOYER_IDENTIFIER = Data("EMPLOYER_IDENTIFIER")
            objReturn.EMPLOYMENT_PERIOD_VALID_FROM = Data("EMPLOYMENT_PERIOD_VALID_FROM")
            objReturn.EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE = Data("EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE")
            objReturn.EMPLOYMENT_PERIOD_VALID_TO = Data("EMPLOYMENT_PERIOD_VALID_TO")
            objReturn.EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE = Data("EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE")

            listReturn.Add(objReturn)
        Next

        Return listReturn
    End Function

    Shared Function GetEntityIdentification(CIF As String) As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_Entity_Identification WHERE CIF = '{CIF}'")
        Dim listReturn As New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)

        For Each Data As DataRow In dataDT.Rows
            Dim objReturn = New DataModel.goAML_Ref_Customer_Entity_Identification
            objReturn.PK_goAML_Ref_Customer_Entity_Identification_ID = Data("PK_goAML_Ref_Customer_Entity_Identification_ID")
            objReturn.CIF = Data("CIF")
            If Not Data.IsNull("TYPE") Then
                objReturn.TYPE = Data("TYPE")
            End If
            If Not Data.IsNull("NUMBER") Then
                objReturn.NUMBER = Data("NUMBER")
            End If

            If Not Data.IsNull("ISSUE_DATE") Then
                objReturn.ISSUE_DATE = Data("ISSUE_DATE")
            End If
            If Not Data.IsNull("EXPIRY_DATE") Then
                objReturn.EXPIRY_DATE = Data("EXPIRY_DATE")
            End If
            If Not Data.IsNull("ISSUE_COUNTRY") Then
                objReturn.ISSUE_COUNTRY = Data("ISSUE_COUNTRY")
            End If
            If Not Data.IsNull("ISSUED_BY") Then
                objReturn.ISSUED_BY = Data("ISSUED_BY")
            End If
            If Not Data.IsNull("COMMENTS") Then
                objReturn.COMMENTS = Data("COMMENTS")
            End If

            listReturn.Add(objReturn)
        Next

        Return listReturn
    End Function

    Shared Function GetPEP(CIF As String) As List(Of DataModel.goAML_Ref_Customer_PEP)
        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_PEP WHERE CIF = '{CIF}'")
        Dim listReturn As New List(Of DataModel.goAML_Ref_Customer_PEP)

        For Each Data As DataRow In dataDT.Rows
            Dim objReturn = New DataModel.goAML_Ref_Customer_PEP
            objReturn.PK_goAML_Ref_Customer_PEP_ID = Data("PK_goAML_Ref_Customer_PEP_ID")
            objReturn.CIF = Data("CIF")
            objReturn.PEP_COUNTRY = Data("PEP_COUNTRY")
            objReturn.FUNCTION_NAME = Data("FUNCTION_NAME")
            objReturn.FUNCTION_DESCRIPTION = Data("FUNCTION_DESCRIPTION")
            If Not Data.IsNull("PEP_DATE_RANGE_VALID_FROM") Then
                objReturn.PEP_DATE_RANGE_VALID_FROM = Data("PEP_DATE_RANGE_VALID_FROM")
            End If
            If Not Data.IsNull("PEP_DATE_RANGE_IS_APPROX_FROM_DATE") Then
                objReturn.PEP_DATE_RANGE_IS_APPROX_FROM_DATE = Data("PEP_DATE_RANGE_IS_APPROX_FROM_DATE")
            End If
            If Not Data.IsNull("PEP_DATE_RANGE_VALID_TO") Then
                objReturn.PEP_DATE_RANGE_VALID_TO = Data("PEP_DATE_RANGE_VALID_TO")
            End If
            If Not Data.IsNull("PEP_DATE_RANGE_IS_APPROX_TO_DATE") Then
                objReturn.PEP_DATE_RANGE_IS_APPROX_TO_DATE = Data("PEP_DATE_RANGE_IS_APPROX_TO_DATE")
            End If
            objReturn.COMMENTS = Data("COMMENTS")

            listReturn.Add(objReturn)
        Next

        Return listReturn
    End Function

    Shared Function GetSanction(CIF As String) As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_Sanction WHERE CIF = '{CIF}'")
        Dim listReturn As New List(Of DataModel.goAML_Ref_Customer_Sanction)

        For Each Data As DataRow In dataDT.Rows
            Dim objReturn = New DataModel.goAML_Ref_Customer_Sanction
            objReturn.PK_goAML_Ref_Customer_Sanction_ID = Data("PK_goAML_Ref_Customer_Sanction_ID")
            objReturn.CIF = Data("CIF")
            objReturn.PK_goAML_Ref_Customer_Sanction_ID = Data("PK_goAML_Ref_Customer_Sanction_ID")
            objReturn.PROVIDER = Data("PROVIDER")
            objReturn.SANCTION_LIST_NAME = Data("SANCTION_LIST_NAME")
            objReturn.MATCH_CRITERIA = Data("MATCH_CRITERIA")
            objReturn.LINK_TO_SOURCE = Data("LINK_TO_SOURCE")
            objReturn.SANCTION_LIST_ATTRIBUTES = Data("SANCTION_LIST_ATTRIBUTES")
            If Not Data.IsNull("SANCTION_LIST_DATE_RANGE_VALID_FROM") Then
                objReturn.SANCTION_LIST_DATE_RANGE_VALID_FROM = Data("SANCTION_LIST_DATE_RANGE_VALID_FROM")
            End If
            If Not Data.IsNull("SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE") Then
                objReturn.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = Data("SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE")
            End If
            If Not Data.IsNull("SANCTION_LIST_DATE_RANGE_VALID_TO") Then
                objReturn.SANCTION_LIST_DATE_RANGE_VALID_TO = Data("SANCTION_LIST_DATE_RANGE_VALID_TO")
            End If
            If Not Data.IsNull("SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE") Then
                objReturn.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = Data("SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE")
            End If
            objReturn.COMMENTS = Data("COMMENTS")

            listReturn.Add(objReturn)
        Next

        Return listReturn
    End Function

    Shared Function GetRelatedPerson(CIF As String) As List(Of DataModel.goAML_Ref_Customer_Related_Person)

        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_Related_Person WHERE CIF = '{CIF}'")
            Dim listReturn As New List(Of DataModel.goAML_Ref_Customer_Related_Person)
            Dim emailDAL = New goAML_Ref_Customer_Email_DAL

        For Each Data As DataRow In dataDT.Rows
            Dim objReturn = New DataModel.goAML_Ref_Customer_Related_Person
            objReturn.PK_goAML_Ref_Customer_Related_Person_ID = Data("PK_goAML_Ref_Customer_Related_Person_ID")
            objReturn.CIF = Data("CIF")
            objReturn.PERSON_PERSON_RELATION = Data("PERSON_PERSON_RELATION")
            'objReturn.RELATION_DATE_RANGE_VALID_FROM = Data("RELATION_DATE_RANGE_VALID_FROM")
            'objReturn.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = Data("RELATION_DATE_RANGE_IS_APPROX_FROM_DATE")
            'objReturn.RELATION_DATE_RANGE_VALID_TO = Data("RELATION_DATE_RANGE_VALID_TO")
            'objReturn.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = Data("RELATION_DATE_RANGE_IS_APPROX_TO_DATE")
            objReturn.COMMENTS = Data("COMMENTS")
            objReturn.GENDER = Data("GENDER")
            objReturn.TITLE = Data("TITLE")
            'objReturn.FIRST_NAME = Data("FIRST_NAME")
            'objReturn.MIDDLE_NAME = Data("MIDDLE_NAME")
            'objReturn.PREFIX = Data("PREFIX")
            objReturn.LAST_NAME = Data("LAST_NAME")
            If Not Data.IsNull("BIRTHDATE") Then
                objReturn.BIRTHDATE = Data("BIRTHDATE")
            End If

            objReturn.BIRTH_PLACE = Data("BIRTH_PLACE")
            'objReturn.COUNTRY_OF_BIRTH = Data("COUNTRY_OF_BIRTH")
            objReturn.MOTHERS_NAME = Data("MOTHERS_NAME")
            objReturn.ALIAS = Data("ALIAS")
            'objReturn.FULL_NAME_FRN = Data("FULL_NAME_FRN")
            objReturn.SSN = Data("SSN")
            objReturn.PASSPORT_NUMBER = Data("PASSPORT_NUMBER")
            objReturn.PASSPORT_COUNTRY = Data("PASSPORT_COUNTRY")
            objReturn.ID_NUMBER = Data("ID_NUMBER")
            objReturn.NATIONALITY1 = Data("NATIONALITY1")
            objReturn.NATIONALITY2 = Data("NATIONALITY2")
            objReturn.NATIONALITY3 = Data("NATIONALITY3")
            objReturn.RESIDENCE = Data("RESIDENCE")
            'objReturn.RESIDENCE_SINCE = Data("RESIDENCE_SINCE")
            objReturn.OCCUPATION = Data("OCCUPATION")
            objReturn.DECEASED = Data("DECEASED")
            If Not Data.IsNull("DATE_DECEASED") Then
                objReturn.DATE_DECEASED = Data("DATE_DECEASED")
            End If
            objReturn.TAX_NUMBER = Data("TAX_NUMBER")
            objReturn.TAX_REG_NUMBER = Data("TAX_REG_NUMBER")
            objReturn.SOURCE_OF_WEALTH = Data("SOURCE_OF_WEALTH")
            objReturn.IS_PROTECTED = Data("IS_PROTECTED")

            'Dim whereEmail = $"FK_REF_DETAIL_OF = 31 AND CIF = {Helper.CommonHelper.WrapSqlVariable(objReturn.PK_goAML_Ref_Customer_Related_Person_ID)}"
            'objReturn.ObjList_GoAML_Ref_Customer_Email = emailDAL.GetData(whereEmail)

            ' 2023
            ' FIXME: Ini harus ditambahin bagian untuk nge read phones, address, 
            objReturn.ObjList_GoAML_Ref_Address = goAML_Customer_Service.AddressService.GetByPkFk(objReturn.PK_goAML_Ref_Customer_Related_Person_ID, 31)
            objReturn.ObjList_GoAML_Ref_Phone = goAML_Customer_Service.PhoneService.GetByPkFk(objReturn.PK_goAML_Ref_Customer_Related_Person_ID, 31)
            ' Lanjut nyari FK_Ref_Detail untuk employer related person
            ' 32 => Related Person Employer address, 31 => Related Person Address
            objReturn.ObjList_GoAML_Ref_Address_Work = goAML_Customer_Service.AddressService.GetByPkFk(objReturn.PK_goAML_Ref_Customer_Related_Person_ID, 32)
            objReturn.ObjList_GoAML_Ref_Phone_Work = goAML_Customer_Service.PhoneService.GetByPkFk(objReturn.PK_goAML_Ref_Customer_Related_Person_ID, 32)
            objReturn.ObjList_GoAML_Ref_Customer_Email = goAML_Customer_Service.EmailService.GetByPkFk(objReturn.PK_goAML_Ref_Customer_Related_Person_ID, 31)
            objReturn.ObjList_GoAML_Person_Identification = goAML_Customer_Service.PersonIdentificationService.GetByPkFk(objReturn.PK_goAML_Ref_Customer_Related_Person_ID, 31)
            objReturn.ObjList_GoAML_Ref_Customer_PEP = goAML_Customer_Service.PEPService.GetByPkFk(objReturn.PK_goAML_Ref_Customer_Related_Person_ID, 31)
            objReturn.ObjList_GoAML_Ref_Customer_Sanction = goAML_Customer_Service.SanctionService.GetByPkFk(objReturn.PK_goAML_Ref_Customer_Related_Person_ID, 31)
            listReturn.Add(objReturn)
        Next
        Return listReturn
    End Function

    Shared Function GetAdditionalInformation(CIF As String) As List(Of goAML_Ref_Customer_Additional_Information)
        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_Additional_Information WHERE CIF = '{CIF}'")
        Dim listReturn As New List(Of goAML_Ref_Customer_Additional_Information)

        For Each Data As DataRow In dataDT.Rows
            Dim objReturn = New goAML_Ref_Customer_Additional_Information
            objReturn.PK_goAML_Ref_Customer_Additional_Information_ID = Data("PK_goAML_Ref_Customer_Additional_Information_ID")
            objReturn.CIF = Data("CIF")
            objReturn.INFO_TYPE = Data("INFO_TYPE")
            objReturn.INFO_SUBJECT = Data("INFO_SUBJECT")
            objReturn.INFO_TEXT = Data("INFO_TEXT")
            objReturn.INFO_NUMERIC = Data("INFO_NUMERIC")
            objReturn.INFO_DATE = Data("INFO_DATE")
            objReturn.INFO_BOOLEAN = Data("INFO_BOOLEAN")

            listReturn.Add(objReturn)
        Next

        Return listReturn
    End Function

    Shared Function GetURL(CIF As String) As List(Of DataModel.goAML_Ref_Customer_URL)
        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_URL WHERE CIF = '{CIF}'")
        Dim listReturn As New List(Of DataModel.goAML_Ref_Customer_URL)

        For Each Data As DataRow In dataDT.Rows
            Dim objReturn = New DataModel.goAML_Ref_Customer_URL
            objReturn.PK_goAML_Ref_Customer_URL_ID = Data("PK_goAML_Ref_Customer_URL_ID")
            objReturn.CIF = Data("CIF")
            objReturn.URL = Data("URL")

            listReturn.Add(objReturn)
        Next

        Return listReturn
    End Function

    Shared Function GetRelatedEntities(CIF As String) As List(Of DataModel.goAML_Ref_Customer_Related_Entities)
        Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_Related_Entities WHERE CIF = '{CIF}'")
        Dim listReturn As New List(Of DataModel.goAML_Ref_Customer_Related_Entities)

        For Each Data As DataRow In dataDT.Rows
            Dim objReturn = New DataModel.goAML_Ref_Customer_Related_Entities
            objReturn.PK_goAML_Ref_Customer_Related_Entities_ID = Data("PK_goAML_Ref_Customer_Related_Entities_ID")
            objReturn.CIF = Data("CIF")
            If Not Data.IsNull("ENTITY_ENTITY_RELATION") Then
                objReturn.ENTITY_ENTITY_RELATION = Data("ENTITY_ENTITY_RELATION")
            End If

            'objReturn.RELATION_DATE_RANGE_VALID_FROM = Data("RELATION_DATE_RANGE_VALID_FROM")
            'objReturn.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = Data("RELATION_DATE_RANGE_IS_APPROX_FROM_DATE")
            'objReturn.RELATION_DATE_RANGE_VALID_TO = Data("RELATION_DATE_RANGE_VALID_TO")
            'objReturn.RELATION_DATE_RANGE_IS_APPROX_TO_DATE = Data("RELATION_DATE_RANGE_IS_APPROX_TO_DATE")
            'objReturn.SHARE_PERCENTAGE = Data("SHARE_PERCENTAGE")
            If Not Data.IsNull("COMMENTS") Then
                objReturn.COMMENTS = Data("COMMENTS")
            End If
            If Not Data.IsNull("NAME") Then
                objReturn.NAME = Data("NAME")
            End If
            If Not Data.IsNull("COMMERCIAL_NAME") Then
                objReturn.COMMERCIAL_NAME = Data("COMMERCIAL_NAME")
            End If
            If Not Data.IsNull("INCORPORATION_LEGAL_FORM") Then
                objReturn.INCORPORATION_LEGAL_FORM = Data("INCORPORATION_LEGAL_FORM")
            End If
            If Not Data.IsNull("INCORPORATION_NUMBER") Then
                objReturn.INCORPORATION_NUMBER = Data("INCORPORATION_NUMBER")
            End If
            If Not Data.IsNull("BUSINESS") Then
                objReturn.BUSINESS = Data("BUSINESS")
            End If

            'objReturn.ENTITY_STATUS = Data("ENTITY_STATUS")
            'objReturn.ENTITY_STATUS_DATE = Data("ENTITY_STATUS_DATE")
            If Not Data.IsNull("INCORPORATION_STATE") Then
                objReturn.INCORPORATION_STATE = Data("INCORPORATION_STATE")
            End If
            If Not Data.IsNull("INCORPORATION_COUNTRY_CODE") Then
                objReturn.INCORPORATION_COUNTRY_CODE = Data("INCORPORATION_COUNTRY_CODE")
            End If
            If Not Data.IsNull("INCORPORATION_DATE") Then
                objReturn.INCORPORATION_DATE = Data("INCORPORATION_DATE")
            End If
            If Not Data.IsNull("BUSINESS_CLOSED") Then
                objReturn.BUSINESS_CLOSED = Data("BUSINESS_CLOSED")
            End If
            If Not Data.IsNull("DATE_BUSINESS_CLOSED") Then
                objReturn.DATE_BUSINESS_CLOSED = Data("DATE_BUSINESS_CLOSED")
            End If
            If Not Data.IsNull("TAX_NUMBER") Then
                objReturn.TAX_NUMBER = Data("TAX_NUMBER")
            End If
            If Not Data.IsNull("TAX_REG_NUMBER") Then
                objReturn.TAX_REG_NUMBER = Data("TAX_REG_NUMBER")
            End If

            objReturn.ObjList_GoAML_Ref_Customer_Address = goAML_Customer_Service.AddressService.GetByPkFk(Data("PK_goAML_Ref_Customer_Related_Entities_ID"), 36)
            objReturn.ObjList_GoAML_Ref_Customer_Phone = goAML_Customer_Service.PhoneService.GetByPkFk(Data("PK_goAML_Ref_Customer_Related_Entities_ID"), 36)
            objReturn.ObjList_GoAML_Ref_Customer_Email = goAML_Customer_Service.EmailService.GetByPkFk(Data("PK_goAML_Ref_Customer_Related_Entities_ID"), 36)
            objReturn.ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Customer_Service.EntityIdentificationService.GetByPkFk(Data("PK_goAML_Ref_Customer_Related_Entities_ID"), 36)
            objReturn.ObjList_GoAML_Ref_Customer_URL = goAML_Customer_Service.UrlService.GetByPkFk(Data("PK_goAML_Ref_Customer_Related_Entities_ID"), 36)
            objReturn.ObjList_GoAML_Ref_Customer_Sanction = goAML_Customer_Service.SanctionService.GetByPkFk(Data("PK_goAML_Ref_Customer_Related_Entities_ID"), 36)

            listReturn.Add(objReturn)
        Next

        Return listReturn
    End Function

    Public Shared Sub SaveTanpaApproval(objData As GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer2, objList As GoAMLBLL.goAML_CustomerDataBLL.goAML_Grid_Customer)
        Try
            GoAMLBLL.goAML_CustomerBLL.InsertUpdate_Customer(objData)
            'GoAMLBLL.goAML_CustomerBLL.InsertUpdate_PreviousName(objList.ObjList_GoAML_PreviousName)
            GoAMLBLL.goAML_CustomerBLL.InsertUpdate_Customer_Email(objList.ObjList_GoAML_Email, objData.PK_Customer_ID)
            'GoAMLBLL.goAML_CustomerBLL.InsertUpdate_EmploymentHistory(objList.ObjList_GoAML_EmploymentHistory)
            GoAMLBLL.goAML_CustomerBLL.InsertUpdate_PEP(objList.ObjList_GoAML_PEP, objData.PK_Customer_ID)
            'GoAMLBLL.goAML_CustomerBLL.InsertUpdate_NetworkDevice(objList.ObjList_GoAML_NetworkDevice)
            'GoAMLBLL.goAML_CustomerBLL.InsertUpdate_SocialMedia(objList.ObjList_GoAML_SocialMedia)
            GoAMLBLL.goAML_CustomerBLL.InsertUpdate_Sanction(objList.ObjList_GoAML_Sanction, objData.PK_Customer_ID)
            GoAMLBLL.goAML_CustomerBLL.InsertUpdate_RelatedPerson(objList.ObjList_GoAML_RelatedPerson)
            'GoAMLBLL.goAML_CustomerBLL.InsertUpdate_AdditionalInformation(objList.ObjList_GoAML_AdditionalInformation)
            GoAMLBLL.goAML_CustomerBLL.InsertUpdate_EntityIdentification(objList.ObjList_GoAML_EntityIdentification, objData.PK_Customer_ID)
            GoAMLBLL.goAML_CustomerBLL.InsertUpdate_CustomerURL(objList.ObjList_GoAML_URL, objData.PK_Customer_ID)
            GoAMLBLL.goAML_CustomerBLL.InsertUpdate_RelatedEntities(objList.ObjList_GoAML_RelatedEntities)
            GoAMLBLL.goAML_CustomerBLL.InsertUpdate_Director(objList.objList_GoAML_Ref_Director)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Sub Delete_Grid(CIF As String)
        Try
            Dim ctx = New DataContext(AmlModule.Customer)

            Dim strQuery As String = ""

            'Delete Related Person
            strQuery += $"DELETE FROM goAML_Ref_Customer_Email WHERE CIF = '{CIF}';"
            strQuery += $"DELETE FROM goAML_Ref_Customer_PEP WHERE CIF = '{CIF}';"
            strQuery += $"DELETE FROM goAML_Ref_Customer_Sanction WHERE CIF = '{CIF}';"

            Dim related_person_list = ctx.goAML_Ref_Customer_Related_Person.GetData($"CIF = '{CIF}'")

            For Each rp As DataModel.goAML_Ref_Customer_Related_Person In related_person_list
                ctx.goAML_Person_Identification.Delete($"FK_Person_ID = {rp.PK_goAML_Ref_Customer_Related_Person_ID} AND FK_REF_DETAIL_OF = 31")
                ctx.goAML_Ref_Address.Delete($"FK_To_Table_ID = {rp.PK_goAML_Ref_Customer_Related_Person_ID} AND FK_REF_DETAIL_OF = 31")
                ctx.goAML_Ref_Phone.Delete($"FK_for_Table_ID = {rp.PK_goAML_Ref_Customer_Related_Person_ID} AND FK_REF_DETAIL_OF = 31")
                ctx.goAML_Ref_Address.Delete($"FK_To_Table_ID = {rp.PK_goAML_Ref_Customer_Related_Person_ID} AND FK_REF_DETAIL_OF = 32")
                ctx.goAML_Ref_Phone.Delete($"FK_for_Table_ID = {rp.PK_goAML_Ref_Customer_Related_Person_ID} AND FK_REF_DETAIL_OF = 32")
                ctx.goAML_Ref_Customer_Email.Delete($"CIF = '{rp.PK_goAML_Ref_Customer_Related_Person_ID}' AND FK_REF_DETAIL_OF = 31")
                ctx.goAML_Ref_Customer_Sanction.Delete($"CIF = '{rp.PK_goAML_Ref_Customer_Related_Person_ID}' AND FK_REF_DETAIL_OF = 31")
                ctx.goAML_Ref_Customer_PEP.Delete($"CIF = '{rp.PK_goAML_Ref_Customer_Related_Person_ID}' AND FK_REF_DETAIL_OF = 31")
            Next

            strQuery += $"DELETE FROM goAML_Ref_Customer_Related_Person WHERE CIF = '{CIF}';"

            strQuery += $"DELETE FROM goAML_Ref_Customer_URL WHERE CIF = '{CIF}';"
            strQuery += $"DELETE FROM goAML_Ref_Customer_Entity_Identification WHERE CIF = '{CIF}';"

            'Delete Related Entity
            Dim related_entity_list = ctx.goAML_Ref_Customer_Related_Entities.GetData($"CIF = '{CIF}'")

            For Each re As DataModel.goAML_Ref_Customer_Related_Entities In related_entity_list
                Dim where_re = $"CIF = '{re.PK_goAML_Ref_Customer_Related_Entities_ID}' AND FK_REF_DETAIL_OF = 36"

                ctx.goAML_Ref_Customer_Email.Delete(where_re)
                ctx.goAML_Ref_Customer_Entity_Identification.Delete(where_re)
                ctx.goAML_Ref_Customer_URL.Delete(where_re)
                ctx.goAML_Ref_Customer_Sanction.Delete(where_re)
            Next

            strQuery += $"DELETE FROM goAML_Ref_Customer_Related_Entities WHERE CIF = '{CIF}';"

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub SaveAddApproval()
        Try
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub InsertUpdate_Customer(objData As goAML_Ref_Customer2)
        Try
            Dim strQuery As String = ""
            strQuery = $"UPDATE [dbo].[goAML_Ref_Customer]"
            strQuery += $"   SET"

            If objData.FK_Customer_Type_ID = 2 Then
                'strQuery += $"      [Entity_Status] = '{objData.Entity_Status}'"
                'strQuery += $"      ,[Entity_Status_Date] = '{objData.Entity_Status_Date}'"
                Return
            Else
                'strQuery += $"       [Country_Of_Birth] = '{objData.Country_Of_Birth}'"
                'strQuery += $"      ,[Full_Name_Frn] = '{objData.Full_Name_Frn}'"
                'strQuery += $"      ,[Residence_Since] = '{objData.Residence_Since}'"
                strQuery += $"      [Is_Protected] = {If(objData.Is_Protected, 1, 0)}"
            End If

            strQuery += $" WHERE PK_Customer_ID = {objData.PK_Customer_ID}"

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub InsertUpdate_PreviousName(objList As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Previous_Name))
        Try
            Dim strQuery As String = ""
            For Each objData In objList
                'If objData.PK_goAML_Ref_Customer_Previous_Name_ID > 0 Then
                '    strQuery = $"update goAML_Ref_Customer_Previous_Name"
                '    strQuery += $" set"
                '    strQuery += $" CIF =  '{objData.CIF}'"
                '    strQuery += $" ,FIRST_NAME = '{objData.FIRST_NAME}'"
                '    strQuery += $" ,LAST_NAME =  '{objData.LAST_NAME}'"
                '    strQuery += $" ,COMMENTS =  '{objData.COMMENTS}'"
                '    strQuery += $" where PK_goAML_Ref_Customer_Previous_Name_ID = '{objData.PK_goAML_Ref_Customer_Previous_Name_ID}';"
                'Else
                strQuery = $"INSERT INTO goAML_Ref_Customer_Previous_Name("
                strQuery += $"   CIF,"
                strQuery += $"   FIRST_NAME,"
                strQuery += $"   LAST_NAME,"
                strQuery += $"   COMMENTS"
                strQuery += " ) VALUES ( "
                strQuery += $"   '{objData.CIF}',"
                strQuery += $"   '{objData.FIRST_NAME}',"
                strQuery += $"   '{objData.LAST_NAME}',"
                strQuery += $"   '{objData.COMMENTS}'"
                strQuery += " )"
                'End If

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next

        Catch ex As Exception

        End Try
    End Sub

    'goAML_Ref_Customer_Network_Device
    Public Shared Sub InsertUpdate_SocialMedia(objList As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device))
        Try
            Dim strQuery As String = ""
            For Each objData In objList
                'If objData.PK_goAML_Ref_Customer_Network_Device_ID > 0 Then
                '    strQuery = $"update goAML_Ref_Customer_Network_Device"
                '    strQuery += $" set"
                '    strQuery += $" CIF =  '{objData.CIF}'"
                '    strQuery += $" ,DEVICE_NUMBER = '{objData.DEVICE_NUMBER}'"
                '    strQuery += $" ,OPERATING_SYSTEM = '{objData.OPERATING_SYSTEM}'"
                '    strQuery += $" ,SERVICE_PROVIDER = '{objData.SERVICE_PROVIDER}'"
                '    strQuery += $" ,IPV6 = '{objData.IPV6}'"
                '    strQuery += $" ,IPV4 = '{objData.IPV4}'"
                '    strQuery += $" ,CGN_PORT = {objData.CGN_PORT}"
                '    strQuery += $" ,IPV4_IPV6 = '{objData.IPV4_IPV6}'"
                '    strQuery += $" ,NAT = '{objData.NAT}'"
                '    strQuery += $" ,FIRST_SEEN_DATE = '{objData.FIRST_SEEN_DATE}'"
                '    strQuery += $" ,LAST_SEEN_DATE = '{objData.LAST_SEEN_DATE}'"
                '    strQuery += $" ,USING_PROXY = {If(objData.USING_PROXY, 1, 0)}"
                '    strQuery += $" ,CITY = '{objData.CITY}'"
                '    strQuery += $" ,COUNTRY = '{objData.COUNTRY}'"
                '    strQuery += $" ,COMMENTS = '{objData.COMMENTS}'"
                '    strQuery += $" where PK_goAML_Ref_Customer_Network_Device_ID = '{objData.PK_goAML_Ref_Customer_Network_Device_ID}';"
                'Else
                strQuery = $"INSERT INTO goAML_Ref_Customer_Network_Device("
                strQuery += $"   CIF,"
                strQuery += $"   DEVICE_NUMBER,"
                strQuery += $"   OPERATING_SYSTEM,"
                strQuery += $"   SERVICE_PROVIDER,"
                strQuery += $"   FK_GOAML_REPORT_ID,"
                strQuery += $"   IPV6,"
                strQuery += $"   IPV4,"
                strQuery += $"   CGN_PORT,"
                strQuery += $"   IPV4_IPV6,"
                strQuery += $"   NAT,"
                strQuery += $"   FIRST_SEEN_DATE,"
                strQuery += $"   LAST_SEEN_DATE,"
                strQuery += $"   USING_PROXY,"
                strQuery += $"   CITY,"
                strQuery += $"   COUNTRY,"
                strQuery += $"   COMMENTS"
                strQuery += " ) VALUES ( "
                strQuery += $"   '{objData.CIF}',"
                strQuery += $"   '{objData.DEVICE_NUMBER}',"
                strQuery += $"   '{objData.OPERATING_SYSTEM}',"
                strQuery += $"   '{objData.SERVICE_PROVIDER}',"
                strQuery += $"   {objData.FK_GOAML_REPORT_ID},"
                strQuery += $"   '{objData.IPV6}',"
                strQuery += $"   '{objData.IPV4}',"
                strQuery += $"   '{objData.CGN_PORT}',"
                strQuery += $"   '{objData.IPV4_IPV6}',"
                strQuery += $"   '{objData.NAT}',"
                strQuery += $"   '{objData.FIRST_SEEN_DATE}',"
                strQuery += $"   '{objData.LAST_SEEN_DATE}',"
                strQuery += $"   {If(objData.USING_PROXY, 1, 0)},"
                strQuery += $"   '{objData.CITY}',"
                strQuery += $"   '{objData.COUNTRY}',"
                strQuery += $"   '{objData.COMMENTS}'"
                strQuery += " )"
                'End If



                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next

        Catch ex As Exception

        End Try
    End Sub

    'goAML_Ref_Customer_Email
    Public Shared Sub InsertUpdate_Customer_Email(objList As List(Of DataModel.goAML_Ref_Customer_Email), Optional headerID As Integer = -1)
        Try
            Dim strQuery As String = ""
            For Each objData In objList
                'If objData.PK_goAML_Ref_Customer_Email_ID > 0 Then
                '    strQuery = $"update goAML_Ref_Customer_Email"
                '    strQuery += $" set"
                '    strQuery += $" CIF =  '{objData.CIF}'"
                '    strQuery += $" ,EMAIL_ADDRESS = '{objData.EMAIL_ADDRESS}'"
                '    strQuery += $" where PK_goAML_Ref_Customer_Email_ID = '{objData.PK_goAML_Ref_Customer_Email_ID}';"
                'Else
                strQuery = $"INSERT INTO [dbo].[goAML_Ref_Customer_Email]"
                strQuery += $"           ([CIF]"
                strQuery += $"           ,[FK_REF_DETAIL_OF]"
                strQuery += $"           ,[FK_FOR_TABLE_ID]"
                strQuery += $"           ,[EMAIL_ADDRESS])"
                strQuery += $"     VALUES"
                strQuery += $"           ('{objData.CIF}'"
                strQuery += $"           ,'{1}'"
                strQuery += $"           ,'{headerID}'"
                strQuery += $"           ,'{objData.EMAIL_ADDRESS}')"
                'End If


                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
        Catch ex As Exception

        End Try
    End Sub

    'goAML_Ref_Customer_Social_Media
    Public Shared Sub InsertUpdate_SocialMedia(objList As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Social_Media))
        Try
            Dim strQuery As String = ""
            For Each objData In objList
                'If objData.PK_goAML_Ref_Customer_Social_Media_ID > 0 Then
                '    strQuery = $"update goAML_Ref_Customer_Social_Media"
                '    strQuery += $" set"
                '    strQuery += $" CIF =  '{objData.CIF}'"
                '    strQuery += $" ,PLATFORM = '{objData.PLATFORM}'"
                '    strQuery += $" ,USER_NAME = '{objData.USER_NAME}'"
                '    strQuery += $" ,COMMENTS = '{objData.COMMENTS}'"
                '    strQuery += $" where PK_goAML_Ref_Customer_Social_Media_ID = '{objData.PK_goAML_Ref_Customer_Social_Media_ID}';"
                'Else
                strQuery = $"INSERT INTO [dbo].[goAML_Ref_Customer_Social_Media]"
                strQuery += $"           ([CIF]"
                strQuery += $"           ,[PLATFORM]"
                strQuery += $"           ,[USER_NAME]"
                strQuery += $"           ,[COMMENTS])"
                strQuery += $"     VALUES"
                strQuery += $"           ('{objData.CIF}'"
                strQuery += $"           ,'{objData.PLATFORM}'"
                strQuery += $"           ,'{objData.USER_NAME}'"
                strQuery += $"           ,'{objData.COMMENTS}')"
                'End If


                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
        Catch ex As Exception

        End Try
    End Sub

    'goAML_Ref_Customer_Employment_History
    Public Shared Sub InsertUpdate_EmploymentHistory(objList As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Employment_History))
        Try
            Dim strQuery As String = ""
            For Each objData In objList
                'If objData.PK_goAML_Ref_Customer_Employment_History_ID > 0 Then
                '    strQuery = $"update goAML_Ref_Customer_Employment_History"
                '    strQuery += $" set"
                '    strQuery += $" CIF =  '{objData.CIF}'"
                '    strQuery += $" ,EMPLOYER_NAME = '{objData.EMPLOYER_NAME}'"
                '    strQuery += $" ,EMPLOYER_BUSINESS = '{objData.EMPLOYER_BUSINESS}'"
                '    strQuery += $" ,EMPLOYER_IDENTIFIER = '{objData.EMPLOYER_IDENTIFIER}'"
                '    strQuery += $" ,EMPLOYMENT_PERIOD_VALID_FROM = '{objData.EMPLOYMENT_PERIOD_VALID_FROM}'"
                '    strQuery += $" ,EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE = {If(objData.EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE, 1, 0)}"
                '    strQuery += $" ,EMPLOYMENT_PERIOD_VALID_TO = '{objData.EMPLOYMENT_PERIOD_VALID_TO}'"
                '    strQuery += $" ,EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE = {If(objData.EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE, 1, 0)}"
                '    strQuery += $" where PK_goAML_Ref_Customer_Employment_History_ID = '{objData.PK_goAML_Ref_Customer_Employment_History_ID}';"
                'Else
                strQuery = $"INSERT INTO [dbo].[goAML_Ref_Customer_Employment_History]"
                strQuery += $"       ([CIF]"
                strQuery += $"       ,[employer_name]"
                strQuery += $"       ,[employer_business]"
                strQuery += $"       ,[employer_identifier]"
                strQuery += $"       ,[employment_period_valid_from]"
                strQuery += $"       ,[employment_period_is_approx_from_date]"
                strQuery += $"       ,[employment_period_valid_to]"
                strQuery += $"       ,[employment_period_is_approx_to_date])"
                strQuery += $" Values"
                strQuery += $"       ('{objData.CIF}'"
                strQuery += $"       ,'{objData.EMPLOYER_NAME}'"
                strQuery += $"       ,'{objData.EMPLOYER_BUSINESS}'"
                strQuery += $"       ,'{objData.EMPLOYER_IDENTIFIER}'"
                strQuery += $"       ,'{objData.EMPLOYMENT_PERIOD_VALID_FROM}'"
                strQuery += $"       ,{If(objData.EMPLOYMENT_PERIOD_IS_APPROX_FROM_DATE, 1, 0)}"
                strQuery += $"       ,'{objData.EMPLOYMENT_PERIOD_VALID_TO}'"
                strQuery += $"       ,{If(objData.EMPLOYMENT_PERIOD_IS_APPROX_TO_DATE, 1, 0)})"
                'End If



                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
        Catch ex As Exception

        End Try
    End Sub

    'goAML_Ref_Customer_Entity_Identification
    Public Shared Sub InsertUpdate_EntityIdentification(objList As List(Of DataModel.goAML_Ref_Customer_Entity_Identification), Optional headerID As Integer = -1)
        Try
            Dim strQuery As String = ""
            For Each objData In objList
                'If objData.PK_goAML_Ref_Customer_Entity_Identification_ID > 0 Then
                '    strQuery = $"update goAML_Ref_Customer_Entity_Identification"
                '    strQuery += $" set"
                '    strQuery += $" CIF =  '{objData.CIF}'"
                '    strQuery += $" ,TYPE = '{objData.TYPE}'"
                '    strQuery += $" ,NUMBER = '{objData.NUMBER}'"
                '    strQuery += $" ,ISSUE_DATE = '{objData.ISSUE_DATE}'"
                '    strQuery += $" ,EXPIRY_DATE = '{objData.EXPIRY_DATE}'"
                '    strQuery += $" ,ISSUED_BY = '{objData.ISSUED_BY}'"
                '    strQuery += $" ,ISSUE_COUNTRY = '{objData.ISSUE_COUNTRY}'"
                '    strQuery += $" ,COMMENTS = '{objData.COMMENTS}'"
                '    strQuery += $" where PK_goAML_Ref_Customer_Entity_Identification_ID = '{objData.PK_goAML_Ref_Customer_Entity_Identification_ID}';"
                'Else
                strQuery = $"INSERT INTO goAML_Ref_Customer_Entity_Identification"
                strQuery += $"           ([CIF]"
                strQuery += $"           ,[FK_REF_DETAIL_OF]"
                strQuery += $"           ,[FK_FOR_TABLE_ID]"
                strQuery += $"           ,[TYPE]"
                strQuery += $"           ,[NUMBER]"
                If objData.ISSUE_DATE IsNot Nothing Then
                    If Not objData.ISSUE_DATE.ToString().Contains("0001") Then
                        strQuery += $"           ,[ISSUE_DATE]"
                    End If
                End If
                If objData.EXPIRY_DATE IsNot Nothing Then
                    If Not objData.EXPIRY_DATE.ToString().Contains("0001") Then
                        strQuery += $"           ,[EXPIRY_DATE]"
                    End If
                End If
                strQuery += $"           ,[ISSUED_BY]"
                strQuery += $"           ,[ISSUE_COUNTRY]"
                strQuery += $"           ,[COMMENTS])"
                strQuery += $"     VALUES"
                strQuery += $"           ('{objData.CIF}'"
                strQuery += $"           ,'{1}'"
                strQuery += $"           ,'{headerID}'"
                strQuery += $"           ,'{objData.TYPE}'"
                strQuery += $"           ,'{objData.NUMBER}'"
                If objData.ISSUE_DATE IsNot Nothing Then
                    If Not objData.ISSUE_DATE.ToString().Contains("0001") Then
                        strQuery += $"           ,'{objData.ISSUE_DATE}'"
                    End If
                End If
                If objData.EXPIRY_DATE IsNot Nothing Then
                    If Not objData.EXPIRY_DATE.ToString().Contains("0001") Then
                        strQuery += $"           ,'{objData.EXPIRY_DATE}'"
                    End If
                End If
                strQuery += $"           ,'{objData.ISSUED_BY}'"
                strQuery += $"           ,'{objData.ISSUE_COUNTRY}'"
                strQuery += $"           ,'{objData.COMMENTS}')"
                'End If



                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
        Catch ex As Exception

        End Try
    End Sub

    'goAML_Ref_Customer_PEP
    Public Shared Sub InsertUpdate_PEP(objList As List(Of DataModel.goAML_Ref_Customer_PEP), Optional headerID As Integer = -1)
        Try
            Dim strQuery As String = ""
            For Each objData In objList
                'If objData.PK_goAML_Ref_Customer_PEP_ID > 0 Then
                '    strQuery = $"update goAML_Ref_Customer_PEP"
                '    strQuery += $" set"
                '    strQuery += $" CIF =  '{objData.CIF}'"
                '    strQuery += $" ,PEP_COUNTRY = '{objData.PEP_COUNTRY}'"
                '    strQuery += $" ,FUNCTION_NAME = '{objData.FUNCTION_NAME}'"
                '    strQuery += $" ,FUNCTION_DESCRIPTION = '{objData.FUNCTION_DESCRIPTION}'"
                '    strQuery += $" ,PEP_DATE_RANGE_VALID_FROM = '{objData.PEP_DATE_RANGE_VALID_FROM}'"
                '    strQuery += $" ,PEP_DATE_RANGE_IS_APPROX_FROM_DATE = {If(objData.PEP_DATE_RANGE_IS_APPROX_FROM_DATE, 1, 0)}"
                '    strQuery += $" ,PEP_DATE_RANGE_VALID_TO = '{objData.PEP_DATE_RANGE_VALID_TO}'"
                '    strQuery += $" ,PEP_DATE_RANGE_IS_APPROX_TO_DATE = {If(objData.PEP_DATE_RANGE_IS_APPROX_TO_DATE, 1, 0)}"
                '    strQuery += $" ,COMMENTS = '{objData.COMMENTS}'"
                '    strQuery += $" where PK_goAML_Ref_Customer_PEP_ID = '{objData.PK_goAML_Ref_Customer_PEP_ID}';"
                'Else
                strQuery = $"INSERT INTO [dbo].[goAML_Ref_Customer_PEP]"
                strQuery += $"           ([CIF]"
                strQuery += $"           ,[FK_REF_DETAIL_OF]"
                strQuery += $"           ,[FK_FOR_TABLE_ID]"
                strQuery += $"           ,[PEP_COUNTRY]"
                strQuery += $"           ,[FUNCTION_NAME]"
                strQuery += $"           ,[FUNCTION_DESCRIPTION]"
                'strQuery += $"           ,[PEP_DATE_RANGE_VALID_FROM]"
                'strQuery += $"           ,[PEP_DATE_RANGE_IS_APPROX_FROM_DATE]"
                'strQuery += $"           ,[PEP_DATE_RANGE_VALID_TO]"
                'strQuery += $"           ,[PEP_DATE_RANGE_IS_APPROX_TO_DATE]"
                strQuery += $"           ,[COMMENTS])"
                strQuery += $"     VALUES"
                strQuery += $"           ('{objData.CIF}'"
                strQuery += $"           ,'{1}'"
                strQuery += $"           ,'{headerID}'"
                strQuery += $"           ,'{objData.PEP_COUNTRY}'"
                strQuery += $"           ,'{objData.FUNCTION_NAME}'"
                strQuery += $"           ,'{objData.FUNCTION_DESCRIPTION}'"
                'strQuery += $"           ,'{objData.PEP_DATE_RANGE_VALID_FROM}'"
                'strQuery += $"           ,{If(objData.PEP_DATE_RANGE_IS_APPROX_FROM_DATE, 1, 0)}"
                'strQuery += $"           ,'{objData.PEP_DATE_RANGE_VALID_TO}'"
                'strQuery += $"           ,{If(objData.PEP_DATE_RANGE_IS_APPROX_TO_DATE, 1, 0)}"
                strQuery += $"           ,'{objData.COMMENTS}')"
                'End If



                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub InsertUpdate_NetworkDevice(objList As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Network_Device))
        Try
            Dim strQuery As String = ""
            For Each objData In objList
                'If objData.PK_goAML_Ref_Customer_Network_Device_ID > 0 Then
                '    strQuery = $"update goAML_Ref_Customer_Network_Device_ID"
                '    strQuery += $" set"
                '    strQuery += $" CIF =  '{objData.CIF}'"
                '    strQuery += $" ,DEVICE_NUMBER = '{objData.DEVICE_NUMBER}'"
                '    strQuery += $" ,OPERATING_SYSTEM = '{objData.OPERATING_SYSTEM}'"
                '    strQuery += $" ,SERVICE_PROVIDER = '{objData.SERVICE_PROVIDER}'"
                '    strQuery += $" ,IPV6 = '{objData.IPV6}'"
                '    strQuery += $" ,IPV4 = '{objData.IPV4}'"
                '    strQuery += $" ,CGN_PORT = {objData.CGN_PORT}"
                '    strQuery += $" ,IPV4_IPV6 = '{objData.IPV4_IPV6}'"
                '    strQuery += $" ,NAT = '{objData.NAT}'"
                '    strQuery += $" ,FIRST_SEEN_DATE = '{objData.FIRST_SEEN_DATE}'"
                '    strQuery += $" ,LAST_SEEN_DATE = '{objData.LAST_SEEN_DATE}'"
                '    strQuery += $" ,USING_PROXY = {If(objData.USING_PROXY, 1, 0)}"
                '    strQuery += $" ,CITY = '{objData.CITY}'"
                '    strQuery += $" ,COUNTRY = '{objData.COUNTRY}'"
                '    strQuery += $" ,COMMENTS = '{objData.COMMENTS}'"
                '    strQuery += $" where PK_goAML_Ref_Customer_Network_Device_ID = {objData.PK_goAML_Ref_Customer_Network_Device_ID};"
                'Else
                strQuery = $"INSERT INTO [dbo].[goAML_Ref_Customer_Network_Device]"
                strQuery += $"           ([CIF]"
                strQuery += $"           ,[FK_GOAML_REPORT_ID]"
                strQuery += $"           ,[DEVICE_NUMBER]"
                strQuery += $"           ,[OPERATING_SYSTEM]"
                strQuery += $"           ,[SERVICE_PROVIDER]"
                strQuery += $"           ,[IPV6]"
                strQuery += $"           ,[IPV4]"
                strQuery += $"           ,[CGN_PORT]"
                strQuery += $"           ,[IPV4_IPV6]"
                strQuery += $"           ,[NAT]"
                strQuery += $"           ,[FIRST_SEEN_DATE]"
                strQuery += $"           ,[LAST_SEEN_DATE]"
                strQuery += $"           ,[USING_PROXY]"
                strQuery += $"           ,[CITY]"
                strQuery += $"           ,[COUNTRY]"
                strQuery += $"           ,[COMMENTS])"
                strQuery += $"     VALUES"
                strQuery += $"           ('{objData.CIF}'"
                strQuery += $"           ,{objData.FK_GOAML_REPORT_ID}"
                strQuery += $"           ,'{objData.DEVICE_NUMBER}'"
                strQuery += $"           ,'{objData.OPERATING_SYSTEM}'"
                strQuery += $"           ,'{objData.SERVICE_PROVIDER}'"
                strQuery += $"           ,'{objData.IPV6}'"
                strQuery += $"           ,'{objData.IPV4}'"
                strQuery += $"           ,{objData.CGN_PORT}"
                strQuery += $"           ,'{objData.IPV4_IPV6}'"
                strQuery += $"           ,'{objData.NAT}'"
                strQuery += $"           ,'{objData.FIRST_SEEN_DATE}'"
                strQuery += $"           ,'{objData.LAST_SEEN_DATE}'"
                strQuery += $"           ,{If(objData.USING_PROXY, 1, 0)}"
                strQuery += $"           ,'{objData.CITY}'"
                strQuery += $"           ,'{objData.COUNTRY}'"
                strQuery += $"           ,'{objData.COMMENTS}')"
                'End If

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
        Catch ex As Exception

        End Try
    End Sub

    'goAML_Ref_Customer_Sanction
    Public Shared Sub InsertUpdate_Sanction(objList As List(Of DataModel.goAML_Ref_Customer_Sanction), Optional headerID As Integer = -1)
        Try
            Dim strQuery As String = ""
            For Each objData In objList
                'If objData.PK_goAML_Ref_Customer_Sanction_ID > 0 Then
                '    strQuery = $"update goAML_Ref_Customer_Sanction"
                '    strQuery += $" set"
                '    strQuery += $" CIF =  '{objData.CIF}'"
                '    strQuery += $" ,PROVIDER = '{objData.PROVIDER}'"
                '    strQuery += $" ,SANCTION_LIST_NAME = '{objData.SANCTION_LIST_NAME}'"
                '    strQuery += $" ,MATCH_CRITERIA = '{objData.MATCH_CRITERIA}'"
                '    strQuery += $" ,LINK_TO_SOURCE = '{objData.LINK_TO_SOURCE}'"
                '    strQuery += $" ,SANCTION_LIST_ATTRIBUTES = '{objData.SANCTION_LIST_ATTRIBUTES}'"
                '    strQuery += $" ,SANCTION_LIST_DATE_RANGE_VALID_FROM = '{objData.SANCTION_LIST_DATE_RANGE_VALID_FROM}'"
                '    strQuery += $" ,SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = {If(objData.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE, 1, 0)}"
                '    strQuery += $" ,SANCTION_LIST_DATE_RANGE_VALID_TO = '{objData.SANCTION_LIST_DATE_RANGE_VALID_TO}'"
                '    strQuery += $" ,SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = {If(objData.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE, 1, 0)}"
                '    strQuery += $" ,COMMENTS = '{objData.COMMENTS}'"
                '    strQuery += $" where PK_goAML_Ref_Customer_Sanction_ID = '{objData.PK_goAML_Ref_Customer_Sanction_ID}';"
                'Else
                strQuery = $"INSERT INTO [dbo].[goAML_Ref_Customer_Sanction]"
                strQuery += $"           ([CIF]"
                strQuery += $"           ,[FK_REF_DETAIL_OF]"
                strQuery += $"           ,[FK_FOR_TABLE_ID]"
                strQuery += $"           ,[PROVIDER]"
                strQuery += $"           ,[SANCTION_LIST_NAME]"
                strQuery += $"           ,[MATCH_CRITERIA]"
                strQuery += $"           ,[LINK_TO_SOURCE]"
                strQuery += $"           ,[SANCTION_LIST_ATTRIBUTES]"
                'strQuery += $"           ,[SANCTION_LIST_DATE_RANGE_VALID_FROM]"
                'strQuery += $"           ,[SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE]"
                'strQuery += $"           ,[SANCTION_LIST_DATE_RANGE_VALID_TO]"
                'strQuery += $"           ,[SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE]"
                strQuery += $"           ,[COMMENTS])"
                strQuery += $"     VALUES"
                strQuery += $"           ('{objData.CIF}'"
                strQuery += $"           ,'{1}'"
                strQuery += $"           ,'{headerID}'"
                strQuery += $"           ,'{objData.PROVIDER}'"
                strQuery += $"           ,'{objData.SANCTION_LIST_NAME}'"
                strQuery += $"           ,'{objData.MATCH_CRITERIA}'"
                strQuery += $"           ,'{objData.LINK_TO_SOURCE}'"
                strQuery += $"           ,'{objData.SANCTION_LIST_ATTRIBUTES}'"
                'strQuery += $"           ,'{objData.SANCTION_LIST_DATE_RANGE_VALID_FROM}'"
                'strQuery += $"           ,{If(objData.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE, 1, 0)}"
                'strQuery += $"           ,'{objData.SANCTION_LIST_DATE_RANGE_VALID_TO}'"
                'strQuery += $"           ,{If(objData.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE, 1, 0)}"
                strQuery += $"           ,'{objData.COMMENTS}')"
                'End If

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
        Catch ex As Exception

        End Try
    End Sub

    'goAML_Ref_Customer_Related_Person
    Public Shared Sub InsertUpdate_RelatedPerson(objList As List(Of DataModel.goAML_Ref_Customer_Related_Person))
        Try
            Dim context = New DataContext(AmlModule.Customer)

            Dim strQuery As String = ""
            For Each objData In objList
                'If objData.PK_goAML_Ref_Customer_Related_Person_ID > 0 Then
                '    strQuery = $"update goAML_Ref_Customer_Related_Person"
                '    strQuery += $" set"
                '    strQuery += $" CIF =  '{objData.CIF}'"
                '    strQuery += $" ,PERSON_PERSON_RELATION = '{objData.PERSON_PERSON_RELATION}'"
                '    strQuery += $" ,RELATION_DATE_RANGE_VALID_FROM = '{objData.RELATION_DATE_RANGE_VALID_FROM}'"
                '    strQuery += $" ,RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = {If(objData.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE, 1, 0)}"
                '    strQuery += $" ,RELATION_DATE_RANGE_VALID_TO = '{objData.RELATION_DATE_RANGE_VALID_TO}'"
                '    strQuery += $" ,RELATION_DATE_RANGE_IS_APPROX_TO_DATE = {If(objData.RELATION_DATE_RANGE_IS_APPROX_TO_DATE, 1, 0)}"
                '    strQuery += $" ,COMMENTS = '{objData.COMMENTS}'"
                '    strQuery += $" ,GENDER = '{objData.GENDER}'"
                '    strQuery += $" ,TITLE = '{objData.TITLE}'"
                '    strQuery += $" ,FIRST_NAME = '{objData.FIRST_NAME}'"
                '    strQuery += $" ,MIDDLE_NAME = '{objData.MIDDLE_NAME}'"
                '    strQuery += $" ,PREFIX = '{objData.PREFIX}'"
                '    strQuery += $" ,LAST_NAME = '{objData.LAST_NAME}'"
                '    strQuery += $" ,BIRTHDATE = '{objData.BIRTHDATE}'"
                '    strQuery += $" ,BIRTH_PLACE = '{objData.BIRTH_PLACE}'"
                '    strQuery += $" ,COUNTRY_OF_BIRTH = '{objData.COUNTRY_OF_BIRTH}'"
                '    strQuery += $" ,MOTHERS_NAME = '{objData.MOTHERS_NAME}'"
                '    strQuery += $" ,ALIAS = '{objData.ALIAS}'"
                '    strQuery += $" ,FULL_NAME_FRN = '{objData.FULL_NAME_FRN}'"
                '    strQuery += $" ,SSN = '{objData.SSN}'"
                '    strQuery += $" ,PASSPORT_NUMBER = '{objData.PASSPORT_NUMBER}'"
                '    strQuery += $" ,PASSPORT_COUNTRY = '{objData.PASSPORT_COUNTRY}'"
                '    strQuery += $" ,ID_NUMBER = '{objData.ID_NUMBER}'"
                '    strQuery += $" ,NATIONALITY1 = '{objData.NATIONALITY1}'"
                '    strQuery += $" ,NATIONALITY2 = '{objData.NATIONALITY2}'"
                '    strQuery += $" ,NATIONALITY3 = '{objData.NATIONALITY3}'"
                '    strQuery += $" ,RESIDENCE = '{objData.RESIDENCE}'"
                '    strQuery += $" ,RESIDENCE_SINCE = '{objData.RESIDENCE_SINCE}'"
                '    strQuery += $" ,OCCUPATION = '{objData.OCCUPATION}'"
                '    strQuery += $" ,DECEASED = {If(objData.DECEASED, 1, 0)}"
                '    strQuery += $" ,DATE_DECEASED = '{objData.DATE_DECEASED}'"
                '    strQuery += $" ,TAX_NUMBER = '{objData.TAX_NUMBER}'"
                '    strQuery += $" ,TAX_REG_NUMBER = '{objData.TAX_REG_NUMBER}'"
                '    strQuery += $" ,SOURCE_OF_WEALTH = '{objData.SOURCE_OF_WEALTH}'"
                '    strQuery += $" ,IS_PROTECTED = {If(objData.IS_PROTECTED, 1, 0)}"
                '    strQuery += $" where PK_goAML_Ref_Customer_Related_Person_ID = '{objData.PK_goAML_Ref_Customer_Related_Person_ID}';"
                'Else
                strQuery = $"INSERT INTO [dbo].[goAML_Ref_Customer_Related_Person]"
                strQuery += $"           ([CIF]"
                strQuery += $"           ,[PERSON_PERSON_RELATION]"
                'strQuery += $"           ,[RELATION_DATE_RANGE_VALID_FROM]"
                'strQuery += $"           ,[RELATION_DATE_RANGE_IS_APPROX_FROM_DATE]"
                'strQuery += $"           ,[RELATION_DATE_RANGE_VALID_TO]"
                'strQuery += $"           ,[RELATION_DATE_RANGE_IS_APPROX_TO_DATE]"
                strQuery += $"           ,[COMMENTS]"
                strQuery += $"           ,[GENDER]"
                strQuery += $"           ,[TITLE]"
                'strQuery += $"           ,[FIRST_NAME]"
                'strQuery += $"           ,[MIDDLE_NAME]"
                'strQuery += $"           ,[PREFIX]"
                strQuery += $"           ,[LAST_NAME]"
                If objData.BIRTHDATE IsNot Nothing Then
                    If Not objData.BIRTHDATE.ToString().Contains("0001") Then
                        strQuery += $"           ,[BIRTHDATE]"
                    End If
                End If
                strQuery += $"           ,[BIRTH_PLACE]"
                'strQuery += $"           ,[COUNTRY_OF_BIRTH]"
                strQuery += $"           ,[MOTHERS_NAME]"
                strQuery += $"           ,[ALIAS]"
                'strQuery += $"           ,[FULL_NAME_FRN]"
                strQuery += $"           ,[SSN]"
                strQuery += $"           ,[PASSPORT_NUMBER]"
                strQuery += $"           ,[PASSPORT_COUNTRY]"
                strQuery += $"           ,[ID_NUMBER]"
                strQuery += $"           ,[NATIONALITY1]"
                strQuery += $"           ,[NATIONALITY2]"
                strQuery += $"           ,[NATIONALITY3]"
                strQuery += $"           ,[RESIDENCE]"
                'strQuery += $"           ,[RESIDENCE_SINCE]"
                strQuery += $"           ,[OCCUPATION]"
                strQuery += $"           ,[DECEASED]"
                If objData.DATE_DECEASED IsNot Nothing Then
                    If Not objData.DATE_DECEASED.ToString().Contains("0001") Then
                        strQuery += $"           ,[DATE_DECEASED]"
                    End If
                End If
                strQuery += $"           ,[TAX_NUMBER]"
                strQuery += $"           ,[TAX_REG_NUMBER]"
                strQuery += $"           ,[SOURCE_OF_WEALTH]"
                strQuery += $"           ,[IS_PROTECTED])"
                strQuery += $"     VALUES"
                strQuery += $"           ('{objData.CIF}'"
                strQuery += $"           ,'{objData.PERSON_PERSON_RELATION}'"
                'strQuery += $"           ,'{objData.RELATION_DATE_RANGE_VALID_FROM}'"
                'strQuery += $"           ,{If(objData.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE, 1, 0)}"
                'strQuery += $"           ,'{objData.RELATION_DATE_RANGE_VALID_TO}'"
                'strQuery += $"           ,{If(objData.RELATION_DATE_RANGE_IS_APPROX_TO_DATE, 1, 0)}"
                strQuery += $"           ,'{objData.COMMENTS}'"
                strQuery += $"           ,'{objData.GENDER}'"
                strQuery += $"           ,'{objData.TITLE}'"
                'strQuery += $"           ,'{objData.FIRST_NAME}'"
                'strQuery += $"           ,'{objData.MIDDLE_NAME}'"
                'strQuery += $"           ,'{objData.PREFIX}'"
                strQuery += $"           ,'{objData.LAST_NAME}'"
                If objData.BIRTHDATE IsNot Nothing Then
                    If Not objData.BIRTHDATE.ToString().Contains("0001") Then
                        strQuery += $"           ,'{objData.BIRTHDATE}'"
                    End If
                End If
                strQuery += $"           ,'{objData.BIRTH_PLACE}'"
                'strQuery += $"           ,'{objData.COUNTRY_OF_BIRTH}'"
                strQuery += $"           ,'{objData.MOTHERS_NAME}'"
                strQuery += $"           ,'{objData.ALIAS}'"
                'strQuery += $"           ,'{objData.FULL_NAME_FRN}'"
                strQuery += $"           ,'{objData.SSN}'"
                strQuery += $"           ,'{objData.PASSPORT_NUMBER}'"
                strQuery += $"           ,'{objData.PASSPORT_COUNTRY}'"
                strQuery += $"           ,'{objData.ID_NUMBER}'"
                strQuery += $"           ,'{objData.NATIONALITY1}'"
                strQuery += $"           ,'{objData.NATIONALITY2}'"
                strQuery += $"           ,'{objData.NATIONALITY3}'"
                strQuery += $"           ,'{objData.RESIDENCE}'"
                'strQuery += $"           ,'{objData.RESIDENCE_SINCE}'"
                strQuery += $"           ,'{objData.OCCUPATION}'"
                strQuery += $"           ,{If(objData.DECEASED, 1, 0)}"
                If objData.DATE_DECEASED IsNot Nothing Then
                    If Not objData.DATE_DECEASED.ToString().Contains("0001") Then
                        strQuery += $"           ,'{objData.DATE_DECEASED}'"
                    End If
                End If
                strQuery += $"           ,'{objData.TAX_NUMBER}'"
                strQuery += $"           ,'{objData.TAX_REG_NUMBER}'"
                strQuery += $"           ,'{objData.SOURCE_OF_WEALTH}'"
                strQuery += $"           ,{If(objData.IS_PROTECTED, 1, 0)});"
                'End If
                strQuery += " SELECT SCOPE_IDENTITY() AS NewID"
                'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                'FIXME
                Dim row = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Dim PK_goAML_Ref_Customer_Related_Person_ID = row("NewID")

                Dim issuedDate, expiryDate As Nullable(Of Date)
                If objData.ObjList_GoAML_Person_Identification IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Person_Identification
                        ObjGrid.FK_Person_ID = Convert.ToInt32(PK_goAML_Ref_Customer_Related_Person_ID)
                        ' 2023-09-29, Nael: Update FK_Person_Type = 31
                        ObjGrid.FK_Person_Type = 31
                        ObjGrid.FK_REF_DETAIL_OF = 31
                        ' 2023-10-04, Nael: IsCustomer harusnya true, karena dia customer
                        ObjGrid.IsCustomer = True
                        issuedDate = ObjGrid.Issue_Date
                        expiryDate = ObjGrid.Expiry_Date
                        context.goAML_Person_Identification.Save(ObjGrid)
                    Next
                    '2023-09-22, Nael: bulk insert goAML_Person_Identification
                    'context.goAML_Person_Identification.S
                End If

                If objData.ObjList_GoAML_Ref_Address IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Address
                        ObjGrid.FK_To_Table_ID = Convert.ToInt32(PK_goAML_Ref_Customer_Related_Person_ID)
                        ObjGrid.FK_Ref_Detail_Of = 31
                        ' 2023-10-04, Nael: IsCustomer harusnya true, karena dia customer
                        ObjGrid.IsCustomer = True
                        context.goAML_Ref_Address.Save(ObjGrid)
                    Next
                End If

                If objData.ObjList_GoAML_Ref_Phone IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Phone
                        ObjGrid.FK_for_Table_ID = Convert.ToInt32(PK_goAML_Ref_Customer_Related_Person_ID)
                        ObjGrid.FK_Ref_Detail_Of = 31
                        ' 2023-10-04, Nael: IsCustomer harusnya true, karena dia customer
                        ObjGrid.IsCustomer = True
                        context.goAML_Ref_Phone.Save(ObjGrid)
                    Next
                End If

                If objData.ObjList_GoAML_Ref_Address_Work IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Address_Work
                        ObjGrid.FK_To_Table_ID = Convert.ToInt32(PK_goAML_Ref_Customer_Related_Person_ID)
                        ObjGrid.FK_Ref_Detail_Of = 32
                        ' 2023-10-04, Nael: IsCustomer harusnya true, karena dia customer
                        ObjGrid.IsCustomer = True
                        context.goAML_Ref_Address.Save(ObjGrid)
                    Next
                End If

                If objData.ObjList_GoAML_Ref_Phone_Work IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Phone_Work
                        ObjGrid.FK_for_Table_ID = Convert.ToInt32(PK_goAML_Ref_Customer_Related_Person_ID)
                        ObjGrid.FK_Ref_Detail_Of = 32
                        ' 2023-10-04, Nael: IsCustomer harusnya true, karena dia customer
                        ObjGrid.IsCustomer = True
                        context.goAML_Ref_Phone.Save(ObjGrid)
                    Next

                End If

                If objData.ObjList_GoAML_Ref_Customer_Email IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_Email
                        ObjGrid.CIF = PK_goAML_Ref_Customer_Related_Person_ID
                        ObjGrid.FK_REF_DETAIL_OF = 31
                        context.goAML_Ref_Customer_Email.Save(ObjGrid)
                    Next
                End If

                If objData.ObjList_GoAML_Ref_Customer_Sanction IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_Sanction
                        ' 2023-10-03, Nael: PK goAML Ref Customer harusnya dimasukin ke FK_FOR_TABLE_ID bukannya ke CIF
                        'ObjGrid.CIF = PK_goAML_Ref_Customer_Related_Person_ID
                        ObjGrid.FK_FOR_TABLE_ID = PK_goAML_Ref_Customer_Related_Person_ID
                        ObjGrid.FK_REF_DETAIL_OF = 31
                        context.goAML_Ref_Customer_Sanction.Save(ObjGrid)
                    Next
                End If

                If objData.ObjList_GoAML_Ref_Customer_PEP IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_PEP
                        ' 2023-10-03, Nael: PK goAML Ref Customer harusnya dimasukin ke FK_FOR_TABLE_ID bukannya ke CIF
                        'ObjGrid.CIF = PK_goAML_Ref_Customer_Related_Person_ID
                        ObjGrid.FK_FOR_TABLE_ID = PK_goAML_Ref_Customer_Related_Person_ID
                        ObjGrid.FK_REF_DETAIL_OF = 31
                        context.goAML_Ref_Customer_PEP.Save(ObjGrid)
                    Next
                End If
            Next

            ' 2023-09-29, Nael: Menambahkan insert ke AuditTrail
            'Using objDba As GoAMLEntities
            '    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
            '    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            '    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
            '    Dim modulename As String = objModule.ModuleLabel
            '    Dim ObjAuditTrailHeader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
            '    NawaFramework.CreateAuditTrailDetailAdd(objDba, ObjAuditTrailHeader.PK_AuditTrail_ID, Data.AMLJudgementHeader)
            '    objDB.SaveChanges()
            'End Using

        Catch ex As Exception

        End Try
    End Sub

    'goAML_Ref_Customer_Additional_Information
    Public Shared Sub InsertUpdate_AdditionalInformation(objList As List(Of GoAMLBLL.goAML_CustomerDataBLL.goAML_Ref_Customer_Additional_Information))
        Try
            Dim strQuery As String = ""
            For Each objData In objList
                'If objData.PK_goAML_Ref_Customer_Additional_Information_ID > 0 Then
                '    strQuery = $"update goAML_Ref_Customer_Additional_Information"
                '    strQuery += $" set"
                '    strQuery += $" CIF =  '{objData.CIF}'"
                '    strQuery += $" ,INFO_TYPE = '{objData.INFO_TYPE}'"
                '    strQuery += $" ,INFO_SUBJECT = '{objData.INFO_SUBJECT}'"
                '    strQuery += $" ,INFO_TEXT = '{objData.INFO_TEXT}'"
                '    strQuery += $" ,INFO_NUMERIC = {objData.INFO_NUMERIC}"
                '    strQuery += $" ,INFO_DATE = '{objData.INFO_DATE}'"
                '    strQuery += $" ,INFO_BOOLEAN = {If(objData.INFO_BOOLEAN, 1, 0)}"
                '    strQuery += $" where PK_goAML_Ref_Customer_Additional_Information_ID = '{objData.PK_goAML_Ref_Customer_Additional_Information_ID}';"
                'Else
                strQuery = $"INSERT INTO goAML_Ref_Customer_Additional_Information"
                strQuery += $"           ([CIF]"
                strQuery += $"           ,[INFO_TYPE]"
                strQuery += $"           ,[INFO_SUBJECT]"
                strQuery += $"           ,[INFO_TEXT]"
                strQuery += $"           ,[INFO_NUMERIC]"
                strQuery += $"           ,[INFO_DATE]"
                strQuery += $"           ,[INFO_BOOLEAN])"
                strQuery += $"     VALUES"
                strQuery += $"           ('{objData.CIF}'"
                strQuery += $"           ,'{objData.INFO_TYPE}'"
                strQuery += $"           ,'{objData.INFO_SUBJECT}'"
                strQuery += $"           ,'{objData.INFO_TEXT}'"
                strQuery += $"           ,{objData.INFO_NUMERIC}"
                strQuery += $"           ,'{objData.INFO_DATE}'"
                strQuery += $"           ,{If(objData.INFO_BOOLEAN, 1, 0)})"
                'End If


                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
        Catch ex As Exception

        End Try
    End Sub

    'goAML_Ref_Customer_URL
    ' FIXME: ternata tidak keinsert ke tabel goAML_Ref_Customer_Related_Person untuk di dev
    Public Shared Sub InsertUpdate_CustomerURL(objList As List(Of DataModel.goAML_Ref_Customer_URL), Optional headerID As Integer = -1)
        Try
            Dim strQuery As String = ""
            For Each objData In objList
                'If objData.PK_goAML_Ref_Customer_URL_ID > 0 Then
                '    strQuery = $"update goAML_Ref_Customer_URL"
                '    strQuery += $" set"
                '    strQuery += $" CIF =  '{objData.CIF}'"
                '    strQuery += $" ,URL = '{objData.URL}'"
                '    strQuery += $" where PK_goAML_Ref_Customer_URL_ID = '{objData.PK_goAML_Ref_Customer_URL_ID}';"
                'Else
                strQuery = $"INSERT INTO goAML_Ref_Customer_URL"
                strQuery += $"           ([CIF]"
                strQuery += $"           ,[FK_REF_DETAIL_OF]"
                strQuery += $"           ,[FK_FOR_TABLE_ID]"
                strQuery += $"           ,[URL])"
                strQuery += $"     VALUES"
                strQuery += $"           ('{objData.CIF}'"
                strQuery += $"           ,'{1}'"
                strQuery += $"           ,'{headerID}'"
                strQuery += $"           ,'{objData.URL}')"
                'End If

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub InsertUpdate_Director(objList As List(Of DataModel.goAML_Ref_Entity_Director))
        Try
            If objList IsNot Nothing Then

                ' 2023-10-06, Nael: Memanggil fungsi save dari goAML_Ref_Customer_Entity_Director_DAL juga untuk get PK terbaru (SELECT SCOPE)
                Dim entityDirectorDAL As New goAML_Ref_Customer_Entity_Director_DAL()

                Dim context = New DataContext(AmlModule.Customer)

                For Each objData In objList
                    ' 2023-10-06, Nael: save data objData dan get PK nya
                    Dim FK_FOR_TABLE_ID As Integer = entityDirectorDAL.Save(objData)
                    Dim where_director = $"FK_FOR_TABLE_ID = '{objData.PK_goAML_Ref_Customer_Entity_Director_ID}' AND FK_REF_DETAIL_OF = 6"

                    context.goAML_Ref_Customer_Email.Delete(where_director)
                    For Each objDataDetail In objData.ObjList_GoAML_Ref_Customer_Email
                        objDataDetail.FK_REF_DETAIL_OF = 6
                        objDataDetail.CIF = objData.PK_goAML_Ref_Customer_Entity_Director_ID
                        objDataDetail.FK_FOR_TABLE_ID = FK_FOR_TABLE_ID
                        context.goAML_Ref_Customer_Email.Save(objDataDetail)
                    Next

                    context.goAML_Ref_Customer_Sanction.Delete(where_director)
                    For Each objDataDetail In objData.ObjList_GoAML_Ref_Customer_Sanction
                        objDataDetail.FK_REF_DETAIL_OF = 6
                        objDataDetail.FK_FOR_TABLE_ID = FK_FOR_TABLE_ID
                        context.goAML_Ref_Customer_Sanction.Save(objDataDetail)
                    Next

                    context.goAML_Ref_Customer_PEP.Delete(where_director)
                    For Each objDataDetail In objData.ObjList_GoAML_Ref_Customer_PEP
                        objDataDetail.FK_REF_DETAIL_OF = 6
                        objDataDetail.FK_FOR_TABLE_ID = FK_FOR_TABLE_ID
                        objDataDetail.CIF = objData.PK_goAML_Ref_Customer_Entity_Director_ID
                        context.goAML_Ref_Customer_PEP.Save(objDataDetail)
                    Next
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub

    'goAML_Ref_Customer_Related_Entities
    Public Shared Sub InsertUpdate_RelatedEntities(objList As List(Of DataModel.goAML_Ref_Customer_Related_Entities))
        Try
            Dim context = New DataContext(AmlModule.Customer)

            Dim strQuery As String = ""
            For Each objData In objList
                'If objData.PK_goAML_Ref_Customer_Related_Entities_ID > 0 Then
                '    strQuery = $"update goAML_Ref_Customer_Related_Entities"
                '    strQuery += $" set"
                '    strQuery += $" CIF =  '{objData.CIF}'"
                '    strQuery += $" ,ENTITY_ENTITY_RELATION = '{objData.ENTITY_ENTITY_RELATION}'"
                '    strQuery += $" ,RELATION_DATE_RANGE_VALID_FROM = '{objData.RELATION_DATE_RANGE_VALID_FROM}'"
                '    strQuery += $" ,RELATION_DATE_RANGE_IS_APPROX_FROM_DATE = {If(objData.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE, 1, 0)}"
                '    strQuery += $" ,RELATION_DATE_RANGE_VALID_TO = '{objData.RELATION_DATE_RANGE_VALID_TO}'"
                '    strQuery += $" ,RELATION_DATE_RANGE_IS_APPROX_TO_DATE = {If(objData.RELATION_DATE_RANGE_IS_APPROX_TO_DATE, 1, 0)}"
                '    strQuery += $" ,SHARE_PERCENTAGE = {objData.SHARE_PERCENTAGE}"
                '    strQuery += $" ,COMMENTS = '{objData.COMMENTS}'"
                '    strQuery += $" ,NAME = '{objData.NAME}'"
                '    strQuery += $" ,COMMERCIAL_NAME = '{objData.COMMERCIAL_NAME}'"
                '    strQuery += $" ,INCORPORATION_LEGAL_FORM = '{objData.INCORPORATION_LEGAL_FORM}'"
                '    strQuery += $" ,INCORPORATION_NUMBER = '{objData.INCORPORATION_NUMBER}'"
                '    strQuery += $" ,BUSINESS = '{objData.BUSINESS}'"
                '    strQuery += $" ,ENTITY_STATUS = '{objData.ENTITY_STATUS}'"
                '    strQuery += $" ,ENTITY_STATUS_DATE = '{objData.ENTITY_STATUS_DATE}'"
                '    strQuery += $" ,INCORPORATION_STATE = '{objData.INCORPORATION_STATE}'"
                '    strQuery += $" ,INCORPORATION_COUNTRY_CODE = '{objData.INCORPORATION_COUNTRY_CODE}'"
                '    strQuery += $" ,INCORPORATION_DATE = '{objData.INCORPORATION_DATE}'"
                '    strQuery += $" ,BUSINESS_CLOSED = {If(objData.BUSINESS_CLOSED, 1, 0)}"
                '    strQuery += $" ,DATE_BUSINESS_CLOSED = '{objData.DATE_BUSINESS_CLOSED}'"
                '    strQuery += $" ,TAX_NUMBER = '{objData.TAX_NUMBER}'"
                '    strQuery += $" ,TAX_REG_NUMBER = '{objData.TAX_REG_NUMBER}'"
                '    strQuery += $" where PK_goAML_Ref_Customer_Related_Entities_ID = '{objData.PK_goAML_Ref_Customer_Related_Entities_ID}';"
                'Else
                strQuery = $"INSERT INTO goAML_Ref_Customer_Related_Entities"
                strQuery += $"           ([CIF]"
                strQuery += $"           ,[ENTITY_ENTITY_RELATION]"
                'strQuery += $"           ,[RELATION_DATE_RANGE_VALID_FROM]"
                'strQuery += $"           ,[RELATION_DATE_RANGE_IS_APPROX_FROM_DATE]"
                'strQuery += $"           ,[RELATION_DATE_RANGE_VALID_TO]"
                'strQuery += $"           ,[RELATION_DATE_RANGE_IS_APPROX_TO_DATE]"
                'strQuery += $"           ,[SHARE_PERCENTAGE]"
                strQuery += $"           ,[COMMENTS]"
                strQuery += $"           ,[NAME]"
                strQuery += $"           ,[COMMERCIAL_NAME]"
                strQuery += $"           ,[INCORPORATION_LEGAL_FORM]"
                strQuery += $"           ,[INCORPORATION_NUMBER]"
                strQuery += $"           ,[BUSINESS]"
                'strQuery += $"           ,[ENTITY_STATUS]"
                'strQuery += $"           ,[ENTITY_STATUS_DATE]"
                strQuery += $"           ,[INCORPORATION_STATE]"
                strQuery += $"           ,[INCORPORATION_COUNTRY_CODE]"
                If objData.INCORPORATION_DATE IsNot Nothing Then
                    If Not objData.INCORPORATION_DATE.ToString().Contains("0001") Then
                        strQuery += $"           ,[INCORPORATION_DATE]"
                    End If
                End If
                strQuery += $"           ,[BUSINESS_CLOSED]"
                If objData.DATE_BUSINESS_CLOSED IsNot Nothing Then
                    If Not objData.DATE_BUSINESS_CLOSED.ToString().Contains("0001") Then
                        strQuery += $"           ,[DATE_BUSINESS_CLOSED]"
                    End If
                End If
                strQuery += $"           ,[TAX_NUMBER]"
                strQuery += $"           ,[TAX_REG_NUMBER])"
                strQuery += $"     VALUES"
                strQuery += $"           ('{objData.CIF}'"
                strQuery += $"           ,'{objData.ENTITY_ENTITY_RELATION}'"
                'strQuery += $"           ,'{objData.RELATION_DATE_RANGE_VALID_FROM}'"
                'strQuery += $"           ,{If(objData.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE, 1, 0)}"
                'strQuery += $"           ,'{objData.RELATION_DATE_RANGE_VALID_TO}'"
                'strQuery += $"           ,{If(objData.RELATION_DATE_RANGE_IS_APPROX_TO_DATE, 1, 0)}"
                'strQuery += $"           ,{objData.SHARE_PERCENTAGE}"
                strQuery += $"           ,'{objData.COMMENTS}'"
                strQuery += $"           ,'{objData.NAME}'"
                strQuery += $"           ,'{objData.COMMERCIAL_NAME}'"
                strQuery += $"           ,'{objData.INCORPORATION_LEGAL_FORM}'"
                strQuery += $"           ,'{objData.INCORPORATION_NUMBER}'"
                strQuery += $"           ,'{objData.BUSINESS}'"
                'strQuery += $"           ,'{objData.ENTITY_STATUS}'"
                'strQuery += $"           ,'{objData.ENTITY_STATUS_DATE}'"
                strQuery += $"           ,'{objData.INCORPORATION_STATE}'"
                strQuery += $"           ,'{objData.INCORPORATION_COUNTRY_CODE}'"
                If objData.INCORPORATION_DATE IsNot Nothing Then
                    If Not objData.INCORPORATION_DATE.ToString().Contains("0001") Then
                        strQuery += $"           ,'{objData.INCORPORATION_DATE}'"
                    End If
                End If
                strQuery += $"           ,{If(objData.BUSINESS_CLOSED, 1, 0)}"
                If objData.DATE_BUSINESS_CLOSED IsNot Nothing Then
                    If Not objData.DATE_BUSINESS_CLOSED.ToString().Contains("0001") Then
                        strQuery += $"           ,'{objData.DATE_BUSINESS_CLOSED}'"
                    End If
                End If
                strQuery += $"           ,'{objData.TAX_NUMBER}'"
                strQuery += $"           ,'{objData.TAX_REG_NUMBER}')"
                'End If

                strQuery += " SELECT SCOPE_IDENTITY() AS NewID"
                'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Dim row = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Dim PK_ID As Integer = row("NewID")

                If objData.ObjList_GoAML_Ref_Customer_Phone IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_Phone
                        ObjGrid.FK_for_Table_ID = PK_ID
                        ObjGrid.FK_Ref_Detail_Of = 36
                        context.goAML_Ref_Phone.Save(ObjGrid)
                    Next

                End If

                If objData.ObjList_GoAML_Ref_Customer_Address IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_Address
                        ObjGrid.FK_To_Table_ID = PK_ID
                        ObjGrid.FK_Ref_Detail_Of = 36
                        context.goAML_Ref_Address.Save(ObjGrid)
                    Next

                End If

                If objData.ObjList_GoAML_Ref_Customer_Email IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_Email
                        ObjGrid.CIF = PK_ID
                        ObjGrid.FK_REF_DETAIL_OF = 36
                        context.goAML_Ref_Customer_Email.Save(ObjGrid)
                    Next

                End If

                If objData.ObjList_GoAML_Ref_Customer_Sanction IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_Sanction
                        ObjGrid.CIF = PK_ID
                        ObjGrid.FK_REF_DETAIL_OF = 36
                        context.goAML_Ref_Customer_Sanction.Save(ObjGrid)
                    Next

                End If

                If objData.ObjList_GoAML_Ref_Customer_Entity_Identification IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_Entity_Identification
                        ObjGrid.CIF = PK_ID
                        ObjGrid.FK_REF_DETAIL_OF = 36
                        context.goAML_Ref_Customer_Entity_Identification.Save(ObjGrid)
                    Next

                End If

                If objData.ObjList_GoAML_Ref_Customer_URL IsNot Nothing Then
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_URL
                        ObjGrid.CIF = PK_ID
                        ObjGrid.FK_REF_DETAIL_OF = 36
                        context.goAML_Ref_Customer_URL.Save(ObjGrid)
                    Next

                End If

                'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Function GetReferenceByCode(Table As String, Kode As String) As goAML_CustomerDataBLL.General_Reference

        Dim query = "SELECT TOP 1 * From " & Table & " WHERE Kode = '" & Kode & "'"
        Dim result = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, query)

        If result IsNot Nothing Then
            Dim objResult = New General_Reference
            objResult.Kode = result("Kode").ToString()
            objResult.Keterangan = result("Keterangan").ToString()

            Return objResult
        End If

        Return Nothing
    End Function
#End Region
End Class
