﻿'==========================================
'Created Date   : 22 Sep 2021
'Created By     : NawaData
'Description    : Library for goAML Report
'                 
'==========================================

Imports System.Data.SqlClient
'Imports goAMLDAL
Imports GoAMLDAL

Public Class GoAML_Report_BLL

    'Enum Identification Person Type : fungsi utama untuk membedakan terutama Person Identification
    Public Enum enumIdentificationPersonType
        Person = 1
        Signatory = 2
        Conductor = 3
        Director = 4
        DirectorAccount = 5
        DirectorEntity = 6
        DirectorWIC = 7
        WIC = 8
        EntityAccount = 9
        RelatedPersonTrnAccountSignatory = 10
        RelatedPersonTrnPerson = 11
        RelatedPersonTrnMultiAccountSignatory = 12
        RelatedPersonTrnMultiPerson = 13
        RelatedPersonActivityAccountSignatory = 14
        RelatedPersonActivityPerson = 15
        RelatedEntityTrnAccountEntityAccount = 16
        RelatedEntityTrnEntity = 17
        RelatedEntityTrnMultiAccountEntityAccount = 18
        RelatedEntityTrnMultiEntity = 19
        RelatedEntityActivityAccountEntityAccount = 20
        RelatedEntityActivityEntity = 21
        RelatedAccountMultipartyAccount = 22
        RelatedAccountActivityAccount = 23

    End Enum

    'Enum SenderInformation : fungsi utama untuk membedakan terutama untuk Entity Director (antara Account Entity Director dan Entity Director 1 table yang sama)
    Public Enum enumSenderInformation
        Account = 1
        Person = 2
        Entity = 3
    End Enum

    Enum enumActionApproval
        Accept = 1
        Reject = 2
        Request = 3
    End Enum

    Enum enumActionForm
        Add = 1
        Edit = 2
        Delete = 3
        Approval = 4
    End Enum

    Shared Function getDirectorAccountPersonType() As Integer
        Try
            Dim intDirectorAccountPersonType As Integer = GoAML_Global_BLL.getGlobalParameterValueByID(3001)
            If String.IsNullOrWhiteSpace(intDirectorAccountPersonType) Then
                intDirectorAccountPersonType = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount
            End If

            Return intDirectorAccountPersonType
        Catch ex As Exception
            Throw ex
            Return GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount
        End Try
    End Function

    Shared Function getTRNorACT(strReportCode As String) As String
        Try
            Dim strResult As String = "TRN"
            Dim drTemp As DataRow = GoAML_Global_BLL.getDataRowByID("goAML_odm_ref_Report_TRNorACT", "ReportType", strReportCode)
            If drTemp IsNot Nothing Then
                If Not IsDBNull(drTemp("TRNorACT")) Then
                    strResult = drTemp("TRNorACT")
                End If
            End If

            Return strResult
        Catch ex As Exception
            Throw ex
            Return "TRN"
        End Try
    End Function

    Shared Function isUseIndicator(strReportCode As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim drTemp As DataRow = GoAML_Global_BLL.getDataRowByID("GoAML_Laporan_With_Indicator", "Kode", strReportCode)
            If drTemp IsNot Nothing Then
                bolResult = True
            End If

            Return bolResult
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Shared Function isRequireIndicator(strReportCode As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim drTemp As DataRow = GoAML_Global_BLL.getDataRowByID("GoAML_Laporan_With_Indicator", "Kode", strReportCode)
            If drTemp IsNot Nothing Then
                If Not IsDBNull(drTemp("ReportType")) Then
                    If drTemp("ReportType").ToString.Contains("STR") Then
                        bolResult = True
                    End If
                End If
            End If

            Return bolResult
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Shared Function getValidationFromType(transmodeCode As String, instrumenFrom As String, SenderInformation As Integer, myClient As Boolean) As String
        Dim keterangan As String = ""
        Try
            Using objdb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
                If SenderInformation = 1 Then ' Account
                    If myClient Then
                        Dim obj As GoAMLDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Account = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Account MyClient"
                        End If
                    Else
                        Dim obj As GoAMLDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Account = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Account NotMyClient"
                        End If
                    End If

                ElseIf SenderInformation = 2 Then 'Person
                    If myClient Then
                        Dim obj As GoAMLDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Person = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Person MyClient"
                        End If
                    Else
                        Dim obj As GoAMLDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Person = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Person NotMyClient"
                        End If
                    End If

                ElseIf SenderInformation = 3 Then 'Entity
                    If myClient Then
                        Dim obj As GoAMLDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Entity = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Entity MyClient"
                        End If
                    Else
                        Dim obj As GoAMLDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Entity = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Entity NotMyClient"
                        End If
                    End If
                End If
            End Using

            Return keterangan
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function getValidationToType(transmodeCode As String, instrumenFrom As String, instrumenTo As String, PenerimaInformation As Integer, TomyClient As Boolean) As String
        Dim keterangan As String = ""
        Using objdb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            If PenerimaInformation = 1 Then ' Account
                If TomyClient Then
                    Dim obj As GoAMLDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Account = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Account MyClient"
                    End If
                Else
                    Dim obj As GoAMLDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Account = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Account NotMyClient"
                    End If
                End If

            ElseIf PenerimaInformation = 2 Then 'Person
                If TomyClient Then
                    Dim obj As GoAMLDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Person = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Person MyClient"
                    End If
                Else
                    Dim obj As GoAMLDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Person = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Person NotMyClient"
                    End If
                End If

            ElseIf PenerimaInformation = 3 Then 'Entity
                If TomyClient Then
                    Dim obj As GoAMLDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Entity = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Entity MyClient"
                    End If
                Else
                    Dim obj As GoAMLDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Entity = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Entity NotMyClient"
                    End If
                End If

            End If
        End Using

        Return keterangan
    End Function

    Shared Function getValidationReportByReportID(strReportID As String)
        Try
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@reportId"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.Int

            Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_ResponseValidateReportByID", param)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Function getValidationReportByTransactionOrActivityID(strReportID As String, strID As String, strTRNorACT As String) As String
        Try
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.Int

            param(1) = New SqlParameter
            param(1).ParameterName = "@PK_TransactionOrActivity_ID"
            param(1).Value = strID
            param(1).DbType = SqlDbType.Int

            param(2) = New SqlParameter
            param(2).ParameterName = "@TRN_or_ACT"
            param(2).Value = strTRNorACT
            param(2).DbType = SqlDbType.VarChar

            Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GOAML_GetValidationReport_PerTransactionOrActivityID", param)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Function GetGoAMLReportClassByID(ID As Long) As GoAML_Report_Class
        Using objDb As New GoAMLDAL.goAMLEntities

            Dim objGoAMLReportClass = New GoAML_Report_Class
            With objGoAMLReportClass
                'GoAML Report
                .obj_goAML_Report = objDb.goAML_Report.Where(Function(x) x.PK_Report_ID = ID).FirstOrDefault

                'Transaction
                Dim listTransaction = objDb.goAML_Transaction.Where(Function(x) x.FK_Report_ID = ID).ToList
                If listTransaction IsNot Nothing Then
                    .list_goAML_Transaction = listTransaction
                End If

                'Activity
                Dim listActivity = objDb.goAML_Act_ReportPartyType.Where(Function(x) x.FK_Report_ID = ID).ToList
                If listActivity IsNot Nothing Then
                    .list_goAML_Act_ReportPartyType = listActivity
                End If

                'Indicator
                Dim listIndicator = objDb.goAML_Report_Indicator.Where(Function(x) x.FK_Report = ID).ToList
                If listIndicator IsNot Nothing Then
                    .list_goAML_Report_Indicator = listIndicator
                End If
            End With

            Return objGoAMLReportClass
        End Using
    End Function

    Shared Function GetGoAMLTransactionClassByID(ID As Long) As GoAML_Transaction_Class
        Using objDb As New GoAMLDAL.goAMLEntities

            Dim objGoAMLTransactionClass = New GoAML_Transaction_Class
            With objGoAMLTransactionClass
                'GoAML Transaction
                .obj_goAML_Transaction = objDb.goAML_Transaction.Where(Function(x) x.PK_Transaction_ID = ID).FirstOrDefault

                ''Add 02-Oct-2023, Felix 
                'Transaction Address
                Dim objTransactionAddress = objDb.goAML_Transaction_Address.Where(Function(x) x.FK_goAML_Transaction_ID = ID).FirstOrDefault
                If objTransactionAddress IsNot Nothing Then
                    .obj_goAML_Transaction_Address = objTransactionAddress
                End If
                '' End 02-Oct-2023

                If .obj_goAML_Transaction IsNot Nothing Then
                    If .obj_goAML_Transaction.FK_Transaction_Type IsNot Nothing Then
                        If .obj_goAML_Transaction.FK_Transaction_Type = 1 Then      'Transaction Bi-Party
                            'Account
                            Dim listAccount = objDb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = ID).ToList
                            If listAccount IsNot Nothing Then .list_goAML_Transaction_Account.AddRange(listAccount)

                            'Signatory
                            If .list_goAML_Transaction_Account IsNot Nothing AndAlso .list_goAML_Transaction_Account.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Account
                                    Dim listSignatory = objDb.goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = item.PK_Account_ID).ToList
                                    If listSignatory IsNot Nothing Then .list_goAML_Trn_acc_Signatory.AddRange(listSignatory)
                                Next
                            End If

                            'Signatory Address, Phone, Identification
                            If .list_goAML_Trn_acc_Signatory IsNot Nothing AndAlso .list_goAML_Trn_acc_Signatory.Count > 0 Then
                                For Each item In .list_goAML_Trn_acc_Signatory
                                    Dim listAddress = objDb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_acc_Signatory_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_acc_Signatory_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_acc_Signatory_ID And x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Acc_sign_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_acc_sign_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)

                                    '' Add 02-Oct-2023, Felix
                                    Dim listSignEmail = objDb.goAML_Report_Email.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_acc_Signatory_ID And x.Parent_Table_Name = "goAML_Trn_acc_Signatory").ToList
                                    If listSignEmail IsNot Nothing Then .list_goAML_Transaction_Account_Signatory_Email.AddRange(listSignEmail)

                                    Dim listSignSanction = objDb.goAML_Report_Sanction.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_acc_Signatory_ID And x.Parent_Table_Name = "goAML_Trn_acc_Signatory").ToList
                                    If listSignSanction IsNot Nothing Then .list_goAML_Transaction_Account_Signatory_Sanctions.AddRange(listSignSanction)

                                    Dim listSignPep = objDb.goAML_Report_Person_PEP.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_acc_Signatory_ID And x.Parent_Table_Name = "goAML_Trn_acc_Signatory").ToList
                                    If listSignPep IsNot Nothing Then .list_goAML_Transaction_Account_Signatory_PEP.AddRange(listSignPep)

                                    Dim listSignRelatedPerson = objDb.goAML_Report_Related_Person.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_acc_Signatory_ID And x.Parent_Table_Name = "goAML_Trn_acc_Signatory").ToList
                                    If listSignRelatedPerson IsNot Nothing Then .list_goAML_Transaction_Account_Signatory_RelatedPerson.AddRange(listSignRelatedPerson)

                                    If .list_goAML_Transaction_Account_Signatory_RelatedPerson IsNot Nothing AndAlso .list_goAML_Transaction_Account_Signatory_RelatedPerson.Count > 0 Then
                                        For Each itemSignRP In .list_goAML_Transaction_Account_Signatory_RelatedPerson
                                            Dim listSignRPAddress = objDb.goAML_Acc_Signatory_Related_Person_Address.Where(Function(x) x.FK_PARENT_TABLE_ID = itemSignRP.PK_goAML_Report_Related_Person_ID And x.Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person").ToList
                                            Dim listSignRPPhone = objDb.goAML_Acc_Signatory_Related_Person_Phone.Where(Function(x) x.FK_PARENT_TABLE_ID = itemSignRP.PK_goAML_Report_Related_Person_ID And x.PARENT_TABLE_NAME = "goAML_Trn_acc_Signatory_Related_Person").ToList
                                            Dim listSignRPIdentification = objDb.goAML_Acc_Signatory_Related_Person_Identification.Where(Function(x) x.FK_PARENT_TABLE_ID = itemSignRP.PK_goAML_Report_Related_Person_ID And x.PARENT_TABLE_NAME = "goAML_Trn_acc_Signatory_Related_Person").ToList

                                            If listSignRPAddress IsNot Nothing Then .list_goAML_Transaction_Account_Signatory_RP_Address.AddRange(listSignRPAddress)
                                            If listSignRPPhone IsNot Nothing Then .list_goAML_Transaction_Account_Signatory_RP_Phone.AddRange(listSignRPPhone)
                                            If listSignRPIdentification IsNot Nothing Then .list_goAML_Transaction_Account_Signatory_RP_Identification.AddRange(listSignRPIdentification)

                                            Dim listSignRPEmail = objDb.goAML_Report_Email.Where(Function(x) x.FK_Parent_Table_ID = itemSignRP.PK_goAML_Report_Related_Person_ID And x.Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person").ToList
                                            If listSignRPEmail IsNot Nothing Then .list_goAML_Transaction_Account_Signatory_RP_Email.AddRange(listSignRPEmail)

                                            Dim listSignRPSanction = objDb.goAML_Report_Sanction.Where(Function(x) x.FK_Parent_Table_ID = itemSignRP.PK_goAML_Report_Related_Person_ID And x.Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person").ToList
                                            If listSignRPSanction IsNot Nothing Then .list_goAML_Transaction_Account_Signatory_RP_Sanctions.AddRange(listSignRPSanction)

                                            Dim listSignRPPep = objDb.goAML_Report_Person_PEP.Where(Function(x) x.FK_Parent_Table_ID = itemSignRP.PK_goAML_Report_Related_Person_ID And x.Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person").ToList
                                            If listSignRPPep IsNot Nothing Then .list_goAML_Transaction_Account_Signatory_RP_PEP.AddRange(listSignRPPep)

                                        Next
                                    End If
                                    '' End 02-Oct-2023
                                Next
                            End If

                            'Account Entity
                            If .list_goAML_Transaction_Account IsNot Nothing AndAlso .list_goAML_Transaction_Account.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Account
                                    Dim listAccountEntity = objDb.goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = item.PK_Account_ID).ToList
                                    If listAccountEntity IsNot Nothing Then .list_goAML_Trn_Entity_account.AddRange(listAccountEntity)
                                Next
                            End If

                            'Account Entity Address, Phone
                            If .list_goAML_Trn_Entity_account IsNot Nothing AndAlso .list_goAML_Trn_Entity_account.Count > 0 Then
                                For Each item In .list_goAML_Trn_Entity_account
                                    Dim listAddress = objDb.goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_Entity_account).ToList
                                    Dim listPhone = objDb.goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_Entity_account).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Acc_Entity_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_Trn_Acc_Entity_Phone.AddRange(listPhone)

                                    '' Add 02-Oct-2023, Felix
                                    Dim listAccEntEmail = objDb.goAML_Report_Email.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_Entity_account And x.Parent_Table_Name = "goAML_Trn_Entity_Account").ToList
                                    If listAccEntEmail IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_Email.AddRange(listAccEntEmail)

                                    Dim listAccEntIdentification = objDb.goAML_Report_Identification.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_Entity_account And x.Parent_Table_Name = "goAML_Trn_Entity_Account").ToList
                                    If listAccEntIdentification IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_Identification.AddRange(listAccEntIdentification)

                                    Dim listAccEntURL = objDb.goAML_Report_Url.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_Entity_account And x.Parent_Table_Name = "goAML_Trn_Entity_Account").ToList
                                    If listAccEntURL IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_URL.AddRange(listAccEntURL)

                                    Dim listAccEntSanction = objDb.goAML_Report_Sanction.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_Entity_account And x.Parent_Table_Name = "goAML_Trn_Entity_Account").ToList
                                    If listAccEntSanction IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_Sanctions.AddRange(listAccEntSanction)

                                    Dim listAccEntRE = objDb.goAML_Report_Related_Entities.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_Entity_account And x.Parent_Table_Name = "goAML_Trn_Entity_Account").ToList
                                    If listAccEntRE IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_RelatedEntity.AddRange(listAccEntRE)

                                    If .list_goAML_Transaction_Account_EntityAccount_RelatedEntity IsNot Nothing AndAlso .list_goAML_Transaction_Account_EntityAccount_RelatedEntity.Count > 0 Then
                                        For Each itemAccEntRE In .list_goAML_Transaction_Account_EntityAccount_RelatedEntity
                                            Dim listAccEntREAddress = objDb.goAML_Acc_Entity_Related_Entity_Address.Where(Function(x) x.FK_PARENT_TABLE_ID = itemAccEntRE.PK_goAML_Report_Related_Entities_ID And x.Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity").ToList
                                            Dim listAccEntREPhone = objDb.goAML_Acc_Entity_Related_Entity_Phone.Where(Function(x) x.FK_PARENT_TABLE_ID = itemAccEntRE.PK_goAML_Report_Related_Entities_ID And x.PARENT_TABLE_NAME = "goAML_Trn_Entity_Account_Related_Entity").ToList
                                            Dim listAccEntREIdentification = objDb.goAML_Report_Identification.Where(Function(x) x.FK_Parent_Table_ID = itemAccEntRE.PK_goAML_Report_Related_Entities_ID And x.Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity").ToList

                                            If listAccEntREAddress IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_RE_Address.AddRange(listAccEntREAddress)
                                            If listAccEntREPhone IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_RE_Phone.AddRange(listAccEntREPhone)
                                            If listAccEntREIdentification IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_RE_Identification.AddRange(listAccEntREIdentification)

                                            Dim listAccEntREEmail = objDb.goAML_Report_Email.Where(Function(x) x.FK_Parent_Table_ID = itemAccEntRE.PK_goAML_Report_Related_Entities_ID And x.Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity").ToList
                                            If listAccEntREEmail IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_RE_Email.AddRange(listAccEntREEmail)

                                            Dim listAccEntRESanction = objDb.goAML_Report_Sanction.Where(Function(x) x.FK_Parent_Table_ID = itemAccEntRE.PK_goAML_Report_Related_Entities_ID And x.Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity").ToList
                                            If listAccEntRESanction IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_RE_Sanctions.AddRange(listAccEntRESanction)

                                            Dim listAccEntREUrl = objDb.goAML_Report_Url.Where(Function(x) x.FK_Parent_Table_ID = itemAccEntRE.PK_goAML_Report_Related_Entities_ID And x.Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity").ToList
                                            If listAccEntREUrl IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_RE_URL.AddRange(listAccEntREUrl)

                                        Next
                                    End If
                                    '' end 02-Oct-2023
                                Next
                            End If

                            'Account Entity Director
                            If .list_goAML_Trn_Entity_account IsNot Nothing AndAlso .list_goAML_Trn_Entity_account.Count > 0 Then
                                For Each item In .list_goAML_Trn_Entity_account
                                    Dim listDirector = objDb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = item.PK_goAML_Trn_Entity_account And x.FK_Sender_Information = GoAMLDAL.GoAMLEnum.enumSenderInformation.Account).ToList
                                    If listDirector IsNot Nothing Then .list_goAML_Trn_Director.AddRange(listDirector)
                                Next
                            End If

                            'Account Entity Director Address, Phone, Identification
                            Dim listAccountEntityDirector = .list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = GoAMLDAL.GoAMLEnum.enumSenderInformation.Account).ToList
                            If listAccountEntityDirector IsNot Nothing AndAlso listAccountEntityDirector.Count > 0 Then
                                For Each item In listAccountEntityDirector
                                    Dim listAddress = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listIdentification As List(Of GoAMLDAL.goAML_Transaction_Person_Identification) = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_Director_ID And x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Director_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Director_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)

                                    '' Add 02-Oct-2023, Felix
                                    Dim listAccEntDirEmail = objDb.goAML_Report_Email.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_Director_ID And x.Parent_Table_Name = "goAML_Trn_Entity_Account_Director").ToList
                                    If listAccEntDirEmail IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_Director_Email.AddRange(listAccEntDirEmail)

                                    Dim listAccEntDirSanction = objDb.goAML_Report_Sanction.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_Director_ID And x.Parent_Table_Name = "goAML_Trn_Entity_Account_Director").ToList
                                    If listAccEntDirSanction IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_Director_Sanctions.AddRange(listAccEntDirSanction)

                                    Dim listAccEntDirPep = objDb.goAML_Report_Person_PEP.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_Director_ID And x.Parent_Table_Name = "goAML_Trn_Entity_Account_Director").ToList
                                    If listAccEntDirPep IsNot Nothing Then .list_goAML_Transaction_Account_EntityAccount_Director_PEP.AddRange(listAccEntDirPep)
                                    '' End 02-Oct-2023
                                Next
                            End If

                            'Person
                            Dim listPerson = objDb.goAML_Transaction_Person.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listPerson IsNot Nothing Then .list_goAML_Transaction_Person.AddRange(listPerson)

                            'Person Address, Phone, Identification
                            If .list_goAML_Transaction_Person IsNot Nothing AndAlso .list_goAML_Transaction_Person.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Person
                                    Dim listAddress = objDb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = item.PK_Person_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = item.PK_Person_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Person_ID And x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Person).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Person_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Person_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)

                                    '' Add 02-Oct-2023, Felix
                                    Dim listPersonEmail = objDb.goAML_Report_Email.Where(Function(x) x.FK_Parent_Table_ID = item.PK_Person_ID And x.Parent_Table_Name = "goAML_Trn_Person").ToList
                                    If listPersonEmail IsNot Nothing Then .list_goAML_Transaction_Person_Email.AddRange(listPersonEmail)

                                    Dim listPersonSanction = objDb.goAML_Report_Sanction.Where(Function(x) x.FK_Parent_Table_ID = item.PK_Person_ID And x.Parent_Table_Name = "goAML_Trn_Person").ToList
                                    If listPersonSanction IsNot Nothing Then .list_goAML_Transaction_Person_Sanctions.AddRange(listPersonSanction)

                                    Dim listPersonPep = objDb.goAML_Report_Person_PEP.Where(Function(x) x.FK_Parent_Table_ID = item.PK_Person_ID And x.Parent_Table_Name = "goAML_Trn_Person").ToList
                                    If listPersonPep IsNot Nothing Then .list_goAML_Transaction_Person_PEP.AddRange(listPersonPep)

                                    Dim listPersonRelatedPerson = objDb.goAML_Report_Related_Person.Where(Function(x) x.FK_Parent_Table_ID = item.PK_Person_ID And x.Parent_Table_Name = "goAML_Trn_Person").ToList
                                    If listPersonRelatedPerson IsNot Nothing Then .list_goAML_Transaction_Person_RelatedPerson.AddRange(listPersonRelatedPerson)

                                    If .list_goAML_Transaction_Person_RelatedPerson IsNot Nothing AndAlso .list_goAML_Transaction_Person_RelatedPerson.Count > 0 Then
                                        For Each itemPersonRP In .list_goAML_Transaction_Person_RelatedPerson
                                            Dim listPersonRPAddress = objDb.goAML_Report_Person_Related_Person_Address.Where(Function(x) x.FK_PARENT_TABLE_ID = itemPersonRP.PK_goAML_Report_Related_Person_ID And x.Parent_Table_Name = "goAML_Trn_Person_Related_Person").ToList
                                            Dim listPersonRPPhone = objDb.goAML_Report_Person_Related_Person_Phone.Where(Function(x) x.FK_PARENT_TABLE_ID = itemPersonRP.PK_goAML_Report_Related_Person_ID And x.PARENT_TABLE_NAME = "goAML_Trn_Person_Related_Person").ToList
                                            Dim listPersonRPIdentification = objDb.goAML_Report_Person_Related_Person_Identification.Where(Function(x) x.FK_PARENT_TABLE_ID = itemPersonRP.PK_goAML_Report_Related_Person_ID And x.PARENT_TABLE_NAME = "goAML_Trn_Person_Related_Person").ToList

                                            If listPersonRPAddress IsNot Nothing Then .list_goAML_Transaction_Person_RP_Address.AddRange(listPersonRPAddress)
                                            If listPersonRPPhone IsNot Nothing Then .list_goAML_Transaction_Person_RP_Phone.AddRange(listPersonRPPhone)
                                            If listPersonRPIdentification IsNot Nothing Then .list_goAML_Transaction_Person_RP_Identification.AddRange(listPersonRPIdentification)

                                            Dim listPersonRPEmail = objDb.goAML_Report_Email.Where(Function(x) x.FK_Parent_Table_ID = itemPersonRP.PK_goAML_Report_Related_Person_ID And x.Parent_Table_Name = "goAML_Trn_Person_Related_Person").ToList
                                            If listPersonRPEmail IsNot Nothing Then .list_goAML_Transaction_Person_RP_Email.AddRange(listPersonRPEmail)

                                            Dim listPersonRPSanction = objDb.goAML_Report_Sanction.Where(Function(x) x.FK_Parent_Table_ID = itemPersonRP.PK_goAML_Report_Related_Person_ID And x.Parent_Table_Name = "goAML_Trn_Person_Related_Person").ToList
                                            If listPersonRPSanction IsNot Nothing Then .list_goAML_Transaction_Person_RP_Sanctions.AddRange(listPersonRPSanction)

                                            Dim listPersonRPPep = objDb.goAML_Report_Person_PEP.Where(Function(x) x.FK_Parent_Table_ID = itemPersonRP.PK_goAML_Report_Related_Person_ID And x.Parent_Table_Name = "goAML_Trn_Person_Related_Person").ToList
                                            If listPersonRPPep IsNot Nothing Then .list_goAML_Transaction_Person_RP_PEP.AddRange(listPersonRPPep)

                                        Next
                                    End If
                                    '' End 02-Oct-2023
                                Next
                            End If

                            'Entity
                            Dim listEntity = objDb.goAML_Transaction_Entity.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listEntity IsNot Nothing Then .list_goAML_Transaction_Entity.AddRange(listEntity)

                            'Entity Address, Phone
                            If .list_goAML_Transaction_Entity IsNot Nothing AndAlso .list_goAML_Transaction_Entity.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Entity
                                    Dim listAddress = objDb.goAML_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = item.PK_Entity_ID).ToList
                                    Dim listPhone = objDb.goAML_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = item.PK_Entity_ID).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Entity_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_Trn_Entity_Phone.AddRange(listPhone)

                                    '' Add 02-Oct-2023, Felix
                                    Dim listEntEmail = objDb.goAML_Report_Email.Where(Function(x) x.FK_Parent_Table_ID = item.PK_Entity_ID And x.Parent_Table_Name = "goAML_Transaction_Entity").ToList
                                    If listEntEmail IsNot Nothing Then .list_goAML_Transaction_Entity_Email.AddRange(listEntEmail)

                                    Dim listEntIdentification = objDb.goAML_Report_Identification.Where(Function(x) x.FK_Parent_Table_ID = item.PK_Entity_ID And x.Parent_Table_Name = "goAML_Transaction_Entity").ToList
                                    If listEntIdentification IsNot Nothing Then .list_goAML_Transaction_Entity_Identification.AddRange(listEntIdentification)

                                    Dim listEntURL = objDb.goAML_Report_Url.Where(Function(x) x.FK_Parent_Table_ID = item.PK_Entity_ID And x.Parent_Table_Name = "goAML_Transaction_Entity").ToList
                                    If listEntURL IsNot Nothing Then .list_goAML_Transaction_Entity_URL.AddRange(listEntURL)

                                    Dim listEntSanction = objDb.goAML_Report_Sanction.Where(Function(x) x.FK_Parent_Table_ID = item.PK_Entity_ID And x.Parent_Table_Name = "goAML_Transaction_Entity").ToList
                                    If listEntSanction IsNot Nothing Then .list_goAML_Transaction_Entity_Sanctions.AddRange(listEntSanction)

                                    Dim listEntRE = objDb.goAML_Report_Related_Entities.Where(Function(x) x.FK_Parent_Table_ID = item.PK_Entity_ID And x.Parent_Table_Name = "goAML_Transaction_Entity").ToList
                                    If listEntRE IsNot Nothing Then .list_goAML_Transaction_Entity_RelatedEntity.AddRange(listEntRE)

                                    If .list_goAML_Transaction_Entity_RelatedEntity IsNot Nothing AndAlso .list_goAML_Transaction_Entity_RelatedEntity.Count > 0 Then
                                        For Each itemEntRE In .list_goAML_Transaction_Entity_RelatedEntity
                                            Dim listEntREAddress = objDb.goAML_Report_Entity_Related_Entity_Address.Where(Function(x) x.FK_PARENT_TABLE_ID = itemEntRE.PK_goAML_Report_Related_Entities_ID And x.Parent_Table_Name = "goAML_Transaction_Entity_Related_Entity").ToList
                                            Dim listEntREPhone = objDb.goAML_Report_Entity_Related_Entity_Phone.Where(Function(x) x.FK_PARENT_TABLE_ID = itemEntRE.PK_goAML_Report_Related_Entities_ID And x.PARENT_TABLE_NAME = "goAML_Transaction_Entity_Related_Entity").ToList
                                            Dim listEntREIdentification = objDb.goAML_Report_Identification.Where(Function(x) x.FK_Parent_Table_ID = itemEntRE.PK_goAML_Report_Related_Entities_ID And x.Parent_Table_Name = "goAML_Transaction_Entity_Related_Entity").ToList

                                            If listEntREAddress IsNot Nothing Then .list_goAML_Transaction_EntityAccount_RE_Address.AddRange(listEntREAddress)
                                            If listEntREPhone IsNot Nothing Then .list_goAML_Transaction_EntityAccount_RE_Phone.AddRange(listEntREPhone)
                                            If listEntREIdentification IsNot Nothing Then .list_goAML_Transaction_EntityAccount_RE_Identification.AddRange(listEntREIdentification)

                                            Dim listEntREEmail = objDb.goAML_Report_Email.Where(Function(x) x.FK_Parent_Table_ID = itemEntRE.PK_goAML_Report_Related_Entities_ID And x.Parent_Table_Name = "goAML_Transaction_Entity_Related_Entity").ToList
                                            If listEntREEmail IsNot Nothing Then .list_goAML_Transaction_EntityAccount_RE_Email.AddRange(listEntREEmail)

                                            Dim listEntRESanction = objDb.goAML_Report_Sanction.Where(Function(x) x.FK_Parent_Table_ID = itemEntRE.PK_goAML_Report_Related_Entities_ID And x.Parent_Table_Name = "goAML_Transaction_Entity_Related_Entity").ToList
                                            If listEntRESanction IsNot Nothing Then .list_goAML_Transaction_EntityAccount_RE_Sanctions.AddRange(listEntRESanction)

                                            Dim listEntREUrl = objDb.goAML_Report_Url.Where(Function(x) x.FK_Parent_Table_ID = itemEntRE.PK_goAML_Report_Related_Entities_ID And x.Parent_Table_Name = "goAML_Transaction_Entity_Related_Entity").ToList
                                            If listEntREUrl IsNot Nothing Then .list_goAML_Transaction_EntityAccount_RE_URL.AddRange(listEntREUrl)

                                        Next
                                    End If
                                    '' end 02-Oct-2023
                                Next
                            End If

                            'Entity Director
                            If .list_goAML_Transaction_Entity IsNot Nothing AndAlso .list_goAML_Transaction_Entity.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Entity
                                    Dim listDirector = objDb.goAML_Trn_Director.Where(Function(x) x.FK_Transaction_ID = ID And x.FK_Sender_Information = GoAMLDAL.GoAMLEnum.enumSenderInformation.Entity).ToList
                                    If listDirector IsNot Nothing Then .list_goAML_Trn_Director.AddRange(listDirector)
                                Next
                            End If

                            'Entity Director Address, Phone, Identification
                            Dim listEntityDirector = .list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = GoAMLDAL.GoAMLEnum.enumSenderInformation.Entity).ToList
                            If listEntityDirector IsNot Nothing AndAlso listEntityDirector.Count > 0 Then
                                For Each item In listEntityDirector
                                    Dim listAddress = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_Director_ID And x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorEntity).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Director_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Director_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)

                                    '' Add 02-Oct-2023, Felix
                                    Dim listEntDirEmail = objDb.goAML_Report_Email.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_Director_ID And x.Parent_Table_Name = "goAML_Trn_Entity_Director").ToList
                                    If listEntDirEmail IsNot Nothing Then .list_goAML_Transaction_Entity_Director_Email.AddRange(listEntDirEmail)

                                    Dim listEntDirSanction = objDb.goAML_Report_Sanction.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_Director_ID And x.Parent_Table_Name = "goAML_Trn_Entity_Director").ToList
                                    If listEntDirSanction IsNot Nothing Then .list_goAML_Transaction_Entity_Director_Sanctions.AddRange(listEntDirSanction)

                                    Dim listEntDirPep = objDb.goAML_Report_Person_PEP.Where(Function(x) x.FK_Parent_Table_ID = item.PK_goAML_Trn_Director_ID And x.Parent_Table_Name = "goAML_Trn_Entity_Director").ToList
                                    If listEntDirPep IsNot Nothing Then .list_goAML_Transaction_Entity_Director_PEP.AddRange(listEntDirPep)
                                    '' End 02-Oct-2023
                                Next
                            End If

                            'Conductor
                            Dim listConductor = objDb.goAML_Trn_Conductor.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listConductor IsNot Nothing Then .list_goAML_Trn_Conductor.AddRange(listConductor)

                            'Conductor Address, Phone, Identification
                            If .list_goAML_Trn_Conductor IsNot Nothing AndAlso .list_goAML_Trn_Conductor.Count > 0 Then
                                For Each item In .list_goAML_Trn_Conductor
                                    Dim listAddress = objDb.goAML_Trn_Conductor_Address.Where(Function(x) x.FK_Trn_Conductor_ID = item.PK_goAML_Trn_Conductor_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Conductor_Phone.Where(Function(x) x.FK_Trn_Conductor_ID = item.PK_goAML_Trn_Conductor_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_Conductor_ID And x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Conductor).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Conductor_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Conductor_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                        Else    'Transaction Multi Party
                            Dim listParty = objDb.goAML_Transaction_Party.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listParty IsNot Nothing Then
                                .list_goAML_Transaction_Party.AddRange(listParty)

                                For Each party In listParty
                                    'Account
                                    Dim listAccount = objDb.goAML_Trn_Party_Account.Where(Function(x) x.FK_Trn_Party_ID = party.PK_Trn_Party_ID).ToList

                                    'Signatory
                                    If listAccount IsNot Nothing Then
                                        .list_goAML_Trn_Party_Account.AddRange(listAccount)

                                        For Each objAccount In listAccount
                                            Dim listSignatory = objDb.goAML_Trn_par_acc_Signatory.Where(Function(x) x.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID).ToList

                                            'Signatory Address, Phone, Identification
                                            If listSignatory IsNot Nothing Then
                                                .list_goAML_Trn_par_acc_Signatory.AddRange(listSignatory)

                                                For Each objSignatory In listSignatory
                                                    Dim listAddress = objDb.goAML_Trn_par_Acc_sign_Address.Where(Function(x) x.FK_Trn_par_acc_Signatory_ID = objSignatory.PK_Trn_par_acc_Signatory_ID).ToList
                                                    Dim listPhone = objDb.goAML_trn_par_acc_sign_Phone.Where(Function(x) x.FK_Trn_Par_Acc_Sign = objSignatory.PK_Trn_par_acc_Signatory_ID).ToList
                                                    Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = objSignatory.PK_Trn_par_acc_Signatory_ID And x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory).ToList

                                                    If listAddress IsNot Nothing Then .list_goAML_Trn_par_Acc_sign_Address.AddRange(listAddress)
                                                    If listPhone IsNot Nothing Then .list_goAML_trn_par_acc_sign_Phone.AddRange(listPhone)
                                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                                Next
                                            End If

                                            'Account Entity
                                            Dim listAccountEntity = objDb.goAML_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID).ToList

                                            'Account Entity Address, Phone
                                            If listAccountEntity IsNot Nothing Then
                                                .list_goAML_Trn_Par_Acc_Entity.AddRange(listAccountEntity)

                                                For Each objAccountEntity In listAccountEntity
                                                    Dim listAddress = objDb.goAML_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID).ToList
                                                    Dim listPhone = objDb.goAML_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID).ToList

                                                    If listAddress IsNot Nothing Then .list_goAML_Trn_par_Acc_Entity_Address.AddRange(listAddress)
                                                    If listPhone IsNot Nothing Then .list_goAML_trn_par_acc_Entity_Phone.AddRange(listPhone)

                                                    'Account Entity Director
                                                    Dim listDirector = objDb.goAML_Trn_Par_Acc_Ent_Director.Where(Function(x) x.FK_Trn_Par_Acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID).ToList

                                                    'Account Entity Director Address, Phone, Identification
                                                    If listDirector IsNot Nothing Then
                                                        .list_goAML_Trn_Par_Acc_Ent_Director.AddRange(listDirector)

                                                        For Each item In listDirector
                                                            Dim listDirectorAddress = objDb.goAML_Trn_Par_Acc_Ent_Director_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = item.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                                                            Dim listDirectorPhone = objDb.goAML_Trn_Par_Acc_Ent_Director_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = item.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                                                            Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Trn_Par_Acc_Ent_Director_ID And x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount).ToList

                                                            If listDirectorAddress IsNot Nothing Then .list_goAML_Trn_Par_Acc_Ent_Director_Address.AddRange(listDirectorAddress)
                                                            If listDirectorPhone IsNot Nothing Then .list_goAML_Trn_Par_Acc_Ent_Director_Phone.AddRange(listDirectorPhone)
                                                            If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                                        Next
                                                    End If
                                                Next
                                            End If
                                        Next
                                    End If

                                    'Person
                                    Dim listPerson = objDb.goAML_Trn_Party_Person.Where(Function(x) x.FK_Transaction_Party_ID = party.PK_Trn_Party_ID).ToList

                                    'Person Address, Phone, Identification
                                    If listPerson IsNot Nothing Then
                                        .list_goAML_Trn_Party_Person.AddRange(listPerson)

                                        For Each objPerson In listPerson
                                            Dim listAddress = objDb.goAML_Trn_Party_Person_Address.Where(Function(x) x.FK_Trn_Party_Person_ID = objPerson.PK_Trn_Party_Person_ID).ToList
                                            Dim listPhone = objDb.goAML_Trn_Party_Person_Phone.Where(Function(x) x.FK_Trn_Party_Person_ID = objPerson.PK_Trn_Party_Person_ID).ToList
                                            Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = objPerson.PK_Trn_Party_Person_ID And x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Person).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_Party_Person_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_Trn_Party_Person_Phone.AddRange(listPhone)
                                            If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                        Next
                                    End If

                                    'Entity
                                    Dim listEntity = objDb.goAML_Trn_Party_Entity.Where(Function(x) x.FK_Transaction_Party_ID = party.PK_Trn_Party_ID).ToList

                                    'Entity Address, Phone, Director
                                    If listEntity IsNot Nothing Then
                                        .list_goAML_Trn_Party_Entity.AddRange(listEntity)

                                        For Each objEntity In listEntity
                                            Dim listAddress = objDb.goAML_Trn_Party_Entity_Address.Where(Function(x) x.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID).ToList
                                            Dim listPhone = objDb.goAML_Trn_Party_Entity_Phone.Where(Function(x) x.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_Party_Entity_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_Trn_Party_Entity_Phone.AddRange(listPhone)

                                            'Entity Director
                                            Dim listDirector = objDb.goAML_Trn_Par_Entity_Director.Where(Function(x) x.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID).ToList
                                            If listDirector IsNot Nothing Then
                                                .list_goAML_Trn_Par_Entity_Director.AddRange(listDirector)

                                                'Entity Director Address, Phone, Identification
                                                For Each objDirector In listDirector
                                                    Dim listDirectorAddress = objDb.goAML_Trn_Par_Entity_Director_Address.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = objDirector.PK_Trn_Par_Entity_Director_ID).ToList
                                                    Dim listDirectorPhone = objDb.goAML_Trn_Par_Entity_Director_Phone.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = objDirector.PK_Trn_Par_Entity_Director_ID).ToList
                                                    Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = objDirector.PK_Trn_Par_Entity_Director_ID And x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorEntity).ToList

                                                    If listDirectorAddress IsNot Nothing Then .list_goAML_Trn_Par_Entity_Director_Address.AddRange(listDirectorAddress)
                                                    If listDirectorPhone IsNot Nothing Then .list_goAML_Trn_Par_Entity_Director_Phone.AddRange(listDirectorPhone)
                                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                                Next
                                            End If
                                        Next
                                    End If
                                Next
                            End If
                        End If
                    End If
                End If
            End With

            Return objGoAMLTransactionClass
        End Using
    End Function

    Shared Function GetGoAMLActivityClassByID(ID As Long) As GoAML_Activity_Class
        Using objDb As New GoAMLDAL.GoAMLEntities

            Dim objGoAMLActivityClass = New GoAML_Activity_Class
            With objGoAMLActivityClass
                'GoAML Activity
                .obj_goAML_Act_ReportPartyType = objDb.goAML_Act_ReportPartyType.Where(Function(x) x.PK_goAML_Act_ReportPartyType_ID = ID).FirstOrDefault

                If .obj_goAML_Act_ReportPartyType IsNot Nothing Then
                    Dim objActivity = .obj_goAML_Act_ReportPartyType

                    'Account
                    Dim listAccount = objDb.goAML_Act_Account.Where(Function(x) x.FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID).ToList
                    If listAccount IsNot Nothing Then .list_goAML_Act_Account.AddRange(listAccount)

                    'Signatory
                    If .list_goAML_Act_Account IsNot Nothing AndAlso .list_goAML_Act_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Account
                            Dim listSignatory = objDb.goAML_Act_acc_Signatory.Where(Function(x) x.FK_Activity_Account_ID = item.PK_goAML_Act_Account_ID).ToList
                            If listSignatory IsNot Nothing Then .list_goAML_Act_acc_Signatory.AddRange(listSignatory)
                        Next
                    End If

                    'Signatory Address, Phone, Identification
                    If .list_goAML_Act_acc_Signatory IsNot Nothing AndAlso .list_goAML_Act_acc_Signatory.Count > 0 Then
                        For Each item In .list_goAML_Act_acc_Signatory
                            Dim listAddress = objDb.goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_Act_Acc_Entity = item.PK_goAML_Act_acc_Signatory_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_acc_Signatory_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_goAML_Act_acc_Signatory_ID And x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Acc_sign_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Acc_sign_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If

                    'Account Entity
                    If .list_goAML_Act_Account IsNot Nothing AndAlso .list_goAML_Act_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Account
                            Dim listAccountEntity = objDb.goAML_Act_Entity_Account.Where(Function(x) x.FK_Act_Account_ID = item.PK_goAML_Act_Account_ID).ToList
                            If listAccountEntity IsNot Nothing Then .list_goAML_Act_Entity_Account.AddRange(listAccountEntity)
                        Next
                    End If

                    'Account Entity Address, Phone
                    If .list_goAML_Act_Entity_Account IsNot Nothing AndAlso .list_goAML_Act_Entity_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity_Account
                            Dim listAddress = objDb.goAML_Act_Acc_Entity_Address.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_account).ToList
                            Dim listPhone = objDb.goAML_Act_Acc_Entity_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_account).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Acc_Entity_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Acc_Entity_Phone.AddRange(listPhone)
                        Next
                    End If

                    'Account Entity Director
                    If .list_goAML_Act_Entity_Account IsNot Nothing AndAlso .list_goAML_Act_Entity_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity_Account
                            Dim listDirector = objDb.goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_account).ToList
                            If listDirector IsNot Nothing Then .list_goAML_Act_Acc_Ent_Director.AddRange(listDirector)
                        Next
                    End If

                    'Account Entity Director Address, Phone, Identification
                    If .list_goAML_Act_Acc_Ent_Director IsNot Nothing AndAlso .list_goAML_Act_Acc_Ent_Director.Count > 0 Then
                        For Each item In .list_goAML_Act_Acc_Ent_Director
                            Dim listAddress = objDb.goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = item.PK_Act_Acc_Ent_Director_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = item.PK_Act_Acc_Ent_Director_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_Act_Acc_Ent_Director_ID And x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Acc_Entity_Director_address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Acc_Entity_Director_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If

                    'Person
                    Dim listPerson = objDb.goAML_Act_Person.Where(Function(x) x.FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID).ToList
                    If listPerson IsNot Nothing Then .list_goAML_Act_Person.AddRange(listPerson)

                    'Person Address, Phone, Identification
                    If .list_goAML_Act_Person IsNot Nothing AndAlso .list_goAML_Act_Person.Count > 0 Then
                        For Each item In .list_goAML_Act_Person
                            Dim listAddress = objDb.goAML_Act_Person_Address.Where(Function(x) x.FK_Act_Person = item.PK_goAML_Act_Person_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Person_Phone.Where(Function(x) x.FK_Act_Person = item.PK_goAML_Act_Person_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_goAML_Act_Person_ID And x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Person).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Person_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Person_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If

                    'Entity
                    Dim listEntity = objDb.goAML_Act_Entity.Where(Function(x) x.FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID).ToList
                    If listEntity IsNot Nothing Then .list_goAML_Act_Entity.AddRange(listEntity)

                    'Entity Address, Phone
                    If .list_goAML_Act_Entity IsNot Nothing AndAlso .list_goAML_Act_Entity.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity
                            Dim listAddress = objDb.goAML_Act_Entity_Address.Where(Function(x) x.FK_Act_Entity_ID = item.PK_goAML_Act_Entity_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Entity_Phone.Where(Function(x) x.FK_Act_Entity_ID = item.PK_goAML_Act_Entity_ID).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Entity_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Entity_Phone.AddRange(listPhone)
                        Next
                    End If

                    'Entity Director
                    If .list_goAML_Act_Entity IsNot Nothing AndAlso .list_goAML_Act_Entity.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity
                            Dim listDirector = objDb.goAML_Act_Director.Where(Function(x) x.FK_Act_Entity_ID = item.PK_goAML_Act_Entity_ID).ToList
                            .list_goAML_Act_Director.AddRange(listDirector)
                        Next
                    End If

                    'Entity Director Address, Phone, Identification
                    If .list_goAML_Act_Director IsNot Nothing AndAlso .list_goAML_Act_Director.Count > 0 Then
                        For Each item In .list_goAML_Act_Director
                            Dim listAddress = objDb.goAML_Act_Director_Address.Where(Function(x) x.FK_Act_Director_ID = item.PK_goAML_Act_Director_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Director_Phone.Where(Function(x) x.FK_Act_Director_ID = item.PK_goAML_Act_Director_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_goAML_Act_Director_ID And x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorEntity).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Director_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Director_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If
                End If
            End With

            Return objGoAMLActivityClass
        End Using
    End Function

    Shared Function GetGoAMLReportIndicatorByID(ID As Long) As goAML_Report_Indicator
        Using objDb As New GoAMLDAL.goAMLEntities
            Dim objResult = objDb.goAML_Report_Indicator.Where(Function(x) x.PK_Report_Indicator = ID).FirstOrDefault
            Return objResult
        End Using
    End Function

    Shared Sub CreateNotes(ModuleID As Integer, ModuleLabel As String, actionApproval As Integer, actionForm As Integer, unikID As String, notes As String)
        Dim objHistory As New GoAMLDAL.goAML_Module_Note_History

        Dim approvalH As String
        Dim formH As String

        Select Case actionApproval
            Case 1
                approvalH = "Accept"
            Case 2
                approvalH = "Reject"
            Case 3
                approvalH = "Request Approval"
            Case Else
                Throw New ApplicationException("Action not found")
        End Select

        Select Case actionForm
            Case 1
                formH = "Add:<br/> "
            Case 2
                formH = "Edit:<br/> "
            Case 3
                formH = "Delete:<br/> "
            Case 4
                formH = ""
            Case Else
                Throw New ApplicationException("Action not found")
        End Select

        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            objHistory.FK_Module_ID = ModuleID
            objHistory.UnikID = unikID
            objHistory.UserID = NawaBLL.Common.SessionCurrentUser.PK_MUser_ID
            objHistory.UserName = NawaBLL.Common.SessionCurrentUser.UserName
            objHistory.RoleID = NawaBLL.Common.SessionCurrentUser.FK_MRole_ID
            objHistory.Status = approvalH
            objHistory.Notes = formH & notes
            objHistory.Active = 1
            objHistory.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            objHistory.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")

            objDb.Entry(objHistory).State = Entity.EntityState.Added
            objDb.SaveChanges()
        End Using
    End Sub

    Shared Sub SaveReportEdit(objModule As NawaDAL.Module, objData As GoAML_Report_Class)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Save Report Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Get Old Data for Audit Trail
            Dim objData_Old = GetGoAMLReportClassByID(objData.obj_goAML_Report.PK_Report_ID)

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Jalankan save state saat status berubah ke 9 untuk pertama kali
            'Hanya dijalankan jika membutuhkan Approval
            If Not (NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not objModule.IsUseApproval) Then
                If objData_Old.obj_goAML_Report.Status <> 9 And objData.obj_goAML_Report.Status = 9 Then
                    SaveStateByReportID(objData.obj_goAML_Report.PK_Report_ID)
                ElseIf objData_Old.obj_goAML_Report.Status <> 9 And objData.obj_goAML_Report.Status = 4 Then
                    SaveStateByReportID(objData.obj_goAML_Report.PK_Report_ID)
                End If
            End If

            'Save New Data
            Using objDB As New GoAMLDAL.GoAMLEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Report
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        objDB.Entry(objData.obj_goAML_Report).State = Entity.EntityState.Modified
                        objDB.SaveChanges()

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                'Save Audit Trail
                'Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName, objData.obj_goAML_Report.CreatedBy)
                '18-Mar-2022 untuk action edit, jika status = 1 (Waiting for Generate) artinya checker approved or status = 5 (Need Correction) artinya Reject.
                'Jika tidak maka Approver dikosongkan
                Dim objAuditTrailheader As GoAMLDAL.AuditTrailHeader = Nothing
                Dim strUserCreator As String = objData.obj_goAML_Report.LastUpdateBy
                Dim strUserApprover As String = Nothing

                If Not IsNothing(objData.obj_goAML_Report.Status) Then
                    Dim intReportStatus = objData.obj_goAML_Report.Status
                    'AuditTrailStatus 1 = Affected to Database, 2 = Rejected
                    'ModuleAction 6 = Approval
                    If intReportStatus = 1 Then     'Waiting for Generate (Approved)
                        intAuditTrailStatus = 1
                        intModuleAction = 6
                        strUserApprover = strUserID
                    ElseIf intReportStatus = 5 Then   'Need Correction
                        intAuditTrailStatus = 2
                        intModuleAction = 6
                        strUserApprover = strUserID
                        '' add 03-Oct-2023, Felix. Thanks to Brian (BSIM)
                    ElseIf intReportStatus = 4 Then
                        intAuditTrailStatus = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                        strUserApprover = strUserID
                    End If
                End If

                If Not IsNothing(objData.obj_goAML_Report.MarkedAsDelete) Then
                    Dim intMarkAsDelete = objData.obj_goAML_Report.MarkedAsDelete
                    If intMarkAsDelete = True Then
                        intModuleAction = NawaBLL.Common.ModuleActionEnum.Delete
                    End If
                End If
                '' end 03-Oct-2023
                objAuditTrailheader = NawaFramework.CreateAuditTrail(objDB, strUserApprover, intAuditTrailStatus, intModuleAction, strModuleName)

                'Save Audit Trail Detail by XML
                Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

                Dim listReport As New List(Of goAML_Report)
                Dim listReport_Old As New List(Of goAML_Report)

                listReport.Add(objData.obj_goAML_Report)
                listReport_Old.Add(objData_Old.obj_goAML_Report)
                NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, listReport, listReport_Old)

                'Update Entity Reference
                objDB.Database.ExecuteSqlCommand("exec usp_updateEntityReference")

            End Using

            Dim strTRN_OR_ACT As String = getTRNorACT(objData.obj_goAML_Report.Report_Code)
            Dim strTRN_OR_ACT_Old As String = getTRNorACT(objData_Old.obj_goAML_Report.Report_Code)
            If strTRN_OR_ACT <> strTRN_OR_ACT_Old Then
                If strTRN_OR_ACT = "TRN" Then
                    DeleteActivityByReportID(objData.obj_goAML_Report.PK_Report_ID)
                ElseIf strTRN_OR_ACT = "ACT" Then
                    DeleteTransactionByReportID(objData.obj_goAML_Report.PK_Report_ID)
                End If
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Save Report Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Function SaveReportAdd(objModule As NawaDAL.Module, objData As GoAML_Report_Class) As String
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Save Report Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Get Old Data (Karena saat save transaction dia metodenya langsung simpan, maka harus dikondisikan logicnya).
            'Jika Old data exists, Secara intAction tetap 1 (Add) tapi secara data dia Modified.
            Dim objData_Old = GetGoAMLReportClassByID(objData.obj_goAML_Report.PK_Report_ID)

            Using objDB As New GoAMLDAL.GoAMLEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Report
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        If objData_Old Is Nothing OrElse objData_Old.obj_goAML_Report Is Nothing Then
                            objDB.Entry(objData.obj_goAML_Report).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(objData.obj_goAML_Report).State = Entity.EntityState.Modified
                        End If
                        objDB.SaveChanges()
                        objTrans.Commit()

                        Return objData.obj_goAML_Report.PK_Report_ID
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                        Return Nothing
                    End Try

                    objDB.Database.ExecuteSqlCommand("exec usp_updateEntityReference")
                End Using

                'Save Audit Trail
                'Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                '18-Mar-2022 untuk action add, hilangkan Approver, tambahkan Creator
                Dim objAuditTrailheader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, Nothing, intAuditTrailStatus, 1, strModuleName)

                'Save Audit Trail Detail by XML
                Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID
                Dim listReport As New List(Of goAML_Report)

                listReport.Add(objData.obj_goAML_Report)
                NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, listReport, Nothing)

            End Using

            Dim strTRN_OR_ACT As String = getTRNorACT(objData.obj_goAML_Report.Report_Code)
            Dim strTRN_OR_ACT_Old As String = getTRNorACT(objData_Old.obj_goAML_Report.Report_Code)
            If strTRN_OR_ACT <> strTRN_OR_ACT_Old Then
                If strTRN_OR_ACT = "TRN" Then
                    DeleteActivityByReportID(objData.obj_goAML_Report.PK_Report_ID)
                ElseIf strTRN_OR_ACT = "ACT" Then
                    DeleteTransactionByReportID(objData.obj_goAML_Report.PK_Report_ID)
                End If
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Save Report Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Sub SaveStateByReportID(strReportID As String)
        Try
            Dim strQuery As String

            Dim strRestoreState As String = GoAML_Global_BLL.getGlobalParameterValueByID(3002)
            If String.IsNullOrWhiteSpace(strRestoreState) Then
                strRestoreState = "0"
            End If

            'Save State if RestoreState feature set to ON
            If strRestoreState = "1" Then
                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Save Report State for later Restore if Rejected by Checker started')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                Dim param(1) As SqlParameter
                param(0) = New SqlParameter
                param(0).ParameterName = "@PK_Report_ID"
                param(0).Value = strReportID
                param(0).DbType = SqlDbType.BigInt

                param(1) = New SqlParameter
                param(1).ParameterName = "@UserID"
                param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
                param(1).DbType = SqlDbType.VarChar
                NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GoAML_Report_XML_SaveState", param)

                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Save Report State for later Restore if Rejected by Checker finished')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub RestoreStateByReportID(strReportID As String)
        Try
            Dim strQuery As String

            Dim strRestoreState As String = GoAML_Global_BLL.getGlobalParameterValueByID(3002)
            If String.IsNullOrWhiteSpace(strRestoreState) Then
                strRestoreState = "0"
            End If

            'Restore State
            If strRestoreState = "1" Then       'Restore State On
                Dim param(1) As SqlParameter
                param(0) = New SqlParameter
                param(0).ParameterName = "@PK_Report_ID"
                param(0).Value = strReportID
                param(0).DbType = SqlDbType.BigInt

                param(1) = New SqlParameter
                param(1).ParameterName = "@UserID"
                param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
                param(1).DbType = SqlDbType.VarChar
                NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GoAML_Report_XML_RestoreState", param)
            End If

            'Hapus GoAML_Report_XML_SavedState by PK Report ID
            strQuery = "DELETE FROM goAML_Report_XML_SavedState WHERE FK_Report_ID=" & strReportID
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Shared Sub SaveTransaction(objModule As NawaDAL.Module, objData As GoAML_Transaction_Class, strReportID As String)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            If objData.obj_goAML_Transaction.PK_Transaction_ID < 0 Then
                intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert
            End If

            'Get Old Data for Audit Trail
            Dim objData_Old = New GoAML_Transaction_Class
            If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                objData_Old = GetGoAMLTransactionClassByID(objData.obj_goAML_Transaction.PK_Transaction_ID)
            End If

            Using objDB As New GoAMLDAL.GoAMLEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Transaction
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        If objData.obj_goAML_Transaction.PK_Transaction_ID < 0 Then
                            objDB.Entry(objData.obj_goAML_Transaction).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(objData.obj_goAML_Transaction).State = Entity.EntityState.Modified
                        End If
                        objDB.SaveChanges()

                        '===================== NEW ADDED/EDITED DATA
                        'Insert ke EODLogSP untuk ukur performance
                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Add/Edit Data started')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        '3 Mei 2023 Ari : Save Transaction Address
                        If objData.obj_goAML_Transaction_Address.address IsNot Nothing Then
                            With objData.obj_goAML_Transaction_Address
                                .FK_goAML_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID
                                .FK_Report_ID = strReportID
                            End With
                            'If objData.obj_goAML_Transaction_Address.PK_goAML_Transaction_Address_ID < 0 Then
                            If objData.obj_goAML_Transaction_Address.PK_goAML_Transaction_Address_ID <= 0 Then '' Edit 25-Sep-2023, Felix
                                objDB.Entry(objData.obj_goAML_Transaction_Address).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(objData.obj_goAML_Transaction_Address).State = Entity.EntityState.Modified
                            End If
                            objDB.SaveChanges()
                        End If

                        If objData.obj_goAML_Transaction.FK_Transaction_Type = 1 Then       'Transaction Bi-Party
                            Dim OldPK As Long = 0
                            Dim OldPKSub As Long = 0
                            Dim OldPKSubSubID As Long = 0

                            ''Add 21-Sep-2023
                            Dim OldPkRelatedPerson As Long = 0
                            Dim OldPkRelatedEntity As Long = 0
                            ''End 21-Sep-2023

                            '1. Account
                            For Each objAccount In objData.list_goAML_Transaction_Account
                                OldPK = objAccount.PK_Account_ID
                                objAccount.FK_Report_ID = strReportID
                                objAccount.FK_Report_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objAccount.PK_Account_ID < 0 Then
                                    objDB.Entry(objAccount).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objAccount).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                'Account Signatory
                                Dim listSignatory = objData.list_goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = OldPK).ToList
                                If listSignatory IsNot Nothing Then
                                    For Each objSignatory In listSignatory
                                        OldPKSub = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                        objSignatory.FK_Report_ID = strReportID
                                        objSignatory.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID
                                        objSignatory.FK_Transaction_Account_ID = objAccount.PK_Account_ID

                                        If objSignatory.PK_goAML_Trn_acc_Signatory_ID < 0 Then
                                            objDB.Entry(objSignatory).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objSignatory).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Account Signatory Address
                                        Dim listAddress = objData.list_goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = OldPKSub).ToList
                                        If listAddress IsNot Nothing Then
                                            For Each objAddress In listAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID

                                                If objAddress.PK_goAML_Trn_Acc_Entity_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Signatory Phone
                                        Dim listPhone = objData.list_goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = OldPKSub).ToList
                                        If listPhone IsNot Nothing Then
                                            For Each objPhone In listPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID

                                                If objPhone.PK_goAML_trn_acc_sign_Phone < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Signatory Identification
                                        Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory And x.FK_Person_ID = OldPKSub).ToList
                                        If listIdentification IsNot Nothing Then
                                            For Each objIdentification In listIdentification
                                                objIdentification.FK_Report_ID = strReportID
                                                objIdentification.FK_Person_ID = objSignatory.PK_goAML_Trn_acc_Signatory_ID

                                                If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        '' Add 21-Sep-2023, Felix
                                        'Account Signatory Email
                                        Dim listSignEmail = objData.list_goAML_Transaction_Account_Signatory_Email.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_acc_Signatory" And x.FK_Parent_Table_ID = OldPKSub).ToList
                                        If listSignEmail IsNot Nothing Then
                                            For Each objSignEmail In listSignEmail
                                                objSignEmail.FK_Report_ID = strReportID
                                                objSignEmail.FK_Parent_Table_ID = objSignatory.PK_goAML_Trn_acc_Signatory_ID

                                                If objSignEmail.PK_goAML_Report_Email_ID < 0 Then
                                                    objDB.Entry(objSignEmail).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objSignEmail).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Signatory Sanction
                                        Dim listSignSanction = objData.list_goAML_Transaction_Account_Signatory_Sanctions.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_acc_Signatory" And x.FK_Parent_Table_ID = OldPKSub).ToList
                                        If listSignSanction IsNot Nothing Then
                                            For Each objSignSanction In listSignSanction
                                                objSignSanction.FK_Report_ID = strReportID
                                                objSignSanction.FK_Parent_Table_ID = objSignatory.PK_goAML_Trn_acc_Signatory_ID

                                                If objSignSanction.PK_goAML_Report_Sanction_ID < 0 Then
                                                    objDB.Entry(objSignSanction).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objSignSanction).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Signatory PEP
                                        Dim listSignPEP = objData.list_goAML_Transaction_Account_Signatory_PEP.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_acc_Signatory" And x.FK_Parent_Table_ID = OldPKSub).ToList
                                        If listSignPEP IsNot Nothing Then
                                            For Each objSignPEP In listSignPEP
                                                objSignPEP.FK_Report_ID = strReportID
                                                objSignPEP.FK_Parent_Table_ID = objSignatory.PK_goAML_Trn_acc_Signatory_ID

                                                If objSignPEP.PK_goAML_Report_Person_PEP_ID < 0 Then
                                                    objDB.Entry(objSignPEP).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objSignPEP).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Signatory Related Person
                                        Dim listSignRP = objData.list_goAML_Transaction_Account_Signatory_RelatedPerson.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_acc_Signatory" And x.FK_Parent_Table_ID = OldPKSub).ToList
                                        If listSignRP IsNot Nothing Then
                                            For Each objSignRP In listSignRP
                                                OldPkRelatedPerson = objSignRP.PK_goAML_Report_Related_Person_ID
                                                objSignRP.FK_Report_ID = strReportID
                                                objSignRP.FK_Parent_Table_ID = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                                objSignRP.FK_From_Or_To = objSignatory.FK_From_Or_To

                                                If objSignRP.PK_goAML_Report_Related_Person_ID < 0 Then
                                                    objDB.Entry(objSignRP).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objSignRP).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'Account Signatory RP Email
                                                Dim listSignRPEmail = objData.list_goAML_Transaction_Account_Signatory_RP_Email.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person" And x.FK_Parent_Table_ID = OldPkRelatedPerson).ToList
                                                If listSignRPEmail IsNot Nothing Then
                                                    For Each objSignRPEmail In listSignRPEmail
                                                        objSignRPEmail.FK_Report_ID = strReportID
                                                        objSignRPEmail.FK_Parent_Table_ID = objSignRP.PK_goAML_Report_Related_Person_ID
                                                        objSignRPEmail.FK_From_Or_To = objSignRP.FK_From_Or_To

                                                        If objSignRPEmail.PK_goAML_Report_Email_ID < 0 Then
                                                            objDB.Entry(objSignRPEmail).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objSignRPEmail).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Signatory RP Sanction
                                                Dim listSignRPSanction = objData.list_goAML_Transaction_Account_Signatory_RP_Sanctions.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person" And x.FK_Parent_Table_ID = OldPkRelatedPerson).ToList
                                                If listSignRPSanction IsNot Nothing Then
                                                    For Each objSignRPSanction In listSignRPSanction
                                                        objSignRPSanction.FK_Report_ID = strReportID
                                                        objSignRPSanction.FK_Parent_Table_ID = objSignRP.PK_goAML_Report_Related_Person_ID
                                                        objSignRPSanction.FK_From_Or_To = objSignRP.FK_From_Or_To

                                                        If objSignRPSanction.PK_goAML_Report_Sanction_ID < 0 Then
                                                            objDB.Entry(objSignRPSanction).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objSignRPSanction).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Signatory RP PEP
                                                Dim listSignRPPEP = objData.list_goAML_Transaction_Account_Signatory_RP_PEP.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person" And x.FK_Parent_Table_ID = OldPkRelatedPerson).ToList
                                                If listSignRPPEP IsNot Nothing Then
                                                    For Each objSignRPPEP In listSignRPPEP
                                                        objSignRPPEP.FK_Report_ID = strReportID
                                                        objSignRPPEP.FK_Parent_Table_ID = objSignRP.PK_goAML_Report_Related_Person_ID
                                                        objSignRPPEP.FK_From_Or_To = objSignRP.FK_From_Or_To

                                                        If objSignRPPEP.PK_goAML_Report_Person_PEP_ID < 0 Then
                                                            objDB.Entry(objSignRPPEP).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objSignRPPEP).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Signatory RP Address
                                                Dim listSignRPAddress = objData.list_goAML_Transaction_Account_Signatory_RP_Address.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person" And x.FK_PARENT_TABLE_ID = OldPkRelatedPerson).ToList
                                                If listSignRPAddress IsNot Nothing Then
                                                    For Each objSignRPAddress In listSignRPAddress
                                                        objSignRPAddress.FK_Report_ID = strReportID
                                                        objSignRPAddress.FK_Parent_Table_ID = objSignRP.PK_goAML_Report_Related_Person_ID
                                                        objSignRPAddress.FK_From_Or_To = objSignRP.FK_From_Or_To

                                                        If objSignRPAddress.PK_Acc_Signatory_Rel_Person_Address_ID < 0 Then
                                                            objDB.Entry(objSignRPAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objSignRPAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Signatory RP Address
                                                Dim listSignRPPhone = objData.list_goAML_Transaction_Account_Signatory_RP_Phone.Where(Function(x) x.PARENT_TABLE_NAME = "goAML_Trn_acc_Signatory_Related_Person" And x.FK_PARENT_TABLE_ID = OldPkRelatedPerson).ToList
                                                If listSignRPPhone IsNot Nothing Then
                                                    For Each objSignRPPhone In listSignRPPhone
                                                        objSignRPPhone.FK_Report_ID = strReportID
                                                        objSignRPPhone.FK_PARENT_TABLE_ID = objSignRP.PK_goAML_Report_Related_Person_ID
                                                        objSignRPPhone.FK_From_Or_To = objSignRP.FK_From_Or_To

                                                        If objSignRPPhone.PK_goAML_Acc_Signatory_Related_Person_Phone_ID < 0 Then
                                                            objDB.Entry(objSignRPPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objSignRPPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Signatory RP Identification
                                                Dim listSignRPIdentification = objData.list_goAML_Transaction_Account_Signatory_RP_Identification.Where(Function(x) x.PARENT_TABLE_NAME = "goAML_Trn_acc_Signatory_Related_Person" And x.FK_PARENT_TABLE_ID = OldPkRelatedPerson).ToList
                                                If listSignRPIdentification IsNot Nothing Then
                                                    For Each objSignRPIdentification In listSignRPIdentification
                                                        objSignRPIdentification.FK_Report_ID = strReportID
                                                        objSignRPIdentification.FK_PARENT_TABLE_ID = objSignRP.PK_goAML_Report_Related_Person_ID
                                                        objSignRPIdentification.FK_From_Or_To = objSignRP.FK_From_Or_To

                                                        If objSignRPIdentification.PK_Acc_Sign_Rel_Person_Identification_ID < 0 Then
                                                            objDB.Entry(objSignRPIdentification).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objSignRPIdentification).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                            Next
                                        End If
                                        '' End 21-Sep-2023
                                    Next
                                End If

                                'Account Entity
                                Dim listAccountEntity = objData.list_goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = OldPK).ToList
                                If listAccountEntity IsNot Nothing Then
                                    For Each objAccountEntity In listAccountEntity
                                        OldPKSub = objAccountEntity.PK_goAML_Trn_Entity_account
                                        objAccountEntity.FK_Report_ID = strReportID
                                        objAccountEntity.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID
                                        objAccountEntity.FK_Account_ID = objAccount.PK_Account_ID

                                        If objAccountEntity.PK_goAML_Trn_Entity_account < 0 Then
                                            objDB.Entry(objAccountEntity).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objAccountEntity).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Account Entity Address
                                        Dim listEntityAddress = objData.list_goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = OldPKSub).ToList
                                        If listEntityAddress IsNot Nothing Then
                                            For Each objAddress In listEntityAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account

                                                If objAddress.PK_Trn_Acc_Entity_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Entity Phone
                                        Dim listEntityPhone = objData.list_goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = OldPKSub).ToList
                                        If listEntityPhone IsNot Nothing Then
                                            For Each objPhone In listEntityPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account

                                                If objPhone.PK_goAML_Trn_Acc_Entity_Phone < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        '' Add 21-Sep-2023, Felix.
                                        'Account Entity Email
                                        Dim listAccEntEmail = objData.list_goAML_Transaction_Account_EntityAccount_Email.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_Entity_Account" And x.FK_Parent_Table_ID = OldPKSub).ToList
                                        If listAccEntEmail IsNot Nothing Then
                                            For Each objAccEntEmail In listAccEntEmail
                                                objAccEntEmail.FK_Report_ID = strReportID
                                                objAccEntEmail.FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account
                                                objAccEntEmail.FK_From_Or_To = objAccountEntity.FK_From_Or_To

                                                If objAccEntEmail.PK_goAML_Report_Email_ID < 0 Then
                                                    objDB.Entry(objAccEntEmail).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAccEntEmail).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Entity Sanction
                                        Dim listAccEntSanction = objData.list_goAML_Transaction_Account_EntityAccount_Sanctions.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_Entity_Account" And x.FK_Parent_Table_ID = OldPKSub).ToList
                                        If listAccEntSanction IsNot Nothing Then
                                            For Each objAccEntSanction In listAccEntSanction
                                                objAccEntSanction.FK_Report_ID = strReportID
                                                objAccEntSanction.FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account
                                                objAccEntSanction.FK_From_Or_To = objAccountEntity.FK_From_Or_To

                                                If objAccEntSanction.PK_goAML_Report_Sanction_ID < 0 Then
                                                    objDB.Entry(objAccEntSanction).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAccEntSanction).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Entity Identification
                                        Dim listAccEntIdentification = objData.list_goAML_Transaction_Account_EntityAccount_Identification.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_Entity_Account" And x.FK_Parent_Table_ID = OldPKSub).ToList
                                        If listAccEntIdentification IsNot Nothing Then
                                            For Each objAccEntIdentification In listAccEntIdentification
                                                objAccEntIdentification.FK_Report_ID = strReportID
                                                objAccEntIdentification.FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account
                                                objAccEntIdentification.FK_From_Or_To = objAccountEntity.FK_From_Or_To

                                                If objAccEntIdentification.PK_goAML_Report_Identifications_ID < 0 Then
                                                    objDB.Entry(objAccEntIdentification).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAccEntIdentification).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Entity URL
                                        Dim listAccEntURL = objData.list_goAML_Transaction_Account_EntityAccount_URL.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_Entity_Account" And x.FK_Parent_Table_ID = OldPKSub).ToList
                                        If listAccEntURL IsNot Nothing Then
                                            For Each objAccEntURL In listAccEntURL
                                                objAccEntURL.FK_Report_ID = strReportID
                                                objAccEntURL.FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account
                                                objAccEntURL.FK_From_Or_To = objAccountEntity.FK_From_Or_To

                                                If objAccEntURL.PK_goAML_Report_Url_ID < 0 Then
                                                    objDB.Entry(objAccEntURL).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAccEntURL).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Related Entity
                                        Dim listAccEntRelatedEntity = objData.list_goAML_Transaction_Account_EntityAccount_RelatedEntity.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_Entity_Account" And x.FK_Parent_Table_ID = OldPKSub).ToList
                                        If listAccEntRelatedEntity IsNot Nothing Then
                                            For Each objAccEntRelatedEntity In listAccEntRelatedEntity
                                                OldPkRelatedEntity = objAccEntRelatedEntity.PK_goAML_Report_Related_Entities_ID
                                                objAccEntRelatedEntity.FK_Report_ID = strReportID
                                                objAccEntRelatedEntity.FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account
                                                objAccEntRelatedEntity.FK_From_Or_To = objAccountEntity.FK_From_Or_To

                                                If objAccEntRelatedEntity.PK_goAML_Report_Related_Entities_ID < 0 Then
                                                    objDB.Entry(objAccEntRelatedEntity).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAccEntRelatedEntity).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'Account Entity RE Email
                                                Dim listAccEntREEmail = objData.list_goAML_Transaction_Account_EntityAccount_RE_Email.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity" And x.FK_Parent_Table_ID = OldPkRelatedEntity).ToList
                                                If listAccEntREEmail IsNot Nothing Then
                                                    For Each objAccEntREEmail In listAccEntREEmail
                                                        objAccEntREEmail.FK_Report_ID = strReportID
                                                        objAccEntREEmail.FK_Parent_Table_ID = objAccEntRelatedEntity.PK_goAML_Report_Related_Entities_ID
                                                        objAccEntREEmail.FK_From_Or_To = objAccEntRelatedEntity.FK_From_Or_To

                                                        If objAccEntREEmail.PK_goAML_Report_Email_ID < 0 Then
                                                            objDB.Entry(objAccEntREEmail).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAccEntREEmail).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity RE Sanction
                                                Dim listAccEntRESanction = objData.list_goAML_Transaction_Account_EntityAccount_RE_Sanctions.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity" And x.FK_Parent_Table_ID = OldPkRelatedEntity).ToList
                                                If listAccEntRESanction IsNot Nothing Then
                                                    For Each objAccEntRESanction In listAccEntRESanction
                                                        objAccEntRESanction.FK_Report_ID = strReportID
                                                        objAccEntRESanction.FK_Parent_Table_ID = objAccEntRelatedEntity.PK_goAML_Report_Related_Entities_ID
                                                        objAccEntRESanction.FK_From_Or_To = objAccEntRelatedEntity.FK_From_Or_To

                                                        If objAccEntRESanction.PK_goAML_Report_Sanction_ID < 0 Then
                                                            objDB.Entry(objAccEntRESanction).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAccEntRESanction).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity RE Identification
                                                Dim listAccEntREIdentification = objData.list_goAML_Transaction_Account_EntityAccount_RE_Identification.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity" And x.FK_Parent_Table_ID = OldPkRelatedEntity).ToList
                                                If listAccEntREIdentification IsNot Nothing Then
                                                    For Each objAccEntREIdentification In listAccEntREIdentification
                                                        objAccEntREIdentification.FK_Report_ID = strReportID
                                                        objAccEntREIdentification.FK_Parent_Table_ID = objAccEntRelatedEntity.PK_goAML_Report_Related_Entities_ID
                                                        objAccEntREIdentification.FK_From_Or_To = objAccEntRelatedEntity.FK_From_Or_To

                                                        If objAccEntREIdentification.PK_goAML_Report_Identifications_ID < 0 Then
                                                            objDB.Entry(objAccEntREIdentification).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAccEntREIdentification).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity RE URL
                                                Dim listAccEntREURL = objData.list_goAML_Transaction_Account_EntityAccount_RE_URL.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity" And x.FK_Parent_Table_ID = OldPkRelatedEntity).ToList
                                                If listAccEntREURL IsNot Nothing Then
                                                    For Each objAccEntREURL In listAccEntREURL
                                                        objAccEntREURL.FK_Report_ID = strReportID
                                                        objAccEntREURL.FK_Parent_Table_ID = objAccEntRelatedEntity.PK_goAML_Report_Related_Entities_ID
                                                        objAccEntREURL.FK_From_Or_To = objAccEntRelatedEntity.FK_From_Or_To

                                                        If objAccEntREURL.PK_goAML_Report_Url_ID < 0 Then
                                                            objDB.Entry(objAccEntREURL).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAccEntREURL).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity RE Address
                                                Dim listAccEntREAddress = objData.list_goAML_Transaction_Account_EntityAccount_RE_Address.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity" And x.FK_PARENT_TABLE_ID = OldPkRelatedEntity).ToList
                                                If listAccEntREAddress IsNot Nothing Then
                                                    For Each objAccEntREAddress In listAccEntREAddress
                                                        objAccEntREAddress.FK_Report_ID = strReportID
                                                        objAccEntREAddress.FK_Parent_Table_ID = objAccEntRelatedEntity.PK_goAML_Report_Related_Entities_ID
                                                        objAccEntREAddress.FK_From_Or_To = objAccEntRelatedEntity.FK_From_Or_To

                                                        If objAccEntREAddress.PK_Acc_Ent_Rel_Ent_Address_ID < 0 Then
                                                            objDB.Entry(objAccEntREAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAccEntREAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity RE Phone
                                                Dim listAccEntREPhone = objData.list_goAML_Transaction_Account_EntityAccount_RE_Phone.Where(Function(x) x.PARENT_TABLE_NAME = "goAML_Trn_Entity_Account_Related_Entity" And x.FK_PARENT_TABLE_ID = OldPkRelatedEntity).ToList
                                                If listAccEntREPhone IsNot Nothing Then
                                                    For Each objAccEntREPhone In listAccEntREPhone
                                                        objAccEntREPhone.FK_Report_ID = strReportID
                                                        objAccEntREPhone.FK_PARENT_TABLE_ID = objAccEntRelatedEntity.PK_goAML_Report_Related_Entities_ID
                                                        objAccEntREPhone.FK_From_Or_To = objAccEntRelatedEntity.FK_From_Or_To

                                                        If objAccEntREPhone.PK_goAML_Acc_Entity_Related_Entity_Phone_ID < 0 Then
                                                            objDB.Entry(objAccEntREPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAccEntREPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If
                                            Next
                                        End If
                                        '' End 21-Sep-2023

                                        'Account Entity Director
                                        Dim listAccountEntityDirector = objData.list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = GoAMLDAL.GoAMLEnum.enumSenderInformation.Account And x.FK_Entity_ID = OldPKSub).ToList
                                        If listAccountEntityDirector IsNot Nothing Then
                                            For Each objDirector In listAccountEntityDirector
                                                OldPKSubSubID = objDirector.PK_goAML_Trn_Director_ID
                                                objDirector.FK_Report_ID = strReportID
                                                objDirector.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID
                                                objDirector.FK_Entity_ID = objAccountEntity.PK_goAML_Trn_Entity_account
                                                objDirector.FK_From_Or_To = objAccountEntity.FK_From_Or_To '' Add 21-Sep-2023

                                                If objDirector.PK_goAML_Trn_Director_ID < 0 Then
                                                    objDB.Entry(objDirector).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'Account Entity Director Address
                                                Dim listAddress = objData.list_goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = OldPKSubSubID).ToList
                                                If listAddress IsNot Nothing Then
                                                    For Each objAddress In listAddress
                                                        objAddress.FK_Report_ID = strReportID
                                                        objAddress.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID

                                                        If objAddress.PK_goAML_Trn_Director_Address_ID < 0 Then
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Director Phone
                                                Dim listPhone = objData.list_goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = OldPKSubSubID).ToList
                                                If listPhone IsNot Nothing Then
                                                    For Each objPhone In listPhone
                                                        objPhone.FK_Report_ID = strReportID
                                                        objPhone.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID

                                                        If objPhone.PK_goAML_trn_Director_Phone < 0 Then
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Director Identification
                                                Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount And x.FK_Person_ID = OldPKSubSubID).ToList
                                                If listIdentification IsNot Nothing Then
                                                    For Each objIdentification In listIdentification
                                                        objIdentification.FK_Report_ID = strReportID
                                                        objIdentification.FK_Person_ID = objDirector.PK_goAML_Trn_Director_ID

                                                        If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                '' Add 21-Sep-2023, Felix
                                                'Account Entity Director Email
                                                Dim listAccEntDirEmail = objData.list_goAML_Transaction_Account_EntityAccount_Director_Email.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_Entity_Account_Director" And x.FK_Parent_Table_ID = OldPKSubSubID).ToList
                                                If listAccEntDirEmail IsNot Nothing Then
                                                    For Each objAccEntDirEmail In listAccEntDirEmail
                                                        objAccEntDirEmail.FK_Report_ID = strReportID
                                                        objAccEntDirEmail.FK_Parent_Table_ID = objDirector.PK_goAML_Trn_Director_ID
                                                        objAccEntDirEmail.FK_From_Or_To = objDirector.FK_From_Or_To

                                                        If objAccEntDirEmail.PK_goAML_Report_Email_ID < 0 Then
                                                            objDB.Entry(objAccEntDirEmail).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAccEntDirEmail).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Director Sanction
                                                Dim listAccEntDirSanction = objData.list_goAML_Transaction_Account_EntityAccount_Director_Sanctions.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_Entity_Account_Director" And x.FK_Parent_Table_ID = OldPKSubSubID).ToList
                                                If listAccEntDirSanction IsNot Nothing Then
                                                    For Each objAccEntDirSanction In listAccEntDirSanction
                                                        objAccEntDirSanction.FK_Report_ID = strReportID
                                                        objAccEntDirSanction.FK_Parent_Table_ID = objDirector.PK_goAML_Trn_Director_ID
                                                        objAccEntDirSanction.FK_From_Or_To = objDirector.FK_From_Or_To

                                                        If objAccEntDirSanction.PK_goAML_Report_Sanction_ID < 0 Then
                                                            objDB.Entry(objAccEntDirSanction).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAccEntDirSanction).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Director PEP
                                                Dim listAccEntDirPEP = objData.list_goAML_Transaction_Account_EntityAccount_Director_PEP.Where(Function(x) x.Parent_Table_Name = "goAML_Trn_Entity_Account_Director" And x.FK_Parent_Table_ID = OldPKSubSubID).ToList
                                                If listAccEntDirPEP IsNot Nothing Then
                                                    For Each objAccEntDirPEP In listAccEntDirPEP
                                                        objAccEntDirPEP.FK_Report_ID = strReportID
                                                        objAccEntDirPEP.FK_Parent_Table_ID = objDirector.PK_goAML_Trn_Director_ID
                                                        objAccEntDirPEP.FK_From_Or_To = objDirector.FK_From_Or_To

                                                        If objAccEntDirPEP.PK_goAML_Report_Person_PEP_ID < 0 Then
                                                            objDB.Entry(objAccEntDirPEP).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAccEntDirPEP).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                '' End 21-Sep-2023, Felix
                                            Next
                                        End If
                                    Next
                                End If
                            Next

                            '2. Person
                            For Each objPerson In objData.list_goAML_Transaction_Person
                                OldPK = objPerson.PK_Person_ID
                                objPerson.FK_Report_ID = strReportID
                                objPerson.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objPerson.PK_Person_ID < 0 Then
                                    objDB.Entry(objPerson).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objPerson).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                'Person Address
                                Dim listAddress = objData.list_goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = OldPK).ToList
                                If listAddress IsNot Nothing Then
                                    For Each objAddress In listAddress
                                        objAddress.FK_Report_ID = strReportID
                                        objAddress.FK_Trn_Person = objPerson.PK_Person_ID

                                        If objAddress.PK_goAML_Trn_Person_Address_ID < 0 Then
                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                'Person Phone
                                Dim listPhone = objData.list_goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = OldPK).ToList
                                If listPhone IsNot Nothing Then
                                    For Each objPhone In listPhone
                                        objPhone.FK_Report_ID = strReportID
                                        objPhone.FK_Trn_Person = objPerson.PK_Person_ID

                                        If objPhone.PK_goAML_trn_Person_Phone < 0 Then
                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                'Person Identification
                                Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Person And x.FK_Person_ID = OldPK).ToList
                                If listIdentification IsNot Nothing Then
                                    For Each objIdentification In listIdentification
                                        objIdentification.FK_Report_ID = strReportID
                                        objIdentification.FK_Person_ID = objPerson.PK_Person_ID

                                        If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If
                            Next

                            '3. Entity
                            For Each objEntity In objData.list_goAML_Transaction_Entity
                                OldPK = objEntity.PK_Entity_ID
                                objEntity.FK_Report_ID = strReportID
                                objEntity.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objEntity.PK_Entity_ID < 0 Then
                                    objDB.Entry(objEntity).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objEntity).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                'Entity Address
                                Dim listAddress = objData.list_goAML_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = OldPK).ToList
                                If listAddress IsNot Nothing Then
                                    For Each objAddress In listAddress
                                        objAddress.FK_Report_ID = strReportID
                                        objAddress.FK_Trn_Entity = objEntity.PK_Entity_ID

                                        If objAddress.PK_goAML_Trn_Entity_Address_ID < 0 Then
                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                'Entity Phone
                                Dim listPhone = objData.list_goAML_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = OldPK).ToList
                                If listPhone IsNot Nothing Then
                                    For Each objPhone In listPhone
                                        objPhone.FK_Report_ID = strReportID
                                        objPhone.FK_Trn_Entity = objEntity.PK_Entity_ID

                                        If objPhone.PK_goAML_Trn_Entity_Phone < 0 Then
                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                'Entity Director
                                Dim listDirector = objData.list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = GoAMLDAL.GoAMLEnum.enumSenderInformation.Entity And x.FK_Entity_ID = OldPK).ToList
                                If listDirector IsNot Nothing Then
                                    For Each objDirector In listDirector
                                        OldPKSub = objDirector.PK_goAML_Trn_Director_ID
                                        objDirector.FK_Report_ID = strReportID
                                        objDirector.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID
                                        objDirector.FK_Entity_ID = objEntity.PK_Entity_ID

                                        If objDirector.PK_goAML_Trn_Director_ID < 0 Then
                                            objDB.Entry(objDirector).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'EntityDirector Address
                                        Dim listDirectorAddress = objData.list_goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = OldPKSub).ToList
                                        If listDirectorAddress IsNot Nothing Then
                                            For Each objAddress In listDirectorAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID

                                                If objAddress.PK_goAML_Trn_Director_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'EntityDirector Phone
                                        Dim listDirectorPhone = objData.list_goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = OldPKSub).ToList
                                        If listDirectorPhone IsNot Nothing Then
                                            For Each objPhone In listDirectorPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID

                                                If objPhone.PK_goAML_trn_Director_Phone < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'EntityDirector Identification
                                        Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorEntity And x.FK_Person_ID = OldPKSub).ToList
                                        If listIdentification IsNot Nothing Then
                                            For Each objIdentification In listIdentification
                                                objIdentification.FK_Report_ID = strReportID
                                                objIdentification.FK_Person_ID = objDirector.PK_goAML_Trn_Director_ID

                                                If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If
                                    Next
                                End If
                            Next

                            '4. Conductor
                            For Each objConductor In objData.list_goAML_Trn_Conductor
                                objConductor.FK_REPORT_ID = strReportID
                                objConductor.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objConductor.PK_goAML_Trn_Conductor_ID < 0 Then
                                    objDB.Entry(objConductor).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objConductor).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                'Conductor Address
                                For Each objAddress In objData.list_goAML_Trn_Conductor_Address
                                    objAddress.FK_REPORT_ID = strReportID
                                    objAddress.FK_Trn_Conductor_ID = objConductor.PK_goAML_Trn_Conductor_ID

                                    If objAddress.PK_goAML_Trn_Conductor_Address_ID < 0 Then
                                        objDB.Entry(objAddress).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next

                                'Conductor Phone
                                For Each objPhone In objData.list_goAML_trn_Conductor_Phone
                                    objPhone.FK_REPORT_ID = strReportID
                                    objPhone.FK_Trn_Conductor_ID = objConductor.PK_goAML_Trn_Conductor_ID

                                    If objPhone.PK_goAML_trn_Conductor_Phone < 0 Then
                                        objDB.Entry(objPhone).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next

                                'Conductor Identification
                                Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Conductor).ToList
                                If listIdentification IsNot Nothing Then
                                    For Each objIdentification In listIdentification
                                        objIdentification.FK_Report_ID = strReportID
                                        objIdentification.FK_Person_ID = objConductor.PK_goAML_Trn_Conductor_ID

                                        If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If
                            Next
                        Else    'Transaction Multi-Party
                            Dim OldPKTParty As Long = 0
                            Dim OldPK As Long = 0
                            Dim OldPKSub As Long = 0
                            Dim OldPKSubSubID As Long = 0

                            For Each objTParty In objData.list_goAML_Transaction_Party
                                OldPKTParty = objTParty.PK_Trn_Party_ID
                                objTParty.FK_Report_ID = strReportID
                                objTParty.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objTParty.PK_Trn_Party_ID < 0 Then
                                    objDB.Entry(objTParty).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objTParty).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                '1. Account
                                Dim listAccount = objData.list_goAML_Trn_Party_Account.Where(Function(x) x.FK_Trn_Party_ID = OldPKTParty).ToList
                                If listAccount IsNot Nothing Then
                                    For Each objAccount In listAccount
                                        OldPK = objAccount.PK_Trn_Party_Account_ID
                                        objAccount.FK_Report_ID = strReportID
                                        objAccount.FK_Trn_Party_ID = objTParty.PK_Trn_Party_ID

                                        If objAccount.PK_Trn_Party_Account_ID < 0 Then
                                            objDB.Entry(objAccount).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objAccount).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Account Signatory
                                        Dim listSignatory = objData.list_goAML_Trn_par_acc_Signatory.Where(Function(x) x.FK_Trn_Party_Account_ID = OldPK).ToList
                                        If listSignatory IsNot Nothing Then
                                            For Each objSignatory In listSignatory
                                                OldPKSub = objSignatory.PK_Trn_par_acc_Signatory_ID
                                                objSignatory.FK_Report_ID = strReportID
                                                objSignatory.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID

                                                If objSignatory.PK_Trn_par_acc_Signatory_ID < 0 Then
                                                    objDB.Entry(objSignatory).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objSignatory).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'Account Signatory Address
                                                Dim listAddress = objData.list_goAML_Trn_par_Acc_sign_Address.Where(Function(x) x.FK_Trn_par_acc_Signatory_ID = OldPKSub).ToList
                                                If listAddress IsNot Nothing Then
                                                    For Each objAddress In listAddress
                                                        objAddress.FK_Report_ID = strReportID
                                                        objAddress.FK_Trn_par_acc_Signatory_ID = objSignatory.PK_Trn_par_acc_Signatory_ID

                                                        If objAddress.PK_Trn_Par_Acc_sign_Address_ID < 0 Then
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Signatory Phone
                                                Dim listPhone = objData.list_goAML_trn_par_acc_sign_Phone.Where(Function(x) x.FK_Trn_Par_Acc_Sign = OldPKSub).ToList
                                                If listPhone IsNot Nothing Then
                                                    For Each objPhone In listPhone
                                                        objPhone.FK_Report_ID = strReportID
                                                        objPhone.FK_Trn_Par_Acc_Sign = objSignatory.PK_Trn_par_acc_Signatory_ID

                                                        If objPhone.PK_trn_Par_Acc_Sign_Phone < 0 Then
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Signatory Identification
                                                Dim listIdentification = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory And x.FK_Person_ID = OldPKSub).ToList
                                                If listIdentification IsNot Nothing Then
                                                    For Each objIdentification In listIdentification
                                                        objIdentification.FK_Report_ID = strReportID
                                                        objIdentification.FK_Person_ID = objSignatory.PK_Trn_par_acc_Signatory_ID

                                                        If objIdentification.PK_Transaction_Party_Identification_ID < 0 Then
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If
                                            Next
                                        End If

                                        'Account Entity
                                        Dim listAccountEntity = objData.list_goAML_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = OldPK).ToList
                                        If listAccountEntity IsNot Nothing Then
                                            For Each objAccountEntity In listAccountEntity
                                                OldPKSub = objAccountEntity.PK_Trn_Par_acc_Entity_ID
                                                objAccountEntity.FK_Report_ID = strReportID
                                                objAccountEntity.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID

                                                If objAccountEntity.PK_Trn_Par_acc_Entity_ID < 0 Then
                                                    objDB.Entry(objAccountEntity).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAccountEntity).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'Account Entity Address
                                                Dim listEntityAddress = objData.list_goAML_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = OldPKSub).ToList
                                                If listEntityAddress IsNot Nothing Then
                                                    For Each objAddress In listEntityAddress
                                                        objAddress.FK_Report_ID = strReportID
                                                        objAddress.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                                        If objAddress.PK_Trn_Par_Acc_Entity_Address_ID < 0 Then
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Phone
                                                Dim listEntityPhone = objData.list_goAML_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = OldPKSub).ToList
                                                If listEntityPhone IsNot Nothing Then
                                                    For Each objPhone In listEntityPhone
                                                        objPhone.FK_Report_ID = strReportID
                                                        objPhone.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                                        If objPhone.PK_trn_Par_Acc_Entity_Phone_ID < 0 Then
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Director
                                                Dim listAccountEntityDirector = objData.list_goAML_Trn_Par_Acc_Ent_Director.Where(Function(x) x.FK_Trn_Par_Acc_Entity_ID = OldPKSub).ToList
                                                If listAccountEntityDirector IsNot Nothing Then
                                                    For Each objDirector In listAccountEntityDirector
                                                        OldPKSubSubID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID
                                                        objDirector.FK_Report_ID = strReportID
                                                        objDirector.FK_Trn_Par_Acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                                        If objDirector.PK_Trn_Par_Acc_Ent_Director_ID < 0 Then
                                                            objDB.Entry(objDirector).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()

                                                        'Account Entity Director Address
                                                        Dim listAddress = objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = OldPKSubSubID).ToList
                                                        If listAddress IsNot Nothing Then
                                                            For Each objAddress In listAddress
                                                                objAddress.FK_Report_ID = strReportID
                                                                objAddress.FK_Trn_par_acc_Entity_Director_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID

                                                                If objAddress.PK_Trn_Par_Acc_Entity_Address_ID < 0 Then
                                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                                Else
                                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                                End If
                                                                objDB.SaveChanges()
                                                            Next
                                                        End If

                                                        'Account Entity Director Phone
                                                        Dim listPhone = objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = OldPKSubSubID).ToList
                                                        If listPhone IsNot Nothing Then
                                                            For Each objPhone In listPhone
                                                                objPhone.FK_Report_ID = strReportID
                                                                objPhone.FK_Trn_par_acc_Entity_Director_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID

                                                                If objPhone.PK_Trn_Par_Acc_Ent_Director_Phone_ID < 0 Then
                                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                                Else
                                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                                End If
                                                                objDB.SaveChanges()
                                                            Next
                                                        End If

                                                        'Account Entity Director Identification
                                                        Dim listIdentification = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount And x.FK_Person_ID = OldPKSubSubID).ToList
                                                        If listIdentification IsNot Nothing Then
                                                            For Each objIdentification In listIdentification
                                                                objIdentification.FK_Report_ID = strReportID
                                                                objIdentification.FK_Person_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID

                                                                If objIdentification.PK_Transaction_Party_Identification_ID < 0 Then
                                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                                Else
                                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                                End If
                                                                objDB.SaveChanges()
                                                            Next
                                                        End If
                                                    Next
                                                End If
                                            Next
                                        End If
                                    Next
                                End If

                                '2. Person
                                Dim listPerson = objData.list_goAML_Trn_Party_Person.Where(Function(x) x.FK_Transaction_Party_ID = OldPKTParty).ToList
                                If listPerson IsNot Nothing Then
                                    For Each objPerson In listPerson
                                        OldPK = objPerson.PK_Trn_Party_Person_ID
                                        objPerson.FK_Report_ID = strReportID
                                        objPerson.FK_Transaction_Party_ID = objTParty.PK_Trn_Party_ID

                                        If objPerson.PK_Trn_Party_Person_ID < 0 Then
                                            objDB.Entry(objPerson).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objPerson).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Person Address
                                        Dim listAddress = objData.list_goAML_Trn_Party_Person_Address.Where(Function(x) x.FK_Trn_Party_Person_ID = OldPK).ToList
                                        If listAddress IsNot Nothing Then
                                            For Each objAddress In listAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Party_Person_ID = objPerson.PK_Trn_Party_Person_ID

                                                If objAddress.PK_Trn_Party_Person_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Person Phone
                                        Dim listPhone = objData.list_goAML_Trn_Party_Person_Phone.Where(Function(x) x.FK_Trn_Party_Person_ID = OldPK).ToList
                                        If listPhone IsNot Nothing Then
                                            For Each objPhone In listPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Party_Person_ID = objPerson.PK_Trn_Party_Person_ID

                                                If objPhone.PK_Trn_Party_Person_Phone_ID < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Person Identification
                                        Dim listIdentification = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Person And x.FK_Person_ID = OldPK).ToList
                                        If listIdentification IsNot Nothing Then
                                            For Each objIdentification In listIdentification
                                                objIdentification.FK_Report_ID = strReportID
                                                objIdentification.FK_Person_ID = objPerson.PK_Trn_Party_Person_ID

                                                If objIdentification.PK_Transaction_Party_Identification_ID < 0 Then
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If
                                    Next
                                End If

                                '3. Entity
                                Dim listEntity = objData.list_goAML_Trn_Party_Entity.Where(Function(x) x.FK_Transaction_Party_ID = OldPKTParty).ToList
                                If listEntity IsNot Nothing Then
                                    For Each objEntity In listEntity
                                        OldPK = objEntity.PK_Trn_Party_Entity_ID
                                        objEntity.FK_Report_ID = strReportID
                                        objEntity.FK_Transaction_Party_ID = objTParty.PK_Trn_Party_ID

                                        If objEntity.PK_Trn_Party_Entity_ID < 0 Then
                                            objDB.Entry(objEntity).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objEntity).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Entity Address
                                        Dim listAddress = objData.list_goAML_Trn_Party_Entity_Address.Where(Function(x) x.FK_Trn_Party_Entity_ID = OldPK).ToList
                                        If listAddress IsNot Nothing Then
                                            For Each objAddress In listAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID

                                                If objAddress.PK_Trn_Party_Entity_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Entity Phone
                                        Dim listPhone = objData.list_goAML_Trn_Party_Entity_Phone.Where(Function(x) x.FK_Trn_Party_Entity_ID = OldPK).ToList
                                        If listPhone IsNot Nothing Then
                                            For Each objPhone In listPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID

                                                If objPhone.PK_Trn_Party_Entity_Phone_ID < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Entity Director
                                        Dim listDirector = objData.list_goAML_Trn_Par_Entity_Director.Where(Function(x) x.FK_Trn_Party_Entity_ID = OldPK).ToList
                                        If listDirector IsNot Nothing Then
                                            For Each objDirector In listDirector
                                                OldPKSub = objDirector.PK_Trn_Par_Entity_Director_ID
                                                objDirector.FK_Report_ID = strReportID
                                                objDirector.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID

                                                If objDirector.PK_Trn_Par_Entity_Director_ID < 0 Then
                                                    objDB.Entry(objDirector).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'EntityDirector Address
                                                Dim listDirectorAddress = objData.list_goAML_Trn_Par_Entity_Director_Address.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = OldPKSub).ToList
                                                If listDirectorAddress IsNot Nothing Then
                                                    For Each objAddress In listDirectorAddress
                                                        objAddress.FK_Report_ID = strReportID
                                                        objAddress.FK_Trn_par_Entity_Director_ID = objDirector.PK_Trn_Par_Entity_Director_ID

                                                        If objAddress.PK_Trn_Par_Entity_Address_ID < 0 Then
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'EntityDirector Phone
                                                Dim listDirectorPhone = objData.list_goAML_Trn_Par_Entity_Director_Phone.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = OldPKSub).ToList
                                                If listDirectorPhone IsNot Nothing Then
                                                    For Each objPhone In listDirectorPhone
                                                        objPhone.FK_Report_ID = strReportID
                                                        objPhone.FK_Trn_par_Entity_Director_ID = objDirector.PK_Trn_Par_Entity_Director_ID

                                                        If objPhone.PK_Trn_Par_Entity_Director_Phone_ID < 0 Then
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'EntityDirector Identification
                                                Dim listIdentification = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorEntity And x.FK_Person_ID = OldPKSub).ToList
                                                If listIdentification IsNot Nothing Then
                                                    For Each objIdentification In listIdentification
                                                        objIdentification.FK_Report_ID = strReportID
                                                        objIdentification.FK_Person_ID = objDirector.PK_Trn_Par_Entity_Director_ID

                                                        If objIdentification.PK_Transaction_Party_Identification_ID < 0 Then
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If
                                            Next
                                        End If
                                    Next
                                End If
                            Next
                        End If

                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Add/Edit Data finished')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        '===================== END OF NEW ADDED/EDITED DATA

                        '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                        If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Deleted Data started')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                            '======================== Bi-PARTY
                            '1. Account
                            For Each item_old In objData_Old.list_goAML_Transaction_Account
                                Dim objCek = objData.list_goAML_Transaction_Account.Find(Function(x) x.PK_Account_ID = item_old.PK_Account_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory
                            For Each item_old In objData_Old.list_goAML_Trn_acc_Signatory
                                Dim objCek = objData.list_goAML_Trn_acc_Signatory.Find(Function(x) x.PK_goAML_Trn_acc_Signatory_ID = item_old.PK_goAML_Trn_acc_Signatory_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Address
                            For Each item_old In objData_Old.list_goAML_Trn_Acc_sign_Address
                                Dim objCek = objData.list_goAML_Trn_Acc_sign_Address.Find(Function(x) x.PK_goAML_Trn_Acc_Entity_Address_ID = item_old.PK_goAML_Trn_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Phone
                            For Each item_old In objData_Old.list_goAML_trn_acc_sign_Phone
                                Dim objCek = objData.list_goAML_trn_acc_sign_Phone.Find(Function(x) x.PK_goAML_trn_acc_sign_Phone = item_old.PK_goAML_trn_acc_sign_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity
                            For Each item_old In objData_Old.list_goAML_Trn_Entity_account
                                Dim objCek = objData.list_goAML_Trn_Entity_account.Find(Function(x) x.PK_goAML_Trn_Entity_account = item_old.PK_goAML_Trn_Entity_account)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Address
                            For Each item_old In objData_Old.list_goAML_Trn_Acc_Entity_Address
                                Dim objCek = objData.list_goAML_Trn_Acc_Entity_Address.Find(Function(x) x.PK_Trn_Acc_Entity_Address_ID = item_old.PK_Trn_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Acc_Entity_Phone
                                Dim objCek = objData.list_goAML_Trn_Acc_Entity_Phone.Find(Function(x) x.PK_goAML_Trn_Acc_Entity_Phone = item_old.PK_goAML_Trn_Acc_Entity_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '2. Person
                            For Each item_old In objData_Old.list_goAML_Transaction_Person
                                Dim objCek = objData.list_goAML_Transaction_Person.Find(Function(x) x.PK_Person_ID = item_old.PK_Person_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Address
                            For Each item_old In objData_Old.list_goAML_Trn_Person_Address
                                Dim objCek = objData.list_goAML_Trn_Person_Address.Find(Function(x) x.PK_goAML_Trn_Person_Address_ID = item_old.PK_goAML_Trn_Person_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Phone
                            For Each item_old In objData_Old.list_goAML_trn_Person_Phone
                                Dim objCek = objData.list_goAML_trn_Person_Phone.Find(Function(x) x.PK_goAML_trn_Person_Phone = item_old.PK_goAML_trn_Person_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '3. Entity
                            For Each item_old In objData_Old.list_goAML_Transaction_Entity
                                Dim objCek = objData.list_goAML_Transaction_Entity.Find(Function(x) x.PK_Entity_ID = item_old.PK_Entity_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Address
                            For Each item_old In objData_Old.list_goAML_Trn_Entity_Address
                                Dim objCek = objData.list_goAML_Trn_Entity_Address.Find(Function(x) x.PK_goAML_Trn_Entity_Address_ID = item_old.PK_goAML_Trn_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Entity_Phone
                                Dim objCek = objData.list_goAML_Trn_Entity_Phone.Find(Function(x) x.PK_goAML_Trn_Entity_Phone = item_old.PK_goAML_Trn_Entity_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account/Entity Director
                            For Each item_old In objData_Old.list_goAML_Trn_Director
                                Dim objCek = objData.list_goAML_Trn_Director.Find(Function(x) x.PK_goAML_Trn_Director_ID = item_old.PK_goAML_Trn_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account/Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Trn_Director_Address
                                Dim objCek = objData.list_goAML_Trn_Director_Address.Find(Function(x) x.PK_goAML_Trn_Director_Address_ID = item_old.PK_goAML_Trn_Director_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account/Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_trn_Director_Phone
                                Dim objCek = objData.list_goAML_trn_Director_Phone.Find(Function(x) x.PK_goAML_trn_Director_Phone = item_old.PK_goAML_trn_Director_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '4. Conductor
                            For Each item_old In objData_Old.list_goAML_Trn_Conductor
                                Dim objCek = objData.list_goAML_Trn_Conductor.Find(Function(x) x.PK_goAML_Trn_Conductor_ID = item_old.PK_goAML_Trn_Conductor_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Conductor Address
                            For Each item_old In objData_Old.list_goAML_Trn_Conductor_Address
                                Dim objCek = objData.list_goAML_Trn_Conductor_Address.Find(Function(x) x.PK_goAML_Trn_Conductor_Address_ID = item_old.PK_goAML_Trn_Conductor_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Conductor Phone
                            For Each item_old In objData_Old.list_goAML_trn_Conductor_Phone
                                Dim objCek = objData.list_goAML_trn_Conductor_Phone.Find(Function(x) x.PK_goAML_trn_Conductor_Phone = item_old.PK_goAML_trn_Conductor_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Conductor Identification
                            For Each item_old In objData_Old.list_goAML_Transaction_Person_Identification
                                Dim objCek = objData.list_goAML_Transaction_Person_Identification.Find(Function(x) x.PK_goAML_Transaction_Person_Identification_ID = item_old.PK_goAML_Transaction_Person_Identification_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '============================ MULTI-PARTY
                            '1. Account
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Account
                                Dim objCek = objData.list_goAML_Trn_Party_Account.Find(Function(x) x.PK_Trn_Party_Account_ID = item_old.PK_Trn_Party_Account_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory
                            For Each item_old In objData_Old.list_goAML_Trn_par_acc_Signatory
                                Dim objCek = objData.list_goAML_Trn_par_acc_Signatory.Find(Function(x) x.PK_Trn_par_acc_Signatory_ID = item_old.PK_Trn_par_acc_Signatory_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Address
                            For Each item_old In objData_Old.list_goAML_Trn_par_Acc_sign_Address
                                Dim objCek = objData.list_goAML_Trn_par_Acc_sign_Address.Find(Function(x) x.PK_Trn_Par_Acc_sign_Address_ID = item_old.PK_Trn_Par_Acc_sign_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Phone
                            For Each item_old In objData_Old.list_goAML_trn_par_acc_sign_Phone
                                Dim objCek = objData.list_goAML_trn_par_acc_sign_Phone.Find(Function(x) x.PK_trn_Par_Acc_Sign_Phone = item_old.PK_trn_Par_Acc_Sign_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Entity
                                Dim objCek = objData.list_goAML_Trn_Par_Acc_Entity.Find(Function(x) x.PK_Trn_Par_acc_Entity_ID = item_old.PK_Trn_Par_acc_Entity_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Address
                            For Each item_old In objData_Old.list_goAML_Trn_par_Acc_Entity_Address
                                Dim objCek = objData.list_goAML_Trn_par_Acc_Entity_Address.Find(Function(x) x.PK_Trn_Par_Acc_Entity_Address_ID = item_old.PK_Trn_Par_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Phone
                            For Each item_old In objData_Old.list_goAML_trn_par_acc_Entity_Phone
                                Dim objCek = objData.list_goAML_trn_par_acc_Entity_Phone.Find(Function(x) x.PK_trn_Par_Acc_Entity_Phone_ID = item_old.PK_trn_Par_Acc_Entity_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director
                                Dim objCek = objData.list_goAML_Trn_Par_Acc_Ent_Director.Find(Function(x) x.PK_Trn_Par_Acc_Ent_Director_ID = item_old.PK_Trn_Par_Acc_Ent_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Address
                                Dim objCek = objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Find(Function(x) x.PK_Trn_Par_Acc_Entity_Address_ID = item_old.PK_Trn_Par_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Phone
                                Dim objCek = objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Find(Function(x) x.PK_Trn_Par_Acc_Ent_Director_Phone_ID = item_old.PK_Trn_Par_Acc_Ent_Director_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '2. Person
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Person
                                Dim objCek = objData.list_goAML_Trn_Party_Person.Find(Function(x) x.PK_Trn_Party_Person_ID = item_old.PK_Trn_Party_Person_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Address
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Person_Address
                                Dim objCek = objData.list_goAML_Trn_Party_Person_Address.Find(Function(x) x.PK_Trn_Party_Person_Address_ID = item_old.PK_Trn_Party_Person_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Person_Phone
                                Dim objCek = objData.list_goAML_Trn_Party_Person_Phone.Find(Function(x) x.PK_Trn_Party_Person_Phone_ID = item_old.PK_Trn_Party_Person_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '3. Entity
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Entity
                                Dim objCek = objData.list_goAML_Trn_Party_Entity.Find(Function(x) x.PK_Trn_Party_Entity_ID = item_old.PK_Trn_Party_Entity_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Address
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Entity_Address
                                Dim objCek = objData.list_goAML_Trn_Party_Entity_Address.Find(Function(x) x.PK_Trn_Party_Entity_Address_ID = item_old.PK_Trn_Party_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Entity_Phone
                                Dim objCek = objData.list_goAML_Trn_Party_Entity_Phone.Find(Function(x) x.PK_Trn_Party_Entity_Phone_ID = item_old.PK_Trn_Party_Entity_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director
                                Dim objCek = objData.list_goAML_Trn_Par_Entity_Director.Find(Function(x) x.PK_Trn_Par_Entity_Director_ID = item_old.PK_Trn_Par_Entity_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director_Address
                                Dim objCek = objData.list_goAML_Trn_Par_Entity_Director_Address.Find(Function(x) x.PK_Trn_Par_Entity_Address_ID = item_old.PK_Trn_Par_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director_Phone
                                Dim objCek = objData.list_goAML_Trn_Par_Entity_Director_Phone.Find(Function(x) x.PK_Trn_Par_Entity_Director_Phone_ID = item_old.PK_Trn_Par_Entity_Director_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Deleted Data finished')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            '===================== END OF _DeleteD DATA
                        End If

                        objDB.SaveChanges()
                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                'Save Audit Trail
                SaveTransaction_AuditTrail(objData, objData_Old, objDB, objModule, intModuleAction)

            End Using

            '2021-11-24 : Coba jalankan validasi satuan per Transaction ID dari Form Reportnya untuk tuning performance
            'Jalankan validasi satuan untuk replace IsValid dan Error Message
            'strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Validate Satuan Started (Report ID = " & strReportID & ")')"
            'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Dim param(0) As SqlParameter
            'param(0) = New SqlParameter
            'param(0).ParameterName = "@PK_Report_ID"
            'param(0).Value = strReportID
            'param(0).DbType = SqlDbType.BigInt
            'NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_ValidasiSatuan", param)

            'strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Validate Satuan Finished (Report ID = " & strReportID & ")')"
            'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveTransaction_AuditTrail(objData As GoAML_Transaction_Class, objData_Old As GoAML_Transaction_Class, objDB As GoAMLEntities, objModule As NawaDAL.Module, intModuleAction As Integer)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("goAML_Transaction")

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
            '18-Mar-2022 Adi : Fixing Audit Trail
            If objData_Old Is Nothing OrElse IsNothing(objData_Old.obj_goAML_Transaction.PK_Transaction_ID) OrElse objData_Old.obj_goAML_Transaction.PK_Transaction_ID = 0 Then
                intModuleAction = 1
            Else
                intModuleAction = 2
            End If
            Dim objAuditTrailheader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, Nothing, intAuditTrailStatus, intModuleAction, objModule.ModuleLabel)

            'Save Audit Trail Detail by XML
            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

            Dim listTransaction As New List(Of goAML_Transaction)
            Dim listTransaction_Old As New List(Of goAML_Transaction)

            listTransaction.Add(objData.obj_goAML_Transaction)
            listTransaction_Old.Add(objData_Old.obj_goAML_Transaction)
            NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, listTransaction, listTransaction_Old)


            If objData.obj_goAML_Transaction.FK_Transaction_Type = 1 Then   'Transaction Bi-Party
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account, objData_Old.list_goAML_Transaction_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_acc_Signatory, objData_Old.list_goAML_Trn_acc_Signatory)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Acc_sign_Address, objData_Old.list_goAML_Trn_Acc_sign_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_trn_acc_sign_Phone, objData_Old.list_goAML_trn_acc_sign_Phone)

                ''Add 21-Sep-2023
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_Signatory_Email, objData_Old.list_goAML_Transaction_Account_Signatory_Email)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_Signatory_Sanctions, objData_Old.list_goAML_Transaction_Account_Signatory_Sanctions)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_Signatory_PEP, objData_Old.list_goAML_Transaction_Account_Signatory_PEP)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_Signatory_RelatedPerson, objData_Old.list_goAML_Transaction_Account_Signatory_RelatedPerson)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_Signatory_RP_Email, objData_Old.list_goAML_Transaction_Account_Signatory_RP_Email)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_Signatory_RP_Sanctions, objData_Old.list_goAML_Transaction_Account_Signatory_RP_Sanctions)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_Signatory_RP_PEP, objData_Old.list_goAML_Transaction_Account_Signatory_RP_PEP)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_Signatory_RP_Address, objData_Old.list_goAML_Transaction_Account_Signatory_RP_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_Signatory_RP_Phone, objData_Old.list_goAML_Transaction_Account_Signatory_RP_Phone)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_Signatory_RP_Identification, objData_Old.list_goAML_Transaction_Account_Signatory_RP_Identification)
                ''End 21-Sep-2023

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Entity_account, objData_Old.list_goAML_Trn_Entity_account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Acc_Entity_Address, objData_Old.list_goAML_Trn_Acc_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Acc_Entity_Phone, objData_Old.list_goAML_Trn_Acc_Entity_Phone)

                '' Add 21-Sep-2023
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_Email, objData_Old.list_goAML_Transaction_Account_EntityAccount_Email)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_Sanctions, objData_Old.list_goAML_Transaction_Account_EntityAccount_Sanctions)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_Identification, objData_Old.list_goAML_Transaction_Account_EntityAccount_Identification)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_URL, objData_Old.list_goAML_Transaction_Account_EntityAccount_URL)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_RelatedEntity, objData_Old.list_goAML_Transaction_Account_EntityAccount_RelatedEntity)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_RE_Email, objData_Old.list_goAML_Transaction_Account_EntityAccount_RE_Email)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_RE_Sanctions, objData_Old.list_goAML_Transaction_Account_EntityAccount_RE_Sanctions)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_RE_Sanctions, objData_Old.list_goAML_Transaction_Account_EntityAccount_RE_Identification)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_RE_Sanctions, objData_Old.list_goAML_Transaction_Account_EntityAccount_RE_URL)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_RE_Sanctions, objData_Old.list_goAML_Transaction_Account_EntityAccount_RE_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_RE_Sanctions, objData_Old.list_goAML_Transaction_Account_EntityAccount_RE_Phone)
                '' End 21-Sep-2023

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Person, objData_Old.list_goAML_Transaction_Person)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Person_Address, objData_Old.list_goAML_Trn_Person_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_trn_Person_Phone, objData_Old.list_goAML_trn_Person_Phone)

                '' Add 21-Sep-2023
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Person_Email, objData_Old.list_goAML_Transaction_Person_Email)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Person_Sanctions, objData_Old.list_goAML_Transaction_Person_Sanctions)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Person_PEP, objData_Old.list_goAML_Transaction_Person_PEP)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Person_RelatedPerson, objData_Old.list_goAML_Transaction_Person_RelatedPerson)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Person_RP_Email, objData_Old.list_goAML_Transaction_Person_RP_Email)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Person_RP_Sanctions, objData_Old.list_goAML_Transaction_Person_RP_Sanctions)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Person_RP_PEP, objData_Old.list_goAML_Transaction_Person_RP_PEP)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Person_RP_Address, objData_Old.list_goAML_Transaction_Person_RP_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Person_RP_Phone, objData_Old.list_goAML_Transaction_Person_RP_Phone)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Person_RP_Identification, objData_Old.list_goAML_Transaction_Person_RP_Identification)
                '' End 21-Sep-2023

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Entity, objData_Old.list_goAML_Transaction_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Entity_Address, objData_Old.list_goAML_Trn_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Entity_Phone, objData_Old.list_goAML_Trn_Entity_Phone)

                '' Add 21-Sep-2023
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Entity_Email, objData_Old.list_goAML_Transaction_Entity_Email)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Entity_Sanctions, objData_Old.list_goAML_Transaction_Entity_Sanctions)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Entity_Identification, objData_Old.list_goAML_Transaction_Entity_Identification)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Entity_URL, objData_Old.list_goAML_Transaction_Entity_URL)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Entity_RelatedEntity, objData_Old.list_goAML_Transaction_Entity_RelatedEntity)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_EntityAccount_RE_Email, objData_Old.list_goAML_Transaction_EntityAccount_RE_Email)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_EntityAccount_RE_Sanctions, objData_Old.list_goAML_Transaction_EntityAccount_RE_Sanctions)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_EntityAccount_RE_Identification, objData_Old.list_goAML_Transaction_EntityAccount_RE_Identification)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_EntityAccount_RE_URL, objData_Old.list_goAML_Transaction_EntityAccount_RE_URL)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_EntityAccount_RE_Address, objData_Old.list_goAML_Transaction_EntityAccount_RE_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_EntityAccount_RE_Phone, objData_Old.list_goAML_Transaction_EntityAccount_RE_Phone)
                '' End 21-Sep-2023

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Director, objData_Old.list_goAML_Trn_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Director_Address, objData_Old.list_goAML_Trn_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_trn_Director_Phone, objData_Old.list_goAML_trn_Director_Phone)

                '' Add 21-Sep-2023
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_Director_Email, objData_Old.list_goAML_Transaction_Account_EntityAccount_Director_Email)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_Director_Sanctions, objData_Old.list_goAML_Transaction_Account_EntityAccount_Director_Sanctions)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Account_EntityAccount_Director_PEP, objData_Old.list_goAML_Transaction_Account_EntityAccount_Director_PEP)
                '' End 21-Sep-2023

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Conductor, objData_Old.list_goAML_Trn_Conductor)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Conductor_Address, objData_Old.list_goAML_Trn_Conductor_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_trn_Conductor_Phone, objData_Old.list_goAML_trn_Conductor_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Person_Identification, objData_Old.list_goAML_Transaction_Person_Identification)
            Else    'Transaction Multi-Party
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Party, objData_Old.list_goAML_Transaction_Party)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Party_Account, objData_Old.list_goAML_Trn_Party_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_par_acc_Signatory, objData_Old.list_goAML_Trn_par_acc_Signatory)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_par_Acc_sign_Address, objData_Old.list_goAML_Trn_par_Acc_sign_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_trn_par_acc_sign_Phone, objData_Old.list_goAML_trn_par_acc_sign_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Par_Acc_Entity, objData_Old.list_goAML_Trn_Par_Acc_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_par_Acc_Entity_Address, objData_Old.list_goAML_Trn_par_Acc_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_trn_par_acc_Entity_Phone, objData_Old.list_goAML_trn_par_acc_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Par_Acc_Ent_Director, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Par_Acc_Ent_Director_Address, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Party_Person, objData_Old.list_goAML_Trn_Party_Person)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Party_Person_Address, objData_Old.list_goAML_Trn_Party_Person_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Party_Person_Phone, objData_Old.list_goAML_Trn_Party_Person_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Party_Entity, objData_Old.list_goAML_Trn_Party_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Party_Entity_Address, objData_Old.list_goAML_Trn_Party_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Party_Entity_Phone, objData_Old.list_goAML_Trn_Party_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Par_Entity_Director, objData_Old.list_goAML_Trn_Par_Entity_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Par_Entity_Director_Address, objData_Old.list_goAML_Trn_Par_Entity_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Trn_Par_Entity_Director_Phone, objData_Old.list_goAML_Trn_Par_Entity_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Transaction_Party_Identification, objData_Old.list_goAML_Transaction_Party_Identification)
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            'End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteTransactionByTransactionID(strTransactionID As String)
        Try
            'Define local variables
            Dim objModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("goAML_Transaction")
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Get Old Data to Delete
            Dim objData_Old = GetGoAMLTransactionClassByID(strTransactionID)
            If objData_Old Is Nothing Then
                objData_Old = New GoAML_Transaction_Class
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Transaction started (Transaction ID = " & strTransactionID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Using objDB As New GoAMLDAL.GoAMLEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Delete Transaction
                        objDB.Entry(objData_Old.obj_goAML_Transaction).State = Entity.EntityState.Deleted

                        '======================== Bi-PARTY
                        '1. Account
                        For Each item_old In objData_Old.list_goAML_Transaction_Account
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory
                        For Each item_old In objData_Old.list_goAML_Trn_acc_Signatory
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory Address
                        For Each item_old In objData_Old.list_goAML_Trn_Acc_sign_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory Phone
                        For Each item_old In objData_Old.list_goAML_trn_acc_sign_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity
                        For Each item_old In objData_Old.list_goAML_Trn_Entity_account
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Address
                        For Each item_old In objData_Old.list_goAML_Trn_Acc_Entity_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Phone
                        For Each item_old In objData_Old.list_goAML_Trn_Acc_Entity_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '2. Person
                        For Each item_old In objData_Old.list_goAML_Transaction_Person
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Person Address
                        For Each item_old In objData_Old.list_goAML_Trn_Person_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Person Phone
                        For Each item_old In objData_Old.list_goAML_trn_Person_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '3. Entity
                        For Each item_old In objData_Old.list_goAML_Transaction_Entity
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Address
                        For Each item_old In objData_Old.list_goAML_Trn_Entity_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Phone
                        For Each item_old In objData_Old.list_goAML_Trn_Entity_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account/Entity Director
                        For Each item_old In objData_Old.list_goAML_Trn_Director
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account/Entity Director Address
                        For Each item_old In objData_Old.list_goAML_Trn_Director_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account/Entity Director Phone
                        For Each item_old In objData_Old.list_goAML_trn_Director_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '4. Conductor
                        For Each item_old In objData_Old.list_goAML_Trn_Conductor
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Conductor Address
                        For Each item_old In objData_Old.list_goAML_Trn_Conductor_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Conductor Phone
                        For Each item_old In objData_Old.list_goAML_trn_Conductor_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Conductor Identification
                        For Each item_old In objData_Old.list_goAML_Transaction_Person_Identification
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '============================ MULTI-PARTY
                        '1. Account
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Account
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory
                        For Each item_old In objData_Old.list_goAML_Trn_par_acc_Signatory
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory Address
                        For Each item_old In objData_Old.list_goAML_Trn_par_Acc_sign_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory Phone
                        For Each item_old In objData_Old.list_goAML_trn_par_acc_sign_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Entity
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Address
                        For Each item_old In objData_Old.list_goAML_Trn_par_Acc_Entity_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Phone
                        For Each item_old In objData_Old.list_goAML_trn_par_acc_Entity_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Director
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Director Address
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Director Phone
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '2. Person
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Person
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Person Address
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Person_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Person Phone
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Person_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '3. Entity
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Entity
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Address
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Entity_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Phone
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Entity_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Director
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Director Address
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Director Phone
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        objDB.SaveChanges()
                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Transaction finished (Transaction ID = " & strTransactionID & ")')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                'Save Audit Trail
                DeleteTransactionByTransactionID_AuditTrail(objData_Old, objDB, objModule)

            End Using

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteTransactionByTransactionID_AuditTrail(objData_Old As GoAML_Transaction_Class, objDB As GoAMLEntities, objModule As NawaDAL.Module)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("goAML_Transaction")

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
            '18-Mar-2022 Adi : Fixing Audit Trail
            Dim objAuditTrailheader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, Nothing, intAuditTrailStatus, 3, objModule.ModuleLabel)

            'Save Audit Trail Detail by XML
            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

            Dim listTransactionToDelete As New List(Of goAML_Transaction)
            listTransactionToDelete.Add(objData_Old.obj_goAML_Transaction)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, listTransactionToDelete)

            If objData_Old.obj_goAML_Transaction.FK_Transaction_Type = 1 Then   'Transaction Bi-Party
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Transaction_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_acc_Signatory)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Acc_sign_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_trn_acc_sign_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Entity_account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Acc_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Acc_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Transaction_Person)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Person_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_trn_Person_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Transaction_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_trn_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Conductor)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Conductor_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_trn_Conductor_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Transaction_Person_Identification)
            Else    'Transaction Multi-Party
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Transaction_Party)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Party_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_par_acc_Signatory)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_par_Acc_sign_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_trn_par_acc_sign_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Par_Acc_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_par_Acc_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_trn_par_acc_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Party_Person)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Party_Person_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Party_Person_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Party_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Party_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Party_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Par_Entity_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Par_Entity_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Trn_Par_Entity_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Transaction_Party_Identification)
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteTransactionByReportID(strReportID As String)
        Try
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Transaction started (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.BigInt
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_DeleteTransaction_ByReportID", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Transaction finished (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveActivity(objModule As NawaDAL.Module, objData As GoAML_Activity_Class, strReportID As String)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            If objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID < 0 Then
                intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert
            End If

            'Get Old Data for Audit Trail
            Dim objData_Old = New GoAML_Activity_Class
            If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                objData_Old = GetGoAMLActivityClassByID(objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID)
            End If

            Using objDB As New GoAMLDAL.GoAMLEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Act_ReportPartyType
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        If objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID < 0 Then
                            objDB.Entry(objData.obj_goAML_Act_ReportPartyType).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(objData.obj_goAML_Act_ReportPartyType).State = Entity.EntityState.Modified
                        End If
                        objDB.SaveChanges()

                        '===================== NEW ADDED/EDITED DATA
                        'Insert ke EODLogSP untuk ukur performance
                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Add/Edit Data started')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Dim OldPK As Long = 0
                        Dim OldPKSub As Long = 0
                        Dim OldPKSubSubID As Long = 0

                        '1. Account
                        For Each objAccount In objData.list_goAML_Act_Account
                            OldPK = objAccount.PK_goAML_Act_Account_ID
                            objAccount.FK_Report_ID = strReportID
                            objAccount.FK_Act_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID

                            If objAccount.PK_goAML_Act_Account_ID < 0 Then
                                objDB.Entry(objAccount).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(objAccount).State = Entity.EntityState.Modified
                            End If
                            objDB.SaveChanges()

                            'Account Signatory
                            Dim listSignatory = objData.list_goAML_Act_acc_Signatory.Where(Function(x) x.FK_Activity_Account_ID = OldPK).ToList
                            If listSignatory IsNot Nothing Then
                                For Each objSignatory In listSignatory
                                    OldPKSub = objSignatory.PK_goAML_Act_acc_Signatory_ID
                                    objSignatory.FK_Report_ID = strReportID
                                    objSignatory.FK_Act_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID
                                    objSignatory.FK_Activity_Account_ID = objAccount.PK_goAML_Act_Account_ID

                                    If objSignatory.PK_goAML_Act_acc_Signatory_ID < 0 Then
                                        objDB.Entry(objSignatory).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objSignatory).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()

                                    'Account Signatory Address
                                    Dim listAddress = objData.list_goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_Act_Acc_Entity = OldPKSub).ToList
                                    If listAddress IsNot Nothing Then
                                        For Each objAddress In listAddress
                                            objAddress.FK_Report_ID = strReportID
                                            objAddress.FK_Act_Acc_Entity = objSignatory.PK_goAML_Act_acc_Signatory_ID

                                            If objAddress.PK_goAML_Act_Acc_Entity_Address_ID < 0 Then
                                                objDB.Entry(objAddress).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'Account Signatory Phone
                                    Dim listPhone = objData.list_goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = OldPKSub).ToList
                                    If listPhone IsNot Nothing Then
                                        For Each objPhone In listPhone
                                            objPhone.FK_Report_ID = strReportID
                                            objPhone.FK_Act_Acc_Entity_ID = objSignatory.PK_goAML_Act_acc_Signatory_ID

                                            If objPhone.PK_goAML_act_acc_sign_Phone_ID < 0 Then
                                                objDB.Entry(objPhone).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'Account Signatory Identification
                                    Dim listIdentification = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory And x.FK_Act_Person_ID = OldPKSub).ToList
                                    If listIdentification IsNot Nothing Then
                                        For Each objIdentification In listIdentification
                                            objIdentification.FK_Report_ID = strReportID
                                            objIdentification.FK_Act_Person_ID = objSignatory.PK_goAML_Act_acc_Signatory_ID

                                            If objIdentification.PK_goAML_Activity_Person_Identification_ID < 0 Then
                                                objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If
                                Next
                            End If

                            'Account Entity
                            Dim listAccountEntity = objData.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_Act_Account_ID = OldPK).ToList
                            If listAccountEntity IsNot Nothing Then
                                For Each objAccountEntity In listAccountEntity
                                    OldPKSub = objAccountEntity.PK_goAML_Act_Entity_account
                                    objAccountEntity.FK_Report_ID = strReportID
                                    objAccountEntity.FK_Activity_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID
                                    objAccountEntity.FK_Act_Account_ID = objAccount.PK_goAML_Act_Account_ID

                                    If objAccountEntity.PK_goAML_Act_Entity_account < 0 Then
                                        objDB.Entry(objAccountEntity).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objAccountEntity).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()

                                    'Account Entity Address
                                    Dim listEntityAddress = objData.list_goAML_Act_Acc_Entity_Address.Where(Function(x) x.FK_Act_Acc_Entity_ID = OldPKSub).ToList
                                    If listEntityAddress IsNot Nothing Then
                                        For Each objAddress In listEntityAddress
                                            objAddress.FK_Report_ID = strReportID
                                            objAddress.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                            If objAddress.PK_Act_Acc_Entity_Address_ID < 0 Then
                                                objDB.Entry(objAddress).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'Account Entity Phone
                                    Dim listEntityPhone = objData.list_goAML_Act_Acc_Entity_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = OldPKSub).ToList
                                    If listEntityPhone IsNot Nothing Then
                                        For Each objPhone In listEntityPhone
                                            objPhone.FK_Report_ID = strReportID
                                            objPhone.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                            If objPhone.PK_goAML_Act_Acc_Entity_Phone_ID < 0 Then
                                                objDB.Entry(objPhone).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'Account Entity Director
                                    Dim listAccountEntityDirector = objData.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = OldPKSub).ToList
                                    If listAccountEntityDirector IsNot Nothing Then
                                        For Each objDirector In listAccountEntityDirector
                                            OldPKSubSubID = objDirector.PK_Act_Acc_Ent_Director_ID
                                            objDirector.FK_Report_ID = strReportID
                                            objDirector.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                            If objDirector.PK_Act_Acc_Ent_Director_ID < 0 Then
                                                objDB.Entry(objDirector).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()

                                            'Account Entity Director Address
                                            Dim listAddress = objData.list_goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = OldPKSubSubID).ToList
                                            If listAddress IsNot Nothing Then
                                                For Each objAddress In listAddress
                                                    objAddress.FK_Report_ID = strReportID
                                                    objAddress.FK_Act_Entity_Director_ID = objDirector.PK_Act_Acc_Ent_Director_ID

                                                    If objAddress.PK_goAML_Act_Acc_Entity_Director_address_ID < 0 Then
                                                        objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                    Else
                                                        objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                    End If
                                                    objDB.SaveChanges()
                                                Next
                                            End If

                                            'Account Entity Director Phone
                                            Dim listPhone = objData.list_goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = OldPKSubSubID).ToList
                                            If listPhone IsNot Nothing Then
                                                For Each objPhone In listPhone
                                                    objPhone.FK_Report_ID = strReportID
                                                    objPhone.FK_Act_Entity_Director_ID = objDirector.PK_Act_Acc_Ent_Director_ID

                                                    If objPhone.PK_goAML_Act_Acc_Entity_Director_Phone_ID < 0 Then
                                                        objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                    Else
                                                        objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                    End If
                                                    objDB.SaveChanges()
                                                Next
                                            End If

                                            'Account Entity Director Identification
                                            Dim listIdentification = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount And x.FK_Act_Person_ID = OldPKSubSubID).ToList
                                            If listIdentification IsNot Nothing Then
                                                For Each objIdentification In listIdentification
                                                    objIdentification.FK_Report_ID = strReportID
                                                    objIdentification.FK_Act_Person_ID = objDirector.PK_Act_Acc_Ent_Director_ID

                                                    If objIdentification.PK_goAML_Activity_Person_Identification_ID < 0 Then
                                                        objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                    Else
                                                        objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                    End If
                                                    objDB.SaveChanges()
                                                Next
                                            End If
                                        Next
                                    End If
                                Next
                            End If
                        Next

                        '2. Person
                        For Each objPerson In objData.list_goAML_Act_Person
                            OldPK = objPerson.PK_goAML_Act_Person_ID
                            objPerson.FK_Report_ID = strReportID
                            objPerson.FK_Act_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID

                            If objPerson.PK_goAML_Act_Person_ID < 0 Then
                                objDB.Entry(objPerson).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(objPerson).State = Entity.EntityState.Modified
                            End If
                            objDB.SaveChanges()

                            'Person Address
                            Dim listAddress = objData.list_goAML_Act_Person_Address.Where(Function(x) x.FK_Act_Person = OldPK).ToList
                            If listAddress IsNot Nothing Then
                                For Each objAddress In listAddress
                                    objAddress.fk_report_id = strReportID
                                    objAddress.FK_Act_Person = objPerson.PK_goAML_Act_Person_ID

                                    If objAddress.PK_goAML_Act_Person_Address_ID < 0 Then
                                        objDB.Entry(objAddress).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If

                            'Person Phone
                            Dim listPhone = objData.list_goAML_Act_Person_Phone.Where(Function(x) x.FK_Act_Person = OldPK).ToList
                            If listPhone IsNot Nothing Then
                                For Each objPhone In listPhone
                                    objPhone.FK_Report_ID = strReportID
                                    objPhone.FK_Act_Person = objPerson.PK_goAML_Act_Person_ID

                                    If objPhone.PK_goAML_Act_Person_Phone_ID < 0 Then
                                        objDB.Entry(objPhone).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If

                            'Person Identification
                            Dim listIdentification = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Person And x.FK_Act_Person_ID = OldPK).ToList
                            If listIdentification IsNot Nothing Then
                                For Each objIdentification In listIdentification
                                    objIdentification.FK_Report_ID = strReportID
                                    objIdentification.FK_Act_Person_ID = objPerson.PK_goAML_Act_Person_ID

                                    If objIdentification.PK_goAML_Activity_Person_Identification_ID < 0 Then
                                        objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If
                        Next

                        '3. Entity
                        For Each objEntity In objData.list_goAML_Act_Entity
                            OldPK = objEntity.PK_goAML_Act_Entity_ID
                            objEntity.FK_Report_ID = strReportID
                            objEntity.FK_Act_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID

                            If objEntity.PK_goAML_Act_Entity_ID < 0 Then
                                objDB.Entry(objEntity).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(objEntity).State = Entity.EntityState.Modified
                            End If
                            objDB.SaveChanges()

                            'Entity Address
                            Dim listAddress = objData.list_goAML_Act_Entity_Address.Where(Function(x) x.FK_Act_Entity_ID = OldPK).ToList
                            If listAddress IsNot Nothing Then
                                For Each objAddress In listAddress
                                    objAddress.FK_Report_ID = strReportID
                                    objAddress.FK_Act_Entity_ID = objEntity.PK_goAML_Act_Entity_ID

                                    If objAddress.PK_goAML_Act_Entity_Address_ID < 0 Then
                                        objDB.Entry(objAddress).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If

                            'Entity Phone
                            Dim listPhone = objData.list_goAML_Act_Entity_Phone.Where(Function(x) x.FK_Act_Entity_ID = OldPK).ToList
                            If listPhone IsNot Nothing Then
                                For Each objPhone In listPhone
                                    objPhone.FK_Report_ID = strReportID
                                    objPhone.FK_Act_Entity_ID = objEntity.PK_goAML_Act_Entity_ID

                                    If objPhone.PK_goAML_Act_Entity_Phone < 0 Then
                                        objDB.Entry(objPhone).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If

                            'Entity Director
                            Dim listDirector = objData.list_goAML_Act_Director.Where(Function(x) x.FK_Act_Entity_ID = OldPK).ToList
                            If listDirector IsNot Nothing Then
                                For Each objDirector In listDirector
                                    OldPKSub = objDirector.PK_goAML_Act_Director_ID
                                    objDirector.FK_Report_ID = strReportID
                                    objDirector.FK_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID
                                    objDirector.FK_Sender_Information = GoAMLDAL.GoAMLEnum.enumSenderInformation.Entity
                                    objDirector.FK_Act_Entity_ID = objEntity.PK_goAML_Act_Entity_ID

                                    If objDirector.PK_goAML_Act_Director_ID < 0 Then
                                        objDB.Entry(objDirector).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()

                                    'EntityDirector Address
                                    Dim listDirectorAddress = objData.list_goAML_Act_Director_Address.Where(Function(x) x.FK_Act_Director_ID = OldPKSub).ToList
                                    If listDirectorAddress IsNot Nothing Then
                                        For Each objAddress In listDirectorAddress
                                            objAddress.FK_Report_ID = strReportID
                                            objAddress.FK_Act_Director_ID = objDirector.PK_goAML_Act_Director_ID

                                            If objAddress.PK_goAML_Act_Director_Address_ID < 0 Then
                                                objDB.Entry(objAddress).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'EntityDirector Phone
                                    Dim listDirectorPhone = objData.list_goAML_Act_Director_Phone.Where(Function(x) x.FK_Act_Director_ID = OldPKSub).ToList
                                    If listDirectorPhone IsNot Nothing Then
                                        For Each objPhone In listDirectorPhone
                                            objPhone.FK_Report_ID = strReportID
                                            objPhone.FK_Act_Director_ID = objDirector.PK_goAML_Act_Director_ID

                                            If objPhone.PK_goAML_Act_Director_Phone < 0 Then
                                                objDB.Entry(objPhone).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'EntityDirector Identification
                                    Dim listIdentification = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorEntity And x.FK_Act_Person_ID = OldPKSub).ToList
                                    If listIdentification IsNot Nothing Then
                                        For Each objIdentification In listIdentification
                                            objIdentification.FK_Report_ID = strReportID
                                            objIdentification.FK_Act_Person_ID = objDirector.PK_goAML_Act_Director_ID

                                            If objIdentification.PK_goAML_Activity_Person_Identification_ID < 0 Then
                                                objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If
                                Next
                            End If
                        Next

                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Add/Edit Data finished')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        '===================== END OF NEW ADDED/EDITED DATA

                        '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                        If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Deleted Data started')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                            '1. Account
                            For Each item_old In objData_Old.list_goAML_Act_Account
                                Dim objCek = objData.list_goAML_Act_Account.Find(Function(x) x.PK_goAML_Act_Account_ID = item_old.PK_goAML_Act_Account_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory
                            For Each item_old In objData_Old.list_goAML_Act_acc_Signatory
                                Dim objCek = objData.list_goAML_Act_acc_Signatory.Find(Function(x) x.PK_goAML_Act_acc_Signatory_ID = item_old.PK_goAML_Act_acc_Signatory_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Address
                            For Each item_old In objData_Old.list_goAML_Act_Acc_sign_Address
                                Dim objCek = objData.list_goAML_Act_Acc_sign_Address.Find(Function(x) x.PK_goAML_Act_Acc_Entity_Address_ID = item_old.PK_goAML_Act_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Phone
                            For Each item_old In objData_Old.list_goAML_Act_Acc_sign_Phone
                                Dim objCek = objData.list_goAML_Act_Acc_sign_Phone.Find(Function(x) x.PK_goAML_act_acc_sign_Phone_ID = item_old.PK_goAML_act_acc_sign_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity
                            For Each item_old In objData_Old.list_goAML_Act_Entity_Account
                                Dim objCek = objData.list_goAML_Act_Entity_Account.Find(Function(x) x.PK_goAML_Act_Entity_account = item_old.PK_goAML_Act_Entity_account)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Address
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Address
                                Dim objCek = objData.list_goAML_Act_Acc_Entity_Address.Find(Function(x) x.PK_Act_Acc_Entity_Address_ID = item_old.PK_Act_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Phone
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Phone
                                Dim objCek = objData.list_goAML_Act_Acc_Entity_Phone.Find(Function(x) x.PK_goAML_Act_Acc_Entity_Phone_ID = item_old.PK_goAML_Act_Acc_Entity_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Ent_Director
                                Dim objCek = objData.list_goAML_Act_Acc_Ent_Director.Find(Function(x) x.PK_Act_Acc_Ent_Director_ID = item_old.PK_Act_Acc_Ent_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Director_address
                                Dim objCek = objData.list_goAML_Act_Acc_Entity_Director_address.Find(Function(x) x.PK_goAML_Act_Acc_Entity_Director_address_ID = item_old.PK_goAML_Act_Acc_Entity_Director_address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Director_Phone
                                Dim objCek = objData.list_goAML_Act_Acc_Entity_Director_Phone.Find(Function(x) x.PK_goAML_Act_Acc_Entity_Director_Phone_ID = item_old.PK_goAML_Act_Acc_Entity_Director_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '2. Person
                            For Each item_old In objData_Old.list_goAML_Act_Person
                                Dim objCek = objData.list_goAML_Act_Person.Find(Function(x) x.PK_goAML_Act_Person_ID = item_old.PK_goAML_Act_Person_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Address
                            For Each item_old In objData_Old.list_goAML_Act_Person_Address
                                Dim objCek = objData.list_goAML_Act_Person_Address.Find(Function(x) x.PK_goAML_Act_Person_Address_ID = item_old.PK_goAML_Act_Person_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Phone
                            For Each item_old In objData_Old.list_goAML_Act_Person_Phone
                                Dim objCek = objData.list_goAML_Act_Person_Phone.Find(Function(x) x.PK_goAML_Act_Person_Phone_ID = item_old.PK_goAML_Act_Person_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '3. Entity
                            For Each item_old In objData_Old.list_goAML_Act_Entity
                                Dim objCek = objData.list_goAML_Act_Entity.Find(Function(x) x.PK_goAML_Act_Entity_ID = item_old.PK_goAML_Act_Entity_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Address
                            For Each item_old In objData_Old.list_goAML_Act_Entity_Address
                                Dim objCek = objData.list_goAML_Act_Entity_Address.Find(Function(x) x.PK_goAML_Act_Entity_Address_ID = item_old.PK_goAML_Act_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Phone
                            For Each item_old In objData_Old.list_goAML_Act_Entity_Phone
                                Dim objCek = objData.list_goAML_Act_Entity_Phone.Find(Function(x) x.PK_goAML_Act_Entity_Phone = item_old.PK_goAML_Act_Entity_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director
                            For Each item_old In objData_Old.list_goAML_Act_Director
                                Dim objCek = objData.list_goAML_Act_Director.Find(Function(x) x.PK_goAML_Act_Director_ID = item_old.PK_goAML_Act_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Act_Director_Address
                                Dim objCek = objData.list_goAML_Act_Director_Address.Find(Function(x) x.PK_goAML_Act_Director_Address_ID = item_old.PK_goAML_Act_Director_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_Act_Director_Phone
                                Dim objCek = objData.list_goAML_Act_Director_Phone.Find(Function(x) x.PK_goAML_Act_Director_Phone = item_old.PK_goAML_Act_Director_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Deleted Data finished')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            '===================== END OF _DeleteD DATA
                        End If

                        objDB.SaveChanges()
                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                'Save Audit Trail
                SaveActivity_AuditTrail(objData, objData_Old, objDB, objModule, intModuleAction)
            End Using

            'Jalankan validasi satuan untuk replace IsValid dan Error Message
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Validate Satuan Started (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.BigInt
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_ValidasiSatuan", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Validate Satuan Finished (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveActivity_AuditTrail(objData As GoAML_Activity_Class, objData_Old As GoAML_Activity_Class, objDB As GoAMLEntities, objModule As NawaDAL.Module, intModuleAction As Integer)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("goAML_Act_ReportPartyType")

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
            '18-Mar-2022 Adi : Fixing Audit Trail
            If objData_Old Is Nothing OrElse IsNothing(objData_Old.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID) OrElse objData_Old.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID = 0 Then
                intModuleAction = 1
            Else
                intModuleAction = 2
            End If
            Dim objAuditTrailheader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, Nothing, intAuditTrailStatus, intModuleAction, objModule.ModuleLabel)

            'Save Audit Trail Detail by XML
            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

            Dim listActivity As New List(Of goAML_Act_ReportPartyType)
            Dim listActivity_Old As New List(Of goAML_Act_ReportPartyType)

            listActivity.Add(objData.obj_goAML_Act_ReportPartyType)
            listActivity_Old.Add(objData_Old.obj_goAML_Act_ReportPartyType)
            NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, listActivity, listActivity_Old)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Account, objData_Old.list_goAML_Act_Account)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_acc_Signatory, objData_Old.list_goAML_Act_acc_Signatory)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_sign_Address, objData_Old.list_goAML_Act_Acc_sign_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_sign_Phone, objData_Old.list_goAML_Act_Acc_sign_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Entity_Account, objData_Old.list_goAML_Act_Entity_Account)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_Entity_Address, objData_Old.list_goAML_Act_Acc_Entity_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_Entity_Phone, objData_Old.list_goAML_Act_Acc_Entity_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_Ent_Director, objData_Old.list_goAML_Act_Acc_Ent_Director)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_Entity_Director_address, objData_Old.list_goAML_Act_Acc_Entity_Director_address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_Entity_Director_Phone, objData_Old.list_goAML_Act_Acc_Entity_Director_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Person, objData_Old.list_goAML_Act_Person)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Person_Address, objData_Old.list_goAML_Act_Person_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Person_Phone, objData_Old.list_goAML_Act_Person_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Entity, objData_Old.list_goAML_Act_Entity)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Entity_Address, objData_Old.list_goAML_Act_Entity_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Entity_Phone, objData_Old.list_goAML_Act_Entity_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Director, objData_Old.list_goAML_Act_Director)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Director_Address, objData_Old.list_goAML_Act_Director_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Director_Phone, objData_Old.list_goAML_Act_Director_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Activity_Person_Identification, objData_Old.list_goAML_Activity_Person_Identification)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            'End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteActivityByActivityID(strActivityID As String)
        Try
            'Define local variables
            Dim objModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("goAML_Act_ReportPartyType")
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Get Old Data to Delete
            Dim objData_Old = GetGoAMLActivityClassByID(strActivityID)
            If objData_Old Is Nothing Then
                objData_Old = New GoAML_Activity_Class
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Activity started (Activity ID = " & strActivityID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Using objDB As New GoAMLDAL.GoAMLEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Delete Activity
                        objDB.Entry(objData_Old.obj_goAML_Act_ReportPartyType).State = Entity.EntityState.Deleted

                        '1. Account
                        For Each item_old In objData_Old.list_goAML_Act_Account
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory
                        For Each item_old In objData_Old.list_goAML_Act_acc_Signatory
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory Address
                        For Each item_old In objData_Old.list_goAML_Act_Acc_sign_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory Phone
                        For Each item_old In objData_Old.list_goAML_Act_Acc_sign_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity
                        For Each item_old In objData_Old.list_goAML_Act_Entity_Account
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Address
                        For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Phone
                        For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Director
                        For Each item_old In objData_Old.list_goAML_Act_Acc_Ent_Director
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Director Address
                        For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Director_address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Director Phone
                        For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Director_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '2. Person
                        For Each item_old In objData_Old.list_goAML_Act_Person
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Person Address
                        For Each item_old In objData_Old.list_goAML_Act_Person_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Person Phone
                        For Each item_old In objData_Old.list_goAML_Act_Person_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '3. Entity
                        For Each item_old In objData_Old.list_goAML_Act_Entity
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Address
                        For Each item_old In objData_Old.list_goAML_Act_Entity_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Phone
                        For Each item_old In objData_Old.list_goAML_Act_Entity_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Director
                        For Each item_old In objData_Old.list_goAML_Act_Director
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Director Address
                        For Each item_old In objData_Old.list_goAML_Act_Director_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Director Phone
                        For Each item_old In objData_Old.list_goAML_Act_Director_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        objDB.SaveChanges()
                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Activity finished (Activity ID = " & strActivityID & ")')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            End Using

            'Save Audit Trail
            DeleteActivityByActivityID_AuditTrail(objData_Old, objModule)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteActivityByActivityID_AuditTrail(objData_Old As GoAML_Activity_Class, objModule As NawaDAL.Module)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("goAML_Act_ReportPartyType")

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Using objDB As New GoAMLDAL.GoAMLEntities
                'Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                '18-Mar-2022 Adi : Fixing Audit Trail
                Dim objAuditTrailheader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, Nothing, intAuditTrailStatus, 3, objModule.ModuleLabel)

                'NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData_Old.obj_goAML_Act_ReportPartyType)

                'Save Audit Trail Detail by XML
                Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

                Dim listActivityToDelete As New List(Of goAML_Act_ReportPartyType)
                listActivityToDelete.Add(objData_Old.obj_goAML_Act_ReportPartyType)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, listActivityToDelete)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_acc_Signatory)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Acc_sign_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Acc_sign_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Entity_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Acc_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Acc_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Acc_Ent_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Acc_Entity_Director_address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Acc_Entity_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Person)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Person_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Person_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Activity_Person_Identification)
            End Using

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteActivityByReportID(strReportID As String)
        Try
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Activity started (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.BigInt
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_DeleteActivity_ByReportID", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Activity finished (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveIndicator(objModule As NawaDAL.Module, objData As goAML_Report_Indicator, strReportID As String)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel

            objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("goAML_Report_Indicator")

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            If objData.PK_Report_Indicator < 0 Then
                intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert
            End If

            'Get Old Data for Audit Trail
            Dim objData_Old = New goAML_Report_Indicator
            If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                objData_Old = GetGoAMLReportIndicatorByID(objData.PK_Report_Indicator)
            End If

            Using objDB As New GoAMLDAL.GoAMLEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        If objData.PK_Report_Indicator < 0 Then
                            objDB.Entry(objData).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(objData).State = Entity.EntityState.Modified
                        End If
                        objDB.SaveChanges()

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try

                    'Audit Trail Header
                    Dim objAuditTrailheader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)

                    Dim listIndicator As New List(Of goAML_Report_Indicator)
                    Dim listIndicator_Old As New List(Of goAML_Report_Indicator)

                    listIndicator.Add(objData)
                    listIndicator_Old.Add(objData_Old)

                    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, listIndicator, listIndicator_Old)

                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteIndicatorByIndicatorID(strIndicatorID As String)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
            Dim objModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("goAML_Report_Indicator")
            Dim strModuleName As String = objModule.ModuleLabel

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Get Old Data to Delete
            Dim objData_Old = New goAML_Report_Indicator
            objData_Old = GetGoAMLReportIndicatorByID(strIndicatorID)

            Using objDB As New GoAMLDAL.GoAMLEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Delete Data
                        objDB.Entry(objData_Old).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try

                    'Audit Trail Header
                    Dim objAuditTrailheader As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    'NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData_Old)

                    Dim listIndicatorToDelete As New List(Of goAML_Report_Indicator)
                    listIndicatorToDelete.Add(objData_Old)
                    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, Nothing, listIndicatorToDelete)

                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "Import Data from GoAML Ref"
    Shared Function GetBiPartyAccountSignatoryClassByAccountID(objData As GoAML_Transaction_Class, strPK_Account_ID As String) As GoAML_Transaction_Class
        Try
            Dim objAccount = objData.list_goAML_Transaction_Account.Where(Function(x) x.PK_Account_ID = strPK_Account_ID).FirstOrDefault

            'Hapus data2 Signatory yang lama (jika ada)
            If objAccount IsNot Nothing Then
                Dim listSignatory_Old = objData.list_goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = objAccount.PK_Account_ID).ToList
                If listSignatory_Old IsNot Nothing Then
                    For Each objSignatory_Old In listSignatory_Old
                        objData.list_goAML_Trn_acc_Signatory.Remove(objSignatory_Old)

                        'Signatory Address
                        Dim listAddress_Old = objData.list_goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = objSignatory_Old.PK_goAML_Trn_acc_Signatory_ID).ToList
                        If listAddress_Old IsNot Nothing Then
                            For Each objAddress_Old In listAddress_Old
                                objData.list_goAML_Trn_Acc_sign_Address.Remove(objAddress_Old)
                            Next
                        End If

                        'Signatory Phone
                        Dim listPhone_Old = objData.list_goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = objSignatory_Old.PK_goAML_Trn_acc_Signatory_ID).ToList
                        If listPhone_Old IsNot Nothing Then
                            For Each objPhone_Old In listPhone_Old
                                objData.list_goAML_trn_acc_sign_Phone.Remove(objPhone_Old)
                            Next
                        End If

                        'Signatory Identification
                        Dim listIdentification_Old = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory And x.FK_Person_ID = objSignatory_Old.PK_goAML_Trn_acc_Signatory_ID).ToList
                        If listIdentification_Old IsNot Nothing Then
                            For Each objIdentification_Old In listIdentification_Old
                                objData.list_goAML_Transaction_Person_Identification.Remove(objIdentification_Old)
                            Next
                        End If

                        'Add Ripan 28-Jul-2023 ( Remove Existing List Data )
                        'Signatory Email
                        Dim listEmail_Old = objData.list_goAML_Transaction_Account_Signatory_Email.Where(Function(x) x.FK_Parent_Table_ID = objSignatory_Old.PK_goAML_Trn_acc_Signatory_ID).ToList
                        If listEmail_Old IsNot Nothing Then
                            For Each objEmail_Old In listEmail_Old
                                objData.list_goAML_Transaction_Account_Signatory_Email.Remove(objEmail_Old)
                            Next
                        End If

                        'Signatory Sanction
                        Dim listSanction_Old = objData.list_goAML_Transaction_Account_Signatory_Sanctions.Where(Function(x) x.FK_Parent_Table_ID = objSignatory_Old.PK_goAML_Trn_acc_Signatory_ID).ToList
                        If listSanction_Old IsNot Nothing Then
                            For Each objSanction_Old In listSanction_Old
                                objData.list_goAML_Transaction_Account_Signatory_Sanctions.Remove(objSanction_Old)
                            Next
                        End If

                        'Signatory PEP
                        Dim listPEP_Old = objData.list_goAML_Transaction_Account_Signatory_PEP.Where(Function(x) x.FK_Parent_Table_ID = objSignatory_Old.PK_goAML_Trn_acc_Signatory_ID).ToList
                        If listPEP_Old IsNot Nothing Then
                            For Each objPEP_Old In listPEP_Old
                                objData.list_goAML_Transaction_Account_Signatory_PEP.Remove(objPEP_Old)
                            Next
                        End If

                        'Signatory Related Person
                        Dim listRelatedPerson_Old = objData.list_goAML_Transaction_Account_Signatory_RelatedPerson.Where(Function(x) x.FK_Parent_Table_ID = objSignatory_Old.PK_goAML_Trn_acc_Signatory_ID).ToList
                        If listRelatedPerson_Old IsNot Nothing Then
                            For Each objRelatedPerson_Old In listRelatedPerson_Old
                                objData.list_goAML_Transaction_Account_Signatory_RelatedPerson.Remove(objRelatedPerson_Old)
                            Next
                        End If
                        'End Ripan
                    Next
                End If
            End If

            'Masukkan data2 baru ke dalam Class
            Using objDB As New GoAMLDAL.GoAMLEntities
                If objAccount IsNot Nothing Then
                    'Get Last PK
                    Dim lngPK_Signatory As Long = 0
                    Dim lngPK_Address As Long = 0
                    Dim lngPK_Phone As Long = 0
                    Dim lngPK_Identification As Long = 0
                    Dim lngPK_Email As Long = 0
                    Dim lngPK_Sanction As Long = 0
                    Dim lngPK_PEP As Long = 0
                    Dim lngPK_RelatedPerson As Long = 0

                    'PK Signatory
                    If objData.list_goAML_Trn_acc_Signatory.Count > 0 Then
                        lngPK_Signatory = objData.list_goAML_Trn_acc_Signatory.Min(Function(x) x.PK_goAML_Trn_acc_Signatory_ID)
                        If lngPK_Signatory > 0 Then
                            lngPK_Signatory = 0
                        End If
                    End If

                    'PK Signatory Address
                    If objData.list_goAML_Trn_Acc_sign_Address.Count > 0 Then
                        lngPK_Address = objData.list_goAML_Trn_Acc_sign_Address.Min(Function(x) x.PK_goAML_Trn_Acc_Entity_Address_ID)
                        If lngPK_Address > 0 Then
                            lngPK_Address = 0
                        End If
                    End If

                    'PK Signatory Phone
                    If objData.list_goAML_trn_acc_sign_Phone.Count > 0 Then
                        lngPK_Phone = objData.list_goAML_trn_acc_sign_Phone.Min(Function(x) x.PK_goAML_trn_acc_sign_Phone)
                        If lngPK_Phone > 0 Then
                            lngPK_Phone = 0
                        End If
                    End If

                    'PK Signatory Identification
                    If objData.list_goAML_Transaction_Person_Identification.Count > 0 Then
                        lngPK_Identification = objData.list_goAML_Transaction_Person_Identification.Min(Function(x) x.PK_goAML_Transaction_Person_Identification_ID)
                        If lngPK_Identification > 0 Then
                            lngPK_Identification = 0
                        End If
                    End If

                    'Add Ripan 28-Jul-2023 ( Set PK )
                    'PK Signatory Email
                    If objData.list_goAML_Transaction_Account_Signatory_Email.Count > 0 Then
                        lngPK_Email = objData.list_goAML_Transaction_Account_Signatory_Email.Min(Function(x) x.PK_goAML_Report_Email_ID)
                        If lngPK_Email > 0 Then
                            lngPK_Email = 0
                        End If
                    End If

                    'PK Signatory Sanction
                    If objData.list_goAML_Transaction_Account_Signatory_Sanctions.Count > 0 Then
                        lngPK_Sanction = objData.list_goAML_Transaction_Account_Signatory_Sanctions.Min(Function(x) x.PK_goAML_Report_Sanction_ID)
                        If lngPK_Sanction > 0 Then
                            lngPK_Sanction = 0
                        End If
                    End If

                    'PK Signatory PEP
                    If objData.list_goAML_Transaction_Account_Signatory_PEP.Count > 0 Then
                        lngPK_PEP = objData.list_goAML_Transaction_Account_Signatory_PEP.Min(Function(x) x.PK_goAML_Report_Person_PEP_ID)
                        If lngPK_PEP > 0 Then
                            lngPK_PEP = 0
                        End If
                    End If

                    'PK Signatory Related Person
                    If objData.list_goAML_Transaction_Account_Signatory_RelatedPerson.Count > 0 Then
                        lngPK_PEP = objData.list_goAML_Transaction_Account_Signatory_RelatedPerson.Min(Function(x) x.PK_goAML_Report_Related_Person_ID)
                        If lngPK_PEP > 0 Then
                            lngPK_PEP = 0
                        End If
                    End If

                    'End Ripan

                    'Masukkand data2 Signatory dari goAML Ref
                    Dim objListgoAML_Ref_Account_Signatory As List(Of goAML_Ref_Account_Signatory) = objDB.goAML_Ref_Account_Signatory.Where(Function(x) x.FK_Account_No = objAccount.Account).ToList
                    If objListgoAML_Ref_Account_Signatory IsNot Nothing Then
                        For Each objSignatory In objListgoAML_Ref_Account_Signatory

                            If objSignatory.isCustomer Then     'Jika Sigantory adalah Nasabah
                                Dim objSignatory_New As New goAML_Trn_acc_Signatory

                                'Get Signatory Nasabah
                                Dim objCIFPerson = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objSignatory.FK_CIF_Person_ID).FirstOrDefault
                                If objCIFPerson IsNot Nothing Then
                                    With objSignatory_New
                                        lngPK_Signatory = lngPK_Signatory - 1

                                        .PK_goAML_Trn_acc_Signatory_ID = lngPK_Signatory
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Transaction_Account_ID = objAccount.PK_Account_ID
                                        .FK_From_Or_To = objAccount.FK_From_Or_To

                                        'Load Person CIF Detail
                                        If objCIFPerson IsNot Nothing Then
                                            .Gender = objCIFPerson.INDV_Gender
                                            .Title = objCIFPerson.INDV_Title
                                            .First_Name = objCIFPerson.INDV_First_name
                                            .Middle_Name = objCIFPerson.INDV_Middle_name
                                            .Prefix = objCIFPerson.INDV_Prefix
                                            .Last_Name = objCIFPerson.INDV_Last_Name
                                            .BirthDate = objCIFPerson.INDV_BirthDate
                                            .Birth_Place = objCIFPerson.INDV_Birth_Place
                                            .Mothers_Name = objCIFPerson.INDV_Mothers_Name
                                            .Alias = objCIFPerson.INDV_Alias
                                            .SSN = objCIFPerson.INDV_SSN
                                            .Passport_Number = objCIFPerson.INDV_Passport_Number
                                            .Passport_Country = objCIFPerson.INDV_Passport_Country
                                            .Id_Number = objCIFPerson.INDV_ID_Number
                                            .Nationality1 = objCIFPerson.INDV_Nationality1
                                            .Nationality2 = objCIFPerson.INDV_Nationality2
                                            .Nationality3 = objCIFPerson.INDV_Nationality3
                                            .Residence = objCIFPerson.INDV_Residence
                                            .Email = objCIFPerson.INDV_Email
                                            .email2 = objCIFPerson.INDV_Email2
                                            .email3 = objCIFPerson.INDV_Email3
                                            .email4 = objCIFPerson.INDV_Email4
                                            .email5 = objCIFPerson.INDV_Email5
                                            .Occupation = objCIFPerson.INDV_Occupation
                                            .Employer_Name = objCIFPerson.INDV_Employer_Name
                                            .Deceased = objCIFPerson.INDV_Deceased
                                            .Deceased_Date = objCIFPerson.INDV_Deceased_Date
                                            .Tax_Number = objCIFPerson.INDV_Tax_Number
                                            .Tax_Reg_Number = objCIFPerson.INDV_Tax_Reg_Number
                                            .Source_Of_Wealth = objCIFPerson.INDV_Source_of_Wealth
                                            .Comment = objCIFPerson.INDV_Comments

                                            .isPrimary = objSignatory.isPrimary
                                            .role = objSignatory.Role
                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End If
                                        objData.list_goAML_Trn_acc_Signatory.Add(objSignatory_New)

                                        'Signatory Address(es)  (1=Address, 5=Employer Address)
                                        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_To_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                            For Each objAddress In objListAddress
                                                Dim objNewSignatoryAddress As New goAML_Trn_Acc_sign_Address
                                                With objNewSignatoryAddress
                                                    lngPK_Address = lngPK_Address - 1

                                                    .PK_goAML_Trn_Acc_Entity_Address_ID = lngPK_Address
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_Acc_Entity = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID

                                                    .Address_Type = objAddress.Address_Type
                                                    .Address = objAddress.Address
                                                    .Town = objAddress.Town
                                                    .City = objAddress.City
                                                    .Zip = objAddress.Zip
                                                    .Country_Code = objAddress.Country_Code
                                                    .State = objAddress.State
                                                    .Comments = objAddress.Comments

                                                    If objAddress.FK_Ref_Detail_Of = 1 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Trn_Acc_sign_Address.Add(objNewSignatoryAddress)
                                            Next
                                        End If

                                        'Signatory Phone(s) (1=Phone, 5=Employer Phone)
                                        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_for_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                            For Each objPhone In objListPhone
                                                Dim objNewSignatoryPhone As New goAML_trn_acc_sign_Phone
                                                With objNewSignatoryPhone
                                                    lngPK_Phone = lngPK_Phone - 1

                                                    .PK_goAML_trn_acc_sign_Phone = lngPK_Phone
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_Acc_Entity = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                    .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                    .tph_country_prefix = objPhone.tph_country_prefix
                                                    .tph_number = objPhone.tph_number
                                                    .tph_extension = objPhone.tph_extension
                                                    .comments = objPhone.comments

                                                    If objPhone.FK_Ref_Detail_Of = 1 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_trn_acc_sign_Phone.Add(objNewSignatoryPhone)
                                            Next
                                        End If

                                        'Signatory Identification(s)
                                        Dim objListIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                            For Each objIdentification As GoAMLDAL.goAML_Person_Identification In objListIdentification
                                                Dim objNewSignatoryIdentification As New GoAMLDAL.goAML_Transaction_Person_Identification
                                                With objNewSignatoryIdentification
                                                    lngPK_Identification = lngPK_Identification - 1

                                                    .PK_goAML_Transaction_Person_Identification_ID = lngPK_Identification
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory
                                                    .FK_Person_ID = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .from_or_To_Type = objAccount.FK_From_Or_To

                                                    .Type = objIdentification.Type
                                                    .Number = objIdentification.Number
                                                    .Issue_Date = objIdentification.Issue_Date
                                                    .Expiry_Date = objIdentification.Expiry_Date
                                                    .Issued_By = objIdentification.Issued_By
                                                    .Issued_Country = objIdentification.Issued_Country
                                                    .Identification_Comment = objIdentification.Identification_Comment

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Transaction_Person_Identification.Add(objNewSignatoryIdentification)
                                            Next
                                        End If

                                        'Add Ripan 28-Jul-2023 ( Add Get Data Signatory Email, Sanction & PEP )
                                        'Signatory Email(s)
                                        Dim objListEmail As List(Of GoAMLDAL.goAML_Ref_Customer_Email) = objDB.goAML_Ref_Customer_Email.Where(Function(x) x.FK_REF_DETAIL_OF = 1 And x.FK_FOR_TABLE_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListEmail IsNot Nothing AndAlso objListEmail.Count > 0 Then
                                            For Each objEmail As GoAMLDAL.goAML_Ref_Customer_Email In objListEmail
                                                Dim objNewSignatoryEmail As New GoAMLDAL.goAML_Report_Email
                                                With objNewSignatoryEmail
                                                    lngPK_Email = lngPK_Email - 1

                                                    .PK_goAML_Report_Email_ID = lngPK_Email
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Parent_Table_ID = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .FK_From_Or_To = objAccount.FK_From_Or_To

                                                    .email_address = objEmail.EMAIL_ADDRESS
                                                    .Parent_Table_Name = "goAML_Trn_acc_Signatory"

                                                    .Active = objEmail.Active
                                                    .CreatedBy = objEmail.CreatedBy
                                                    .CreatedDate = objEmail.CreatedDate
                                                End With
                                                objData.list_goAML_Transaction_Account_Signatory_Email.Add(objNewSignatoryEmail)
                                            Next
                                        End If

                                        'Signatory Sanction(s)
                                        Dim objListSanction As List(Of GoAMLDAL.goAML_Ref_Customer_Sanction) = objDB.goAML_Ref_Customer_Sanction.Where(Function(x) x.FK_REF_DETAIL_OF = 1 And x.FK_FOR_TABLE_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListSanction IsNot Nothing AndAlso objListSanction.Count > 0 Then
                                            For Each objSanction As GoAMLDAL.goAML_Ref_Customer_Sanction In objListSanction
                                                Dim objNewSignatorySanction As New GoAMLDAL.goAML_Report_Sanction
                                                With objNewSignatorySanction
                                                    lngPK_Sanction = lngPK_Sanction - 1

                                                    .PK_goAML_Report_Sanction_ID = lngPK_Sanction
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Parent_Table_ID = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .FK_From_Or_To = objAccount.FK_From_Or_To

                                                    .provider = objSanction.PROVIDER
                                                    .sanction_list_name = objSanction.SANCTION_LIST_NAME
                                                    .link_to_source = objSanction.LINK_TO_SOURCE
                                                    .match_criteria = objSanction.MATCH_CRITERIA
                                                    .sanction_list_attributes = objSanction.SANCTION_LIST_ATTRIBUTES
                                                    .comments = objSanction.COMMENTS
                                                    .Parent_Table_Name = "goAML_Trn_acc_Signatory"

                                                    .Active = objSanction.Active
                                                    .CreatedBy = objSanction.CreatedBy
                                                    .CreatedDate = objSanction.CreatedDate
                                                End With
                                                objData.list_goAML_Transaction_Account_Signatory_Sanctions.Add(objNewSignatorySanction)
                                            Next
                                        End If

                                        'Signatory Sanction(s)
                                        Dim objListPEP As List(Of GoAMLDAL.goAML_Ref_Customer_PEP) = objDB.goAML_Ref_Customer_PEP.Where(Function(x) x.FK_REF_DETAIL_OF = 1 And x.FK_FOR_TABLE_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListPEP IsNot Nothing AndAlso objListPEP.Count > 0 Then
                                            For Each objPEP As GoAMLDAL.goAML_Ref_Customer_PEP In objListPEP
                                                Dim objNewSignatoryPEP As New GoAMLDAL.goAML_Report_Person_PEP
                                                With objNewSignatoryPEP
                                                    lngPK_PEP = lngPK_PEP - 1

                                                    .PK_goAML_Report_Person_PEP_ID = lngPK_PEP
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Parent_Table_ID = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .FK_From_Or_To = objAccount.FK_From_Or_To

                                                    .pep_country = objPEP.PEP_COUNTRY
                                                    .function_name = objPEP.FUNCTION_NAME
                                                    .function_description = objPEP.FUNCTION_DESCRIPTION
                                                    .comments = objPEP.COMMENTS
                                                    .Parent_Table_Name = "goAML_Trn_acc_Signatory"

                                                    .Active = objPEP.Active
                                                    .CreatedBy = objPEP.CreatedBy
                                                    .CreatedDate = objPEP.CreatedDate
                                                End With
                                                objData.list_goAML_Transaction_Account_Signatory_PEP.Add(objNewSignatoryPEP)
                                            Next
                                        End If

                                        'Signatory Related Person(s)
                                        Dim objGoAML_Transaction_Class_Get_Account_Signatory_Related_Person_Class = GetBiPartyAccountSignatoryRelatedPersonClassBySignatoryID(objData, objSignatory_New.PK_goAML_Trn_acc_Signatory_ID, objCIFPerson.CIF, True)
                                        If objGoAML_Transaction_Class_Get_Account_Signatory_Related_Person_Class IsNot Nothing Then
                                            objData = objGoAML_Transaction_Class_Get_Account_Signatory_Related_Person_Class
                                        End If

                                        'End Ripan
                                    End With
                                End If
                            Else        '====== SIGNATORY NON NASABAH ======
                                Dim objSignatory_New As New goAML_Trn_acc_Signatory

                                'Get Signatory Non Nasabah
                                Dim objWICPerson = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = objSignatory.WIC_No).FirstOrDefault
                                If objWICPerson IsNot Nothing Then
                                    With objSignatory_New
                                        lngPK_Signatory = lngPK_Signatory - 1

                                        .PK_goAML_Trn_acc_Signatory_ID = lngPK_Signatory
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Transaction_Account_ID = objAccount.PK_Account_ID
                                        .FK_From_Or_To = objAccount.FK_From_Or_To

                                        'Load Person CIF Detail
                                        If objWICPerson IsNot Nothing Then
                                            .Gender = objWICPerson.INDV_Gender
                                            .Title = objWICPerson.INDV_Title
                                            .First_Name = objWICPerson.INDV_First_Name
                                            .Middle_Name = objWICPerson.INDV_Middle_Name
                                            .Prefix = objWICPerson.INDV_Prefix
                                            .Last_Name = objWICPerson.INDV_Last_Name
                                            .BirthDate = objWICPerson.INDV_BirthDate
                                            .Birth_Place = objWICPerson.INDV_Birth_Place
                                            .Mothers_Name = objWICPerson.INDV_Mothers_Name
                                            .Alias = objWICPerson.INDV_Alias
                                            .SSN = objWICPerson.INDV_SSN
                                            .Passport_Number = objWICPerson.INDV_Passport_Number
                                            .Passport_Country = objWICPerson.INDV_Passport_Country
                                            .Id_Number = objWICPerson.INDV_ID_Number
                                            .Nationality1 = objWICPerson.INDV_Nationality1
                                            .Nationality2 = objWICPerson.INDV_Nationality2
                                            .Nationality3 = objWICPerson.INDV_Nationality3
                                            .Residence = objWICPerson.INDV_Residence
                                            .Email = objWICPerson.INDV_Email
                                            .email2 = objWICPerson.INDV_Email2
                                            .email3 = objWICPerson.INDV_Email3
                                            .email4 = objWICPerson.INDV_Email4
                                            .email5 = objWICPerson.INDV_Email5
                                            .Occupation = objWICPerson.INDV_Occupation
                                            .Employer_Name = objWICPerson.INDV_Employer_Name
                                            .Deceased = objWICPerson.INDV_Deceased
                                            .Deceased_Date = objWICPerson.INDV_Deceased_Date
                                            .Tax_Number = objWICPerson.INDV_Tax_Number
                                            .Tax_Reg_Number = objWICPerson.INDV_Tax_Reg_Number
                                            .Source_Of_Wealth = objWICPerson.INDV_SumberDana
                                            .Comment = objWICPerson.INDV_Comment

                                            .isPrimary = objSignatory.isPrimary
                                            .role = objSignatory.Role
                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End If
                                        objData.list_goAML_Trn_acc_Signatory.Add(objSignatory_New)

                                        'Signatory Address(es)  (3=Address, 10=Employer Address)
                                        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_To_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                            For Each objAddress In objListAddress
                                                Dim objNewSignatoryAddress As New goAML_Trn_Acc_sign_Address
                                                With objNewSignatoryAddress
                                                    lngPK_Address = lngPK_Address - 1

                                                    .PK_goAML_Trn_Acc_Entity_Address_ID = lngPK_Address
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_Acc_Entity = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID

                                                    .Address_Type = objAddress.Address_Type
                                                    .Address = objAddress.Address
                                                    .Town = objAddress.Town
                                                    .City = objAddress.City
                                                    .Zip = objAddress.Zip
                                                    .Country_Code = objAddress.Country_Code
                                                    .State = objAddress.State
                                                    .Comments = objAddress.Comments

                                                    If objAddress.FK_Ref_Detail_Of = 3 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Trn_Acc_sign_Address.Add(objNewSignatoryAddress)
                                            Next
                                        End If

                                        'Signatory Phone(s) (3=Phone, 10=Employer Phone)
                                        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_for_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                            For Each objPhone In objListPhone
                                                Dim objNewSignatoryPhone As New goAML_trn_acc_sign_Phone
                                                With objNewSignatoryPhone
                                                    lngPK_Phone = lngPK_Phone - 1

                                                    .PK_goAML_trn_acc_sign_Phone = lngPK_Phone
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_Acc_Entity = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                    .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                    .tph_country_prefix = objPhone.tph_country_prefix
                                                    .tph_number = objPhone.tph_number
                                                    .tph_extension = objPhone.tph_extension
                                                    .comments = objPhone.comments

                                                    If objPhone.FK_Ref_Detail_Of = 3 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_trn_acc_sign_Phone.Add(objNewSignatoryPhone)
                                            Next
                                        End If

                                        'Signatory Identification(s)
                                        Dim objListIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 8 And x.FK_Person_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                            For Each objIdentification As GoAMLDAL.goAML_Person_Identification In objListIdentification
                                                Dim objNewSignatoryIdentification As New GoAMLDAL.goAML_Transaction_Person_Identification
                                                With objNewSignatoryIdentification
                                                    lngPK_Identification = lngPK_Identification - 1

                                                    .PK_goAML_Transaction_Person_Identification_ID = lngPK_Identification
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory
                                                    .FK_Person_ID = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .from_or_To_Type = objAccount.FK_From_Or_To

                                                    .Type = objIdentification.Type
                                                    .Number = objIdentification.Number
                                                    .Issue_Date = objIdentification.Issue_Date
                                                    .Expiry_Date = objIdentification.Expiry_Date
                                                    .Issued_By = objIdentification.Issued_By
                                                    .Issued_Country = objIdentification.Issued_Country
                                                    .Identification_Comment = objIdentification.Identification_Comment

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Transaction_Person_Identification.Add(objNewSignatoryIdentification)
                                            Next
                                        End If

                                        'Add Ripan 28-Jul-2023 ( Add Get Data Signatory Email, Sanction & PEP )
                                        'Signatory Email(s)
                                        Dim objListEmail As List(Of GoAMLDAL.goAML_Ref_Customer_Email) = objDB.goAML_Ref_Customer_Email.Where(Function(x) x.FK_REF_DETAIL_OF = 3 And x.FK_FOR_TABLE_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListEmail IsNot Nothing AndAlso objListEmail.Count > 0 Then
                                            For Each objEmail As GoAMLDAL.goAML_Ref_Customer_Email In objListEmail
                                                Dim objNewSignatoryEmail As New GoAMLDAL.goAML_Report_Email
                                                With objNewSignatoryEmail
                                                    lngPK_Email = lngPK_Email - 1

                                                    .PK_goAML_Report_Email_ID = lngPK_Email
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Parent_Table_ID = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .FK_From_Or_To = objAccount.FK_From_Or_To

                                                    .email_address = objEmail.EMAIL_ADDRESS
                                                    .Parent_Table_Name = "goAML_Trn_acc_Signatory"

                                                    .Active = objEmail.Active
                                                    .CreatedBy = objEmail.CreatedBy
                                                    .CreatedDate = objEmail.CreatedDate
                                                End With
                                                objData.list_goAML_Transaction_Account_Signatory_Email.Add(objNewSignatoryEmail)
                                            Next
                                        End If

                                        'Signatory Sanction(s)
                                        Dim objListSanction As List(Of GoAMLDAL.goAML_Ref_Customer_Sanction) = objDB.goAML_Ref_Customer_Sanction.Where(Function(x) x.FK_REF_DETAIL_OF = 3 And x.FK_FOR_TABLE_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListSanction IsNot Nothing AndAlso objListSanction.Count > 0 Then
                                            For Each objSanction As GoAMLDAL.goAML_Ref_Customer_Sanction In objListSanction
                                                Dim objNewSignatorySanction As New GoAMLDAL.goAML_Report_Sanction
                                                With objNewSignatorySanction
                                                    lngPK_Sanction = lngPK_Sanction - 1

                                                    .PK_goAML_Report_Sanction_ID = lngPK_Sanction
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Parent_Table_ID = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .FK_From_Or_To = objAccount.FK_From_Or_To

                                                    .provider = objSanction.PROVIDER
                                                    .sanction_list_name = objSanction.SANCTION_LIST_NAME
                                                    .link_to_source = objSanction.LINK_TO_SOURCE
                                                    .match_criteria = objSanction.MATCH_CRITERIA
                                                    .sanction_list_attributes = objSanction.SANCTION_LIST_ATTRIBUTES
                                                    .comments = objSanction.COMMENTS
                                                    .Parent_Table_Name = "goAML_Trn_acc_Signatory"

                                                    .Active = objSanction.Active
                                                    .CreatedBy = objSanction.CreatedBy
                                                    .CreatedDate = objSanction.CreatedDate
                                                End With
                                                objData.list_goAML_Transaction_Account_Signatory_Sanctions.Add(objNewSignatorySanction)
                                            Next
                                        End If

                                        'Signatory Sanction(s)
                                        Dim objListPEP As List(Of GoAMLDAL.goAML_Ref_Customer_PEP) = objDB.goAML_Ref_Customer_PEP.Where(Function(x) x.FK_REF_DETAIL_OF = 3 And x.FK_FOR_TABLE_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListPEP IsNot Nothing AndAlso objListPEP.Count > 0 Then
                                            For Each objPEP As GoAMLDAL.goAML_Ref_Customer_PEP In objListPEP
                                                Dim objNewSignatoryPEP As New GoAMLDAL.goAML_Report_Person_PEP
                                                With objNewSignatoryPEP
                                                    lngPK_PEP = lngPK_PEP - 1

                                                    .PK_goAML_Report_Person_PEP_ID = lngPK_PEP
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Parent_Table_ID = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .FK_From_Or_To = objAccount.FK_From_Or_To

                                                    .pep_country = objPEP.PEP_COUNTRY
                                                    .function_name = objPEP.FUNCTION_NAME
                                                    .function_description = objPEP.FUNCTION_DESCRIPTION
                                                    .comments = objPEP.COMMENTS
                                                    .Parent_Table_Name = "goAML_Trn_acc_Signatory"

                                                    .Active = objPEP.Active
                                                    .CreatedBy = objPEP.CreatedBy
                                                    .CreatedDate = objPEP.CreatedDate
                                                End With
                                                objData.list_goAML_Transaction_Account_Signatory_PEP.Add(objNewSignatoryPEP)
                                            Next
                                        End If

                                        'Signatory Related Person(s)
                                        Dim objGoAML_Transaction_Class_Get_Account_Signatory_Related_Person_Class = GetBiPartyAccountSignatoryRelatedPersonClassBySignatoryID(objData, objSignatory_New.PK_goAML_Trn_acc_Signatory_ID, objWICPerson.WIC_No, False)
                                        If objGoAML_Transaction_Class_Get_Account_Signatory_Related_Person_Class IsNot Nothing Then
                                            objData = objGoAML_Transaction_Class_Get_Account_Signatory_Related_Person_Class
                                        End If

                                        'End Ripan
                                    End With
                                End If
                            End If
                        Next
                    End If
                End If
            End Using

            'Balikin lagi datanya ke Front End
            Return objData
        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetBiPartyAccountEntityClassByAccountID(objData As GoAML_Transaction_Class, strPK_Account_ID As String) As GoAML_Transaction_Class
        Try
            Dim objAccount = objData.list_goAML_Transaction_Account.Where(Function(x) x.PK_Account_ID = strPK_Account_ID).FirstOrDefault

            'Hapus data2 yang lama (jika ada)
            If objAccount IsNot Nothing Then
                Dim objAccountEntity = objData.list_goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = objAccount.PK_Account_ID).FirstOrDefault
                If objAccountEntity IsNot Nothing Then
                    'Account Entity Address
                    Dim listAccountEntityAddress = objData.list_goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account).ToList
                    If listAccountEntityAddress IsNot Nothing Then
                        For Each objAddress_Old In listAccountEntityAddress
                            objData.list_goAML_Trn_Acc_Entity_Address.Remove(objAddress_Old)
                        Next
                    End If

                    'Account Entity Phone
                    Dim listAccountEntityPhone = objData.list_goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account).ToList
                    If listAccountEntityPhone IsNot Nothing Then
                        For Each objPhone_Old In listAccountEntityPhone
                            objData.list_goAML_Trn_Acc_Entity_Phone.Remove(objPhone_Old)
                        Next
                    End If

                    'Director
                    Dim listDirector_Old = objData.list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = GoAMLDAL.GoAMLEnum.enumSenderInformation.Account And x.FK_Entity_ID = objAccountEntity.PK_goAML_Trn_Entity_account).ToList
                    If listDirector_Old IsNot Nothing Then
                        For Each objDirector_Old In listDirector_Old
                            objData.list_goAML_Trn_Director.Remove(objDirector_Old)

                            'Director Address
                            Dim listAddress_Old = objData.list_goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = objDirector_Old.PK_goAML_Trn_Director_ID).ToList
                            If listAddress_Old IsNot Nothing Then
                                For Each objAddress_Old In listAddress_Old
                                    objData.list_goAML_Trn_Director_Address.Remove(objAddress_Old)
                                Next
                            End If

                            'Director Phone
                            Dim listPhone_Old = objData.list_goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = objDirector_Old.PK_goAML_Trn_Director_ID).ToList
                            If listPhone_Old IsNot Nothing Then
                                For Each objPhone_Old In listPhone_Old
                                    objData.list_goAML_trn_Director_Phone.Remove(objPhone_Old)
                                Next
                            End If

                            'Director Identification
                            Dim listIdentification_Old = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount And x.FK_Person_ID = objDirector_Old.PK_goAML_Trn_Director_ID).ToList
                            If listIdentification_Old IsNot Nothing Then
                                For Each objIdentification_Old In listIdentification_Old
                                    objData.list_goAML_Transaction_Person_Identification.Remove(objIdentification_Old)
                                Next
                            End If

                            '' Add 12-Oct-2023
                            'Director PEP
                            Dim listPEP_Old = objData.list_goAML_Transaction_Account_EntityAccount_Director_PEP.Where(Function(x) x.FK_Parent_Table_ID = objDirector_Old.PK_goAML_Trn_Director_ID).ToList
                            If listPEP_Old IsNot Nothing Then
                                For Each objPEP_Old In listPEP_Old
                                    objData.list_goAML_Transaction_Account_EntityAccount_Director_PEP.Remove(objPEP_Old)
                                Next
                            End If

                            'Director Sanction
                            Dim listSanction_Old = objData.list_goAML_Transaction_Account_EntityAccount_Director_Sanctions.Where(Function(x) x.FK_Parent_Table_ID = objDirector_Old.PK_goAML_Trn_Director_ID).ToList
                            If listSanction_Old IsNot Nothing Then
                                For Each objSanction_Old In listSanction_Old
                                    objData.list_goAML_Transaction_Account_EntityAccount_Director_Sanctions.Remove(objSanction_Old)
                                Next
                            End If

                            'Director Email
                            Dim listEmail_Old = objData.list_goAML_Transaction_Account_EntityAccount_Director_Email.Where(Function(x) x.FK_Parent_Table_ID = objDirector_Old.PK_goAML_Trn_Director_ID).ToList
                            If listEmail_Old IsNot Nothing Then
                                For Each objEmail_Old In listEmail_Old
                                    objData.list_goAML_Transaction_Account_EntityAccount_Director_Email.Remove(objEmail_Old)
                                Next
                            End If
                            '' End 12-Oct-2023
                        Next
                    End If

                    '' Add 13-Sep-2023, Felix. 
                    ' Email Account Entity
                    Dim listAccountEntityEmail = objData.list_goAML_Transaction_Account_EntityAccount_Email.Where(Function(x) x.FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account And x.Parent_Table_Name = "goAML_Trn_Entity_Account").ToList
                    If listAccountEntityEmail IsNot Nothing Then
                        For Each objEmail_Old In listAccountEntityEmail
                            objData.list_goAML_Transaction_Account_EntityAccount_Email.Remove(objEmail_Old)
                        Next
                    End If

                    ' Website
                    Dim listAccountEntityWebsite = objData.list_goAML_Transaction_Account_EntityAccount_URL.Where(Function(x) x.FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account And x.Parent_Table_Name = "goAML_Trn_Entity_Account").ToList
                    If listAccountEntityWebsite IsNot Nothing Then
                        For Each objURL_Old In listAccountEntityWebsite
                            objData.list_goAML_Transaction_Account_EntityAccount_URL.Remove(objURL_Old)
                        Next
                    End If

                    ' Entity Identification
                    Dim listAccountEntityIdentification = objData.list_goAML_Transaction_Account_EntityAccount_Identification.Where(Function(x) x.FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account And x.Parent_Table_Name = "goAML_Trn_Entity_Account").ToList
                    If listAccountEntityIdentification IsNot Nothing Then
                        For Each objEntID_Old In listAccountEntityIdentification
                            objData.list_goAML_Transaction_Account_EntityAccount_Identification.Remove(objEntID_Old)
                        Next
                    End If

                    ' Sanction
                    Dim listAccountEntitySanction = objData.list_goAML_Transaction_Account_EntityAccount_Sanctions.Where(Function(x) x.FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account And x.Parent_Table_Name = "goAML_Trn_Entity_Account").ToList
                    If listAccountEntitySanction IsNot Nothing Then
                        For Each objSanction_Old In listAccountEntitySanction
                            objData.list_goAML_Transaction_Account_EntityAccount_Sanctions.Remove(objSanction_Old)
                        Next
                    End If

                    ' Related Entity
                    Dim listAccountEntityRelEntity = objData.list_goAML_Transaction_Account_EntityAccount_RelatedEntity.Where(Function(x) x.FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account And x.Parent_Table_Name = "goAML_Trn_Entity_Account").ToList
                    If listAccountEntityRelEntity IsNot Nothing Then
                        For Each objRelEnt_Old In listAccountEntityRelEntity
                            objData.list_goAML_Transaction_Account_EntityAccount_RelatedEntity.Remove(objRelEnt_Old)
                        Next
                    End If
                    '' End 13-Sep-2023
                End If
            End If

            'Masukkan data2 baru ke dalam Class
            Using objDB As New GoAMLDAL.GoAMLEntities
                If objAccount IsNot Nothing Then
                    Dim objAccountEntity = objData.list_goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = objAccount.PK_Account_ID).FirstOrDefault
                    If objAccount IsNot Nothing Then
                        'Get Last PK
                        Dim lngPK_Entity_Address As Long = 0
                        Dim lngPK_Entity_Phone As Long = 0

                        '' Add 14-Sep-2023, Felix. 
                        Dim lngPK_Entity_Email As Long = 0
                        Dim lngPK_Entity_Website As Long = 0
                        Dim lngPK_Entity_Identification As Long = 0
                        Dim lngPK_Entity_Sanction As Long = 0
                        Dim lngPK_Entity_RelatedEntity As Long = 0

                        Dim lngPK_Entity_RelatedEntity_Address As Long = 0
                        Dim lngPK_Entity_RelatedEntity_Phone As Long = 0
                        Dim lngPK_Entity_RelatedEntity_Email As Long = 0
                        Dim lngPK_Entity_RelatedEntity_Identification As Long = 0
                        Dim lngPK_Entity_RelatedEntity_URL As Long = 0
                        Dim lngPK_Entity_RelatedEntity_Sanction As Long = 0
                        '' End 14-Sep-2023

                        Dim lngPK_Director As Long = 0
                        Dim lngPK_Director_Address As Long = 0
                        Dim lngPK_Director_Phone As Long = 0
                        Dim lngPK_Director_Identification As Long = 0
                        '' Add 12-Oct-2023
                        Dim lngPK_Director_PEP As Long = 0
                        Dim lngPK_Director_Sanction As Long = 0
                        Dim lngPK_Director_Email As Long = 0
                        '' End 12-Oct-2023

                        'PK Entity Address
                        If objData.list_goAML_Trn_Acc_Entity_Address.Count > 0 Then
                            lngPK_Director_Address = objData.list_goAML_Trn_Acc_Entity_Address.Min(Function(x) x.PK_Trn_Acc_Entity_Address_ID)
                            If lngPK_Director_Address > 0 Then
                                lngPK_Director_Address = 0
                            End If
                        End If

                        'PK Entity Phone
                        If objData.list_goAML_Trn_Acc_Entity_Phone.Count > 0 Then
                            lngPK_Director_Phone = objData.list_goAML_Trn_Acc_Entity_Phone.Min(Function(x) x.PK_goAML_Trn_Acc_Entity_Phone)
                            If lngPK_Director_Phone > 0 Then
                                lngPK_Director_Phone = 0
                            End If
                        End If

                        '' Add 14-Sep-2023, Felix. 
                        ' Email Account Entity
                        If objData.list_goAML_Transaction_Account_EntityAccount_Email.Count > 0 Then
                            lngPK_Entity_Email = objData.list_goAML_Transaction_Account_EntityAccount_Email.Min(Function(x) x.PK_goAML_Report_Email_ID)
                            If lngPK_Entity_Email > 0 Then
                                lngPK_Entity_Email = 0
                            End If
                        End If

                        ' Website
                        If objData.list_goAML_Transaction_Account_EntityAccount_URL.Count > 0 Then
                            lngPK_Entity_Website = objData.list_goAML_Transaction_Account_EntityAccount_URL.Min(Function(x) x.PK_goAML_Report_Url_ID)
                            If lngPK_Entity_Website > 0 Then
                                lngPK_Entity_Website = 0
                            End If
                        End If

                        ' Entity Identification
                        If objData.list_goAML_Transaction_Account_EntityAccount_Identification.Count > 0 Then
                            lngPK_Entity_Identification = objData.list_goAML_Transaction_Account_EntityAccount_Identification.Min(Function(x) x.PK_goAML_Report_Identifications_ID)
                            If lngPK_Entity_Identification > 0 Then
                                lngPK_Entity_Identification = 0
                            End If
                        End If

                        ' Sanction
                        If objData.list_goAML_Transaction_Account_EntityAccount_Sanctions.Count > 0 Then
                            lngPK_Entity_Sanction = objData.list_goAML_Transaction_Account_EntityAccount_Sanctions.Min(Function(x) x.PK_goAML_Report_Sanction_ID)
                            If lngPK_Entity_Sanction > 0 Then
                                lngPK_Entity_Sanction = 0
                            End If
                        End If

                        ' Related Entity
                        If objData.list_goAML_Transaction_Account_EntityAccount_RelatedEntity.Count > 0 Then
                            lngPK_Entity_RelatedEntity = objData.list_goAML_Transaction_Account_EntityAccount_RelatedEntity.Min(Function(x) x.PK_goAML_Report_Related_Entities_ID)
                            If lngPK_Entity_RelatedEntity > 0 Then
                                lngPK_Entity_RelatedEntity = 0
                            End If
                        End If

                        ' Related Entity Address
                        If objData.list_goAML_Transaction_Account_EntityAccount_RE_Address.Count > 0 Then
                            lngPK_Entity_RelatedEntity_Address = objData.list_goAML_Transaction_Account_EntityAccount_RE_Address.Min(Function(x) x.PK_Acc_Ent_Rel_Ent_Address_ID)
                            If lngPK_Entity_RelatedEntity_Address > 0 Then
                                lngPK_Entity_RelatedEntity_Address = 0
                            End If
                        End If

                        ' Related Entity Phone
                        If objData.list_goAML_Transaction_Account_EntityAccount_RE_Phone.Count > 0 Then
                            lngPK_Entity_RelatedEntity_Phone = objData.list_goAML_Transaction_Account_EntityAccount_RE_Phone.Min(Function(x) x.PK_goAML_Acc_Entity_Related_Entity_Phone_ID)
                            If lngPK_Entity_RelatedEntity_Phone > 0 Then
                                lngPK_Entity_RelatedEntity_Phone = 0
                            End If
                        End If

                        ' Related Entity Email
                        If objData.list_goAML_Transaction_Account_EntityAccount_RE_Email.Count > 0 Then
                            lngPK_Entity_RelatedEntity_Email = objData.list_goAML_Transaction_Account_EntityAccount_RE_Email.Min(Function(x) x.PK_goAML_Report_Email_ID)
                            If lngPK_Entity_RelatedEntity_Email > 0 Then
                                lngPK_Entity_RelatedEntity_Email = 0
                            End If
                        End If

                        ' Related Entity Identification
                        If objData.list_goAML_Transaction_Account_EntityAccount_RE_Identification.Count > 0 Then
                            lngPK_Entity_RelatedEntity_Identification = objData.list_goAML_Transaction_Account_EntityAccount_RE_Identification.Min(Function(x) x.PK_goAML_Report_Identifications_ID)
                            If lngPK_Entity_RelatedEntity_Identification > 0 Then
                                lngPK_Entity_RelatedEntity_Identification = 0
                            End If
                        End If

                        ' Related Entity Url
                        If objData.list_goAML_Transaction_Account_EntityAccount_RE_URL.Count > 0 Then
                            lngPK_Entity_RelatedEntity_URL = objData.list_goAML_Transaction_Account_EntityAccount_RE_URL.Min(Function(x) x.PK_goAML_Report_Url_ID)
                            If lngPK_Entity_RelatedEntity_URL > 0 Then
                                lngPK_Entity_RelatedEntity_URL = 0
                            End If
                        End If

                        ' Related Entity Sanction
                        If objData.list_goAML_Transaction_Account_EntityAccount_RE_Sanctions.Count > 0 Then
                            lngPK_Entity_RelatedEntity_Sanction = objData.list_goAML_Transaction_Account_EntityAccount_RE_Sanctions.Min(Function(x) x.PK_goAML_Report_Sanction_ID)
                            If lngPK_Entity_RelatedEntity_Sanction > 0 Then
                                lngPK_Entity_RelatedEntity_Sanction = 0
                            End If
                        End If
                        '' End 14-Sep-2023

                        'PK Director
                        If objData.list_goAML_Trn_Director.Count > 0 Then
                            '' Edit 28-Sep-2022
                            'lngPK_Director = objData.list_goAML_Trn_Director.Min(Function(x) x.PK_goAML_Trn_Director_ID)

                            Dim listDirectorAccount As List(Of GoAMLDAL.goAML_Trn_Director) = objData.list_goAML_Trn_Director.Where(Function(x) x.PK_goAML_Trn_Director_ID <= -1 And x.PK_goAML_Trn_Director_ID > -100).ToList

                            If listDirectorAccount.Count <> 0 Then
                                lngPK_Director = listDirectorAccount.Min(Function(x) x.PK_goAML_Trn_Director_ID)
                            End If
                            '' End Edit 28-Sep-2022

                            If lngPK_Director > 0 Then
                                lngPK_Director = 0
                            End If
                        End If

                        'PK Director Address
                        If objData.list_goAML_Trn_Director_Address.Count > 0 Then
                            '' Edit 28-Sep-2022
                            'lngPK_Director_Address = objData.list_goAML_Trn_Director_Address.Min(Function(x) x.PK_goAML_Trn_Director_Address_ID)

                            Dim listAddressDirectorAccount As List(Of GoAMLDAL.goAML_Trn_Director_Address) = objData.list_goAML_Trn_Director_Address.Where(Function(x) x.PK_goAML_Trn_Director_Address_ID <= -1 And x.PK_goAML_Trn_Director_Address_ID > -100).ToList

                            If listAddressDirectorAccount.Count <> 0 Then
                                lngPK_Director_Address = listAddressDirectorAccount.Min(Function(x) x.PK_goAML_Trn_Director_Address_ID)
                            End If
                            '' End Edit 28-Sep-2022

                            If lngPK_Director_Address > 0 Then
                                lngPK_Director_Address = 0
                            End If
                        End If

                        'PK Director Phone
                        If objData.list_goAML_trn_Director_Phone.Count > 0 Then
                            '' Edit 28-Sep-2022
                            'lngPK_Director_Phone = objData.list_goAML_trn_Director_Phone.Min(Function(x) x.PK_goAML_trn_Director_Phone)

                            Dim listPhoneDirectorAccount As List(Of GoAMLDAL.goAML_trn_Director_Phone) = objData.list_goAML_trn_Director_Phone.Where(Function(x) x.PK_goAML_trn_Director_Phone <= -1 And x.PK_goAML_trn_Director_Phone > -100).ToList

                            If listPhoneDirectorAccount.Count <> 0 Then
                                lngPK_Director_Phone = listPhoneDirectorAccount.Min(Function(x) x.PK_goAML_trn_Director_Phone)
                            End If
                            '' End Edit 28-Sep-2022
                            If lngPK_Director_Phone > 0 Then
                                lngPK_Director_Phone = 0
                            End If
                        End If

                        'PK Director Identification
                        If objData.list_goAML_Transaction_Person_Identification.Count > 0 Then
                            '' Edit 29-Sep-2022
                            'lngPK_Director_Identification = objData.list_goAML_Transaction_Person_Identification.Min(Function(x) x.PK_goAML_Transaction_Person_Identification_ID)

                            Dim listIdentificationDirectorAccount As List(Of GoAMLDAL.goAML_Transaction_Person_Identification) = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.PK_goAML_Transaction_Person_Identification_ID <= -1 And x.PK_goAML_Transaction_Person_Identification_ID > -100).ToList

                            If listIdentificationDirectorAccount.Count <> 0 Then
                                lngPK_Director_Identification = listIdentificationDirectorAccount.Min(Function(x) x.PK_goAML_Transaction_Person_Identification_ID)
                            End If
                            '' End Edit 29-Sep-2022

                            If lngPK_Director_Identification > 0 Then
                                lngPK_Director_Identification = 0
                            End If
                        End If

                        '' Add 12-Oct-2023, Felix
                        'PK Director PEP
                        If objData.list_goAML_Transaction_Account_EntityAccount_Director_PEP.Count > 0 Then

                            Dim listPEPDirectorAccount As List(Of GoAMLDAL.goAML_Report_Person_PEP) = objData.list_goAML_Transaction_Account_EntityAccount_Director_PEP.Where(Function(x) x.PK_goAML_Report_Person_PEP_ID <= -1 And x.PK_goAML_Report_Person_PEP_ID > -100).ToList

                            If listPEPDirectorAccount.Count <> 0 Then
                                lngPK_Director_PEP = listPEPDirectorAccount.Min(Function(x) x.PK_goAML_Report_Person_PEP_ID)
                            End If
                            '' End Edit 29-Sep-2022

                            If lngPK_Director_PEP > 0 Then
                                lngPK_Director_PEP = 0
                            End If
                        End If

                        'PK Director Sanction
                        If objData.list_goAML_Transaction_Account_EntityAccount_Director_Sanctions.Count > 0 Then

                            Dim listSanctionDirectorAccount As List(Of GoAMLDAL.goAML_Report_Sanction) = objData.list_goAML_Transaction_Account_EntityAccount_Director_Sanctions.Where(Function(x) x.PK_goAML_Report_Sanction_ID <= -1 And x.PK_goAML_Report_Sanction_ID > -100).ToList

                            If listSanctionDirectorAccount.Count <> 0 Then
                                lngPK_Director_Sanction = listSanctionDirectorAccount.Min(Function(x) x.PK_goAML_Report_Sanction_ID)
                            End If
                            '' End Edit 29-Sep-2022

                            If lngPK_Director_Sanction > 0 Then
                                lngPK_Director_Sanction = 0
                            End If
                        End If

                        'PK Director Email
                        If objData.list_goAML_Transaction_Account_EntityAccount_Director_Email.Count > 0 Then

                            Dim listEmailDirectorAccount As List(Of GoAMLDAL.goAML_Report_Email) = objData.list_goAML_Transaction_Account_EntityAccount_Director_Email.Where(Function(x) x.PK_goAML_Report_Email_ID <= -1 And x.PK_goAML_Report_Email_ID > -100).ToList

                            If listEmailDirectorAccount.Count <> 0 Then
                                lngPK_Director_Email = listEmailDirectorAccount.Min(Function(x) x.PK_goAML_Report_Email_ID)
                            End If
                            '' End Edit 29-Sep-2022

                            If lngPK_Director_Email > 0 Then
                                lngPK_Director_Email = 0
                            End If
                        End If
                        '' End 12-Oct-2023

                        'Masukkan data2 Entity Address & Phone dari goAML Ref
                        'Account Entity Address(es)
                        Dim objCustomer = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objAccount.Client_Number).FirstOrDefault
                        If objCustomer IsNot Nothing Then
                            Dim objListEntityAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = objCustomer.PK_Customer_ID).ToList
                            If objListEntityAddress IsNot Nothing AndAlso objListEntityAddress.Count > 0 Then
                                For Each objAddress In objListEntityAddress
                                    Dim objNewEntityAddress As New goAML_Trn_Acc_Entity_Address
                                    With objNewEntityAddress
                                        lngPK_Entity_Address = lngPK_Entity_Address - 1

                                        .PK_Trn_Acc_Entity_Address_ID = lngPK_Entity_Address
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account

                                        .Address_Type = objAddress.Address_Type
                                        .Address = objAddress.Address
                                        .Town = objAddress.Town
                                        .City = objAddress.City
                                        .Zip = objAddress.Zip
                                        .Country_Code = objAddress.Country_Code
                                        .State = objAddress.State
                                        .Comments = objAddress.Comments

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Trn_Acc_Entity_Address.Add(objNewEntityAddress)
                                Next
                            End If

                            'Entity Phone(s)
                            Dim objListEntityPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = objCustomer.PK_Customer_ID).ToList
                            If objListEntityPhone IsNot Nothing AndAlso objListEntityPhone.Count > 0 Then
                                For Each objPhone In objListEntityPhone
                                    Dim objNewEntityPhone As New goAML_Trn_Acc_Entity_Phone
                                    With objNewEntityPhone
                                        lngPK_Entity_Phone = lngPK_Entity_Phone - 1

                                        .PK_goAML_Trn_Acc_Entity_Phone = lngPK_Entity_Phone
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account

                                        .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                        .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                        .tph_country_prefix = objPhone.tph_country_prefix
                                        .tph_number = objPhone.tph_number
                                        .tph_extension = objPhone.tph_extension
                                        .comments = objPhone.comments

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Trn_Acc_Entity_Phone.Add(objNewEntityPhone)
                                Next
                            End If

                            '' Add 14-Sep-2023, Felix. 
                            'Entity Email
                            Dim objListEntityEmail = objDB.goAML_Ref_Customer_Email.Where(Function(x) x.FK_REF_DETAIL_OF = 1 And x.CIF = objCustomer.CIF).ToList
                            If objListEntityEmail IsNot Nothing AndAlso objListEntityEmail.Count > 0 Then
                                For Each objEmail In objListEntityEmail
                                    Dim objNewEntityEmail As New goAML_Report_Email
                                    With objNewEntityEmail
                                        lngPK_Entity_Email = lngPK_Entity_Email - 1

                                        .PK_goAML_Report_Email_ID = lngPK_Entity_Email
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account
                                        .Parent_Table_Name = "goAML_Trn_Entity_Account"
                                        .FK_From_Or_To = objAccountEntity.FK_From_Or_To

                                        .email_address = objEmail.EMAIL_ADDRESS

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Transaction_Account_EntityAccount_Email.Add(objNewEntityEmail)
                                Next
                            End If

                            'Entity Url / Website
                            Dim objListEntityURL = objDB.goAML_Ref_Customer_URL.Where(Function(x) x.FK_REF_DETAIL_OF = 1 And x.CIF = objCustomer.CIF).ToList
                            If objListEntityURL IsNot Nothing AndAlso objListEntityURL.Count > 0 Then
                                For Each objURL In objListEntityURL
                                    Dim objNewEntityURL As New goAML_Report_Url
                                    With objNewEntityURL
                                        lngPK_Entity_Website = lngPK_Entity_Website - 1

                                        .PK_goAML_Report_Url_ID = lngPK_Entity_Website
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account
                                        .Parent_Table_Name = "goAML_Trn_Entity_Account"
                                        .FK_From_Or_To = objAccountEntity.FK_From_Or_To

                                        .url = objURL.URL

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Transaction_Account_EntityAccount_URL.Add(objNewEntityURL)
                                Next
                            End If

                            'Entity Identification
                            Dim objListEntityIdentification = objDB.goAML_Ref_Customer_Entity_Identification.Where(Function(x) x.FK_REF_DETAIL_OF = 1 And x.CIF = objCustomer.CIF).ToList
                            If objListEntityIdentification IsNot Nothing AndAlso objListEntityIdentification.Count > 0 Then
                                For Each objIdentification In objListEntityIdentification
                                    Dim objNewEntityIdentification As New goAML_Report_Identification
                                    With objNewEntityIdentification
                                        lngPK_Entity_Identification = lngPK_Entity_Identification - 1

                                        .PK_goAML_Report_Identifications_ID = lngPK_Entity_Identification
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account
                                        .Parent_Table_Name = "goAML_Trn_Entity_Account"
                                        .FK_From_Or_To = objAccountEntity.FK_From_Or_To

                                        .type = objIdentification.TYPE
                                        .number = objIdentification.NUMBER
                                        .issue_date = objIdentification.ISSUE_DATE
                                        .expiry_date = objIdentification.EXPIRY_DATE
                                        .issued_by = objIdentification.ISSUED_BY
                                        .issue_country = objIdentification.ISSUE_COUNTRY
                                        .comments = objIdentification.COMMENTS

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Transaction_Account_EntityAccount_Identification.Add(objNewEntityIdentification)
                                Next
                            End If

                            'Entity Sanction
                            Dim objListEntitySanction = objDB.goAML_Ref_Customer_Sanction.Where(Function(x) x.FK_REF_DETAIL_OF = 1 And x.CIF = objCustomer.CIF).ToList
                            If objListEntitySanction IsNot Nothing AndAlso objListEntitySanction.Count > 0 Then
                                For Each objSanction In objListEntitySanction
                                    Dim objNewEntitySanction As New goAML_Report_Sanction
                                    With objNewEntitySanction
                                        lngPK_Entity_Sanction = lngPK_Entity_Sanction - 1

                                        .PK_goAML_Report_Sanction_ID = lngPK_Entity_Sanction
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account
                                        .Parent_Table_Name = "goAML_Trn_Entity_Account"
                                        .FK_From_Or_To = objAccountEntity.FK_From_Or_To

                                        .provider = objSanction.PROVIDER
                                        .sanction_list_name = objSanction.SANCTION_LIST_NAME
                                        .match_criteria = objSanction.MATCH_CRITERIA
                                        .link_to_source = objSanction.LINK_TO_SOURCE
                                        .sanction_list_attributes = objSanction.SANCTION_LIST_ATTRIBUTES
                                        .sanction_list_date_range_valid_from = objSanction.SANCTION_LIST_DATE_RANGE_VALID_FROM
                                        .comments = objSanction.COMMENTS

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Transaction_Account_EntityAccount_Sanctions.Add(objNewEntitySanction)
                                Next
                            End If

                            'Entity Related Entity
                            Dim objListEntityRelatedEntity = objDB.goAML_Ref_Customer_Related_Entities.Where(Function(x) x.CIF = objCustomer.CIF).ToList
                            If objListEntityRelatedEntity IsNot Nothing AndAlso objListEntityRelatedEntity.Count > 0 Then
                                For Each objRelatedEntity In objListEntityRelatedEntity
                                    Dim objNewEntityRelatedEntity As New goAML_Report_Related_Entities
                                    With objNewEntityRelatedEntity
                                        lngPK_Entity_Sanction = lngPK_Entity_Sanction - 1

                                        .PK_goAML_Report_Related_Entities_ID = lngPK_Entity_Sanction
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Parent_Table_ID = objAccountEntity.PK_goAML_Trn_Entity_account
                                        .Parent_Table_Name = "goAML_Trn_Entity_Account"
                                        .FK_From_Or_To = objAccountEntity.FK_From_Or_To

                                        .entity_entity_relation = objRelatedEntity.ENTITY_ENTITY_RELATION
                                        .relation_date_range_valid_from = objRelatedEntity.RELATION_DATE_RANGE_VALID_FROM
                                        .relation_date_range_is_approx_from_date = objRelatedEntity.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE
                                        .relation_date_range_valid_to = objRelatedEntity.RELATION_DATE_RANGE_VALID_TO
                                        .relation_date_range_is_approx_to_date = objRelatedEntity.RELATION_DATE_RANGE_IS_APPROX_TO_DATE
                                        .share_percentage = objRelatedEntity.SHARE_PERCENTAGE
                                        .comments = objRelatedEntity.COMMENTS
                                        .name = objRelatedEntity.NAME
                                        .commercial_name = objRelatedEntity.COMMERCIAL_NAME
                                        .incorporation_legal_form = objRelatedEntity.INCORPORATION_LEGAL_FORM
                                        .incorporation_number = objRelatedEntity.INCORPORATION_NUMBER
                                        .business = objRelatedEntity.BUSINESS
                                        .entity_status = objRelatedEntity.ENTITY_STATUS
                                        .entity_status_date = objRelatedEntity.ENTITY_STATUS_DATE
                                        .incorporation_state = objRelatedEntity.INCORPORATION_STATE
                                        .incorporation_country_code = objRelatedEntity.INCORPORATION_COUNTRY_CODE
                                        .incorporation_date = objRelatedEntity.INCORPORATION_DATE
                                        .business_closed = objRelatedEntity.BUSINESS_CLOSED
                                        .date_business_closed = objRelatedEntity.DATE_BUSINESS_CLOSED
                                        .tax_number = objRelatedEntity.TAX_NUMBER
                                        .tax_reg_number = objRelatedEntity.TAX_REG_NUMBER
                                        .relation_comments = objRelatedEntity.relation_comments

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Transaction_Account_EntityAccount_RelatedEntity.Add(objNewEntityRelatedEntity)

                                    'Related Entity Address(es)  (36=Address Related Entity)
                                    Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 36 And x.FK_To_Table_ID = objRelatedEntity.PK_goAML_Ref_Customer_Related_Entities_ID).ToList
                                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                        For Each objAddress In objListAddress
                                            Dim objNewRelEntityAddress As New goAML_Acc_Entity_Related_Entity_Address
                                            With objNewRelEntityAddress
                                                lngPK_Entity_RelatedEntity_Address = lngPK_Entity_RelatedEntity_Address - 1

                                                .PK_Acc_Ent_Rel_Ent_Address_ID = lngPK_Entity_RelatedEntity_Address
                                                .FK_Report_ID = objAccount.FK_Report_ID
                                                .FK_PARENT_TABLE_ID = objNewEntityRelatedEntity.PK_goAML_Report_Related_Entities_ID
                                                .Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity"
                                                .FK_From_Or_To = objNewEntityRelatedEntity.FK_From_Or_To

                                                .Address_Type = objAddress.Address_Type
                                                .Address = objAddress.Address
                                                .Town = objAddress.Town
                                                .City = objAddress.City
                                                .Zip = objAddress.Zip
                                                .Country_Code = objAddress.Country_Code
                                                .State = objAddress.State
                                                .Comments = objAddress.Comments

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objData.list_goAML_Transaction_Account_EntityAccount_RE_Address.Add(objNewRelEntityAddress)
                                        Next
                                    End If

                                    'Related Entity Phone(s) (36=Phone Related Entity)
                                    Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 36 And x.FK_for_Table_ID = objRelatedEntity.PK_goAML_Ref_Customer_Related_Entities_ID).ToList
                                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                        For Each objPhone In objListPhone

                                            Dim objNewRelEntityPhone As New goAML_Acc_Entity_Related_Entity_Phone
                                            With objNewRelEntityPhone
                                                lngPK_Entity_RelatedEntity_Phone = lngPK_Entity_RelatedEntity_Phone - 1

                                                .PK_goAML_Acc_Entity_Related_Entity_Phone_ID = lngPK_Entity_RelatedEntity_Phone
                                                .FK_Report_ID = objAccount.FK_Report_ID
                                                .FK_PARENT_TABLE_ID = objNewEntityRelatedEntity.PK_goAML_Report_Related_Entities_ID
                                                .Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity"
                                                .FK_From_Or_To = objNewEntityRelatedEntity.FK_From_Or_To

                                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                .tph_country_prefix = objPhone.tph_country_prefix
                                                .tph_number = objPhone.tph_number
                                                .tph_extension = objPhone.tph_extension
                                                .comments = objPhone.comments

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objData.list_goAML_Transaction_Account_EntityAccount_RE_Phone.Add(objNewRelEntityPhone)
                                        Next
                                    End If

                                    'Related Entity Email(s) (36=Phone Related Entity)
                                    Dim objListEmail = objDB.goAML_Ref_Customer_Email.Where(Function(x) x.FK_REF_DETAIL_OF = 36 And x.FK_FOR_TABLE_ID = objRelatedEntity.PK_goAML_Ref_Customer_Related_Entities_ID).ToList
                                    If objListEmail IsNot Nothing AndAlso objListEmail.Count > 0 Then
                                        For Each objEmail In objListEmail

                                            Dim objNewRelEntityEmail As New goAML_Report_Email
                                            With objNewRelEntityEmail
                                                lngPK_Entity_RelatedEntity_Email = lngPK_Entity_RelatedEntity_Email - 1

                                                .PK_goAML_Report_Email_ID = lngPK_Entity_RelatedEntity_Phone
                                                .FK_Report_ID = objAccount.FK_Report_ID
                                                .FK_PARENT_TABLE_ID = objNewEntityRelatedEntity.PK_goAML_Report_Related_Entities_ID
                                                .PARENT_TABLE_NAME = "goAML_Trn_Entity_Account_Related_Entity"
                                                .FK_From_Or_To = objNewEntityRelatedEntity.FK_From_Or_To

                                                .email_address = objEmail.EMAIL_ADDRESS

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objData.list_goAML_Transaction_Account_EntityAccount_RE_Email.Add(objNewRelEntityEmail)
                                        Next
                                    End If

                                    'Related Entity Identification(s)
                                    Dim objListIdentification As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Identification) = objDB.goAML_Ref_Customer_Entity_Identification.Where(Function(x) x.FK_REF_DETAIL_OF = 36 And x.FK_FOR_TABLE_ID = objRelatedEntity.PK_goAML_Ref_Customer_Related_Entities_ID).ToList
                                    If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                        For Each objIdentification As GoAMLDAL.goAML_Ref_Customer_Entity_Identification In objListIdentification
                                            Dim objNewRelEntityIdentification As New GoAMLDAL.goAML_Report_Identification
                                            With objNewRelEntityIdentification
                                                lngPK_Entity_RelatedEntity_Identification = lngPK_Entity_RelatedEntity_Identification - 1

                                                .PK_goAML_Report_Identifications_ID = lngPK_Entity_RelatedEntity_Identification
                                                .FK_Report_ID = objAccount.FK_Report_ID
                                                .FK_Parent_Table_ID = objNewEntityRelatedEntity.PK_goAML_Report_Related_Entities_ID
                                                .Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity"
                                                .FK_From_Or_To = objNewEntityRelatedEntity.FK_From_Or_To

                                                .Type = objIdentification.TYPE
                                                .Number = objIdentification.NUMBER
                                                .Issue_Date = objIdentification.ISSUE_DATE
                                                .Expiry_Date = objIdentification.EXPIRY_DATE
                                                .Issued_By = objIdentification.ISSUED_BY
                                                .issue_country = objIdentification.ISSUE_COUNTRY
                                                .comments = objIdentification.COMMENTS

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objData.list_goAML_Transaction_Account_EntityAccount_RE_Identification.Add(objNewRelEntityIdentification)
                                        Next
                                    End If

                                    'Related Entity URL(s)
                                    Dim objListURL As List(Of GoAMLDAL.goAML_Ref_Customer_URL) = objDB.goAML_Ref_Customer_URL.Where(Function(x) x.FK_REF_DETAIL_OF = 36 And x.FK_FOR_TABLE_ID = objRelatedEntity.PK_goAML_Ref_Customer_Related_Entities_ID).ToList
                                    If objListURL IsNot Nothing AndAlso objListURL.Count > 0 Then
                                        For Each objURL As GoAMLDAL.goAML_Ref_Customer_URL In objListURL
                                            Dim objNewRelEntityURL As New GoAMLDAL.goAML_Report_Url
                                            With objNewRelEntityURL
                                                lngPK_Entity_RelatedEntity_URL = lngPK_Entity_RelatedEntity_URL - 1

                                                .PK_goAML_Report_Url_ID = lngPK_Entity_RelatedEntity_URL
                                                .FK_Report_ID = objAccount.FK_Report_ID
                                                .FK_Parent_Table_ID = objNewEntityRelatedEntity.PK_goAML_Report_Related_Entities_ID
                                                .Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity"
                                                .FK_From_Or_To = objNewEntityRelatedEntity.FK_From_Or_To

                                                .url = objURL.URL

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objData.list_goAML_Transaction_Account_EntityAccount_RE_URL.Add(objNewRelEntityURL)
                                        Next
                                    End If

                                    'Related Entity Sanction(s)
                                    Dim objListSanction As List(Of GoAMLDAL.goAML_Ref_Customer_Sanction) = objDB.goAML_Ref_Customer_Sanction.Where(Function(x) x.FK_REF_DETAIL_OF = 36 And x.FK_FOR_TABLE_ID = objRelatedEntity.PK_goAML_Ref_Customer_Related_Entities_ID).ToList
                                    If objListSanction IsNot Nothing AndAlso objListSanction.Count > 0 Then
                                        For Each objSanction As GoAMLDAL.goAML_Ref_Customer_Sanction In objListSanction
                                            Dim objNewRelEntitySanction As New GoAMLDAL.goAML_Report_Sanction
                                            With objNewRelEntitySanction
                                                lngPK_Entity_RelatedEntity_Sanction = lngPK_Entity_RelatedEntity_Sanction - 1

                                                .PK_goAML_Report_Sanction_ID = lngPK_Entity_RelatedEntity_Sanction
                                                .FK_Report_ID = objAccount.FK_Report_ID
                                                .FK_Parent_Table_ID = objNewEntityRelatedEntity.PK_goAML_Report_Related_Entities_ID
                                                .Parent_Table_Name = "goAML_Trn_Entity_Account_Related_Entity"
                                                .FK_From_Or_To = objNewEntityRelatedEntity.FK_From_Or_To

                                                .provider = objSanction.PROVIDER
                                                .sanction_list_name = objSanction.SANCTION_LIST_NAME
                                                .match_criteria = objSanction.MATCH_CRITERIA
                                                .link_to_source = objSanction.LINK_TO_SOURCE
                                                .sanction_list_attributes = objSanction.SANCTION_LIST_ATTRIBUTES
                                                .sanction_list_date_range_valid_from = objSanction.SANCTION_LIST_DATE_RANGE_VALID_FROM
                                                .sanction_list_date_range_is_approx_from_date = objSanction.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE
                                                .sanction_list_date_range_valid_to = objSanction.SANCTION_LIST_DATE_RANGE_VALID_TO
                                                .sanction_list_date_range_is_approx_to_date = objSanction.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE
                                                .comments = objSanction.COMMENTS

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objData.list_goAML_Transaction_Account_EntityAccount_RE_Sanctions.Add(objNewRelEntitySanction)
                                        Next
                                    End If
                                Next
                            End If
                            '' End 14-Sep-2023.
                        End If

                        'Masukkand data2 Director dari goAML Ref
                        Dim objListgoAML_Ref_Account_Director As List(Of goAML_Ref_Customer_Entity_Director) = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.FK_Entity_ID = objAccount.Client_Number).ToList
                        If objListgoAML_Ref_Account_Director IsNot Nothing Then
                            For Each objDirector In objListgoAML_Ref_Account_Director
                                Dim objDirector_New As New goAML_Trn_Director
                                With objDirector_New
                                    lngPK_Director = lngPK_Director - 1

                                    .PK_goAML_Trn_Director_ID = lngPK_Director
                                    .FK_Report_ID = objAccount.FK_Report_ID
                                    .FK_Transaction_ID = objAccount.FK_Report_Transaction_ID
                                    .FK_From_Or_To = objAccount.FK_From_Or_To
                                    .FK_Sender_Information = GoAMLDAL.GoAMLEnum.enumSenderInformation.Account
                                    .FK_Entity_ID = objAccountEntity.PK_goAML_Trn_Entity_account

                                    .Gender = objDirector.Gender
                                    .Title = objDirector.Title
                                    .First_Name = objDirector.First_name
                                    .Middle_Name = objDirector.Middle_Name
                                    .Prefix = objDirector.Prefix
                                    .Last_Name = objDirector.Last_Name
                                    .BirthDate = objDirector.BirthDate
                                    .Birth_Place = objDirector.Birth_Place
                                    .Mothers_Name = objDirector.Mothers_Name
                                    .Alias = objDirector.Alias
                                    .SSN = objDirector.SSN
                                    .Passport_Number = objDirector.Passport_Number
                                    .Passport_Country = objDirector.Passport_Country
                                    .Id_Number = objDirector.ID_Number
                                    .Nationality1 = objDirector.Nationality1
                                    .Nationality2 = objDirector.Nationality2
                                    .Nationality3 = objDirector.Nationality3
                                    .Residence = objDirector.Residence
                                    .Email = objDirector.Email
                                    .email2 = objDirector.Email2
                                    .email3 = objDirector.Email3
                                    .email4 = objDirector.Email4
                                    .email5 = objDirector.Email5
                                    .Occupation = objDirector.Occupation
                                    .Employer_Name = objDirector.Employer_Name
                                    .Deceased = objDirector.Deceased
                                    .Deceased_Date = objDirector.Deceased_Date
                                    .Tax_Number = objDirector.Tax_Number
                                    .Tax_Reg_Number = objDirector.Tax_Reg_Number
                                    .Source_Of_Wealth = objDirector.Source_of_Wealth
                                    .Comment = objDirector.Comments

                                    .role = objDirector.Role
                                    .Active = 1
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                End With
                                objData.list_goAML_Trn_Director.Add(objDirector_New)

                                'Director Address(es)  (6=Address, 7=Employer Address)
                                Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_To_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                    For Each objAddress In objListAddress
                                        Dim objNewDirectorAddress As New goAML_Trn_Director_Address
                                        With objNewDirectorAddress
                                            lngPK_Director_Address = lngPK_Director_Address - 1

                                            .PK_goAML_Trn_Director_Address_ID = lngPK_Director_Address
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Trn_Director_ID = objDirector_New.PK_goAML_Trn_Director_ID

                                            .Address_Type = objAddress.Address_Type
                                            .Address = objAddress.Address
                                            .Town = objAddress.Town
                                            .City = objAddress.City
                                            .Zip = objAddress.Zip
                                            .Country_Code = objAddress.Country_Code
                                            .State = objAddress.State
                                            .Comments = objAddress.Comments

                                            If objAddress.FK_Ref_Detail_Of = 6 Then
                                                .isEmployer = False
                                            Else
                                                .isEmployer = True
                                            End If

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Trn_Director_Address.Add(objNewDirectorAddress)
                                    Next
                                End If

                                'Director Phone(s) (6=Phone, 7=Employer Phone)
                                Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_for_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                    For Each objPhone In objListPhone
                                        'Dim objNewDirectorPhone As New goAML_trn_acc_sign_Phone
                                        Dim objNewDirectorPhone As New goAML_trn_Director_Phone
                                        With objNewDirectorPhone
                                            lngPK_Director_Phone = lngPK_Director_Phone - 1

                                            .PK_goAML_trn_Director_Phone = lngPK_Director_Phone
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Trn_Director_ID = objDirector_New.PK_goAML_Trn_Director_ID

                                            .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                            .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                            .tph_country_prefix = objPhone.tph_country_prefix
                                            .tph_number = objPhone.tph_number
                                            .tph_extension = objPhone.tph_extension
                                            .comments = objPhone.comments

                                            If objPhone.FK_Ref_Detail_Of = 6 Then
                                                .isEmployer = False
                                            Else
                                                .isEmployer = True
                                            End If

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_trn_Director_Phone.Add(objNewDirectorPhone)
                                    Next
                                End If

                                'Director Identification(s)
                                'Ada 2 faham yang berbeda untuk FK_Person_Type Director Account di goAML_Person_Identification. Yang diketahui SCB pakai 4, BSIM pakai 5
                                'Sesuaikan global parameter 3001. Karena kalau mau disesuaikan semua harus patches data, ubah SP generate CTR, IFTI, dll.
                                Dim intDirectrorAccountPersonType As Integer = getDirectorAccountPersonType()
                                Dim objListIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = intDirectrorAccountPersonType And x.FK_Person_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                    For Each objIdentification As GoAMLDAL.goAML_Person_Identification In objListIdentification
                                        Dim objNewDirectorIdentification As New GoAMLDAL.goAML_Transaction_Person_Identification
                                        With objNewDirectorIdentification
                                            lngPK_Director_Identification = lngPK_Director_Identification - 1

                                            .PK_goAML_Transaction_Person_Identification_ID = lngPK_Director_Identification
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount
                                            .FK_Person_ID = objDirector_New.PK_goAML_Trn_Director_ID
                                            .from_or_To_Type = objAccount.FK_From_Or_To

                                            .Type = objIdentification.Type
                                            .Number = objIdentification.Number
                                            .Issue_Date = objIdentification.Issue_Date
                                            .Expiry_Date = objIdentification.Expiry_Date
                                            .Issued_By = objIdentification.Issued_By
                                            .Issued_Country = objIdentification.Issued_Country
                                            .Identification_Comment = objIdentification.Identification_Comment

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Transaction_Person_Identification.Add(objNewDirectorIdentification)
                                    Next
                                End If

                                'Director PEP (6 = Director )
                                Dim objListPEP As List(Of GoAMLDAL.goAML_Ref_Customer_PEP) = objDB.goAML_Ref_Customer_PEP.Where(Function(x) x.FK_REF_DETAIL_OF = 6 And x.FK_FOR_TABLE_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListPEP IsNot Nothing AndAlso objListPEP.Count > 0 Then
                                    For Each objPEP As GoAMLDAL.goAML_Ref_Customer_PEP In objListPEP
                                        Dim objNewPersonRPPEP As New GoAMLDAL.goAML_Report_Person_PEP
                                        With objNewPersonRPPEP
                                            lngPK_Director_PEP = lngPK_Director_PEP - 1

                                            .PK_goAML_Report_Person_PEP_ID = lngPK_Director_PEP
                                            .FK_Report_ID = objDirector_New.FK_Report_ID
                                            .Parent_Table_Name = "goAML_Trn_Entity_Account_Director"
                                            .FK_Parent_Table_ID = objDirector_New.PK_goAML_Trn_Director_ID
                                            .FK_From_Or_To = objDirector_New.FK_From_Or_To

                                            .pep_country = objPEP.PEP_COUNTRY
                                            .function_name = objPEP.FUNCTION_NAME
                                            .function_description = objPEP.FUNCTION_DESCRIPTION
                                            .comments = objPEP.COMMENTS

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Transaction_Account_EntityAccount_Director_PEP.Add(objNewPersonRPPEP)
                                    Next
                                End If

                                'Director PEP (6 = Director )
                                Dim objListSanction As List(Of GoAMLDAL.goAML_Ref_Customer_Sanction) = objDB.goAML_Ref_Customer_Sanction.Where(Function(x) x.FK_REF_DETAIL_OF = 6 And x.FK_FOR_TABLE_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListSanction IsNot Nothing AndAlso objListSanction.Count > 0 Then
                                    For Each objSanction As GoAMLDAL.goAML_Ref_Customer_Sanction In objListSanction
                                        Dim objNewPersonRPSanction As New GoAMLDAL.goAML_Report_Sanction
                                        With objNewPersonRPSanction
                                            lngPK_Director_Sanction = lngPK_Director_Sanction - 1

                                            .PK_goAML_Report_Sanction_ID = lngPK_Director_Sanction
                                            .FK_Report_ID = objDirector_New.FK_Report_ID
                                            .Parent_Table_Name = "goAML_Trn_Entity_Account_Director"
                                            .FK_Parent_Table_ID = objDirector_New.PK_goAML_Trn_Director_ID
                                            .FK_From_Or_To = objDirector_New.FK_From_Or_To

                                            .provider = objSanction.PROVIDER
                                            .sanction_list_name = objSanction.SANCTION_LIST_NAME
                                            .match_criteria = objSanction.MATCH_CRITERIA
                                            .link_to_source = objSanction.LINK_TO_SOURCE
                                            .comments = objSanction.COMMENTS

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Transaction_Account_EntityAccount_Director_Sanctions.Add(objNewPersonRPSanction)
                                    Next
                                End If

                                'Account Signatory Rel Person Email(s)
                                Dim objListEmail As List(Of GoAMLDAL.goAML_Ref_Customer_Email) = objDB.goAML_Ref_Customer_Email.Where(Function(x) x.FK_REF_DETAIL_OF = 6 And x.FK_FOR_TABLE_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListEmail IsNot Nothing AndAlso objListEmail.Count > 0 Then
                                    For Each objEmail As GoAMLDAL.goAML_Ref_Customer_Email In objListEmail
                                        Dim objNewPersonRPEmail As New GoAMLDAL.goAML_Report_Email
                                        With objNewPersonRPEmail
                                            lngPK_Director_Email = lngPK_Director_Email - 1

                                            .PK_goAML_Report_Email_ID = lngPK_Director_Email
                                            .FK_Report_ID = objDirector_New.FK_Report_ID
                                            .Parent_Table_Name = "goAML_Trn_Entity_Account_Director"
                                            .FK_Parent_Table_ID = objDirector_New.PK_goAML_Trn_Director_ID
                                            .FK_From_Or_To = objDirector_New.FK_From_Or_To

                                            .email_address = objEmail.EMAIL_ADDRESS

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Transaction_Account_EntityAccount_Director_Email.Add(objNewPersonRPEmail)
                                    Next
                                End If


                            Next
                        End If
                    End If
                End If
            End Using

            'Balikin lagi datanya ke Front End
            Return objData
        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetMultiPartyAccountSignatoryClassByAccountID(objData As GoAML_Transaction_Class, strPK_Account_ID As String) As GoAML_Transaction_Class
        Try
            Dim objAccount = objData.list_goAML_Trn_Party_Account.Where(Function(x) x.PK_Trn_Party_Account_ID = strPK_Account_ID).FirstOrDefault

            'Hapus data2 Signatory yang lama (jika ada)
            If objAccount IsNot Nothing Then
                Dim listSignatory_Old = objData.list_goAML_Trn_par_acc_Signatory.Where(Function(x) x.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID).ToList
                If listSignatory_Old IsNot Nothing Then
                    For Each objSignatory_Old In listSignatory_Old
                        objData.list_goAML_Trn_par_acc_Signatory.Remove(objSignatory_Old)

                        'Signatory Address
                        Dim listAddress_Old = objData.list_goAML_Trn_par_Acc_sign_Address.Where(Function(x) x.FK_Trn_par_acc_Signatory_ID = objSignatory_Old.PK_Trn_par_acc_Signatory_ID).ToList
                        If listAddress_Old IsNot Nothing Then
                            For Each objAddress_Old In listAddress_Old
                                objData.list_goAML_Trn_par_Acc_sign_Address.Remove(objAddress_Old)
                            Next
                        End If

                        'Signatory Phone
                        Dim listPhone_Old = objData.list_goAML_trn_par_acc_sign_Phone.Where(Function(x) x.FK_Trn_Par_Acc_Sign = objSignatory_Old.PK_Trn_par_acc_Signatory_ID).ToList
                        If listPhone_Old IsNot Nothing Then
                            For Each objPhone_Old In listPhone_Old
                                objData.list_goAML_trn_par_acc_sign_Phone.Remove(objPhone_Old)
                            Next
                        End If

                        'Signatory Identification
                        Dim listIdentification_Old = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory And x.FK_Person_ID = objSignatory_Old.PK_Trn_par_acc_Signatory_ID).ToList
                        If listIdentification_Old IsNot Nothing Then
                            For Each objIdentification_Old In listIdentification_Old
                                objData.list_goAML_Transaction_Party_Identification.Remove(objIdentification_Old)
                            Next
                        End If
                    Next
                End If
            End If

            'Masukkan data2 baru ke dalam Class
            Using objDB As New GoAMLDAL.GoAMLEntities
                If objAccount IsNot Nothing Then
                    'Get Last PK
                    Dim lngPK_Signatory As Long = 0
                    Dim lngPK_Address As Long = 0
                    Dim lngPK_Phone As Long = 0
                    Dim lngPK_Identification As Long = 0

                    'PK Signatory
                    If objData.list_goAML_Trn_par_acc_Signatory.Count > 0 Then
                        lngPK_Signatory = objData.list_goAML_Trn_par_acc_Signatory.Min(Function(x) x.PK_Trn_par_acc_Signatory_ID)
                        If lngPK_Signatory > 0 Then
                            lngPK_Signatory = 0
                        End If
                    End If

                    'PK Signatory Address
                    If objData.list_goAML_Trn_par_Acc_sign_Address.Count > 0 Then
                        lngPK_Address = objData.list_goAML_Trn_par_Acc_sign_Address.Min(Function(x) x.PK_Trn_Par_Acc_sign_Address_ID)
                        If lngPK_Address > 0 Then
                            lngPK_Address = 0
                        End If
                    End If

                    'PK Signatory Phone
                    If objData.list_goAML_trn_par_acc_sign_Phone.Count > 0 Then
                        lngPK_Phone = objData.list_goAML_trn_par_acc_sign_Phone.Min(Function(x) x.PK_trn_Par_Acc_Sign_Phone)
                        If lngPK_Phone > 0 Then
                            lngPK_Phone = 0
                        End If
                    End If

                    'PK Signatory Identification
                    If objData.list_goAML_Transaction_Party_Identification.Count > 0 Then
                        lngPK_Identification = objData.list_goAML_Transaction_Party_Identification.Min(Function(x) x.PK_Transaction_Party_Identification_ID)
                        If lngPK_Identification > 0 Then
                            lngPK_Identification = 0
                        End If
                    End If

                    'Masukkand data2 Signatory dari goAML Ref
                    Dim objListgoAML_Ref_Account_Signatory As List(Of goAML_Ref_Account_Signatory) = objDB.goAML_Ref_Account_Signatory.Where(Function(x) x.FK_Account_No = objAccount.Account).ToList
                    If objListgoAML_Ref_Account_Signatory IsNot Nothing Then
                        For Each objSignatory In objListgoAML_Ref_Account_Signatory

                            If objSignatory.isCustomer Then     'Jika Sigantory adalah Nasabah
                                Dim objSignatory_New As New goAML_Trn_par_acc_Signatory

                                'Get Signatory Nasabah
                                Dim objCIFPerson = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objSignatory.FK_CIF_Person_ID).FirstOrDefault
                                If objCIFPerson IsNot Nothing Then
                                    With objSignatory_New
                                        lngPK_Signatory = lngPK_Signatory - 1

                                        .PK_Trn_par_acc_Signatory_ID = lngPK_Signatory
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID

                                        'Load Person CIF Detail
                                        If objCIFPerson IsNot Nothing Then
                                            .Gender = objCIFPerson.INDV_Gender
                                            .Title = objCIFPerson.INDV_Title
                                            .First_Name = objCIFPerson.INDV_First_name
                                            .Middle_Name = objCIFPerson.INDV_Middle_name
                                            .Prefix = objCIFPerson.INDV_Prefix
                                            .Last_Name = objCIFPerson.INDV_Last_Name
                                            .BirthDate = objCIFPerson.INDV_BirthDate
                                            .Birth_Place = objCIFPerson.INDV_Birth_Place
                                            .Mothers_Name = objCIFPerson.INDV_Mothers_Name
                                            .Alias = objCIFPerson.INDV_Alias
                                            .SSN = objCIFPerson.INDV_SSN
                                            .Passport_Number = objCIFPerson.INDV_Passport_Number
                                            .Passport_Country = objCIFPerson.INDV_Passport_Country
                                            .Id_Number = objCIFPerson.INDV_ID_Number
                                            .Nationality1 = objCIFPerson.INDV_Nationality1
                                            .Nationality2 = objCIFPerson.INDV_Nationality2
                                            .Nationality3 = objCIFPerson.INDV_Nationality3
                                            .Residence = objCIFPerson.INDV_Residence
                                            .Email = objCIFPerson.INDV_Email
                                            .email2 = objCIFPerson.INDV_Email2
                                            .email3 = objCIFPerson.INDV_Email3
                                            .email4 = objCIFPerson.INDV_Email4
                                            .email5 = objCIFPerson.INDV_Email5
                                            .Occupation = objCIFPerson.INDV_Occupation
                                            .Employer_Name = objCIFPerson.INDV_Employer_Name
                                            .Deceased = objCIFPerson.INDV_Deceased
                                            .Deceased_Date = objCIFPerson.INDV_Deceased_Date
                                            .Tax_Number = objCIFPerson.INDV_Tax_Number
                                            .Tax_Reg_Number = objCIFPerson.INDV_Tax_Reg_Number
                                            .Source_Of_Wealth = objCIFPerson.INDV_Source_of_Wealth
                                            .Comment = objCIFPerson.INDV_Comments

                                            .isPrimary = objSignatory.isPrimary
                                            .role = objSignatory.Role
                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End If
                                        objData.list_goAML_Trn_par_acc_Signatory.Add(objSignatory_New)

                                        'Signatory Address(es)  (1=Address, 5=Employer Address)
                                        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_To_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                            For Each objAddress In objListAddress
                                                Dim objNewSignatoryAddress As New goAML_Trn_par_Acc_sign_Address
                                                With objNewSignatoryAddress
                                                    lngPK_Address = lngPK_Address - 1

                                                    .PK_Trn_Par_Acc_sign_Address_ID = lngPK_Address
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_par_acc_Signatory_ID = objSignatory_New.PK_Trn_par_acc_Signatory_ID

                                                    .Address_Type = objAddress.Address_Type
                                                    .Address = objAddress.Address
                                                    .Town = objAddress.Town
                                                    .City = objAddress.City
                                                    .Zip = objAddress.Zip
                                                    .Country_Code = objAddress.Country_Code
                                                    .State = objAddress.State
                                                    .Comments = objAddress.Comments

                                                    If objAddress.FK_Ref_Detail_Of = 1 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Trn_par_Acc_sign_Address.Add(objNewSignatoryAddress)
                                            Next
                                        End If

                                        'Signatory Phone(s) (1=Phone, 5=Employer Phone)
                                        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_for_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                            For Each objPhone In objListPhone
                                                Dim objNewSignatoryPhone As New goAML_trn_par_acc_sign_Phone
                                                With objNewSignatoryPhone
                                                    lngPK_Phone = lngPK_Phone - 1

                                                    .PK_trn_Par_Acc_Sign_Phone = lngPK_Phone
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_Par_Acc_Sign = objSignatory_New.PK_Trn_par_acc_Signatory_ID

                                                    .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                    .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                    .tph_country_prefix = objPhone.tph_country_prefix
                                                    .tph_number = objPhone.tph_number
                                                    .tph_extension = objPhone.tph_extension
                                                    .comments = objPhone.comments

                                                    If objPhone.FK_Ref_Detail_Of = 1 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_trn_par_acc_sign_Phone.Add(objNewSignatoryPhone)
                                            Next
                                        End If

                                        'Signatory Identification(s)
                                        Dim objListIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                            For Each objIdentification As GoAMLDAL.goAML_Person_Identification In objListIdentification
                                                Dim objNewSignatoryIdentification As New GoAMLDAL.goAML_Transaction_Party_Identification
                                                With objNewSignatoryIdentification
                                                    lngPK_Identification = lngPK_Identification - 1

                                                    .PK_Transaction_Party_Identification_ID = lngPK_Identification
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory
                                                    .FK_Person_ID = objSignatory_New.PK_Trn_par_acc_Signatory_ID

                                                    .Type = objIdentification.Type
                                                    .Number = objIdentification.Number
                                                    .Issue_Date = objIdentification.Issue_Date
                                                    .Expiry_Date = objIdentification.Expiry_Date
                                                    .Issued_By = objIdentification.Issued_By
                                                    .Issued_Country = objIdentification.Issued_Country
                                                    .Identification_Comment = objIdentification.Identification_Comment

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Transaction_Party_Identification.Add(objNewSignatoryIdentification)
                                            Next
                                        End If
                                    End With
                                End If
                            Else        '====== SIGNATORY NON NASABAH ======
                                Dim objSignatory_New As New goAML_Trn_par_acc_Signatory

                                'Get Signatory Non Nasabah
                                Dim objWICPerson = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = objSignatory.WIC_No).FirstOrDefault
                                If objWICPerson IsNot Nothing Then
                                    With objSignatory_New
                                        lngPK_Signatory = lngPK_Signatory - 1

                                        .PK_Trn_par_acc_Signatory_ID = lngPK_Signatory
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID

                                        'Load Person CIF Detail
                                        If objWICPerson IsNot Nothing Then
                                            .Gender = objWICPerson.INDV_Gender
                                            .Title = objWICPerson.INDV_Title
                                            .First_Name = objWICPerson.INDV_First_Name
                                            .Middle_Name = objWICPerson.INDV_Middle_Name
                                            .Prefix = objWICPerson.INDV_Prefix
                                            .Last_Name = objWICPerson.INDV_Last_Name
                                            .BirthDate = objWICPerson.INDV_BirthDate
                                            .Birth_Place = objWICPerson.INDV_Birth_Place
                                            .Mothers_Name = objWICPerson.INDV_Mothers_Name
                                            .Alias = objWICPerson.INDV_Alias
                                            .SSN = objWICPerson.INDV_SSN
                                            .Passport_Number = objWICPerson.INDV_Passport_Number
                                            .Passport_Country = objWICPerson.INDV_Passport_Country
                                            .Id_Number = objWICPerson.INDV_ID_Number
                                            .Nationality1 = objWICPerson.INDV_Nationality1
                                            .Nationality2 = objWICPerson.INDV_Nationality2
                                            .Nationality3 = objWICPerson.INDV_Nationality3
                                            .Residence = objWICPerson.INDV_Residence
                                            .Email = objWICPerson.INDV_Email
                                            .email2 = objWICPerson.INDV_Email2
                                            .email3 = objWICPerson.INDV_Email3
                                            .email4 = objWICPerson.INDV_Email4
                                            .email5 = objWICPerson.INDV_Email5
                                            .Occupation = objWICPerson.INDV_Occupation
                                            .Employer_Name = objWICPerson.INDV_Employer_Name
                                            .Deceased = objWICPerson.INDV_Deceased
                                            .Deceased_Date = objWICPerson.INDV_Deceased_Date
                                            .Tax_Number = objWICPerson.INDV_Tax_Number
                                            .Tax_Reg_Number = objWICPerson.INDV_Tax_Reg_Number
                                            .Source_Of_Wealth = objWICPerson.INDV_SumberDana
                                            .Comment = objWICPerson.INDV_Comment

                                            .isPrimary = objSignatory.isPrimary
                                            .role = objSignatory.Role
                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End If
                                        objData.list_goAML_Trn_par_acc_Signatory.Add(objSignatory_New)

                                        'Signatory Address(es)  (3=Address, 10=Employer Address)
                                        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_To_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                            For Each objAddress In objListAddress
                                                Dim objNewSignatoryAddress As New goAML_Trn_par_Acc_sign_Address
                                                With objNewSignatoryAddress
                                                    lngPK_Address = lngPK_Address - 1

                                                    .PK_Trn_Par_Acc_sign_Address_ID = lngPK_Address
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_par_acc_Signatory_ID = objSignatory_New.PK_Trn_par_acc_Signatory_ID

                                                    .Address_Type = objAddress.Address_Type
                                                    .Address = objAddress.Address
                                                    .Town = objAddress.Town
                                                    .City = objAddress.City
                                                    .Zip = objAddress.Zip
                                                    .Country_Code = objAddress.Country_Code
                                                    .State = objAddress.State
                                                    .Comments = objAddress.Comments

                                                    If objAddress.FK_Ref_Detail_Of = 3 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Trn_par_Acc_sign_Address.Add(objNewSignatoryAddress)
                                            Next
                                        End If

                                        'Signatory Phone(s) (3=Phone, 10=Employer Phone)
                                        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_for_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                            For Each objPhone In objListPhone
                                                Dim objNewSignatoryPhone As New goAML_trn_par_acc_sign_Phone
                                                With objNewSignatoryPhone
                                                    lngPK_Phone = lngPK_Phone - 1

                                                    .PK_trn_Par_Acc_Sign_Phone = lngPK_Phone
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_Par_Acc_Sign = objSignatory_New.PK_Trn_par_acc_Signatory_ID

                                                    .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                    .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                    .tph_country_prefix = objPhone.tph_country_prefix
                                                    .tph_number = objPhone.tph_number
                                                    .tph_extension = objPhone.tph_extension
                                                    .comments = objPhone.comments

                                                    If objPhone.FK_Ref_Detail_Of = 3 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_trn_par_acc_sign_Phone.Add(objNewSignatoryPhone)
                                            Next
                                        End If

                                        'Signatory Identification(s)
                                        Dim objListIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 8 And x.FK_Person_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                            For Each objIdentification As GoAMLDAL.goAML_Person_Identification In objListIdentification
                                                Dim objNewSignatoryIdentification As New GoAMLDAL.goAML_Transaction_Party_Identification
                                                With objNewSignatoryIdentification
                                                    lngPK_Identification = lngPK_Identification - 1

                                                    .PK_Transaction_Party_Identification_ID = lngPK_Identification
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory
                                                    .FK_Person_ID = objSignatory_New.PK_Trn_par_acc_Signatory_ID

                                                    .Type = objIdentification.Type
                                                    .Number = objIdentification.Number
                                                    .Issue_Date = objIdentification.Issue_Date
                                                    .Expiry_Date = objIdentification.Expiry_Date
                                                    .Issued_By = objIdentification.Issued_By
                                                    .Issued_Country = objIdentification.Issued_Country
                                                    .Identification_Comment = objIdentification.Identification_Comment

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Transaction_Party_Identification.Add(objNewSignatoryIdentification)
                                            Next
                                        End If
                                    End With
                                End If
                            End If
                        Next
                    End If
                End If
            End Using

            'Balikin lagi datanya ke Front End
            Return objData
        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetMultiPartyAccountEntityClassByAccountID(objData As GoAML_Transaction_Class, strPK_Account_ID As String) As GoAML_Transaction_Class
        Try
            Dim objAccount = objData.list_goAML_Trn_Party_Account.Where(Function(x) x.PK_Trn_Party_Account_ID = strPK_Account_ID).FirstOrDefault

            'Hapus data2 yang lama (jika ada)
            If objAccount IsNot Nothing Then
                Dim objAccountEntity = objData.list_goAML_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID).FirstOrDefault
                If objAccountEntity IsNot Nothing Then
                    'Account Entity Address
                    Dim listAccountEntityAddress = objData.list_goAML_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID).ToList
                    If listAccountEntityAddress IsNot Nothing Then
                        For Each objAddress_Old In listAccountEntityAddress
                            objData.list_goAML_Trn_par_Acc_Entity_Address.Remove(objAddress_Old)
                        Next
                    End If

                    'Account Entity Phone
                    Dim listAccountEntityPhone = objData.list_goAML_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID).ToList
                    If listAccountEntityPhone IsNot Nothing Then
                        For Each objPhone_Old In listAccountEntityPhone
                            objData.list_goAML_trn_par_acc_Entity_Phone.Remove(objPhone_Old)
                        Next
                    End If

                    'Director
                    Dim listDirector_Old = objData.list_goAML_Trn_Par_Acc_Ent_Director.Where(Function(x) x.FK_Trn_Par_Acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID).ToList
                    If listDirector_Old IsNot Nothing Then
                        For Each objDirector_Old In listDirector_Old
                            objData.list_goAML_Trn_Par_Acc_Ent_Director.Remove(objDirector_Old)

                            'Director Address
                            Dim listAddress_Old = objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = objDirector_Old.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                            If listAddress_Old IsNot Nothing Then
                                For Each objAddress_Old In listAddress_Old
                                    objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Remove(objAddress_Old)
                                Next
                            End If

                            'Director Phone
                            Dim listPhone_Old = objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = objDirector_Old.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                            If listPhone_Old IsNot Nothing Then
                                For Each objPhone_Old In listPhone_Old
                                    objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Remove(objPhone_Old)
                                Next
                            End If

                            'Director Identification
                            Dim listIdentification_Old = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount And x.FK_Person_ID = objDirector_Old.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                            If listIdentification_Old IsNot Nothing Then
                                For Each objIdentification_Old In listIdentification_Old
                                    objData.list_goAML_Transaction_Party_Identification.Remove(objIdentification_Old)
                                Next
                            End If
                        Next
                    End If
                End If
            End If

            'Masukkan data2 baru ke dalam Class
            Using objDB As New GoAMLDAL.GoAMLEntities
                If objAccount IsNot Nothing Then
                    Dim objAccountEntity = objData.list_goAML_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID).FirstOrDefault
                    If objAccount IsNot Nothing Then
                        'Get Last PK
                        Dim lngPK_Entity_Address As Long = 0
                        Dim lngPK_Entity_Phone As Long = 0

                        Dim lngPK_Director As Long = 0
                        Dim lngPK_Director_Address As Long = 0
                        Dim lngPK_Director_Phone As Long = 0
                        Dim lngPK_Director_Identification As Long = 0

                        'PK Entity Address
                        If objData.list_goAML_Trn_par_Acc_Entity_Address.Count > 0 Then
                            lngPK_Director_Address = objData.list_goAML_Trn_par_Acc_Entity_Address.Min(Function(x) x.PK_Trn_Par_Acc_Entity_Address_ID)
                            If lngPK_Director_Address > 0 Then
                                lngPK_Director_Address = 0
                            End If
                        End If

                        'PK Entity Phone
                        If objData.list_goAML_trn_par_acc_Entity_Phone.Count > 0 Then
                            lngPK_Director_Phone = objData.list_goAML_trn_par_acc_Entity_Phone.Min(Function(x) x.PK_trn_Par_Acc_Entity_Phone_ID)
                            If lngPK_Director_Phone > 0 Then
                                lngPK_Director_Phone = 0
                            End If
                        End If

                        'PK Director
                        If objData.list_goAML_Trn_Par_Acc_Ent_Director.Count > 0 Then
                            lngPK_Director = objData.list_goAML_Trn_Par_Acc_Ent_Director.Min(Function(x) x.PK_Trn_Par_Acc_Ent_Director_ID)
                            If lngPK_Director > 0 Then
                                lngPK_Director = 0
                            End If
                        End If

                        'PK Director Address
                        If objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Count > 0 Then
                            lngPK_Director_Address = objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Min(Function(x) x.PK_Trn_Par_Acc_Entity_Address_ID)
                            If lngPK_Director_Address > 0 Then
                                lngPK_Director_Address = 0
                            End If
                        End If

                        'PK Director Phone
                        If objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Count > 0 Then
                            lngPK_Director_Phone = objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Min(Function(x) x.PK_Trn_Par_Acc_Ent_Director_Phone_ID)
                            If lngPK_Director_Phone > 0 Then
                                lngPK_Director_Phone = 0
                            End If
                        End If

                        'PK Director Identification
                        If objData.list_goAML_Transaction_Party_Identification.Count > 0 Then
                            lngPK_Director_Identification = objData.list_goAML_Transaction_Party_Identification.Min(Function(x) x.PK_Transaction_Party_Identification_ID)
                            If lngPK_Director_Identification > 0 Then
                                lngPK_Director_Identification = 0
                            End If
                        End If

                        'Masukkan data2 Entity Address & Phone dari goAML Ref
                        'Account Entity Address(es)
                        Dim objCustomer = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objAccount.Client_Number).FirstOrDefault
                        If objCustomer IsNot Nothing Then
                            Dim objListEntityAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = objCustomer.PK_Customer_ID).ToList
                            If objListEntityAddress IsNot Nothing AndAlso objListEntityAddress.Count > 0 Then
                                For Each objAddress In objListEntityAddress
                                    Dim objNewEntityAddress As New goAML_Trn_par_Acc_Entity_Address
                                    With objNewEntityAddress
                                        lngPK_Entity_Address = lngPK_Entity_Address - 1

                                        .PK_Trn_Par_Acc_Entity_Address_ID = lngPK_Entity_Address
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                        .Address_Type = objAddress.Address_Type
                                        .Address = objAddress.Address
                                        .Town = objAddress.Town
                                        .City = objAddress.City
                                        .Zip = objAddress.Zip
                                        .Country_Code = objAddress.Country_Code
                                        .State = objAddress.State
                                        .Comments = objAddress.Comments

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Trn_par_Acc_Entity_Address.Add(objNewEntityAddress)
                                Next
                            End If

                            'Entity Phone(s)
                            Dim objListEntityPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = objCustomer.PK_Customer_ID).ToList
                            If objListEntityPhone IsNot Nothing AndAlso objListEntityPhone.Count > 0 Then
                                For Each objPhone In objListEntityPhone
                                    Dim objNewEntityPhone As New goAML_trn_par_acc_Entity_Phone
                                    With objNewEntityPhone
                                        lngPK_Entity_Phone = lngPK_Entity_Phone - 1

                                        .PK_trn_Par_Acc_Entity_Phone_ID = lngPK_Entity_Phone
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                        .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                        .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                        .tph_country_prefix = objPhone.tph_country_prefix
                                        .tph_number = objPhone.tph_number
                                        .tph_extension = objPhone.tph_extension
                                        .comments = objPhone.comments

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_trn_par_acc_Entity_Phone.Add(objNewEntityPhone)
                                Next
                            End If
                        End If

                        'Masukkand data2 Director dari goAML Ref
                        Dim objListgoAML_Ref_Account_Director As List(Of goAML_Ref_Customer_Entity_Director) = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.FK_Entity_ID = objAccount.Client_Number).ToList
                        If objListgoAML_Ref_Account_Director IsNot Nothing Then
                            For Each objDirector In objListgoAML_Ref_Account_Director
                                Dim objDirector_New As New goAML_Trn_Par_Acc_Ent_Director
                                With objDirector_New
                                    lngPK_Director = lngPK_Director - 1

                                    .PK_Trn_Par_Acc_Ent_Director_ID = lngPK_Director
                                    .FK_Report_ID = objAccount.FK_Report_ID
                                    .FK_Trn_Par_Acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                    .Gender = objDirector.Gender
                                    .Title = objDirector.Title
                                    .First_Name = objDirector.First_name
                                    .Middle_Name = objDirector.Middle_Name
                                    .Prefix = objDirector.Prefix
                                    .Last_Name = objDirector.Last_Name
                                    .BirthDate = objDirector.BirthDate
                                    .Birth_Place = objDirector.Birth_Place
                                    .Mothers_Name = objDirector.Mothers_Name
                                    .Alias = objDirector.Alias
                                    .SSN = objDirector.SSN
                                    .Passport_Number = objDirector.Passport_Number
                                    .Passport_Country = objDirector.Passport_Country
                                    .Id_Number = objDirector.ID_Number
                                    .Nationality1 = objDirector.Nationality1
                                    .Nationality2 = objDirector.Nationality2
                                    .Nationality3 = objDirector.Nationality3
                                    .Residence = objDirector.Residence
                                    .Email = objDirector.Email
                                    .email2 = objDirector.Email2
                                    .email3 = objDirector.Email3
                                    .email4 = objDirector.Email4
                                    .email5 = objDirector.Email5
                                    .Occupation = objDirector.Occupation
                                    .Employer_Name = objDirector.Employer_Name
                                    .Deceased = objDirector.Deceased
                                    .Deceased_Date = objDirector.Deceased_Date
                                    .Tax_Number = objDirector.Tax_Number
                                    .Tax_Reg_Number = objDirector.Tax_Reg_Number
                                    .Source_Of_Wealth = objDirector.Source_of_Wealth
                                    .Comment = objDirector.Comments

                                    .role = objDirector.Role
                                    .Active = 1
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                End With
                                objData.list_goAML_Trn_Par_Acc_Ent_Director.Add(objDirector_New)

                                'Director Address(es)  (6=Address, 7=Employer Address)
                                Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_To_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                    For Each objAddress In objListAddress
                                        Dim objNewDirectorAddress As New goAML_Trn_Par_Acc_Ent_Director_Address
                                        With objNewDirectorAddress
                                            lngPK_Director_Address = lngPK_Director_Address - 1

                                            .PK_Trn_Par_Acc_Entity_Address_ID = lngPK_Director_Address
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Trn_par_acc_Entity_Director_ID = objDirector_New.PK_Trn_Par_Acc_Ent_Director_ID

                                            .Address_Type = objAddress.Address_Type
                                            .Address = objAddress.Address
                                            .Town = objAddress.Town
                                            .City = objAddress.City
                                            .Zip = objAddress.Zip
                                            .Country_Code = objAddress.Country_Code
                                            .State = objAddress.State
                                            .Comments = objAddress.Comments

                                            If objAddress.FK_Ref_Detail_Of = 6 Then
                                                .isemployer = False
                                            Else
                                                .isemployer = True
                                            End If

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Add(objNewDirectorAddress)
                                    Next
                                End If

                                'Director Phone(s) (6=Phone, 7=Employer Phone)
                                Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_for_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                    For Each objPhone In objListPhone
                                        Dim objNewDirectorPhone As New goAML_Trn_Par_Acc_Ent_Director_Phone
                                        With objNewDirectorPhone
                                            lngPK_Director_Phone = lngPK_Director_Phone - 1

                                            .PK_Trn_Par_Acc_Ent_Director_Phone_ID = lngPK_Director_Phone
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Trn_par_acc_Entity_Director_ID = objDirector_New.PK_Trn_Par_Acc_Ent_Director_ID

                                            .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                            .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                            .tph_country_prefix = objPhone.tph_country_prefix
                                            .tph_number = objPhone.tph_number
                                            .tph_extension = objPhone.tph_extension
                                            .comments = objPhone.comments

                                            If objPhone.FK_Ref_Detail_Of = 6 Then
                                                .isemployer = False
                                            Else
                                                .isemployer = True
                                            End If

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Add(objNewDirectorPhone)
                                    Next
                                End If

                                'Director Identification(s)
                                'Ada 2 faham yang berbeda untuk FK_Person_Type Director Account di goAML_Person_Identification. Yang diketahui SCB pakai 4, BSIM pakai 5
                                'Sesuaikan global parameter 3001. Karena kalau mau disesuaikan semua harus patches data, ubah SP generate CTR, IFTI, dll.
                                Dim intDirectrorAccountPersonType As Integer = getDirectorAccountPersonType()
                                Dim objListIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = intDirectrorAccountPersonType And x.FK_Person_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                    For Each objIdentification As GoAMLDAL.goAML_Person_Identification In objListIdentification
                                        Dim objNewDirectorIdentification As New GoAMLDAL.goAML_Transaction_Party_Identification
                                        With objNewDirectorIdentification
                                            lngPK_Director_Identification = lngPK_Director_Identification - 1

                                            .PK_Transaction_Party_Identification_ID = lngPK_Director_Identification
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount
                                            .FK_Person_ID = objDirector_New.PK_Trn_Par_Acc_Ent_Director_ID

                                            .Type = objIdentification.Type
                                            .Number = objIdentification.Number
                                            .Issue_Date = objIdentification.Issue_Date
                                            .Expiry_Date = objIdentification.Expiry_Date
                                            .Issued_By = objIdentification.Issued_By
                                            .Issued_Country = objIdentification.Issued_Country
                                            .Identification_Comment = objIdentification.Identification_Comment

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Transaction_Party_Identification.Add(objNewDirectorIdentification)
                                    Next
                                End If
                            Next
                        End If
                    End If
                End If
            End Using

            'Balikin lagi datanya ke Front End
            Return objData
        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetActivityAccountSignatoryClassByAccountID(objData As GoAML_Activity_Class, strPK_Account_ID As String) As GoAML_Activity_Class
        Try
            Dim objAccount = objData.list_goAML_Act_Account.Where(Function(x) x.PK_goAML_Act_Account_ID = strPK_Account_ID).FirstOrDefault

            'Hapus data2 Signatory yang lama (jika ada)
            If objAccount IsNot Nothing Then
                Dim listSignatory_Old = objData.list_goAML_Act_acc_Signatory.Where(Function(x) x.FK_Activity_Account_ID = objAccount.PK_goAML_Act_Account_ID).ToList
                If listSignatory_Old IsNot Nothing Then
                    For Each objSignatory_Old In listSignatory_Old
                        objData.list_goAML_Act_acc_Signatory.Remove(objSignatory_Old)

                        'Signatory Address
                        Dim listAddress_Old = objData.list_goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_Act_Acc_Entity = objSignatory_Old.PK_goAML_Act_acc_Signatory_ID).ToList
                        If listAddress_Old IsNot Nothing Then
                            For Each objAddress_Old In listAddress_Old
                                objData.list_goAML_Act_Acc_sign_Address.Remove(objAddress_Old)
                            Next
                        End If

                        'Signatory Phone
                        Dim listPhone_Old = objData.list_goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = objSignatory_Old.PK_goAML_Act_acc_Signatory_ID).ToList
                        If listPhone_Old IsNot Nothing Then
                            For Each objPhone_Old In listPhone_Old
                                objData.list_goAML_Act_Acc_sign_Phone.Remove(objPhone_Old)
                            Next
                        End If

                        'Signatory Identification
                        Dim listIdentification_Old = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory And x.FK_Act_Person_ID = objSignatory_Old.PK_goAML_Act_acc_Signatory_ID).ToList
                        If listIdentification_Old IsNot Nothing Then
                            For Each objIdentification_Old In listIdentification_Old
                                objData.list_goAML_Activity_Person_Identification.Remove(objIdentification_Old)
                            Next
                        End If
                    Next
                End If
            End If

            'Masukkan data2 baru ke dalam Class
            Using objDB As New GoAMLDAL.GoAMLEntities
                If objAccount IsNot Nothing Then
                    'Get Last PK
                    Dim lngPK_Signatory As Long = 0
                    Dim lngPK_Address As Long = 0
                    Dim lngPK_Phone As Long = 0
                    Dim lngPK_Identification As Long = 0

                    'PK Signatory
                    If objData.list_goAML_Act_acc_Signatory.Count > 0 Then
                        lngPK_Signatory = objData.list_goAML_Act_acc_Signatory.Min(Function(x) x.PK_goAML_Act_acc_Signatory_ID)
                        If lngPK_Signatory > 0 Then
                            lngPK_Signatory = 0
                        End If
                    End If

                    'PK Signatory Address
                    If objData.list_goAML_Act_Acc_sign_Address.Count > 0 Then
                        lngPK_Address = objData.list_goAML_Act_Acc_sign_Address.Min(Function(x) x.PK_goAML_Act_Acc_Entity_Address_ID)
                        If lngPK_Address > 0 Then
                            lngPK_Address = 0
                        End If
                    End If

                    'PK Signatory Phone
                    If objData.list_goAML_Act_Acc_sign_Phone.Count > 0 Then
                        lngPK_Phone = objData.list_goAML_Act_Acc_sign_Phone.Min(Function(x) x.PK_goAML_act_acc_sign_Phone_ID)
                        If lngPK_Phone > 0 Then
                            lngPK_Phone = 0
                        End If
                    End If

                    'PK Signatory Identification
                    If objData.list_goAML_Activity_Person_Identification.Count > 0 Then
                        lngPK_Identification = objData.list_goAML_Activity_Person_Identification.Min(Function(x) x.PK_goAML_Activity_Person_Identification_ID)
                        If lngPK_Identification > 0 Then
                            lngPK_Identification = 0
                        End If
                    End If

                    'Masukkand data2 Signatory dari goAML Ref
                    Dim objListgoAML_Ref_Account_Signatory As List(Of goAML_Ref_Account_Signatory) = objDB.goAML_Ref_Account_Signatory.Where(Function(x) x.FK_Account_No = objAccount.Account).ToList
                    If objListgoAML_Ref_Account_Signatory IsNot Nothing Then
                        For Each objSignatory In objListgoAML_Ref_Account_Signatory

                            If objSignatory.isCustomer Then     'Jika Sigantory adalah Nasabah
                                Dim objSignatory_New As New goAML_Act_acc_Signatory

                                'Get Signatory Nasabah
                                Dim objCIFPerson = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objSignatory.FK_CIF_Person_ID).FirstOrDefault
                                If objCIFPerson IsNot Nothing Then
                                    With objSignatory_New
                                        lngPK_Signatory = lngPK_Signatory - 1

                                        .PK_goAML_Act_acc_Signatory_ID = lngPK_Signatory
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Act_ReportParty_ID = objAccount.FK_Act_ReportParty_ID
                                        .FK_Activity_Account_ID = objAccount.PK_goAML_Act_Account_ID

                                        'Load Person CIF Detail
                                        If objCIFPerson IsNot Nothing Then
                                            .Gender = objCIFPerson.INDV_Gender
                                            .Title = objCIFPerson.INDV_Title
                                            .First_Name = objCIFPerson.INDV_First_name
                                            .Middle_Name = objCIFPerson.INDV_Middle_name
                                            .Prefix = objCIFPerson.INDV_Prefix
                                            .Last_Name = objCIFPerson.INDV_Last_Name
                                            .BirthDate = objCIFPerson.INDV_BirthDate
                                            .Birth_Place = objCIFPerson.INDV_Birth_Place
                                            .Mothers_Name = objCIFPerson.INDV_Mothers_Name
                                            .Alias = objCIFPerson.INDV_Alias
                                            .SSN = objCIFPerson.INDV_SSN
                                            .Passport_Number = objCIFPerson.INDV_Passport_Number
                                            .Passport_Country = objCIFPerson.INDV_Passport_Country
                                            .Id_Number = objCIFPerson.INDV_ID_Number
                                            .Nationality1 = objCIFPerson.INDV_Nationality1
                                            .Nationality2 = objCIFPerson.INDV_Nationality2
                                            .Nationality3 = objCIFPerson.INDV_Nationality3
                                            .Residence = objCIFPerson.INDV_Residence
                                            .Email = objCIFPerson.INDV_Email
                                            .email2 = objCIFPerson.INDV_Email2
                                            .email3 = objCIFPerson.INDV_Email3
                                            .email4 = objCIFPerson.INDV_Email4
                                            .email5 = objCIFPerson.INDV_Email5
                                            .Occupation = objCIFPerson.INDV_Occupation
                                            .Employer_Name = objCIFPerson.INDV_Employer_Name
                                            .Deceased = objCIFPerson.INDV_Deceased
                                            .Deceased_Date = objCIFPerson.INDV_Deceased_Date
                                            .Tax_Number = objCIFPerson.INDV_Tax_Number
                                            .Tax_Reg_Number = objCIFPerson.INDV_Tax_Reg_Number
                                            .Source_Of_Wealth = objCIFPerson.INDV_Source_of_Wealth
                                            .Comment = objCIFPerson.INDV_Comments

                                            .isPrimary = objSignatory.isPrimary
                                            .role = objSignatory.Role
                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End If
                                        objData.list_goAML_Act_acc_Signatory.Add(objSignatory_New)

                                        'Signatory Address(es)  (1=Address, 5=Employer Address)
                                        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_To_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                            For Each objAddress In objListAddress
                                                Dim objNewSignatoryAddress As New goAML_Act_Acc_sign_Address
                                                With objNewSignatoryAddress
                                                    lngPK_Address = lngPK_Address - 1

                                                    .PK_goAML_Act_Acc_Entity_Address_ID = lngPK_Address
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Act_Acc_Entity = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                    .Address_Type = objAddress.Address_Type
                                                    .Address = objAddress.Address
                                                    .Town = objAddress.Town
                                                    .City = objAddress.City
                                                    .Zip = objAddress.Zip
                                                    .Country_Code = objAddress.Country_Code
                                                    .State = objAddress.State
                                                    .Comments = objAddress.Comments

                                                    If objAddress.FK_Ref_Detail_Of = 1 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Act_Acc_sign_Address.Add(objNewSignatoryAddress)
                                            Next
                                        End If

                                        'Signatory Phone(s) (1=Phone, 5=Employer Phone)
                                        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_for_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                            For Each objPhone In objListPhone
                                                Dim objNewSignatoryPhone As New goAML_Act_Acc_sign_Phone
                                                With objNewSignatoryPhone
                                                    lngPK_Phone = lngPK_Phone - 1

                                                    .PK_goAML_act_acc_sign_Phone_ID = lngPK_Phone
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Act_Acc_Entity_ID = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                    .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                    .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                    .tph_country_prefix = objPhone.tph_country_prefix
                                                    .tph_number = objPhone.tph_number
                                                    .tph_extension = objPhone.tph_extension
                                                    .comments = objPhone.comments

                                                    If objPhone.FK_Ref_Detail_Of = 1 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Act_Acc_sign_Phone.Add(objNewSignatoryPhone)
                                            Next
                                        End If

                                        'Signatory Identification(s)
                                        Dim objListIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                            For Each objIdentification As GoAMLDAL.goAML_Person_Identification In objListIdentification
                                                Dim objNewSignatoryIdentification As New GoAMLDAL.goAML_Activity_Person_Identification
                                                With objNewSignatoryIdentification
                                                    lngPK_Identification = lngPK_Identification - 1

                                                    .PK_goAML_Activity_Person_Identification_ID = lngPK_Identification
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory
                                                    .FK_Act_Person_ID = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                    .Type = objIdentification.Type
                                                    .Number = objIdentification.Number
                                                    .Issue_Date = objIdentification.Issue_Date
                                                    .Expiry_Date = objIdentification.Expiry_Date
                                                    .Issued_By = objIdentification.Issued_By
                                                    .Issued_Country = objIdentification.Issued_Country
                                                    .Identification_Comment = objIdentification.Identification_Comment

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Activity_Person_Identification.Add(objNewSignatoryIdentification)
                                            Next
                                        End If
                                    End With
                                End If
                            Else        '====== SIGNATORY NON NASABAH ======
                                Dim objSignatory_New As New goAML_Act_acc_Signatory

                                'Get Signatory Non Nasabah
                                Dim objWICPerson = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = objSignatory.WIC_No).FirstOrDefault
                                If objWICPerson IsNot Nothing Then
                                    With objSignatory_New
                                        lngPK_Signatory = lngPK_Signatory - 1

                                        .PK_goAML_Act_acc_Signatory_ID = lngPK_Signatory
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Act_ReportParty_ID = objAccount.FK_Act_ReportParty_ID
                                        .FK_Activity_Account_ID = objAccount.PK_goAML_Act_Account_ID

                                        'Load Person CIF Detail
                                        If objWICPerson IsNot Nothing Then
                                            .Gender = objWICPerson.INDV_Gender
                                            .Title = objWICPerson.INDV_Title
                                            .First_Name = objWICPerson.INDV_First_Name
                                            .Middle_Name = objWICPerson.INDV_Middle_Name
                                            .Prefix = objWICPerson.INDV_Prefix
                                            .Last_Name = objWICPerson.INDV_Last_Name
                                            .BirthDate = objWICPerson.INDV_BirthDate
                                            .Birth_Place = objWICPerson.INDV_Birth_Place
                                            .Mothers_Name = objWICPerson.INDV_Mothers_Name
                                            .Alias = objWICPerson.INDV_Alias
                                            .SSN = objWICPerson.INDV_SSN
                                            .Passport_Number = objWICPerson.INDV_Passport_Number
                                            .Passport_Country = objWICPerson.INDV_Passport_Country
                                            .Id_Number = objWICPerson.INDV_ID_Number
                                            .Nationality1 = objWICPerson.INDV_Nationality1
                                            .Nationality2 = objWICPerson.INDV_Nationality2
                                            .Nationality3 = objWICPerson.INDV_Nationality3
                                            .Residence = objWICPerson.INDV_Residence
                                            .Email = objWICPerson.INDV_Email
                                            .email2 = objWICPerson.INDV_Email2
                                            .email3 = objWICPerson.INDV_Email3
                                            .email4 = objWICPerson.INDV_Email4
                                            .email5 = objWICPerson.INDV_Email5
                                            .Occupation = objWICPerson.INDV_Occupation
                                            .Employer_Name = objWICPerson.INDV_Employer_Name
                                            .Deceased = objWICPerson.INDV_Deceased
                                            .Deceased_Date = objWICPerson.INDV_Deceased_Date
                                            .Tax_Number = objWICPerson.INDV_Tax_Number
                                            .Tax_Reg_Number = objWICPerson.INDV_Tax_Reg_Number
                                            .Source_Of_Wealth = objWICPerson.INDV_SumberDana
                                            .Comment = objWICPerson.INDV_Comment

                                            .isPrimary = objSignatory.isPrimary
                                            .role = objSignatory.Role
                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End If
                                        objData.list_goAML_Act_acc_Signatory.Add(objSignatory_New)

                                        'Signatory Address(es)  (3=Address, 10=Employer Address)
                                        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_To_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                            For Each objAddress In objListAddress
                                                Dim objNewSignatoryAddress As New goAML_Act_Acc_sign_Address
                                                With objNewSignatoryAddress
                                                    lngPK_Address = lngPK_Address - 1

                                                    .PK_goAML_Act_Acc_Entity_Address_ID = lngPK_Address
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Act_Acc_Entity = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                    .Address_Type = objAddress.Address_Type
                                                    .Address = objAddress.Address
                                                    .Town = objAddress.Town
                                                    .City = objAddress.City
                                                    .Zip = objAddress.Zip
                                                    .Country_Code = objAddress.Country_Code
                                                    .State = objAddress.State
                                                    .Comments = objAddress.Comments

                                                    If objAddress.FK_Ref_Detail_Of = 3 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Act_Acc_sign_Address.Add(objNewSignatoryAddress)
                                            Next
                                        End If

                                        'Signatory Phone(s) (3=Phone, 10=Employer Phone)
                                        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_for_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                            For Each objPhone In objListPhone
                                                Dim objNewSignatoryPhone As New goAML_Act_Acc_sign_Phone
                                                With objNewSignatoryPhone
                                                    lngPK_Phone = lngPK_Phone - 1

                                                    .PK_goAML_act_acc_sign_Phone_ID = lngPK_Phone
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Act_Acc_Entity_ID = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                    .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                    .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                    .tph_country_prefix = objPhone.tph_country_prefix
                                                    .tph_number = objPhone.tph_number
                                                    .tph_extension = objPhone.tph_extension
                                                    .comments = objPhone.comments

                                                    If objPhone.FK_Ref_Detail_Of = 3 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Act_Acc_sign_Phone.Add(objNewSignatoryPhone)
                                            Next
                                        End If

                                        'Signatory Identification(s)
                                        Dim objListIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 8 And x.FK_Person_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                            For Each objIdentification As GoAMLDAL.goAML_Person_Identification In objListIdentification
                                                Dim objNewSignatoryIdentification As New GoAMLDAL.goAML_Activity_Person_Identification
                                                With objNewSignatoryIdentification
                                                    lngPK_Identification = lngPK_Identification - 1

                                                    .PK_goAML_Activity_Person_Identification_ID = lngPK_Identification
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.Signatory
                                                    .FK_Act_Person_ID = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                    .Type = objIdentification.Type
                                                    .Number = objIdentification.Number
                                                    .Issue_Date = objIdentification.Issue_Date
                                                    .Expiry_Date = objIdentification.Expiry_Date
                                                    .Issued_By = objIdentification.Issued_By
                                                    .Issued_Country = objIdentification.Issued_Country
                                                    .Identification_Comment = objIdentification.Identification_Comment

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Activity_Person_Identification.Add(objNewSignatoryIdentification)
                                            Next
                                        End If
                                    End With
                                End If
                            End If
                        Next
                    End If
                End If
            End Using

            'Balikin lagi datanya ke Front End
            Return objData
        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetActivityAccountEntityClassByAccountID(objData As GoAML_Activity_Class, strPK_Account_ID As String) As GoAML_Activity_Class
        Try
            Dim objAccount = objData.list_goAML_Act_Account.Where(Function(x) x.PK_goAML_Act_Account_ID = strPK_Account_ID).FirstOrDefault

            'Hapus data2 yang lama (jika ada)
            If objAccount IsNot Nothing Then
                Dim objAccountEntity = objData.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_Act_Account_ID = objAccount.PK_goAML_Act_Account_ID).FirstOrDefault
                If objAccountEntity IsNot Nothing Then
                    'Account Entity Address
                    Dim listAccountEntityAddress = objData.list_goAML_Act_Acc_Entity_Address.Where(Function(x) x.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account).ToList
                    If listAccountEntityAddress IsNot Nothing Then
                        For Each objAddress_Old In listAccountEntityAddress
                            objData.list_goAML_Act_Acc_Entity_Address.Remove(objAddress_Old)
                        Next
                    End If

                    'Account Entity Phone
                    Dim listAccountEntityPhone = objData.list_goAML_Act_Acc_Entity_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account).ToList
                    If listAccountEntityPhone IsNot Nothing Then
                        For Each objPhone_Old In listAccountEntityPhone
                            objData.list_goAML_Act_Acc_Entity_Phone.Remove(objPhone_Old)
                        Next
                    End If

                    'Director
                    Dim listDirector_Old = objData.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account).ToList
                    If listDirector_Old IsNot Nothing Then
                        For Each objDirector_Old In listDirector_Old
                            objData.list_goAML_Act_Acc_Ent_Director.Remove(objDirector_Old)

                            'Director Address
                            Dim listAddress_Old = objData.list_goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = objDirector_Old.PK_Act_Acc_Ent_Director_ID).ToList
                            If listAddress_Old IsNot Nothing Then
                                For Each objAddress_Old In listAddress_Old
                                    objData.list_goAML_Act_Acc_Entity_Director_address.Remove(objAddress_Old)
                                Next
                            End If

                            'Director Phone
                            Dim listPhone_Old = objData.list_goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = objDirector_Old.PK_Act_Acc_Ent_Director_ID).ToList
                            If listPhone_Old IsNot Nothing Then
                                For Each objPhone_Old In listPhone_Old
                                    objData.list_goAML_Act_Acc_Entity_Director_Phone.Remove(objPhone_Old)
                                Next
                            End If

                            'Director Identification
                            Dim listIdentification_Old = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount And x.FK_Act_Person_ID = objDirector_Old.PK_Act_Acc_Ent_Director_ID).ToList
                            If listIdentification_Old IsNot Nothing Then
                                For Each objIdentification_Old In listIdentification_Old
                                    objData.list_goAML_Activity_Person_Identification.Remove(objIdentification_Old)
                                Next
                            End If
                        Next
                    End If
                End If
            End If

            'Masukkan data2 baru ke dalam Class
            Using objDB As New GoAMLDAL.GoAMLEntities
                If objAccount IsNot Nothing Then
                    Dim objAccountEntity = objData.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_Act_Account_ID = objAccount.PK_goAML_Act_Account_ID).FirstOrDefault
                    If objAccount IsNot Nothing Then
                        'Get Last PK
                        Dim lngPK_Entity_Address As Long = 0
                        Dim lngPK_Entity_Phone As Long = 0

                        Dim lngPK_Director As Long = 0
                        Dim lngPK_Director_Address As Long = 0
                        Dim lngPK_Director_Phone As Long = 0
                        Dim lngPK_Director_Identification As Long = 0

                        'PK Entity Address
                        If objData.list_goAML_Act_Acc_Entity_Address.Count > 0 Then
                            lngPK_Director_Address = objData.list_goAML_Act_Acc_Entity_Address.Min(Function(x) x.PK_Act_Acc_Entity_Address_ID)
                            If lngPK_Director_Address > 0 Then
                                lngPK_Director_Address = 0
                            End If
                        End If

                        'PK Entity Phone
                        If objData.list_goAML_Act_Acc_Entity_Phone.Count > 0 Then
                            lngPK_Director_Phone = objData.list_goAML_Act_Acc_Entity_Phone.Min(Function(x) x.PK_goAML_Act_Acc_Entity_Phone_ID)
                            If lngPK_Director_Phone > 0 Then
                                lngPK_Director_Phone = 0
                            End If
                        End If

                        'PK Director
                        If objData.list_goAML_Act_Acc_Ent_Director.Count > 0 Then
                            lngPK_Director = objData.list_goAML_Act_Acc_Ent_Director.Min(Function(x) x.PK_Act_Acc_Ent_Director_ID)
                            If lngPK_Director > 0 Then
                                lngPK_Director = 0
                            End If
                        End If

                        'PK Director Address
                        If objData.list_goAML_Act_Acc_Entity_Director_address.Count > 0 Then
                            lngPK_Director_Address = objData.list_goAML_Act_Acc_Entity_Director_address.Min(Function(x) x.PK_goAML_Act_Acc_Entity_Director_address_ID)
                            If lngPK_Director_Address > 0 Then
                                lngPK_Director_Address = 0
                            End If
                        End If

                        'PK Director Phone
                        If objData.list_goAML_Act_Acc_Entity_Director_Phone.Count > 0 Then
                            lngPK_Director_Phone = objData.list_goAML_Act_Acc_Entity_Director_Phone.Min(Function(x) x.PK_goAML_Act_Acc_Entity_Director_Phone_ID)
                            If lngPK_Director_Phone > 0 Then
                                lngPK_Director_Phone = 0
                            End If
                        End If

                        'PK Director Identification
                        If objData.list_goAML_Activity_Person_Identification.Count > 0 Then
                            lngPK_Director_Identification = objData.list_goAML_Activity_Person_Identification.Min(Function(x) x.PK_goAML_Activity_Person_Identification_ID)
                            If lngPK_Director_Identification > 0 Then
                                lngPK_Director_Identification = 0
                            End If
                        End If

                        'Masukkan data2 Entity Address & Phone dari goAML Ref
                        'Account Entity Address(es)
                        Dim objCustomer = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objAccount.Client_Number).FirstOrDefault
                        If objCustomer IsNot Nothing Then
                            Dim objListEntityAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = objCustomer.PK_Customer_ID).ToList
                            If objListEntityAddress IsNot Nothing AndAlso objListEntityAddress.Count > 0 Then
                                For Each objAddress In objListEntityAddress
                                    Dim objNewEntityAddress As New goAML_Act_Acc_Entity_Address
                                    With objNewEntityAddress
                                        lngPK_Entity_Address = lngPK_Entity_Address - 1

                                        .PK_Act_Acc_Entity_Address_ID = lngPK_Entity_Address
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                        .Address_Type = objAddress.Address_Type
                                        .Address = objAddress.Address
                                        .Town = objAddress.Town
                                        .City = objAddress.City
                                        .Zip = objAddress.Zip
                                        .Country_Code = objAddress.Country_Code
                                        .State = objAddress.State
                                        .Comments = objAddress.Comments

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Act_Acc_Entity_Address.Add(objNewEntityAddress)
                                Next
                            End If

                            'Entity Phone(s)
                            Dim objListEntityPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = objCustomer.PK_Customer_ID).ToList
                            If objListEntityPhone IsNot Nothing AndAlso objListEntityPhone.Count > 0 Then
                                For Each objPhone In objListEntityPhone
                                    Dim objNewEntityPhone As New goAML_Act_Acc_Entity_Phone
                                    With objNewEntityPhone
                                        lngPK_Entity_Phone = lngPK_Entity_Phone - 1

                                        .PK_goAML_Act_Acc_Entity_Phone_ID = lngPK_Entity_Phone
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                        .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                        .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                        .tph_country_prefix = objPhone.tph_country_prefix
                                        .tph_number = objPhone.tph_number
                                        .tph_extension = objPhone.tph_extension
                                        .comments = objPhone.comments

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Act_Acc_Entity_Phone.Add(objNewEntityPhone)
                                Next
                            End If
                        End If

                        'Masukkand data2 Director dari goAML Ref
                        Dim objListgoAML_Ref_Account_Director As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director) = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.FK_Entity_ID = objAccount.Client_Number).ToList
                        If objListgoAML_Ref_Account_Director IsNot Nothing Then
                            For Each objDirector In objListgoAML_Ref_Account_Director
                                Dim objDirector_New As New goAML_Act_Acc_Ent_Director
                                With objDirector_New
                                    lngPK_Director = lngPK_Director - 1

                                    .PK_Act_Acc_Ent_Director_ID = lngPK_Director
                                    .FK_Report_ID = objAccount.FK_Report_ID
                                    .FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                    .Gender = objDirector.Gender
                                    .Title = objDirector.Title
                                    .First_Name = objDirector.First_name
                                    .Middle_Name = objDirector.Middle_Name
                                    .Prefix = objDirector.Prefix
                                    .Last_Name = objDirector.Last_Name
                                    .BirthDate = objDirector.BirthDate
                                    .Birth_Place = objDirector.Birth_Place
                                    .Mothers_Name = objDirector.Mothers_Name
                                    .Alias = objDirector.Alias
                                    .SSN = objDirector.SSN
                                    .Passport_Number = objDirector.Passport_Number
                                    .Passport_Country = objDirector.Passport_Country
                                    .Id_Number = objDirector.ID_Number
                                    .Nationality1 = objDirector.Nationality1
                                    .Nationality2 = objDirector.Nationality2
                                    .Nationality3 = objDirector.Nationality3
                                    .Residence = objDirector.Residence
                                    .Email = objDirector.Email
                                    .email2 = objDirector.Email2
                                    .email3 = objDirector.Email3
                                    .email4 = objDirector.Email4
                                    .email5 = objDirector.Email5
                                    .Occupation = objDirector.Occupation
                                    .Employer_Name = objDirector.Employer_Name
                                    .Deceased = objDirector.Deceased
                                    .Deceased_Date = objDirector.Deceased_Date
                                    .Tax_Number = objDirector.Tax_Number
                                    .Tax_Reg_Number = objDirector.Tax_Reg_Number
                                    .Source_Of_Wealth = objDirector.Source_of_Wealth
                                    .Comment = objDirector.Comments

                                    .role = objDirector.Role
                                    .Active = 1
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                End With
                                objData.list_goAML_Act_Acc_Ent_Director.Add(objDirector_New)

                                'Director Address(es)  (6=Address, 7=Employer Address)
                                Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_To_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                    For Each objAddress In objListAddress
                                        Dim objNewDirectorAddress As New goAML_Act_Acc_Entity_Director_address
                                        With objNewDirectorAddress
                                            lngPK_Director_Address = lngPK_Director_Address - 1

                                            .PK_goAML_Act_Acc_Entity_Director_address_ID = lngPK_Director_Address
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Act_Entity_Director_ID = objDirector_New.PK_Act_Acc_Ent_Director_ID

                                            .Address_Type = objAddress.Address_Type
                                            .Address = objAddress.Address
                                            .Town = objAddress.Town
                                            .City = objAddress.City
                                            .Zip = objAddress.Zip
                                            .Country_Code = objAddress.Country_Code
                                            .State = objAddress.State
                                            .Comments = objAddress.Comments

                                            If objAddress.FK_Ref_Detail_Of = 6 Then
                                                .isEmployer = False
                                            Else
                                                .isEmployer = True
                                            End If

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Act_Acc_Entity_Director_address.Add(objNewDirectorAddress)
                                    Next
                                End If

                                'Director Phone(s) (6=Phone, 7=Employer Phone)
                                Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_for_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                    For Each objPhone In objListPhone
                                        Dim objNewDirectorPhone As New goAML_Act_Acc_Entity_Director_Phone
                                        With objNewDirectorPhone
                                            lngPK_Director_Phone = lngPK_Director_Phone - 1

                                            .PK_goAML_Act_Acc_Entity_Director_Phone_ID = lngPK_Director_Phone
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Act_Entity_Director_ID = objDirector_New.PK_Act_Acc_Ent_Director_ID

                                            .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                            .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                            .tph_country_prefix = objPhone.tph_country_prefix
                                            .tph_number = objPhone.tph_number
                                            .tph_extension = objPhone.tph_extension
                                            .comments = objPhone.comments

                                            If objPhone.FK_Ref_Detail_Of = 6 Then
                                                .isEmployer = False
                                            Else
                                                .isEmployer = True
                                            End If

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Act_Acc_Entity_Director_Phone.Add(objNewDirectorPhone)
                                    Next
                                End If

                                'Director Identification(s)
                                'Ada 2 faham yang berbeda untuk FK_Person_Type Director Account di goAML_Person_Identification. Yang diketahui SCB pakai 4, BSIM pakai 5
                                'Sesuaikan global parameter 3001. Karena kalau mau disesuaikan semua harus patches data, ubah SP generate CTR, IFTI, dll.
                                Dim intDirectrorAccountPersonType As Integer = getDirectorAccountPersonType()
                                Dim objListIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = intDirectrorAccountPersonType And x.FK_Person_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                    For Each objIdentification As GoAMLDAL.goAML_Person_Identification In objListIdentification
                                        Dim objNewDirectorIdentification As New goAML_Activity_Person_Identification
                                        With objNewDirectorIdentification
                                            lngPK_Director_Identification = lngPK_Director_Identification - 1

                                            .PK_goAML_Activity_Person_Identification_ID = lngPK_Director_Identification
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Person_Type = GoAMLDAL.GoAMLEnum.enumIdentificationPersonType.DirectorAccount
                                            .FK_Act_Person_ID = objDirector_New.PK_Act_Acc_Ent_Director_ID

                                            .Type = objIdentification.Type
                                            .Number = objIdentification.Number
                                            .Issue_Date = objIdentification.Issue_Date
                                            .Expiry_Date = objIdentification.Expiry_Date
                                            .Issued_By = objIdentification.Issued_By
                                            .Issued_Country = objIdentification.Issued_Country
                                            .Identification_Comment = objIdentification.Identification_Comment

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Activity_Person_Identification.Add(objNewDirectorIdentification)
                                    Next
                                End If
                            Next
                        End If
                    End If
                End If
            End Using

            'Balikin lagi datanya ke Front End
            Return objData
        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

#End Region

#Region "22-Nov-2021 : Save Data To Validate by XML"
    Shared Function validateTransaction(objReport As GoAML_Report_Class, objTransaction As GoAML_Transaction_Class) As String
        Try
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Data to Validate by XML started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim listGoAML_Report As New List(Of GoAMLDAL.goAML_Report)
            Dim listGoAML_Transaction As New List(Of GoAMLDAL.goAML_Transaction)

            listGoAML_Report.Add(objReport.obj_goAML_Report)
            listGoAML_Transaction.Add(objTransaction.obj_goAML_Transaction)

            SaveDataToValidateWithXML(listGoAML_Report)
            SaveDataToValidateWithXML(listGoAML_Transaction)

            If objTransaction.obj_goAML_Transaction.FK_Transaction_Type = 1 Then   'Transaction Bi-Party
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_acc_Signatory)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Acc_sign_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_trn_acc_sign_Phone)

                '' Add 20-Sep-2023, Felix
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_Signatory_Email)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_Signatory_Sanctions)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_Signatory_PEP)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_Signatory_RelatedPerson)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_Signatory_RP_Email)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_Signatory_RP_Sanctions)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_Signatory_RP_PEP)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_Signatory_RP_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_Signatory_RP_Phone)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_Signatory_RP_Identification)
                '' End 20-Sep-2023

                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Entity_account)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Acc_Entity_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Acc_Entity_Phone)

                '' Add 20-Sep-2023, Felix
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_Email)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_Sanctions)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_Identification)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_URL)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_RelatedEntity)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_RE_Email)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_RE_Sanctions)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_RE_Identification)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_RE_URL)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_RE_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_RE_Phone)

                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_Director_Email)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_Director_Sanctions)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Account_EntityAccount_Director_PEP)
                '' End 20-Sep-2023

                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Person)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Person_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_trn_Person_Phone)

                '' Add 20-Sep-2023, Felix
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Person_Sanctions)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Person_PEP)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Person_RelatedPerson)

                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Person_RP_Email)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Person_RP_Sanctions)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Person_RP_PEP)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Person_RP_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Person_RP_Phone)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Person_RP_Identification)
                '' End 20-Sep-2023

                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Entity)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Entity_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Entity_Phone)

                '' Add 20-Sep-2023, Felix
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Entity_Email)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Entity_Sanctions)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Entity_Identification)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Entity_URL)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Entity_RelatedEntity)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_EntityAccount_RE_Email)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_EntityAccount_RE_Sanctions)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_EntityAccount_RE_Identification)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_EntityAccount_RE_URL)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_EntityAccount_RE_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_EntityAccount_RE_Phone)
                '' End 20-Sep-2023

                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Director)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Director_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_trn_Director_Phone)

                '' Add 20-Sep-2023, Felix
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Entity_Director_Email)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Entity_Director_Sanctions)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Entity_Director_PEP)
                '' End 20-Sep-2023

                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Conductor)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Conductor_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_trn_Conductor_Phone)

                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Person_Identification)
            Else    'Transaction Multi-Party
                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Party)

                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Party_Account)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_par_acc_Signatory)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_par_Acc_sign_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_trn_par_acc_sign_Phone)

                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Par_Acc_Entity)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_par_Acc_Entity_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_trn_par_acc_Entity_Phone)

                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Par_Acc_Ent_Director)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Par_Acc_Ent_Director_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Par_Acc_Ent_Director_Phone)

                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Party_Person)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Party_Person_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Party_Person_Phone)

                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Party_Entity)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Party_Entity_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Party_Entity_Phone)

                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Par_Entity_Director)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Par_Entity_Director_Address)
                SaveDataToValidateWithXML(objTransaction.list_goAML_Trn_Par_Entity_Director_Phone)

                SaveDataToValidateWithXML(objTransaction.list_goAML_Transaction_Party_Identification)
            End If

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Data to Validate by XML finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Jalankan SP usp_GOAML_Report_Validate_PerTransactionID
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Validate Transaction started (Transaction ID = " & objTransaction.obj_goAML_Transaction.PK_Transaction_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(3) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = objReport.obj_goAML_Report.PK_Report_ID
            param(0).DbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@PK_TransactionOrActivity_ID"
            param(1).Value = objTransaction.obj_goAML_Transaction.PK_Transaction_ID
            param(1).DbType = SqlDbType.BigInt

            param(2) = New SqlParameter
            param(2).ParameterName = "@UserID"
            param(2).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(2).DbType = SqlDbType.VarChar

            param(3) = New SqlParameter
            param(3).ParameterName = "@TRN_or_ACT"
            param(3).Value = "TRN"
            param(3).DbType = SqlDbType.VarChar

            Dim strValidationResult As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GOAML_Report_Validate_PerTransactionOrActivityID", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Validate Transaction finished (Transaction ID = " & objTransaction.obj_goAML_Transaction.PK_Transaction_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Return strValidationResult

        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function validateActivity(objReport As GoAML_Report_Class, objActivity As GoAML_Activity_Class) As String
        Try
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Data to Validate by XML started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim listGoAML_Report As New List(Of GoAMLDAL.goAML_Report)
            Dim listGoAML_Activity As New List(Of GoAMLDAL.goAML_Act_ReportPartyType)

            listGoAML_Report.Add(objReport.obj_goAML_Report)
            listGoAML_Activity.Add(objActivity.obj_goAML_Act_ReportPartyType)

            SaveDataToValidateWithXML(listGoAML_Report)
            SaveDataToValidateWithXML(listGoAML_Activity)

            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Account)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_acc_Signatory)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Acc_sign_Address)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Acc_sign_Phone)

            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Entity_Account)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Acc_Entity_Address)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Acc_Entity_Phone)

            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Acc_Ent_Director)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Acc_Entity_Director_address)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Acc_Entity_Director_Phone)

            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Person)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Person_Address)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Person_Phone)

            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Entity)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Entity_Address)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Entity_Phone)

            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Director)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Director_Address)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Director_Phone)

            SaveDataToValidateWithXML(objActivity.list_goAML_Activity_Person_Identification)

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Data to Validate by XML finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Jalankan SP usp_GOAML_Report_Validate_PerActivityID
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Validate Activity started (Activity ID = " & objActivity.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(3) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = objReport.obj_goAML_Report.PK_Report_ID
            param(0).DbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@PK_TransactionOrActivity_ID"
            param(1).Value = objActivity.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID
            param(1).DbType = SqlDbType.BigInt

            param(2) = New SqlParameter
            param(2).ParameterName = "@UserID"
            param(2).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(2).DbType = SqlDbType.VarChar

            param(3) = New SqlParameter
            param(3).ParameterName = "@TRN_or_ACT"
            param(3).Value = "ACT"
            param(3).DbType = SqlDbType.VarChar

            Dim strValidationResult As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GOAML_Report_Validate_PerTransactionOrActivityID", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Validate Activity finished (Activity ID = " & objActivity.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Return strValidationResult

        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function validateReportAndIndicator(objReport As GoAML_Report_Class) As String
        Try
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Report & Indicator', 'Save Data to Validate by XML started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim listGoAML_Report As New List(Of GoAMLDAL.goAML_Report)
            listGoAML_Report.Add(objReport.obj_goAML_Report)
            SaveDataToValidateWithXML(listGoAML_Report)
            SaveDataToValidateWithXML(objReport.list_goAML_Report_Indicator)
            If objReport.list_goAML_Transaction IsNot Nothing Then
                SaveDataToValidateWithXML(objReport.list_goAML_Transaction)
            End If
            If objReport.list_goAML_Act_ReportPartyType IsNot Nothing Then
                SaveDataToValidateWithXML(objReport.list_goAML_Act_ReportPartyType)
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Report & Indicator', 'Save Data to Validate by XML finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Jalankan SP usp_GOAML_Report_Validate_ReportAndIndicator untuk Validasi hanya level Report dan Indicator
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Report & Indicator', 'Validate Report & Indicator started (Report ID = " & objReport.obj_goAML_Report.PK_Report_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(1) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = objReport.obj_goAML_Report.PK_Report_ID
            param(0).DbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@UserID"
            param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(1).DbType = SqlDbType.VarChar

            Dim strValidationResult As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GOAML_Report_Validate_ReportAndIndicator", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Report & Indicator', 'Validate Report & Indicator finished (Report ID = " & objReport.obj_goAML_Report.PK_Report_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Ambil lagi semua ValidationReport by ReportID
            strValidationResult = getValidationReportByReportID(objReport.obj_goAML_Report.PK_Report_ID)
            Return strValidationResult

        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Sub SaveDataToValidateWithXML(objData As Object)
        Try
            Dim objtype As Type = objData.GetType
            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
            Dim tablename As String = properties(2).PropertyType.Name

            Dim objdt As New DataTable
            Dim objdataset = New DataSet()
            Dim objXMLData As String = ""
            Dim byteData As Byte() = Nothing

            If objData IsNot Nothing Then
                objdt = NawaBLL.Common.CopyGenericToDataTable(objData)
                objdataset.Tables.Add(objdt)
                objXMLData = objdataset.GetXml()
                '' Add 28-Sep-2022
                objXMLData = objXMLData.Replace("–", "&ndash;")
                objXMLData = objXMLData.Replace("""", "&quot;")
                objXMLData = objXMLData.Replace("'", "&apos;")
                'objXMLData = objXMLData.Replace("<", "&lt;")
                'objXMLData = objXMLData.Replace(">", "&gt;")
                objXMLData = objXMLData.Replace("&", "&amp;")
                '' End 28-Sep-2022

                byteData = System.Text.Encoding.Default.GetBytes(objXMLData)
            End If

            Dim param(2) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@TableName"
            param(0).Value = tablename
            param(0).SqlDbType = SqlDbType.VarChar

            param(1) = New SqlParameter
            param(1).ParameterName = "@XmlDocument"
            param(1).Value = byteData
            param(1).SqlDbType = SqlDbType.VarBinary

            param(2) = New SqlParameter
            param(2).ParameterName = "@UserID"
            param(2).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(2).SqlDbType = SqlDbType.VarChar

            Dim dtPK As DataTable = Nothing
            dtPK = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_Report_XML_ToValidate_Save", param)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "29-Nov-2021 : Delete Report"
    Shared Sub DeleteReportByReportID(strReportID As String, strUserMaker As String, strUserChecker As String)
        Try

            Dim strQuery As String = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Report', 'Delete Report started (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Jalankan semua fungsi Delete, Audit Trail, Update Status dan List of Generated lewat SP
            Dim param(2) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).SqlDbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@UserID_Maker"
            param(1).Value = strUserMaker
            param(1).SqlDbType = SqlDbType.VarChar

            param(2) = New SqlParameter
            param(2).ParameterName = "@UserID_Checker"
            param(2).Value = strUserChecker
            param(2).SqlDbType = SqlDbType.VarChar

            NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GoAML_Report_DeleteReportByReportID", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Report', 'Delete Report finished (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Get Account Signatory Related Person from Search Account. Ripan 28-Jul-2023"
    Shared Function GetBiPartyAccountSignatoryRelatedPersonClassBySignatoryID(objData As GoAML_Transaction_Class, intPK_Signatory_ID As Integer, strSelectedCIF As String, isCustomer As Boolean) As GoAML_Transaction_Class
        Try
            Dim ObjSignatory = objData.list_goAML_Trn_acc_Signatory.Where(Function(x) x.PK_goAML_Trn_acc_Signatory_ID = intPK_Signatory_ID).FirstOrDefault

            'Hapus data2 Signatory yang lama (jika ada)
            If ObjSignatory IsNot Nothing Then
                Dim listAccountSignatoryRelatedPerson_Old = objData.list_goAML_Transaction_Account_Signatory_RelatedPerson.Where(Function(x) x.FK_Parent_Table_ID = ObjSignatory.PK_goAML_Trn_acc_Signatory_ID And x.Parent_Table_Name = "goAML_Trn_acc_Signatory").ToList
                If listAccountSignatoryRelatedPerson_Old IsNot Nothing Then
                    For Each objAccountSignatoryRelatedPerson_Old In listAccountSignatoryRelatedPerson_Old
                        objData.list_goAML_Transaction_Person_RelatedPerson.Remove(objAccountSignatoryRelatedPerson_Old)

                        'Account Signatory Rel Person Address
                        Dim listAddress_Old = objData.list_goAML_Transaction_Account_Signatory_RP_Address.Where(Function(x) x.FK_PARENT_TABLE_ID = objAccountSignatoryRelatedPerson_Old.PK_goAML_Report_Related_Person_ID And x.Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person").ToList
                        If listAddress_Old IsNot Nothing Then
                            For Each objAddress_Old In listAddress_Old
                                objData.list_goAML_Transaction_Account_Signatory_RP_Address.Remove(objAddress_Old)
                            Next
                        End If

                        'Account Signatory Rel Person Phone
                        Dim listPhone_Old = objData.list_goAML_Transaction_Account_Signatory_RP_Phone.Where(Function(x) x.FK_PARENT_TABLE_ID = objAccountSignatoryRelatedPerson_Old.PK_goAML_Report_Related_Person_ID And x.PARENT_TABLE_NAME = "goAML_Trn_acc_Signatory_Related_Person").ToList
                        If listPhone_Old IsNot Nothing Then
                            For Each objPhone_Old In listPhone_Old
                                objData.list_goAML_Transaction_Account_Signatory_RP_Phone.Remove(objPhone_Old)
                            Next
                        End If

                        'Account Signatory Rel Person Identification
                        Dim listIdentification_Old = objData.list_goAML_Transaction_Account_Signatory_RP_Identification.Where(Function(x) x.FK_PARENT_TABLE_ID = objAccountSignatoryRelatedPerson_Old.PK_goAML_Report_Related_Person_ID And x.PARENT_TABLE_NAME = "goAML_Trn_acc_Signatory_Related_Person").ToList
                        If listIdentification_Old IsNot Nothing Then
                            For Each objIdentification_Old In listIdentification_Old
                                objData.list_goAML_Transaction_Account_Signatory_RP_Identification.Remove(objIdentification_Old)
                            Next
                        End If

                        'Account Signatory Rel Person Email
                        Dim listEmail_Old = objData.list_goAML_Transaction_Account_Signatory_RP_Email.Where(Function(x) x.FK_Parent_Table_ID = objAccountSignatoryRelatedPerson_Old.PK_goAML_Report_Related_Person_ID And x.Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person").ToList
                        If listEmail_Old IsNot Nothing Then
                            For Each objEmail_Old In listEmail_Old
                                objData.list_goAML_Transaction_Account_Signatory_RP_Email.Remove(objEmail_Old)
                            Next
                        End If

                        'Account Signatory Rel Person Sanction
                        Dim listSanction_Old = objData.list_goAML_Transaction_Account_Signatory_RP_Sanctions.Where(Function(x) x.FK_Parent_Table_ID = objAccountSignatoryRelatedPerson_Old.PK_goAML_Report_Related_Person_ID And x.Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person").ToList
                        If listSanction_Old IsNot Nothing Then
                            For Each objSanction_Old In listSanction_Old
                                objData.list_goAML_Transaction_Account_Signatory_RP_Sanctions.Remove(objSanction_Old)
                            Next
                        End If

                        'Account Signatory Rel Person PEP
                        Dim listPEP_Old = objData.list_goAML_Transaction_Account_Signatory_RP_PEP.Where(Function(x) x.FK_Parent_Table_ID = objAccountSignatoryRelatedPerson_Old.PK_goAML_Report_Related_Person_ID And x.Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person").ToList
                        If listPEP_Old IsNot Nothing Then
                            For Each objPEP_Old In listPEP_Old
                                objData.list_goAML_Transaction_Account_Signatory_RP_PEP.Remove(objPEP_Old)
                            Next
                        End If
                    Next
                End If
            End If

            'Masukkan data2 baru ke dalam Class
            Using objDB As New GoAMLDAL.GoAMLEntities
                If ObjSignatory IsNot Nothing Then
                    'Get Last PK
                    Dim lngPK_AccountSignatoryRelPerson As Long = 0
                    Dim lngPK_Address As Long = 0
                    Dim lngPK_Phone As Long = 0
                    Dim lngPK_Identification As Long = 0
                    Dim lngPK_Email As Long = 0
                    Dim lngPK_Sanction As Long = 0
                    Dim lngPK_PEP As Long = 0

                    'PK Account Signatory Related Person
                    If objData.list_goAML_Transaction_Account_Signatory_RelatedPerson.Count > 0 Then
                        lngPK_AccountSignatoryRelPerson = objData.list_goAML_Transaction_Account_Signatory_RelatedPerson.Min(Function(x) x.PK_goAML_Report_Related_Person_ID)
                        If lngPK_AccountSignatoryRelPerson > 0 Then
                            lngPK_AccountSignatoryRelPerson = 0
                        End If
                    End If

                    'PK Account Signatory Related Person Address
                    If objData.list_goAML_Transaction_Account_Signatory_RP_Address.Count > 0 Then
                        lngPK_Address = objData.list_goAML_Transaction_Account_Signatory_RP_Address.Min(Function(x) x.PK_Acc_Signatory_Rel_Person_Address_ID)
                        If lngPK_Address > 0 Then
                            lngPK_Address = 0
                        End If
                    End If

                    'PK Account Signatory Related Person Phone
                    If objData.list_goAML_Transaction_Account_Signatory_RP_Phone.Count > 0 Then
                        lngPK_Phone = objData.list_goAML_Transaction_Account_Signatory_RP_Phone.Min(Function(x) x.PK_goAML_Acc_Signatory_Related_Person_Phone_ID)
                        If lngPK_Phone > 0 Then
                            lngPK_Phone = 0
                        End If
                    End If

                    'PK Account Signatory Related Person Identification
                    If objData.list_goAML_Transaction_Account_Signatory_RP_Identification.Count > 0 Then
                        lngPK_Identification = objData.list_goAML_Transaction_Account_Signatory_RP_Identification.Min(Function(x) x.PK_Acc_Sign_Rel_Person_Identification_ID)
                        If lngPK_Identification > 0 Then
                            lngPK_Identification = 0
                        End If
                    End If

                    'PK Account Signatory Related Person Email
                    If objData.list_goAML_Transaction_Account_Signatory_RP_Email.Count > 0 Then
                        lngPK_Email = objData.list_goAML_Transaction_Account_Signatory_RP_Email.Min(Function(x) x.PK_goAML_Report_Email_ID)
                        If lngPK_Email > 0 Then
                            lngPK_Email = 0
                        End If
                    End If

                    'PK Account Signatory Related Person Sanctions
                    If objData.list_goAML_Transaction_Account_Signatory_RP_Sanctions.Count > 0 Then
                        lngPK_Sanction = objData.list_goAML_Transaction_Account_Signatory_RP_Sanctions.Min(Function(x) x.PK_goAML_Report_Sanction_ID)
                        If lngPK_Sanction > 0 Then
                            lngPK_Sanction = 0
                        End If
                    End If

                    'PK Account Signatory Related Person PEP
                    If objData.list_goAML_Transaction_Account_Signatory_RP_PEP.Count > 0 Then
                        lngPK_PEP = objData.list_goAML_Transaction_Account_Signatory_RP_PEP.Min(Function(x) x.PK_goAML_Report_Person_PEP_ID)
                        If lngPK_PEP > 0 Then
                            lngPK_PEP = 0
                        End If
                    End If

                    'Masukkand data2 Related Person dari goAML Ref
                    Dim objListgoAML_Ref_Customer_Related_Person As List(Of GoAMLDAL.goAML_Ref_Customer_Related_Person) = objDB.goAML_Ref_Customer_Related_Person.Where(Function(x) x.CIF = strSelectedCIF).ToList
                    If objListgoAML_Ref_Customer_Related_Person IsNot Nothing Then
                        For Each ObjSignatoryRelPerson In objListgoAML_Ref_Customer_Related_Person

                            Dim ObjSignatoryRelPerson_New As New GoAMLDAL.goAML_Report_Related_Person ''To Do

                            'Get Related Person Nasabah
                            Dim objCIFPersonRelPerson = objDB.goAML_Ref_Customer_Related_Person.Where(Function(x) x.PK_goAML_Ref_Customer_Related_Person_ID = ObjSignatoryRelPerson.PK_goAML_Ref_Customer_Related_Person_ID).FirstOrDefault
                            If objCIFPersonRelPerson IsNot Nothing Then
                                With ObjSignatoryRelPerson_New
                                    lngPK_AccountSignatoryRelPerson = lngPK_AccountSignatoryRelPerson - 1

                                    .PK_goAML_Report_Related_Person_ID = lngPK_AccountSignatoryRelPerson
                                    .FK_Report_ID = ObjSignatory.FK_Report_ID
                                    .FK_Parent_Table_ID = ObjSignatory.PK_goAML_Trn_acc_Signatory_ID
                                    .Parent_Table_Name = "goAML_Trn_acc_Signatory"
                                    .FK_From_Or_To = ObjSignatory.FK_From_Or_To

                                    'Load Person CIF Detail
                                    If objCIFPersonRelPerson IsNot Nothing Then

                                        .gender = objCIFPersonRelPerson.gender
                                        .title = objCIFPersonRelPerson.title
                                        .first_name = objCIFPersonRelPerson.first_name
                                        .middle_name = objCIFPersonRelPerson.middle_name
                                        .prefix = objCIFPersonRelPerson.prefix
                                        .last_name = objCIFPersonRelPerson.last_name
                                        .birthdate = objCIFPersonRelPerson.birthdate
                                        .birth_place = objCIFPersonRelPerson.birth_place
                                        .mothers_name = objCIFPersonRelPerson.mothers_name
                                        .alias = objCIFPersonRelPerson.alias
                                        .ssn = objCIFPersonRelPerson.ssn
                                        .passport_number = objCIFPersonRelPerson.passport_number
                                        .passport_country = objCIFPersonRelPerson.passport_country
                                        .id_number = objCIFPersonRelPerson.id_number
                                        .nationality1 = objCIFPersonRelPerson.nationality1
                                        .nationality2 = objCIFPersonRelPerson.nationality2
                                        .nationality3 = objCIFPersonRelPerson.nationality3
                                        .residence = objCIFPersonRelPerson.residence
                                        .occupation = objCIFPersonRelPerson.occupation
                                        .employer_name = objCIFPersonRelPerson.EMPLOYER_NAME
                                        .deceased = objCIFPersonRelPerson.deceased
                                        .date_deceased = objCIFPersonRelPerson.date_deceased
                                        .tax_number = objCIFPersonRelPerson.tax_number
                                        .tax_reg_number = objCIFPersonRelPerson.tax_reg_number
                                        .source_of_wealth = objCIFPersonRelPerson.source_of_wealth
                                        .comments = objCIFPersonRelPerson.comments
                                        .relation_comments = objCIFPersonRelPerson.relation_comments

                                        .person_person_relation = objCIFPersonRelPerson.person_person_relation
                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End If
                                    objData.list_goAML_Transaction_Account_Signatory_RelatedPerson.Add(ObjSignatoryRelPerson_New)

                                    'Account Signatory Rel Person Phone Address(es)  (31=Address, 32=Employer Address)
                                    Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) x.IsCustomer = isCustomer And (x.FK_Ref_Detail_Of = 31 Or x.FK_Ref_Detail_Of = 32) And x.FK_To_Table_ID = objCIFPersonRelPerson.PK_goAML_Ref_Customer_Related_Person_ID).ToList
                                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                        For Each objAddress In objListAddress
                                            Dim objNewPersonRPAddressNew As New GoAMLDAL.goAML_Acc_Signatory_Related_Person_Address
                                            With objNewPersonRPAddressNew
                                                lngPK_Address = lngPK_Address - 1

                                                .PK_Acc_Signatory_Rel_Person_Address_ID = lngPK_Address
                                                .FK_Report_ID = ObjSignatory.FK_Report_ID
                                                .Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person"
                                                .FK_PARENT_TABLE_ID = lngPK_AccountSignatoryRelPerson
                                                .FK_From_Or_To = ObjSignatory.FK_From_Or_To

                                                .Address_Type = objAddress.Address_Type
                                                .Address = objAddress.Address
                                                .Town = objAddress.Town
                                                .City = objAddress.City
                                                .Zip = objAddress.Zip
                                                .Country_Code = objAddress.Country_Code
                                                .State = objAddress.State
                                                .Comments = objAddress.Comments

                                                If objAddress.FK_Ref_Detail_Of = 31 Then
                                                    .IsEmployer = False
                                                Else
                                                    .IsEmployer = True
                                                End If

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objData.list_goAML_Transaction_Account_Signatory_RP_Address.Add(objNewPersonRPAddressNew)
                                        Next
                                    End If

                                    'Account Signatory Rel Person Phone(s) (31=Phone, 32=Employer Phone)
                                    Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.IsCustomer = isCustomer And (x.FK_Ref_Detail_Of = 31 Or x.FK_Ref_Detail_Of = 32) And x.FK_for_Table_ID = objCIFPersonRelPerson.PK_goAML_Ref_Customer_Related_Person_ID).ToList
                                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                        For Each objPhone In objListPhone
                                            Dim objNewPersonRPPhone As New GoAMLDAL.goAML_Acc_Signatory_Related_Person_Phone
                                            With objNewPersonRPPhone
                                                lngPK_Phone = lngPK_Phone - 1

                                                .PK_goAML_Acc_Signatory_Related_Person_Phone_ID = lngPK_Phone
                                                .FK_Report_ID = ObjSignatory.FK_Report_ID
                                                .FK_PARENT_TABLE_ID = lngPK_AccountSignatoryRelPerson
                                                .PARENT_TABLE_NAME = "goAML_Trn_acc_Signatory_Related_Person"
                                                .FK_From_Or_To = ObjSignatory.FK_From_Or_To

                                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                .tph_country_prefix = objPhone.tph_country_prefix
                                                .tph_number = objPhone.tph_number
                                                .tph_extension = objPhone.tph_extension
                                                .comments = objPhone.comments

                                                If objPhone.FK_Ref_Detail_Of = 31 Then
                                                    .isEmployer = False
                                                Else
                                                    .isEmployer = True
                                                End If

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objData.list_goAML_Transaction_Account_Signatory_RP_Phone.Add(objNewPersonRPPhone)
                                        Next
                                    End If

                                    'Account Signatory Rel Person Identification(s)
                                    Dim objListIdentification As List(Of GoAMLDAL.goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.IsCustomer = isCustomer And x.FK_Person_Type = 31 And x.FK_Person_ID = objCIFPersonRelPerson.PK_goAML_Ref_Customer_Related_Person_ID).ToList
                                    If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                        For Each objIdentification As GoAMLDAL.goAML_Person_Identification In objListIdentification
                                            Dim objNewPersonRPIdentification As New GoAMLDAL.goAML_Acc_Signatory_Related_Person_Identification
                                            With objNewPersonRPIdentification
                                                lngPK_Identification = lngPK_Identification - 1

                                                .PK_Acc_Sign_Rel_Person_Identification_ID = lngPK_Identification
                                                .FK_Report_ID = ObjSignatory.FK_Report_ID
                                                .PARENT_TABLE_NAME = "goAML_Trn_acc_Signatory_Related_Person"
                                                .FK_PARENT_TABLE_ID = lngPK_AccountSignatoryRelPerson
                                                .FK_From_Or_To = ObjSignatory.FK_From_Or_To

                                                .Type = objIdentification.Type
                                                .Number = objIdentification.Number
                                                .Issue_Date = objIdentification.Issue_Date
                                                .Expiry_Date = objIdentification.Expiry_Date
                                                .Issued_By = objIdentification.Issued_By
                                                .Issued_Country = objIdentification.Issued_Country
                                                .Identification_Comment = objIdentification.Identification_Comment

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objData.list_goAML_Transaction_Account_Signatory_RP_Identification.Add(objNewPersonRPIdentification)
                                        Next
                                    End If

                                    'Account Signatory Rel Person Email(s)
                                    Dim objListEmail As List(Of GoAMLDAL.goAML_Ref_Customer_Email) = objDB.goAML_Ref_Customer_Email.Where(Function(x) x.FK_REF_DETAIL_OF = 31 And x.FK_FOR_TABLE_ID = objCIFPersonRelPerson.PK_goAML_Ref_Customer_Related_Person_ID).ToList
                                    If objListEmail IsNot Nothing AndAlso objListEmail.Count > 0 Then
                                        For Each objEmail As GoAMLDAL.goAML_Ref_Customer_Email In objListEmail
                                            Dim objNewPersonRPEmail As New GoAMLDAL.goAML_Report_Email
                                            With objNewPersonRPEmail
                                                lngPK_Email = lngPK_Email - 1

                                                .PK_goAML_Report_Email_ID = lngPK_Email
                                                .FK_Report_ID = ObjSignatory.FK_Report_ID
                                                .Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person"
                                                .FK_Parent_Table_ID = lngPK_AccountSignatoryRelPerson
                                                .FK_From_Or_To = ObjSignatory.FK_From_Or_To

                                                .email_address = objEmail.EMAIL_ADDRESS

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objData.list_goAML_Transaction_Account_Signatory_RP_Email.Add(objNewPersonRPEmail)
                                        Next
                                    End If

                                    'Account Signatory Rel Person Sanction(s)
                                    Dim objListSanction As List(Of GoAMLDAL.goAML_Ref_Customer_Sanction) = objDB.goAML_Ref_Customer_Sanction.Where(Function(x) x.FK_REF_DETAIL_OF = 31 And x.FK_FOR_TABLE_ID = objCIFPersonRelPerson.PK_goAML_Ref_Customer_Related_Person_ID).ToList
                                    If objListSanction IsNot Nothing AndAlso objListSanction.Count > 0 Then
                                        For Each objSanction As GoAMLDAL.goAML_Ref_Customer_Sanction In objListSanction
                                            Dim objNewPersonRPSanction As New GoAMLDAL.goAML_Report_Sanction
                                            With objNewPersonRPSanction
                                                lngPK_Sanction = lngPK_Sanction - 1

                                                .PK_goAML_Report_Sanction_ID = lngPK_Sanction
                                                .FK_Report_ID = ObjSignatory.FK_Report_ID
                                                .Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person"
                                                .FK_Parent_Table_ID = lngPK_AccountSignatoryRelPerson
                                                .FK_From_Or_To = ObjSignatory.FK_From_Or_To

                                                .provider = objSanction.PROVIDER
                                                .sanction_list_name = objSanction.SANCTION_LIST_NAME
                                                .match_criteria = objSanction.MATCH_CRITERIA
                                                .link_to_source = objSanction.LINK_TO_SOURCE
                                                .comments = objSanction.COMMENTS

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objData.list_goAML_Transaction_Account_Signatory_RP_Sanctions.Add(objNewPersonRPSanction)
                                        Next
                                    End If

                                    'Account Signatory Rel Person PEP(s)
                                    Dim objListPEP As List(Of GoAMLDAL.goAML_Ref_Customer_PEP) = objDB.goAML_Ref_Customer_PEP.Where(Function(x) x.FK_REF_DETAIL_OF = 31 And x.FK_FOR_TABLE_ID = objCIFPersonRelPerson.PK_goAML_Ref_Customer_Related_Person_ID).ToList
                                    If objListPEP IsNot Nothing AndAlso objListPEP.Count > 0 Then
                                        For Each objPEP As GoAMLDAL.goAML_Ref_Customer_PEP In objListPEP
                                            Dim objNewPersonRPPEP As New GoAMLDAL.goAML_Report_Person_PEP
                                            With objNewPersonRPPEP
                                                lngPK_PEP = lngPK_PEP - 1

                                                .PK_goAML_Report_Person_PEP_ID = lngPK_PEP
                                                .FK_Report_ID = ObjSignatory.FK_Report_ID
                                                .Parent_Table_Name = "goAML_Trn_acc_Signatory_Related_Person"
                                                .FK_Parent_Table_ID = lngPK_AccountSignatoryRelPerson
                                                .FK_From_Or_To = ObjSignatory.FK_From_Or_To

                                                .pep_country = objPEP.PEP_COUNTRY
                                                .function_name = objPEP.FUNCTION_NAME
                                                .function_description = objPEP.FUNCTION_DESCRIPTION
                                                .comments = objPEP.COMMENTS

                                                .Active = 1
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objData.list_goAML_Transaction_Account_Signatory_RP_PEP.Add(objNewPersonRPPEP)
                                        Next
                                    End If

                                End With
                            End If

                        Next
                    End If
                End If
            End Using

            'Balikin lagi datanya ke Front End
            Return objData
        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function
#End Region

End Class


Public Class GoAML_Report_Class

    'GoAML Report
    Public obj_goAML_Report As New goAML_Report

    'Transaction
    Public list_goAML_Transaction As New List(Of GoAMLDAL.goAML_Transaction)

    'Activity
    Public list_goAML_Act_ReportPartyType As New List(Of GoAMLDAL.goAML_Act_ReportPartyType)

    'Indicator
    Public list_goAML_Report_Indicator As New List(Of GoAMLDAL.goAML_Report_Indicator)

End Class


Public Class GoAML_Transaction_Class
    'Transaction
    Public obj_goAML_Transaction As New GoAMLDAL.goAML_Transaction

    'Transaction Bi-Party
    Public list_goAML_Transaction_Account As New List(Of GoAMLDAL.goAML_Transaction_Account)
    Public list_goAML_Trn_acc_Signatory As New List(Of GoAMLDAL.goAML_Trn_acc_Signatory)
    Public list_goAML_Trn_Acc_sign_Address As New List(Of GoAMLDAL.goAML_Trn_Acc_sign_Address)
    Public list_goAML_trn_acc_sign_Phone As New List(Of GoAMLDAL.goAML_trn_acc_sign_Phone)

    Public list_goAML_Trn_Entity_account As New List(Of GoAMLDAL.goAML_Trn_Entity_account)
    Public list_goAML_Trn_Acc_Entity_Address As New List(Of GoAMLDAL.goAML_Trn_Acc_Entity_Address)
    Public list_goAML_Trn_Acc_Entity_Phone As New List(Of GoAMLDAL.goAML_Trn_Acc_Entity_Phone)

    Public list_goAML_Transaction_Person As New List(Of GoAMLDAL.goAML_Transaction_Person)
    Public list_goAML_Trn_Person_Address As New List(Of GoAMLDAL.goAML_Trn_Person_Address)
    Public list_goAML_trn_Person_Phone As New List(Of GoAMLDAL.goAML_trn_Person_Phone)

    Public list_goAML_Transaction_Entity As New List(Of GoAMLDAL.goAML_Transaction_Entity)
    Public list_goAML_Trn_Entity_Address As New List(Of GoAMLDAL.goAML_Trn_Entity_Address)
    Public list_goAML_Trn_Entity_Phone As New List(Of GoAMLDAL.goAML_Trn_Entity_Phone)

    Public list_goAML_Trn_Director As New List(Of GoAMLDAL.goAML_Trn_Director)
    Public list_goAML_Trn_Director_Address As New List(Of GoAMLDAL.goAML_Trn_Director_Address)
    Public list_goAML_trn_Director_Phone As New List(Of GoAMLDAL.goAML_trn_Director_Phone)

    Public list_goAML_Trn_Conductor As New List(Of GoAMLDAL.goAML_Trn_Conductor)
    Public list_goAML_Trn_Conductor_Address As New List(Of GoAMLDAL.goAML_Trn_Conductor_Address)
    Public list_goAML_trn_Conductor_Phone As New List(Of GoAMLDAL.goAML_trn_Conductor_Phone)

    Public list_goAML_Transaction_Person_Identification As New List(Of GoAMLDAL.goAML_Transaction_Person_Identification)

    'Transaction Multi Party
    Public list_goAML_Transaction_Party As New List(Of GoAMLDAL.goAML_Transaction_Party)

    Public list_goAML_Trn_Party_Account As New List(Of GoAMLDAL.goAML_Trn_Party_Account)
    Public list_goAML_Trn_par_acc_Signatory As New List(Of GoAMLDAL.goAML_Trn_par_acc_Signatory)
    Public list_goAML_Trn_par_Acc_sign_Address As New List(Of GoAMLDAL.goAML_Trn_par_Acc_sign_Address)
    Public list_goAML_trn_par_acc_sign_Phone As New List(Of GoAMLDAL.goAML_trn_par_acc_sign_Phone)

    Public list_goAML_Trn_Par_Acc_Entity As New List(Of GoAMLDAL.goAML_Trn_Par_Acc_Entity)
    Public list_goAML_Trn_par_Acc_Entity_Address As New List(Of GoAMLDAL.goAML_Trn_par_Acc_Entity_Address)
    Public list_goAML_trn_par_acc_Entity_Phone As New List(Of GoAMLDAL.goAML_trn_par_acc_Entity_Phone)

    Public list_goAML_Trn_Par_Acc_Ent_Director As New List(Of GoAMLDAL.goAML_Trn_Par_Acc_Ent_Director)
    Public list_goAML_Trn_Par_Acc_Ent_Director_Address As New List(Of GoAMLDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
    Public list_goAML_Trn_Par_Acc_Ent_Director_Phone As New List(Of GoAMLDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)

    Public list_goAML_Trn_Party_Person As New List(Of GoAMLDAL.goAML_Trn_Party_Person)
    Public list_goAML_Trn_Party_Person_Address As New List(Of GoAMLDAL.goAML_Trn_Party_Person_Address)
    Public list_goAML_Trn_Party_Person_Phone As New List(Of GoAMLDAL.goAML_Trn_Party_Person_Phone)

    Public list_goAML_Trn_Party_Entity As New List(Of GoAMLDAL.goAML_Trn_Party_Entity)
    Public list_goAML_Trn_Party_Entity_Address As New List(Of GoAMLDAL.goAML_Trn_Party_Entity_Address)
    Public list_goAML_Trn_Party_Entity_Phone As New List(Of GoAMLDAL.goAML_Trn_Party_Entity_Phone)

    Public list_goAML_Trn_Par_Entity_Director As New List(Of GoAMLDAL.goAML_Trn_Par_Entity_Director)
    Public list_goAML_Trn_Par_Entity_Director_Address As New List(Of GoAMLDAL.goAML_Trn_Par_Entity_Director_Address)
    Public list_goAML_Trn_Par_Entity_Director_Phone As New List(Of GoAMLDAL.goAML_Trn_Par_Entity_Director_Phone)

    Public list_goAML_Transaction_Party_Identification As New List(Of GoAMLDAL.goAML_Transaction_Party_Identification)

    'Ari 12 April 2023 : List Table Transaction baru untuk goaml v.5.1
    'Transaction Address
    Public obj_goAML_Transaction_Address As New GoAMLDAL.goAML_Transaction_Address
    'Transaction - FromTo - Account - Signatory
    Public list_goAML_Transaction_Account_Signatory_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Transaction_Account_Signatory_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Transaction_Account_Signatory_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    Public list_goAML_Transaction_Account_Signatory_RelatedPerson As New List(Of GoAMLDAL.goAML_Report_Related_Person)
    'Transaction - FromTo - Account - Signatory - Related Person
    Public list_goAML_Transaction_Account_Signatory_RP_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Transaction_Account_Signatory_RP_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Transaction_Account_Signatory_RP_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    Public list_goAML_Transaction_Account_Signatory_RP_Address As New List(Of GoAMLDAL.goAML_Acc_Signatory_Related_Person_Address)
    Public list_goAML_Transaction_Account_Signatory_RP_Phone As New List(Of GoAMLDAL.goAML_Acc_Signatory_Related_Person_Phone)
    Public list_goAML_Transaction_Account_Signatory_RP_Identification As New List(Of GoAMLDAL.goAML_Acc_Signatory_Related_Person_Identification)
    'Transaction - FromTo - Account - Entity Account
    Public list_goAML_Transaction_Account_EntityAccount_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Transaction_Account_EntityAccount_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Transaction_Account_EntityAccount_Identification As New List(Of GoAMLDAL.goAML_Report_Identification)
    Public list_goAML_Transaction_Account_EntityAccount_URL As New List(Of GoAMLDAL.goAML_Report_Url)
    Public list_goAML_Transaction_Account_EntityAccount_RelatedEntity As New List(Of GoAMLDAL.goAML_Report_Related_Entities)
    'Transaction - FromTo - Account - Entity Account - Related Entity
    Public list_goAML_Transaction_Account_EntityAccount_RE_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Transaction_Account_EntityAccount_RE_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Transaction_Account_EntityAccount_RE_Identification As New List(Of GoAMLDAL.goAML_Report_Identification)
    Public list_goAML_Transaction_Account_EntityAccount_RE_URL As New List(Of GoAMLDAL.goAML_Report_Url)
    Public list_goAML_Transaction_Account_EntityAccount_RE_Address As New List(Of GoAMLDAL.goAML_Acc_Entity_Related_Entity_Address)
    Public list_goAML_Transaction_Account_EntityAccount_RE_Phone As New List(Of GoAMLDAL.goAML_Acc_Entity_Related_Entity_Phone)
    ''Transaction - FromTo - Account - Entity Account - Director
    Public list_goAML_Transaction_Account_EntityAccount_Director_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Transaction_Account_EntityAccount_Director_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Transaction_Account_EntityAccount_Director_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    'Transaction - FromTo - Account - Related Account
    'Public list_goAML_Transaction_Account_RelatedAccount As New List(Of goAML_Ref_Account_Related_Account)
    ''Transaction - FromTo - Account - Account Related Entity
    'Public list_goAML_Transaction_Account_AccountRelatedEntity As New List(Of goAML_Ref_Account_Related_Entity)

    'Transaction - FromTo - Person 
    Public list_goAML_Transaction_Person_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Transaction_Person_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Transaction_Person_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    Public list_goAML_Transaction_Person_RelatedPerson As New List(Of GoAMLDAL.goAML_Report_Related_Person)
    'Transaction - FromTo - Person - Related Person
    Public list_goAML_Transaction_Person_RP_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Transaction_Person_RP_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Transaction_Person_RP_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    Public list_goAML_Transaction_Person_RP_Address As New List(Of GoAMLDAL.goAML_Report_Person_Related_Person_Address)
    Public list_goAML_Transaction_Person_RP_Phone As New List(Of GoAMLDAL.goAML_Report_Person_Related_Person_Phone)
    Public list_goAML_Transaction_Person_RP_Identification As New List(Of GoAMLDAL.goAML_Report_Person_Related_Person_Identification)
    'Transaction - FromTo - Entity
    Public list_goAML_Transaction_Entity_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Transaction_Entity_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Transaction_Entity_Identification As New List(Of GoAMLDAL.goAML_Report_Identification)
    Public list_goAML_Transaction_Entity_URL As New List(Of GoAMLDAL.goAML_Report_Url)
    Public list_goAML_Transaction_Entity_RelatedEntity As New List(Of GoAMLDAL.goAML_Report_Related_Entities)
    'Transaction - FromTo - Entity - Related Entity
    Public list_goAML_Transaction_EntityAccount_RE_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Transaction_EntityAccount_RE_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Transaction_EntityAccount_RE_Identification As New List(Of GoAMLDAL.goAML_Report_Identification)
    Public list_goAML_Transaction_EntityAccount_RE_URL As New List(Of GoAMLDAL.goAML_Report_Url)
    Public list_goAML_Transaction_EntityAccount_RE_Address As New List(Of GoAMLDAL.goAML_Report_Entity_Related_Entity_Address)
    Public list_goAML_Transaction_EntityAccount_RE_Phone As New List(Of GoAMLDAL.goAML_Report_Entity_Related_Entity_Phone)

    'Transaction - FromTo - Entity - Director
    Public list_goAML_Transaction_Entity_Director_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Transaction_Entity_Director_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Transaction_Entity_Director_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)

    'Multiparty - FromTo - Account - Signatory
    Public list_goAML_Multiparty_Account_Signatory_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Multiparty_Account_Signatory_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Multiparty_Account_Signatory_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    Public list_goAML_Multiparty_Account_Signatory_RelatedPerson As New List(Of GoAMLDAL.goAML_Report_Related_Person)
    'Multiparty - FromTo - Account - Signatory - Related Person
    Public list_goAML_Multiparty_Account_Signatory_RP_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Multiparty_Account_Signatory_RP_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Multiparty_Account_Signatory_RP_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    Public list_goAML_Multiparty_Account_Signatory_RP_Address As New List(Of GoAMLDAL.goAML_Acc_Signatory_Related_Person_Address)
    Public list_goAML_Multiparty_Account_Signatory_RP_Phone As New List(Of GoAMLDAL.goAML_Acc_Signatory_Related_Person_Phone)
    Public list_goAML_Multiparty_Account_Signatory_RP_Identification As New List(Of GoAMLDAL.goAML_Acc_Signatory_Related_Person_Identification)
    'Multiparty - FromTo - Account - Entity Account
    Public list_goAML_Multiparty_Account_EntityAccount_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Multiparty_Account_EntityAccount_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Multiparty_Account_EntityAccount_Identification As New List(Of GoAMLDAL.goAML_Report_Identification)
    Public list_goAML_Multiparty_Account_EntityAccount_URL As New List(Of GoAMLDAL.goAML_Report_Url)
    Public list_goAML_Multiparty_Account_EntityAccount_RelatedEntity As New List(Of GoAMLDAL.goAML_Report_Related_Entities)
    'Multiparty - FromTo - Account - Entity Account - Related Entity
    Public list_goAML_Multiparty_Account_EntityAccount_RE_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Multiparty_Account_EntityAccount_RE_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Multiparty_Account_EntityAccount_RE_Identification As New List(Of GoAMLDAL.goAML_Report_Identification)
    Public list_goAML_Multiparty_Account_EntityAccount_RE_URL As New List(Of GoAMLDAL.goAML_Report_Url)
    Public list_goAML_Multiparty_Account_EntityAccount_RE_Address As New List(Of GoAMLDAL.goAML_Acc_Entity_Related_Entity_Address)
    Public list_goAML_Multiparty_Account_EntityAccount_RE_Phone As New List(Of GoAMLDAL.goAML_Acc_Entity_Related_Entity_Phone)
    ''Multiparty - FromTo - Account - Entity Account - Director
    Public list_goAML_Multiparty_Account_EntityAccount_Director_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Multiparty_Account_EntityAccount_Director_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Multiparty_Account_EntityAccount_Director_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    'Multiparty - FromTo - Account - Related Account
    Public list_goAML_Multiparty_Account_RelatedAccount As New List(Of GoAMLDAL.goAML_Report_Related_Account)
    ''Multiparty - FromTo - Account - Account Related Entity
    'Public list_goAML_Multiparty_Account_AccountRelatedEntity As New List(Of goAML_Ref_Account_Related_Entity)

    'Multiparty - FromTo - Person 
    Public list_goAML_Multiparty_Person_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Multiparty_Person_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Multiparty_Person_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    Public list_goAML_Multiparty_Person_RelatedPerson As New List(Of GoAMLDAL.goAML_Report_Related_Person)
    'Multiparty - FromTo - Person - Related Person
    Public list_goAML_Multiparty_Person_RP_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Multiparty_Person_RP_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Multiparty_Person_RP_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    Public list_goAML_Multiparty_Person_RP_Address As New List(Of GoAMLDAL.goAML_Report_Person_Related_Person_Address)
    Public list_goAML_Multiparty_Person_RP_Phone As New List(Of GoAMLDAL.goAML_Report_Person_Related_Person_Phone)
    Public list_goAML_Multiparty_Person_RP_Identification As New List(Of GoAMLDAL.goAML_Report_Person_Related_Person_Identification)
    'Multiparty - FromTo - Entity
    Public list_goAML_Multiparty_Entity_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Multiparty_Entity_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Multiparty_Entity_Identification As New List(Of GoAMLDAL.goAML_Report_Identification)
    Public list_goAML_Multiparty_Entity_URL As New List(Of GoAMLDAL.goAML_Report_Url)
    Public list_goAML_Multiparty_Entity_RelatedEntity As New List(Of GoAMLDAL.goAML_Report_Related_Entities)
    'Multiparty - FromTo - Entity - Related Entity
    Public list_goAML_Multiparty_EntityAccount_RE_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Multiparty_EntityAccount_RE_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Multiparty_EntityAccount_RE_Identification As New List(Of GoAMLDAL.goAML_Report_Identification)
    Public list_goAML_Multiparty_EntityAccount_RE_URL As New List(Of GoAMLDAL.goAML_Report_Url)
    Public list_goAML_Multiparty_EntityAccount_RE_Address As New List(Of GoAMLDAL.goAML_Report_Entity_Related_Entity_Address)
    Public list_goAML_Multiparty_EntityAccount_RE_Phone As New List(Of GoAMLDAL.goAML_Report_Entity_Related_Entity_Phone)

    'Multiparty - FromTo - Entity - Director
    Public list_goAML_Multiparty_Entity_Director_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Multiparty_Entity_Director_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Multiparty_Entity_Director_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)


End Class

Public Class GoAML_Activity_Class
    Public obj_goAML_Act_ReportPartyType As New goAML_Act_ReportPartyType

    Public list_goAML_Act_Account As New List(Of GoAMLDAL.goAML_Act_Account)
    Public list_goAML_Act_acc_Signatory As New List(Of GoAMLDAL.goAML_Act_acc_Signatory)
    Public list_goAML_Act_Acc_sign_Address As New List(Of GoAMLDAL.goAML_Act_Acc_sign_Address)
    Public list_goAML_Act_Acc_sign_Phone As New List(Of GoAMLDAL.goAML_Act_Acc_sign_Phone)

    Public list_goAML_Act_Entity_Account As New List(Of GoAMLDAL.goAML_Act_Entity_Account)
    Public list_goAML_Act_Acc_Entity_Address As New List(Of GoAMLDAL.goAML_Act_Acc_Entity_Address)
    Public list_goAML_Act_Acc_Entity_Phone As New List(Of GoAMLDAL.goAML_Act_Acc_Entity_Phone)

    Public list_goAML_Act_Acc_Ent_Director As New List(Of GoAMLDAL.goAML_Act_Acc_Ent_Director)
    Public list_goAML_Act_Acc_Entity_Director_address As New List(Of GoAMLDAL.goAML_Act_Acc_Entity_Director_address)
    Public list_goAML_Act_Acc_Entity_Director_Phone As New List(Of GoAMLDAL.goAML_Act_Acc_Entity_Director_Phone)

    Public list_goAML_Act_Person As New List(Of GoAMLDAL.goAML_Act_Person)
    Public list_goAML_Act_Person_Address As New List(Of GoAMLDAL.goAML_Act_Person_Address)
    Public list_goAML_Act_Person_Phone As New List(Of GoAMLDAL.goAML_Act_Person_Phone)

    Public list_goAML_Act_Entity As New List(Of GoAMLDAL.goAML_Act_Entity)
    Public list_goAML_Act_Entity_Address As New List(Of GoAMLDAL.goAML_Act_Entity_Address)
    Public list_goAML_Act_Entity_Phone As New List(Of GoAMLDAL.goAML_Act_Entity_Phone)

    Public list_goAML_Act_Director As New List(Of GoAMLDAL.goAML_Act_Director)
    Public list_goAML_Act_Director_Address As New List(Of GoAMLDAL.goAML_Act_Director_Address)
    Public list_goAML_Act_Director_Phone As New List(Of GoAMLDAL.goAML_Act_Director_Phone)

    Public list_goAML_Activity_Person_Identification As New List(Of GoAMLDAL.goAML_Activity_Person_Identification)

    'Ari 12 April 2023 : List Table Activity baru untuk goaml v.5.1
    'Activity - FromTo - Account - Signatory
    Public list_goAML_Activity_Account_Signatory_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Activity_Account_Signatory_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Activity_Account_Signatory_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    Public list_goAML_Activity_Account_Signatory_RelatedPerson As New List(Of GoAMLDAL.goAML_Report_Related_Person)
    'Activity - FromTo - Account - Signatory - Related Person
    Public list_goAML_Activity_Account_Signatory_RP_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Activity_Account_Signatory_RP_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Activity_Account_Signatory_RP_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    Public list_goAML_Activity_Account_Signatory_RP_Address As New List(Of GoAMLDAL.goAML_Acc_Signatory_Related_Person_Address)
    Public list_goAML_Activity_Account_Signatory_RP_Phone As New List(Of GoAMLDAL.goAML_Acc_Signatory_Related_Person_Phone)
    Public list_goAML_Activity_Account_Signatory_RP_Identification As New List(Of GoAMLDAL.goAML_Acc_Signatory_Related_Person_Identification)
    'Activity - FromTo - Account - Entity Account
    Public list_goAML_Activity_Account_EntityAccount_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Activity_Account_EntityAccount_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Activity_Account_EntityAccount_Identification As New List(Of GoAMLDAL.goAML_Report_Identification)
    Public list_goAML_Activity_Account_EntityAccount_URL As New List(Of GoAMLDAL.goAML_Report_Url)
    Public list_goAML_Activity_Account_EntityAccount_RelatedEntity As New List(Of GoAMLDAL.goAML_Report_Related_Entities)
    'Activity - FromTo - Account - Entity Account - Related Entity
    Public list_goAML_Activity_Account_EntityAccount_RE_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Activity_Account_EntityAccount_RE_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Activity_Account_EntityAccount_RE_Identification As New List(Of GoAMLDAL.goAML_Report_Identification)
    Public list_goAML_Activity_Account_EntityAccount_RE_URL As New List(Of GoAMLDAL.goAML_Report_Url)
    Public list_goAML_Activity_Account_EntityAccount_RE_Address As New List(Of GoAMLDAL.goAML_Acc_Entity_Related_Entity_Address)
    Public list_goAML_Activity_Account_EntityAccount_RE_Phone As New List(Of GoAMLDAL.goAML_Acc_Entity_Related_Entity_Phone)
    ''Activity - FromTo - Account - Entity Account - Director
    Public list_goAML_Activity_Account_EntityAccount_Director_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Activity_Account_EntityAccount_Director_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Activity_Account_EntityAccount_Director_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    'Activity - FromTo - Account - Related Account
    Public list_goAML_Activity_Account_RelatedAccount As New List(Of GoAMLDAL.goAML_Report_Related_Account)
    ''Activity - FromTo - Account - Account Related Entity
    'Public list_goAML_Activity_Account_AccountRelatedEntity As New List(Of goAML_Ref_Account_Related_Entity)

    'Activity - FromTo - Person 
    Public list_goAML_Activity_Person_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Activity_Person_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Activity_Person_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    Public list_goAML_Activity_Person_RelatedPerson As New List(Of GoAMLDAL.goAML_Report_Related_Person)
    'Activity - FromTo - Person - Related Person
    Public list_goAML_Activity_Person_RP_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Activity_Person_RP_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Activity_Person_RP_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
    Public list_goAML_Activity_Person_RP_Address As New List(Of GoAMLDAL.goAML_Report_Person_Related_Person_Address)
    Public list_goAML_Activity_Person_RP_Phone As New List(Of GoAMLDAL.goAML_Report_Person_Related_Person_Phone)
    Public list_goAML_Activity_Person_RP_Identification As New List(Of GoAMLDAL.goAML_Report_Person_Related_Person_Identification)
    'Activity - FromTo - Entity
    Public list_goAML_Activity_Entity_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Activity_Entity_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Activity_Entity_Identification As New List(Of GoAMLDAL.goAML_Report_Identification)
    Public list_goAML_Activity_Entity_URL As New List(Of GoAMLDAL.goAML_Report_Url)
    Public list_goAML_Activity_Entity_RelatedEntity As New List(Of GoAMLDAL.goAML_Report_Related_Entities)
    'Activity - FromTo - Entity - Related Entity
    Public list_goAML_Activity_EntityAccount_RE_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Activity_EntityAccount_RE_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Activity_EntityAccount_RE_Identification As New List(Of GoAMLDAL.goAML_Report_Identification)
    Public list_goAML_Activity_EntityAccount_RE_URL As New List(Of GoAMLDAL.goAML_Report_Url)
    Public list_goAML_Activity_EntityAccount_RE_Address As New List(Of GoAMLDAL.goAML_Report_Entity_Related_Entity_Address)
    Public list_goAML_Activity_EntityAccount_RE_Phone As New List(Of GoAMLDAL.goAML_Report_Entity_Related_Entity_Phone)

    'Activity - FromTo - Entity - Director
    Public list_goAML_Activity_Entity_Director_Email As New List(Of GoAMLDAL.goAML_Report_Email)
    Public list_goAML_Activity_Entity_Director_Sanctions As New List(Of GoAMLDAL.goAML_Report_Sanction)
    Public list_goAML_Activity_Entity_Director_PEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)

#Region "GoAML v.5.1"
    Shared Function getEmailsByID(id As String) As List(Of GoAMLDAL.goAML_Report_Email)
        Dim listEmail As New List(Of GoAMLDAL.goAML_Report_Email)
        Dim listObjEmail As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Report_Email where PK_goAML_Report_Email_ID = '" & id & "'", Nothing)
        For Each item As DataRow In listObjEmail.Rows
            Dim Email As New GoAMLDAL.goAML_Report_Email
            Email.PK_goAML_Report_Email_ID = item("PK_goAML_Report_Email_ID")
            If Not IsDBNull(item("email_address")) Then
                Email.email_address = item("email_address")
            End If
            If Not IsDBNull(item("FK_From_Or_To")) Then
                Email.FK_From_Or_To = item("FK_From_Or_To")
            End If
            If Not IsDBNull(item("FK_Parent_Table_ID")) Then
                Email.FK_Parent_Table_ID = item("FK_Parent_Table_ID")
            End If
            If Not IsDBNull(item("FK_Report_ID")) Then
                Email.FK_Report_ID = item("FK_Report_ID")
            End If
            If Not IsDBNull(item("Parent_Table_Name")) Then
                Email.Parent_Table_Name = item("Parent_Table_Name")
            End If
            If Not IsDBNull(item("CreatedDate")) Then
                Email.CreatedDate = item("CreatedDate")
            End If
            If Not IsDBNull(item("CreatedBy")) Then
                Email.CreatedBy = item("CreatedBy")
            End If
            If Not IsDBNull(item("LastUpdateBy")) Then
                Email.LastUpdateBy = item("LastUpdateBy")
            End If
            If Not IsDBNull(item("LastUpdateDate")) Then
                Email.LastUpdateDate = item("LastUpdateDate")
            End If

            listEmail.Add(Email)
        Next
        Return listEmail
    End Function

    Shared Function getSanctionsByID(id As String) As List(Of GoAMLDAL.goAML_Report_Sanction)
        Dim listSanction As New List(Of GoAMLDAL.goAML_Report_Sanction)
        Dim listObjSanction As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Report_Sanction where PK_goAML_Report_Sanction_ID = '" & id & "'", Nothing)
        For Each item As DataRow In listObjSanction.Rows
            Dim Sanction As New GoAMLDAL.goAML_Report_Sanction
            Sanction.PK_goAML_Report_Sanction_ID = item("PK_goAML_Report_Sanction_ID")
            If Not IsDBNull(item("comments")) Then
                Sanction.comments = item("comments")
            End If
            If Not IsDBNull(item("FK_From_Or_To")) Then
                Sanction.FK_From_Or_To = item("FK_From_Or_To")
            End If
            If Not IsDBNull(item("FK_Parent_Table_ID")) Then
                Sanction.FK_Parent_Table_ID = item("FK_Parent_Table_ID")
            End If
            If Not IsDBNull(item("FK_Report_ID")) Then
                Sanction.FK_Report_ID = item("FK_Report_ID")
            End If
            If Not IsDBNull(item("Parent_Table_Name")) Then
                Sanction.Parent_Table_Name = item("Parent_Table_Name")
            End If
            If Not IsDBNull(item("CreatedDate")) Then
                Sanction.CreatedDate = item("CreatedDate")
            End If
            If Not IsDBNull(item("CreatedBy")) Then
                Sanction.CreatedBy = item("CreatedBy")
            End If
            If Not IsDBNull(item("LastUpdateBy")) Then
                Sanction.LastUpdateBy = item("LastUpdateBy")
            End If
            If Not IsDBNull(item("LastUpdateDate")) Then
                Sanction.LastUpdateDate = item("LastUpdateDate")
            End If
            If Not IsDBNull(item("provider")) Then
                Sanction.provider = item("provider")
            End If
            If Not IsDBNull(item("sanction_list_attributes")) Then
                Sanction.sanction_list_attributes = item("sanction_list_attributes")
            End If
            If Not IsDBNull(item("sanction_list_date_range_is_approx_from_date")) Then
                Sanction.sanction_list_date_range_is_approx_from_date = item("sanction_list_date_range_is_approx_from_date")
            End If
            If Not IsDBNull(item("sanction_list_date_range_is_approx_to_date")) Then
                Sanction.sanction_list_date_range_is_approx_to_date = item("sanction_list_date_range_is_approx_to_date")
            End If
            If Not IsDBNull(item("sanction_list_date_range_valid_from")) Then
                Sanction.sanction_list_date_range_valid_from = item("sanction_list_date_range_valid_from")
            End If
            If Not IsDBNull(item("sanction_list_date_range_valid_to")) Then
                Sanction.sanction_list_date_range_valid_to = item("sanction_list_date_range_valid_to")
            End If
            If Not IsDBNull(item("sanction_list_name")) Then
                Sanction.sanction_list_name = item("sanction_list_name")
            End If

            listSanction.Add(Sanction)
        Next
        Return listSanction
    End Function

    Shared Function getPEPsByID(id As String) As List(Of GoAMLDAL.goAML_Report_Person_PEP)
        Dim listPEP As New List(Of GoAMLDAL.goAML_Report_Person_PEP)
        Dim listObjPEP As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Report_Person_PEP where PK_goAML_Report_Person_PEP_ID = '" & id & "'", Nothing)
        For Each item As DataRow In listObjPEP.Rows
            Dim PEP As New GoAMLDAL.goAML_Report_Person_PEP
            PEP.PK_goAML_Report_Person_PEP_ID = item("PK_goAML_Report_Person_PEP_ID")
            If Not IsDBNull(item("comments")) Then
                PEP.comments = item("comments")
            End If
            If Not IsDBNull(item("FK_From_Or_To")) Then
                PEP.FK_From_Or_To = item("FK_From_Or_To")
            End If
            If Not IsDBNull(item("FK_Parent_Table_ID")) Then
                PEP.FK_Parent_Table_ID = item("FK_Parent_Table_ID")
            End If
            If Not IsDBNull(item("FK_Report_ID")) Then
                PEP.FK_Report_ID = item("FK_Report_ID")
            End If
            If Not IsDBNull(item("Parent_Table_Name")) Then
                PEP.Parent_Table_Name = item("Parent_Table_Name")
            End If
            If Not IsDBNull(item("CreatedDate")) Then
                PEP.CreatedDate = item("CreatedDate")
            End If
            If Not IsDBNull(item("CreatedBy")) Then
                PEP.CreatedBy = item("CreatedBy")
            End If
            If Not IsDBNull(item("LastUpdateBy")) Then
                PEP.LastUpdateBy = item("LastUpdateBy")
            End If
            If Not IsDBNull(item("LastUpdateDate")) Then
                PEP.LastUpdateDate = item("LastUpdateDate")
            End If
            If Not IsDBNull(item("function_description")) Then
                PEP.function_description = item("function_description")
            End If
            If Not IsDBNull(item("function_name")) Then
                PEP.function_name = item("function_name")
            End If
            If Not IsDBNull(item("pep_country")) Then
                PEP.pep_country = item("pep_country")
            End If
            If Not IsDBNull(item("pep_date_range_is_approx_from_date")) Then
                PEP.pep_date_range_is_approx_from_date = item("pep_date_range_is_approx_from_date")
            End If
            If Not IsDBNull(item("pep_date_range_is_approx_to_date")) Then
                PEP.pep_date_range_is_approx_to_date = item("pep_date_range_is_approx_to_date")
            End If
            If Not IsDBNull(item("pep_date_range_valid_from")) Then
                PEP.pep_date_range_valid_from = item("pep_date_range_valid_from")
            End If
            If Not IsDBNull(item("pep_date_range_valid_to")) Then
                PEP.pep_date_range_valid_to = item("pep_date_range_valid_to")
            End If


            listPEP.Add(PEP)
        Next
        Return listPEP
    End Function

    Shared Function getRelatedPersonsByID(id As String) As List(Of GoAMLDAL.goAML_Report_Related_Person)
        Dim listRelatedPerson As New List(Of GoAMLDAL.goAML_Report_Related_Person)
        Dim listObjRelatedPerson As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Report_Related_Person where PK_goAML_Report_Related_Person_ID = '" & id & "'", Nothing)
        For Each item As DataRow In listObjRelatedPerson.Rows
            Dim RelatedPerson As New GoAMLDAL.goAML_Report_Related_Person
            RelatedPerson.PK_goAML_Report_Related_Person_ID = item("PK_goAML_Report_Related_Person_ID")
            If Not IsDBNull(item("comments")) Then
                RelatedPerson.comments = item("comments")
            End If
            If Not IsDBNull(item("FK_From_Or_To")) Then
                RelatedPerson.FK_From_Or_To = item("FK_From_Or_To")
            End If
            If Not IsDBNull(item("FK_Parent_Table_ID")) Then
                RelatedPerson.FK_Parent_Table_ID = item("FK_Parent_Table_ID")
            End If
            If Not IsDBNull(item("FK_Report_ID")) Then
                RelatedPerson.FK_Report_ID = item("FK_Report_ID")
            End If
            If Not IsDBNull(item("Parent_Table_Name")) Then
                RelatedPerson.Parent_Table_Name = item("Parent_Table_Name")
            End If
            If Not IsDBNull(item("CreatedDate")) Then
                RelatedPerson.CreatedDate = item("CreatedDate")
            End If
            If Not IsDBNull(item("CreatedBy")) Then
                RelatedPerson.CreatedBy = item("CreatedBy")
            End If
            If Not IsDBNull(item("LastUpdateBy")) Then
                RelatedPerson.LastUpdateBy = item("LastUpdateBy")
            End If
            If Not IsDBNull(item("LastUpdateDate")) Then
                RelatedPerson.LastUpdateDate = item("LastUpdateDate")
            End If
            If Not IsDBNull(item("alias")) Then
                RelatedPerson.alias = item("alias")
            End If
            If Not IsDBNull(item("birthdate")) Then
                RelatedPerson.birthdate = item("birthdate")
            End If
            If Not IsDBNull(item("birth_place")) Then
                RelatedPerson.birth_place = item("birth_place")
            End If
            If Not IsDBNull(item("date_deceased")) Then
                RelatedPerson.date_deceased = item("date_deceased")
            End If
            If Not IsDBNull(item("deceased")) Then
                RelatedPerson.deceased = item("deceased")
            End If
            If Not IsDBNull(item("first_name")) Then
                RelatedPerson.first_name = item("first_name")
            End If
            If Not IsDBNull(item("full_name_frn")) Then
                RelatedPerson.full_name_frn = item("full_name_frn")
            End If
            If Not IsDBNull(item("gender")) Then
                RelatedPerson.gender = item("gender")
            End If
            If Not IsDBNull(item("id_number")) Then
                RelatedPerson.id_number = item("id_number")
            End If
            If Not IsDBNull(item("is_protected")) Then
                RelatedPerson.is_protected = item("is_protected")
            End If
            If Not IsDBNull(item("last_name")) Then
                RelatedPerson.last_name = item("last_name")
            End If
            If Not IsDBNull(item("middle_name")) Then
                RelatedPerson.middle_name = item("middle_name")
            End If
            If Not IsDBNull(item("mothers_name")) Then
                RelatedPerson.mothers_name = item("mothers_name")
            End If
            If Not IsDBNull(item("nationality1")) Then
                RelatedPerson.nationality1 = item("nationality1")
            End If
            If Not IsDBNull(item("nationality2")) Then
                RelatedPerson.nationality2 = item("nationality2")
            End If
            If Not IsDBNull(item("nationality3")) Then
                RelatedPerson.nationality3 = item("nationality3")
            End If
            If Not IsDBNull(item("occupation")) Then
                RelatedPerson.occupation = item("occupation")
            End If
            If Not IsDBNull(item("passport_country")) Then
                RelatedPerson.passport_country = item("passport_country")
            End If
            If Not IsDBNull(item("passport_number")) Then
                RelatedPerson.passport_number = item("passport_number")
            End If
            If Not IsDBNull(item("person_person_relation")) Then
                RelatedPerson.person_person_relation = item("person_person_relation")
            End If
            If Not IsDBNull(item("prefix")) Then
                RelatedPerson.prefix = item("prefix")
            End If
            If Not IsDBNull(item("relation_date_range_is_approx_from_date")) Then
                RelatedPerson.relation_date_range_is_approx_from_date = item("relation_date_range_is_approx_from_date")
            End If
            If Not IsDBNull(item("relation_date_range_is_approx_to_date")) Then
                RelatedPerson.relation_date_range_is_approx_to_date = item("relation_date_range_is_approx_to_date")
            End If
            If Not IsDBNull(item("relation_date_range_valid_from")) Then
                RelatedPerson.relation_date_range_valid_from = item("relation_date_range_valid_from")
            End If
            If Not IsDBNull(item("relation_date_range_valid_to")) Then
                RelatedPerson.relation_date_range_valid_to = item("relation_date_range_valid_to")
            End If
            If Not IsDBNull(item("residence")) Then
                RelatedPerson.residence = item("residence")
            End If
            If Not IsDBNull(item("residence_since")) Then
                RelatedPerson.residence_since = item("residence_since")
            End If
            If Not IsDBNull(item("ssn")) Then
                RelatedPerson.ssn = item("ssn")
            End If
            If Not IsDBNull(item("tax_number")) Then
                RelatedPerson.tax_number = item("tax_number")
            End If
            If Not IsDBNull(item("tax_reg_number")) Then
                RelatedPerson.tax_reg_number = item("tax_reg_number")
            End If
            If Not IsDBNull(item("title")) Then
                RelatedPerson.title = item("title")
            End If



            listRelatedPerson.Add(RelatedPerson)
        Next
        Return listRelatedPerson
    End Function

    Shared Function getRelatedEntitysByID(id As String) As List(Of GoAMLDAL.goAML_Report_Related_Entities)
        Dim listRelatedEntity As New List(Of GoAMLDAL.goAML_Report_Related_Entities)
        Dim listObjRelatedEntity As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Report_Related_Entities where PK_goAML_Report_Related_Entities_ID = '" & id & "'", Nothing)
        For Each item As DataRow In listObjRelatedEntity.Rows
            Dim RelatedEntity As New GoAMLDAL.goAML_Report_Related_Entities
            RelatedEntity.PK_goAML_Report_Related_Entities_ID = item("PK_goAML_Report_Related_Entities_ID")
            If Not IsDBNull(item("comments")) Then
                RelatedEntity.comments = item("comments")
            End If
            If Not IsDBNull(item("FK_From_Or_To")) Then
                RelatedEntity.FK_From_Or_To = item("FK_From_Or_To")
            End If
            If Not IsDBNull(item("FK_Parent_Table_ID")) Then
                RelatedEntity.FK_Parent_Table_ID = item("FK_Parent_Table_ID")
            End If
            If Not IsDBNull(item("FK_Report_ID")) Then
                RelatedEntity.FK_Report_ID = item("FK_Report_ID")
            End If
            If Not IsDBNull(item("Parent_Table_Name")) Then
                RelatedEntity.Parent_Table_Name = item("Parent_Table_Name")
            End If
            If Not IsDBNull(item("CreatedDate")) Then
                RelatedEntity.CreatedDate = item("CreatedDate")
            End If
            If Not IsDBNull(item("CreatedBy")) Then
                RelatedEntity.CreatedBy = item("CreatedBy")
            End If
            If Not IsDBNull(item("LastUpdateBy")) Then
                RelatedEntity.LastUpdateBy = item("LastUpdateBy")
            End If
            If Not IsDBNull(item("LastUpdateDate")) Then
                RelatedEntity.LastUpdateDate = item("LastUpdateDate")
            End If
            If Not IsDBNull(item("business")) Then
                RelatedEntity.business = item("business")
            End If
            If Not IsDBNull(item("business_closed")) Then
                RelatedEntity.business_closed = item("business_closed")
            End If
            If Not IsDBNull(item("commercial_name")) Then
                RelatedEntity.commercial_name = item("commercial_name")
            End If
            If Not IsDBNull(item("date_business_closed")) Then
                RelatedEntity.date_business_closed = item("date_business_closed")
            End If
            If Not IsDBNull(item("entity_entity_relation")) Then
                RelatedEntity.entity_entity_relation = item("entity_entity_relation")
            End If
            If Not IsDBNull(item("entity_status")) Then
                RelatedEntity.entity_status = item("entity_status")
            End If
            If Not IsDBNull(item("entity_status_date")) Then
                RelatedEntity.entity_status_date = item("entity_status_date")
            End If
            If Not IsDBNull(item("incorporation_country_code")) Then
                RelatedEntity.incorporation_country_code = item("incorporation_country_code")
            End If
            If Not IsDBNull(item("incorporation_date")) Then
                RelatedEntity.incorporation_date = item("incorporation_date")
            End If
            If Not IsDBNull(item("incorporation_legal_form")) Then
                RelatedEntity.incorporation_legal_form = item("incorporation_legal_form")
            End If
            If Not IsDBNull(item("incorporation_number")) Then
                RelatedEntity.incorporation_number = item("incorporation_number")
            End If
            If Not IsDBNull(item("incorporation_state")) Then
                RelatedEntity.incorporation_state = item("incorporation_state")
            End If
            If Not IsDBNull(item("relation_date_range_is_approx_from_date")) Then
                RelatedEntity.relation_date_range_is_approx_from_date = item("relation_date_range_is_approx_from_date")
            End If
            If Not IsDBNull(item("relation_date_range_is_approx_to_date")) Then
                RelatedEntity.relation_date_range_is_approx_to_date = item("relation_date_range_is_approx_to_date")
            End If
            If Not IsDBNull(item("relation_date_range_valid_from")) Then
                RelatedEntity.relation_date_range_valid_from = item("relation_date_range_valid_from")
            End If
            If Not IsDBNull(item("relation_date_range_valid_to")) Then
                RelatedEntity.relation_date_range_valid_to = item("relation_date_range_valid_to")
            End If
            If Not IsDBNull(item("share_percentage")) Then
                RelatedEntity.share_percentage = item("share_percentage")
            End If
            If Not IsDBNull(item("tax_number")) Then
                RelatedEntity.tax_number = item("tax_number")
            End If
            If Not IsDBNull(item("tax_reg_number")) Then
                RelatedEntity.tax_reg_number = item("tax_reg_number")
            End If


            listRelatedEntity.Add(RelatedEntity)
        Next
        Return listRelatedEntity
    End Function

    Shared Function getRelatedAccountysByID(id As String) As List(Of GoAMLDAL.goAML_Report_Related_Account)
        Dim listRelatedAccount As New List(Of GoAMLDAL.goAML_Report_Related_Account)
        Dim listObjRelatedAccount As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Report_Related_Account where PK_goAML_Report_Related_Account_ID = '" & id & "'", Nothing)
        For Each item As DataRow In listObjRelatedAccount.Rows
            Dim RelatedAccount As New GoAMLDAL.goAML_Report_Related_Account
            RelatedAccount.PK_goAML_Report_Related_Account_ID = item("PK_goAML_Report_Related_Account_ID")
            If Not IsDBNull(item("comments")) Then
                RelatedAccount.comments = item("comments")
            End If
            If Not IsDBNull(item("FK_From_Or_To")) Then
                RelatedAccount.FK_From_Or_To = item("FK_From_Or_To")
            End If
            If Not IsDBNull(item("FK_Parent_Table_ID")) Then
                RelatedAccount.FK_Parent_Table_ID = item("FK_Parent_Table_ID")
            End If
            If Not IsDBNull(item("FK_Report_ID")) Then
                RelatedAccount.FK_Report_ID = item("FK_Report_ID")
            End If
            If Not IsDBNull(item("Parent_Table_Name")) Then
                RelatedAccount.Parent_Table_Name = item("Parent_Table_Name")
            End If
            If Not IsDBNull(item("CreatedDate")) Then
                RelatedAccount.CreatedDate = item("CreatedDate")
            End If
            If Not IsDBNull(item("CreatedBy")) Then
                RelatedAccount.CreatedBy = item("CreatedBy")
            End If
            If Not IsDBNull(item("LastUpdateBy")) Then
                RelatedAccount.LastUpdateBy = item("LastUpdateBy")
            End If
            If Not IsDBNull(item("LastUpdateDate")) Then
                RelatedAccount.LastUpdateDate = item("LastUpdateDate")
            End If
            If Not IsDBNull(item("account")) Then
                RelatedAccount.account = item("account")
            End If
            If Not IsDBNull(item("account_account_relation")) Then
                RelatedAccount.account_account_relation = item("account_account_relation")
            End If
            If Not IsDBNull(item("account_category")) Then
                RelatedAccount.account_category = item("account_category")
            End If
            If Not IsDBNull(item("account_name")) Then
                RelatedAccount.account_name = item("account_name")
            End If
            If Not IsDBNull(item("account_type")) Then
                RelatedAccount.account_type = item("account_type")
            End If
            If Not IsDBNull(item("balance")) Then
                RelatedAccount.balance = item("balance")
            End If
            If Not IsDBNull(item("beneficiary")) Then
                RelatedAccount.beneficiary = item("beneficiary")
            End If
            If Not IsDBNull(item("beneficiary_comment")) Then
                RelatedAccount.beneficiary_comment = item("beneficiary_comment")
            End If
            If Not IsDBNull(item("branch")) Then
                RelatedAccount.branch = item("branch")
            End If
            If Not IsDBNull(item("client_number")) Then
                RelatedAccount.client_number = item("client_number")
            End If
            If Not IsDBNull(item("closed_date")) Then
                RelatedAccount.closed_date = item("closed_date")
            End If
            If Not IsDBNull(item("collection_account")) Then
                RelatedAccount.collection_account = item("collection_account")
            End If
            If Not IsDBNull(item("currency_code")) Then
                RelatedAccount.currency_code = item("currency_code")
            End If
            If Not IsDBNull(item("date_balance")) Then
                RelatedAccount.date_balance = item("date_balance")
            End If
            If Not IsDBNull(item("iban")) Then
                RelatedAccount.iban = item("iban")
            End If
            If Not IsDBNull(item("institution_code")) Then
                RelatedAccount.institution_code = item("institution_code")
            End If
            If Not IsDBNull(item("institution_country")) Then
                RelatedAccount.institution_country = item("institution_country")
            End If
            If Not IsDBNull(item("institution_name")) Then
                RelatedAccount.institution_name = item("institution_name")
            End If
            If Not IsDBNull(item("non_bank_institution")) Then
                RelatedAccount.non_bank_institution = item("non_bank_institution")
            End If
            If Not IsDBNull(item("opened_date")) Then
                RelatedAccount.opened_date = item("opened_date")
            End If
            If Not IsDBNull(item("relation_date_range_is_approx_from_date")) Then
                RelatedAccount.relation_date_range_is_approx_from_date = item("relation_date_range_is_approx_from_date")
            End If
            If Not IsDBNull(item("relation_date_range_is_approx_to_date")) Then
                RelatedAccount.relation_date_range_is_approx_to_date = item("relation_date_range_is_approx_to_date")
            End If
            If Not IsDBNull(item("relation_date_range_valid_from")) Then
                RelatedAccount.relation_date_range_valid_from = item("relation_date_range_valid_from")
            End If
            If Not IsDBNull(item("relation_date_range_valid_to")) Then
                RelatedAccount.relation_date_range_valid_to = item("relation_date_range_valid_to")
            End If
            If Not IsDBNull(item("status_code")) Then
                RelatedAccount.status_code = item("status_code")
            End If
            If Not IsDBNull(item("status_date")) Then
                RelatedAccount.status_date = item("status_date")
            End If
            If Not IsDBNull(item("swift")) Then
                RelatedAccount.swift = item("swift")
            End If


            listRelatedAccount.Add(RelatedAccount)
        Next
        Return listRelatedAccount
    End Function
#End Region

End Class