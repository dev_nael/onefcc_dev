﻿Imports System.Collections.Generic
Imports GoAMLDAL
Public Class GenerateSARData
    Public objGenerateSAR As New goAML_ODM_Generate_STR_SAR
    'Public listObjGenerateSARTransaction As New List(Of goAML_ODM_Generate_STR_SAR_Transaction)
    Public listObjGenerateSARTransaction As New List(Of goAML_ODM_Generate_STR_SAR_Transaction_withSuspected) '' Edit 15-May-2023
    Public listObjDokumen As New List(Of goAML_ODM_Generate_STR_SAR_Attachment)

    Partial Public Class goAML_ODM_Generate_STR_SAR_Transaction_withSuspected
        Public Property Pk_goAML_ODM_Generate_STR_SAR_Transaction As Integer
        Public Property Fk_goAML_Generate_STR_SAR As Nullable(Of Integer)
        Public Property Date_Transaction As Nullable(Of Date)
        Public Property CIF_NO As String
        Public Property Account_NO As String
        Public Property WIC_NO As String
        Public Property Ref_Num As String
        Public Property Debit_Credit As String
        Public Property Original_Amount As Nullable(Of Double)
        Public Property Currency As String
        Public Property Exchange_Rate As Nullable(Of Double)
        Public Property IDR_Amount As Nullable(Of Double)
        Public Property Transaction_Code As String
        Public Property Source_Data As String
        Public Property Transaction_Remark As String
        Public Property Transaction_Number As String
        Public Property Transaction_Location As String
        Public Property Teller As String
        Public Property Authorized As String
        Public Property Date_Posting As Nullable(Of Date)
        Public Property Transmode_Code As String
        Public Property Transmode_Comment As String
        Public Property Comments As String
        Public Property CIF_No_Lawan As String
        Public Property ACCOUNT_No_Lawan As String
        Public Property WIC_No_Lawan As String
        Public Property Conductor_ID As String
        Public Property Swift_Code_Lawan As String
        Public Property country_code_lawan As String
        Public Property MsgTypeSwift As String
        Public Property From_Funds_Code As String
        Public Property To_Funds_Code As String
        Public Property country_code As String
        Public Property Currency_Lawan As String
        Public Property BiMultiParty As Nullable(Of Integer)
        Public Property GCN As String
        Public Property GCN_Lawan As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
        Public Property Business_date As Nullable(Of Date)
        Public Property From_Type As Nullable(Of Integer)
        Public Property To_Type As Nullable(Of Integer)

        Public Property address_type As String
        Public Property address As String
        Public Property town As String
        Public Property city As String
        Public Property zip As String
        Public Property trn_addr_country_code As String
        Public Property state As String
        Public Property trn_addr_comments As String
        Public Property AGENT_NAME As String
        Public Property TRANSACTION_TYPE_CODE As String
        Public Property TRANSACTION_STATUS As String


        Public Property Is_Customer_Suspected As Nullable(Of Boolean)
        Public Property Is_Counter_Party_Suspected As Nullable(Of Boolean)
        Public Property Is_Conductor_Suspected As Nullable(Of Boolean)

        Public Property IS_SUSPECTED_FROM As Nullable(Of Boolean)
        Public Property IS_SUSPECTED_TO As Nullable(Of Boolean)
        Public Property IS_SUSPECTED_CONDUCTOR As Nullable(Of Boolean)

    End Class
End Class
