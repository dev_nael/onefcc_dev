Using objDba As New GoAMLEntities
    Dim header As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)

    ' AuditTrailDetail, NaelEdit
    ' 2023-09-29, Nael: Menambahkan Audit Trail
    ' CreateAuditTrailDetailEdit
    ' objData
    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, objData, objData2)
    objDba.SaveChanges()

    ' ============ Customer Phones ============
    If objListgoAML_Ref_Phone.Count > 0 Then
        ' 2023-10-02, Nael: Cek data new di old
        For Each newUserPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
            Dim oldUserPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newUserPhone.PK_goAML_Ref_Phone))
            If oldUserPhone IsNot Nothing Then
                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserPhone, oldUserPhone)
            Else
                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserPhone)
            End If
            objDba.SaveChanges()
        Next

        ' 2023-10-02, Nael: Cek data old di new
        For Each oldUserPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
            Dim newUserPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldUserPhone.PK_goAML_Ref_Phone))
            If newUserPhone IsNot Nothing Then
                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserPhone, oldUserPhone)
            Else
                NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserPhone)
            End If
            objDba.SaveChanges()
        Next
    End If

    ' ========== Customer Address ===========
    If objListgoAML_Ref_Address.Count > 0 Then
        ' 2023-10-02, Nael: Cek data new di old
        For Each newUserAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Address
            Dim oldUserAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newUserAddress.PK_Customer_Address_ID))
            If oldUserAddress IsNot Nothing Then
                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserAddress, oldUserAddress)
            Else
                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserAddress)
            End If
            objDba.SaveChanges()
        Next

        ' 2023-10-02, Nael: Cek data old di new
        For Each oldUserAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Address
            Dim newUserAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldUserAddress.PK_Customer_Address_ID))
            If newUserAddress IsNot Nothing Then
                NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserAddress, oldUserAddress)
            Else
                NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserAddress)
            End If
            objDba.SaveChanges()
        Next
    End If

    If objData.FK_Customer_Type_ID = 1 Then
        ' ======== Person Identification =========
        If objListgoAML_Ref_Identification.Count > 0 Then
            ' 2023-10-02, Nael: Cek data new di old
            For Each newUserIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                Dim oldUserIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(newUserIdentification.PK_Person_Identification_ID))
                If oldUserIdentification IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserIdentification, oldUserIdentification)
                Else
                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newUserIdentification)
                End If
                objDba.SaveChanges()
            Next

            ' 2023-10-02, Nael: Cek data old di new
            For Each oldUserIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                Dim newUserIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(oldUserIdentification.PK_Person_Identification_ID))
                If newUserIdentification IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newUserIdentification, oldUserIdentification)
                Else
                    NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldUserIdentification)
                End If
                objDba.SaveChanges()
            Next
        End If

        ' ======== Employer Address ==========
        If objListgoAML_Ref_Employer_Address.Count > 0 Then
            ' 2023-10-02, Nael: Cek data new di old
            For Each newEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                Dim oldEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newEmployerAddress.PK_Customer_Address_ID))
                If oldEmployerAddress IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerAddress, oldEmployerAddress)
                Else
                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newEmployerAddress)
                End If
                objDba.SaveChanges()
            Next

            ' 2023-10-02, Nael: Cek data old di new
            For Each oldEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                Dim newEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(oldEmployerAddress.PK_Customer_Address_ID))
                If newEmployerAddress IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerAddress, oldEmployerAddress)
                Else
                    NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldEmployerAddress)
                End If
                objDba.SaveChanges()
            Next
        End If

        ' ======= Employer Phone =======
        If objListgoAML_Ref_Employer_Phone.Count > 0 Then
            ' 2023-10-02, Nael: Cek data new di old
            For Each newEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                Dim oldEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newEmployerPhone.PK_goAML_Ref_Phone))
                If oldEmployerPhone IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerPhone, oldEmployerPhone)
                Else
                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newEmployerPhone)
                End If
                objDba.SaveChanges()
            Next

            ' 2023-10-02, Nael: Cek data old di new
            For Each oldEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                Dim newEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(oldEmployerPhone.PK_goAML_Ref_Phone))
                If newEmployerPhone IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newEmployerPhone, oldEmployerPhone)
                Else
                    NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldEmployerPhone)
                End If
                objDba.SaveChanges()
            Next
        End If
    End If

    If objData.FK_Customer_Type_ID = 2 Then
        If objListgoAML_Ref_Director_Employer_Address.Count > 0 Then
            ' 2023-10-02, Nael: Cek data new di old
            For Each newDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                Dim oldDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorEmployerAddress.PK_Customer_Address_ID))
                If newDirectorEmployerAddress IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress, oldDirectorEmployerAddress)
                Else
                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress)
                End If
                objDba.SaveChanges()
            Next

            ' 2023-10-02, Nael: Cek data old di new
            For Each oldDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                Dim newDirectorEmployerAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Employer_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorEmployerAddress.PK_Customer_Address_ID))
                If newDirectorEmployerAddress IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerAddress, oldDirectorEmployerAddress)
                Else
                    NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorEmployerAddress)
                End If
                objDba.SaveChanges()
            Next
        End If

        If objListgoAML_Ref_Director_Employer_Phone.Count > 0 Then
            ' 2023-10-02, Nael: Cek data new di old
            For Each newDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                Dim oldDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorEmployerPhone.PK_goAML_Ref_Phone))
                If newDirectorEmployerPhone IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone, oldDirectorEmployerPhone)
                Else
                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone)
                End If
                objDba.SaveChanges()
            Next

            ' 2023-10-02, Nael: Cek data old di new
            For Each oldDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                Dim newDirectorEmployerPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorEmployerPhone.PK_goAML_Ref_Phone))
                If newDirectorEmployerPhone IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorEmployerPhone, oldDirectorEmployerPhone)
                Else
                    NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorEmployerPhone)
                End If
                objDba.SaveChanges()
            Next
        End If

        If objListgoAML_Ref_Director_Address.Count > 0 Then
            ' 2023-10-02, Nael: Cek data new di old
            For Each newDirectorAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                Dim oldDirectorAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorAddress.PK_Customer_Address_ID))
                If newDirectorAddress IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorAddress, oldDirectorAddress)
                Else
                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorAddress)
                End If
                objDba.SaveChanges()
            Next

            ' 2023-10-02, Nael: Cek data old di new
            For Each oldDirectorAddress As GoAMLDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                Dim newDirectorAddress As GoAMLDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Address.FirstOrDefault(Function(x) x.PK_Customer_Address_ID.Equals(newDirectorAddress.PK_Customer_Address_ID))
                If newDirectorAddress IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorAddress, oldDirectorAddress)
                Else
                    NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorAddress)
                End If
                objDba.SaveChanges()
            Next
        End If

        If objListgoAML_Ref_Director_Phone.Count > 0 Then
            For Each newDirectorPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                Dim oldDirectorPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorPhone.PK_goAML_Ref_Phone))
                If newDirectorPhone IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorPhone, oldDirectorPhone)
                Else
                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorPhone)
                End If
                objDba.SaveChanges()
            Next

            ' 2023-10-02, Nael: Cek data old di new
            For Each oldDirectorPhone As GoAMLDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                Dim newDirectorPhone As GoAMLDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.FirstOrDefault(Function(x) x.PK_goAML_Ref_Phone.Equals(newDirectorPhone.PK_goAML_Ref_Phone))
                If newDirectorPhone IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorPhone, oldDirectorPhone)
                Else
                    NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorPhone)
                End If
                objDba.SaveChanges()
            Next
        End If

        If objListgoAML_Ref_Identification_Director.Count > 0 Then
            ' 2023-10-02, Nael: Cek data new di old
            For Each newDirectorIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                Dim oldDirectorIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification_Director.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(newDirectorIdentification.PK_Person_Identification_ID))
                If newDirectorIdentification IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorIdentification, oldDirectorIdentification)
                Else
                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, newDirectorIdentification)
                End If
                objDba.SaveChanges()
            Next

            ' 2023-10-02, Nael: Cek data old di new
            For Each oldDirectorIdentification As GoAMLDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                Dim newDirectorIdentification As GoAMLDAL.goAML_Person_Identification = objListgoAML_Ref_Identification_Director.FirstOrDefault(Function(x) x.PK_Person_Identification_ID.Equals(newDirectorIdentification.PK_Person_Identification_ID))
                If newDirectorIdentification IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailEdit(objDba, header.PK_AuditTrail_ID, newDirectorIdentification, oldDirectorIdentification)
                Else
                    NawaFramework.CreateAuditTrailDetailDelete(objDba, header.PK_AuditTrail_ID, oldDirectorIdentification)
                End If
                objDba.SaveChanges()
            Next
        End If

    End If

    objDba.SaveChanges() ' 2023-09-29, Nael: SaveChanges
End Using